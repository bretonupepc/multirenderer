using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Xml;
using Breton.MathUtils;
using TraceLoggers;

namespace Breton.Polygons
{
    public class Path
    {
        #region Variables

        private const string sInnerNameKey = "__INTERNAL_NAME_";
        private static UInt64 ui64InstanceCount = 0;

        public enum VertexType
        {
            VertexErr = -1,			                // verifica non possibile
            VertexOpen = 0,			                // vertice aperto
            VertexConv = 1,			                // vertice convesso
            VertexConc = 2,			                // vertice concavo
            VertexAll = 3			                // punti allineati
        }

        public enum EN_PATH_MODE
        {
            EN_PATH_MODE_EXTERIOR,
            EN_PATH_MODE_INTERIOR,
            EN_PATH_MODE_UNKNOW
        };

        public enum EN_ARC_DISCRETIZE_MODE
        {
            INTERNAL,                               // discretizza nella parte interna dell'arco
            EXTERNAL,                               // discretizza nella parte esterna dell'arco
            RIGHT,                                  // discretizza a destra dell'arco
            LEFT                                    // discretizza a sinistra dell'arco
        }

        protected List<Side> mSides;                    // array di lati componenti il percorso
        private RTMatrix mMatrixRT;					    // matrice di rototraslazione
        private Point mPtInit;						    // punto iniziale
        private Boolean mAllowSelect;                   // abilita/disabilita selezione in modalita' disegno
        private string mLayer;                          // nome del layer a cui e' associato il percorso

        private SortedList<string, string> mAttributes;	// Lista degli attributi del path

        private EN_PATH_MODE enPathMode;                // indica se il percorso e' interno, esterno o non definito

        public static bool IsClosedClosesPath = true;   // property IsClosed modifica il percorso se first.P1.AlmostEqual(last.P2) e ritorna true. Funzionamento di default, plugins rhinoceros impostano a false per evitare modifica di mPath al get di IsClosed

        public static bool AddedPathInClonedZoneCCW_AllZonesContainedCW = true; // funzionamento di default a true: nel costruttore copia, quando si aggiunge una zona interna, si setta orientazione CCW per il path rappresentato dalla zona da aggiungere. I plugins di rhinoceros invece impostano a false; in questo modo si setta CCW solo per il path della zona esterna e la zona interna ha orientazione opposta a quella della zona container

        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso vuoto
        /// </summary>
        ///*********************************************************************
        public Path()
        {
            mSides = new List<Side>();
            mPtInit = null;
            mMatrixRT = new RTMatrix();
            mAllowSelect = false;
            mLayer = "";
            mAttributes = new SortedList<string, string>();
            enPathMode = EN_PATH_MODE.EN_PATH_MODE_UNKNOW;
            Name = "INST_" + (ui64InstanceCount++).ToString(CultureInfo.InvariantCulture);
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public Path(Path p)
            : this()
        {
            if (p == null)
                return;

            mAllowSelect = p.mAllowSelect;
            mLayer = p.mLayer;

            if (p.Count == 0)
                return;

            // Aggiunge tutti i lati del percorso
            for (int i = 0; i < p.Count; i++)
                AddSide(p.GetSide(i));

            mMatrixRT = new RTMatrix(p.MatrixRT);

            foreach (KeyValuePair<string, string> a in p.mAttributes)
                this[a.Key] = a.Value;

            enPathMode = p.enPathMode;
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il poligono di un rettangolo
        /// </summary>
        /// <param name="r">rettangolo</param>
        ///*********************************************************************
        public Path(Rect r)
            : this()
        {
            if (r != null)
            {
                AddVertex(r.Left, r.Top);
                AddVertex(r.Right, r.Top);
                AddVertex(r.Right, r.Bottom);
                AddVertex(r.Left, r.Bottom);
                Close();
            }
            mAllowSelect = false;
            mLayer = "";
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il poligono aperto di una lista di punti
        /// </summary>
        /// <param name="r">lista di punti</param>
        ///*********************************************************************
        public Path(IEnumerable<Breton.Polygons.Point> l)
            : this()
        {
            if (l != null)
            {
                foreach (var p in l)
                    AddVertex(p);
            }
            mAllowSelect = false;
            mLayer = "";
        }

        //Distruttore
        ~Path()
        {
            Empty();
            mSides = null;
        }

        #endregion

        #region Properties

        //**************************************************************************
        // IsClosed
        // Ritorna lo stato chiuso/aperto di un percorso
        //**************************************************************************
        public bool IsClosed
        {
            get
            {
                //Nessun lato
                if (mSides.Count == 0)
                    return false;

                //Legge il primo lato
                Side first = mSides[0];

                //Lato unico
                if (mSides.Count == 1)
                    //Se ha un solo lato, verifica se si tratta di una circonferenza completa
                    return (first.IsClosed);

                //Legge l'ultimo lato
                Side last = mSides[mSides.Count - 1];

                //Controlla se il percorso � chiuso
                if (first.P1 == last.P2)
                    return true;
                if (!first.P1.AlmostEqual(last.P2))
                    return false;

                if (IsClosedClosesPath)
                    AddVertex(first.P1);

                return true;
            }
        }

        //**************************************************************************
        // Count
        // Ritorna il numero di lati
        //**************************************************************************
        public int Count
        {
            get { return mSides.Count; }
        }

        ///*************************************************************************
        /// <summary>
        /// Ritorna il numero dei vertici
        /// </summary>
        ///*************************************************************************
        public int VertexCount
        {
            get { return mSides.Count + 1; }
        }

        //**************************************************************************
        // MatrixRT
        // Ritorna la matrice di rototraslazione
        //**************************************************************************
        public RTMatrix MatrixRT
        {
            set { mMatrixRT = value; }
            get { return mMatrixRT; }
        }

        //**************************************************************************
        // RotAngle
        // Ritorna l'angolo di rotazione del percorso
        //**************************************************************************
        public double RotAngle
        {
            get
            {
                double offsetX = 0d, offsetY = 0d, angle = 0d;

                mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);

                return angle;
            }
        }

        //**************************************************************************
        // TranslPoint
        // Ritorna il punto di traslazione del percorso
        //**************************************************************************
        public Point TranslPoint
        {
            get
            {
                double offsetX = 0d, offsetY = 0d, angle = 0d;

                mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);

                return new Point(offsetX, offsetY);
            }
        }

        //**************************************************************************
        // Surface
        // Restituisce l'area del percorso (senza segno)
        //**************************************************************************
        public double Surface
        {
            get { return Math.Abs(SurfaceSign); }
        }

        //**************************************************************************
        // SurfaceSign
        // Restituisce l'area del percorso (con segno)
        //**************************************************************************
        public double SurfaceSign
        {
            get
            {
                // Verifica che abbia dei lati
                if (mSides.Count == 0)
                    return 0d;

                // Verifica che il percorso sia chiuso
                if (!IsClosed)
                    return 0d;

                double area2 = 0d;

                // Preleva il primo lato
                Side lato1 = mSides[0];
                Side lato2;

                //Aggiunge l'area dell'eventuale arco
                area2 += lato1.Surface * 2d;

                //Calcola l'area di tutti i triangoli componenti il percorso
                for (int i = 1; i < mSides.Count; i++)
                {
                    lato2 = mSides[i];
                    area2 += lato1.P1.Surface2(lato2.P1, lato2.P2);

                    //Aggiunge l'area dell'eventuale arco
                    area2 += lato2.Surface * 2d;
                }

                // Ritorna l'area
                return area2 / 2d;
            }
        }

        //**************************************************************************
        // SenseOfRotation
        // Restituisce il senso di rotazione del percorso
        //**************************************************************************
        public MathUtil.RotationSense SenseOfRotation
        {
            get
            {
                //Controlla che il percorso sia chiuso
                if (!IsClosed)
                    return MathUtil.RotationSense.ALIGNED;

                //Calcola l'area
                double area = SurfaceSign;

                if (area < 0d)
                    return MathUtil.RotationSense.CW;

                if (area > 0d)
                    return MathUtil.RotationSense.CCW;

                return MathUtil.RotationSense.ALIGNED;
            }
        }

        //**************************************************************************
        // IsCCW
        // Restituisce TRUE se il senso di rotazione del percorso � antiorario
        //**************************************************************************
        public bool IsCCW
        {
            get { return SenseOfRotation == MathUtil.RotationSense.CCW; }
        }

        //**************************************************************************
        // IsCW
        // Restituisce TRUE se il senso di rotazione del percorso � orario
        //**************************************************************************
        public bool IsCW
        {
            get { return SenseOfRotation == MathUtil.RotationSense.CW; }
        }

        //**************************************************************************
        // AllowSelect
        // Permette o nega la selezionabilita' del percorso in modalita' grafica
        //**************************************************************************
        public bool AllowSelect
        {
            set { mAllowSelect = value; }
            get { return mAllowSelect; }
        }

        //**************************************************************************
        // Layer
        // Definisce il layer su cui il path deve essere disegnato
        //**************************************************************************
        public string Layer
        {
            set { mLayer = value; }
            get { return mLayer; }
        }

        //**************************************************************************
        // Attributes
        // Restituisce la lista degli attributi del path
        //**************************************************************************
        public SortedList<string, string> Attributes
        {
            get { return mAttributes; }
        }

        //**************************************************************************
        // Type
        // Imposta o Ritorna il tipo di percorso (Interno/Esterno/NonDefinito
        //**************************************************************************
        public virtual EN_PATH_MODE Type
        {
            get { return enPathMode; }
            set { enPathMode = value; }
        }

        //**************************************************************************
        // []
        // operatore parantesi quadre per accedere direttamente alla lista sides
        //**************************************************************************
        public virtual Side this[int i]
        {
            get { return mSides[i]; }
        }

        //**************************************************************************
        // []
        // operatore parantesi quadre per accedere direttamente alla lista degli attributi
        //**************************************************************************
        public virtual string this[string key]
        {
            get
            {
                if (mAttributes.ContainsKey(key))
                    return mAttributes[key];
                else
                    return "";
            }
            set
            {
                if (mAttributes.ContainsKey(key))
                    mAttributes[key] = value;
                else
                    mAttributes.Add(key, value);
            }
        }

        ///**************************************************************************
        /// Name
        /// Imposta o Ritorna il nome del percorso
        ///**************************************************************************
        public string Name
        {
            get { return this[sInnerNameKey]; }
            set { this[sInnerNameKey] = value; }
        }

        //**************************************************************************
        // Length
        // ritorna la lunghezza complessiva del percorso
        //**************************************************************************
        public double Length
        {
            get
            {
                double length = 0;
                for (int i = 0; i < Count; i++)
                    length += this[i].Length;
                return length;
            }
        }

        //**************************************************************************
        // IsClosed
        // Ritorna lo stato chiuso/aperto di un percorso senza modificarlo
        //**************************************************************************
        public bool CheckIfIsClosed
        {
            get
            {
                //Nessun lato
                if (mSides.Count == 0)
                    return false;

                //Legge il primo lato
                Side first = mSides[0];

                //Lato unico
                if (mSides.Count == 1)
                    //Se ha un solo lato, verifica se si tratta di una circonferenza completa
                    return (first.IsClosed);

                //Legge l'ultimo lato
                Side last = mSides[mSides.Count - 1];

                //Controlla se il percorso � chiuso
                if (first.P1 == last.P2)
                    return true;
                if (!first.P1.AlmostEqual(last.P2))
                    return false;

                return true;
            }
        }

        //**************************************************************************
        // IsCircle: set di archi che uniti formano una circonferenza
        // Ritorna lo stato chiuso/aperto di un percorso senza modificarlo
        //**************************************************************************
        public bool CheckIfIsCircle
        {
            get
            {
                if(!CheckIfIsClosed)
                    return false;

                Point center = null;
                double radius = -1;
                for (int i = 0; i < mSides.Count; i++)
                {
                    var s = mSides[i];
                    if (s is Arc)
                    {
                        var a = s as Arc;
                        if (radius == -1)
                            radius = a.Radius;
                        else if (!MathUtil.QuoteIsEQ(radius, a.Radius, MathUtil.QUOTE_EPSILON))
                            return false;

                        if (center == null)
                            center = a.CenterPoint;
                        else if (!center.IsDistanceLesserThan(a.CenterPoint, MathUtil.QUOTE_EPSILON))
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion

        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     Inserisce o modifica il valore di un attributo
        /// </summary>
        /// <param name="key">
        ///     chiave dell'attributo
        /// </param>
        /// <param name="value">
        ///     valore dell'attributo
        /// </param>
        ///**************************************************************************
        public void AttributeAddOrUpdate( string key, string value )
        {
            if (Attributes.ContainsKey( key ))
                Attributes[key] = value;
            else
                Attributes.Add( key, value );
        }

        //**************************************************************************
        // Clear
        // Clear di tutti i sides
        //**************************************************************************
        public void Clear()
        {
            for (int i = 0; i < Count; i++)
                this.RemoveSide(i);
        }

        //**************************************************************************
        // AddSide
        // Aggiunge un lato al percorso
        // Parametri:
        //			s	: lato da aggiungere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int AddSide(Side s)
        {
            mSides.Add(s.GetCopy());

            return mSides.Count;
        }

        //**************************************************************************
        // AddSide
        // Aggiunge un lato al percorso
        // Parametri:
        //			s	: lato da aggiungere
        //          pos : posizione di inserimento:
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int AddSide(Side s, int pos)
        {
            if (pos < 0 || pos > mSides.Count)
                return -1;
            mSides.Insert(pos, (s.GetCopy()));

            return mSides.Count;
        }

        ///***************************************************************************
        /// <summary>
        /// Aggiunge un lato, per riferimento, senza crearne una copia
        /// </summary>
        /// <param name="s">lato da aggiungere</param>
        /// <returns>Numero lati del percorso</returns>
        ///***************************************************************************
        public virtual int AddSideRef(Side s)
        {
            mSides.Add(s);

            return mSides.Count;
        }

        //**************************************************************************
        // RemoveSide
        // Rimuove un lato al percorso
        // Parametri:
        //			s	: lato da rimuovere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int RemoveSide(Side s)
        {
            if (!mSides.Contains(s))
                return mSides.Count;

            int i = mSides.IndexOf(s);

            return RemoveSide(i);
        }

        //**************************************************************************
        // RemoveSide
        // Rimuove un lato al percorso
        // Parametri:
        //			i	: indice lato da rimuovere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int RemoveSide(int i)
        {
            // Verifica l'indice
            if (i < 0 || i >= mSides.Count)
                return mSides.Count;

            // Cancella l'ultimo lato
            if (i == (mSides.Count - 1))
                mSides.RemoveAt(i);

            // Cancella il primo lato
            else if (i == 0)
                mSides.RemoveAt(i);

            // Cancella un lato intermedio
            else
            {
                // Preleva il lati precedente a quello da cancellare
                Side prev = mSides[i - 1];
                Side del = mSides[i];

                // Modifica il lato precedente
                prev = CreateSegment(prev.P1, del.P2);

                // Rimuove i vecchi lati
                mSides.RemoveRange(i - 1, 2);

                // Inserisce i lati precedente modificato
                mSides.Insert(i - 1, prev);
            }

            // Ritorna il numero di lati
            return mSides.Count;
        }

        //**************************************************************************
        // DeleteSide
        // Elimina un lato al percorso; a differenza della funzione RemoveSide
        // non garantisce che il percorso risultate sia connesso.
        // Parametri:
        //			s	: lato da rimuovere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int DeleteSide(Side s)
        {
            if (!mSides.Contains(s))
                return mSides.Count;

            int i = mSides.IndexOf(s);

            return DeleteSide(i);
        }

        //**************************************************************************
        // RemoveSide
        // Elimina un lato al percorso; a differenza della funzione RemoveSide
        // non garantisce che il percorso risultate sia connesso.
        // Parametri:
        //			i	: indice lato da rimuovere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int DeleteSide(int i)
        {
            if (i < mSides.Count)
                mSides.RemoveAt(i);
            return mSides.Count;
        }

        //**************************************************************************
        // AddVertex
        // Aggiunge un vertice al percorso
        // Parametri:
        //			x	: ascissa punto da aggiungere
        //			y	: ordinata punto da aggiungere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int AddVertex(double x, double y)
        {
            return AddVertex(new Point(x, y));
        }

        //**************************************************************************
        // AddVertex
        // Aggiunge un vertice al percorso
        // Parametri:
        //			p	: punto da aggiungere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int AddVertex(Point p)
        {
            // Primo punto
            if (mSides.Count == 0 && mPtInit == null)
                mPtInit = new Point(p);

            // Secondo punto 
            else if (mSides.Count == 0)
                mSides.Add(CreateSegment(mPtInit, p));

            // Punti successivi
            else
            {
                Side last = mSides[mSides.Count - 1];
                mSides.Add(CreateSegment(last.P2, p));
            }

            // Ritorna il numero di lati
            return mSides.Count;
        }

        //**************************************************************************
        // RemoveVertex
        // Rimuove un vertice del percorso
        // Parametri:
        //			i	: indice vertice da rimuovere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int RemoveVertex(int i)
        {
            // Verifica l'indice
            if (i < 0 || i > mSides.Count || mSides.Count == 0)
                return mSides.Count;

            // Cancella l'ultimo lato
            if (i == mSides.Count)
                mSides.RemoveAt(i - 1);

                // Cancella il primo lato
            else if (i == 0)
            {
                Side prev = mSides[mSides.Count - 1];
                Side next = mSides[i];

                // Modifica i lati adiacenti
                prev = CreateSegment(prev.P1, next.P2);

                // Rimuove i vecchi lati
                mSides.RemoveAt(mSides.Count - 1);
                mSides.RemoveAt(i);

                // Inserisce i lati adiacenti modificati
                mSides.Insert(0, prev);
            }
            // Cancella un vertice intermedio
            else
            {
                // Preleva i lati adiacenti al vertice da cancellare
                Side prev = mSides[i - 1];
                Side next = mSides[i];

                // Modifica i lati adiacenti
                prev = CreateSegment(prev.P1, next.P2);

                // Rimuove i vecchi lati
                mSides.RemoveRange(i - 1, 2);

                // Inserisce i lati adiacenti modificati
                mSides.Insert(i - 1, prev);
            }

            // Ritorna il numero di lati
            return mSides.Count;
        }

        //**************************************************************************
        // ModifyVertex
        // Modifica un vertice del percorso
        // Parametri:
        //			i	: indice vertice da modificare
        //			x	: ascissa punto modificato
        //			y	: ordinata punto modificato
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public int ModifyVertex(int i, double x, double y)
        {
            return ModifyVertex(i, new Point(x, y));
        }

        //**************************************************************************
        // ModifyVertex
        // Modifica un vertice del percorso
        // Parametri:
        //			i	: indice vertice da modificare
        //			p	: punto modificato
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public virtual int ModifyVertex(int i, Point p)
        {
            Side prev, next;

            // Verifica l'indice
            if (i < 0 || i > mSides.Count || mSides.Count == 0)
                return mSides.Count;

            // Modifica l'ultimo vertice
            if (i == mSides.Count)
                // Poligono chiuso
                if (this.IsClosed)
                {
                    // Modifica l'ultimo lato
                    prev = mSides[i - 1];
                    prev = CreateSegment(prev.P1, p);
                    mSides.RemoveAt(i - 1);
                    mSides.Add(prev);
                    // Modifica il primo lato
                    next = mSides[0];
                    next = CreateSegment(p, next.P2);
                    mSides.RemoveAt(0);
                    mSides.Insert(0, next);
                }
                // Poligono aperto
                else
                {
                    // Modifica l'ultimo lato
                    prev = mSides[i - 1];
                    prev = CreateSegment(prev.P1, p);
                    mSides.RemoveAt(i - 1);
                    mSides.Add(prev);
                }


            // Modifica il primo lato
            else if (i == 0)
                // Poligono chiuso
                if (this.IsClosed)
                {
                    // Modifica il primo lato
                    next = mSides[0];
                    next = CreateSegment(p, next.P2);
                    mSides.RemoveAt(0);
                    mSides.Insert(0, next);
                    // Modifica l'ultimo lato
                    prev = mSides[i - 1];
                    prev = CreateSegment(prev.P1, p);
                    mSides.RemoveAt(i - 1);
                    mSides.Add(prev);
                }
                // Poligono aperto
                else
                {
                    // Modifica il primo lato
                    next = mSides[0];
                    next = CreateSegment(p, next.P2);
                    mSides.RemoveAt(0);
                    mSides.Insert(0, next);
                }

                // Modifica un vertice intermedio
            else
            {
                // Preleva i lati adiacenti al vertice da cancellare
                prev = mSides[i - 1];
                next = mSides[i];

                // Modifica i lati adiacenti
                prev = CreateSegment(prev.P1, p);
                next = CreateSegment(p, next.P2);

                // Rimuove i vecchi lati
                mSides.RemoveRange(i - 1, 2);

                // Inserisce i lati adiacenti modificati
                mSides.Insert(i - 1, next);
                mSides.Insert(i - 1, prev);
            }

            // Ritorna il numero di lati
            return mSides.Count;
        }

        //**************************************************************************
        // GetSide
        // Ritorna il lato specificato
        // Parametri:
        //			i	: indice lato
        // Restituisce:
        //			lato
        //**************************************************************************
        public virtual Side GetSide(int i)
        {
            if (i < 0 || i >= mSides.Count)
                return null;

            return mSides[i];
        }

        //**************************************************************************
        // GetSide successivo
        // Ritorna il lato successivo a quello specificato
        // Parametri:
        //			i	: indice lato
        // Restituisce:
        //			lato
        //**************************************************************************
        public Side GetSideNext(int i)
        {
            if (i < 0 || i >= mSides.Count || mSides.Count <= 1)
                return null;

            int iNext = i + 1;
            if (iNext == mSides.Count)
                if (CheckIfIsClosed)
                    iNext = 0;
                else
                    return null;

            return mSides[iNext];
        }
        public Side GetSideNext(Side s)
        {
            int i = mSides.IndexOf(s);
            return GetSideNext(i);
        }

        //**************************************************************************
        // GetSide precedente
        // Ritorna il lato precedente a quello specificato
        // Parametri:
        //			i	: indice lato
        // Restituisce:
        //			lato
        //**************************************************************************
        public Side GetSidePrev(int i)
        {
            if (i < 0 || i >= mSides.Count || mSides.Count <= 1)
                return null;

            int iPrev = i - 1;
            if (iPrev == -1)
                if (CheckIfIsClosed)
                    iPrev = mSides.Count - 1;
                else
                    return null;

            return mSides[iPrev];
        }
        public Side GetSidePrev(Side s)
        {
            int i = mSides.IndexOf(s);
            return GetSidePrev(i);
        }

        //**************************************************************************
        // GetSideRT
        // Ritorna il lato specificato, rototraslato
        // Parametri:
        //			i	: indice lato
        // Restituisce:
        //			lato
        //**************************************************************************
        public virtual Side GetSideRT(int i)
        {
            Side s = GetSide(i);
            if (s == null)
                return null;

            // Esegue la rototraslazione del lato
            double xt, yt, angle;
            mMatrixRT.GetRotoTransl(out xt, out yt, out angle);
            //return s.RotoTrasl(angle, xt, yt);
            return s.RotoTrasl(mMatrixRT);
        }

        //**************************************************************************
        // GetVertex
        // Ritorna il vertice specificato
        // Parametri:
        //			i	: indice lato
        //			x	: ascissa vertice ritornato
        //			y	: ordinata vertice ritornato
        // Restituisce:
        //			punto
        //**************************************************************************
        public Point GetVertex(int i, out double x, out double y)
        {
            x = 0d; y = 0d;

            Point p = GetVertex(i);
            if (p != null)
            {
                x = p.X;
                y = p.Y;
            }

            return p;
        }

        //**************************************************************************
        // GetVertex
        // Ritorna il vertice specificato
        // Parametri:
        //			i	: indice lato
        // Restituisce:
        //			punto
        //**************************************************************************
        public Point GetVertex(int i)
        {
            // Verifica l'indice
            if (i < 0 || i > mSides.Count || mSides.Count == 0)
                return null;

            Side side;
            Point p;

            if (i == mSides.Count)
            {
                side = (Side)mSides[i - 1];
                p = side.P2;
            }
            else
            {
                side = (Side)mSides[i];
                p = side.P1;
            }

            return new Point(p);
        }

        //**************************************************************************
        // GetVertexRT 
        // Ritorna il vertice specificato rototraslato
        // Parametri:
        //			i	: indice lato
        //			x	: ascissa vertice ritornato
        //			y	: ordinata vertice ritornato
        // Restituisce:
        //			punto rototraslato
        //**************************************************************************
        public Point GetVertexRT(int i, out double x, out double y)
        {
            x = 0d; y = 0d;

            Point p = GetVertexRT(i);
            if (p != null)
            {
                x = p.X;
                y = p.Y;
            }

            return p;
        }

        //**************************************************************************
        // GetVertexRT
        // Ritorna il vertice specificato rototraslato
        // Parametri:
        //			i	: indice lato
        // Restituisce:
        //			punto rototraslato
        //**************************************************************************
        public Point GetVertexRT(int i)
        {
            Point p = GetVertex(i);
            if (p != null)
            {
                double x, y;
                mMatrixRT.PointTransform(p.X, p.Y, out x, out y);
                p.X = x; p.Y = y;
            }
            return p;
        }

        //**************************************************************************
        // Close
        // Chiude il percorso
        //**************************************************************************
        public bool Close()
        {
            // Verifica se � gi� chiuso
            if (this.IsClosed)
                return true;

            // Verifica se ha almeno 2 lati
            if (mSides.Count <= 1)
                return false;

            //Legge il primo lato
            Side first = (Side)mSides[0];

            // Aggiunge il vertice di chiusura
            AddVertex(first.P1);
            return true;
        }

        //**************************************************************************
        // Open
        // Apre il percorso
        //**************************************************************************
        public bool Open()
        {
            // Verifica se � gi� chiuso
            if (!this.IsClosed)
                return true;

            // Verifica se ha almeno 2 lati
            if (mSides.Count <= 1)
                return false;

            //Elimina l'ultimo lato
            RemoveSide(mSides.Count - 1);
            return true;
        }

        //**************************************************************************
        // Barycentre
        // Restituisce il punto che � il baricentro del percorso
        //**************************************************************************
        public Point Barycentre()
        {
            // Verifica che esistano dei lati
            if (mSides.Count == 0)
                return null;

            // Crea un nuovo punto
            Point p = new Point();

            //Scorre tutti i vertici del percorso
            for (int i = 0; i < mSides.Count; i++)
            {
                Side lato = (Side)mSides[i];
                p = p + lato.P1;
            }

            return p / (double)mSides.Count;
        }

        //**************************************************************************
        // BarycentreRT
        // Restituisce il punto che � il baricentro del percorso rototraslato
        //**************************************************************************
        public Point BarycentreRT()
        {
            Point b = Barycentre();

            if (b == null)
                return b;

            // Esegue la rototraslazione del baricentro
            double x, y;
            mMatrixRT.PointTransform(b.X, b.Y, out x, out y);
            b.X = x; b.Y = y;
            return b;
        }

        //**************************************************************************
        // Simplify
        // Semplifica il percorso eliminando i punti allineati
        // Parametri:
        //           maxDist	: distanza max per considerare tre punti allineati
        //**************************************************************************
        public void Simplify(double maxDist)
        {
            Side l1, l2, l3;

            try
            {
                if (mSides.Count <= 2)
                    return;

                //Rimuove i punti allineati
                int p = 0, q = 1, r = 2;

                while (p < mSides.Count)
                {
                    if (q >= mSides.Count)
                        q -= mSides.Count;

                    if (r >= mSides.Count)
                        r -= mSides.Count;

                    l1 = (Side)mSides[p];
                    l2 = (Side)mSides[q];
                    l3 = (Side)mSides[r];

                    if (l1.P1.CollinearAndOrdered(l2.P1, l3.P1))
                    {
                        if (q != mSides.Count)
                            RemoveVertex(q);
                    }
                    else
                    {
                        p++; q++; r++;
                    }
                }

                //Verifica se � stata impostata una distanza per considerare i punti allineati
                if (maxDist <= 0d)
                    return;

                //Elimina i punti quasi allineati
                p = 0; q = 1; r = 2;
                while (p < mSides.Count)
                {
                    if (q >= mSides.Count)
                        q -= mSides.Count;

                    if (r >= mSides.Count)
                        r -= mSides.Count;

                    l1 = (Side)mSides[p];
                    l2 = (Side)mSides[q];
                    l3 = (Side)mSides[r];

                    // Cicla per trovare i punti da rimuovere
                    int n = 0;
                    bool near = l1.P1.Near(l2.P1, l3.P1, maxDist);
                    if (near)
                        n = 1;
                    while (near)
                    {
                        r++;
                        if (r >= mSides.Count)
                            r -= mSides.Count;

                        if (r == p)
                            break;

                        l3 = (Side)mSides[r];

                        for (int i = p + 1; i <= (p + n); i++)
                        {
                            if (i >= mSides.Count)
                                l2 = (Side)mSides[i - mSides.Count];
                            else
                                l2 = (Side)mSides[i];

                            if (!(near = l1.P1.Near(l2.P1, l3.P1, maxDist)))
                                break;
                        }

                        if (near)
                            n++;
                    }


                    // Elimina i punti
                    while (n > 0 && mSides.Count > 3)
                    {
                        if (q != mSides.Count)
                            RemoveVertex(q);
                        n--;
                    }

                    p++;
                    q = p + 1;
                    r = q + 1;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Path.Simplify Error", ex);
            }
        }

        //**************************************************************************
        // RemoveInvalidPoints
        // Rimuove i punti non validi
        //**************************************************************************
        public void RemoveInvalidPoints()
        {
            Side l1, l2;

            //Elimina i lati fatti a "T": __|__
            int i = 0;
            while (i < mSides.Count - 2)
            {
                l1 = (Side)mSides[i];
                l2 = (Side)mSides[i + 2];

                if (l1.P1 == l2.P1)
                {
                    l1 = CreateSegment(l1.P1, l2.P2);
                    mSides.Insert(i, l1);
                    mSides.RemoveAt(i + 1);
                    mSides.RemoveAt(i + 1);
                    mSides.RemoveAt(i + 1);
                }
                else
                    i++;
            }
        }

        /// **************************************************************************
        /// <summary>
        /// verifica se nel percorso indicato ci sono lati nulli; se ce ne sono li 
        /// elimina in modo opportuno.
        /// </summary>
        /// <returns>
        ///     false = non e' stata applicata nessuna modifica
        ///     true  = il percorso e' stato modificato
        /// </returns>
        /// **************************************************************************
        public bool RemoveNullSides(double epsilon)
        {
            bool rc = false;

            for (int idx = 0; idx < mSides.Count; idx++)
            {
                int idx_1 = idx + 1;
                if (idx_1 >= mSides.Count)
                    idx_1 = 0;

                Segment s1 = (Segment)GetSide(idx_1);
                if (MathUtil.Compare(s1.Length, 0d, epsilon) == 0)
                {
                    RemoveSide(idx_1);
                    if (idx_1 > idx)
                    {
                        GetSide(idx).P1.X = s1.P2.X;
                        GetSide(idx).P1.Y = s1.P2.Y;
                    }
                    rc = true;
                }
            }
            return rc;
        }

        //**************************************************************************
        // SetRotationSense
        // Setta il senso di rotazione del percorso orario/antiorario
        // Parametri:
        //           sense	: senso di rotazione da settare
        //**************************************************************************
        public MathUtil.RotationSense SetRotationSense(MathUtil.RotationSense sense)
        {
            //Se il percorso non � chiuso, esce
            if (!IsClosed)
                return MathUtil.RotationSense.ALIGNED;

            //Verifica il senso di rotazione attuale
            MathUtil.RotationSense senseAct = SenseOfRotation;

            //Verifica se serve cambiare il senso di rotazione del percorso
            if (sense == senseAct)
                return sense;

            // Copia i lati in un vettore
            Side[] sidesTmp = new Side[mSides.Count];
            mSides.CopyTo(sidesTmp);
            mSides.Clear();

            // Ricrea la lista invertendo i lati
            Side l;
            for (int i = sidesTmp.Length - 1; i >= 0; i--)
            {
                l = sidesTmp[i];
                l = l.Invert();
                mSides.Add(l);
            }

            return sense;
        }

        //**************************************************************************
        // GetRectangle
        // Ritorna il rettangolo che contiene il percorso
        //**************************************************************************
        public Rect GetRectangle()
        {
            if (mSides.Count == 0)
                return null;

            Side s = mSides[0];
            Rect rett = s.GetRectangle;

            for (int i = 1; i < mSides.Count; i++)
            {
                s = mSides[i];
                rett = rett + s.GetRectangle;
            }

            return rett;
        }

        //**************************************************************************
        // GetRectangleRT
        // Ritorna il rettangolo che contiene il percorso rototraslato
        //**************************************************************************
        public Rect GetRectangleRT()
        {
            if (mSides.Count == 0)
                return null;

            Side s = GetSideRT(0);
            Rect rett = s.GetRectangle;

            for (int i = 1; i < mSides.Count; i++)
            {
                s = GetSideRT(i);
                rett = rett + s.GetRectangle;
            }

            return rett;
        }

        ///'**************************************************************************
        /// <summary>
        /// Verifica se un vertice � convesso o concavo
        /// </summary>
        /// <param name="vertex">indice del vertice da controllare</param>
        /// <returns>tipo di vertice</returns>
        ///'**************************************************************************
        public VertexType VertexConvex(int vertex)
        {
            Side lato1, lato2;
            MathUtil.DBL_POINT pt0, pt1, pt2;
            Vector vectP0, vectP2;
            MathUtil.RotationSense tipoVert;

            // Controlla parametri
            if (mSides.Count < 2 || vertex < 0 || vertex > mSides.Count)
                return VertexType.VertexErr;

            // Poligono aperto
            if (!IsClosed)
                return VertexType.VertexErr;

            // Vertice aperto
            if (!IsClosed && (vertex == 0 || vertex == mSides.Count))
                return VertexType.VertexOpen;

            //--------------------------------------------------------------------------
            // Legge i 2 lati interessati
            //--------------------------------------------------------------------------
            if (vertex == 0 || vertex == mSides.Count)
            {
                lato1 = GetSide(mSides.Count - 1);
                lato2 = GetSide(0);
            }
            else
            // De Conti 2016/07/11
            //{
            //    lato1 = GetSide(vertex);
            //    lato2 = GetSide(vertex + 1);
            //}            
            {
                lato1 = GetSide(vertex - 1);
                lato2 = GetSide(vertex);
            }



            //--------------------------------------------------------------------------
            // Legge i tre vertici interessati
            //--------------------------------------------------------------------------

            if (lato1 is Segment)
            {
                pt0.dx = lato1.P1.X;
                pt0.dy = lato1.P1.Y;
                vectP0 = lato1.VectorP2;
            }
            else
            {
                pt0.dx = lato1.P2.X;
                pt0.dy = lato1.P2.Y;
                vectP0 = lato1.VectorP2;
                vectP0.ApplToPoint(pt0.dx, pt0.dy, out pt0.dx, out pt0.dy);
            }

            pt1.dx = lato1.P2.X;
            pt1.dy = lato1.P2.Y;

            if (lato2 is Segment)
            {
                pt2.dx = lato2.P2.X;
                pt2.dy = lato2.P2.Y;
                vectP2 = lato2.VectorP1;
            }
            else
            {
                pt2.dx = lato2.P1.X;
                pt2.dy = lato2.P1.Y;
                vectP2 = lato2.VectorP1;
                vectP2.ApplToPoint(pt2.dx, pt2.dy, out pt2.dx, out pt2.dy);
            }

            // Verifica il senso di rotazione dei 3 vertici
            tipoVert = MathUtil.CCW(pt0, pt1, pt2);

            // Punti allineati
            if (tipoVert == MathUtil.RotationSense.ALIGNED)
            {
                // Calcola l'angolo fra i due vettori
                double ang = vectP0.Angle(vectP2);

                if (lato1 is Arc)
                {
                    // Angolo = 0�
                    if (MathUtil.IsZero(ang))
                    {
                        if (lato1.Direction == -1)
                            tipoVert = MathUtil.RotationSense.CCW;
                        else if (lato1.Direction == 1)
                            tipoVert = MathUtil.RotationSense.CW;
                    }

                    // Angolo = 180�
                    else
                    {
                        if (lato1.Direction == 1)
                            tipoVert = MathUtil.RotationSense.CCW;
                        else if (lato1.Direction == -1)
                            tipoVert = MathUtil.RotationSense.CW;
                    }
                }
                else if (lato2 is Arc)
                {
                    // Angolo = 0�
                    if (MathUtil.IsZero(ang))
                    {
                        if (lato2.Direction == -1)
                            tipoVert = MathUtil.RotationSense.CCW;
                        else if (lato2.Direction == 1)
                            tipoVert = MathUtil.RotationSense.CW;
                    }

                    // Angolo = 180�
                    else
                    {
                        if (lato2.Direction == 1)
                            tipoVert = MathUtil.RotationSense.CCW;
                        else if (lato2.Direction == -1)
                            tipoVert = MathUtil.RotationSense.CW;
                    }
                }
            }

            // Vertice allineato
            if (tipoVert == MathUtil.RotationSense.ALIGNED)
                return VertexType.VertexAll;

            // Verifica il senso di rotazione del poligono
            MathUtil.RotationSense rs = SenseOfRotation;

            // Vertice convesso
            if (tipoVert == rs)
                return VertexType.VertexConv;

            // Vertice concavo
            return VertexType.VertexConc;
        }

        ///**************************************************************************
        /// <summary>
        /// Svuota il percorso
        /// </summary>
        ///**************************************************************************
        public void Empty()
        {
            if (mSides != null)
                mSides.Clear();
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un altro path
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <param name="points">Lista di punti di intersezione ritornata</param>
        /// <returns>
        ///		true	i paths si intersecano
        ///		false	i paths non si intersecano
        ///	</returns>
        ///**************************************************************************
        public bool Intersect(Path path, out List<Point> points)
        {
            Point[] inters;

            // Crea la lista di punti di intersezione da restituire
            points = new List<Point>();

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            foreach (Side s1 in mSides)
                foreach (Side s2 in path.mSides)
                {
                    int num = s1.Intersect(s2, out inters);
                    // Considera solo le intersezioni nell'estremo 1
                    if (num > 0 && s1.P2 != inters[0])
                        points.Add(inters[0]);
                    if (num > 1 && s1.P2 != inters[1])
                        points.Add(inters[1]);
                }

            // Intersezioni presenti se trovato almeno un punto di intesezione
            return (points.Count > 0);
        }
        public bool IntersectIfNotTangent(Path path, out List<Point> points)
        {
            Point[] inters;

            // Crea la lista di punti di intersezione da restituire
            points = new List<Point>();

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            foreach (Side s1 in mSides)
                foreach (Side s2 in path.mSides)
                {
                    int num = s1.Intersect(s2, out inters);
                    // Considera solo le intersezioni nell'estremo 1
                    if (num > 0 && s1.P2 != inters[0])
                        points.Add(inters[0]);
                    if (num > 1 && s1.P2 != inters[1])
                        points.Add(inters[1]);
                }

            // Intersezioni presenti se trovato almeno un punto di intesezione
            return (points.Count > 0);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un altro path
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <param name="exact">
        ///     se true indica che le interesezioni possono essere anche solo
        ///     sugli estremi dei lati, cioe' non e' necessario che ci sia un
        ///     incrocio completo
        /// </param>
        /// <param name="enableIntersectionsOfTangentSides">se false si valuta solo per ogni intersezione le cui tangenti ai due sides sono inclinate di un angolo > MathUtil.RAD_EPSILON</param>
        /// <returns>
        ///		true	i paths si intersecano
        ///		false	i paths non si intersecano
        ///	</returns>
        ///**************************************************************************
        public bool Intersect(Path path, bool exact, bool enableIntersectionsOfTangentSides)
        {
            Point[] inters;

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            foreach (Side s1 in mSides)
            {
                foreach (Side s2 in path.mSides)
                {
                    // Se trova una intersezione, esce
                    if (s1.Intersect(s2, out inters) > 0)
                    {
                        if (!enableIntersectionsOfTangentSides)
                        {
                            List<Point> filtered = new List<Point>();
                            foreach (var p in inters)
                            {
                                if (p == null)
                                    continue;

                                Vector t1 = null, t2 = null;
                                if (s1 is Segment)
                                    t1 = (s1 as Segment).Tangent();
                                else if (s1 is Arc)
                                    t1 = (s1 as Arc).TangentAtPoint(p.X, p.Y);

                                if (s2 is Segment)
                                    t2 = (s2 as Segment).Tangent();
                                else if (s2 is Arc)
                                    t2 = (s2 as Arc).TangentAtPoint(p.X, p.Y);

                                if (Math.Acos(Vector.ScalarProduct(t1, t2)) < MathUtil.RAD_EPSILON)
                                    continue;

                                filtered.Add(p);
                            }
                            inters = filtered.ToArray();
                        }

                        // se e' richiesta la verifica esatta, allora la presenza
                        // di una intersezione, anche sugli estremi dei lati
                        // e' sufficiente.
                        if (exact)
                        {
                            return inters.Length > 0;
                        }

                        // si deve verificare se l'interesezione corrisponde
                        // ad un estremo di un lato
                        for (int i = 0; i < inters.Length; i++)
                        {
                            if (inters[i].AlmostEqual(s1.P1))
                                continue;
                            if (inters[i].AlmostEqual(s1.P2))
                                continue;
                            if (inters[i].AlmostEqual(s2.P1))
                                continue;
                            if (inters[i].AlmostEqual(s2.P2))
                                continue;
                            return true;
                        }
                    }
                }
            }

            // Intersezioni non presenti
            return false;
        }
        public bool Intersect(Path path, bool exact, bool enableIntersectionsOfTangentSides, out List<Point> intersections, out List<Vector> this_tangents, out List<double> path_intersection_distance_from_start)
        {
            intersections = null;
            this_tangents = null;
            path_intersection_distance_from_start = null;
            Point[] inters;

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            foreach (Side s1 in mSides)
            {
                double lPath = 0;
                foreach (Side s2 in path.mSides)
                {
                    // Se trova una intersezione, esce
                    if (s1.Intersect(s2, out inters) > 0)
                    {
                        if (!enableIntersectionsOfTangentSides)
                        {
                            List<Point> filtered = new List<Point>();
                            foreach (var p in inters)
                            {
                                if (p == null)
                                    continue;

                                Vector t1 = null, t2 = null;
                                if (s1 is Segment)
                                    t1 = (s1 as Segment).Tangent();
                                else if (s1 is Arc)
                                    t1 = (s1 as Arc).TangentAtPoint(p.X, p.Y);

                                if (s2 is Segment)
                                    t2 = (s2 as Segment).Tangent();
                                else if (s2 is Arc)
                                    t2 = (s2 as Arc).TangentAtPoint(p.X, p.Y);

                                if (Math.Acos(Vector.ScalarProduct(t1, t2)) < MathUtil.RAD_EPSILON)
                                    continue;

                                filtered.Add(p);
                            }
                            inters = filtered.ToArray();
                        }

                        for (int i = 0; i < inters.Length; i++)
                        {
                            if (exact)
                            {
                                // se e' richiesta la verifica esatta, allora la presenza
                                // di una intersezione, anche sugli estremi dei lati
                                // e' sufficiente.
                            }
                            else
                            {
                                // si deve verificare se l'intersezione corrisponde
                                // ad un estremo di un lato
                                if (inters[i].AlmostEqual(s1.P1))
                                    continue;
                                if (inters[i].AlmostEqual(s1.P2))
                                    continue;
                                if (inters[i].AlmostEqual(s2.P1))
                                    continue;
                                if (inters[i].AlmostEqual(s2.P2))
                                    continue;
                            }

                            if (intersections == null)
                                intersections = new List<Point>();
                            if (this_tangents == null)
                                this_tangents = new List<Vector>();
                            if (path_intersection_distance_from_start == null)
                                path_intersection_distance_from_start = new List<double>();

                            intersections.Add(inters[i]);
                            this_tangents.Add(s1.TangentAtPoint(inters[i].X, inters[i].Y));
                            double distanceFromStart;
                            if (!s2.DistanceFromStart(inters[i], out distanceFromStart))
                                distanceFromStart = 0;
                            path_intersection_distance_from_start.Add(lPath + distanceFromStart);
                        }
                    }
                    lPath += s2.Length;
                }
            }

            // Intersezioni
            return intersections != null;
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un altro path
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <param name="exact">
        ///     se true indica che le interesezioni possono essere anche solo
        ///     sugli estremi dei lati, cioe' non e' necessario che ci sia un
        ///     incrocio completo
        /// </param>
        /// <returns>
        ///		true	i paths si intersecano
        ///		false	i paths non si intersecano
        ///	</returns>
        ///**************************************************************************
        public bool Intersect(Path path, bool exact)
        {
            return Intersect(path, exact, true);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un altro path
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <returns>
        ///		true	i paths si intersecano
        ///		false	i paths non si intersecano
        ///	</returns>
        ///**************************************************************************
        public bool Intersect(Path path)
        {
            return Intersect(path, true);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un altro path rototraslati
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <param name="points">Lista di punti di intersezione ritornata</param>
        /// <returns>
        ///		true	i paths si intersecano
        ///		false	i paths non si intersecano
        ///	</returns>
        ///**************************************************************************
        public bool IntersectRT(Path path, out List<Point> points)
        {
            Point[] inters;

            // Crea la lista di punti di intersezione da restituire
            points = new List<Point>();

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            for (int i = 0; i < mSides.Count; i++)
            {
                Side s1 = GetSideRT(i);

                for (int j = 0; j < path.mSides.Count; j++)
                {
                    Side s2 = path.GetSideRT(j);

                    int num = s1.Intersect(s2, out inters);
                    // Considera solo le intersezioni nell'estremo 1
                    if (num > 0 && s1.P2 != inters[0])
                        points.Add(inters[0]);
                    if (num > 1 && s1.P2 != inters[1])
                        points.Add(inters[1]);
                }
            }

            // Intersezioni presenti se trovato almeno un punto di intesezione
            return (points.Count > 0);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un altro path rototraslati
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <param name="exact">
        ///     se true indica che le interesezioni possono essere anche solo
        ///     sugli estremi dei lati, cioe' non e' necessario che ci sia un
        ///     incrocio completo
        /// </param>
        /// <returns>
        ///		true	i paths si intersecano
        ///		false	i paths non si intersecano
        ///	</returns>
        ///**************************************************************************
        public bool IntersectRT(Path path, bool exact)
        {
            Point[] inters;

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            for (int i = 0; i < mSides.Count; i++)
            {
                Side s1 = GetSideRT(i);

                for (int j = 0; j < path.mSides.Count; j++)
                {
                    Side s2 = path.GetSideRT(j);

                    // Se trova una intersezione, esce
                    if (s1.Intersect(s2, out inters) > 0)
                    {
                        // se e' richiesta la verifica esatta, allora la presenza
                        // di una intersezione, anche sugli estremi dei lati
                        // e' sufficiente.
                        if (exact)
                            return true;

                        // si deve verificare se l'interesezione corrisponde
                        // ad un estremo di un lato
                        for (int k = 0; k < inters.Length; k++)
                        {
                            if (inters[k] == s1.P1)
                                continue;
                            if (inters[k] == s1.P2)
                                continue;
                            if (inters[k] == s2.P1)
                                continue;
                            if (inters[k] == s2.P2)
                                continue;
                            return true;
                        }
                    }
                }
            }

            // Intersezioni non presenti
            return false;
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un altro path rototraslati
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <returns>
        ///		true	i paths si intersecano
        ///		false	i paths non si intersecano
        ///	</returns>
        ///**************************************************************************
        public bool IntersectRT(Path path)
        {
            return IntersectRT(path, true);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <param name="points">Lista di punti di intersezione ritornata</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public virtual bool Intersect(Side s2, out List<Point> points)
        {
            Point[] inters;

            // Crea la lista di punti di intersezione da restituire
            points = new List<Point>();

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            foreach (Side s1 in mSides)
            {
                int num = s1.Intersect(s2, out inters);
                // Considera solo le intersezioni nell'estremo 1
                if (num > 0 && s1.P2 != inters[0])
                    points.Add(inters[0]);
                if (num > 1 && s1.P2 != inters[1])
                    points.Add(inters[1]);
            }

            // Intersezioni presenti se trovato almeno un punto di intesezione
            return (points.Count > 0);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public virtual bool Intersect(Side s2)
        {
            Point[] inters;

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            foreach (Side s1 in mSides)
            {
                // Se trova una intersezione, esce
                if (s1.Intersect(s2, out inters) > 0)
                    return true;
            }

            // Intersezioni non presenti
            return false;
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side (con rototraslazione)
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <param name="points">Lista di punti di intersezione ritornata</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public virtual bool IntersectRT(Side s2, out List<Point> points)
        {
            Point[] inters;

            // Crea la lista di punti di intersezione da restituire
            points = new List<Point>();

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            for (int i = 0; i < mSides.Count; i++)
            {
                Side s1 = GetSideRT(i);

                int num = s1.Intersect(s2, out inters);
                // Considera solo le intersezioni nell'estremo 1
                if (num > 0 && s1.P2 != inters[0])
                    points.Add(inters[0]);
                if (num > 1 && s1.P2 != inters[1])
                    points.Add(inters[1]);
            }

            // Intersezioni presenti se trovato almeno un punto di intesezione
            return (points.Count > 0);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side (con rototraslazione)
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public virtual bool IntersectRT(Side s2)
        {
            Point[] inters;

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            for (int i = 0; i < mSides.Count; i++)
            {
                Side s1 = GetSideRT(i);

                // Se trova una intersezione, esce
                if (s1.Intersect(s2, out inters) > 0)
                    return true;
            }

            // Intersezioni non presenti
            return false;
        }

        /// **************************************************************************
        /// <summary>
        /// Determina se un punto e` interno al percorso
        /// </summary>
        /// <param name="p">punto da testare</param>
        /// <returns>
        ///		true	il punto e` interno
        ///		false	il punto e` esterno
        /// </returns>
        /// **************************************************************************
        public bool IsInside(Point p)
        {
            return IsInside(p, useRT: false);
        }

        /// **************************************************************************
        /// <summary>
        /// Determina se un punto e` interno al percorso rototraslato
        /// </summary>
        /// <param name="p">punto da testare</param>
        /// <returns>
        ///		true	il punto e` interno
        ///		false	il punto e` esterno
        /// </returns>
        /// **************************************************************************
        public bool IsInsideRT(Point p)
        {
            return IsInside(p, useRT: true);
            //int num;

            ////Trova il rettangolo che include il percorso
            //Rect rect = GetRectangleRT();
            //if (!rect.IsInternalPoint(p.X, p.Y))
            //    return false;

            //// Crea un segmento orizzontale a partire dal punto fino ad un punto esterno del percorso
            //Side test = new Segment(p.X, p.Y, rect.Right + 50d, p.Y);

            //Point[] inters;
            //Side s;
            //int numInter = 0;

            //// Now go through each of the lines in the polygon and see if it intersects
            //for (int i = 0; i < mSides.Count; i++)
            //{
            //    s = GetSideRT(i);

            //    num = s.Intersect(test, out inters);
            //    // Considera solo le intersezioni nell'estremo 1
            //    if (num > 0 && s.P2 == inters[0] || num > 1 && s.P2 == inters[1])
            //        num--;
            //    // Controlla il caso in cui il segmento sia sovrapposto al lato, non bisogna contarlo
            //    else if (num < 0)
            //        num = 0;

            //    numInter += num;
            //}

            //// se il numero di intersezioni � dispari, il punto � interno
            //return (numInter & 1) == 1;
        }

        private bool IsInside(Point p, bool useRT)
        {
            int num = 0;

            //Trova il rettangolo che include il percorso
            Rect rect = useRT ? GetRectangleRT() : GetRectangle();
            if (!rect.IsInternalPoint(p.X, p.Y))
                return false;

            //Se � una circonferenza
            if (Count == 1 && mSides[0] is Arc && mSides[0].IsCircle)
            {
                Side s = useRT ? GetSideRT(0) : mSides[0];
                Arc arc = s as Arc;
                if (arc.CenterPoint != null)
                {
                    double radius = p.Distance(arc.CenterPoint);
                    return radius < arc.Radius;
                }
            }

            // Crea un segmento orizzontale a partire dal punto fino ad un punto esterno del percorso,
            // prolungo oltre rect.Right della lunghezza della diagonale per assicurarmi che il segmento anche quando ruotato sia sempre abbastanza lungo da attraversare tutta la shape
            double rayLength = rect.Diagonal + 50d;
            Side testRay = new Segment(p.X, p.Y, rect.Right + rayLength, p.Y);

            Point[] inters;
            int numInter = 0;
            // Now go through each of the lines in the polygon and see if it intersects
            //foreach (Side s in mSides)
            // warning: foreach has been substituted with for i ... since the foreach
            //          version generate random unaxpected exceptions in release mode 
            for (int i = 0; i < mSides.Count; i++)
            {
                Side s = useRT ? GetSideRT(i) : mSides[i];
                num = s.Intersect(testRay, out inters);
                // Considera solo le intersezioni nell'estremo 1
                if (num > 0 && s.P2 == inters[0] || num > 1 && s.P2 == inters[1])
                    num--;
                else if (num < 0)   //sovrapposizione del segmento
                    num = 0;

                numInter += num;
            }
            // se il numero di intersezioni � dispari, il punto � interno
            return (numInter & 1) == 1;
        }

        /// **************************************************************************
        /// <summary>
        /// Restituisce se il poligono � un rettangolo
        /// </summary>
        /// <returns>
        ///		true	rettangolo
        ///		false	non � un rettangolo
        ///	</returns>
        /// **************************************************************************
        public bool IsRectangle()
        {
            Side Lato1, Lato2;
            Vector v1, v2;
            Double ang1, ang2;
            int i;

            // In geometria il sostantivo rettangolo denota il quadrilatero con tutti gli angoli interni congruenti (e quindi retti)

            // Verifica che sia chiuso
            if (!this.IsClosed)
                return false;

            // Verifica che abbia 4 lati
            if (this.Count != 4)
                return false;

            // Verifica che siano tutti segmenti e che gli angoli siano tutti uguali
            Lato1 = this.GetSide(3);
            v1 = Lato1.VectorSide;
            ang1 = 0d;

            for (i = 0; i < this.Count; i++)
            {
                // Legge il nuovo lato
                Lato2 = this.GetSide(i);

                // Verifica che sia un segmento
                if (!(Lato2 is Segment))
                    return false;

                // Calcola il vettore
                v2 = Lato2.VectorSide;

                // Verifica che sia uguale al precedente
                ang2 = v1.Angle(v2);
                if (ang1 != 0d && MathUtil.AngleCompare(ang1, ang2) != 0)
                    return false;

                Lato1 = Lato2;
                v1 = v2;
                ang1 = ang2;
            }

            return true;
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se un percorso e` interno al percorso
        /// </summary>
        /// <param name="path">percorso da controllare se � contenuto</param>
        /// <param name="exact">
        ///     se true indica che il percorso deve essere completeamente contenuto
        ///     cioe' che intersezioni con il perimetro non sono ammesse
        /// </param>
        /// <returns>
        ///		true	il percorso � completamente contenuto
        ///		false	il percorso non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool Contains(Path path, bool exact)
        {
            // Se ci sono delle intersezioni fra i poligoni, significa che il path non � contenuto
            if (Intersect(path, exact))
                return false;

            // Se non ci sono intersezioni, e un punto qualsiasi del path � interno al poligono
            // significa che il path � contenuto nel poligono
            Point p0 = path.GetVertex(0);
            //return ((p0 != null) && IsInside( p0 ));

            if (p0 == null)
                return false;
            if (IsInside( p0 ))
                return true;

            // se e' consentita la sovrapposizone si controlla se il punto
            // e' sul perimetro
            if (!exact)
                for (int i = 0; i < Count; i++)
                    if (this[i].IsInSide( p0 ))
                        return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se un percorso e` interno al percorso rototraslato
        /// </summary>
        /// <param name="path">percorso da controllare se � contenuto</param>
        /// <param name="exact">
        ///     se true indica che il percorso deve essere completeamente contenuto
        ///     cioe' che intersezioni con il perimetro non sono ammesse
        /// </param>
        /// <returns>
        ///		true	il percorso � completamente contenuto
        ///		false	il percorso non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool ContainsRT(Path path, bool exact)
        {
            // Se ci sono delle intersezioni fra i poligoni, significa che il path non � contenuto
            if (IntersectRT(path, exact))
                return false;

            // Se non ci sono intersezioni, e un punto qualsiasi del path � interno al poligono
            // significa che il path � contenuto nel poligono
            Point p0 = path.GetVertexRT(0);

            return ((p0 != null) && IsInsideRT(p0));
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se un percorso e` interno al percorso
        /// </summary>
        /// <param name="path">percorso da controllare se � contenuto</param>
        /// <returns>
        ///		true	il percorso � completamente contenuto
        ///		false	il percorso non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool Contains(Path path)
        {
            return Contains(path, true);
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se un percorso e` interno al percorso rototraslato
        /// </summary>
        /// <param name="path">percorso da controllare se � contenuto</param>
        /// <returns>
        ///		true	il percorso � completamente contenuto
        ///		false	il percorso non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool ContainsRT(Path path)
        {
            return ContainsRT(path, true);
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se un percorso interseca o e` interno al percorso
        /// </summary>
        /// <param name="path">percorso da controllare se interseca o � contenuto</param>
        /// <returns>
        ///		true	il percorso interseca o � contenuto
        ///		false	il percorso non interseca e non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool ContainsOrIntersect(Path path)
        {
            // Se ci sono delle intersezioni fra i poligoni, significa che il path interseva
            if (Intersect(path, true))
                return true;

            // Se non ci sono intersezioni, e un punto qualsiasi del path � interno al poligono
            // significa che il path � contenuto nel poligono
            Point p0 = path.GetVertex(0);

            return ((p0 != null) && IsInside(p0));
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se un percorso interseca o e` interno al percorso rototraslato
        /// </summary>
        /// <param name="path">percorso da controllare se interseca o � contenuto</param>
        /// <returns>
        ///		true	il percorso interseca o � contenuto
        ///		false	il percorso non interseca e non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool ContainsOrIntersectRT(Path path)
        {
            // Se ci sono delle intersezioni fra i poligoni, significa che il path non � contenuto
            if (IntersectRT(path, true))
                return true;

            // Se non ci sono intersezioni, e un punto qualsiasi del path � interno al poligono
            // significa che il path � contenuto nel poligono
            Point p0 = path.GetVertexRT(0);

            return ((p0 != null) && IsInsideRT(p0));
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se il lato indicato e' completamente contentuto in un lato
        /// del percorso
        /// </summary>
        /// <param name="side">lato da controllare</param>
        /// <param name="maxdist">massima distanza laterale ammessa</param>
        /// <param name="continer">eventuale lato che contiene quello indicato</param>
        /// <returns>
        ///		true	il lato e' contentuto completamente
        ///		false	il lato non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool ContainsSide(Side side, double maxdist, out Side container)
        {
            container = null;
            for (int i = 0; i < Count; i++)
                if (this[i].Contains(side, maxdist))
                {
                    container = this[i];
                    return true;
                }
            return false;
        }

        ///**************************************************************************
        /// <summary>
        /// Determina se il lato indicato e' completamente contentuto in un lato
        /// del percorso rototraslato
        /// </summary>
        /// <param name="side">lato da controllare</param>
        /// <param name="maxdist">massima distanza laterale ammessa</param>
        /// <param name="continer">eventuale lato che contiene quello indicato</param>
        /// <returns>
        ///		true	il lato e' contentuto completamente
        ///		false	il lato non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool ContainsSideRT(Side side, double maxdist, out Side container)
        {
            return GetPathRT().ContainsSide(side, maxdist, out container);
        }

        ///**************************************************************************
        /// <summary>
        /// Determina il lato indicato e' contiene completamente un lato
        /// del percorso
        /// </summary>
        /// <param name="side">lato da controllare</param>
        /// <param name="maxdist">massima distanza laterale ammessa</param>
        /// <param name="continer">eventuale lato che contenuto</param>
        /// <returns>
        ///		true	il lato e' contentuto completamente
        ///		false	il lato non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool SideContained(Side side, double maxdist, out Side container)
        {
            container = null;

            for (int i = 0; i < Count; i++)
                if (side.Contains(this[i], maxdist))
                {
                    container = this[i];
                    return true;
                }
            return false;
        }

        ///**************************************************************************
        /// <summary>
        /// Determina il lato indicato e' contiene completamente un lato
        /// del percorso rototraslato
        /// </summary>
        /// <param name="side">lato da controllare</param>
        /// <param name="maxdist">massima distanza laterale ammessa</param>
        /// <param name="continer">eventuale lato che contenuto</param>
        /// <returns>
        ///		true	il lato e' contentuto completamente
        ///		false	il lato non � contenuto
        /// </returns>
        ///**************************************************************************
        public bool SideContainedRT(Side side, double maxdist, out Side container)
        {
            return GetPathRT().SideContained(side, maxdist, out container);
        }

        //**************************************************************************
        // RotoTranslAbs
        // Rototraslazione assoluta 
        // Parametri:
        //			offsetX	: offset di traslazione X del percorso
        //			offsetY	: offset di traslazione Y del percorso
        //			centerX	: ascissa X del centro di rotazione
        //			centerY	: ascissa Y del centro di rotazione
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
        public void RotoTranslAbs(double offsetX, double offsetY, double centerX, double centerY, double angle)
        {
            mMatrixRT.RotoTranslAbs(offsetX, offsetY, centerX, centerY, angle);
        }

        //**************************************************************************
        // RotoTranslRel
        // Rototraslazione relativa 
        // Parametri:
        //			offsetX	: offset di traslazione X del percorso
        //			offsetY	: offset di traslazione Y del percorso
        //			centerX	: ascissa X del centro di rotazione
        //			centerY	: ascissa Y del centro di rotazione
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
        public void RotoTranslRel(double offsetX, double offsetY, double centerX, double centerY, double angle)
        {
            mMatrixRT.RotoTranslRel(offsetX, offsetY, centerX, centerY, angle);
        }

        //**************************************************************************
        // GetRotTrasl
        // Legge la traslazione e l'angolo di rotazione del percorso
        // Parametri:
        //			offsetX	: traslazione X
        //			offsetY	: traslazione Y
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
        public void GetRotoTransl(out double offsetX, out double offsetY, out double angle)
        {
            mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);
        }

        //**************************************************************************
        // SetRotoTransl
        // Setta la traslazione e l'angolo di rotazione del percorso
        // Parametri:
        //			offsetX	: traslazione X
        //			offsetY	: traslazione Y
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
        public void SetRotoTransl(double offsetX, double offsetY, double angle)
        {
            mMatrixRT.SetRotoTransl(offsetX, offsetY, angle);
        }

        //**************************************************************************
        // Normalize
        // Ritorna il percorso normalizzato su un punto di offset
        // Parametri:
        //			offset	: punto di normalizzazione
        //**************************************************************************
        public Path Normalize(double x, double y)
        {
            return Normalize(new Point(x, y));
        }

        public Path Normalize(Point offset)
        {
            Path p = new Path();

            foreach (Side s in mSides)
                p.AddSideRef(s.Normalize(offset));

            p.mAllowSelect = mAllowSelect;
            p.mLayer = mLayer;
            p.MatrixRT = new RTMatrix(mMatrixRT);

            p.Attributes.Clear();
            foreach (KeyValuePair<string, string> a in mAttributes)
                p.Attributes.Add(a.Key, a.Value);

            return p;
        }

        public Path NormalizeRT(double x, double y)
        {
            return NormalizeRT(new Point(x, y));
        }

        public Path NormalizeRT(Point offset)
        {
            Path p = GetPathRT();

            p.mAllowSelect = mAllowSelect;
            p.mLayer = mLayer;
            p.Attributes.Clear();
            foreach (KeyValuePair<string, string> a in mAttributes)
                p.Attributes.Add(a.Key, a.Value);

            return p.Normalize(offset);
        }

        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public virtual bool ReadDerivedFileXml(XmlNode n)
        {
            return true;
        }

        //**************************************************************************
        // ReadFileXml
        // Legge il lato dal file XML
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public virtual bool ReadFileXml(XmlNode n)
        {
            Side s;
            Arc a;


            if (!ReadDerivedFileXml(n))
                return false;

            try
            {
                if (n.Attributes.GetNamedItem("Type") != null)
                    this.Layer = n.Attributes.GetNamedItem("Type").Value.ToString(CultureInfo.InvariantCulture);

                if (n.Attributes.GetNamedItem("Mode") != null)
                    this.enPathMode = String_To_EN_PATH_MODE(n.Attributes.GetNamedItem("Mode").Value.ToString(CultureInfo.InvariantCulture));

                XmlNodeList l = n.ChildNodes;

                // Scorre tutti i sottonodi
                foreach (XmlNode chn in l)
                {
                    // Legge i nodi dei lati
                    if (chn.Name == "Segment")
                    {
                        s = CreateSegment();
                        if (s.ReadFileXml(chn))
                            mSides.Add(s);
                    }

                    // Legge i nodi degli archi
                    else if (chn.Name == "Arc")
                    {
                        a = CreateArc();
                        if (a.ReadFileXml(chn))
                            mSides.Add(a);
                    }

                    // Legge il nodo della matrice
                    else if (chn.Name == "MatrixRT")
                    {
                        double[,] m = new double[3, 3];

                        m[0, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m00").Value, CultureInfo.InvariantCulture);
                        m[0, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m01").Value, CultureInfo.InvariantCulture);
                        m[0, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m02").Value, CultureInfo.InvariantCulture);
                        m[1, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m10").Value, CultureInfo.InvariantCulture);
                        m[1, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m11").Value, CultureInfo.InvariantCulture);
                        m[1, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m12").Value, CultureInfo.InvariantCulture);
                        m[2, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m20").Value, CultureInfo.InvariantCulture);
                        m[2, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m21").Value, CultureInfo.InvariantCulture);
                        m[2, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m22").Value, CultureInfo.InvariantCulture);

                        mMatrixRT.Matrix = m;
                    }
                    else if (chn.Name == "Attributes")
                    {
                        foreach (XmlNode attr in chn.ChildNodes)
                        {
                            XmlAttribute val = attr.Attributes["value"];
                            this[attr.Name] = val.Value;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Path.ReadFileXml Error.", ex);
                return false;
            }
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public virtual void WriteDerivedFileXml(XmlTextWriter w)
        {
        }

        //**************************************************************************
        // WriteFileXml
        // Scrive il lato su file XML
        // Parametri:
        //			w			: oggetto per scrivere il file XML
        //			saveMatrix	: salva la matrice di rototraslazione
        //**************************************************************************
        public virtual void WriteFileXml(XmlTextWriter w)
        {
            WriteFileXml(w, true);
        }

        public virtual void WriteFileXml(XmlTextWriter w, bool saveMatrix)
        {
            w.WriteStartElement("Path");

            w.WriteAttributeString("Type", this.Layer);

            w.WriteAttributeString("Mode", this.enPathMode.ToString());

            WriteDerivedFileXml(w);

            w.WriteStartElement("Attributes");
            foreach (KeyValuePair<string, string> a in mAttributes)
            {
                w.WriteStartElement(a.Key);
                w.WriteAttributeString("value", a.Value);
                w.WriteEndElement();
            }
            w.WriteEndElement();

            if (saveMatrix)
            {
                double[,] m = mMatrixRT.Matrix;
                // Scrive la matrice di rototraslazione
                w.WriteStartElement("MatrixRT");
                w.WriteAttributeString("m00", m[0, 0].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m01", m[0, 1].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m02", m[0, 2].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m10", m[1, 0].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m11", m[1, 1].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m12", m[1, 2].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m20", m[2, 0].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m21", m[2, 1].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m22", m[2, 2].ToString(CultureInfo.InvariantCulture));
                w.WriteEndElement();
            }

            // Scrive tutti i lati
            foreach (Side s in mSides)
                s.WriteFileXml(w);

            w.WriteEndElement();
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public virtual void WriteDerivedFileXml(XmlNode baseNode)
        {
        }

        ///**************************************************************************
        /// <summary>
        /// Scrive il path sul nodo XML
        /// </summary>
        /// <param name="baseNode">nodo XML dove scrivere il path</param>
        /// <param name="saveMatrix">scrive anche la matrice di rototraslazione</param>
        ///**************************************************************************
        public virtual void WriteFileXml(XmlNode baseNode)
        {
            WriteFileXml(baseNode, true);
        }

        public virtual void WriteFileXml(XmlNode baseNode, bool saveMatrix)
        {
            XmlDocument doc = baseNode.OwnerDocument;
            XmlAttribute attr;
            XmlNode node = doc.CreateNode(XmlNodeType.Element, "Path", "");

            WriteDerivedFileXml(node);

            attr = doc.CreateAttribute("Type");
            attr.Value = this.Layer;
            node.Attributes.Append(attr);

            attr = doc.CreateAttribute("Mode");
            attr.Value = enPathMode.ToString();
            node.Attributes.Append(attr);

            // Inserisce gli attributi del path
            XmlNode attributes = doc.CreateNode(XmlNodeType.Element, "Attributes", ""); ;
            foreach (KeyValuePair<string, string> a in mAttributes)
            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, a.Key, ""); ;
                attr = doc.CreateAttribute("value");
                attr.Value = a.Value;
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }
            node.AppendChild(attributes);

            if (saveMatrix)
            {
                double[,] m = mMatrixRT.Matrix;

                // Scrive la matrice di rototraslazione
                XmlNode matrix = doc.CreateNode(XmlNodeType.Element, "MatrixRT", "");

                attr = doc.CreateAttribute("m00");
                attr.Value = m[0, 0].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m01");
                attr.Value = m[0, 1].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m02");
                attr.Value = m[0, 2].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m10");
                attr.Value = m[1, 0].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m11");
                attr.Value = m[1, 1].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m12");
                attr.Value = m[1, 2].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m20");
                attr.Value = m[2, 0].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m21");
                attr.Value = m[2, 1].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m22");
                attr.Value = m[2, 2].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);

                node.AppendChild(matrix);
            }

            // Scrive tutti i lati
            foreach (Side s in mSides)
                s.WriteFileXml(node);

            baseNode.AppendChild(node);
        }

        /// ********************************************************************
        /// <summary>
        /// Trasforma il poligono, scala data da autovalore maggiore
        /// </summary>
        /// <returns>transform != null</returns>
        /// ********************************************************************
        public bool Transform(Breton.MathUtils.RTMatrix transform)
        {
            if (transform != null)
            {
                for (int i = 0; i < this.Count; i++)
                {
                    var side = this[i];
                    side.Transform(transform);
                    //var transformInv = transform.InvMat();
                }

                return true;
            }

            return false;
        }

        /// ********************************************************************
        /// <summary>
        /// Ritorna la transform dall'origine ad asse x tangente alla curva, y ottenuto ruotando l'xy originario per portare x origine a tangente su primo side del path
        /// </summary>
        /// <returns>RTMatrix</returns>
        /// ********************************************************************
        public RTMatrix FrameAtStart()
        {
            if (this.Count > 0)
            {
                var offset = (Breton.MathUtils.Vector)this[0].P1;
                var tangent = this[0].Tangent();
                var transform = new Breton.MathUtils.RTMatrix(offset, tangent);
                return mMatrixRT * transform;
            }
            else
            {
                return null;
            }
        }

        /// ********************************************************************
        /// <summary>
        /// Ritorna il poligono rototraslato
        /// </summary>
        /// <returns>nuovo path rototraslato</returns>
        /// ********************************************************************
        public Path GetPathRT()
        {
            Path p = Clone();
            p.Empty();

            //p.AllowSelect = mAllowSelect;
            //p.Layer = mLayer;

            // Aggiunge tutti i lati del percorso
            for (int i = 0; i < mSides.Count; i++)
                p.AddSide(GetSideRT(i));

            //foreach (KeyValuePair<string, string> a in mAttributes)
            //	p.Attributes.Add(a.Key, a.Value);

            // si annulla la matrice di rototraslazione sul percorso finale
            p.MatrixRT.Reset();

            return p;
        }

        /// ********************************************************************
        /// <summary>
        /// Ritorna un array con tutti i vertici del poligono
        /// </summary>
        /// <returns>Array di punti</returns>
        /// ********************************************************************
        public Point[] GetVertexList()
        {
            int i = 0;
            // Crea il vettore di punti
            Point[] points = new Point[VertexCount];

            if (points.Length > 0)
            {
                // Scorre tutti i lati del poligono
                foreach (Side s in mSides)
                    points[i++] = new Point(s.P1);

                // Aggiunge il vertice di chiusura
                points[i++] = mSides[mSides.Count - 1].P2;
            }
            return points;
        }

        ///**************************************************************************
        /// <summary>Clone
        ///     Crea un path uguale a quello corrente
        /// </summary>
        /// <returns>
        ///     percorso clonato
        /// </returns>
        ///**************************************************************************
        public virtual Path Clone()
        {
            return new Path(this);
        }

        ///**************************************************************************
        ///<summary>GetSideNumber
        ///     determina l'indice dell'elemento indicato nell'arra interno
        ///</summary>
        ///<param name="s">
        ///     riferimento all'elemnto da ricercare
        ///</param>
        ///<returns>
        ///     >= 0 indica l'indice trovato
        ///     -1   indica che non e' stato trovato l'elemento
        ///</returns>
        ///**************************************************************************
        public int GetSideNumber(Side s)
        {
            return mSides.IndexOf(s);
        }

        ///**************************************************************************
        ///<summary>
        ///     ritorna le coordinate del punto che si trova alla distanza
        ///     indicata dall'inizio del percorso
        ///</summary>
        ///<param name="distance">
        ///     distanza alla quale cercare il punto
        ///</param>
        ///<returns>
        ///     punto trovato o null se la distanza e' incongruente
        ///</returns>
        ///**************************************************************************
        public Point PointAtDistanceFromStart(double distance)
        {
            if (Count == 0)
                return null;

            double dist = 0;

            for (int i = 0; i < Count; i++)
            {
                if ((dist + this[i].Length) < distance)
                {
                    dist += this[i].Length;
                    continue;
                }

                double delta = distance - dist;

                return this[i].PointAtDistanceFromStart(delta);
            }

            return this[Count - 1].P2;
        }

        public bool TryGetStart(out Point point)
        {
            if (Count == 0)
            {
                point = null;
                return false;
            }
            else
            {
                point = this[0].StartPoint();
                return true;
            }
        }

        public bool TryGetEnd(out Point point)
        {
            if (Count == 0)
            {
                point = null;
                return false;
            }
            else
            {
                point = this[Count-1].EndPoint();
                return true;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     sostituzione degli archi contenuti con sequenze di segmenti
        ///     con errore cordale definito
        /// </summary>
        /// <param name="coorderror">
        ///     massimo errore cordale ammesso
        /// </param>
        /// <param name="mode">
        ///     modo di discretizzazione
        /// </param>
        ///**************************************************************************
        public void RemoveArcs(double coorderror, EN_ARC_DISCRETIZE_MODE mode)
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i] is Arc)
                {
                    Arc a = (Arc)this[i];
                    if (coorderror > 0)
                    {
                        Point[] pn = null;

                        switch (mode)
                        {
                            case EN_ARC_DISCRETIZE_MODE.INTERNAL:
                                {
                                    pn = a.InternBrokenLines(coorderror);
                                    break;
                                }

                            case EN_ARC_DISCRETIZE_MODE.EXTERNAL:
                                {
                                    pn = a.ExternBrokenLines(coorderror);
                                    break;
                                }

                            case EN_ARC_DISCRETIZE_MODE.RIGHT:
                                {
                                    if (a.AmplAngle > 0)
                                        pn = a.ExternBrokenLines(coorderror);
                                    else
                                        pn = a.InternBrokenLines(coorderror);
                                    break;
                                }

                            case EN_ARC_DISCRETIZE_MODE.LEFT:
                                {
                                    if (a.AmplAngle > 0)
                                        pn = a.InternBrokenLines(coorderror);
                                    else
                                        pn = a.ExternBrokenLines(coorderror);
                                    break;
                                }
                        }

                        if (pn != null && pn.Length > 0)
                        {
                            for (int j = 0; j < pn.Length - 1; j++)
                            {
                                Side s = CreateSegment(pn[j], pn[j + 1]);
                                AddSide(s, i++);
                            }
                            i--;
                            DeleteSide(a);
                        }
                    }
                }
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Crea un percorso che non contiene archi; gli archi vengono discretizzati
        ///     con il paramentro indicato e dallato indicato
        /// </summary>
        /// <param name="coorderror">
        ///     massimo errore cordale ammesso
        /// </param>
        /// <param name="mode">
        ///     modo di discretizzazione
        /// </param>
        /// <returns>
        ///     percorso risultante
        /// </returns>
        ///**************************************************************************
        public Path GetPathWithoutArcs(double coorderror, EN_ARC_DISCRETIZE_MODE mode)
        {
            Path pt = Clone();

            pt.RemoveArcs(coorderror, mode);

            return pt;
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcolo il baricentro del percorso, eventualmente controllando
        ///     che sia all'interno; se non lo e' si cerca il puntop piu' vicino
        /// </summary>
        /// <param name="mustbeinside">
        ///     indica se il baricentro deve essere all'interno della figura
        /// </param>
        /// <param name="distfromexact">
        ///     nel caso il baricentro non sia all'interno della figura, indica
        ///     la distanza dal punto esatto.
        /// </param>
        /// <returns>
        ///     Baricentro calcolato
        /// </returns>
        ///**************************************************************************
        public Point BarycentreEx(bool mustbeinside, out double distfromexact)
        {
            return BarycentreEx(mustbeinside, out distfromexact, 1);
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcolo il baricentro del percorso, eventualmente controllando
        ///     che sia all'interno; se non lo e' si cerca il puntop piu' vicino
        /// </summary>
        /// <param name="mustbeinside">
        ///     indica se il baricentro deve essere all'interno della figura
        /// </param>
        /// <param name="distfromexact">
        ///     nel caso il baricentro non sia all'interno della figura, indica
        ///     la distanza dal punto esatto.
        /// </param>
        /// <param name="coorderror">
        ///     errore cordale da utilizzare nel caso ci siano archi
        /// </param>
        /// <returns>
        ///     Baricentro calcolato
        /// </returns>
        ///**************************************************************************
        public Point BarycentreEx(bool mustbeinside, out double distfromexact, double coorderror)
        {
            // distanza nulla per default
            distfromexact = 0;

            // Verifica che esistano dei lati
            if (mSides.Count == 0 || !IsClosed)
                return null;

            // se il percorso contiene almeno un arco, si deve discretizzare l'itero percorso
            Path pth = this;
            for (int i = 0; i < Count; i++)
            {
                if (this[i] is Arc)
                {
                    pth = GetPathWithoutArcs(coorderror, EN_ARC_DISCRETIZE_MODE.EXTERNAL);
                    break;
                }
            }

            return pth.Centroid(mustbeinside, out distfromexact);
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcolo il baricentro del percorso rototraslato, eventualmente 
        ///     controllando sia all'interno; se non lo e' si cerca il punto 
        ///     piu' vicino.
        /// </summary>
        /// <param name="mustbeinside">
        ///     indica se il baricentro deve essere all'interno della figura
        /// </param>
        /// <param name="distfromexact">
        ///     nel caso il baricentro non sia all'interno della figura, indica
        ///     la distanza dal punto esatto.
        /// </param>
        /// <returns>
        ///     Baricentro calcolato
        /// </returns>
        ///**************************************************************************
        public Point BarycentreExRT(bool mustbeinside, out double distfromexact)
        {
            return BarycentreExRT(mustbeinside, out distfromexact, 1);
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcolo il baricentro del percorso rototraslato, eventualmente 
        ///     controllando sia all'interno; se non lo e' si cerca il punto 
        ///     piu' vicino.
        /// </summary>
        /// <param name="mustbeinside">
        ///     indica se il baricentro deve essere all'interno della figura
        /// </param>
        /// <param name="distfromexact">
        ///     nel caso il baricentro non sia all'interno della figura, indica
        ///     la distanza dal punto esatto.
        /// </param>
        /// <param name="coorderror">
        ///     errore cordale da utilizzare nel caso ci siano archi
        /// </param>
        /// <returns>
        ///     Baricentro calcolato
        /// </returns>
        ///**************************************************************************
        public Point BarycentreExRT(bool mustbeinside, out double distfromexact, double coorderror)
        {
            Point b = BarycentreEx(mustbeinside, out distfromexact, coorderror);

            if (b == null)
                return b;

            // Esegue la rototraslazione del baricentro
            double x, y;
            mMatrixRT.PointTransform(b.X, b.Y, out x, out y);
            b.X = x; b.Y = y;
            return b;
        }

        ///******************************************************************************
        /// <summary>
        ///     Divide tutti i percorsi che sono cerchi completi in due archi di ampiezza
        ///     pari a 180 gradi.
        /// </summary>
        ///******************************************************************************
        public void DivideCircles()
        {
            // controllo percorso esterno
            if (Count == 1 && this[0] is Arc)
            {
                // una solo elemento arco: si controlla se completo
                Arc a = (Arc)this[0];
                if (MathUtil.AngleIsEQ(Math.Abs(a.AmplAngle), Math.PI * 2))
                {
                    Arc b = new Arc(a);
                    a.AmplAngle = a.AmplAngle / 2;
                    b.StartAngle = a.StartAngle + a.AmplAngle;
                    b.AmplAngle = a.AmplAngle;
                    AddSide(b);
                }
            }
        }

        ///******************************************************************************
        /// <summary>
        ///     Connessione degli elementi interni del percorso; ogni coppia consecutiva
        ///     di elementi viene valutata rispetto la distanza massima indicata; se la 
        ///     distanza e' superiore, si procede con la connesione dei due elementi, 
        ///     operazione che dipende dal tipo dei due elementi; il controllo, se richiesto,
        ///     viene esteso anche tra il primo e l'ultimo elemento, al fine di chiudere il 
        ///     percorso.
        /// </summary>
        ///******************************************************************************
        public void Connect()
        {
            Connect(MathUtil.QUOTE_EPSILON, 0);
        }

        ///******************************************************************************
        /// <summary>
        ///     Connessione degli elementi interni del percorso; ogni coppia consecutiva
        ///     di elementi viene valutata rispetto la distanza massima indicata; se la 
        ///     distanza e' superiore, si procede con la connesione dei due elementi, 
        ///     operazione che dipende dal tipo dei due elementi; il controllo, se richiesto,
        ///     viene esteso anche tra il primo e l'ultimo elemento, al fine di chiudere il 
        ///     percorso. Per quest'ultima chiusura osservare il significato del parametro
        ///     'close_dist'.
        /// </summary>
        /// <param name="connection_dist">
        ///     distanza massima ammessa per definire connessi due elementi
        /// </param>
        /// <param name="close_dist">
        ///     indica la distanza minima tra primo ed ultimo punto al di sopra della quale
        ///     si forza la chiusura del percorso.
        /// </param>
        ///******************************************************************************
        public virtual void Connect(double connection_dist, double close_dist)
        {
            // se il numero di elementi e' inferiore a due non si esegue alcun controllo
            if (Count < 2)
                return;

            // per tutte le coppie di elementi
            for (int i = 0; i < Count - 1; i++)
            {
                Side s = mSides[i].ConnectSides(mSides[i + 1], connection_dist);
                if (s != null)
                {
                    mSides.Insert(i + 1, s);
                    i++;
                }
            }

            // controllo primo / ultimo elemento
            double dist = mSides[Count - 1].P2.Distance(mSides[0].P1);
            if (dist > close_dist)
            {
                Side s = mSides[Count - 1].ConnectSides(mSides[0], connection_dist);
                if (s != null)
                {
                    mSides.Add(s);
                }
            }
        }

        ///******************************************************************************
        /// <summary>
        ///     Connessione degli elementi interni del percorso; ogni coppia consecutiva
        ///     di elementi viene valutata rispetto la distanza massima indicata; se la 
        ///     distanza e' superiore, si procede con la connesione dei due elementi, 
        ///     operazione che dipende dal tipo dei due elementi; il controllo, se richiesto,
        ///     viene esteso anche tra il primo e l'ultimo elemento, al fine di chiudere il 
        ///     percorso. Per quest'ultima chiusura osservare il significato del parametro
        ///     'close_dist'.
        /// </summary>
        /// <param name="connection_dist">
        ///     distanza massima ammessa per definire connessi due elementi
        /// </param>
        /// <param name="close_dist">
        ///     indica la distanza massima tra primo ed ultimo punto al di sotto della quale
        ///     si forza la chiusura del percorso.
        /// </param>
        ///******************************************************************************
        public virtual void ConnectWithinTolerance(double connection_dist, double close_dist)
        {
            // se il numero di elementi e' inferiore a due non si esegue alcun controllo
            if (Count < 2)
                return;

            // per tutte le coppie di elementi
            for (int i = 0; i < Count - 1; i++)
            {
                Side s = mSides[i].ConnectSides(mSides[i + 1], connection_dist);
                if (s != null)
                {
                    mSides.Insert(i + 1, s);
                    i++;
                }
            }

            // controllo primo / ultimo elemento
            double dist = mSides[Count - 1].P2.Distance(mSides[0].P1);
            if (dist <= close_dist)
            {
                Side s = mSides[Count - 1].ConnectSides(mSides[0], connection_dist);
                if (s != null)
                {
                    mSides.Add(s);
                }
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se il percorso e' chiuso, a meno della distanza indicata
        /// </summary>
        /// <param name="max_dist">
        ///     distanza massima ammessa per considerare il percorso chiuso.
        /// </param>
        /// <returns>
        ///     false = il percorso e' aperto
        ///     true  = il percorso e' chiuso
        /// </returns>
        ///**************************************************************************
        public bool IsPathClosed(double max_dist)
        {
            //Nessun lato
            if (mSides.Count == 0)
                return false;

            //Legge il primo lato
            Side first = mSides[0];

            //Lato unico
            if (mSides.Count == 1)
                //Se ha un solo lato, verifica se si tratta di una circonferenza completa
                return (first.IsClosed);

            //Legge l'ultimo lato
            Side last = mSides[mSides.Count - 1];

            // distanza
            double dist = first.P1.Distance(last.P2);
            if (dist > max_dist)
                return false;
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se il percorso e' chiuso e connesso, a meno della 
        ///     distanza indicata
        /// </summary>
        /// <param name="max_dist">
        ///     distanza massima ammessa per considerare il percorso chiuso.
        /// </param>
        /// <returns>
        ///     false = il percorso e' aperto
        ///     true  = il percorso e' chiuso
        /// </returns>
        ///**************************************************************************
        public bool IsPathFullyClosed(double max_dist)
        {
            //Nessun lato
            if (mSides.Count == 0)
                return false;

            //Legge il primo lato
            Side first = mSides[0];

            //Lato unico
            if (mSides.Count == 1)
                //Se ha un solo lato, verifica se si tratta di una circonferenza completa
                return (first.IsClosed);

            double dist;

            for (int i = 0; i < mSides.Count - 1; i++)
            {
                //Legge l'ultimo lato
                Side l1 = mSides[i];
                Side l2 = mSides[i + 1];

                // distanza
                dist = l1.P2.Distance(l2.P1);
                if (dist > max_dist)
                    return false;
            }


            //Legge l'ultimo lato
            Side last = mSides[mSides.Count - 1];

            // distanza
            dist = first.P1.Distance(last.P2);
            if (dist > max_dist)
                return false;

            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     Clona gli attributi del percorso corrente a partire da quelli del
        ///     percorso indicato.
        /// </summary>
        /// <param name="src">
        ///     percorso origine della clonazione.
        /// </param>
        ///**************************************************************************
        public void CloneAttributes(Path src)
        {
            Attributes.Clear();
            foreach (var attr in src.Attributes)
                Attributes.Add(attr.Key, attr.Value);
        }

        ///**************************************************************************
        /// <summary>
        ///     Crea un nuovo nome univoco e se richiesto aggiorna i parametri relativi
        ///     del percorso (attributo e nome del percorso)
        /// </summary>
        /// <param name="set_attribute">
        ///     se true applica il nuovo nome all'attributo interno
        /// </param>
        /// <param name="set_name">
        ///     se true sostituisce il nome corrente del percorso
        /// </param>
        /// <returns>
        ///     nuovo nome univoco  creato
        /// </returns>
        ///**************************************************************************
        public string CreateNewUniqueName(bool set_attribute, bool set_name)
        {
            string key = "INST_" + (ui64InstanceCount++).ToString(CultureInfo.InvariantCulture);

            if (set_attribute)
            {
                if (Attributes.ContainsKey("__INTERNAL_NAME_"))
                    Attributes["__INTERNAL_NAME_"] = key;
                else
                    Attributes.Add("__INTERNAL_NAME_", key);
            }

            if (set_name)
                Name = key;

            return key;
        }

        ///**************************************************************************
        /// <summary>
        ///     Get dell'attributo in this.Attributes
        /// </summary>
        /// <param name="key">
        ///     this.Attributes key
        /// </param>
        /// <param name="value">
        ///     this.Attributes value
        /// </param>
        /// <returns>
        ///     1 se diverso
        ///     0 se uguale
        ///     -1 se non esiste l'attributo
        /// </returns>
        ///**************************************************************************
        public int GetAttribute(string key, ref string value)
        {
            if (Attributes.ContainsKey(key))
            {
                string tmp = Attributes[key];
                if (value != tmp)
                {
                    value = tmp;
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            return -1;
        }
        public int GetAttribute(string key, ref double value)
        {
            if (Attributes.ContainsKey(key))
            {
                double tmp;
                if (double.TryParse(Attributes[key], NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out tmp))
                {
                    if (value != tmp)
                    {
                        value = tmp;
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            return -1;
        }
        public int GetAttribute(string key, ref int value)
        {
            if (Attributes.ContainsKey(key))
            {
                int tmp;
                if (int.TryParse(Attributes[key], out tmp))
                {
                    if (value != tmp)
                    {
                        value = tmp;
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            return -1;
        }
        public int GetAttribute(string key, ref bool value)
        {
            if (Attributes.ContainsKey(key))
            {
                bool tmp;
                if (bool.TryParse(Attributes[key], out tmp))
                {
                    if (value != tmp)
                    {
                        value = tmp;
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            return -1;
        }
        public static bool GetAttributeRatingCode(Breton.Polygons.Path path, out string value)
        {
            if (path == null || path.Attributes == null || path.Attributes.Count == 0)
            {
                value = null;
                return false;
            }

            foreach (var attr in path.Attributes)
            {
                string k = attr.Key;
                string v = attr.Value;
                if (k.StartsWith("DrawAttribute_") && k.EndsWith("_C"))
                {
                    value = v;
                    return true;
                }
            }

            value = null;
            return false;
        }
        public static bool GetAttributeRatingValue(Breton.Polygons.Path path, out string value)
        {
            if (path == null || path.Attributes == null || path.Attributes.Count == 0)
            {
                value = null;
                return false;
            }

            foreach (var attr in path.Attributes)
            {
                string k = attr.Key;
                string v = attr.Value;
                if (k.StartsWith("DrawAttribute_") && k.EndsWith("_V"))
                {
                    value = v;
                    return true;
                }
            }

            value = null;
            return false;
        }
        public bool IsAttributeEqualTo(string key, string value)
        {
            if (Attributes.ContainsKey(key))
            {
                return Attributes[key] == value;
            }
            else
            {
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Set dell'attributo in this.Attributes
        /// </summary>
        /// <param name="key">
        ///     this.Attributes key
        /// </param>
        /// <param name="value">
        ///     this.Attributes value
        /// </param>
        /// <returns>
        ///     2 se esisteva e viene sovrascritto con diverso valore
        ///     1 se esisteva e con lo stesso valore
        ///     0 se viene aggiunto
        ///     -1 se non viene aggiunto
        /// </returns>
        ///**************************************************************************
        public int SetAttribute(string key, string value, bool doNotWriteIfDoesntExist = false)
        {
            if (doNotWriteIfDoesntExist)
            {
                string old = null;
                if (GetAttribute(key, ref old) != -1 && old != value)
                {
                    return SetAttribute(key, value);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                string old = null;
                if (GetAttribute(key, ref old) != -1)
                {
                    Attributes[key] = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    if (value != old)
                        return 2;
                    else
                        return 1;
                }
                else
                {
                    Attributes.Add(key, value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    return 0;
                }
            }
        }
        public int SetAttribute(string key, double value, bool doNotWriteIfDoesntExist = false)
        {
            if (doNotWriteIfDoesntExist)
            {
                double old = double.MaxValue;
                if (GetAttribute(key, ref old) != -1 && old != value)
                {
                    return SetAttribute(key, value);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                double old = double.MaxValue;
                if (GetAttribute(key, ref old) != -1)
                {
                    Attributes[key] = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    if (value != old)
                        return 2;
                    else
                        return 1;
                }
                else
                {
                    Attributes.Add(key, value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    return 0;
                }
            }
        }
        public int SetAttribute(string key, int value, bool doNotWriteIfDoesntExist = false)
        {
            if (doNotWriteIfDoesntExist)
            {
                int old = int.MaxValue;
                if (GetAttribute(key, ref old) != -1 && old != value)
                {
                    return SetAttribute(key, value);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                int old = int.MaxValue;
                if (GetAttribute(key, ref old) != -1)
                {
                    Attributes[key] = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    if (value != old)
                        return 2;
                    else
                        return 1;
                }
                else
                {
                    Attributes.Add(key, value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    return 0;
                }
            }
        }
        public int SetAttribute(string key, bool value, bool doNotWriteIfDoesntExist = false)
        {
            if (doNotWriteIfDoesntExist)
            {
                bool old = false;
                if (GetAttribute(key, ref old) != -1 && old != value)
                {
                    return SetAttribute(key, value);
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                bool old = false;
                if (GetAttribute(key, ref old) != -1)
                {
                    Attributes[key] = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
                    if (value != old)
                        return 2;
                    else
                        return 1;
                }
                else
                {
                    Attributes.Add(key, value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    return 0;
                }
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Set dell'attributo in this.Attributes
        /// </summary>
        /// <param name="key">
        ///     this.Attributes key
        /// </param>
        /// <returns>
        ///     true se l'attributo esiste
        /// </returns>
        ///**************************************************************************
        public bool HasAttribute(string key)
        {
            if (Attributes.ContainsKey(key))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Remove dell'attributo in this.Attributes
        /// </summary>
        /// <param name="key">
        ///     this.Attributes key
        /// </param>
        /// <returns>
        ///     true se l'attributo esisteva e viene rimosso
        /// </returns>
        ///**************************************************************************
        public bool RemoveAttribute(string key)
        {
            if (Attributes.ContainsKey(key))
            {
                Attributes.Remove(key);
                return true;
            }
            else
            {
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Set dell'attributo in this.Attributes
        /// </summary>
        /// <param name="key">
        ///     this.Attributes key
        /// </param>
        /// <param name="t">
        ///     type per TryParse
        /// </param>
        /// <returns>
        ///     true se l'attributo esiste
        /// </returns>
        ///**************************************************************************
        public bool HasAttribute(string key, Type t)
        {
            if (Attributes.ContainsKey(key))
            {
                if (t == typeof(double))
                {
                    double tmp;
                    return double.TryParse(key, out tmp);
                }
                else if (t == typeof(int))
                {
                    int tmp;
                    return int.TryParse(key, out tmp);
                }
                else if (t == typeof(bool))
                {
                    bool tmp;
                    return bool.TryParse(key, out tmp);
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        /// Freccia come percorso chiuso
        /// </summary>
        /// <param name="arrowPoints">punti</param>
        /// <param name="transform">transform</param>
        /// <returns>true se s ha una lunghezza > 0</returns>
        ///**************************************************************************
        public static bool GetArrow(Segment arrowPoints, RTMatrix transform, out Breton.Polygons.Path p)
        {
            Segment s;
            if (transform.IsNeutral)
            {
                s = arrowPoints;
            }
            else
            {
                s = new Segment(arrowPoints);
                s.Transform(transform);
            }
            double l = s.Length;
            if (l <= 0)
            {
                p = null;
                return false;
            }

            double r1 = l * 0.005;
            double r2 = l * 0.02;
            double l1 = l * 0.9;
            double l2 = l * 0.1;
            Breton.MathUtils.Vector vx = s.Tangent();
            Breton.MathUtils.Vector vy = new Breton.MathUtils.Vector(-vx.Y, vx.X);

            p = new Breton.Polygons.Path();
            p.AddVertex(s.P1);
            p.AddVertex(s.P1 + vx * 0 + vy * r1);
            p.AddVertex(s.P1 + vx * l1 + vy * r1);
            p.AddVertex(s.P1 + vx * l1 + vy * r2);
            p.AddVertex(s.P1 + vx * l + vy * 0);
            p.AddVertex(s.P1 + vx * l1 - vy * r2);
            p.AddVertex(s.P1 + vx * l1 - vy * r1);
            p.AddVertex(s.P1 + vx * 0 - vy * r1);
            p.AddVertex(s.P1);
            return true;
        }

        /// <summary>
        /// Arrotonda i punti dei lati del percorso ad un determinato numero di decimali,
        /// esegue poi la connessione dei lati nel caso in cui l'arrotondamento li abbia separati
        /// </summary>
        /// <param name="decimals">Numero di decimali da mantenere</param>
        public void RoundPointsDecimalsAndConnectSides(int decimals)
        {
            for (int i = 0; i < mSides.Count; i++)
                mSides[i].RoundPointsDecimals(decimals);

            double maxDist = Math.Pow(1.0, -(decimals + 1));
            Connect(maxDist, maxDist);
        }

        public void RoundPointsDecimals(int decimals)
        {
            for (int i = 0; i < mSides.Count; i++)
                mSides[i].RoundPointsDecimals(decimals);
        }

        public void InvertSideDirections()
        {
            for (int i = 0; i < mSides.Count; i++)
                mSides[i].Reverse();
        }

        /// <summary>
        /// Corregge la direzione degli archi del percorso basandosi sui lati di tipo segmento che dovrebbero essere gi� corretti
        /// </summary>
        public void CorrectArcDirections()
        {
            Point lastPoint = null;
            int firstValidSide = -1;
            //trovo il primo lato di tipo segmento per avere un riferimento corretto
            for (int i = 0; i < mSides.Count; i++)
            {
                Side s = mSides[i];
                Segment seg = s as Segment;
                if (seg != null)
                {
                    firstValidSide = i;
                    lastPoint = seg.P2;
                    break;
                }
            }
            if (firstValidSide < 0) //se non trovo almeno un segmento non posso continuare
                return;
            //elaboro dal primo lato valido verso all'ultimo
            for (int i = firstValidSide + 1; i < mSides.Count; i++)
            {
                Side s = mSides[i];
                Arc arc = s as Arc;
                if (arc != null && !arc.P1.AlmostEqual(lastPoint))
                    arc.Reverse();

                lastPoint = s.P2;
            }
            lastPoint = mSides[firstValidSide].P1;
            //elaboro dal primo lato valido verso il primo (torno indietro)
            for (int i = (firstValidSide - 1); i >= 0; i--)
            {
                Side s = mSides[i];
                Arc arc = s as Arc;
                if (arc != null && !arc.P2.AlmostEqual(lastPoint))
                    arc.Reverse();

                lastPoint = s.P1;
            }
        }

        #endregion

        #region Private Methods

        /// ********************************************************************
        /// <summary>
        /// Conversione da enumerativo EN_PATH_MODE a stringa
        /// </summary>
        /// <param name="e">valore da convertire</param>
        /// <returns>stringa</returns>
        /// ********************************************************************
        private string EN_PATH_MODE_To_String(EN_PATH_MODE e)
        {
            return e.ToString();
        }

        /// ********************************************************************
        /// <summary>
        /// Conversione da stringa a enumerativo EN_PATH_MODE
        /// </summary>
        /// <param name="s">stringa da convertire</param>
        /// <returns>enumerativo</returns>
        /// ********************************************************************
        private EN_PATH_MODE String_To_EN_PATH_MODE(string s)
        {
            if (EN_PATH_MODE_To_String(EN_PATH_MODE.EN_PATH_MODE_EXTERIOR) == s)
                return EN_PATH_MODE.EN_PATH_MODE_EXTERIOR;

            if (EN_PATH_MODE_To_String(EN_PATH_MODE.EN_PATH_MODE_INTERIOR) == s)
                return EN_PATH_MODE.EN_PATH_MODE_INTERIOR;

            return EN_PATH_MODE.EN_PATH_MODE_UNKNOW;
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcolo il centroide del poligono; se richiesto determina
        ///     un punto comunque all'interno del poligono, che non e' esattamente
        ///     il centroide ma che e' comunque all'interno del poligono.
        /// </summary>
        /// <param name="mustbeinside">
        ///     indica se il baricentro deve essere all'interno della figura
        /// </param>
        /// <param name="distfromexact">
        ///     nel caso il baricentro non sia all'interno della figura, indica
        ///     la distanza dal punto esatto.
        /// </param>
        /// <returns>
        ///     Baricentro calcolato
        /// </returns>
        /// <remarks>
        ///     il poligono non deve contenere archi.
        /// </remarks>
        ///**************************************************************************
        private Point Centroid(bool mustbeinside, out double distfromexact)
        {
            // calcolo centroide esatto
            Point cnt = Centroid();
            distfromexact = 0;

            // se non e' richiesto che sia all'interno del poligono
            if (!mustbeinside || IsInside(cnt))
                return cnt;

            // determino il massimo raggio di ricerca delle intersezioni
            Rect r = GetRectangle();
            double maxr = Math.Max(r.Width, r.Height) / 2;

            Arc a = new Arc(cnt, 0.5, 0, Math.PI * 2);
            List<Point> ip = null;

            // ricerca delle intersezioni tra il poligono e un cerchio con centro il 
            // centroide corretto, in questo caso esterno al poligono
            while (a.Radius < maxr)
            {
                // ci sono intersezioni ?
                if (!Intersect(a, out ip))
                {
                    a.Radius += 0.5;
                    continue;
                }

                // se c'e' una sola intersezione allora si prosegue
                if (ip.Count == 1)
                {
                    a.Radius += 0.5;
                    continue;
                }

                // si calcola un punto medio a distanza pari al raggio
                Point mid = (ip[0] + ip[1]) / 2;
                Segment s = new Segment(cnt, mid);
                double delta = a.Radius - s.Length;
                Point cnti = null;
                if (delta <= 0)
                    cnti = mid;
                else
                {
                    s.Extend(Side.EN_VERTEX.EN_VERTEX_END, delta);
                    cnti = s.P2;
                }

                if (IsInside(cnti))
                {
                    distfromexact = cnt.Distance(cnti);
                    return cnti;
                }

                // si prosegue la ricerca
                a.Radius += 0.5;
            }

            // se uscito senza intersezioni, si tratta di un errore logico
            // si ritorna un valore nullo.
            return null;
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcolo il centroide del poligono; il punto non e' necessariamente
        ///     interno al poligono.
        /// </summary>
        /// <returns>
        ///     Baricentro calcolato
        /// </returns>
        /// <remarks>
        ///     il poligono non deve contenere archi.
        /// </remarks>
        ///**************************************************************************
        private Point Centroid()
        {
            Point[] pts = GetVertexList();

            // Add the first point at the end of the array.
            int nuPoints = VertexCount - 1;

            // Find the centroid.
            double X = 0;
            double Y = 0;
            double second_factor;
            for (int i = 0; i < nuPoints; i++)
            {
                second_factor =
                    pts[i].X * pts[i + 1].Y -
                    pts[i + 1].X * pts[i].Y;
                X += (pts[i].X + pts[i + 1].X) * second_factor;
                Y += (pts[i].Y + pts[i + 1].Y) * second_factor;
            }

            // Divide by 6 times the polygon's area.
            double polygon_area = SurfaceSign;
            X /= (6 * polygon_area);
            Y /= (6 * polygon_area);

            return new Point(X, Y);
        }

        #endregion

        #region Protected Methods

        ///**************************************************************************
        /// <summary>
        /// CreateSegment: create a new segment object
        /// </summary>
        /// <remarks>created segment</remarks> 
        //**************************************************************************
        protected virtual Segment CreateSegment()
        {
            return new Segment();
        }

        ///**************************************************************************
        /// <summary>
        /// CreateSegment: create a new segment object
        /// </summary>
        /// <param name="p1">first segment's point</param>
        /// <param name="p2">second segment's point</param>
        /// <remarks>created segment</remarks> 
        //**************************************************************************
        protected virtual Segment CreateSegment(Point p1, Point p2)
        {
            return new Segment(p1, p2);
        }

        ///**************************************************************************
        /// <summary>
        /// CreateArc: create a new arc object
        /// </summary>
        /// <remarks>created arc</remarks> 
        //**************************************************************************
        protected virtual Arc CreateArc()
        {
            return new Arc();
        }

        #endregion
    }
}

