using System;
using System.Drawing.Drawing2D;
using Breton.MathUtils;

namespace Breton.Polygons
{
	public class Point
	{
		#region Variables
		
		private double mX;
		private double mY;

		#endregion

		#region Constructors

		public Point(double x, double y)
		{
			mX = x;
			mY = y;
		}
		public Point(): this(0d,0d)
		{}
		public Point(Point p): this(p.X, p.Y)
		{}

		#endregion

		#region Properties

		public double X
		{
			set { mX = value; }			
			get { return mX; }
		}
		public double Y
		{
			set { mY = value; }			
			get { return mY; }
		}
			   		
		//**************************************************************************
		// gAbs
		// Restituisce la distanza del punto dall'origine
		//**************************************************************************
		public double Abs
		{
			get { return Math.Sqrt(Abs2); }
		}
		
		//**************************************************************************
		// gAbs2
		// Restituisce il quadrato della distanza del punto dall'origine
		//**************************************************************************
		public double Abs2
		{
			get { return (mX * mX + mY * mY); }
		}

		#endregion

        #region Operator overloading

        public static Point operator + (Point p, Vector v)
        {
            return new Point(p.X + v.X, p.Y + v.Y);
        }
		public static Point operator + (Point p, double val)
		{
			return new Point(p.X + val, p.Y + val);
		}
		public static Point operator + (Point p1, Point p2)
		{
			return new Point(p1.X + p2.X, p1.Y + p2.Y);
		}

        public static Point operator - (Point p, Vector v)
        {
            return new Point(p.X - v.X, p.Y - v.Y);
        }
		public static Point operator - (Point p, double val)
		{
			return new Point(p.X - val, p.Y - val);
		}
		public static Point operator - (Point p1, Point p2)
		{
			return new Point(p1.X - p2.X, p1.Y - p2.Y);
		}

		public static Point operator * (Point p, double val)
		{
			return new Point(p.X * val, p.Y * val);
		}
		public static Point operator * (Point p1, Point p2)
		{
			return new Point(p1.X * p2.X, p1.Y * p2.Y);
		}
		public static Point operator * (RTMatrix m, Point p)
		{
            double x, y;
            m.PointTransform(p.X, p.Y, out x, out y);
            return new Point(x,y);
		}

		public static Point operator / (Point p, double val)
		{
			return new Point(p.X / val, p.Y / val);
		}
		public static Point operator / (Point p1, Point p2)
		{
			return new Point(p1.X / p2.X, p1.Y / p2.Y);
		}

		public static bool operator == (Point p1, Point p2)
		{
			if ((object) p1 == null)
				return ((object)p2 == null);

			if ((object)p2 == null)
				return false;

			return (MathUtil.IsEQ(p1.X, p2.X) && MathUtil.IsEQ(p1.Y, p2.Y));
		}

		public static bool operator <(Point p1, Point p2)
		{
			if ((object)p1 == null)
				return false;

			if ((object)p2 == null)
				return false;

			return (MathUtil.IsLT(p1.X, p2.X) || MathUtil.IsEQ(p1.X, p2.X) && MathUtil.IsLT(p1.Y, p2.Y));
		}

		public static bool operator >(Point p1, Point p2)
		{
			if ((object)p1 == null)
				return false;

			if ((object)p2 == null)
				return false;

			return (MathUtil.IsGT(p1.X, p2.X) || MathUtil.IsEQ(p1.X, p2.X) && MathUtil.IsGT(p1.Y, p2.Y));
		}

		public static bool operator !=(Point p1, Point p2)
		{
			return !(p1 == p2);
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		public static explicit operator double(Point p)
		{
			return p.Abs;
		}

        public static explicit operator Vector(Point p)
        {
            return new Vector(p.X, p.Y);
        }

		#endregion

		#region Public Methods

		//**************************************************************************
		// Set
		// Imposta le coordinate uguali a quelle di un altro punto
		// Parametri:
		//			p	: punto da cui prelevare le coordinate
		//**************************************************************************
		public void Set(Point p)
		{
			mX = p.X;
			mY = p.Y;
		}

		//**************************************************************************
		// Distance
		// Ritorna la distanza del punto dal punto dato
		// Parametri:
		//			p	: punto dal quale calcolare la distanza
		//**************************************************************************
		public double Distance(Point p)
		{
			return Math.Sqrt((mX - p.X) * (mX - p.X) + (mY - p.Y) * (mY - p.Y));
		}

        //**************************************************************************
        // IsDistanceLesserThan
        // Ritorna true se la distanza del punto dal punto dato � < tol
        // Parametri:
        //			p	: punto dal quale calcolare la distanza
        //**************************************************************************
        public bool IsDistanceLesserThan(Point p, double tol = 0.01)
        {
            double dx = p.X - this.X;
            double dy = p.Y - this.Y;
            return Math.Sqrt(dx * dx + dy * dy) < tol;
        }

		//**************************************************************************
		// Direction
		// Calcola la direzione del segmento che collega il punto corrente con quello
		// indicato come parametro.
		// Parametri:
		//			p	: punto dal quale calcolare la direzione
		//**************************************************************************
		public double Direction(Point p)
		{
			double    alpha;

			/* controllo caso particolare (segmento verticale) */
			if (MathUtil.IsEQ(mX, p.X))
				if (p.Y > mY)
					return (Math.PI / 2d);
				else
					return (3d / 2d * Math.PI);

			/* altro caso particolare (segmento orizzontale) */
			if (MathUtil.IsEQ(mY, p.Y))
				if (p.X > mX)
					return 0d;
				else
					return Math.PI;

			/* caso generale */
			alpha = Math.Atan((p.Y - mY) / (p.X - mX));
			if (p.Y > mY)
				if (p.X > mX)
					return alpha;
				else
					return (alpha + Math.PI);

			if (p.X > mX)
				return (2d * Math.PI + alpha);
			else
				return (Math.PI + alpha);


			/*
			double dx, dy, atn2;
		
			dx = p.X - mX;
			dy = p.Y - mY;
    
			if (MathUtil.IsZero(dx) < 0.0000001d)
			{
				if(dy > 0d)		atn2 = Math.PI / 2d;
				else			atn2 = -Math.PI / 2d;
			}
			else
			{
				atn2 = Math.Atan(dy / dx);
				if (dy > 0d)
				{
					if (dx < 0d)		atn2 = atn2 + Math.PI;
				}
				else
				{
					if (dx < 0d)     atn2 = atn2 - Math.PI;
				}   
			}
			
			if (atn2 < 0d)	atn2 = atn2 + 2d * Math.PI;    
			if (atn2 >= 2d * Math.PI)	atn2 = atn2 - 2d * Math.PI;
			
			return atn2;
			*/
		}

		//**************************************************************************
		// Surface2
		// Ritorna il doppio dell'area (con segno) definita da 3 punti
		// Parametri:
		//			b	: 2' punto
		//			c	: 3' punto
		//**************************************************************************
		public double Surface2(Point b, Point c)
		{
			return (b.X - mX) * (c.Y - mY) - (c.X - mX) * (b.Y - mY);
		}

		//**************************************************************************
		// RotoTrasl
		// Ritorna un nuovo punto che � la rototraslazione di quello attuale.
		// Parametri:
		//			offset	: offset di traslazione
		//			rot		: angolo di rotazione (in radianti)
		//**************************************************************************
		public Point RotoTrasl(Point offset, double rot)
		{
			return new Point(Math.Cos(rot) * mX - Math.Sin(rot) * mY + offset.X, 
				Math.Sin(rot) * mX + Math.Cos(rot) * mY + offset.Y);
		}

        //**************************************************************************
        // RotoTrasl
        // Ritorna un nuovo punto che � la rototraslazione di quello attuale.
        // Parametri:
        //			matrx	: matrice di traslazione
        //**************************************************************************
        public Point RotoTrasl(RTMatrix matrx)
        {
            double x, y, rot;
            matrx.GetRotoTransl(out x, out y, out rot);
            return RotoTrasl(new Point(x,y), rot);
        }

        //**************************************************************************
		// Collinear
		// Verifica se 3 punti sono allineati
		// Parametri:
		//			q		: 2' punto
		//			r		: 3' punto
		//			epsilon	: epsilon per determinare il dato nullo
		// Ritorna:
		//			true	punti allineati
		//			false	punti non allineati
		//**************************************************************************
		public bool Collinear(Point q, Point r)
		{
			return (SignOfDeterminat2x2(mX - r.X, mX - q.X, mY - r.Y, mY - q.Y) == 0);
		}

        public static bool AreCollinear(Point p0, Point p1, Point p2, double tol = MathUtil.FLT_EPSILON)
        {
            // (n - b)(x - m) == (y - n)(m - a)  per i punti p0(a,b) p1(m,n) p2(x,y)
            // (n - b)(x - m) - (y - n)(m - a) == 0

            return MathUtil.IsAlmost((p1.Y - p0.Y) * (p2.X - p1.X) - (p2.Y - p1.Y) * (p1.X - p0.X), 0d, tol);
        }

		//**************************************************************************
		// CollinearAndOrdered
		// Verifica se 3 punti sono allineati e ordinati
		// Parametri:
		//			q		: 2' punto
		//			r		: 3' punto
		//			epsilon	: epsilon per determinare il dato nullo
		// Ritorna:
		//			true	punti allineati e ordinati
		//			false	punti non allineati o non ordinati
		//**************************************************************************
		public bool CollinearAndOrdered(Point q, Point r)
		{
			if (!Collinear(q, r))
				return false;
    
			if (mX < q.X)
				if (q.X < r.X)
					return true;
    
			if (q.X < mX)
				if (r.X < q.X)
					return true;
    
			if (mY < q.Y)
				if (q.Y < r.Y)
					return true;
	
			if (q.Y < mY)
				if (r.Y < q.Y)
					return true;

			return false;
		}		

		//**************************************************************************
		// Near
		// Verifica se 3 punti vicini
		// Parametri:
		//			q		: 2' punto
		//			r		: 3' punto
		//			maxDist	: massima distanza per determinare che sono vicini
		// Ritorna:
		//			true	punti vicini
		//			false	punti lontani
		//**************************************************************************
		public bool Near(Point q, Point r, double maxDist)
		{
			double a = r.Y - mY;
			double b = mX - r.X;
			double c = r.X * mY - mX * r.Y;
    
			if (a == 0d && b == 0d && c == 0d)
				return true;
     
			double d = Math.Abs(a * q.X + b * q.Y + c) / Math.Sqrt(a * a + b * b);

			if (d <= maxDist)
				return true;
			
			return false;
		}

		//**************************************************************************
		// PuntoDaModuloDirezione
		// Calcola il punto che si ottiene spostanto il punto corrente di una
		// quantita' pari a 'modulo' nella direzione 'dir'
		// Parametri:
		//           modulo	: modulo dello spostamento
		//           dir	: direzione dello spostamento (angolo in radianti)
		// Restituisce:
		//           punto risultate.
		//**************************************************************************
		public Point PuntoDaModuloDirezione(double modulo, double dir)
		{
			return new Point(mX + modulo * Math.Cos(dir), 
							 mY + modulo * Math.Sin(dir));
		}

        //**************************************************************************
        // AlmostEqual
        // Verifica se due punti sono uguali a meno di un delta definito
        // Parametri:
        //           p	    : punto di controllo
        //           delta	: entita' massima della differenza.
        // Restituisce:
        //           false : i due punti non coincidono
        //           true  : i due punti coincidono
        //**************************************************************************
        public bool AlmostEqual( Point p, double delta )
        {
            double dist = Distance(p);
            return (dist <= delta ? true : false);
        }

        //**************************************************************************
        // AlmostEqual
        // Verifica se due punti sono uguali a meno di un delta fisso
        // Parametri:
        //           p	    : punto di controllo
        // Restituisce:
        //           false : i due punti non coincidono
        //           true  : i due punti coincidono
        //**************************************************************************
        public bool AlmostEqual(Point p)
        {
            return AlmostEqual(p, MathUtil.QUOTE_EPSILON);
        }

        #endregion

		#region Private Methods

		//**************************************************************************
		// SignOfDeterminat2x2
		// Calcola il segno del determinante di una matrice 2x2
		// Parametri:
		//			a00	: elemento 00 matrice
		//			a01	: elemento 01 matrice
		//			a10	: elemento 10 matrice
		//			a11	: elemento 11 matrice
		// Ritorna:
		//			0	determinante nullo
		//			1	determinante positivo
		//			-1	determinante negativo
		//**************************************************************************
		private int SignOfDeterminat2x2(double a00, double a01, double a10, double a11)
		{
			double det;
    
			det = (a00 * a11) - (a01 * a10);
    
			// Segno pari a 0
			if (MathUtil.IsZero(det))
				return 0;

			if (det > 0d)
				return 1;
			else
				return -1;
		}
		
		#endregion

	}
}
