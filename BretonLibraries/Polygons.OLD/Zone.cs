using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Breton.MathUtils;
using TraceLoggers;

namespace Breton.Polygons
{
    public class Zone
    {
        #region Variables

        // Elemento Zone, per la collection di zone interne
        public struct ZoneElement
        {
            public Path path;
            public Zone zone;

            public ZoneElement(Path p, Zone z)
            {
                path = p;
                zone = z;
            }
            public ZoneElement(ZoneElement ze)
            {
                path = new Path(ze.path);
                zone = new Zone(ze.zone);
            }

            public ZoneElement GetRT(RTMatrix rtm)
            {
                Path p = new Path(path);
                p.MatrixRT = rtm;
                p = p.GetPathRT();

                Zone z = zone.GetRT(rtm);

                ZoneElement ze = new ZoneElement(p, z);
                return ze;
            }

        }

        private Path mPath;							// Percorso esterno
        private ArrayList mZones;					// Zone interne
        protected RTMatrix mMatrixRT;				// matrice di rototraslazione

        private SortedList<string, string> mAttributes;	// Lista degli attributi del path
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Costruisce la zona vuota
        /// </summary>
        ///*********************************************************************
        public Zone()
        {
            mPath = null;
            mZones = new ArrayList();
            mMatrixRT = new RTMatrix();
            mAttributes = new SortedList<string, string>();
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce la zone con un solo percorso esterno
        /// </summary>
        /// <param name="p">percorso esterno</param>
        ///*********************************************************************
        public Zone(Path p)
            : this()
        {
            if (p == null)
                mPath = null;
            else
            {
                SetPath(p);
            }
        }


        ///*********************************************************************
        /// <summary>
        /// Costruisce la zone come copia di un altra zona
        /// </summary>
        /// <param name="z">zone da copiare</param>
        ///*********************************************************************
        public Zone(Zone z)
            : this()
        {
            if (z == null)
                return;

            mMatrixRT = new RTMatrix(z.mMatrixRT);

            // Percorso esterno non esistente
            if (z.mPath == null)
            {
                mPath = null;
                return;
            }

            // Percorso esterno
            SetPath(z.mPath);

            // Aggiunge tutti i lati del percorso
            for (int i = 0; i < z.mZones.Count; i++)
            {
                ZoneElement ze = new ZoneElement((ZoneElement)z.mZones[i]);
                if(Breton.Polygons.Path.AddedPathInClonedZoneCCW_AllZonesContainedCW == false)
                    SetOrientationToAddingZoneElement(this, ze);
                mZones.Add(ze);
            }

            // Aggiunge tutti gli attributi
            foreach (KeyValuePair<string, string> a in z.mAttributes)
                mAttributes.Add(a.Key, a.Value);
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce la zona definita da un rettangolo
        /// </summary>
        /// <param name="r">rettangolo</param>
        ///*********************************************************************
        public Zone(Rect r)
            : this(new Path(r))
        { }

        #endregion

        #region Properties
        ///**************************************************************************
        /// <summary>
        ///     Inserisce o modifica il valore di un attributo
        /// </summary>
        /// <param name="key">
        ///     chiave dell'attributo
        /// </param>
        /// <param name="value">
        ///     valore dell'attributo
        /// </param>
        ///**************************************************************************
        public void AttributeAddOrUpdate( string key, string value )
        {
            if (Attributes.ContainsKey( key ))
                Attributes[key] = value;
            else
                Attributes.Add( key, value );
        }

        //**************************************************************************
        // ZoneCount
        // Ritorna il numero di zone interne
        //**************************************************************************
        public int ZoneCount
        {
            get { return mZones.Count; }
        }

        //**************************************************************************
        // MatrixRT
        // Ritorna la matrice di rototraslazione
        //**************************************************************************
        public RTMatrix MatrixRT
        {
            set
            {
                mMatrixRT = value;

                if (mPath != null)
                {
                    mPath.MatrixRT = mMatrixRT;

                    // Imposta la stessa matrice di rototraslazione per tutti i percorsi
                    foreach (ZoneElement ze in mZones)
                    {
                        ze.path.MatrixRT = mMatrixRT;
                        ze.zone.MatrixRT = mMatrixRT;
                    }
                }
            }
            get { return mMatrixRT; }
        }

        //**************************************************************************
        // RotAngle
        // Ritorna l'angolo di rotaazione del percorso
        //**************************************************************************
        public double RotAngle
        {
            get
            {
                double offsetX = 0d, offsetY = 0d, angle = 0d;

                mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);

                return angle;
            }
        }

        //**************************************************************************
        // TranslPoint
        // Ritorna il punto di traslazione del percorso
        //**************************************************************************
        public Point TranslPoint
        {
            get
            {
                double offsetX = 0d, offsetY = 0d, angle = 0d;

                mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);

                return new Point(offsetX, offsetY);
            }
        }

        //**************************************************************************
        // Surface
        // Restituisce l'area dell'insieme di percorsi
        //**************************************************************************
        public double Surface
        {
            get
            {
                // Verifica che esista il percorso esterno
                if (mPath == null)
                    return 0d;

                double area = mPath.Surface;

                // Somma le aree di tutti i percorsi
                foreach (ZoneElement ze in mZones)
                    area -= ze.zone.Surface;

                return area;
            }
        }

        //**************************************************************************
        /// <summary>
        /// Ritorna il livello di zone contenuto nella zona stessa
        /// </summary>
        //**************************************************************************
        public int InnerLevels
        {
            get
            {
                if (ZoneCount == 0)
                    return 0;

                int max = 0;
                foreach (ZoneElement ze in mZones)
                {
                    int level = ze.zone.InnerLevels;
                    if (level > max)
                        max = level;
                }
                return max + 1;
            }
        }

        ///**************************************************************************
        /// <summary>
        /// Restituisce la lista degli attributi del path
        /// </summary>
        ///**************************************************************************
        public SortedList<string, string> Attributes
        {
            get { return mAttributes; }
        }

        #endregion

        #region Public Methods

        /// **************************************************************************
        /// <summary>
        /// Setta il percorso esterno.
        /// </summary>
        /// <param name="p">percorso da settare</param>
        /// <returns>
        ///		true	percorso settato
        ///		false	non � possibile inserire il percorso
        ///	</returns>
        /// **************************************************************************
        public bool SetPath(Path p)
        {
            // Verifica che il percorso sia valido e che non sia gi� stato inserito
            if (p == null || mPath != null)
                return false;

            mPath = p.Clone();
            mPath.SetRotationSense(MathUtil.RotationSense.CCW);
            mPath.MatrixRT = mMatrixRT;
            return true;
        }

        //**************************************************************************
        // RemovePath
        // Rimuove il percorso esterno
        // Restituisce:
        //			true	percorso eliminato
        //			false	non � possibile eliminare il percorso
        //**************************************************************************
        public bool RemovePath()
        {
            if (mZones.Count > 0)
                return false;

            mPath = null;
            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna il percorso esterno della zona
        /// </summary>
        /// <returns>percorso esterno della zona</returns>
        /// **************************************************************************
        public Path GetPath()
        {
            return mPath;
        }

        /// **************************************************************************
        /// <summary>
        /// Aggiunge una zone
        /// </summary>
        /// <param name="z">zone da aggiungere</param>
        /// <param name="forceAddOpenPaths">aggiungo percorsi aperti come isole interne</param>
        /// <param name="tryParallelize">for di aggiunta zone contenute in z viene eseguito in parallelo se this.mZones.Count > 10 zones</param>
        /// <returns>
        ///		true	zone inserita
        ///		se	non � possibile inserire la zone
        ///	</returns>
        /// **************************************************************************
        public bool AddZone(Zone z, bool forceAddOpenPaths = false, bool tryParallelize = true)
        {
            try
            {
                // Verifica se la zone pu� essere inserita
                if (forceAddOpenPaths)
                {
                    if (z.GetPath() != null && z.GetPath().IsClosed && !MayAddZone(z))
                        return false;
                }
                else
                {
                    if (!MayAddZone(z))
                        return false;
                }

                // Verifica se la zone pu� essere inserita in una zone interna
                if (AddZoneDelegate != null && tryParallelize && mZones.Count > 10)
                {
                    //Se � disponibile un delegato per la parallellizzazione
                    AddZoneDelegate(mZones, z);
                }
                else
                {
                    lock(mZones)
                    {
                        foreach (ZoneElement ze in mZones)
                            if (ze.zone.AddZone(z))
                                return true;
                    }
                }

                if (forceAddOpenPaths)
                    if (z.GetPath() != null && !z.GetPath().IsClosed && mPath == null)
                        mPath = (Path)Activator.CreateInstance(z.GetPath().GetType());

                // Altrimenti la inserisce allo stesso livello delle altre zone
                Path p = CreatePath(z);
                p.MatrixRT = mMatrixRT;
                ZoneElement zoneEl = new ZoneElement(p, new Zone(z));
                lock(mZones)
                {
                    SetOrientationToAddingZoneElement(this, zoneEl);
                    mZones.Add(zoneEl);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        protected virtual Path CreatePath(Zone z)
        {
            return new Path(z.mPath);
        }

        public delegate O P2Delegate<P1, P2, O>(P1 parameter1, P2 parameter2);
        public delegate O P5Delegate<P1, P2, P3, P4, P5, O>(P1 parameter1, P2 parameter2, P3 parameter3, P4 parameter4, P5 parameter5);
        public static P2Delegate<ArrayList, Zone, bool> AddZoneDelegate = null;
        public static P5Delegate<ArrayList, Zone, bool, Point, int, bool> NormalizeZoneDelegate = null;

        /// **************************************************************************
        /// <summary>
        /// Aggiunta di una zone a this anche se non � contenuta nel path this.mPath
        /// </summary>
        /// <param name="z">zone da aggiungere</param>
        /// <returns>
        ///		true	zone inserita
        ///	</returns>
        /// **************************************************************************
        public bool ForceAddZone(Zone z)
        {
            // la inserisce allo stesso livello delle altre zone
            Path p = new Path(z.mPath);
            p.MatrixRT = mMatrixRT;
            ZoneElement zoneEl = new ZoneElement(p, new Zone(z));
            zoneEl.path.SetRotationSense(MathUtil.RotationSense.CW);
            mZones.Add(zoneEl);
            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Set orientazione della zone da aggiungere
        /// Breton.Polygons.Path.AddedPathInClonedZoneCCW_AllZonesContainedCW = true DEFAULT
        ///   funzionamento default; con un clone di una Zone, elementi contenuti ZoneElement.zone.mPath sempre CCW; ZoneElement.path ha orientazione originaria di eventuale costruttore copia
        /// Breton.Polygons.Path.AddedPathInClonedZoneCCW_AllZonesContainedCW = false
        ///   ZoneElement Zone.mZones[i] ha ZoneElement.zone.mPath con orientazione contraria risp. a zone.mPath; stesso per ZoneElement.path
        /// </summary>
        /// <param name="container">zone in cui si sta aggiungendo ze (in container.mZones)</param>
        /// <param name="ze">ZoneElement che si sta aggiungendo</param>
        /// <returns>true se � stato fatto qualcosa</returns>
        /// **************************************************************************
        private static bool SetOrientationToAddingZoneElement(Zone container, ZoneElement ze)
        {
            //funzionamento precedente l'aggiunta di AddedPathInClonedZoneCCW_AllZonesContainedCW a false
            if (Breton.Polygons.Path.AddedPathInClonedZoneCCW_AllZonesContainedCW == true)
                ze.path.SetRotationSense(MathUtil.RotationSense.CW);

            //AddedPathInClonedZoneCCW_AllZonesContainedCW a true
            if (Breton.Polygons.Path.AddedPathInClonedZoneCCW_AllZonesContainedCW == false &&
                container.mPath != null &&
                ze.zone.mPath != null && ze.zone.mPath.CheckIfIsClosed)
            {
                MathUtil.RotationSense rs;
                if (container.mPath.SenseOfRotation == MathUtil.RotationSense.CCW)
                    rs = MathUtil.RotationSense.CW;
                else
                    rs = MathUtil.RotationSense.CCW;

                ze.path.SetRotationSense(rs);
                ze.zone.mPath.SetRotationSense(rs);

                return true;
            }
            else
            {
                return false;
            }
        }

        //**************************************************************************
        // RemoveZone
        // Rimuove una zone
        // Parametri:
        //			index	: indice zone da rimuovere
        // Restituisce:
        //			true	zone eliminato
        //			false	non � possibile eliminare la zone
        //**************************************************************************
        public bool RemoveZone(int index)
        {
            // Verifica l'indice
            if (index < 0 || index >= mZones.Count)
                return false;

            mZones.RemoveAt(index);

            // Ritorna il numero di percorsi
            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna la zona specificata
        /// </summary>
        /// <param name="index">indice della zona da ritornare</param>
        /// <returns>zona</returns>
        /// **************************************************************************
        public Zone GetZone(int index)
        {
            // Verifica l'indice
            if (index < 0 || index >= mZones.Count)
                return null;

            return ((ZoneElement)mZones[index]).zone;
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna l'eventuale percorso interno della sottozona indicata
        /// </summary>
        /// <param name="index">indice della zona da considerare</param>
        /// <returns>
        ///     percorso interno della sotto zona indicata
        /// </returns>
        /// **************************************************************************
        public Path GetZonePath(int index)
        {
            // Verifica l'indice
            if (index < 0 || index >= mZones.Count)
                return null;

            return ((ZoneElement)mZones[index]).path;
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna l'eventuale percorso interno della sottozona indicata, considerando
        /// la matrice di rototraslazione principale.
        /// </summary>
        /// <param name="index">indice della zona da considerare</param>
        /// <returns>
        ///     percorso interno della sotto zona indicata
        /// </returns>
        /// **************************************************************************
        public Path GetZonePathRT(int index)
        {
            // Verifica l'indice
            if (index < 0 || index >= mZones.Count)
                return null;

            Path p = new Path(((ZoneElement)mZones[index]).path);
            p.MatrixRT = this.MatrixRT * p.MatrixRT;

            return p.GetPathRT();
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna l'eventuale percorso interno della sottozona indicata, considerando
        /// la matrice di rototraslazione indicata.
        /// </summary>
        /// <param name="index">indice della zona da considerare</param>
        /// <param name="rtm">matrice di rototraslazione da considerare</param>
        /// <returns>
        ///     percorso interno della sotto zona indicata
        /// </returns>
        /// **************************************************************************
        public Path GetZonePathRT(int index, RTMatrix rtm)
        {
            // Verifica l'indice
            if (index < 0 || index >= mZones.Count)
                return null;

            Path p = new Path(((ZoneElement)mZones[index]).path);
            p.MatrixRT = rtm;

            return p.GetPathRT();
        }

        //**************************************************************************
        // GetRectangle
        // Ritorna il rettangolo che contiene la zone
        //**************************************************************************
        public Rect GetRectangle()
        {
            if (mPath == null)
                return null;

            if (mPath.Count == 0 && mZones != null && mZones.Count > 0)
            {
                Rect zonesRectangle = null;
                if (mZones != null)
                    for (int i = 0; i < mZones.Count; i++)
                    {
                        var zoneRectangle = GetZone(i).GetRectangle();
                        if (zoneRectangle != null)
                        {
                            if (zonesRectangle == null)
                                zonesRectangle = zoneRectangle;
                            else
                                zonesRectangle = zoneRectangle.Union(zonesRectangle);
                        }
                    }
                return zonesRectangle;
            }

            return mPath.GetRectangle();
        }

        //**************************************************************************
        // GetRectangleRT
        // Ritorna il rettangolo che contiene l'insieme dei percorsi rototraslati
        //**************************************************************************
        public Rect GetRectangleRT()
        {
            if (mPath == null)
                return null;

            if (mPath.Count == 0 && mZones != null && mZones.Count > 0)
            {
                Rect zonesRectangle = null;
                if (mZones != null)
                    for (int i = 0; i < mZones.Count; i++)
                    {
                        var zoneRectangle = GetZone(i).GetRectangleRT();
                        if (zoneRectangle != null)
                        {
                            if (zonesRectangle == null)
                                zonesRectangle = zoneRectangle;
                            else
                                zonesRectangle = zoneRectangle.Union(zonesRectangle);
                        }
                    }
                return zonesRectangle;
            }

            return mPath.GetRectangleRT();
        }

        /// **************************************************************************
        /// <summary>
        /// Rototraslazione assoluta
        /// </summary>
        /// <param name="offsetX">offset di traslazione X del percorso</param>
        /// <param name="offsetY">offset di traslazione Y del percorso</param>
        /// <param name="centerX">ascissa X del centro di rotazione</param>
        /// <param name="centerY">ascissa Y del centro di rotazione</param>
        /// <param name="angle">angolo di rotazione in radianti</param>
        /// **************************************************************************
        public void RotoTranslAbs(double offsetX, double offsetY, double centerX, double centerY, double angle)
        {
            mMatrixRT.RotoTranslAbs(offsetX, offsetY, centerX, centerY, angle);
        }

        /// **************************************************************************
        /// <summary>
        /// Rototraslazione relativa
        /// </summary>
        /// <param name="offsetX">offset di traslazione X del percorso</param>
        /// <param name="offsetY">offset di traslazione Y del percorso</param>
        /// <param name="centerX">ascissa X del centro di rotazione</param>
        /// <param name="centerY">ascissa Y del centro di rotazione</param>
        /// <param name="angle">angolo di rotazione in radianti</param>
        /// **************************************************************************
        public void RotoTranslRel(double offsetX, double offsetY, double centerX, double centerY, double angle)
        {
            mMatrixRT.RotoTranslRel(offsetX, offsetY, centerX, centerY, angle);
        }

        /// **************************************************************************
        /// <summary>
        /// Legge la traslazione e l'angolo di rotazione della zone
        /// </summary>
        /// <param name="offsetX">traslazione X</param>
        /// <param name="offsetY">traslazione Y</param>
        /// <param name="angle">angolo di rotazione in radianti</param>
        /// **************************************************************************
        public void GetRotoTransl(out double offsetX, out double offsetY, out double angle)
        {
            mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);
        }

        /// **************************************************************************
        /// <summary>
        /// Setta la traslazione e l'angolo di rotazione della zone
        /// </summary>
        /// <param name="offsetX">traslazione X</param>
        /// <param name="offsetY">traslazione Y</param>
        /// <param name="angle">angolo di rotazione in radianti</param>
        /// **************************************************************************
        public void SetRotoTransl(double offsetX, double offsetY, double angle)
        {
            mMatrixRT.SetRotoTransl(offsetX, offsetY, angle);
        }

        /// **************************************************************************
        /// <summary>
        /// ritorna la zona con tutti i percorsi rototraslati
        /// </summary>
        /// <param name="rtm">matrice di rototraslazione da considerare</param>
        /// <returns>zona rototraslata</returns>
        /// **************************************************************************
        public Zone GetRT(RTMatrix rtm)
        {
            Zone z = new Zone();

            foreach (var att in Attributes)
                z.Attributes.Add(att.Key, att.Value);

            if (rtm == null)
            {
                rtm = this.MatrixRT;
                z.SetPath(GetPath().GetPathRT());
            }
            else
            {
                Path p = GetPath().Clone();
                p.MatrixRT = rtm;
                z.SetPath(p.GetPathRT());
            }

            foreach (ZoneElement ze in mZones)
                z.mZones.Add(ze.GetRT(rtm));

            return z;
        }

        /// **************************************************************************
        /// <summary>
        /// ritorna la zona con tutti i percorsi rototraslati
        /// </summary>
        /// <returns>zona rototraslata</returns>
        /// **************************************************************************
        public Zone GetRT()
        {
            return GetRT(null);
        }

        /// **************************************************************************
        /// <summary>
        /// Normalizza la zona (solo traslazione)
        /// </summary>
        /// <returns>zona normalizzata</returns>
        /// **************************************************************************
        public Zone Normalize(bool forceAddOpenPaths = false)
        {
            return Normalize(null, forceAddOpenPaths);
        }

        /// **************************************************************************
        /// <summary>
        /// Normalizza la zona (rotazioni e traslazione)
        /// </summary>
        /// <returns>zona normalizzata</returns>
        /// **************************************************************************
        public Zone NormalizeRT(bool forceAddOpenPaths = false)
        {
            return NormalizeRT(null);
        }

        /// **************************************************************************
        /// <summary>
        /// Normalizza la zona (solo traslazione)
        /// </summary>
        /// <param name="offset">punto di offset</param>
        /// <returns>zona normalizzata</returns>
        /// **************************************************************************
        public Zone Normalize(Point offset, bool forceAddOpenPaths = false)
        {
            // Verifica il percorso esterno
            if (mPath == null)
                return null;

            if (offset == null)
            {
                // Legge il rettangolo che contiene la zone
                Rect rect = GetRectangle();
                offset = new Point(rect.Left, rect.Top);
            }

            // Crea la nuova zone normalizzata
            Zone newReg = Normalize(offset, 0, forceAddOpenPaths);

            // Aggiunge tutti gli attributi
            foreach (KeyValuePair<string, string> a in mAttributes)
                newReg.Attributes.Add(a.Key, a.Value);

            // Ricalcola la matrice di rototraslazione
            newReg.MatrixRT = new RTMatrix(mMatrixRT);
            RTMatrix rtm = new RTMatrix(offset.X, offset.Y, 0);
            newReg.MatrixRT = newReg.MatrixRT * rtm;

            return newReg;
        }

        /// **************************************************************************
        /// <summary>
        /// Normalizza la zona (rotazione e traslazione)
        /// </summary>
        /// <param name="offset">punto di offset</param>
        /// <returns>zona normalizzata</returns>
        /// **************************************************************************
        public Zone NormalizeRT(Point offset, bool forceAddOpenPaths = false)
        {
            // Verifica il percorso esterno
            if (mPath == null)
                return null;

            // Crea la nuova zone normalizzata
            Zone newReg = GetRT();

            // normalizza
            return newReg.Normalize(offset);
        }

        /// **************************************************************************
        /// <summary>
        /// Normalizza la zona
        /// </summary>
        /// <param name="offset">punto di offset</param>
        /// <param name="level">livello di ricorsivit�</param>
        /// <returns>zona normalizzata</returns>
        /// **************************************************************************
        public Zone Normalize(Point offset, int level, bool forceAddOpenPaths = false)
        {
            // Verifica il percorso esterno
            if (mPath == null)
                return null;

            // Crea la nuova zone normalizzata
            Zone newReg = new Zone(mPath.Normalize(offset));

            if (NormalizeZoneDelegate != null && mZones.Count > 10)
            {
                NormalizeZoneDelegate(mZones, newReg, forceAddOpenPaths, offset, level);
            }
            else
            {
                foreach (ZoneElement ze in mZones)
                    newReg.AddZone(ze.zone.Normalize(offset, level + 1), forceAddOpenPaths);
            }

            return newReg;
        }

        /// ********************************************************************
        /// <summary>
        /// Trasforma il poligono, scala data da autovalore maggiore
        /// </summary>
        /// <returns>transform != null</returns>
        /// ********************************************************************
        public bool Transform( Breton.MathUtils.RTMatrix transform)
        {
            if (transform != null)
            {
                if (GetPath() != null)
                    GetPath().Transform(transform);

                for (int i = 0; i < ZoneCount; i++)
                    GetZone(i).Transform(transform);

                return true;
            }
            else
                return false;
        }

        //**************************************************************************
        // MayAddZone
        // Verifica se il percorso � inseribile
        // Parametri:
        //			index	: posizione di inserimento
        //			p		: percorso da inserire
        // Restituisce:
        //			true	: il percorso pu� essere inseribile
        //			false	: il percorso non pu� essere inserito
        //**************************************************************************
        public bool MayAddZone(Zone z)
        {
            // Verifica che la zone sia valida e con un percorso esterno
            if (z == null || z.mPath == null)
                return false;

            // Verifica che la zone attuale abbia un percorso esterno chiuso
            if (mPath == null || !mPath.IsClosed)
                return false;

            // Il percorso esterno della zone deve essere completamente contenuto nel percorso esterno
            return mPath.Contains(z.mPath);
        }

        ///*********************************************************************
        /// <summary>
        /// Semplifica il percorso eliminando i punti allineati
        /// </summary>
        /// <param name="maxDist">distanza max per considerare tre punti allineati</param>
        ///*********************************************************************
        public void Simplify(double maxDist)
        {
            // Verifica che esista il percorso esterno
            if (mPath != null)
            {
                // Semplifica il percorso esterno
                mPath.Simplify(maxDist);

                // Semplifica tutte le zone interne
                foreach (ZoneElement ze in mZones)
                    ze.zone.Simplify(maxDist);
            }
        }

        ///*********************************************************************
        /// <summary>
        /// Rimuove i punti non validi
        /// </summary>
        ///*********************************************************************
        public void RemoveInvalidPoints()
        {
            // Verifica che esista il percorso esterno
            if (mPath != null)
            {
                // Rimuove i punti non validi dal percorso esterno
                mPath.RemoveInvalidPoints();

                // Rimuove i punti non validi da tutte le zone interne
                foreach (ZoneElement ze in mZones)
                    ze.zone.RemoveInvalidPoints();
            }
        }

        //**************************************************************************
        // ReadFileXml
        // Legge il lato dal file XML
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public bool ReadFileXml(XmlNode n)
        {
            return ReadFileXml(n, -1);
        }

        public bool ReadFileXml(XmlNode n, double maxdist)
        {
            return ReadFileXml(n, maxdist, false);
        }

        public bool ReadFileXml(XmlNode n, double maxdist, bool forceAddOpenPaths)
        {
            try
            {
                XmlNodeList l = n.ChildNodes;

                // Scorre tutti i sottonodi
                foreach (XmlNode chn in l)
                {
                    // Legge il nodo del percorso esterno
                    if (chn.Name == "Path")
                    {
                        mPath = CreatePath();
                        if (!mPath.ReadFileXml(chn))
                        {
                            mPath = null;
                            return false;
                        }
                        else
                        {
							//if (!mPath.IsPathFullyClosed(MathUtil.QUOTE_EPSILON))
							//{
							//    mPath = null;
							//    return true;
							//}
							//else 
							if (maxdist >= 0)
                                this.Simplify(maxdist);
                        }

                    }

                    // Legge una sotto zona
                    else if (chn.Name == "Zone")
                    {
                        Zone z = CreateZone();
                        if (!z.ReadFileXml(chn, maxdist, forceAddOpenPaths))
                            return false;

                        AddZone(z, forceAddOpenPaths);
                    }

                        // Legge il nodo della matrice
                    else if (chn.Name == "MatrixRT")
                    {
                        double[,] m = new double[3, 3];

                        m[0, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m00").Value, CultureInfo.InvariantCulture);
                        m[0, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m01").Value, CultureInfo.InvariantCulture);
                        m[0, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m02").Value, CultureInfo.InvariantCulture);
                        m[1, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m10").Value, CultureInfo.InvariantCulture);
                        m[1, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m11").Value, CultureInfo.InvariantCulture);
                        m[1, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m12").Value, CultureInfo.InvariantCulture);
                        m[2, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m20").Value, CultureInfo.InvariantCulture);
                        m[2, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m21").Value, CultureInfo.InvariantCulture);
                        m[2, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m22").Value, CultureInfo.InvariantCulture);

                        mMatrixRT.Matrix = m;
                    }
                    else if (chn.Name == "Attributes")
                    {
                        foreach (XmlNode attr in chn.ChildNodes)
                        {
                            XmlAttribute val = attr.Attributes["value"];
                            mAttributes.Add(attr.Name, val.Value);
                        }
                    }

                }

                // Assegna la matrice di rototraslazione alla zona e tutte le zone interne
                MatrixRT = mMatrixRT;

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("PathCollection.ReadFileXml Error.", ex);
                return false;
            }
        }

        protected virtual Zone CreateZone()
        {
            return new Zone();
        }

        protected virtual Path CreatePath()
        {
            return new Path();
        }

        //**************************************************************************
        // WriteFileXml
        // Scrive il lato su file XML
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public void WriteFileXml(XmlTextWriter w)
        {
            WriteFileXml(w, 0);
        }
        internal void WriteFileXml(XmlTextWriter w, int level)
        {
            w.WriteStartElement("Zone");

            w.WriteStartElement("Attributes");
            foreach (KeyValuePair<string, string> a in mAttributes)
            {
                w.WriteStartElement(a.Key);
                w.WriteAttributeString("value", a.Value);
                w.WriteEndElement();
            }
            w.WriteEndElement();

            // Se � la prima 
            if (level == 0)
            {
                double[,] m = mMatrixRT.Matrix;
                // Scrive la matrice di rototraslazione
                w.WriteStartElement("MatrixRT");
                w.WriteAttributeString("m00", m[0, 0].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m01", m[0, 1].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m02", m[0, 2].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m10", m[1, 0].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m11", m[1, 1].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m12", m[1, 2].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m20", m[2, 0].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m21", m[2, 1].ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("m22", m[2, 2].ToString(CultureInfo.InvariantCulture));
                w.WriteEndElement();
            }

            if (mPath == null)
                return;

            // Salva il percorso
            mPath.WriteFileXml(w, false);

            // Scrive tutti le zone
            foreach (ZoneElement ze in mZones)
                ze.zone.WriteFileXml(w, level + 1);

            w.WriteEndElement();
        }

        ///**************************************************************************
        /// <summary>
        /// Scrive la zona sul nodo XML
        /// </summary>
        /// <param name="baseNode">nodo XML dove scrivere il path</param>
        /// <param name="saveMatrix">livello</param>
        ///**************************************************************************
        public void WriteFileXml(XmlNode baseNode)
        {
            WriteFileXml(baseNode, 0);
        }

        internal void WriteFileXml(XmlNode baseNode, int level)
        {
            XmlDocument doc = baseNode.OwnerDocument;
            XmlAttribute attr;
            XmlNode node = doc.CreateNode(XmlNodeType.Element, "Zone", "");

            // Inserisce gli attributi del path
            XmlNode attributes = doc.CreateNode(XmlNodeType.Element, "Attributes", ""); ;
            foreach (KeyValuePair<string, string> a in mAttributes)
            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, a.Key, ""); ;
                attr = doc.CreateAttribute("value");
                attr.Value = a.Value;
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }
            node.AppendChild(attributes);

            if (level == 0)
            {
                double[,] m = mMatrixRT.Matrix;

                // Scrive la matrice di rototraslazione
                XmlNode matrix = doc.CreateNode(XmlNodeType.Element, "MatrixRT", "");

                attr = doc.CreateAttribute("m00");
                attr.Value = m[0, 0].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m01");
                attr.Value = m[0, 1].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m02");
                attr.Value = m[0, 2].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m10");
                attr.Value = m[1, 0].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m11");
                attr.Value = m[1, 1].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m12");
                attr.Value = m[1, 2].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m20");
                attr.Value = m[2, 0].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m21");
                attr.Value = m[2, 1].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);
                attr = doc.CreateAttribute("m22");
                attr.Value = m[2, 2].ToString(CultureInfo.InvariantCulture);
                matrix.Attributes.Append(attr);

                node.AppendChild(matrix);
            }

            if (mPath == null)
                return;

            // Salva il percorso
            mPath.WriteFileXml(node, false);

            // Scrive tutti le zone
            foreach (ZoneElement ze in mZones)
                ze.zone.WriteFileXml(node, level + 1);

            baseNode.AppendChild(node);
        }

        ///**************************************************************************
        /// <summary>
        /// Ritorna la PathCollection che contiene tutti i path della zone
        /// </summary>
        /// <param name="pc">PathCollection da creare</param>
        ///**************************************************************************
        public void GetPathCollection(ref PathCollection pc)
        {
            // Crea la PathCollection, se necessario
            if (pc == null)
            {
                pc = new PathCollection();

                // Aggiunge tutti gli attributi
                foreach (KeyValuePair<string, string> a in mAttributes)
                    pc.Attributes.Add(a.Key, a.Value);
            }

            // Se � il primo path, associa anche la matrice di rototraslazione
            if (pc.Count == 0)
                pc.MatrixRT = new RTMatrix(mMatrixRT);

            // Verifica se esiste il path
            if (mPath == null)
                return;

            // Aggiunge il path alla collection
            pc.AddPath(mPath);

            // Recupera tutti i path delle zone interne
            foreach (ZoneElement ze in mZones)
                ze.zone.GetPathCollection(ref pc);

        }

        ///******************************************************************************
        /// <summary>
        /// Divide tutti i percorsi che sono cerchi completi in due archi di ampiezza
        /// pari a 180 gradi.
        /// </summary>
        /// <param name="z">zona di riferimento</param>
        ///******************************************************************************
        public void DivideCircles()
        {
            // controllo percorso esterno
            GetPath().DivideCircles();

            // scansione percorsi interni
            foreach (ZoneElement ze in mZones)
            {
                ze.path.DivideCircles();
                ze.zone.DivideCircles();
            }
        }

        ///******************************************************************************
        /// <summary>
        /// Verifica se un punto e' all'interno della zona (cioe' all'interno del percorso
        /// esterno e all'aesterno dei eventuali percorsi interni)
        /// </summary>
        /// <param name="p">
        ///     punto oggetto della verifica
        /// </param>
        /// <returns>
        ///     false il punto non e' all'intero
        ///     true il punto e' all'interno della zona
        /// </returns>
        ///******************************************************************************
        public bool IsPointInside(Point p)
        {
            if (GetPath().IsInside(p) == false)
                return false;

            for (int i = 0; i < ZoneCount; i++)
                if (GetZone(i).IsPointInside(p))
                    return false;

            return true;
        }

        ///******************************************************************************
        /// <summary>
        /// Verifica se un punto e' all'interno della zona (cioe' all'interno del percorso
        /// esterno e all'aesterno dei eventuali percorsi interni)
        /// </summary>
        /// <param name="p">
        ///     punto oggetto della verifica
        /// </param>
        /// <returns>
        ///     false il punto non e' all'intero
        ///     true il punto e' all'interno della zona
        /// </returns>
        ///******************************************************************************
        public bool IsPointInsideRT(Point p)
        {
            if (GetPath().IsInsideRT(p) == false)
                return false;

            for (int i = 0; i < ZoneCount; i++)
                if (GetZone(i).IsPointInsideRT(p))
                    return false;

            return true;
        }

        /// <summary>
        /// Arrotonda i punti dei lati dei percorsi della zona ad un determinato numero di decimali
        /// </summary>
        /// <param name="decimals">Numero di decimali da mantenere</param>
        public void RoundPointsDecimals(int decimals)
        {
            mPath.RoundPointsDecimalsAndConnectSides(decimals);
            for (int i = 0; i < ZoneCount; i++)
                GetZone(i).RoundPointsDecimals(decimals);
        }

        /// <summary>
        /// Verifica se la zona interseca un path
        /// </summary>
        /// <param name="path">path da verificare</param>
        /// <param name="checkInternalZones">indica se considerare o meno anche i percorsi interni</param>
        /// <returns>True se il percorso interseca la zona o eventualmente un suo percorso interno, False altrimenti</returns>
        public bool Intersect(Path path, bool checkInternalZones = true)
        {
            if (mPath.Intersect(path))
                return true;

            //TODO: Valutare la parallellizzazione delle intersezioni con le zone interne
            if (checkInternalZones)
                for (int i = 0; i < ZoneCount; i++)
                    if (GetZone(i).Intersect(path, checkInternalZones))
                        return true;

            return false;
        }

        /// <summary>
        /// Verifica se la zona contiene l'intero path
        /// </summary>
        /// <param name="path">Path da verificare</param>
        /// <param name="checkInternalZones">indica se considerare o meno anche i percorsi interni</param>
        /// <returns>True se il percorso � completamente contenuto nella zona e in nessuno dei suoi percorsi interni, False altrimenti</returns>
        public bool Contains(Path path, bool checkInternalZones = true)
        {
            if (!mPath.Contains(path))
                return false;

            //TODO: Valutare la parallellizzazione delle intersezioni con le zone interne
            if (checkInternalZones)
                for (int i = 0; i < ZoneCount; i++)
                    if (GetZone(i).Contains(path, checkInternalZones))
                        return false;

            return true;
        }

        #endregion

    }
}
