using System;
using System.Collections;
using System.Globalization;
using System.Xml;
using Breton.MathUtils;

namespace Breton.Polygons
{
	public class PathZone: PathCollection
	{
		/// <summary>
		/// PathZone � un insieme di percorsi costituito da un percorso esterno chiuso con 0, 1 o pi� percorsi interni chiusi o aperti
		/// </summary>
		#region Variables

		#endregion

		#region Constructors

		//**************************************************************************
		// Costruisce l'insieme vuoto
		//**************************************************************************
		public PathZone(): base()
		{
		}

		//**************************************************************************
		// Costruisce l'insieme di percorsi come copia di un altro insieme
		// Parametri:
		//			z	: zona da copiare
		//**************************************************************************
		public PathZone(PathZone pz): this()
		{
			if (pz.Count == 0)
				return;
			
			// Aggiunge tutti i lati del percorso
			for (int i = 0; i < pz.Count; i++)
				mPaths.Add(pz.GetPath(i));

			mMatrixRT = pz.MatrixRT;
		}

		//**************************************************************************
		// operator PathZone
		// Converte la PathCollection in una PathZone
		// Parametri:
		//			pc	: PathCollection da convertire
		//**************************************************************************
		public PathZone(PathCollection pc): this()
		{
			int posExt;

			if (!PathZone.IsPathZone(pc, out posExt))
				throw new ApplicationException("Error creating a PathZone from a PoligonCollection error.");

			// Aggiunge il percorso esterno
			mPaths.Add(pc.GetPath(posExt));

			// Aggiunge tutti i percorsi interni
			for (int i = 0; i < pc.Count; i++)
				if (i != posExt)
					mPaths.Add(pc.GetPath(i));
		}

		#endregion

		#region Properties

		#endregion

		#region Public Methods

		//**************************************************************************
		// AddPath
		// Aggiunge un percorso
		// Parametri:
		//			p	: percorso da aggiungere
		// Restituisce:
		//			true	percorso inserito
		//			false	non � possibile inserire il percorso
		//**************************************************************************
		public override bool AddPath(Path p)
		{
			// Verifica se il percorso pu� essere inserito
			if (!MayAdd(p))
				return false;

			// Se � il primo percorso
			if (mPaths.Count == 0)
				// Lo rendo antiorario
				p.SetRotationSense(MathUtil.RotationSense.CCW);

			// altrimenti, se � chiuso
			else if (p.IsClosed)
				// Lo rendo orario
				p.SetRotationSense(MathUtil.RotationSense.CW);
			
			mPaths.Add(new Path(p));

			return true;
		}

		//**************************************************************************
		// InsertPath
		// Inserisce un percorso in una posizione
		// Parametri:
		//			index	: indice posizione
		//			p		: percorso da aggiungere
		// Restituisce:
		//			true	percorso inserito
		//			false	non � possibile inserire il percorso
		//**************************************************************************
		public override bool InsertPath(int index, Path p)
		{
			// Verifica se il percorso pu� essere inserito
			if (!MayInsert(index, p))
				return false;

			// Se devo inserirlo come primo percorso
			if (index == 0)
				// Lo rendo antiorario
				p.SetRotationSense(MathUtil.RotationSense.CCW);

			// altrimenti, se � chiuso
			else if (p.IsClosed)
				// Lo rendo orario
				p.SetRotationSense(MathUtil.RotationSense.CW);
			
			mPaths.Insert(index, new Path(p));

			return true;
		}

		//**************************************************************************
		// RemovePath
		// Rimuove un percorso
		// Parametri:
		//			i	: indice percorso da rimuovere
		// Restituisce:
		//			true	percorso eliminato
		//			false	non � possibile eliminare il percorso
		//**************************************************************************
		public override bool RemovePath(int i)
		{
			// Verifica l'indice
			if (i < 0 || i >= mPaths.Count)
				return false;

			// Non si pu� eliminare il percorso esterno se sono presenti dei percorsi interni
			if (i == 0 && mPaths.Count == 1)
				return false;

			mPaths.RemoveAt(i);

			// Ritorna il numero di percorsi
			return true;
		}

		//**************************************************************************
		// GetRectangle
		// Ritorna il rettangolo che contiene l'insieme dei percorsi
		//**************************************************************************
		public override Rect GetRectangle()
		{
			if (mPaths.Count == 0)
				return null;

			Path p = (Path) mPaths[0];

			return p.GetRectangle();
		}

		//**************************************************************************
		// GetRectangleRT
		// Ritorna il rettangolo che contiene l'insieme dei percorsi rototraslati
		//**************************************************************************
		public override Rect GetRectangleRT()
		{
			if (mPaths.Count == 0)
				return null;

			Path p = (Path) mPaths[0];

			return p.GetRectangleRT();
		}


		//**************************************************************************
		// Empty
		// Svuota il percorso
		//**************************************************************************
		public void Empty()
		{
			mPaths.Clear();
		}		

		//**************************************************************************
		// Normalize
		// Ritorna l'insieme dei percorsi normalizzato
		// Parametri:
		//			offset	: punto di normalizzazione
		//**************************************************************************
		public override PathCollection Normalize()
		{
			Rect r = GetRectangle();
			Point offset = new Point(r.Left, r.Top);

			PathZone pz = new PathZone();

			foreach (Path p in mPaths)
				pz.mPaths.Add(p.Normalize(offset));

			pz.MatrixRT = mMatrixRT;

			return (PathCollection) pz;
		}

		//**************************************************************************
		// MayInsert
		// Verifica se il percorso � inseribile
		// Parametri:
		//			index	: posizione di inserimento
		//			p		: percorso da inserire
		// Restituisce:
		//			true	: il percorso pu� essere inseribile
		//			false	: il percorso non pu� essere inserito
		//**************************************************************************
		public override bool MayInsert(int index, Path p)
		{
			bool closed = p.IsClosed;

			// Se � il primo percorso, quello esterno
			if (mPaths.Count == 0)
				// allora deve essere chiuso
				return closed;

			// Se deve essere inserito come percorso esterno
			if (index == 0)
			{
				// Deve essere chiuso
				if (!closed)
					return false;

				// Deve contenere tutti gli altri percorsi
				foreach(Path pi in mPaths)
					if (!p.Contains(pi))
						return false;
			}

			// Se deve essere inserito come percorso interno
			else
			{
				// Deve essere completamente contenuto nel percorso esterno
				Path ext = (Path) mPaths[0];
				if (!ext.Contains(p))
					return false;
			}

			// Il percorso pu� essere inserito
			return true;
		}

		//**************************************************************************
		// IsPathZone
		// Verifica se la collezione di percorsi � una zona valida
		// Parametri:
		//			pc		: collezione di percorsi
		//			posExt	: ritorna la posizione del percorso esterno
		// Restituisce:
		//			true	: � una zona valida
		//			false	: non � una zona valida
		//**************************************************************************
		public static bool IsPathZone(PathCollection pc, out int posExt)
		{
			/* NOTA BENE:
			 * Il metodo prevede di trovare il percorso con area maggiore, considerandolo
			 * come candidato ad essere il percorso contenitore degli altri.
			 * Quindi verifica se tutti gli altri percorsi sono contenuti nel percorso
			 * contenitore. Questo metodo � ragionevolmente sicuro, ma non al 100%,
			 * soprattutto a causa del metodo con cui viene valutato se un percorso
			 * � contenuto in un altro */

			posExt = -1;

			// Verifica che contenga almeno un percorso
			if (pc.Count == 0)
				return false;

			Path ext = null, p;

			// Se contiene un solo percorso
			if (pc.Count == 1)
			{
				// Verifica che sia chiuso
				ext = pc.GetPath(0);
				if (ext.IsClosed)
					return true;
			}

			// Scorre tutti i percorsi per trovare il percorso pi� grande (area maggiore)
			double areaMax = 0d;
			for (int i = 0; i < pc.Count; i++)
			{
				p = pc.GetPath(i);
		        
				//Verifica che il percorso sia chiuso
				if (p.IsClosed)
				{
					//Calcola l'area
					double area = p.Surface;

					//Trova il percorso con area maggiore
					if (area > areaMax)
					{
						areaMax = area;
						ext = p;
						posExt = i;
					}
				}
			}

			// Verifica se ha trovato un percorso esterno
			if (ext == null)
				return false;

			// Deve contenere tutti gli altri percorsi
			for (int i = 0; i < pc.Count; i++)
				// Verifica che non sia la stessa istanza
				if (i != posExt)
				{
					p = pc.GetPath(i);
					if (!ext.Contains(p))
						return false;
				}

			// E' una zona
			return true;
		}

		//**************************************************************************
		// WriteFileXml
		// Scrive il lato su file XML
		// Parametri:
		//			w: oggetto per scrivere il file XML
		//**************************************************************************
		public override void WriteFileXml(XmlTextWriter w)
		{
			w.WriteStartElement("PathZone");

			double[,] m = mMatrixRT.Matrix;
			// Scrive la matrice di rototraslazione
			w.WriteStartElement("MatrixRT");
            w.WriteAttributeString("m00", m[0, 0].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m01", m[0, 1].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m02", m[0, 2].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m10", m[1, 0].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m11", m[1, 1].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m12", m[1, 2].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m20", m[2, 0].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m21", m[2, 1].ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("m22", m[2, 2].ToString(CultureInfo.InvariantCulture));
			w.WriteEndElement();

			// Scrive tutti i lati
			foreach (Path p in mPaths)
				p.WriteFileXml(w, false);

			w.WriteEndElement();
		}		    

		#endregion
	}
}
