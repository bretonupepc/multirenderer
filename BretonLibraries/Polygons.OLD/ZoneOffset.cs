using System;
using System.Runtime.InteropServices;
using System.Collections;
using System.Text;
using Breton.MathUtils;
using Breton.Polygons;

namespace Breton.Polygons
{

	/// <summary>
	/// Classe utilizzata per eseguire l'offset di una zona
	/// </summary>
	public class ZoneOffset
	{

		#region Structs & Enums

		public enum EXPANSION_MODE
		{
			EXPANSION_MODE_CIRC = 0,
			EXPANSION_MODE_RECT = 1
		}

		private struct OTTIMP_VERTEX
		{
			public double X;
			public double Y;
		}

		private enum OTTIMP_EN_ENT_TYPE
		{
			OTTIMP_EN_ENT_TYPE_END,			// definisce il termine di un percorso
			OTTIMP_EN_ENT_TYPE_SEG,			// segmento P1->P2
			OTTIMP_EN_ENT_TYPE_ARC			// arco orientato
		}

		private struct OTTIMP_ENT_SEG
		{
			public OTTIMP_VERTEX vxP1;
			public OTTIMP_VERTEX vxP2;
		}

		private struct OTTIMP_ENT_ARC
		{
			public OTTIMP_VERTEX vxCenter;
			public double Radius;
			public double StartAng;
			public double AmpAng;
		}

		private struct OTTIMP_ENT
		{
			public OTTIMP_EN_ENT_TYPE Type;
			private int Filler;
			public double Value0, Value1, Value2, Value3, Value4;

			public OTTIMP_ENT(OTTIMP_EN_ENT_TYPE type)
			{
				Type = type;
				Filler = 0;
				Value0 = Value1 = Value2 = Value3 = Value4 = 0d;
			}

			public OTTIMP_ENT(double x1, double y1, double x2, double y2)
				: this(OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_SEG)
			{
				Value0 = x1;
				Value1 = y1;
				Value2 = x2;
				Value3 = y2;
			}

			public OTTIMP_ENT(double xc, double yc, double r, double alpha, double beta)
				: this(OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_ARC)
			{
				Value0 = xc;
				Value1 = yc;
				Value2 = r;
				Value3 = alpha;
				Value4 = beta;
			}
		}

		/* non � possibile utilizzare la struttura come in VB6, perch� il vettore non funziona.
		 * Bisogna invece utilizzare 5 variabili double separate
		 * 
		private struct OTTIMP_ENT
		{
			public OTTIMP_EN_ENT_TYPE Type;
			public int Filler;
			public double[] Values;

			public OTTIMP_ENT(OTTIMP_EN_ENT_TYPE type)
			{
				Type = type;
				Values = new double[] {0d, 0d, 0d, 0d, 0d};
			}

			public OTTIMP_ENT(double x1, double y1, double x2, double y2): this(OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_SEG)
			{
				Values[0] = x1;
				Values[1] = y1;
				Values[2] = x2;
				Values[3] = y2;
			}

			public OTTIMP_ENT(double xc, double yc, double r, double alpha, double beta)
				: this(OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_ARC)
			{
				Values[0] = xc;
				Values[1] = yc;
				Values[2] = r;
				Values[3] = alpha;
				Values[4] = beta;
			}
		}

		*/

		// VB allinea a 4 byte, mentre la libreria CGAL usata in C++ pretende una compilazione allineamento a 8 byte

		#endregion


		#region OttPol.dll Imports

		// Funzioni di ottimizzazione a poligoni

		[DllImport("OttPol.dll", EntryPoint = "_OttPoligoni_Vers@4")]
		private static extern int OttPoligoni_Vers(StringBuilder sVers);

		//UPGRADE_WARNING: Structure OTTIMP_VERTEX may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: //ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"//
		[DllImport("OttPol.dll", EntryPoint = "_OttPoligoni_Offset@20")]
		private static extern int OttPoligoni_Offset(int num_vert, ref OTTIMP_VERTEX Pvert, double offset, int expmode);

		//UPGRADE_WARNING: Structure OTTIMP_VERTEX may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: //ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"//
		[DllImport("OttPol.dll", EntryPoint = "_OttPoligoni_OffsetGetVertex@8")]
		private static extern int OttPoligoni_OffsetGetVertex(int max_vert, ref OTTIMP_VERTEX Pvert);

		//UPGRADE_WARNING: Structure OTTIMP_ENT may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: //ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"//
		[DllImport("OttPol.dll", EntryPoint = "_OttPoligoni_OffsetEx@20")]
		private static extern int OttPoligoni_OffsetEx(int num_ent, ref OTTIMP_ENT pent, double offset, int expmode);

		//UPGRADE_WARNING: Structure OTTIMP_ENT may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: //ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1050"//
		[DllImport("OttPol.dll", EntryPoint = "_OttPoligoni_OffsetGetEnt@8")]
		private static extern int OttPoligoni_OffsetGetEnt(int max_ent, ref OTTIMP_ENT pent);

		#endregion

        #region private variables
        static private object objLockOffset = new object(); //Oggetto per evitare l'accesso multiplo all'offset di OttPol.dll in quanto provoca spesso la corruzione della memoria
        #endregion

		#region Constructors & Disposers

		public ZoneOffset()
		{
		}

		#endregion


		#region Public Static Methods

		///***************************************************************************
		/// <summary>
		/// Disassamento di una zona composta da un percorso e da eventuali fori.
		/// </summary>
		/// <param name="zone">zona</param>
		/// <param name="external_offset">offset di disassamento percorso esterno in mm
		///							maggiore 0	rimpicciolisce
		///							minore 0	ingrandisce
		/// </param>
		/// <param name="internal_offset">offset di disassamento percorsi interni in mm
		///									minore 0	rimpicciolisce
		///									maggiore 0	ingrandisce
		/// </param>
		/// <param name="mode">modo espansione percorsi</param>
		/// <param name="paths">insieme di path risultante</param>
		/// <returns>
		///		true	disassamento eseguito
		///		false	errore nell'operazione
		///	</returns>
		///***************************************************************************
		public static bool PathAndHolesOffset(ref Zone zone, double external_offset, double internal_offset, EXPANSION_MODE mode,
			out PathCollection paths)
		{
			int numEntitiesTot;

			paths = null;

			//Nessun disassamento da eseguire
			if (external_offset == 0d && internal_offset == 0d)
				return false;

			//Verifica che la zona esista
			if (zone == null || zone.GetPath() == null)
				return false;

			//Se non ci sono percorsi interni nella zona, esclude il disassamento percorsi interni
			if (zone.ZoneCount == 0)
				internal_offset = 0d;

			//Conta il numero di lati del percorso esterno della zona
			Path extPath = zone.GetPath();
			int numEntitiesExt = extPath.Count + 1;

			// Crea il vettore per contenere i vertici
			OTTIMP_ENT[] entities = new OTTIMP_ENT[numEntitiesExt];

			//Scorre tutti i lati del poligono
			for (int i = 0; i < extPath.Count; i++)
			{
				Side s = extPath.GetSide(i);
				if (s is Arc)
				{
					Point c = s.CenterPoint;
					OTTIMP_ENT ent = new OTTIMP_ENT(c.X, c.Y, s.Radius, s.StartAngle, s.AmplAngle);
					entities[i] = ent;
				}
				else
				{
					Point p1 = s.P1;
					Point p2 = s.P2;
					OTTIMP_ENT ent = new OTTIMP_ENT(p1.X, p1.Y, p2.X, p2.Y);
					entities[i] = ent;
				}
			}

			//terminatore di percorso
			entities[extPath.Count] = new OTTIMP_ENT(OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_END);

			//Esegue il disassamento del percorso esterno della zona
			if ((external_offset - internal_offset) != 0)
			{
				//Richiama l'algoritmo di disassamento per il percorso esterno della zona
                int ret = 0;
                lock (objLockOffset) 
                    ret = OttPoligoni_OffsetEx(numEntitiesExt, ref entities[0], external_offset - internal_offset, (int)mode);
				if (ret == 0)
					return false;

				//Legge i nuovi lati disassati
				numEntitiesExt = ret;
				entities = new OTTIMP_ENT[numEntitiesExt];
				ret = OttPoligoni_OffsetGetEnt(numEntitiesExt, ref entities[0]);
				if (ret <= 0)
					return false;
			}

			//Se ci sono percorsi interni nella zona, aggiunge anche i percorsi interni
			if (zone.ZoneCount > 0)
			{
				//Conta il numero di vertici
				numEntitiesTot = numEntitiesExt;
				for (int i = 0; i < zone.ZoneCount; i++)
					numEntitiesTot += zone.GetZone(i).GetPath().Count + 1;

				entities = (OTTIMP_ENT[])GenUtils.ResizeArray(entities, numEntitiesTot);

				int j = numEntitiesExt;
				//Scorre tutti i poligoni che compongono la zona
				for (int i = 0; i < zone.ZoneCount; i++)
				{
					Path intPath = zone.GetZone(i).GetPath();

					//Scorre tutti i lati del poligono
					for (int t = 0; t < intPath.Count; t++)
					{
						Side s = intPath.GetSide(t);
						if (s is Arc)
						{
							Point c = s.CenterPoint;
							OTTIMP_ENT ent = new OTTIMP_ENT(c.X, c.Y, s.Radius, s.StartAngle, s.AmplAngle);
							entities[j++] = ent;
						}
						else
						{
							Point p1 = s.P1;
							Point p2 = s.P2;
							OTTIMP_ENT ent = new OTTIMP_ENT(p1.X, p1.Y, p2.X, p2.Y);
							entities[j++] = ent;
						}
					}

					//terminatore di percorso
					entities[j++] = new OTTIMP_ENT(OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_END);
				}

				//Esegue il disassamento di zona e percorsi interni
				if (internal_offset != 0d)
				{
					//Richiama l'algoritmo di disassamento per tutta la zona
                    int ret = 0;
                    lock (objLockOffset)
                        ret = OttPoligoni_OffsetEx(numEntitiesTot, ref entities[0], internal_offset, (int)mode);
					if (ret == 0)
						return false;

					//Legge i nuovi lati disassati
					numEntitiesTot = ret;
					entities = new OTTIMP_ENT[numEntitiesTot];
					ret = OttPoligoni_OffsetGetEnt(numEntitiesTot, ref entities[0]);
					if (ret <= 0)
						return false;
				}
			}

				//Zona senza percorsi interni
			else
			{
				//Legge i nuovi vertici disassati
				numEntitiesTot = numEntitiesExt;
			}

			//Rigenera la costruzione della zona
			zone = null;
			zone = new Zone();

			//Scorre tutti i lati della zona
			double maxArea = 0d;
			Path path = null;
			int extPathIndex = 0;
			paths = new PathCollection();
			for (int i = 0; i < numEntitiesTot; i++)
			{
				//Crea il nuovo poligono
				if (path == null)
				{
					path = new Path();
					//paths.AddPath(path);
				}

				// Terminatore di percorso
				if (entities[i].Type == OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_END)
				{
					paths.AddPath(path);

					// Verifica se � il poligono maggiore
					double area = path.Surface;
					if (area > maxArea)
					{
						extPathIndex = paths.Count - 1;
						maxArea = area;
					}

					path = null;
				}

					// Arco
				else if (entities[i].Type == OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_ARC)
				{
					Side s = new Arc(entities[i].Value0, entities[i].Value1, entities[i].Value2, entities[i].Value3, entities[i].Value4);
					path.AddSide(s);
				}

					// Segmento
				else
				{
					Side s = new Segment(entities[i].Value0, entities[i].Value1, entities[i].Value2, entities[i].Value3);
					path.AddSide(s);
				}
			}

			//Chiude l'eventuale ultimo poligono non chiuso
			if (path != null)
			{
				// Verifica se � il poligono maggiore
				double area = path.Surface;
				if (area > maxArea)
				{
					extPathIndex = paths.Count - 1;
					maxArea = area;
				}

				path = null;
			}

			// Verifica se ha trovato un percorso esterno
			if (extPathIndex >= paths.Count)
				return false;

			// Imposta il percorso esterno della zona
			zone.SetPath(paths.GetPath(extPathIndex));

			// Imposta i percorsi interni
			for (int i = 0; i < paths.Count; i++)
				if (i != extPathIndex)
					zone.AddZone(new Zone(paths.GetPath(i)));

			return true;

		}


		///***************************************************************************
		/// <summary>
		/// Disassamento di una percorso
		/// </summary>
		/// <param name="extPath">percorso</param>
		/// <param name="external_offset">offset di disassamento percorso in mm
		///							maggiore 0	rimpicciolisce
		///							minore 0	ingrandisce
		/// </param>
		/// <param name="mode">modo espansione percorsi</param>
		/// <param name="paths">insieme di path risultante</param>
		/// <returns>
		///		true	disassamento eseguito
		///		false	errore nell'operazione
		///	</returns>
		///***************************************************************************
		public static bool PathOffset(ref Path extPath, double offset, EXPANSION_MODE mode,
			out PathCollection paths)
		{
			int numEntitiesTot;

			paths = null;

			//Nessun disassamento da eseguire
			if (offset == 0d)
				return false;

			//Verifica che la zona esista
			if (extPath == null)
				return false;

			//Conta il numero di lati del percorso 
			int numEntitiesExt = extPath.Count + 1;

			// Crea il vettore per contenere i vertici
			OTTIMP_ENT[] entities = new OTTIMP_ENT[numEntitiesExt];

			//Scorre tutti i lati del poligono
			for (int i = 0; i < extPath.Count; i++)
			{
				Side s = extPath.GetSide(i);
				if (s is Arc)
				{
					Point c = s.CenterPoint;
					OTTIMP_ENT ent = new OTTIMP_ENT(c.X, c.Y, s.Radius, s.StartAngle, s.AmplAngle);
					entities[i] = ent;
				}
				else
				{
					Point p1 = s.P1;
					Point p2 = s.P2;
					OTTIMP_ENT ent = new OTTIMP_ENT(p1.X, p1.Y, p2.X, p2.Y);
					entities[i] = ent;
				}
			}

			//terminatore di percorso
			entities[extPath.Count] = new OTTIMP_ENT(OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_END);

			//Esegue il disassamento del percorso
			//Richiama l'algoritmo di disassamento per il percorso esterno della zona
            int ret = 0;
            lock (objLockOffset)
                ret = OttPoligoni_OffsetEx(numEntitiesExt, ref entities[0], offset, (int)mode);
			if (ret == 0)
				return false;

			//Legge i nuovi lati disassati
			numEntitiesTot = ret;
			entities = new OTTIMP_ENT[numEntitiesTot];
			ret = OttPoligoni_OffsetGetEnt(numEntitiesTot, ref entities[0]);
			if (ret <= 0)
				return false;

			//Scorre tutti i lati della zona
			double maxArea = 0d;
			Path path = null;
			int extPathIndex = 0;
			paths = new PathCollection();
			for (int i = 0; i < numEntitiesTot; i++)
			{
				//Crea il nuovo poligono
				if (path == null)
				{
					path = new Path();
				}

				// Terminatore di percorso
				if (entities[i].Type == OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_END)
				{
					paths.AddPath(path);

					// Verifica se � il poligono maggiore
					double area = path.Surface;
					if (area > maxArea)
					{
						extPathIndex = paths.Count - 1;
						maxArea = area;
					}

					path = null;
				}

					// Arco
				else if (entities[i].Type == OTTIMP_EN_ENT_TYPE.OTTIMP_EN_ENT_TYPE_ARC)
				{
					Side s = new Arc(entities[i].Value0, entities[i].Value1, entities[i].Value2, entities[i].Value3, entities[i].Value4);
					path.AddSide(s);
				}

					// Segmento
				else
				{
					Side s = new Segment(entities[i].Value0, entities[i].Value1, entities[i].Value2, entities[i].Value3);
					path.AddSide(s);
				}
			}

			//Chiude l'eventuale ultimo poligono non chiuso
			if (path != null)
			{
				// Verifica se � il poligono maggiore
				double area = path.Surface;
				if (area > maxArea)
				{
					extPathIndex = paths.Count - 1;
					maxArea = area;
				}

				path = null;
			}

			// Verifica se ha trovato un percorso maggiore
			if (extPathIndex >= paths.Count)
				return false;

			// Imposta il percorso esterno della zona
			extPath = paths.GetPath(extPathIndex);

			return true;

		}

		#endregion

	}
}