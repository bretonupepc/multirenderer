using System;
using System.Xml;
using Breton.MathUtils;

namespace Breton.Polygons
{
	public abstract class Side
	{
        public enum EN_VERTEX
        {
            EN_VERTEX_START,
            EN_VERTEX_END
        }

        #region Variables

        protected bool bAllowSelect;               // abilita/disabilita selezione in modalita' disegno
        protected bool bHasWorking;                 // indica se su questo lato insiste una lavorazione

        #endregion

        #region Constructors

        protected Side()
        {
            bAllowSelect = false;
            bHasWorking = false;
        }

        protected Side(Side s)
        {
            bAllowSelect = s.bAllowSelect;
            bHasWorking = s.bHasWorking;
        }

        #endregion

        #region Properties

        public bool AllowSelect
        {
            set { bAllowSelect = value; }
            get { return bAllowSelect; }
        }

        public bool HasWorking
        {
            set { bHasWorking = value; }
            get { return bHasWorking; }
        }

		public abstract Point P1
		{
			get;
		}

		public abstract Point P2
		{
			get;
		}

		//**************************************************************************
		// Dx
		// Restituisce la differenza DX
		//**************************************************************************
		public abstract double Dx
		{
			get;
		}

		//**************************************************************************
		// Dy
		// Restituisce la differenza DY
		//**************************************************************************
		public abstract double Dy
		{
			get;
		}

		//******************************************************************************
		// Length
		// Restituisce la lunghezza del lato
		//******************************************************************************
		public abstract double Length
		{
			get;
		}

		//**************************************************************************
		// Inclination
		// Restituisce l'angolo del lato rispetto all'asse delle ascisse
		//**************************************************************************
		public abstract double Inclination
		{
			get;
		}

		//**************************************************************************
		// MiddlePoint
		// Restituisce il punto medio del lato
		//**************************************************************************
		public abstract Point MiddlePoint
		{
			get;
		}

		//**************************************************************************
		// VectorSide
		// Restituisce il vettore parallelo al lato
		//**************************************************************************
		public abstract Vector VectorSide
		{
			get;
		}

		//**************************************************************************
		// VectorP1
		// Restituisce il vettore tangente nel vertice 1
		//**************************************************************************
		public abstract Vector VectorP1
		{
			get;
		}

		//**************************************************************************
		// VectorP2
		// Restituisce il vettore tangente nel vertice 2
		//**************************************************************************
		public abstract Vector VectorP2
		{
			get;
		}

		//**************************************************************************
		// GetRectangle
		// Restituisce il rettangolo che contiene il lato
		//**************************************************************************
		public abstract Rect GetRectangle
		{
			get;
		}

		//**************************************************************************
		// Radius
		// Restituisce/imposta il raggio
		//**************************************************************************
		public virtual double Radius
		{
			set { }
			get { return 0d; }
		}

		//**************************************************************************
		// StartAngle
		// Restituisce/imposta l'angolo di partenza
		//**************************************************************************
		public virtual double StartAngle
		{
			set { }
			get { return 0d; }
		}

		//**************************************************************************
		// AmplAngle
		// Restituisce/imposta l'angolo di ampiezza
		//**************************************************************************
		public virtual double AmplAngle
		{
			set { }
			get { return 0d; }
		}

		//**************************************************************************
		// EndAngle
		// Restituisce/imposta l'angolo finale
		//**************************************************************************
		public virtual double EndAngle
		{
			set { }
			get { return 0d; }
		}

		//******************************************************************************
		// CenterPoint
		// Restituisce il centro dell'arco
		//******************************************************************************
		public virtual Point CenterPoint
		{
			set { }
			get	{ return null; }
		}

		//******************************************************************************
		// Direction
		// Direzione del lato
		//******************************************************************************
		public virtual int Direction
		{
			set { }
			get { return 0; }
		}

		//******************************************************************************
		// IsClosed
		// Restituisce:
		//           true	se il lato � chiuso
		//           false	se il lato non � chiuso
		//******************************************************************************
		public virtual bool IsClosed
		{
			get	{ return false; }
		}

		//******************************************************************************
		// IsCircle
		// Restituisce:
		//           true	se l'arco � una circonferenza completa
		//           false	se � un arco
		//******************************************************************************
		public virtual bool IsCircle
		{
			get	{ return false; }
		}

		//******************************************************************************
		// Function Surface
		// Restituisce l'area del lato (con segno)
		//******************************************************************************
		public virtual double Surface
		{
			get { return 0d; }
		}

		#endregion 

		#region Public Methods

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        //******************************************************************************
        // Equals
        // Verifica se due lati sono uguali (come valori)
        //******************************************************************************
        public abstract override bool Equals(Object s);

        //******************************************************************************
        // Equals
        // Verifica se due lati sono uguali (come valori) a meno dell'epsilon di approssimazione
        //******************************************************************************
        public abstract bool AlmostEquals(Object s);
        public abstract bool AlmostEquals(Object s, double delta);
		
		//******************************************************************************
		// GetCopy
		// Restituisce una copia del lato
		//******************************************************************************
		public abstract Side GetCopy();
		
		//******************************************************************************
		// RotoTrasl
		// Restituisce il lato rototraslato
		// Parametri:
		//			rot	: angolo di rototraslazione (in radianti)
		//			xt	: traslazione in x
		//			yt	: traslazione in y
		//******************************************************************************
		//public Side RotoTrasl(double rot, double xt, double yt)
		//{
		//    return RotoTrasl(rot, new Point(xt, yt));
		//}
		//public abstract Side RotoTrasl(double rot, Point offset);
		public abstract Side RotoTrasl(RTMatrix matrix);
		
		//**************************************************************************
		// Normalize
		// Ritorna il lato normalizzato su un punto di offset
		// Parametri:
		//			offset	: punto di normalizzazione
		//**************************************************************************
		public virtual Side Normalize(double x, double y)
		{
			return Normalize(new Point(x, y));
		}
		public abstract Side Normalize(Point offset);

		//**************************************************************************
		// Invert
		// Ritorna il lato invertito
		//**************************************************************************
		public abstract Side Invert();

        //**************************************************************************
        // Reverse
        // Inverte il lato corrente
        //**************************************************************************
        public virtual void Reverse()
        {
        }

		//**************************************************************************
		// StraightLine
		// Restituisce la retta definita dal lato
		// Parametri:
		//			a:	coefficiente variabile X
		//			b:	coefficiente variabile Y
		//			c:	coefficiente costante
		// Restituisce:
		//               true	retta calcolata
		//               false	retta non esistente
		//**************************************************************************
		public abstract bool StraightLine(out double a, out double b, out double c);

		//**************************************************************************
		// OffsetSide
		// Restituisce un nuovo lato spostato perpendicolarmente di un offset 
		// rispetto al lato
		// Parametri:
		//			offset	: offset di spostamento
		//			verso	: verso di spostamento  > 0 sinistra (rot. CCW)
		//											< 0 destra (rot. CW)
		//**************************************************************************
		public abstract Side OffsetSide(double offset, int verso);

		//******************************************************************************
		// StartPoint
		// Calcola il punto iniziale dell'arco
		// Parametri:
		//			x	: ascissa punto iniziale
		//			y	: ordinata punto iniziale
		// Ritorna:
		//			punto iniziale
		//******************************************************************************
		public abstract Point StartPoint();

		public Point StartPoint(out double x, out double y)
		{
			Point p = StartPoint();
			if (p == null)
			{
				x = y = 0d; 
				return null;
			}

			x = p.X;
			y = p.Y;
			return p;
		}

		//******************************************************************************
		// EndPoint
		// Calcola il punto finale dell'arco
		// Parametri:
		//			x	: ascissa punto finale
		//			y	: ordinata punto finale
		// Ritorna:
		//			punto finale
		//******************************************************************************
		public abstract Point EndPoint();

		public Point EndPoint(out double x, out double y)
		{
			Point p = EndPoint();
			if (p == null)
			{
				x = y = 0d; 
				return null;
			}

			x = p.X;
			y = p.Y;
			return p;
		}

		//******************************************************************************
		// CalcPoint
		// Calcola un punto sul lato
		// Parametri:
		//			tipo	: tipo angolo 0=assoluto, 1=relativo inizio, 2=relativo fine
		//			ang		: angolo
		//			x		: ascissa punto calcolato
		//			y		: ordinata punto calcolato
		// Ritorna:
		//			punto calcolato
		//			null se il punto non appartiene all'arco
		//******************************************************************************
		public Point CalcPoint(int tipo, double ang, out double x, out double y)
		{
			Point p = CalcPoint(tipo, ang);
			if (p == null)
			{
				x = y = 0d;
				return null;
			}

			x = p.X;
			y = p.Y;
			return p;
		}
		public abstract Point CalcPoint(int tipo, double ang);

		//******************************************************************************
		// CalcAngPoint
		// Calcola l'angolo di un punto sull'arco
		// Parametri:
		//			tipo	: tipo angolo 0=assoluto, 1=relativo inizio, 2=relativo fine
		//			x		: ascissa punto
		//			y		: ordinata punto
		// Ritorna:
		//           angolo calcolato in radianti
		//******************************************************************************
		public double CalcAngPoint(int tipo, Point p)
		{
			return CalcAngPoint(tipo, p.X, p.Y);
		}
		public abstract double CalcAngPoint(int tipo, double x, double y);

		//***************************************************************************/
		//                                                                          */
		// Intersect:		   calcola il punto di intersezione, se esiste, tra		*/
		//                     due lati.		                                    */
		//                                                                          */
		// Inputs:   side  = lato con cui verificare le intersezioni				*/
		//           inter = vettore dei punti di intersezione trovati				*/
		//                                                                          */
		// Outputs: -1	= i lati sono sovrapposti.									*/
		//           0	= non ci sono intersezioni.                                 */
		//           >0	= numero di intersezioni trovate	                        */
		//                                                                          */
		//****************************************************************************/
		public abstract int Intersect(Side side, out Point[] inter);

		/****************************************************************************
		 * ContainPoint
		 * Verifica se il punto specificato appartiene al lato.
		 * Parametri:
		 *			p	: punto da controllare.
		 * Ritorna:
		 *			false	il punto non appartiene
		 *			true	il punto appartiene
		 ****************************************************************************/
		public abstract bool ContainPoint(Point p);

        /****************************************************************************
         * ContainPointWithinTolerance
         * Verifica se il punto specificato appartiene al segmento, utilizzando i 
         * controlli con tolleranza estesa per le quote.
         * Parametri:
         *			p	: punto da controllare.
         * Ritorna:
         *			false	il punto non appartiene
         *			true	il punto appartiene
         ****************************************************************************/
        public abstract bool ContainPointWithinTolerance(Point p);

		///****************************************************************************/
		///*                                                                          */
		///* IsConcurrentExtremity: controlla se il punto specificato e` allo stesso  */
		///*                        tempo estremo di due entita`, specificate dai     */
		///*                        relativi punti estremi.                           */
		///*                                                                          */
		///* Inputs:   p       = puntatore al punto in analisi.                       */
		///*           e1p1    = puntatore al primo estremo della prima entita`.      */
		///*           e1p2    = puntatore al secondo estremo della prima entita`.    */
		///*           e2p1    = puntatore al primo estremo della seconda entita`.    */
		///*           e2p2    = puntatore al secondo estremo della seconda entita`.  */
		///*                                                                          */
		///*                                                                          */
		///* Outputs:  true	il punto non e` contemporaneamente estremo.               */
		///*           false	il punto e` contemporaneamente estremo.                   */
		///*                                                                          */
		///****************************************************************************/
		public static bool IsConcurrentExtremity(Point p, Point e1p1, Point e1p2, Point e2p1, Point e2p2)
		{
			return (p == e1p1 || p == e1p2) && (p == e2p1 || p == e2p2);
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se un punto appartiene al lato
		/// </summary>
		/// <param name="p">punto da verificare</param>
		/// <returns>
		///		true	appartiene
		///		false	non appartiene
		///	</returns>
		/// **************************************************************************
		public bool IsInSide(Point p)
		{
            //Point[] inters;
             
            //// Crea un segmento orizzontale fittizio, con estremo il punto
            //Segment seg = new Segment(p, new Point(p.X + 50, p.Y));

            //// Valuta le intersezioni del segmento creato con il lato
            //int n = this.Intersect(seg, out inters);
            //// Trovate delle intersezioni
            //if (n > 0)
            //{
            //    // Verifica se una delle intersezioni coincide con il punto stesso
            //    for (int i = 0; i < n; i++)
            //        if (p == inters[i])
            //            // il punto appartiene al lato
            //            return true;
            //}
            //// Lati sovrapposti
            //else if (n < 0)
            return ContainPointWithinTolerance(p);

			// il punto non appartiene al lato
			//return false;
		}

        /// **************************************************************************
        /// <summary>
        /// Estensione dell'elemento verso rispetto ad uno dei due estremi
        /// </summary>
        /// <param name="which">veritice di riferimento</param>
        /// <param name="howmuch">entita' dello spostamento, verso l'interno se negativo </param>
        /// <returns>
        ///		true	oeprazione eseguita
        ///		false	operazione non possibile
        ///	</returns>
        /// **************************************************************************
        public abstract bool Extend(EN_VERTEX which, double howmuch);

        /// **************************************************************************
        /// <summary>
        /// Estrazione di una porzione di lato, a partire dalle distanze dal primo
        /// veritice.
        /// </summary>
        /// <param name="from">distanza iniziale</param>
        /// <param name="to">distanza finale</param>
        /// <returns>
        ///		lato estratto (null se lato nullo)
        ///	</returns>
        /// **************************************************************************
        public abstract Side Extract(double from, double to);

        /// **************************************************************************
        /// <summary>
        /// Estrazione di una porzione di lato, a partire dalle distanze dal 
        /// vertice indicato.
        /// </summary>
        /// <param name="which">veritice di riferimento</param>
        /// <param name="from">distanza iniziale</param>
        /// <param name="to">distanza finale</param>
        /// <returns>
        ///		true	oeprazione eseguita
        ///		false	operazione non possibile
        ///	</returns>
        /// **************************************************************************
        public Side Extract(EN_VERTEX which, double from, double to)
        {
            if (which == EN_VERTEX.EN_VERTEX_END)
            {
                if (from > to)
                {
                    from = Length - from;
                    to = Length - to;
                }
                else
                {
                    from = Length - to;
                    to = Length - from;
                }
            }
            return Extract(from, to);
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna il punto che si trova alla distanza indicata dall'inziio
        /// del lato
        /// </summary>
        ///<param name="distance">
        ///     distanza alla quale cercare il punto
        ///</param>
        ///<returns>
        ///     punto trovato o null se la distanza e' incongruente
        ///</returns>
        /// **************************************************************************
        public abstract Point PointAtDistanceFromStart(double distance);

        /// **************************************************************************
        /// <summary>
        ///     Distanza lungo il side dal punto di start al punto <paramref name="end"/>
        /// </summary>
        /// <param name="end">
        ///     punto finale per il calcolo della lunghezza
        /// </param>
        /// <param name="distanceFromStart">
        ///     Distanza lungo il side dal punto di start al punto <paramref name="end"/>
        /// </param>
        /// <returns>
        ///     false se l'operazione non e' stata eseguita
        ///     true altrimenti
        /// </returns>
        /// **************************************************************************
        public abstract bool DistanceFromStart(Point end, out double distanceFromStart);

        /// **************************************************************************
        /// <summary>
        /// Verifica se il lato corrente contiene completamente quello indicato
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato non e' completamente contenuto
        ///     true    il lato indicato e' completamente contenuto.
        ///</returns>
        /// **************************************************************************
        public abstract bool Contains(Side sd, double maxdist);

        /// **************************************************************************
        /// <summary>
        /// Verifica se il lato indicato e' uguale a quello corrente, considerando
        /// un parametro di distanza massima
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>
        /// **************************************************************************
        public abstract bool IsEqual(Side sd, double maxdist);

        /// **************************************************************************
        /// <summary>
        /// Se i due lati sono omogenei e connessi, ritorna il lato somma dei due.
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>
        /// **************************************************************************
        public virtual Side Connect(Side sd, double maxdist)
        {
            if (Contains(sd, MathUtil.QUOTE_EPSILON))
                return this;

            if (sd.Contains(this, MathUtil.QUOTE_EPSILON))
                return sd;

            return null;
        }

        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public virtual bool ReadDerivedFileXml(XmlNode n)
        {
            return true;
        }

		//**************************************************************************
		// ReadFileXml
		// Legge il lato dal file XML
		// Parametri:
		//			n	: nodo XML contenete il lato
		// Ritorna:
		//			true	lettura eseguita con successo
		//			false	errore nella lettura
		//**************************************************************************
		public abstract bool ReadFileXml(XmlNode n);

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public virtual void WriteDerivedFileXml(XmlTextWriter w)
        {
        }

		//**************************************************************************
		// WriteFileXml
		// Scrive il lato su file XML
		// Parametri:
		//			w: oggetto per scrivere il file XML
		//**************************************************************************
		public abstract void WriteFileXml(XmlTextWriter w);

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public virtual void WriteDerivedFileXml(XmlNode baseNode)
        {
        }

        ///**************************************************************************
		/// <summary>
		/// Scrive il lato sul nodo XML
		/// </summary>
		/// <param name="baseNode">nodo XML dove scrivere il lato</param>
		///**************************************************************************
		public abstract void WriteFileXml(XmlNode baseNode);

        /// **************************************************************************
        /// <summary>
        ///     connessione del lato corrente con il successivo in base allla distanza
        ///     indicata.
        /// </summary>
        ///<param name="nextsd">
        ///     lato successivo
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     eventuale lato da aggiungere per completare la connessione
        ///</returns>
        /// **************************************************************************
        public abstract Side ConnectSides(Side nextsd, double maxdist);

        /// **************************************************************************
        /// <summary>
        ///     verficia se i due lati sono omogenei, cioe' sono paralleli.
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>        
        /// **************************************************************************
        public abstract bool Homogeneous(Side next, double maxdist);

        /// **************************************************************************
        /// <summary>
        ///     Determina il punto dell'entita' corrente che si trova alla minima 
        ///     distanza rispetto il punto indicato
        /// </summary>
        /// <param name="ref_point">
        ///     punto rispetto il quale determinare la distanza
        /// </param>
        /// <param name="mindist_point">
        ///     punto a minima distanza indentificato nella entita'
        /// </param>
        /// <returns>
        ///     minima distanza calcolata
        /// </returns>
        /// **************************************************************************
        public abstract double MinDistanceToPoint(Point ref_point, out Point mindist_point);

        /// **************************************************************************
        /// <summary>
        ///     Determina le due parti risultati dall'interruzione dell'entita' corrente
        ///     sul punto indicato
        /// </summary>
        /// <param name="break_point">
        ///     punto sul quale eseguire l'interruzione
        /// </param>
        /// <param name="sd1">
        ///     prima parte dell'elemento
        /// </param>
        /// <param name="sd2">
        ///     seconda parte dell'elmento
        /// </param>
        /// <returns>
        ///     false se l'operazione non e' stata eseguita
        ///     true altrimenti
        /// </returns>
        /// **************************************************************************
        public abstract bool BreakAtPoint(Point break_point, out Side sd1, out Side sd2);

        /// ********************************************************************
        /// <summary>
        /// Trasforma il side
        /// </summary>
        /// <returns>transform != null</returns>
        /// ********************************************************************
        public abstract bool Transform(Breton.MathUtils.RTMatrix transform);

        /// ********************************************************************
        /// <summary>
        /// Tangente sul primo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sul primo punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        public abstract Breton.MathUtils.Vector Tangent();//direzione 1 -> 2

        /// ********************************************************************
        /// <summary>
        /// Tangente sull'ultimo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sull'ultimo punto punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        public abstract Breton.MathUtils.Vector TangentAtEnd();//direzione 1 -> 2

        /// ********************************************************************
        /// <summary>
        /// Tangente su coordinata relativa 0 ;&lt t ;&lt 1
        /// </summary>
        /// <returns>tangente su punto definito da 0 ;&lt t ;&lt 1</returns>
        /// ********************************************************************
        public abstract Breton.MathUtils.Vector TangentAt(double t); //direzione 1 -> 2

        /// ********************************************************************
        /// <summary>
        /// Tangente
        /// </summary>
        /// <param name="x">x dove valutare la tangente</param>
        /// <param name="y">y dove valutare la tangente</param>
        /// <returns>tangente</returns>
        /// ********************************************************************
        public abstract Vector TangentAtPoint(double x, double y); //direzione 1 -> 2

        /// <summary>
        /// Arrotonda i punti del lato ad un determinato numero di decimali
        /// </summary>
        /// <param name="decimals">Numero di decimali da mantenere</param>
        public virtual void RoundPointsDecimals(int decimals)
        {
            P1.X = Math.Round(P1.X, decimals, MidpointRounding.AwayFromZero);
            P1.Y = Math.Round(P1.Y, decimals, MidpointRounding.AwayFromZero);

            P2.X = Math.Round(P2.X, decimals, MidpointRounding.AwayFromZero);
            P2.Y = Math.Round(P2.Y, decimals, MidpointRounding.AwayFromZero);
        }

        public static double Distance(Side sideReference, Side sideToCheck, bool minValue = true, bool checkOnlySideToCheck = false)
        {
            if (sideReference is Segment && sideToCheck is Segment)
                return Segment.DistanceFromSegment((Segment)sideReference, (Segment)sideToCheck, minValue, checkOnlySideToCheck);
            else
            {
                Breton.Polygons.Point pminCheck;
                Breton.Polygons.Point pmaxCheck;
                Breton.Polygons.Point pminRef;
                Breton.Polygons.Point pmaxRef;

                return double.MaxValue;
            }
        }

        public static bool ExtendToInclude(Side sideToExtend, Side sideToBeIncluded)
        {
            if (sideToExtend is Segment && sideToBeIncluded is Segment)
            {
                bool changed = false;

                var segmentToExtend = sideToExtend as Segment;
                var segmentToBeIncluded = sideToBeIncluded as Segment;

                changed |= Segment.ExtendToInclude(segmentToExtend, segmentToBeIncluded.P1);
                changed |= Segment.ExtendToInclude(segmentToExtend, segmentToBeIncluded.P2);

                return changed;
            }
            else
            {
                Breton.Polygons.Point pminCheck;
                Breton.Polygons.Point pmaxCheck;
                Breton.Polygons.Point pminRef;
                Breton.Polygons.Point pmaxRef;

                return false;
            }
        }
		#endregion
    }
}
