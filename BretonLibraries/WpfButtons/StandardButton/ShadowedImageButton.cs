﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfButtons.StandardButton
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:WpfCustomControlLibrary1"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:WpfCustomControlLibrary1;assembly=WpfCustomControlLibrary1"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:ShadowedImageButton/>
    ///
    /// </summary>
    public class ShadowedImageButton : Button
    {
        public static readonly DependencyProperty ShadowDepthProperty =
            DependencyProperty.Register("ShadowDepth", typeof(double), typeof(ShadowedImageButton));

        public static readonly DependencyProperty ShadowDirectionProperty =
            DependencyProperty.Register("ShadowDirection", typeof(double), typeof(ShadowedImageButton));

        public static readonly DependencyProperty ShadowBlurRadiusProperty =
            DependencyProperty.Register("ShadowBlurRadius", typeof(double), typeof(ShadowedImageButton));

        public static readonly DependencyProperty ShadowOpacityProperty =
            DependencyProperty.Register("ShadowOpacity", typeof(double), typeof(ShadowedImageButton));

        public static readonly DependencyProperty ShadowColorProperty =
            DependencyProperty.Register("ShadowColor", typeof(Color), typeof(ShadowedImageButton));

        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.Register("Image", typeof(ImageSource), typeof(ShadowedImageButton));



        public double ShadowDepth
        {
            get { return (double)this.GetValue(ShadowDepthProperty); }
            set { this.SetValue(ShadowDepthProperty, value); }
        }

        public double ShadowDirection
        {
            get { return (double)this.GetValue(ShadowDirectionProperty); }
            set { this.SetValue(ShadowDirectionProperty, value); }
        }

        public double ShadowBlurRadius
        {
            get { return (double)this.GetValue(ShadowBlurRadiusProperty); }
            set { this.SetValue(ShadowBlurRadiusProperty, value); }
        }

        public double ShadowOpacity
        {
            get { return (double)this.GetValue(ShadowOpacityProperty); }
            set { this.SetValue(ShadowOpacityProperty, value); }
        }

        public Color ShadowColor
        {
            get { return (Color)this.GetValue(ShadowColorProperty); }
            set { this.SetValue(ShadowColorProperty, value); }
        }

        public ImageSource Image
        {
            get { return (ImageSource)this.GetValue(ImageProperty); }
            set { this.SetValue(ImageProperty, value); }
        }




        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        static ShadowedImageButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ShadowedImageButton), new FrameworkPropertyMetadata(typeof(ShadowedImageButton)));
        }


    }
}
