﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WpfButtons.StandardButton
{
    [DefaultEvent("ButtonClick")]
    public partial class WpfShadowedImageButton : UserControl
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration

        public event EventHandler<EventArgs> ButtonClick;

        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private double _imageShadowDepth = 6;
        private double _imageShadowDirection = 320;
        private double _imageShadowBlurRadius = 5;
        private double _imageShadowOpacity = 0.5;
        private Color _imageShadowColor;

        private Bitmap _image = Properties.Resources.Cancel;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public WpfShadowedImageButton()
        {
            InitializeComponent();
            ImageShadowColor = Color.DarkGray;
            wpfButton.Click += new EventHandler<EventArgs>(wpfButton_Click);
        }

        void wpfButton_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(this, new EventArgs());
            }
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            wpfButton.IsEnabled = Enabled;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        [Browsable(true)]
        [Description("Profondità in pixel dell'ombreggiatura dell'immagine")]
        public double ImageShadowDepth
        {
            get { return _imageShadowDepth; }
            set
            {
                _imageShadowDepth = value;
                wpfButton.ShadowDepth = _imageShadowDepth;
            }
        }

        [Browsable(true)]
        public double ImageShadowDirection
        {
            get { return _imageShadowDirection; }
            set
            {
                _imageShadowDirection = value;
                wpfButton.ShadowDirection = _imageShadowDirection;
            }
        }

        [Browsable(true)]
        public double ImageShadowBlurRadius
        {
            get { return _imageShadowBlurRadius; }
            set
            {
                _imageShadowBlurRadius = value;
                wpfButton.ShadowBlurRadius = _imageShadowBlurRadius; 
                
            }
        }

        public double ImageShadowOpacity
        {
            get { return _imageShadowOpacity; }
            set
            {
                _imageShadowOpacity = value;
                wpfButton.ShadowOpacity = _imageShadowOpacity;
            }
        }

        public Color ImageShadowColor
        {
            get
            {
                return _imageShadowColor;
            }
            set
            {
                _imageShadowColor = value;
                wpfButton.ShadowColor = System.Windows.Media.Color.FromArgb(_imageShadowColor.A, _imageShadowColor.R,
                    _imageShadowColor.G, _imageShadowColor.B);
            }
        }

        public Bitmap Image
        {
            get
            {
                return _image;
            }
            set
            {
                if (value == null)
                {
                    value = Properties.Resources.Cancel;
                }
                _image = value;
                wpfButton.Image = value.ToWpfBitmap();
            }
        }
    }
}
