﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfButtons.StandardButton
{
    /// <summary>
    /// Interaction logic for Button.xaml
    /// </summary>
    public partial class StandardButton : UserControl, INotifyPropertyChanged
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration

        public event EventHandler<EventArgs> Click; 

        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private BitmapSource _image = null; 

        private double _shadowDepth = 6;
        private double _shadowDirection = 320;
        private double _shadowBlurRadius = 5;
        private double _shadowOpacity = 0.5;
        private Color _shadowColor;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public StandardButton()
        {
            InitializeComponent();
            Image = new BitmapImage(new Uri("../Resources/Cancel.png", UriKind.RelativeOrAbsolute));

            ShadowColor = Colors.DarkGray;
            DataContext = this;

            ThisButton.Click += new RoutedEventHandler(ThisButton_Click);

        }

        void ThisButton_Click(object sender, RoutedEventArgs e)
        {
            if (Click != null)
            {
                Click(this, new EventArgs());
            }
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        public BitmapSource Image
        {
            get { return _image; }
            set
            {
                _image = value;
                ThisButton.Image = (BitmapImage)value;
                OnPropertyChanged("Image");
                OnPropertyChanged("HoverImage");
                OnPropertyChanged("PressedImage");
            }
        }



        public double ShadowDepth
        {
            get { return _shadowDepth; }
            set
            {
                _shadowDepth = value;
                OnPropertyChanged("ShadowDepth");
            }
        }

        public double ShadowDirection
        {
            get { return _shadowDirection; }
            set
            {
                _shadowDirection = value;
                OnPropertyChanged("ShadowDirection");
                
            }
        }

        public double ShadowBlurRadius
        {
            get { return _shadowBlurRadius; }
            set
            {
                _shadowBlurRadius = value;
                OnPropertyChanged("ShadowBlurRadius");
                
            }
        }

        public double ShadowOpacity
        {
            get { return _shadowOpacity; }
            set
            {
                _shadowOpacity = value;
                OnPropertyChanged("ShadowOpacity");
                
            }
        }

        public Color ShadowColor
        {
            get { return _shadowColor; }
            set
            {
                _shadowColor = value;
                OnPropertyChanged("ShadowColor");
                
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
