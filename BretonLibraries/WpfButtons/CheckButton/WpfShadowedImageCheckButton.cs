﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WpfButtons.CheckButton
{
    [DefaultEvent("ButtonCheckedChanged")]
    public partial class WpfShadowedImageCheckButton : UserControl
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration

        public event EventHandler<EventArgs> ButtonCheckedChanged;

        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private double _imageShadowDepth = 6;
        private double _imageShadowDirection = 320;
        private double _imageShadowBlurRadius = 5;
        private double _imageShadowOpacity = 0.5;
        private Color _imageShadowColor;

        private Bitmap _image = Properties.Resources.Cancel;

        private bool _checked = false;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public WpfShadowedImageCheckButton()
        {
            InitializeComponent();
            ImageShadowColor = Color.DarkGray;

            checkButton.CheckedChanged += new EventHandler<EventArgs>(checkButton_CheckedChanged);
        }

        void checkButton_CheckedChanged(object sender, EventArgs e)
        {
            if (ButtonCheckedChanged != null)
            {
                ButtonCheckedChanged(this, new EventArgs());
            }
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            checkButton.IsEnabled = Enabled;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        [Browsable(true)]
        [Description("Selezione Checkbox")]
        public bool Checked
        {
            get { return checkButton.Checked; }
            set
            {
                checkButton.Checked = value;
            }
        }

        
        [Browsable(true)]
        [Description("Profondità in pixel dell'ombreggiatura dell'immagine")]
        public double ImageShadowDepth
        {
            get { return _imageShadowDepth; }
            set
            {
                _imageShadowDepth = value;
                checkButton.ShadowDepth = _imageShadowDepth;
            }
        }

        [Browsable(true)]
        public double ImageShadowDirection
        {
            get { return _imageShadowDirection; }
            set
            {
                _imageShadowDirection = value;
                checkButton.ShadowDirection = _imageShadowDirection;
            }
        }

        [Browsable(true)]
        public double ImageShadowBlurRadius
        {
            get { return _imageShadowBlurRadius; }
            set
            {
                _imageShadowBlurRadius = value;
                checkButton.ShadowBlurRadius = _imageShadowBlurRadius; 
                
            }
        }

        public double ImageShadowOpacity
        {
            get { return _imageShadowOpacity; }
            set
            {
                _imageShadowOpacity = value;
                checkButton.ShadowOpacity = _imageShadowOpacity;
            }
        }

        public Color ImageShadowColor
        {
            get
            {
                return _imageShadowColor;
            }
            set
            {
                _imageShadowColor = value;
                checkButton.ShadowColor = System.Windows.Media.Color.FromArgb(_imageShadowColor.A, _imageShadowColor.R,
                    _imageShadowColor.G, _imageShadowColor.B);
            }
        }

        public Bitmap Image
        {
            get
            {
                return _image;
            }
            set
            {
                if (value == null)
                {
                    value = Properties.Resources.Cancel;
                }
                _image = value;
                checkButton.Image = value.ToWpfBitmap();
            }
        }
    }
}
