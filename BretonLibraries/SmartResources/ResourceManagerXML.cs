using System;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Threading;
using System.Collections;
using C1.Win.C1TrueDBGrid;
using C1.Win.C1List;
using C1.Win.C1Command;
using System.Xml;
using System.IO;

namespace Breton.SmartResources
{
	/// <summary>
	/// Load the localized resources for the project.
	/// </summary>
	public class ResourceManagerXML : ResourceManager
	{
		#region Variables
		private XmlDocument mResMgrXML;
		private string mResourceFile;
		#endregion

		#region Constructors

		/// <summary>
		/// Constructors
		/// </summary>
		/// <param name="resourcePathXML">File xml that contains strings</param>
		public ResourceManagerXML(string resourceFileXML)
		{
			mResourceFile = resourceFileXML;
		}

		#endregion

		/// <summary>
		/// Gets the value of the System.String resource localized for the specified culture.
		/// </summary>
		/// <param name="name">The name of the resource to get</param>
		/// <param name="culture">The System.Globalization.CultureInfo object that represents the culture for which the resource is localized</param>
		/// <returns></returns>
		public override string GetString(string name, CultureInfo culture)
		{
			// Verifica se il gestore delle lingue � inizializzato
			if (mResMgrXML == null)
			{
				string resFile;

				mResMgrXML = new XmlDocument();

				// Verifica la lingua e l'esistenza del file appropriato
				if (culture.Name != "it-IT" && File.Exists(culture.Name + "\\" + mResourceFile))
					resFile = culture.Name + "\\" + mResourceFile;
				else
					resFile = mResourceFile;


				mResMgrXML.Load(resFile);
			}

			XmlNode nd = mResMgrXML.SelectSingleNode("/root/data[@name='" + name + "']");

			if (nd != null)
			{
				return nd.FirstChild.InnerText.ToString();
			}

			return null;
		}

		/// <summary>
		/// Gets the value of the System.Object resource localized for the specified culture.
		/// </summary>
		/// <param name="name">The name of the resource to get</param>
		/// <param name="culture">The System.Globalization.CultureInfo object that represents the culture for which the resource is localized</param>
		/// <returns></returns>
		public override object GetObject(string name, CultureInfo culture)
		{
			return null;
		}
	}
}