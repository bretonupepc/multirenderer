using System;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Resources;
using System.Globalization;
using System.Threading;
using System.Collections;
using C1.Win.C1TrueDBGrid;
using C1.Win.C1List;
using C1.Win.C1Command;

namespace Breton.SmartResources
{
	/// <summary>
	/// Load the localized resources for the project.
	/// </summary>
	public class SmartResource
	{
		#region Variables

		private ResourceManager mResMgr;
		private CultureInfo mActualCulture;
		private string mResourcePath;
		private Assembly mAssembly;

		#endregion

		#region Constructors

		/// <summary>
		/// Constructors
		/// </summary>
		/// <param name="resourcePath">Resource path (ex. Resources.ResourceName)</param>
		public SmartResource(string resourcePath)
		{			
			mResourcePath =  resourcePath;
			mAssembly = Assembly.GetCallingAssembly();
			CreateResourceManager();
			mActualCulture = new CultureInfo("");
		}

		#endregion

		#region Public Methods
		/// <summary>
		/// Load localized strings into form controls
		/// </summary>
		/// <param name="form">Load strings into these form controls</param>
		public void LoadStringForm(Form form)
		{
			string frmName = form.Name + "_";

			string s = mResMgr.GetString(frmName + "Text", mActualCulture);
			if (s != null)
				form.Text = s;

			LoadFormString(form, frmName);
		}

        /// <summary>
        /// Load localized strings into form controls
        /// </summary>
        /// <param name="form">Load strings into these form controls</param>
        public void LoadStringControl(Control ctr)
        {
            string ctrName = ctr.Name + "_";

            LoadFormString(ctr, ctrName);
        }

		/// <summary>
		/// Load localized strings into C1 Menu
		/// </summary>
		/// <param name="menu">Load strings into this menu</param>
		public void LoadStringC1Menu(string frmName, C1CommandHolder menu)
		{
			LoadFormString(menu.Commands, frmName);
		}

		/// <summary>
		/// Load localized strings into C1 Menu
		/// </summary>
		/// <param name="form">Form container</param>
		/// <param name="menu">Load strings into this menu</param>
		public void LoadStringMenu(Form form, ToolStrip menu)
		{
			string frmName = form.Name + "_";
			LoadMenuString(menu.Items, frmName);
		}

		/// <summary>
		/// Load a form fix string
		/// </summary>
		/// <param name="form">Form to load</param>
		/// <param name="inde">String index</param>
		/// <returns>Loaded string</returns>
		public string LoadFixString(Form form, int index)
		{
			return LoadString(form.Name + "_FixString" + index.ToString());
		}

		/// <summary>
		/// Load all form fix string 
		/// </summary>
		/// <param name="form">Form to load</param>
		/// <returns>Strings array</returns>
		public string[] LoadFixStrings(Form form)
		{
			int i = 0;
			ArrayList stringList = new ArrayList();

			string s = mResMgr.GetString(form.Name + "_FixString" + (i++).ToString(), mActualCulture);
			while (s != null)
			{
				stringList.Add(s);
				s = mResMgr.GetString(form.Name + "_FixString" + (i++).ToString(), mActualCulture);
			}

			if (stringList.Count == 0)
				return null;

			return (string[]) stringList.ToArray();
		}

		/// <summary>
		/// Load named fix string
		/// </summary>
		/// <param name="name">String name to load</param>
		/// <param name="index">String index</param>
		/// <returns>Loaded string</returns>
		public string LoadFixString(string name, int index)
		{
			return LoadString(name + "_FixString" + index.ToString());
		}

		/// <summary>
		/// Load localized strings for form messagebox
		/// </summary>
		/// <param name="form">Form to load</param>
		/// <param name="index">Message index</param>
		/// <param name="caption">Localize Caption returned</param>
		/// <param name="message">Localized Message returned</param>
		/// <returns>
		///				true	if localized messagebox exists
		///				false	otherwise
		///	</returns>
		public bool LoadMessageBox(Form form, int index, out string caption, out string message)
		{
			string id = (form == null ? "" : form.Name + "_") + "MessageBox" + index.ToString();

			caption = mResMgr.GetString(id + "_Caption", mActualCulture);
			message = mResMgr.GetString(id + "_Text", mActualCulture);

			return caption != null && message != null;
		}

		/// <summary>
		/// Load localized strings for class messagebox
		/// </summary>
		/// <param name="name">class name to load</param>
		/// <param name="index">Message index</param>
		/// <param name="caption">Localize Caption returned</param>
		/// <param name="message">Localized Message returned</param>
		/// <returns>
		///				true	if localized messagebox exists
		///				false	otherwise
		///	</returns>
		public bool LoadMessageBox(string name, int index, out string caption, out string message)
		{
			string id = name + "_" + "MessageBox" + index.ToString();

			caption = mResMgr.GetString(id + "_Caption", mActualCulture);
			message = mResMgr.GetString(id + "_Text", mActualCulture);

			return caption != null && message != null;
		}

		/// <summary>
		/// Load localized strings for messagebox
		/// </summary>
		/// <param name="index">Message index</param>
		/// <param name="caption">Localize Caption returned</param>
		/// <param name="message">Localized Message returned</param>
		/// <returns>
		///				true	if localized messagebox exists
		///				false	otherwise
		///	</returns>
		public bool LoadMessageBox(int index, out string caption, out string message)
		{
			return LoadMessageBox((Form) null, index, out caption, out message);
		}

		/// <summary>
		/// Load a localized generic string from resource
		/// </summary>
		/// <param name="id">string id</param>
		/// <returns>String loaded</returns>
		public string LoadString(string id)
		{
			string ret = mResMgr.GetString(id, mActualCulture);

			if (ret == null)
				return "";

			return ret;
		}

		/// <summary>
		/// Load a localized image from resource
		/// </summary>
		/// <param name="id">Image id</param>
		/// <returns>Image loaded</returns>
		public Image LoadImage(string id)
		{
			return (Image) mResMgr.GetObject(id, mActualCulture);
		}

		/// <summary>
		/// Change the active language
		/// </summary>
		/// <param name="culture">Culture id (ex. "it-IT")</param>
		public void ChangeLanguage(string culture)
		{
            try
            {
                mActualCulture = new CultureInfo(culture);
                Thread.CurrentThread.CurrentCulture = new CultureInfo("");	// regional settings
            }
            catch (Exception ex)
            {
                mActualCulture = new CultureInfo("");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("");	// regional settings
            }
		}

		/// <summary>
		/// Change the thread active language
		/// </summary>
		/// <param name="culture">Culture id (ex. "it-IT")</param>
		public void ChangeThreadLanguage(string culture)
		{
			ChangeLanguage(culture);

			Thread.CurrentThread.CurrentUICulture = mActualCulture;	// user interface
			Thread.CurrentThread.CurrentCulture = mActualCulture;	// regional settings
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Create the resourcemanager for the caller assembly
		/// </summary>
		private void CreateResourceManager()
		{
			mResMgr = null;

			if (mResourcePath.EndsWith(".xml", StringComparison.InvariantCultureIgnoreCase))
				mResMgr = new ResourceManagerXML(mResourcePath);
			else
				mResMgr = new ResourceManager(mAssembly.GetName().Name + "." + mResourcePath, mAssembly);
		}

		///***************************************************************************
		/// <summary>
		/// Loads strings for controls contained in a control
		/// </summary>
		/// <param name="control">Container control</param>
		/// <param name="frmName">Form name</param>
		///***************************************************************************
		private void LoadFormString(System.Windows.Forms.Control control, string frmName)
		{
			foreach (Control ctrl in control.Controls)
			{
				string s = mResMgr.GetString(frmName + ctrl.Name + "_Text", mActualCulture);
				if (s != null)
					ctrl.Text = s;

				if (ctrl is DataGrid)
				{
					DataGrid dg = (DataGrid) ctrl;

					// Scroll grid tablestyles 
					for (int i = 0; i < dg.TableStyles.Count; i++)
					{
						DataGridTableStyle ts = dg.TableStyles[i];
						for (int j = 0; j < ts.GridColumnStyles.Count; j++)
						{
							string colName = dg.Name + "_columns(" + i.ToString() + "_" + j.ToString() + ")";
							// Read column header
							s = mResMgr.GetString(frmName + colName + "_HeaderText", mActualCulture);
							if (s != null)
								ts.GridColumnStyles[j].HeaderText = s;
						}
					}
				}
				else if (ctrl is C1TrueDBGrid)
				{
					C1TrueDBGrid tDbg = (C1TrueDBGrid) ctrl;

					for (int i = 0; i < tDbg.Columns.Count; i++)
					{
						string colName = tDbg.Name + "_columns(0_" + i.ToString() + ")";
						// Read column header
						s = mResMgr.GetString(frmName + colName + "_HeaderText", mActualCulture);
						if (s != null)
							tDbg.Columns[i].Caption = s;
					}

                    // Legge l'intestazione della griglia
                    s = mResMgr.GetString(frmName + tDbg.Name + "_Caption", mActualCulture);
                    if (s != null)
                        tDbg.Caption = s;
				}
				else if (ctrl is C1Combo)
				{
					C1Combo cmbo = (C1Combo) ctrl;

					for (int i = 0; i < cmbo.Columns.Count; i++)
					{
						string colName = cmbo.Name + "_columns(0_" + i.ToString() + ")";
						// Read column header
						s = mResMgr.GetString(frmName + colName + "_HeaderText", mActualCulture);
						if (s != null)
							cmbo.Columns[i].Caption = s;
					}
				}
				else if (ctrl is ToolBar)
				{
					ToolBar tb = (ToolBar) ctrl;
					
					for (int i = 0; i < tb.Buttons.Count; i++)
					{
						string btnName = tb.Name + "_Button" + i.ToString();
						s = mResMgr.GetString(frmName + btnName + "_ToolTipText", mActualCulture);
						if (s != null)
							tb.Buttons[i].ToolTipText = s;
					}
					
				}
                else if (ctrl is ToolStrip && !(ctrl is MenuStrip))
                {
                    ToolStrip ts = (ToolStrip)ctrl;

                    for (int i = 0; i < ts.Items.Count; i++)
                    {
						ToolStripItem tsbtn = null;
  
						if (ts.Items[i] is ToolStripItem)
						{
							tsbtn = (ToolStripItem)ts.Items[i];
							
							s = mResMgr.GetString(frmName + tsbtn.Name + "_ToolTipText", mActualCulture);
							if (s != null)
								tsbtn.ToolTipText = s;

							s = mResMgr.GetString(frmName + tsbtn.Name + "_Text", mActualCulture);
							if (s != null)
								tsbtn.Text = s;

							if (tsbtn is ToolStripDropDownButton)
							{
								if (((ToolStripDropDownButton)tsbtn).DropDownItems.Count > 0)
									LoadMenuString(((ToolStripDropDownButton)tsbtn).DropDownItems, frmName);
							}
							else if (tsbtn is ToolStripSplitButton)
							{
								if (((ToolStripSplitButton)tsbtn).DropDownItems.Count > 0)
									LoadMenuString(((ToolStripSplitButton)tsbtn).DropDownItems, frmName);
							}
						}
                    }

                }
                else if (ctrl is MenuStrip)
                {
                    MenuStrip m = (MenuStrip)ctrl;

                    LoadMenuString(m.Items, frmName);
                }
				else if (ctrl is ListView)
				{
					ListView lv = (ListView)ctrl;

					for (int i = 0; i < lv.Columns.Count; i++)
					{
						ColumnHeader colH = lv.Columns[i];

						s = mResMgr.GetString(frmName + lv.Name + "_Column" + i + "_Text", mActualCulture);
						if (s != null)
							colH.Text = s; 
					}
				}

				if (ctrl.Controls.Count > 0)
					LoadFormString(ctrl, frmName);
			}
		}

        /// <summary>
        /// Load strings for Menu
        /// </summary>
        /// <param name="items">Item collection</param>
        /// <param name="frmName">Form name</param>
        private void LoadMenuString(ToolStripItemCollection items, string frmName)
        {
            string s;

            foreach (ToolStripItem item in items)
            {
                ToolStripMenuItem mi;

                if (item is ToolStripMenuItem)
                {
                    mi = (ToolStripMenuItem)item;

                    s = mResMgr.GetString(frmName + item.Name + "_Text", mActualCulture);
                    if (s != null)
                        item.Text = s;
                    if (mi.DropDownItems.Count > 0)
                        LoadMenuString(mi.DropDownItems, frmName);
                }
            }
        }

		///***************************************************************************
		/// <summary>
		/// Loads strings for commands of C1CommandMenu
		/// </summary>
		/// <param name="commands">Command collection</param>
		/// <param name="frmName">Form name</param>
		///***************************************************************************
		private void LoadFormString(C1Commands commands, string frmName)
		{
			foreach (C1Command cmd in commands)
			{
				string s = mResMgr.GetString(frmName + "_" + cmd.Name + "_Text", mActualCulture);
				if (s != null)
					cmd.Text = s;
			}
		}

		#endregion
	}
}
