using System;

namespace Breton.SmartResources
{
	/// <summary>
	/// Classe per la descrizione dei valori nelle combobox
	/// </summary>
	public class ComboDescription
	{
		private object mValue;
		private string mValueDescription;
    
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="val">value</param>
		/// <param name="description">value description</param>
		public ComboDescription(int val, string description): this((object) val, description)
		{}
		public ComboDescription(object val, string description)
		{
			mValue = val;
			mValueDescription = description;
		}

		public virtual object Value
		{
			get	{ return mValue; }
		}

		public virtual string ValueDescription
		{        
			get	{ return mValueDescription;	}
		}

		public override string ToString()
		{
			return ValueDescription;
		}
	}
}
