using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Breton.MathUtils;
using TraceLoggers;


namespace Breton.Polygons
{
	public class PathCollection
	{
		/// <summary>
		/// PathCollection � un insieme di percorsi senza regole particolari
		/// </summary>
		#region Variables

		protected ArrayList mPaths;					// array dei percorsi
		protected RTMatrix mMatrixRT;				// matrice di rototraslazione

		private SortedList<string, string> mAttributes;	// Lista degli attributi del path

		protected string szOriginalType;

		protected string szName;
		#endregion

		#region Constructors

		//**************************************************************************
		// Costruisce l'insieme vuoto
		//**************************************************************************
		public PathCollection()
		{
			mPaths = new ArrayList();
			mMatrixRT = new RTMatrix();
			mAttributes = new SortedList<string, string>();
			szOriginalType = "PathCollection";
			szName = "";
		}

		//**************************************************************************
		// Costruisce l'insieme di percorsi come copia di un altro insieme
		// Parametri:
		//			p	: percorso da copiare
		//**************************************************************************
		public PathCollection(PathCollection pc): this()
		{
			if (pc.Count == 0)
				return;
			
			// Aggiunge tutti i lati del percorso
			for (int i = 0; i < pc.Count; i++)
				AddPath((Path)pc.mPaths[i]);

			mMatrixRT = new RTMatrix(pc.mMatrixRT);

			// Aggiunge tutti gli attributi
			foreach (KeyValuePair<string, string> a in pc.mAttributes)
				mAttributes.Add(a.Key, a.Value);

			szOriginalType = pc.szOriginalType;

			szName = pc.szName;
		}

		#endregion

		#region Properties

		//**************************************************************************
		// Count
		// Ritorna il numero di percorsi
		//**************************************************************************
		public int Count
		{
			get { return mPaths.Count; }
		}
		
		//**************************************************************************
		// MatrixRT
		// Ritorna la matrice di rototraslazione
		//**************************************************************************
		public RTMatrix MatrixRT
		{
			set { mMatrixRT = new RTMatrix(value); }
			get { return new RTMatrix(mMatrixRT); }
		}

		//**************************************************************************
		// RotAngle
		// Ritorna l'angolo di rotaazione del percorso
		//**************************************************************************
		public double RotAngle
		{
			get
			{
				double offsetX = 0d, offsetY = 0d, angle = 0d;

				mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);
				
				return angle;
			}
		}

		//**************************************************************************
		// TranslPoint
		// Ritorna il punto di traslazione del percorso
		//**************************************************************************
		public Point TranslPoint
		{
			get
			{
				double offsetX = 0d, offsetY = 0d, angle = 0d;

				mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);
				
				return new Point(offsetX, offsetY);
			}
		}

		//**************************************************************************
		// Surface
		// Restituisce l'area dell'insieme di percorsi (senza segno)
		//**************************************************************************
		public double Surface
		{
			get	{ return Math.Abs(SurfaceSign); }
		}
		//**************************************************************************
		// SurfaceSign
		// Restituisce l'area dell'insieme di percorsi (con segno)
		//**************************************************************************
		public double SurfaceSign
		{
			get
			{
				double area = 0d;

				// Somma le aree di tutti i percorsi
				foreach (Path p in mPaths)
					area += p.SurfaceSign;

				return area;
			}
		}

		///**************************************************************************
		/// <summary>
		/// Restituisce la lista degli attributi del path
		/// </summary>
		///**************************************************************************
		public SortedList<string, string> Attributes
		{
			get { return mAttributes; }
		}

		///**************************************************************************
		/// <summary>
		/// Restituisce il tipo originale dell'oggetto
		/// </summary>
		///**************************************************************************
		public string OriginalType
		{
			get { return szOriginalType; }
		}

		///**************************************************************************
		/// <summary>
		/// Restituisce il nome dell'oggetto.
		/// </summary>
		///**************************************************************************
		public string Name
		{
			get { return szName; }
			set { szName = value; }
		}

		///**************************************************************************
		/// <summary>
		/// Indicizzazione della lista interna
		/// </summary>
		///**************************************************************************
		public Path this [int i]
		{
			get { return (Path)mPaths[i]; 
			}
		}

		#endregion

		#region Public Methods
		///**************************************************************************
		/// <summary>
		///     Inserisce o modifica il valore di un attributo
		/// </summary>
		/// <param name="key">
		///     chiave dell'attributo
		/// </param>
		/// <param name="value">
		///     valore dell'attributo
		/// </param>
		///**************************************************************************
		public void AttributeAddOrUpdate( string key, string value )
		{
			if (Attributes.ContainsKey( key ))
				Attributes[key] = value;
			else
				Attributes.Add( key, value );
		}

		//**************************************************************************
		// Clear
		// Clear di tutti i percorsi
		//**************************************************************************
		public bool Clear()
		{
			bool changed = Count > 0;
			while(Count > 0)
				this.RemovePath(Count-1);
			return changed;
		}

		//**************************************************************************
		// AddPath
		// Aggiunge un percorso
		// Parametri:
		//			p	: percorso da aggiungere
		// Restituisce:
		//			true	percorso inserito
		//			false	non � possibile inserire il percorso
		//**************************************************************************
		public virtual bool AddPath(Path p)
		{
			mPaths.Add(CreatePath(p));
			return true;
		}

        public virtual bool AddPathWithoutClone(Path p)
		{
			mPaths.Add(p);
			return true;
		}

		//**************************************************************************
		// InsertPath
		// Inserisce un percorso in una posizione
		// Parametri:
		//			index	: indice posizione
		//			p		: percorso da aggiungere
		// Restituisce:
		//			true	percorso inserito
		//			false	non � possibile inserire il percorso
		//**************************************************************************
		public virtual bool InsertPath(int index, Path p)
		{
			mPaths.Insert(index, CreatePath(p));
			return true;
		}

		//**************************************************************************
		// RemovePath
		// Rimuove un percorso
		// Parametri:
		//			p	: percorso da rimuovere
		// Restituisce:
		//			true	percorso eliminato
		//			false	non � possibile eliminare il percorso
		//**************************************************************************
		public bool RemovePath(Path p)
		{
			if (!mPaths.Contains(p))
				return false;
			
			int i = mPaths.IndexOf(p);

			return RemovePath(i);
		}

		//**************************************************************************
		// RemovePath
		// Rimuove un percorso
		// Parametri:
		//			i	: indice percorso da rimuovere
		// Restituisce:
		//			true	percorso eliminato
		//			false	non � possibile eliminare il percorso
		//**************************************************************************
		public virtual bool RemovePath(int i)
		{
			// Verifica l'indice
			if (i < 0 || i >= mPaths.Count)
				return false;

			mPaths.RemoveAt(i);

			// Ritorna il numero di percorsi
			return true;
		}

		//**************************************************************************
		// GetPath
		// Ritorna il percorso specificato
		// Parametri:
		//			i	: indice percorso
		// Restituisce:
		//			percorso
		//**************************************************************************
		public virtual Path GetPath(int i)
		{
			if (i < 0 || i >= mPaths.Count)
				return null;

			return CreatePath((Path)mPaths[i]);
		}

		//**************************************************************************
		// GetRectangle
		// Ritorna il rettangolo che contiene l'insieme dei percorsi
		//**************************************************************************
		public virtual Rect GetRectangle()
		{
			if (mPaths.Count == 0)
				return null;

			Path p = (Path) mPaths[0];
			Rect rett = p.GetRectangle();

			for (int i = 1; i < mPaths.Count; i++)
			{
				p = (Path) mPaths[i];
				rett = rett + p.GetRectangle();
			}

			return rett;
		}

		//**************************************************************************
		// GetRectangleRT
		// Ritorna il rettangolo che contiene l'insieme dei percorsi rototraslati
		//**************************************************************************
		public virtual Rect GetRectangleRT()
		{
			if (mPaths.Count == 0)
				return null;

			Path p = (Path) mPaths[0];
			Rect rett = p.GetRectangleRT();

			for (int i = 1; i < mPaths.Count; i++)
			{
				p = (Path) mPaths[i];
				rett = rett + p.GetRectangleRT();
			}

			return rett;
		}

		//**************************************************************************
		// RotoTranslAbs
		// Rototraslazione assoluta 
		// Parametri:
		//			offsetX	: offset di traslazione X del percorso
		//			offsetY	: offset di traslazione Y del percorso
		//			centerX	: ascissa X del centro di rotazione
		//			centerY	: ascissa Y del centro di rotazione
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void RotoTranslAbs(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			mMatrixRT.RotoTranslAbs(offsetX, offsetY, centerX, centerY, angle);

			// Imposta la stessa matrice di rototraslazione per tutti i percorsi
			foreach (Path p in mPaths)
				p.MatrixRT = mMatrixRT;
		}

		//**************************************************************************
		// RotoTranslRel
		// Rototraslazione relativa 
		// Parametri:
		//			offsetX	: offset di traslazione X del percorso
		//			offsetY	: offset di traslazione Y del percorso
		//			centerX	: ascissa X del centro di rotazione
		//			centerY	: ascissa Y del centro di rotazione
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void RotoTranslRel(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			mMatrixRT.RotoTranslRel(offsetX, offsetY, centerX, centerY, angle);

			// Imposta la stessa matrice di rototraslazione per tutti i percorsi
			foreach (Path p in mPaths)
				p.MatrixRT = mMatrixRT;
		}

		//**************************************************************************
		// GetRotTrasl
		// Legge la traslazione e l'angolo di rotazione del percorso
		// Parametri:
		//			offsetX	: traslazione X
		//			offsetY	: traslazione Y
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void GetRotoTransl(out double offsetX, out double offsetY, out double angle)
		{
			mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);
		}

		//**************************************************************************
		// SetRotoTransl
		// Setta la traslazione e l'angolo di rotazione del percorso
		// Parametri:
		//			offsetX	: traslazione X
		//			offsetY	: traslazione Y
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void SetRotoTransl(double offsetX, double offsetY, double angle)
		{
			mMatrixRT.SetRotoTransl(offsetX, offsetY, angle);

			// Imposta la stessa matrice di rototraslazione per tutti i percorsi
			foreach (Path p in mPaths)
				p.MatrixRT = mMatrixRT;
		}

		//**************************************************************************
		// Normalize
		// Ritorna l'insieme dei percorsi normalizzato
		// Parametri:
		//			offset	: punto di normalizzazione
		//**************************************************************************
		public virtual PathCollection Normalize()
		{
			Rect r = GetRectangle();
			Point offset = new Point(r.Left, r.Top);

			PathCollection pc = CreatePathCollection();

			foreach (Path p in mPaths)
				pc.AddPath(p.Normalize(offset));

			pc.MatrixRT = mMatrixRT;

			return pc;
		}

		//**************************************************************************
		// MayAdd
		// Verifica se � possibile aggiungere il percorso alla zona
		// Parametri:
		//			p		: percorso da aggiungere
		// Restituisce:
		//			true	: il percorso pu� essere inseribile
		//			false	: il percorso non pu� essere inserito
		//**************************************************************************
		public bool MayAdd(Path p)
		{
			return MayInsert(mPaths.Count, p);
		}
		//**************************************************************************
		// MayInsert
		// Verifica se il percorso � inseribile
		// Parametri:
		//			index	: posizione di inserimento
		//			p		: percorso da inserire
		// Restituisce:
		//			true	: il percorso pu� essere inseribile
		//			false	: il percorso non pu� essere inserito
		//**************************************************************************
		public virtual bool MayInsert(int index, Path p)
		{
			return true;
		}


		/// ********************************************************************
		/// <summary>
		/// Ritorna un array con tutti i vertici di tutti i poligoni
		/// </summary>
		/// <returns>Array di punti</returns>
		/// ********************************************************************
		public Point[] GetVertexList()
		{
			int i = 0, npoints = 0;

			// Conta il numero di vertici totali
			foreach (Path p in mPaths)
				npoints += p.VertexCount;

			// Crea il vettore di punti
			Point[] points = new Point[npoints];

			// Scorre tutti i poligoni
			foreach (Path p in mPaths)
			{
				// Ritorna tutti i punti del poligono
				Point[] pts = p.GetVertexList();
				pts.CopyTo(points, i);
				i += pts.Length;
			}

			return points;
		}

		public PathCollection GetPathCollectionWithoutArcs(double coorderror, Breton.Polygons.Path.EN_ARC_DISCRETIZE_MODE mode)
		{
			PathCollection localPathCollection = new PathCollection();
			foreach (Path p in mPaths)
				localPathCollection.AddPath(p.GetPathWithoutArcs(coorderror, mode));

			return localPathCollection;
		}

		//**************************************************************************
		// GetOriginalTypeFromXML
		// ritorna il tipo originale descritto nel file XML di cui viene passato il 
		// nodo.
		// Parametri:
		//			n	: nodo XML contenete il lato
		// Ritorna:
		//			tipo originale identificato
		//**************************************************************************
		public static string GetOriginalTypeFromXML(XmlNode n)
		{
			XmlNode xn = n.Attributes.GetNamedItem("OriginalType");
			if (xn == null)
				return "";
			return xn.Value;
		}

		//**************************************************************************
		// ReadDerivedFileXml
		// lettura di parametri accessori previsti da classi derivate.
		// Parametri:
		//			n	: nodo XML contenete il lato
		// Ritorna:
		//			true	lettura eseguita con successo
		//			false	errore nella lettura
		//**************************************************************************
		public virtual bool ReadDerivedFileXml(XmlNode n)
		{
			return true;
		}

		//**************************************************************************
		// ReadFileXml
		// Legge il lato dal file XML
		// Parametri:
		//			n	: nodo XML contenete il lato
		// Ritorna:
		//			true	lettura eseguita con successo
		//			false	errore nella lettura
		//**************************************************************************
		public bool ReadFileXml(XmlNode n)
		{
			Path p;


			if (!ReadDerivedFileXml(n))
				return false;

			try
			{
				XmlNodeList l = n.ChildNodes;
				
				// Scorre tutti i sottonodi
				foreach (XmlNode chn in l)
				{
					// Legge i nodi dei polygoni
					if (chn.Name == "Path")
					{
						p = CreatePath();
						if (p.ReadFileXml(chn))
							mPaths.Add(p);
					}
						
						// Legge il nodo della matrice
					else if (chn.Name == "MatrixRT")
					{
						double[,] m = new double[3,3];

						m[0, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m00").Value, CultureInfo.InvariantCulture);
						m[0, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m01").Value, CultureInfo.InvariantCulture);
						m[0, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m02").Value, CultureInfo.InvariantCulture);
						m[1, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m10").Value, CultureInfo.InvariantCulture);
						m[1, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m11").Value, CultureInfo.InvariantCulture);
						m[1, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m12").Value, CultureInfo.InvariantCulture);
						m[2, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m20").Value, CultureInfo.InvariantCulture);
						m[2, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m21").Value, CultureInfo.InvariantCulture);
						m[2, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m22").Value, CultureInfo.InvariantCulture);

						mMatrixRT.Matrix = m;
					}
					else if (chn.Name == "Attributes")
					{
						foreach (XmlNode attr in chn.ChildNodes)
						{
							XmlAttribute val = attr.Attributes["value"];
							mAttributes.Add(attr.Name, val.Value);
						}
					}

				}
				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("PathCollection.ReadFileXml Error.", ex);
				return false;
			}
		}

		//**************************************************************************
		// WriteDerivedFileXml
		// Scrittura informazioni accessorie previste da classi derivate
		// Parametri:
		//			w: oggetto per scrivere il file XML
		//**************************************************************************
		public virtual void WriteDerivedFileXml(XmlTextWriter w)
		{
		}

		//**************************************************************************
		// WriteFileXml
		// Scrive il lato su file XML
		// Parametri:
		//			w: oggetto per scrivere il file XML
		//**************************************************************************
		public virtual void WriteFileXml(XmlTextWriter w)
		{
			w.WriteStartElement("PathCollection");

			w.WriteAttributeString( "OriginalType", szOriginalType );

			w.WriteAttributeString("Name", szName);

			WriteDerivedFileXml(w);

			w.WriteStartElement("Attributes");
			foreach (KeyValuePair<string, string> a in mAttributes)
			{
				w.WriteStartElement(a.Key);
				w.WriteAttributeString("value", a.Value);
				w.WriteEndElement();
			}
			w.WriteEndElement();

			double[,] m = mMatrixRT.Matrix;
			// Scrive la matrice di rototraslazione
			w.WriteStartElement("MatrixRT");
			w.WriteAttributeString("m00", m[0, 0].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m01", m[0, 1].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m02", m[0, 2].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m10", m[1, 0].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m11", m[1, 1].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m12", m[1, 2].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m20", m[2, 0].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m21", m[2, 1].ToString(CultureInfo.InvariantCulture));
			w.WriteAttributeString("m22", m[2, 2].ToString(CultureInfo.InvariantCulture));
			w.WriteEndElement();

			// Scrive tutti i lati
			foreach (Path p in mPaths)
				p.WriteFileXml(w, false);

			w.WriteEndElement();
		}

		//**************************************************************************
		// WriteDerivedFileXml
		// Scrittura informazioni accessorie previste da classi derivate
		// Parametri:
		//			w: oggetto per scrivere il file XML
		//**************************************************************************
		public virtual void WriteDerivedFileXml(XmlNode baseNode)
		{
		}

		///**************************************************************************
		/// <summary>
		/// Scrive la zona sul nodo XML
		/// </summary>
		/// <param name="baseNode">nodo XML dove scrivere il path</param>
		///**************************************************************************
		public void WriteFileXml(XmlNode baseNode)
		{
			WriteFileXml(baseNode, true);
		}

		///**************************************************************************
		/// <summary>
		/// Scrive la zona sul nodo XML
		/// </summary>
		/// <param name="baseNode">nodo XML dove scrivere il path</param>
		/// <param name="saveMatrix">salva la matrice di rototraslazione</param>
		///**************************************************************************
		public void WriteFileXml(XmlNode baseNode, bool saveMatrix)
		{
			WriteFileXml(baseNode, saveMatrix, false);
		}

		///************************************************************************************
		/// <summary>
		/// Scrive la zona sul nodo XML
		/// </summary>
		/// <param name="baseNode">nodo XML dove scrivere il path</param>
		/// <param name="saveMatrix">salva la matrice di rototraslazione</param>
		/// <param name="savePathMatrix">salva la matrice di rototraslazione per i path</param>
		///*************************************************************************************
		public void WriteFileXml(XmlNode baseNode, bool saveMatrix, bool savePathMatrix)
		{
			XmlDocument doc = baseNode.OwnerDocument;
			XmlAttribute attr;
			XmlNode node = doc.CreateNode(XmlNodeType.Element, "PathCollection", "");

			attr = doc.CreateAttribute("OriginalType");
			attr.Value = szOriginalType;
			node.Attributes.Append(attr);

			attr = doc.CreateAttribute("Name");
			attr.Value = szName;
			node.Attributes.Append(attr);

			WriteDerivedFileXml(node);

			// Inserisce gli attributi del path
			XmlNode attributes = doc.CreateNode(XmlNodeType.Element, "Attributes", ""); ;
			foreach (KeyValuePair<string, string> a in mAttributes)
			{
				XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, a.Key, ""); ;
				attr = doc.CreateAttribute("value");
				attr.Value = a.Value;
				attributeValue.Attributes.Append(attr);
				attributes.AppendChild(attributeValue);
			}
			node.AppendChild(attributes);

			if (saveMatrix)
			{
				double[,] m = mMatrixRT.Matrix;

				// Scrive la matrice di rototraslazione
				XmlNode matrix = doc.CreateNode(XmlNodeType.Element, "MatrixRT", "");

				attr = doc.CreateAttribute("m00");
				attr.Value = m[0, 0].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m01");
				attr.Value = m[0, 1].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m02");
				attr.Value = m[0, 2].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m10");
				attr.Value = m[1, 0].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m11");
				attr.Value = m[1, 1].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m12");
				attr.Value = m[1, 2].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m20");
				attr.Value = m[2, 0].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m21");
				attr.Value = m[2, 1].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);
				attr = doc.CreateAttribute("m22");
				attr.Value = m[2, 2].ToString(CultureInfo.InvariantCulture);
				matrix.Attributes.Append(attr);

				node.AppendChild(matrix);
			}

			// Scrive tutti i paths
			foreach (Path path in mPaths)
				path.WriteFileXml(node, savePathMatrix);

			baseNode.AppendChild(node);
		}

		///**************************************************************************
		/// <summary>
		/// Verifica se il nome originale della classe corrisponde con la stringa
		/// indicata come parametro.
		/// <param name="s">stringa da controllare</param>
		/// </summary>
		/// <returns>
		///     true se la stringa corrisponde con il nome interno, 
		///     false altrimenti
		/// </returns>
		///**************************************************************************
		public bool IsThisOriginalType( string s )
		{
			if (s == szOriginalType)
				return  true;
			return  false;
		}

		///**************************************************************************
		/// <summary>
		///     Determina se la lista corrente di percorsi insterseca il percorso
		///     indicato.
		/// </summary>
		/// <param name="p">
		///     percorso verso la quale eseguire il controllo
		/// </param>
		/// <param name="points">
		///     se indicato, rappresenta la lista che conterra' gli eventuali
		///     punti di intersezione
		/// </param>
		/// <returns>
		///     true se esiste almento una intersezione
		///     false altrimenti.
		/// </returns>
		/// <remarks>
		///     se no viene indicata la lista di punti di intersezione, la
		///     prima intersezione trovata interrompe la valutazione;
		/// </remarks>
		///**************************************************************************
		public bool Intersect(Path p, List<Point> points)
		{
			for (int i = 0; i < Count; i++)
			{
				if (points == null)
				{
					if (this[i].Intersect(p))
						return true;
				}
				else
				{
					List<Point> ips;
					if (this[i].Intersect(p, out ips))
					{
						foreach (Point pt in ips)
							points.Add(pt);
					}
				}
			}

			if (points == null)
				return false;

			return (points.Count > 0 ? true : false);
		}

		///**************************************************************************
		/// <summary>
		///     Determina se la lista corrente di percorsi e quella indicata
		///     hanno intersezioni.
		/// </summary>
		/// <param name="pc">
		///     lista di percorsi verso la quale eseguire il controllo
		/// </param>
		/// <param name="points">
		///     se indicato, rappresenta la lista che conterra' gli eventuali
		///     punti di intersezione
		/// </param>
		/// <returns>
		///     true se esiste almento una intersezione
		///     false altrimenti.
		/// </returns>
		/// <remarks>
		///     se no viene indicata la lista di punti di intersezione, la
		///     prima intersezione trovata interrompe la valutazione;
		///     in caso contrario si collezionano tutti i punti di intersezione;
		///     il controllo viene fatto tra tutti i percorsi della lista
		///     corrente verso tutti i percorsi della lista indicata; non sono
		///     eseguiti controlli all'interno delle singole liste.
		/// </remarks>
		///**************************************************************************
		public bool Intersect(PathCollection pc, List<Point> points)
		{
			for (int i = 0; i < pc.Count; i++)
				if (Intersect(pc[i], points))
					if (points == null)
						return true;

			if (points == null)
				return false;

			return (points.Count > 0 ? true : false);
		}

		#endregion

		#region Protected Methods

		///**************************************************************************
		/// <summary>
		/// CreatePath: create a new path object
		/// </summary>
		/// <remarks>created path</remarks> 
		//**************************************************************************
		protected virtual Path CreatePath()
		{
			return new Path();
		}

		///**************************************************************************
		/// <summary>
		/// CreatePath: create a new path object
		/// </summary>
		/// <param name="p">source path to copy from</param>
		/// <remarks>created path</remarks> 
		//**************************************************************************
		protected virtual Path CreatePath(Path p)
		{
			return p.Clone();
		}

		///**************************************************************************
		/// <summary>
		/// CreatePathCollection: create a new paths collection
		/// </summary>
		/// <remarks>created path</remarks> 
		//**************************************************************************
		protected virtual PathCollection CreatePathCollection()
		{
			return new PathCollection();
		}

		#endregion

	}
}
