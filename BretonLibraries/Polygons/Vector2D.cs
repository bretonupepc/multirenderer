﻿using System;
using System.Diagnostics;
using Breton.MathUtils;

namespace Breton.Polygons
{
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    public class Vector2D : IComparable<Vector2D>, IEquatable<Vector2D>
    {
        // proprietà utilizzata per la visualizzazione in debug
        private string DebuggerDisplay { get { return string.Format("X: {0:0.000} Y: {1:0.000}", X, Y); } }

        public static Vector2D Zero { get { return new Vector2D(0d, 0d); } }
        public static Vector2D XAxis { get { return new Vector2D(1d, 0d); } }
        public static Vector2D YAxis { get { return new Vector2D(0d, 1d); } }

        public double X { get; set; }
        public double Y { get; set; }
        public double MagnitudeSquared { get { return X * X + Y * Y; } }
        public double Magnitude { get { return Math.Sqrt(MagnitudeSquared); } }
        public double Length { get { return Math.Sqrt(MagnitudeSquared); } set { SetLen(value); } }
        public double LengthSquared { get { return MagnitudeSquared; } }
        public double DirectionAngle { get { return MathUtils.MathUtil.Angolo_0_2PI_Ex(Math.Atan2(Y, X)); } }
        public bool IsValid { get { return Length > 0d; } }
        public Vector2D UnitVector { get { return GetNormalized(); } }

        public Vector2D()
        {
            // x e y sono già a 0 come valore di default
        }

        public Vector2D(double _x, double _y)
        {
            X = _x;
            Y = _y;
        }

        public Vector2D(Vector2D v)
        {
            X = v.X;
            Y = v.Y;
        }

        public Vector2D(Point v)
        {
            X = v.X;
            Y = v.Y;
        }
        
        public static Vector2D Create(double x, double y)
        {
            return new Vector2D(x, y);
        }

        public static Vector2D CreateNormalized(double x, double y)
        {
            if (MathUtil.QuoteIsZero(x) && MathUtil.QuoteIsZero(y))
            {
                return new Vector2D(x, y);
            }
            else
            {
                double magnitude = 1d / Math.Sqrt(x * x + y * y);
                return new Vector2D(x * magnitude, y * magnitude);
            }
        }

        public static Vector2D CreateNormalizedFromPoint(Point coordinates)
        {
            if (MathUtil.QuoteIsZero(coordinates.X) && MathUtil.QuoteIsZero(coordinates.Y))
            {
                return new Vector2D(coordinates);
            }
            else
            {
                double magnitude = 1d / Math.Sqrt(coordinates.X * coordinates.X + coordinates.Y * coordinates.Y);
                return new Vector2D(coordinates.X * magnitude, coordinates.Y * magnitude);
            }
        }

        public static Vector2D CreateFromPoints(Point start, Point end)
        {
            return new Vector2D(end.X - start.X, end.Y - start.Y);
        }

        public static Vector2D CreateNormalizedFromPoints(Point start, Point end)
        {
            return Vector2D.CreateNormalized(end.X - start.X, end.Y - start.Y);
        }
        
        public static Vector2D CreateFromAngle(double angle, double len)
        {
            return new Vector2D(Math.Cos(angle) * len, Math.Sin(angle) * len);
        }

        public static Vector2D CreateOrthogonalFromPoints(Point start, Point end)
        {
            return Vector2D.Create(start.Y - end.Y, end.X - start.X);
        }

        public static Vector2D CreateNormalizedOrthogonalFromPoints(Point start, Point end)
        {
            return Vector2D.CreateNormalized(start.Y - end.Y, end.X - start.X);
        }

        public Point ToPoint()
        {
            return new Point(X, Y);
        }

        public void CopyFrom(Point point)
        {
            X = point.X;
            Y = point.Y;
        }

        public double SignedAngleTo(Vector2D v2)
        {
            double dot = DotProduct(v2);
            double perpDot = PerpDotProduct(v2);

            return Math.Atan2(perpDot, dot);
        }

        public double AngleTo(Vector2D toVector2D)
        {
            return Math.Acos(GetNormalized().DotProduct(toVector2D.GetNormalized()));
        }

        public bool IsParallelTo(Vector2D othervector, double tolerance = MathUtil.FLT_EPSILON)
        {
            if (IsZero(tolerance) || othervector.IsZero(tolerance))
                return false;
            //return MathUtil.QuoteIsZero(1 - Math.Abs(GetNormalized().DotProduct(othervector.GetNormalized())));
            //return MathUtil.QuoteIsEQs(x / othervector.x, y / othervector.y);
            return MathUtil.QuoteIsEQ(Math.Abs(DotProduct(othervector)), Magnitude * othervector.Magnitude, tolerance);
        }

        public bool IsPerpendicularTo(Vector2D othervector, double tolerance = MathUtil.FLT_EPSILON)
        {
            return MathUtil.QuoteIsZero(DotProduct(othervector), tolerance);
        }

        public double DotProduct(Vector2D other)
        {
            return X * other.X + Y * other.Y;
        }

        public double PerpDotProduct(Vector2D other)
        {
            return X * other.Y - Y * other.X;
        }

        public void Round(int decimals)
        {
            X = Math.Round(X, decimals);
            Y = Math.Round(Y, decimals);
        }

        public void SetLen(double len)
        {
            if (MathUtil.IsZero(len))
                return;

            Normalize();
            X *= len;
            Y *= len;
        }

        public Vector2D GetNormalized()
        {
            double magnitude = Magnitude;
            if (MathUtil.QuoteIsZero(magnitude, MathUtil.FLT_EPSILON))
                return Vector2D.Zero;

            return new Vector2D(X / magnitude, Y / magnitude);
        }

        public Vector2D GetInverted()
        {
            Vector2D vec = new Vector2D(this);
            vec.Invert();

            return vec;
        }

        public Vector2D GetRotated(double radians)
        {
            Vector2D vect = new Vector2D(this);
            vect.Rotate(radians);

            return vect;
        }

        public Vector2D GetOrthogonal()
        {
            return new Vector2D(-Y, X);
        }

        public Vector2D GetInvertedOrthogonal()
        {
            return new Vector2D(Y, -X);
        }

        public Vector2D GetProjectedOn(Vector2D other)
        {
            return other * (DotProduct(other) / other.LengthSquared);
        }

        public Vector2D GetTransformedBy(RTMatrix mtx)
        {
            double x, y;
            mtx.PointTransform(X, Y, out x, out y);

            return new Vector2D(x, y);
        }

        public void Normalize()
        {
            double magnitude = Magnitude;
            if (MathUtil.QuoteIsZero(magnitude, MathUtil.FLT_EPSILON))
                return;

            X /= magnitude;
            Y /= magnitude;
        }

        public void Invert()
        {
            X = -X;
            Y = -Y;
        }

        public void Rotate(double radians)
        {
            double cs = Math.Cos(radians);
            double sn = Math.Sin(radians);
            double xLocal = (X * cs) - (Y * sn);
            double yLocal = (X * sn) + (Y * cs);

            X = xLocal;
            Y = yLocal;
        }

        public void TransformBy(RTMatrix mtx)
        {
            double x, y;
            mtx.PointTransform(X, Y, out x, out y);
            X = x;
            Y = y;
        }

        public bool AreEqualsOrInverses(Vector2D other)
        {
            return CompareTo(other) == 0 || CompareTo(other.GetInverted()) == 0;
        }

        public static Vector2D Sum(params Vector2D[] points)
        {
            Vector2D p = Vector2D.Zero;
            for (int i = 0; i < points.Length; i++)
                p += points[i];

            return p;
        }

        public static Vector2D Average(params Vector2D[] points)
        {
            Vector2D p = Vector2D.Sum(points);
            p.X /= points.Length;
            p.Y /= points.Length;

            return p;
        }

        public static Vector2D operator +(Vector2D v0, Vector2D v1)
        {
            return new Vector2D(v0.X + v1.X, v0.Y + v1.Y);
        }

        public static Vector2D operator -(Vector2D v0, Vector2D v1)
        {
            return new Vector2D(v0.X - v1.X, v0.Y - v1.Y);
        }

        public static Vector2D operator -(Vector2D v0)
        {
            Vector2D v = new Vector2D(v0);
            v.Invert();

            return v;
        }

        public static Vector2D operator *(Vector2D v, double scalar)
        {
            return new Vector2D(v.X * scalar, v.Y * scalar);
        }

        public static Vector2D operator *(double scalar, Vector2D v)
        {
            return new Vector2D(v.X * scalar, v.Y * scalar);
        }

        public static Vector2D operator /(Vector2D v, double scalar)
        {
            return new Vector2D(v.X / scalar, v.Y / scalar);
        }

        public bool Equals(Vector2D other)
        {
            return other != null && CompareTo(other) == 0;
        }

        public bool Equals(Vector2D other, double epsilon)
        {
            return other != null && CompareTo(other, epsilon) == 0;
        }

        public bool Equals(Point other)
        {
            return other != null && CompareTo(other) == 0;
        }

        public bool Equals(Point other, double epsilon)
        {
            return other != null && CompareTo(other, epsilon) == 0;
        }

        public int CompareTo(Vector2D other)
        {
            return CompareTo(other, MathUtil.QUOTE_EPSILON);
        }

        public int CompareTo(Point other)
        {
            int compareX = MathUtil.Compare(X, other.X, MathUtil.QUOTE_EPSILON);
            if (compareX != 0)
                return compareX;

            return MathUtil.Compare(Y, other.Y, MathUtil.QUOTE_EPSILON);
        }

        public int CompareTo(Point value, double epsilon)
        {
            int compareX = MathUtil.Compare(X, value.X, epsilon);
            if (compareX != 0)
                return compareX;

            return MathUtil.Compare(Y, value.Y, epsilon);
        }

        public int CompareTo(Vector2D value, double epsilon)
        {
            int compareX = MathUtil.Compare(X, value.X, epsilon);
            if (compareX != 0)
                return compareX;

            return MathUtil.Compare(Y, value.Y, epsilon);
        }

        public bool IsZero(double epsilon = MathUtil.FLT_EPSILON)
        {
            return MathUtil.QuoteIsZero(X, epsilon) && MathUtil.QuoteIsZero(Y, epsilon);
        }

        public override string ToString()
        {
            return String.Format(System.Globalization.CultureInfo.InvariantCulture, "[{0}][{1}]", X, Y);
        }

        public string ToString(string format)
        {
            return String.Format(System.Globalization.CultureInfo.InvariantCulture, "[{0}][{1}]", X.ToString(format, System.Globalization.CultureInfo.InvariantCulture), Y.ToString(format, System.Globalization.CultureInfo.InvariantCulture));
        }
    }
}
