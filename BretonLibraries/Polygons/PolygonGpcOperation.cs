using System;
using System.Collections.Generic;
using System.Text;
using Breton.GpcWrapper;

namespace Breton.Polygons
{
	public class PolygonGpcOperation
	{
		#region Public Static Methods

		/// *************************************************************************
		/// <summary>
		/// Esegue l'operazione selezionata tra le due zone
		/// </summary>
		/// <param name="operation">operazione da eseguire</param>
		/// <param name="subject_zone">zona su cui operare</param>
		/// <param name="clip_zone">zona operatore</param>
		/// <returns>
		///		la path collection risultante
		/// </returns>
		/// *************************************************************************
		public static PathCollection Clip(GpcOperation operation, Zone subject_zone, Zone clip_zone)
		{
			Polygon subject_polygon = ZoneToGpcPolygon(subject_zone, false);
			Polygon clip_polygon = ZoneToGpcPolygon(clip_zone, false);
			Polygon result_polygon = Breton.GpcWrapper.GpcWrapper.Clip(operation, subject_polygon, clip_polygon);
			return GpcPolygonToPathCollection(result_polygon);
		}

		/// *************************************************************************
		/// <summary>
		/// Esegue l'operazione selezionata tra le due zone
		/// </summary>
		/// <param name="operation">operazione da eseguire</param>
		/// <param name="subject_zone">zona su cui operare</param>
		/// <param name="clip_zone">zona operatore</param>
		/// <returns>
		///		la path collection risultante
		/// </returns>
		/// *************************************************************************
		public static PathCollection ClipRT(GpcOperation operation, Zone subject_zone, Zone clip_zone)
		{
			Polygon subject_polygon = ZoneToGpcPolygon(subject_zone, true);
			Polygon clip_polygon = ZoneToGpcPolygon(clip_zone, true);
			Polygon result_polygon = Breton.GpcWrapper.GpcWrapper.Clip(operation, subject_polygon, clip_polygon);
			return GpcPolygonToPathCollection(result_polygon);
		}

		#endregion


		#region Private Static Methods

		/// *************************************************************************
		/// <summary>
		/// Trasforma una zona in un poligono Gpc
		/// </summary>
		/// <param name="z">zona da trasformare</param>
		/// <param name="rt">considera la zona rototraslata</param>
		/// <returns>poligono Gpc risultante</returns>
		/// *************************************************************************
		private static Polygon ZoneToGpcPolygon(Zone z, bool rt)
		{
			return ZoneToGpcPolygon(z, rt, new Polygon(), 0);
		}
		private static Polygon ZoneToGpcPolygon(Zone z, bool rt, Polygon p, int level)
		{
			// Verifica che sia una zona valida
			if (z == null)
				return p;

			// Preleva il percorso esterno
			Path path = z.GetPath();
			if (path == null || path.VertexCount < 3)
				return p;

			// Crea un percorso
			VertexList contour = new VertexList();
			contour.NofVertices = path.VertexCount;
			contour.Vertex = new Vertex[path.VertexCount];

			for (int i = 0; i < path.VertexCount; i++)
			{
				Point pt;
				if (rt)
					pt = path.GetVertexRT(i);
				else
					pt = path.GetVertex(i);

				contour.Vertex[i].X = pt.X;
				contour.Vertex[i].Y = pt.Y;
			}
			
			// Aggiunge il percorso al poligono
			p.AddContour(contour, (level & 1) == 1);

			// Scorre tutte le zone interne
			for (int i = 0; i < z.ZoneCount; i++)
			{
				Zone ze = z.GetZone(i);

				p = ZoneToGpcPolygon(ze, rt, p, level + 1);
			}

			return p;
		}


		/// *************************************************************************
		/// <summary>
		/// Trasforma un poligono Gpc in una Path Collection
		/// </summary>
		/// <param name="p">poligono Gpc da trasformare</param>
		/// <returns>PathCollection risultante</returns>
		/// *************************************************************************
		private static PathCollection GpcPolygonToPathCollection(Polygon p)
		{
			PathCollection pc = new PathCollection();

			for (int i = 0; i < p.NofContours; i++)
			{
				VertexList vl = p.Contour[i];
				Path pt = new Path();
				
				for (int j = 0; j < vl.NofVertices; j++)
					pt.AddVertex(vl.Vertex[j].X, vl.Vertex[j].Y);
				
				pt.Close();
				
				pc.AddPath(pt);
			}
			
			return pc;
		}

		#endregion

	}

}
