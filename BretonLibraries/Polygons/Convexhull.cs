// Finding a convex hull in the plane
// This program requires .Net version 2.0.
// Peter Sestoft (sestoft@itu.dk) * Java 2000-10-07, GC# 2001-10-27
// Adattato da: Nicola Guarise 2009-06-10

using System;
using Breton.MathUtils;

// ------------------------------------------------------------

// Find the convex hull of a point set in the plane

// An implementation of Graham's (1972) point elimination algorithm,
// as modified by Andrew (1979) to find lower and upper hull separately.

// This implementation correctly handles duplicate points, and
// multiple points with the same x-coordinate.

// 1. Sort the points lexicographically by increasing (x,y), thus 
//    finding also a leftmost point L and a rightmost point R.
// 2. Partition the point set into two lists, upper and lower, according as 
//    point is above or below the segment LR.  The upper list begins with 
//    L and ends with R; the lower list begins with R and ends with L.
// 3. Traverse the point lists clockwise, eliminating all but the extreme
//    points (thus eliminating also duplicate points).
// 4. Eliminate L from lower and R from upper, if necessary.
// 5. Join the point lists (in clockwise order) in an array.

namespace Breton.Polygons
{
    public class Convexhull
    {
        /// ********************************************************************
        /// <summary>
        /// Ritorna il path convesso, a partire dall'insieme dei vertici definiti 
        /// dal path di ingresso.
        /// </summary>
        /// <param name="path">path di ingresso</param>
        /// <returns>path convesso ritornato</returns>
        /// ********************************************************************
        public static Path convexhull(Path path)
        {
            // Crea il convex hull della lista di punti
            Point[] pts = convexhull(path.GetVertexList());

            // Trasforma la lista di punti ottenuta in un path
            Path newpath = new Path();
            for (int i = 0; i < pts.Length; i++)
                newpath.AddVertex(pts[i]);

            // Chiude il path
            newpath.Close();
            // Imposta la stessa matrice di rototraslazione del path originario
            newpath.MatrixRT = new RTMatrix(path.MatrixRT);

            return newpath;
        }

        /// ********************************************************************
        /// <summary>
        /// Ritorna il path convesso, a partire dall'insieme dei vertici definiti 
        /// da una lista di path in ingresso.
        /// </summary>
        /// <param name="paths">path collection di ingresso</param>
        /// <returns>path convesso ritornato</returns>
        /// ********************************************************************
        public static Path convexhull(PathCollection paths)
        {
            // Crea il convex hull della lista di punti
            Point[] pts = convexhull(paths.GetVertexList());

            // Trasforma la lista di punti ottenuta in un path
            Path newpath = new Path();
            for (int i = 0; i < pts.Length; i++)
                newpath.AddVertex(pts[i]);
            // Chiude il path
            newpath.Close();
            // Imposta la stessa matrice di rototraslazione del path originario
            newpath.MatrixRT = new RTMatrix(paths.MatrixRT);

            return newpath;
        }

        /// ********************************************************************
        /// <summary>
        /// Ritorna l'insieme di punti convessi, a partire dall'insieme di punti in ingresso.
        /// </summary>
        /// <param name="pts">insieme di punti in ingresso</param>
        /// <returns>insieme di punti convessi</returns>
        /// ********************************************************************
        public static Point[] convexhull(Point[] pts)
        {
            // Sort points lexicographically by increasing (x, y)
            int N = pts.Length;
            PointQuickSort.Quicksort(pts);
            Point left = pts[0], right = pts[N - 1];
            // Partition into lower hull and upper hull
            CDLL<Point> lower = new CDLL<Point>(left), upper = new CDLL<Point>(left);
            for (int i = 0; i < N; i++)
            {
                if (!right.AlmostEqual(pts[i], MathUtil.FLT_EPSILON))
                {
                    double det = left.Surface2(right, pts[i]);
                    if (det > 0)
                        upper = upper.Append(new CDLL<Point>(pts[i]));
                    else if (det < 0)
                        lower = lower.Prepend(new CDLL<Point>(pts[i]));
                }
            }
            lower = lower.Prepend(new CDLL<Point>(right));
            upper = upper.Append(new CDLL<Point>(right)).Next;
            // Eliminate points not on the hull
            eliminate(lower);
            eliminate(upper);
            // Eliminate duplicate endpoints
            if (lower.Prev.val == upper.val)
                lower.Prev.Delete();
            if (upper.Prev.val == lower.val)
                upper.Prev.Delete();
            // Join the lower and upper hull
            Point[] res = new Point[lower.Size() + upper.Size()];
            lower.CopyInto(res, 0);
            upper.CopyInto(res, lower.Size());
            return res;
        }

        // Graham's scan
        private static void eliminate(CDLL<Point> start)
        {
            CDLL<Point> v = start, w = start.Prev;
            bool fwd = false;
            while (v.Next != start || !fwd)
            {
                if (v.Next == w)
                    fwd = true;
                if (v.val.Surface2(v.Next.val, v.Next.Next.val) < 0) // right turn
                    v = v.Next;
                else
                {                                       // left turn or straight
                    v.Next.Delete();
                    v = v.Prev;
                }
            }
        }
    }

    // ------------------------------------------------------------

    // Circular doubly linked lists of T

    class CDLL<T>
    {
        private CDLL<T> prev, next;     // not null, except in deleted elements
        public T val;

        // A new CDLL node is a one-element circular list
        public CDLL(T val)
        {
            this.val = val; next = prev = this;
        }

        public CDLL<T> Prev
        {
            get { return prev; }
        }

        public CDLL<T> Next
        {
            get { return next; }
        }

        // Delete: adjust the remaining elements, make this one point nowhere
        public void Delete()
        {
            next.prev = prev; prev.next = next;
            next = prev = null;
        }

        public CDLL<T> Prepend(CDLL<T> elt)
        {
            elt.next = this; elt.prev = prev; prev.next = elt; prev = elt;
            return elt;
        }

        public CDLL<T> Append(CDLL<T> elt)
        {
            elt.prev = this; elt.next = next; next.prev = elt; next = elt;
            return elt;
        }

        public int Size()
        {
            int count = 0;
            CDLL<T> node = this;
            do
            {
                count++;
                node = node.next;
            } while (node != this);
            return count;
        }

        public void PrintFwd()
        {
            CDLL<T> node = this;
            do
            {
                Console.WriteLine(node.val);
                node = node.next;
            } while (node != this);
            Console.WriteLine();
        }

        public void CopyInto(T[] vals, int i)
        {
            CDLL<T> node = this;
            do
            {
                vals[i++] = node.val;	// still, implicit checkcasts at runtime 
                node = node.next;
            } while (node != this);
        }
    }

    // ------------------------------------------------------------

    class PointQuickSort
    {
        private static void swap(Point[] arr, int s, int t)
        {
            Point tmp = arr[s]; arr[s] = arr[t]; arr[t] = tmp;
        }

        // Typed OO-style quicksort a la Hoare/Wirth

        private static void qsort(Point[] arr, int a, int b)
        {
            // sort arr[a..b]
            if (a < b)
            {
                int i = a, j = b;
                Point x = arr[(i + j) / 2];
                do
                {
                    //while (arr[i] < x) i++;
                    //while (x < arr[j]) j--;
                    // 20-04-2016 per il convex hull il confronto tra i punti non deve avere tolleranze (che invece sono utilizzate negli operatori del Point)
                    while (PointCompare(arr[i], x) < 0) i++;
                    while (PointCompare(x, arr[j]) < 0) j--;
                    if (i <= j)
                    {
                        swap(arr, i, j);
                        i++; j--;
                    }
                } while (i <= j);
                qsort(arr, a, j);
                qsort(arr, i, b);
            }
        }

        // 20-04-2016 per il convex hull il confronto tra i punti non deve avere tolleranze (che invece sono utilizzate negli operatori del Point)
        private static int PointCompare(Point a, Point b)
        {
            if (a.X < b.X)
                return -1;
            if (a.X > b.X)
                return 1;

            if (a.Y < b.Y)
                return -1;
            if (a.Y > b.Y)
                return 1;

            return 0;
        }

        public static void Quicksort(Point[] arr)
        {
            qsort(arr, 0, arr.Length - 1);
        }
    }
}