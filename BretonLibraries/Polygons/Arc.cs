using System;
using System.Xml;
using System.Globalization;
using Breton.MathUtils;
using TraceLoggers;


namespace Breton.Polygons
{
    public class Arc: Side
    {
        #region Variables
        
        private Point mC;			// Centro arco
        private double mR;			// Raggio arco
        private double mAlpha;		// Angolo di partenza arco (in radianti)
        private double mBeta;		// Angolo sotteso dall'arco (in radianti) (>0 = antiorario / <0 = orario)

        #endregion

        #region Constructors
        //******************************************************************************
        // Constructor
        // Crea l'arco
        // Parametri:
        //           xc		: ascissa del centro
        //           yc		: ordinata del centro
        //           r		: raggio
        //           alpha	: angolo di partenza arco (in radianti)
        //           beta	: angolo sotteso dall'arco (in radianti)
        ////******************************************************************************
        public Arc()
        {
            InitArc(0d, 0d, 0d, 0d, 0d, false);
        }
        public Arc(Arc a): base(a)
        {
            InitArc(a.CenterPoint.X, a.CenterPoint.Y, a.Radius, a.StartAngle, a.AmplAngle, false);
        }
        public Arc(Point c, double r, double alpha, double beta)
        {
            InitArc(c.X, c.Y, r, alpha, beta, false);
        }
        public Arc(double xc, double yc, double r, double alpha, double beta)
        {
            InitArc(xc, yc, r, alpha, beta, false);
        }
        public Arc(double xc, double yc, double r, double alpha, double beta, bool approx)
        {
            InitArc(xc, yc, r, alpha, beta, approx);
        }
        public Arc(double x0, double y0, double x1, double y1, double x2, double y2)
        {
            string log = InitArc(x0, y0, x1, y1, x2, y2);
        }
        public Arc(Breton.Polygons.Point start, Breton.Polygons.Point mid, Breton.Polygons.Point end)
        {
            ArcFrom3PointsMethods.InitArcConsideringDegenerates(this, start, mid, end);
        }

        #endregion

        #region Properties

        public override Point P1
        {
            get { return StartPoint(); }
        }

        public override Point P2
        {
            get { return EndPoint(); }
        }

        //**************************************************************************
        // Dx
        // Restituisce la differenza DX
        //**************************************************************************
        public override double Dx
        {
            get { return P2.X - P1.X; }
        }

        //**************************************************************************
        // Dy
        // Restituisce la differenza DY
        //**************************************************************************
        public override double Dy
        {
            get { return P2.Y - P1.Y; }
        }

        //******************************************************************************
        // Length
        // Restituisce la lunghezza dell'arco
        //******************************************************************************
        public override double Length
        {
            get	{ return Math.Abs(mBeta * mR); }
        }

        //**************************************************************************
        // Inclination
        // Restituisce l'angolo della bisettrice rispetto all'asse delle ascisse
        //**************************************************************************
        public override double Inclination
        {
            get	{ return (mAlpha + mBeta / 2d); }
        }

        //**************************************************************************
        // MiddlePoint
        // Restituisce il punto medio del lato
        //**************************************************************************
        public override Point MiddlePoint
        {
            get	{ return CalcPoint(1, mBeta / 2d); }
        }

        //**************************************************************************
        // VectorSide
        // Restituisce il vettore parallelo al lato
        //**************************************************************************
        public override Vector VectorSide
        {
            get
            {
                Point p1 = this.P1;
                Point p2 = this.P2;
                return new Vector(p2.X, p2.Y, 0d, p1.X, p1.Y, 0d);
            }
        }

        //**************************************************************************
        // VectorP1
        // Restituisce il vettore tangente nel vertice 1
        //**************************************************************************
        public override Vector VectorP1
        {
            get
            {
                Vector v;
                Point p1 = P1;
                
                //Crea il vettore parallelo al raggio nel vertice 1
                Vector r = new Vector(p1.X, p1.Y, mC.X, mC.Y);
                
                //Calcola il vettore perpendicolare al raggio, che va verso la sinistra dell'arco
                if (Direction == 1)
                    v = new Vector(-r.Y, r.X);
                    
                    //Calcola il vettore perpendicolare al raggio, che va verso la destra dell'arco
                else
                    v = new Vector(r.Y, -r.X);
                
                //Imposta la lunghezza del vettore perpendicolare pari al raggio
                return new Vector(v, mR);
            }
        }

        //**************************************************************************
        // VectorP2
        // Restituisce il vettore tangente nel vertice 2
        //**************************************************************************
        public override Vector VectorP2
        {
            get
            {
                Vector v;
                Point p2 = P2;
                
                //Crea il vettore parallelo al raggio nel vertice 2
                Vector r = new Vector(p2.X, p2.Y, mC.X, mC.Y);
                
                //Calcola il vettore perpendicolare al raggio, che va verso la destra dell'arco
                if (Direction == 1)
                    v = new Vector(r.Y, -r.X);
                    
                    //Calcola il vettore perpendicolare al raggio, che va verso la sinistra dell'arco
                else
                    v = new Vector(-r.Y, r.X);
                
                //Imposta la lunghezza del vettore perpendicolare pari al raggio
                return new Vector(v, mR);
            }
        }

        //**************************************************************************
        // GetRectangle
        // Restituisce il rettangolo che contiene l'arco
        //**************************************************************************
        public override Rect GetRectangle
        {
            get
            { 
                double 	   gamma, delta, beta, teta, lim;

                if (mBeta > 0d) 
                {
                    gamma  = mAlpha;
                    delta  = teta = mAlpha + mBeta;
                }
                else 
                {
                    gamma  = mAlpha + mBeta;
                    delta  = teta = mAlpha;
                }

                Point f1 = new Point(mR * Math.Cos(gamma) + mC.X, mR * Math.Sin(gamma) + mC.Y);
                Point f2 = new Point(mR * Math.Cos(delta) + mC.X, mR * Math.Sin(delta) + mC.Y);
                
                Rect rett1 = new Rect(f1.X - MathUtil.FLT_EPSILON, f1.Y - MathUtil.FLT_EPSILON, 
                    f1.X + MathUtil.FLT_EPSILON, f1.Y + MathUtil.FLT_EPSILON);
                Rect rett2 = new Rect(f2.X - MathUtil.FLT_EPSILON, f2.Y - MathUtil.FLT_EPSILON, 
                    f2.X + MathUtil.FLT_EPSILON, f2.Y + MathUtil.FLT_EPSILON);

                rett1 = rett1 + rett2;

                /* porto gamma nel primo quadrante */
                while (gamma < 0d)
                {
                    gamma += Math.PI/2d;
                    delta += Math.PI/2d;
                }

                while (gamma > Math.PI/2d) 
                {
                    gamma -= Math.PI/2d;
                    delta -= Math.PI/2d;
                }

                /* cambio quadrante */
                if (delta > Math.PI/2d) 
                {

                    lim = Math.PI/2d;

                    for (int i = 0; i < 4; i++, lim += Math.PI/2d) 
                    {
                        if (delta < lim)
                            break;

                        beta = teta - (delta - lim);

                        f1 = new Point(mR * Math.Cos(beta) + mC.X, mR * Math.Sin(beta) + mC.Y);
                        
                        rett2 = new Rect(f1.X - MathUtil.FLT_EPSILON, f1.Y - MathUtil.FLT_EPSILON, 
                            f1.X + MathUtil.FLT_EPSILON, f1.Y + MathUtil.FLT_EPSILON);
                        
                        rett1 = rett1 + rett2;
                    }
                }

                /* cambio quadrante */
                if (delta < 0d) 
                {

                    lim = 0d;

                    for (int i = 0; i < 4; i++, lim -= Math.PI/2.0) 
                    {

                        if (delta > lim)
                            break;

                        beta = teta - (delta - lim);

                        f1 = new Point(mR * Math.Cos(beta) + mC.X, mR * Math.Sin(beta) + mC.Y);

                        rett2 = new Rect(f1.X - MathUtil.FLT_EPSILON, f1.Y - MathUtil.FLT_EPSILON, 
                            f1.X + MathUtil.FLT_EPSILON, f1.Y + MathUtil.FLT_EPSILON);
                        
                        rett1 = rett1 + rett2;
                    }
                }

                //Ritorna il rettangolo risultante
                return rett1;
            }
        }

        //**************************************************************************
        // Radius
        // Restituisce/imposta il raggio
        //**************************************************************************
        public override double Radius
        {
            set { mR = value; }
            get { return mR; }
        }

        //**************************************************************************
        // StartAngle
        // Restituisce/imposta l'angolo di partenza
        //**************************************************************************
        public override double StartAngle
        {
            set { mAlpha = value; }
            get { return mAlpha; }
        }

        //**************************************************************************
        // AmplAngle
        // Restituisce/imposta l'angolo di ampiezza
        //**************************************************************************
        public override double AmplAngle
        {
            set { mBeta = value; }
            get { return mBeta; }
        }

        //**************************************************************************
        // EndAngle
        // Restituisce/imposta l'angolo finale
        //**************************************************************************
        public override double EndAngle
        {
            set { mBeta = value - mAlpha; }
            get 
            { 
                double end = mAlpha + mBeta;
                if (end < (-2d * Math.PI))
                    end += 2d * Math.PI;
                else if (end > (2d * Math.PI))
                    end -= 2d * Math.PI;

                return end; 
            }
        }

        //******************************************************************************
        // CenterPoint
        // Restituisce/imposta il centro dell'arco
        //******************************************************************************
        public override Point CenterPoint
        {
            set { mC = new Point(value); }
            get	{ return mC; }
        }

        //******************************************************************************
        // Direction
        // Direzione dell'arco 1=antiorario/-1=orario/0=nullo
        //******************************************************************************
        public override int Direction
        {
            set { mBeta = Math.Abs(mBeta) * value; }
            get { return Math.Sign(mBeta); }
        }

        //******************************************************************************
        // IsClosed
        // Restituisce:
        //           true	se il lato � chiuso
        //           false	se il lato non � chiuso
        //******************************************************************************
        public override bool IsClosed
        {
            get	{ return IsCircle; }
        }

        //******************************************************************************
        // IsCircle
        // Restituisce:
        //           true	se l'arco � una circonferenza completa
        //           false	se � un arco
        //******************************************************************************
        public override bool IsCircle
        {
            get	{ return ( MathUtil.Compare(Math.Abs(mBeta), 2d * Math.PI, MathUtil.RAD_EPSILON) == 0 ); }
        }

        //******************************************************************************
        // Function Surface
        // Restituisce l'area dell'arco (con segno)
        //******************************************************************************
        public override double Surface
        {
            get
            {
                double dAngle = Math.Abs(mBeta);
                if ( dAngle > Math.PI )
                    dAngle = Math.PI - dAngle;
                
                double area = (mR * mR) * (dAngle / 2d - Math.Sin(dAngle / 2d) * Math.Cos(dAngle / 2d));
                
                if ( Math.Abs(mBeta) > Math.PI )
                    area = mR * mR * Math.PI - area;
                
                //Segno dell'area
                return area * Direction;
            }
        }
        #endregion

        #region Operator overloading

        //******************************************************************************
        // Operatore -
        // Inverte la direzione dell'arco
        //******************************************************************************
        public static Arc operator - (Arc a)
        {
            return new Arc(a.CenterPoint, a.Radius, a.EndAngle, -a.AmplAngle);
        }
        #endregion

        #region Public Methods

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        //******************************************************************************
        // Equals
        // Verifica se due lati sono uguali (come valori)
        //******************************************************************************
        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType()) 
                return false;

            Arc a = (Arc)obj;
            if (AmplAngle != a.AmplAngle ||
                StartAngle != a.StartAngle ||
                Radius != a.Radius ||
                CenterPoint.X != a.CenterPoint.X ||
                CenterPoint.Y != a.CenterPoint.Y)
                return false;

            return true;
        }

        //******************************************************************************
        // AlmostEquals
        // Verifica se due archi sono uguali (come valori) a meno dell'epsilon di approssimazione
        //******************************************************************************
        public override bool AlmostEquals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Arc a = (Arc)obj;
            if (!MathUtil.AngleIsEQ(AmplAngle,a.AmplAngle) ||
                !MathUtil.AngleIsEQ(StartAngle, a.StartAngle) ||
                !MathUtil.QuoteIsEQ(Radius, a.Radius) ||
                !CenterPoint.AlmostEqual(a.CenterPoint))
                return false;

            return true;
        }
        public override bool AlmostEquals(Object obj, double delta)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Arc a = (Arc)obj;
            if (!StartPoint().AlmostEqual(a.StartPoint(), delta)||
                !CenterPoint.AlmostEqual(a.CenterPoint, delta) ||
                !EndPoint().AlmostEqual(a.EndPoint(), delta))
                return false;

            return true;
        }

        //******************************************************************************
        // GetCopy
        // Restituisce una copia del lato
        //******************************************************************************
        public override Side GetCopy()
        {
            return (Side) new Arc(this);
        }
        
        //******************************************************************************
        // RotoTrasl
        // Restituisce l'arco rototraslato
        // Parametri:
        //			rot	: angolo di rototraslazione (in radianti)
        //			xt	: traslazione in x
        //			yt	: traslazione in y
        //******************************************************************************
        public override Side RotoTrasl(RTMatrix matrix)
        {
            double x, y, rot;
            matrix.GetRotoTransl(out x, out y, out rot);
            matrix.PointTransform(mC.X, mC.Y, out x, out y);

            //Esegue la rototraslazione del centro
            Point c = new Point(x, y);

            //Ruota l'angolo di partenza
            double alpha = mAlpha + rot;
            if ( alpha > (2d * Math.PI) )
                alpha -= 2d * Math.PI;
            else if ( alpha < -(2d * Math.PI) )
                alpha += 2d * Math.PI;
            
            //Crea la copia dell'arco
            return (Side) new Arc(c, mR, alpha, mBeta);
        }

        //**************************************************************************
        // Normalize
        // Ritorna il lato normalizzato su un punto di offset
        // Parametri:
        //			offset	: punto di normalizzazione
        //**************************************************************************
        public override Side Normalize(Point offset)
        {
            //return (Side)RotoTrasl(0d, -offset.X, -offset.Y);
            return (Side)RotoTrasl(new RTMatrix(-offset.X, -offset.Y, 0d));
        }

        //**************************************************************************
        // Invert
        // Ritorna l'arco invertito
        //**************************************************************************
        public override Side Invert()
        {
            return (Side) new Arc(-this);
        }

        //**************************************************************************
        // Reverse
        // Inverte il lato corrente
        //**************************************************************************
        public override void Reverse()
        {
            //double tmp = StartAngle;
            //StartAngle = EndAngle;
            //EndAngle = tmp;

            StartAngle += AmplAngle;
            AmplAngle = -AmplAngle;
        }

        //**************************************************************************
        // StraightLine
        // Restituisce la retta definita dal lato
        // Parametri:
        //			a:	coefficiente variabile X
        //			b:	coefficiente variabile Y
        //			c:	coefficiente costante
        // Restituisce:
        //               true	retta calcolata
        //               false	retta non esistente
        //**************************************************************************
        public override bool StraightLine(out double a, out double b, out double c)
        {
            a = b = c = 0d;
            return false;
        }

        //**************************************************************************
        // OffsetSide
        // Restituisce un nuovo lato spostato perpendicolarmente di un offset 
        // rispetto al lato
        // Parametri:
        //			offset	: offset di spostamento
        //			verso	: verso di spostamento  > 0 sinistra (rot. CCW)
        //											< 0 destra (rot. CW)
        //**************************************************************************
        public override Side OffsetSide(double offset, int verso)
        {
            double r;

            //Calcola il raggio dell'arco, che va verso la sinistra del lato
            if(verso > 0d)
                //CCW
                if(Direction == 1)
                    r = mR - offset;

                    //CW
                else
                    r = mR + offset;

                //Calcola raggio dell'arco, che va verso la destra del lato
            else
                //CCW
                if(Direction == 1)
                r = mR + offset;
                //CW
            else
                r = mR - offset;
            
            //Verifica che il raggio rimanga positivo
            if(r < 0d)
                return null;
            
            //Crea il nuovo lato
            return (Side) new Arc(mC, r, mAlpha, mBeta);
        }

        //******************************************************************************
        // StartPoint
        // Calcola il punto iniziale dell'arco
        // Parametri:
        //			x	: ascissa punto iniziale
        //			y	: ordinata punto iniziale
        // Ritorna:
        //			punto iniziale
        //******************************************************************************
        public override Point StartPoint()
        {
            return CalcPoint(1, 0d);
        }

        //******************************************************************************
        // EndPoint
        // Calcola il punto finale dell'arco
        // Parametri:
        //			x	: ascissa punto finale
        //			y	: ordinata punto finale
        // Ritorna:
        //			punto finale
        //******************************************************************************
        public override Point EndPoint()
        {
            return CalcPoint(2, 0d);
        }
        
        //******************************************************************************
        // CalcPoint
        // Calcola un punto sull'arco
        // Parametri:
        //			tipo	: tipo angolo 0=assoluto, 1=relativo inizio, 2=relativo fine
        //			ang		: angolo
        // Ritorna:
        //			punto calcolato
        //			null se il punto non appartiene all'arco
        //******************************************************************************
        public override Point CalcPoint(int tipo, double ang)
        {
            switch (tipo)
            {
                    //Angolo assoluto
                case 0:
                    break;
                
                    //Angolo relativo all'angolo iniziale
                case 1:
                    ang += mAlpha;
                    break;
                
                    //Angolo relativo all'angolo finale
                case 2:
                    ang += mAlpha + mBeta;
                    break;   
            }
            
            //Calcolo punto
            double x = mC.X + mR * Math.Cos(ang);
            double y = mC.Y + mR * Math.Sin(ang);
            
            //Verifica se l'angolo appartiene all'arco, allora ritorna il punto
            //if (IsCircle || (mBeta > 0d && ang >= mAlpha && ang <= (mAlpha + mBeta)) || (mBeta < 0d && ang <= mAlpha && ang >= (mAlpha + mBeta)))
            if (
                 IsCircle || 
                 (mBeta > 0d && MathUtil.AngleIsGE(ang, mAlpha) && MathUtil.AngleIsLE(ang, (mAlpha + mBeta))) || 
                 (mBeta < 0d && MathUtil.AngleIsLE(ang, mAlpha) && MathUtil.AngleIsGE(ang, (mAlpha + mBeta)))
               )
                return new Point(x, y);

            // Altrimenti ritorna null
            return null;
        }
        
        //******************************************************************************
        // CalcAngPoint
        // Calcola l'angolo di un punto sull'arco
        // Parametri:
        //			tipo	: tipo angolo 0=assoluto, 1=relativo inizio, 2=relativo fine
        //			x		: ascissa punto
        //			y		: ordinata punto
        // Ritorna:
        //           angolo calcolato in radianti
        //******************************************************************************
        public override double CalcAngPoint(int tipo, double x, double y)
        {
            double ang=0d;

            double modulo = (y - mC.Y) / mR;
            if (modulo > 1)
                modulo = 1;
            else
                if (modulo < -1)
                    modulo = -1;
            double arcSin = Math.Asin(modulo);
            
            //Verifica il quadrante del punto
            bool diffXPos = (x - mC.X) >= 0d;
            bool diffYPos = (y - mC.Y) >= 0d;
            
            //Calcolo l'angolo positivo tra 0 e 2PIGRECO
            //Primo quadrante
            if ( diffXPos && diffYPos )
                ang = arcSin;
            
                //Secondo quadrante
            else if ( !diffXPos && diffYPos )
                ang = Math.PI - arcSin;

                //Terzo quadrante
            else if ( !diffXPos && !diffYPos )
                ang = Math.PI - arcSin;

                //Quarto quadrante
            else if ( diffXPos && !diffYPos )
                ang = 2d * Math.PI + arcSin;
            
            //Angolo relativo all'angolo iniziale
            double angRel = ang - mAlpha;
            
            //Orienta l'angolo secondo l'orientamento dell'arco
            if (MathUtil.Compare(angRel, 0d, MathUtil.RAD_EPSILON) == 0)
                angRel = 0d;

            else if (MathUtil.Compare(angRel, mBeta, MathUtil.RAD_EPSILON) == 0)
                angRel = mBeta;

            else if (angRel > 0d && Direction < 0)
                angRel -= 2d * Math.PI;

            else if (angRel < 0d && Direction > 0)
                angRel += 2d * Math.PI;
                
            
            switch (tipo)
            {
                    //Angolo assoluto
                case 0:
                    return mAlpha + angRel;
                
                    //Angolo relativo all'angolo iniziale
                case 1:
                    return angRel;
                
                    //Angolo relativo all'angolo finale
                case 2:
                    return angRel - mBeta;
                
                    //Altra scelta, angolo assoluto
                default:
                    return mAlpha + angRel;
                
            }
            
        }
        
        //***************************************************************************/
        //                                                                          */
        // Intersect:		   calcola il punto di intersezione, se esiste, tra		*/
        //                     due lati.		                                    */
        //                                                                          */
        // Inputs:   side  = lato con cui verificare le intersezioni				*/
        //           inter = vettore dei punti di intersezione trovati				*/
        //                                                                          */
        // Outputs: -1	= i lati sono sovrapposti.									*/
        //           0	= non ci sono intersezioni.                                 */
        //           >0	= numero di intersezioni trovate	                        */
        //                                                                          */
        //****************************************************************************/
        public override int Intersect(Side side, out Point[] inter)
        {
            int num = 0;

            if (side is Segment)
            {
                inter = new Point[2];
                num = Intersect((Segment) side, out inter[0], out inter[1]);
            }
            else if (side is Arc)
            {
                inter = new Point[2];
                num = Intersect((Arc) side, out inter[0], out inter[1]);
            }
            else
            {
                inter = null;
                num = 0;
            }

            return num;
        }
        public override int EpsIntersection(Side side, out Point[] inter, double eps = MathUtil.FLT_EPSILON)
        {
            int num = 0;

            if (side is Segment)
            {
                inter = new Point[2];
                num = Intersect((Segment)side, out inter[0], out inter[1]);
            }
            else if (side is Arc)
            {
                inter = new Point[2];
                num = Intersect((Arc)side, out inter[0], out inter[1]);
            }
            else
            {
                inter = null;
                num = 0;
            }

            return num;
        }

        /****************************************************************************
         * ContainPoint
         * Verifica se il punto specificato appartiene al'arco.
         * Parametri:
         *			p	: punto da controllare.
         * Ritorna:
         *			false	il punto non appartiene
         *			true	il punto appartiene
         ****************************************************************************/
        public override bool ContainPoint(Point p)
        {
            double   vector, mina, maxa;

            /* calcolo la direzione del vettore dal centro dell'arco al punto (x,y) */
            vector = mC.Direction(p);

            /* estremi dell'arco */
            if (mBeta > 0d) 
            {
                mina = mAlpha;
                maxa = mAlpha + mBeta;
            }
            else 
            {
                maxa = mAlpha;
                mina = mAlpha + mBeta;
            }
            if (mina < 0d)
            {
                mina += 2d * Math.PI;
                maxa += 2d * Math.PI;
            }

            /* confronto */
            if (maxa > 2d * Math.PI  &&  vector < mina)
                vector += 2d * Math.PI;

            return (MathUtil.IsLT(vector, mina) ||  MathUtil.IsGT(vector, maxa) ? false : true);
        }

        /****************************************************************************
        * ContainPointWithinTolerance
        * Verifica se il punto specificato appartiene al segmento, utilizzando i 
        * controlli con tolleranza estesa per gli angoli.
        * Parametri:
        *			p	: punto da controllare.
        * Ritorna:
        *			false	il punto non appartiene
        *			true	il punto appartiene
        ****************************************************************************/
        public override bool ContainPointWithinTolerance(Point p)
        {
            double vector, mina, maxa;

            /* calcolo la direzione del vettore dal centro dell'arco al punto (x,y) */
            vector = mC.Direction(p);

            /* estremi dell'arco */
            if (mBeta > 0d)
            {
                mina = mAlpha;
                maxa = mAlpha + mBeta;
            }
            else
            {
                maxa = mAlpha;
                mina = mAlpha + mBeta;
            }
            if (mina < 0d)
            {
                mina += 2d * Math.PI;
                maxa += 2d * Math.PI;
            }

            /* confronto */
            if (maxa > 2d * Math.PI && vector < mina)
                vector += 2d * Math.PI;

            return (MathUtil.AngleIsLT(vector, mina) || MathUtil.AngleIsGT(vector, maxa) ? false : true);
        }

        /// **************************************************************************
        /// <summary>
        /// Estensione dell'elemento verso rispetto ad uno dei due estremi
        /// </summary>
        /// <param name="which">veritice di riferimento</param>
        /// <param name="howmuch">entita' dello spostamento, verso l'interno se negativo </param>
        /// <returns>
        ///		true	oeprazione eseguita
        ///		false	operazione non possibile
        ///	</returns>
        /// **************************************************************************
        public override bool Extend(EN_VERTEX which, double howmuch)
        {

            // controllo se la lunghezza del segmento e' compatibile
            if (howmuch < 0 && Length <= Math.Abs(howmuch))
                return false;

            // indicatore di verso: 1 => antiorario, -1 => orario
            int sign = Math.Sign(mBeta);

            // in base al vertice da modificare
            if (which == EN_VERTEX.EN_VERTEX_START)
            {
                mAlpha = mAlpha - sign * howmuch / mR;
                mBeta = mBeta + sign * howmuch / mR;
            }
            else
                mBeta = mBeta + sign * howmuch / mR;

            return true;
        }
        
        /// **************************************************************************
        /// <summary>
        /// Estrazione di una porzione di lato, a partire dalle distanze dal primo
        /// veritice.
        /// </summary>
        /// <param name="from">distanza iniziale</param>
        /// <param name="to">distanza finale</param>
        /// <returns>
        ///		lato estratto (null se lato nullo)
        ///	</returns>
        /// **************************************************************************
        public override Side Extract(double from, double to)
        {
            if (from < 0)
                from = 0;
            if (to > Length)
                to = Length;
            if (from >= to)
                return null;

            double alpha = mAlpha + Math.Sign(mBeta) * from / mR;
            double beta = Math.Sign(mBeta) * (to-from) / mR;

            Arc a = new Arc(CenterPoint, Radius, alpha, beta);
            return a;
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna il punto che si trova alla distanza indicata dall'inziio
        /// del lato
        /// </summary>
        ///<param name="distance">
        ///     distanza alla quale cercare il punto
        ///</param>
        ///<returns>
        ///     punto trovato o null se la distanza e' incongruente
        ///</returns>
        /// **************************************************************************
        public override Point PointAtDistanceFromStart(double distance)
        {
            if (MathUtil.QuoteIsLT(distance, 0) || MathUtil.QuoteIsGT(distance,Length))
                return null;

            double angle = mAlpha + Math.Sign(mBeta) * distance / mR;

            return CenterPoint.PuntoDaModuloDirezione(Radius, angle);
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna la tangente seguendo l'orientazione del path sul punto che si trova alla distanza indicata dall'inziio
        /// del lato
        /// </summary>
        ///<param name="distance">
        ///     distanza alla quale calcolare la tangente dal punto di start del lato
        ///</param>
        ///<returns>
        ///     tangente seguente l'orientazione del lato o null se la distanza e' incongruente secondo MAthUtil.QuoteEpsilon
        ///</returns>
        /// **************************************************************************
        public override Vector TangentAtDistanceFromStart(double distance)
        {
            if (MathUtil.QuoteIsLT(distance, 0) || MathUtil.QuoteIsGT(distance,Length))
                return null;

            double lArc = Length;
            double tFromStart = distance / lArc;
            var tangent = TangentAt(tFromStart);
            return tangent;
        }

        /// **************************************************************************
        /// <summary>
        ///     Distanza lungo il side dal punto di start al punto <paramref name="point"/>
        /// </summary>
        /// <param name="point">
        ///     punto finale per il calcolo della lunghezza
        /// </param>
        /// <param name="distanceFromStart">
        ///     Distanza lungo il side dal punto di start al punto <paramref name="point"/>. punto fuori dall'intervallo, distante come angolo dal punto dell'origine pi� di circa 0.000, restituisce il valore dell'arco completo se � pi� vicino linearmente al punto finale, 0 se � pi� vicino al punto iniziale
        /// </param>
        /// <returns>
        ///     false se l'operazione non e' stata eseguita
        ///     true altrimenti
        /// </returns>
        /// **************************************************************************
        public override bool DistanceFromStart(Point point, out double distanceFromStart)
        {
            var pStart = StartPoint();
            var c = CenterPoint;
            var r = Radius;
            if (pStart != null &&
                c != null &&
                r > MathUtil.gdDBLEPSILON)
            {
                var vStart = (Breton.MathUtils.Vector)(pStart - c);
                var vPoint = (Breton.MathUtils.Vector)(point - c);
                if (Math.Abs(mBeta) < Math.PI)//caso generico, angolo sweep<180�
                {
                    if (vStart.Magnitude > MathUtil.gdDBLEPSILON &&
                        vPoint.Magnitude > MathUtil.gdDBLEPSILON)
                    {
                        var uStart = vStart.UnitVector;
                        var uPoint = vPoint.UnitVector;
                        var angle = Math.Acos(Math.Max(-1, Math.Min(1, Vector.ScalarProduct(uStart, uPoint))));
                        var arc = angle * r;
                        distanceFromStart = Math.Abs(arc);
                        return true;
                    }
                }
                else
                {
                    var pMid = MiddlePoint;
                    var pEnd = EndPoint();
                    var vMid = (Breton.MathUtils.Vector)(pMid - c);
                    var vEnd = (Breton.MathUtils.Vector)(pEnd - c);
                    if (vStart.Magnitude > MathUtil.gdDBLEPSILON &&
                        vPoint.Magnitude > MathUtil.gdDBLEPSILON)
                    {
                        var uStart = vStart.UnitVector;
                        var uQuarter = ((Breton.MathUtils.Vector)(PointAt(0.25) - c)).UnitVector;
                        var uMid = vMid.UnitVector;
                        var uEnd = vEnd.UnitVector;
                        var uPoint = vPoint.UnitVector;

                        var dotStartToPoint = Vector.ScalarProduct(uStart, uPoint);
                        var crossStartToPoint = uStart * uPoint;
                        
                        var crossStartToQuarter = uStart * uQuarter;

                        var dotMidToPoint = Vector.ScalarProduct(uMid, uPoint);
                        var crossMidToPoint = uMid * uPoint;

                        var crossEndToPoint = uEnd * uPoint;

                        if (dotStartToPoint > 1 - MathUtil.FLT_EPSILON)//start arc o start/end circle
                        {
                            if (IsCircle)//start/end circle
                            {
                                if (crossStartToPoint.Z * crossStartToQuarter.Z > 0)
                                {
                                    distanceFromStart = 0;
                                    return true;
                                }
                                else
                                {
                                    distanceFromStart = r * Math.Abs(mBeta);
                                    return true;
                                }
                            }
                            else//start arc
                            {
                                distanceFromStart = 0;
                                return true;
                            }
                        }
                        else if (dotMidToPoint > 1 - MathUtil.FLT_EPSILON) //mid circle o arc
                        {
                            distanceFromStart = r * Math.Abs(mBeta * 0.5);
                            return true;
                        }
                        else if (crossStartToPoint.Z * crossStartToQuarter.Z < 0 && crossStartToPoint.Z > 0 && crossEndToPoint.Z < 0)//fuori intervallo con centro->punto a CCW da direzione centro->start, arco CW
                        {
                            double distanceStart = point.Distance(pStart);
                            double distanceEnd = point.Distance(pEnd);
                            if (distanceStart < distanceEnd)
                            {
                                distanceFromStart = 0;
                                return true;
                            }
                            else
                            {
                                distanceFromStart = r * Math.Abs(mBeta);
                                return true;
                            }
                        }
                        else if (crossStartToPoint.Z * crossStartToQuarter.Z < 0 && crossStartToPoint.Z < 0 && crossEndToPoint.Z > 0)//fuori intervallo con centro->punto a CW da direzione centro->start, arco CCW
                        {
                            double distanceStart = point.Distance(pStart);
                            double distanceEnd = point.Distance(pEnd);
                            if (distanceStart < distanceEnd)
                            {
                                distanceFromStart = 0;
                                return true;
                            }
                            else
                            {
                                distanceFromStart = r * Math.Abs(mBeta);
                                return true;
                            }
                        }
                        else//caso generico, angolo sweep>180�
                        {
                            bool fromMid;
                            if (crossStartToPoint.Z > 0 && crossMidToPoint.Z < 0)
                            {
                                if (crossStartToQuarter.Z > 0)
                                    fromMid = false;//2//arco CCW(Da x a y)//p.es. 1�-134� su un arco di 270�
                                else
                                    fromMid = true;//1//arco CW(Da x a y)//p.es. 181-269� su un arco di 270�
                            }
                            else if (crossStartToPoint.Z < 0 && crossMidToPoint.Z > 0)
                            {
                                if (crossStartToQuarter.Z < 0)
                                    fromMid = false;//2//arco CW(Da x a y)
                                else
                                    fromMid = true;//1//arco CCW(Da x a y)
                            }
                            else if (crossStartToPoint.Z > 0 && crossMidToPoint.Z > 0)
                            {
                                fromMid = true;//3//arco CCWp.es. 136�-179� su un arco di 270�; quando si va ad angolo >180� si ha crossStartToPoint.Z < 0
                            }
                            else if (crossStartToPoint.Z < 0 && crossMidToPoint.Z < 0)
                            {
                                fromMid = true;//3//arco CW
                            }
                            else//errore, bisognerebbe gestirlo con casi dot
                            {
                                fromMid = false;
                            }

                            Vector uHalvedStart;
                            double angleForHalvedSituation;
                            if (fromMid)
                            {
                                uHalvedStart = uMid;
                                angleForHalvedSituation = Math.Abs(mBeta * 0.5);
                            }
                            else
                            {
                                uHalvedStart = uStart;
                                angleForHalvedSituation = 0;
                            }
                            var angleFromHS = Math.Acos(Math.Max(-1, Math.Min(1, Vector.ScalarProduct(uHalvedStart, uPoint))));
                            var arc = Math.Abs(angleForHalvedSituation + angleFromHS) * r;
                            distanceFromStart = arc;
                            return true;
                        }
                    }
                }
            }
            distanceFromStart = 0;
            return false;
        }

        /// **************************************************************************
        /// <summary>
        /// Verifica se il lato corrente contiene completamente quello indicato
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato non e' completamente contenuto
        ///     true    il lato indicato e' completamente contenuto.
        ///</returns>
        /// **************************************************************************
        public override bool Contains(Side sd, double maxdist)
        {
            if (!(sd is Arc))
                return false;

            Arc a = (Arc)sd;

            double dist = Math.Abs(Radius - a.Radius);
            if (dist > maxdist)
                return false;

            if (!CenterPoint.AlmostEqual(a.CenterPoint,maxdist))
                return false;

            Segment s = new Segment(a.CenterPoint, a.StartPoint());
            s.Extend(EN_VERTEX.EN_VERTEX_END, 10 * maxdist);
            Point [] ints;
            if (Intersect( s, out ints) == 0)
                return false;

            s = new Segment(a.CenterPoint, a.EndPoint());
            s.Extend(EN_VERTEX.EN_VERTEX_END, 10 * maxdist);
            Point[] inte;
            if (Intersect(s, out inte) == 0)
                return false;

            s = new Segment(a.CenterPoint, a.MiddlePoint);
            s.Extend(EN_VERTEX.EN_VERTEX_END, 10 * maxdist);
            Point[] intm;
            if (Intersect(s, out intm) == 0)
                return false;

            if (!ContainPoint(intm[0]))
                return false;

            if (!ContainPoint(ints[0]))
                return false;

            if (!ContainPoint(inte[0]))
                return false;

            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Verifica se il lato indicato e' uguale a quello corrente, considerando
        /// un parametro di distanza massima
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>
        /// **************************************************************************
        public override bool IsEqual(Side sd, double maxdist)
        {
            if (!(sd is Arc))
                return false;

            Arc a = (Arc)sd;

            double pdist = maxdist * Math.Sqrt(2); // /2.0;

            double dist = Math.Abs(Radius - a.Radius);
            if (MathUtil.QuoteIsGT(dist, maxdist))
                return false;

            if (!CenterPoint.AlmostEqual(a.CenterPoint,maxdist))
                return false;

            if (Math.Sign(AmplAngle) == Math.Sign(a.AmplAngle))
            {
                dist = StartPoint().Distance(a.StartPoint());
                if (dist > pdist)
                    return false;

                dist = EndPoint().Distance(a.EndPoint());
                if (dist > pdist)
                    return false;
            }
            else
            {
                dist = StartPoint().Distance(a.EndPoint());
                if (dist > pdist)
                    return false;

                dist = EndPoint().Distance(a.EndPoint());
                if (dist > pdist)
                    return false;
            }

            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Se i due lati sono omogenei e connessi, ritorna il lato somma dei due.
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>
        /// **************************************************************************
        public override Side Connect(Side sd, double maxdist)
        {
            if (!(sd is Arc))
                return null;

            Side osd = base.Connect(sd, maxdist);
            if (osd != null)
                return osd;

            Arc a = (Arc)sd;

            double dist = Math.Abs(Radius - a.Radius);
            if (dist > maxdist)
                return null;

            if (!CenterPoint.AlmostEqual(a.CenterPoint, maxdist))
                return null;

            if (P1.Distance(a.P1) < maxdist)
            {
                Arc af = new Arc(a.CenterPoint, a.Radius, StartAngle + AmplAngle, AmplAngle - a.AmplAngle);
                return af;
            }
            if (P1.Distance(a.P2) < maxdist)
            {
                Arc af = new Arc(a.CenterPoint, a.Radius, a.StartAngle, AmplAngle + a.AmplAngle);
                return af;
            }
            if (P2.Distance(a.P1) < maxdist)
            {
                Arc af = new Arc(a.CenterPoint, a.Radius, StartAngle, AmplAngle + a.AmplAngle);
                return af;
            }
            if (P2.Distance(a.P2) < maxdist)
            {
                Arc af = new Arc(a.CenterPoint, a.Radius, StartAngle, AmplAngle - a.AmplAngle);
                return af;
            }

            return  null;
        }

        //******************************************************************************
        // InternBrokenLines
        // Calcola la spezzata che approssima internamente l'arco
        // Parametri:
        //			epsilon	: errore cordale desiderato
        // Ritorna:
        //           vettore contenente i punti della spezzata
        //           null altrimenti
        //******************************************************************************
        public Point[] InternBrokenLines(double epsilon)
        {
            //Calcola l'angolo che assicura l'errore cordale richiesto
            double angStep = Math.Acos((mR - epsilon) / mR) * 2d;
            if ( angStep == 0d )
                return null;
            
            //Calcola quante volte ci sta l'angolo dentro all'arco,
            //in modo da avere tutti gli angoli uguali
            double ang = Math.Abs(mBeta);
            
            int n = 0;
            while (ang > 0d)
            {
                ang -= angStep;
                n++;
            }
            
            //Inizializza il vettore dei punti
            Point[] points = new Point[n+1];
            
            //Crea tutti i punti della spezzata
            for (int i = 0; i <= n; i++)
                points[i] = CalcPoint(0, mAlpha + mBeta * i / n);
            
            return points;
        }

        //******************************************************************************
        // InternBrokenLines
        // Calcola la spezzata che approssima esternamente l'arco
        // Parametri:
        //			epsilon	: errore cordale desiderato
        // Ritorna:
        //           vettore contenente i punti della spezzata
        //           null altrimenti
        //******************************************************************************
        public Point[] ExternBrokenLines(double epsilon)
        {
            //Calcola l'angolo che assicura l'errore cordale richiesto
            double angStep = Math.Acos(mR / (mR + epsilon)) * 2d;

            if ( angStep == 0d ) 
                return null;
            
            //Calcola quante volte ci sta l'angolo dentro all'arco
            //in modo da avere tutti gli angoli uguali
            double ang = Math.Abs(mBeta);

            int n = 0;
            while (ang > 0d)
            {
                ang -= angStep;
                n++;
            }
            
            //Ricalcola l'errore
            epsilon = mR / Math.Cos(Math.Abs(mBeta) / (2 * n)) - mR;

            Arc a = new Arc(mC, mR + epsilon, mAlpha, 2 * Math.PI);
            
            //Inizializza il vettore dei punti
            Point[] points = new Point[n+2];
            int i = 0;
            
            //Crea tutti i punti della spezzata
            for (int j = 0; j <= n; j++)
            {
                Point p1 = a.CalcPoint(0, mAlpha + mBeta * (2d * j - 1d) / (2d * n));
                Point p2 = a.CalcPoint(0, mAlpha + mBeta * (2d * j + 1d) / (2d * n));
                
                if ( j == 0 )
                    points[i++] = new Point((p1.X + p2.X) / 2d, (p1.Y + p2.Y) / 2d);
                
                else if ( j == n )
                {
                    points[i++] = new Point(p1);
                    points[i++] = new Point((p1.X + p2.X) / 2d, (p1.Y + p2.Y) / 2d);
                }   
                else
                    points[i++] = new Point(p1);
            }
            
            return points;
        }

        //**************************************************************************
        // ReadFileXml
        // Legge il lato dal file XML
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public override bool ReadFileXml(XmlNode n)
        {

            if (!ReadDerivedFileXml(n))
                return false;

            try
            {
                mC.X = Convert.ToDouble(n.Attributes.GetNamedItem("Xc").Value, CultureInfo.InvariantCulture);
                mC.Y = Convert.ToDouble(n.Attributes.GetNamedItem("Yc").Value, CultureInfo.InvariantCulture);
                mR = Convert.ToDouble(n.Attributes.GetNamedItem("R").Value, CultureInfo.InvariantCulture);
                mAlpha = Convert.ToDouble(n.Attributes.GetNamedItem("Alpha").Value, CultureInfo.InvariantCulture);
                mBeta = Convert.ToDouble(n.Attributes.GetNamedItem("Beta").Value, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Arc.ReadFileXml Error.", ex);
                return false;
            }
        }

        //**************************************************************************
        // WriteFileXml
        // Scrive il lato su file XML
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("Arc");

            w.WriteAttributeString("Xc", mC.X.ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("Yc", mC.Y.ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("R", mR.ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("Alpha", mAlpha.ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("Beta", mBeta.ToString(CultureInfo.InvariantCulture));

            WriteDerivedFileXml(w);

            w.WriteEndElement();
        }

        ///**************************************************************************
        /// <summary>
        /// Scrive il lato sul nodo XML
        /// </summary>
        /// <param name="baseNode">nodo XML dove scrivere il lato</param>
        ///**************************************************************************
        public override void WriteFileXml(XmlNode baseNode)
        {
            XmlDocument doc = baseNode.OwnerDocument;
            XmlAttribute attr;
            XmlNode node = doc.CreateNode(XmlNodeType.Element, "Arc", "");

            attr = doc.CreateAttribute("Xc");
            attr.Value = mC.X.ToString(CultureInfo.InvariantCulture);
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("Yc");
            attr.Value = mC.Y.ToString(CultureInfo.InvariantCulture);
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("R");
            attr.Value = mR.ToString(CultureInfo.InvariantCulture);
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("Alpha");
            attr.Value = mAlpha.ToString(CultureInfo.InvariantCulture);
            node.Attributes.Append(attr);
            attr = doc.CreateAttribute("Beta");
            attr.Value = mBeta.ToString(CultureInfo.InvariantCulture);
            node.Attributes.Append(attr);

            WriteDerivedFileXml(node);

            baseNode.AppendChild(node);
        }

        ///**************************************************************************
        /// <summary>
        ///     Nodo side del salvataggio di geometria in FKSQL, avente solo le informazioni di Breton.Polygons, con nome elemento=Side
        /// </summary>
        ///**************************************************************************
        public override void WriteSideXml(XmlNode baseNode)
        {
            XmlNode xSide = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Side", "");

            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Xc", "");
                node.InnerText = CenterPoint.X.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }
            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Yc", "");
                node.InnerText = CenterPoint.Y.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }
            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Radius", "");
                node.InnerText = Radius.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }
            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Alpha", "");
                node.InnerText = StartAngle.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }
            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Beta", "");
                node.InnerText = AmplAngle.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }

            baseNode.AppendChild(xSide);
        }

        /// **************************************************************************
        /// <summary>
        ///     connessione del lato corrente con il successivo in base allla distanza
        ///     indicata.
        /// </summary>
        ///<param name="nextsd">
        ///     lato successivo
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     eventuale lato da aggiungere per completare la connessione
        ///</returns>
        /// **************************************************************************
        public override Side ConnectSides(Side nextsd, double maxdist)
        {
            if (nextsd is Segment)
            {
                double dist = P2.Distance(nextsd.P1);
                if (dist <= maxdist)
                    return null;

                Point pThis = P2;
                Point pNext = nextsd.P1;
                Point pMid = (pThis + pNext) * 0.5;
                Point[] inters;
                var current = this as Arc;
                var next = nextsd as Segment;
                int n_inters = new Arc(current.CenterPoint, current.Radius, 0, Math.PI * 2).Intersect(new Segment(next.P1 - next.Tangent() * 1000, next.P2 + next.Tangent() * 1000), out inters);
                Point pInters = null;
                if (n_inters > 0 && inters != null && !current.IsCircle)
                {
                    if (n_inters == 1 && (inters[0] != null || inters[1] != null))
                    {
                        if (inters[0] != null)
                            pInters = inters[0];
                        else if (inters[1] != null)
                            pInters = inters[1];
                    }
                    else if (inters[0] != null && inters[1] != null)
                    {
                        if (inters[0].Distance(pMid) < inters[1].Distance(pMid))
                            pInters = inters[0];
                        else
                            pInters = inters[1];
                    }
                }
                if (pInters != null && current.Length > 1 && next.Length > 1 && current.P2.Distance(pInters) < 1)
                {
                    //20160624 provo a modificare le due curve in base al punto di intersezione ottenuto estendendo le due curve non connesse
                    var p0 = current.P1;
                    var p1 = current.MiddlePoint;
                    var p2 = pInters;
                    current.InitArc(p0.X, p0.Y, p1.X, p1.Y, p2.X, p2.Y);

                    next.P1.X = pInters.X;
                    next.P1.Y = pInters.Y;
                }
                else
                {
                    nextsd.P1.X = P2.X;
                    nextsd.P1.Y = P2.Y;
                }
                return null;
            }

            if (nextsd is Arc)
            {
                double dist = P2.Distance(nextsd.P1);
                if (dist <= maxdist)
                    return null;

                Arc na = (Arc)nextsd;
                Point ip1, ip2;
                int ni = na.Intersect(this, out ip1, out ip2);
                if (ni > 0)
                {
                    if (ni > 1)
                    {
                        double d1 = P2.Distance(ip1);
                        double d2 = P2.Distance(ip2);
                        if (d2 < d1)
                            ip1 = ip2;
                    }

                    ////20170202//gestione modifica con punto di intersezione che va a posizionarsi nell'intervallo angolo start � MathUtil.FLT_EPSILON crea un nuovo arco che da valore di ampiezza 0.008 radianti diventava circa 6.28 radianti. Caso trovato in ConnectSides di archi per creazione foretti di distacco automatico con due archi di circonferenza molto elevata con centro da stesso lato che dovevano approssimare un singolo segmento
                    //double dir = CenterPoint.Direction(ip1);
                    //if (ContainPoint(ip1))
                    //{
                    //    double new_amp;
                    //    if (AmplAngle > 0)
                    //        new_amp = AmplAngle - MathUtil.Angolo_0_2PI(EndAngle - dir);
                    //    else
                    //        new_amp = AmplAngle + MathUtil.Angolo_0_2PI(dir - EndAngle);
                    //    AmplAngle = new_amp;
                    //}
                    //else
                    //{
                    //    double new_amp;
                    //    if (AmplAngle > 0)
                    //        new_amp = AmplAngle + MathUtil.Angolo_0_2PI(dir - EndAngle);
                    //    else
                    //        new_amp = AmplAngle - MathUtil.Angolo_0_2PI(EndAngle - dir);
                    //    AmplAngle = new_amp;
                    //}

                    //dir = na.CenterPoint.Direction(ip1);
                    //if (na.ContainPoint(ip1))
                    //{
                    //    double new_amp;
                    //    if (na.AmplAngle > 0)
                    //        new_amp = na.AmplAngle - MathUtil.Angolo_0_2PI(dir - na.StartAngle);
                    //    else
                    //        new_amp = na.AmplAngle + MathUtil.Angolo_0_2PI(na.StartAngle - dir);
                    //    na.AmplAngle = new_amp;
                    //    na.StartAngle = dir;
                    //}
                    //else
                    //{
                    //    double new_amp;
                    //    if (na.AmplAngle > 0)
                    //        new_amp = AmplAngle + MathUtil.Angolo_0_2PI(na.StartAngle - dir);
                    //    else
                    //        new_amp = AmplAngle - MathUtil.Angolo_0_2PI(dir - na.StartAngle);
                    //    na.AmplAngle = new_amp;
                    //    na.StartAngle = dir;
                    //}

                    ConnectSidesMethods.ConnectSides_ChangeArcAmplitudeFromNewEstremity(this, ip1);//20170202//
                    ConnectSidesMethods.ConnectSides_ChangeArcAmplitudeFromNewEstremity(na, ip1);//20170202//

                    return null;
                }

                Point pThis = P2;
                Point pNext = nextsd.P1;
                Point pMid = (pThis + pNext) * 0.5;
                Point[] inters;
                var current = this as Arc;
                var next = nextsd as Arc;
                int n_inters = new Arc(current.CenterPoint, current.Radius, 0, Math.PI * 2).Intersect(new Arc(next.CenterPoint, next.Radius, 0, Math.PI * 2), out inters);
                Point pInters = null;
                if (n_inters > 0 && inters != null && !current.IsCircle && !next.IsCircle)
                {
                    if (n_inters == 1 && (inters[0] != null || inters[1] != null))
                    {
                        if (inters[0] != null)
                            pInters = inters[0];
                        else if (inters[1] != null)
                            pInters = inters[1];
                    }
                    else if (inters[0] != null && inters[1] != null)
                    {
                        if (inters[0].Distance(pMid) < inters[1].Distance(pMid))
                            pInters = inters[0];
                        else
                            pInters = inters[1];
                    }
                }
                if (pInters != null && current.Length > 1 && next.Length > 1 && current.P2.Distance(pInters) < 1)
                {
                    //20160624 provo a modificare le due curve in base al punto di intersezione ottenuto estendendo le due curve non connesse
                    var p0 = current.P1;
                    var p1 = current.MiddlePoint;
                    var p2 = pInters;
                    //current.InitArc(p0.X, p0.Y, p1.X, p1.Y, p2.X, p2.Y);//20170202//
                    ArcFrom3PointsMethods.InitArcConsideringDegenerates(current, p0, p1, p2);

                    p0 = pInters;
                    p1 = next.MiddlePoint;
                    p2 = next.P2;
                    //next.InitArc(p0.X, p0.Y, p1.X, p1.Y, p2.X, p2.Y);//20170202//
                    ArcFrom3PointsMethods.InitArcConsideringDegenerates(next, p0, p1, p2);

                    return null;
                }
                else
                {
                    Segment s = new Segment(P2, nextsd.P1);
                    return s;
                }
            }

            return null;
        }
        
        /// **************************************************************************
        /// <summary>
        ///     verficia se i due lati sono omogenei, cioe' sono paralleli.
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>        
        /// **************************************************************************
        public override bool Homogeneous(Side sd, double maxdist)
        {
            if (!(sd is Arc))
                return false;

            Arc a = (Arc)sd;

            double dist = Math.Abs(Radius - a.Radius);
            if (dist > maxdist)
                return false;

            if (!CenterPoint.AlmostEqual(a.CenterPoint, maxdist))
                return false;

            return true;
        }

        /// **************************************************************************
        /// <summary>
        ///     Determina il punto dell'entita' corrente che si trova alla minima 
        ///     distanza rispetto il punto indicato
        /// </summary>
        /// <param name="ref_point">
        ///     punto rispetto il quale determinare la distanza
        /// </param>
        /// <param name="mindist_point">
        ///     punto a minima distanza indentificato nella entita'
        /// </param>
        /// <returns>
        ///     minima distanza calcolata
        /// </returns>
        /// **************************************************************************
        public override double MinDistanceToPoint(Point ref_point, out Point mindist_point)
        {
            mindist_point = null;

            double dir = CenterPoint.Direction(ref_point);
            mindist_point = CenterPoint.PuntoDaModuloDirezione(Radius, dir);

            if (ContainPoint(mindist_point))
                return mindist_point.Distance(ref_point);

            double dp1 = P1.Distance(ref_point);
            double dp2 = P2.Distance(ref_point);
            if (dp1 < dp2)
            {
                mindist_point = P1;
                return dp1;
            }
            mindist_point = P2;
            return dp2;
        }

        /// **************************************************************************
        /// <summary>
        ///     Determina le due parti risultati dall'interruzione dell'entita' corrente
        ///     sul punto indicato
        /// </summary>
        /// <param name="break_point">
        ///     punto sul quale eseguire l'interruzione
        /// </param>
        /// <param name="sd1">
        ///     prima parte dell'elemento
        /// </param>
        /// <param name="sd2">
        ///     seconda parte dell'elmento
        /// </param>
        /// <returns>
        ///     false se l'operazione non e' stata eseguita
        ///     true altrimenti
        /// </returns>
        /// **************************************************************************
        public override bool BreakAtPoint(Point break_point, out Side sd1, out Side sd2)
        {
            sd1 = null;
            sd2 = null;

            if (ContainPoint(break_point) == false)
                return false;

            Arc ca1 = (Arc)GetCopy();
            Arc ca2 = (Arc)GetCopy();

            double ang = CalcAngPoint(1, break_point);
            ca1.AmplAngle = ang;
            ca2.StartAngle = StartAngle + ang;
            ca2.AmplAngle = ca2.AmplAngle - ang;

            sd1 = ca1;
            sd2 = ca2;

            return true;
        }

        /// ********************************************************************
        /// <summary>
        /// Trasforma il side, scala data da lunghezza asse x trasformato
        /// </summary>
        /// <returns>transform != null</returns>
        /// ********************************************************************
        public override bool Transform(Breton.MathUtils.RTMatrix transform)
        {
            if (transform != null)
            {
                var a = this;
                var mC = a.CenterPoint;
                double xC, yC, mR;
                transform.PointTransform(mC.X, mC.Y, out xC, out yC);
                a.CenterPoint.X = xC;
                a.CenterPoint.Y = yC;

                double x1,y1,x2, y2;
                transform.PointTransform(0, 0, out x1, out y1);
                transform.PointTransform(1, 0, out x2, out y2);
                double dx = x2-x1;
                double dy = y2-y1;
                double rotation = Math.Atan2(dy, dx);
                double newStartAngle = a.StartAngle + rotation;
                double newStartAngle_0_2PI = Breton.MathUtils.MathUtil.Angolo_0_2PI(newStartAngle);
                a.StartAngle = newStartAngle_0_2PI;

                Segment s = new Segment(new Point(0, 0), new Point(1, 0));
                s.Transform(transform);
                mR = a.Radius * s.Length;
                a.Radius = mR;

                return true;
            }

            return false;
        }

        /// ********************************************************************
        /// <summary>
        /// Tangente sul primo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sul primo punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        public override Vector Tangent()//direzione 1 -> 2
        {
            Vector v;
            Point p1 = P1;

            //Crea il vettore parallelo al raggio nel vertice 1
            Vector r = new Vector(p1.X, p1.Y, mC.X, mC.Y);

            if (Direction == 1) //Calcola il vettore perpendicolare al raggio, che va verso la sinistra dell'arco
                v = new Vector(-r.Y, r.X);
            else //Calcola il vettore perpendicolare al raggio, che va verso la destra dell'arco
                v = new Vector(r.Y, -r.X);

            //Imposta la lunghezza del vettore perpendicolare pari al raggio
            return new Vector(v, 1);
        }
        public Vector DirToRadius()
        {
            var dir = (Vector)(this.CenterPoint - StartPoint());
            if (dir.Magnitude > MathUtil.gdDBLEPSILON)
                return dir.UnitVector;
            else
                return null;
        }

        /// ********************************************************************
        /// <summary>
        /// Tangente sull'ultimo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sull'ultimo punto punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        public override Breton.MathUtils.Vector TangentAtEnd()//direzione 1 -> 2
        {
            Vector v;
            Point p2 = P2;

            //Crea il vettore parallelo al raggio nel vertice 2
            Vector r = new Vector(p2.X, p2.Y, mC.X, mC.Y);

            if (Direction == 1) //Calcola il vettore perpendicolare al raggio, che va verso la sinistra dell'arco
                v = new Vector(-r.Y, r.X);
            else //Calcola il vettore perpendicolare al raggio, che va verso la destra dell'arco
                v = new Vector(r.Y, -r.X);

            //Imposta la lunghezza del vettore perpendicolare pari al raggio
            return new Vector(v, 1);
        }
        public Vector DirEndToRadius()
        {
            var dir = (Vector)(this.CenterPoint - EndPoint());
            if (dir.Magnitude > MathUtil.gdDBLEPSILON)
                return dir.UnitVector;
            else
                return null;
        }

        /// ********************************************************************
        /// <summary>
        /// Tangente su coordinata relativa 0 ;&lt t ;&lt 1
        /// </summary>
        /// <returns>tangente su punto definito da 0 ;&lt t ;&lt 1</returns>
        /// ********************************************************************
        public override Breton.MathUtils.Vector TangentAt(double t) //direzione 1 -> 2
        {
            double tRel = Math.Max(0, Math.Min(1, t));
            double ang = StartAngle + AmplAngle * tRel;

            double c = Math.Cos(ang);
            double s = Math.Sin(ang);

            Vector v;

            //Calcola il vettore perpendicolare al raggio, che va verso la sinistra dell'arco
            if (Direction == 1)
                v = new Vector(-s, c);
            //Calcola il vettore perpendicolare al raggio, che va verso la destra dell'arco
            else
                v = new Vector(s, -c);

            //Imposta la lunghezza del vettore perpendicolare pari al raggio
            return v;
        }

        /// ********************************************************************
        /// <summary>
        /// Tangente dell'arco passante per il punto p
        /// </summary>
        /// <param name="x">x dove valutare la tangente</param>
        /// <param name="y">y dove valutare la tangente</param>
        /// <returns>tangente dell'arco passante per il punto p</returns>
        /// ********************************************************************
        public override Vector TangentAtPoint(double x, double y)
        {
            Vector v;
            Point p1 = new Point(x, y);

            //Crea il vettore parallelo al raggio nel vertice 1
            Vector r = new Vector(p1.X, p1.Y, mC.X, mC.Y);

            //Calcola il vettore perpendicolare al raggio, che va verso la sinistra dell'arco
            if (Direction == 1)
                v = new Vector(-r.Y, r.X);
                //Calcola il vettore perpendicolare al raggio, che va verso la destra dell'arco
            else
                v = new Vector(r.Y, -r.X);

            //Imposta la lunghezza del vettore perpendicolare pari al raggio
            return new Vector(v, 1);
        }

        /// ********************************************************************
        /// <summary>
        /// Punto a distanza relativa 0-1
        /// </summary>
        /// <param name="t">distanza relativa 0-1</param>
        /// <returns>punto a distanza relativa 0-1</returns>
        /// ********************************************************************
        public Breton.Polygons.Point PointAt(double t)
        {
            double posAngRad = mAlpha + mBeta * t;
            
            //Calcolo punto
            double x = mC.X + mR * Math.Cos(posAngRad);
            double y = mC.Y + mR * Math.Sin(posAngRad);
            Breton.Polygons.Point p = new Breton.Polygons.Point(x, y);

            return p;
        }

        /// <summary>
        /// Calcola il circocentro del triangolo formato da p1, p2 e p3
        /// ATTENZIONE: i punti non devono essere collineari!
        /// </summary>
        public static Point GetCircumcenter(Point p1, Point p2, Point p3)
        {
            //Funzione che utilizza la regola di Cramer per calcolare il circocentro di un triangolo
            // per la spiegazione vedere https://www.ics.uci.edu/~eppstein/junkyard/circumcenter.html
            // un'altro sistema valido � il calcolo dell'intersezione delle rette ortogonali passanti per il punto medio di una coppia di segmenti formati dai punti
            double axcx = p1.X - p3.X;
            double aycy = p1.Y - p3.Y;
            double bycy = p2.Y - p3.Y;
            double bxcx = p2.X - p3.X;
            double d = axcx * bycy - bxcx * aycy;
            double a1 = (axcx * (p1.X + p3.X) + aycy * (p1.Y + p3.Y)) / 2d;
            double a2 = (bxcx * (p2.X + p3.X) + bycy * (p2.Y + p3.Y)) / 2d;

            return new Point((a1 * bycy - a2 * aycy) / d, (a2 * axcx - a1 * bxcx) / d);
        }

        public static Breton.Polygons.Point ProjectToArc(Arc arc, Breton.Polygons.Point pointToProject)
        {
            var pStart = arc.StartPoint();
            var pEnd = arc.EndPoint();
            var arcRadius = arc.Radius;
            var arcLength = arc.Length;

            if (arcRadius <= MathUtil.gdDBLEPSILON)
                return pStart;

            if (Math.Abs(arcLength) <= MathUtil.FLT_EPSILON)
                return pStart;
            
            var arcCenter = arc.CenterPoint;

            var pPointToProject = pointToProject;
            var vPointToProjectToCenter = (Breton.MathUtils.Vector)(arcCenter - pPointToProject);
            var distancePointToProjectToCenter = vPointToProjectToCenter.Magnitude;
            if (distancePointToProjectToCenter < MathUtil.FLT_EPSILON)
                return pStart;
            var dPointToProjectToCenter = vPointToProjectToCenter * (1 / distancePointToProjectToCenter);

            var arcTangentStart  = arc.Tangent();
            var arcOrthoStart = new Breton.MathUtils.Vector(-arcTangentStart.Y, arcTangentStart.X);
            var arcTangentEnd = arc.TangentAtEnd();
            var arcOrthoEnd = new Breton.MathUtils.Vector(-arcTangentEnd.Y, arcTangentEnd.X);

            Breton.Polygons.Point nearestPoint = null;

            var isCircle = arc.IsCircle;
            if (isCircle)
            {
                nearestPoint = arcCenter - dPointToProjectToCenter * arcRadius;
            }
            else
            {
                var distancePointToProjectToStart = pPointToProject.Distance(pStart);
                if (distancePointToProjectToStart < MathUtil.FLT_EPSILON)
                    return pStart;

                var distancePointToProjectToEnd = pPointToProject.Distance(pEnd);
                if (distancePointToProjectToEnd < MathUtil.FLT_EPSILON)
                    return pEnd;

                var dpStart = Breton.MathUtils.Vector.ScalarProduct(arcOrthoStart, dPointToProjectToCenter);
                var arcOrthoStart_dPointToProjectToCenter = arcOrthoStart * dPointToProjectToCenter;
                var cpStart = arcOrthoStart_dPointToProjectToCenter.Z;

                var dpEnd = Breton.MathUtils.Vector.ScalarProduct(arcOrthoEnd, dPointToProjectToCenter);
                var arcOrthoEnd_dPointToProjectToCenter = arcOrthoEnd * dPointToProjectToCenter;
                var cpEnd = arcOrthoEnd_dPointToProjectToCenter.Z;

                var eps = MathUtil.FLT_EPSILON;

                if(Math.Abs(cpStart) < eps && dpStart > eps)//punto su raggio start
                {
                    nearestPoint = pStart;
                }
                else if (Math.Abs(cpEnd) < eps && dpEnd > eps)//punto su raggio end
                {
                    nearestPoint = pEnd;
                }
                else if(cpStart > eps && cpEnd < -eps)//punto su raggi compresi dall'arco
                {
                    nearestPoint = arcCenter - dPointToProjectToCenter * arcRadius;
                }
                else
                {
                    if (distancePointToProjectToStart < distancePointToProjectToEnd)
                        nearestPoint = pStart;
                    else
                        nearestPoint = pEnd;
                }
            }

            return nearestPoint;
        }

        public override Breton.Polygons.Point GetNearestPoint(Breton.Polygons.Point pointToProject)
        {
            return Arc.ProjectToArc(this, pointToProject);
        }
        public override Breton.Polygons.Side GetSubSide(double lStart, double lEnd)
        {
            var arcCenter = mC;
            var arcTotalStart = mAlpha;
            var arcTotalAmplitude = mBeta;
            var arcOrientation = arcTotalAmplitude != 0 ? Math.Sign(arcTotalAmplitude) : 1;
            var arcRadius = Radius;
            var arcLength = Length;
            if (lStart < 0)
                lStart = 0;
            if (lEnd > arcLength)
                lEnd = arcLength;
            var subSideLengthToStart = lStart / arcRadius;
            var subSideLengthNewSweep = (lEnd - lStart) / arcRadius;
            var start = arcTotalStart + (subSideLengthToStart * arcOrientation);
            var sweep = subSideLengthNewSweep * arcOrientation;
            return new Breton.Polygons.Arc(arcCenter, arcRadius, start, sweep);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calcoli di inizializzazione dell'arco: Arco per tre punti
        /// </summary>
        /// <param name="x0">x primo punto</param>
        /// <param name="y0">y primo punto</param>
        /// <param name="x1">x secondo punto</param>
        /// <param name="y1">y secondo punto</param>
        /// <param name="x2">x terzo punto</param>
        /// <param name="x2">y terzo punto</param>
        public string InitArc(double x0, double y0, double x1, double y1, double x2, double y2)
        {
            return ArcFrom3PointsMethods.InitArcQuoteEpsilon(this, x0, y0, x1, y1, x2, y2);
            //Point p0 = new Point(x0, y0);
            //Point p1 = new Point(x1, y1);
            //Point p2 = new Point(x2, y2);
            //Segment sA = new Segment(p0, p1);
            //double lA = sA.Length;
            //Segment sB = new Segment(p1, p2);
            //double lB = sB.Length;

            //Vector dA = new Vector(x1 - x0, y1 - y0);
            //if (lA < MathUtil.QUOTE_EPSILON)
            //{
            //    Radius = -1;
            //    return "first 2 points are nearer than " + MathUtil.QUOTE_EPSILON;
            //}
            //dA = dA.UnitVector;

            //Vector dB = new Vector(x2 - x1, y2 - y1);
            //if (lB < MathUtil.QUOTE_EPSILON)
            //{
            //    Radius = -1;
            //    return "last 2 points are nearer than " + MathUtil.QUOTE_EPSILON;
            //}
            //dB = dB.UnitVector;

            //Vector oA = new Vector(-dA.Y, dA.X);
            //Vector oB = new Vector(-dB.Y, dB.X);

            //Point cA = sA.MiddlePoint;
            //Point cB = sB.MiddlePoint;

            //Point center;
            //double start, end, radius;
            //if (p0.Distance(p2) < MathUtils.MathUtil.QUOTE_EPSILON)
            //{
            //    center = cA;
            //    start = Math.Atan2(p0.Y - center.Y, p0.X - center.X);
            //    while (start < 0)
            //        start += Math.PI * 2;
            //    end = start + Math.PI * 2;
            //    radius = lA * 0.5;
            //    InitArc(center.X, center.Y, radius, start, Math.PI * 2, false);
            //}
            //else
            //{
            //    double lMax = Math.Max(lA, lB) / MathUtil.FLT_EPSILON;
            //    Segment oSA = new Segment(cA - (oA * lMax), cA + (oA * lMax));
            //    Segment oSB = new Segment(cB - (oB * lMax), cB + (oB * lMax));
            //    Point[] intersections;
            //    int count = oSA.Intersect(oSB, out intersections);
            //    if (count == 1)
            //    {
            //        center = intersections[0];
            //        start = Math.Atan2(p0.Y - center.Y, p0.X - center.X);
            //        end = Math.Atan2(p2.Y - center.Y, p2.X - center.X);
            //        radius = center.Distance(p0);
            //    }
            //    else
            //    {
            //        Radius = -1;
            //        return "found " + count + " intersections; lA = " + lA + " lb = " + lB;
            //    }

            //    var vc = dA * dB;
            //    double startCCW, endCCW, sweepCCW, sweep;
            //    bool isCCW = vc.Z > 0;
            //    if (isCCW)
            //    {
            //        startCCW = start;
            //        endCCW = end;
            //    }
            //    else
            //    {
            //        startCCW = end;
            //        endCCW = start;
            //    }
            //    if (endCCW < startCCW)
            //        endCCW += Math.PI * 2;

            //    sweepCCW = endCCW - startCCW;

            //    if (isCCW)
            //        sweep = sweepCCW;
            //    else
            //        sweep = -sweepCCW;

            //    InitArc(center.X, center.Y, radius, start, sweep, false);
            //}

            //return string.Empty;
        }

        ///****************************************************************************************************
        /// <summary>
        /// Calcoli di inizializzazione dell'arco
        /// </summary>
        /// <param name="xc">ascissa centro arco</param>
        /// <param name="yc">ordinata centro arco</param>
        /// <param name="r">raggio arco</param>
        /// <param name="alpha">angolo iniziale arco</param>
        /// <param name="beta">angolo ampiezza arco</param>
        /// <param name="approx">indica se si deve cercare di approssimare gli angoli ad angoli noti</param>
        ///****************************************************************************************************
        private void InitArc(double xc, double yc, double r, double alpha, double beta, bool approx)
        {
            mC = new Point(xc, yc);
            mR = r;

            //Normalizza fra 0 e 2PIGRECO
            if (alpha > (2d * Math.PI)) 
                alpha = alpha - 2d * Math.PI;
            if (alpha < (-2d * Math.PI))
                alpha = alpha + 2d * Math.PI;
            if (alpha < 0d)
                alpha += 2d * Math.PI;

            if (approx)
            {
                if (MathUtil.Compare(alpha, 0d, MathUtil.RAD_EPSILON) == 0)
                    mAlpha = 0d;
                else if (MathUtil.Compare(alpha, Math.PI, MathUtil.RAD_EPSILON) == 0)
                    mAlpha = Math.PI;
                else if (MathUtil.Compare(alpha, 2d * Math.PI, MathUtil.RAD_EPSILON) == 0)
                    mAlpha = 0d;
                else
                    mAlpha = alpha;
            }
            else
                mAlpha = alpha;

            mBeta = beta;
        }

        //***************************************************************************/
        //                                                                          */
        // Intersect:      calcola il punto di intersezione, se esiste, tra			*/
        //                 l'arco e un segmento.									*/
        //                                                                          */
        // Inputs:    seg     = puntatore alla definzione del segmento.             */
        //            inter1  = puntatore al punto di intersezione (1).             */
        //            inter2  = puntatore al punto di intersezione (2).             */
        //                                                                          */
        // Outputs:   0     = nessuna intersezione.                                 */
        //            1     = una intersezione trovata.                             */
        //            2     = due intersezioni trovate.                             */
        //                                                                          */
        //***************************************************************************/
        private int Intersect(Segment seg, out Point inter1, out Point inter2)
        {
            int n_inter = 0, np_validi = 0;
            double q, m, a, b, c, d, w, rd;
            Point p1 = new Point(), p2 = new Point();
            Point e2p1, e2p2;

            inter1 = inter2 = null;

            /* estensioni del segmento */
            double dx = seg.Dx;
            double dy = seg.Dy;

            /* tipo della retta */
            if (Math.Abs(dx) > Math.Abs(dy)) 
            {
                /* parametri della retta */
                m = dy / dx;
                q = seg.P2.Y - m * seg.P2.X;

                /* soluzione del sistema (parametri equazione ax^2 + bx + c = 0) */
                w = q - mC.Y;
                a = 1d + (m * m);
                b = 2d * (m * w - mC.X);
                c = w * w + mC.X * mC.X - mR * mR;
                d = b * b - 4 * a * c;
                if (d <= -MathUtil.FLT_EPSILON)
                    return 0;
                if ( d <= 0d )
                    p1.X = p2.X = -b / (2d * a);
                else
                {
                    rd  = Math.Sqrt(d);
                    p1.X = (-b - rd) / (2d * a);
                    p2.X = (-b + rd) / (2d * a);
                }
                p1.Y = m * p1.X + q;
                p2.Y = m * p2.X + q;

            }
            else 
            {

                /* parametri della retta */
                m = dx / dy;
                q = seg.P2.X - m * seg.P2.Y;

                /* soluzione del sistema (parametri equazione ay^2 + by + c = 0) */
                w = q - mC.X;
                a = 1d + (m * m);
                b = 2d * (m * w - mC.Y);
                c = w * w + mC.Y * mC.Y - mR * mR;
                d = b * b - 4 * a * c;
                if (d <= -MathUtil.FLT_EPSILON)
                    return   0;
                if (d <= 0d)
                    p1.Y = p2.Y = -b / (2d * a);
                else
                {
                    rd  = Math.Sqrt(d);
                    p1.Y = (-b - rd) / (2d * a);
                    p2.Y = (-b + rd) / (2d * a);
                }
                p1.X = m * p1.Y + q;
                p2.X = m * p2.Y + q;

            }

            /* controlla se i punti sono diversi */
            if (p1 != p2)
                np_validi = 2;
            else
                np_validi = 1;

            /* controllo appartenza al segmento e all'arco del primo punto */
            if (seg.ContainPoint(p1))
                if (ContainPoint(p1))
                {
                    inter1 = p1;
                    n_inter++;
                }

            /* controllo se devo controllare anche il secondo punto */
            if (np_validi > n_inter) 
            {
                /* controllo appartenza al segmento e all'arco del secondo punto */
                if (seg.ContainPoint(p2))
                    if (ContainPoint(p2))
                    {
                        if (n_inter == 1)
                            inter2 = p2;
                        else
                            inter1 = p2;

                        n_inter++;
                    }
            }

            /* una intersezione e` valida se il punto trovato non e` */
            /* contemporaneamente estremo di entrambe le entita`     */
            if (n_inter != 0)
            {
                /* ricavo gli estremi dell'arco */
                e2p1 = StartPoint();
                e2p2 = EndPoint();

                /* controllo appartenenza a due estremi contemporaneamente */
                if (IsConcurrentExtremity(inter1, seg.P1, seg.P2, e2p1, e2p2))
                {
                    /* questa intersezione non deve essere considerata */
                    n_inter--;

                    /* se esiste anche la seconda intersezione */
                    if (n_inter > 0)
                    {
                        inter1.X = inter2.X;
                        inter1.Y = inter2.Y;

                        /* controllo appartenenza a due estremi contemporaneamente */
                        if (IsConcurrentExtremity(inter1, seg.P1, seg.P2, e2p1, e2p2))
                            return 0;
                    }
                }

                else
                    /* se esiste anche la seconda intersezione */
                    if (n_inter > 1)
                        /* controllo appartenenza a due estremi contemporaneamente */
                        if (IsConcurrentExtremity(inter2, seg.P1, seg.P2, e2p1, e2p2))
                            return 1;
            }

            /* numero di punti validi */
            return   n_inter;
        }

        //***************************************************************************/
        //                                                                          */
        // Intersect:      calcola il punto di intersezione, se esiste, tra			*/
        //                 l'arco e un altro arco.									*/
        //                                                                          */
        // Inputs:    arc     = puntatore alla definzione dell'arco.                */
        //            inter1  = puntatore al punto di intersezione (1).             */
        //            inter2  = puntatore al punto di intersezione (2).             */
        //                                                                          */
        // Outputs:   0     = nessuna intersezione.                                 */
        //            1     = una intersezione trovata.                             */
        //            2     = due intersezioni trovate.                             */
        //                                                                          */
        //***************************************************************************/
        private int Intersect(Arc arc, out Point inter1, out Point inter2)
        {

            int n_inter = 0, np_validi = 0;
            double A, B, C, D, E, F, G, H, I, J;
            double delta;
            Point p1 = new Point(), p2 = new Point();
            Point e1p1, e1p2, e2p1, e2p2;
            Point c2 = arc.CenterPoint;

            inter1 = inter2 = null;

            /* calcolo i parametri per la soluzione del sistema congiunto */
            A = mR * mR - mC.X * mC.X - mC.Y * mC.Y;
            B = arc.Radius * arc.Radius - c2.X * c2.X - c2.Y * c2.Y;
            C = (A - B) / 2d;
            D = (c2.X - mC.X);
            E = (c2.Y - mC.Y);

            //if (!MathUtil.IsZero(D)) 
            if (Math.Abs(D) > Math.Abs(E)) 			
            {
                F = C / D;
                G = E / D;
                H = G * G + 1d;
                I = mC.X * G - F * G - mC.Y;
                J = F * F - 2d * mC.X * F - A;
                if (MathUtil.IsZero(H))
                    return 0;

                /* a questo punto ho trovato: y1,2 = (-I -/+ Math.Sqrt(I*I - H*J)) / H */
                delta = I * I - H * J;
                if (delta < 0d)
                    return 0;
                if (MathUtil.IsZero(delta))
                    delta = 0d;
                p1.Y = (-I - Math.Sqrt(delta)) / H;
                p2.Y = (-I + Math.Sqrt(delta)) / H;

                /* trovati y1 e y2, trovo x1 e x2 */
                p1.X = F - G * p1.Y;
                p2.X = F - G * p2.Y;
            }
            else 
            {
                F = C / E;
                G = D / E;
                H = G * G + 1d;
                I = mC.Y * G - F * G - mC.X;
                J = F * F - 2d * mC.Y * F - A;
                if (MathUtil.IsZero(H))
                    return 0;

                /* a questo punto ho trovato: y1,2 = (-I -/+ Math.Sqrt(I*I - H*J)) / H */
                delta = I * I - H * J;
                if (delta < 0d)
                    return 0;
                if (MathUtil.IsZero(delta))
                    delta = 0d;
                p1.X = (-I - Math.Sqrt(delta)) / H;
                p2.X = (-I + Math.Sqrt(delta)) / H;

                /* trovati x1 e x2, trovo y1 e y2 */
                p1.Y = F - G * p1.X;
                p2.Y = F - G * p2.X;
            }

            /* controlla se i punti sono diversi */
            if (p1 != p2)
                np_validi = 2;
            else
                np_validi = 1;

            /* controllo appartenza agli archi della prima intersezione */
            if (ContainPoint(p1))
                if (arc.ContainPoint(p1))
                {
                    inter1 = p1;
                    n_inter = 1;
                }

            /* controllo se devo controllare anche il secondo punto */
            if (np_validi > n_inter) 
                /* controllo appartenza agli archi della seconda intersezione */
                if (ContainPoint(p2))
                    if (arc.ContainPoint(p2))
                    {
                        if (n_inter == 1)
                            inter2 = p2;
                        else
                            inter1 = p2;

                        n_inter++;
                    }

            /* una intersezione e` valida se il punto trovato non e` */
            /* contemporaneamente estremo di entrambe le entita`     */
            if (n_inter > 0)
            {

                /* ricavo gli estremi degli archi */
                e1p1 = StartPoint();
                e1p2 = EndPoint();
                e2p1 = arc.StartPoint();
                e2p2 = arc.EndPoint();

                /* controllo appartenenza a due estremi contemporaneamente */
                if (IsConcurrentExtremity(inter1, e1p1, e1p2, e2p1, e2p2))
                {
                    /* questa intersezione non deve essere considerata */
                    n_inter--;

                    /* se esiste anche la seconda intersezione */
                    if (n_inter > 0)
                    {
                        inter1.X = inter2.X;
                        inter1.Y = inter2.Y;

                        /* controllo apparteneza a due estremi contemporaneamente */
                        if (IsConcurrentExtremity(inter1, e1p1, e1p2, e2p1, e2p2))
                            return 0;
                    }
                }
                else
                    /* se esiste anche la seconda intersezione */
                    if (n_inter > 1)
                        /* controllo apparteneza a due estremi contemporaneamente */
                        if (IsConcurrentExtremity(inter2, e1p1, e1p2, e2p1, e2p2))
                            return 1;
            }

            /* numero di intersezioni valide */
            return   n_inter;

        }

        private static class ConnectSidesMethods
        {
            public static readonly double DOT_PRODUCT_FLT_EPSILON = 1 - Math.Cos(MathUtil.FLT_EPSILON);

            public static void ConnectSides_ChangeArcAmplitudeFromNewEstremity(Breton.Polygons.Arc arc1, Breton.Polygons.Point intersection)
            {
                var dir = arc1.CenterPoint.Direction(intersection);
                if (arc1.ContainPoint(intersection))
                {
                    if (ConnectSides_ChangeArcAmplitudeFromNewEstremity_HandleDegenerate(arc1, intersection))
                        return;

                    double new_amp;
                    if (arc1.AmplAngle > 0)
                        new_amp = arc1.AmplAngle - MathUtil.Angolo_0_2PI(dir - arc1.StartAngle);
                    else
                        new_amp = arc1.AmplAngle + MathUtil.Angolo_0_2PI(arc1.StartAngle - dir);
                    arc1.AmplAngle = new_amp;
                    arc1.StartAngle = dir;
                }
                else
                {
                    double new_amp;
                    if (arc1.AmplAngle > 0)
                        new_amp = arc1.AmplAngle + MathUtil.Angolo_0_2PI(arc1.StartAngle - dir);
                    else
                        new_amp = arc1.AmplAngle - MathUtil.Angolo_0_2PI(dir - arc1.StartAngle);
                    arc1.AmplAngle = new_amp;
                    arc1.StartAngle = dir;
                }
            }
            public static bool ConnectSides_ChangeArcAmplitudeFromNewEstremity_HandleDegenerate(Breton.Polygons.Arc arc1, Breton.Polygons.Point intersection)
            {
                var arc1_Center = arc1.CenterPoint;

                var c1_ict = (Breton.MathUtils.Vector)(intersection - arc1_Center);
                var l_c1_ict = c1_ict.Magnitude;
                if (l_c1_ict < MathUtil.gdDBLEPSILON)
                    return true;

                var u_c1_pIntersection = c1_ict * (1 / l_c1_ict);

                if (arc1.Radius < MathUtil.gdDBLEPSILON)
                    return true;

                var arc1_pStart = arc1.P1;
                var arc1_pMid = arc1.MiddlePoint;
                var arc1_pEnd = arc1.P2;

                var c1_pStart = ((Breton.MathUtils.Vector)(arc1_pStart - arc1_Center));
                var l_c1_pStart = c1_pStart.Magnitude;
                if (l_c1_pStart > MathUtil.FLT_EPSILON)
                {
                    var u_c1_pStart = c1_pStart * (1 / l_c1_pStart);
                    var z_c1_Start = (u_c1_pStart * u_c1_pIntersection).Z;
                    var dp_c1_Start = Breton.MathUtils.Vector.ScalarProduct(u_c1_pStart, u_c1_pIntersection);

                    if (MathUtil.IsAlmost(dp_c1_Start, 1, DOT_PRODUCT_FLT_EPSILON))
                    {
                        //intersection � newStart

                        Breton.Polygons.Point newStart;
                        Breton.Polygons.Point newMid;
                        Breton.Polygons.Point newEnd;

                        newStart = intersection;
                        newMid = arc1_pMid;
                        newEnd = arc1_pEnd;

                        ArcFrom3PointsMethods.InitArcConsideringDegenerates(arc1, newStart, newMid, newEnd);

                        return true;
                    }
                }

                var c1_pEnd = (Breton.MathUtils.Vector)(arc1_pEnd - arc1_Center);
                var l_c1_pEnd = c1_pEnd.Magnitude;
                if (l_c1_pEnd > MathUtil.FLT_EPSILON)
                {
                    var u_c1_pEnd = c1_pEnd * (1 / l_c1_pEnd);
                    var z_c1_End = (u_c1_pEnd * u_c1_pIntersection).Z;
                    var dp_c1_End = Breton.MathUtils.Vector.ScalarProduct(u_c1_pEnd, u_c1_pIntersection);

                    if (MathUtil.IsAlmost(dp_c1_End, 1, DOT_PRODUCT_FLT_EPSILON))
                    {
                        //intersection � newEnd

                        Breton.Polygons.Point newStart;
                        Breton.Polygons.Point newMid;
                        Breton.Polygons.Point newEnd;

                        newStart = arc1_pStart;
                        newMid = arc1_pMid;
                        newEnd = intersection;

                        ArcFrom3PointsMethods.InitArcConsideringDegenerates(arc1, newStart, newMid, newEnd);

                        return true;
                    }
                }

                return false;
            }
        }

        private static class ArcFrom3PointsMethods
        {
            public static readonly double MINIMUM_EPS = MathUtil.gdDBLEPSILON;
            public static readonly double MINIMUM_RADIUS_CREATED = MathUtil.FLT_EPSILON;
            public static readonly double MINIMUM_ARC_CREATED = MathUtil.FLT_EPSILON;
            public static readonly double BIG_RADIUS_FOR_SMALL_AMPLITUDE = 1 / MathUtil.FLT_EPSILON;
            public static readonly double INTERVAL_LARGE_AMPLITUDE = 2 * Math.PI - MathUtil.FLT_EPSILON;
            public static readonly double INTERVAL_EQUAL_POINTS = 2 * Math.PI;

            /// <summary>
            /// Calcoli di inizializzazione dell'arco: Arco per tre punti generando errore in caso di MathUtil.QUOTE_EPSILON
            /// </summary>
            /// <param name="x0">x primo punto</param>
            /// <param name="y0">y primo punto</param>
            /// <param name="x1">x secondo punto (intermedio)</param>
            /// <param name="y1">y secondo punto (intermedio)</param>
            /// <param name="x2">x terzo punto</param>
            /// <param name="x2">y terzo punto</param>
            public static string InitArcQuoteEpsilon(Arc defining, double x0, double y0, double x1, double y1, double x2, double y2)
            {
                Point p0 = new Point(x0, y0);
                Point p1 = new Point(x1, y1);
                Point p2 = new Point(x2, y2);
                Segment sA = new Segment(p0, p1);
                double lA = sA.Length;
                Segment sB = new Segment(p1, p2);
                double lB = sB.Length;

                Vector dA = new Vector(x1 - x0, y1 - y0);
                if (lA < MathUtil.QUOTE_EPSILON)
                {
                    defining.Radius = -1;
                    return "first 2 points are nearer than " + MathUtil.QUOTE_EPSILON;
                }
                dA = dA.UnitVector;

                Vector dB = new Vector(x2 - x1, y2 - y1);
                if (lB < MathUtil.QUOTE_EPSILON)
                {
                    defining.Radius = -1;
                    return "last 2 points are nearer than " + MathUtil.QUOTE_EPSILON;
                }
                dB = dB.UnitVector;

                Vector oA = new Vector(-dA.Y, dA.X);
                Vector oB = new Vector(-dB.Y, dB.X);

                Point cA = sA.MiddlePoint;
                Point cB = sB.MiddlePoint;

                Point center;
                double start, end, radius;
                if (p0.Distance(p2) < MathUtils.MathUtil.QUOTE_EPSILON)
                {
                    center = cA;
                    start = Math.Atan2(p0.Y - center.Y, p0.X - center.X);
                    while (start < 0)
                        start += Math.PI * 2;
                    end = start + Math.PI * 2;
                    radius = lA * 0.5;
                    initArc_Standard(defining, center.X, center.Y, radius, start, Math.PI * 2, false);
                }
                else
                {
                    double lMax = Math.Max(lA, lB) / MathUtil.FLT_EPSILON;
                    Segment oSA = new Segment(cA - (oA * lMax), cA + (oA * lMax));
                    Segment oSB = new Segment(cB - (oB * lMax), cB + (oB * lMax));
                    Point[] intersections;
                    int count = oSA.Intersect(oSB, out intersections);
                    if (count == 1)
                    {
                        center = intersections[0];
                        start = Math.Atan2(p0.Y - center.Y, p0.X - center.X);
                        end = Math.Atan2(p2.Y - center.Y, p2.X - center.X);
                        radius = center.Distance(p0);
                    }
                    else
                    {
                        defining.Radius = -1;
                        return "found " + count + " intersections; lA = " + lA + " lb = " + lB;
                    }

                    var vc = dA * dB;
                    var sweep = initArc_GetSweepFromPoints(vc.Z > 0, start, end);

                    initArc_Standard(defining, center.X, center.Y, radius, start, sweep, false);
                }

                return string.Empty;
            }

            private static bool UnitizeIfPossible(Vector vectorToUnitize, out double length, out Breton.MathUtils.Vector u)
            {
                length = vectorToUnitize.Magnitude;
                if (length < MINIMUM_EPS)
                {
                    u = vectorToUnitize;
                    return false;
                }
                else
                {
                    u = vectorToUnitize * (1 / length);
                    return true;
                }
            }

            /// <summary>
            /// Calcoli di inizializzazione dell'arco: Arco per tre punti forzando creazione elementi di piccola ampiezza
            /// </summary>
            /// <param name="x0">x primo punto</param>
            /// <param name="y0">y primo punto</param>
            /// <param name="x1">x secondo punto (intermedio)</param>
            /// <param name="y1">y secondo punto (intermedio)</param>
            /// <param name="x2">x terzo punto</param>
            /// <param name="x2">y terzo punto</param>
            public static string InitArcConsideringDegenerates(Arc defining, double x0, double y0, double x1, double y1, double x2, double y2)
            {
                Point p0 = new Point(x0, y0);
                Point p1 = new Point(x1, y1);
                Point p2 = new Point(x2, y2);
                return InitArcConsideringDegenerates(defining, p0, p1, p2);
            }
            public static string InitArcConsideringDegenerates(Arc defining, Point p0, Point p1, Point p2)
            {
                Vector dA,dB,uA,uB;
                dA = new Vector(p1.X - p0.X, p1.Y - p0.Y);
                dB  = new Vector(p2.X - p1.X, p2.Y - p1.Y);
                double lA,lB;
                UnitizeIfPossible(dA, out lA, out uA);
                UnitizeIfPossible(dB, out lB, out uB);

                bool initArcParametersDefined = false;
                Point center = null;
                double start = double.NaN;
                double end = double.NaN;
                double radius = double.NaN;

                var vc = dA * dB;
                bool isCCW = vc.Z >= 0;
                double sweep = double.NaN;

                if (!initArcParametersDefined)
                {
                    if (MathUtil.IsAlmost(lA, 0, MINIMUM_RADIUS_CREATED) || MathUtil.IsAlmost(lB, 0, MINIMUM_RADIUS_CREATED))//arco simile ad un segmento se uno dei due segmenti � troppo corto
                    {
                        string error;
                        if (!initArc_CreateDegenerateSmallAmplitude(isCCW, p0, p1, p2, BIG_RADIUS_FOR_SMALL_AMPLITUDE, out center, out radius, out start, out end, out sweep, out error))
                        {
                            return error;
                        }
                        else
                        {
                            initArcParametersDefined = true;
                        }
                    }
                }

                if (!initArcParametersDefined)
                {
                    if (initArc_HandleStandard(isCCW, uA, lA, uB, lB, p0, p1, p2, out center, out radius, out start, out end, out sweep))//arco
                    {
                        initArcParametersDefined = true;
                    }
                }

                if (!initArcParametersDefined)
                {
                    Breton.Polygons.Point furthest1, furthest2;
                    if (initArc_GetFurthestPoints(p0, p1, p2, false, out furthest1, out furthest2))
                    {
                        var distance_OriginalExtremities_IfCircle = p0.Distance(p2);
                        var center_IfCircle = furthest1.MidPoint(furthest2);
                        var vCenterToStart_OriginalExtremities_IfCircle = (Breton.MathUtils.Vector)(p0 - center_IfCircle);
                        var vCenterToEnd_OriginalExtremities_IfCircle = (Breton.MathUtils.Vector)(p2 - center_IfCircle);
                        Breton.MathUtils.Vector uCenterToStart_OriginalExtremities_IfCircle, uCenterToEnd_OriginalExtremities_IfCircle;
                        double lCenterToStart_OriginalExtremities_IfCircle, lCenterToEnd_OriginalExtremities_IfCircle;
                        var okUnitize1 = UnitizeIfPossible(vCenterToStart_OriginalExtremities_IfCircle, out lCenterToStart_OriginalExtremities_IfCircle, out uCenterToStart_OriginalExtremities_IfCircle);
                        var okUnitize2 = UnitizeIfPossible(vCenterToEnd_OriginalExtremities_IfCircle, out lCenterToEnd_OriginalExtremities_IfCircle, out uCenterToEnd_OriginalExtremities_IfCircle);
                        if (okUnitize1 && okUnitize2)
                        {
                            var angle_OriginalExtremities_IfCircle = Math.Acos(Math.Max(-1, Math.Min(1, Breton.MathUtils.Vector.ScalarProduct(uCenterToStart_OriginalExtremities_IfCircle, uCenterToEnd_OriginalExtremities_IfCircle))));
                            var distanceTriggersCircle = MathUtil.IsAlmost(distance_OriginalExtremities_IfCircle, 0, MINIMUM_RADIUS_CREATED);
                            var angleTriggersCircle = MathUtil.IsAlmost(angle_OriginalExtremities_IfCircle, 0, MINIMUM_ARC_CREATED);
                            if (distanceTriggersCircle || angleTriggersCircle)//circonferenza se p0 e p1 vicini, oppure distribuiti su un segmento formando angolo 360�
                            {
                                var radiusFromDiameter = (furthest1 - furthest2).Abs * 0.5;
                                if (radiusFromDiameter > MINIMUM_RADIUS_CREATED)
                                {
                                    string error;
                                    if (!initArc_CreateDegenerateLargeAmplitude(isCCW, p0, p1, p2, out center, out radius, out start, out end, out sweep, out error))
                                    {
                                        return error;
                                    }
                                    else
                                    {
                                        initArcParametersDefined = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        return "all points are coincident, cannot define an arc";
                    }
                }

                if (!initArcParametersDefined)//se non si riesce a creare una circonferenza perch� raggio risultante � minore di minimo raggio creabile, si crea un arco simile ad un segmento, forzando inizio su p0 e fine su p2
                {
                    string error;
                    if (!initArc_CreateDegenerateSmallAmplitude(isCCW, p0, p2, p2, BIG_RADIUS_FOR_SMALL_AMPLITUDE, out center, out radius, out start, out end, out sweep, out error))
                    {
                        return error;
                    }
                    else
                    {
                        initArcParametersDefined = true;
                    }
                }

                if (initArcParametersDefined)
                {
                    initArc_Standard(defining, center.X, center.Y, radius, start, sweep, false);

                    return null;
                }
                else
                {
                    return "conditions to define an arc from 3 points not handled";
                }
            }
            private static bool initArc_HandleStandard(bool isCCW, Vector uA, double lA, Vector uB, double lB, Breton.Polygons.Point p0, Breton.Polygons.Point p1, Breton.Polygons.Point p2, out Breton.Polygons.Point center, out double radius, out double start, out double end, out double sweep)
            {
                Vector oA = new Vector(-uA.Y, uA.X);
                Vector oB = new Vector(-uB.Y, uB.X);

                Point cA = p0.MidPoint(p1);
                Point cB = p1.MidPoint(p2);

                double lMax = Math.Max(lA, lB) / MathUtil.FLT_EPSILON;
                Segment oSA = new Segment(cA - (oA * lMax), cA + (oA * lMax));
                Segment oSB = new Segment(cB - (oB * lMax), cB + (oB * lMax));
                
                
                Point tmp = null;
                var d_cA_cB = cA.Distance(cB);
                if (d_cA_cB < MINIMUM_RADIUS_CREATED)
                {
                    tmp = cA.MidPoint(cB);
                }
                else
                {
                    Point inters;
                    if (Segment.Intersection(oSA, oSB, out inters, MINIMUM_RADIUS_CREATED) == 1)
                    {
                        tmp = inters;
                    }
                }

                if(tmp != null)
                {
                    center = tmp;
                    start = Math.Atan2(p0.Y - center.Y, p0.X - center.X);
                    end = Math.Atan2(p2.Y - center.Y, p2.X - center.X);
                    radius = center.Distance(p0);
                    sweep = initArc_GetSweepFromPoints(isCCW, start, end);
                    return true;
                }
                else
                {
                    center = null;
                    start = double.NaN;
                    end = double.NaN;
                    radius = -1;
                    sweep = double.NaN;
                    return false;
                }
            }

            private static bool initArc_CreateDegenerateSmallAmplitude(bool isCCW, Breton.Polygons.Point p0, Breton.Polygons.Point p1, Breton.Polygons.Point p2, double radiusForSmallAmplitude, out Breton.Polygons.Point center, out double radius, out double start, out double end, out double sweep, out string error)
            {
                Breton.Polygons.Point furthest1, furthest2;
                if (initArc_GetFurthestPoints(p0, p1, p2, true, out furthest1, out furthest2))
                {
                    radius = radiusForSmallAmplitude;

                    var segment = (Breton.MathUtils.Vector)(furthest2 - furthest1);
                    var tangent = segment.UnitVector;
                    Breton.MathUtils.Vector orthogonal = new Breton.MathUtils.Vector(-tangent.Y, tangent.X);
                    if (!isCCW)
                        orthogonal = orthogonal * -1;
                    center = furthest1.MidPoint(furthest2) + orthogonal * radiusForSmallAmplitude;

                    start = Math.Atan2(furthest1.Y - center.Y, furthest1.X - center.X);
                    var vCenterToStart = (Breton.MathUtils.Vector)(furthest1 - center);
                    var vCenterToEnd = (Breton.MathUtils.Vector)(furthest2 - center);
                    var lCenterToStart = vCenterToStart.Magnitude;
                    var lCenterToEnd = vCenterToEnd.Magnitude;
                    var l_min = MINIMUM_EPS * 10;
                    if (lCenterToStart > l_min && lCenterToEnd > l_min)
                    {
                        var uCenterToStart = vCenterToStart.UnitVector;
                        var uCenterToEnd = vCenterToEnd.UnitVector;
                        sweep = Math.Acos(Math.Max(-1, Math.Min(1, Breton.MathUtils.Vector.ScalarProduct(vCenterToStart, vCenterToEnd) / (lCenterToStart * lCenterToEnd))));

                        if(sweep == 0)
                        {
                            sweep = furthest1.Distance(furthest2) / radiusForSmallAmplitude;
                        }

                        if (!isCCW)
                            sweep = sweep * -1;
                        end = start + sweep;

                        error = null;
                    }
                    else
                    {
                        center = null;
                        radius = -1;
                        start = double.NaN;
                        end = double.NaN;
                        sweep = double.NaN;

                        error = "cannot evaluate distances original furthest points are too close";
                    }
                }
                else
                {
                    center = null;
                    radius = -1;
                    start = double.NaN;
                    end = double.NaN;
                    sweep = double.NaN;

                    error = "all points are coincident, cannot define an arc";
                }

                return error == null;
            }
            private static bool initArc_CreateDegenerateLargeAmplitude(bool isCCW, Breton.Polygons.Point p0, Breton.Polygons.Point p1, Breton.Polygons.Point p2, out Breton.Polygons.Point center, out double radius, out double start, out double end, out double sweep, out string error)
            {
                Breton.Polygons.Point pDiameter1, pDiameter2;
                if (initArc_GetFurthestPoints(p0, p1, p2, false, out pDiameter1, out pDiameter2))
                {
                    center = pDiameter1.MidPoint(pDiameter2);
                    var vCenterToStart = (Breton.MathUtils.Vector)(pDiameter1 - center);
                    start = Math.Atan2(vCenterToStart.Y, vCenterToStart.X);
                    while (start < 0)
                        start += Math.PI * 2;
                    var xEqual = MathUtil.IsAlmost(p0.X, p2.X, MINIMUM_RADIUS_CREATED);
                    var yEqual = MathUtil.IsAlmost(p0.Y, p2.Y, MINIMUM_RADIUS_CREATED);
                    if (xEqual && yEqual)
                        sweep = INTERVAL_EQUAL_POINTS;
                    else
                        sweep = INTERVAL_LARGE_AMPLITUDE;
                    if (!isCCW)
                        sweep = sweep * -1;
                    end = start + sweep;
                    radius = vCenterToStart.Magnitude;

                    error = null;
                }
                else
                {
                    center = null;
                    radius = -1;
                    start = double.NaN;
                    end = double.NaN;
                    sweep = double.NaN;

                    error = "all points are coincident, cannot define an arc";
                }
                return error == null;
            }

            private static double initArc_GetSweepFromPoints(bool isCCW, double start, double end)
            {
                double startCCW, endCCW, sweepCCW, sweep;
                if (isCCW)
                {
                    startCCW = start;
                    endCCW = end;
                }
                else
                {
                    startCCW = end;
                    endCCW = start;
                }
                if (endCCW < startCCW)
                    endCCW += Math.PI * 2;

                sweepCCW = endCCW - startCCW;

                if (isCCW)
                    sweep = sweepCCW;
                else
                    sweep = -sweepCCW;

                return sweep;
            }

            private static void initArc_Standard(Arc defining, double xc, double yc, double r, double alpha, double beta, bool approx)
            {
                defining.InitArc(xc, yc, r, alpha, beta, approx);
            }

            private static bool initArc_GetFurthestPoints(Breton.Polygons.Point p0, Breton.Polygons.Point p1, Breton.Polygons.Point p2, bool isSmallAmplitude, out Point furthest1, out Point furthest2)
            {
                furthest1 = null;
                furthest2 = null;

                var l0to1 = p0.Distance(p1);
                var l1to2 = p1.Distance(p2);
                var l0to2 = p0.Distance(p2);
                double l;

                if (l0to2 > l0to1 && l0to2 > l1to2)
                {
                    l = l0to2;
                    furthest1 = p0;
                    furthest2 = p2;
                }
                else if (l1to2 > l0to1 && l1to2 > l0to2)
                {
                    l = l1to2;
                    if (isSmallAmplitude)
                    {
                        //arco simile a segmento: p0 p1 p2 sono allineati
                        furthest1 = p1;
                        furthest2 = p2;
                    }
                    else
                    {
                        //arco simile a circonferenza: p0 e p2 erano punti uguali di inizio
                        furthest1 = p2;
                        furthest2 = p1;
                    }
                }
                else
                {
                    l = l0to1;
                    furthest1 = p0;
                    furthest2 = p1;
                }

                if (l < MathUtil.gdDBLEPSILON)
                {
                    furthest1 = null;
                    furthest2 = null;
                }

                return furthest1 != null && furthest2 != null;
            }
        }

        #endregion
    }
}


