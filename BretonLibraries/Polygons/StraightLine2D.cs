﻿using System;
using System.Collections;
using System.Diagnostics;
using Breton.MathUtils;

namespace Breton.Polygons
{
    /// <summary>
    /// Equazione della retta 2D nella forma di Ax + By + C = 0
    /// </summary>
    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    public class StraightLine2D : IEquatable<StraightLine2D>
    {
        //Ax + By + C = 0,
        //y = Mx + Q
        //M = Slope = -a/b -> coefficente angolare = tan(angolo con asse X)
        //Q = Y Intercept = -c/b  -> coordinata Y dove la retta intercetta l'asse Y

        private string DebuggerDisplay { get { return string.Format("({0:0.00})X + ({1:0.00})Y + ({2:0.00}) = 0", a, b, c); } }

        private double a;
        private double b;
        private double c;

        public enum Side
        {
            Left,
            Right,
            Overlap
        }

        /// <summary>
        /// Tipologia di risultato sul calcolo delle intersezioni
        /// </summary>
        public enum IntersectionResultType
        {
            /// <summary>
            /// Nessuna intersezione
            /// </summary>
            None,

            /// <summary>
            /// Almeno una intersezione
            /// </summary>
            Intersect,

            /// <summary>
            /// Gli oggetti si sovrappongono o coincidono,
            /// ho quindi un numero teoricamente infinito di intersezioni
            /// </summary>
            OverlapOrCoincide
        }

        public class Intersection
        {
            private readonly Point point;
            private readonly IntersectionResultType intersectionResultType;
            public IntersectionResultType IntersectionResultType { get { return intersectionResultType; } }
            public Point Point { get { return point; } }

            internal Intersection(IntersectionResultType _intersectionResultType, Point _point)
            {
                intersectionResultType = _intersectionResultType;
                point = _point;
            }
        }

        public double A
        {
            get { return a; }
            set { a = value; }
        }
        public double B
        {
            get { return b; }
            set { b = value; }
        }
        public double C
        {
            get { return c; }
            set { c = value; }
        }
        public double M { get { return MathUtil.IsZero(b) ? double.PositiveInfinity : -a / b; } }
        public double Q { get { return MathUtil.IsZero(b) ? double.NaN : -c / b; } }
        public Vector2D Direction { get { return Vector2D.CreateNormalized(-b, a); } }
        public double DirectionAngle { get { return MathUtil.Angolo_0_2PI_Ex(Math.Atan2(a, -b)); } }
        public double NormalizeFactor { get { return Math.Sqrt(a * a + b * b); } }
        public bool IsValid { get { return !MathUtil.IsZero(a) || !MathUtil.IsZero(b); } }

        public StraightLine2D()
        {
            //Asse X
            a = 1;
            b = 0;
            c = 0;
        }

        public StraightLine2D(StraightLine2D copy)
        {
            a = copy.a;
            b = copy.b;
            c = copy.c;
        }

        public StraightLine2D(double _a, double _b, double _c)
        {
            a = _a;
            b = _b;
            c = _c;
        }

        public StraightLine2D(Point startPoint, Point endPoint)
        {
            a = endPoint.Y - startPoint.Y;
            b = startPoint.X - endPoint.X;
            c = -startPoint.X * a - startPoint.Y * b;
        }

        public StraightLine2D(Point point, double angle)
        {
            a = Math.Sin(angle);
            b = -Math.Cos(angle);
            c = -point.X * a - point.Y * b;
        }

        public StraightLine2D(Point point, Vector2D direction)
        {
            direction = direction.GetNormalized();
            a = direction.Y;
            b = -direction.X;
            c = -point.X * a - point.Y * b;
        }

        public static StraightLine2D CreateFromCoefficients(double _a, double _b, double _c)
        {
            return new StraightLine2D(_a, _b, _c);
        }

        public static StraightLine2D CreateFrom2Points(Point startPoint, Point endPoint)
        {
            return new StraightLine2D(startPoint, endPoint);
        }

        public static StraightLine2D CreateFromPointAndAngle(Point point, double angle)
        {
            return new StraightLine2D(point, angle);
        }

        public static StraightLine2D CreateFromPointAndDirection(Point point, Vector2D direction)
        {
            return new StraightLine2D(point, direction);
        }

        public static StraightLine2D CreatePerpendicularOnFirstPoint(Point origin, Point aux)
        {
            StraightLine2D line = new StraightLine2D();
            line.a = origin.X - aux.X;
            line.b = origin.Y - aux.Y;
            line.c = -line.b * origin.Y - line.a * origin.X;

            return line;
        }

        public void Normalize()
        {
            double normalizeFactor = 1d / NormalizeFactor;

            a *= normalizeFactor;
            b *= normalizeFactor;
            c *= normalizeFactor;
        }

        public void Invert()
        {
            a = -a;
            b = -b;
            c = -c;
        }

        public StraightLine2D GetNormalized()
        {
            StraightLine2D line = new StraightLine2D(this);
            line.Normalize();

            return line;
        }

        public StraightLine2D GetInverted()
        {
            StraightLine2D vec = new StraightLine2D(this);
            vec.Invert();

            return vec;
        }

        public StraightLine2D GetPerpendicularOnPoint(Point point)
        {
            StraightLine2D perp = new StraightLine2D();
            perp.a = b;
            perp.b = -a;
            perp.c = a * point.Y - b * point.X;

            return perp;
        }

        public StraightLine2D GetPerpendicularOnOrigin()
        {
            StraightLine2D perp = new StraightLine2D();
            perp.a = b;
            perp.b = -a;
            perp.c = 0;

            return perp;
        }

        public StraightLine2D GetParallelOnPoint(Point point)
        {
            StraightLine2D parallel = new StraightLine2D();

            parallel.a = a;
            parallel.b = b;
            parallel.c = -point.X * a - point.Y * b;

            return parallel;
        }

        public StraightLine2D GetParallelOnOrigin()
        {
            StraightLine2D parallel = new StraightLine2D();

            parallel.a = a;
            parallel.b = b;
            parallel.c = 0;

            return parallel;
        }

        public bool IsParallelTo(StraightLine2D other, double tolerance = MathUtil.FLT_EPSILON)
        {
            return MathUtil.QuoteIsZero(a * other.b - other.a * b, tolerance);
        }

        public bool IsParallelToWithAngleTolerance(StraightLine2D other, double toleranceInRadians)
        {
            // normalizzando i coefficienti della retta il valore dato dalla condizion di parallelismo indica esattamente la differenza in radianti tra gli angoli delle rette rispetto all'asse X
            double normalizeFactor = 1d / NormalizeFactor;
            double normA0 = a * normalizeFactor;
            double normB0 = b * normalizeFactor;
            normalizeFactor = 1d / other.NormalizeFactor;
            double normA1 = other.a * normalizeFactor;
            double normB1 = other.b * normalizeFactor;

            return MathUtil.QuoteIsZero(normA0 * normB1 - normA1 * normB0, toleranceInRadians);
        }

        public bool IsParallelAndInverseTo(StraightLine2D other, double tolerance = MathUtil.FLT_EPSILON)
        {
            return IsParallelTo(other, tolerance) && IsInverseTo(other);
        }

        public bool IsHorizontal(double toleranceInRadians = MathUtil.RAD_EPSILON)
        {
            double angle = DirectionAngle;
            return MathUtil.IsAlmost(angle, 0, toleranceInRadians) || MathUtil.IsAlmost(angle, Math.PI, toleranceInRadians)|| MathUtil.IsAlmost(angle, 2 * Math.PI, toleranceInRadians);
        }

        public bool IsVertical(double toleranceInRadians = MathUtil.RAD_EPSILON)
        {
            double angle = DirectionAngle;
            return MathUtil.IsAlmost(angle, Math.PI / 2d, toleranceInRadians) || MathUtil.IsAlmost(DirectionAngle, Math.PI + Math.PI / 2d, toleranceInRadians);
        }

        public bool IsInverseTo(StraightLine2D other)
        {
            return (!MathUtil.IsZero(a) && !MathUtil.IsZero(other.a) && Math.Sign(a) == -Math.Sign(other.a)) || (!MathUtil.IsZero(b) && !MathUtil.IsZero(other.b) && Math.Sign(b) == -Math.Sign(other.b));
        }

        public bool IsPerpendicularTo(StraightLine2D other, double tolerance = MathUtil.FLT_EPSILON)
        {
            return MathUtil.QuoteIsZero(a * other.a + other.b * b, tolerance);
        }

        public bool IsCoincidentWith(StraightLine2D other, double tolerance = MathUtil.QUOTE_EPSILON)
        {
            if (!IsParallelTo(other, tolerance))
                return false;

            double localC = Math.Abs(c / NormalizeFactor);
            double otherC = Math.Abs(other.c / other.NormalizeFactor);

            return MathUtil.QuoteIsEQ(localC, otherC, tolerance);

            // oppure: se sono parallele e hanno almeno un punto in comune allora sono coincidenti
            //Point2d p1 = GetPointOnLineAtPosition(100.0);
            //return other.OverlapsWith(p1, tolerance);
        }

        public double GetDistanceTo(Point point)
        {
            return Math.Abs(a * point.X + b * point.Y + c) / NormalizeFactor;
        }

        public double GetDistanceTo(StraightLine2D other)
        {
            //Se le rette non sono parallele la distanza è 0
            if (!IsParallelTo(other))
                return 0d;

            //Le rette parallele hanno a e b uguali quindi dipende tutto da c
            // controllo se le rette hanno direzioni inverse
            double otherC = IsInverseTo(other) ? -other.c : other.c;

            return Math.Abs((otherC / other.NormalizeFactor) - (c / NormalizeFactor));

            // altro metodo: utilizzo la distanza da un punto dell'altra retta
            //double x = 0d;
            //double y = 0d;
            //if (Compare.IsZero(other.b))
            //    x = other.GetXAtY(y);
            //else
            //    y = other.GetYAtX(x);
            //return GetDistanceTo(new Point2d(x, y));
        }

        public double GetSignedDistanceTo(Point point)
        {
            return (a * point.X + b * point.Y + c) / Math.Sqrt(a * a + b * b);
        }

        /// <summary>
        /// Distanza con segno tra rette,
        /// se positiva la seconda retta si trova a destra, se negativa si trova a sinistra (rispetto la direzione della retta originale)
        /// </summary>
        public double GetSignedDistanceTo(StraightLine2D other)
        {
            return GetSignedDistanceTo(other, true);
        }

        /// <summary>
        /// Distanza con segno tra rette,
        /// se positiva la seconda retta si trova a destra, se negativa si trova a sinistra (rispetto la direzione della retta originale)
        /// </summary>
        /// <param name="checkIsParallel">Disattivando il controllo di parallelismo la distanza è data dalla somma delle distanze delle rette dall'origine</param>
        public double GetSignedDistanceTo(StraightLine2D other, bool checkIsParallel)
        {
            //Se le rette non sono parallele la distanza è 0
            if (checkIsParallel && !IsParallelTo(other))
                return 0d;

            //Le rette parallele hanno a e b uguali quindi dipende tutto da c
            // controllo se le rette hanno direzioni inverse
            double otherC = IsInverseTo(other) ? -other.c : other.c;

            return -((otherC / other.NormalizeFactor) - (c / NormalizeFactor));

            // altro metodo: utilizzo la distanza da un punto dell'altra retta
            //double x = 0d;
            //double y = 0d;
            //if (Compare.IsZero(other.b))
            //    x = other.GetXAtY(y);
            //else
            //    y = other.GetYAtX(x);
            //return GetSignedDistanceTo(new Point2d(x, y));
        }

        /// <summary>
        /// Distanza dalla retta parallela passante per l'origine
        /// </summary>
        public double GetDistanceToParallelOnOrigin()
        {
            return Math.Abs(c) / NormalizeFactor;
        }

        /// <summary>
        /// Distanza con segno dalla retta parallela passante per l'origine
        /// se positiva la retta si trova a destra rispetto alla retta passante per l'origine, se negativa si trova a sinistra
        /// </summary>
        public double GetSignedDistanceToParallelOnOrigin()
        {
            return -c / NormalizeFactor;
        }

        /// <summary>
        /// Distanza con segno dalla retta parallela passante per l'origine con direzione della retta normalizzata
        /// se positiva la retta si trova verso x+ / y+ rispetto alla retta passante per l'origine, se negativa si trova verso x- / y-
        /// </summary>
        public double GetSignedDistanceToParallelOnOriginWithNormalizedDirection()
        {
            switch (MathUtil.Compare(a, 0))
            {
                case 1:
                    return -c / NormalizeFactor;
                case -1:
                    return c / NormalizeFactor;
                case 0:
                default:
                    switch (MathUtil.Compare(b, 0))
                    {
                        case 1:
                            return -c / NormalizeFactor;
                        case -1:
                            return c / NormalizeFactor;
                        default:
                            return double.NaN;
                    }
            }
        }

        /// <summary>
        /// Indica da che parte si trova il punto rispetto alla retta (destra, sinistra o coincidente)
        /// La direzione della retta è determinante
        /// </summary>
        public Side GetSideofPoint(Point point, double tol = MathUtil.QUOTE_EPSILON)
        {
            int sign = MathUtil.Compare(a * point.X + b * point.Y + c, 0d, tol);
            Side side = Side.Overlap;
            if (sign > 0)
                side = Side.Right;
            else if (sign < 0)
                side = Side.Left;

            return side;
        }

        /// <summary>
        /// Ritorna il punto sulla retta che dista 'pos' dal punto della proiezione dell'origine sulla retta
        /// </summary>
        public Point GetPointOnLineAtPosition(double pos)
        {
            double normalizeFactor = 1d / NormalizeFactor;
            double an = a * normalizeFactor;
            double bn = b * normalizeFactor;
            double cn = c * normalizeFactor;
            //Per calcolare il punto viene semplicemente moltiplicato il vettore normalizzato della retta per il valore 'pos' e quindi proiettato sulla retta
            return new Point(bn * pos - cn * an, -an * pos - cn * bn);
        }

        public double GetYatX(double x)
        {
            // Ax + By + C = 0
            // Ax + C = -By
            // y = -(Ax + C) / B

            if (MathUtil.IsZero(b))
                return double.NaN;

            return -(a * x + c) / b;
        }

        public double GetXatY(double y)
        {
            // Ax + By + C = 0
            // By + C = -Ax
            // x = -(By + C) / A

            if (MathUtil.IsZero(a))
                return double.NaN;

            return -(b * y + c) / a;
        }

        /// <summary>
        /// Ritorna la distanza del punto sulla retta dal punto della proiezione dell'origine sulla retta
        /// </summary>
        /// <param name="point">Punto sulla retta</param>
        public double GetPositionOnLineOf(Point point)
        {
            double normalizeFactor = 1d / NormalizeFactor;
            double an = a * normalizeFactor;
            double position;
            if (MathUtil.IsZero(b))
                //position = -point.Y * normalizeFactor / a - c * b / normalizeFactor * a;
                position = -point.Y / an; //rimuovo completamente la parte con b in quanto a 0
            else
            {
                double bn = b * normalizeFactor;
                double cn = c * normalizeFactor;
                position = (point.X + cn * an) / bn;
            }

            return position;
        }

        public double GetPositionOnLineOfProjectionOf(Point point)
        {
            return GetPositionOnLineOf(GetProjectedPointOf(point));
        }

        public double GetAbsoluteLinePositionForParallelSortingBottomUp()
        {
            switch (MathUtil.Compare(b, 0))
            {
                case 1:
                    return -c / NormalizeFactor;
                case -1:
                    return c / NormalizeFactor;
                default:
                    switch (MathUtil.Compare(a, 0))
                    {
                        case 1:
                            return -c / NormalizeFactor;
                        case -1:
                            return c / NormalizeFactor;
                        default:
                            return double.NaN;
                    }
            }
        }

        public double GetAbsoluteLinePositionForParallelSortingLeftRight()
        {
            switch (MathUtil.Compare(a, 0))
            {
                case 1:
                    return -c / NormalizeFactor;
                case -1:
                    return c / NormalizeFactor;
                default:
                    switch (MathUtil.Compare(b, 0))
                    {
                        case 1:
                            return -c / NormalizeFactor;
                        case -1:
                            return c / NormalizeFactor;
                        default:
                            return double.NaN;
                    }
            }
        }

        public Point GetProjectedPointOf(Point point)
        {
            // Calcolo l'intersezione della retta perpendicolare passante per il punto dato, codice ottimizzato
            ////a1 = b, b1 = -a;
            //double c1 = a * point.Y - b * point.X;
            //double determinant = -a * a - b * b;
            //return new Point2d((b * c1  + a * c) / determinant, (b * c - a * c1) / determinant);

            // Calcolo della proiezione utilizzando il vettore ortogonale normalizzato,
            // basta aggiungerlo al punto da proiettare dopo averlo moltiplicato per la distanza del punto dalla retta.
            // il codice seguente è ottimizzato per ridurre al minimo il numero di calcoli
            double factor = 1d / (a * a + b * b);
            double dist = -(a * point.X + b * point.Y + c);
            return new Point(point.X + (a * dist) * factor, point.Y + (b * dist) * factor);
        }

        public bool OverlapsWith(Point point, double tolerance = MathUtil.FLT_EPSILON)
        {
            //Se l'equazione è valida con le coordinate del punto allora il punto fa parte della retta
            //Ax + By + C = 0
            return MathUtil.QuoteIsZero(a * point.X + b * point.Y + c, tolerance);
        }

        public bool IntersectsWith(StraightLine2D other, double tolerance = MathUtil.FLT_EPSILON)
        {
            return !IsParallelTo(other);
        }

        public bool IntersectsWithSegment(Point p1, Point p2, double tolerance = MathUtil.FLT_EPSILON)
        {
            StraightLine2D segLine = StraightLine2D.CreateFrom2Points(p1, p2);
            Point intPoint = GetIntersectionPointWith(segLine, tolerance);
            if (intPoint == null)
                return false;

            return intPoint.IsBetweenOrEqual(p1, p2);
        }

        public Intersection GetIntersectionsWith(StraightLine2D line, double tolerance = MathUtil.QUOTE_EPSILON)
        {
            double determinant = a * line.b - line.a * b;
            //Se il determinante è 0 le rette sono parallele (e se hanno almeno un punto in comune sono anche coincidenti)
            if (MathUtil.IsZero(determinant))
                return new Intersection(line.OverlapsWith(GetPointOnLineAtPosition(0), tolerance) ? IntersectionResultType.OverlapOrCoincide : IntersectionResultType.None, new Point(0, 0));

            return new Intersection(IntersectionResultType.Intersect, new Point((b * line.c - line.b * c) / determinant, (line.a * c - a * line.c) / determinant));
        }

        public Point GetIntersectionPointWith(StraightLine2D line, double tolerance = MathUtil.QUOTE_EPSILON)
        {
            double determinant = a * line.b - line.a * b;
            //Se il determinante è 0 le rette sono parallele (e se hanno almeno un punto in comune sono anche coincidenti)
            return MathUtil.IsZero(determinant) ? null : new Point((b * line.c - line.b * c) / determinant, (line.a * c - a * line.c) / determinant);
        }

        public bool Equals(StraightLine2D other)
        {
            return a.Equals(other.a) && b.Equals(other.b) && c.Equals(other.c);
        }

        public bool Equals(StraightLine2D other, double epsilon)
        {
            return MathUtil.QuoteIsEQ(a, other.a, epsilon) && MathUtil.QuoteIsEQ(b, other.b, epsilon) && MathUtil.QuoteIsEQ(c, other.c, epsilon);
        }

        public override string ToString()
        {
            return String.Format(System.Globalization.CultureInfo.InvariantCulture, "({0})X + ({1})Y + ({2}) = 0", a, b, c);
        }

        public string ToString(string format)
        {
            return String.Format(System.Globalization.CultureInfo.InvariantCulture, "({0})X + ({1})Y + ({2}) = 0", a.ToString(format, System.Globalization.CultureInfo.InvariantCulture), b.ToString(format, System.Globalization.CultureInfo.InvariantCulture), c.ToString(format, System.Globalization.CultureInfo.InvariantCulture));
        }

        public static int ParallelComparerBottomUp(StraightLine2D line0, StraightLine2D line1)
        {
            return MathUtil.Compare(line0.GetAbsoluteLinePositionForParallelSortingBottomUp(), line1.GetAbsoluteLinePositionForParallelSortingBottomUp());
        }

        public static int ParallelComparerTopDown(StraightLine2D line0, StraightLine2D line1)
        {
            return -MathUtil.Compare(line0.GetAbsoluteLinePositionForParallelSortingBottomUp(), line1.GetAbsoluteLinePositionForParallelSortingBottomUp());
        }

        public static int ParallelComparerLeftRight(StraightLine2D line0, StraightLine2D line1)
        {
            return MathUtil.Compare(line0.GetAbsoluteLinePositionForParallelSortingLeftRight(), line1.GetAbsoluteLinePositionForParallelSortingLeftRight());
        }

        public static int ParallelComparerRightLeft(StraightLine2D line0, StraightLine2D line1)
        {
            return -MathUtil.Compare(line0.GetAbsoluteLinePositionForParallelSortingLeftRight(), line1.GetAbsoluteLinePositionForParallelSortingLeftRight());
        }
    }
}