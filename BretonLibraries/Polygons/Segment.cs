using System;
using System.Xml;
using System.Globalization;
using Breton.MathUtils;
using TraceLoggers;


namespace Breton.Polygons
{
	public class Segment: Side
	{
		#region Variables

		public Point mP1;			// Punto 1 del lato
		public Point mP2;			// Punto 2 del lato

		#endregion

		#region Constructors

		public Segment()
			: this(0d, 0d, 0d, 0d)
		{ }
		public Segment(double x1, double y1, double x2, double y2)
			: base()
		{
			mP1 = new Point(x1, y1);
			mP2 = new Point(x2, y2);
		}
		public Segment(Point p1, Point p2)
			: base()
		{
			mP1 = new Point(p1);
			mP2 = new Point(p2);
		}
		public Segment(Segment s)
			: base(s)
		{
			mP1 = new Point(s.P1);
			mP2 = new Point(s.P2);
		}

		#endregion 

		#region Properties

		public override Point P1
		{
			get { return mP1; }
		}

		public override Point P2
		{
			get { return mP2; }
		}

		//**************************************************************************
		// Dx
		// Restituisce la differenza DX
		//**************************************************************************
		public override double Dx
		{
			get { return mP2.X - mP1.X; }
		}

		//**************************************************************************
		// Dy
		// Restituisce la differenza DY
		//**************************************************************************
		public override double Dy
		{
			get { return mP2.Y - mP1.Y; }
		}

		//******************************************************************************
		// Length
		// Restituisce la lunghezza del lato
		//******************************************************************************
		public override double Length
		{
			get { return Math.Sqrt(Dx * Dx + Dy * Dy); }
		}

		//**************************************************************************
		// Inclination
		// Restituisce l'angolo del lato rispetto all'asse delle ascisse
		//**************************************************************************
		public override double Inclination
		{
			get
			{
				switch (MathUtil.QuoteCompare(Dx, 0d))
				{
						// Lato nullo
					case 0:
						switch (MathUtil.QuoteCompare(Dy, 0d))
						{
								// Lato nullo
							case 0:
								return 0d;
					        
								// Lato verticale (90�)
							case 1:
								return Math.PI / 2d;
					        
								// Lato verticale (270�)
							case -1:
								return Math.PI * 3d / 2d;
					            
						}
						break;
			        
						// Lato inclinato, sui primi due quadranti
					case 1:
						// Lato orizzontale (0�)
						if (MathUtil.QuoteIsZero(Dy))
							return 0d;
						else
							return Math.Atan2(Dy, Dx);
			        
						// Lato inclinato, sui secondi due quadranti
					case -1:
						// Lato orizzontale (180�)
						if (MathUtil.QuoteIsZero(Dy))
							return Math.PI;
						else
							return Math.Atan2(Dy, Dx); // +Math.PI;
				}

				return 0d;
			}
		}

        //**************************************************************************
		// MiddlePoint
		// Restituisce il punto medio del lato
		//**************************************************************************
		public override Point MiddlePoint
		{
			get	{ return new Point((mP1.X + mP2.X) / 2d, (mP1.Y + mP2.Y) / 2d); }
		}

		//**************************************************************************
		// VectorSide
		// Restituisce il vettore parallelo al lato
		//**************************************************************************
		public override Vector VectorSide
		{
			get { return VectorP1; }
		}

		//**************************************************************************
		// VectorP1
		// Restituisce il vettore tangente nel vertice 1
		//**************************************************************************
		public override Vector VectorP1
		{
			get { return new Vector(mP2.X, mP2.Y, 0d, mP1.X, mP1.Y, 0d); }
		}

		//**************************************************************************
		// VectorP2
		// Restituisce il vettore tangente nel vertice 2
		//**************************************************************************
		public override Vector VectorP2
		{
			get { return new Vector(mP1.X, mP1.Y, 0d, mP2.X, mP2.Y, 0d); }
		}

		//**************************************************************************
		// GetRectangle
		// Restituisce il rettangolo che contiene il lato
		//**************************************************************************
		public override Rect GetRectangle
		{
			get { return new Rect(mP1.X, mP1.Y, mP2.X, mP2.Y); }
		}

        //**************************************************************************
        // VectorDirection
        // Restituisce ladirezione (0-2PI) del vettore associato al segmento
        //**************************************************************************
        public virtual double VectorDirection
        {
            get { return MathUtil.gdDirezione(P1.X,P1.Y,P2.X,P2.Y); }
        }

		#endregion 

		#region Operator overloading

		//******************************************************************************
		// Operatore -
		// Inverte la direzione del segmento
		//******************************************************************************
		public static Segment operator - (Segment s)
		{
			return new Segment(s.P2, s.P1);
		}

		#endregion

		#region Public Methods

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        //******************************************************************************
        // Equals
        // Verifica se due lati sono uguali (come valori)
        //******************************************************************************
        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Segment s = (Segment)obj;
            if (P1.X != s.P1.X ||
                P1.Y != s.P1.Y ||
                P2.X != s.P2.X ||
                P2.Y != s.P2.Y)
                return false;

            return true;
        }

        //******************************************************************************
        // AlmostEquals
        // Verifica se due lati sono uguali (come valori) a meno dell'epsilon di approssimazione
        //******************************************************************************
        public override bool AlmostEquals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Segment s = (Segment)obj;
            if (!P1.AlmostEqual(s.P1) ||
                !P2.AlmostEqual(s.P2))
                return false;

            return true;
        }
        public override bool AlmostEquals(Object obj, double delta)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Segment s = (Segment)obj;
            if (!P1.AlmostEqual(s.P1, delta) ||
                !P2.AlmostEqual(s.P2, delta))
                return false;

            return true;
        }

        //******************************************************************************
		// GetCopy
		// Restituisce una copia del lato
		//******************************************************************************
		public override Side GetCopy()
		{
			return (Side) new Segment(this);
		}

		//******************************************************************************
		// RotoTrasl
		// Restituisce il lato rototraslato
		// Parametri:
		//			rot	: angolo di rototraslazione (in radianti)
		//			xt	: traslazione in x
		//			yt	: traslazione in y
		//******************************************************************************
		public override Side RotoTrasl(RTMatrix matrix)
		{
			double x1, y1, x2, y2;
			matrix.PointTransform(mP1.X, mP1.Y, out x1, out y1);
			matrix.PointTransform(mP2.X, mP2.Y, out x2, out y2);
			return (Side)new Segment(x1, y1, x2, y2);
		}

		//**************************************************************************
		// Normalize
		// Ritorna il lato normalizzato su un punto di offset
		// Parametri:
		//			offset	: punto di normalizzazione
		//**************************************************************************
		public override Side Normalize(Point offset)
		{
			return (Side) new Segment(mP1.X - offset.X, mP1.Y - offset.Y, mP2.X - offset.X, mP2.Y - offset.Y);
		}

		//**************************************************************************
		// Invert
		// Ritorna il lato invertito
		//**************************************************************************
		public override Side Invert()
		{
			return (Side) new Segment(mP2, mP1);
		}

        //**************************************************************************
        // Reverse
        // Inverte il lato corrente
        //**************************************************************************
        public override void Reverse()
        {
            Point p = mP1;
            mP1 = mP2;
            mP2 = p;
        }

		//**************************************************************************
		// StraightLine
		// Restituisce la retta definita dal lato
		// Parametri:
		//			a:	coefficiente variabile X
		//			b:	coefficiente variabile Y
		//			c:	coefficiente costante
		// Restituisce:
		//               true	retta calcolata
		//               false	retta non esistente
		//**************************************************************************
		public override bool StraightLine(out double a, out double b, out double c)
		{
			a = 0d;
			b = 0d;
			c = 0d;
		    
			//Lato di lunghezza 0, non calcola la retta
			if (MathUtil.QuoteIsZero(Length))
				return false;
		    
			//Calcola la retta
			MathUtil.RettaPer2Punti(mP1.X, mP1.Y, mP2.X, mP2.Y, out a, out b, out c);
		    
			return true;

		}

		//**************************************************************************
		// OffsetSide
		// Restituisce un nuovo lato spostato perpendicolarmente di un offset 
		// rispetto al lato
		// Parametri:
		//			offset	: offset di spostamento
		//			verso	: verso di spostamento  > 0 sinistra (rot. CCW)
		//											< 0 destra (rot. CW)
		//**************************************************************************
		public override Side OffsetSide(double offset, int verso)
		{
			//Crea il vettore parallelo al lato
			Vector v = VectorSide;
		    
			//Calcola il vettore perpendicolare al lato, che va verso la sinistra del lato
			if (verso > 0d)
				v = new Vector(-v.Y, v.X);
		        
				//Calcola il vettore perpendicolare al lato, che va verso la destra del lato
			else
				v = new Vector(v.Y, -v.X);

            try
            {
                //Imposta la lunghezza del vettore perpendicolare pari all'offset di spostamento
                v = new Vector(v, offset);
            }
            catch (Exception ex)
            {

            }

			//Calcola gli estremi del nuovo lato spostati
			double x1, y1;
			v.ApplToPoint(mP1.X, mP1.Y, out x1, out y1);

			double x2, y2;
			v.ApplToPoint(mP2.X, mP2.Y, out x2, out y2);

			return (Side) new Segment(x1, y1, x2, y2);
		}

		//******************************************************************************
		// StartPoint
		// Calcola il punto iniziale dell'arco
		// Ritorna:
		//			punto iniziale
		//******************************************************************************
		public override Point StartPoint()
		{
			return new Point(mP1);
		}

		//******************************************************************************
		// EndPoint
		// Calcola il punto finale dell'arco
		// Ritorna:
		//			punto finale
		//******************************************************************************
		public override Point EndPoint()
		{
			return new Point(mP2);
		}
		
		//******************************************************************************
		// CalcPoint
		// Calcola un punto sull'arco
		// Parametri:
		//			tipo	: tipo angolo 0=assoluto, 1=relativo inizio, 2=relativo fine
		//			ang		: angolo
		// Ritorna:
		//			punto calcolato
		//			null se il punto non appartiene all'arco
		//******************************************************************************
		public override Point CalcPoint(int tipo, double ang)
		{
			return null;
		}

		//******************************************************************************
		// CalcAngPoint
		// Calcola l'angolo di un punto sull'arco
		// Parametri:
		//			tipo	: tipo angolo 0=assoluto, 1=relativo inizio, 2=relativo fine
		//			x		: ascissa punto
		//			y		: ordinata punto
		// Ritorna:
		//           angolo calcolato in radianti
		//******************************************************************************
		public override double CalcAngPoint(int tipo, double x, double y)
		{
			return 0d;
		}
		
		//***************************************************************************/
		//                                                                          */
		// Intersect:		   calcola il punto di intersezione, se esiste, tra		*/
		//                     due lati.		                                    */
		//                                                                          */
		// Inputs:   side  = lato con cui verificare le intersezioni				*/
		//           inter = vettore dei punti di intersezione trovati				*/
		//                                                                          */
		// Outputs: -1	= i lati sono sovrapposti.									*/
		//           0	= non ci sono intersezioni.                                 */
		//           >0	= numero di intersezioni trovate	                        */
		//                                                                          */
		//****************************************************************************/
		public override int Intersect(Side side, out Point[] inter)
		{
			int num = 0;

			if (side is Segment)
			{
				inter = new Point[1];
				num = Intersect((Segment) side, out inter[0]);
			}
			else if (side is Arc)
			{
				return side.Intersect(this, out inter);
			}
			else
			{
				inter = null;
				num = 0;
			}

			return num;
		}

		/****************************************************************************
		 * ContainPoint
		 * Verifica se il punto specificato appartiene al segmento.
		 * Parametri:
		 *			p	: punto da controllare.
		 * Ritorna:
		 *			false	il punto non appartiene
		 *			true	il punto appartiene
		 ****************************************************************************/
		public override bool ContainPoint(Point p)
		{
			/* casi notevoli di segmenti orizzontali .... */
			if (MathUtil.IsEQ(mP1.X, mP2.X))
			{
				if (!MathUtil.IsEQ(p.X, mP1.X))
					return false;

				if (MathUtil.IsLT(mP1.Y, mP2.Y)) 
				{
					if (MathUtil.IsLT(p.Y, mP1.Y) || MathUtil.IsGT(p.Y, mP2.Y))
						return false;
				}
				else if (MathUtil.IsLT(p.Y, mP2.Y) || MathUtil.IsGT(p.Y, mP1.Y))
					return false;

				return true;
			}

			/* ... e verticali */
			if (MathUtil.IsEQ(mP1.Y, mP2.Y)) 
			{
				if (!MathUtil.IsEQ(p.Y, mP1.Y))
					return false;

				if (MathUtil.IsLT(mP1.X, mP2.X)) 
				{
					if (MathUtil.IsLT(p.X, mP1.X) || MathUtil.IsGT(p.X, mP2.X))
						return false;
				}
				else if (MathUtil.IsLT(p.X, mP2.X) || MathUtil.IsGT(p.X, mP1.X))
					return false;

				return true;
			}

			/* se nel rettangolo di contenimento */
			if (MathUtil.IsLT(mP1.X, mP2.X)) 
			{
				if (MathUtil.IsLT(p.X, mP1.X)  ||  MathUtil.IsGT(p.X, mP2.X))
					return false;
			}
			else if (MathUtil.IsGT(p.X, mP1.X)  ||  MathUtil.IsLT(p.X, mP2.X))
				return false;

			if (MathUtil.IsLT(mP1.Y, mP2.Y)) 
			{
				if (MathUtil.IsLT(p.Y, mP1.Y)  ||  MathUtil.IsGT(p.Y, mP2.Y))
					return false;
			}
			else if (MathUtil.IsGT(p.Y, mP1.Y)  ||  MathUtil.IsLT(p.Y, mP2.Y))
				return false;

            //Creo il vettore tra il primo punto del segmento e il punto da controllare (da p1 a p)
            Vector vPoint = new Vector(p.X, p.Y, mP1.X, mP1.Y);
            //Creo il vettore del segmento (da p1 a p2)
            Vector vSeg = new Vector(mP2.X, mP2.Y, mP1.X, mP1.Y);
            //Se la distanza � maggiore della lunghezza del segmento il punto non pu� trovarsi nel segmento 
            if (MathUtil.IsGT(vPoint.Magnitude, vSeg.Magnitude))
                return false;
            //Se la distanza dal primo punto del segmento � 0 allora si trova nel segmento
            if (MathUtil.IsZero(vPoint.Magnitude))
                return true;
            vPoint = vPoint.UnitVector;
            vSeg = vSeg.UnitVector;
            //Se il vettore di direzione normalizzato � diverso allora il punto non pu� trovarsi nel segmento
            if (!MathUtil.IsEQ(vPoint.X, vSeg.X) || !MathUtil.IsEQ(vPoint.Y, vSeg.Y))
                return false;

			/* il punto appartiene */
			return true;

		}

        /****************************************************************************
         * ContainPointWithinTolerance
         * Verifica se il punto specificato appartiene al segmento, utilizzando i 
         * controlli con tolleranza estesa per le quote.
         * Parametri:
         *			p	: punto da controllare.
         * Ritorna:
         *			false	il punto non appartiene
         *			true	il punto appartiene
         ****************************************************************************/
        public override bool ContainPointWithinTolerance(Point p)
        {
            /* casi notevoli di segmenti orizzontali .... */
            if (MathUtil.QuoteIsEQ(mP1.X, mP2.X))
            {
                if (!MathUtil.QuoteIsEQ(p.X, mP1.X))
                    return false;

                if (MathUtil.QuoteIsLT(mP1.Y, mP2.Y))
                {
                    if (MathUtil.QuoteIsLT(p.Y, mP1.Y) || MathUtil.QuoteIsGT(p.Y, mP2.Y))
                        return false;
                }
                else if (MathUtil.QuoteIsLT(p.Y, mP2.Y) || MathUtil.QuoteIsGT(p.Y, mP1.Y))
                    return false;

                return true;
            }

            /* ... e verticali */
            if (MathUtil.QuoteIsEQ(mP1.Y, mP2.Y))
            {
                if (!MathUtil.QuoteIsEQ(p.Y, mP1.Y))
                    return false;

                if (MathUtil.QuoteIsLT(mP1.X, mP2.X))
                {
                    if (MathUtil.QuoteIsLT(p.X, mP1.X) || MathUtil.QuoteIsGT(p.X, mP2.X))
                        return false;
                }
                else if (MathUtil.QuoteIsLT(p.X, mP2.X) || MathUtil.QuoteIsGT(p.X, mP1.X))
                    return false;

                return true;
            }

            /* se nel rettangolo di contenimento */
            if (MathUtil.QuoteIsLT(mP1.X, mP2.X))
            {
                if (MathUtil.QuoteIsLT(p.X, mP1.X) || MathUtil.QuoteIsGT(p.X, mP2.X))
                    return false;
            }
            else if (MathUtil.QuoteIsGT(p.X, mP1.X) || MathUtil.QuoteIsLT(p.X, mP2.X))
                return false;

            if (MathUtil.QuoteIsLT(mP1.Y, mP2.Y))
            {
                if (MathUtil.QuoteIsLT(p.Y, mP1.Y) || MathUtil.QuoteIsGT(p.Y, mP2.Y))
                    return false;
            }
            else if (MathUtil.QuoteIsGT(p.Y, mP1.Y) || MathUtil.QuoteIsLT(p.Y, mP2.Y))
                return false;

            //Creo il vettore tra il primo punto del segmento e il punto da controllare (da p1 a p)
            Vector vPoint = new Vector(p.X, p.Y, mP1.X, mP1.Y);
            //Creo il vettore del segmento (da p1 a p2)
            Vector vSeg = new Vector(mP2.X, mP2.Y, mP1.X, mP1.Y);
            //Se la distanza � maggiore della lunghezza del segmento il punto non pu� trovarsi nel segmento 
            if (MathUtil.QuoteIsGT(vPoint.Magnitude, vSeg.Magnitude))
                return false;
            //Se la distanza dal primo punto del segmento � 0 allora si trova nel segmento
            if (MathUtil.QuoteIsZero(vPoint.Magnitude))
                return true;
            vPoint = vPoint.UnitVector;
            vSeg = vSeg.UnitVector;
            //Se il vettore di direzione normalizzato � diverso allora il punto non pu� trovarsi nel segmento
            if (MathUtil.QuoteIsNEQ(vPoint.X, vSeg.X) || MathUtil.QuoteIsNEQ(vPoint.Y, vSeg.Y))
                return false;

            /* il punto appartiene */
            return true;

        }

        /// **************************************************************************
        /// <summary>
        /// Estensione dell'elemento verso rispetto ad uno dei due estremi
        /// </summary>
        /// <param name="which">veritice di riferimento</param>
        /// <param name="howmuch">entita' dello spostamento, verso l'interno se negativo </param>
        /// <returns>
        ///		true	oeprazione eseguita
        ///		false	operazione non possibile
        ///	</returns>
        /// **************************************************************************
        public override bool Extend(EN_VERTEX which, double howmuch)
        {

            // controllo se la lunghezza del segmento e' compatibile
            if (howmuch < 0 && Length <= Math.Abs(howmuch))
                return false;

            // calcolo dei versori
            double dx = Dx / Length;
            double dy = Dy / Length;

            // spostamento effettivo
            if (which == EN_VERTEX.EN_VERTEX_START)
                howmuch = -howmuch;
            dx = dx * howmuch;
            dy = dy * howmuch;

            // correzione finale
            if (which == EN_VERTEX.EN_VERTEX_START)
            {
                mP1.X = mP1.X + dx;
                mP1.Y = mP1.Y + dy;
            }
            else
            {
                mP2.X = mP2.X + dx;
                mP2.Y = mP2.Y + dy;
            }

            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Estrazione di una porzione di lato, a partire dalle distanze dal primo
        /// veritice.
        /// </summary>
        /// <param name="from">distanza iniziale</param>
        /// <param name="to">distanza finale</param>
        /// <returns>
        ///		lato estratto (null se lato nullo)
        ///	</returns>
        /// **************************************************************************
        public override Side Extract(double from, double to)
        {
            if (from < 0)
                from = 0;
            if (to > Length)
                to = Length;
            if (from >= to)
                return null;

            // calcolo dei versori
            double dx = Dx / Length;
            double dy = Dy / Length;

            // spostamento effettivo
            double dx1 = dx * from;
            double dy1 = dy * from;
            double dx2 = dx * to;
            double dy2 = dy * to;

            // creazione del nuovo segmento
            Segment s = new Segment(mP1.X + dx1, mP1.Y + dy1, mP1.X + dx2, mP1.Y + dy2); 
            return s;
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna il punto che si trova alla distanza indicata dall'inziio
        /// del lato
        /// </summary>
        ///<param name="distance">
        ///     distanza alla quale cercare il punto
        ///</param>
        ///<returns>
        ///     punto trovato o null se la distanza e' incongruente
        ///</returns>
        /// **************************************************************************
        public override Point PointAtDistanceFromStart(double distance)
        {
            if (MathUtil.QuoteIsLT(distance, 0) || MathUtil.QuoteIsGT(distance, Length))
                return null;

            return  P1.PuntoDaModuloDirezione(distance, VectorP1.DirectionXY());
        }

        /// **************************************************************************
        /// <summary>
        /// Ritorna la tangente seguendo l'orientazione del path sul punto che si trova alla distanza indicata dall'inziio
        /// del lato
        /// </summary>
        ///<param name="distance">
        ///     distanza alla quale calcolare la tangente dal punto di start del lato
        ///</param>
        ///<returns>
        ///     tangente seguente l'orientazione del lato o null se la distanza e' incongruente secondo MAthUtil.QuoteEpsilon
        ///</returns>
        /// **************************************************************************
        public override Vector TangentAtDistanceFromStart(double distance)
        {
            if (MathUtil.QuoteIsLT(distance, 0) || MathUtil.QuoteIsGT(distance, Length))
                return null;

            return Tangent();
        }

        /// **************************************************************************
        /// <summary>
        ///     Distanza lungo il side dal punto di start al punto <paramref name="end"/>
        /// </summary>
        /// <param name="end">
        ///     punto finale per il calcolo della lunghezza
        /// </param>
        /// <param name="distanceFromStart">
        ///     Distanza lungo il side dal punto di start al punto <paramref name="end"/>
        /// </param>
        /// <returns>
        ///     false se l'operazione non e' stata eseguita
        ///     true altrimenti
        /// </returns>
        /// **************************************************************************
        public override bool DistanceFromStart(Point end, out double distanceFromStart)
        {
            var p1 = P1;
            if (p1 != null)
            {
                distanceFromStart = p1.Distance(end);
                return true;
            }
            else
            {
                distanceFromStart = 0;
                return false;
            }
        }

        /// **************************************************************************
        /// <summary>
        /// Verifica se il lato corrente contiene completamente quello indicato
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato non e' completamente contenuto
        ///     true    il lato indicato e' completamente contenuto.
        ///</returns>
        ///<remarks>
        /// la valutazione viene fatta considerando una distanza massima tra i due
        /// segmenti
        ///</remarks>
        /// **************************************************************************
        public override bool Contains(Side sd, double maxdist)
        {
            if (!(sd is Segment))
                return false;

            Segment s = (Segment)sd;

            double a1, b1, c1;
            StraightLine(out a1, out b1, out c1);

            double a2, b2, c2;
            s.StraightLine(out a2, out b2, out c2);

            if (!MathUtil.IsParallelStraight(a1, b1, c1, a2, b2, c2))
                return false;

            double dist = MathUtil.ParallelStraightDistance(a1, b1, c1, a2, b2, c2);
            if (dist > maxdist)
                return false;

            double x, y;
            MathUtil.ProjectedPointToStraight(a1, b1, c1, s.P1.X, s.P1.Y, out x, out y);
            Point p1 = new Point(x, y);

            MathUtil.ProjectedPointToStraight(a1, b1, c1, s.P2.X, s.P2.Y, out x, out y);
            Point p2 = new Point(x, y);

            if (!ContainPoint(p1))
                return false;

            if (!ContainPoint(p2))
                return false;

            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Verifica se il lato indicato e' uguale a quello corrente, considerando
        /// un parametro di distanza massima
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>
        /// **************************************************************************
        public override bool IsEqual(Side sd, double maxdist)
        {
            if (!(sd is Segment))
                return false;

            Segment s = (Segment)sd;

            double a1, b1, c1;
            StraightLine(out a1, out b1, out c1);

            double a2, b2, c2;
            s.StraightLine(out a2, out b2, out c2);

            if (!MathUtil.IsParallelStraight(a1, b1, c1, a2, b2, c2))
                return false;

            double dist = MathUtil.ParallelStraightDistance(a1, b1, c1, a2, b2, c2);
            if (MathUtil.QuoteIsGT(dist, maxdist))
                return false;

            double pdist = maxdist * Math.Sqrt(2); // / 2.0;

            if (P1.Distance(s.P1) > pdist)
            {
                if (P2.Distance(s.P1) > pdist)
                    return false;
                if (P1.Distance(s.P2) > pdist)
                    return false;
            }
            else
            {
                if (P2.Distance(s.P2) > pdist)
                    return false;
            }

            return true;
        }

        /// **************************************************************************
        /// <summary>
        /// Se i due lati sono omogenei e connessi, ritorna il lato somma dei due.
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>
        /// **************************************************************************
        public override Side Connect(Side sd, double maxdist)
        {
            if (!(sd is Segment))
                return null;

            Side osd = base.Connect(sd, maxdist);
            if (osd != null)
                return osd;

            Segment s = (Segment)sd;

            double a1, b1, c1;
            StraightLine(out a1, out b1, out c1);

            double a2, b2, c2;
            s.StraightLine(out a2, out b2, out c2);

            if (!MathUtil.IsParallelStraight(a1, b1, c1, a2, b2, c2))
                return null;

            double dist = MathUtil.ParallelStraightDistance(a1, b1, c1, a2, b2, c2);
            if (MathUtil.QuoteIsZero(dist) == false)
                return null;

            double dir = Inclination;
            RTMatrix rtm = new RTMatrix(0,0,-dir);
            Segment s1 = (Segment)RotoTrasl(rtm);
            Segment s2 = (Segment)s.RotoTrasl(rtm);
            Segment sr = null;
            if (s1.P1.X > s1.P2.X)
            {
                dir += Math.PI;
                rtm = new RTMatrix(0,0,-dir);
                s1 = (Segment)RotoTrasl(rtm);
            }
            if (s1.P1.X <= s2.P1.X && s2.P1.X <= s1.P2.X)
            {
                if (s2.P2.X < s1.P1.X)
                    sr = new Segment( s2.P2, s1.P2);
                else
                    if (s2.P2.X > s1.P2.X)
                        sr = new Segment( s1.P1, s2.P2);
            }
            else
            {
                if (s1.P1.X <= s2.P2.X && s2.P2.X <= s1.P2.X)
                {
                    if (s2.P1.X < s1.P1.X)
                        sr = new Segment( s2.P1, s1.P2);
                    else
                        if (s2.P1.X > s1.P2.X)
                            sr = new Segment( s1.P1, s2.P1);
                }
            }
            if (sr != null)
            {
                Segment sf = (Segment)sr.RotoTrasl(rtm.InvMat());
                return sf;
            }

            if (P1.Distance(s.P1) <= maxdist)
            {
                Segment sf = new Segment(s.P2, P2);
                return sf;
            }

            if (P1.Distance(s.P2) <= maxdist)
            {
                Segment sf = new Segment(s.P1, P2);
                return sf;
            }

            if (P2.Distance(s.P1) <= maxdist)
            {
                Segment sf = new Segment(P1, s.P2);
                return sf;
            }

            if (P2.Distance(s.P2) <= maxdist)
            {
                Segment sf = new Segment(P1, s.P1);
                return sf;
            }

            // valutazione segemnti sovrapposti
            double dir_1 = VectorDirection;
            double dir_2 = s.VectorDirection;
            if (MathUtil.AngleIsEQ(dir_1, dir_2) == false)
            {
                dir_2 = MathUtil.Angolo_0_2PI_Ex(dir_2 + Math.PI);
            }
            RTMatrix rtm1 = new RTMatrix(0, 0, -dir_1);
            RTMatrix rtm2 = new RTMatrix(0, 0, -dir_2);
            Segment sr1 = (Segment)RotoTrasl(rtm1);
            Segment sr2 = (Segment)s.RotoTrasl(rtm2);
            Point Pminx_1 = sr1.P1;
            Point Pmaxx_1 = sr1.P2;
            Point Pminx_2 = sr2.P1;
            Point Pmaxx_2 = sr2.P2;
            if (sr1.P1.X > sr1.P2.X)
            {
                Pminx_1 = sr1.P2;
                Pmaxx_1 = sr1.P1;
            }
            if (sr2.P1.X > sr2.P2.X)
            {
                Pminx_2 = sr2.P2;
                Pmaxx_2 = sr2.P1;
            }

            if (Pminx_1.X <= Pminx_2.X && Pminx_2.X <= Pmaxx_1.X)
            {
                Segment sfr = new Segment(Pminx_1, Pmaxx_2);
                rtm = new RTMatrix(0, 0, dir_1);
                Segment sf = (Segment)sfr.RotoTrasl(rtm);
                return sf;
            }

            if (Pminx_2.X <= Pminx_1.X && Pminx_1.X <= Pmaxx_2.X)
            {
                Segment sfr = new Segment(Pminx_2, Pmaxx_1);
                rtm = new RTMatrix(0, 0, dir_1);
                Segment sf = (Segment)sfr.RotoTrasl(rtm);
                return sf;
            }

            return null;
        }

        //**************************************************************************
		// ReadFileXml
		// Legge il lato dal file XML
		// Parametri:
		//			n	: nodo XML contenete il lato
		// Ritorna:
		//			true	lettura eseguita con successo
		//			false	errore nella lettura
		//**************************************************************************
		public override bool ReadFileXml(XmlNode n)
		{
            if (!ReadDerivedFileXml(n))
                return false;

			try
			{
				mP1.X = Convert.ToDouble(n.Attributes.GetNamedItem("X1").Value, CultureInfo.InvariantCulture);
                mP1.Y = Convert.ToDouble(n.Attributes.GetNamedItem("Y1").Value, CultureInfo.InvariantCulture);
                mP2.X = Convert.ToDouble(n.Attributes.GetNamedItem("X2").Value, CultureInfo.InvariantCulture);
                mP2.Y = Convert.ToDouble(n.Attributes.GetNamedItem("Y2").Value, CultureInfo.InvariantCulture);
				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Segment.ReadFileXml Error.", ex);
				return false;
			}
		}

        //**************************************************************************
		// WriteFileXml
		// Scrive il lato su file XML
		// Parametri:
		//			w: oggetto per scrivere il file XML
		//**************************************************************************
		public override void WriteFileXml(XmlTextWriter w)
		{

			w.WriteStartElement("Segment");

            w.WriteAttributeString("X1", mP1.X.ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("Y1", mP1.Y.ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("X2", mP2.X.ToString(CultureInfo.InvariantCulture));
            w.WriteAttributeString("Y2", mP2.Y.ToString(CultureInfo.InvariantCulture));

            WriteDerivedFileXml(w);

			w.WriteEndElement();
		}

        ///**************************************************************************
		/// <summary>
		/// Scrive il lato sul nodo XML
		/// </summary>
		/// <param name="baseNode">nodo XML dove scrivere il lato</param>
		///**************************************************************************
		public override void WriteFileXml(XmlNode baseNode)
		{
			XmlDocument doc = baseNode.OwnerDocument;
			XmlAttribute attr;
			XmlNode node = doc.CreateNode(XmlNodeType.Element, "Segment", "");

			attr = doc.CreateAttribute("X1");
            attr.Value = mP1.X.ToString(CultureInfo.InvariantCulture);
			node.Attributes.Append(attr);
			attr = doc.CreateAttribute("Y1");
            attr.Value = mP1.Y.ToString(CultureInfo.InvariantCulture);
			node.Attributes.Append(attr);
			attr = doc.CreateAttribute("X2");
            attr.Value = mP2.X.ToString(CultureInfo.InvariantCulture);
			node.Attributes.Append(attr);
			attr = doc.CreateAttribute("Y2");
            attr.Value = mP2.Y.ToString(CultureInfo.InvariantCulture);
			node.Attributes.Append(attr);

            WriteDerivedFileXml(node);

            baseNode.AppendChild(node);
		}

        ///**************************************************************************
        /// <summary>
        ///     Nodo side del salvataggio di geometria in FKSQL, avente solo le informazioni di Breton.Polygons, con nome elemento=Side
        /// </summary>
        ///**************************************************************************
        public override void WriteSideXml(XmlNode baseNode)
        {
            XmlNode xSide = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Side", "");

            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "X1", "");
                node.InnerText = P1.X.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }
            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Y1", "");
                node.InnerText = P1.Y.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }
            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "X2", "");
                node.InnerText = P2.X.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }
            {
                XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Y2", "");
                node.InnerText = P2.Y.ToString(System.Globalization.CultureInfo.InvariantCulture);
                xSide.AppendChild(node);
            }

            baseNode.AppendChild(xSide);
        }

        /// **************************************************************************
        /// <summary>
        ///     connessione del lato corrente con il successivo in base allla distanza
        ///     indicata.
        /// </summary>
        ///<param name="nextsd">
        ///     lato successivo
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     eventuale lato da aggiungere per completare la connessione
        ///</returns>
        /// **************************************************************************
        public override Side ConnectSides(Side nextsd, double maxdist)
        {
            if (nextsd is Segment)
            {
                double dist = P2.Distance(nextsd.P1);
                if (dist <= maxdist)
                    return  null;

                Point pThis = P2;
                Point pNext = nextsd.P1;
                Point pMid = (pThis + pNext) * 0.5;
                Point inters;
                var current = this as Segment;
                var next = nextsd as Segment;
                int n_inters = new Segment(current.P1 - current.Tangent() * 1000, current.P2 + current.Tangent() * 1000).Intersect(new Segment(next.P1 - next.Tangent() * 1000, next.P2 + next.Tangent() * 1000), out inters);
                Point pInters = null;
                if (n_inters > 0 && inters != null)
                {
                    pInters = inters;
                }
                if (pInters != null && current.Length > 1 && next.Length > 1 && current.P2.Distance(pInters) < 1)
                {
                    //20160624 provo a modificare le due curve in base al punto di intersezione ottenuto estendendo le due curve non connesse
                    current.P2.X = pInters.X;
                    current.P2.Y = pInters.Y;
                    next.P1.X = pInters.X;
                    next.P1.Y = pInters.Y;
                }
                else
                {
                    Point p = new Point((P2.X + nextsd.P1.X) / 2, (P2.Y + nextsd.P1.Y) / 2);
                    P2.X = nextsd.P1.X = p.X;
                    P2.Y = nextsd.P1.Y = p.Y;
                }
                return null;
            }

            if (nextsd is Arc)
            {
                double dist = P2.Distance(nextsd.P1);
                if (dist <= maxdist)
                    return null;
                
                Point pThis = P2;
                Point pNext = nextsd.P1;
                Point pMid = (pThis + pNext) * 0.5;
                Point[] inters;
                var current = this as Segment;
                var next = nextsd as Arc;
                int n_inters = new Segment(current.P1 - current.Tangent() * 1000, current.P2 + current.Tangent() * 1000).Intersect(new Arc(next.CenterPoint, next.Radius, 0, Math.PI * 2), out inters);
                Point pInters = null;
                if (n_inters > 0 && inters != null && !next.IsCircle)
                {
                    if (n_inters == 1 && (inters[0] != null || inters[1] != null))
                    {
                        if (inters[0] != null)
                            pInters = inters[0];
                        else if (inters[1] != null)
                            pInters = inters[1];
                    }
                    else if (inters[0] != null && inters[1] != null)
                    {
                        if (inters[0].Distance(pMid) < inters[1].Distance(pMid))
                            pInters = inters[0];
                        else
                            pInters = inters[1];
                    }
                }
                if (pInters != null && current.Length > 1 && next.Length > 1 && current.P2.Distance(pInters) < 1)
                {
                    //20160624 provo a modificare le due curve in base al punto di intersezione ottenuto estendendo le due curve non connesse
                    current.P2.X = pInters.X;
                    current.P2.Y = pInters.Y;

                    var p0 = pInters;
                    var p1 = next.MiddlePoint;
                    var p2 = next.P2;
                    next.InitArc(p0.X, p0.Y, p1.X, p1.Y, p2.X, p2.Y);
                }
                else
                {
                    //modifico estremit� segmento
                    P2.X = nextsd.P1.X;
                    P2.Y = nextsd.P1.Y;
                }
                return null;
            }

            return null;
        }

        /// **************************************************************************
        /// <summary>
        ///     verficia se i due lati sono omogenei, cioe' sono paralleli.
        /// </summary>
        ///<param name="sd">
        ///     lato da verificare
        ///</param>
        ///<param name="maxdist">
        ///     distanza massima ammessa tra i due lati
        ///</param>
        ///<returns>
        ///     false   il lato indicato e' diverso.
        ///     true    il lato indicato e' uguale a quello corrente.
        ///</returns>        
        /// **************************************************************************
        public override bool Homogeneous(Side sd, double maxdist)
        {
            if (!(sd is Segment))
                return false;

            Segment s = (Segment)sd;

            double a1, b1, c1;
            StraightLine(out a1, out b1, out c1);

            double a2, b2, c2;
            s.StraightLine(out a2, out b2, out c2);

            if (!MathUtil.IsParallelStraight(a1, b1, c1, a2, b2, c2))
                return false;

            double dist = MathUtil.ParallelStraightDistance(a1, b1, c1, a2, b2, c2);
            if (dist > maxdist)
                return false;

            return true;
        }

        /// **************************************************************************
        /// <summary>
        ///     Determina il punto dell'entita' corrente che si trova alla minima 
        ///     distanza rispetto il punto indicato
        /// </summary>
        /// <param name="ref_point">
        ///     punto rispetto il quale determinare la distanza
        /// </param>
        /// <param name="mindist_point">
        ///     punto a minima distanza indentificato nella entita'
        /// </param>
        /// <returns>
        ///     minima distanza calcolata
        /// </returns>
        /// **************************************************************************
        public override double MinDistanceToPoint(Point ref_point, out Point mindist_point)
        {
            mindist_point = null;
            
            double a1, b1, c1;
            if (StraightLine(out a1, out b1, out c1) == false)
                return double.MaxValue;

            double a2, b2, c2;
            MathUtil.PerpendicularStraight( a1, b1, c1, ref_point.X, ref_point.Y, out a2, out b2, out c2);
            
            double xi, yi;
            if (MathUtil.gbIntersFra2Rette(a1, b1, c1, a2, b2, c2, out xi, out yi) == false)
                return double.MaxValue;

            mindist_point = new Point(xi, yi);

            if (this.ContainPoint(mindist_point))
                return mindist_point.Distance(ref_point);

            double dp1 = P1.Distance(ref_point);
            double dp2 = P2.Distance(ref_point);
            if (dp1 < dp2)
            {
                mindist_point = P1;
                return dp1;
            }
            mindist_point = P2;
            return dp2;
        }

        /// **************************************************************************
        /// <summary>
        ///     Determina le due parti risultati dall'interruzione dell'entita' corrente
        ///     sul punto indicato
        /// </summary>
        /// <param name="break_point">
        ///     punto sul quale eseguire l'interruzione
        /// </param>
        /// <param name="sd1">
        ///     prima parte dell'elemento
        /// </param>
        /// <param name="sd2">
        ///     seconda parte dell'elmento
        /// </param>
        /// <returns>
        ///     false se l'operazione non e' stata eseguita
        ///     true altrimenti
        /// </returns>
        /// **************************************************************************
        public override bool BreakAtPoint(Point break_point, out Side sd1, out Side sd2)
        {
            sd1 = null;
            sd2 = null;

            if (ContainPoint(break_point) == false)
                return false;

            Segment sa1 = (Segment)GetCopy();
            Segment sa2 = (Segment)GetCopy();

            sa1.P2.X = break_point.X;
            sa1.P2.Y = break_point.Y;
            sa2.P1.X = break_point.X;
            sa2.P1.Y = break_point.Y;

            sd1 = sa1;
            sd2 = sa2;

            return true;
        }

        /// ********************************************************************
        /// <summary>
        /// Trasforma il side
        /// </summary>
        /// <returns>transform != null</returns>
        /// ********************************************************************
        public override bool Transform(Breton.MathUtils.RTMatrix transform)
        {
            if (transform != null)
            {
                var s = this;
                var mP1 = s.P1;
                var mP2 = s.P2;
                double x1, y1, x2, y2;
                transform.PointTransform(mP1.X, mP1.Y, out x1, out y1);
                transform.PointTransform(mP2.X, mP2.Y, out x2, out y2);
                s.P1.X = x1;
                s.P1.Y = y1;
                s.P2.X = x2;
                s.P2.Y = y2;

                return true;
            }

            return false;
        }

        /// ********************************************************************
        /// <summary>
        /// Tangente sul primo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sul primo punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        public override Vector Tangent()//direzione 1 -> 2
        {
            return new Vector(VectorP1, 1);
        }

        /// ********************************************************************
        /// <summary>
        /// Tangente sull'ultimo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sull'ultimo punto punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        public override Breton.MathUtils.Vector TangentAtEnd()//direzione 1 -> 2
        {
            return new Vector(VectorP1, 1);
        }        
        
        /// ********************************************************************
        /// <summary>
        /// Tangente su coordinata relativa 0 ;&lt t ;&lt 1
        /// </summary>
        /// <returns>tangente su punto definito da 0 ;&lt t ;&lt 1</returns>
        /// ********************************************************************
        public override Breton.MathUtils.Vector TangentAt(double t) //direzione 1 -> 2
        {
            return new Vector(VectorP1, 1);
        }

        /// ********************************************************************
        /// <summary>
        /// Tangente del segmento
        /// </summary>
        /// <param name="x">x dove valutare la tangente</param>
        /// <param name="y">y dove valutare la tangente</param>
        /// <returns>tangente</returns>
        /// ********************************************************************
        public override Vector TangentAtPoint(double x, double y)
        {
            return Tangent();
        }

        /// ********************************************************************
        /// <summary>
        /// Somma di due segmenti
        /// </summary>
        /// <param name="segmentWithDirection">segmento che definisce la direzione risultante</param>
        /// <param name="segmentToAdd">segmento da aggiungere anche se non parallelo si prende il punto min e max proiettato sulla direzione</param>
        /// <returns>somma di due segmenti</returns>
        /// ********************************************************************
        public static Segment Union(Segment segmentWithDirection, Segment segmentToAdd)
        {
            var s1 = segmentWithDirection;
            var origin = s1.P1;
            var direction = s1.Tangent();

            var s2 = segmentToAdd;
            var s2a = s2.P1;
            var s2b = s2.P2;

            var pa = (Vector)(s2a - origin);
            var pb = (Vector)(s2b - origin);

            var l1a = 0;
            var l1b = s1.Length;

            var l2a = Vector.ScalarProduct(pa, direction);
            var l2b = Vector.ScalarProduct(pb, direction);

            var l2Min = Math.Min(l2a, l2b);
            var l2Max = Math.Max(l2a, l2b);

            var p1 = origin + (direction * Math.Min(l1a, l2Min));
            var p2 = origin + (direction * Math.Max(l1b, l2Max));

            return new Segment(p1, p2);
        }
        public static bool ExtendToInclude(Segment segmentToExtend, Breton.Polygons.Point pointToBeIncluded)
        {
            bool changed = false;

            var dSegment = segmentToExtend.Tangent();
            var lSegment = segmentToExtend.Length;

            var p1 = Segment.ProjectToRay(segmentToExtend, pointToBeIncluded);
            var d1 = (Vector)(p1 - segmentToExtend.P1);
            var l1 = d1.Magnitude;
            if (Vector.ScalarProduct(dSegment, d1) < 0)
            {
                segmentToExtend.Extend(EN_VERTEX.EN_VERTEX_START, l1);
                changed = true;
            }
            else if (l1 > lSegment)
            {
                segmentToExtend.Extend(EN_VERTEX.EN_VERTEX_END, l1 - lSegment);
                changed = true;
            }
            return changed;
        }

        /// ********************************************************************
        /// <summary>
        /// Proiezione di pointToProject su ray
        /// </summary>
        /// <param name="ray">raggio su cui si proietta prendendo la sua direzione ortogonale</param>
        /// <param name="pointToProject">punto da proiettare</param>
        /// <returns>punto appartenente alla retta del raggio</returns>
        /// ********************************************************************
        public static Breton.Polygons.Point ProjectToRay(Segment ray, Breton.Polygons.Point pointToProject)
        {
            var s1 = ray;
            var origin = s1.P1;
            var direction = s1.Tangent();
            var orthogonal = new Vector(-direction.Y, direction.X);

            var p0 = s1.P1;
            var p1 = p0 + direction;
            var p2 = pointToProject;
            var p3 = p2 + orthogonal;

            double s1_x, s1_y, s2_x, s2_y;
            s1_x = direction.X;
            s1_y = direction.Y;
            s2_x = orthogonal.X;
            s2_y = orthogonal.Y;

            double t = (s2_x * (p0.Y - p2.Y) - s2_y * (p0.X - p2.X)) / (-s2_x * s1_y + s1_x * s2_y);

            return origin + (direction * t);
        }
        public static Breton.Polygons.Point ProjectToRay(Segment ray, Breton.Polygons.Point pointToProject, Breton.MathUtils.Vector directionToProject)
        {
            var s1 = ray;
            var origin = s1.P1;
            var direction = s1.Tangent();
            var orthogonal = directionToProject;

            var p0 = s1.P1;
            var p1 = p0 + direction;
            var p2 = pointToProject;
            var p3 = p2 + orthogonal;

            double s1_x, s1_y, s2_x, s2_y;
            s1_x = direction.X;
            s1_y = direction.Y;
            s2_x = orthogonal.X;
            s2_y = orthogonal.Y;

            double t = (s2_x * (p0.Y - p2.Y) - s2_y * (p0.X - p2.X)) / (-s2_x * s1_y + s1_x * s2_y);

            return origin + (direction * t);
        }

        /// ********************************************************************
        /// <summary>
        /// Proiezione di pointToProject su segment; se il punto si trova fuori dal segmento, si prende l'estremo pi� vicino
        /// </summary>
        /// <param name="segment">segmento su cui si proietta prendendo la sua direzione ortogonale</param>
        /// <param name="pointToProject">punto da proiettare</param>
        /// <returns>punto appartenente al segmento</returns>
        /// ********************************************************************
        public static Breton.Polygons.Point ProjectToSegment(Segment segment, Breton.Polygons.Point pointToProject)
        {
            var s1 = segment;
            var origin = s1.P1;
            var direction = s1.Tangent();
            var orthogonal = new Vector(-direction.Y, direction.X);

            var p0 = s1.P1;
            var p1 = p0 + direction;
            var p2 = pointToProject;
            var p3 = p2 + orthogonal;

            double s1_x, s1_y, s2_x, s2_y;
            s1_x = direction.X;
            s1_y = direction.Y;
            s2_x = orthogonal.X;
            s2_y = orthogonal.Y;

            double t = (s2_x * (p0.Y - p2.Y) - s2_y * (p0.X - p2.X)) / (-s2_x * s1_y + s1_x * s2_y);

            if (t < 0)
                t = 0;
            else if (t > segment.Length)
                t = segment.Length;

            return origin + (direction * t);
        }
        public static Breton.Polygons.Point ProjectToSegment(Segment segment, Breton.Polygons.Point pointToProject, Breton.MathUtils.Vector directionToProject)
        {
            var s1 = segment;
            var origin = s1.P1;
            var direction = s1.Tangent();
            var orthogonal = directionToProject;

            var p0 = s1.P1;
            var p1 = p0 + direction;
            var p2 = pointToProject;
            var p3 = p2 + orthogonal;

            double s1_x, s1_y, s2_x, s2_y;
            s1_x = direction.X;
            s1_y = direction.Y;
            s2_x = orthogonal.X;
            s2_y = orthogonal.Y;

            double t = (s2_x * (p0.Y - p2.Y) - s2_y * (p0.X - p2.X)) / (-s2_x * s1_y + s1_x * s2_y);

            if (t < 0)
                t = 0;
            else if (t > segment.Length)
                t = segment.Length;

            return origin + (direction * t);
        }

        /// ********************************************************************
        /// <summary>
        /// Distanza del segmento dal raggio
        /// </summary>
        /// <param name="ray">raggio su cui si proietta prendendo la sua direzione ortogonale</param>
        /// <param name="segmentToProject">segmento da proiettare</param>
        /// <param name="minValue">valore minimo o massimo</param>
        /// <returns>Distanza del segmento dal raggio</returns>
        /// ********************************************************************
        public static double DistanceFromRay(Segment ray, Segment segmentToProject, bool minValue = true)
        {
            var p1 = ProjectToRay(ray, segmentToProject.P1);
            var p2 = ProjectToRay(ray, segmentToProject.P2);
            var d1 = segmentToProject.P1.Distance(p1);
            var d2 = segmentToProject.P2.Distance(p2);
            if (minValue)
            {
                if (d1 > MathUtil.FLT_EPSILON && d2 > MathUtil.FLT_EPSILON && Math.Abs(Vector.ScalarProduct(new Segment(p1, segmentToProject.P1).Tangent(), new Segment(p2, segmentToProject.P2).Tangent()) + 1) < MathUtil.FLT_EPSILON)
                    return 0;
                else
                    return Math.Min(d1, d2);
            }
            else
            {
                return Math.Max(d1, d2);
            }
        }
        public static bool DistancesFromRay(Segment ray, Segment segmentToProject, out double min, out double max)
        {
            var p1 = ProjectToRay(ray, segmentToProject.P1);
            var p2 = ProjectToRay(ray, segmentToProject.P2);
            var d1 = segmentToProject.P1.Distance(p1);
            var d2 = segmentToProject.P2.Distance(p2);
            if (d1 > MathUtil.FLT_EPSILON && d2 > MathUtil.FLT_EPSILON && Math.Abs(Vector.ScalarProduct(new Segment(p1, segmentToProject.P1).Tangent(), new Segment(p2, segmentToProject.P2).Tangent()) + 1) < MathUtil.FLT_EPSILON)
                min = 0;
            else
                min = Math.Min(d1, d2);
            max = Math.Max(d1, d2);
            return true;
        }

        /// ********************************************************************
        /// <summary>
        /// Distanza delle estremit� di <paramref name="segment"/> da <paramref name="segment"/>
        /// </summary>
        /// <param name="segmentReference">segmento di riferimento, si proietta sulla sua direzione ortogonale</param>
        /// <param name="segment">segmento da controllare</param>
        /// <param name="minValue">valore minimo o massimo</param>
        /// <param name="checkOnlySegmentToProjectExtremitiesFromSegmentReference">se true si controlla anche il contrario: segmentReference su segment come reference</param>
        /// <returns>Distanza di <paramref name="segment"/> da <paramref name="segment"/></returns>
        /// ********************************************************************
        public static double DistanceFromSegment(Segment segmentReference, Segment segment, bool minValue = true, bool checkOnlySegmentToProjectExtremitiesFromSegmentReference = false)
        {
            double min;
            double max;
            DistancesFromSegment(segmentReference, segment, out min, out max, checkOnlySegmentToProjectExtremitiesFromSegmentReference);
            if (minValue)
                return min;
            else
                return max;
        }
        public static bool DistancesFromSegment(Segment segmentReference, Segment segmentToProject, out double min, out double max, bool checkOnlySegmentToProjectExtremitiesFromSegmentReference = false)
        {
            double d1 = double.MaxValue;
            double d2 = double.MaxValue;
            double d3 = double.MaxValue;
            double d4 = double.MaxValue;
            var p1 = ProjectToSegment(segmentReference, segmentToProject.P1);
            var p2 = ProjectToSegment(segmentReference, segmentToProject.P2);
            d1 = segmentToProject.P1.Distance(p1);//distanza segment.P1 da segmentReference, limitando proiezione di segment.P1 a lunghezza di segmentReference
            d2 = segmentToProject.P2.Distance(p2);
            if (checkOnlySegmentToProjectExtremitiesFromSegmentReference == false)
            {
                var p3 = ProjectToSegment(segmentToProject, segmentReference.P1);
                var p4 = ProjectToSegment(segmentToProject, segmentReference.P2);
                d3 = segmentReference.P1.Distance(p3);
                d4 = segmentReference.P2.Distance(p4);

                if (d1 > MathUtil.FLT_EPSILON && d2 > MathUtil.FLT_EPSILON && Math.Abs(Vector.ScalarProduct(new Segment(p1, segmentToProject.P1).Tangent(), new Segment(p2, segmentToProject.P2).Tangent()) + 1) < MathUtil.FLT_EPSILON)
                    min = 0;
                else if (d3 > MathUtil.FLT_EPSILON && d4 > MathUtil.FLT_EPSILON && Math.Abs(Vector.ScalarProduct(new Segment(p3, segmentReference.P1).Tangent(), new Segment(p4, segmentReference.P2).Tangent()) + 1) < MathUtil.FLT_EPSILON)
                    min = 0;
                else
                    min = Math.Min(d1, Math.Min(d2, Math.Min(d3, d4)));
                max = Math.Max(d1, Math.Max(d2, Math.Max(d3, d4)));
            }
            else
            {
                if (d1 > MathUtil.FLT_EPSILON && d2 > MathUtil.FLT_EPSILON && Math.Abs(Vector.ScalarProduct(new Segment(p1, segmentToProject.P1).Tangent(), new Segment(p2, segmentToProject.P2).Tangent()) + 1) < MathUtil.FLT_EPSILON)
                    min = 0;
                else
                    min = Math.Min(d1, d2);
                max = Math.Max(d1, d2);
            }
            return true;
        }
        public static double DistanceFromPoint(Segment segmentReference, Breton.Polygons.Point point)
        {
            var p1 = ProjectToSegment(segmentReference, point);
            var d = p1.Distance(point);
            return d;
        }
        public static int Intersection(Segment sA, Segment sB, out Point intersection, double eps)
        {
            var pB1 = ProjectToSegment(sA, sB.P1);
            var pB2 = ProjectToSegment(sA, sB.P2);
            var pA1 = ProjectToSegment(sB, sA.P1);
            var pA2 = ProjectToSegment(sB, sA.P2);
            var sANearEndB1 = pB1.Distance(sB.P1) < eps;
            var sANearEndB2 = pB2.Distance(sB.P2) < eps;
            var sBNearEndA1 = pA1.Distance(sA.P1) < eps;
            var sBNearEndA2 = pA2.Distance(sA.P2) < eps;
            //sovrapposizioni dei due segmenti con contenimento di segmento
            if (sANearEndB1 && sANearEndB2)
            {
                intersection = pB1.MidPoint(pB2);//sA contiene i due punti di sB. Si prende come intersezione il punto medio del segmento pi� piccolo: sB
                return 1;
            }
            if (sBNearEndA1 && sBNearEndA2)
            {
                intersection = pA1.MidPoint(pA2);//sB contiene i due punti di sA
                return 1;
            }
            //sovrapposizioni dei due segmenti con contenimento di un estremo per segmento
            if (sANearEndB1 && sBNearEndA1)
            {
                intersection = pB1.MidPoint(pA1);
                return 1;
            }
            if (sANearEndB1 && sBNearEndA2)
            {
                intersection = pB1.MidPoint(pA2);
                return 1;
            }
            if (sANearEndB2 && sBNearEndA1)
            {
                intersection = pB2.MidPoint(pA1);
                return 1;
            }
            if (sANearEndB2 && sBNearEndA2)
            {
                intersection = pB2.MidPoint(pA2);
                return 1;
            }
            //un segmento contiene solo un estremo dell'altro segmento
            if (sANearEndB1)
            {
                intersection = pB1;
                return 1;
            }
            if (sANearEndB2)
            {
                intersection = pB2;
                return 1;
            }
            if (sBNearEndA1)
            {
                intersection = pA1;
                return 1;
            }
            if (sBNearEndA2)
            {
                intersection = pA2;
                return 1;
            }

            var x12 = sA.P1.X - sA.P2.X;
            var x34 = sB.P1.X - sB.P2.X;
            var y12 = sA.P1.Y - sA.P2.Y;
            var y34 = sB.P1.Y - sB.P2.Y;

            var c = x12 * y34 - y12 * x34;

            if (Math.Abs(c) < MathUtil.gdDBLEPSILON)
            {
                // No intersection
                intersection = null;
                return 0;
            }
            else
            {
                // Intersection
                var a = sA.P1.X * sA.P2.Y - sA.P1.Y * sA.P2.X;
                var b = sB.P1.X * sB.P2.Y - sB.P1.Y * sB.P2.X;

                var x = (a * x34 - b * x12) / c;
                var y = (a * y34 - b * y12) / c;

                var p = new Point(x,y);
                var d1 = Vector.ScalarProduct((Vector)(p - sA.P1), sA.Tangent());
                if(d1  < -eps)
                {
                    intersection = null;
                    return 0;
                }
                else if (d1 > sA.Length + eps)
                {
                    intersection = null;
                    return 0;
                }
                else
                {
                    var d2 = Vector.ScalarProduct((Vector)(p - sB.P1), sB.Tangent());
                    if (d2 < -eps)
                    {
                        intersection = null;
                        return 0;
                    }
                    else if (d2 > sB.Length + eps)
                    {
                        intersection = null;
                        return 0;
                    }
                    else
                    {
                        if (double.IsInfinity(p.X) || double.IsInfinity(p.Y))
                        {
                            intersection = null;
                            return 0;
                        }
                        else
                        {
                            intersection = p;
                            return 1;
                        }
                    }
                }
            }
        }
        public override int EpsIntersection(Side side, out Point[] inter, double eps = MathUtil.FLT_EPSILON)
        {
            int num = 0;

            if (side is Segment)
            {
                inter = new Point[1];
                num = Intersection(this, (Segment)side, out inter[0], eps);
            }
            else if (side is Arc)
            {
                return side.Intersect(this, out inter);
            }
            else
            {
                inter = null;
                num = 0;
            }

            return num;
        }

        /// <summary>
        /// angolo fra i due vettori, in radianti, tra 0 e 180�
        /// </summary>
        public static double AngleBetweenDirections(Segment s1, Segment s2)
        {
            return s1.VectorSide.Angle(s2.VectorSide);
        }

        public override Breton.Polygons.Point GetNearestPoint(Breton.Polygons.Point pointToProject)
        {
            return Segment.ProjectToSegment(this, pointToProject);
        }

        public override Breton.Polygons.Side GetSubSide(double lStart, double lEnd)
        {
            double l_tot = Length;
            if (lStart < 0)
                lStart = 0;
            if (lEnd > l_tot)
                lEnd = l_tot;
            var start = P1;
            var dir = Tangent();
            return new Breton.Polygons.Segment(start + (dir * lStart), start + (dir * lEnd));
        }

        #endregion

		#region Private Methods

		//***************************************************************************/
		//                                                                          */
		// Intersect:		   calcola il punto di intersezione, se esiste, tra		*/
		//                     due lati.		                                    */
		//                                                                          */
		// Inputs:   seg2  = puntatore alla coppia di punti del primo segmento.     */
		//           inter = puntatore al punto di intersezione trovato.            */
		//                                                                          */
		// Outputs: -1  = i segmenti sono sovrapposti.                              */
		//           0  = non ci sono intersezioni.                                 */
		//           1  = trovato il punto  di intersezione.                        */
		//                                                                          */
		//****************************************************************************/
		private int Intersect(Segment seg2, out Point inter)
		{
			double m1, m2, m, q1, q2, q;
			double[] over = new double[4];

			inter = new Point();

			/* differenze coordinate nei due segmenti */
			double dx1 = this.Dx;
			double dy1 = this.Dy;
			double dx2 = seg2.Dx;
			double dy2 = seg2.Dy;

            Segment v = null, h = null;

			/* vautazione sul primo segmento */
			if (Math.Abs(dx1) > Math.Abs(dy1)) 
			{
				/* parametri prima retta */
				m1 = dy1 / dx1;
				q1 = mP2.Y - m1 * mP2.X;

				/* vautazione sul secondo segmento */
				if (Math.Abs(dx2) > Math.Abs(dy2)) 
				{

					/* calcolo parametri rette */
					m2 = dy2 / dx2;
					q2 = seg2.P2.Y - m2 * seg2.P2.X;

					/* controllo se parallele */
					if (MathUtil.IsEQ(m1, m2)) 
					{
						/* valuto se le rette sono sovrapposte */
						if (MathUtil.IsEQ(q1, q2)) 
						{
							/* segmenti in X ordinati */
							over[0] = Math.Min(mP1.X, mP2.X);
							over[1] = Math.Max(mP1.X, mP2.X);

							over[2] = Math.Min(seg2.P1.X, seg2.P2.X);
							over[3] = Math.Max(seg2.P1.X, seg2.P2.X);

							/* controllo ordinamento */
							if (over[1] < over[2]  ||  over[3] < over[0])
								return   0;

							/* sono parzialmente sovrapposti */
							return   -1;
						}

						/* sono sicuramente disgiunti */
						return   0;

					}

					/* punto di intersezione */
					inter.X = (q2 - q1) / (m1 - m2);
					inter.Y = inter.X * m1 + q1;

				}
				else 
				{

					/* calcolo parametri rette */
					m2 = dx2 / dy2;
					q2 = seg2.P2.X - m2 * seg2.P2.Y;

					/* controllo se perpendicolari */
					if ((MathUtil.IsZero(m1) && MathUtil.IsZero(m2)) ||
						MathUtil.IsEQ(Math.Abs(m1 * m2), 1d)) 
					{

						/* se verticale */
						if (MathUtil.IsZero(m2)) 
						{
							/* le coordinate sono le intercette ! */
							inter.X = (seg2.P1.X + seg2.P2.X) / 2d;
							inter.Y = (mP1.Y + mP2.Y) / 2d;

                            h = this;
                            v = seg2;
						}
						else 
						{
							/* mi riporto in una condizione y = f(x) per la seconda retta */
							m = 1d / m2;
							q = - q2 / m2;
							inter.X = (q - q1) / (m1 - m);
							inter.Y = inter.X * m1 + q1;
						}
					}
					else 
					{
						/* punto di intersezione */
						inter.Y = (m1 * q2 + q1) / (1d - m1 * m2);
						inter.X = inter.Y * m2 + q2;
					}
				}
			}
			else 
			{
				/* parametri prima retta */
				m1 = dx1 / dy1;
				q1 = mP2.X - m1 * mP2.Y;

				/* tipo della seconda retta */
				if (Math.Abs(dx2) <= Math.Abs(dy2)) 
				{
					/* calcolo parametri rette */
					m2 = dx2 / dy2;
					q2 = seg2.P2.X - m2 * seg2.P2.Y;

					/* x = f(y): controllo se parallele */
					if (MathUtil.IsEQ(m1, m2)) 
					{
						/* valuto se le rette sono sovrapposte */
						if (MathUtil.IsEQ(q1, q2)) 
						{
							over[0] = Math.Min(mP1.Y, mP2.Y);
							over[1] = Math.Max(mP1.Y, mP2.Y);

							over[2] = Math.Min(seg2.P1.Y, seg2.P2.Y);
							over[3] = Math.Max(seg2.P1.Y, seg2.P2.Y);

							/* controllo ordinamento */
							if (over[1] < over[2]  ||  over[3] < over[0])
								return   0;

							/* sono parzialmente sovrapposti */
							return   -1;
						}

						/* sono sicuramente disgiunti */
						return   0;
					}

					/* punto di intersezione */
					inter.Y = (q2 - q1) / (m1 - m2);
					inter.X = inter.Y * m1 + q1;

				}
				else 
				{

					/* calcolo parametri rette */
					m2 = dy2 / dx2;
					q2 = seg2.P2.Y - m2 * seg2.P2.X;

					/* y = f(x): controllo se perpendicolari */
					if ((MathUtil.IsZero(m1) && MathUtil.IsZero(m2)) ||
						MathUtil.IsEQ(Math.Abs(m1 * m2), 1d)) 
					{

						/* se verticale */
						if (MathUtil.IsZero(m1)) 
						{
							/* le coordinate sono le intercette ! */
							inter.X = (mP1.X + mP2.X) / 2d;
                            inter.Y = (seg2.P1.Y + seg2.P2.Y) / 2d;

                            h = seg2;
                            v = this;
						}
						else 
						{

							/* mi riporto in una condizione y = f(x) per la seconda retta */
							m = 1d / m1;
							q = - q1 / m1;
							inter.X = (q - q2) / (m2 - m);
							inter.Y = inter.X * m2 + q2;
						}
					}
					else 
					{
						/* punto di intersezione */
						inter.X = (m1 * q2 + q1) / (1d - m1 * m2);
						inter.Y = inter.X * m2 + q2;
					}
				}
			}

            if (h != null && v!= null)
            {
                //20160204 condizione quasi ortogonale per entrambi i segmenti: con un segmento lungo 10 volte l'altro e un'inclinazione quasi orizzontale e verticale dei due segmenti,
                //pu� succedere che il punto di intersezione calcolato stia al di fuori del FLT_EPSILON con i metodi ContainPoint(inter) dell'else, 
                //ma sia dello stesso ordine di grandezza (p.es. segmenti Dx=10603.699999999952;Dy=-0.0010240052333756466 e Dx=-0.00012989791730433353;Dy=-1337.700570299317 che si intersecano vicino ad una estremit� del segmento pi� lungo)
                Breton.Polygons.Point pMinH, pMaxH, pMinV, pMaxV;
                if (h.P1.X < h.P2.X)
                {
                    pMinH = h.P1;
                    pMaxH = h.P2;
                }
                else
                {
                    pMinH = h.P2;
                    pMaxH = h.P1;
                }
                if (v.P1.Y < v.P2.Y)
                {
                    pMinV = v.P1;
                    pMaxV = v.P2;
                }
                else
                {
                    pMinV = v.P2;
                    pMaxV = v.P1;
                }

                if ((!(MathUtil.IsLE(pMinH.X, v.MiddlePoint.X) && MathUtil.IsGE(pMaxH.X, v.MiddlePoint.X))) || //segmento verticale ha punto centrale fuori dal range del segmento orizzontale
                    (!(MathUtil.IsLE(pMinV.Y, h.MiddlePoint.Y) && MathUtil.IsGE(pMaxV.Y, h.MiddlePoint.Y))))   //segmento orizzontale ha punto centrale fuori dal range del segmento verticale
                    return 0;
            }
            else
            {
                /* controllo finale: appartenza in X al primo segmento */
                if (!ContainPoint(inter))
                    return 0;

                /* appartenza in X al secondo segmento */
                if (!seg2.ContainPoint(inter))
                    return 0;
            }

			/* una intersezione e` valida se il punto trovato non e` */
			/* contemporaneamente estremo di entrambe le entita`     */
			if (IsConcurrentExtremity(inter, mP1, mP2, seg2.P1, seg2.P2))
				return   0;

			/* intersezione valida */
			return   1;
		}

        /// <summary>
        /// Calcoli di inizializzazione del segmento
        /// </summary>
        /// <param name="x1">x primo punto</param>
        /// <param name="y1">y primo punto</param>
        /// <param name="x2">x secondo punto</param>
        /// <param name="y2">y secondo punto</param>
        public string InitSegment(double x1, double y1, double x2, double y2)
        {
            mP1 = new Point(x1, y1);
            mP2 = new Point(x2, y2);

            return string.Empty;
        }

		#endregion

	}
}


