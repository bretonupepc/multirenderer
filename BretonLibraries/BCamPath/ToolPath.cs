﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class ToolPath : Path
    {
        #region Constants
        private const double MAX_DIST_CONNECT_TOOLPATH_ENTITIES = 1;
        #endregion

        #region Variables
        private const string sLinkToTecnoPathKey = "__LINK_TO_TECNO_PATH_";
        private ToolPathInfo clToolPathInfo;
        private TecnoPath clTecnoPath;
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default constructor
        /// </summary>
        ///*********************************************************************
        public ToolPath()
            : base()
        {
            clToolPathInfo = new ToolPathInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public ToolPath(ToolPath p)
            : base(p)
        {
            clToolPathInfo = new ToolPathInfo(p.Info);
            clTecnoPath = p.clTecnoPath;
        }

        ///*********************************************************************
        /// <summary>
        /// Base Copy Constructor
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public ToolPath(Path p)
            : base(p)
        {
            clToolPathInfo = new ToolPathInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il poligono di un rettangolo
        /// </summary>
        /// <param name="r">rettangolo</param>
        ///*********************************************************************
        public ToolPath(Rect r)
            : base(r)
        {
            clToolPathInfo = new ToolPathInfo();
        }

        #endregion

        #region Properties
        //**************************************************************************
        // []
        // operatore parantesi quadre per accedere direttamente alla lista iToolPathSide
        //**************************************************************************
        public new IToolPathSide this[int index]
        {
            get
            {
                if (base[index] is ToolPathArc)
                    return (IToolPathSide)((ToolPathArc)base[index]);
                else
                    return (IToolPathSide)((ToolPathSegment)base[index]);
            }
        }

        //**************************************************************************
        // Impostazione delle informazioni specifiche del percorso
        //**************************************************************************
        public ToolPathInfo Info
        {
            get { return clToolPathInfo; }
            set { clToolPathInfo = new ToolPathInfo(value); }
        }

        //**************************************************************************
        // Ritorna il riferimento al nome del TecnoPath che ha generato questo percorso
        //**************************************************************************
        public string LinkToTecnoPathKey
        {
            get { return this[sLinkToTecnoPathKey]; }
            set { this[sLinkToTecnoPathKey] = value; }
        }

        //**************************************************************************
        // Ritorna il riferimento al TecnoPath che ha generato questo percorso
        //**************************************************************************
        public TecnoPath LinkToTecnoPath
        {
            get { return clTecnoPath; }
            set { clTecnoPath = value; }
        }
        #endregion

        #region Public Methods

        //**************************************************************************
        // AddSide
        // Aggiunge un lato al percorso
        // Parametri:
        //			s	: lato da aggiungere
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public override int AddSide(Side s)
        {
            if (!IsToolPathSide(s))
                return base.AddSide(Clone(s));

            return base.AddSide(s);
        }

        //**************************************************************************
        // AddSide
        // Aggiunge un lato al percorso
        // Parametri:
        //			s	: lato da aggiungere
        //          pos : posozione di inserimento
        // Restituisce:
        //			Numero lati del percorso
        //**************************************************************************
        public override int AddSide(Side s, int pos)
        {
            if (!IsToolPathSide(s))
                return base.AddSide(Clone(s), pos);

            return base.AddSide(s, pos);
        }

        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public override bool ReadDerivedFileXml(XmlNode n)
        {
            if (clToolPathInfo != null)
                return clToolPathInfo.ReadFileXml(n);
            return true;
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlTextWriter w)
        {
            if (clToolPathInfo != null)
                clToolPathInfo.WriteFileXml(w);
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlNode baseNode)
        {
            if (clToolPathInfo != null)
                clToolPathInfo.WriteFileXml(baseNode);
        }

        ///******************************************************************************
        /// <summary>
        ///     Connessione degli elementi interni del percorso; ogni coppia consecutiva
        ///     di elementi viene valutata rispetto la distanza massima indicata; se la 
        ///     distanza e' superiore, si procede con la connesione dei due elementi, 
        ///     operazione che dipende dal tipo dei due elementi; il controllo, se richiesto,
        ///     viene esteso anche tra il primo e l'ultimo elemento, al fine di chiudere il 
        ///     percorso. Per quest'ultima chiusura osservare il significato del parametro
        ///     'close_dist'.
        /// </summary>
        /// <param name="connection_dist">
        ///     distanza massima ammessa per definire connessi due elementi
        /// </param>
        /// <param name="close_dist">
        ///     indica la distanza minima tra primo ed ultimo punto al di sopra della quale
        ///     si forza la chiusura del percorso.
        /// </param>
        ///******************************************************************************
        public override void Connect(double connection_dist, double close_dist)
        {
            // se il numero di elementi e' inferiore a due non si esegue alcun controllo
            if (Count < 2)
                return;

            // per tutte le coppie di elementi
            for (int i = 0; i < Count - 1; i++)
            {
                Side s = mSides[i].ConnectSides(mSides[i + 1], connection_dist);
                if (s != null)
                {
                    IToolPathSide tps;
                    if (s is Arc)
                        tps = new ToolPathArc((Arc)s, this[i].Info);
                    else
                        tps = new ToolPathSegment((Segment)s, this[i].Info);

                    mSides.Insert(i + 1, (Side)tps);
                    i++;
                }
            }

            // controllo primo / ultimo elemento
            if (this[0].Info.SideType != EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADIN &&
                this[0].Info.SideType != EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADOUT &&
                this[Count - 1].Info.SideType != EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADIN &&
                this[Count - 1].Info.SideType != EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADOUT)
            {
                double dist = mSides[Count - 1].P2.Distance(mSides[0].P1);
                if (dist > close_dist && dist < MAX_DIST_CONNECT_TOOLPATH_ENTITIES)
                {
                    Side s = mSides[Count - 1].ConnectSides(mSides[0], connection_dist);
                    if (s != null)
                    {
                        IToolPathSide tps;
                        if (s is Arc)
                            tps = new ToolPathArc((Arc)s, this[Count - 1].Info);
                        else
                            tps = new ToolPathSegment((Segment)s, this[Count - 1].Info);
                        mSides.Add((Side)tps);
                    }
                }
            }
        }

        ///**************************************************************************
        /// <summary>
		/// Crea un path uguale a quello corrente
		/// </summary>
		/// <returns></returns>
        ///**************************************************************************
        public override Path Clone()
		{
			return (Path)new ToolPath(this);
		}

        ///**************************************************************************
        /// <summary>
        /// RecoverLinkToTecnoPath:
        ///     a partire da una lista di percorsi originali, ricrea il link con
        ///     l'esatto percorso che ha generato questo percorso utensile
        /// </summary>
        /// <returns></returns>
        ///**************************************************************************
        public bool RecoverLinkToTecnoPath(TecnoPaths tcps)
        {
            for (int i = 0; i < tcps.Count; i++)
            {
                if (tcps[i].Name == LinkToTecnoPathKey)
                {
                    LinkToTecnoPath = tcps[i];
                    return PropagateSideLink();
                }
            }

            return false;
        }

        ///**************************************************************************
        /// <summary>
        /// SaveLintoToTecnoPath:
        ///     Salva il riferimento al percorso che ha originato questo percorso
        ///     utensile.
        /// </summary>
        /// <param name="tcp">
        ///     riferimento al percorso originale che genera questo percorso utensile
        /// </param>
        ///**************************************************************************
        public void SaveLintoToTecnoPath(TecnoPath tcp)
        {
            LinkToTecnoPathKey = tcp.Name;
            LinkToTecnoPath = tcp;
        }

        ///**************************************************************************
        /// <summary>
        ///     ridefinisce se possibile il punto iniziale dei percorsi considerando
        ///     il punto di approccio indicato
        /// </summary>
        /// <param name="x">
        ///     coordinata X del punto di approccio
        /// </param>
        /// <param name="y">
        ///     coordinata X del punto di approccio
        /// </param>
        /// <param name="added_sides">
        ///     nes caso si stata introdotta una interruzione sulle entita' originali
        ///     indica le nuove entita' introdotte
        /// </param>
        /// <returns>
        ///     false se l'elaborazione no e' stata eseguita
        ///     true altrimenti
        /// </returns>
        ///**************************************************************************
        public bool ApplyApproachPoint(double x, double y, ref List<Side> added_sides)
        {
            // si applica solo su percorsi chiusu
            if (IsClosed == false)
                return true;

            // ricerca l'entita' piu' vicina al punto indicato
            Point ref_point = new Point(x,y);
            double bdist = double.MaxValue;
            int bidx = -1;
            Point bpoint = null;
            for (int i = 0; i < Count; i++)
            {
                Point mindist_point;
                double dist = ((Side)this[i]).MinDistanceToPoint(ref_point, out mindist_point);
                if (dist < bdist)
                {
                    bdist = dist;
                    bidx = i;
                    bpoint = mindist_point;
                }
            }

            // travato un punto ottimale di approccio ?
            if (bidx < 0)
                return true;

            // il punto ottimale e' il primo dell'elemento trovato ?
            if (this[bidx].FirstPoint.AlmostEqual(bpoint))
            {
                // si spostano a fine lista tutti gli elementi precedenti
                while (bidx-- > 0)
                {
                    IToolPathSide its = this[0];
                    this.RemoveSide(0);
                    this.AddSide((Side)its);
                }
            }
            else
            {
                // il punto ottimale e' il secondo dell'elemento trovato ?
                if (this[bidx].LastPoint.AlmostEqual(bpoint))
                {
                    // essendo un percorso chiuso, corrisponde al primo punto dell'entita' successiva
                    if (++bidx >= Count)
                        bidx = 0;

                    // si spostano a fine lista tutti gli elementi precedenti
                    while (bidx-- > 0)
                    {
                        IToolPathSide its = this[0];
                        this.RemoveSide(0);
                        this.AddSide((Side)its);
                    }
                }
                else
                {
                    // si deve interrompere l'entita'
                    Side sd1, sd2;
                    if (((Side)this[bidx]).BreakAtPoint(bpoint, out sd1, out sd2))
                    {
                        // si inseriscono in sequenza
                        AddSide(sd2, bidx + 1);
                        AddSide(sd1, bidx + 1);
                        DeleteSide(bidx);
                        added_sides.Add(sd1);
                        added_sides.Add(sd2);
                        
                        // essendo un percorso chiuso, corrisponde al primo punto dell'entita' successiva
                        if (++bidx >= Count)
                            bidx = 0;

                        // si spostano a fine lista tutti gli elementi precedenti
                        while (bidx-- > 0)
                        {
                            IToolPathSide its = this[0];
                            this.RemoveSide(0);
                            this.AddSide((Side)its);
                        }
                    }
                }
            }

            return true;
        }
        #endregion

        #region Private Methods

        ///**************************************************************************
        ///
        /// <summary>IsToolPathSide: 
        ///     Test if object is a kind of ToolPathSide type</summary>
        /// <param name="s">
        ///     object to be tested</param>
        /// <returns>
        ///     true 
        ///         if object is a ToolPathSide side type
        ///     false
        ///         otherwise
        /// </returns>
        ///
        ///**************************************************************************
        private bool IsToolPathSide(Side s)
        {
            if (s is ToolPathArc || s is ToolPathSegment)
                return true;

            return false;
        }

        ///**************************************************************************
        ///
        /// <summary>Clone
        ///     Clone a non ToolPathSide side object to a proper ToolPathSide one</summary>
        /// <param name="s">
        ///     object to be cloned</param>
        /// <returns>cloned object</returns>
        ///
        ///**************************************************************************
        private Side Clone(Side s)
        {
            if (s is Arc)
                return new ToolPathArc((Arc)s);

            if (s is Segment)
                return new ToolPathSegment((Segment)s);

            return s.GetCopy();
        }

        ///**************************************************************************
        ///
        /// <summary>PropagateSideLink
        ///     ricostruzione dei link tra elementi dei percorsi originali ed
        ///     entita' nei percorsi utensile</summary>
        /// <returns>
        ///     true
        ///         se tutti gli elementi del percorso utensile hanno trovato un link
        ///     false
        ///         almeno un elemento non ha riferimento nel percorso originale
        /// </returns>
        ///
        ///**************************************************************************
        private bool PropagateSideLink()
        {
            bool rc = true;

            for (int i = 0; i < Count; i++)
            {
                ToolPathSideInfo tpsi = this[i].Info;

                if (LinkToTecnoPath.Count > tpsi.SideNumber)
                    tpsi.OriginalSide = LinkToTecnoPath.GetSide(tpsi.SideNumber);
                else
                    rc = false;
            }
            return rc;
        }

        #endregion

        #region Protected Methods

        ///**************************************************************************
        /// <summary>
        /// CreateSegment: create a new segment object
        /// </summary>
        /// <remarks>created segment</remarks> 
        //**************************************************************************
        protected override Segment CreateSegment()
        {
            return (Segment)new ToolPathSegment();
        }

        ///**************************************************************************
        /// <summary>
        /// CreateSegment: create a new segment object
        /// </summary>
        /// <param name="p1">first segment's point</param>
        /// <param name="p2">second segment's point</param>
        /// <remarks>created segment</remarks> 
        //**************************************************************************
        protected override Segment CreateSegment(Point p1, Point p2)
        {
            return (Segment)new ToolPathSegment(p1, p2);
        }

        ///**************************************************************************
        /// <summary>
        /// CreateArc: create a new arc object
        /// </summary>
        /// <remarks>created arc</remarks> 
        //**************************************************************************
        protected override Arc CreateArc()
        {
            return (Arc)new ToolPathArc();
        }

        #endregion
    }
}
