﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;
using TraceLoggers;

namespace Breton.BCamPath
{
    ///**************************************************************************
    /// <summary>
    ///     Classe per organizzare i percorsi utensili ordinati
    ///     per tipo di utensile utilizzato
    /// </summary>
    ///**************************************************************************
    public class ToolPathsByTool : Dictionary<ToolInfo, ToolPaths>
    {
        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     Aggiunge una lista di percorsi che utilizzano un solo utensile
        ///     alla lista ordinata interna.
        /// </summary>
        /// <param name="tool">
        ///     utensile utilizzato, chiave della lista interna
        /// </param>
        /// <param name="tps">
        ///     elenco di percorsi da aggiungere
        /// </param>
        /// <remarks>
        ///     questo metodo sostituisce quello della classe base in modo
        ///     da gestire autonomamente il caso in cui la chiave esista gia'.
        /// </remarks>
        ///**************************************************************************
        new public void Add(ToolInfo tool, ToolPaths tps)
        {
            ToolPaths cltp;

            try
            {
                if (TryGetValue(tool, out cltp))
                {
                    for (int i = 0; i < tps.Count; i++)
                        cltp.AddPath(tps[i]);
                }
                else
                    base.Add(tool, tps);
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + "Add Exception: " + ex.Message);
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Scrittura di tutti i percorsi contenuti nella lista, secondo
        ///     l'ordine delle chiavi inserite.
        /// </summary>
        /// <param name="w">
        ///     stream di uscita
        /// </param>
        ///************************************************************************** 
        public virtual void WriteFileXml(XmlTextWriter w)
        {			
            foreach (var tpe in this)
            {
                tpe.Value.WriteFileXml(w);
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Scrittura di tutti i percorsi contenuti nella lista, secondo
        ///     l'ordine delle chiavi inserite.
        /// </summary>
        /// <param name="baseNode">
        ///     nodo XML dove scrivere il path
        /// </param>
        ///**************************************************************************
        public void WriteFileXml(XmlNode baseNode)
        {
            foreach (var tpe in this)
            {
                tpe.Value.WriteFileXml(baseNode, true);
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Scrittura di tutti i percorsi contenuti nella lista, secondo
        ///     l'ordine delle chiavi inserite.
        /// </summary>
        /// <param name="baseNode">
        ///     nodo XML dove scrivere il path
        /// </param>
        /// <param name="saveMatrix">
        ///     salva la matrice di rototraslazione
        /// </param>
        ///**************************************************************************
        public void WriteFileXml(XmlNode baseNode, bool saveMatrix)
        {
            foreach (var tpe in this)
            {
                tpe.Value.WriteFileXml(baseNode, saveMatrix, false);
            }
        }

        ///************************************************************************************
        /// <summary>
        ///     Scrittura di tutti i percorsi contenuti nella lista, secondo
        ///     l'ordine delle chiavi inserite.
        /// </summary>
        /// <param name="baseNode">
        ///     nodo XML dove scrivere il path
        /// </param>
        /// <param name="saveMatrix">
        ///     salva la matrice di rototraslazione
        /// </param>
        /// <param name="savePathMatrix">
        ///     salva la matrice di rototraslazione per i path
        /// </param>
        ///*************************************************************************************
        public void WriteFileXml(XmlNode baseNode, bool saveMatrix, bool savePathMatrix)
        {
            foreach (var tpe in this)
            {
                tpe.Value.WriteFileXml(baseNode, saveMatrix, savePathMatrix);
            }
        }

        #endregion
    }
}
