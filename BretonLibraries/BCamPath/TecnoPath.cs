﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Globalization;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class TecnoPath : Path
    {
        #region Variables
        private TecnoPathInfo clTecnoPathInfo;
		#endregion

		#region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Costructor
		/// </summary>
        ///*********************************************************************
        public TecnoPath() : base()
		{
            clTecnoPathInfo = new TecnoPathInfo();
		}

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public TecnoPath(TecnoPath p) : this( (Path)p )
		{
            clTecnoPathInfo = new TecnoPathInfo(p.Info);
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Base Constructor
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public TecnoPath(Path p)
            : base(p)
        {
            clTecnoPathInfo = new TecnoPathInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il poligono di un rettangolo
        /// </summary>
        /// <param name="r">rettangolo</param>
        ///*********************************************************************
        public TecnoPath(Rect r) : base( r )
        {
            clTecnoPathInfo = new TecnoPathInfo();
        }
			
		//Distruttore
		~TecnoPath()
		{
		}

		#endregion

		#region Properties
        //**************************************************************************
        // []
        // operatore parantesi quadre per accedere direttamente alla lista iTecnoSide
        //**************************************************************************
        public new ITecnoSide this[int index]
        {
            get
            {
                if (base[index] is TecnoArc)
                    return (TecnoArc)base[index];
                else
                    return (TecnoSegment)base[index];
            }
        }

		//**************************************************************************
		// Impostazione delle informazioni specifiche del percorso
		//**************************************************************************
        public TecnoPathInfo Info
        {
            get { return clTecnoPathInfo; }
            set { clTecnoPathInfo = new TecnoPathInfo(value); }
        }
        #endregion

        #region Static Method
        public static string GetCamUsageAttributeNameFor(EN_CAM_ENABLED cam)
        {
            return "UsedCAM_" + cam.ToString();
        }
        #endregion

        #region Public Methods

        ///**************************************************************************
        ///<summary>AddSide
        ///     Aggiunge un lato al percorso
        ///</summary>
        ///<param name="s">
        ///		lato da aggiungere
        ///</param>
        ///<returns>
        ///     Numero lati del percorso
        ///</returns>
        ///**************************************************************************
        public override int AddSide(Side s)
        {
            if (!IsTecnoSide(s))
                 return base.AddSide(Clone(s));

            return  base.AddSide(s);
        }

        ///**************************************************************************
        ///<summary>AddSide
        ///     Aggiunge un lato al percorso
        ///</summary>
        ///<param name="s">
        ///		lato da aggiungere
        ///</param>
        ///<param name="pos">
        ///		posizione di inserimento
        ///</param>
        ///<returns>
        ///     Numero lati del percorso
        ///</returns>
        ///**************************************************************************
        public override int AddSide(Side s, int pos)
        {
            if (!IsTecnoSide(s))
                return base.AddSide(Clone(s), pos);

            return base.AddSide(s, pos);
        }

        //**************************************************************************
        // SetTecnoPathInfoToSide
        //      imposta le infomrazioni tecnologiche all'entita indicata
        // Parametri:
        //      index = indice dell'entita' da impostare
        //      tpi   = riferimento alla informazioni tecnologiche da impostare
        // Restituisce:
        //**************************************************************************
        public virtual void SetTecnoPathInfoToSide(int index, TecnoPathInfo tpi)
        {
            this[index].Info.TecnoPathInfo = tpi;
        }

        //**************************************************************************
        // SetTecnoPathInfoToSide
        //      imposta le infomrazioni tecnologiche all'entita indicata
        // Parametri:
        //      index = indice dell'entita' da impostare
        // Restituisce:
        //**************************************************************************
        public virtual void SetTecnoPathInfoToSide(int index)
        {
            this[index].Info.TecnoPathInfo = Info;
        }

        //**************************************************************************
        // GetTecnoSideInfo
        //      ricava il riferimento alle proprieta' specifiche dell'elemento
        // Parametri:
        //      index = indice dell'entita' da cui ricavare le proprieta'
        // Restituisce:
        //**************************************************************************
        public virtual TecnoSideInfo GetTecnoSideInfo(int index)
        {
            return  this[index].Info;
        }

        //**************************************************************************
        // GetToolpathType
        //      ricava la tipologia di path definita da Breton.BCamPath.EN_TOOLPATH_TYPE
        // Restituisce:
        //      tipologia di path definita da Breton.BCamPath.EN_TOOLPATH_TYPE
        //**************************************************************************
        public EN_TOOLPATH_TYPE GetToolpathType()
        {
            Path p = this;
            if (p.Attributes.ContainsKey("ToolPath"))
            {
                UInt32 uv = Convert.ToUInt32(p.Attributes["ToolPath"]);
                EN_TOOLPATH_TYPE ty = (EN_TOOLPATH_TYPE)uv;
                return ty;
            }
            return EN_TOOLPATH_TYPE.None;
        }

        //**************************************************************************
        // GetIndexOf
        //      ricava l'indice nella sequenza ToolPathSide del lato indicato
        // Parametri:
        //      s = riferimento al lato da cercare
        // Restituisce:
        //      indice del lato trovato o -1 se non trovato
        //**************************************************************************
        public virtual int GetIndexOf(Side s)
        {
            for (int i = 0; i < this.Count; i++)
                if (this.GetSide(i) == s)
                    return i;
            return -1;
        }

        //**************************************************************************
        // IsTecnoPathInfoToSideValid
        //      verifica se le informazioni tecnologiche dell'entita' indicata sono
        //      valide.
        // Parametri:
        //      index = indice dell'entita' da valutare
        // Restituisce:
        //**************************************************************************
        public virtual bool IsTecnoPathInfoToSideValid(int index)
        {
            return this[index].Info.TecnoPathInfo.IsValid()
                || ((this[index].Info.DefaultEnabledCAM & EN_CAM_ENABLED.RhinoNC) == EN_CAM_ENABLED.RhinoNC && this[index].Info.TecnoPathInfo.IsValidWithExternalCam());
        }

        //**************************************************************************
        // PropagatesTecnoPathInfo
        //      propaga le informazioni tecnologiche a tutti gli elementi della lista
        // Parametri:
        //      tpi   = riferimento alla informazioni tecnologiche da impostare
        //      forced = se true indica che la propagazione va forzata anche se
        //               le entita' gia' dispongono di informazioni
        // Restituisce:
        //**************************************************************************
        public virtual void PropagatesTecnoPathInfo( TecnoPathInfo tpi, bool forced )
        {
            for (int i = 0; i < Count; i++)
            {
                bool propagate = true;
                if (!forced)
                    propagate = !IsTecnoPathInfoToSideValid( i );

                if (propagate)
                    SetTecnoPathInfoToSide( i, tpi );

                this[i].Info.SetSideLength( this[i].Length );

                this[i].ResetUsedTools();
            }
        }

        //**************************************************************************
        // PropagatesTecnoPathInfo
        //      propaga le informazioni tecnologiche a tutti gli elementi della lista
        // Parametri:
        //      forced = se true indica che la propagazione va forzata anche se
        //               le entita' gia' dispongono di informazioni
        // Restituisce:
        //**************************************************************************
        public virtual void PropagatesTecnoPathInfo(bool forced)
        {
            PropagatesTecnoPathInfo(Info, forced);
        }

        //**************************************************************************
        // SetThickness
        //      imposta lo spessore generale del percorso e di tutti i suoi 
        //      elementi.
        // Parametri:
        //      thickness = spessore materiale da impostare
        // Restituisce:
        //**************************************************************************
        public virtual void SetThickness(double thickness)
        {
            Info.MaterialThickness = thickness;
            for (int i = 0; i < Count; i++)
            {
                this[i].Info.TecnoPathInfo.MaterialThickness = thickness;
            }
        }

        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public override bool ReadDerivedFileXml(XmlNode n)
        {
            if (clTecnoPathInfo != null)
                return clTecnoPathInfo.ReadFileXml(n);
            return true;
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlTextWriter w)
        {
            if (clTecnoPathInfo != null)
                clTecnoPathInfo.WriteFileXml(w);
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlNode baseNode)
        {
            if (clTecnoPathInfo != null)
                clTecnoPathInfo.WriteFileXml(baseNode);
        }

        //**************************************************************************
        /// <summary>
		/// Crea un path uguale a quello corrente
		/// </summary>
		/// <returns></returns>
        //**************************************************************************
        public override Path Clone()
		{
			return (Path)new TecnoPath(this);
		}

        //**************************************************************************
        /// <summary> DiscretizeArcs
        ///     verifica se ci sono archi da discretizzare
        /// </summary>
        /// <returns></returns>
        //**************************************************************************
        public void DiscretizeArcs()
        {
            for (int i = 0; i < Count; i++)
            {
                if (this[i] is TecnoArc)
                {
                    TecnoArc a = (TecnoArc)this[i];
                    if (a.CoordError > 0)
                    {
                        Point[] pn = null;

                        if ((a.AmplAngle > 0 && a.Info.TecnoPathInfo.ToolSide == EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT) ||
                             (a.AmplAngle < 0 && a.Info.TecnoPathInfo.ToolSide == EN_TOOL_SIDE.EN_TOOL_SIDE_LEFT))
                            pn = a.ExternBrokenLines(a.CoordError);
                        else
                            pn = a.InternBrokenLines(a.CoordError);

                        if (pn.Length > 0)
                        {
                            for (int j = 0; j < pn.Length - 1; j++)
                            {
                                TecnoSegment s = new TecnoSegment( new Segment(pn[j], pn[j + 1]), a.Info );
                                AddSide(s, i++);
                            }
                            i--;
                            DeleteSide(a);
                        }
                    }
                }
            }
        }

        //**************************************************************************
        /// <summary>
        ///     verifica se la geometria attuale rappresenta il perimetro esterno
        ///     di un for.
        /// </summary>
        /// <param name="ti">
        ///     utensile di foratura da usato
        /// </param>
        /// <returns>
        ///     true se la geometria corrente rappresenta un foro compatibile
        ///          con l'utensile indicato
        ///     false altrimenti.
        /// </returns>
        //**************************************************************************
        public bool IsHoleBoudary(ToolInfo ti)
        {
            if (ti.IsCoreDrill())
            {
                if (Count == 1)
                {
                    if (this[0] is Arc)
                    {
                        Arc a = (Arc)this[0];
                        if (MathUtil.QuoteIsEQ(a.Radius, ti.Radius, 0.1))
                            return true;
                    }
                }
                else if (Count == 2)
                {
                    if (this[0] is Arc && this[1] is Arc)
                    {
                        Arc a1 = (Arc)this[0];
                        Arc a2 = (Arc)this[0];
                        if (MathUtil.QuoteIsEQ(a1.Radius, ti.Radius, 0.1) &&
                            MathUtil.QuoteIsEQ(a2.Radius, ti.Radius, 0.1) &&
                            a1.CenterPoint.Distance(a2.CenterPoint) < MathUtil.QUOTE_EPSILON)
                            return true;
                    }
                }
            }
            return false;
        }

        //**************************************************************************
        /// <summary>
        ///     verifica se il cam indicato e' stato usato per la creazione di
        ///     percorsi utensile.
        /// </summary>
        /// <param name="cam">
        ///     tipo di cam da verificare
        /// </param>
        /// <returns>
        ///     true se il cam e' stato usato
        ///     false altrimenti
        /// </returns>
        //**************************************************************************
        public bool IsCamUsed(EN_CAM_ENABLED cam)
        {
            string key = GetCamUsageAttributeNameFor(cam);
            if (Attributes.ContainsKey(key))
                return true;
            return false;
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni sui cam utilizzabili nella generazione dei
        ///     percorsi utensile associati a questo percorso.
        /// </summary>
        //**************************************************************************
        public void ResetEnabledCams()
        {
            for (int i = 0; i < Count; i++)
            {
                ITecnoSide its = (ITecnoSide)this[i];
                its.ResetEnabledCams();
            }
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni sui cam utilizzati nella generazione dei
        ///     percorsi utensile associati a questo percorso.
        /// </summary>
        //**************************************************************************
        public void ResetUsedCams()
        {
            for (int i = 0; i < Count; i++)
            {
                ITecnoSide its = (ITecnoSide)this[i];
                its.ResetUsedCams();
            }

            foreach (var value in Enum.GetValues(typeof(EN_CAM_ENABLED))) 
            {
                EN_CAM_ENABLED en = (EN_CAM_ENABLED)value;
                string key = GetCamUsageAttributeNameFor(en);
                if (Attributes.ContainsKey(key))
                    Attributes.Remove(key);
            }
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni sul cam indicato se gia' in uso
        ///     sui percorsi utensili derivati da percorso corrente..
        /// </summary>
        //**************************************************************************
        public void ResetUsedCam(EN_CAM_ENABLED cam)
        {
            if (IsCamUsed(cam))
            {
                Attributes.Remove(GetCamUsageAttributeNameFor(cam));
                for (int i = 0; i < Count; i++)
                {
                    ITecnoSide its = (ITecnoSide)this[i];
                    its.ResetUsedCam(cam);
                }
            }
        }

        //**************************************************************************
        /// <summary>
        ///     aggiorna la lista dei cam utilizzati nello sviluppo dei percorsi
        ///     utensili connessi a questo percorso.
        /// </summary>
        /// <param name="cam">
        ///     riferimento al cam utilizzato.
        /// </param>
        /// <param name="attribute">
        ///     informazione da associare all'utilizzo del cam.
        /// </param>
        /// <remarks>
        ///     deve essere indicato solo un cam per volta
        /// </remarks>
        //**************************************************************************
        public void UpdateUsedCams(EN_CAM_ENABLED cam, string attribute)
        {
            string key = GetCamUsageAttributeNameFor(cam);
            if (!Attributes.ContainsKey(key))
                Attributes.Add(key, attribute);
            else
                Attributes[key] = attribute;
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni sui cam abilitati ed utilizzati nella 
        ///     generazione dei percorsi utensile associati a questo percorso.
        /// </summary>
        //**************************************************************************
        public void ResetCams()
        {
            for (int i = 0; i < Count; i++)
            {
                ITecnoSide its = (ITecnoSide)this[i];
                its.ResetEnabledCams();
                its.ResetUsedCams();
            }
        }

        //**************************************************************************
        /// <summary>
        ///     copia delle sole informazioni di utilizzo cam da un percorso 
        ///     indicato.
        /// </summary>
        /// <param name="reference">
        ///     percorso da cui prelevare le informazioni.
        /// </param>
        //**************************************************************************
        public void UpdateUsedCamInfo(TecnoPath reference)
        {
            foreach (var obj in Enum.GetValues(typeof(EN_CAM_ENABLED)))
            {
                string key = TecnoPath.GetCamUsageAttributeNameFor((EN_CAM_ENABLED)obj);
                if (reference.Attributes.ContainsKey(key))
                    if (Attributes.ContainsKey(key))
                        Attributes[key] =reference.Attributes[key];
                    else
                        Attributes.Add(key, reference.Attributes[key]);
                else
                    Attributes.Remove(key);
            }

            // copy single element usage
            for (int j = 0; j < reference.Count && j < Count; j++)
            {
                ITecnoSide itps = (ITecnoSide)this[j];
                ITecnoSide iutps = (ITecnoSide)reference[j];

                itps.Info.UsedCAM = iutps.Info.UsedCAM;
                itps.Info.FirstVertexInterruptOffset = iutps.Info.FirstVertexInterruptOffset;
                itps.Info.SecondVertexInterruptOffset = iutps.Info.SecondVertexInterruptOffset;
                itps.Info.FirstVertexCompletedWithAvailableTools = iutps.Info.FirstVertexCompletedWithAvailableTools;
                itps.Info.SecondVertexCompletedWithAvailableTools = iutps.Info.SecondVertexCompletedWithAvailableTools;
                itps.Info.StartOnGroove = iutps.Info.StartOnGroove;
                itps.Info.EndOnGroove = iutps.Info.EndOnGroove;
                itps.Info.GrooveSize = iutps.Info.GrooveSize;
                itps.ResetUsedTools();
                foreach (var ti in iutps.UsedTools)
                    itps.AddUsedTool(ti.Value, double.MaxValue);
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     estrazione dei percorsi formati da elementi connessi per i quali
        ///     non sono stati creati percorsi completi (o eventualmente anche 
        ///     parziali) e per i quali e' abilitato l'uso del cam indicato.
        /// </summary>
        /// <param name="cams_to_evaluate">
        ///     elenco dei cam da valutare per la creazione della lista
        /// </param>
        /// <param name="only_complete_elements">
        ///     se true indica che si valutano come utilizzabili solo elementi
        ///     che non sono stati utilizzati in nessun modo da altri cam;
        ///     se false si considerano anche elementi parzialmente lavorati
        ///     da altri cam.
        /// </param>
        /// <returns>
        ///     lista di percorsi ognuno dei quali contiene una sequenza connessa
        ///     di elementi che possono essere gestiti dai cam indicati
        /// </returns>
        ///**************************************************************************
        public TecnoPaths GetPathsAvailableForCAM(EN_CAM_ENABLED cams_to_evaluate, bool only_complete_elements)
        {
            TecnoPaths lp = new TecnoPaths();
            TecnoPath tp = null;

            if (cams_to_evaluate == EN_CAM_ENABLED.RhinoNC)
            {
                EN_TOOLPATH_TYPE ptype = GetToolpathType();
                var i = ptype & EN_TOOLPATH_TYPE.RHINONC_EXCLUDED;
                if (i != 0)
                    return lp;
            }

            #region previus
            //for (int i = 0, j = -1; i < Count; i++)
            //{
            //    ITecnoSide its = (ITecnoSide)this[i];
            //    if (its.IsAvailableForCAM(cams_to_evaluate, only_complete_elements))
            //    {
            //        if (j == i - 1)
            //        {
            //            if (tp == null)
            //            {
            //                tp = new TecnoPath(this);
            //                tp.Name = tp.Name + "_" + i.ToString("000");
            //                tp.clTecnoPathInfo = new TecnoPathInfo(clTecnoPathInfo);
            //                lp.AddPath(tp);

            //                // azzeramento della lista iniziale delle entita'
            //                // si opera in questo modo per garantire che le informazioni
            //                // generali sul percorso siano uguali a quello originale
            //                tp = lp[lp.Count-1];
            //                while (tp.Count > 0)
            //                    tp.DeleteSide(0);

            //            }
            //        }
            //        else
            //        {
            //            tp = new TecnoPath(this);
            //            tp.Name = tp.Name + "_" + i.ToString("000");
            //            tp.clTecnoPathInfo = new TecnoPathInfo(clTecnoPathInfo);
            //            lp.AddPath(tp);

            //            // azzeramento della lista iniziale delle entita'
            //            // si opera in questo modo per garantire che le informazioni
            //            // generali sul percorso siano uguali a quello originale
            //            tp = lp[lp.Count - 1];
            //            while (tp.Count > 0)
            //                tp.DeleteSide(0);
            //        }

            //        List<ITecnoSide> lts = this[i].GetListOfUnFinischedIntervals();
            //        if (lts == null)
            //        {
            //            tp.AddSide((Side)this[i]);
            //            j = i;
            //        }
            //        else
            //        {
            //            foreach (var sd in lts)
            //            {
            //                tp.AddSide((Side)sd);
            //        }
            //    }
            //}

            //// se ci sono almeno due percorsi, si controlla se il primo e l'ultimo 
            //// di fatto sono connessi e in questo caso si crea un solo percorso
            //if (lp.Count > 1)
            //{
            //    TecnoPath tpf = lp[0];
            //    TecnoPath tpl = lp[lp.Count-1];

            //    if(!tpf.IsClosed && !tpl.IsClosed)
            //        if (tpl[tpl.Count - 1].LastPoint.AlmostEqual(tpf[0].FirstPoint))
            //        {
            //            while (tpf.Count > 0)
            //            {
            //                tpl.AddSide((Side)tpf[0]);
            //                tpf.DeleteSide(0);
            //            }
            //            lp.RemovePath(0);
            //        }
            //}

            #endregion

            for (int i = 0, j = 0; i < Count; i++)
            {
                ITecnoSide its = (ITecnoSide)this[i];
                if (!its.Info.IsEnabled)
                    continue;
                // 20160922 se la geometria è esterna ai limiti dei toolpaths, non si prova a creare il relativo percorso utensile
                if (its.Info.IsOutsideToolpathLimits == true)
                    continue;
                if (its.IsAvailableForCAM(cams_to_evaluate, only_complete_elements))
                {
                    List<ITecnoSide> lts = this[i].GetListOfUnFinishedIntervalsWithAvailableTools();
                    if (lts != null)
                    {
                        foreach (var sd in lts)
                        {
                            tp = new TecnoPath(this);
                            for (int iattr = 0; iattr < tp.Attributes.Count; iattr++)
                            {
                                if (tp.Attributes.Keys[iattr].StartsWith(ORIGINAL_ORG_SIDE))
                                {
                                    tp.Attributes.Remove(tp.Attributes.Keys[iattr]);
                                    iattr = -1;
                                }
                            }
                            tp.Name = tp.Name + "_" + j.ToString("000");
                            tp.Attributes.Add(GetOriginalSideIndexKey(tp), (i + 1).ToString());
                            tp.clTecnoPathInfo = new TecnoPathInfo(clTecnoPathInfo);
                            lp.AddPath(tp);

                            // azzeramento della lista iniziale delle entita'
                            // si opera in questo modo per garantire che le informazioni
                            // generali sul percorso siano uguali a quello originale
                            tp = lp[lp.Count - 1];
                            while (tp.Count > 0)
                                tp.DeleteSide(0);

                            tp.AddSide((Side)sd);
                            j++;
                        }
                    }
                }
            }

            // se ci sono almeno due percorsi, si controlla se il primo e l'ultimo 
            // di fatto sono connessi e in questo caso si crea un solo percorso
            if (lp.Count > 1)
            {
                for (int i = 0; i < lp.Count; i++)
                {
                    int j = (i + 1) % lp.Count;

                    TecnoPath tpf = lp[i];
                    TecnoPath tpl = lp[j];

                    if (!tpf.IsClosed && !tpl.IsClosed)
                        if (tpf[tpf.Count - 1].LastPoint.AlmostEqual(tpl[0].FirstPoint))
                        {
                            while (tpl.Count > 0)
                            {
                                tpf.AddSide((Side)tpl[0]);
                                tpl.DeleteSide(0);
                            }
                            MergeOriginalSideIndex(tpl, tpf);
                            lp.RemovePath(j);
                            i--;
                        }
                }
            }

            return lp;
        }
        public const string ORIGINAL_ORG_SIDE = "OriginalOrgSide";
        public const string ORIGINAL_ORG_POLYGON = "OriginalOrgPolygon";
        public static string GetOriginalSideIndexKey(Path dst)
        {
            int iOrgSideLeadNext = 0;
            while (dst.Attributes.ContainsKey(ORIGINAL_ORG_SIDE + iOrgSideLeadNext))
                iOrgSideLeadNext++;
            return ORIGINAL_ORG_SIDE + iOrgSideLeadNext;
        }
        bool MergeOriginalSideIndex(Path src, Path dst)
        {
            List<string> lSrc = null;
            foreach (var kv in src.Attributes)
                if (kv.Key.StartsWith(ORIGINAL_ORG_SIDE))
                {
                    if (lSrc == null)
                        lSrc = new List<string>();
                    if (!lSrc.Contains(kv.Value))
                        lSrc.Add(kv.Value);
                }
            if (lSrc == null)
                return false;

            List<string> lDst = new List<string>();
            foreach (var kv in dst.Attributes)
                if (kv.Key.StartsWith(ORIGINAL_ORG_SIDE))
                {
                    if (!lDst.Contains(kv.Value))
                        lDst.Add(kv.Value);
                }

            int count = 0;
            foreach (var s in lSrc)
                if (!lDst.Contains(s))
                {
                    lDst.Add(s);
                    dst.Attributes.Add(GetOriginalSideIndexKey(dst), s);
                    count++;
                }
            return count > 0;
        }
        
        ///**************************************************************************
        /// <summary>
        ///     recupera le eventuali coordinate del punto di appoccio definito
        ///     dall'utente.
        /// </summary>
        /// <param name="x">
        ///     riferimento alla eventuale coordinata X del punto di approccio.
        /// </param>
        /// <param name="y">
        ///     riferimento alla eventuale coordinata Y del punto di approccio.
        /// </param>
        /// <returns>
        ///     false   se il percorso non prevede un punto di approccio definito
        ///             dall'utente
        ///     true    le coordinate del punto di approccio sono state valorizzate.
        /// </returns>
        ///**************************************************************************
        public bool GetUserDefinedApproachPoint(ref double x, ref double y)
        {
            if (Attributes.ContainsKey("ApproachPoint_X") == false)
                return false;
            if (Attributes.ContainsKey("ApproachPoint_Y") == false)
                return false;
            x = Convert.ToDouble(Attributes["ApproachPoint_X"], CultureInfo.InvariantCulture);
            y = Convert.ToDouble(Attributes["ApproachPoint_Y"], CultureInfo.InvariantCulture);
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     impostazione delle coordinate del punto di approccio.
        /// </summary>
        /// <param name="x">
        ///     coordinata X del punto di approccio.
        /// </param>
        /// <param name="y">
        ///     coordinata Y del punto di approccio.
        /// </param>
        ///**************************************************************************
        public void SetUserDefinedApproachPoint(double x, double y)
        {
            Attributes["ApproachPoint_X"] = x.ToString(CultureInfo.InvariantCulture);
            Attributes["ApproachPoint_Y"] = y.ToString(CultureInfo.InvariantCulture);
            //RemoveUserdefinedPiercingPoint();
        }

        ///**************************************************************************
        /// <summary>
        ///     rimuove le indicazioni del punto di approccio sul percorso utensile
        /// </summary>
        ///**************************************************************************
        public void RemoveUserdefinedApprochPoint()
        {
            Attributes.Remove("ApproachPoint_X");
            Attributes.Remove("ApproachPoint_Y");
        }

        ///**************************************************************************
        /// <summary>
        ///     verifica se e' definito un punto di approccio esplicito.
        /// </summary>
        /// <returns>
        ///     false se non esiste un punto di approccio esplicito
        ///     true altrimenti
        /// </returns>
        ///**************************************************************************
        public bool HasUserDefinedApproachPoint()
        {
            double x = 0, y = 0;
            return GetUserDefinedApproachPoint(ref x, ref y);
        }

        ///**************************************************************************
        /// <summary>
        ///     recupera le eventuali coordinate del punto di piercing definito 
        ///     dall'utente .
        /// </summary>
        /// <param name="x">
        ///     riferimento alla eventuale coordinata X del punto di piercing.
        /// </param>
        /// <param name="y">
        ///     riferimento alla eventuale coordinata Y del punto di piercing.
        /// </param>
        /// <returns>
        ///     false   se il percorso non prevede un punto di piercing definito
        ///             dall'utente
        ///     true    le coordinate del punto di piercing sono state valorizzate.
        /// </returns>
        ///**************************************************************************
        public bool GetUserDefinedPiercingPoint(ref double x, ref double y)
        {
            if (Attributes.ContainsKey("PiercingPoint_X") == false)
                return false;
            if (Attributes.ContainsKey("PiercingPoint_Y") == false)
                return false;
            x = Convert.ToDouble(Attributes["PiercingPoint_X"], CultureInfo.InvariantCulture);
            y = Convert.ToDouble(Attributes["PiercingPoint_Y"], CultureInfo.InvariantCulture);
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     impostazione delle coordinate del punto di piercing.
        /// </summary>
        /// <param name="x">
        ///     coordinata X del punto di piercing.
        /// </param>
        /// <param name="y">
        ///     coordinata Y del punto di piercing.
        /// </param>
        ///**************************************************************************
        public void SetUserDefinedPiercingPoint(double x, double y)
        {
            Attributes["PiercingPoint_X"] = x.ToString(CultureInfo.InvariantCulture);
            Attributes["PiercingPoint_Y"] = y.ToString(CultureInfo.InvariantCulture);
            //RemoveUserdefinedApprochPoint();
        }

        ///**************************************************************************
        /// <summary>
        ///     rimuove le indicazioni del punto di piercing sul percorso utensile
        /// </summary>
        ///**************************************************************************
        public void RemoveUserdefinedPiercingPoint()
        {
            Attributes.Remove("PiercingPoint_X");
            Attributes.Remove("PiercingPoint_Y");
        }

        ///**************************************************************************
        /// <summary>
        ///     verifica se e' definito un punto di approccio esplicito.
        /// </summary>
        /// <returns>
        ///     false se non esiste un punto di approccio esplicito
        ///     true altrimenti
        /// </returns>
        ///**************************************************************************
        public bool HasUserDefinedPiercingPoint()
        {
            double x = 0, y = 0;
            return GetUserDefinedPiercingPoint(ref x, ref y);
        }

        ///**************************************************************************
        /// <summary>
        ///     ricalcola l'eventuale punto di approccio in funzione della matrice
        ///     di rototraslazione indicata.
        /// </summary>
        /// <returns>
        ///     false se non esiste un punto di approccio esplicito
        ///     true altrimenti
        /// </returns>
        ///**************************************************************************
        public void ApplyRototraslationToApproachPoint(RTMatrix rtm)
        {
            double ax = 0, ay = 0;
 
            if (GetUserDefinedApproachPoint( ref ax, ref ay))
            {
                Point pa = new Point(ax, ay);
                pa = pa.RotoTrasl(rtm);
                SetUserDefinedApproachPoint(pa.X, pa.Y);
            }
            //else//20160301
                if (GetUserDefinedPiercingPoint(ref ax, ref ay))
                {
                    Point pa = new Point(ax, ay);
                    pa = pa.RotoTrasl(rtm);
                    SetUserDefinedPiercingPoint(pa.X, pa.Y);
                }

            for (int i = 0; i < Count; i++)
            {
                ITecnoSide ts = (ITecnoSide)this[i];
                ts.Info.ApplyRototraslationToApproachPoint(rtm);
            }

        }
        #endregion

        #region Private Methods

        ///**************************************************************************
        ///
        /// <summary>IsTecnoSide: Test if object is a kind of TecnoXXX side type</summary>
        /// <param name="s">object to be tested</param>
        /// <returns>true if object is a TecnoXXX side type, false otherwise</returns>
        ///
        ///**************************************************************************
        protected bool IsTecnoSide(Side s)
        {
            if (s is TecnoArc || s is TecnoSegment)
                return true;

            return false;
        }

        ///**************************************************************************
        ///
        /// <summary>Clone: Clone a non TecnoXXX side object to a proper TecnoXXX one</summary>
        /// <param name="s">object to be cloned</param>
        /// <returns>cloned object</returns>
        ///
        ///**************************************************************************
        protected Side Clone(Side s)
        {
            if (s is Arc)
                return new TecnoArc((Arc)s);

            if (s is Segment)
                return new TecnoSegment((Segment)s);

            return s.GetCopy();
        }
        #endregion

        #region Protected Methods

        ///**************************************************************************
        /// <summary>
        /// CreateSegment: create a new segment object
        /// </summary>
        /// <remarks>created segment</remarks> 
        //**************************************************************************
        protected override Segment CreateSegment()
        {
            return (Segment)new TecnoSegment();
        }

        ///**************************************************************************
        /// <summary>
        /// CreateSegment: create a new segment object
        /// </summary>
        /// <param name="p1">first segment's point</param>
        /// <param name="p2">second segment's point</param>
        /// <remarks>created segment</remarks> 
        //**************************************************************************
        protected override Segment CreateSegment(Point p1, Point p2)
        {
            return (Segment)new TecnoSegment(p1, p2);
        }

        ///**************************************************************************
        /// <summary>
        /// CreateArc: create a new arc object
        /// </summary>
        /// <remarks>created arc</remarks> 
        //**************************************************************************
        protected override Arc CreateArc()
        {
            return (Arc)new TecnoArc();
        }

        #endregion
    }
}
