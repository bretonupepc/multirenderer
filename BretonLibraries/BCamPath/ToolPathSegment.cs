﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class ToolPathSegment :  Segment, IToolPathSide
    {
        #region Variables
        private ToolPathSideInfo clToolPathSideInfo;
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Constructor
        /// </summary>
        ///*********************************************************************
        public ToolPathSegment()
            : base()
        {
            clToolPathSideInfo = new ToolPathSideInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="ts">object to copy from</param>
        ///*********************************************************************
        public ToolPathSegment(ToolPathSegment ts)
            : base(ts)
        {
            clToolPathSideInfo = new ToolPathSideInfo(ts.clToolPathSideInfo);
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 1
        /// </summary>
        /// <param name="x1">first point X coordinate</param>
        /// <param name="y1">first point Y coordinate</param>
        /// <param name="x2">second point X coordinate</param>
        /// <param name="y2">second point Y coordinate</param>
        ///*********************************************************************
		public ToolPathSegment(double x1, double y1, double x2, double y2)
			: this(x1,y1,x2,y2, new ToolPathSideInfo())
		{
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 2
        /// </summary>
        /// <param name="x1">first point X coordinate</param>
        /// <param name="y1">first point Y coordinate</param>
        /// <param name="x2">second point X coordinate</param>
        /// <param name="y2">second point Y coordinate</param>
        /// <param name="tsi">side specifics informations</param>
        ///*********************************************************************
        public ToolPathSegment(double x1, double y1, double x2, double y2, ToolPathSideInfo tpsi)
            : base(x1, y1, x2, y2)
        {
            clToolPathSideInfo = new ToolPathSideInfo(tpsi);
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 3
        /// </summary>
        /// <param name="p1">first point</param>
        /// <param name="p2">second point</param>
        ///*********************************************************************
        public ToolPathSegment(Point p1, Point p2)
			: this(p1, p2, new ToolPathSideInfo())
		{
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 4
        /// </summary>
        /// <param name="p1">first point</param>
        /// <param name="p2">second point</param>
        /// <param name="tsi">side specifics informations</param>
        /// *********************************************************************
        public ToolPathSegment(Point p1, Point p2, ToolPathSideInfo tpsi)
            : base(p1, p2)
        {
            clToolPathSideInfo = new ToolPathSideInfo(tpsi);
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 5
        /// </summary>
        /// <param name="s">base segment</param>
        ///*********************************************************************
        public ToolPathSegment(Segment s)
			: this(s, new ToolPathSideInfo())
		{
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 6
        /// </summary>
        /// <param name="s">base segment</param>
        /// <param name="leadin">lead in indicator</param>
        /// <param name="leadout">lead out indicator</param>
        /// <param name="feed">specific speed if > 0</param>
        ///*********************************************************************
        public ToolPathSegment(Segment s, ToolPathSideInfo tpsi)
            : base(s)
        {
            clToolPathSideInfo = new ToolPathSideInfo(tpsi);
        }

        //Distruttore
        ~ToolPathSegment()
        {
        }

        #endregion

        #region Properties
        ///**************************************************************************
        /// <summary>
        ///     Impostazione delle informazioni specifiche del percorso 
        /// </summary>
        ///**************************************************************************
        public ToolPathSideInfo Info
        {
            get { return clToolPathSideInfo; }
            set { clToolPathSideInfo = value; }
        }
        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate del primo punto del lato
        /// </summary>
        ///**************************************************************************
        public Point FirstPoint
        {
            get { return P1; }
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate dell'ultimo punto del lato
        /// </summary>
        ///**************************************************************************
        public Point LastPoint
        {
            get { return P2; }
        }
        #endregion

        #region Operators
        #endregion

        #region Public Methods

        //******************************************************************************
        // GetCopy
        // Restituisce una copia del lato
        //******************************************************************************
        public override Side GetCopy()
        {
            return (Side)new ToolPathSegment(this);
        }

        //******************************************************************************
        // RotoTrasl
        // Restituisce il lato rototraslato
        // Parametri:
        //			rot	: angolo di rototraslazione (in radianti)
        //			xt	: traslazione in x
        //			yt	: traslazione in y
        //******************************************************************************
        public override Side RotoTrasl(RTMatrix matrix)
        {
            return (Side)new ToolPathSegment((Segment)base.RotoTrasl(matrix), clToolPathSideInfo);
        }

        //**************************************************************************
        // Invert
        // Ritorna l'arco invertito
        //**************************************************************************
        public override Side Invert()
        {
            return (Side)new ToolPathSegment((Segment)base.Invert(), clToolPathSideInfo);
        }

        //**************************************************************************
        // OffsetSide
        // Restituisce un nuovo lato spostato perpendicolarmente di un offset 
        // rispetto al lato
        // Parametri:
        //			offset	: offset di spostamento
        //			verso	: verso di spostamento  > 0 sinistra (rot. CCW)
        //											< 0 destra (rot. CW)
        //**************************************************************************
        public override Side OffsetSide(double offset, int verso)
        {
            return (Side)new ToolPathSegment((Segment)base.OffsetSide(offset, verso), clToolPathSideInfo);
        }

        /// **************************************************************************
        /// <summary>
        ///     Estrazione di una porzione di lato, a partire dalle distanze dal primo
        ///     veritice.
        /// </summary>
        /// <param name="from">distanza iniziale</param>
        /// <param name="to">distanza finale</param>
        /// <returns>
        ///		lato estratto (null se lato nullo)
        ///	</returns>
        /// **************************************************************************
        public override Side Extract(double from, double to)
        {
            Segment s = (Segment)base.Extract(from, to);
            if (s == null)
                return null;
            return new ToolPathSegment(s, clToolPathSideInfo);
        }

        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public override bool ReadDerivedFileXml(XmlNode n)
        {
            if (clToolPathSideInfo != null)
                return clToolPathSideInfo.ReadDerivedFileXml(n);
            return true;
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlTextWriter w)
        {
            if (clToolPathSideInfo != null)
                clToolPathSideInfo.WriteDerivedFileXml(w);
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlNode baseNode)
        {
            if (clToolPathSideInfo != null)
                clToolPathSideInfo.WriteDerivedFileXml(baseNode);
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sul primo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        public double TangentOnFirstPoint()
        {
            return VectorP1.DirectionXY();
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sull'ultimo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        public double TangentOnLastPoint()
        {
            return VectorP2.DirectionXY();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
