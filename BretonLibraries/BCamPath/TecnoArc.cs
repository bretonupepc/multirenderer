﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class TecnoArc : Arc, ITecnoSide
    {
        #region Variables
        private TecnoSideInfo clTecnoSideInfo;
        private double dfCoordError = 0;
        private Tools aUsedTools = new Tools();
        private double dfMaxExpansionModule = -double.MaxValue;
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Constructor
        /// </summary>
        ///*********************************************************************
        public TecnoArc()
            : base()
        {
            clTecnoSideInfo = new TecnoSideInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="ta">object to copy from</param>
        ///*********************************************************************
        public TecnoArc(TecnoArc ta)
            : base(ta)
        {
            clTecnoSideInfo = new TecnoSideInfo(ta.clTecnoSideInfo);
            dfCoordError = ta.dfCoordError;
            dfMaxExpansionModule = ta.dfMaxExpansionModule;
            aUsedTools.Clear();
            foreach (var ut in ta.aUsedTools)
                aUsedTools.Add(new ToolInfo(ut.Value));
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 1
        /// </summary>
        /// <param name="a">base arc</param>
        ///*********************************************************************
        public TecnoArc(Arc a)
            : base(a)
        {
            clTecnoSideInfo = new TecnoSideInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 2
        /// </summary>
        /// <param name="a">base arc</param>
        ///*********************************************************************
        public TecnoArc(Arc a, TecnoSideInfo tsi)
            : base(a)
        {
            clTecnoSideInfo = new TecnoSideInfo(tsi);
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 3
        /// </summary>
        /// <param name="c">center point</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        ///*********************************************************************
        public TecnoArc(Point c, double r, double alpha, double beta)
            : this(c, r, alpha, beta, new TecnoSideInfo())
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 4
        /// </summary>
        /// <param name="c">center point</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        /// <param name="tsi">side specifics informations</param>
        ///*********************************************************************
        public TecnoArc(Point c, double r, double alpha, double beta, TecnoSideInfo tsi) 
            : base(c, r, alpha, beta)
        {
            clTecnoSideInfo = new TecnoSideInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 5
        /// </summary>
        /// <param name="xc">center point X</param>
        /// <param name="yc">center point Y</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        ///*********************************************************************
        public TecnoArc(double xc, double yc, double r, double alpha, double beta)
            : this(xc, yc, r, alpha, beta, new TecnoSideInfo())
        {
            clTecnoSideInfo = new TecnoSideInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 6
        /// </summary>
        /// <param name="xc">center point X</param>
        /// <param name="yc">center point Y</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        /// <param name="tsi">side specifics informations</param>
        ///*********************************************************************
        public TecnoArc(double xc, double yc, double r, double alpha, double beta, TecnoSideInfo tsi)
            : base(xc, yc, r, alpha, beta)
        {
            clTecnoSideInfo = new TecnoSideInfo(tsi);
        }

        #endregion

        #region Properties
        ///**************************************************************************
        /// <summary>
        ///     Impostazione delle informazioni specifiche del percorso
        /// </summary>
        ///**************************************************************************
        public TecnoSideInfo Info
        {
            get { return clTecnoSideInfo; }
            set { clTecnoSideInfo = new TecnoSideInfo(value); }
        }

        ///**************************************************************************
        /// <summary>
        ///     Impostazione errore cordale
        /// </summary>
        ///**************************************************************************
        public double CoordError
        {
            get { return dfCoordError; }
            set { dfCoordError = value; }
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate del primo punto del lato
        /// </summary>
        ///**************************************************************************
        public Point FirstPoint
        {
            get { return P1; }
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate dell'ultimo punto del lato
        /// </summary>
        ///**************************************************************************
        public Point LastPoint
        {
            get { return P2; }
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna il massimo modulo di espansione utilizzato
        /// </summary>
        ///**************************************************************************
        public double MaxExpantionModule
        {
            get { return dfMaxExpansionModule; }
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna l'elenco degli utensili utilizzati nelle generazione dei 
        ///     percorsi utensile.
        /// </summary>
        ///**************************************************************************
        public Tools UsedTools
        {
            get { return aUsedTools; }
        }
        #endregion

        #region Operators
        #endregion

        #region Public Methods

        ///**************************************************************************
        /// <summary>
        ///     Restituisce una copia del lato 
        /// </summary>
        /// <returns>
        ///     riferimento al lato creato.
        /// </returns>
        ///**************************************************************************
        public override Side GetCopy()
        {
            return (Side)new TecnoArc(this);
        }

        ///**************************************************************************
        /// <summary>
        ///     Restituisce il lato rototraslato
        /// </summary>
        /// <param name="matrix">
        ///     matrice di rototraslazione da considerare
        /// </param>
        /// <returns>
        ///     riferimento al lato creato.
        /// </returns>
        ///**************************************************************************
        public override Side RotoTrasl(RTMatrix matrix)
        {
            return (Side)new TecnoArc((Arc)base.RotoTrasl(matrix), clTecnoSideInfo);
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna l'arco invertito
        /// </summary>
        /// <returns></returns>
        ///**************************************************************************
        public override Side Invert()
        {
            return (Side)new TecnoArc((Arc)base.Invert(), clTecnoSideInfo);
        }

        ///**************************************************************************
        /// <summary>
        ///     Restituisce un nuovo lato spostato perpendicolarmente di un offset 
        /// </summary>
        /// <param name="offset">
        ///     offset (distanza) da considerare
        /// </param>
        /// <param name="verso">
        ///     direzione di applicazione dell'offset:
        ///         +1 sinistra (rot. CCW)
        ///         -1 destra (rot. CW)
        /// </param>
        /// <returns>
        ///     riferimento al lato calcolato
        /// </returns>
        ///**************************************************************************
        public override Side OffsetSide(double offset, int verso)
        {
            return (Side)new TecnoArc((Arc)base.OffsetSide(offset, verso), clTecnoSideInfo);
        }

        /// **************************************************************************
        /// <summary>
        ///     Estrazione di una porzione di lato, a partire dalle distanze dal primo
        ///     veritice.
        /// </summary>
        /// <param name="from">distanza iniziale</param>
        /// <param name="to">distanza finale</param>
        /// <returns>
        ///		lato estratto (null se lato nullo)
        ///	</returns>
        /// **************************************************************************
        public override Side Extract(double from, double to)
        {
            Arc a = (Arc)base.Extract(from, to);
            if (a == null)
                return  null;
            return new TecnoArc(a, clTecnoSideInfo);
        }

        ///**************************************************************************
        /// <summary>
        ///     lettura di parametri accessori previsti da classi derivate.
        /// </summary>
        /// <param name="n">
        ///     nodo XML contenete il lato
        /// </param>
        /// <returns>
        ///	    true	lettura eseguita con successo
        ///		false	errore nella lettura
        /// </returns>
        ///**************************************************************************
        public override bool ReadDerivedFileXml(XmlNode n)
        {
            if (n.SelectSingleNode("CoordError") != null)
                dfCoordError = Convert.ToDouble(n.SelectSingleNode("CoordError").Attributes.GetNamedItem("value").Value);
            if (clTecnoSideInfo != null)
                return clTecnoSideInfo.ReadDerivedFileXml(n);
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     Scrittura informazioni accessorie previste da classi derivate
        /// </summary>
        /// <param name="w">
        ///     stream dove eseguire la scrittura
        /// </param>
        ///**************************************************************************
        public override void WriteDerivedFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("CoordError");
            w.WriteAttributeString("value", dfCoordError.ToString());
            w.WriteEndElement();

            if (clTecnoSideInfo != null)
                clTecnoSideInfo.WriteDerivedFileXml(w);
        }

        ///**************************************************************************
        /// <summary>
        ///     Scrittura informazioni accessorie previste da classi derivate
        /// </summary>
        /// <param name="baseNode">
        ///     nodo padre al di sotto del quale inserire le informazioni
        /// </param>
        ///**************************************************************************
        public override void WriteDerivedFileXml(XmlNode baseNode)
        {
            XmlNode node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "CoordError", ""); ;
            XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
            attr.Value = dfCoordError.ToString();
            node.Attributes.Append(attr);
            baseNode.AppendChild(node);

            if (clTecnoSideInfo != null)
                clTecnoSideInfo.WriteDerivedFileXml(baseNode);
        }

        ///**************************************************************************
        /// <summary>
        ///     Rimuove i riferimenti agli utensili utilizzati.
        /// </summary>
        ///**************************************************************************
        public void ResetUsedTools()
        {
            aUsedTools.Clear();
            dfMaxExpansionModule = -double.MaxValue;
        }

        ///**************************************************************************
        /// <summary>
        ///     Rimuove l'indicazione sui CAM abilitati nella generazione di
        ///     percorsi utensile per questo elemento.
        /// </summary>
        ///**************************************************************************
        public void ResetEnabledCams()
        {
            Info.EnabledCAM = EN_CAM_ENABLED.None;
        }

        ///**************************************************************************
        /// <summary>
        ///     Rimuove l'indicazione sui CAM utilizzati nella generazione di
        ///     percorsi utensile per questo elemento.
        /// </summary>
        ///**************************************************************************
        public void ResetUsedCams()
        {
            Info.ExternalCam = null;
            Info.UsedCAM = EN_CAM_ENABLED.None;
            Info.FirstVertexInterruptOffset = 0;
            Info.FirstVertexDiskInterruptOffset = 0;
            Info.SecondVertexInterruptOffset = 0;
            Info.SecondVertexDiskInterruptOffset = 0;
            Info.StartOnGroove = false;
            Info.EndOnGroove = false;
            Info.GrooveSize = 0;
            Info.FirstVertexCompletedWithAvailableTools = false;
            Info.SecondVertexCompletedWithAvailableTools = false;
            aUsedTools.Clear();
        }

        ///**************************************************************************
        /// <summary>
        ///     Rimuove l'indicazione sul CAM indicato nella generazione di
        ///     percorsi utensile per questo elemento.
        /// </summary>
        ///**************************************************************************
        public void ResetUsedCam(EN_CAM_ENABLED cam)
        {
            clTecnoSideInfo.ResetUsedCam(cam);
            if (clTecnoSideInfo.ExternalCamUsed() == false)
                Info.ExternalCam = null;
        }

        ///**************************************************************************
        /// <summary>
        ///     Verifica se sul lato sono stati realizzati percorsi utensile con
        ///     CAM esterni.
        /// </summary>
        /// <returns>
        ///     true se e' stato utilizzato almento un cam esterno.
        ///     false altrimenti.
        /// </returns>
        ///**************************************************************************
        public bool ExternalCamUsed()
        {
            return clTecnoSideInfo.ExternalCamUsed();
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'elemento e' utilizzabile dalla lista di cam indicati
        ///     eventualmente anche parzialmente.
        /// </summary>
        /// <param name="cams_to_evaluate">
        ///     elenco dei cam da valutare per la creazione della lista
        /// </param>
        /// <param name="only_complete_elements">
        ///     se true indica che si valutano come utilizzabili solo elementi
        ///     che non sono stati utilizzati in nessun modo da altri cam;
        ///     se false si considerano anche elementi parzialmente lavorati
        ///     da altri cam.
        /// </param>
        /// <returns>
        ///     true se l'elemento e' compatibile con almeno uno dei cam indicati
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public bool IsAvailableForCAM(EN_CAM_ENABLED cams_to_evaluate, bool only_complete_elements)
        {
            // si controlla de almento uno dei cam indicati e' nella lista dei cam abilitati
            if ((Info.EnabledCAM & cams_to_evaluate) == EN_CAM_ENABLED.None &&
                (Info.DefaultEnabledCAM & cams_to_evaluate) == EN_CAM_ENABLED.None)
                return false;

            // si controlla se un utensile ha gia' operato, anche parzialmente, su questo elemento
            if (aUsedTools.Count == 0)
                return true;

            // un utensile ha operato; si controlla se esistono parti non ancora lavorate
            if (only_complete_elements)
                return false;
            if (MathUtil.QuoteIsGE(Info.FirstVertexInterruptOffset, 0) &&
                MathUtil.QuoteIsGE(Info.SecondVertexInterruptOffset, 0))
                return false;

            // l'elemento puo' essere utilizzato parzialmente
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna la lista di parti dell'elemento che non sono state lavorate
        ///     dagli utensili previsti.
        /// </summary>
        /// <returns>
        ///     lista delle parti non lavorate nell'elemento corrente.
        ///     null se non ci sono parti non lavorate.
        /// </returns>
        ///**************************************************************************
        public List<ITecnoSide> GetListOfUnFinishedIntervalsWithAvailableTools()
        {
            List<ITecnoSide> lts = new List<ITecnoSide>();

            if (MathUtil.QuoteIsZero(Info.GrooveSize) || UsedTools.Count == 0)//20161123 stesso controllo del segmento
            {
                if (!Info.FirstVertexCompletedWithAvailableTools && !Info.SecondVertexCompletedWithAvailableTools)
                {
                    lts.Add(this);
                }
            }
            else
            {
                if (Info.FirstVertexInterruptOffset < 0 && !Info.FirstVertexCompletedWithAvailableTools)
                {
                    TecnoArc ta = new TecnoArc(this);
                    Arc a = (Arc)ta.Extract(0, -Info.FirstVertexInterruptOffset);
                    ta.StartAngle = a.StartAngle;
                    ta.AmplAngle = a.AmplAngle;
                    ta.Info.FirstVertexInterruptOffset = 0;
                    ta.Info.FirstVertexDiskInterruptOffset = 0;
                    ta.Info.SecondVertexInterruptOffset = 0;
                    ta.Info.SecondVertexDiskInterruptOffset = 0;
                    ta.Info.EndOnGroove = true;
                    ta.Info.StartOnGroove = false;
                    lts.Add(ta);
                }

                if (Info.SecondVertexInterruptOffset < 0 && !Info.SecondVertexCompletedWithAvailableTools)
                {
                    TecnoArc ta = new TecnoArc(this);
                    Arc a = (Arc)ta.Extract(Length+Info.SecondVertexInterruptOffset, Length);
                    ta.StartAngle = a.StartAngle;
                    ta.AmplAngle = a.AmplAngle;
                    ta.Info.FirstVertexInterruptOffset = 0;
                    ta.Info.FirstVertexDiskInterruptOffset = 0;
                    ta.Info.SecondVertexInterruptOffset = 0;
                    ta.Info.SecondVertexDiskInterruptOffset = 0;
                    ta.Info.StartOnGroove = true;
                    ta.Info.EndOnGroove = false; 
                    lts.Add(ta);
                }
            }
            return lts;
        }

        ///**************************************************************************
        /// <summary>
        ///     Aggiunge un utensile alla lista degli utensili utilizzati
        ///     nella costruzione dei pecorsi utensile
        /// </summary>
        /// <param name="ti">
        ///     Utensile utilizzato
        /// </param>
        /// <param name="expansion_module">
        ///     modulo di espansione utilizzato
        /// </param>
        ///**************************************************************************
        public void AddUsedTool(ToolInfo ti, double expansion_module)
        {
            if (!aUsedTools.Contains(ti))
                aUsedTools.Add(ti);
            if (dfMaxExpansionModule < expansion_module)
                dfMaxExpansionModule = expansion_module;
        }

        ///**************************************************************************
        /// <summary>
        ///     Verifica se un utensile e' stato utilizzato nel calcolo
        ///     dei percorsi utensile per questo lato.
        /// </summary>
        /// <param name="ti">
        ///     utensile da verificare
        /// </param>
        /// <returns>
        ///     true se l'utensile e' stato utilizzato
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public bool ToolWasUsed(ToolInfo ti)
        {
            return (aUsedTools.Contains(ti) ? true : false);
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna il numero di utensili gia' utilizzati su questo lato.
        /// </summary>
        /// <returns>
        ///     numero di utensil utilizzati.
        /// </returns>
        ///**************************************************************************
        public int NumberOfUsedTools()
        {
            return aUsedTools.Count;
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sul primo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        public double TangentOnFirstPoint()
        {
            return VectorP1.DirectionXY();
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sull'ultimo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        public double TangentOnLastPoint()
        {
            return VectorP2.DirectionXY();
        }

        ///**************************************************************************
        /// <summary>
        ///     Se il lato prevede una lavorazione con angolo negativo, si
        ///     determina la zona di interferenza che deve essere considerata
        ///     nel calcolo delle collisioni con gli elementi originali; se
        ///     la lavorazione ha angolo positovo o nullo si ritorna il lato stesso.
        /// </summary>
        /// <returns>
        ///     percorso che determina la zona di interferenza generata da questo
        ///     lato.
        /// </returns>
        ///**************************************************************************
        public Path GetInterferenceZone()
        {
            Path p = new Path();
            p.AddSide(this);

            // se l'inclinazione della lavorazione e' negativa esiste una
            // zona di interferenza completa
            if (MathUtil.AngleIsLT(Info.Slope, 0))
            {
                double dist = Info.TecnoPathInfo.MaterialThickness * Math.Tan(Math.Abs(Info.Slope)) + Info.TecnoPathInfo.OverMaterialMax();
                Arc a1 = (Arc)this.OffsetSide(dist, Info.TecnoPathInfo.ToolSide == EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT ? -1 : 1);
                if (a1 == null)
                    return null;
                p.AddSide(new Segment(this.P2, a1.P2));
                p.AddSide(a1.Invert());
                p.AddSide(new Segment(a1.P1, this.P1));
                p.SetRotationSense(MathUtil.RotationSense.CCW);
            }

            return p;
        }
        #endregion

        #region Private Methods

        #endregion
    }
}
