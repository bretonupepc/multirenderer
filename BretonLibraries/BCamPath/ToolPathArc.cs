﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class ToolPathArc : Arc, IToolPathSide
    {
        #region Variables
        private ToolPathSideInfo clToolPathSideInfo;
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Constructor
        /// </summary>
        ///*********************************************************************
        public ToolPathArc()
            : base()
        {
            clToolPathSideInfo = new ToolPathSideInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="ta">object to copy from</param>
        ///*********************************************************************
        public ToolPathArc(ToolPathArc ta)
            : base(ta)
        {
            clToolPathSideInfo = new ToolPathSideInfo(ta.clToolPathSideInfo);
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 1
        /// </summary>
        /// <param name="a">base arc</param>
        ///*********************************************************************
        public ToolPathArc(Arc a)
            : this(a, new ToolPathSideInfo())
        {
            clToolPathSideInfo = new ToolPathSideInfo();
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 1
        /// </summary>
        /// <param name="a">base arc</param>
        ///*********************************************************************
        public ToolPathArc(Arc a, ToolPathSideInfo tpsi)
            : base(a)
        {
            clToolPathSideInfo = new ToolPathSideInfo(tpsi);
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 2
        /// </summary>
        /// <param name="c">center point</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        ///*********************************************************************
        public ToolPathArc(Point c, double r, double alpha, double beta)
            : this(c, r, alpha, beta, new ToolPathSideInfo())
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 3
        /// </summary>
        /// <param name="c">center point</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        /// <param name="tpsi">specifics side informations</param>
        ///*********************************************************************
        public ToolPathArc(Point c, double r, double alpha, double beta, ToolPathSideInfo tpsi)
            : base(c, r, alpha, beta)
        {
            clToolPathSideInfo = new ToolPathSideInfo(tpsi);
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 4
        /// </summary>
        /// <param name="xc">center point X</param>
        /// <param name="yc">center point Y</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        ///*********************************************************************
        public ToolPathArc(double xc, double yc, double r, double alpha, double beta)
            : this(xc, yc, r, alpha, beta, new ToolPathSideInfo())
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 5
        /// </summary>
        /// <param name="xc">center point X</param>
        /// <param name="yc">center point Y</param>
        /// <param name="r">radius</param>
        /// <param name="alpha">start angle</param>
        /// <param name="beta">amplidute</param>
        ///*********************************************************************
        public ToolPathArc(double xc, double yc, double r, double alpha, double beta, ToolPathSideInfo tpsi)
            : base(xc, yc, r, alpha, beta)
        {
            clToolPathSideInfo = new ToolPathSideInfo(tpsi);
        }

        //Distruttore
        ~ToolPathArc()
        {
        }

        #endregion

        #region Properties
        ///**************************************************************************
        /// <summary>
        ///     Impostazione delle informazioni specifiche del percorso 
        /// </summary>
        ///**************************************************************************
        public ToolPathSideInfo Info
        {
            get { return clToolPathSideInfo; }
            set { clToolPathSideInfo = new ToolPathSideInfo(value); }
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate del primo punto del lato
        /// </summary>
        ///**************************************************************************
        public Point FirstPoint
        {
            get { return P1; }
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate dell'ultimo punto del lato
        /// </summary>
        ///**************************************************************************
        public Point LastPoint
        {
            get { return P2; }
        }
        #endregion

        #region Operators
        #endregion

        #region Public Methods

        //******************************************************************************
        // GetCopy
        // Restituisce una copia del lato
        //******************************************************************************
        public override Side GetCopy()
        {
            return (Side)new ToolPathArc(this);
        }

        //******************************************************************************
        // RotoTrasl
        // Restituisce il lato rototraslato
        // Parametri:
        //			rot	: angolo di rototraslazione (in radianti)
        //			xt	: traslazione in x
        //			yt	: traslazione in y
        //******************************************************************************
        public override Side RotoTrasl(RTMatrix matrix)
        {
            return (Side)new ToolPathArc((Arc)base.RotoTrasl(matrix), clToolPathSideInfo);
        }

        //**************************************************************************
        // Invert
        // Ritorna l'arco invertito
        //**************************************************************************
        public override Side Invert()
        {
            return (Side)new ToolPathArc((Arc)base.Invert(), clToolPathSideInfo);
        }

        //**************************************************************************
        // OffsetSide
        // Restituisce un nuovo lato spostato perpendicolarmente di un offset 
        // rispetto al lato
        // Parametri:
        //			offset	: offset di spostamento
        //			verso	: verso di spostamento  > 0 sinistra (rot. CCW)
        //											< 0 destra (rot. CW)
        //**************************************************************************
        public override Side OffsetSide(double offset, int verso)
        {
            Side s = base.OffsetSide(offset, verso);
            if (s == null)
                return null;
            return (Side)new ToolPathArc((Arc)s, clToolPathSideInfo);
        }

        /// **************************************************************************
        /// <summary>
        ///     Estrazione di una porzione di lato, a partire dalle distanze dal primo
        ///     vertice.
        /// </summary>
        /// <param name="from">distanza iniziale</param>
        /// <param name="to">distanza finale</param>
        /// <returns>
        ///		lato estratto (null se lato nullo)
        ///	</returns>
        /// **************************************************************************
        public override Side Extract(double from, double to)
        {
            Arc a = (Arc)(Arc)base.Extract(from, to);
            if (a == null)
                return null;
            return new ToolPathArc(a, clToolPathSideInfo);
        }

        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public override bool ReadDerivedFileXml(XmlNode n)
        {
            if (clToolPathSideInfo != null)
                return clToolPathSideInfo.ReadDerivedFileXml(n);
            return true;
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlTextWriter w)
        {
            if (clToolPathSideInfo != null)
                clToolPathSideInfo.WriteDerivedFileXml(w);
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public override void WriteDerivedFileXml(XmlNode baseNode)
        {
            if (clToolPathSideInfo != null)
                clToolPathSideInfo.WriteDerivedFileXml(baseNode);
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sul primo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        public double TangentOnFirstPoint()
        {
            return VectorP1.DirectionXY();
        }

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sull'ultimo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        public double TangentOnLastPoint()
        {
            return VectorP2.DirectionXY();
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
