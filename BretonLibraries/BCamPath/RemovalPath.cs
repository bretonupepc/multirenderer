﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class RemovalPath : Path
    {
        #region Variables
        private const string sLinkToToolPathKey = "__LINK_TO_TOOL_PATH_";
        private const string sLinkToToolPathSideKey = "__LINK_TO_TOOL_PATH_SIDE_KEY_";
        private ToolPath clToolPath;
        private int iToolPathSideNumber;
        #endregion

        #region properties
        public string LinkToToolPathKey
        {
            get { return this[sLinkToToolPathKey]; }
            set { this[sLinkToToolPathKey] = value; }
        }

        public string LinkToToolPathSideKey
        {
            get { return this[sLinkToToolPathSideKey]; }
            set { this[sLinkToToolPathSideKey] = value; }
        }

        public ToolPath LinkToToolPath
        {
            get { return clToolPath; }
            set { clToolPath = value; }
        }

        public int LinkedToolPathSideNumber
        {
            get { return iToolPathSideNumber; }
            set 
            { 
                iToolPathSideNumber = value;
                LinkToToolPathSideKey = iToolPathSideNumber.ToString();
            }
        }

        public IToolPathSide LinkedToolPathSide
        {
            get
            {
                if (clToolPath != null &&
                    iToolPathSideNumber >= 0 &&
                    iToolPathSideNumber < clToolPath.Count)
                    return (IToolPathSide)clToolPath[iToolPathSideNumber];
                return null;
            }
        }
        
        #endregion

        #region Constructors
        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso vuoto
		/// </summary>
        ///*********************************************************************
        public RemovalPath() : base()
		{
            clToolPath = null;
            iToolPathSideNumber = -1;
		}

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public RemovalPath(Path p)
            : base(p)
        {
            clToolPath = null;
            iToolPathSideNumber = -1;
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public RemovalPath(RemovalPath p)
            : base((Path)p)
        {
            clToolPath = p.clToolPath;
            iToolPathSideNumber = p.iToolPathSideNumber;
        }
        #endregion

		#region Public Methods
        ///*********************************************************************
        /// <summary> Clone
		///     Crea un path uguale a quello corrente
		/// </summary>
		/// <returns>
        ///     percorso clonato
        /// </returns>
        ///*********************************************************************
        public override Path Clone()
		{
			return (Path)new RemovalPath(this);
		}

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <param name="points">Lista di punti di intersezione ritornata</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public override bool Intersect(Side s2, out List<Point> points)
        {
            Point[] inters;

            // Crea la lista di punti di intersezione da restituire
            points = new List<Point>();

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            for (int i = 0; i < Count; i++)
            {
                Side s1 = this[i];

                int num = s1.Intersect(s2, out inters);

                // si considerando solo intersezioni che sono sostanzialmente
                // diverse dai vertici e che non sono tangenti ad archi
                for (int j = 0; j < num; j++)
                {
                    // verifica punto estreno di almeno uno dei due lati
                    if (s1.P1.AlmostEqual(inters[j], 1e-3) ||
                        s1.P2.AlmostEqual(inters[j], 1e-3) ||
                        s2.P1.AlmostEqual(inters[j], 1e-3) ||
                        s2.P2.AlmostEqual(inters[j], 1e-3))
                        continue;

                    // verifica condizione di tangenza 
                    double tg1;
                    if (s1 is Arc)
                    {
                        Arc a = (Arc)s1;
                        tg1 = a.CalcAngPoint(0, inters[j].X, inters[j].Y);
                        tg1 = MathUtil.Angolo_0_2PI(tg1 + Math.PI/2);
                    }
                    else
                    {
                        Segment s = (Segment)s1;
                        tg1 = MathUtil.Angolo_0_2PI(s.VectorP1.DirectionXY());
                    }

                    double tg2;
                    if (s2 is Arc)
                    {
                        Arc a = (Arc)s2;
                        tg2 = a.CalcAngPoint(0, inters[j].X, inters[j].Y);
                        tg2 = MathUtil.Angolo_0_2PI(tg1 + Math.PI/2);
                    }
                    else
                    {
                        Segment s = (Segment)s2;
                        tg2 = MathUtil.Angolo_0_2PI(s.VectorP1.DirectionXY());
                    }

                    if ( MathUtil.AngleIsEQ(tg1, tg2) ||
                         MathUtil.AngleIsEQ(tg1, MathUtil.Angolo_0_2PI(tg2+Math.PI)) )
                        continue;

                    // intersezione valida
                    points.Add(inters[j]);
                }
            }

            // Intersezioni presenti se trovato almeno un punto di intesezione
            return (points.Count > 0);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public override bool Intersect(Side s2)
        {
            List<Point> points = null;

            return Intersect(s2, out points);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side (con rototraslazione)
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <param name="points">Lista di punti di intersezione ritornata</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public override bool IntersectRT(Side s2, out List<Point> points)
        {
            Point[] inters;

            // Crea la lista di punti di intersezione da restituire
            points = new List<Point>();

            // Scorre tutti i lati dei poligoni, e verifica se si intersecano
            for (int i = 0; i < Count; i++)
            {
                Side s1 = GetSideRT(i);

                int num = s1.Intersect(s2, out inters);

                // si considerando solo intersezioni che non vertice di uno dei due elementi
                for (int j = 0; j < num; j++)
                {
                    if (!s1.P1.AlmostEqual(inters[j]) &&
                         !s1.P2.AlmostEqual(inters[j]) &&
                         !s2.P1.AlmostEqual(inters[j]) &&
                         !s2.P2.AlmostEqual(inters[j]))
                        points.Add(inters[0]);
                }
            }

            // Intersezioni presenti se trovato almeno un punto di intesezione
            return (points.Count > 0);
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se il path interseca un elemento di tipo side (con rototraslazione)
        /// </summary>
        /// <param name="s2">elemento da verificare</param>
        /// <returns>
        ///		true	l'elemento interseca il percorso
        ///		false	non ci sono intersezioni
        ///	</returns>
        ///**************************************************************************
        public override bool IntersectRT(Side s2)
        {
            List<Point> points = null;

            return IntersectRT(s2, out points);
        }

        //**************************************************************************
        /// <summary>
        /// Ricostruisce i corretti link tra il percorso di asportazione e il lato
        /// di un percorso utensile che lo ha generato.
        /// </summary>
        /// <
        /// <returns></returns>
        //**************************************************************************
        public bool RecoverLinkToToolPath(ToolPaths tops)
        {
            string key = LinkToToolPathSideKey;
            if (key == null)
                return  false;

            iToolPathSideNumber = Convert.ToInt32(key);
            if (iToolPathSideNumber < 0)
                return  false;

            for (int i = 0; i < tops.Count; i++)
            {
                if (tops[i].Name == LinkToToolPathKey)
                {
                    if (iToolPathSideNumber < tops[i].Count)
                    {
                        LinkToToolPath = tops[i];
                        return  true;
                    }
                }
            }

            iToolPathSideNumber = -1;
            return false;
        }

        //**************************************************************************
        /// <summary>
        ///     Salva il riferimento al percorso utensile e al lato che ha originato
        ///     questo percorso di asportazione.
        /// </summary>
        /// <param name="tp">
        ///     riferimento al percorso utensile che genera questo percorso di
        ///     asportazione
        /// </param>
        /// <param name="sd">
        ///     riferimento al lato del percorso utensile che genera l'asportazione.
        /// </param>
        //**************************************************************************
        public void SaveLintoToToolPath(ToolPath tp, Side sd )
        {
            for (int i = 0; i < tp.Count; i++)
            {
                if (sd.Equals(tp[i]))
                {
                    SaveLintoToToolPath( tp, i );
                    break;
                }
            }
        }

        //**************************************************************************
        /// <summary>
        ///     Salva il riferimento al percorso utensile e al lato che ha originato
        ///     questo percorso di asportazione.
        /// </summary>
        /// <param name="tp">
        ///     riferimento al percorso utensile che genera questo percorso di
        ///     asportazione
        /// </param>
        /// <param name="index">
        ///     riferimento al lato del percorso utensile che genera l'asportazione.
        /// </param>
        //**************************************************************************
        public void SaveLintoToToolPath(ToolPath tp, int index)
        {
            LinkToToolPathKey = tp.Name;
            LinkToToolPath = tp;
            LinkedToolPathSideNumber = index;
        }

        ///**************************************************************************
        /// <summary>
        /// Verifica se questo percorso di asportazione e' stato generato dalla
        /// coppia (percorso, elemento) indicati.
        /// </summary>
        /// <param name="tp">
        ///     percorso utensile potenzialmente generatore
        /// </param>
        /// <param name="index">
        ///     indice dell'elemento nel percorso utensile per cui verificare
        ///     la condizione
        /// </param>
        /// <returns>
        ///     true il (percorso, elemento) sono i generatori del percorso di
        ///          asportazione.
        ///     false altrimenti.
        /// </returns>
        ///**************************************************************************
        public bool TestParent(ToolPath tp, int index)
        {
            if (LinkToToolPath == tp && LinkedToolPathSideNumber == index)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        /// rettifica la posizione del percorso in base alle matrici indicate.
        /// </summary>
        /// <param name="org_rtm">
        ///     matrice di rototraslazione iniziale.
        /// </param>
        /// <param name="new_rtm">
        ///     matrice di rototraslazione finale.
        /// </param>
        ///**************************************************************************
        public void Normalize(RTMatrix org_rtm, RTMatrix new_rtm)
        {
            RTMatrix artm = org_rtm * MatrixRT;
            MatrixRT = new_rtm.InvMat() * artm;

            Path z = GetPathRT();
            while (Count > 0)
                RemoveSide(0);

            for (int i = 0; i < z.Count; i++)
                AddSide(z[i]);

            MatrixRT = new RTMatrix();
        }
		#endregion
	}
}
