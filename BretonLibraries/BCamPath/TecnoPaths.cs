﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class TecnoPaths : PathCollection
    {
        #region Variables

        private bool _HasGlobalTecnoPaths = false;
        private bool _IsLocked = false;

        #endregion

		#region Constructors

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso vuoto
		/// </summary>
        ///*********************************************************************
        public TecnoPaths() : base()
		{
            szOriginalType = "TecnoPaths";
		}

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public TecnoPaths(TecnoPaths p) : base( (PathCollection)p )
		{
            szOriginalType = "TecnoPaths";
            _HasGlobalTecnoPaths = p._HasGlobalTecnoPaths;
            _IsLocked = p._IsLocked;
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public TecnoPaths(PathCollection p)
            : base(p)
        {
            szOriginalType = "TecnoPaths";
        }

        //Distruttore
        ~TecnoPaths()
		{
		}

		#endregion

        public override void WriteFileXml(XmlTextWriter w)
        {
            base.WriteFileXml(w);

            w.WriteAttributeString("HasGlobalTecnoPaths", HasGlobalTecnoPaths ? "1" : "0");//20160513
            w.WriteAttributeString("IsLocked", IsLocked ? "1" : "0");
        }

        public override void WriteDerivedFileXml(XmlNode baseNode)
        {
            base.WriteDerivedFileXml(baseNode);

            //HasGlobalTecnoPaths
            XmlNode xHasGlobalTecnoPaths = baseNode.SelectSingleNode("HasGlobalTecnoPaths");//20160513
            if (xHasGlobalTecnoPaths == null)
            {
                xHasGlobalTecnoPaths = (XmlNode)baseNode.OwnerDocument.CreateElement("HasGlobalTecnoPaths");
                baseNode.AppendChild(xHasGlobalTecnoPaths);
            }
            xHasGlobalTecnoPaths.InnerText = (HasGlobalTecnoPaths ? "1" : "0");
            //IsLocked
            XmlNode xIsLocked = baseNode.SelectSingleNode("IsLocked");
            if (xIsLocked == null)
            {
                xIsLocked = (XmlNode)baseNode.OwnerDocument.CreateElement("IsLocked");
                baseNode.AppendChild(xIsLocked);
            }
            xIsLocked.InnerText = (IsLocked ? "1" : "0");
        }

        public override bool ReadDerivedFileXml(XmlNode n)
        {
            bool result = base.ReadDerivedFileXml(n);

            //HasGlobalTecnoPaths
            var xHasGlobalTecnoPaths = n.SelectSingleNode("HasGlobalTecnoPaths");//20160513
            if (xHasGlobalTecnoPaths != null)
                HasGlobalTecnoPaths = xHasGlobalTecnoPaths.InnerText == "1";
            //IsLocked
            var xIsLocked = n.SelectSingleNode("IsLocked");
            if (xIsLocked != null)
                IsLocked = xIsLocked.InnerText == "1";

            return result;
        }

        #region Properties

        ///**************************************************************************
        /// <summary>
        /// Indicizzazione della lista interna
        /// </summary>
        ///**************************************************************************
        public new TecnoPath this[int i]
        {
            get { return (TecnoPath)base[i]; }
        }

        public bool HasGlobalTecnoPaths
        {
            get { return _HasGlobalTecnoPaths; }
            set { _HasGlobalTecnoPaths = value; }
        }

        public bool IsLocked
        {
            get { return _IsLocked; }
            set { _IsLocked = value; }
        }

        #endregion

        #region Public Methods
        //**************************************************************************
        // GetPath
        // Ritorna il percorso specificato
        // Parametri:
        //			i	: indice percorso
        // Restituisce:
        //			percorso
        //**************************************************************************
        public new TecnoPath GetPath(int i)
        {
            return (TecnoPath)base.GetPath(i);
        }

        //**************************************************************************
        // GetPath
        // Ritorna il percorso specificato
        // Parametri:
        //			name	: Path.Name
        // Restituisce:
        //			percorso
        //**************************************************************************
        public new TecnoPath GetPathByName(string name)
        {
            for (int i = 0; i < Count; i++)
                if (this[i].Name == name)
                    return this[i];
            return null;
        }

        //**************************************************************************
        // GetIndexOf
        //      Ritorna l'indice del percorso indicato
        // Parametri:
        //		p = riferimento al percorso da trovare
        // Restituisce:
        //		indice del perfocorso trovato o -1 se non trovato
        //**************************************************************************
        public int GetIndexOf(Path p)
        {
            for (int i = 0; i < Count; i++)
                if (this[i] == p)
                    return i;
            return -1;
        }

        //**************************************************************************
        /// <summary> DiscretizeArcs
        ///     verifica se ci sono archi da discretizzare
        /// </summary>
        /// <returns></returns>
        //**************************************************************************
        public void DiscretizeArcs()
        {
            for (int i = 0; i < Count; i++)
                this[i].DiscretizeArcs();
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni sui cam utilizzabili nella generazione dei
        ///     percorsi utensile associati alla lista di percorsi.
        /// </summary>
        //**************************************************************************
        public void ResetEnabledCams()
        {
            for (int i = 0; i < Count; i++)
                this[i].ResetEnabledCams();
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni sui cam utilizzati nella generazione dei
        ///     percorsi utensile associati  alla lista di percorsi.
        /// </summary>
        //**************************************************************************
        public void ResetUsedCams()
        {
            for (int i = 0; i < Count; i++)
                this[i].ResetUsedCams();
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni su un cam utilizzato nella generazione dei
        ///     percorsi utensile associati  alla lista di percorsi.
        /// </summary>
        //**************************************************************************
        public void ResetUsedCam(EN_CAM_ENABLED cam)
        {
            for (int i = 0; i < Count; i++)
                this[i].ResetUsedCam(cam);
        }

        //**************************************************************************
        /// <summary>
        ///     reset delle informazioni sui cam abilitati ed utilizzati nella 
        ///     generazione dei percorsi utensile associati  alla lista di percorsi.
        /// </summary>
        //**************************************************************************
        public void ResetCams()
        {
            for (int i = 0; i < Count; i++)
                this[i].ResetCams();
        }

        //**************************************************************************
        /// <summary>
        ///     estrazione dei percorsi formati da elementi connessi per i quali
        ///     non sono stati creati percorsi completi (o eventualmente anche 
        ///     parziali) e per i quali e' abilitato l'uso del cam indicato.
        /// </summary>
        /// <param name="cams_to_evaluate">
        ///     elenco dei cam da valutare per la creazione della lista
        /// </param>
        /// <param name="only_complete_elements">
        ///     se true indica che si valutano come utilizzabili solo elementi
        ///     che non sono stati utilizzati in nessun modo da altri cam;
        ///     se false si considerano anche elementi parzialmente lavorati
        ///     da altri cam.
        /// </param>
        /// <returns>
        ///     lista di percorsi ognuno dei quali contiene una sequenza connessa
        ///     di elementi che possono essere gestiti dai cam indicati
        /// </returns>
        //**************************************************************************
        public TecnoPaths GetPathsAvailableForCAM(EN_CAM_ENABLED cams_to_evaluate, bool only_complete_elements)
        {
            TecnoPaths lp = new TecnoPaths();

            for (int i = 0; i < Count; i++)
            {
                TecnoPath tp = (TecnoPath)this[i];
                TecnoPaths tlp = tp.GetPathsAvailableForCAM(cams_to_evaluate, only_complete_elements);
                if (tlp != null && tlp.Count > 0)
                    for (int j = 0; j < tlp.Count; j++)
                        lp.AddPath(tlp[j]);
            }
            return lp;
        }

        //**************************************************************************
        /// <summary>
        ///     copia delle sole informazioni di utilizzo cam da una lista di
        ///     percorsi indicati.
        /// </summary>
        /// <param name="reference">
        ///     list adi percorsi da cui prelevare le informazioni.
        /// </param>
        //**************************************************************************
        public void UpdateUsedCamInfo(TecnoPaths reference)
        {
            for (int i = 0; i < reference.Count; i++)
            {
                for (int j = 0; j < Count; j++)
                {
                    if (reference[i].Name == this[j].Name)
                        this[j].UpdateUsedCamInfo( reference[i] );
                }
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     ricalcola l'eventuale punto di approccio in funzione della matrice
        ///     di rototraslazione indicata.
        /// </summary>
        /// <returns>
        ///     false se non esiste un punto di approccio esplicito
        ///     true altrimenti
        /// </returns>
        ///**************************************************************************
        public void ApplyRototraslationToApproachPoint(RTMatrix rtm)
        {
            for (int i = 0; i < Count; i++)
                this[i].ApplyRototraslationToApproachPoint(rtm);
        }
        #endregion

        #region Private Methods

        #endregion

        #region Protected Methods

        ///**************************************************************************
        /// <summary>
        /// CreatePath: create a new path object
        /// </summary>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override Path CreatePath()
        {
            return (Path)new TecnoPath();
        }

        ///**************************************************************************
        /// <summary>
        /// CreatePath: create a new path object
        /// </summary>
        /// <param name="p">source path to copy from</param>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override Path CreatePath(Path p)
        {
            if (p is TecnoPath)
                return (Path)new TecnoPath((TecnoPath)p);
            return (Path) new TecnoPath(p);
        }

        ///**************************************************************************
        /// <summary>
        /// CreatePathCollection: create a new paths collection
        /// </summary>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override PathCollection CreatePathCollection()
        {
            return (PathCollection)new TecnoPaths();
        }

        #endregion

    }
}
