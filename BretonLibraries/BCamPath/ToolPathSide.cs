﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public interface IToolPathSide
    {
        #region Properties
        ///**************************************************************************
        /// <summary>
        ///     Impostazione delle informazioni specifiche del percorso 
        /// </summary>
        ///**************************************************************************
        ToolPathSideInfo Info
        {
            get;
            set;
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate del primo punto del lato
        /// </summary>
        ///**************************************************************************
        Point FirstPoint
        {
            get;
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate dell'ultimo punto del lato
        /// </summary>
        ///**************************************************************************
        Point LastPoint
        {
            get;
        }

        ///******************************************************************************
        /// <summary>
        /// Restituisce la lunghezza del lato
        /// </summary>
        ///******************************************************************************
        double Length
        {
            get;
        }

        #endregion

        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sul primo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        double TangentOnFirstPoint();

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sull'ultimo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        double TangentOnLastPoint();

        /// **************************************************************************
        /// <summary>
        ///     Ritorna il punto che si trova alla distanza indicata dall'inziio
        ///     del lato
        /// </summary>
        ///<param name="distance">
        ///     distanza alla quale cercare il punto
        ///</param>
        ///<returns>
        ///     punto trovato o null se la distanza e' incongruente
        ///</returns>
        /// **************************************************************************
        Point PointAtDistanceFromStart(double distance);

        ///**************************************************************************
        /// <summary>
        ///     Verifica se due lati sono uguali (come valori)
        /// </summary>
        /// <param name="obj">
        ///     lato da controllare verso quello corrente
        /// </param>
        /// <returns>
        ///     true i lati hanno gli stessi valori
        ///     false i lati hanno valori diversi
        /// </returns>
        ///**************************************************************************
        bool Equals(Object obj);
        #endregion
    }
}
