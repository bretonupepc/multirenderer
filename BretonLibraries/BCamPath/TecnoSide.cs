﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public interface ITecnoSide
    {
        #region Properties
        ///**************************************************************************
        /// <summary>
        ///     Impostazione delle informazioni specifiche del percorso
        /// </summary>
        ///**************************************************************************
        TecnoSideInfo Info
        {
            get;
            set;
        }

        ///**************************************************************************
        /// <summary>
        ///     Lunghezza del lato associato
        /// </summary>
        ///**************************************************************************
        double Length
        {
            get;
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate del primo punto del lato
        /// </summary>
        ///**************************************************************************
        Point FirstPoint
        {
            get;
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna le coordinate dell'ultimo punto del lato
        /// </summary>
        ///**************************************************************************
        Point LastPoint
        {
            get;
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna il massimo modulo di espansione utilizzato
        /// </summary>
        ///**************************************************************************
        double MaxExpantionModule
        {
            get;
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna l'elenco degli utensili utilizzati nelle generazione dei 
        ///     percorsi utensile.
        /// </summary>
        ///**************************************************************************
        Tools UsedTools
        {
            get;
        }

        #endregion

        #region Public Methods

        ///**************************************************************************
        /// <summary>
        ///     Rimuove i riferimenti agli utensili utilizzati.
        /// </summary>
        ///**************************************************************************
        void ResetUsedTools();

        ///**************************************************************************
        /// <summary>
        ///     Rimuove l'indicazione sui CAM abilitati nella generazione di
        ///     percorsi utensile per questo elemento.
        /// </summary>
        ///**************************************************************************
        void ResetEnabledCams();

        ///**************************************************************************
        /// <summary>
        ///     Rimuove l'indicazione sui CAM utilizzati nella generazione di
        ///     percorsi utensile per questo elemento.
        /// </summary>
        ///**************************************************************************
        void ResetUsedCams();

        ///**************************************************************************
        /// <summary>
        ///     Rimuove l'indicazione sul CAM indicato nella generazione di
        ///     percorsi utensile per questo elemento.
        /// </summary>
        ///**************************************************************************
        void ResetUsedCam(EN_CAM_ENABLED cam);

        ///**************************************************************************
        /// <summary>
        ///     Verifica se sul lato sono stati realizzati percorsi utensile con
        ///     CAM esterni.
        /// </summary>
        /// <returns>
        ///     true se e' stato utilizzato almento un cam esterno.
        ///     false altrimenti.
        /// </returns>
        ///**************************************************************************
        bool ExternalCamUsed();

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'elemento e' utilizzabile dalla lista di cam indicati
        ///     eventualmente anche parzialmente.
        /// </summary>
        /// <param name="cams_to_evaluate">
        ///     elenco dei cam da valutare per la creazione della lista
        /// </param>
        /// <param name="only_complete_elements">
        ///     se true indica che si valutano come utilizzabili solo elementi
        ///     che non sono stati utilizzati in nessun modo da altri cam;
        ///     se false si considerano anche elementi parzialmente lavorati
        ///     da altri cam.
        /// </param>
        /// <returns>
        ///     true se l'elemento e' compatibile con almeno uno dei cam indicati
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        bool IsAvailableForCAM(EN_CAM_ENABLED cams_to_evaluate, bool only_complete_elements);

        ///**************************************************************************
        /// <summary>
        ///     Ritorna la lista di parti dell'elemento che non sono state lavorate
        ///     dagli utensili previsti.
        /// </summary>
        /// <returns>
        ///     lista delle parti non lavorate nell'elemento corrente.
        ///     null se non ci sono parti non lavorate.
        /// </returns>
        ///**************************************************************************
        List<ITecnoSide> GetListOfUnFinishedIntervalsWithAvailableTools();

        ///**************************************************************************
        /// <summary>
        ///     Aggiunge un utensile alla lista degli utensili utilizzati
        ///     nella costruzione dei pecorsi utensile
        /// </summary>
        /// <param name="ti">
        ///     Utensile utilizzato
        /// </param>
        /// <param name="expansion_module">
        ///     modulo di espansione utilizzato
        /// </param>
        ///**************************************************************************
        void AddUsedTool(ToolInfo ti, double expansion_module);

        ///**************************************************************************
        /// <summary>
        ///     Verifica se un utensile e' stato utilizzato nel calcolo
        ///     dei percorsi utensile per questo lato.
        /// </summary>
        /// <param name="ti">
        ///     utensile da verificare
        /// </param>
        /// <returns>
        ///     true se l'utensile e' stato utilizzato
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        bool ToolWasUsed(ToolInfo ti);

        ///**************************************************************************
        /// <summary>
        ///     Ritorna il numero di utensili gia' utilizzati su questo lato.
        /// </summary>
        /// <returns>
        ///     numero di utensil utilizzati.
        /// </returns>
        ///**************************************************************************
        int NumberOfUsedTools();

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sul primo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        double TangentOnFirstPoint();

        ///**************************************************************************
        /// <summary>
        ///     Calcola la tangente sull'ultimo punto del lato, con direzione verso
        ///     l'interno del lato.
        /// </summary>
        /// <returns>
        ///     tangente calcolata [0,2pi).
        /// </returns>
        ///**************************************************************************
        double TangentOnLastPoint();

        /// ********************************************************************
        /// <summary>
        /// Tangente sul primo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sul primo punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        Vector Tangent(); //direzione 1 -> 2

        /// ********************************************************************
        /// <summary>
        /// Tangente sull'ultimo punto con verso da punto iniziale a punto finale
        /// </summary>
        /// <returns>tangente sull'ultimo punto punto con verso da punto iniziale a punto finale</returns>
        /// ********************************************************************
        Breton.MathUtils.Vector TangentAtEnd(); //direzione 1 -> 2

        /// ********************************************************************
        /// <summary>
        /// Tangente su coordinata relativa 0 ;&lt t ;&lt 1
        /// </summary>
        /// <returns>tangente su punto definito da 0 ;&lt t ;&lt 1</returns>
        /// ********************************************************************
        Breton.MathUtils.Vector TangentAt(double t); //direzione 1 -> 2 equivale a 0 ≤ t ≤ 1

        ///**************************************************************************
        /// <summary>
        ///     Verifica se due lati sono uguali (come valori)
        /// </summary>
        /// <param name="obj">
        ///     lato da controllare verso quello corrente
        /// </param>
        /// <returns>
        ///     true i lati hanno gli stessi valori
        ///     false i lati hanno valori diversi
        /// </returns>
        ///**************************************************************************
        bool Equals(Object obj);

        ///**************************************************************************
        /// <summary>
        ///     Se il lato prevede una lavorazione con angolo negativo, si
        ///     determina la zona di interferenza che deve essere considerata
        ///     nel calcolo delle collisioni con gli elementi originali; se
        ///     la lavorazione ha angolo positovo o nullo si ritorna il lato stesso.
        /// </summary>
        /// <returns>
        ///     percorso che determina la zona di interferenza generata da questo
        ///     lato.
        /// </returns>
        ///**************************************************************************
        Path GetInterferenceZone();

        #endregion
    }
}
