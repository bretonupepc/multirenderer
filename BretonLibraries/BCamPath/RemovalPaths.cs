﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class RemovalPaths : PathCollection
    {
		#region Constructors

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso vuoto
		/// </summary>
        ///*********************************************************************
        public RemovalPaths() : base()
		{
            szOriginalType = "RemovalPaths";
		}

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public RemovalPaths(RemovalPaths p) : base( (PathCollection)p )
		{
            szOriginalType = "RemovalPaths";
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public RemovalPaths(PathCollection p)
            : base(p)
        {
            szOriginalType = "RemovalPaths";
        }

        //Distruttore
        ~RemovalPaths()
		{
        }

        #endregion

        #region Properties

        ///**************************************************************************
        /// <summary>
        /// Indicizzazione della lista interna
        /// </summary>
        ///**************************************************************************
        public new RemovalPath this[int i]
        {
            get { return (RemovalPath)base[i]; }
        }

        #endregion

        #region Public Methods
        //**************************************************************************
        // GetPath
        // Ritorna il percorso specificato
        // Parametri:
        //			i	: indice percorso
        // Restituisce:
        //			percorso
        //**************************************************************************
        public new RemovalPath GetPath(int i)
        {
            return (RemovalPath)base.GetPath(i);
        }

        ///**************************************************************************
        /// <summary>
        /// Determina il percorso di asportazione generato dall'elemento 
        /// del perocorso utensile indicato
        /// </summary>
        /// <param name="tp">
        ///     riferimento al percorso utensile
        /// </param>
        /// <param name="sd">
        ///     riferimento all'elemento del percorso utensile
        /// </param>
        /// <returns>
        ///     riferimento al percorso di asportazione se esiste
        ///     null altrimenti
        /// </returns>
        ///**************************************************************************
        public RemovalPath GetRemovalToolPath(ToolPath tp, Side sd)
        {
            int index = tp.GetSideNumber(sd);
            return GetRemovalToolPath(tp, index);
        }

        ///**************************************************************************
        /// <summary>
        /// Determina il percorso di asportazione generato dall'elemento 
        /// del perocorso utensile indicato
        /// </summary>
        /// <param name="tp">
        ///     riferimento al percorso utensile
        /// </param>
        /// <param name="index">
        ///     riferimento (indice) dell'elemento del percorso utensile
        /// </param>
        /// <returns>
        ///     riferimento al percorso di asportazione se esiste
        ///     null altrimenti
        /// </returns>
        ///**************************************************************************
        public RemovalPath GetRemovalToolPath(ToolPath tp, int index)
        {
            for (int i = 0; i < Count; i++)
            {
                RemovalPath rp = (RemovalPath)this[i];
                if (rp.TestParent(tp, index))
                    return rp;
            }
            return null;
        }

        ///**************************************************************************
        /// <summary>
        ///     Crea una lista di percorsi dopo aver applicato la matrice di 
        ///     rototraslazione indicata
        /// </summary>
        /// <param name="rtm">
        ///     matrice da considerare
        /// </param>
        /// <returns>
        ///     percorsi ruototraslati
        /// </returns>
        ///**************************************************************************
        public RemovalPaths GetRT(RTMatrix rtm)
        {
            RemovalPaths rmr = new RemovalPaths();
            szOriginalType = "RemovalPaths";
            for (int i = 0; i < Count; i++)
            {
                Path p = new Path(this[i]);
                p.MatrixRT = rtm;
                rmr.AddPath(p.GetPathRT());
            }
            return rmr;
        }

        ///**************************************************************************
        /// <summary>
        /// rettifica la posizione del percorso in base alle matrici indicate.
        /// </summary>
        /// <param name="org_rtm">
        ///     matrice di rototraslazione iniziale.
        /// </param>
        /// <param name="new_rtm">
        ///     matrice di rototraslazione finale.
        /// </param>
        ///**************************************************************************
        public void Normalize(RTMatrix org_rtm, RTMatrix new_rtm)
        {
            for (int i = 0; i < Count; i++)
                this[i].Normalize(org_rtm, new_rtm);
        }

        #endregion

        #region Protected Methods

        ///**************************************************************************
        /// <summary>
        /// CreatePath: create a new path object
        /// </summary>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override Path CreatePath()
        {
            return (Path)new RemovalPath();
        }

        ///**************************************************************************
        /// <summary>
        /// CreatePath: create a new path object
        /// </summary>
        /// <param name="p">source path to copy from</param>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override Path CreatePath(Path p)
        {
            if (p is RemovalPath)
                return (Path)new RemovalPath((RemovalPath)p);
            return (Path)new RemovalPath(p);
        }

        ///**************************************************************************
        /// <summary>
        /// CreatePathCollection: create a new paths collection
        /// </summary>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override PathCollection CreatePathCollection()
        {
            return (PathCollection)new RemovalPaths();
        }

        #endregion

    }
}
