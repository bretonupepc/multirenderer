﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Globalization;
using Breton.MathUtils;

using Breton.Polygons;
using TraceLoggers;

namespace Breton.BCamPath
{
    [Flags]
    public enum EN_CAM_ENABLED
    {
        None = 0,
        CAMTaglio = 1,
        RhinoNC = 2
    }

    public enum EN_VERTEX_DISK_MODE
    {
        EN_VERTEX_DISK_MODE_COMPLETE,                                   // sul vertice non si assume nessun vincolo
        EN_VERTEX_DISK_MODE_AUTO_COLLISION,                             // si valutano i vincoli di collisione con altri oggetti
        EN_VERTEX_DISK_MODE_DEFAULT_INTERRUPT,                          // vincolo dovuto alla profondita' di taglio
        EN_VERTEX_DISK_MODE_USER_INTERRUPT,                             // vincolo imposto dall'utente
        EN_VERTEX_DISK_MODE_BOUNDARY_EXTENSION,                         // vincolo generato dall'estensione al bordo lastra
        EN_VERTEX_DISK_MODE_NUM_OF
    }

    public enum EN_SIDE_LEADIN
    {
        EN_SIDE_LEADIN_DISABLED,                                        // non e' definito un leadin specifico per questo lato
        EN_SIDE_LEADIN_PIERCING_DEFINED                                 // e' definito un punto di piercing esplicito per questo lato
    }

    public enum EN_SIDE_DIRECTION
    {
        EN_SIDE_DIRECTION_DEFAULT,                                      // direzione di sviluppo dei percorsi utensile standard
        EN_SIDE_DIRECTION_P1_P2,                                        // direzione di sviluppo dei percorsi vincolata da P1 a P2
        EN_SIDE_DIRECTION_P2_P1,                                        // direzione di sviluppo dei percorsi vincolata da P2 a P1
    }

    public class TecnoPieceInfo
    {
        #region consts enums
        public const string PREFIX_SLAB_PERIMETER = "SlabPerimeter";
        public const string PREFIX_RECUPERO = "SlabRectangle";
        public const string PREFIX_RECUPERO_PENDING_CREATION = "SlabRectanglePendingCreation";
        public const string PREFIX_SINGLE_CUT = "SlabCut";
        public const string PREFIX_FRAME = "Unframe";
        public const string SUFFIX_DISCARDED = "[DISCARDED]";

        public const string KEY_TOOL_NUMBER = "ToolNumber";
        public const string KEY_TOOL_ID = "ToolId";
        #endregion
    }

    public class TecnoSideInfo
    {
        #region Variables
        private EN_VERTEX_DISK_MODE enFirstVertexDiskMode;              // indicates how to handle Disk Tool Path on First Vertex

        private EN_VERTEX_DISK_MODE enSecondVertexDiskMode;             // indicates how to handle Disk Tool Path on Second Vertex

        private double dfFirstVertexDiskUserInterruptOffset;            // if user interrupt mode has been selected for first vertex
                                                                        // this parameter indicates the distance from the vertex where
                                                                        // the interrupt should take place; note that if the value is
                                                                        // negative, the effective position is outside the segment

        private double dfSecondVertexDiskUserInterruptOffset;           // if user interrupt mode has been selected for second vertex
                                                                        // this parameter indicates the distance from the vertex where
                                                                        // the interrupt should take place; note that if the value is
                                                                        // negative, the effective position is outside the segment

        private TecnoPathInfo clTecnoPathInfo;                          // specific technological informations derived from full path

        private double dfSlope;                                         // tool slope against XY plane (0 = vertical)

        private double dfFirstVertexInterruptOffset;                    // indicates actual distance that remains to be completed with respect
                                                                        // to the first vertex; this value should be intialized, by the owner,
                                                                        // to the maximum double value and at the end (i.e. after all tools application)
                                                                        // it should be zero or negative; if not it means that the side has not been 
                                                                        // completed and shall be manually; note that this value might be negative, 
                                                                        // indicating that at least one tools has gone over the side boundary 
        private double dfFirstVertexDiskInterruptOffset;                // as dfFirstVertexInterruptOffset but only with disk tolpaths

        private double dfSecondVertexInterruptOffset;                   // indicates actual distance that remains to be completed with respect
                                                                        // to the second vertex; this value should be intialized, by the owner,
                                                                        // to the maximum double value and at the end (i.e. after all tools application)
                                                                        // it should be zero or negative; if not it means that the side has not been 
                                                                        // completed and shall be manually; note that this value might be negative, 
                                                                        // indicating that at least one tools has gone over the side boundary 
        private double dfSecondVertexDiskInterruptOffset;               // as dfSecondVertexInterruptOffset but only with disk tolpaths

        private double dfSideLength;                                    // length of the linked side

        private bool bIsEnabled;                                        // indicates if tool paths for this side should be generated

        private bool bExcludeBore;                                      // indicates if bore is excluded by user

        private bool bExcludeDisk;                                      // indicates if disk is excluded by user

        private bool bExcludeWJ;                                        // indicates if WJ is excluded by user

        private string bExternalCam;                                    // indicates CAM data

        private EN_CAM_ENABLED bEnabledCAM;                             // CAM enabled

        private EN_CAM_ENABLED bDefaultEnabledCAM;                      // CAM enabled from default parameters to be added to bEnabledCAM

        private EN_CAM_ENABLED bUsedCAM;                                // CAM used on derived tool paths

        private double dfFirstVertexDiskDefaultExtension;               // for disk tools, this parameter indicates the entity of the
                                                                        // extension to be applied to the first vertex of the side
                                                                        // this parameter will be used if there are no collisions
                                                                        // on the first vertex against other geometries

        private double dfFirstVertexDiskDefaultReduction;               // for disk tools, this parameter indicates the entity of the
                                                                        // extension to be applied to the first vertex of the side
                                                                        // this parameter will be used if there are collisions
                                                                        // on the first vertex against other geometries

        private double dfSecondVertexDiskDefaultExtension;              // for disk tools, this parameter indicates the entity of the
                                                                        // extension to be applied to the second vertex of the side
                                                                        // this parameter will be used if there are no collisions
                                                                        // on the second vertex against other geometries

        private double dfSecondVertexDiskDefaultReduction;              // for disk tools, this parameter indicates the entity of the
                                                                        // extension to be applied to the second vertex of the side
                                                                        // this parameter will be used if there are collisions
                                                                        // on the second vertex against other geometries

        private EN_SIDE_LEADIN enSideLeadIn;                            // side specific lead in handling

        private Point cPiercingPoint;                                   // explicit piercing point for side specific lead in 

        private bool bConsiderForCollision;                             // indicate if this element need to be considered during
                                                                        // collision detection between tool paths and original 
                                                                        // geometries

        private EN_SIDE_DIRECTION enSideDirection;                      // tool path direction (default or forced)

        private double dfWantedDrillRadius;                             // requested drill radius to be honored by available tools
        
        private List<string> lAttributesSide;                           // list of side attributes

        private bool bStartOnGroove;                                    // indicate that this element was not completed and residual part
                                                                        // to the end of the element start on the groove of a previus tool
        private bool bEndOnGroove;                                      // indicate that this element was not completed and residual part
                                                                        // from the start of the elementend on the groove of a previus tool
        private double dfGrooveSize;                                    // groove size

        private bool bEnableBevel;                                      // bevel working enabled on side

        private int bCutQuality;                                        // cut quality, has value if != -1

        private bool bDiskForInterruptedSlopedCuts;                     // create disk toolpaths that do not complete a side

        private bool bFirstVertexCompletedWithAvailableTools;           //20160225 per il momento è gestito solo per waterjet, per considerare completato nei confronti di RhinoNC un vertice con una concavità nel materiale che viene eseguito con waterjet 3 assi

        private bool bSecondVertexCompletedWithAvailableTools;          //20160225 per il momento è gestito solo per waterjet, per considerare completato nei confronti di RhinoNC un vertice con una concavità nel materiale che viene eseguito con waterjet 3 assi

        private bool bIsOutsideToolpathLimits;                          //20160922 (LimitsToolpath.Enable)Abilitazione limiti toolpaths per evitare taglio geometrie rialzate della table rispetto al piano di appoggio

        private int iIndexSequenceSimulazioneNC;                        //20160928 indice di richiesta di simulazioneNC dei tagli disco generati da questo side

        #endregion

		#region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Constructor
		/// </summary>
        ///*********************************************************************
        public TecnoSideInfo()
		{
            enFirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_COMPLETE;
            enSecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_COMPLETE;
            dfFirstVertexDiskUserInterruptOffset = 0;
            dfSecondVertexDiskUserInterruptOffset = 0;
            dfFirstVertexInterruptOffset = 0;
            dfSecondVertexInterruptOffset = 0;
            dfFirstVertexDiskInterruptOffset = 0;
            dfSecondVertexDiskInterruptOffset = 0;
            clTecnoPathInfo = new TecnoPathInfo();
            dfSlope = 0;
            bIsEnabled = true;
            bExcludeBore = false;
            bExcludeDisk = false;
            bExcludeWJ = false;
            bExternalCam = null;
            bEnabledCAM = EN_CAM_ENABLED.CAMTaglio;
            bDefaultEnabledCAM = EN_CAM_ENABLED.None;
            bUsedCAM = EN_CAM_ENABLED.None;
            dfFirstVertexDiskDefaultExtension = 0;
            dfFirstVertexDiskDefaultReduction = 0;
            dfSecondVertexDiskDefaultExtension = 0;
            dfSecondVertexDiskDefaultReduction = 0;
            enSideLeadIn = EN_SIDE_LEADIN.EN_SIDE_LEADIN_DISABLED;
            cPiercingPoint = null;
            bConsiderForCollision = true;
            enSideDirection = EN_SIDE_DIRECTION.EN_SIDE_DIRECTION_DEFAULT;
            dfWantedDrillRadius = 0;
            bStartOnGroove = false;
            bEndOnGroove = false;
            dfGrooveSize = 0;
            bCutQuality = -1;
            bDiskForInterruptedSlopedCuts = true;
            bFirstVertexCompletedWithAvailableTools = false;
            bSecondVertexCompletedWithAvailableTools = false;
            bIsOutsideToolpathLimits = false;
            iIndexSequenceSimulazioneNC = -1;
		}

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor 
        /// </summary>
        /// <param name="tsi">source object to copy from</param>
        ///*********************************************************************
        public TecnoSideInfo(TecnoSideInfo tsi)
		{
            enFirstVertexDiskMode = tsi.enFirstVertexDiskMode;
            enSecondVertexDiskMode = tsi.enSecondVertexDiskMode;
            dfFirstVertexDiskUserInterruptOffset = tsi.dfFirstVertexDiskUserInterruptOffset;
            dfSecondVertexDiskUserInterruptOffset = tsi.dfSecondVertexDiskUserInterruptOffset;
            dfFirstVertexInterruptOffset = tsi.dfFirstVertexInterruptOffset;
            dfSecondVertexInterruptOffset = tsi.dfSecondVertexInterruptOffset;
            dfFirstVertexDiskInterruptOffset = tsi.dfFirstVertexDiskInterruptOffset;
            dfSecondVertexDiskInterruptOffset = tsi.dfSecondVertexDiskInterruptOffset;
            clTecnoPathInfo = new TecnoPathInfo(tsi.clTecnoPathInfo);
            dfSlope = tsi.dfSlope;
            bIsEnabled = tsi.bIsEnabled;
            bExcludeBore = tsi.bExcludeBore;
            bExcludeDisk = tsi.bExcludeDisk;
            bExcludeWJ = tsi.bExcludeWJ;
            bExternalCam = tsi.ExternalCam;
            bEnabledCAM = tsi.EnabledCAM;
            bDefaultEnabledCAM = tsi.DefaultEnabledCAM;
            bUsedCAM = tsi.bUsedCAM;
            dfFirstVertexDiskDefaultExtension = tsi.dfFirstVertexDiskDefaultExtension;
            dfFirstVertexDiskDefaultReduction = tsi.dfFirstVertexDiskDefaultReduction;
            dfSecondVertexDiskDefaultExtension = tsi.dfSecondVertexDiskDefaultExtension;
            dfSecondVertexDiskDefaultReduction = tsi.dfSecondVertexDiskDefaultReduction;
            enSideLeadIn = tsi.enSideLeadIn;
            if (tsi.cPiercingPoint != null)
                cPiercingPoint = new Point(tsi.cPiercingPoint);
            else
                cPiercingPoint = null;
            bConsiderForCollision = tsi.bConsiderForCollision;
            enSideDirection = tsi.enSideDirection;
            dfWantedDrillRadius = tsi.dfWantedDrillRadius;
            if (tsi.AttributeSide != null)
                lAttributesSide = new List<string>(tsi.AttributeSide);
            else
                lAttributesSide = null;
            bStartOnGroove = tsi.bStartOnGroove;
            bEndOnGroove = tsi.bEndOnGroove;
            dfGrooveSize = tsi.dfGrooveSize;
            bEnableBevel = tsi.bEnableBevel;
            bCutQuality = tsi.bCutQuality;
            bDiskForInterruptedSlopedCuts = tsi.bDiskForInterruptedSlopedCuts;
            bFirstVertexCompletedWithAvailableTools = tsi.bFirstVertexCompletedWithAvailableTools;
            bSecondVertexCompletedWithAvailableTools = tsi.bSecondVertexCompletedWithAvailableTools;
            bIsOutsideToolpathLimits = tsi.bIsOutsideToolpathLimits;
            iIndexSequenceSimulazioneNC = tsi.iIndexSequenceSimulazioneNC;
        }
			
		#endregion

		#region Properties
        public EN_VERTEX_DISK_MODE FirstVertexDiskMode
        {
            get { return enFirstVertexDiskMode; }
            set { enFirstVertexDiskMode = value; }
        }
        
        public EN_VERTEX_DISK_MODE SecondVertexDiskMode
        {
            get { return enSecondVertexDiskMode; }
            set { enSecondVertexDiskMode = value; }
        }

        public double FirstVertexDiskUserInterruptOffset
        {
            get { return dfFirstVertexDiskUserInterruptOffset; }
            set { dfFirstVertexDiskUserInterruptOffset = value; }
        } 

        public double SecondVertexUserInterruptOffset
        {
            get { return dfSecondVertexDiskUserInterruptOffset; }
            set { dfSecondVertexDiskUserInterruptOffset = value; }
        }

        public TecnoPathInfo TecnoPathInfo
        {
            get { return clTecnoPathInfo; }
            set { clTecnoPathInfo = value; }
        }

        public double Slope
        {
            get { return dfSlope; }
            set { dfSlope = value; }
        }

        public double FirstVertexInterruptOffset
        {
            get { return dfFirstVertexInterruptOffset; }
            set { dfFirstVertexInterruptOffset = value; }
        }

        public double FirstVertexDiskInterruptOffset
        {
            get { return dfFirstVertexDiskInterruptOffset; }
            set { dfFirstVertexDiskInterruptOffset = value; }
        }

        public double SecondVertexInterruptOffset
        {
            get { return dfSecondVertexInterruptOffset; }
            set { dfSecondVertexInterruptOffset = value; }
        }

        public double SecondVertexDiskInterruptOffset
        {
            get { return dfSecondVertexDiskInterruptOffset; }
            set { dfSecondVertexDiskInterruptOffset = value; }
        }

        public double SideLength
        {
            get { return dfSideLength; }
            set { dfSideLength = value; }
        }
        
        public bool IsEnabled
        {
            get { return bIsEnabled; }
            set { bIsEnabled = value; }
        }

        public bool ExcludeBore
        {
            get { return bExcludeBore; }
            set { bExcludeBore = value; }
        }

        public bool ExcludeDisk
        {
            get { return bExcludeDisk; }
            set { bExcludeDisk = value; }
        }

        public bool ExcludeWJ
        {
            get { return bExcludeWJ; }
            set { bExcludeWJ = value; }
        }

        public string ExternalCam
        {
            get { return bExternalCam; }
            set { bExternalCam = value; }
        }

        public EN_CAM_ENABLED EnabledCAM
        {
            get { return bEnabledCAM; }
            set { bEnabledCAM = value; }
        }

        public EN_CAM_ENABLED DefaultEnabledCAM
        {
            get { return bDefaultEnabledCAM; }
            set { bDefaultEnabledCAM = value; }
        }

        public EN_CAM_ENABLED UsedCAM
        {
            get { return bUsedCAM; }
            set { bUsedCAM = value; }
        }

        public double FirstVertexDiskDefaultExtension
        {
            get { return dfFirstVertexDiskDefaultExtension; }
            set { dfFirstVertexDiskDefaultExtension = value; }
        }

        public double FirstVertexDiskDefaultReduction
        {
            get { return dfFirstVertexDiskDefaultReduction; }
            set { dfFirstVertexDiskDefaultReduction = value; }
        }

        public double SecondVertexDiskDefaultExtension
        {
            get { return dfSecondVertexDiskDefaultExtension; }
            set { dfSecondVertexDiskDefaultExtension = value; }
        }
        
        public double SecondVertexDiskDefaultReduction
        {
            get { return dfSecondVertexDiskDefaultReduction; }
            set { dfSecondVertexDiskDefaultReduction = value; }
        }

        public bool ConsiderForCollision
        {
            get { return bConsiderForCollision; }
            set { bConsiderForCollision = value; }
        }

        public EN_SIDE_DIRECTION SideDirection
        {
            get { return enSideDirection; }
            set { enSideDirection = value; }
        }

        public double WantedDrillRadius
        {
            get { return dfWantedDrillRadius; }
            set { dfWantedDrillRadius = value; }
        }

        public List<string> AttributeSide
        {
            get { return lAttributesSide; }
            set { lAttributesSide = value; }
        }

        public bool StartOnGroove
        {
            get { return bStartOnGroove; }
            set { bStartOnGroove = value;  }
        }

        public bool EndOnGroove
        {
            get { return bEndOnGroove; }
            set { bEndOnGroove = value; }
        }

        public double GrooveSize
        {
            get { return dfGrooveSize; }
            set { dfGrooveSize = value; }
        }

        public bool Bevel
        {
            get { return bEnableBevel; }
            set { bEnableBevel = value; }
        }

        public int CutQuality
        {
            get { return bCutQuality; }
            set { bCutQuality = value; }
        }
        public bool CutQualityHasValue { get { return bCutQuality != -1; } }

        public bool DiskForInterruptedSlopedCuts
        {
            get { return bDiskForInterruptedSlopedCuts; }
            set { bDiskForInterruptedSlopedCuts = value; }
        }

        public bool FirstVertexCompletedWithAvailableTools
        {
            get { return bFirstVertexCompletedWithAvailableTools; }
            set { bFirstVertexCompletedWithAvailableTools = value; }
        }

        public bool SecondVertexCompletedWithAvailableTools
        {
            get { return bSecondVertexCompletedWithAvailableTools; }
            set { bSecondVertexCompletedWithAvailableTools = value; }
        }

        public bool IsOutsideToolpathLimits
        {
            get { return bIsOutsideToolpathLimits; }
            set { bIsOutsideToolpathLimits = value; }
        }

        public int IndexSequenceSimulazioneNC
        {
            get { return iIndexSequenceSimulazioneNC; }
            set { iIndexSequenceSimulazioneNC = value; }
        }

        #endregion

		#region Public Methods
        ///*************************************************************************
        /// <summary>
        ///     Verifica se e' ammesso l'uso di cam esterni.
        /// </summary>
        /// <returns>
        ///     true se e' ammesso l'uso di cam esterni.
        ///     false altrimenti.
        /// </returns>
        ///**************************************************************************
        public virtual bool ExternalCamAllowed()
        {
            if (bEnabledCAM == EN_CAM_ENABLED.None)
                return false;
            if (bEnabledCAM == EN_CAM_ENABLED.CAMTaglio)
                return false;
            return true;
        }

        ///*************************************************************************
        /// <summary>
        ///     Verifica se e' ammesso l'uso del cam indicato.
        /// </summary>
        /// <param name="cam">
        ///     codice del cam per il quale verificare l'abilitazione all'uso
        /// </param>
        /// <returns>
        ///     true se e' ammesso l'uso del cam indicato.
        ///     false altrimenti.
        /// </returns>
        ///**************************************************************************
        public virtual bool CamAllowed(EN_CAM_ENABLED cam)
        {
            if ((bEnabledCAM & cam) == cam)
                return true;
            return false;
        }

        ///*************************************************************************
        /// <summary>
        ///     Verifica se sul lato sono stati realizzati percorsi utensile con
        ///     CAM esterni.
        /// </summary>
        /// <returns>
        ///     true se e' stato utilizzato almento un cam esterno.
        ///     false altrimenti.
        /// </returns>
        ///**************************************************************************
        public virtual bool ExternalCamUsed()
        {
            if (bUsedCAM == EN_CAM_ENABLED.None)
                return false;
            if (bUsedCAM == EN_CAM_ENABLED.CAMTaglio)
                return false;
            return true;
        }

        ///*************************************************************************
        /// <summary>
        ///     Verifica se sul lato sono stati realizzati percorsi utensile con
        ///     il CAM indicato.
        /// </summary>
        /// <param name="cam">
        ///     codice del cam per il quale verificare l'uso
        /// </param>
        /// <returns>
        ///     true se il cam e' stato utilizzato.
        ///     false altrimenti.
        /// </returns>
        ///**************************************************************************
        public virtual bool CamUsed(EN_CAM_ENABLED cam)
        {
            if ((bUsedCAM & cam) == cam)
                return true;
            return false;
        }

        ///*************************************************************************
        /// <summary>
        ///     Reset dell'indicatore sull'uso del cam indicato.
        /// </summary>
        /// <param name="cam">
        ///     codice del cam da eliminare nella lista in uso.
        /// </param>
        ///**************************************************************************
        public virtual void ResetUsedCam(EN_CAM_ENABLED cam)
        {
            bUsedCAM &= cam;
        }

        ///*************************************************************************
        /// <summary>
        ///     impostazione dei parametri interni che sono legati alla lunghezza
        ///     del lato associato.
        /// </summary>
        /// <param name="sidelen">
        ///     lunghezza del lato associato
        /// </param>
        /// <remarks>
        ///     questa funzione deve essere richiamata prima di iniziare il calcolo
        ///     dei percorsi utensili; imposta le distanze residue rispetto al primo
        ///     e al secondo vertice.
        /// </remarks>
        ///**************************************************************************
        public virtual void SetSideLength(double sidelen)
        {
            SideLength = sidelen;
            FirstVertexInterruptOffset = double.MaxValue;
            SecondVertexInterruptOffset = double.MaxValue;
            StartOnGroove = false;
            EndOnGroove = false;
            GrooveSize = 0;
        }

        ///**************************************************************************
        /// <summary>
        ///     verifica se il lato e' stato attivamente processato da almeno un 
        ///     utensile, cioe' se e' stato prodotto un percorso utensile.
        /// </summary>
        /// <returns>
        ///     true se almeno un utensile ha generato un percorso non nullo
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool AnyToolApplyed()
        {
            if (dfFirstVertexInterruptOffset == double.MaxValue)
                return false;
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     recupera le eventuali coordinate del punto di piercing definito 
        ///     dall'utente .
        /// </summary>
        /// <param name="x">
        ///     riferimento alla eventuale coordinata X del punto di piercing.
        /// </param>
        /// <param name="y">
        ///     riferimento alla eventuale coordinata Y del punto di piercing.
        /// </param>
        /// <returns>
        ///     false   se il percorso non prevede un punto di piercing definito
        ///             dall'utente
        ///     true    le coordinate del punto di piercing sono state valorizzate.
        /// </returns>
        ///**************************************************************************
        public bool GetUserDefinedPiercingPoint(ref double x, ref double y)
        {
            if (HasUserDefinedPiercingPoint() == false)
                return false;

            x = cPiercingPoint.X;
            y = cPiercingPoint.Y;
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     impostazione delle coordinate del punto di piercing.
        /// </summary>
        /// <param name="x">
        ///     coordinata X del punto di piercing.
        /// </param>
        /// <param name="y">
        ///     coordinata Y del punto di piercing.
        /// </param>
        ///**************************************************************************
        public void SetUserDefinedPiercingPoint(double x, double y)
        {
            if (cPiercingPoint == null)
                cPiercingPoint = new Point();
            cPiercingPoint.X = x;
            cPiercingPoint.Y = y;
            enSideLeadIn = EN_SIDE_LEADIN.EN_SIDE_LEADIN_PIERCING_DEFINED;
        }

        ///**************************************************************************
        /// <summary>
        ///     rimuove le indicazioni del punto di piercing sul percorso utensile
        /// </summary>
        ///**************************************************************************
        public void RemoveUserdefinedPiercingPoint()
        {
            enSideLeadIn = EN_SIDE_LEADIN.EN_SIDE_LEADIN_DISABLED;
            cPiercingPoint = null;
        }

        ///**************************************************************************
        /// <summary>
        ///     verifica se e' definito un punto di approccio esplicito.
        /// </summary>
        /// <returns>
        ///     false se non esiste un punto di approccio esplicito
        ///     true altrimenti
        /// </returns>
        ///**************************************************************************
        public bool HasUserDefinedPiercingPoint()
        {
            if (enSideLeadIn == EN_SIDE_LEADIN.EN_SIDE_LEADIN_DISABLED)
                return false;
            if (cPiercingPoint == null)
                return false;
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     ricalcola l'eventuale punto di approccio in funzione della matrice
        ///     di rototraslazione indicata.
        /// </summary>
        /// <returns>
        ///     false se non esiste un punto di approccio esplicito
        ///     true altrimenti
        /// </returns>
        ///**************************************************************************
        public void ApplyRototraslationToApproachPoint(RTMatrix rtm)
        {
            if (HasUserDefinedPiercingPoint())
            {
                double ax = 0, ay = 0;
                if (GetUserDefinedPiercingPoint(ref ax, ref ay))
                {
                    Point pa = new Point(ax, ay);
                    pa = pa.RotoTrasl(rtm);
                    SetUserDefinedPiercingPoint(pa.X, pa.Y);
                }
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     lettura di parametri accessori previsti da classi derivate
        /// </summary>
        /// <param name="n">
        ///     nodo XML contenete il lato
        /// </param>
        /// <returns>
        ///			true	lettura eseguita con successo
        ///			false	errore nella lettura
        /// </returns>
        ///**************************************************************************
        public virtual bool ReadDerivedFileXml(XmlNode n)
        {
            try
            {
                XmlNodeList l = n.ChildNodes;

                // Scorre tutti i sottonodi
                foreach (XmlNode chn in l)
                {
                    // Cerca il nodo TecnoPathInfo
                    if (chn.Name != "TecnoSideInfo")
                        continue;
					
                    // legge i parametri noti
					dfFirstVertexDiskUserInterruptOffset = Convert.ToDouble(chn.SelectSingleNode("FirstVertexDiskUserInterruptOffset").Attributes.GetNamedItem("value").Value);
					dfSecondVertexDiskUserInterruptOffset = Convert.ToDouble(chn.SelectSingleNode("SecondVertexDiskUserInterruptOffset").Attributes.GetNamedItem("value").Value);
					enFirstVertexDiskMode = GetDiskModeFromString(chn.SelectSingleNode("FirstVertexDiskMode").Attributes.GetNamedItem("value").Value);
					enSecondVertexDiskMode = GetDiskModeFromString(chn.SelectSingleNode("SecondVertexDiskMode").Attributes.GetNamedItem("value").Value);
                    if (chn.SelectSingleNode("Slope") != null)
                        dfSlope = Convert.ToDouble(chn.SelectSingleNode("Slope").Attributes.GetNamedItem("value").Value);
                    if (chn.SelectSingleNode("FirstVertexInterruptOffset") != null)
                    {
                        if (double.MaxValue.ToString() == chn.SelectSingleNode("FirstVertexInterruptOffset").Attributes.GetNamedItem("value").Value)
                            dfFirstVertexInterruptOffset = double.MaxValue;
                        else
                            dfFirstVertexInterruptOffset = Convert.ToDouble(chn.SelectSingleNode("FirstVertexInterruptOffset").Attributes.GetNamedItem("value").Value);
                    }
                    if (chn.SelectSingleNode("SecondVertexInterruptOffset") != null)
                    {
                        if (double.MaxValue.ToString() == chn.SelectSingleNode("SecondVertexInterruptOffset").Attributes.GetNamedItem("value").Value)
                            dfSecondVertexInterruptOffset = double.MaxValue;
                        else
                            dfSecondVertexInterruptOffset = Convert.ToDouble(chn.SelectSingleNode("SecondVertexInterruptOffset").Attributes.GetNamedItem("value").Value);
                    }
                    if (chn.SelectSingleNode("EnabledCAMS") != null)
                    {
                        string value = chn.SelectSingleNode("EnabledCAMS").Attributes.GetNamedItem("value").Value;
                        Object obj = Enum.Parse(typeof(EN_CAM_ENABLED), value);
                        if (obj != null)
                            EnabledCAM = (EN_CAM_ENABLED)obj;
                    }
                    if (chn.SelectSingleNode("DefaultEnabledCAMS") != null)
                    {
                        string value = chn.SelectSingleNode("DefaultEnabledCAMS").Attributes.GetNamedItem("value").Value;
                        Object obj = Enum.Parse(typeof(EN_CAM_ENABLED), value);
                        if (obj != null)
                            DefaultEnabledCAM = (EN_CAM_ENABLED)obj;
                    }
                    if (chn.SelectSingleNode("UsedCAMS") != null)
                    {
                        string value = chn.SelectSingleNode("UsedCAMS").Attributes.GetNamedItem("value").Value;
                        Object obj = Enum.Parse(typeof(EN_CAM_ENABLED), value);
                        if (obj != null)
                            UsedCAM = (EN_CAM_ENABLED)obj;
                    }
                    if (chn.SelectSingleNode("SideLeadIn") != null)
                    {
                        //EN_SIDE_LEADIN ensl = EN_SIDE_LEADIN.EN_SIDE_LEADIN_PIERCING_DEFINED;
                        XmlNode xn = chn.SelectSingleNode("SideLeadIn");
                        XmlAttribute xa = xn.Attributes["Type"];
                        if (xa != null)
                        {
                            if (xa.Value == "EN_SIDE_LEADIN_PIERCING_DEFINED")
                            {
                                //ensl = EN_SIDE_LEADIN.EN_SIDE_LEADIN_PIERCING_DEFINED;
                                XmlNode xnp = xn.SelectSingleNode("PiercingPoint");
                                if (xnp != null)
                                {
                                    double x;
                                    double y;
                                    xa = xnp.Attributes["X"];
                                    if (xa != null)
                                    {
                                        x = Convert.ToDouble(xa.Value, CultureInfo.InvariantCulture);
                                        xa = xnp.Attributes["Y"];
                                        if (xa != null)
                                        {
                                            y = Convert.ToDouble(xa.Value, CultureInfo.InvariantCulture);
                                            SetUserDefinedPiercingPoint( x, y );
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (chn.SelectSingleNode("ConsiderForCollision") != null)
                    {
                        string value = chn.SelectSingleNode("ConsiderForCollision").Attributes.GetNamedItem("value").Value.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        if (value == "TRUE" || value == "1")
                            ConsiderForCollision = true;
                        else
                            ConsiderForCollision = false;
                    }
                    if (chn.SelectSingleNode("SideDirection") != null)
                    {
                        string value = chn.SelectSingleNode("SideDirection").Attributes.GetNamedItem("value").Value.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        int ival = Convert.ToInt32(value);
                        if (ival == 1)
                            enSideDirection = EN_SIDE_DIRECTION.EN_SIDE_DIRECTION_P1_P2;
                        else
                            if (ival == 2)
                                enSideDirection = EN_SIDE_DIRECTION.EN_SIDE_DIRECTION_P2_P1;
                            else
                                enSideDirection = EN_SIDE_DIRECTION.EN_SIDE_DIRECTION_DEFAULT;
                    }
                    if (chn.SelectSingleNode("Enabled") != null)
                    {
                        string value = chn.SelectSingleNode("Enabled").Attributes.GetNamedItem("value").Value.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        if (value == "TRUE" || value == "1")
                            IsEnabled = true;
                        else
                            IsEnabled = false;
                    }
                    if (chn.SelectSingleNode("ExcludeBore") != null)
                    {
                        string value = chn.SelectSingleNode("ExcludeBore").Attributes.GetNamedItem("value").Value.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        if (value == "TRUE" || value == "1")
                            ExcludeBore = true;
                        else
                            ExcludeBore = false;
                    }
                    if (chn.SelectSingleNode("ExcludeDisk") != null)
                    {
                        string value = chn.SelectSingleNode("ExcludeDisk").Attributes.GetNamedItem("value").Value.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        if (value == "TRUE" || value == "1")
                            ExcludeDisk = true;
                        else
                            ExcludeDisk = false;
                    }
                    if (chn.SelectSingleNode("ExcludeWJ") != null)
                    {
                        string value = chn.SelectSingleNode("ExcludeWJ").Attributes.GetNamedItem("value").Value.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        if (value == "TRUE" || value == "1")
                            ExcludeWJ = true;
                        else
                            ExcludeWJ = false;
                    }
                    if (chn.SelectSingleNode("CutQuality") != null)
                    {
                        CutQuality = Convert.ToInt32(chn.SelectSingleNode("CutQuality").Attributes.GetNamedItem("value").Value);
                    }
                    if (chn.SelectSingleNode("EnableBevel") != null)
                    {
                        string value = chn.SelectSingleNode("EnableBevel").Attributes.GetNamedItem("value").Value.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
                        if (value == "TRUE" || value == "1")
                            Bevel = true;
                        else
                            Bevel = false;
                    }
                    if (chn.SelectSingleNode("AttributeSide") != null)
                    {
                        var xAttributeSide = chn.SelectSingleNode("AttributeSide");
                        int count;
                        if(xAttributeSide.Attributes.GetNamedItem("count") != null && int.TryParse(xAttributeSide.Attributes.GetNamedItem("count").Value, out count) && count > 0)
                        {
                            AttributeSide = new List<string>();
                            foreach(var ch_xAttributeSide in xAttributeSide)
                            {
                                if(ch_xAttributeSide is XmlElement)
                                {
                                    var xChAttributeSide = ch_xAttributeSide as XmlElement;
                                    string value = xChAttributeSide.Attributes.GetNamedItem("value").Value;
                                    if(!string.IsNullOrEmpty(value))
                                    {
                                        AttributeSide.Add(value);
                                    }
                                }
                            }
                        }
                    }
                    if (chn.SelectSingleNode("IndexSequenceSimulazioneNC") != null)
                    {
                        iIndexSequenceSimulazioneNC = Convert.ToInt32(chn.SelectSingleNode("IndexSequenceSimulazioneNC").Attributes.GetNamedItem("value").Value);
                    }

                    // legge gli eventuali parametri tecnologici
                    if (clTecnoPathInfo != null)
                        clTecnoPathInfo.ReadFileXml(n);

                    // finito
                    break;
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("TecnoSideInfo.ReadFileXml Error.", ex);
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     Scrittura informazioni accessorie previste da classi derivate
        /// </summary>
        /// <param name="w">
        ///     oggetto per scrivere il file XML
        /// </param>
        ///**************************************************************************
        public virtual void WriteDerivedFileXml(XmlTextWriter w)
        {

            w.WriteStartElement("TecnoSideInfo");

            w.WriteStartElement("FirstVertexDiskUserInterruptOffset");
            w.WriteAttributeString("value", FirstVertexDiskUserInterruptOffset.ToString());
            w.WriteEndElement();

            w.WriteStartElement("SecondVertexDiskUserInterruptOffset");
            w.WriteAttributeString("value", dfSecondVertexDiskUserInterruptOffset.ToString());
            w.WriteEndElement();

            w.WriteStartElement("FirstVertexDiskMode");
            w.WriteAttributeString("value", GetDiskModeFromEnum(enFirstVertexDiskMode));
            w.WriteEndElement();

            w.WriteStartElement("SecondVertexDiskMode");
            w.WriteAttributeString("value", GetDiskModeFromEnum(enSecondVertexDiskMode));
            w.WriteEndElement();

            w.WriteStartElement("Slope");
            w.WriteAttributeString("value", Slope.ToString());
            w.WriteEndElement();

            w.WriteStartElement("FirstVertexInterruptOffset");
            w.WriteAttributeString("value", FirstVertexInterruptOffset.ToString());
            w.WriteEndElement();

            w.WriteStartElement("SecondVertexInterruptOffset");
            w.WriteAttributeString("value", dfSecondVertexInterruptOffset.ToString());
            w.WriteEndElement();

            w.WriteStartElement("EnabledCAMS");
            w.WriteAttributeString("value", EnabledCAM.ToString());
            w.WriteEndElement();

            w.WriteStartElement("DefaultEnabledCAMS");
            w.WriteAttributeString("value", DefaultEnabledCAM.ToString());
            w.WriteEndElement();

            w.WriteStartElement("UsedCAMS");
            w.WriteAttributeString("value", UsedCAM.ToString());
            w.WriteEndElement();

            if (enSideLeadIn == EN_SIDE_LEADIN.EN_SIDE_LEADIN_PIERCING_DEFINED)
            {
                w.WriteStartElement("SideLeadIn");
                w.WriteAttributeString("value", "EN_SIDE_LEADIN_PIERCING_DEFINED");
                w.WriteEndElement();

                w.WriteStartElement("PiercingPoint");
                w.WriteAttributeString("X", cPiercingPoint.X.ToString(CultureInfo.InvariantCulture));
                w.WriteAttributeString("Y", cPiercingPoint.Y.ToString(CultureInfo.InvariantCulture));
                w.WriteEndElement();
            }

            w.WriteStartElement("ConsiderForCollision");
            w.WriteAttributeString("value", (ConsiderForCollision ? "1" : "0"));
            w.WriteEndElement();

            w.WriteStartElement("SideDirection");
            w.WriteAttributeString("value", Convert.ToInt32(enSideDirection).ToString());
            w.WriteEndElement();

            w.WriteStartElement("Enabled");
            w.WriteAttributeString("value", (IsEnabled ? "1" : "0").ToString());
            w.WriteEndElement();

            w.WriteStartElement("ExcludeBore");
            w.WriteAttributeString("value", (ExcludeBore ? "1" : "0").ToString());
            w.WriteEndElement();

            w.WriteStartElement("ExcludeDisk");
            w.WriteAttributeString("value", (ExcludeDisk ? "1" : "0").ToString());
            w.WriteEndElement();

            w.WriteStartElement("ExcludeWJ");
            w.WriteAttributeString("value", (ExcludeWJ ? "1" : "0").ToString());
            w.WriteEndElement();

            w.WriteStartElement("CutQuality");
            w.WriteAttributeString("value", (CutQuality).ToString());
            w.WriteEndElement();

            w.WriteStartElement("EnableBevel");
            w.WriteAttributeString("value", (Bevel ? "1" : "0").ToString());
            w.WriteEndElement();

            if (iIndexSequenceSimulazioneNC != -1)
            {
                w.WriteStartElement("IndexSequenceSimulazioneNC");
                w.WriteAttributeString("value", iIndexSequenceSimulazioneNC.ToString());
                w.WriteEndElement();
            }

            if (clTecnoPathInfo != null)
                clTecnoPathInfo.WriteFileXml(w);

            w.WriteEndElement();

        }

        ///**************************************************************************
        /// <summary>
        ///     Scrittura informazioni accessorie previste da classi derivate
        /// </summary>
        /// <param name="baseNode">
        ///     nodo padre dove aggiungere la parte di XML necessaria
        /// </param>
        ///**************************************************************************
        public virtual void WriteDerivedFileXml(XmlNode baseNode)
        {

            XmlDocument doc = baseNode.OwnerDocument;

            XmlNode attributes = doc.CreateNode(XmlNodeType.Element, "TecnoSideInfo", ""); ;

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "FirstVertexDiskUserInterruptOffset", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = FirstVertexDiskUserInterruptOffset.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SecondVertexDiskUserInterruptOffset", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfSecondVertexDiskUserInterruptOffset.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "FirstVertexDiskMode", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = GetDiskModeFromEnum(enFirstVertexDiskMode);
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SecondVertexDiskMode", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = GetDiskModeFromEnum(enSecondVertexDiskMode);
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "Slope", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = Slope.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "FirstVertexInterruptOffset", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = FirstVertexInterruptOffset.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SecondVertexInterruptOffset", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = SecondVertexInterruptOffset.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "EnabledCAMS", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = EnabledCAM.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "UsedCAMS", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = UsedCAM.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            if (enSideLeadIn == EN_SIDE_LEADIN.EN_SIDE_LEADIN_PIERCING_DEFINED)
            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SideLeadIn", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = "EN_SIDE_LEADIN_PIERCING_DEFINED";
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
                XmlNode xpn = doc.CreateNode(XmlNodeType.Element, "PiercingPoint", ""); ;
                attr = doc.CreateAttribute("X");
                attr.Value = cPiercingPoint.X.ToString(CultureInfo.InvariantCulture);
                xpn.Attributes.Append(attr);
                attr = doc.CreateAttribute("Y");
                attr.Value = cPiercingPoint.Y.ToString(CultureInfo.InvariantCulture);
                xpn.Attributes.Append(attr);
                attributeValue.AppendChild(xpn);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ConsiderForCollision", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (ConsiderForCollision ? "1" : "0").ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SideDirection", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = Convert.ToInt32(enSideDirection).ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "Enabled", "");
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (IsEnabled ? "1" : "0").ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ExcludeBore", "");
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (ExcludeBore ? "1" : "0").ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ExcludeDisk", "");
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (ExcludeDisk ? "1" : "0").ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ExcludeWJ", "");
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (ExcludeWJ ? "1" : "0").ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "CutQuality", "");
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = CutQuality.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "EnableBevel", "");
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (Bevel ? "1" : "0").ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            if(AttributeSide != null && AttributeSide.Count > 0)
            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "AttributeSide", "");
                XmlAttribute attr = doc.CreateAttribute("count");
                attr.Value = (AttributeSide.Count).ToString();
                attributeValue.Attributes.Append(attr);
                foreach (var attributeSide in AttributeSide)
                {
                    XmlNode attributeSideValue = doc.CreateNode(XmlNodeType.Element, "AttributeSideItem", "");
                    XmlAttribute attrSideValue = doc.CreateAttribute("value");
                    attrSideValue.Value = TraceLog.ConvertToVisual(attributeSide);
                    attributeSideValue.Attributes.Append(attrSideValue);
                    attributeValue.AppendChild(attributeSideValue);
                }
                attributes.AppendChild(attributeValue);
            }

            if (iIndexSequenceSimulazioneNC != -1)
            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "IndexSequenceSimulazioneNC", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = iIndexSequenceSimulazioneNC.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            baseNode.AppendChild(attributes);

            if (clTecnoPathInfo != null)
                clTecnoPathInfo.WriteFileXml(baseNode);
        }

        #endregion

        #region Private Methods

        private EN_VERTEX_DISK_MODE GetDiskModeFromString(string szDiskMode)
        {
            if (szDiskMode == EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_COMPLETE.ToString())
                return EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_COMPLETE;

            if (szDiskMode == EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION.ToString())
                return EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;

            if (szDiskMode == EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_DEFAULT_INTERRUPT.ToString())
                return EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_DEFAULT_INTERRUPT;

            if (szDiskMode == EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_USER_INTERRUPT.ToString())
                return EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_USER_INTERRUPT;

            if (szDiskMode == EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_BOUNDARY_EXTENSION.ToString())
                return EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_BOUNDARY_EXTENSION;

            return EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_COMPLETE;
        }

        private string GetDiskModeFromEnum(EN_VERTEX_DISK_MODE enDiskMode)
        {
            return enDiskMode.ToString();
        }

		#endregion
    }
}
