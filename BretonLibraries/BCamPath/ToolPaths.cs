﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;
using Breton.Polygons;

namespace Breton.BCamPath
{
    public class ToolPathSideLink
    {
        public Side clToolPathSide;
        public ToolInfo clToolInfo;

        public ToolPathSideLink( Side tps, ToolInfo tool )
        {
            clToolPathSide = tps;
            clToolInfo = tool;
        }
             
        public ToolPathSideLink() :
            this( null, null )
        {
        }

        public ToolPathSideLink( ToolPathSideLink tpsl )
        {
            clToolInfo = tpsl.clToolInfo;
            clToolPathSide = tpsl.clToolPathSide;
        }
    }

    public class ToolPathSideLinks : List<ToolPathSideLink>
    {
    }

    public class ToolPaths : PathCollection
    {
        #region Variables
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso vuoto
        /// </summary>
        ///*********************************************************************
        public ToolPaths()
            : base()
        {
            szOriginalType = "ToolPaths";
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public ToolPaths(ToolPaths p)
            : base((PathCollection)p)
        {
            szOriginalType = "ToolPaths";
        }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il percorso come copia di un altro percorso
        /// </summary>
        /// <param name="p">percorso da copiare</param>
        ///*********************************************************************
        public ToolPaths(PathCollection p)
            : base(p)
        {
            szOriginalType = "ToolPaths";
        }

        //Distruttore
        ~ToolPaths()
        {
        }

        #endregion

        #region Properties

        ///**************************************************************************
        /// <summary>
        /// Indicizzazione della lista interna
        /// </summary>
        ///**************************************************************************
        public new ToolPath this[int i]
        {
            get { return (ToolPath)base[i]; }
        }

        #endregion

        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     Aggiunge una lista di percorsi
        /// </summary>
        /// <param name="source">
        ///     riferimento alla lista di percorsi da aggiungere
        /// </param>
        ///**************************************************************************
        public void AddPaths(ToolPaths source)
        {
            for (int i = 0; i < source.Count; i++)
                AddPath(source[i]);
        }

        ///**************************************************************************
        /// <summary>
        ///     Ritorna una lista di entita' di percorsi utensili, collegate all'entita
        ///     della geometria originale indicata
        /// </summary>
        /// <param name="tps">
        ///     geometria originale di riferimento
        /// </param>
        /// <param name="tpe">
        ///     elemento della geometria originale per il quale si cercano gli elementi
        ///     di percorsi utensili generati
        /// </param>
        /// <param name="tool">
        ///     se diverso da null, indica lo specifico utensile per cui eseguire la ricerca
        /// </param>
        /// <returns>
        ///     elenco delle entita' di percorsi utensile trovate.
        /// </returns>
        ///**************************************************************************
        public ToolPathSideLinks GetToolPathSidesFromTecnoPathSide(TecnoPath tps, Side tpe, ToolInfo tool)
        {
            ToolPathSideLinks tpsls = new ToolPathSideLinks();

            // ricerca dei percorsi collegati al percorso in analisi
            for (int i = 0; i < Count; i++)
            {
                if (this[i].LinkToTecnoPath == tps)
                {
                    ToolPath tp = this[i];
                    if (tool != null && tool != tp.Info.Tool)
                        continue;

                    // ricerca i lati collegati
                    for (int j = 0; j < tp.Count; j++)
                    {
                        if (((IToolPathSide)tp[j]).Info.OriginalSide == tpe)
                        {
                            tpsls.Add(new ToolPathSideLink((Side)tp[j], tp.Info.Tool));
                        }
                    }
                }
            }

            return tpsls;
        }

        #endregion

        #region Private Methods
        #endregion

        #region Protected Methods

        ///**************************************************************************
        /// <summary>
        /// CreatePath: create a new path object
        /// </summary>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override Path CreatePath()
        {
            return (Path)new ToolPath();
        }

        ///**************************************************************************
        /// <summary>
        /// CreatePath: create a new path object
        /// </summary>
        /// <param name="p">source path to copy from</param>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override Path CreatePath(Path p)
        {
            if (p is ToolPath)
                return (Path)new ToolPath((ToolPath)p);
            return (Path)new ToolPath(p);
        }

        ///**************************************************************************
        /// <summary>
        /// CreatePathCollection: create a new paths collection
        /// </summary>
        /// <remarks>created path</remarks> 
        //**************************************************************************
        protected override PathCollection CreatePathCollection()
        {
            return (PathCollection)new ToolPaths();
        }

        #endregion
    }
}
