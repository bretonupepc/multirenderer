﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public class ToolPathInfo
    {
        #region Variables
        private ToolInfo clToolInfo;
        private bool bNeedLeadIn;
        private double dfDepth;
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Constructor
        /// </summary>
        ///*********************************************************************
        public ToolPathInfo()
        {
            clToolInfo = new ToolInfo();
            bNeedLeadIn = true;
            dfDepth = double.MaxValue;
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor 
        /// </summary>
        /// <param name="tpi">oggetto da copiare</param>
        ///*********************************************************************
        public ToolPathInfo(ToolPathInfo tpi)
        {
            clToolInfo = new ToolInfo(tpi.Tool);
            bNeedLeadIn = tpi.bNeedLeadIn;
            dfDepth = tpi.dfDepth;
        }

        //Distruttore
        ~ToolPathInfo()
        {
        }

        #endregion

        #region Properties
        public ToolInfo Tool
        {
            get { return clToolInfo; }
            set { clToolInfo = value; }
        }

        public bool NeedLeadIn
        {
            get { return bNeedLeadIn; }
            set { bNeedLeadIn = value; }
        }

        public double Depth
        {
            get { return dfDepth; }
            set { dfDepth = value; }
        }
        #endregion

        #region Public Methods
        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public virtual bool ReadFileXml(XmlNode n)
        {
            if (clToolInfo != null)
                return clToolInfo.ReadFileXml(n.SelectSingleNode("Tool"));
            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="w">strem to write to</param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlTextWriter w)
        {
            if (clToolInfo != null)
                clToolInfo.WriteFileXml(w);
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="baseNode">base node of XML</param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlNode baseNode)
        {
            if (clToolInfo != null)
                clToolInfo.WriteFileXml(baseNode);
        }

        //**************************************************************************
        /// <summary>
        ///     HasSpecificDepth: test if this tool path has a specific depth
        ///                       i.e. this value does not derive from the
        ///                       parent TecnoPath
        /// </summary>
        /// <returns>
        ///     true if this tool path has a specific depth
        ///     false otherwise
        /// </returns>
        //**************************************************************************
        public bool HasSpecificDepth()
        {
            if (dfDepth == double.MaxValue)
                return false;
            return true;
        }

        #endregion

        #region Private Methods
        #endregion
    }
}
