﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;
using Breton.Polygons;
using TraceLoggers;

namespace Breton.BCamPath
{
    public enum EN_TOOLPATH_SIDE_TYPE
    {
        EN_TOOLPATH_SIDE_LEADIN,
        EN_TOOLPATH_SIDE_LEADOUT,
        EN_TOOLPATH_SIDE_MOVE,
        EN_TOOLPATH_SIDE_DRILL,
        EN_TOOLPATH_SIDE_NUM_OF
    }

    public class ToolPathSideInfo
    {
        #region Variables
        private EN_TOOLPATH_SIDE_TYPE enSideType;       // tipo di elemnto nel percorso utensile
        private double dfFeed;                          // velocita' di avanzamento
        private Side clSide;                            // riferimento all'entita' del percorso originale
        private int iSideNumber;                        // indice dell'entita' originale nel percorso utensile
        private double dfStartOffset;                   // offset di inizio nell'entita' originale
        private double dfEndOffset;                     // offset di fine nell'entita' originale
        private int iSequenceIndex;                     // indice nella sequenza di esecuzione
        private double dfSlope;                         // inclinazione del disco (0 = verticale, > 0 verso interno materiale, < 0 esterno materiale
        private bool bCutEnabled;                       // cut enabled
        private bool bStartWithinTrace;                 // indica se il tratto inizia sulla traccia di un utensile precedente
        private bool bEndWithinTrace;                   // indica se il tratto termina sulla traccia di un utensile precedente
        private bool bIsJunction;                       // indica se il tratto e' un raccordo ovvero un completamento del percorso di un altro utensile
        private bool bLeadInExtendingOriginalToolpath;  // indica se il lead in è ottenuto estendendo la prima curva del toolpath
        private bool bLeadOutExtendingOriginalToolpath; // indica se il lead out è ottenuto estendendo la prima curva del toolpath
        private bool bLeadInPath;                       // indica se è un percorso di lead in
        private bool bLeadOutPath;                      // indica se è un percorso di lead out
        private string sSideName;                       // nome side; indicetoolpath + '_' + indice side
        private string sSideNamePrev;                   // nome side precedente; indicetoolpath + '_' + indice side
        private string sSideNameNext;                   // nome side successivo; indicetoolpath + '_' + indice side
        private Point clIncisionPoint;                  // punto di incisione disco (toolpath ha lunghezza != 0 per definire la direzione) //20161213 gestione tasteggio incisione
        #endregion

        #region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Constructor
        /// </summary>
        ///*********************************************************************
        public ToolPathSideInfo()
        {
            enSideType = EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_MOVE;
            dfFeed = 0;
            clSide = null;
            dfStartOffset = 0;
            dfEndOffset = 0;
            iSideNumber = 0;
            iSequenceIndex = -1;
            dfSlope = 0;
            bCutEnabled = true;
            bStartWithinTrace = false;
            bEndWithinTrace = false;
            bIsJunction = false;
            bLeadInExtendingOriginalToolpath = false;
            bLeadOutExtendingOriginalToolpath = false;
            bLeadInPath = false;
            bLeadOutPath = false;
            sSideName = null;
            sSideNamePrev = null;
            sSideNameNext = null;
            clIncisionPoint = null;
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor 
        /// </summary>
        /// <param name="tsi">source object to copy from</param>
        ///*********************************************************************
        public ToolPathSideInfo(ToolPathSideInfo tsi)
        {
            enSideType = tsi.enSideType;
            dfFeed = tsi.dfFeed;
            clSide = tsi.clSide;
            iSideNumber = tsi.iSideNumber;
            dfStartOffset = tsi.dfStartOffset;
            dfEndOffset = tsi.dfEndOffset;
            iSideNumber = tsi.iSideNumber;
            iSequenceIndex = tsi.iSequenceIndex;
            dfSlope = tsi.dfSlope;
            bCutEnabled = tsi.bCutEnabled;
            bStartWithinTrace = tsi.bStartWithinTrace;
            bEndWithinTrace = tsi.bEndWithinTrace;
            bIsJunction = tsi.IsJunction;
            bLeadInExtendingOriginalToolpath = tsi.bLeadInExtendingOriginalToolpath;
            bLeadOutExtendingOriginalToolpath = tsi.bLeadOutExtendingOriginalToolpath;
            bLeadInPath = tsi.bLeadInPath;
            bLeadOutPath = tsi.bLeadOutPath;
            sSideName = tsi.sSideName;
            sSideNamePrev = tsi.sSideNamePrev;
            sSideNameNext = tsi.sSideNameNext;
            if (tsi.clIncisionPoint != null)
                clIncisionPoint = new Point(tsi.clIncisionPoint);
            else
                clIncisionPoint = null;
        }

        #endregion

        #region Properties
        public EN_TOOLPATH_SIDE_TYPE SideType
        {
            get { return enSideType; }
            set { enSideType = value; }
        }

        public double Feed
        {
            get { return dfFeed; }
            set { dfFeed = value; }
        }

        public Side OriginalSide
        {
            get { return clSide; }
            set { clSide = value; }
        }

        public int SideNumber
        {
            get { return iSideNumber; }
            set { iSideNumber = value; }
        }

        public double StartOffset
        {
            get { return dfStartOffset; }
            set { dfStartOffset = value; }
        }

        public double EndOffset
        {
            get { return dfEndOffset; }
            set { dfEndOffset = value; }
        }

        public int SequenceIndex
        {
            get { return iSequenceIndex; }
            set { iSequenceIndex = value; }
        }

        public TecnoSideInfo SideInfo
        {
            get
            {
                if (clSide == null)
                    return null;
                return ((ITecnoSide)clSide).Info;
            }
        }

        public double Slope
        {
            get { return dfSlope; }
            set { dfSlope = value; }
        }

        public bool CutEnabled
        {
            get { return bCutEnabled; }
            set { bCutEnabled = value; }
        }

        public bool StartWithinTrace
        {
            get { return bStartWithinTrace; }
            set { bStartWithinTrace = value; }
        }

        public bool EndWithinTrace
        {
            get { return bEndWithinTrace; }
            set { bEndWithinTrace = value; }
        }

        public bool IsJunction
        {
            get { return bIsJunction; }
            set { bIsJunction = value; }
        }

        public bool LeadInExtendingOriginalToolpath
        {
            get { return bLeadInExtendingOriginalToolpath; }
            set { bLeadInExtendingOriginalToolpath = value; }
        }

        public bool LeadOutExtendingOriginalToolpath
        {
            get { return bLeadOutExtendingOriginalToolpath; }
            set { bLeadOutExtendingOriginalToolpath = value; }
        }

        public bool LeadInPath
        {
            get { return bLeadInPath; }
            set { bLeadInPath = value; }
        }

        public bool LeadOutPath
        {
            get { return bLeadOutPath; }
            set { bLeadOutPath = value; }
        }

        public string SideName
        {
            get { return sSideName; }
            set { sSideName = value; }
        }

        public string SideNamePrev
        {
            get { return sSideNamePrev; }
            set { sSideNamePrev = value; }
        }

        public string SideNameNext
        {
            get { return sSideNameNext; }
            set { sSideNameNext = value; }
        }

        public Point IncisionPoint
        {
            get { return clIncisionPoint; }
            set { clIncisionPoint = value; }
        }

        public bool LeadOnTraceCenter { get { return StartWithinTrace || EndWithinTrace; } }
        #endregion

        #region Public Methods

        //**************************************************************************
        // ReadDerivedFileXml
        // lettura di parametri accessori previsti da classi derivate.
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public virtual bool ReadDerivedFileXml(XmlNode n)
        {
            try
            {
                XmlNodeList l = n.ChildNodes;

                // Scorre tutti i sottonodi
                foreach (XmlNode chn in l)
                {
                    // Cerca il nodo TecnoPathInfo
                    if (chn.Name != "ToolPathSideInfo")
                        continue;

                    // legge i parametri noti
                    dfFeed = Convert.ToDouble(chn.SelectSingleNode("Feed").Attributes.GetNamedItem("value").Value);
                    enSideType = GetSideModeFromString(chn.SelectSingleNode("SideType").Attributes.GetNamedItem("value").Value);
                    iSideNumber = Convert.ToInt32(chn.SelectSingleNode("OriginalSideNumber").Attributes.GetNamedItem("value").Value);
                    dfStartOffset = Convert.ToDouble(chn.SelectSingleNode("StartOffset").Attributes.GetNamedItem("value").Value);
                    dfEndOffset = Convert.ToDouble(chn.SelectSingleNode("EndOffset").Attributes.GetNamedItem("value").Value);
                    dfSlope = Convert.ToDouble(chn.SelectSingleNode("Slope").Attributes.GetNamedItem("value").Value);
					if (chn.SelectSingleNode("StartWithinTrace") != null)
						if (chn.SelectSingleNode("StartWithinTrace").Attributes.GetNamedItem("value").Value != null)
							bStartWithinTrace = (chn.SelectSingleNode("StartWithinTrace").Attributes.GetNamedItem("value").Value == "0" ? false : true);
					if (chn.SelectSingleNode("EndWithinTrace") != null)
						if (chn.SelectSingleNode("EndWithinTrace").Attributes.GetNamedItem("value").Value != null)
							bEndWithinTrace = (chn.SelectSingleNode("EndWithinTrace").Attributes.GetNamedItem("value").Value == "0" ? false : true);
                    var xIncisionPointX = chn.SelectSingleNode("IncisionPointCoordX");
                    var xIncisionPointY = chn.SelectSingleNode("IncisionPointCoordY");
                    if (xIncisionPointX != null && xIncisionPointY != null)
                    {
                        var p = new Breton.Polygons.Point(
                            Convert.ToDouble(xIncisionPointX.Attributes.GetNamedItem("value").Value), 
                            Convert.ToDouble(xIncisionPointY.Attributes.GetNamedItem("value").Value));
                        IncisionPoint = p;
                    }

                    // finito
                    break;
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ToolPathSideInfo.ReadFileXml Error.", ex);
                return false;
            }
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public virtual void WriteDerivedFileXml(XmlTextWriter w)
        {

            w.WriteStartElement("ToolPathSideInfo");

            w.WriteStartElement("Feed");
            w.WriteAttributeString("value", dfFeed.ToString());
            w.WriteEndElement();

            w.WriteStartElement("SideType");
            w.WriteAttributeString("value", GetSideModeFromEnum(enSideType));
            w.WriteEndElement();

            w.WriteStartElement("OriginalSideNumber");
            w.WriteAttributeString("value", iSideNumber.ToString());
            w.WriteEndElement();

            w.WriteStartElement("StartOffset");
            w.WriteAttributeString("value", dfStartOffset.ToString());
            w.WriteEndElement();

            w.WriteStartElement("EndOffset");
            w.WriteAttributeString("value", dfEndOffset.ToString());
            w.WriteEndElement();

            w.WriteStartElement("SequenceIndex");
            w.WriteAttributeString("value", iSequenceIndex.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Slope");
            w.WriteAttributeString("value", dfSlope.ToString());
            w.WriteEndElement();

			w.WriteStartElement("StartWithinTrace");
			w.WriteAttributeString("value", (bStartWithinTrace ? "1" : "0"));
			w.WriteEndElement();

			w.WriteStartElement("EndWithinTrace");
			w.WriteAttributeString("value", (bEndWithinTrace ? "1" : "0"));
			w.WriteEndElement();

			w.WriteEndElement();
        }

        //**************************************************************************
        // WriteDerivedFileXml
        // Scrittura informazioni accessorie previste da classi derivate
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public virtual void WriteDerivedFileXml(XmlNode baseNode)
        {

            XmlDocument doc = baseNode.OwnerDocument;

            XmlNode attributes = doc.CreateNode(XmlNodeType.Element, "ToolPathSideInfo", ""); ;

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "Feed", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfFeed.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SideType", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = GetSideModeFromEnum(enSideType);
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "OriginalSideNumber", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = iSideNumber.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "StartOffset", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfStartOffset.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "EndOffset", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfEndOffset.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SequenceIndex", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = iSequenceIndex.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "Slope", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfSlope.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

			{
				XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "StartWithinTrace", ""); ;
				XmlAttribute attr = doc.CreateAttribute("value");
				attr.Value = (bStartWithinTrace ? "1" : "0");
				attributeValue.Attributes.Append(attr);
				attributes.AppendChild(attributeValue);
			}

			{
				XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "EndWithinTrace", ""); ;
				XmlAttribute attr = doc.CreateAttribute("value");
				attr.Value = (bEndWithinTrace ? "1" : "0");
				attributeValue.Attributes.Append(attr);
				attributes.AppendChild(attributeValue);
			}

            var incisionPoint = IncisionPoint;
            if (incisionPoint != null)
            {
                {
                    XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "IncisionPointCoordX", ""); ;
                    XmlAttribute attr = doc.CreateAttribute("value");
                    attr.Value = incisionPoint.X.ToString();
                    attributeValue.Attributes.Append(attr);
                    attributes.AppendChild(attributeValue);
                }
                {
                    XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "IncisionPointCoordY", ""); ;
                    XmlAttribute attr = doc.CreateAttribute("value");
                    attr.Value = incisionPoint.Y.ToString();
                    attributeValue.Attributes.Append(attr);
                    attributes.AppendChild(attributeValue);
                }
            }

			baseNode.AppendChild(attributes);
        }

        #endregion

        #region Private Methods

        private EN_TOOLPATH_SIDE_TYPE GetSideModeFromString(string szSideMode)
        {
            if (szSideMode == EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADIN.ToString())
                return EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADIN;

            if (szSideMode == EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADOUT.ToString())
                return EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_LEADOUT;

            if (szSideMode == EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_MOVE.ToString())
                return EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_MOVE;

            return EN_TOOLPATH_SIDE_TYPE.EN_TOOLPATH_SIDE_MOVE;
        }

        private string GetSideModeFromEnum(EN_TOOLPATH_SIDE_TYPE enSideMode)
        {
            return enSideMode.ToString();
        }

        #endregion
    }
}
