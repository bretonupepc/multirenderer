﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;

namespace Breton.BCamPath
{
    public enum EN_TOOL_TYPE
    {
        EN_TOOL_TYPE_DISK,              // disco
        EN_TOOL_TYPE_ROUTER,            // fresa da taglio
        EN_TOOL_TYPE_WATER,             // waterjet
        EN_TOOL_TYPE_DRILL,             // foratura/carotatura
        EN_TOOL_TYPE_CORE_DRILL,        // foro rubinetto
        EN_TOOL_TYPE_VDISK,             // utensile/disco per bisellatura

        EN_TOOL_TYPE_UNKNOW
    }

    public enum EN_TOOL_STATE
    {
        DISABLED,                       // utensile non utilizzabile nei calcoli CAM
        MANUAL,                         // utensile utilizzabile solo nei percorsi utensile espliciti
                                        // creati manualmente o imposti (es. forature).
        AUTOMATIC                       // utensile utilizzabile sia in modo manuale che automatico
    }

    public class ToolInfo
    {
        #region Variables
        private int iDbId = 0;
        private int iId = 0;
        private int iNumber = 0;
        private int iPriority = -1;
        private double dfLength = 0;
        private double dfRadius = 0;
        private int iCorrector = 0;
        private string szName = "";
        private EN_TOOL_TYPE enType = EN_TOOL_TYPE.EN_TOOL_TYPE_UNKNOW;
        private double dfReduction = 0;
        private double dfAdvance = 0;
        private double dfStep = 0;
        private EN_TOOL_STATE enState = EN_TOOL_STATE.AUTOMATIC;
        private double dfMinToolMoveLength = 0;
        private double dfMinDiskSlope = 0;
        private double dfMaxDiskSlope = MathUtil.gdRadianti(80);
        private double dfOffsetDiskSlope = 0;
        private int iNumHoles = 0;
        private double dfHolesDiameterError = 0.1;
        #endregion

        #region Constructors
        ///*********************************************************************
        /// <summary>
        /// Default Constructor
        /// </summary>
        ///*********************************************************************
        public ToolInfo()
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor 
        /// </summary>
        /// <param name="ti">source object to copy from</param>
        ///*********************************************************************
        public ToolInfo(ToolInfo ti)
        {
            iDbId = ti.iDbId;
            iId = ti.iId;
            iNumber = ti.iNumber;
            iPriority = ti.iPriority;
            dfLength = ti.dfLength;
            dfRadius = ti.dfRadius;
            iCorrector = ti.iCorrector;
            szName = ti.szName;
            enType = ti.enType;
            dfReduction = ti.dfReduction;
            dfAdvance = ti.dfAdvance;
            dfStep = ti.dfStep;
            enState = ti.enState;
            dfMinToolMoveLength = ti.dfMinToolMoveLength;
            dfMinDiskSlope = ti.dfMinDiskSlope;
            dfMaxDiskSlope = ti.dfMaxDiskSlope;
            dfOffsetDiskSlope = ti.dfOffsetDiskSlope;
            iNumHoles = ti.iNumHoles;
            dfHolesDiameterError = ti.dfHolesDiameterError;
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 1
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        ///*********************************************************************
        public ToolInfo(int id, int num, double len, double radius, int corr, string name, EN_TOOL_TYPE tt) :
            this(id, num, len, radius, corr, name, tt, 0, 0, 0, -1)
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 2
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        ///*********************************************************************
        public ToolInfo(int id, int num, double len, double radius, int corr, string name, EN_TOOL_TYPE tt, double reduction) :
            this(id, num, len, radius, corr, name, tt, 0, 0, 0, -1)
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 3
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        /// <param name="advance">tool's advance (for standard tools only)</param>
        ///*********************************************************************
        public ToolInfo(int id, int num, double len, double radius, int corr, string name, EN_TOOL_TYPE tt, double reduction, double advance) :
            this(id, num, len, radius, corr, name, tt, 0, 0, 0, -1)
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 4
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        /// <param name="advance">tool's advance (for standard tools only)</param>
        /// <param name="step">tool's step (for drill tools only)</param>
        ///*********************************************************************
        public ToolInfo(int id, int num, double len, double radius, int corr, string name, EN_TOOL_TYPE tt, double reduction, double advance, double step) :
           this(id, num, len, radius, corr, name, tt, 0, 0, 0, -1)
        {
            iId = id;
            iNumber = num;
            dfLength = len;
            dfRadius = radius;
            iCorrector = corr;
            szName = name;
            enType = tt;
            dfReduction = reduction;
            dfAdvance = advance;
            dfStep = step;
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 5
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        /// <param name="advance">tool's advance (for standard tools only)</param>
        /// <param name="step">tool's step (for drill tools only)</param>
        /// <param name="priority">tool's use priority</param>
        ///*********************************************************************
        public ToolInfo(int id, int num, double len, double radius, int corr, string name, EN_TOOL_TYPE tt, double reduction, double advance, double step, int priority)
            : this(id, num, len, radius, corr, name, tt, 0, 0, 0, -1,EN_TOOL_STATE.AUTOMATIC)
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 6
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        /// <param name="advance">tool's advance (for standard tools only)</param>
        /// <param name="step">tool's step (for drill tools only)</param>
        /// <param name="priority">tool's use priority</param>
        /// <param name="state">tool's usability state</param>
        ///*********************************************************************
        public ToolInfo(int id, int num, double len, double radius, int corr, string name, EN_TOOL_TYPE tt, double reduction, double advance, double step, int priority, EN_TOOL_STATE state)
            : this(id, num, len, radius, corr, name, tt, 0, 0, 0, -1, EN_TOOL_STATE.AUTOMATIC, 0)
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 7
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        /// <param name="advance">tool's advance (for standard tools only)</param>
        /// <param name="step">tool's step (for drill tools only)</param>
        /// <param name="priority">tool's use priority</param>
        /// <param name="state">tool's usability state</param>
        /// <param name="mintoolmovelen">tool's min movement in toolpaths</param>
        ///*********************************************************************
        public ToolInfo(int id, int num, double len, double radius, int corr, string name, EN_TOOL_TYPE tt, double reduction, double advance, double step, int priority, EN_TOOL_STATE state, double mintoolmovelen)
            : this(id, num, len, radius, corr, name, tt, 0, 0, 0, -1, EN_TOOL_STATE.AUTOMATIC, 0, 0, MathUtil.gdRadianti(80), 0)
        {
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 8
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        /// <param name="advance">tool's advance (for standard tools only)</param>
        /// <param name="step">tool's step (for drill tools only)</param>
        /// <param name="priority">tool's use priority</param>
        /// <param name="state">tool's usability state</param>
        /// <param name="mintoolmovelen">tool's min movement in toolpaths</param>
        /// <param name="mindiskslope">tool's min acceptable slope (for disk only)</param>
        /// <param name="maxdiskslope">tool's max acceptable slope (for disk only)</param>
        /// <param name="offsetdiskslope">tool's tool offset slope (for disk only on concave arcs)</param>
        ///*********************************************************************
        public ToolInfo( int id, 
                         int num, 
                         double len, 
                         double radius, 
                         int corr, 
                         string name, 
                         EN_TOOL_TYPE tt, 
                         double reduction, 
                         double advance, 
                         double step, 
                         int priority, 
                         EN_TOOL_STATE state, 
                         double mintoolmovelen,
                         double mindiskslope,
                         double maxdiskslope,
                         double offsetdiskslope)
        {
            iId = id;
            iNumber = num;
            dfLength = len;
            dfRadius = radius;
            iCorrector = corr;
            szName = name;
            enType = tt;
            dfReduction = reduction;
            dfAdvance = advance;
            dfStep = step;
            iPriority = priority;
            enState = state;
            dfMinToolMoveLength = mintoolmovelen;
            dfMinDiskSlope = mindiskslope;
            dfMaxDiskSlope = maxdiskslope;
            dfOffsetDiskSlope = offsetdiskslope;
            iNumHoles = 0;
        }

        ///*********************************************************************
        /// <summary>
        /// Optional Constructor 9
        /// </summary>
        /// <param name="id">tool's id</param>
        /// <param name="num">tool's number</param>
        /// <param name="len">tool's length</param>
        /// <param name="radius">tool's radius</param>
        /// <param name="corr">tool's corrector</param>
        /// <param name="name">tool's name</param>
        /// <param name="tt">tool's type</param>
        /// <param name="reduction">tool's extension reduction (for disks only)</param>
        /// <param name="advance">tool's advance (for standard tools only)</param>
        /// <param name="step">tool's step (for drill tools only)</param>
        /// <param name="priority">tool's use priority</param>
        /// <param name="state">tool's usability state</param>
        /// <param name="mintoolmovelen">tool's min movement in toolpaths</param>
        /// <param name="mindiskslope">tool's min acceptable slope (for disk only)</param>
        /// <param name="maxdiskslope">tool's max acceptable slope (for disk only)</param>
        /// <param name="offsetdiskslope">tool's tool offset slope (for disk only on concave arcs)</param>
        /// <param name="numholes">number of consecutive holes to be produced by this tool</param>
        ///*********************************************************************
        public ToolInfo(int id,
                         int num,
                         double len,
                         double radius,
                         int corr,
                         string name,
                         EN_TOOL_TYPE tt,
                         double reduction,
                         double advance,
                         double step,
                         int priority,
                         EN_TOOL_STATE state,
                         double mintoolmovelen,
                         double mindiskslope,
                         double maxdiskslope,
                         double offsetdiskslope,
                         int numholes)
        {
            iId = id;
            iNumber = num;
            dfLength = len;
            dfRadius = radius;
            iCorrector = corr;
            szName = name;
            enType = tt;
            dfReduction = reduction;
            dfAdvance = advance;
            dfStep = step;
            iPriority = priority;
            enState = state;
            dfMinToolMoveLength = mintoolmovelen;
            dfMinDiskSlope = mindiskslope;
            dfMaxDiskSlope = maxdiskslope;
            dfOffsetDiskSlope = offsetdiskslope;
            iNumHoles = numholes;
        }
        #endregion

        #region Properties
        public int DbId
        {
            get { return iDbId; }
            set { iDbId = value; }
        }

        public int Id
        {
            get { return iId; }
            set { iId = value; }
        }

        public int Number
        {
            get { return iNumber; }
            set { iNumber = value; }
        }

        public int Priority
        {
            get { return iPriority; }
            set { iPriority = value; }
        }

        public double Length
        {
            get { return dfLength; }
            set { dfLength = value; }
        }
        public double Radius
        {
            get { return dfRadius; }
            set { dfRadius = value; }
        }

        public int Corrector
        {
            get { return iCorrector; }
            set { iCorrector = value; }
        }

        public string Name
        {
            get { return szName; }
            set { szName = value; }
        }

        public EN_TOOL_TYPE Type
        {
            get { return enType; }
            set { enType = value; }
        }

        public double Reduction
        {
            get { return dfReduction; }
            set { dfReduction = value; }
        }

        public double Advance
        {
            get { return dfAdvance; }
            set { dfAdvance = value; }
        }

        public double Step
        {
            get { return dfStep; }
            set { dfStep = value; }
        }

        public EN_TOOL_STATE State
        {
            get { return enState; }
            set { enState = value; }
        }

        public double MinToolMoveLength
        {
            get { return dfMinToolMoveLength; }
            set { dfMinToolMoveLength = value; }
        }

        public double MinDiskSlope
        {
            get { return dfMinDiskSlope; }
            set { dfMinDiskSlope = value; }
        }

        public double MaxDiskSlope
        {
            get { return dfMaxDiskSlope; }
            set { dfMaxDiskSlope = value; }
        }

        public double OffsetDiskSlope
        {
            get { return dfOffsetDiskSlope; }
            set { dfOffsetDiskSlope = value; }
        }

        public int NumHoles
        {
            get { return iNumHoles; }
            set { iNumHoles = value; }
        }

        public double HolesDiameterError
        {
            get { return dfHolesDiameterError; }
            set { dfHolesDiameterError = value; }
        }

        #endregion

        #region Operators
        public static bool operator == (ToolInfo a, ToolInfo b)
        {
            if (System.Object.ReferenceEquals(a, b))
                return true;

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(ToolInfo tfa, ToolInfo tfb)
        {
            return (!(tfa == tfb));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
                return false;

            ToolInfo p = obj as ToolInfo;
            if ((System.Object)p == null)
                return false;

            // Return true if the fields match:
            return (iNumber == p.iNumber);
        }

        public bool Equals(ToolInfo p)
        {
            if ((object)p == null)
                return false;

            // Return true if the fields match:
            if (iId != p.Id)									return false;
            if (iNumber != p.Number)							return false; 
            if (iPriority != p.Priority)						return false; 
            if (dfLength != p.Length)							return false; 
            if (dfRadius != p.Radius)							return false;
            if (szName != p.Name)								return false;
            if (enType != p.enType)								return false;
            if (dfReduction != p.Reduction)						return false;
            if (dfAdvance != p.Advance)							return false;
            if (dfStep != p.Step)								return false;
            if (enState != p.enState)							return false;
            if (dfMinToolMoveLength != p.MinToolMoveLength)		return false;
            if (dfMinDiskSlope != p.MinDiskSlope)				return false;
            if (dfMaxDiskSlope != p.MaxDiskSlope)				return false;
            if (dfOffsetDiskSlope != p.OffsetDiskSlope)			return false;

            return true;
        }

        #endregion

        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e'di tipo disco
        /// </summary>
        /// <returns>
        ///     true se tipo disco
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsDisk()
        {
            if (Type == EN_TOOL_TYPE.EN_TOOL_TYPE_DISK)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e'di tipo waterjet
        /// </summary>
        /// <returns>
        ///     true se tipo disco
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsWaterJet()
        {
            if (Type == EN_TOOL_TYPE.EN_TOOL_TYPE_WATER)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e'di tipo standard
        /// </summary>
        /// <returns>
        ///     true se tipo standard
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsStandard()
        {
            if (Type == EN_TOOL_TYPE.EN_TOOL_TYPE_ROUTER ||
                Type == EN_TOOL_TYPE.EN_TOOL_TYPE_WATER)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e' una fresa cilindrica
        /// </summary>
        /// <returns>
        ///     true se tipo fresa cilindrica
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsRouter()
        {
            if (Type == EN_TOOL_TYPE.EN_TOOL_TYPE_ROUTER)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e' per forature di stacco
        /// </summary>
        /// <returns>
        ///     true se l'utensile e' per forature
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsDrill()
        {
            if (Type == EN_TOOL_TYPE.EN_TOOL_TYPE_DRILL)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e' per forature interne (fori rubinetti)
        /// </summary>
        /// <returns>
        ///     true se l'utensile e' per forature
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsCoreDrill()
        {
            if (Type == EN_TOOL_TYPE.EN_TOOL_TYPE_CORE_DRILL)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e'di tipo VDisk (disco per bisellatura)
        /// </summary>
        /// <returns>
        ///     true se tipo VDisk
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsVDisk()
        {
            if (Type == EN_TOOL_TYPE.EN_TOOL_TYPE_VDISK)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e' per forature in modo non definito
        /// </summary>
        /// <returns>
        ///     true se l'utensile e' per forature
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsGenericDrill()
        {
            return IsDrill() || IsCoreDrill();
        }

        ///**************************************************************************
        /// <summary>
        ///     Determina se l'utensile e' per forature in modo non definito
        /// </summary>
        /// <returns>
        ///     true se l'utensile e' per forature
        ///     false altrimenti
        /// </returns>
        ///**************************************************************************
        public virtual bool IsGenericStandard()
        {
            return IsRouter() || IsWaterJet();
        }

        ///**************************************************************************
        /// <summary>
        ///     Extend: compute surface semi-imprint for disk-like tools 
        /// </summary>
        /// <param name="depth">
        ///     working's depth
        /// </param>
        /// <returns>
        ///     semi-imprint size
        /// </returns>
        ///**************************************************************************
        public virtual double Extend(double depth)
        {
            return Extend(depth, 0);
        }

        ///**************************************************************************
        /// <summary>
        ///     Extend: compute surface semi-imprint for disk-like tools, using
        ///     the slope (tool's inclination) information
        /// </summary>
        /// <param name="depth">
        ///     working's depth
        /// </param>
        /// <param name="slope">
        ///     tool's inclination
        /// </param>
        /// <returns>
        ///     semi-imprint size
        /// </returns>
        ///**************************************************************************
        public virtual double Extend(double depth, double slope)
        {
            if (Type != EN_TOOL_TYPE.EN_TOOL_TYPE_DISK)
                return 0;

            if (depth <= 0)
                return 0;

            slope = Math.PI / 2 - Math.Abs(slope);
            depth = depth / Math.Sin(slope);

            double r2 = Length * Length;
            double l2 = Length - depth;
            double e = Math.Sqrt(r2 - l2 * l2);

            return e + dfReduction;
        }

        ///**************************************************************************
        /// <summary>
        ///     ReadFileXml: read Tool Info frmm XML stream
        /// </summary> 
        /// <param name="baseNode">base node of XML</param>
        //**************************************************************************
        public virtual bool ReadFileXml(XmlNode n)
        {
            Type = String_To_EN_TOOL_TYPE(n.SelectSingleNode("Type").Attributes.GetNamedItem("value").Value);

            Number = Convert.ToInt32(n.SelectSingleNode("Number").Attributes.GetNamedItem("value").Value);

            Length = Convert.ToDouble(n.SelectSingleNode("Length").Attributes.GetNamedItem("value").Value);

            Radius = Convert.ToDouble(n.SelectSingleNode("Radius").Attributes.GetNamedItem("value").Value);

            Corrector = Convert.ToInt32(n.SelectSingleNode("Corrector").Attributes.GetNamedItem("value").Value);

            Name = n.SelectSingleNode("Name").Attributes.GetNamedItem("value").Value;

            if (n.SelectSingleNode("Id") != null && n.SelectSingleNode("Id").Attributes.GetNamedItem("value") != null)
                Id = Convert.ToInt32(n.SelectSingleNode("Id").Attributes.GetNamedItem("value").Value);

            if (n.SelectSingleNode("Reduction") != null && n.SelectSingleNode("Reduction").Attributes.GetNamedItem("value") != null)
                dfReduction = Convert.ToDouble(n.SelectSingleNode("Reduction").Attributes.GetNamedItem("value").Value);

            if (n.SelectSingleNode("Advance") != null && n.SelectSingleNode("Advance").Attributes.GetNamedItem("value") != null)
                dfAdvance = Convert.ToDouble(n.SelectSingleNode("Advance").Attributes.GetNamedItem("value").Value);

            if (n.SelectSingleNode("Step") != null && n.SelectSingleNode("Step").Attributes.GetNamedItem("value") != null)
                dfStep = Convert.ToDouble(n.SelectSingleNode("Step").Attributes.GetNamedItem("value").Value);

            if (n.SelectSingleNode("Priority") != null && n.SelectSingleNode("Priority").Attributes.GetNamedItem("value") != null)
            {
                iPriority = Convert.ToInt32(n.SelectSingleNode("Priority").Attributes.GetNamedItem("value").Value);
                if (iPriority < 0)
                    iPriority = -1;
            }

            if (n.SelectSingleNode("State") != null && n.SelectSingleNode("State").Attributes.GetNamedItem("value") != null)
                State = String_To_EN_TOOL_STATE( n.SelectSingleNode("State").Attributes.GetNamedItem("value").Value );

            if (n.SelectSingleNode("MinToolMoveLength") != null && n.SelectSingleNode("MinToolMoveLength").Attributes.GetNamedItem("value") != null)
                MinToolMoveLength = Convert.ToDouble(n.SelectSingleNode("MinToolMoveLength").Attributes.GetNamedItem("value").Value);

            if (n.SelectSingleNode("MinDiskSlope") != null && n.SelectSingleNode("MinDiskSlope").Attributes.GetNamedItem("value") != null)
                MinDiskSlope = Convert.ToDouble(n.SelectSingleNode("MinDiskSlope").Attributes.GetNamedItem("value").Value);

            if (n.SelectSingleNode("MaxDiskSlope") != null && n.SelectSingleNode("MaxDiskSlope").Attributes.GetNamedItem("value") != null)
                MaxDiskSlope = Convert.ToDouble(n.SelectSingleNode("MaxDiskSlope").Attributes.GetNamedItem("value").Value);

            if (n.SelectSingleNode("OffsetDiskSlope") != null && n.SelectSingleNode("OffsetDiskSlope").Attributes.GetNamedItem("value") != null)
                OffsetDiskSlope = Convert.ToDouble(n.SelectSingleNode("OffsetDiskSlope").Attributes.GetNamedItem("value").Value);

            //si eseguono controlli con id database; adesso l'informazione sull'utensile attivo è in T_PARAMETERS; per un certo utensile (p.es. rodding o groove), come valore, è l'id della riga di T_TOOLS che rappresenta l'utensile abilitato in fknc
            if(Id == 0)
                Id = Number;
            if(DbId == 0)
                DbId = Number;

            return true;
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="w">strem to write to</param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("Tool");

            w.WriteStartElement("Type");
            w.WriteAttributeString("value", EN_TOOL_TYPE_To_String(Type));
            w.WriteEndElement();

            w.WriteStartElement("State");
            w.WriteAttributeString("value", EN_TOOL_STATE_To_String(State));
            w.WriteEndElement();

            w.WriteStartElement("Id");
            w.WriteAttributeString("value", Id.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Number");
            w.WriteAttributeString("value", Number.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Priority");
            w.WriteAttributeString("value", Priority.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Length");
            w.WriteAttributeString("value", Length.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Radius");
            w.WriteAttributeString("value", Radius.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Corrector");
            w.WriteAttributeString("value", Corrector.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Name");
            w.WriteAttributeString("value", Name);
            w.WriteEndElement();

            w.WriteStartElement("Reduction");
            w.WriteAttributeString("value", Reduction.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Advance");
            w.WriteAttributeString("value", Advance.ToString());
            w.WriteEndElement();

            w.WriteStartElement("Step");
            w.WriteAttributeString("value", Step.ToString());
            w.WriteEndElement();

            w.WriteStartElement("MinToolMoveLength");
            w.WriteAttributeString("value", MinToolMoveLength.ToString());
            w.WriteEndElement();

            w.WriteStartElement("MinDiskSlope");
            w.WriteAttributeString("value", MinDiskSlope.ToString());
            w.WriteEndElement();

            w.WriteStartElement("MaxDiskSlope");
            w.WriteAttributeString("value", MaxDiskSlope.ToString());
            w.WriteEndElement();

            w.WriteStartElement("OffsetDiskSlope");
            w.WriteAttributeString("value", OffsetDiskSlope.ToString());
            w.WriteEndElement();

            w.WriteEndElement();
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="baseNode">base node of XML</param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlNode baseNode)
        {
            XmlDocument doc = baseNode.OwnerDocument;

            XmlNode toolValue = doc.CreateNode(XmlNodeType.Element, "Tool", "");

            XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "Type", "");
            XmlAttribute attr = doc.CreateAttribute("value");
            attr.Value = EN_TOOL_TYPE_To_String(Type);
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "State", "");
            attr = doc.CreateAttribute("value");
            attr.Value = EN_TOOL_STATE_To_String(State);
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Id", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Id.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Number", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Number.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Priority", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Priority.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Length", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Length.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Radius", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Radius.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Corrector", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Corrector.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Name", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Name;
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Reduction", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Reduction.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Advance", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Advance.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "Step", "");
            attr = doc.CreateAttribute("value");
            attr.Value = Step.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "MinToolMoveLength", "");
            attr = doc.CreateAttribute("value");
            attr.Value = MinToolMoveLength.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "MinDiskSlope", "");
            attr = doc.CreateAttribute("value");
            attr.Value = MinDiskSlope.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "MaxDiskSlope", "");
            attr = doc.CreateAttribute("value");
            attr.Value = MaxDiskSlope.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            attributeValue = doc.CreateNode(XmlNodeType.Element, "OffsetDiskSlope", "");
            attr = doc.CreateAttribute("value");
            attr.Value = OffsetDiskSlope.ToString();
            attributeValue.Attributes.Append(attr);
            toolValue.AppendChild(attributeValue);

            baseNode.AppendChild(toolValue);

        }
        #endregion

        #region Public Helper Methods
        /// ********************************************************************
        /// <summary>
        /// Conversione da enumerativo EN_TOOL_TYPE a stringa
        /// </summary>
        /// <param name="e">valore da convertire</param>
        /// <returns>stringa</returns>
        /// ********************************************************************
        public static string EN_TOOL_TYPE_To_String(EN_TOOL_TYPE e)
        {
            return e.ToString();
        }

        /// ********************************************************************
        /// <summary>
        /// Conversione da stringa a enumerativo EN_TOOL_TYPE
        /// </summary>
        /// <param name="s">stringa da convertire</param>
        /// <returns>enumerativo</returns>
        /// ********************************************************************
        public static EN_TOOL_TYPE String_To_EN_TOOL_TYPE(string s)
        {
            try
            {
                EN_TOOL_TYPE es = (EN_TOOL_TYPE)Enum.Parse(typeof(EN_TOOL_TYPE), s);
                return es;
            }
            catch
            {
                return EN_TOOL_TYPE.EN_TOOL_TYPE_UNKNOW;
            }
        }

        /// ********************************************************************
        /// <summary>
        /// Conversione da enumerativo EN_TOOL_STATE a stringa
        /// </summary>
        /// <param name="e">valore da convertire</param>
        /// <returns>stringa</returns>
        /// ********************************************************************
        public static string EN_TOOL_STATE_To_String(EN_TOOL_STATE e)
        {
            return e.ToString();
        }

        /// ********************************************************************
        /// <summary>
        /// Conversione da stringa a enumerativo EN_TOOL_TYPE
        /// </summary>
        /// <param name="s">stringa da convertire</param>
        /// <returns>enumerativo</returns>
        /// ********************************************************************
        public static EN_TOOL_STATE String_To_EN_TOOL_STATE(string s)
        {
            try
            {
                EN_TOOL_STATE es = (EN_TOOL_STATE)Enum.Parse(typeof(EN_TOOL_STATE), s);
                return  es;
            }
            catch 
            {
                return  EN_TOOL_STATE.AUTOMATIC;
            }
        }

        #endregion
    }

    public class Tools : SortedList<int,ToolInfo>
    {
        public void Add(ToolInfo tool)
        {
            if (tool.Priority == -1)
                tool.Priority = Count;
            else
                if (this.ContainsKey(tool.Priority))
                    tool.Priority = Count;

            Add(tool.Priority, tool);
        }
      
        public bool Contains(ToolInfo tool)
        {
            foreach (var ti in this)
            {
                if (tool == ti.Value)
                    return true;
            }
            return false;
        }

        public bool ContainType(EN_TOOL_TYPE tool_type)
        {
            foreach (var ti in this)
            {
                if (ti.Value.Type == tool_type)
                    return true;
            }
            return false;
        }

        public ToolInfo GetFirstToolOfType(EN_TOOL_TYPE tool_type, bool only_if_active)
        {
            foreach (var ti in this)
            {
                if (ti.Value.Type == tool_type)
                    if (only_if_active == false || ti.Value.State != EN_TOOL_STATE.DISABLED)
                        return ti.Value;
            }
            return null;
        }

        public ToolInfo GetFirstToolOfType(List<EN_TOOL_TYPE> list_tool_type, bool only_if_active)
        {
            foreach (var ti in this)
            {
                if (list_tool_type.Contains(ti.Value.Type))
                    if (only_if_active == false || ti.Value.State != EN_TOOL_STATE.DISABLED)
                        return ti.Value;
            }
            return null;
        }

        public ToolInfo GetDisk()
        {
            return GetFirstToolOfType(EN_TOOL_TYPE.EN_TOOL_TYPE_DISK, true);
        }

        public ToolInfo GetOptimalToolForCoreDrill(double drill_radius)
        {
            ToolInfo best_ti = null;

            foreach (var ti in this)
            {
                if (ti.Value.IsCoreDrill() || ti.Value.IsDrill())
                {
                    if (ti.Value.State == EN_TOOL_STATE.DISABLED)
                        continue;
                    if (MathUtil.QuoteIsLE(ti.Value.Radius - ti.Value.HolesDiameterError, drill_radius))
                    {
                        if (best_ti == null)
                            best_ti = ti.Value;
                        else
                            if (best_ti.Radius < ti.Value.Radius)
                                best_ti = ti.Value;
                    }
                }
            }

            return best_ti;
        }

        public bool IsSmallestTool(ToolInfo tool)
        {
            //cerco utensili che vanno ad attivare i flags
            //TecnoSideInfo.FirstVertexCompletedWithAvailableTools
            //TecnoSideInfo.SecondVertexCompletedWithAvailableTools
            //20160225 per il momento è gestito solo per waterjet, per considerare completato nei confronti di RhinoNC un vertice con una concavità nel materiale che viene eseguito con waterjet 3 assi

            if(Count == 0)
                return false;

            //utensile standard: waterjet o router
            var tStandard = GetFirstToolOfType(new List<EN_TOOL_TYPE>(){EN_TOOL_TYPE.EN_TOOL_TYPE_WATER, EN_TOOL_TYPE.EN_TOOL_TYPE_ROUTER}, true);
            //foretto di qualsiasi tipo
            var tBore = GetFirstToolOfType(new List<EN_TOOL_TYPE>(){EN_TOOL_TYPE.EN_TOOL_TYPE_DRILL, EN_TOOL_TYPE.EN_TOOL_TYPE_CORE_DRILL}, true);

            if (tStandard == null)
            {
                List<ToolInfo> ltb = null;
                var keys = this.Keys;
                for (int i = 0; i < keys.Count; i++)
                {
                    var ti = this[keys[i]];
                    if(ti.IsGenericDrill())
                    {
                        if(ltb == null)
                            ltb = new List<ToolInfo>();
                        ltb.Add(ti);
                    }
                }
                ltb.Sort(new GenericToolRadiusComparer());
                if (ltb != null && tool.IsGenericDrill())
                {
                    return ltb[0].DbId == tool.DbId;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                List<ToolInfo> ltb = null;
                var keys = this.Keys;
                for (int i = 0; i < keys.Count; i++)
                {
                    var ti = this[keys[i]];
                    if (ti.IsGenericStandard())
                    {
                        if (ltb == null)
                            ltb = new List<ToolInfo>();
                        ltb.Add(ti);
                    }
                }
                ltb.Sort(new GenericToolRadiusComparer());
                if (ltb != null && tool.IsGenericStandard())
                {
                    return ltb[0].DbId == tool.DbId;
                }
                else
                {
                    return false;
                }
            }
        }
        public class GenericToolRadiusComparer : IComparer<ToolInfo>
        {
            public int Compare(ToolInfo t1, ToolInfo t2)
            {
                return t1.Radius.CompareTo(t2.Radius);
            }
        }

        /// <summary>
        /// Scrive il file xml
        /// </summary>
        /// <param name="file"></param>
        /// <param name="tools"></param>
        /// <returns></returns>
        public static bool WriteFileXml(string file, string workcentergroupcode, Breton.BCamPath.Tools tools)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode root;
            XmlNode wcgNode = null;

            if (!System.IO.File.Exists(file))
            {
                root = doc.CreateNode(XmlNodeType.Element, "TOOLS", "");
                XmlAttribute attr = doc.CreateAttribute("version");
                attr.Value = "1.0";
                root.Attributes.Append(attr);
            }
            else
            {
                doc.Load(file);
                root = doc.SelectSingleNode("TOOLS");

                wcgNode = root.SelectSingleNode(workcentergroupcode);

                if (wcgNode != null)
                    root.RemoveChild(wcgNode);
            }

            // Crea il nodo del centro di lavoro
            wcgNode = doc.CreateNode(XmlNodeType.Element, workcentergroupcode, "");
            root.AppendChild(wcgNode);

            //Scorre tutti gli utensili e li scrive nel file xml
            foreach (KeyValuePair<int, ToolInfo> ti in tools)
                ti.Value.WriteFileXml(wcgNode);

            doc.AppendChild(root);
            doc.Save(file);

            return true;
        }

        /// <summary>
        /// Legge il file xml e carica la struttura dati
        /// </summary>
        /// <param name="file"></param>
        /// <param name="tools"></param>
        /// <returns></returns>
        public static bool ReadFileXml(string file, string workcentergroupcode, out Breton.BCamPath.Tools tools)
        {
            tools = new Breton.BCamPath.Tools();

            if (String.IsNullOrEmpty(workcentergroupcode))
                return false;

            // Legge il file xml
            XmlDocument doc = new XmlDocument();
            doc.Load(file);
            XmlNode root = doc.SelectSingleNode("TOOLS/" + workcentergroupcode.Trim());

            if (root == null)
                return false;

            //Scorre tutti i nodi trovati e inserisce i valori sulla struttura dati
            foreach (XmlNode nd in root.ChildNodes)
            {
                ToolInfo ti = new ToolInfo();
                ti.ReadFileXml(nd);

                tools.Add(ti);
            }
            return true;
        }
    }
}
