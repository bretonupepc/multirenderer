﻿using System;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Breton.MathUtils;

using Breton.Polygons;
using TraceLoggers;

namespace Breton.BCamPath
{
    [FlagsAttribute]
    public enum EN_TOOLPATH_TYPE
    {
        None = 0,                   // nessuno
        CENTER = 1,                 // Centro
        SX = 2,                     // Sinistra
        DX = 4,                     // Destra
        INTERNAL = 8,               // Interno
        EXTERNAL = 16,              // Esterno
        TOOLPATH = 256,             // è un percorso utensile
        RODDINGPATH = 512,          // percorso utensile di rinforzo

        GROOVEPATH = 1024,          // zona che definisce uno svuotamento

        VDISKPATH = 2048,           // percorso utensile lavorazione bisellatura

        EXT_CAM = 4096,             // percorso prodotto da un cam esterno

        UNFRAMEPATH = 8192,         // percorso di refilatura o scorniciatura

        BLIND_HOLE = 16384,         // foro cieco

        RODDINGTOOLPATH = RODDINGPATH | TOOLPATH,

        BLIND_HOLE_TOOLPATH = BLIND_HOLE | TOOLPATH,

        TOOLPATH_EXTERNAL = TOOLPATH | EXTERNAL,
        TOOLPATH_EXTERNAL_CENTER = TOOLPATH | CENTER | EXTERNAL,
        TOOLPATH_EXTERNAL_SX = TOOLPATH | SX | EXTERNAL,
        TOOLPATH_EXTERNAL_DX = TOOLPATH | DX | EXTERNAL,

        TOOLPATH_INTERNAL = TOOLPATH | INTERNAL,
        TOOLPATH_INTERNAL_CENTER = TOOLPATH | CENTER | INTERNAL,
        TOOLPATH_INTERNAL_SX = TOOLPATH | SX | INTERNAL,
        TOOLPATH_INTERNAL_DX = TOOLPATH | DX | INTERNAL,

        RHINONC_EXCLUDED = GROOVEPATH | RODDINGPATH | RODDINGTOOLPATH | BLIND_HOLE | BLIND_HOLE_TOOLPATH,
        AUTO_BRIDGE_EXCLUDED = EXTERNAL | GROOVEPATH | RODDINGPATH | RODDINGTOOLPATH | BLIND_HOLE | BLIND_HOLE_TOOLPATH,
    }

    public enum EN_TOOL_SIDE
    {
        EN_TOOL_SIDE_CENTER,
        EN_TOOL_SIDE_RIGHT,
        EN_TOOL_SIDE_LEFT,
        EN_TOOL_SIDE_NUM_OF
    }

    public enum EN_TOOL_USE_ON_ARC
    {
        ALWAYS_DISK,
        ALWAYS_WATERJET,
        ALWAYS_ROUTER,
        ALWAYS_DRILL,
        DISK_THAN_WATERJET,
        DISK_THAN_ROUTER,
        DISK_THAN_DRILL,
        NUM_OF
    }

    public enum EN_LEAD_MODE
    {
        NONE,
        DEFAULT,
        SEGMENT_ONLY,
        ARC_ONLY,
        SEGMENT_ARC,
        ARC_SEGMENT,
        NUM_OF
    }

    public enum EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS
    {
        NONE = 0,
        PREVIOUS_SEGMENT = 1,
        NEXT_SEGMENT = 2,
        BOTH_EXTREMITIES_SEGMENT = 3
    }

    public class LeadMode
    {
        #region Variables
        private EN_LEAD_MODE enMode;
        private double dfSegmentLength;
        private double dfSegmentInclination;
        private double dfArcRadius;
        private double dfArcAmplitute;
        private bool dbFromTraceCenter;
        #endregion

        #region Properties
        public EN_LEAD_MODE Mode
        {
            get { return enMode; }
            set { enMode = value; }
        }

        public double SegmentLength
        {
            get { return dfSegmentLength; }
            set { dfSegmentLength = value; }
        }

        public double SegmentInclination
        {
            get { return dfSegmentInclination; }
            set { dfSegmentInclination = value; }
        }

        public double ArcRadius
        {
            get { return dfArcRadius; }
            set { dfArcRadius = value; }
        }

        public double ArcAmplitute
        {
            get { return dfArcAmplitute; }
            set { dfArcAmplitute = value; }
        }

        public bool FromTraceCenter
        {
            get { return dbFromTraceCenter; }
            set { dbFromTraceCenter = value; }
        }
        #endregion

        #region Costructors
        public LeadMode()
        {
            enMode = EN_LEAD_MODE.DEFAULT;
            dfArcAmplitute = 0;
            dfArcRadius = 0;
            dfSegmentInclination = 0;
            dfSegmentLength = 0;
            dbFromTraceCenter = false;
        }

        public LeadMode(LeadMode src)
        {
            enMode = src.enMode;
            dfArcAmplitute = src.dfArcAmplitute;
            dfArcRadius = src.ArcRadius;
            dfSegmentInclination = src.dfSegmentInclination;
            dfSegmentLength = src.dfSegmentLength;
            dbFromTraceCenter = src.dbFromTraceCenter;
        }
        #endregion

        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     ReadFileXml: read private informations from XMl stream
        /// </summary>
        /// <param name="n">XML node to read form</param>
        /// <returns>true if no errors encontred, false otherwise</returns>
        //**************************************************************************
        public virtual bool ReadXml(XmlNode n)
        {
            try
            {
                foreach (XmlNode chn in n.ChildNodes)
                {
                    if (chn.Name == "Mode")
                    {
                        Mode = LeadMode.GetLeadModeFromString(chn.Attributes.GetNamedItem("value").Value);
                        continue;
                    }

                    if (chn.Name == "SegmentLength")
                    {
                        SegmentLength = Convert.ToDouble(chn.Attributes.GetNamedItem("value").Value);
                        continue;
                    }

                    if (chn.Name == "SegmentInclination")
                    {
                        SegmentInclination = (Convert.ToDouble(chn.Attributes.GetNamedItem("value").Value));
                        continue;
                    }

                    if (chn.Name == "ArcAmplitute")
                    {
                        ArcAmplitute = Convert.ToDouble(chn.Attributes.GetNamedItem("value").Value);
                        continue;
                    }

                    if (chn.Name == "ArcRadius")
                    {
                        ArcRadius = Convert.ToDouble(chn.Attributes.GetNamedItem("value").Value);
                        continue;
                    }

                    if (chn.Name == "FromTraceCenter")
                    {
                        FromTraceCenter = Convert.ToBoolean(chn.Attributes.GetNamedItem("value").Value);
                        continue;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("TecnoPathInfo.ReadFileXml.LeadMode.ReadFileXml Error.", ex);
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="w">
        ///     strem to write to
        /// </param>
        /// <param name="name">
        ///     node's name
        /// </param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlTextWriter w, string name)
        {

            w.WriteStartElement(name);

            w.WriteStartElement("Mode");
            w.WriteAttributeString("value", GetLeadModeFromEnum(Mode));
            w.WriteEndElement();

            w.WriteStartElement("SegmentLength");
            w.WriteAttributeString("value", SegmentLength.ToString());
            w.WriteEndElement();

            w.WriteStartElement("SegmentInclination");
            w.WriteAttributeString("value", SegmentInclination.ToString());
            w.WriteEndElement();

            w.WriteStartElement("ArcAmplitute");
            w.WriteAttributeString("value", ArcAmplitute.ToString());
            w.WriteEndElement();

            w.WriteStartElement("ArcRadius");
            w.WriteAttributeString("value", ArcRadius.ToString());
            w.WriteEndElement();

            w.WriteStartElement("FromTraceCenter");
            w.WriteAttributeString("value", FromTraceCenter.ToString());
            w.WriteEndElement();

            w.WriteEndElement();
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="baseNode">
        ///     base node of XML
        /// </param>
        /// <param name="name">
        ///     node's name
        /// </param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlNode baseNode, string name)
        {

            XmlNode leadnode = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, name, ""); ;
            baseNode.AppendChild(leadnode);

            {
                XmlNode attributeValue = leadnode.OwnerDocument.CreateNode(XmlNodeType.Element, "Mode", ""); ;
                XmlAttribute attr = leadnode.OwnerDocument.CreateAttribute("value");
                attr.Value = GetLeadModeFromEnum(Mode);
                attributeValue.Attributes.Append(attr);
                leadnode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = leadnode.OwnerDocument.CreateNode(XmlNodeType.Element, "SegmentLength", ""); ;
                XmlAttribute attr = leadnode.OwnerDocument.CreateAttribute("value");
                attr.Value = SegmentLength.ToString();
                attributeValue.Attributes.Append(attr);
                leadnode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = leadnode.OwnerDocument.CreateNode(XmlNodeType.Element, "SegmentInclination", ""); ;
                XmlAttribute attr = leadnode.OwnerDocument.CreateAttribute("value");
                attr.Value = SegmentInclination.ToString();
                attributeValue.Attributes.Append(attr);
                leadnode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = leadnode.OwnerDocument.CreateNode(XmlNodeType.Element, "ArcAmplitute", ""); ;
                XmlAttribute attr = leadnode.OwnerDocument.CreateAttribute("value");
                attr.Value = ArcAmplitute.ToString();
                attributeValue.Attributes.Append(attr);
                leadnode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = leadnode.OwnerDocument.CreateNode(XmlNodeType.Element, "ArcRadius", ""); ;
                XmlAttribute attr = leadnode.OwnerDocument.CreateAttribute("value");
                attr.Value = ArcRadius.ToString();
                attributeValue.Attributes.Append(attr);
                leadnode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = leadnode.OwnerDocument.CreateNode(XmlNodeType.Element, "FromTraceCenter", ""); ;
                XmlAttribute attr = leadnode.OwnerDocument.CreateAttribute("value");
                attr.Value = FromTraceCenter.ToString();
                attributeValue.Attributes.Append(attr);
                leadnode.AppendChild(attributeValue);
            }
        }
        #endregion

        #region Private Methods
        private static EN_LEAD_MODE GetLeadModeFromString(string szmode)
        {
            EN_LEAD_MODE em = EN_LEAD_MODE.DEFAULT;
            for ( int i = (int)EN_LEAD_MODE.NONE; i < (int)EN_LEAD_MODE.NUM_OF; i++)
            {
                em = (EN_LEAD_MODE)i;
                if (szmode == em.ToString())
                    return em;
            }
            return EN_LEAD_MODE.DEFAULT;
        }

        static private string GetLeadModeFromEnum(EN_LEAD_MODE mode)
        {
            return mode.ToString();
        }

        #endregion
    }

    public class ToolLeadMode
    {
        #region Variables
        private int iToolId;
        private LeadMode clLeadIn;
        private LeadMode clLeadOut;
        #endregion

        #region Properties
        public int ToolId
        {
            get { return iToolId; }
            set { iToolId = value; }
        }

        public LeadMode LeadIn
        {
            get { return clLeadIn; }
            set { clLeadIn = value; }
        }

        public LeadMode LeadOut
        {
            get { return clLeadOut; }
            set { clLeadOut = value; }
        }
        #endregion

        #region Costructors
        public ToolLeadMode()
        {
            iToolId = -1;
            clLeadIn = new LeadMode();
            clLeadOut = new LeadMode();
        }

        public ToolLeadMode(ToolLeadMode src)
        {
            iToolId = src.iToolId;
            clLeadIn = new LeadMode(src.clLeadIn);
            clLeadOut = new LeadMode(src.clLeadOut);
        }
        #endregion

        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     ReadFileXml: read private informations from XMl stream
        /// </summary>
        /// <param name="n">XML node to read form</param>
        /// <returns>true if no errors encontred, false otherwise</returns>
        //**************************************************************************
        public virtual bool ReadXml(XmlNode n)
        {
            try
            {
                foreach (XmlNode chn in n.ChildNodes)
                {
                    if (chn.Name == "ToolNumber")
                    {
                        ToolId = Convert.ToInt32(chn.Attributes.GetNamedItem("value").Value);
                        continue;
                    }

                    if (chn.Name == "LeadIn")
                    {
                        if (!clLeadIn.ReadXml(chn))
                            return  false;
                        continue;
                    }

                    if (chn.Name == "LeadOut")
                    {
                        if (!clLeadOut.ReadXml(chn))
                            return false;
                        continue;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("TecnoPathInfo.ReadFileXml.ToolLeadMode.ReadFileXml Error.", ex);
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="w">
        ///     strem to write to
        /// </param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("ToolLeadMode");

            w.WriteStartElement("ToolNumber");
            w.WriteAttributeString("value", ToolId.ToString());
            w.WriteEndElement();

            LeadIn.WriteFileXml(w, "LeadIn");

            LeadOut.WriteFileXml(w, "LeadIn");

            w.WriteEndElement();
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="baseNode">
        ///     base node of XML
        /// </param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlNode baseNode)
        {

            XmlNode toolleadnode = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ToolLeadMode", ""); ;
            baseNode.AppendChild(toolleadnode);

            {
                XmlNode attributeValue = toolleadnode.OwnerDocument.CreateNode(XmlNodeType.Element, "ToolNumber", ""); ;
                XmlAttribute attr = toolleadnode.OwnerDocument.CreateAttribute("value");
                attr.Value = ToolId.ToString();
                attributeValue.Attributes.Append(attr);
                toolleadnode.AppendChild(attributeValue);
            }

            LeadIn.WriteFileXml(toolleadnode, "LeadIn");

            LeadIn.WriteFileXml(toolleadnode, "LeadOut");
        }
        #endregion
    }

    public class ToolsLeadMode : Dictionary<int, ToolLeadMode>
    {
        #region Constructors
        public ToolsLeadMode()
            : base()
        {
        }

        public ToolsLeadMode(ToolsLeadMode src)
            : base(src)
        {
        }
        #endregion

        #region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     ReadFileXml: read private informations from XMl stream
        /// </summary>
        /// <param name="n">XML node to read form</param>
        /// <returns>true if no errors encontred, false otherwise</returns>
        //**************************************************************************
        public virtual bool ReadXml(XmlNode n)
        {
            if (n.Name != "ToolsLeadMode")
                return true;

            Clear();

            try
            {
                foreach (XmlNode chn in n.ChildNodes)
                {
                    if (chn.Name == "ToolLeadMode")
                    {
                        ToolLeadMode tlm = new ToolLeadMode();
                        if (!tlm.ReadXml(chn))
                            return false;
                        Add(tlm.ToolId, tlm);
                        continue;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("TecnoPathInfo.ReadFileXml.ToolLeadMode.ReadFileXml Error.", ex);
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="w">
        ///     strem to write to
        /// </param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("ToolsLeadMode");

            foreach (var tlm in this)
                tlm.Value.WriteFileXml(w);                

            w.WriteEndElement();
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="baseNode">
        ///     base node of XML
        /// </param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlNode baseNode)
        {

            XmlNode toolsleadnode = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ToolsLeadMode", ""); ;
            baseNode.AppendChild(toolsleadnode);

            foreach (var tlm in this)
                tlm.Value.WriteFileXml(toolsleadnode);
        }
        #endregion
    }

    public class SideForbiddenPointsOffset
    {
        #region Variables
        public double _FromStart;                           // ultima posizione non consentita a partire dal primo punto del lato, in % rispetto la lunghezza del lato
        public double _ToEnd;                               // prima posizione non consentita nella parte finale del lato, in % rispetto la lunghezza del lato
        #endregion

        #region Properties
        public double FromStart
        { 
            get {return _FromStart; } 
            set
            {
                if (value < 0)
                    _FromStart = 0;
                else
                    if (value > 1)
                        _FromStart = 1;
                    else
                        _FromStart = value;
            } 
        }
        public double ToEnd
        {
            get { return _ToEnd; }
            set
            {
                if (value < 0)
                    _ToEnd = 0;
                else
                    if (value > 1)
                        _ToEnd = 1;
                    else
                        _ToEnd = value;
            }
        }
        #endregion

        #region Constructors
        public SideForbiddenPointsOffset()
        {
            FromStart = 0;
            ToEnd = 0;
        }

        public SideForbiddenPointsOffset(SideForbiddenPointsOffset src)
        {
            FromStart = src.FromStart;
            ToEnd = src.ToEnd;
        }
        #endregion

        #region Public Methods
        public void GetSideForbiddenRelativeLengths(double side_length, out double start_length, out double end_length)
        {
            start_length = side_length * FromStart;
            end_length = side_length * ToEnd;
        }

        public virtual bool ReadFileXml(XmlNode n)
        {
            XmlNode nd = n.SelectSingleNode("FromStart");
            if (nd == null)
                return  false;
            FromStart = Convert.ToDouble(nd.Attributes.GetNamedItem("value").Value);

            nd = n.SelectSingleNode("ToEnd");
            if (nd == null)
                return false;
            ToEnd = Convert.ToDouble(nd.Attributes.GetNamedItem("value").Value);

            return true;
        }

        public virtual void WriteFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("SideForbiddenPointsOffset");

            w.WriteStartElement("FromStart");
            w.WriteAttributeString("value", FromStart.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();

            w.WriteStartElement("ToEnd");
            w.WriteAttributeString("value", ToEnd.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();

            w.WriteEndElement();
        }

        public virtual void WriteFileXml(XmlNode baseNode)
        {
            XmlNode sfpo_node = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "SideForbiddenPointsOffset", "");
            baseNode.AppendChild(sfpo_node);

            XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "FromStart", "");
            XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
            attr.Value = FromStart.ToString(CultureInfo.InvariantCulture);
            attributeValue.Attributes.Append(attr);
            sfpo_node.AppendChild(attributeValue);

            attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ToEnd", "");
            attr = baseNode.OwnerDocument.CreateAttribute("value");
            attr.Value = ToEnd.ToString(CultureInfo.InvariantCulture);
            attributeValue.Attributes.Append(attr);
            sfpo_node.AppendChild(attributeValue);
        }

        #endregion
    }

    public class ToolWorkingInfo
    {
        #region Variables
        public SideForbiddenPointsOffset ForbiddenPointsOffset { get; set; }
        public double OverMaterial { get; set; }
        /// <summary>
        /// Valore minimo incisione disco
        /// </summary>
        public double MinEngrave { get; set; }
        /// <summary>
        /// Abilitazione incisione disco
        /// </summary>
        public bool UseEngrave { get; set; }
        /// <summary>
        /// Abilitazione bypass incisione disco a causa di valore minimo o mancanza tastatore
        /// </summary>
        public bool EnableLimitIncision { get; set; }
        /// <summary>
        /// Valore minimo incisione disco utilizzato da bypass
        /// </summary>
        public double LimitIncision { get; set; }
        /// <summary>
        /// (Config.CheckSpessTgIncl)Bypass CAM incisioni a causa di mancanza tastatore e bypass CN esecuzione tastature tagli inclinati
        /// </summary>
        public bool EnableCheckSpessTgIncl { get; set; }
        #endregion

        #region Properties
        #endregion

        #region Constructors
        public ToolWorkingInfo()
        {
            ForbiddenPointsOffset = new BCamPath.SideForbiddenPointsOffset();
            OverMaterial = 0;
            MinEngrave = 0;
            UseEngrave = false;
            EnableLimitIncision = true;
            LimitIncision = 0.5;
            EnableCheckSpessTgIncl = true;
        }

        public ToolWorkingInfo(ToolWorkingInfo src)
        {
            ForbiddenPointsOffset = new SideForbiddenPointsOffset(src.ForbiddenPointsOffset);
            OverMaterial = src.OverMaterial;
            MinEngrave = src.MinEngrave;
            UseEngrave = src.UseEngrave;
            EnableLimitIncision = src.EnableLimitIncision;
            LimitIncision = src.LimitIncision;
            EnableCheckSpessTgIncl = src.EnableCheckSpessTgIncl;
        }
        #endregion

        #region Public Methods
        public void GetSideForbiddenRelativeLengths(double side_length, out double start_length, out double end_length)
        {
            start_length = side_length * ForbiddenPointsOffset.FromStart;
            end_length = side_length * ForbiddenPointsOffset.ToEnd;
        }

        public double GetWorkingToolRadius(ToolInfo ti)
        {
            return ti.Radius + OverMaterial;
        }

        public virtual bool ReadFileXml(XmlNode n)
        {
            XmlNode nd = n.SelectSingleNode("SideForbiddenPointsOffset");
            if (nd != null)
                ForbiddenPointsOffset.ReadFileXml(nd);

            nd = n.SelectSingleNode("OverMaterial");
            if (nd != null)
                OverMaterial = Convert.ToDouble(nd.Attributes.GetNamedItem("value").Value);

            nd = n.SelectSingleNode("MinEngrave");
            if (nd != null)
                MinEngrave = Convert.ToDouble(nd.Attributes.GetNamedItem("value").Value);

            nd = n.SelectSingleNode("UseEngrave");
            if (nd != null)
                UseEngrave = Convert.ToBoolean(nd.Attributes.GetNamedItem("value").Value);

            nd = n.SelectSingleNode("EnableLimitIncision");
            if (nd != null)
                EnableLimitIncision = Convert.ToBoolean(nd.Attributes.GetNamedItem("value").Value);

            nd = n.SelectSingleNode("LimitIncision");
            if (nd != null)
                LimitIncision = Convert.ToDouble(nd.Attributes.GetNamedItem("value").Value);

            nd = n.SelectSingleNode("EnableCheckSpessTgIncl");
            if (nd != null)
                EnableCheckSpessTgIncl = Convert.ToBoolean(nd.Attributes.GetNamedItem("value").Value);

            return true;
        }

        public virtual void WriteFileXml(XmlTextWriter w)
        {
            ForbiddenPointsOffset.WriteFileXml(w);

            w.WriteStartElement("OverMaterial");
            w.WriteAttributeString("value", OverMaterial.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();

            w.WriteStartElement("MinEngrave");
            w.WriteAttributeString("value", MinEngrave.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();

            w.WriteStartElement("UseEngrave");
            w.WriteAttributeString("value", UseEngrave.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();

            w.WriteStartElement("EnableLimitIncision");
            w.WriteAttributeString("value", EnableLimitIncision.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();

            w.WriteStartElement("LimitIncision");
            w.WriteAttributeString("value", LimitIncision.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();

            w.WriteStartElement("EnableCheckSpessTgIncl");
            w.WriteAttributeString("value", EnableCheckSpessTgIncl.ToString(CultureInfo.InvariantCulture));
            w.WriteEndElement();
        }

        public virtual void WriteFileXml(XmlNode baseNode)
        {
            ForbiddenPointsOffset.WriteFileXml(baseNode);

            {
                XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "OverMaterial", "");
                XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
                attr.Value = OverMaterial.ToString(CultureInfo.InvariantCulture);
                attributeValue.Attributes.Append(attr);
                baseNode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "MinEngrave", "");
                XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
                attr.Value = MinEngrave.ToString(CultureInfo.InvariantCulture);
                attributeValue.Attributes.Append(attr);
                baseNode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "UseEngrave", "");
                XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
                attr.Value = UseEngrave.ToString(CultureInfo.InvariantCulture);
                attributeValue.Attributes.Append(attr);
                baseNode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "EnableLimitIncision", "");
                XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
                attr.Value = EnableLimitIncision.ToString(CultureInfo.InvariantCulture);
                attributeValue.Attributes.Append(attr);
                baseNode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "LimitIncision", "");
                XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
                attr.Value = LimitIncision.ToString(CultureInfo.InvariantCulture);
                attributeValue.Attributes.Append(attr);
                baseNode.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "EnableCheckSpessTgIncl", "");
                XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
                attr.Value = EnableCheckSpessTgIncl.ToString(CultureInfo.InvariantCulture);
                attributeValue.Attributes.Append(attr);
                baseNode.AppendChild(attributeValue);
            }
        }
        #endregion
    }

    public class ToolsWorkingInfo : Dictionary<int, ToolWorkingInfo>
    {
        #region Constructors
        public ToolsWorkingInfo()
            : base()
        {
        }

        public ToolsWorkingInfo(ToolsWorkingInfo src)
            : base()
        {
            foreach (var el in src)
            {
                ToolWorkingInfo rwi = new ToolWorkingInfo(el.Value);
                Add(el.Key, rwi);
            }
        }
        #endregion

        #region Public Methods
        public virtual bool ReadFileXml(XmlNode n)
        {
            try
            {
                // Scorre tutti i sottonodi
                foreach (XmlNode chn in n.ChildNodes)
                {
                    // Cerca il nodo TecnoPathInfo
                    if (chn.Name == "Tool")
                    {
                        XmlNode tool = chn.SelectSingleNode("Id");
                        if (tool == null)
                            continue;

                        int id = Convert.ToInt32(tool.Attributes.GetNamedItem("value").Value);

                        ToolWorkingInfo el = new ToolWorkingInfo();
                        if (el.ReadFileXml(chn) == false)
                            return false;

                        Add(id, el);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this + "ReadFileXml exception: ", ex);
                return false;
            }
        }

        public virtual void WriteFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("ToolsWorkingInfo");

            foreach (var el in this)
            {
                w.WriteStartElement("Tool");

                w.WriteStartElement("Id");
                w.WriteAttributeString("value", el.Key.ToString());
                w.WriteEndElement();

                el.Value.WriteFileXml(w);

                w.WriteEndElement();
            }

            w.WriteEndElement();
        }

        public virtual void WriteFileXml(XmlNode baseNode)
        {
            //20160510 si salvava solo il nodo 'ToolsWorkingInfo' senza 'Tool' perchè c'era 'baseNode.AppendChild(attributes)', al posto della versione corrente 'attributes.AppendChild(toolValue);'
            XmlNode attributes = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ToolsWorkingInfo", ""); ;

            baseNode.AppendChild(attributes);

            foreach (var el in this)
            {
                XmlNode toolValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Tool", "");

                XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Id", "");
                XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
                attr.Value = el.Key.ToString();
                attributeValue.Attributes.Append(attr);

                toolValue.AppendChild(attributeValue);

                el.Value.WriteFileXml(toolValue);

                attributes.AppendChild(toolValue);
            }
        }
        //public virtual void WriteFileXml(XmlNode baseNode)
        //{
        //    XmlNode attributes = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ToolsWorkingInfo", ""); ;

        //    foreach (var el in this)
        //    {
        //        XmlNode toolValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Tool", "");

        //        XmlNode attributeValue = baseNode.OwnerDocument.CreateNode(XmlNodeType.Element, "Id", "");
        //        XmlAttribute attr = baseNode.OwnerDocument.CreateAttribute("value");
        //        attr.Value = el.Key.ToString();
        //        attributeValue.Attributes.Append(attr);

        //        toolValue.AppendChild(attributeValue);

        //        el.Value.WriteFileXml(toolValue);

        //        baseNode.AppendChild(attributes);
        //    }
        //}

        #endregion
    }

    public class TecnoPathInfo
    {
        #region Variables
        private double dfMaterialThickness;
        private double dfSlabThickness;
        private double dfTableIncision;
        private double dfMinToolPathLengthForDiskTool;
        private EN_TOOL_SIDE enToolSide;
        private EN_TOOL_USE_ON_ARC enToolUseOnArc;
        private Tools lsTools;
        private ToolsLeadMode clToolsLeadMode;
        private bool bBridgeMode;
        private bool bBridgeModeUser;
        private bool bBridgeModeDefault;
        private bool bNoCutPolygon;
        private bool bBlindHole;
        private bool bHoleBoundary;
        private bool bWJSingleSegmentsWithNoArcs;
        private bool bToolpathLimits;
        private EN_TOOLPATH_TYPE enType;
        private ToolsWorkingInfo lsToolsWorkingInfo;
        private double dfMinArcRadiusForDiskTool;
        private double dfMinArcRadiusForDiskToolConvex;
        private EN_TOOL_SIDE enToolSideOnGroove;
        private EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS enArcInterruptsDiskOnAdjacentSegments;
        private XmlNode xExternalCamParameters;
        #endregion

		#region Constructors

        ///*********************************************************************
        /// <summary>
        /// Default Constructor
		/// </summary>
        ///*********************************************************************
        public TecnoPathInfo()
		{
            dfMaterialThickness = 0;
            dfSlabThickness = 0;
            dfTableIncision = 0;
            MinToolPathLengthForDiskTool = 0;
            enToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_CENTER;
            enToolUseOnArc = EN_TOOL_USE_ON_ARC.ALWAYS_DISK;
            lsTools = new Tools();
            clToolsLeadMode = new ToolsLeadMode();
            bBridgeMode = false;
            bBridgeModeUser = false;
            bBridgeModeDefault = false;
			bNoCutPolygon = false;
            bBlindHole = false;
            bHoleBoundary = false;
            enType = EN_TOOLPATH_TYPE.EXTERNAL | EN_TOOLPATH_TYPE.DX;
            lsToolsWorkingInfo = new ToolsWorkingInfo();
            dfMinArcRadiusForDiskTool = 0;
            dfMinArcRadiusForDiskToolConvex = 0;
            enToolSideOnGroove = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
            enArcInterruptsDiskOnAdjacentSegments = EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS.NONE;
            bWJSingleSegmentsWithNoArcs = true;
            bToolpathLimits = false;
            xExternalCamParameters = null;
        }

        ///*********************************************************************
        /// <summary>
        /// Copy Constructor 
        /// </summary>
        /// <param name="tpi">oggetto da copiare</param>
        ///*********************************************************************
        public TecnoPathInfo(TecnoPathInfo tpi)
		{
            dfMaterialThickness = tpi.dfMaterialThickness;
            dfSlabThickness = tpi.dfSlabThickness;
            dfTableIncision = tpi.dfTableIncision;
            dfMinToolPathLengthForDiskTool = tpi.dfMinToolPathLengthForDiskTool;
            enToolSide = tpi.enToolSide;
            MaterialThickness = tpi.MaterialThickness;
            enToolUseOnArc = tpi.enToolUseOnArc;
            lsTools = new Tools();
            foreach (var ti in tpi.lsTools)
            {
                lsTools.Add(new ToolInfo(ti.Value));
            }
            clToolsLeadMode = new ToolsLeadMode(tpi.clToolsLeadMode);
            bBridgeMode = tpi.bBridgeMode;
            bBridgeModeUser = tpi.bBridgeModeUser;
            bBridgeModeDefault = tpi.bBridgeModeDefault;
			bNoCutPolygon = tpi.NoCutPolygon;
            bBlindHole = tpi.bBlindHole;
            bHoleBoundary = tpi.bHoleBoundary;
            enType = tpi.enType;
            lsToolsWorkingInfo = new ToolsWorkingInfo(tpi.lsToolsWorkingInfo);
            dfMinArcRadiusForDiskTool = tpi.dfMinArcRadiusForDiskTool;
            dfMinArcRadiusForDiskToolConvex = tpi.dfMinArcRadiusForDiskToolConvex;
            enToolSideOnGroove = tpi.enToolSideOnGroove;
            enArcInterruptsDiskOnAdjacentSegments = tpi.enArcInterruptsDiskOnAdjacentSegments;
            bWJSingleSegmentsWithNoArcs = tpi.bWJSingleSegmentsWithNoArcs;
            bToolpathLimits = tpi.bToolpathLimits;
            if(tpi.xExternalCamParameters != null)
                xExternalCamParameters = tpi.xExternalCamParameters.Clone();
        }
			
		#endregion

		#region Properties
        public double MaterialThickness
        {
          get { return dfMaterialThickness; }
          set 
          { 
              dfMaterialThickness = value;
          }
        }

        public double SlabThickness
        {
            get { return dfSlabThickness; }
            set
            {
                dfSlabThickness = value;
            }
        }

        public double TableIncision
        {
            get { return dfTableIncision; }
            set
            {
                dfTableIncision = value;
            }
        }

        public double MinToolPathLengthForDiskTool
        {
            get { return dfMinToolPathLengthForDiskTool; }
            set { dfMinToolPathLengthForDiskTool = value; }
        }

        public EN_TOOL_SIDE ToolSide
        {
          get { return enToolSide; }
          set { enToolSide = value; }
        }

        public EN_TOOL_USE_ON_ARC ToolUseOnArc
        {
            get { return enToolUseOnArc; }
            set { enToolUseOnArc = value; }
        }

        public Tools AvailableTools
        {
            get { return lsTools; }
            set { lsTools = value; }
        }

        public ToolsLeadMode ToolsLead
        {
            get { return clToolsLeadMode; }
            set { clToolsLeadMode = value; }
        }

        public bool BridgeMode
        {
            get { return bBridgeMode; }
            set { bBridgeMode = value; }
        }

        public bool BridgeModeUser
        {
            get { return bBridgeModeUser; }
            set { bBridgeModeUser = value; }
        }

        public bool BridgeModeDefault
        {
            get { return bBridgeModeDefault; }
            set { bBridgeModeDefault = value; }
        }

        public bool NoCutPolygon
        {
            get { return bNoCutPolygon; }
            set { bNoCutPolygon = value; }
        }

        public bool BlindHole
        {
            get { return bBlindHole; }
            set { bBlindHole = value; }
        }

        public bool HoleBoundary
        {
            get { return bHoleBoundary; }
            set { bHoleBoundary = value; }
        }

        public bool WJSingleSegmentsWithNoArcs
        {
            get { return bWJSingleSegmentsWithNoArcs; }
            set { bWJSingleSegmentsWithNoArcs = value; }
        }

        public bool ToolpathLimits
        {
            get { return bToolpathLimits; }
            set { bToolpathLimits = value; }
        }

        public EN_TOOLPATH_TYPE Type
        {
            get { return enType; }
            set { enType = value; }
        }

        public ToolsWorkingInfo ToolsWorkingInfo
        {
            get { return lsToolsWorkingInfo; }
            set { lsToolsWorkingInfo = value; }
        }

        public double MinArcRadiusForDiskTool
        {
            get { return dfMinArcRadiusForDiskTool; }
            set { dfMinArcRadiusForDiskTool = value; }
        }

        public double MinArcRadiusForDiskToolConvex
        {
            get { return dfMinArcRadiusForDiskToolConvex; }
            set { dfMinArcRadiusForDiskToolConvex = value; }
        }
        public EN_TOOL_SIDE ToolSideOnGroove
        {
            get { return enToolSideOnGroove; }
            set { enToolSideOnGroove = value; }
        }
        public EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS ArcInterruptsDiskOnAdjacentSegments
        {
            get { return enArcInterruptsDiskOnAdjacentSegments; }
            set { enArcInterruptsDiskOnAdjacentSegments = value; }
        }

        public XmlNode ExternalCamParameters
        {
            get { return xExternalCamParameters; }
            set { xExternalCamParameters = value; }
        }

        #endregion
        
		#region Public Methods
        ///**************************************************************************
        /// <summary>
        ///     test if this is an inner boundary (internal path)
        /// </summary>
        /// <returns>
        ///     true if this is an inner boundary (internal path)
        /// </returns>
        ///**************************************************************************
        public bool IsInternal()
        {
            if ((Type & EN_TOOLPATH_TYPE.INTERNAL) == EN_TOOLPATH_TYPE.INTERNAL)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     test if this is the outer boundary of a Reinforcement Working
        /// </summary>
        /// <returns>
        ///     true if this is an outer boundary of a reinforcement worning
        ///     false otherwise
        /// </returns>
        ///**************************************************************************
        public bool IsReinforcement()
        {
            if ((Type & EN_TOOLPATH_TYPE.RODDINGPATH) == EN_TOOLPATH_TYPE.RODDINGPATH)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     test if this is the outer boundary of a Groove Working
        /// </summary>
        /// <returns>
        ///     true if this is an outer boundary of a groove worning
        ///     false otherwise
        /// </returns>
        ///**************************************************************************
        public bool IsGroove()
        {
            if ((Type & EN_TOOLPATH_TYPE.GROOVEPATH) == EN_TOOLPATH_TYPE.GROOVEPATH)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     test if this is an unframe geometry or toolpath
        /// </summary>
        /// <returns>
        ///     true if this is unframe geometry or toolpath
        ///     false otherwise
        /// </returns>
        ///**************************************************************************
        public bool IsUnframe()
        {
            if ((Type & EN_TOOLPATH_TYPE.UNFRAMEPATH) == EN_TOOLPATH_TYPE.UNFRAMEPATH)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     test if this is a blind hole
        /// </summary>
        /// <returns>
        ///     true if this is a blind hole
        ///     false otherwise
        /// </returns>
        ///**************************************************************************
        public bool IsBlindHole()
        {
            if ((Type & EN_TOOLPATH_TYPE.BLIND_HOLE) == EN_TOOLPATH_TYPE.BLIND_HOLE)
                return true;
            return false;
        }

        ///**************************************************************************
        /// <summary>
        ///     test if internal values are accettable; if not accettable
        ///     it may indicates there was no explicit initialization.
        /// </summary>
        /// <returns></returns>
        ///**************************************************************************
        public bool IsValid()
        {
            return (lsTools.Count >0 && dfMaterialThickness >= 0 ? true : false);
        }
        public bool IsValidWithExternalCam()
        {
            return (dfMaterialThickness >= 0 ? true : false);
        }

        ///**************************************************************************
        /// <summary>
        ///     verify if the indicated tool is usable in this side.
        /// </summary>
        /// <param name="s">
        ///     side to consider
        /// </param>
        /// <param name="tool">
        ///     tool to verify
        /// </param>
        /// <returns>
        ///     true if indicated tool can be used
        ///     false otherwise
        /// </returns>
        ///**************************************************************************
        public bool GetToolToUse(ITecnoSide s, ToolInfo tool)
        {
            // l'utensile deve essere nella lista prevista
            if (lsTools.Contains(tool) == false)
                return false;

            // ci sono regole particolari per gli archi
            if (s is Arc)
            {
                double minArcRadiusForDiskTool = GetMinArcRadius((Arc)s, MinArcRadiusForDiskToolConvex, MinArcRadiusForDiskTool);

                // analyze rules defined for arcs handling
                switch (enToolUseOnArc)
                {
                    case EN_TOOL_USE_ON_ARC.ALWAYS_DISK:
                    {
                        if (!tool.IsDisk() || ((Arc)s).Radius < minArcRadiusForDiskTool)
                            return false;
                        return true;
                    }

                    case EN_TOOL_USE_ON_ARC.ALWAYS_ROUTER:
                    {
                        if (tool.Type != EN_TOOL_TYPE.EN_TOOL_TYPE_ROUTER)
                            return false;
                        return true;
                    }

                    case EN_TOOL_USE_ON_ARC.ALWAYS_WATERJET:
                    {
                        if (tool.Type != EN_TOOL_TYPE.EN_TOOL_TYPE_WATER)
                            return false;
                        return true;
                    }

                    case EN_TOOL_USE_ON_ARC.ALWAYS_DRILL:
                    {
                        if (!tool.IsDrill())
                            return false;
                        return true;
                    }

                    case EN_TOOL_USE_ON_ARC.DISK_THAN_ROUTER:
                    {
                        if ((tool.IsDisk() && ((Arc)s).Radius >= minArcRadiusForDiskTool) || 
                             tool.Type == EN_TOOL_TYPE.EN_TOOL_TYPE_ROUTER)
                            return true;
                        return false;
                    }

                    case EN_TOOL_USE_ON_ARC.DISK_THAN_WATERJET:
                    {
                        if ((tool.IsDisk() && ((Arc)s).Radius >= minArcRadiusForDiskTool) || 
                             tool.Type == EN_TOOL_TYPE.EN_TOOL_TYPE_WATER)
                            return true;
                        return false;
                    }

                    case EN_TOOL_USE_ON_ARC.DISK_THAN_DRILL:
                    {
                        if ((tool.IsDisk() && ((Arc)s).Radius >= minArcRadiusForDiskTool) || tool.IsDrill() || tool.IsCoreDrill())
                            return true;
                        return false;
                    }
                }

                // utensile non compatibile
                return false;
            }

            // utensile compatibile
            return true;
        }
        bool lesserThanMinArcRadiusForDiskTool(ITecnoSide s)
        {
            if (s is Arc == false)
                return true;

            Arc arc = ((Arc)s);
            double valMin = GetMinArcRadius(arc, MinArcRadiusForDiskToolConvex, MinArcRadiusForDiskTool);
            return arc.Radius < valMin;
        }
        public static double GetMinArcRadius(Arc arc, double convexValue, double concaveValue)
        {
            TecnoSegment s1 = new TecnoSegment(arc.StartPoint(), arc.MiddlePoint);
            TecnoSegment s2 = new TecnoSegment(arc.MiddlePoint, arc.PointAt(0.99));
            bool convex = (s1.Tangent() * s2.Tangent()).Z > 0;
            if (convex)
                return convexValue; //se arco convesso, si utilizza il limite per archi esterni
            else
                return concaveValue;
        }

        ///**************************************************************************
        /// <summary>
        ///     return forbidden point offset for indicated tool, if any
        /// </summary>
        /// <param name="tool_id">
        ///     indetification of the tool for which ther forbidden offset points
        ///     are requested
        /// </param>
        /// <returns>
        ///     forbidden offset points for indicated tool; if tool doesn't indicate
        ///     any forbidden point, than a default (no forbidded points) value will 
        ///     be returned.
        /// </returns>
        ///**************************************************************************
        public SideForbiddenPointsOffset GetSideForbiddenPointsOffsetForTool(int tool_id)
        {
            SideForbiddenPointsOffset rc = new SideForbiddenPointsOffset();

            if (lsToolsWorkingInfo != null)
                if (lsToolsWorkingInfo.ContainsKey(tool_id))
                    rc = lsToolsWorkingInfo[tool_id].ForbiddenPointsOffset;

            return rc;
        }

        ///**************************************************************************
        /// <summary>
        ///     return overmaterial to apply, if any
        /// </summary>
        /// <param name="tool_id">
        ///     indetification of the tool for which overmaterial value has to be
        ///     determined
        /// </param>
        /// <returns>
        ///     overmaterial to apply for this tool.
        /// </returns>
        ///**************************************************************************
        public double OverMaterial(int tool_id)
        {
            if (lsToolsWorkingInfo.ContainsKey(tool_id))
                return lsToolsWorkingInfo[tool_id].OverMaterial;

            return 0;
        }

        ///**************************************************************************
        /// <summary>
        ///     return max overmaterial to be applied.
        /// </summary>
        /// <returns>
        ///     maximum overmaterial to be applyed.
        /// </returns>
        ///**************************************************************************
        public double OverMaterialMax()
        {
            double overmaterial = 0;

            foreach (var tool in lsToolsWorkingInfo)
                if (tool.Value.OverMaterial > overmaterial)
                    overmaterial = tool.Value.OverMaterial;

            return overmaterial;
        }

        ///**************************************************************************
        /// <summary>
        ///     ReadFileXml: read private informations from XMl stream
        /// </summary>
        /// <param name="n">XML node to read form</param>
        /// <returns>true if no errors encontred, false otherwise</returns>
        //**************************************************************************
        public virtual bool ReadFileXml(XmlNode n)
        {

            lsTools.Clear();
            lsToolsWorkingInfo.Clear();

            try
            {
                XmlNodeList l = n.ChildNodes;

                // Scorre tutti i sottonodi
                foreach (XmlNode chn in l)
                {
                    // Cerca il nodo TecnoPathInfo
                    if (chn.Name == "TecnoPathInfo")
                    {
                        XmlNodeList tl = chn.ChildNodes;

                        foreach (XmlNode tn in tl)
                        {
                            // legge i parametri noti
                            if (tn.Name == "MaterialThickness")
                                dfMaterialThickness = Convert.ToDouble(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "SlabThickness")
                                dfSlabThickness = Convert.ToDouble(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "TableIncision")
                                dfTableIncision = Convert.ToDouble(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "MinToolPathLengthForDiskTool")
                                dfMinToolPathLengthForDiskTool = Convert.ToDouble(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "ToolSide")
                                enToolSide = GetToolSideFromString(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "ToolOnArc")
                                enToolUseOnArc = GetToolOnArcFromString(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "BridgeMode")
                                bBridgeMode = (tn.Attributes.GetNamedItem("value").Value == "YES" ? true : false);

                            if (tn.Name == "BridgeModeUser")
                                bBridgeModeUser = (tn.Attributes.GetNamedItem("value").Value == "YES" ? true : false);

                            if (tn.Name == "BridgeModeDefault")
                                bBridgeModeDefault = (tn.Attributes.GetNamedItem("value").Value == "YES" ? true : false);

                            if (tn.Name == "NoCutPolygon")
                                bNoCutPolygon = (tn.Attributes.GetNamedItem("value").Value == "YES" ? true : false);

                            if (tn.Name == "BlindHole")
                                bBlindHole = (tn.Attributes.GetNamedItem("value").Value == "YES" ? true : false);

                            if (tn.Name == "HoleBoundary")
                                bHoleBoundary = (tn.Attributes.GetNamedItem("value").Value == "YES" ? true : false);

                            if (tn.Name == "ToolSideOnGroove")
                                enToolSideOnGroove = GetToolSideFromString(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "ArcInterruptsDiskOnAdjacentSegments")
                                enArcInterruptsDiskOnAdjacentSegments = GetArcInterruptsDiskOnAdjacentSegmentsFromString(tn.Attributes.GetNamedItem("value").Value);

                            if (tn.Name == "ExternalCamParameters")
                                xExternalCamParameters = tn;//20160511
                        }
                    }
                    else if (chn.Name == "TecnoPathTools")
                    {
                        XmlNodeList tl = chn.ChildNodes;

                        foreach (XmlNode tn in tl)
                        {
                            if (tn.Name == "Tool")
                            {
                                ToolInfo tool = new ToolInfo();

                                if (!tool.ReadFileXml(tn))
                                    return false;

                                lsTools.Add(tool);
                            }
                        }
                    }
                    else if (chn.Name == "ToolsWorkingInfo")
                    {
                        if (!lsToolsWorkingInfo.ReadFileXml(chn))
                            return  false;
                    }
                    else if (chn.Name == "ToolsLeadMode")
                    {
                        if (!clToolsLeadMode.ReadXml( chn ))
                            return  false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("TecnoPathInfo.ReadFileXml Error.", ex);
                return false;
            }
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="w">strem to write to</param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlTextWriter w)
        {

            w.WriteStartElement("TecnoPathInfo");

            w.WriteStartElement("MaterialThickness");
            w.WriteAttributeString("value", dfMaterialThickness.ToString());
            w.WriteEndElement();

            w.WriteStartElement("SlabThickness");
            w.WriteAttributeString("value", dfSlabThickness.ToString());
            w.WriteEndElement();

            w.WriteStartElement("TableIncision");
            w.WriteAttributeString("value", dfTableIncision.ToString());
            w.WriteEndElement();

            w.WriteStartElement("MinToolPathLengthForDiskTool");
            w.WriteAttributeString("value", dfMinToolPathLengthForDiskTool.ToString());
            w.WriteEndElement();

            w.WriteStartElement("ToolSide");
            w.WriteAttributeString("value", GetToolSideFromEnum(enToolSide));
            w.WriteEndElement();

            w.WriteStartElement("ToolOnArc");
            w.WriteAttributeString("value", GetToolOnArcFromEnum(enToolUseOnArc));
            w.WriteEndElement();

            w.WriteStartElement("BridgeMode");
            w.WriteAttributeString("value", (bBridgeMode ? "YES" : "NO"));
            w.WriteEndElement();

            w.WriteStartElement("BridgeModeUser");
            w.WriteAttributeString("value", (bBridgeModeUser ? "YES" : "NO"));
            w.WriteEndElement();

            w.WriteStartElement("BridgeModeDefault");
            w.WriteAttributeString("value", (bBridgeModeDefault ? "YES" : "NO"));
            w.WriteEndElement();

            w.WriteStartElement("NoCutPolygon");
            w.WriteAttributeString("value", (bNoCutPolygon ? "YES" : "NO"));
            w.WriteEndElement();

            w.WriteStartElement("BlindHole");
            w.WriteAttributeString("value", (bBlindHole ? "YES" : "NO"));
            w.WriteEndElement();

            w.WriteStartElement("HoleBoundary");
            w.WriteAttributeString("value", (bHoleBoundary ? "YES" : "NO"));
            w.WriteEndElement();

            w.WriteStartElement("ToolSideOnGroove");
            w.WriteAttributeString("value", GetToolSideFromEnum(enToolSideOnGroove));
            w.WriteEndElement();

            w.WriteStartElement("ArcInterruptsDiskOnAdjacentSegments");
            w.WriteAttributeString("value", GetArcInterruptsDiskOnAdjacentSegmentsFromEnum(enArcInterruptsDiskOnAdjacentSegments));
            w.WriteEndElement();

            if (xExternalCamParameters != null)
            {
                //w.WriteStartElement("ExternalCamParameters");
                //w.WriteAttributeString("value", "xml");
                //w.WriteEndElement();
                xExternalCamParameters.WriteTo(w);//20160511
            }

            w.WriteEndElement();

            if (lsTools.Count > 0)
            {
                w.WriteStartElement("TecnoPathTools");

                foreach (var ti in lsTools)
                {
                    ti.Value.WriteFileXml(w);
                }

                w.WriteEndElement();
            }

            if (lsToolsWorkingInfo.Count > 0)
                lsToolsWorkingInfo.WriteFileXml(w);

            if (clToolsLeadMode.Count > 0)
                clToolsLeadMode.WriteFileXml(w);
        }

        ///**************************************************************************
        /// <summary>
        ///     WriteFileXml: write Tool Info to XML stream
        /// </summary> 
        /// <param name="baseNode">base node of XML</param>
        //**************************************************************************
        public virtual void WriteFileXml(XmlNode baseNode)
        {

            XmlDocument doc = baseNode.OwnerDocument;

            XmlNode attributes = doc.CreateNode(XmlNodeType.Element, "TecnoPathInfo", ""); ;

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "MaterialThickness", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfMaterialThickness.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "SlabThickness", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfSlabThickness.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "TableIncision", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfTableIncision.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "MinToolPathLengthForDiskTool", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = dfMinToolPathLengthForDiskTool.ToString();
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ToolSide", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = GetToolSideFromEnum(enToolSide);
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ToolOnArc", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = GetToolOnArcFromEnum(enToolUseOnArc);
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "BridgeMode", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (bBridgeMode ? "YES" : "NO");
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "BridgeModeUser", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (bBridgeModeUser ? "YES" : "NO");
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "BridgeModeDefault", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (bBridgeModeDefault ? "YES" : "NO");
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "NoCutPolygon", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (bNoCutPolygon ? "YES" : "NO");
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "BlindHole", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (bBlindHole ? "YES" : "NO");
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "HoleBoundary", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = (bHoleBoundary ? "YES" : "NO");
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ToolSideOnGroove", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = GetToolSideFromEnum(enToolSideOnGroove);
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            {
                XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, "ArcInterruptsDiskOnAdjacentSegments", ""); ;
                XmlAttribute attr = doc.CreateAttribute("value");
                attr.Value = GetArcInterruptsDiskOnAdjacentSegmentsFromEnum(enArcInterruptsDiskOnAdjacentSegments);
                attributeValue.Attributes.Append(attr);
                attributes.AppendChild(attributeValue);
            }

            if(xExternalCamParameters != null)
            {
                var imported = baseNode.OwnerDocument.ImportNode(xExternalCamParameters, true);//20160511
                attributes.AppendChild(imported);
            }

            baseNode.AppendChild(attributes);

            if (lsTools.Count > 0)
            {
                doc = baseNode.OwnerDocument;

                attributes = doc.CreateNode(XmlNodeType.Element, "TecnoPathTools", ""); ;

                foreach (var ti in lsTools)
                {
                    ti.Value.WriteFileXml(attributes);
                }

                baseNode.AppendChild(attributes);
            }

            if (lsToolsWorkingInfo.Count > 0)
                lsToolsWorkingInfo.WriteFileXml(baseNode);

            if (clToolsLeadMode.Count > 0)
                clToolsLeadMode.WriteFileXml(baseNode);
        }

        #endregion

        #region Private Methods

        private EN_TOOL_SIDE GetToolSideFromString(string szToolSide)
        {
            for (int i = (int)EN_TOOL_SIDE.EN_TOOL_SIDE_CENTER; i < (int)EN_TOOL_SIDE.EN_TOOL_SIDE_NUM_OF; i++)
            {
                EN_TOOL_SIDE en = (EN_TOOL_SIDE)i;

                if (szToolSide == en.ToString())
                    return en;
            }
            return EN_TOOL_SIDE.EN_TOOL_SIDE_CENTER;
        }

        private string GetToolSideFromEnum( EN_TOOL_SIDE enToolSide )
        {
            return  enToolSide.ToString();
        }

        private EN_TOOL_USE_ON_ARC GetToolOnArcFromString(string szToolOnArc)
        {
            for (int i = (int)EN_TOOL_USE_ON_ARC.ALWAYS_DISK; i < (int)EN_TOOL_USE_ON_ARC.NUM_OF; i++)
            {
                EN_TOOL_USE_ON_ARC en = (EN_TOOL_USE_ON_ARC)i;

                if (szToolOnArc == en.ToString())
                    return en;
            }
            return EN_TOOL_USE_ON_ARC.ALWAYS_DISK;
        }

        private string GetToolOnArcFromEnum(EN_TOOL_USE_ON_ARC enToolOnArc)
        {
            return enToolOnArc.ToString();
        }

        private EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS GetArcInterruptsDiskOnAdjacentSegmentsFromString(string szArcInterruptsDiskOnAdjacentSegments)
        {
            try
            {
                var parsed = System.Enum.Parse(typeof(EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS), szArcInterruptsDiskOnAdjacentSegments);
                return (EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS)parsed;
            }
            catch(Exception ex)
            {

            }
            return EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS.NONE;
        }

        private string GetArcInterruptsDiskOnAdjacentSegmentsFromEnum(EN_ARC_INTERRUPTS_DISK_ON_ADJACENT_SEGMENTS enArcInterruptsDiskOnAdjacentSegments)
        {
            return enArcInterruptsDiskOnAdjacentSegments.ToString();
        }

        #endregion
    }
}
