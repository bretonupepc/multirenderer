using System;

namespace Breton.MathUtils
{
	public class MathUtil
	{
		public enum RotationSense { CW = -1, ALIGNED = 0, CCW = 1};

		public struct DBL_POINT
		{
			public double dx;
			public double dy;
		}

		public struct DBL_LINE
		{
			public DBL_POINT dptPunto1;
			public DBL_POINT dptPunto2;
		}

		public struct DBL_CLINE
		{
			public double a;
			public double b;
			public double c;
		}

		public struct DBL_RECT
		{
			public double dLeft;
			public double dTop;
			public double dRight;
			public double dBottom;
		}

		//struttura dati per l'arco
		public struct DBL_ARC
		{
			public int nTipo;			// Tipo elemento: 1=linea/2=arco
			public DBL_POINT pt1;		// Punto 1
			public DBL_POINT pt2;		// Punto 2
			public int nDir;			// Direzione arco: 1=antiorario/-1=orario
			public DBL_POINT PtC;	    // Centro
			public double dr;			// Raggio
			public double dAng;			// Angolo sotteso dall'arco
			public double dStartAng;	// Angolo di partenza
			public double dEndAng;		// Angolo di arrivo
		}

		// struttura dati per l'ellisse
		public struct DBL_ELL
		{
			public int nTipo;			// Tipo elemento: 1=linea/2=arco
			public DBL_POINT pt1;		// Punto 1
			public DBL_POINT pt2;		// Punto 2
			public int nDir;			// Direzione ellisse: 1=antiorario/-1=orario
			public DBL_POINT PtC;	    // Centro
			public double dra;			// Raggio a
			public double drb;			// Raggio b
			public double dInc;			// Inclinazione
			public double dAng;			// Angolo sotteso dall'arco
			public double dStartAng;	// Angolo di partenza
			public double dEndAng;		// Angolo di arrivo
		}


		public const double gdCONV_INCHES = 25.4;
		public const double gdDBLMAX = 1.79769313486232E+307;
		public const double gdDBLMIN = 2.2250738585072E-307;
		public const double gdDBLEPSILON = 2.22044604925031E-16;
		public const float gdFLTMAXe = 3.402823466E+38F;
		public const float gdFLTMIN = 1.175494351E-38F;

		public const float FLT_EPSILON = 1.192092896E-07F;
		public const double QUOTE_EPSILON = 0.0001;
		public const double RAD_EPSILON =  0.00001; //(< 0.001�)

		public MathUtil()
		{
		}

		//**************************************************************************
		// bool gEqual
		// Verifica se due numeri sono uguali.
		// Parametri:
		//           val1 : primo valore da controllare
		//           val2 : secondo valore da controllare
		// Restituisce:
		//           True: se i due valori sono uguali
		//**************************************************************************
		public static bool  gEqual(double val1, double val2)
		{
			double diff;

			diff = Math.Abs(val1 - val2);
			if (diff <= FLT_EPSILON)	return true;
			else						return false;
		}

		/// **************************************************************************
		/// <summary>
		/// Calcola l'equazione cartesiana di una retta passante per 2 punti.
		/// </summary>
		/// <param name="x1">Ascissa punto 1</param>
		/// <param name="y1">Ordinata punto 1</param>
		/// <param name="x2">Ascissa punto 2</param>
		/// <param name="y2">Ordinata punto 2</param>
		/// <param name="a">coefficiente variabile X della retta calcolato</param>
		/// <param name="b">coefficiente variabile Y della retta calcolato</param>
		/// <param name="c">coefficiente costante della retta calcolato</param>
		/// **************************************************************************
		public static void RettaPer2Punti(double x1, double y1, double x2, double y2,
										  out double a, out double b, out double c)
		{
			//Calcola coefficiente variabile X
			if (QuoteIsEQ(y2, y1))
				a = 0d;
			else
				a = y2 - y1;
        
			//Calcola coefficiente variabile Y
			if (QuoteIsEQ(x2, x1))
				b = 0d;
			else
				b = -(x2 - x1);  
    
			//Calcola coefficiente costante
			c = -x1 * a - y1 * b;
		}

		/// **************************************************************************
		/// <summary>
		/// Calcola l'equazione in forma implicita (ax + by + c = 0) di una retta 
		/// passante per un punto con un angolo dato.
		/// </summary>
		/// <param name="x">ascissa punto</param>
		/// <param name="y">ordinata punto</param>
		/// <param name="ang">angolo della retta (radianti)</param>
		/// <param name="a">coefficiente 'a' della retta in forma implicita</param>
		/// <param name="b">coefficiente 'b' della retta in forma implicita</param>
		/// <param name="c">coefficiente 'c' della retta in forma implicita</param>
		/// **************************************************************************
		public static void PointAngleStraight(double x, double y, double ang,
			out double a, out double b, out double c)
		{
			double sin, cos;

			// Calcola coefficiente 'a'
			sin = Math.Sin(ang);
			a = sin;

			// Calcola coefficiente 'b'
			cos = Math.Cos(ang);
			b = -cos;

			// Calcola coefficiente 'c'
			c = -x * sin + y * cos;
		}

		/// **************************************************************************
		/// <summary>
		/// Calcola l'equazione cartesiana di una retta parallela ad una data,
		/// distante da questa un valore fissato.
		/// </summary>
		/// <param name="rdA">coefficiente variabile X retta data</param>
		/// <param name="rdB">coefficiente variabile Y retta data</param>
		/// <param name="rdC">coefficiente costante retta data</param>
		/// <param name="rdD">distanza fra le due rette</param>
		/// <param name="rdK1">coefficiente costante 1 retta parallela calcolato</param>
		/// <param name="rdK2">coefficiente costante 2 retta parallela calcolato</param>
		/// **************************************************************************
		public static void gRettaParallela(double rdA, double rdB, double rdC,
			double rdD, out double rdK1, out double rdK2)
		{
			double dTmp;

			dTmp = rdD * Math.Sqrt(rdA * rdA + rdB * rdB);

			//Calcola coefficienti costanti
			rdK1 = rdC - dTmp;
			rdK2 = rdC + dTmp;
		}

        /// **************************************************************************
        /// <summary>
        /// Calcola l'equazione cartesiana di una retta parallela ad una data,
        /// passante per un punto.
        /// </summary>
        /// <param name="rdA">coefficiente variabile X retta data</param>
        /// <param name="rdB">coefficiente variabile Y retta data</param>
        /// <param name="rdC">coefficiente costante retta data</param>
        /// <param name="x">ascissa del punto di passaggio</param>
        /// <param name="y">ordinata del punto di passaggio</param>
        /// <param name="a">coefficiente variabile X della retta calcolato</param>
        /// <param name="b">coefficiente variabile Y della retta calcolato</param>
        /// <param name="c">coefficiente costante della retta calcolato</param>
        /// **************************************************************************
        public static void gRettaParallelaPerPunto(double rdA, double rdB, double rdC,
            double x, double y, out double a, out double b, out double c)
        {
            a = rdA;
            b = rdB;
            c = -(rdA * x + rdB * y);
        }

        //**************************************************************************
		// Function StraightLineAngle
		// Calcola l'inclinazione di una retta rispetto all'asse delle ascisse
		// Parametri:
		//           a   : coefficiente variabile X della retta
		//           b   : coefficiente variabile Y della retta
		//           c   : coefficiente costante della retta
		// Restituisce:
		//           inclinazione della retta in gradi
		//**************************************************************************
		public static double StraightLineAngle(double a, double b, double c)
		{             
            if (IsZero(a))
				return 0d;
			else if (IsZero(b))
				return Math.PI / 2d;
			else
				return Math.Atan(-a / b);
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se due rette sono parallele
		/// </summary>
		/// <param name="a1">coefficiente 'a' della retta 1 in forma implicita</param>
		/// <param name="b1">coefficiente 'b' della retta 1 in forma implicita</param>
		/// <param name="c1">coefficiente 'c' della retta 1 in forma implicita</param>
		/// <param name="a2">coefficiente 'a' della retta 2 in forma implicita</param>
		/// <param name="b2">coefficiente 'b' della retta 2 in forma implicita</param>
		/// <param name="c2">coefficiente 'c' della retta 2 in forma implicita</param>
		/// <returns>true se le due rette sono parallele, false altrimenti</returns>
		/// **************************************************************************
		public static bool IsParallelStraight(double a1, double b1, double c1, double a2, double b2, double c2)
		{
			double incl1, incl2;
		    
			// Calcola l'inclinazione della retta 1
			incl1 = StraightLineAngle(a1, b1, c1);
		    
			// Normalizza l'angolo fra 0 e PI
			incl1 = Angolo_0_PI(incl1);
		    
			// Calcola l'inclinazione della retta 2
			incl2 = StraightLineAngle(a2, b2, c2);
		    
			// Normalizza l'angolo fra 0 e PI
			incl2 = Angolo_0_PI(incl2);
		    
			// Verifica se le inclinazioni sono uguali
			return (Compare(incl1, incl2, RAD_EPSILON) == 0);
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna la distanza fra due rette parallele
		/// </summary>
		/// <param name="a1">coefficiente 'a' della retta 1 in forma implicita</param>
		/// <param name="b1">coefficiente 'b' della retta 1 in forma implicita</param>
		/// <param name="c1">coefficiente 'c' della retta 1 in forma implicita</param>
		/// <param name="a2">coefficiente 'a' della retta 2 in forma implicita</param>
		/// <param name="b2">coefficiente 'b' della retta 2 in forma implicita</param>
		/// <param name="c2">coefficiente 'c' della retta 2 in forma implicita</param>
		/// <returns>
		///			>= 0	distanza fra le due rette
		///			-1		le due rette non sono parallele
		/// </returns>
		/// **************************************************************************
		public static double ParallelStraightDistance(double a1, double b1, double c1, double a2, double b2, double c2)
		{
			double x, y;
			
			// Verifica se le due rette non sono parallele
		    if (!IsParallelStraight(a1, b1, c1, a2, b2, c2))
		        return -1;

			// Calcola un punto sulla retta 2
			if (IsZero(b2))
			{
				y = 0d;
				x = -(b2 * y + c2) / a2;
			}
			else
			{
				x = 0d;
				y = -(a2 * x + c2) / b2;
			}

			// Ritorna la distanza del punto dalla retta 1
			return Point2StraightDistance(a1, b1, c1, x, y);
		}

		/// **************************************************************************
		/// <summary>
		/// Calcola la distanza tra un punto e la retta
		/// </summary>
		/// <param name="a">coefficiente 'a' della retta in forma implicita</param>
		/// <param name="b">coefficiente 'b' della retta in forma implicita</param>
		/// <param name="c">coefficiente 'c' della retta in forma implicita</param>
		/// <param name="x">coordinata x del punto</param>
		/// <param name="y">coordinata y del punto</param>
		/// <returns>distanza punto-retta</returns>
		/// **************************************************************************
		public static double Point2StraightDistance(double a, double b, double c, double x, double y)
		{
			return Math.Abs(a * x + b * y + c) / Math.Sqrt(a * a + b * b);
		}

		/// **************************************************************************
		/// <summary>
		/// Calcola la retta passante per un punto e perpendicolare alla retta data
		/// </summary>
		/// <param name="a1">coefficiente 'a' della retta in forma implicita</param>
		/// <param name="b1">coefficiente 'b' della retta in forma implicita</param>
		/// <param name="c1">coefficiente 'c' della retta in forma implicita</param>
		/// <param name="x">coordinata x del punto</param>
		/// <param name="y">coordinata y del punto</param>
		/// <param name="a2">coefficiente 'a' della retta calcolata in forma implicita</param>
		/// <param name="b2">coefficiente 'b' della retta calcolata in forma implicita</param>
		/// <param name="c2">coefficiente 'c' della retta calcolata in forma implicita</param>
		/// **************************************************************************
		public static void PerpendicularStraight(double a1, double b1, double c1, double x, double y,
			out double a2, out double b2, out double c2)
		{
			a2 = b1;
			b2 = -a1;
			c2 = a1 * y - b1 * x;
		}

        /// **************************************************************************
        /// <summary>
        /// Calcola la proiezione di un punto su una retta
        /// </summary>
        /// <param name="a1">coefficiente 'a' della retta in forma implicita</param>
        /// <param name="b1">coefficiente 'b' della retta in forma implicita</param>
        /// <param name="c1">coefficiente 'c' della retta in forma implicita</param>
        /// <param name="x">coordinata x del punto</param>
        /// <param name="y">coordinata y del punto</param>
        /// <param name="xp">coordinata x del punto proiettato</param>
        /// <param name="yp">coordinata y del punto proiettato</param>
        /// **************************************************************************
        public static void ProjectedPointToStraight(double a1, double b1, double c1, double x, double y,
            out double xp, out double yp)
        {
            double a2, b2, c2;
            PerpendicularStraight( a1, b1, c1, x, y, out a2, out b2, out c2);
            gbIntersFra2Rette(a1, b1, c1, a2, b2, c2, out xp, out yp);
        }

        /// **************************************************************************
		/// <summary>
		/// Verifica l'intersezione di due rette
		/// </summary>
		/// <param name="rdA1">coefficiente variabile rdX retta 1</param>
		/// <param name="rdB1">coefficiente variabile rdY retta 1</param>
		/// <param name="rdC1">coefficiente costante retta 1</param>
		/// <param name="rdA2">coefficiente variabile rdX retta 2</param>
		/// <param name="rdB2">coefficiente variabile rdY retta 2</param>
		/// <param name="rdC2">coefficiente costante retta 2</param>
		/// <param name="rdX">Ascissa del punto di intersezione calcolato</param>
		/// <param name="rdY">Ordinata del punto di intersezione calcolato</param>
		/// <returns>
		///		true	le due rette si intersecano
		///		false	le due rette sono parallele o coincidenti
		///	</returns>
		/// **************************************************************************
		public static bool gbIntersFra2Rette(double rdA1, double rdB1, double rdC1, double rdA2, 
			double rdB2, double rdC2, out double rdX, out double rdY)
		{
			double dDet;

			rdX = rdY = 0;

			//Calcola il determinante della matrice principale
			dDet = rdA1 * rdB2 - rdA2 * rdB1;
    
			//Verifica se le rette sono parallele
			if (IsZero(dDet))	return false;
    
			//Calcola il punto di intersezione
			rdX = (-rdC1 * rdB2 + rdB1 * rdC2) / dDet;
			rdY = (-rdA1 * rdC2 + rdC1 * rdA2) / dDet;
    
			//Intersezione trovata
			return true;
		}    

		//**************************************************************************
		// double gdRadianti
		// Trasforma un angolo da gradi a radianti
		// Parametri:
		//           rdGradi   : Valore dell'angolo in gradi
		// Restituisce:
		//           valore dell'angolo in radianti
		//**************************************************************************
		public static double gdRadianti(double rdGradi)
		{
			return  (rdGradi * Math.PI / 180);
		}

		//**************************************************************************
		// double gdGradi
		// Trasforma un angolo da radianti a gradi
		// Parametri:
		//           rdRadianti    : Valore dell'angolo in radianti
		// Restituisce:
		//           valore dell'angolo in gradi
		//**************************************************************************
		public static double gdGradi(double rdRadianti)
		{
			return (rdRadianti * 180 / Math.PI);
		}

		//**************************************************************************
		// double gdArcSin
		// Calcola l'angolo corrispondente ad un seno
		// Ritorna:
		//           valore dell'angolo espresso in radianti
		// Parametri:
		//           rdX   : Seno dell'angolo
		//**************************************************************************
		public static double gdArcSin(double rdX)
		{
			if (rdX >= 1)		return (Math.PI / 2);
			else if (rdX <= -1)	return (-Math.PI / 2);
			else			    return Math.Atan(rdX / Math.Sqrt(-rdX * rdX + 1));
		}

		//**************************************************************************
		// double gdArcCos
		// Calcola l'angolo corrispondente ad un coseno
		// Ritorna:
		//           valore dell'angolo espresso in radianti
		// Parametri:
		//           rdX   : Seno dell'angolo
		//**************************************************************************
		public static double gdArcCos(double rdX)
		{
			if (rdX >= 1)		return 0;
			else if (rdX <= -1)	return Math.PI;
			else				return Math.Atan(-rdX / Math.Sqrt(-rdX * rdX + 1)) + Math.PI / 2;
		}

		//**************************************************************************
		// double SegmentAngle
		// Calcola l'inclinazione di un segmento rispetto all'asse delle ascisse
		// Parametri:
		//           rdX1  : Ascissa del punto 1 del segmento
		//           rdY1  : Ordinata del punto 1 del segmento
		//           rdX2  : Ascissa del punto 2 del segmento
		//           rdY2  : Ordinata del punto 2 del segmento
		// Restituisce:
		//           inclinazione del segmento in gradi
		//**************************************************************************
		public static double SegmentAngle(double x1, double y1, double x2, double y2)
		{
			double a, b, c;

			//Calcola la retta passante per i due estremi del lato
			RettaPer2Punti(x1, y1, x2, y2, out a, out b, out c);
		    
			//Calcola l'inclinazione della retta
			return StraightLineAngle(a, b, c);
		}


		#region Comparisons

		//**************************************************************************
		// Compare
		// Confronta due numeri double, tenendo conto di un errore minimo
		// Parametri:
		//			val1	: Valore 1
		//			val2	: Valore 2
		//			epsilon	: errore minimo
		// Ritorna:
		//           0   val1 = val2
		//          -1   val1 < val2
		//           1   val1 > val2
		//**************************************************************************
		public static int Compare(double val1, double val2)
		{
			return Compare(val1, val2, FLT_EPSILON);
		}
		public static int Compare(double val1, double val2, double epsilon)
		{
			if ((val1 - val2) < -epsilon)
				return -1;

			if ((val1 - val2) > epsilon)
				return 1;
			
			return 0;
		}
		//**************************************************************************
		// IsEQ
		// Ritorna true se val1 == val2
		//**************************************************************************
		public static bool IsEQ(double val1, double val2)
		{
			return Compare(val1, val2, FLT_EPSILON) == 0;
		}
		//**************************************************************************
		// IsLT
		// Ritorna true se val1 < val2
		//**************************************************************************
		public static bool IsLT(double val1, double val2)
		{
			return Compare(val1, val2, FLT_EPSILON) < 0;
		}
		//**************************************************************************
		// IsLE
		// Ritorna true se val1 < val2
		//**************************************************************************
		public static bool IsLE(double val1, double val2)
		{
			return Compare(val1, val2, FLT_EPSILON) <= 0;
		}
		//**************************************************************************
		// IsGT
		// Ritorna true se val1 > val2
		//**************************************************************************
		public static bool IsGT(double val1, double val2)
		{
			return Compare(val1, val2, FLT_EPSILON) > 0;
		}
		//**************************************************************************
		// IsGE
		// Ritorna true se val1 > val2
		//**************************************************************************
		public static bool IsGE(double val1, double val2)
		{
			return Compare(val1, val2, FLT_EPSILON) >= 0;
		}
		//**************************************************************************
		// IsZero
		// Ritorna true se val == 0
		//**************************************************************************
		public static bool IsZero(double val)
		{
			return Compare(val, 0d, FLT_EPSILON) == 0;
		}

		#endregion


		#region Quote Comparisons

		/// **************************************************************************
		/// <summary>
		/// Confronta due quote, tenendo conto di un errore minimo
		/// </summary>
		/// <param name="val1">Valore quota 1</param>
		/// <param name="val2">Valore quota 2</param>
		/// <returns>
		///           0   val1 uguale val2
		///          -1   val1 minore di val2
		///           1   val1 maggiore di val2
		/// </returns>
		/// **************************************************************************
		public static int QuoteCompare(double val1, double val2)
		{
			return Compare(val1, val2, QUOTE_EPSILON);
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se due quote sono uguali
		/// </summary>
		/// <param name="val1">quota 1</param>
		/// <param name="val2">quota 2</param>
		/// <returns>true se sono uguali</returns>
		/// **************************************************************************
		public static bool QuoteIsEQ(double val1, double val2)
		{
			return Compare(val1, val2, QUOTE_EPSILON) == 0;
		}
		public static bool QuoteIsEQ(double val1, double val2, double epsilon)
		{
            return Compare(val1, val2, epsilon) == 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se la prima quota � minore della seconda
		/// </summary>
		/// <param name="val1">quota 1</param>
		/// <param name="val2">quota 2</param>
		/// <returns>true se sono val1 minore di val2</returns>
		/// **************************************************************************
		public static bool QuoteIsLT(double val1, double val2)
		{
			return Compare(val1, val2, QUOTE_EPSILON) < 0;
		}
        public static bool QuoteIsLT(double val1, double val2, double epsilon)
        {
            return Compare(val1, val2, epsilon) < 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se la prima quota � minore o uguale della seconda
		/// </summary>
		/// <param name="val1">quota 1</param>
		/// <param name="val2">quota 2</param>
		/// <returns>true se sono val1 minore o uguale di val2</returns>
		/// **************************************************************************
		public static bool QuoteIsLE(double val1, double val2)
		{
			return Compare(val1, val2, QUOTE_EPSILON) <= 0;
		}
        public static bool QuoteIsLE(double val1, double val2, double epsilon)
		{
            return Compare(val1, val2, epsilon) <= 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se la prima quota � maggiore della seconda
		/// </summary>
		/// <param name="val1">quota 1</param>
		/// <param name="val2">quota 2</param>
		/// <returns>true se sono val1 maggiore di val2</returns>
		/// **************************************************************************
		public static bool QuoteIsGT(double val1, double val2)
		{
			return Compare(val1, val2, QUOTE_EPSILON) > 0;
		}
        public static bool QuoteIsGT(double val1, double val2, double epsilon)
		{
            return Compare(val1, val2, epsilon) > 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se la prima quota � maggiore o uguale della seconda
		/// </summary>
		/// <param name="val1">quota 1</param>
		/// <param name="val2">quota 2</param>
		/// <returns>true se sono val1 maggiore o uguale di val2</returns>
		/// **************************************************************************
		public static bool QuoteIsGE(double val1, double val2)
		{
			return Compare(val1, val2, QUOTE_EPSILON) >= 0;
		}
        public static bool QuoteIsGE(double val1, double val2, double epsilon)
		{
            return Compare(val1, val2, epsilon) >= 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se la quota � uguale a 0
		/// </summary>
		/// <param name="val">quota 1</param>
		/// <returns>true se val � uguale a 0</returns>
		/// **************************************************************************
		public static bool QuoteIsZero(double val)
		{
			return Compare(val, 0d, QUOTE_EPSILON) == 0;
		}
        public static bool QuoteIsZero(double val, double epsilon)
		{
            return Compare(val, 0d, epsilon) == 0;
		}
        /// **************************************************************************
        /// <summary>
        /// Verifica se due quote sono diverse
        /// </summary>
        /// <param name="val1">quota 1</param>
        /// <param name="val2">quota 2</param>
        /// <returns>true se sono diverse</returns>
        /// **************************************************************************
        public static bool QuoteIsNEQ( double val1, double val2 )
        {
            return !QuoteIsEQ( val1, val2 );
        }
        public static bool QuoteIsNEQ(double val1, double val2, double epsilon)
        {
            return !QuoteIsEQ(val1, val2, epsilon);
        }

        public static bool QuoteIsBetween(double val, double min, double max, double epsilon = QUOTE_EPSILON)
        {
            return QuoteIsGT(val, min, epsilon) && QuoteIsLT(val, max, epsilon);
        }

        public static bool QuoteIsBetweenOrEq(double val, double min, double max, double epsilon = QUOTE_EPSILON)
        {
            return QuoteIsGE(val, min, epsilon) && QuoteIsLE(val, max, epsilon);
        }

        #endregion


		#region Radiants Comparisons

		/// **************************************************************************
		/// <summary>
		/// Confronta due angoli, tenendo conto di un errore minimo
		/// </summary>
		/// <param name="ang1">Valore angolo 1</param>
		/// <param name="ang2">Valore angolo 2</param>
		/// <returns>
		///           0   ang1 uguale ang2
		///          -1   ang1 minore di ang2
		///           1   ang1 maggiore di ang2
		/// </returns>
		/// **************************************************************************
		public static int AngleCompare(double ang1, double ang2)
		{
			return Compare(ang1, ang2, RAD_EPSILON);
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se due angoli sono uguali
		/// </summary>
		/// <param name="ang1">angolo 1</param>
		/// <param name="ang2">angolo 2</param>
		/// <returns>true se sono uguali</returns>
		/// **************************************************************************
		public static bool AngleIsEQ(double ang1, double ang2)
		{
			return Compare(ang1, ang2, RAD_EPSILON) == 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se il primo angolo � minore del secondo
		/// </summary>
		/// <param name="ang1">angolo 1</param>
		/// <param name="ang2">angolo 2</param>
		/// <returns>true se sono ang1 minore di ang2</returns>
		/// **************************************************************************
		public static bool AngleIsLT(double ang1, double ang2)
		{
			return Compare(ang1, ang2, RAD_EPSILON) < 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se il primo angolo � minore o uguale del secondo
		/// </summary>
		/// <param name="ang1">angolo 1</param>
		/// <param name="ang2">angolo 2</param>
		/// <returns>true se sono ang1 minore o uguale di ang2</returns>
		/// **************************************************************************
		public static bool AngleIsLE(double ang1, double ang2)
		{
			return Compare(ang1, ang2, RAD_EPSILON) <= 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se il primo angolo � maggiore del secondo
		/// </summary>
		/// <param name="ang1">angolo 1</param>
		/// <param name="ang2">angolo 2</param>
		/// <returns>true se sono ang1 maggiore di ang2</returns>
		/// **************************************************************************
		public static bool AngleIsGT(double ang1, double ang2)
		{
			return Compare(ang1, ang2, RAD_EPSILON) > 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se il primo angolo � maggiore o uguale del secondo
		/// </summary>
		/// <param name="ang1">angolo 1</param>
		/// <param name="ang2">angolo 2</param>
		/// <returns>true se sono ang1 maggiore o uguale di ang2</returns>
		/// **************************************************************************
		public static bool AngleIsGE(double ang1, double ang2)
		{
			return Compare(ang1, ang2, RAD_EPSILON) >= 0;
		}
		/// **************************************************************************
		/// <summary>
		/// Verifica se l'angolo � uguale a 0
		/// </summary>
		/// <param name="val">angolo 1</param>
		/// <returns>true se val � uguale a 0</returns>
		/// **************************************************************************
		public static bool AngleIsZero(double val)
		{
			return Compare(val, 0d, RAD_EPSILON) == 0;
		}
        /// **************************************************************************
        /// <summary>
        /// Verifica se due angoli sono diversi
        /// </summary>
        /// <param name="ang1">angolo 1</param>
        /// <param name="ang2">angolo 2</param>
        /// <returns>true se sono diversi</returns>
        /// **************************************************************************
        public static bool AngleIsNEQ( double ang1, double ang2 )
        {
            return !AngleIsEQ( ang1, ang2 );
        }
        #endregion


		//**************************************************************************
		// int gnIntersect
		// Dati due segmenti, determina se si intersecano.
		// Parametri:
		//           rdptP1  : estremo 1 segmento 1
		//           rdptP2  : estremo 2 segmento 1
		//           rdptP3  : estremo 1 segmento 2
		//           rdptP4  : estremo 2 segmento 2
		// Ritorna:
		//       bit 0   (&H1)   intersezione fra i segmenti
		//       bit 1   (&H2)   segmenti giacenti sulla stessa retta
		//       bit 2   (&H4)   intersezione nel punto 1
		//       bit 3   (&H8)   intersezione nel punto 2
		//       bit 4   (&H10)  intersezione nel punto 3
		//       bit 5   (&H20)  intersezione nel punto 4
        // 26/03/2015 GM Corretto il risultato nel caso di intersezione nei punti 3 e 4
		//**************************************************************************
		public static int gnIntersect(DBL_POINT rdptP1, DBL_POINT rdptP2, DBL_POINT rdptP3, DBL_POINT rdptP4)
		{
			int nRis1,nRis2,nRis3,nRis4;
			int nMolt1,nMolt2;
			DBL_RECT DblRect;
			int retValue;
 
			nRis1 = (int) CCW(rdptP3, rdptP4, rdptP1);
			nRis2 = (int) CCW(rdptP3, rdptP4, rdptP2);
			nRis3 = (int) CCW(rdptP1, rdptP2, rdptP3);
			nRis4 = (int) CCW(rdptP1, rdptP2, rdptP4);
			nMolt1 = nRis1 * nRis2;
			nMolt2 = nRis3 * nRis4;
    
			retValue=0;
    
				//Intersezione piena
				if (nMolt1 < 0 && nMolt2 < 0)			retValue=1;  
					//Nessuna intersezione
				else if (nMolt1 != 0 && nMolt2 != 0)	retValue=0;
				else
				{
					//Intersezione sul vertice 1
					if (nRis1 == 0 && nRis2 != 0)		retValue=retValue | 1 | 4;
						//Intersezione sul vertice 2
					else if (nRis1 != 0 && nRis2 == 0)	retValue=retValue | 1 | 8;
        
					//Intersezione sul vertice 3
					if (nRis3 == 0 && nRis4 != 0)       retValue=retValue | 1 | 0x10;
						//Intersezione sul vertice 4
					else if (nRis3 != 0 && nRis4 == 0)  retValue=retValue | 1 | 0x20;
        
					//Segmenti posti sulla stessa retta
					if (nRis1 == 0 && nRis2 == 0)
					{        
						//Segmenti paralleli
						retValue=2;
						DblRect = gDblRectDefine(rdptP3.dx, rdptP3.dy, rdptP4.dx, rdptP4.dy);
						if (gbPointInRect(rdptP1, DblRect))	retValue=retValue | 1 | 4;
						if (gbPointInRect(rdptP2, DblRect))   retValue=retValue | 1 | 8;
                        
						DblRect = gDblRectDefine(rdptP1.dx, rdptP1.dy, rdptP2.dx, rdptP2.dy);
						if (gbPointInRect(rdptP3, DblRect))	retValue=retValue | 1 | 0x10;
						if (gbPointInRect(rdptP4, DblRect))	retValue=retValue | 1 | 0x20;
					}
				}
			return retValue;
		}

		//**************************************************************************
		// CCW (CounterClockWise)
		// Determina, dati tre punti, se viaggiando dal primo al secondo al terzo,
		// viaggiamo in senso antiorario.
		// Parametri:
		//           rdptP0  : punto 0
		//           rdptP1  : punto 1
		//           rdptP2  : punto 2
		// Ritorna:
		//       1   movimento in senso antiorario
		//       0   punti allineati
		//       -1  movimento in senso orario
		//*************************************************************************
		public static RotationSense CCW(DBL_POINT rdptP0, DBL_POINT rdptP1, DBL_POINT rdptP2)
		{
			return CCW(rdptP0, rdptP1, rdptP2, 0.001);
		}
		public static RotationSense CCW(DBL_POINT rdptP0, DBL_POINT rdptP1, DBL_POINT rdptP2, double epsilon)
		{
			double dDist;
			double dBase;
			double dProdVett;

			//Calcola il prodotto vettoriale tra i due vettori
			dProdVett = ((rdptP1.dx - rdptP0.dx) * (rdptP2.dy - rdptP0.dy))
				- ((rdptP1.dy - rdptP0.dy) * (rdptP2.dx - rdptP0.dx));
    
			dBase = Math.Sqrt((rdptP1.dx - rdptP0.dx) * (rdptP1.dx - rdptP0.dx)) 
							+ ((rdptP1.dy - rdptP0.dy) * (rdptP1.dy - rdptP0.dy));
      
			// Il prodotto vettoriale di due vettori sul piano da come risultato
			// un vettore perpendicolare al piano stesso. Il modulo del vettore
			// risultante rappresenta l'area del parallelogramma formato dai due
			// vettori. Dividendo l'area per il modulo del vettore 1 , si ottiene
			// l'altezza del parallelogrammo che ha come base il vettore 1. Questa
			// altezza e` la distanza del punto appartenente al vettore 2 dal
			// vettore 1.
       
			//Vettore non nullo
			if (dBase != 0)
			{
				//Calcola la distanza
				dDist = dProdVett / dBase;
        
				//Senso antiorario
				if (dDist >= epsilon)			return RotationSense.CCW;
				//Senso orario
				else if (dDist <= -epsilon)		return RotationSense.CW;
				//Punti allineati
			else						return RotationSense.ALIGNED;
			}
			//Vettore nullo
			else						return RotationSense.ALIGNED;	//Punti allineati
		}

		//**************************************************************************
		// DBL_RECT gDblRectDefine
		// Definisce un rettangolo
		// Parametri:
		//           rdX1    : ascissa punto 1
		//           rdY1    : ordinata punto 1
		//           rdX2    : ascissa punto 2
		//           rdY2    : ordinata punto 2
		// Ritorna:
		//       Rettangolo
		//*************************************************************************
		public static DBL_RECT gDblRectDefine(double rdX1, double rdY1, double rdX2, double rdY2)
		{
			DBL_RECT retValue;
			//Ordina le ascisse
			if (rdX1 <= rdX2)
			{
				retValue.dLeft = rdX1;
				retValue.dRight = rdX2;
			}
			else
			{
				retValue.dLeft = rdX2;
				retValue.dRight = rdX1;
			}

			//Ordina le ordinate
			if (rdY1 <= rdY2)
			{
				retValue.dTop = rdY1;
				retValue.dBottom = rdY2;
			}
			else
			{
				retValue.dTop = rdY2;
				retValue.dBottom = rdY1;
			}
			return retValue;
		}

		//**************************************************************************
		// bool gbPointInRect
		// Determina, se un punto e` contenuto in un rettangolo.
		// Parametri:
		//           rdptP       : punto
		//           rDblRect    : rettangolo
		// Ritorna:
		//       TRUE    punto interno al rettangolo
		//       FALSE   punto esterno al rettangolo
		//*************************************************************************
		public static bool gbPointInRect(DBL_POINT rdptP, DBL_RECT rDblRect)
		{    
			return (rdptP.dx >= rDblRect.dLeft && rdptP.dx <= rDblRect.dRight 
				&& rdptP.dy >= rDblRect.dTop && rdptP.dy <= rDblRect.dBottom);
		}

		//**************************************************************************
		// double gdNormAngolo
		// Normalizza un angolo da -90 a 90
		// Parametri:
		//           rdAng : Angolo da normalizzare (in gradi)
		// Ritorna:
		//           Valore angolo normalizzato (in gradi)
		//**************************************************************************
		public static double gdNormAngolo(double rdAng)
		{
			if (Compare(rdAng, -90) == -1)		return (rdAng + 180);
			else if (Compare(rdAng, 90) == 1)	return (rdAng - 180);
			else											return rdAng;
		}

		//**************************************************************************
		// double gdDistanzaTra2Punti
		// Calcola la distanza tra 2 punti
		// Parametri:
		//           rdX1  : Ascissa punto 1
		//           rdY1  : Ordinata punto 1
		//           rdX2  : Ascissa punto 2
		//           rdY2  : Ordinata punto 2
		// Restituisce:
		//           la distanza
		//**************************************************************************
		public static double gdDistanzaTra2Punti(double rdX1, double rdY1, double rdX2, double rdY2)
		{
			return Math.Sqrt((rdX2 - rdX1)*(rdX2 - rdX1) + (rdY2 - rdY1)*(rdY2 - rdY1));
		}

		//**************************************************************************
		// double gdDirezione
		// Calcola la direzione di un vettore
		// Parametri:
		//           rdX1  : Ascissa punto 1
		//           rdY1  : Ordinata punto 1
		//           rdX2  : Ascissa punto 2
		//           rdY2  : Ordinata punto 2
		// Restituisce:
		//           direzione in radianti
		//**************************************************************************
		public static double gdDirezione(double rdX1, double rdY1, double rdX2, double rdY2)
		{
			double dx, dy, atn2;

			dx = rdX2 - rdX1;
			dy = rdY2 - rdY1;

			if (Math.Abs(dx) < 0.0000001)
			{
				if(dy>0)	atn2=Math.PI / 2;
				else		atn2=-Math.PI / 2;
			}
			else
			{
				atn2 = Math.Atan(dy / dx);
				if (dy > 0)
				{
					if (dx < 0)	atn2 = atn2 + Math.PI;
				}
				else
				{
					if (dx < 0)	atn2 = atn2 - Math.PI;
				}
			}

			if (atn2 < 0)				atn2 = atn2 + 2 * Math.PI;

			if (atn2 >= 2 * Math.PI)	atn2 = atn2 - 2 * Math.PI;

			return atn2;
		}

		///****************************************************************************
		/// <summary>
		/// Riporta l'angolo specificato nel range [0..2PI] (2PI incluso).
        /// </summary>
        /// <param name="angolo">angolo da controllare</param>
        ///	<returns>angolo nel range [0..2PI]</returns>
		///****************************************************************************/
		public static double Angolo_0_2PI(double angolo)
		{

			if (!double.IsInfinity(angolo))
			{
				if ((angolo < 0d) || (2d * Math.PI < angolo))
				{
					double a, f, i;

					a = angolo / (2d * Math.PI);
					f = modf(a, out i);

					a = f * (2d * Math.PI) ;

					if (a < 0d)
						a += (2d * Math.PI);

					angolo = a;
				}
			}
			else if (double.IsNegativeInfinity(angolo))
				angolo = -2d * Math.PI;
			else
				angolo = 2d * Math.PI;

			// angolo risultante
			return angolo;
		}

		/// **************************************************************************
		/// <summary>
		/// Riporta l'angolo specificato nel range [0..PI].
		/// </summary>
		/// <param name="angolo">angolo da controllare</param>
		/// <returns>angolo nel range [0..PI]</returns>
		/// **************************************************************************
		public static double Angolo_0_PI(double angolo)
		{
			// Normalizza l'angolo fra 0 e 2PI
			angolo = Angolo_0_2PI(angolo);

			// Normalizza l'angolo fra 0 e PI
			return (angolo < Math.PI ? angolo : angolo - Math.PI);
		}

		/// **************************************************************************
		/// <summary>
		/// Riporta l'angolo specificato nel range [-PI..PI].
		/// </summary>
		/// <param name="angolo">angolo da controllare</param>
		/// <returns>angolo nel range [-PI..PI]</returns>
		/// **************************************************************************
		public static double Angolo_nPI_PI(double angolo)
		{
			// Normalizza l'angolo fra 0 e 2PI
			angolo = Angolo_0_2PI(angolo);

			// Normalizza l'angolo fra -PI e PI
			return (angolo <= Math.PI ? angolo : angolo - 2 * Math.PI);
		}

        ///****************************************************************************
        /// <summary>
        /// Riporta l'angolo specificato nel range [0..2PI) (2PI escluso).
        /// </summary>
        /// <param name="angolo">angolo da controllare</param>
        /// <returns>angolo nel range [0..2PI)</returns>
        ///	26/03/2015 GM Ottimizzato l'algoritmo
        ///****************************************************************************/
        public static double Angolo_0_2PI_Ex(double angolo)
        {
            angolo %= Math.PI * 2.0;
            if (angolo < 0) angolo += Math.PI * 2.0;

            return angolo;
        }

        //****************************************************************************
		// modf
		// Divide un double in parte frazionaria e parte intera
		// Parametri:
		//			x	: numero da calcolare
		//			i	: parte intera di x ritornata
		// Ritorna:
		//			parte frazionaria di x
		/****************************************************************************/
		public static double modf(double x, out double i)
		{
			if (x > 0d)
			{
				i = Math.Floor(x);
				return x - i;
			}
			else if (x < 0d)
			{
				i = Math.Ceiling(x);
				return x - i;
			}
			else
			{
				i = 0d;
				return 0d;
			}
		}

		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero, anche con la virgola
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsDouble(string test)
		{
			double num;
			return IsDouble(test, out num);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero, anche con la virgola in 
		/// Invariant Culture
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsDoubleIC(string test)
		{
			double num;
			return IsDoubleIC(test, out num);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero, anche con la virgola
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">valore convertito</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsDouble(string test, out double conv)
		{
			return IsDouble(test, out conv, System.Globalization.CultureInfo.CurrentCulture);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero, anche con la virgola, in 
		/// Invariant Culture
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">valore convertito</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsDoubleIC(string test, out double conv)
		{
			return IsDouble(test, out conv, System.Globalization.CultureInfo.InvariantCulture);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero, anche con la virgola
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">valore convertito</param>
		/// <param name="ci">tipo di formattazione</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsDouble(string test, out double conv, System.Globalization.CultureInfo ci)
		{
			return double.TryParse(test, System.Globalization.NumberStyles.Float, ci, out conv);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero intero
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsInteger(string test)
		{
			int num;
			return IsInteger(test, out num);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero intero in Invariant Culture
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsIntegerIC(string test)
		{
			int num;
			return IsIntegerIC(test, out num);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero intero
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">valore convertito</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsInteger(string test, out int conv)
		{
			return IsInteger(test, out conv, System.Globalization.CultureInfo.CurrentCulture);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero intero in Invariant Culture
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">valore convertito</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsIntegerIC(string test, out int conv)
		{
			return IsInteger(test, out conv, System.Globalization.CultureInfo.InvariantCulture);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene un numero intero
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">valore convertito</param>
		/// <param name="ci">tipo di formattazione</param>
		/// <returns>
		///		true	la stringa � un numero valido
		///		false	la stringa non � un numero valido
		/// </returns>
		///****************************************************************************
		public static bool IsInteger(string test,out int conv, System.Globalization.CultureInfo ci)
		{
			double num;
			if (double.TryParse(test, System.Globalization.NumberStyles.Integer, ci, out num))
			{
				conv = (int) num;
				return true;
			}
			else
			{
				conv = 0;
				return false;
			}
		}

		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene una data/ora valida
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <returns>
		///		true	la stringa � una data/ora valida
		///		false	la stringa non � una data/ora valida
		/// </returns>
		///****************************************************************************
		public static bool IsDateTime(string test)
		{
			DateTime conv;
			return IsDateTime(test, out conv);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene una data/ora valida in Invariant Culture
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <returns>
		///		true	la stringa � una data/ora valida
		///		false	la stringa non � una data/ora valida
		/// </returns>
		///****************************************************************************
		public static bool IsDateTimeIC(string test)
		{
			DateTime conv;
			return IsDateTimeIC(test, out conv);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene una data/ora valida
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">data/ora convertita</param>
		/// <returns>
		///		true	la stringa � una data/ora valida
		///		false	la stringa non � una data/ora valida
		/// </returns>
		///****************************************************************************
		public static bool IsDateTime(string test, out DateTime conv)
		{
			if (test == null || test == "" || test.Length < 5)
			{
				conv = DateTime.MinValue;
				return false;
			}

			try
			{
				conv = DateTime.Parse(test);
				return true;
			}
			catch
			{
				conv = DateTime.MinValue;
				return false;
			}
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene una data/ora valida in Invariant Culture
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">data/ora convertita</param>
		/// <returns>
		///		true	la stringa � una data/ora valida
		///		false	la stringa non � una data/ora valida
		/// </returns>
		///****************************************************************************
		public static bool IsDateTimeIC(string test, out DateTime conv)
		{
			return IsDateTime(test, out conv, System.Globalization.CultureInfo.InvariantCulture);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene una data/ora valida
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">data/ora convertita</param>
		/// <param name="ci">tipo di formattazione</param>
		/// <returns>
		///		true	la stringa � una data/ora valida
		///		false	la stringa non � una data/ora valida
		/// </returns>
		///****************************************************************************
		public static bool IsDateTime(string test, out DateTime conv, System.Globalization.CultureInfo ci)
		{
			if (test == null || test == "" || test.Length < 5)
			{
				conv = DateTime.MinValue;
				return false;
			}

			try
			{
				conv = DateTime.Parse(test, ci);
				return true;
			}
			catch
			{
				conv = DateTime.MinValue;
				return false;
			}
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene una data/ora valida in Invariant Culture
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">data/ora convertita</param>
		/// <param name="format">stringa di formattazione</param>
		/// <returns>
		///		true	la stringa � una data/ora valida
		///		false	la stringa non � una data/ora valida
		/// </returns>
		///****************************************************************************
		public static bool IsDateTimeIC(string test, out DateTime conv, string format)
		{
			return IsDateTime(test, out conv, System.Globalization.CultureInfo.InvariantCulture, format);
		}
		///****************************************************************************
		/// <summary>
		/// Verifica se una stringa contiene una data/ora valida
		/// </summary>
		/// <param name="test">stringa da testare</param>
		/// <param name="conv">data/ora convertita</param>
		/// <param name="ci">tipo di formattazione</param>
		/// <param name="format">stringa di formattazione</param>
		/// <returns>
		///		true	la stringa � una data/ora valida
		///		false	la stringa non � una data/ora valida
		/// </returns>
		///****************************************************************************
		public static bool IsDateTime(string test, out DateTime conv, System.Globalization.CultureInfo ci, string format)
		{
			if (test == null || test == "" || test.Length < 5)
			{
				conv = DateTime.MinValue;
				return false;
			}

			try
			{
				conv = DateTime.ParseExact(test, format, ci);
				return true;
			}
			catch
			{
				conv = DateTime.MinValue;
				return false;
			}
		}

		///***************************************************************************
		/// <summary>
		/// Calcola lo sbordamento di un disco, sulla profondit� data
		/// </summary>
		/// <param name="diameter">diametro disco</param>
		/// <param name="cutDepth">profondit� di taglio</param>
		/// <returns>
		///		&gt; 0		sbordamento disco calcolato
		///		&lt; 0		errore: profondit� maggiore del diametro
		/// </returns>
		///***************************************************************************
		public static double DiskOutside(double diameter, double cutDepth)
		{
			// Verifica se la profondit� � maggiore del diametro
			if (cutDepth > diameter)
				return -1d;

			// Verifica se la profondit� � maggiore del raggio
			if (cutDepth >= diameter/2d)
				return -diameter/2d;

			// Profondit� minore del raggio
			return Math.Sqrt(cutDepth * diameter - cutDepth * cutDepth);
        }

        public static bool IsModuleLesserThan(double value, double tol = 0.01)
        {
            return Math.Abs(value) < tol;
        }

        public static bool IsAlmost(double value, double comparisionValue, double tol = 0.01)
        {
            return Math.Abs(value - comparisionValue) < tol;
        }

        public static double ToDegrees(double radians, int digits = int.MaxValue)
        {
            if (digits == int.MaxValue)
                return radians * 180.0 / Math.PI;
            else
                return Math.Round(radians * 180.0 / Math.PI, digits);
        }

        public static double ToRadians(double degrees, int digits = int.MaxValue)
        {
            if (digits == int.MaxValue)
                return degrees * Math.PI / 180.0;
            else
                return Math.Round(degrees * Math.PI / 180.0, digits);
        }

        /// <summary>
        /// Restringe un valore all'interno di un dato range
        /// </summary>
        /// <param name="value">valore da controllare</param>
        /// <param name="min">limite minimo</param>
        /// <param name="max">limite massimo</param>
        /// <returns>Il valore ristretto nel range [min, max]</returns>
        public static T Clamp<T>(T value, T min, T max) where T : System.IComparable<T>
        {
            T result = value;
            if (value.CompareTo(max) > 0)
                result = max;
            if (value.CompareTo(min) < 0)
                result = min;

            return result;
        }

        public static double AngleBetween_Unsigned(Vector v1, Vector v2)
        {
            double angle;
            if (AngleBetween_SignXtoY(v1, v2, out angle))
                return angle;
            else
                return 0;
        }
        public static bool AngleBetween_SignXtoY(Vector v1, Vector v2, out double angle)
        {
            angle = 0;

            if (v1 == null || v2 == null)
                return false;

            var l1 = v1.Magnitude;
            var l2 = v2.Magnitude;
            if (l1 < MathUtil.FLT_EPSILON)
                return false;
            if (l2 < MathUtil.FLT_EPSILON)
                return false;

            var v1u = v1.UnitVector;
            var v2u = v2.UnitVector;
            double dot = Vector.ScalarProduct(v1u, v2u);
            double cross = (v1u * v2u).Z;
            angle = Math.Acos(dot);
            if (cross < 0)
                angle *= -1;
            return true;
        }
	}
}
