using System;

namespace Breton.MathUtils
{
	public class RTMatrix
	{
		#region Variables

		double[,] mMatrix;				// Matrice di rototraslazione
		double[,] mSaveMatrix;			// Salvataggio matrice di rototraslazione 

		#endregion


		#region Constructors

		//**************************************************************************
		// Crea la matrice
		// Parametri:
		//			offsetX	: offset di traslazione X del poligono
		//			offsetY	: offset di traslazione Y del poligono
		//			centerX	: ascissa X del centro di rotazione
		//			centerY	: ascissa Y del centro di rotazione
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public RTMatrix()
		{
			mMatrix = InitMatrixRT();
			mSaveMatrix = null;
		}
		public RTMatrix(RTMatrix m)
		{
			mMatrix = (double[,])m.Matrix.Clone();
			mSaveMatrix = null;
		}
		public RTMatrix(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			mMatrix = MatAssRotoTraslate(offsetX, offsetY, centerX, centerY, angle);
			mSaveMatrix = null;
		}
		public RTMatrix(double offsetX, double offsetY, double angle)
		{
			mMatrix = InitMatrixRT();
			SetRotoTransl(offsetX, offsetY, angle);
			mSaveMatrix = null;
		}

        public RTMatrix(double offsetX, double offsetY, double xAxisNew_From_xAxisOld_xComponent, double xAxisNew_From_xAxisOld_yComponent)
		{
			mMatrix = InitMatrixRT();

            mMatrix[0, 0] = xAxisNew_From_xAxisOld_xComponent;
            mMatrix[0, 1] = -xAxisNew_From_xAxisOld_yComponent;
            mMatrix[0, 2] = offsetX;
            mMatrix[1, 0] = xAxisNew_From_xAxisOld_yComponent;
            mMatrix[1, 1] = xAxisNew_From_xAxisOld_xComponent;
            mMatrix[1, 2] = offsetY;
            mMatrix[2, 0] = 0d;
            mMatrix[2, 1] = 0d;
            mMatrix[2, 2] = 1d;

			mSaveMatrix = null;
        }
        public RTMatrix(Breton.MathUtils.Vector offset, Breton.MathUtils.Vector xAxisNew_From_xAxisOld)
            : this(offset.X, offset.Y, xAxisNew_From_xAxisOld.X, xAxisNew_From_xAxisOld.Y)
        {
        }

        //**************************************************************************
        // Crea la matrice
        // Costruttore privato
        //**************************************************************************
        private RTMatrix(double [,] arrayMat)
        {
            mMatrix = InitMatrixRT();

            mMatrix[0, 0] = arrayMat[0, 0];
            mMatrix[0, 1] = arrayMat[0, 1];
            mMatrix[0, 2] = arrayMat[0, 2];
            mMatrix[1, 0] = arrayMat[1, 0];
            mMatrix[1, 1] = arrayMat[1, 1];
            mMatrix[1, 2] = arrayMat[1, 2];
            mMatrix[2, 0] = arrayMat[2, 0];
            mMatrix[2, 1] = arrayMat[2, 1];
            mMatrix[2, 2] = arrayMat[2, 2];

            mSaveMatrix = null;
        }



		#endregion


		#region Properties

		/// **************************************************************************
		/// <summary>
		/// Imposta o ritorna la matrice di rototraslazione
		/// </summary>
		/// **************************************************************************
		public double[,] Matrix
		{
			get { return mMatrix; }
			set { mMatrix = value; }
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se la matrice contiene solo un movimento in X
		/// </summary>
		/// **************************************************************************
		public bool IsXMoveOnly
		{
			get
			{
				double offsetX, offsetY, angle;

				GetRotoTransl(out offsetX, out offsetY, out angle);

				return (!MathUtil.QuoteIsZero(offsetX) && MathUtil.QuoteIsZero(offsetY) && MathUtil.AngleIsZero(angle));
			}
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se la matrice contiene solo un movimento in Y
		/// </summary>
		/// **************************************************************************
		public bool IsYMoveOnly
		{
			get
			{
				double offsetX, offsetY, angle;

				GetRotoTransl(out offsetX, out offsetY, out angle);

				return (MathUtil.QuoteIsZero(offsetX) && !MathUtil.QuoteIsZero(offsetY) && MathUtil.AngleIsZero(angle));
			}
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se la matrice contiene solo una rotazione
		/// </summary>
		/// **************************************************************************
		public bool IsRotOnly
		{
			get
			{
				double offsetX, offsetY, angle;

				GetRotoTransl(out offsetX, out offsetY, out angle);

				return (MathUtil.QuoteIsZero(offsetX) && MathUtil.QuoteIsZero(offsetY) && !MathUtil.AngleIsZero(angle));
			}
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se la matrice contiene solo una traslazione
		/// </summary>
		/// **************************************************************************
		public bool IsTranslOnly
		{
			get
			{
				double offsetX, offsetY, angle;

				GetRotoTransl(out offsetX, out offsetY, out angle);

				return ((!MathUtil.QuoteIsZero(offsetX) || !MathUtil.QuoteIsZero(offsetY)) && MathUtil.AngleIsZero(angle));
			}
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se la matrice � neutra (non produce ne' traslazioni ne' rotazione).
		/// </summary>
		/// **************************************************************************
		public bool IsNeutral
		{
			get
			{
				double offsetX, offsetY, angle;

				GetRotoTransl(out offsetX, out offsetY, out angle);

				return (MathUtil.QuoteIsZero(offsetX) && MathUtil.QuoteIsZero(offsetY) && MathUtil.AngleIsZero(angle));
			}
		}

		#endregion


		#region Overloaded Binary Operators ( +-*/^ == !=)


		#region Multiply

		/// **************************************************************************
		/// <summary>
		/// Moltiplica due matrici
		/// </summary>
		/// <param name="a">matrice a</param>
		/// <param name="b">matrice b</param>
		/// <returns>matrice risultante</returns>
		/// **************************************************************************
		public static RTMatrix operator *(RTMatrix a, RTMatrix b)
		{
			// Ritorna la matrice di rototraslazione risultante
			return new RTMatrix(MatrxMatr(a.mMatrix, b.mMatrix));
		}

		#endregion


		#region Division

		/// **************************************************************************
		/// <summary>
		/// Divide due matrici
		/// </summary>
		/// <param name="a">matrice a</param>
		/// <param name="b">matrice b</param>
		/// <returns>matrice risultante</returns>
		/// **************************************************************************
		public static RTMatrix operator /(RTMatrix a, RTMatrix b)
		{
			// Ritorna la matrice di rototraslazione risultante
			return a * b.InvMat();
		}

		#endregion


		#region Sum

		/// **************************************************************************
		/// <summary>
		/// Somma due matrici
		/// </summary>
		/// <param name="a">matrice a</param>
		/// <param name="b">matrice b</param>
		/// <returns>matrice risultante</returns>
		/// **************************************************************************
		public static RTMatrix operator +(RTMatrix a, RTMatrix b)
		{
			double[,] m = new double[3, 3];

			// Esegue la somma
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					m[i, j] = a.mMatrix[i, j] + b.mMatrix[i, j];

			// Ritorna la matrice di rototraslazione risultante
			return new RTMatrix(m);
		}

		#endregion


		#region Subtraction

		/// **************************************************************************
		/// <summary>
		/// Sottrae due matrici
		/// </summary>
		/// <param name="a">matrice a</param>
		/// <param name="b">matrice b</param>
		/// <returns>matrice risultante</returns>
		/// **************************************************************************
		public static RTMatrix operator -(RTMatrix a, RTMatrix b)
		{
			double[,] m = new double[3, 3];

			// Esegue la somma
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					m[i, j] = a.mMatrix[i, j] - b.mMatrix[i, j];

			// Ritorna la matrice di rototraslazione risultante
			return new RTMatrix(m);
		}
         
        #endregion


		#region Comparison

		public static bool operator ==(RTMatrix m1, RTMatrix m2)
		{
			if ((object)m1 == null)
				return ((object)m2 == null);

			if ((object)m2 == null)
				return false;

			// confronto cella per cella
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					if (!MathUtil.IsEQ(m1.mMatrix[i, j], m2.mMatrix[i, j]))
						return false;

			return true;
		}

		public static bool operator !=(RTMatrix m1, RTMatrix m2)
		{
			return !(m1 == m2);
		}

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion


		#endregion


		#region Public Methods

		//**************************************************************************
		// RotoTranslAbs
		// Rototraslazione assoluta 
		// Parametri:
		//			offsetX	: offset di traslazione X del poligono
		//			offsetY	: offset di traslazione Y del poligono
		//			centerX	: ascissa X del centro di rotazione
		//			centerY	: ascissa Y del centro di rotazione
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void RotoTranslAbs(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			mMatrix = MatAssRotoTraslate(offsetX, offsetY, centerX, centerY, angle);
		}

		//**************************************************************************
		// RotoTranslRel
		// Rototraslazione relativa del poligono
		// Parametri:
		//			offsetX	: offset di traslazione X del poligono
		//			offsetY	: offset di traslazione Y del poligono
		//			centerX	: ascissa X del centro di rotazione
		//			centerY	: ascissa Y del centro di rotazione
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void RotoTranslRel(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			mMatrix = MatRelRotoTraslate(mMatrix, offsetX, offsetY, centerX, centerY, angle);
		}

		//**************************************************************************
		// Symmetry
		// Ottiene la matrice simmetrica rispetto ad un segmento
		// Parametri:
		//			x1	: ascissa vertice 1 segmento
		//			y1	: ordinata vertice 1 segmento
		//			x2	: ascissa vertice 2 segmento
		//			y2	: ordinata vertice 2 segmento
		//**************************************************************************
		public void Symmetry(double x1, double y1, double x2, double y2)
		{
			// Calcola il punto medio del segmento
			double xm = (x1 + x2) / 2d;
			double ym = (y1 + y2) / 2d;

			// Calcola il vettore
			Vector v = new Vector(x1, y1, x2, y2);
		    
			// Lato verticale
			if (MathUtil.QuoteIsZero(v.X))
				// Calcola la matrice di simmetria
				mMatrix = MatRelSimmAxis(mMatrix, true, false, xm, 0d);
			// Lato orizzontale
			else if (MathUtil.QuoteIsZero(v.Y))
				// Calcola la matrice di simmetria
				mMatrix = MatRelSimmAxis(mMatrix, false, true, 0d, ym);

				// Lato con altre inclinazioni
			else
			{
				// Calcola l'inclinazione del lato
				double incl = v.Angle();

				// Ruota la matrice dell'inclinazione del segmento
				mMatrix = MatRelRotoTraslate(mMatrix, 0d, 0d, xm, ym, incl);
				// Esegue la simmetria
				mMatrix = MatRelSimmAxis(mMatrix, false, true, 0d, ym);
				// Ritorna il poligono all'inclinazione originale
				mMatrix = MatRelRotoTraslate(mMatrix, 0d, 0d, xm, ym, -incl);
			}
		}

		//**************************************************************************
		// PointTransform
		// Restituisce il punto trasformato
		// Parametri:
		//			x	: ascissa punto da trasformare
		//			y	: ordinata punto da trasformare
		//			xt	: ascissa punto trasformato
		//			yt	: ordinata punto trasformato
		//**************************************************************************
		public void PointTransform(double x, double y, out double xt, out double yt)
		{
			xt = x * mMatrix[0,0] + y * mMatrix[0,1] + 1 * mMatrix[0,2];
			yt = x * mMatrix[1,0] + y * mMatrix[1,1] + 1 * mMatrix[1,2];
		}

		//**************************************************************************
		// Save
		// Salva la matrice di rototraslazione
		//**************************************************************************
		public void Save()
		{
			mSaveMatrix = (double[,])mMatrix.Clone();
		}

		//**************************************************************************
		// Restore
		// Ripristina la matrice di rototraslazione salvata
		//**************************************************************************
		public void Restore()
		{
			if (mSaveMatrix != null)
				mMatrix = (double[,])mSaveMatrix.Clone();
		}

		//**************************************************************************
		// GetRotoTransl
		// Legge la traslazione e l'angolo di rotazione della matrice
		// Parametri:
		//			offsetX	: traslazione X
		//			offsetY	: traslazione Y
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void GetRotoTransl(out double offsetX, out double offsetY, out double angle)
		{
			//Legge la matrice di rototraslazione
			double cos = mMatrix[0, 0];
			double sin = mMatrix[1, 0];

            //attenzione: sin e cos devono essere strettamente compresi tra -1 e 1
            if (cos < -1)
                cos = -1;
            else
                if (cos > 1)
                    cos = 1;
            if (sin < -1)
                sin = -1;
            else
                if (sin > 1)
                    sin = 1;

			if (cos > 0d)
				angle = Math.Asin(sin);
			else if(sin > 0d)
				angle = Math.PI - Math.Asin(sin);
			else
				angle = -Math.PI - Math.Asin(sin);

			offsetX = mMatrix[0, 2];
			offsetY = mMatrix[1, 2];
		}

		//**************************************************************************
		// SetRotoTransl
		// Setta la traslazione e l'angolo di rotazione della matrice
		// Parametri:
		//			offsetX	: traslazione X
		//			offsetY	: traslazione Y
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void SetRotoTransl(double offsetX, double offsetY, double angle)
		{
			mMatrix[0, 0] = Math.Cos(angle);
			mMatrix[0, 1] = -Math.Sin(angle);
			mMatrix[0, 2] = offsetX;
			mMatrix[1, 0] = Math.Sin(angle);
			mMatrix[1, 1] = Math.Cos(angle);
			mMatrix[1, 2] = offsetY;
			mMatrix[2, 0] = 0d;
			mMatrix[2, 1] = 0d;
			mMatrix[2, 2] = 1d;
		}

		//**************************************************************************
		// Reset
		// Resetta la matrice di rototraslazione
		//**************************************************************************
		public void Reset()
		{
			mMatrix = InitMatrixRT();
		}

        //**************************************************************************
        // InvMat
        // Restituisce la matrice inversa
        //**************************************************************************
        public RTMatrix InvMat()
        {
            RTMatrix m = new RTMatrix();
            double[,] matrInv = new double[3, 3];
            matrInv = MatrInversa3(mMatrix);
            m = new RTMatrix(matrInv);
            return m;
        }

        public Vector GetTranslationVector()
        {
            return new Vector(this.Matrix[0, 2], this.Matrix[1, 2]);
        }
        public Vector GetXAxisVector()
        {
            return new Vector(this.Matrix[0, 0], this.Matrix[1, 0]);
        }
        public Vector GetYAxisVector()
        {
            return new Vector(this.Matrix[0, 1], this.Matrix[1, 1]);
        }

		#endregion


		#region Private Methods

		//***************************************************************************/ 
		//**                                                                      ***/
		//**   MatriceRT                                                          ***/
		//**                                                                      ***/
		//**   Crea la matrice di rototraslazione                                 ***/
		//**                                                                      ***/
		//***************************************************************************/ 
		double[,] MatriceRT(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			double sinAng = Math.Sin(angle);
			double cosAng = Math.Cos(angle);

			double[,] matrRT = new double[3, 3];

			matrRT[0, 0] = cosAng;
			matrRT[0, 1] = -sinAng;
			matrRT[0, 2] = centerX * (1 - cosAng) + centerY * sinAng + offsetX;
			matrRT[1, 0] = sinAng;
			matrRT[1, 1] = cosAng;
			matrRT[1, 2] = centerY * (1 - cosAng) - centerX * sinAng + offsetY;
			matrRT[2, 0] = 0;
			matrRT[2, 1] = 0;
			matrRT[2, 2] = 1d;

			return matrRT;
		}

		//***************************************************************************/ 
		//**                                                                      ***/
		//**   MatrSimmAsse                                                       ***/
		//**                                                                      ***/
		//**   Crea la matrice di simmetria rispetto agli assi cartesiani         ***/
		//**                                                                      ***/
		//***************************************************************************/ 
		private double[,] MatrSimmAsse(bool simmX, bool simmY, double axisX, double axisY)
		{
			double[,] matrSimm = new double[3, 3];

			if (!simmX)
				matrSimm[0, 0] = 1d;
			else
				matrSimm[0, 0] = -1d;
			matrSimm[0, 1] = 0d;
			matrSimm[0, 2] = 2d * axisX;

			matrSimm[1, 0] = 0d;

			if (!simmY)
				matrSimm[1, 1] = 1d;
			else
				matrSimm[1, 1] = -1d;

			matrSimm[1, 2] = 2d * axisY;

			matrSimm[2, 0] = 0d;
			matrSimm[2, 1] = 0d;
			matrSimm[2, 2] = 1d;

			return matrSimm;
		}

		//***************************************************************************/ 
		//**  MatrxMatr                                                           ***/
		//**                                                                      ***/
		//**   Moltiplica una matrice quadrata per un'altra matrice quadrata      ***/
		//**   delle stesse dimensione.                                           ***/
		//**                                                                      ***/
		//***************************************************************************/ 
		private static double[,] MatrxMatr(double[,] m1, double[,] m2)
		{
			double[,] matrProd = new double[3,3];

			matrProd[0,0] = m1[0,0] * m2[0,0] + m1[0,1] * m2[1,0] + m1[0,2] * m2[2,0];
			matrProd[0,1] = m1[0,0] * m2[0,1] + m1[0,1] * m2[1,1] + m1[0,2] * m2[2,1];
			matrProd[0,2] = m1[0,0] * m2[0,2] + m1[0,1] * m2[1,2] + m1[0,2] * m2[2,2];
			
			matrProd[1,0] = m1[1,0] * m2[0,0] + m1[1,1] * m2[1,0] + m1[1,2] * m2[2,0];
			matrProd[1,1] = m1[1,0] * m2[0,1] + m1[1,1] * m2[1,1] + m1[1,2] * m2[2,1];
			matrProd[1,2] = m1[1,0] * m2[0,2] + m1[1,1] * m2[1,2] + m1[1,2] * m2[2,2];

			matrProd[2,0] = m1[2,0] * m2[0,0] + m1[2,1] * m2[1,0] + m1[2,2] * m2[2,0];
			matrProd[2,1] = m1[2,0] * m2[0,1] + m1[2,1] * m2[1,1] + m1[2,2] * m2[2,1];
			matrProd[2,2] = m1[2,0] * m2[0,2] + m1[2,1] * m2[1,2] + m1[2,2] * m2[2,2];

			return matrProd;
		}

		//***************************************************************************/ 
		//**  Determinant	                                                      ***/
		//**                                                                      ***/
		//**   Calcola il determinante della matrice quadrata di ordine 3		  ***/
		//**                                                                      ***/
		//***************************************************************************/ 
		private double Determinant(double[,] matr)
		{
			//Ritorna il determinante della matrice
			return (matr[0,0] * matr[1,1] * matr[2,2] -
					matr[0,0] * matr[1,2] * matr[2,1] +
					matr[0,1] * matr[1,2] * matr[2,0] -
					matr[0,1] * matr[1,0] * matr[2,2] +
					matr[0,2] * matr[1,0] * matr[2,1] -
					matr[0,2] * matr[1,1] * matr[2,0]);
		}

		//***************************************************************************/ 
		//**  MatrInversa3	                                                      ***/
		//**                                                                      ***/
		//**   Calcola la matrice inversa di una matrice quadrata di ordine 3     ***/
		//**                                                                      ***/
		//***************************************************************************/ 
		private double[,] MatrInversa3(double[,] matr)
		{
			/* Calcola il determinante della matrice */
			double det = Determinant(matr);

			/* Controlla se il determinante e` 0 */
			if (det == 0d)
				return null;

			double[,] matrInv = new double[3,3];

			/* Calcola la matrice inversa */
			matrInv[0,0] =  (matr[1,1] * matr[2,2] - matr[1,2] * matr[2,1]) / det;
			matrInv[1,0] = -(matr[1,0] * matr[2,2] - matr[1,2] * matr[2,0]) / det;
			matrInv[2,0] =  (matr[1,0] * matr[2,1] - matr[1,1] * matr[2,0]) / det;
			matrInv[0,1] = -(matr[0,1] * matr[2,2] - matr[0,2] * matr[2,1]) / det;
			matrInv[1,1] =  (matr[0,0] * matr[2,2] - matr[0,2] * matr[2,0]) / det;
			matrInv[2,1] = -(matr[0,0] * matr[2,1] - matr[0,1] * matr[2,0]) / det;
			matrInv[0,2] =  (matr[0,1] * matr[1,2] - matr[0,2] * matr[1,1]) / det;
			matrInv[1,2] = -(matr[0,0] * matr[1,2] - matr[0,2] * matr[1,0]) / det;
			matrInv[2,2] =  (matr[0,0] * matr[1,1] - matr[0,1] * matr[1,0]) / det;

			return matrInv;
		}

		//***************************************************************************/ 
		//**                                                                      ***/
		//**   InitMatrixRT                                                      ***/
		//**                                                                      ***/
		//**   Inizializza la matrice di rototraslazione                          ***/
		//**                                                                      ***/
		//***************************************************************************/ 
		private double[,] InitMatrixRT()
		{
			double[,] matr = new double[,] {{1d,0d,0d}, {0d,1d,0d}, {0d,0d,1d}};
			return matr;
		}

		//***************************************************************************/ 
		//**                                                                      ***/
		//**  MatAssRotoTraslate	                                              ***/
		//**                                                                      ***/
		//**  Calcola la nuova rototraslazione, in modo assoluto.                 ***/
		//**                                                                      ***/
		//**  ptOffset    punto che definisce l'offset di traslazione             ***/
		//**  ptCentro    punto che definisce il centro di rotazione              ***/
		//**  ptAngolo    angolo di rotazione                                     ***/
		//***************************************************************************/ 
		private double[,] MatAssRotoTraslate(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			/* Calcola la matrice di rototraslazione risultante   */
			return MatriceRT(offsetX, offsetY, centerX, centerY, angle);
		}

		//***************************************************************************/ 
		//**                                                                      ***/
		//**  MatRelRotoTraslate                                                ***/
		//**                                                                      ***/
		//**  Calcola la nuova rototraslazione, in modo relativo rispetto alla    ***/
		//**  rototraslazione precedente.                                         ***/
		//**                                                                      ***/
		//**  ptMatr      puntatore alla matrice di rototraslazione iniziale      ***/
		//**  ptOffset    punto che definisce l'offset di traslazione             ***/
		//**  ptCentro    punto che definisce il centro di rotazione              ***/
		//**  ptAngolo    angolo di rotazione                                     ***/
		//***************************************************************************/ 
		private double[,] MatRelRotoTraslate(double[,] matr1, 
											 double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			// Calcola la nuova matrice di rototraslazione
			double [,] matr2 = MatriceRT(offsetX, offsetY, centerX, centerY, angle);

			// Ritorna la matrice di rototraslazione risultante
			return MatrxMatr(matr2, matr1);
		}

		//***************************************************************************/ 
		//**                                                                      ***/
		//**  MatRelSimmAxis                                                      ***/
		//**                                                                      ***/
		//**  Calcola la matrice in modo da eseguire una simmetria rispetto       ***/
		//**  ad un asse parallelo ad uno degli assi cartesiani.                  ***/
		//**                                                                      ***/
		//**  ptMatr      puntatore alla matrice di rototraslazione iniziale      ***/
		//**  axisX       coordinata X dell'asse di simmetria parall.all'asse Y   ***/
		//**  axisY       coordinata Y dell'asse di simmetria parall.all'asse X   ***/
		//***************************************************************************/ 
		private double[,] MatRelSimmAxis(double[,] matr1, bool simmX, bool simmY, double axisX, double axisY)
		{
			/* Calcola la matrice di simmetria rispetto all'asse */
			double[,] matr2 = MatrSimmAsse(simmX, simmY, axisX, axisY);
			   
			/* Calcola la matrice di rototraslazione risultante */
			return MatrxMatr(matr1, matr2);
		}

		#endregion


        #region Overrides

        public override string ToString()
        {
            string format = "0.000";
            string sep = "   ";
            return Matrix[0, 2].ToString(format, System.Globalization.CultureInfo.InvariantCulture) + sep + Matrix[1, 2].ToString(format, System.Globalization.CultureInfo.InvariantCulture) + sep + (Math.Atan2(Matrix[0, 1], Matrix[0, 0]) * 180 / Math.PI).ToString(format, System.Globalization.CultureInfo.InvariantCulture);
        }

        #endregion
    }
}
