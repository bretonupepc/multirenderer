using System;

namespace Breton.MathUtils
{
	public class Vector
	{
		#region Variables
		
		private double mX;		// Componente X del vettore
		private double mY;		// Componente Y del vettore
		private double mZ;		// Componente Z del vettore
		
		#endregion

		#region Constructors

		public Vector(): this(0d, 0d, 0d)
		{}

		/// **************************************************************************
		/// <summary>
		/// Crea un vettore unitario, con inclinazione data
		/// </summary>
		/// <param name="angle">angolo (in radianti)</param>
		/// **************************************************************************
		public Vector(double angle)
		{
			mX = Math.Cos(angle);
			mY = Math.Sin(angle);
			mZ = 0d;
		}

		/// **************************************************************************
		/// <summary>
		/// Crea un vettore, dati x e y
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// **************************************************************************
		public Vector(double x, double y): this(x, y, 0d)
		{}
		/// **************************************************************************
		/// <summary>
		/// Crea un vettore, dati x, y e z
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// **************************************************************************
		public Vector(double x, double y, double z)
		{
			mX = x; mY = y;	mZ = z;
		}

		//**************************************************************************
		// Costruisce il vettore associato ad un segmento
		//**************************************************************************
		public Vector(double x1, double y1, double z1, double x2, double y2, double z2): this(x1-x2, y1-y2, z1-z2)
		{}
		public Vector(double x1, double y1, double x2, double y2): this(x1-x2, y1-y2, 0d)
		{}

		/// **************************************************************************
		/// <summary>
		/// Costruisce una copia del vettore dato
		/// </summary>
		/// <param name="v">vettore da copiare</param>
		/// **************************************************************************
		public Vector(Vector v)
		{
			mX = v.mX;
			mY = v.mY;
			mZ = v.mZ;
		}

		/// **************************************************************************
		/// <summary>
		/// Costruisce un vettore parallelo al vettore dato, con il modulo dato
		/// </summary>
		/// <param name="v">vettore origine</param>
		/// <param name="magn">modulo</param>
		/// **************************************************************************
		public Vector(Vector v, double magn)
		{
			// Calcola il versore del vettore
			Vector vers = v.UnitVector;

			mX = vers.mX * magn;
			mY = vers.mY * magn;
			mZ = vers.mZ * magn;
		}

		#endregion

		#region Properties

		public double X
		{
			get { return mX; }
		}

		public double Y
		{
			get { return mY; }
		}

		public double Z
		{
			get { return mZ; }
		}

		//**************************************************************************
		// Magnitude
		// Restituisce il modulo del vettore
		//**************************************************************************
		public double Magnitude
		{
			get { return Math.Sqrt(mX * mX + mY * mY + mZ * mZ); }
		}

		//**************************************************************************
		// UnitVector
		// Restituisce il versore del vettore
		//**************************************************************************
		public Vector UnitVector
		{
			get
			{
				double mod = Magnitude;
				if (mod == 0d)
					return null;

				return new Vector(mX / mod, mY / mod, mZ / mod);
			}
		}

		#endregion

		#region Operator overloading

		//******************************************************************************
		// Operatore *
		// Prodotto vettoriale fra 2 vettori 
		//******************************************************************************
		public static Vector operator * (Vector v1, Vector v2)
		{
			return new Vector(v1.Y * v2.Z - v1.Z * v2.Y,  -v1.X * v2.Z + v1.Z * v2.X, v1.X * v2.Y - v1.Y * v2.X);
		}

		//**************************************************************************
		// Operatore *
		// Calcola il prodotto di un vettore per uno scalare
		//**************************************************************************
		public static Vector operator * (Vector v, double k)
		{
			return new Vector(v.X * k, v.Y * k, v.Z * k);
		}

		#endregion

		#region Public Methods

		//**************************************************************************
		// ScalarProduct
		// Calcola il prodotto scalare fra 2 vettori
		//**************************************************************************
		public static double ScalarProduct(Vector v1, Vector v2)
		{
			return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
		}

		//**************************************************************************
		// Angle
		// Calcola l'angolo fra il vettore e il vettore dato
		// Parametri:
		//			v:	vettore 1
		// Restituisce:
		//			angolo fra i due vettori, in radianti
		//**************************************************************************
		public double Angle(Vector v)
		{
            double scalarProduct = ScalarProduct(this, v) / (this.Magnitude * v.Magnitude);
            if (scalarProduct > 1)
                scalarProduct = 1;
            else if (scalarProduct < -1)
                scalarProduct = -1;
            return Math.Acos(scalarProduct);
		}
		
		//**************************************************************************
		// Angle
		// Calcola l'angolo del vettore rispetto all'asse delle ascisse
		//**************************************************************************
		public double Angle()
		{
			return Angle(new Vector(1d, 0d));
		}

        //**************************************************************************
        // Direction
        // Calcola la direzione sul piano XY del vettore (da 0 a 2PI)
        //**************************************************************************
        public double DirectionXY()
        {
            return MathUtil.gdDirezione(0, 0 , X, Y);
        }

        //**************************************************************************
        // Direction
        // Calcola la direzione sul piano perpendicolare a XY del vettore (da 0 a 2PI)
        //**************************************************************************
        public double DirectionZ()
        {
            double l = Math.Sqrt(X*X + Y*Y);
            return MathUtil.gdDirezione(0, 0, l, Z);
        }

        //**************************************************************************
		// ApplToPoint
		// Applicando il vettore ad un punto, restituisce il punto risultante
		// Parametri:
		//			x1	: ascissa punto a cui applicare il vettore
		//			y1	: ordinata punto a cui applicare il vettore
		//			x2	: ascissa punto risultante
		//			y2	: ordinata punto risultante
		//**************************************************************************
		public void ApplToPoint(double x1, double y1, out double x2, out double y2)
		{
			x2 = x1 + mX;
			y2 = y1 + mY;
		}

		#endregion

	}
}
