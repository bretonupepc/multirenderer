using System;
using System.IO;

namespace Breton.MathUtils
{
	/// <summary>
	/// Summary description for GenUtils.
	/// </summary>
	public class GenUtils
	{
		public GenUtils()
		{
		}

	
		///*********************************************************************
		/// <summary>
		/// Reallocates an array with a new size, and copies the contents of the 
		/// old array to the new array.
		/// </summary>
		/// <param name="oldArray">the old array, to be reallocated.</param>
		/// <param name="newSize">the new array size.</param>
		/// <returns>A new array with the same contents.</returns>
		///*********************************************************************
		public static System.Array ResizeArray(System.Array oldArray, int newSize) 
		{
			int oldSize = oldArray.Length;

			Type elementType = oldArray.GetType().GetElementType();

			Array newArray = Array.CreateInstance(elementType, newSize);

			int preserveLength = Math.Min(oldSize, newSize);

			if (preserveLength > 0)
				Array.Copy (oldArray, newArray, preserveLength);

			return newArray;
		}

		///*********************************************************************
		/// <summary>
		/// Cancella il file indicato
		/// </summary>
		/// <param name="filePath">path completo del file</param>
		/// <returns>
		///		true	cancellazione eseguita
		///		false	errore nell'operazione
		///	</returns>
		///*********************************************************************
		public static bool DeleteFile(string filePath)
		{
			try
			{
				// Esegue la cancellazione del file
				if (filePath != null && filePath.Length > 0)
					System.IO.File.Delete(filePath);
				return true;
			}
			catch
			{
				// Cancellazione non riuscita
				return false;
			}

		}

		///*********************************************************************
		/// <summary>
		/// Copia il file sorgente sul file destinazione
		/// </summary>
		/// <param name="srcFile">path completo del file sorgente</param>
		/// <param name="dstFile">path completo del file destinazione</param>
		/// <returns>
		///		true	copia eseguita
		///		false	errore nell'operazione
		///	</returns>
		///*********************************************************************
		public static bool CopyFile(string srcFile, string dstFile)
		{
			try
			{
				// Esegue la cancellazione del file
				if (srcFile != null && srcFile.Length > 0 && dstFile != null && dstFile.Length > 0)
					System.IO.File.Copy(srcFile, dstFile, true);

				return true;
			}
			catch
			{
				// Copia non riuscita
				return false;
			}

		}

		/// **************************************************************************
		/// <summary>
		/// Confronta due file in ingresso
		/// </summary>
		/// <param name="file1">path file 1 da confrontare</param>
		/// <param name="file2">path file 2 da confrontare</param>
		/// <returns>
		///		true	i file sono uguali
		///		false	i file sono diversi
		///	</returns>
		/// **************************************************************************
		public static bool FileCompare(string file1, string file2)
		{
			int file1byte;
			int file2byte;
			FileStream fs1;
			FileStream fs2;

			try
			{
				// Verifica che i files esistano
				if (!File.Exists(file1) || !File.Exists(file2))
					return false;

				// Determine if the same file was referenced two times.
				if (file1 == file2)
				{
					// Return true to indicate that the files are the same.
					return true;
				}

				// Open the two files.
				fs1 = new FileStream(file1, FileMode.Open);
				fs2 = new FileStream(file2, FileMode.Open);

				// Check the file sizes. If they are not the same, the files 
				// are not the same.
				if (fs1.Length != fs2.Length)
				{
					// Close the file
					fs1.Close();
					fs2.Close();

					// Return false to indicate files are different
					return false;
				}

				// Read and compare a byte from each file until either a
				// non-matching set of bytes is found or until the end of
				// file1 is reached.
				do
				{
					// Read one byte from each file.
					file1byte = fs1.ReadByte();
					file2byte = fs2.ReadByte();
				}
				while ((file1byte == file2byte) && (file1byte != -1));

				// Close the files.
				fs1.Close();
				fs2.Close();

				// Return the success of the comparison. "file1byte" is 
				// equal to "file2byte" at this point only if the files are 
				// the same.
				return ((file1byte - file2byte) == 0);
			}
			catch (Exception ex)
			{
                throw ex;
			}
		}

		/// **************************************************************************
		/// <summary>
		/// Divide il path dal nome del file
		/// </summary>
		/// <param name="pathName">path file completo</param>
		/// <param name="path">path file senza nome</param>
		/// <param name="name">nome file senza path</param>
		/// **************************************************************************
		public static void DivPathName(string pathName, out string path, out string name)
		{
			path = name = "";

			int lastPos = pathName.LastIndexOf('\\');

			if (lastPos >= 0)
			{
				path = pathName.Remove(lastPos);
				name = pathName.Substring(lastPos + 1);
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string RemoveSpecialCharacters(string str)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			foreach (char c in str)
			{
				if (c >= ' ' && c <= '~')
				{
					sb.Append(c);
				}
			}
			return sb.ToString();
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string RemoveInvalidFolderChar(string folder)
		{
			foreach (char c in Path.GetInvalidFileNameChars())
			{
				folder = folder.Replace(c.ToString(), "");
			}

			return folder;
		}
	}
}
