﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Breton.MathUtils
{
	public class Ellipse
	{
		const int NUMPOINTS = 50;
		const double MINIMOSEGMENTO = 0.05;			// minimo segmento espansione: 5 1/100
		const double RAD_1 = Math.PI / 180.0;
		const int PUNTIPERGRADO = 10;				// numero punti di espansione per grado
		const double SOGLIACAMBIODIR = 0.0002;		// differenza che genera un nuovo arco
		const int MINPUNTIARCO = 10;				// minimo numero di punti per un arco
		const int MAXCERCHI = 600;					// massimo numero di cerchi sviluppabili

		/// <summary>
		/// determina le coordinate di un punto di un'ellisse
		/// dato l'angolo.                                   
		/// </summary>
		/// <param name="pell">definizione dell'arco di ellisse.</param>
		/// <param name="angolo">angolo del punto da calcolare [0..2Pi).</param>
		/// <param name="punto">punto calcolato</param>
		public static void PuntoEllisse(ref MathUtil.DBL_ELL pell, double angolo, ref MathUtil.DBL_POINT punto)
		{
			double cos_a, sin_a, cos_i, sin_i;

			// calcolo unificato del seno e coseno dell'angolo
			cos_a = Math.Cos(angolo);
			sin_a = Math.Sin(angolo);

			// ottimizzo nel caso non ci sia inclinazione
			if (MathUtil.IsZero(pell.dInc))
			{
				// come nel caso dell'arco di cerchio ...
				punto.dx = pell.PtC.dx + pell.dra * cos_a;
				punto.dy = pell.PtC.dy + pell.drb * sin_a;
			}
			else
			{
				// calcolo seno e coseno dell'angolo di inclinazione
				cos_i = Math.Cos(pell.dInc);
				sin_i = Math.Sin(pell.dInc);

				// calcolo completo
				punto.dx = pell.PtC.dx + pell.dra * cos_a * cos_i - pell.drb * sin_a * sin_i;
				punto.dy = pell.PtC.dy + pell.dra * cos_a * sin_i + pell.drb * sin_a * cos_i;
			}
		}

		/// <summary>
		/// calcola il punto di intersezione, se esiste, tra
		///	due archi.                                      
		/// </summary>
		/// <param name="arc1">definizione del primo arco</param>
		/// <param name="arc2">definizione del secondo arco</param>
		/// <param name="inter1">punto di intersezione (1)</param>
		/// <param name="inter2">punto di intersezione (2)</param>
		/// <returns>
		/// 0     = nessuna intersezione.               
		/// 1     = una intersezione trovata (tangenza).
		/// 2     = due intersezioni trovate.           
		/// </returns>
		public static int inters_arc_arc(ref MathUtil.DBL_ARC arc1, ref MathUtil.DBL_ARC arc2, ref MathUtil.DBL_POINT inter1, ref MathUtil.DBL_POINT inter2)
		{
			int n_inter = 0, np_validi = 0;
			double A, B, C, D, E, F, G, H, I, J;
			double delta;
			MathUtil.DBL_POINT p1, p2, e1p1, e1p2, e2p1, e2p2; //, p;

			// calcolo i parametri per la soluzione del sistema congiunto
			A = arc1.dr * arc1.dr - arc1.PtC.dx * arc1.PtC.dx - arc1.PtC.dy * arc1.PtC.dy;
			B = arc2.dr * arc2.dr - arc2.PtC.dx * arc2.PtC.dx - arc2.PtC.dy * arc2.PtC.dy;
			C = (A - B) / 2;
			D = (arc2.PtC.dx - arc1.PtC.dx);
			E = (arc2.PtC.dy - arc1.PtC.dy);
			if (!MathUtil.IsZero(D))
			{
				F = C / D;
				G = E / D;
				H = G * G + 1;
				I = arc1.PtC.dx * G - F * G - arc1.PtC.dy;
				J = F * F - 2 * arc1.PtC.dx * F - A;
				if (MathUtil.IsZero(H))
					return 0;

				// a questo punto ho trovato: y1,2 = (-I -/+ sqrt(I*I - H*J)) / H
				delta = I * I - H * J;
				if (delta < 0)
					return 0;
				if (MathUtil.IsZero(delta))
					delta = 0;
				p1.dy = (-I - Math.Sqrt(delta)) / H;
				p2.dy = (-I + Math.Sqrt(delta)) / H;

				// trovati y1 e y2, trovo x1 e x2
				p1.dx = F - G * p1.dy;
				p2.dx = F - G * p2.dy;
			}
			else
			{
				F = C / E;
				G = D / E;
				H = G * G + 1;
				I = arc1.PtC.dy * G - F * G - arc1.PtC.dx;
				J = F * F - 2 * arc1.PtC.dy * F - A;
				if (MathUtil.IsZero(H))
					return 0;

				// a questo punto ho trovato: y1,2 = (-I -/+ sqrt(I*I - H*J)) / H
				delta = I * I - H * J;
				if (delta < 0)
					return 0;
				if (MathUtil.IsZero(delta))
					delta = 0;
				p1.dx = (-I - Math.Sqrt(delta)) / H;
				p2.dx = (-I + Math.Sqrt(delta)) / H;

				// trovati x1 e x2, trovo y1 e y2
				p1.dy = F - G * p1.dx;
				p2.dy = F - G * p2.dx;
			}

			// controlla se i punti sono diversi 
			if (!MathUtil.gEqual(p1.dx, p2.dx) || !MathUtil.gEqual(p1.dy, p2.dy))
				np_validi = 2;
			else
				np_validi = 1;

			// controllo appartenza agli archi della prima intersezione 
			if (PuntoInArc(arc1, p1))
			{
				if (PuntoInArc(arc2, p1))
				{
                    inter1 = p1;
					n_inter = 1;
				}
			}

			// controllo se devo controllare anche il secondo punto 
			if (np_validi > n_inter)
			{
				// controllo appartenza agli archi della seconda intersezione 
				if (PuntoInArc(arc1, p2))
				{
					if (PuntoInArc(arc2, p2))
					{
                        if (n_inter > 0)
                            inter2 = p2;
                        else
                            inter1 = p2;
                        n_inter++;
					}
				}
			}

			// una intersezione e` valida se il punto trovato non e` 
			// contemporaneamente estremo di entrambe le entita`     
			if (n_inter > 0)
			{
				// ricavo gli estremi degli archi 
				e1p1.dx = arc1.PtC.dx + arc1.dr * Math.Cos(arc1.dStartAng);
				e1p1.dy = arc1.PtC.dy + arc1.dr * Math.Sin(arc1.dStartAng);
				e1p2.dx = arc1.PtC.dx + arc1.dr * Math.Cos(arc1.dStartAng + arc1.dAng);
				e1p2.dy = arc1.PtC.dy + arc1.dr * Math.Sin(arc1.dStartAng + arc1.dAng);
				e2p1.dx = arc2.PtC.dx + arc2.dr * Math.Cos(arc2.dStartAng);
				e2p1.dy = arc2.PtC.dy + arc2.dr * Math.Sin(arc2.dStartAng);
				e2p2.dx = arc2.PtC.dx + arc2.dr * Math.Cos(arc2.dStartAng + arc2.dAng);
				e2p2.dy = arc2.PtC.dy + arc2.dr * Math.Sin(arc2.dStartAng + arc2.dAng);

				// controllo apparteneza a due estremi contemporaneamente 
				if (PuntoEstremoSimultaneo(inter1, e1p1, e1p2, e2p1, e2p2))
				{
					// questa intersezione non deve essere considerata 
					n_inter--;
					inter1 = inter2;

					// se esiste anche la seconda intersezione 
					if (n_inter > 0)
					{

						// controllo apparteneza a due estremi contemporaneamente 
						if (PuntoEstremoSimultaneo(inter1, e1p1, e1p2, e2p1, e2p2))
							return 0;
					}
				}
				else
				{

					// se esiste anche la seconda intersezione 
					if (n_inter > 1)

						// controllo apparteneza a due estremi contemporaneamente 
						if (PuntoEstremoSimultaneo(inter2, e1p1, e1p2, e2p1, e2p2))
							return 1;
				}
			}

			// numero di intersezioni valide 
			return n_inter;

		}

		/// <summary>
		/// calcola il punto di intersezione, se esiste, tra
		/// un segmento e un arco.                          
		/// </summary>
		/// <param name="seg">definzione del segmento</param>
		/// <param name="arc">definizione dell'arco</param>
		/// <param name="inter1">punto di intersezione (1)</param>
		/// <param name="inter2">punto di intersezione (2)</param>
		/// <returns>
		/// 0 = nessuna intersezione.     
		/// 1 = una intersezione trovata. 
		/// 2 = due intersezioni trovate. 
		/// </returns>
		public static int inters_seg_arc(ref MathUtil.DBL_LINE seg, ref MathUtil.DBL_ARC arc, ref MathUtil.DBL_POINT inter1, ref MathUtil.DBL_POINT inter2)
		{
			int n_inter = 0, np_validi = 0;
			double dx, dy, q, m, a, b, c, d, w, rd;
			MathUtil.DBL_POINT dptPunto1, dptPunto2, e2p1, e2p2, p;

			// estensioni del segmento 
			dx = seg.dptPunto2.dx - seg.dptPunto1.dx;
			dy = seg.dptPunto2.dy - seg.dptPunto1.dy;

			// tipo della retta 
			if (Math.Abs(dx) > Math.Abs(dy))
			{
				// parametri della retta 
				m = dy / dx;
				q = seg.dptPunto2.dy - m * seg.dptPunto2.dx;

				// soluzione del sistema (parametri equazione ax^2 + bx + c = 0) 
				w = q - arc.PtC.dx;
				a = 1 + (m * m);
				b = 2 * (m * w - arc.PtC.dx);
				c = w * w + arc.PtC.dx * arc.PtC.dx - arc.dr * arc.dr;
				d = b * b - 4 * a * c;
				if (d <= -MathUtil.FLT_EPSILON)
					return 0;
				if (d <= 0.0)
					dptPunto1.dx = dptPunto2.dx = -b / (2 * a);
				else
				{
					rd = Math.Sqrt(d);
					dptPunto1.dx = (-b - rd) / (2 * a);
					dptPunto2.dx = (-b + rd) / (2 * a);
				}
				dptPunto1.dy = m * dptPunto1.dx + q;
				dptPunto2.dy = m * dptPunto2.dx + q;
			}
			else
			{
				// parametri della retta 
				m = dx / dy;
				q = seg.dptPunto2.dx - m * seg.dptPunto2.dy;

				// soluzione del sistema (parametri equazione ay^2 + by + c = 0) 
				w = q - arc.PtC.dx;
				a = 1 + (m * m);
				b = 2 * (m * w - arc.PtC.dx);
				c = w * w + arc.PtC.dx * arc.PtC.dx - arc.dr * arc.dr;
				d = b * b - 4 * a * c;
				if (d <= -MathUtil.FLT_EPSILON)
					return 0;
				if (d <= 0.0)
					dptPunto1.dy = dptPunto2.dy = -b / (2 * a);
				else
				{
					rd = Math.Sqrt(d);
					dptPunto1.dy = (-b - rd) / (2 * a);
					dptPunto2.dy = (-b + rd) / (2 * a);
				}
				dptPunto1.dx = m * dptPunto1.dy + q;
				dptPunto2.dx = m * dptPunto2.dy + q;
			}

			// controlla se i punti sono diversi 
			if (!MathUtil.gEqual(dptPunto1.dx, dptPunto2.dx) || !MathUtil.gEqual(dptPunto1.dy, dptPunto2.dy))
				np_validi = 2;
			else
				np_validi = 1;

			// controllo appartenza al segmento e all'arco del primo punto 
			p = inter1;
			if (PuntoInSeg(seg, dptPunto1))
			{
				if (PuntoInArc(arc, dptPunto1))
				{
					p = dptPunto1;
					p = inter2;
					n_inter++;
				}
			}

			// controllo se devo controllare anche il secondo punto 
			if (np_validi > n_inter)
			{
				// controllo appartenza al segmento e all'arco del secondo punto 
				if (PuntoInSeg(seg, dptPunto2))
				{
					if (PuntoInArc(arc, dptPunto2))
					{
						p = dptPunto2;
						n_inter++;
					}
				}
			}

			// una intersezione e` valida se il punto trovato non e` 
			// contemporaneamente estremo di entrambe le entita`     
			if (n_inter > 0)
			{
				// ricavo gli estremi dell'arco 
				e2p1.dx = arc.PtC.dx + arc.dr * Math.Cos(arc.dStartAng);
				e2p1.dy = arc.PtC.dx + arc.dr * Math.Sin(arc.dStartAng);
				e2p2.dx = arc.PtC.dx + arc.dr * Math.Cos(arc.dStartAng + arc.dAng);
				e2p2.dy = arc.PtC.dx + arc.dr * Math.Sin(arc.dStartAng + arc.dAng);

				// controllo apparteneza a due estremi contemporaneamente 
				if (PuntoEstremoSimultaneo(inter1, seg.dptPunto1, seg.dptPunto2, e2p1, e2p2))
				{
					// questa intersezione non deve essere considerata 
					n_inter--;
					inter1 = inter2;

					// se esiste anche la seconda intersezione 
					if (n_inter > 0)
					{
						// controllo apparteneza a due estremi contemporaneamente 
						if (PuntoEstremoSimultaneo(inter1, seg.dptPunto1, seg.dptPunto2, e2p1, e2p2))
							return 0;
					}
				}
				else
				{
					// se esiste anche la seconda intersezione 
					if (n_inter > 1)
					{
						// controllo apparteneza a due estremi contemporaneamente 
						if (PuntoEstremoSimultaneo(inter2, seg.dptPunto1, seg.dptPunto2, e2p1, e2p2))
							return 1;
					}
				}
			}

			// numero di punti validi 
			return n_inter;
		}

		/// <summary>
		/// verifica se il punto specificato appartiene ad un arco.                                      
		/// </summary>
		/// <param name="arc">arco</param>
		/// <param name="punto">punto da controllare</param>
		/// <returns>
		/// false  = il punto no appartiene.
		/// true  = il punto appartiene.   
		/// </returns>
		private static bool PuntoInArc(MathUtil.DBL_ARC arc, MathUtil.DBL_POINT punto)
		{
			double vector, mina, maxa;

			// calcolo la direzione del vettore dal centro dell'arco al punto (x,y)
			vector = MathUtil.gdDirezione(arc.PtC.dx, arc.PtC.dy, punto.dx, punto.dy);

			// estremi dell'arco
			if (arc.dAng > 0)
			{
				mina = arc.dStartAng;
				maxa = arc.dStartAng + arc.dAng;
			}
			else
			{
				maxa = arc.dStartAng;
				mina = arc.dStartAng + arc.dAng;
			}
			if (mina < 0)
			{
				mina += 2 * Math.PI;
				maxa += 2 * Math.PI;
			}

			// confronto
			if (maxa > 2 * Math.PI && vector < mina)
				vector += 2 * Math.PI;

			bool b1 = ((vector) < (mina) + MathUtil.FLT_EPSILON);
			bool b2 = ((vector) > (maxa) - MathUtil.FLT_EPSILON);

			return (b1 || b2 ? false : true);
		}

		/// <summary>
		/// verifica se il punto specificato appartiene ad un segmento
		/// </summary>
		/// <param name="seg">segmento</param>
		/// <param name="punto">punto da controllare</param>
		/// <returns>
		/// false  = il punto no appartiene.
		/// true  = il punto appartiene.   
		/// </returns>
		private static bool PuntoInSeg(MathUtil.DBL_LINE seg, MathUtil.DBL_POINT punto)
		{
			// casi notevoli di segmenti orizzontali .... 
			if (MathUtil.gEqual(seg.dptPunto1.dx, seg.dptPunto2.dx))
			{
				if (!MathUtil.gEqual(punto.dx, seg.dptPunto1.dx))
					return false;
				if (MathUtil.IsLT(seg.dptPunto1.dy, seg.dptPunto2.dy))
				{
					if (MathUtil.IsLT(punto.dy, seg.dptPunto1.dy) || MathUtil.IsGT(punto.dy, seg.dptPunto2.dy))
						return false;
				}
				else
					if (MathUtil.IsLT(punto.dy, seg.dptPunto2.dy) || MathUtil.IsGT(punto.dy, seg.dptPunto1.dy))
						return false;
				return true;
			}

			// ... e verticali 
			if (MathUtil.gEqual(seg.dptPunto1.dy, seg.dptPunto2.dy))
			{
				if (!MathUtil.gEqual(punto.dy, seg.dptPunto1.dy))
					return false;
				if (MathUtil.IsLT(seg.dptPunto1.dx, seg.dptPunto2.dx))
				{
					if (MathUtil.IsLT(punto.dx, seg.dptPunto1.dx) || MathUtil.IsGT(punto.dx, seg.dptPunto2.dx))
						return false;
				}
				else
					if (MathUtil.IsLT(punto.dx, seg.dptPunto2.dx) || MathUtil.IsGT(punto.dx, seg.dptPunto1.dx))
						return false;
				return true;
			}

			// se nel rettangolo di contenimento 
			if (MathUtil.IsLT(seg.dptPunto1.dx, seg.dptPunto2.dx))
			{
				if (MathUtil.IsLT(punto.dx, seg.dptPunto1.dx) || MathUtil.IsGT(punto.dx, seg.dptPunto2.dx))
					return false;
			}
			else
				if (MathUtil.IsGT(punto.dx, seg.dptPunto1.dx) || MathUtil.IsLT(punto.dx, seg.dptPunto2.dx))
					return false;
			if (MathUtil.IsLT(seg.dptPunto1.dy, seg.dptPunto2.dy))
			{
				if (MathUtil.IsLT(punto.dy, seg.dptPunto1.dy) || MathUtil.IsGT(punto.dy, seg.dptPunto2.dy))
					return false;
			}
			else
				if (MathUtil.IsGT(punto.dy, seg.dptPunto1.dy) || MathUtil.IsLT(punto.dy, seg.dptPunto2.dy))
					return false;

			// il punto appartiene 
			return true;
		}

		/// <summary>
		/// controlla se il punto specificato e` allo stesso
		/// tempo estremo di due entita`, specificate dai   
		/// relativi punti estremi.                         
		/// </summary>
		/// <param name="punto">punto in analisi</param>
		/// <param name="e1p1">primo estremo della prima entita</param>
		/// <param name="e1p2">secondo estremo della prima entita</param>
		/// <param name="e2p1">primo estremo della seconda entita</param>
		/// <param name="e2p2">secondo estremo della seconda entita</param>
		/// <returns></returns>
		private static bool PuntoEstremoSimultaneo(MathUtil.DBL_POINT punto, MathUtil.DBL_POINT e1p1, MathUtil.DBL_POINT e1p2, MathUtil.DBL_POINT e2p1, MathUtil.DBL_POINT e2p2)
		{
			if ((MathUtil.gEqual(punto.dx, e1p1.dx) && MathUtil.gEqual(punto.dy, e1p1.dy)) ||
				(MathUtil.gEqual(punto.dx, e1p2.dx) && MathUtil.gEqual(punto.dy, e1p2.dy)))
				if ((MathUtil.gEqual(punto.dx, e2p1.dx) && MathUtil.gEqual(punto.dy, e2p1.dy)) ||
				(MathUtil.gEqual(punto.dx, e2p2.dx) && MathUtil.gEqual(punto.dy, e2p2.dy)))
					return true;

			return false;
		}

		/// <summary>
		/// conversione di un arco di ellisse in una serie di archi di cerchio
		/// </summary>
		/// <param name="pel">struttura che descrive l'ellisse</param>
		/// <param name="parc">lista di archi ottenuti</param>
		/// <param name="pseg">puntatore all'eventuale segmento</param>
		/// <returns>
		/// = -2   = errore interno.                         
		/// = -1   = arco con ampiezza nulla.                
		/// = 0    = l'arco e` stato trasformato in segmento.
		/// > 0    = numero di archi ottenuti.               
		/// </returns>
		public static int conv_ell_arc(ref MathUtil.DBL_ELL pel, ref List<MathUtil.DBL_ARC> parc, ref MathUtil.DBL_LINE pseg)
		{
			int i, contacerchi;
			int npmin, npmax;
			int numpoints = NUMPOINTS, np;
			double cx, cy, srot, crot, sact, cact, x0, y0, x1, y1, x2, y2;
			double alfa, dAng, rapp, x, y, mseg = MINIMOSEGMENTO;
			MathUtil.DBL_ELL el;
			List<MathUtil.DBL_ARC> psai = new List<MathUtil.DBL_ARC>();
			MathUtil.DBL_ARC pa = new MathUtil.DBL_ARC(), psc = new MathUtil.DBL_ARC();
			List<MathUtil.DBL_POINT> p1d = new List<MathUtil.DBL_POINT>();

			// normalizzazione dell'angolo 
			pel.dStartAng = MathUtil.Angolo_0_2PI(pel.dStartAng);

			// comunque annulla la lista di archi 
			parc.Clear();

			// primo caso: almeno un asse e` nullo 
			if (MathUtil.IsZero(pel.dra) || MathUtil.IsZero(pel.drb))
			{
				// se anche l'arco e` nullo allora, trascuro il tutto 
				if (MathUtil.IsZero(pel.dAng))
					return -1;

				// calcolo del seno e del coseno dell'angolo di rotazione 
				crot = Math.Cos(pel.dInc);
				srot = Math.Sin(pel.dInc);
				cx = pel.PtC.dx;
				cy = pel.PtC.dy;

				// se l'asse nullo e` primo 
				if (MathUtil.IsZero(pel.dra))
				{
					// si tratta di un segmento che si trova sul secondo asse 
					x0 = 0;
					y0 = pel.drb * Math.Sin(pel.dStartAng);
					pseg.dptPunto1.dx = cx - y0 * srot;
					pseg.dptPunto1.dy = cy + y0 * crot;
					y0 = pel.drb * Math.Sin(pel.dStartAng + pel.dAng);
					pseg.dptPunto2.dx = cx - y0 * srot;
					pseg.dptPunto2.dy = cy + y0 * crot;
				}
				else
				{
					// si tratta di un segmento che si trova sul primo asse 
					x0 = pel.dra * Math.Cos(pel.dStartAng);
					y0 = 0;
					pseg.dptPunto1.dx = cx + x0 * crot;
					pseg.dptPunto1.dy = cy + x0 * srot;
					x0 = pel.dra * Math.Cos(pel.dStartAng + pel.dAng);
					pseg.dptPunto2.dx = cx + x0 * crot;
					pseg.dptPunto2.dy = cy + x0 * srot;
				}

				// arco trasformato in segmento 
				return 0;
			}

			// secondo caso: archi piatti (archi con ampiezza < 1 grado) 
			if (Math.Abs(pel.dAng) < RAD_1)
			{
				// espansione di esattamente 2 punti 
				crot = Math.Cos(pel.dInc);
				srot = Math.Sin(pel.dInc);
				cact = Math.Cos(pel.dStartAng);
				sact = Math.Sin(pel.dStartAng);

				x0 = pel.dra * cact;
				y0 = pel.drb * sact;
				x1 = pel.PtC.dx + x0 * crot - y0 * srot;
				y1 = pel.PtC.dy + x0 * srot + y0 * crot;
				cact = Math.Cos(pel.dStartAng + pel.dAng);
				sact = Math.Sin(pel.dStartAng + pel.dAng);
				x0 = pel.dra * cact;
				y0 = pel.drb * sact;
				x2 = pel.PtC.dx + x0 * crot - y0 * srot;
				y2 = pel.PtC.dy + x0 * srot + y0 * crot;

				// l'entita` finale diventa un segmento 
				pseg.dptPunto1.dx = x1;
				pseg.dptPunto2.dx = x2;
				pseg.dptPunto1.dy = y1;
				pseg.dptPunto2.dy = y2;
				return 0;
			}

			// terzo caso: archi di cerchio 
			if (MathUtil.gEqual(pel.dra, pel.drb))
			{
				// la nuova entita` sara` un arco di cerchio semplice 
				MathUtil.DBL_ARC ar = new MathUtil.DBL_ARC();
				ar.PtC.dx = pel.PtC.dx;
				ar.PtC.dy = pel.PtC.dy;
				ar.dr = (pel.dra + pel.drb) / 2.0;
				ar.dStartAng = pel.dStartAng + pel.dInc;
				ar.dAng = pel.dAng;

				parc.Add(ar);

				return 1;
			}

			// quarto caso: arco di cerchio in simmetria orizzontale (x=-x) 
			if (pel.dra < 0 && MathUtil.gEqual(-pel.dra, pel.drb))
			{
				// la nuova entita` sara` un arco di cerchio semplice 
				MathUtil.DBL_ARC ar = new MathUtil.DBL_ARC();
				ar.PtC.dx = pel.PtC.dx;
				ar.PtC.dy = pel.PtC.dy;
				ar.dr = (Math.Abs(pel.dra) + Math.Abs(pel.drb)) / 2.0;
				if ((alfa = pel.dStartAng + pel.dInc) < Math.PI)
					alfa = Math.PI - alfa;
				else
					alfa = 3 * Math.PI - alfa;
				ar.dStartAng = alfa;
				ar.dAng = -pel.dAng;

				parc.Add(ar);
				return 1;

			}

			// caso generale: si tratta di una vera ellisse 
			crot = Math.Cos(pel.dInc + pel.dAng);
			srot = Math.Sin(pel.dInc + pel.dAng);

			// considero l'ellisse traslata in (0, 0) e senza rotazione e antioraria 
			el = pel;
			el.PtC.dx = 0;
			el.PtC.dy = 0;
			el.dInc = 0;
			el.dra = Math.Abs(el.dra);
			el.drb = Math.Abs(el.drb);
			if (el.dAng < 0)
			{
				el.dStartAng += el.dAng;
				el.dAng = -el.dAng;
			}

			// ampiezza complessiva 
			dAng = el.dAng;
			np = (int)(MathUtil.gdGradi(dAng) * PUNTIPERGRADO);

			// calcolo del numero di punti di espansione 
			if (el.dra < el.drb)
			{
				rapp = el.drb / el.dra;
				npmin = (int)(dAng * el.dra / mseg);
				npmax = (int)(dAng * el.drb / mseg);
			}
			else
			{
				rapp = el.dra / el.drb;
				npmin = (int)(dAng * el.drb / mseg);
				npmax = (int)(dAng * el.dra / mseg);
			}
			numpoints = np;

			// calcolo lo sviluppo dell'arco di ellisse 
			PntExpandArcD(ref el, ref p1d, numpoints);

			// centri correttamente calcolati: esegue l'espansione in archi 
			contacerchi = Interpola(ref p1d, ref psai, numpoints);
			
			// se devo cambiare segno al primo asse (x=-x) 
			if (pel.dra < 0)
			{
				for (i = 0; i < contacerchi; i++)
				{
					psc = psai[i];

					pa.PtC.dx = -psc.PtC.dx;
					pa.PtC.dy = psc.PtC.dy;
					pa.dr = psc.dr;
					if ((alfa = psc.dStartAng) < Math.PI)
						alfa = Math.PI - alfa;
					else
						alfa = 3 * Math.PI - alfa;
					pa.dStartAng = alfa;
					pa.dAng = -psc.dAng;

					parc.Add(pa);
				}
			}
			else
			{
				for (i = 0; i < psai.Count; i++)
				{
					MathUtil.DBL_ARC ar = new MathUtil.DBL_ARC();
					ar.dAng = psai[i].dAng;
					ar.dStartAng = psai[i].dStartAng;
                    ar.dr = psai[i].dr;
                    ar.PtC.dx = psai[i].PtC.dx;
					ar.PtC.dy = psai[i].PtC.dy;

					parc.Add(ar);
				}
			}

			// applico la rotazione 
			if (!MathUtil.IsZero(pel.dInc))
			{
				for (i = 0; i < contacerchi; i++)
				{
					pa = parc[i];

					x = pa.PtC.dx;
					y = pa.PtC.dy;
					pa.PtC.dx = x * crot - y * srot;
					pa.PtC.dy = x * srot + y * crot;
					pa.dStartAng += pel.dInc;

					parc[i] = pa;
				}
			}

			// applico la traslazione 
			if (!MathUtil.IsZero(pel.PtC.dx) || !MathUtil.IsZero(pel.PtC.dy))
			{				
				for (i = 0; i < contacerchi; i++)
				{
					pa = parc[i];
					pa.PtC.dx += pel.PtC.dx;
					pa.PtC.dy += pel.PtC.dy;
					parc[i] = pa;
				}
			}

			// se bisogna invertire gli angoli 
			if (pel.dAng < 0)
			{
				for (i = contacerchi - 1; i >= 0; i--)
				{
					pa = parc[i];
					pa.dStartAng += pa.dAng;
					pa.dAng = -pa.dAng;
					parc[i] = pa;
				}
			}

			for (i = 0; i < contacerchi; i++)
			{
				pa = parc[i];
				// normalizzazione dell'angolo 
				pa.dStartAng = MathUtil.Angolo_0_2PI(pa.dStartAng);
				parc[i] = pa;
			}

			// numero di cerchi allocati 
			return contacerchi;
		}

		/// <summary>
		/// conversione di un arco di ellisse in una serie di archi
		/// di cerchio; l'array in ingresso deve essere allocato   
		/// dal chiamante.                                         
		/// </summary>
		/// <param name="pel">struttura che descrive l'ellisse</param>
		/// <param name="parc">lista di archi ottenuti</param>
		/// <param name="pseg">eventuale segmento</param>
		/// <returns>
		/// = -2   = errore interno.                          
		/// = -1   = arco con ampiezza nulla.                 
		/// = 0    = l'arco e` stato trasformato in segmento. 
		/// > 0    = numero di archi ottenuti.                
		/// </returns>
		public static int conv_ell_arc2(ref MathUtil.DBL_ELL pel, ref List<MathUtil.DBL_ARC> parc, ref MathUtil.DBL_LINE pseg)
		{
            return conv_ell_arc(ref pel, ref parc, ref pseg);
		}

		/// <summary>
		/// verifica se il punto specificato appartiene ad
		/// un'ellisse; in caso positivo ritorna l'angolo.
		/// </summary>
		/// <param name="ell">ellisse</param>
		/// <param name="punto">punto da controllare</param>
		/// <param name="angolo">angolo eventualmente determinato</param>
		/// <returns>
		/// 0  = il punto no appartiene.
		/// 1  = il punto appartiene.   
		/// </returns>
		public static int punto_in_ell2(ref MathUtil.DBL_ELL ell, ref MathUtil.DBL_POINT punto, ref double angolo)
		{
			double vector, mina, maxa, A, B, C, D;
			double sin_b, cos_b;
			MathUtil.DBL_POINT norm;

			// punto normalizzato 
			norm.dx = punto.dx - ell.PtC.dx;
			norm.dy = punto.dy - ell.PtC.dy;

			// parametri delle equazioni parametriche in questo caso 
			A = ell.dra * Math.Cos(ell.dInc);
			B = ell.drb * Math.Cos(ell.dInc);
			C = ell.dra * Math.Sin(ell.dInc);
			D = ell.drb * Math.Sin(ell.dInc);

			// calcolo del cos-beta e del sin-beta, come soluzione del sistema parametrico 
			sin_b = (A * norm.dy - C * norm.dx) / (A * B + C * D);
			cos_b = (norm.dx + D * sin_b) / A;

			// valuto i quadranti 
			vector = Math.Atan2(sin_b, cos_b);
			if (vector < 0)
				vector += 2 * Math.PI;

			// estremi dell'arco 
			if (ell.dAng > 0)
			{
				mina = ell.dStartAng;
				maxa = ell.dStartAng + ell.dAng;
			}
			else
			{
				maxa = ell.dStartAng;
				mina = ell.dStartAng + ell.dAng;
			}
			if (mina < 0)
			{
				mina += 2 * Math.PI;
				maxa += 2 * Math.PI;
			}
			if (maxa > 2 * Math.PI)
			{
				mina -= 2 * Math.PI;
				maxa -= 2 * Math.PI;
			}

			// confronto 
			//DANIELE: introdotta l'approssimazione nel controllo dei limiti
			maxa += MathUtil.FLT_EPSILON;
			mina -= MathUtil.FLT_EPSILON;

			if (MathUtil.IsGT(vector, maxa))
				vector -= 2 * Math.PI;
			else
				if (MathUtil.IsLT(vector, mina))
					vector += 2 * Math.PI;

			angolo = vector;

			return (MathUtil.IsLT(vector, mina) || MathUtil.IsGT(vector, maxa) ? 0 : 1);
		}

		/// <summary>
		/// calcola ilnuovo cerchio da inserire nella lista
		/// </summary>
		/// <param name="p1d">lista dei punti da generare</param>
		/// <param name="psai">lista di cerchi allocata</param>
		/// <param name="inir">punto a partire dal quale si deve partire</param>
		/// <param name="finr">ultimo punto da considerare</param>
		/// <param name="narc">primo arco libero</param>
		private static void InserisciCerchio(ref List<MathUtil.DBL_POINT> p1d, ref List<MathUtil.DBL_ARC> psai, int inir, int finr, int narc)
		{
			double r, x1, y1, x2, y2, x3, y3;
			MathUtil.DBL_POINT pi = new MathUtil.DBL_POINT();
			int medr, intermedio;
			bool antiorario;
			double ang1, ang2, ai, af;
			double xr1, yr1, xr2, yr2, xs1, ys1, xs2, ys2;
			double a, b, c, d, dx1, dx2, dy1, dy2;
			bool x12t, x13t, y12t, y13t;

			// salvo il punto iniziale e finale 
			x1 = p1d[inir].dx;
			y1 = p1d[inir].dy;
			x3 = p1d[finr].dx;
			y3 = p1d[finr].dy;

			// determino il punto medio 
			medr = (finr - inir) / 2 + inir;
			x2 = p1d[medr].dx;
			y2 = p1d[medr].dy;

			// calcolo l'arco di cerchio che passa per i tre punti 
			y12t = MathUtil.IsZero(dy1 = Math.Abs(y1 - y2));
			y13t = MathUtil.IsZero(dy2 = Math.Abs(y1 - y3));
			x12t = (MathUtil.IsZero(dx1 = Math.Abs(x1 - x2)) || (!y12t && (MathUtil.IsZero(dx1 / dy1) || MathUtil.IsZero(dy1 / dx1))));
			x13t = (MathUtil.IsZero(dx2 = Math.Abs(x1 - x3)) || (!y13t && (MathUtil.IsZero(dx2 / dy2) || MathUtil.IsZero(dy2 / dx2))));

			// in linea verticali ? 
			if (x12t && x13t)
				r = 0;
			else
			{
				xr2 = x2;
				yr2 = y2;
				xs2 = x3;
				ys2 = y3;

				if (x12t)
				{
					xr1 = x3;
					yr1 = y3;
					xs1 = x1;
					ys1 = y1;
				}
				else
				{
					if (x13t)
					{
						xr1 = x1;
						yr1 = y1;
						xs1 = x2;
						ys1 = y2;
					}
					else
					{
						xr1 = x1;
						yr1 = y1;
						xs1 = x1;
						ys1 = y1;
					}
				}

				a = (yr1 - yr2) / (xr2 - xr1);
				b = (xr2 * xr2 + yr2 * yr2 - xr1 * xr1 - yr1 * yr1) / (2 * (xr2 - xr1));
				c = (ys1 - ys2) / (xs2 - xs1);
				d = (xs2 * xs2 + ys2 * ys2 - xs1 * xs1 - ys1 * ys1) / (2 * (xs2 - xs1));

				if (MathUtil.IsEQ(a, c))
					r = 0;
				else
				{
					pi.dy = ((d - b) / (a - c));
					pi.dx = ((pi.dy) * c + d);
					r = Math.Sqrt((x1 - pi.dx) * (x1 - pi.dx) + (y1 - pi.dy) * (y1 - pi.dy));
				}
			}

			// definzione del cerchio 
			MathUtil.DBL_ARC ar = new MathUtil.DBL_ARC();
			ar.PtC.dx = pi.dx;
			ar.PtC.dy = pi.dy;
			ar.dr = r;

			ai = Math.Atan2(y1 - pi.dy, x1 - pi.dx);
			af = Math.Atan2(y3 - pi.dy, x3 - pi.dx);

			// normalizzazione angoli 
			if (ai < 0.0)
				ai += 2.0 * Math.PI;
			if (af < 0.0)
				af += 2.0 * Math.PI;

			// indice del punto intermedio 
			intermedio = (inir + finr) / 2;

			// direzione del segmento intero e del semisegmento 
			ang1 = Math.Atan2(y3 - y1, x3 - x1);
			ang2 = Math.Atan2(y2 - y1, x2 - x1);

			// verifica sul verso dell'arco 
			if (ang1 < -Math.PI / 2 && ang2 > 0.0)
				ang1 += 2.0 * Math.PI;
			if (ang2 < -Math.PI / 2 && ang1 > 0.0)
				ang2 += 2.0 * Math.PI;
			antiorario = ang1 > ang2;

			// completo la normalizzazione (ai sempre < af) 
			if (antiorario && af < ai)
				af += 2.0 * Math.PI;
			if (!antiorario && af > ai)
				ai += 2.0 * Math.PI;

			// conversione in angolo iniziale e ampiezza 
			ar.dStartAng = ai;
			ar.dAng = af - ai;
            psai.Add(ar);
        }

		/// <summary>
		/// esegue l'interpolazione dei punti generati a partire dall'espansione dell'arco di ellisse, utilizzando una lista di 
		/// archi di cerchio.                                         
		/// </summary>
		/// <param name="p1d">lista dei punti da generare</param>
		/// <param name="psai">lista di cerchi allocata</param>
		/// <param name="np">numero punti di espansione</param>
		/// <returns>
		/// 0 = sviluppo completato.                             
		/// 1 = e` stata trovata una condizione di parallelismo. 
		/// </returns>
		private static int Interpola(ref List<MathUtil.DBL_POINT> p1d, ref List<MathUtil.DBL_ARC> psai, int np)
		{
			MathUtil.DBL_POINT p0, p1, p2;
			int i, inir, finr, inirp, contacerchi;
			double aprec, dir0, dir1, ddir, ddirp = 0;
			
			// condizioni iniziali 
			inir = finr = inirp = 0;
			contacerchi = 0;

			// calcolo della direzione del segmento precedente (normalizzato 0,2PI) 
			aprec = Math.Atan2(p1d[1].dy - p1d[0].dy, p1d[1].dx - p1d[0].dx);
			while (aprec < 0.0)
				aprec += Math.PI;

			// loop per terne di punti 
			for (i = 0; i < np - 1; i++)
			{
				p0 = p1d[i];
				p1 = p1d[i+1];
				p2 = p1d[i+2];

				// valutazione differenza direzione 
				dir0 = MathUtil.gdDirezione(p0.dx, p0.dy, p1.dx, p1.dy);
				dir1 = MathUtil.gdDirezione(p1.dx, p1.dy, p2.dx, p2.dy);
				ddir = Math.Abs(dir1 - dir0);
				if (i == 0)
				{
					ddirp = ddir;
					continue;
				}

				// se la derivata seconda e` limitata 
				if (Math.Abs(ddir - ddirp) > SOGLIACAMBIODIR)
				{

					// salvo l'indice dell'ultimo punto con derivata seconda ok 
					finr = i;

					// se il numero di punti e` sufficiente e non ho esaurito i cerchi possibili 
					if (i > inir + MINPUNTIARCO && contacerchi < MAXCERCHI - 1)
					{

						// calcola il cerchio 
						inirp = inir;
						InserisciCerchio(ref p1d, ref psai, inir, finr, contacerchi);

						// angolo finale del cerchio calclolato 
						aprec = psai[contacerchi].dStartAng + psai[contacerchi].dAng;
						contacerchi++;

						// normalizzato [0, 2PI) 
						while (aprec < 0.0)
							aprec += Math.PI;
						while (aprec > Math.PI)
							aprec -= Math.PI;

						// salvo l'indice di inizio scansione 
						finr = inir = i;
						ddirp = ddir;
					}
				}
			}

			// condizione per un eventuali ultimo cerchio 
			if (contacerchi < MAXCERCHI - 1)
			{
				// controllo se i punti che rimangono sono sufficienti per un nuovo arco 
				if ((i - inir) > MINPUNTIARCO)
				{
					// arco addizionale 
					InserisciCerchio(ref p1d, ref psai, inir, finr = np, contacerchi);
				}
				else
				{
					// modifico l'ultimo arco 
					if (contacerchi > 0)
						contacerchi--;
					InserisciCerchio(ref p1d, ref psai, inirp, finr = np, contacerchi);
				}
				contacerchi++;
			}
			return contacerchi;
		}

		/// <summary>
		/// calcola i punti di espasione a partire dall'arco di
		/// ellisse specificato.
		/// 
		/// Nota: nell'espansione si sfrutta la formula che definisce il seno e il
		///       coseno di una somma:                                            
		///                                                                       
		///       cos(a+b) = cos(a)*cos(b) - sin(a)*sin(b).                       
		///       sin(a+b) = sin(a)*cos(a) + sin(b)*cos(a).                       
		/// </summary>
		/// <param name="pell">arco di ellisse originale</param>
		/// <param name="mp">lista dei punti da generare</param>
		/// <param name="n">numero massimo di punti da generare (-1, perche` l'ultimo e` forzato).</param>
		private static void PntExpandArcD(ref MathUtil.DBL_ELL pell, ref List<MathUtil.DBL_POINT> mp, int n)
		{
			double temp, a = pell.dra, b = pell.drb;
			double cx = pell.PtC.dx, cy = pell.PtC.dy;
			double cgap, sgap, cact, sact;
			double auscact, aussact, swe;
			MathUtil.DBL_POINT p;
			int i;

			// ampiezza dell'arco 
			swe = Math.Abs(pell.dAng);
			temp = swe / (double)n;

			// coseno e seno dell'angolo incrementale 
			cgap = Math.Cos(temp);
			sgap = Math.Sin(temp);

			// coseno e seno dell'angolo iniziziale 
			cact = Math.Cos(pell.dStartAng);
			sact = Math.Sin(pell.dStartAng);

			// punto iniziale dell'ellisse 
			p.dx = cx + a * cact;
			p.dy = cy + b * sact;
            mp.Add(p);
			
			// loop di espansione dei punti rimanenti 
			for (i = 1; i < n; i++)
			{
				// coseno e seno dell'angolo relativo al successivo step 
				auscact = cact * cgap - sact * sgap;
				aussact = cact * sgap + sact * cgap;
				cact = auscact;
				sact = aussact;

				// calcolo il prosimo punto nell'espansione 
				p.dx = cx + a * cact;
				p.dy = cy + b * sact;
				mp.Add(p);
			}

			// impongo l'ultimo punto
			cact = Math.Cos(pell.dStartAng + pell.dAng);
			sact = Math.Sin(pell.dStartAng + pell.dAng);

			p.dx = cx + a * cact;
			p.dy = cy + b * sact;
			mp.Add(p);
		}
	}
}
