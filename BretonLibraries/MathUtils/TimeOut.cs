using System;
using System.Collections.Generic;
using System.Text;

namespace Breton.MathUtils
{
	///******************************************************************************
	/// <summary>
	/// Classe che implementa dei timeout
	/// </summary>
	///******************************************************************************
	public class TimeOut
	{
		#region Variables

		private TimeSpan mTimeOut;
		private DateTime mEndTimeout = DateTime.MinValue;

		#endregion


		#region Constructors

		///******************************************************************************
		/// <summary>
		/// Costruttore classe
		/// </summary>
		/// <param name="timeout">intervallo di timeout</param>
		///******************************************************************************
		public TimeOut(TimeSpan timeout)
		{
			mTimeOut = timeout;
		}
		public TimeOut()
			: this(TimeSpan.Zero)
		{ }

		#endregion


		#region Properties

		///******************************************************************************
		/// <summary>
		/// Restituisce il valore di timeout
		/// </summary>
		///******************************************************************************
		public TimeSpan gTimeOut
		{
			get { return mTimeOut; }
		}

		///******************************************************************************
		/// <summary>
		/// Restituisce la data/ora di scadenza timeout
		/// </summary>
		///******************************************************************************
		public DateTime gEndTimeout
		{
			get { return mEndTimeout; }
		}

		///******************************************************************************
		/// <summary>
		/// Restituisce l'intervallo di tempo che manca alla scadenza del timeout
		/// </summary>
		///******************************************************************************
		public TimeSpan TimeToExpired
		{
			get
			{
				if (IsSet && IsStarted && !IsExpired)
					return mEndTimeout - DateTime.Now;
				
				return TimeSpan.Zero;
			}
		}

		///******************************************************************************
		/// <summary>
		/// Ritorna true se il timeout � impostato
		/// </summary>
		///******************************************************************************
		public bool IsSet
		{
			get { return mTimeOut > TimeSpan.Zero; }
		}

		///******************************************************************************
		/// <summary>
		/// Ritorna true se il timeout � avviato
		/// </summary>
		///******************************************************************************
		public bool IsStarted
		{
			get { return IsSet && mEndTimeout > DateTime.MinValue; }
		}

		///******************************************************************************
		/// <summary>
		/// Ritorna true se il timeout � scaduto
		/// </summary>
		///******************************************************************************
		public bool IsExpired
		{
			get { return IsStarted && DateTime.Now >= mEndTimeout; }
		}

		#endregion


		#region Public Methods

		///******************************************************************************
		/// <summary>
		/// Ferma il timeout
		/// </summary>
		///******************************************************************************
		public void Stop()
		{
			mEndTimeout = DateTime.MinValue;
		}

		///******************************************************************************
		/// <summary>
		/// Avvia il timeout
		/// </summary>
		/// <returns>
		///		true	timeout avviato
		///		false	impossibile avviare il timeout
		/// </returns>
		///******************************************************************************
		public bool Start()
		{
			if (IsSet && !IsStarted)
			{
				mEndTimeout = DateTime.Now + mTimeOut;
				return true;
			}

			return false;
		}

		///******************************************************************************
		/// <summary>
		/// Riavvia il timeout
		/// </summary>
		/// <returns>
		///		true	timeout avviato
		///		false	impossibile riavviare il timeout
		/// </returns>
		///******************************************************************************
		public bool ReStart()
		{
			Stop();
			return Start();
		}

		///******************************************************************************
		/// <summary>
		/// Imposta il valore del timeout
		/// </summary>
		/// <param name="newTimeout">nuovo valore di timeout</param>
		///******************************************************************************
		public void Set(TimeSpan newTimeout)
		{
			if (IsStarted)
				Stop();

			mTimeOut = newTimeout;
		}

		///******************************************************************************
		/// <summary>
		/// Resetta il timeout
		/// </summary>
		///******************************************************************************
		public void Reset()
		{
			Stop();
			mTimeOut = TimeSpan.Zero;
		}

		#endregion
	}
}
