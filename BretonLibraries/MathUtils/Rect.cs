using System;

namespace Breton.MathUtils
{
	public class Rect
	{
		#region Variables

		private double mLeft;
		private double mTop;
		private double mRight;
		private double mBottom;
		
		#endregion


		#region Constructors

        ///*********************************************************************
        /// <summary>
        /// Costruisce il rettangolo nullo
        /// </summary>
        ///*********************************************************************
		public Rect(): this(0d, 0d, 0d, 0d)
		{
		}

        ///*********************************************************************
        /// <summary>
        /// Costruisce un rettangolo con limiti invertiti, da usare per le
        /// ricerche del rettangolo di contenimento
        /// </summary>
        ///*********************************************************************
        public Rect(double val)
        {
            mLeft = val;
            mRight = -val;
            mTop = val;
            mBottom = -val;
        }

        ///*********************************************************************
        /// <summary>
        /// Costruttore del rettangolo
        /// </summary>
        /// <param name="x1">ascissa primo vertice</param>
        /// <param name="y1">ordinata primo vertice</param>
        /// <param name="x2">ascissa secondo vertice</param>
        /// <param name="y2">ordinata secondo vertice</param>
        ///*********************************************************************
        public Rect(double x1, double y1, double x2, double y2)
		{
			mLeft = Math.Min(x1, x2);
			mRight = Math.Max(x1, x2);
			mTop = Math.Min(y1, y2);
			mBottom = Math.Max(y1, y2);
		}

        ///*********************************************************************
        /// <summary>
        /// Costruisce un rettangolo con origine 0,0
        /// </summary>
        /// <param name="dimX">dimensione X (larghezza) del rettangolo</param>
        /// <param name="dimY">dimensione Y (altezza) del rettangolo</param>
        ///*********************************************************************
        public Rect(double dimX, double dimY)
            : this(0d, 0d, dimX, dimY)
        { }

        ///*********************************************************************
        /// <summary>
        /// Costruisce il rettangolo come copia di un altro rettangolo
        /// </summary>
        /// <param name="r">rettangolo origine</param>
        ///*********************************************************************
        public Rect(Rect r)
            : this(r.mLeft, r.mTop, r.mRight, r.mBottom)
        { }

		#endregion


		#region Properties

		/// <summary>
		/// Restituisce la quota Left del rettangolo (Xmin)
		/// </summary>
		public double Left
		{
			get { return mLeft; }
		}

		/// <summary>
		/// Restituisce la quota Right del rettangolo (Xmax)
		/// </summary>
		public double Right
		{
			get { return mRight; }
		}

		/// <summary>
		/// Restituisce la quota Top del rettangolo (Ymin)
		/// </summary>
		public double Top
		{
			get { return mTop; }
		}

		/// <summary>
		/// Restituisce la quota Bottom del rettangolo (Ymax)
		/// </summary>
		public double Bottom
		{
			get { return mBottom; }
		}

		/// <summary>
		/// Restituisce la quota minima di X del rettangolo
		/// </summary>
		public double MinX
		{
			get { return mLeft; }
		}

		/// <summary>
		/// Restituisce la quota massima di X del rettangolo
		/// </summary>
		public double MaxX
		{
			get { return mRight; }
		}

		/// <summary>
		/// Restituisce la quota minima di Y del rettangolo
		/// </summary>
		public double MinY
		{
			get { return mTop; }
		}

		/// <summary>
		/// Restituisce la quota massima di Y del rettangolo
		/// </summary>
		public double MaxY
		{
			get { return mBottom; }
		}

		/// <summary>
		/// Restituisce la base del rettangolo
		/// </summary>
		public double Width
		{
			get { return mRight - mLeft; }
		}

		/// <summary>
		/// Restituisce l'altezza del rettangolo
		/// </summary>
		public double Height
		{
			get { return mBottom - mTop; }
		}

		/// <summary>
		/// Restituisce la dimensione X del rettangolo
		/// </summary>
		public double DeltaX
		{
			get { return mRight - mLeft; }
		}

		/// <summary>
		/// Restituisce la dimensione Y del rettangolo
		/// </summary>
		public double DeltaY
		{
			get { return mBottom - mTop; }
		}

		/// <summary>
		/// Restituisce l'area del rettangolo
		/// </summary>
		public double Surface
		{
			get { return Width * Height; }
		}

        /// <summary>
        /// Restituisce la lunghezza della diagonale del rettangolo
        /// </summary>
        public double Diagonal
        {
            get { return Math.Sqrt(Width * Width + Height * Height); }
        }

		#endregion


		#region Operator overloading

		/// **************************************************************************
		/// <summary>
		/// Operatore +, somma fra due rettangoli
		/// </summary>
		/// <param name="r1">rettangolo 1</param>
		/// <param name="r2">rettangolo 2</param>
		/// <returns>rettangolo risultante</returns>
		/// **************************************************************************
		public static Rect operator +(Rect r1, Rect r2)
		{
			if (r1 == null && r2 == null)
				return null;

			if (r1 == null)
				return new Rect(r2);
		    
			if (r2 == null)
				return new Rect(r1);

			return (new Rect( Math.Min(r1.mLeft, r2.mLeft), Math.Min(r1.mTop, r2.mTop),
									Math.Max(r1.mRight, r2.mRight), Math.Max(r1.mBottom, r2.mBottom)));
		}

		#endregion


		#region Public Methods

		/// **************************************************************************
		/// <summary>
		/// Somma al rettangolo un offset X, Y
		/// </summary>
		/// <param name="x">offset X</param>
		/// <param name="y">offset Y</param>
		/// **************************************************************************
		public void AddOffset(double x, double y)
		{
			mLeft += x;
			mTop += y;
			mRight += x;
			mBottom += y;
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna un rettangolo esteso in x e y del valore specificato
		/// </summary>
		/// <param name="x">estensione in x</param>
		/// <param name="y">estensione in y</param>
		/// <returns>
		///		Rettangolo esteso
		/// </returns>
		/// **************************************************************************
		public Rect ExtendRect(double x, double y)
		{
			return new Rect(mLeft - x / 2d, mTop - y / 2d, mRight + x / 2d, mBottom + y / 2d);
		}

		/// **************************************************************************
		/// <summary>
		/// Verifica se un punto � interno al rettangolo
		/// </summary>
		/// <param name="x">ascissa punto</param>
		/// <param name="y">ordinata punto</param>
		/// <returns>
		///		true	punto interno
		///		false	punto esterno
		///	</returns>
		/// **************************************************************************
		public bool IsInternalPoint(double x, double y)
		{
			return (x >= mLeft && x < mRight) && (y >= mTop && y < mBottom);
		}

        /// **************************************************************************
        /// <summary>
        /// ritorna il rettangolo ottenuto come unione di quello corrente e di quello
        /// indicato.
        /// </summary>
        /// <param name="r">secondo rettangolo da considerare</param>
        /// <returns>
        ///		rettangolo unione dei due
        ///	</returns>
        /// **************************************************************************
        public Rect Union(Rect r)
        {
            Rect union = new Rect( Math.Min(MinX, r.MinX),
                                   Math.Min(MinY, r.MinY),
                                   Math.Max(MaxX, r.MaxX),
                                   Math.Max(MaxY, r.MaxY)
                                 );

            return union;
        }

        /// **************************************************************************
        /// <summary>
        /// ritorna il rettangolo ottenuto come intersezione di quello corrente e di 
        /// quello indicato.
        /// </summary>
        /// <param name="r">secondo rettangolo da considerare</param>
        /// <returns>
        ///		rettangolo intersezione dei due
        ///		null se l'intersezione e' nulla
        ///	</returns>
        /// **************************************************************************
        public Rect Intersection(Rect r)
        {
            if (MinX > r.MaxX ||
                MaxX < r.MinX ||
                MinY > r.MaxY ||
                MaxY < r.MinY)
                return null;

            double x1 = Math.Max(MinX, r.MinX);
            double y1 = Math.Max(MinY, r.MinY);
            double x2 = Math.Min(MaxX, r.MaxX);
            double y2 = Math.Min(MaxY, r.MaxY);

            Rect intersection = new Rect( x1, y1, x2, y2);

            return intersection;
        }

        #endregion

	}
}
