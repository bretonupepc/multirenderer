using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Breton.MathUtils
{
	public class AssemblyUtil
	{

		#region Assembly Attribute Accessors

		/// **************************************************************************
		/// <summary>
		/// Ritorna il titolo dell'assembly chiamante
		/// </summary>
		/// <returns>titolo</returns>
		/// **************************************************************************
		public static string AssemblyTitle()
		{
			return AssemblyTitle(Assembly.GetCallingAssembly());
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna il titolo dell'assembly
		/// </summary>
		/// <param name="assembly">assembly</param>
		/// <returns>titolo</returns>
		/// **************************************************************************
		public static string AssemblyTitle(Assembly assembly)
		{
			// Get all Title attributes on this assembly
			object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
			// If there is at least one Title attribute
			if (attributes.Length > 0)
			{
				// Select the first one
				AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
				// If it is not an empty string, return it
				if (titleAttribute.Title != "")
					return titleAttribute.Title;
			}
			// If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
			return System.IO.Path.GetFileNameWithoutExtension(assembly.CodeBase);
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna la versione dell'assembly chiamante
		/// </summary>
		/// <returns>versione</returns>
		/// **************************************************************************
		public static string AssemblyVersion()
		{
			return AssemblyVersion(Assembly.GetCallingAssembly());
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna la versione dell'assembly
		/// </summary>
		/// <param name="assembly">assembly</param>
		/// <returns>versione</returns>
		/// **************************************************************************
		public static string AssemblyVersion(Assembly assembly)
		{
			return assembly.GetName().Version.ToString();
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna la descrizione dell'assembly chiamante
		/// </summary>
		/// <returns>descrizione</returns>
		/// **************************************************************************
		public static string AssemblyDescription()
		{
			return AssemblyDescription(Assembly.GetCallingAssembly());
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna la descrizione dell'assembly
		/// </summary>
		/// <param name="assembly">assembly</param>
		/// <returns>descrizione</returns>
		/// **************************************************************************
		public static string AssemblyDescription(Assembly assembly)
		{
			// Get all Description attributes on this assembly
			object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
			// If there aren't any Description attributes, return an empty string
			if (attributes.Length == 0)
				return "";
			// If there is a Description attribute, return its value
			return ((AssemblyDescriptionAttribute)attributes[0]).Description;
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna il prodotto dell'assembly chiamante
		/// </summary>
		/// <returns>prodotto</returns>
		/// **************************************************************************
		public static string AssemblyProduct()
		{
			return AssemblyProduct(Assembly.GetCallingAssembly());
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna il prodotto dell'assembly
		/// </summary>
		/// <param name="assembly">assembly</param>
		/// <returns>prodotto</returns>
		/// **************************************************************************
		public static string AssemblyProduct(Assembly assembly)
		{
			// Get all Product attributes on this assembly
			object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
			// If there aren't any Product attributes, return an empty string
			if (attributes.Length == 0)
				return "";
			// If there is a Product attribute, return its value
			return ((AssemblyProductAttribute)attributes[0]).Product;
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna il copyright dell'assembly chiamante
		/// </summary>
		/// <returns>copyright</returns>
		/// **************************************************************************
		public static string AssemblyCopyright()
		{
			return AssemblyCopyright(Assembly.GetCallingAssembly());
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna il copyright dell'assembly
		/// </summary>
		/// <param name="assembly">assembly</param>
		/// <returns>copyright</returns>
		/// **************************************************************************
		public static string AssemblyCopyright(Assembly assembly)
		{
			// Get all Copyright attributes on this assembly
			object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
			// If there aren't any Copyright attributes, return an empty string
			if (attributes.Length == 0)
				return "";
			// If there is a Copyright attribute, return its value
			return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna la compagnia dell'assembly chiamante
		/// </summary>
		/// <returns>compagnia</returns>
		/// **************************************************************************
		public static string AssemblyCompany()
		{
			return AssemblyCompany(Assembly.GetCallingAssembly());
		}

		/// **************************************************************************
		/// <summary>
		/// Ritorna la compagnia dell'assembly
		/// </summary>
		/// <param name="assembly">assembly</param>
		/// <returns>compagnia</returns>
		/// **************************************************************************
		public static string AssemblyCompany(Assembly assembly)
		{
			// Get all Company attributes on this assembly
			object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
			// If there aren't any Company attributes, return an empty string
			if (attributes.Length == 0)
				return "";
			// If there is a Company attribute, return its value
			return ((AssemblyCompanyAttribute)attributes[0]).Company;
		}

		#endregion
	}
}
