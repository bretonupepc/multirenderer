//#define DONT_UPDATE_BLOB_FIELD

using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using System.IO;
using System.Collections;
using TraceLoggers;

namespace DbAccess
{
    internal class DBI_Mutex
    {
        private UInt32 _LockCounter = 0;
        private Mutex _Mutex;

        public DBI_Mutex()
        {
            _Mutex = new Mutex();
        }

        public DBI_Mutex(Boolean value)
        {
            _Mutex = new Mutex(value);
        }

        public bool WaitOne()
        {
            bool rc = _Mutex.WaitOne();
            if (rc)
                ++_LockCounter;
            return rc;
        }

        public void ReleaseMutex()
        {
            if (_LockCounter > 0)
            {
                _Mutex.ReleaseMutex();
                --_LockCounter;
            }
        }

        public void ReleaseMutexAll()
        {
            while (_LockCounter > 0)
                ReleaseMutex();
        }
    }

	/// <summary>
	/// Summary description for DbInterface.
	/// </summary>
	public partial class DbInterface: IDisposable
	{
		public const int DEADLOCK_RETRY = 3; // EX DEADLOCK_RETRY
		public const int DEADLOCK_WAIT = 1000;
		public const int TIMEOUT_WAIT = 5000;
		private const int KILOBYTE = 1024;
		private const int BUFFER_LENGTH = 512 * KILOBYTE;

		public enum REQUERY_RC
		{
			REQUERY_RC_OK,
			REQUERY_RC_FAILED,
			REQUERY_RC_ABORT
		}
		
		private OleDbConnection mDbConnection;
		private OleDbCommand mDbCommand;

        private DBI_Mutex mMutex;
		private Mutex mMutexTransaction;

		//Sezione messaggi per MsgBox
		private const string SECTION_MSG = "clsDbInterface";

		OleDbTransaction mTransaction = null;
		bool mActiveTransaction = false;

		private bool mDisposed = false;

		private bool mJetOLEDB = false;
		private bool mAceOLEDB = false;


		#region Constructors
		public DbInterface()
		{
			mDbConnection = new OleDbConnection();
			mDbCommand = new OleDbCommand();
            mMutex = new DBI_Mutex(false);
			mMutexTransaction = new Mutex(false);
			//mMutexTransaction = mMutex;
		} 
		#endregion

		#region Disposer

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!this.mDisposed)
			{
                if (disposing)
                {
                    if (mDbConnection != null)
                    {
                        Close();
                        mDbConnection = null;
                        mDbCommand = null;
                    }
                }

				try	{	mMutex.ReleaseMutex();	}
				catch	{}

				try	{	mMutexTransaction.ReleaseMutex();	}
				catch	{}

				mDisposed = true;
			}
		}


		#endregion

		public DateTime DateTimeNow
		{
			get 
			{
				OleDbDataReader dr;
				Requery("SELECT GETDATE()",out dr);

				dr.Read();

				return (DateTime)dr[0];
			}
		}

		public OleDbInfoMessageEventHandler InfoMessaggio
		{
			set 
			{ 
				mDbConnection.InfoMessage += value;
			}
		}


		//**************************************************************************
		// TransactionActive
		// Verifica se c'� una transazione aperta
		// Ritorna:
		//           TRUE  = transazione aperta
		//           FALSE = transazione chiusa
		//**************************************************************************
		public bool TransactionActive()
		{
			return (mActiveTransaction);
		}


		//**************************************************************************
		// Open()
		// Apertura del DataBase generale e recupero delle informazioni preliminari
		// Parametri:
		//           vsDbName        : nome del database
		//           vsServer        : nome del server
		// Ritorna:
		//           TRUE  = apertura eseguita
		//           FALSE = apertura non eseguita
		//**************************************************************************
		public bool Open(string dbName, string server)
		{
			string connString;

			try
			{
				//"Persist Security Info=False;Integrated Security=SSPI;database=northwind;server=mySQLServer"

				//Nome del server contenuto in un file UDL
				if (server.ToUpper().EndsWith(".UDL"))
				{
					//UDLConnectionString udl = new UDLConnectionString(ref server);
					//connString = udl.GetSQLConnectionString();
					connString = "File Name = " + server;
				}
	
				//Altrimenti
				else
				{
					//connString = "Provider=SQLOLEDB;Persist Security Info=False;Integrated Security=false;server=";
					connString = "Provider=SQLOLEDB.1;Persist Security Info=True;Data Source=";
					//Verifica il nome del server
					if (server == "")
						server = "localhost"; //Environment.MachineName;
					
					connString += server;
					connString += ";Initial Catalog=" + dbName;
					connString += "; User ID=sa;Password=\"\";";
					
				}
				mConnectionString = connString;
				
				mDbConnection.Open();
				if (mDbConnection.State == ConnectionState.Closed)
				{
					Close();
					return false;
				}
				
				mDbCommand.Connection = mDbConnection;
				mDbCommand.CommandType = CommandType.StoredProcedure;
				
				// Chiude la connessione
				Close();
				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("DbInterface: error opening connection", ex);
				Close();
				throw;
			}
		}

		//**************************************************************************
		// Open()
		// Apertura del DataBase generale e recupero delle informazioni preliminari
		// Parametri:
		//           vsConnectionString : stringa di connesione
		// Ritorna:
		//           TRUE  = apertura eseguita
		//           FALSE = apertura non eseguita
		//**************************************************************************
		public bool Open(string vsConnectionString )
		{
			try
			{
				mConnectionString = vsConnectionString;

				mDbConnection.Open();
				if (mDbConnection.State == ConnectionState.Closed)
				{
					Close();
					return false;
				}

				mDbCommand.Connection = mDbConnection;
				mDbCommand.CommandType = CommandType.StoredProcedure;

				// Chiude la connessione
				Close();
				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("DbInterface: error opening connection", ex);
				Close();
				throw;
			}
		}

		//**************************************************************************
		// Sub Close
		// Chiusura di tutto il database
		//**************************************************************************
		public void Close()
		{
			if (mDbConnection != null)

                // De Conti 20140802
                // aggiunto try/catch

			    try
			    {
			        if (mDbConnection.State != ConnectionState.Closed)
			        {
			            mDbConnection.Close();
			            mMutex.ReleaseMutexAll();
			        }
			    }
			    catch (Exception ex)
			    {
                    TraceLog.WriteLine("DbInterface:Close() error ", ex);
                }
		}
	
		//**************************************************************************
		// Function IsOpen
		// verifica se il DataBase e` in linea
		//**************************************************************************
		public bool IsOpen()
		{		
			return (mDbConnection.State != ConnectionState.Closed);
		}
	
		//**************************************************************************
		// string ConnectionString
		// Restituisce la stringa di connessione al database
		//**************************************************************************
		public string ConnectionString()
		{
			return mConnectionString;
		}

		///**************************************************************************
		/// <summary>
		/// Imposta/Restituisce la stringa di connessione
		/// </summary>
		///**************************************************************************
		private string mConnectionString
		{
			get { return mDbConnection.ConnectionString; }
			set
			{
				mDbConnection.ConnectionString = value;
				mJetOLEDB = (mDbConnection.ConnectionString.IndexOf("Jet.OLEDB") >= 0 ? true : false);
				mAceOLEDB = (mDbConnection.ConnectionString.IndexOf("ACE.OLEDB") >= 0 ? true : false);
			}
		}

		//**************************************************************************
		// string OleDbToDate
		// prepara la conversione in data, nel formato SQL, a partire dalla
		// data specificata
		// Parametri:
		//           reqDate	:  data da convertire
		// Ritorna:
		//           Stringa da utilizzare in una query SQL
		//**************************************************************************
		public string OleDbToDate(DateTime reqDate)
		{
			string date = reqDate.ToString("u");

			return ("{ts '" + date.Substring(0, date.Length-1) + "'}");
		}

		/// <summary>
		/// Read and return your Sql Version
		/// </summary>
		/// <param name="version">Version number</param>
		/// <param name="servicepack">Service pack</param>
		/// <param name="edition">Sql Server Edition</param>
		/// <returns></returns>
		public bool SqlVersion(out string version, out string servicepack, out string edition)
		{
			OleDbDataReader reader;
			version = "";
			servicepack = "";
			edition = "";

			try
			{
				Requery("SELECT SERVERPROPERTY('productversion'), SERVERPROPERTY ('productlevel'), SERVERPROPERTY ('edition')", out reader);

				while (reader.Read())
				{
					version = reader[0].ToString();
					servicepack = reader[1].ToString();
					edition = reader[2].ToString();
				}

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("DbInterface.SqlVersion Error.", ex);
				return false;
			}			
		}

		///**************************************************************************
		/// <summary>
		/// Prepare a string to insert into a query
		/// </summary>
		/// <param name="str">string to prepare</param>
		/// <returns>string ready to insert into a query</returns>
		///**************************************************************************
		static public string QueryString(string str)
		{
			// Verifica se la stringa contiene il carattere '
			if (str.IndexOf('\'') < 0)
				return str;

			// Se lo contiene, lo sostituisce con ''
			return str.Replace("\'", "\'\'");
 		}
	
		//**************************************************************************
		// Function Requery
		// Ritorna un recordset con una nuovo comando SQL
		// Parametri:
		//           sqlCmd			: stringa che contiene il nuovo comando SQL
		//           sqlDataReader	: recordset interessato dall'operazione
		//			 singleRow		: si aspetta una sola riga di risultato
		// Ritorna:
		//           true		= operazione eseguita senza errori.
		//           false		= operazione fallita.
		//**************************************************************************
		public bool Requery(string sqlCmd, out OleDbDataReader dbDataRead)
		{
			return Requery(sqlCmd, out dbDataRead, false, false);
		}
		public bool Requery(string sqlCmd, out OleDbDataReader dbDataRead, bool singleRow)
		{
			return Requery(sqlCmd, out dbDataRead, singleRow, false);
		}
		public bool Requery(string sqlCmd, out OleDbDataReader dbDataRead, bool singleRow, bool sequentialAccess)
		{
			return Requery(sqlCmd, out dbDataRead, singleRow, false, -1);
		}
		
		public bool Requery(string sqlCmd, out OleDbDataReader dbDataRead, bool singleRow, bool sequentialAccess, int commandTimeout)
		{
			int retry = 0;
			OleDbCommand cmd = null;
			dbDataRead = null;
			CommandBehavior flagCmd = CommandBehavior.Default;
			bool rc = false;

			//Riprova pi� volte
			while (retry < DEADLOCK_RETRY)
			{
				//Verifica se c'� gi� una query in esecuzione
				mMutex.WaitOne();

				// Se chiusa, apre la connessione
				if (!IsOpen())
					mDbConnection.Open();

				// gestione errori nelle operazioni DB
				try
				{
					if (cmd != null)
					{
						cmd.Cancel();
						cmd.Dispose();
						cmd = null;
					}
					cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
					if (commandTimeout >= 0)
						cmd.CommandTimeout = commandTimeout;

					if (singleRow)
						flagCmd |= CommandBehavior.SingleRow;
					if (sequentialAccess)
						flagCmd |= CommandBehavior.SequentialAccess;

					dbDataRead = cmd.ExecuteReader(flagCmd);

					retry = DEADLOCK_RETRY * 10;
					rc = true;
				}
				
				catch (OleDbException ex)
				{
					string errorMessages = "";
					bool deadLock = false;
					bool timeout = false;

					//Registra le info sull'eccezione
					for (int i=0; i < ex.Errors.Count; i++)
					{
						errorMessages += "Index #" + i + "\r\n" +
							"NativeError: " + ex.Errors[i].NativeError + "\r\n" +
							"Message: " + ex.Errors[i].Message + "\r\n" +
							"Source: " + ex.Errors[i].Source + "\r\n" +
							"SQLState: " + ex.Errors[i].SQLState + "\r\n";

						//Verifica se c'� anche un errore di deadlock
						if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
							deadLock = true;
						else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
							timeout = true;
					}
					errorMessages += "Server: " + mConnectionString + "\r\n" +
						"Query: " + sqlCmd + "\r\n";

					System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
					log.Source = "DbInterface";
					log.WriteEntry(errorMessages);
					TraceLog.WriteLine(log.Source + " " + errorMessages);
					
					if (dbDataRead != null) dbDataRead.Close();
					cmd.Cancel();

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (deadLock && !TransactionActive() && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(DEADLOCK_WAIT);
						retry++;
					}
					//Se timeout e non c'� una transazione attiva, riprova pi� volte
					else if (timeout && !TransactionActive() && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(TIMEOUT_WAIT);
						retry++;
					}

					//Se deadlock, solleva l'eccezione di deadlock
					else if (deadLock)
						throw new DeadLockException(errorMessages);

					//Se timeout, solleva l'eccezione di timeout
					else if (timeout)
						throw new TimeoutException(errorMessages);
						
					//Altrimenti, ripete la stessa eccezione
					else
						throw ex;
				}

				//Altre eccezioni
				catch (Exception ex)
				{
					if (dbDataRead != null) dbDataRead.Close();
					cmd.Cancel();
					throw ex;
				}

				finally
				{
					cmd.Dispose();
					cmd = null;
					if (!rc)
						mMutex.ReleaseMutex();
				}
			}			
			
			return rc;
		}
	
		//**************************************************************************
		// Function EndRequery
		// Termina il comando di requery
		// Parametri:
		//           sqlDataReader	: recordset da chiudere
		//**************************************************************************
		public void EndRequery(OleDbDataReader dbDataRead)
		{
			if (dbDataRead != null)
			{
				if (!dbDataRead.IsClosed)
				{
					/* Ciclo necessario per arrivare all'EOF del datareader.
					 * Se il datareader viene chiuso senza essere giunti all'EOF si ottiene
					 * un memory leak, specie se il datareader contiene dei campi BLOB.
					 * (Problema non presente sulla documentazione ufficiale Microsoft, 
					 * ne' sui vari forum.)
					 */
					while (dbDataRead.Read())
						;
					dbDataRead.Close();
				}
				dbDataRead.Dispose();
			}

			// Chiude la connessione, se non c'� una transazione attiva
			if (IsOpen() && !TransactionActive())
				Close();

			try	{	mMutex.ReleaseMutex();	}
			catch	{}
		}

		/***************************************************************************
		 * Execute
		 * Esegue un comando SQL
		 * Parametri:
		 *			sqlCmd		: stringa che contiene il nuovo comando SQL
		 *			identity	: identity resituita dall'operazione
		 *			parameters	: parametri della stored procedure
		 * Ritorna:
		 *			true	operazione eseguita senza errori.
		 *			false	operazione fallita.
		 ****************************************************************************/
		public bool Execute(string sqlCmd)
		{
			int identity;
			ArrayList parameters = null;
			return Execute(sqlCmd, out identity, ref parameters);
		}
		public bool Execute(string sqlCmd, out int identity)
		{
			ArrayList parameters = null;
			return Execute(sqlCmd, out identity, ref parameters);
		}
		public bool Execute(string sqlCmd, ref ArrayList parameters)
		{
			int identity;
			return Execute(sqlCmd, out identity, ref parameters);
		}
		public bool Execute(string sqlCmd, out int identity, ref ArrayList parameters)
		{
			int retry = 0;
			OleDbCommand cmd = null;
			bool rc = false;

			identity = 0;

			//Riprova pi� volte
			while (retry < DEADLOCK_RETRY)
			{
				//Verifica se c'� gi� una query in esecuzione
				mMutex.WaitOne();

				// Se chiusa, apre la connessione
				if (!IsOpen())
					mDbConnection.Open();

				// gestione errori nelle operazioni DB
				try
				{
					if (cmd != null)
					{
						cmd.Cancel();
						cmd.Dispose();
						cmd = null;
					}

					// Apre il command
					cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
					// Assegna i parametri
					if (parameters != null)
					{
						cmd.CommandType = CommandType.StoredProcedure;
						foreach (OleDbParameter par in parameters)
							cmd.Parameters.Add(par);
					}
					object obj = cmd.ExecuteScalar();
					if (obj != null && obj != DBNull.Value)
						identity = int.Parse(obj.ToString());	// identity = decimal.ToInt32((decimal) obj);
						
					retry = DEADLOCK_RETRY * 10;
					rc = true;
				}

				catch (OleDbException ex)
				{
					string errorMessages = "";
					bool deadLock = false;
					bool timeout = false;

					//Registra le info sull'eccezione
					for (int i=0; i < ex.Errors.Count; i++)
					{
						errorMessages += "Index #" + i + "\r\n" +
							"NativeError: " + ex.Errors[i].NativeError + "\r\n" +
							"Message: " + ex.Errors[i].Message + "\r\n" +
							"Source: " + ex.Errors[i].Source + "\r\n" +
							"SQLState: " + ex.Errors[i].SQLState + "\r\n";

						//Verifica se c'� anche un errore di deadlock
						if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
							deadLock = true;
						else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
							timeout = true;
					}
					errorMessages += "Server: " + mConnectionString + "\r\n" +
						"Query: " + sqlCmd + "\r\n";
					
					System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
					log.Source = "DbInterface";
					log.WriteEntry(errorMessages);
					TraceLog.WriteLine(log.Source + " " + errorMessages + "Query: " + sqlCmd);
					
					cmd.Cancel();

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (deadLock && !TransactionActive() && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(DEADLOCK_WAIT);
						retry++;
					}
					//Se timeout e non c'� una transazione attiva, riprova pi� volte
					else if (timeout && !TransactionActive() && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(TIMEOUT_WAIT);
						retry++;
					}

					//Se deadlock, solleva l'eccezione di deadlock
					else if (deadLock)
						throw new DeadLockException(errorMessages);

					//Se timeout, solleva l'eccezione di timeout
					else if (timeout)
						throw new TimeoutException(errorMessages);

					//altri errori
					else
						throw ex;
				}
				
				//Altre eccezioni
				catch (Exception ex)
				{
					throw(ex);
				}

				// Operazioni finali
				finally
				{
					cmd.Dispose();
					cmd = null;

					// Chiude la connessione, se non c'� una transazione attiva
					if (IsOpen() && !TransactionActive())
						Close();

					mMutex.ReleaseMutex();
				}
			}			
			
			return rc;
		}

		//**************************************************************************
		// Function BeginTransaction
		// Inizio di una nuova transazione verso DB
		// Ritorna:
		//           FALSE = errore nell'operazione
		//           TRUE  = transazione iniziata
		//**************************************************************************
		public bool BeginTransaction()
		{
			try
			{
				mMutexTransaction.WaitOne();

				// Se chiusa, apre la connessione
				if (!IsOpen())
					mDbConnection.Open();

				// Start a local transaction
				mTransaction = mDbConnection.BeginTransaction();
				mActiveTransaction = true;
				return true;
			}

			catch (OleDbException ex)
			{
				mTransaction = null;

				string errorMessages = "";
				bool deadLock = false;
				bool timeout = false;

				//Registra le info sull'eccezione
				for (int i=0; i < ex.Errors.Count; i++)
				{
					errorMessages += "Index #" + i + "\r\n" +
						"NativeError: " + ex.Errors[i].NativeError + "\r\n" +
						"Message: " + ex.Errors[i].Message + "\r\n" +
						"Source: " + ex.Errors[i].Source + "\r\n" +
						"SQLState: " + ex.Errors[i].SQLState + "\r\n";

					//Verifica se c'� anche un errore di deadlock
					if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
						deadLock = true;
					else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
						timeout = true;
				}
				errorMessages += "Server: " + mConnectionString + "\r\n";
			
				System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
				log.Source = "DbInterface";
				log.WriteEntry(errorMessages);
				mActiveTransaction = false;
				mMutexTransaction.ReleaseMutex();

				//Se deadlock, solleva l'eccezione di deadlock
				if (deadLock)
					throw new DeadLockException(errorMessages);

				//Se timeout, solleva l'eccezione di timeout
				else if (timeout)
					throw new TimeoutException(errorMessages);

				//altri errori
				else
					throw ex;
			}

			catch (Exception ex)
			{
				mActiveTransaction = false;
				mMutexTransaction.ReleaseMutex();
				throw(ex);
			}

			finally	{}
		}
	
		//**************************************************************************
		// void EndTransaction
		// Fine di una transazione
		// Parametri:
		//           good	: se TRUE indica che deve essere eseguita una COMMIT;
		//                     altrimenti si esegue una ROLLBACK
		//**************************************************************************
		public void EndTransaction(bool good)
		{
			//mMutexTransaction.WaitOne();
			bool transaction = TransactionActive();

			if (transaction)
			{
				/* WORKAROUND: Quando si chiude una transaction la cui ultima istruzione � un OleDbDataReader,
				 * la transazione d� il seguente errore:
				 * 
				 *		RollbackTransaction requires an open and available Connection. 
				 *		The connection's current state is Open, Fetching.
				 * 
				 * Viene dunque eseguita una execute fittizia solo per portare lo StateInternal della OleDbConnection
				 * da 9 (Open + Fetching) a 1 (Open)
				 */
				if (mJetOLEDB || mAceOLEDB)
					Execute("SELECT @@IDENTITY");
				else
					Execute("SELECT SCOPE_IDENTITY()");

				if (good)
					mTransaction.Commit();
				else
					mTransaction.Rollback();
			}

			// Chiude la connessione
			Close();

			mActiveTransaction = false;
			
			if (transaction)
				mMutexTransaction.ReleaseMutex();
		}

		//**************************************************************************
		// Function GetBlobField
		// Legge un campo blob e lo memorizza su un file
		// Parametri:
		//          tableName	: nome della tabella da cui recuperare il campo blob
		//			columnName	: nome della colonna da cui recuperare il campo blob
		//			sqlWhere	: condizione WHERE per identificare il record
		//			fileName	: nome del file dove memorizzare il campo blob
		// Ritorna:
		//           true		= operazione eseguita senza errori.
		//           false		= operazione fallita.
		//**************************************************************************
		public bool GetBlobField(string tableName, string columnName, string sqlWhere, string fileName)
		{
			FileStream fs=null;						// Writes the BLOB to a file (*.bmp).
			BinaryWriter bw=null;					// Streams the BLOB to the FileStream object.

			int bufferSize = BUFFER_LENGTH;			// Size of the BLOB buffer.
			byte[] outbyte=null;					// The BLOB byte[] buffer to be filled by GetBytes.
			char[] outchar=null;					// The BLOB byte[] buffer to be filled by GetChars.
			long retval=0;							// The bytes returned from GetBytes.
			long startIndex = 0;					// The starting position in the BLOB output.
			string sqlCmd = "";
			OleDbDataReader dbReader = null;

			try
			{
				// Create a file to hold the output.
				fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
				bw = new BinaryWriter(fs);

				sqlCmd = "SELECT " + columnName + " FROM " + tableName + " WHERE " + sqlWhere;

				// Legge il datareader con il campo BLOB
				Requery(sqlCmd, out dbReader, true, true);

				if (dbReader.Read())
				{
					if (dbReader[0] != System.DBNull.Value)
					{
						string typeName = dbReader.GetFieldType(0).Name;

						// Reset the starting byte for the new BLOB.
						startIndex = 0;

						if (typeName == "Byte[]")
						{
							outbyte = new byte[bufferSize];
							// Read the bytes into outbyte[] and retain the number of bytes returned.
							retval = dbReader.GetBytes(0, startIndex, outbyte, 0, bufferSize);

							//System.Text.Encoding enc = System.Text.Encoding.ASCII;
							//string myString = enc.GetString(outbyte);

							//if (Uri.IsWellFormedUriString(myString, UriKind.Absolute))
							//{
							//    int a = 0;
							//}

							//string s = System.Text.UTF8Encoding.UTF8.GetString(outbyte);
							//Path.GetFullPath(s);

							//if (!Directory.Exists(s))
							//    throw new ApplicationException("No valid folder");

						}
						else if (typeName == "String")
						{
							outchar = new char[bufferSize];
							// Read the chars into outchar[] and retain the number of chars returned.
							retval = dbReader.GetChars(0, startIndex, outchar, 0, bufferSize);
						}

						// Continue reading and writing while there are bytes beyond the size of the buffer.
						while (retval == bufferSize)
						{
							if (typeName == "Byte[]")
							{
								bw.Write(outbyte);
								bw.Flush();

								// Reposition the start index to the end of the last buffer and fill the buffer.
								startIndex += bufferSize;

								retval = dbReader.GetBytes(0, startIndex, outbyte, 0, bufferSize);
							}
							else if (typeName == "String")
							{
								bw.Write(outchar);
								bw.Flush();

								// Reposition the start index to the end of the last buffer and fill the buffer.
								startIndex += bufferSize;

								retval = dbReader.GetChars(0, startIndex, outchar, 0, bufferSize);
							}
						}

						// Write the remaining buffer.
						if (typeName == "Byte[]")
							bw.Write(outbyte, 0, (int)retval);
						else if (typeName == "String")
							bw.Write(outchar, 0, (int)retval);

						bw.Flush();

						return true;
					}
				}

				return false;

			}

			catch (OleDbException ex)
			{
				string errorMessages = "";
				bool deadLock = false;
				bool timeout = false;

				//Registra le info sull'eccezione
				for (int i=0; i < ex.Errors.Count; i++)
				{
					errorMessages += "Index #" + i + "\r\n" +
						"NativeError: " + ex.Errors[i].NativeError + "\r\n" +
						"Message: " + ex.Errors[i].Message + "\r\n" +
						"Source: " + ex.Errors[i].Source + "\r\n" +
						"SQLState: " + ex.Errors[i].SQLState + "\r\n";

					//Verifica se c'� anche un errore di deadlock
					if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
						deadLock = true;
					else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
						timeout = true;
				}
				errorMessages += "Server: " + mConnectionString + "\r\n";

				System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
				log.Source = "DbInterface";
				log.WriteEntry(errorMessages);
				TraceLog.WriteLine(log.Source + " " + errorMessages);
					
				//Se deadlock, solleva l'eccezione di deadlock
				if (deadLock)
					throw new DeadLockException(errorMessages);

				//Se timeout, solleva l'eccezione di timeout
				else if (timeout)
					throw new TimeoutException(errorMessages);
				
				//Altrimenti, ripete la stessa eccezione
				else
					throw ex;

			}

				//Altre eccezioni
			catch (Exception ex)
			{
				throw ex;
			}

			finally
			{
				// Chiude la query
				EndRequery(dbReader);

				// Close the output file.
				if (bw != null)
					bw.Close();
				if (fs != null)
					fs.Close();
			}

		}

        //**************************************************************************
        // Function GetBlobField
        // Legge un campo blob e lo memorizza su un file
        // Parametri:
        //          dbReader    : data reader
        //          fieldIndex  : indice del campo da trattare.
        //			fileName	: nome del file dove memorizzare il campo blob
        // Ritorna:
        //           true		= operazione eseguita senza errori.
        //           false		= operazione fallita.
        //**************************************************************************
        public bool GetBlobField(OleDbDataReader dbReader, int fieldIndex, string fileName)
        {
            FileStream fs = null;					// Writes the BLOB to a file (*.bmp).
            BinaryWriter bw = null;					// Streams the BLOB to the FileStream object.

            int bufferSize = BUFFER_LENGTH;			// Size of the BLOB buffer.
            byte[] outbyte = null;					// The BLOB byte[] buffer to be filled by GetBytes.
            char[] outchar = null;					// The BLOB byte[] buffer to be filled by GetChars.
            long retval = 0;						// The bytes returned from GetBytes.
            long startIndex = 0;					// The starting position in the BLOB output.
            bool rc = false;


            try
            {
                // Create a file to hold the output.
                fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                bw = new BinaryWriter(fs);

                string typeName = dbReader.GetFieldType(fieldIndex).Name;

                // Reset the starting byte for the new BLOB.
                startIndex = 0;

                if (typeName == "Byte[]")
                {
                    outbyte = new byte[bufferSize];
                    // Read the bytes into outbyte[] and retain the number of bytes returned.
                    retval = dbReader.GetBytes(fieldIndex, startIndex, outbyte, 0, bufferSize);
                }
                else if (typeName == "String")
                {
                    outchar = new char[bufferSize];
                    // Read the chars into outchar[] and retain the number of chars returned.
                    retval = dbReader.GetChars(fieldIndex, startIndex, outchar, 0, bufferSize);
                }

                // Continue reading and writing while there are bytes beyond the size of the buffer.
                while (retval == bufferSize)
                {
                    if (typeName == "Byte[]")
                    {
                        bw.Write(outbyte);
                        bw.Flush();

                        // Reposition the start index to the end of the last buffer and fill the buffer.
                        startIndex += bufferSize;

                        retval = dbReader.GetBytes(fieldIndex, startIndex, outbyte, 0, bufferSize);
                    }
                    else if (typeName == "String")
                    {
                        bw.Write(outchar);
                        bw.Flush();

                        // Reposition the start index to the end of the last buffer and fill the buffer.
                        startIndex += bufferSize;

                        retval = dbReader.GetChars(fieldIndex, startIndex, outchar, 0, bufferSize);
                    }
                }

                // Write the remaining buffer.
                if (typeName == "Byte[]")
                    bw.Write(outbyte, 0, (int)retval);
                else if (typeName == "String")
                    bw.Write(outchar, 0, (int)retval);

                bw.Flush();

                rc = true;
            }

            catch (OleDbException ex)
            {
                string errorMessages = "";
                bool deadLock = false;
				bool timeout = false;

                //Registra le info sull'eccezione
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    errorMessages += "Index #" + i + "\r\n" +
                        "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
                        "Message: " + ex.Errors[i].Message + "\r\n" +
                        "Source: " + ex.Errors[i].Source + "\r\n" +
                        "SQLState: " + ex.Errors[i].SQLState + "\r\n";

                    //Verifica se c'� anche un errore di deadlock
                    if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
                        deadLock = true;
					else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
						timeout = true;
                }
				errorMessages += "Server: " + mConnectionString + "\r\n";

                System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
                log.Source = "DbInterface";
                log.WriteEntry(errorMessages);
                TraceLog.WriteLine(log.Source + " " + errorMessages);

                //Se deadlock, solleva l'eccezione di deadlock
                if (deadLock)
                    throw new DeadLockException(errorMessages);

				//Se timeout, solleva l'eccezione di timeout
				else if (timeout)
					throw new TimeoutException(errorMessages);
				
				//Altrimenti, ripete la stessa eccezione
                else
                    throw ex;

            }

                //Altre eccezioni
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                // Close the output file.
                if (bw != null)
                    bw.Close();
                if (fs != null)
                    fs.Close();
            }

            return rc;

        }

        //**************************************************************************
        // Function GetBlobField
        // Legge un campo blob e lo memorizza su un buffer
        // Parametri:
        //          dbReader    : data reader
        //          fieldIndex  : indice del campo da trattare.
        //			destBuffer	: buffer destinazione
        // Ritorna:
        //           true		= operazione eseguita senza errori.
        //           false		= operazione fallita.
        //**************************************************************************
        public bool GetBlobField(OleDbDataReader dbReader, int fieldIndex, ref byte[] destBuffer )
        {
            MemoryStream fs = null;					    // Writes the BLOB to a memrory stream
            BinaryWriter bw = null;					// Streams the BLOB to the FileStream object.

            int bufferSize = BUFFER_LENGTH;			// Size of the BLOB buffer.
            byte[] outbyte = null;					// The BLOB byte[] buffer to be filled by GetBytes.
            char[] outchar = null;					// The BLOB byte[] buffer to be filled by GetChars.
            long retval = 0;						// The bytes returned from GetBytes.
            long startIndex = 0;					// The starting position in the BLOB output.
            bool rc = false;


            try
            {
                // Create a file to hold the output.
                fs = new MemoryStream();
                bw = new BinaryWriter(fs);

                string typeName = dbReader.GetFieldType(fieldIndex).Name;

                // Reset the starting byte for the new BLOB.
                startIndex = 0;

                if (typeName == "Byte[]")
                {
                    outbyte = new byte[bufferSize];
                    // Read the bytes into outbyte[] and retain the number of bytes returned.
                    retval = dbReader.GetBytes(fieldIndex, startIndex, outbyte, 0, bufferSize);
                }
                else if (typeName == "String")
                {
                    outchar = new char[bufferSize];
                    // Read the chars into outchar[] and retain the number of chars returned.
                    retval = dbReader.GetChars(fieldIndex, startIndex, outchar, 0, bufferSize);
                }

                // Continue reading and writing while there are bytes beyond the size of the buffer.
                while (retval == bufferSize)
                {
                    if (typeName == "Byte[]")
                    {
                        bw.Write(outbyte);
                        bw.Flush();

                        // Reposition the start index to the end of the last buffer and fill the buffer.
                        startIndex += bufferSize;

                        retval = dbReader.GetBytes(fieldIndex, startIndex, outbyte, 0, bufferSize);
                    }
                    else if (typeName == "String")
                    {
                        bw.Write(outchar);
                        bw.Flush();

                        // Reposition the start index to the end of the last buffer and fill the buffer.
                        startIndex += bufferSize;

                        retval = dbReader.GetChars(fieldIndex, startIndex, outchar, 0, bufferSize);
                    }
                }

                // Write the remaining buffer.
                if (typeName == "Byte[]")
                    bw.Write(outbyte, 0, (int)retval);
                else if (typeName == "String")
                    bw.Write(outchar, 0, (int)retval);

                bw.Flush();

                destBuffer = new byte[fs.Length];
				fs.Seek(0, SeekOrigin.Begin);
                fs.Read(destBuffer, 0, (int)fs.Length);

                rc = true;
            }

            catch (OleDbException ex)
            {
                string errorMessages = "";
                bool deadLock = false;
				bool timeout = false;

                //Registra le info sull'eccezione
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    errorMessages += "Index #" + i + "\r\n" +
                        "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
                        "Message: " + ex.Errors[i].Message + "\r\n" +
                        "Source: " + ex.Errors[i].Source + "\r\n" +
                        "SQLState: " + ex.Errors[i].SQLState + "\r\n";

                    //Verifica se c'� anche un errore di deadlock
                    if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
                        deadLock = true;
					else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
						timeout = true;
                }
				errorMessages += "Server: " + mConnectionString + "\r\n";

                System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
                log.Source = "DbInterface";
                log.WriteEntry(errorMessages);
                TraceLog.WriteLine(log.Source + " " + errorMessages);

                //Se deadlock, solleva l'eccezione di deadlock
                if (deadLock)
                    throw new DeadLockException(errorMessages);

				//Se timeout, solleva l'eccezione di timeout
				else if (timeout)
					throw new TimeoutException(errorMessages);

                //Altrimenti, ripete la stessa eccezione
                else
                    throw ex;

            }

                //Altre eccezioni
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                // Close the output file.
                if (bw != null)
                    bw.Close();
                if (fs != null)
                    fs.Close();
            }

            return rc;

        }


	    public bool WriteSingleBlobField(string tableName,
	        string columnName,
	        string sqlWhere,
	        string filepath)
	    {
	        bool retVal = false;

	        bool transaction;

            int retry = 0;

            string sqlCmd = "";
            bool updateDone = false;

	        int length;
	        byte[] blobStorage;

            OleDbCommand cmd = null;

	        if (!File.Exists(filepath))
	        {
	            TraceLog.WriteLine(string.Format("DbInterface.WriteSingleBlobField error: file <{0}> not found.", filepath));
                return false;
	        }

	        try
	        {
	            using (var fs = new FileStream(filepath, FileMode.OpenOrCreate, FileAccess.Read))
	            {
	                length = (int) fs.Length;
	                blobStorage = new byte[length];
	                fs.Read(blobStorage, 0, length);
                    fs.Close();
	            }

                mMutex.WaitOne();

                transaction = !TransactionActive();

	            while (retry < DEADLOCK_RETRY)
	            {
                    if (transaction) BeginTransaction();

                    if (cmd != null)
                    {
                        cmd.Cancel();
                        cmd.Dispose();
                        cmd = null;
                    }
	                sqlCmd = "UPDATE " + tableName + " SET " + columnName + " = ? " + " WHERE " + sqlWhere;

                    cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                    cmd.CommandType = CommandType.Text;
                    OleDbParameter param = new OleDbParameter("@BlobData", OleDbType.VarBinary, length);
	                param.Value = blobStorage;
                    param.Direction = ParameterDirection.Input;

	                cmd.Parameters.Add(param);

	                try
	                {
	                    int rowsAffected = cmd.ExecuteNonQuery();

	                    retVal = (rowsAffected >0);

                        if (transaction) EndTransaction(true);

	                    break;
	                }

	                catch (OleDbException ex)
	                {
	                    string errorMessages = "";
	                    bool deadLock = false;
	                    bool timeout = false;

	                    //Registra le info sull'eccezione
	                    for (int i = 0; i < ex.Errors.Count; i++)
	                    {
	                        errorMessages += "Index #" + i + "\r\n" +
	                                         "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
	                                         "Message: " + ex.Errors[i].Message + "\r\n" +
	                                         "Source: " + ex.Errors[i].Source + "\r\n" +
	                                         "SQLState: " + ex.Errors[i].SQLState + "\r\n";

	                        //Verifica se c'� anche un errore di deadlock
	                        if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
	                            deadLock = true;
	                        else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
	                            timeout = true;
	                    }
	                    errorMessages += "Server: " + mConnectionString + "\r\n" +
	                                     "Query: " + sqlCmd + "\r\n";

	                    TraceLog.WriteLine("DbInterface.WriteSingleBlobField error: " + errorMessages + "Query: " + sqlCmd);

	                    cmd.Cancel();

                        if (transaction) EndTransaction(false);

	                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
	                    if (deadLock && transaction && retry < DEADLOCK_RETRY)
	                    {
	                        Thread.Sleep(DEADLOCK_WAIT);
	                        retry++;
	                    }
	                        //Se timeout e non c'� una transazione attiva, riprova pi� volte
	                    else if (timeout && transaction && retry < DEADLOCK_RETRY)
	                    {
	                        Thread.Sleep(TIMEOUT_WAIT);
	                        retry++;
	                    }

	                        //Se deadlock, solleva l'eccezione di deadlock
	                    else if (deadLock)
	                        throw new DeadLockException(errorMessages);

	                        //Se timeout, solleva l'eccezione di timeout
	                    else if (timeout)
	                        throw new TimeoutException(errorMessages);

	                        //altri errori
	                    else
	                        throw ex;
	                }

	                    //Altre eccezioni
	                catch (Exception ex)
	                {
	                    // Se ha aperto internamente la transazione, la chiude
	                    if (transaction) EndTransaction(false);

	                    throw (ex);
	                }

	            }




	        }
	        catch (Exception ex)
	        {
	            TraceLog.WriteLine("DbInterface.WriteSingleBlobField error", ex);
	            retVal = false;
	        }

	        return retVal;
	    }


		//**************************************************************************
		// Function WriteBlobField
		// Scrive un campo blob prelevandolo da un file
		// Parametri:
		//          tableName	: nome della tabella dove scrivere il campo blob
		//			columnName	: nome della colonna dove scrivere il campo blob
		//			optSet  	: eventuali altri campi da impostare
		//			sqlWhere	: condizione WHERE per identificare il record
		//			fileName	: nome del file dove prelevare il campo blob
		//          updateField : nome del campo da usare per indicare l'aggiornamento
		// Ritorna:
		//           true		= operazione eseguita senza errori.
		//           false		= operazione fallita.
		//**************************************************************************
		public bool WriteBlobField_ORI(string tableName,
									string columnName,
									string optSet,
									string sqlWhere,
									string sourceFileName,
									string updatedField,
									bool saveDB,
									string folderStorage,
									string destFileName)
		{
			int retry = 0;
			OleDbCommand cmd = null;
			int bufferLen = BUFFER_LENGTH;  // The size of the "chunks" of the image.
			FileStream fs = null;
			BinaryReader br = null;
			bool transaction = !TransactionActive();
			string sqlCmd = "";
			bool update_done = false;

			//Riprova pi� volte
			while (retry < DEADLOCK_RETRY)
			{
				// Verifica se occorre aprire una transazione
				if (transaction) BeginTransaction();

				//Verifica se c'� gi� una query in esecuzione
				mMutex.WaitOne();

				// gestione errori nelle operazioni DB
				try
				{
					if (cmd != null)
					{
						cmd.Cancel();
						cmd.Dispose();
						cmd = null;
					}

					if (saveDB)
					{
						// Recupera il puntatore del campo blob
						sqlCmd = "SELECT ? = TEXTPTR(" + columnName + ") FROM " + tableName + " WHERE " + sqlWhere;
						cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
						OleDbParameter ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
						ptrParm.Direction = ParameterDirection.Output;
						cmd.ExecuteNonQuery();

						// verifica se il puntatore campo blob � valido...
						if (ptrParm.Value == System.DBNull.Value)
						{
							// ... altrimenti ne crea uno di valido
							cmd.Dispose();
							cmd = null;
							sqlCmd = "UPDATE " + tableName + " SET " + columnName + " = NULL " + optSet + " WHERE " + sqlWhere + ";" +
									 sqlCmd;
							cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
							ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
							ptrParm.Direction = ParameterDirection.Output;
							cmd.ExecuteNonQuery();
#if DONT_UPDATE_BLOB_FIELD
                        update_done = true;
#endif
						}

						// legge il puntatore
						byte[] pointer = (byte[])ptrParm.Value;
						cmd.Dispose();
						cmd = null;

						// Crea la query per la memrorizzazione dell'immagine
                        sqlCmd = "UPDATETEXT " + tableName + "." + columnName + " ? ? NULL ?";
						cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
						ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
						ptrParm.Value = pointer;
						OleDbParameter offsetParm = cmd.Parameters.Add("@Offset", OleDbType.Integer);
						offsetParm.Value = 0;
						OleDbParameter photoParm = cmd.Parameters.Add("@Bytes", OleDbType.VarBinary, bufferLen);

						//''''''''''''''''''''''''''''''''''
						// Read the image in and write it to the database 128 (bufferLen) bytes at a time.
						// Tune bufferLen for best performance. Larger values write faster, but
						// use more system resources.
						fs = new FileStream(sourceFileName, FileMode.Open, FileAccess.Read);
						br = new BinaryReader(fs);

						byte[] buffer = br.ReadBytes(bufferLen);
						int offset_ctr = 0;

						while (buffer.Length > 0)
						{
							photoParm.Value = buffer;
							cmd.ExecuteNonQuery();
							offset_ctr += bufferLen;
							offsetParm.Value = offset_ctr;
							buffer = br.ReadBytes(bufferLen);
						}
					}
					else
					{
						// Verifica se il file sorgente esiste
						if (!File.Exists(sourceFileName))
							throw new ApplicationException("Source file " + sourceFileName + " not found");

						// Verifica se esiste la cartella di destinazione
						if (!Directory.Exists(folderStorage))
						{
							// Se non esiste la crea
							DirectoryInfo dir = new DirectoryInfo(folderStorage);
							dir.Create();
						}

						// Scrive il file nella cartella di destinazione.
						File.Copy(sourceFileName, folderStorage + "\\" + destFileName);
						
						// Crea la query per la memrorizzazione del folder dell'immagine
						if (cmd != null)
						{
							cmd.Dispose();
							cmd = null;
						}
						sqlCmd = "UPDATE " + tableName + " SET " + columnName + " = '" + folderStorage + "' WHERE " + sqlWhere;
						cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
						cmd.ExecuteNonQuery();
					}

					cmd.Dispose();
					cmd = null;

					// Esegue un aggiornamento fittizio per generare il trigger
					if (!update_done && updatedField != "" && updatedField != null)
					{
						sqlCmd = "UPDATE " + tableName + " SET " + updatedField + " = " + updatedField + " " + optSet + " WHERE " + sqlWhere;
						cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
						cmd.ExecuteNonQuery();
					}

					// Chiude l'eventuale transazione aperta
					if (transaction) EndTransaction(true);

					retry = DEADLOCK_RETRY * 10;
				}

				catch (OleDbException ex)
				{
					string errorMessages = "";
					bool deadLock = false;
					bool timeout = false;

					//Registra le info sull'eccezione
					for (int i = 0; i < ex.Errors.Count; i++)
					{
						errorMessages += "Index #" + i + "\r\n" +
							"NativeError: " + ex.Errors[i].NativeError + "\r\n" +
							"Message: " + ex.Errors[i].Message + "\r\n" +
							"Source: " + ex.Errors[i].Source + "\r\n" +
							"SQLState: " + ex.Errors[i].SQLState + "\r\n";

						//Verifica se c'� anche un errore di deadlock
						if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
							deadLock = true;
						else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
							timeout = true;
					}
					errorMessages += "Server: " + mConnectionString + "\r\n" +
						"Query: " + sqlCmd + "\r\n";

					System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
					log.Source = "DbInterface";
					log.WriteEntry(errorMessages);
					TraceLog.WriteLine(log.Source + " " + errorMessages + "Query: " + sqlCmd);

					cmd.Cancel();

					if (transaction) EndTransaction(false);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (deadLock && transaction && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(DEADLOCK_WAIT);
						retry++;
					}
					//Se timeout e non c'� una transazione attiva, riprova pi� volte
					else if (timeout && transaction && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(TIMEOUT_WAIT);
						retry++;
					}

						//Se deadlock, solleva l'eccezione di deadlock
					else if (deadLock)
						throw new DeadLockException(errorMessages);

					//Se timeout, solleva l'eccezione di timeout
					else if (timeout)
						throw new TimeoutException(errorMessages);

						//altri errori
					else
						throw ex;
				}

					//Altre eccezioni
				catch (Exception ex)
				{
					// Se ha aperto internamente la transazione, la chiude
					if (transaction) EndTransaction(false);

					throw (ex);
				}

					// Operazioni finali
				finally
				{
					if (cmd != null)
					{
						cmd.Dispose();
						cmd = null;
					}

					// Chiude la connessione, se non c'� una transazione attiva
					if (IsOpen() && !TransactionActive())
						Close();

					// Chiude i file
					if (br != null) br.Close();
					if (fs != null) fs.Close();

					mMutex.ReleaseMutex();
				}
			}

			return true;
		}


		public bool WriteBlobField(string tableName,
									string columnName,
									string optSet,
									string sqlWhere,
									string sourceFileName,
									string updatedField,
									bool saveDB,
									string folderStorage,
									string destFileName)
		{
			int retry = 0;
			OleDbCommand cmd = null;
			int bufferLen = BUFFER_LENGTH;  // The size of the "chunks" of the image.
			FileStream fs = null;
			BinaryReader br = null;
			bool transaction = !TransactionActive();
			string sqlCmd = "";
			bool update_done = false;

			//Riprova pi� volte
			while (retry < DEADLOCK_RETRY)
			{
				// Verifica se occorre aprire una transazione
				if (transaction) BeginTransaction();

				//Verifica se c'� gi� una query in esecuzione
				mMutex.WaitOne();

				// gestione errori nelle operazioni DB
				try
				{
					if (cmd != null)
					{
						cmd.Cancel();
						cmd.Dispose();
						cmd = null;
					}

					if (saveDB)
					{
//                        // Recupera il puntatore del campo blob
//                        sqlCmd = "UPDATE " + tableName + " SET " + columnName + " .WRITE  ? ? NULL ?";
//                        cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
//                        OleDbParameter ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
//                        ptrParm.Direction = ParameterDirection.Output;
//                        cmd.ExecuteNonQuery();

//                        // verifica se il puntatore campo blob � valido...
//                        if (ptrParm.Value == System.DBNull.Value)
//                        {
//                            // ... altrimenti ne crea uno di valido
//                            cmd.Dispose();
//                            cmd = null;
//                            sqlCmd = "UPDATE " + tableName + " SET " + columnName + " = NULL " + optSet + " WHERE " + sqlWhere + ";" +
//                                     sqlCmd;
//                            cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
//                            ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
//                            ptrParm.Direction = ParameterDirection.Output;
//                            cmd.ExecuteNonQuery();
//#if DONT_UPDATE_BLOB_FIELD
//                        update_done = true;
//#endif
//                        }

						// legge il puntatore
                        //byte[] pointer = (byte[])ptrParm.Value;
                        //cmd.Dispose();
                        //cmd = null;

                        //// Crea la query per la memrorizzazione dell'immagine
                        //sqlCmd = "UPDATETEXT " + tableName + "." + columnName + " ? ? NULL ?";
                        //cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                        //ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
                        //ptrParm.Value = pointer;
                        //OleDbParameter offsetParm = cmd.Parameters.Add("@Offset", OleDbType.Integer);
                        //offsetParm.Value = 0;
                        //OleDbParameter photoParm = cmd.Parameters.Add("@Bytes", OleDbType.VarBinary, bufferLen);

                        ////''''''''''''''''''''''''''''''''''
                        //// Read the image in and write it to the database 128 (bufferLen) bytes at a time.
                        //// Tune bufferLen for best performance. Larger values write faster, but
                        //// use more system resources.
                        //fs = new FileStream(sourceFileName, FileMode.Open, FileAccess.Read);
                        //br = new BinaryReader(fs);

						byte[] buffer = File.ReadAllBytes(sourceFileName);

                        sqlCmd = "UPDATE " + tableName + " SET " + columnName + " = ?  WHERE " + sqlWhere ;
                        cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                        OleDbParameter param = new OleDbParameter("@Content", OleDbType.Binary);
                        param.Value = buffer;
					    cmd.Parameters.Add(param);

					    var result = cmd.ExecuteNonQuery();


					    //int offset_ctr = 0;

					    //while (buffer.Length > 0)
					    //{
					    //    photoParm.Value = buffer;
					    //    cmd.ExecuteNonQuery();
					    //    offset_ctr += bufferLen;
					    //    offsetParm.Value = offset_ctr;
					    //    buffer = br.ReadBytes(bufferLen);
					    //}
					}
					else
					{
						// Verifica se il file sorgente esiste
						if (!File.Exists(sourceFileName))
							throw new ApplicationException("Source file " + sourceFileName + " not found");

						// Verifica se esiste la cartella di destinazione
						if (!Directory.Exists(folderStorage))
						{
							// Se non esiste la crea
							DirectoryInfo dir = new DirectoryInfo(folderStorage);
							dir.Create();
						}

						// Scrive il file nella cartella di destinazione.
						File.Copy(sourceFileName, folderStorage + "\\" + destFileName);
						
						// Crea la query per la memrorizzazione del folder dell'immagine
						if (cmd != null)
						{
							cmd.Dispose();
							cmd = null;
						}
						sqlCmd = "UPDATE " + tableName + " SET " + columnName + " = '" + folderStorage + "' WHERE " + sqlWhere;
						cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
						cmd.ExecuteNonQuery();
					}

					cmd.Dispose();
					cmd = null;

					// Esegue un aggiornamento fittizio per generare il trigger
                    //if (!update_done && updatedField != "" && updatedField != null)
                    //{
                    //    sqlCmd = "UPDATE " + tableName + " SET " + updatedField + " = " + updatedField + " " + optSet + " WHERE " + sqlWhere;
                    //    cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                    //    cmd.ExecuteNonQuery();
                    //}

					// Chiude l'eventuale transazione aperta
					if (transaction) EndTransaction(true);

					retry = DEADLOCK_RETRY * 10;
				}

				catch (OleDbException ex)
				{
					string errorMessages = "";
					bool deadLock = false;
					bool timeout = false;

					//Registra le info sull'eccezione
					for (int i = 0; i < ex.Errors.Count; i++)
					{
						errorMessages += "Index #" + i + "\r\n" +
							"NativeError: " + ex.Errors[i].NativeError + "\r\n" +
							"Message: " + ex.Errors[i].Message + "\r\n" +
							"Source: " + ex.Errors[i].Source + "\r\n" +
							"SQLState: " + ex.Errors[i].SQLState + "\r\n";

						//Verifica se c'� anche un errore di deadlock
						if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
							deadLock = true;
						else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
							timeout = true;
					}
					errorMessages += "Server: " + mConnectionString + "\r\n" +
						"Query: " + sqlCmd + "\r\n";

					System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
					log.Source = "DbInterface";
					log.WriteEntry(errorMessages);
					TraceLog.WriteLine(log.Source + " " + errorMessages + "Query: " + sqlCmd);

					cmd.Cancel();

					if (transaction) EndTransaction(false);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (deadLock && transaction && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(DEADLOCK_WAIT);
						retry++;
					}
					//Se timeout e non c'� una transazione attiva, riprova pi� volte
					else if (timeout && transaction && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(TIMEOUT_WAIT);
						retry++;
					}

						//Se deadlock, solleva l'eccezione di deadlock
					else if (deadLock)
						throw new DeadLockException(errorMessages);

					//Se timeout, solleva l'eccezione di timeout
					else if (timeout)
						throw new TimeoutException(errorMessages);

						//altri errori
					else
						throw ex;
				}

					//Altre eccezioni
				catch (Exception ex)
				{
					// Se ha aperto internamente la transazione, la chiude
					if (transaction) EndTransaction(false);

					throw (ex);
				}

					// Operazioni finali
				finally
				{
					if (cmd != null)
					{
						cmd.Dispose();
						cmd = null;
					}

					// Chiude la connessione, se non c'� una transazione attiva
					if (IsOpen() && !TransactionActive())
						Close();

					// Chiude i file
					if (br != null) br.Close();
					if (fs != null) fs.Close();

					mMutex.ReleaseMutex();
				}
			}

			return true;
		}

        //**************************************************************************
		// Function WriteBlobField
		// Scrive un campo blob prelevandolo da un file
		// Parametri:
		//          tableName	: nome della tabella dove scrivere il campo blob
		//			columnName	: nome della colonna dove scrivere il campo blob
        //			optSet  	: eventuali altri campi da impostare
        //			sqlWhere	: condizione WHERE per identificare il record
		//			fileName	: nome del file dove prelevare il campo blob
        //          updateField : nome del campo da usare per indicare l'aggiornamento
        // Ritorna:
		//           true		= operazione eseguita senza errori.
		//           false		= operazione fallita.
		//**************************************************************************
		public bool WriteBlobField(string tableName, 
                                   string columnName, 
                                   string optSet, 
                                   string sqlWhere, 
                                   string fileName,
                                   string updatedField)
		{
			return WriteBlobField(tableName, columnName, optSet, sqlWhere, fileName, updatedField, true, "", "");
		}

        //**************************************************************************
        // Function WriteBlobField
        // Scrive un campo blob prelevandolo da un buffer
        // Parametri:
        //          tableName	: nome della tabella dove scrivere il campo blob
        //			columnName	: nome della colonna dove scrivere il campo blob
        //			optSet  	: eventuali altri campi da impostare
        //			sqlWhere	: condizione WHERE per identificare il record
        //			destBuffer	: buffer che contiene il campo da scrivere
        //          updateField : nome del campo da usare per indicare l'aggiornamento
        // Ritorna:
        //           true		= operazione eseguita senza errori.
        //           false		= operazione fallita.
        //**************************************************************************
        public bool WriteBlobField(string tableName, 
                                   string columnName, 
                                   string optSet, 
                                   string sqlWhere, 
                                   byte [] destBuffer,
                                   string updatedField )
        {
            int retry = 0;
            OleDbCommand cmd = null;
            int bufferLen = BUFFER_LENGTH;  // The size of the "chunks" of the image.
            bool transaction = !TransactionActive();
            string sqlCmd = "";
            bool update_done = false;


            //Riprova pi� volte
            while (retry < DEADLOCK_RETRY)
            {
                // Verifica se occorre aprire una transazione
                if (transaction) BeginTransaction();

                //Verifica se c'� gi� una query in esecuzione
                mMutex.WaitOne();

                // gestione errori nelle operazioni DB
                try
                {
                    if (cmd != null)
                    {
                        cmd.Cancel();
                        cmd.Dispose();
                        cmd = null;
                    }

                    // Recupera il puntatore del campo blob
                    sqlCmd = "SELECT ? = TEXTPTR(" + columnName + ") FROM " + tableName + " WHERE " + sqlWhere;
                    cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                    OleDbParameter ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
                    ptrParm.Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();

                    // verifica se il puntatore campo blob � valido...
                    if (ptrParm.Value == System.DBNull.Value)
                    {
                        // ... altrimenti ne crea uno di valido
                        cmd.Dispose();
                        cmd = null;
                        sqlCmd = "UPDATE " + tableName + " SET " + columnName + " = NULL " + optSet + " WHERE " + sqlWhere + ";" +
                                 sqlCmd;
                        cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                        ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
                        ptrParm.Direction = ParameterDirection.Output;
                        cmd.ExecuteNonQuery();
#if DONT_UPDATE_BLOB_FIELD
						update_done = true;
#endif
                    }

                    // legge il puntatore
                    byte[] pointer = (byte[])ptrParm.Value;
                    cmd.Dispose();
                    cmd = null;

                    // Crea la query per la memrorizzazione dell'immagine
                    sqlCmd = "UPDATETEXT " + tableName + "." + columnName + " ? ? NULL ?";
                    cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                    ptrParm = cmd.Parameters.Add("@Pointer", OleDbType.Binary, 16);
                    ptrParm.Value = pointer;
                    OleDbParameter offsetParm = cmd.Parameters.Add("@Offset", OleDbType.Integer);
                    offsetParm.Value = 0;	
                    OleDbParameter photoParm = cmd.Parameters.Add("@Bytes", OleDbType.VarBinary, bufferLen);

                    //''''''''''''''''''''''''''''''''''
                    // Read the image in and write it to the database 128 (bufferLen) bytes at a time.
                    // Tune bufferLen for best performance. Larger values write faster, but
                    // use more system resources.
                    int offset_ctr = 0;

                    if (destBuffer.Length < bufferLen )
                        bufferLen = destBuffer.Length;
                    byte[] buffer = new byte [bufferLen];
					Array.Copy(destBuffer, 0, buffer, 0, bufferLen);

                    while (buffer.Length > 0)
                    {
                        photoParm.Value = buffer;
                        cmd.ExecuteNonQuery();
                        offset_ctr += bufferLen;
                        offsetParm.Value = offset_ctr;
                        if ((offset_ctr + bufferLen) > destBuffer.Length)
                            bufferLen = destBuffer.Length - offset_ctr;
						if (bufferLen > 0)
						{
							buffer = null;
							buffer = new byte[bufferLen];
							Array.Copy(destBuffer, offset_ctr, buffer, 0, bufferLen);
						}
						else
							break;
                    }

                    cmd.Dispose();
                    cmd = null;

                    // Esegue un aggiornamento fittizio per generare il trigger
					if (!update_done && updatedField != "" && updatedField != null)
                    {
                        sqlCmd = "UPDATE " + tableName + " SET " + updatedField + " = " + updatedField + " " + optSet + " WHERE " + sqlWhere;
                        cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);
                        cmd.ExecuteNonQuery();
                    }

                    // Chiude l'eventuale transazione aperta
                    if (transaction) EndTransaction(true);

                    retry = DEADLOCK_RETRY * 10;
                }

                catch (OleDbException ex)
                {
                    string errorMessages = "";
                    bool deadLock = false;
					bool timeout = false;

                    //Registra le info sull'eccezione
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages += "Index #" + i + "\r\n" +
                            "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
                            "Message: " + ex.Errors[i].Message + "\r\n" +
                            "Source: " + ex.Errors[i].Source + "\r\n" +
                            "SQLState: " + ex.Errors[i].SQLState + "\r\n";

                        //Verifica se c'� anche un errore di deadlock
                        if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
                            deadLock = true;
                    }
					errorMessages += "Server: " + mConnectionString + "\r\n" +
						"Query: " + sqlCmd + "\r\n";

                    System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
                    log.Source = "DbInterface";
                    log.WriteEntry(errorMessages);
                    TraceLog.WriteLine(log.Source + " " + errorMessages + "Query: " + sqlCmd);

                    cmd.Cancel();

                    if (transaction) EndTransaction(false);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (deadLock && transaction && retry < DEADLOCK_RETRY)
                    {
                        Thread.Sleep(DEADLOCK_WAIT);
                        retry++;
                    }
					//Se timeout e non c'� una transazione attiva, riprova pi� volte
					else if (timeout && transaction && retry < DEADLOCK_RETRY)
					{
						Thread.Sleep(TIMEOUT_WAIT);
						retry++;
					}

					//Se deadlock, solleva l'eccezione di deadlock
                    else if (deadLock)
                        throw new DeadLockException(errorMessages);

					//Se timeout, solleva l'eccezione di timeout
					else if (timeout)
						throw new TimeoutException(errorMessages);

					//altri errori
                    else
                        throw ex;
                }

                    //Altre eccezioni
                catch (Exception ex)
                {
                    // Se ha aperto internamente la transazione, la chiude
                    if (transaction) EndTransaction(false);

                    throw (ex);
                }

                    // Operazioni finali
                finally
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                        cmd = null;
                    }

                    // Chiude la connessione, se non c'� una transazione attiva
                    if (IsOpen() && !TransactionActive())
                        Close();

                    mMutex.ReleaseMutex();
                }
            }

            return true;
        }

        //**************************************************************************
		// Function WriteBlobField
		// Scrive un campo blob prelevandolo da un file
		// Parametri:
		//          tableName	: nome della tabella dove scrivere il campo blob
		//			columnName	: nome della colonna dove scrivere il campo blob
		//			sqlWhere	: condizione WHERE per identificare il record
		//			fileName	: nome del file dove prelevare il campo blob
		// Ritorna:
		//           true		= operazione eseguita senza errori.
		//           false		= operazione fallita.
		//**************************************************************************
        public bool WriteBlobField(string tableName, 
									string columnName, 
									string sqlWhere, 
									string fileName)
        {
            return WriteBlobField(tableName, columnName, "", sqlWhere, fileName, "F_LUD ");
        }

		//**************************************************************************
		// Function WriteBlobField
		// Scrive un campo blob prelevandolo da un file
		// Parametri:
		//          tableName	: nome della tabella dove scrivere il campo blob
		//			columnName	: nome della colonna dove scrivere il campo blob
		//			sqlWhere	: condizione WHERE per identificare il record
		//			fileName	: nome del file dove prelevare il campo blob
		//			folderDest	: percorso dove archiviare la lastra
		// Ritorna:
		//           true		= operazione eseguita senza errori.
		//           false		= operazione fallita.
		//**************************************************************************
		public bool WriteBlobField(string tableName,
									string columnName,
									string sqlWhere,
									string sourceFileName,
									bool saveDB,
									string folderDest,
									string destFileName)
		{
			return WriteBlobField(tableName, columnName, "", sqlWhere, sourceFileName, "F_LUD ", saveDB, folderDest, destFileName);
		}

        //**************************************************************************
        // Function WriteBlobField
        // Scrive un campo blob prelevandolo da un buffer
        // Parametri:
        //          tableName	: nome della tabella dove scrivere il campo blob
        //			columnName	: nome della colonna dove scrivere il campo blob
        //			sqlWhere	: condizione WHERE per identificare il record
        //			destBuffer	: buffer che contiene il campo da scrivere
        // Ritorna:
        //           true		= operazione eseguita senza errori.
        //           false		= operazione fallita.
        //**************************************************************************
        public bool WriteBlobField(string tableName, 
									string columnName, 
									string sqlWhere, 
									byte[] destBuffer)
        {
            return WriteBlobField(tableName, columnName, "", sqlWhere, destBuffer, "F_CODE");
        }

        //**************************************************************************
		// Function WriteBlobField
		// Scrive un campo blob prelevandolo da un file
		// Parametri:
		//          tableName	: nome della tabella dove scrivere il campo blob
		//			columnName	: nome della colonna dove scrivere il campo blob
        //			optSet  	: eventuali altri campi da impostare
        //			sqlWhere	: condizione WHERE per identificare il record
		//			fileName	: nome del file dove prelevare il campo blob
        // Ritorna:
		//           true		= operazione eseguita senza errori.
		//           false		= operazione fallita.
		//**************************************************************************
		public bool WriteBlobField(string tableName, 
                                   string columnName, 
                                   string optSet, 
                                   string sqlWhere, 
                                   string fileName)
        {
            return WriteBlobField(tableName, columnName, optSet, sqlWhere, fileName, "F_CODE");
        }

        //**************************************************************************
        // Function WriteBlobField
        // Scrive un campo blob prelevandolo da un buffer
        // Parametri:
        //          tableName	: nome della tabella dove scrivere il campo blob
        //			columnName	: nome della colonna dove scrivere il campo blob
        //			optSet  	: eventuali altri campi da impostare
        //			sqlWhere	: condizione WHERE per identificare il record
        //			destBuffer	: buffer che contiene il campo da scrivere
        // Ritorna:
        //           true		= operazione eseguita senza errori.
        //           false		= operazione fallita.
        //**************************************************************************
        public bool WriteBlobField(string tableName, 
                                   string columnName, 
                                   string optSet, 
                                   string sqlWhere, 
                                   byte [] destBuffer )
        {
            return WriteBlobField(tableName, columnName, optSet, sqlWhere, destBuffer, "F_CODE");
        }

		/// <summary>
		/// Verifica le dimensioni del database di destinazione
		/// </summary>
		/// <param name="dbFull">Indica se il database ha raggiunto il limite impostato</param>
		/// <param name="total">Dimensione totale</param>
		/// <param name="actualval">Dimensione occupata</param>
		/// <param name="free">Dimensione libera</param>
		public bool CheckDbSize(out bool dbFull, out double total, out double actualval, out double free)
		{
			OleDbParameter par, odbp;
			ArrayList parameters = new ArrayList();

			dbFull = false;
			total = actualval = free = 0d;

			try
			{
				odbp = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
				odbp.Direction = ParameterDirection.ReturnValue;
				parameters.Add(odbp);

				par = new OleDbParameter("@TotalSizeinMB", OleDbType.Decimal, 2, System.Data.ParameterDirection.Output, false, 8, 2, "", DataRowVersion.Current, null);
				parameters.Add(par);

				par = new OleDbParameter("@UsedSizeinMB", OleDbType.Decimal, 2, System.Data.ParameterDirection.Output, false, 8, 2, "", DataRowVersion.Current, null);
				parameters.Add(par);

				par = new OleDbParameter("@FreeSizeinMB", OleDbType.Decimal, 2, System.Data.ParameterDirection.Output, false, 8, 2, "", DataRowVersion.Current, null);
				parameters.Add(par);


				if (this.Execute("sp_genutil_checkdbsize", ref parameters))
				{
					// Verifica il risultato ritornato
					odbp = (OleDbParameter)parameters[0];
					dbFull = ((int)(odbp.Value) != 0);// 0=ok, 1=full

					// Verifica il risultato ritornato
					odbp = (OleDbParameter)parameters[1];
					total = double.Parse(odbp.Value.ToString());

					// Verifica il risultato ritornato
					odbp = (OleDbParameter)parameters[2];
					actualval = double.Parse(odbp.Value.ToString());

					// Verifica il risultato ritornato
					odbp = (OleDbParameter)parameters[3];
					free = double.Parse(odbp.Value.ToString());
					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".CheckDbSize ", ex);
				return false;
			}
		}


        ////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     extract a blob field from a recordset and save it to the 
        ///     indicated file
        /// </summary>
        /// <param name="dbinterface">
        ///     db interface object
        /// </param>
        /// <param name="ord">
        ///     recordset containg the blob field
        /// </param>
        /// <param name="fieldnumber">
        ///     number of the blob field (0..n-1) into recordset
        /// </param>
        /// <param name="outfilename">
        ///     destination file name (full path)
        /// </param>
        /// <param name="onlyifnotexist">
        ///     if true save file only if it does not exist
        /// </param>
        /// <returns>
        ///     true if operation completed correctly
        ///     false otherwise
        /// </returns>
        ////////////////////////////////////////////////////////////////////////////
        public static bool SaveBlobFildToFile(DbInterface dbinterface, OleDbDataReader ord, int fieldnumber, string outfilename, bool onlyifnotexist)
        {
            if (onlyifnotexist)
                if (System.IO.File.Exists(outfilename))
                    return true;

            byte[] refbuffer = null;
            if (!dbinterface.GetBlobField(ord, fieldnumber, ref refbuffer))
                return false;

            FileStream fs = new FileStream(outfilename, FileMode.Create);
            if (fs == null)
                return false;
            fs.Write(refbuffer, 0, refbuffer.Length);
            fs.Close();

            return true;
        }

        ////////////////////////////////////////////////////////////////////////////
        /// <summary>
        ///     extract a blob field from a recordset and save it to the 
        ///     indicated byte array
        /// </summary>
        /// <param name="dbinterface">
        ///     db interface object
        /// </param>
        /// <param name="ord">
        ///     recordset containg the blob field
        /// </param>
        /// <param name="fieldnumber">
        ///     number of the blob field (0..n-1) into recordset
        /// </param>
        /// <param name="buffer">
        ///     destination byte array
        /// </param>
        /// <returns>
        ///     true if operation completed correctly
        ///     false otherwise
        /// </returns>
        ////////////////////////////////////////////////////////////////////////////
        public static bool SaveBlobFildToByteArray(DbInterface dbinterface, OleDbDataReader ord, int fieldnumber, out byte[] buffer)
        {
            buffer = null;
            if (!dbinterface.GetBlobField(ord, fieldnumber, ref buffer))
                return false;

            return true;
        }
	}
}

	
