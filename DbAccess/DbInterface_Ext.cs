﻿using System;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using TraceLoggers;

namespace DbAccess
{
    public partial class DbInterface
    {
        public string ServerName
        {
            get
            {
                return mDbConnection.DataSource;
            }
        }

        public string DatabaseName
        {
            get
            {
                return mDbConnection.Database;
            }
        }


        public DbInterface(DbInterface dbInterface):this()
        {
            mConnectionString = dbInterface.mConnectionString;

            mDbCommand.Connection = mDbConnection;
            mDbCommand.CommandType = CommandType.StoredProcedure;

        }

        public DbInterface Clone()
        {
            return new DbInterface(this);
        }

        public OleDbConnection CurrentConnection
        {
            get { return mDbConnection; }
        }

        //**************************************************************************
        // Function Requery
        // Ritorna un recordset con una nuovo comando SQL
        // Parametri:
        //           sqlCmd			: stringa che contiene il nuovo comando SQL
        //           sqlDataReader	: recordset interessato dall'operazione
        //			 singleRow		: si aspetta una sola riga di risultato
        // Ritorna:
        //           true		= operazione eseguita senza errori.
        //           false		= operazione fallita.
        //**************************************************************************
        public bool Requery(string sqlCmd, out OleDbDataReader dbDataRead, ref ArrayList parameters, int timeout = -1)
        {
            return Requery(sqlCmd, out dbDataRead, false, false, ref  parameters, timeout);
        }
        public bool Requery(string sqlCmd, out OleDbDataReader dbDataRead, bool singleRow, ref ArrayList parameters, int timeout = -1)
        {
            return Requery(sqlCmd, out dbDataRead, singleRow, false,  ref  parameters, timeout);
        }

        public bool Requery(string sqlCmd, out OleDbDataReader dbDataRead, bool singleRow, bool sequentialAccess, ref ArrayList parameters, int timeout = -1)
        {

            int retry = 0;
            OleDbCommand cmd = null;
            dbDataRead = null;
            CommandBehavior flagCmd = CommandBehavior.Default;
            bool rc = false;

            //Riprova più volte
            while (retry < DEADLOCK_RETRY)
            {
                //Verifica se c'è già una query in esecuzione
                mMutex.WaitOne();

                // Se chiusa, apre la connessione
                if (!IsOpen())
                    mDbConnection.Open();

                // gestione errori nelle operazioni DB
                try
                {
                    if (cmd != null)
                    {
                        cmd.Cancel();
                        cmd.Dispose();
                        cmd = null;
                    }
                    cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);

                    if (singleRow)
                        flagCmd |= CommandBehavior.SingleRow;
                    if (sequentialAccess)
                        flagCmd |= CommandBehavior.SequentialAccess;

                    // Timeout
                    if (timeout != -1)
                    {
                        cmd.CommandTimeout = timeout;
                    }

                    // Assegna i parametri
                    if (parameters != null)
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (OleDbParameter par in parameters)
                            cmd.Parameters.Add(par);
                    }


                    dbDataRead = cmd.ExecuteReader(flagCmd);

                    retry = DEADLOCK_RETRY * 10;
                    rc = true;
                }

                catch (OleDbException ex)
                {
                    string errorMessages = "";
                    bool deadLock = false;

                    //Registra le info sull'eccezione
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages += "Index #" + i + "\r\n" +
                            "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
                            "Message: " + ex.Errors[i].Message + "\r\n" +
                            "Source: " + ex.Errors[i].Source + "\r\n" +
                            "SQLState: " + ex.Errors[i].SQLState + "\r\n";

                        //Verifica se c'è anche un errore di deadlock
                        if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
                            deadLock = true;
                    }
                    errorMessages += "Server: " + mConnectionString + "\r\n" +
                        "Query: " + sqlCmd + "\r\n";

                    System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
                    log.Source = "DbInterface";
                    log.WriteEntry(errorMessages);
                    TraceLog.WriteLine(log.Source + " " + errorMessages);

                    if (dbDataRead != null) dbDataRead.Close();
                    cmd.Cancel();

                    //Se deadlock e non c'è una transazione attiva, riprova più volte
                    if (deadLock && !TransactionActive() && retry < DEADLOCK_RETRY)
                    {
                        Thread.Sleep(DEADLOCK_WAIT);
                        retry++;
                    }

                    //Se deadlock, solleva l'eccezione di deadlock
                    else if (deadLock)
                        throw new DeadLockException(errorMessages);

                    //Altrimenti, ripete la stessa eccezione
                    else
                        throw ex;
                }

                //Altre eccezioni
                catch (Exception ex)
                {
                    if (dbDataRead != null) dbDataRead.Close();
                    cmd.Cancel();
                    throw ex;
                }

                finally
                {
                    cmd.Dispose();
                    cmd = null;
                    if (!rc)
                        mMutex.ReleaseMutex();
                }
            }

            return rc;
        }

        //**************************************************************************
        // Function BeginTransaction
        // Inizio di una nuova transazione verso DB
        // Ritorna:
        //           FALSE = errore nell'operazione
        //           TRUE  = transazione iniziata
        //**************************************************************************
        public bool BeginTransaction(IsolationLevel isolationLevel)
        {
            try
            {
                mMutexTransaction.WaitOne();

                // Se chiusa, apre la connessione
                if (!IsOpen())
                    mDbConnection.Open();

                // Start a local transaction
                mTransaction = mDbConnection.BeginTransaction(isolationLevel);
                mActiveTransaction = true;
                return true;
            }

            catch (OleDbException ex)
            {
                mTransaction = null;

                string errorMessages = "";
                bool deadLock = false;
                bool timeout = false;

                //Registra le info sull'eccezione
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    errorMessages += "Index #" + i + "\r\n" +
                        "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
                        "Message: " + ex.Errors[i].Message + "\r\n" +
                        "Source: " + ex.Errors[i].Source + "\r\n" +
                        "SQLState: " + ex.Errors[i].SQLState + "\r\n";

                    //Verifica se c'è anche un errore di deadlock
                    if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
                        deadLock = true;
                    else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
                        timeout = true;
                }
                errorMessages += "Server: " + mConnectionString + "\r\n";

                System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
                log.Source = "DbInterface";
                log.WriteEntry(errorMessages);
                mActiveTransaction = false;
                mMutexTransaction.ReleaseMutex();

                //Se deadlock, solleva l'eccezione di deadlock
                if (deadLock)
                    throw new DeadLockException(errorMessages);

                //Se timeout, solleva l'eccezione di timeout
                else if (timeout)
                    throw new TimeoutException(errorMessages);

                //altri errori
                else
                    throw ex;
            }

            catch (Exception ex)
            {
                mActiveTransaction = false;
                mMutexTransaction.ReleaseMutex();
                throw (ex);
            }

            finally { }
        }



        public bool ExecuteNonQuery(string sqlCmd, out int affectedRows)
        {
            int retry = 0;
            OleDbCommand cmd = null;
            bool rc = false;

            affectedRows = 0;

            //Riprova più volte
            while (retry < DEADLOCK_RETRY)
            {
                //Verifica se c'è già una query in esecuzione
                mMutex.WaitOne();

                // Se chiusa, apre la connessione
                if (!IsOpen())
                    mDbConnection.Open();

                // gestione errori nelle operazioni DB
                try
                {
                    if (cmd != null)
                    {
                        cmd.Cancel();
                        cmd.Dispose();
                        cmd = null;
                    }

                    // Apre il command
                    cmd = new OleDbCommand(sqlCmd, mDbConnection, mTransaction);

                    object obj = cmd.ExecuteNonQuery();
                    if (obj != null && obj != DBNull.Value)
                        affectedRows = int.Parse(obj.ToString());	// identity = decimal.ToInt32((decimal) obj);

                    retry = DEADLOCK_RETRY * 10;
                    rc = true;
                }

                catch (OleDbException ex)
                {
                    string errorMessages = "";
                    bool deadLock = false;
                    bool timeout = false;

                    //Registra le info sull'eccezione
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages += "Index #" + i + "\r\n" +
                            "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
                            "Message: " + ex.Errors[i].Message + "\r\n" +
                            "Source: " + ex.Errors[i].Source + "\r\n" +
                            "SQLState: " + ex.Errors[i].SQLState + "\r\n";

                        //Verifica se c'è anche un errore di deadlock
                        if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
                            deadLock = true;
                        else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
                            timeout = true;
                    }
                    errorMessages += "Server: " + mConnectionString + "\r\n" +
                        "Query: " + sqlCmd + "\r\n";

                    System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
                    log.Source = "DbInterface";
                    log.WriteEntry(errorMessages);
                    TraceLog.WriteLine(log.Source + " " + errorMessages + "Query: " + sqlCmd);

                    cmd.Cancel();

                    //Se deadlock e non c'è una transazione attiva, riprova più volte
                    if (deadLock && !TransactionActive() && retry < DEADLOCK_RETRY)
                    {
                        Thread.Sleep(DEADLOCK_WAIT);
                        retry++;
                    }
                    //Se timeout e non c'è una transazione attiva, riprova più volte
                    else if (timeout && !TransactionActive() && retry < DEADLOCK_RETRY)
                    {
                        Thread.Sleep(TIMEOUT_WAIT);
                        retry++;
                    }

                    //Se deadlock, solleva l'eccezione di deadlock
                    else if (deadLock)
                        throw new DeadLockException(errorMessages);

                    //Se timeout, solleva l'eccezione di timeout
                    else if (timeout)
                        throw new TimeoutException(errorMessages);

                    //altri errori
                    else
                        throw ex;
                }

                //Altre eccezioni
                catch (Exception ex)
                {
                    throw (ex);
                }

                // Operazioni finali
                finally
                {
                    cmd.Dispose();
                    cmd = null;

                    // Chiude la connessione, se non c'è una transazione attiva
                    if (IsOpen() && !TransactionActive())
                        Close();

                    mMutex.ReleaseMutex();
                }
            }

            return rc;
        }


        public bool ExecuteNonQuery(OleDbCommand command, out int affectedRows)
        {
            int retry = 0;
            OleDbCommand cmd = null;
            bool rc = false;

            affectedRows = 0;

            //Riprova più volte
            while (retry < DEADLOCK_RETRY)
            {
                //Verifica se c'è già una query in esecuzione
                mMutex.WaitOne();

                // Se chiusa, apre la connessione
                if (!IsOpen())
                    mDbConnection.Open();

                // gestione errori nelle operazioni DB
                try
                {
                    //if (cmd != null)
                    //{
                    //    cmd.Cancel();
                    //    cmd.Dispose();
                    //    cmd = null;
                    //}

                    // Apre il command
                    //cmd = new OleDbCommand(command.CommandText, mDbConnection, mTransaction);
                    //if (command.Parameters != null && command.Parameters.Count > 0)
                    //{
                    //    foreach (OleDbParameter param in command.Parameters)
                    //    {
                    //        cmd.Parameters.Add(param);
                    //    }
                    //}

                    cmd = command;
                    cmd.Connection = mDbConnection;
                    cmd.Transaction = mTransaction;

                    object obj = cmd.ExecuteNonQuery();
                    if (obj != null && obj != DBNull.Value)
                        affectedRows = int.Parse(obj.ToString());	// identity = decimal.ToInt32((decimal) obj);

                    retry = DEADLOCK_RETRY * 10;
                    rc = true;
                }

                catch (OleDbException ex)
                {
                    string errorMessages = "";
                    bool deadLock = false;
                    bool timeout = false;

                    //Registra le info sull'eccezione
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages += "Index #" + i + "\r\n" +
                            "NativeError: " + ex.Errors[i].NativeError + "\r\n" +
                            "Message: " + ex.Errors[i].Message + "\r\n" +
                            "Source: " + ex.Errors[i].Source + "\r\n" +
                            "SQLState: " + ex.Errors[i].SQLState + "\r\n";

                        //Verifica se c'è anche un errore di deadlock
                        if (ex.Errors[i].NativeError == 1205 || ex.Errors[i].SQLState == "40001")
                            deadLock = true;
                        else if (ex.Errors[i].NativeError == 0 || ex.Errors[i].SQLState == "HTY00")
                            timeout = true;
                    }
                    errorMessages += "Server: " + mConnectionString + "\r\n" +
                        "Query: " + command.CommandText + "\r\n";

                    System.Diagnostics.EventLog log = new System.Diagnostics.EventLog();
                    log.Source = "DbInterface";
                    log.WriteEntry(errorMessages);
                    TraceLog.WriteLine(log.Source + " " + errorMessages + "Query: " + command.CommandText);

                    cmd.Cancel();

                    //Se deadlock e non c'è una transazione attiva, riprova più volte
                    if (deadLock && !TransactionActive() && retry < DEADLOCK_RETRY)
                    {
                        Thread.Sleep(DEADLOCK_WAIT);
                        retry++;
                    }
                    //Se timeout e non c'è una transazione attiva, riprova più volte
                    else if (timeout && !TransactionActive() && retry < DEADLOCK_RETRY)
                    {
                        Thread.Sleep(TIMEOUT_WAIT);
                        retry++;
                    }

                    //Se deadlock, solleva l'eccezione di deadlock
                    else if (deadLock)
                        throw new DeadLockException(errorMessages);

                    //Se timeout, solleva l'eccezione di timeout
                    else if (timeout)
                        throw new TimeoutException(errorMessages);

                    //altri errori
                    else
                        throw ex;
                }

                //Altre eccezioni
                catch (Exception ex)
                {
                    throw (ex);
                }

                // Operazioni finali
                finally
                {
                    cmd.Dispose();
                    cmd = null;

                    // Chiude la connessione, se non c'è una transazione attiva
                    if (IsOpen() && !TransactionActive())
                        Close();

                    mMutex.ReleaseMutex();
                }
            }

            return rc;
        }

        //public bool GetFilestreamImage()
        //public bool GetFilestreamImage(string tableName, string columnName, string sqlWhere, string fileName)
        //{
        //    string sqlCmd = "";
        //    OleDbDataReader dbReader = null;

        //    bool transaction = !TransactionActive();
        //    bool retVal = false;

        //    try
        //    {
        //        if (transaction) BeginTransaction();


        //        sqlCmd = "SELECT " + columnName + ".PathName() as filePath, " + 
        //            "GET_FILESTREAM_TRANSACTION_CONTEXT() AS txContext " +
        //            "FROM " + tableName + " WHERE " + sqlWhere;

        //        // Legge il datareader con il campo BLOB
        //        Requery(sqlCmd, out dbReader, true, true);

        //        if (dbReader.Read())
        //        {
        //            if (dbReader[0] != System.DBNull.Value)
        //            {
        //                string filePath = dbReader.GetString(0);
        //                object objContext = dbReader.GetValue(1);
        //                byte[] txContext = (byte[])objContext;

        //                SqlFileStream sfs = new SqlFileStream(filePath, txContext, FileAccess.Read);

        //                FileStream fStream = new FileStream(fileName, FileMode.CreateNew);

        //                //Transfer data from FILESTREAM data file to disk file using a 4KB buffer size

        //                sfs.CopyTo(fStream, 4096);


        //                //Close the source file
        //                fStream.Close();


        //                retVal = true;
        //            }
        //        }


        //    }


        //        //Altre eccezioni
        //    catch (Exception ex)
        //    {
        //        if (transaction) EndTransaction(false);
        //        TraceLog.WriteLine("GetFileStreamImage error", ex);
        //        retVal = false;
        //    }

        //    finally
        //    {
        //        // Chiude la query
        //        EndRequery(dbReader);

        //        if (transaction) EndTransaction(false);

        //    }

        //    return retVal;

        //}

    }
}
