using System;
using System.IO;

namespace DbAccess
{
	public class UDLConnectionString
	{
		private string m_strFileName;
		private string m_strConnectionText;
		private string m_strPesistSecurity;
		private string m_strIntegradedSecurity;
		private string m_strReconstructed;
		private string m_strDefaultFilename;
		private string m_strDatasource;
		private string m_strPassword;
		private string m_strUserID;
		private string m_strCatalog;

	    public string Catalog
	    {
	        get { return m_strCatalog; }
	    }

	    public string Datasource
	    {
	        get { return m_strDatasource; }
	        set { m_strDatasource = value; }
	    }

	    //	public event CloseApplicationEventHandler CloseApplication;

		#region "Public Methods"
		public void GetLogonInformation(ref string strServerName, ref string strUserID, ref string strPassword, ref string strDatabaseName)
		{
            strServerName = m_strDatasource;
			strPassword = m_strPassword;
			strUserID = m_strUserID;
            strDatabaseName = m_strCatalog;
		}

		public string GetSQLConnectionString()
		{
			ReconstructSQLConnectionString();
			return m_strReconstructed;
		}

		public string GetOracleConnectionString()
		{
			ReconstructOracleConnectionString();
			return m_strReconstructed;
		}
		#endregion

		#region "Constructors"
		public UDLConnectionString()
		{
			m_strDefaultFilename = "wfp.udl";
			ReadFromFile();
			SplitConnectionString();
		}

		public UDLConnectionString(ref string strDefaultFilename)
		{
			m_strDefaultFilename = strDefaultFilename;
			ReadFromFile();
			SplitConnectionString();
		}
		#endregion

		#region "Recostruction"
		private void ReconstructSQLConnectionString()
		{
			m_strReconstructed = m_strPesistSecurity + ";";
			if (m_strIntegradedSecurity != null)
			{
				m_strReconstructed += m_strIntegradedSecurity + ";";
			} 
			else 
			{
				m_strReconstructed += m_strPassword + ";";
				if (m_strUserID.Length != 0)
				{
					m_strReconstructed += m_strUserID + ";";
				}
			}
            m_strReconstructed += m_strCatalog + ";";
            m_strReconstructed += m_strDatasource + ";";
            m_strCatalog = m_strCatalog.Substring(16);
            m_strDatasource = m_strDatasource.Substring(12);
			if (m_strPassword != null) 
			{
				m_strPassword = m_strPassword.Substring(9);
				m_strUserID = m_strUserID.Substring(8);
			}
		}

		private void ReconstructOracleConnectionString()
		{
			if (m_strIntegradedSecurity == null) 
			{
				m_strReconstructed += m_strPassword + ";";
				m_strReconstructed += m_strUserID + ";";
			} 
			else 
			{
				m_strReconstructed += m_strIntegradedSecurity + ";";
			}
            m_strReconstructed += m_strDatasource + ";";
            m_strDatasource = m_strDatasource.Substring(12);
			if (m_strPassword != null) 
			{
				m_strPassword = m_strPassword.Substring(9);
				m_strUserID = m_strUserID.Substring(8);
			}
		}
		#endregion

		#region "Spliting"
		private void SplitConnectionString()
		{
			string[] strArray = null;
			string strProvider="";
			string strDRIVER="";
			string strSERVER="";
			string strUID="";
			string strDATABASE="";
			strArray = m_strConnectionText.Split(';');
			if (strArray[0].IndexOf("[oledb]") == -1) 
			{
					string strDescription = "Connection Failed.\r\n" + "Closing Application";
					throw (new Exception (strDescription));
			}
			foreach (string strString in strArray) 
			{
				if (strString.IndexOf("Provider") > -1) 
				{
					strProvider = strString.Substring(strString.IndexOf("Provider"));
				}
				if (strString.IndexOf("Password") > -1) 
				{
					m_strPassword = strString;
				}
				if (strString.IndexOf("Data") > -1) 
				{
                    m_strDatasource = strString;
				}
				if (strString.IndexOf("User") > -1) 
				{
					m_strUserID = strString;
				}
				if (strString.IndexOf("Initial") > -1) 
				{
					m_strCatalog = strString;
				}
				if (strString.IndexOf("Integrated") > -1) 
				{
					m_strIntegradedSecurity = strString;
				}
				if (strString.IndexOf("Persist") > -1) 
				{
					m_strPesistSecurity = strString;
				}
				if (strString.IndexOf("DRIVER") > -1) 
				{
					strDRIVER = strString.Substring(strString.IndexOf("DRIVER"));
				}
				if (strString.IndexOf("SERVER") > -1) 
				{
					strSERVER = strString;
				}
				if (strString.IndexOf("UID") > -1) 
				{
					strUID = strString;
				}
				if (strString.IndexOf("DATABASE") > -1) 
				{
					strDATABASE = strString;
					strDATABASE = strDATABASE.Remove(strDATABASE.Length - 3, 3);
				}
			}
			if (strProvider.IndexOf("MSDASQL") > -1) 
			{
				if (strDRIVER.IndexOf("SQL Server") > -1) 
				{
                    m_strDatasource = "Data Source" + strSERVER.Substring(6);
					if (strUID.Length > 0) 
					{
						m_strUserID = "User ID" + strUID.Substring(3);
					}
					m_strCatalog = "Initial Catalog " + strDATABASE.Substring(8);
				} 
				else 
				{
					string strDescription = "Connection Failed.\r\n" + "Closing Application";
					throw (new Exception (strDescription));
				}
			} 
			else if (strProvider.IndexOf("SQLOLEDB") > -1) 
			{
                m_strDatasource = m_strDatasource.Remove(m_strDatasource.Length - 2, 2);
			}
			else if (strProvider.IndexOf("SQLNCLI") > -1)
			{
                m_strDatasource = m_strDatasource.Remove(m_strDatasource.Length - 2, 2);
			}
			else if (strProvider.IndexOf("OraOLEDB") > -1)
			{
			}
			else if (strProvider.IndexOf("MSDAORA") > -1)
			{
			}
			else
			{
				string strDescription = "Connection Failed.\r\n" + "Closing Application";
				throw (new Exception(strDescription));
			}
		}
		#endregion

		#region "Data Link Files"
		private void ReadFromFile()
		{
			StreamReader myStreamReader=null;
			string strFileName;
			try 
			{
				m_strFileName = AppDomain.CurrentDomain.BaseDirectory + m_strDefaultFilename;
				strFileName = m_strFileName;
				if (!(File.Exists(m_strFileName))) 
				{
					m_strFileName = strFileName;
					CreateUDLFile(strFileName);
				}
				myStreamReader = File.OpenText(m_strFileName);
				m_strConnectionText = myStreamReader.ReadToEnd();
			} 
			catch (Exception exc) 
			{
				string strDescription = "File could not be opened or read.\r\n" + "Please verify that the filename is correct, " + "and that you have read permissions for the desired directory.\r\n\r\n" + "Exception: " + exc.Message;
				throw(new Exception(strDescription, exc));
				/*			if (CloseApplication != null) 
							{
								CloseApplication();
							}*/
			} 
			finally 
			{
				if (myStreamReader != null)
				{
					myStreamReader.Close();
				}
			}
		}

		private void CreateUDLFile(string strFileName)
		{
			System.IO.FileStream fs = new System.IO.FileStream(strFileName, System.IO.FileMode.CreateNew);
			fs.Close();
			/*		ProcessStartInfo prProcess = new ProcessStartInfo();
					prProcess.FileName = strFileName;
					Process pProcess = Process.Start(prProcess);
					pProcess.WaitForInputIdle();
					pProcess.WaitForExit();
					pProcess.Close();*/
			System.IO.StreamReader myStreamReader = System.IO.File.OpenText(strFileName);
			string m_strConnectionText = myStreamReader.ReadToEnd();
			myStreamReader.Close();
			string[] strArray = m_strConnectionText.Split(';');
			if (strArray[0].IndexOf("[oledb]") == -1) 
			{
				if (System.IO.File.Exists(strFileName)) 
				{
					System.IO.File.Delete(strFileName);
				}
			}
		}

	    #endregion
	}
}