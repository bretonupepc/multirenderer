using System;

namespace DbAccess
{
	public abstract class DbObject: IComparable
	{
		#region Variables
		
		public enum ObjectStates { Unchanged, Updated, Inserted, Deleted };
		private ObjectStates mState;

        private int mTableId = 0;		//Id elemento nella tabella di visualizzazione

		#endregion

		#region Constructors
		public DbObject()
		{
			mState = ObjectStates.Unchanged;
		}
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public abstract int CompareTo(object obj);

		#endregion

		#region Properties

		public ObjectStates gState
		{
			set { mState = value; }
			get { return mState; }
		}

		public int gTableId
		{
			set { mTableId = value; }
			get { return mTableId; }
		}

		#endregion 

		#region Public Methods

		public abstract TypeCode GetFieldType(int index);
		public abstract TypeCode GetFieldType(string key);

		public virtual bool GetField(int index, out object retValue)
		{
			retValue = null;
			return false;
		}
		public virtual bool GetField(string key, out object retValue)
		{
			retValue = null;
			return false;
		}

		public virtual bool GetField(int index, out int retValue)
		{
			retValue = 0;
			return false;
		}
		public virtual bool GetField(string key, out int retValue)
		{
			retValue = 0;
			return false;
		}

		public virtual bool GetField(int index, out string retValue)
		{
			retValue = "";
			return false;
		}

		public virtual bool GetField(string key, out string retValue)
		{
			retValue = "";
			return false;
		}

		public virtual bool GetField(int index, out double retValue)
		{
			retValue = 0.0;
			return false;
		}

		public virtual bool GetField(string key, out double retValue)
		{
			retValue = 0.0;
			return false;
		}


		public virtual bool GetField(int index, out DateTime retValue)
		{
			retValue = DateTime.MinValue;
			return false;
		}

		public virtual bool GetField(string key, out DateTime retValue)
		{
			retValue = DateTime.MinValue;
			return false;
		}

		public virtual bool GetField(int index, out Decimal retValue)
		{
			retValue = 0;
			return false;
		}

		public virtual bool GetField(string key, out Decimal retValue)
		{
			retValue = 0;
			return false;
		}

		public virtual bool GetField(int index, out bool retValue)
		{
			retValue = false;
			return false;
		}

		public virtual bool GetField(string key, out bool retValue)
		{
			retValue = false;
			return false;
		}

		public virtual bool GetField(int index, out char retValue)
		{
			retValue = '\0';
			return false;
		}

		public virtual bool GetField(string key, out char retValue)
		{
			retValue = '\0';
			return false;
		}

		public virtual bool GetField(int index, out byte retValue)
		{
			retValue = 0;
			return false;
		}

		public virtual bool GetField(string key, out byte retValue)
		{
			retValue = 0;
			return false;
		}
		#endregion

	}
}
