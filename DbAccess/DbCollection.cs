using System;
using System.Collections;
using System.Data.OleDb;
using TraceLoggers;

namespace DbAccess
{
	public abstract class DbCollection
	{
		#region Variables

		protected ArrayList mRecordset;
		protected SortedList mIndex;
		protected ArrayList mDeleted;
		protected DbInterface mDbInterface;
        protected int mCursor;
		protected string mSqlGetData = "";
		
		#endregion

		#region Constructors
		public DbCollection(DbInterface db)
		{
			mDbInterface = db;
			mRecordset = new ArrayList();
			mIndex = new SortedList();
			mDeleted = new ArrayList();
			mCursor = 0;
		}

		#endregion

		#region Properties

		public int Count
		{
			get { return mRecordset.Count; }
		}

		#endregion

		#region Public Methods

		public virtual void Clear()
		{
			mRecordset.Clear();
			mIndex.Clear();
			mDeleted.Clear();
			mCursor = 0;
		}

		public virtual bool IsEmpty()
		{
			if (mRecordset == null)
				return true;
			return (mRecordset.Count == 0);
		}
		
		public virtual bool IsBOF()
		{
			if (IsEmpty())
				return true;

			return (mCursor < 0);
		}

		public bool IsStartPosition()
		{
			if (IsEmpty())
				return false;

			return (mCursor == 0);
		}

		public bool IsEndPosition()
		{
			if (IsEmpty())
				return false;

			return (mCursor == (mRecordset.Count - 1));
		}

		public virtual bool IsEOF()
		{
			if (IsEmpty())
				return true;

			return (mCursor >= mRecordset.Count);
		}

		public virtual void MoveFirst()
		{
			if (!IsEmpty())
				mCursor = 0;
		}

		public virtual void MoveLast()
		{
			if (!IsEmpty())
				mCursor = mRecordset.Count-1;
		}

		public virtual void MoveNext()
		{
			if (!IsEmpty() && !IsEOF())
				mCursor++;
		}

		public virtual void MovePrevious()
		{
			if (!IsEmpty() && !IsBOF())
				mCursor--;
		}

        public virtual void MoveTo(int i)
        {
            if (!IsEmpty() && IsIndexOk(i))
                mCursor = i;
        }

		//**********************************************************************
		// DeleteObjectTable
		// Cancella l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		// Ritorna:
		//			true	oggetto cancellato
		//			false	errore nella cancellazione oggetto
		//**********************************************************************
		public bool DeleteObjectTable(int tableId)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
				return DeleteObject(i);
	
			//oggetto non trovato
			return false;
		}

		//**********************************************************************
		// DeleteObject
		// Cancella l'oggetto passato come parametro
		// Parametri:
		//			objDel	: oggetto da cancellare
		// Ritorna:
		//			true	oggetto cancellato
		//			false	errore nella cancellazione oggetto
		//**********************************************************************
		public virtual bool DeleteObject(DbObject objDel)
		{
			if (IsEmpty())
				return false;

			//Recupera l'indice dell'oggetto
			int i = mRecordset.IndexOf(objDel);
			if (IsIndexOk(i))
				//Cancella l'oggetto
				return (DeleteObject(i));
			else
				return false;
		}
		
		//**********************************************************************
		// DeleteObject
		// Cancella l'oggetto attualmente indicato dal cursore
		// Ritorna:
		//			true	oggetto cancellato
		//			false	errore nella cancellazione oggetto
		//**********************************************************************
		public virtual bool DeleteObject()
		{
			return (DeleteObject(mCursor));
		}

		//**********************************************************************
		// DeleteObject
		// Cancella l'oggetto attualmente indicato dal cursore
		// Ritorna:
		//			true	oggetto cancellato
		//			false	errore nella cancellazione oggetto
		//**********************************************************************
		public virtual bool DeleteObject(int index)
		{
			DbObject obj;
			
			if (IsEmpty())
				return false;

			// Verifica l'indice
			if (!IsIndexOk(index))
				return false;

			obj = (DbObject) mRecordset[index];
			obj.gState = DbObject.ObjectStates.Deleted;
			mRecordset.RemoveAt(index);
			mDeleted.Add(obj);

			//Controlla il posizionamento del cursore
			if (IsEmpty())
				mCursor = 0;
			else if (IsEOF())
				MoveLast(); 

			return true;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out DbObject obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (DbObject) mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		public virtual bool GetObject(out DbObject obj)
		{
			if (IsEmpty() || IsEOF())
			{
				obj = null;
				return false;
			}

			obj = (DbObject) mRecordset[mCursor];
			return true;
		}

		public virtual bool AddObject(DbObject obj)
		{
			mRecordset.Add(obj);
			obj.gState=DbObject.ObjectStates.Inserted;
            
			return true;
		}

		public abstract bool GetDataFromDb();

		public abstract bool UpdateDataToDb();

		public virtual void SortByField(int index)
		{
			//Verifica se � vuoto
			if (IsEmpty())
				return;

		}

		public abstract void SortByField(string key);

		/***********************************************************************
		 * GetNewCode
		 * Ritorna un nuovo codice
		 * Parametri:
		 *			prefix		: prefisso del codice
		 *			checkExist	: verifica se il codice gi� esiste
		 * Ritorna:
		 *			codice ritornato
		 *			""	errore
		 ************************************************************************/
		public virtual string GetNewCode(string prefix, bool checkExist)
		{
			// Definisce i parametri della stored procedure
			// sp_get_new_code (@prefix varchar(18), @check_exist int, @code varchar(18) OUTPUT) 

			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			try
			{

				par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
				par.Direction = System.Data.ParameterDirection.ReturnValue;
				parameters.Add(par);

				par = new OleDbParameter("@prefix", OleDbType.VarChar, 18);
				par.Value = prefix;
				parameters.Add(par);

				par = new OleDbParameter("@check_exist", OleDbType.Integer, 18);
				par.Value = (checkExist ? 1 : 0);
				parameters.Add(par);

				par = new OleDbParameter("@code", OleDbType.VarChar, 18);
				par.Direction = System.Data.ParameterDirection.Output;
				parameters.Add(par);

				// Lancia la stored procedure
				if (!mDbInterface.Execute("sp_get_new_code", ref parameters))
					return "";

				par = (OleDbParameter) parameters[0];
				if (((int) par.Value) == 0)
					return "";

				par = (OleDbParameter) parameters[3];
				return par.Value.ToString();
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("DbCollection.GetNewCode Error", ex);
				return "";
			}
		}

		/***********************************************************************
		 * CodeFromId
		 * Ritorna il codice passando l'id
		 * Parametri:
		 *			id			: prefisso del codice
		 *			tableName	: nome della tabella su cui ricercare
		 * Ritorna:
		 *			codice trovato
		 ************************************************************************/
		public string CodeFromId(int id, string tableName)
		{
			return CodeFromId(id, "F_ID", tableName);
		}

		public string CodeFromId(int id, string idField, string tableName)
		{
			string sqlCmd = "";
			OleDbDataReader dbReader = null;

			sqlCmd = "SELECT F_CODE FROM " + tableName + " WHERE " + idField + " = " + id;

			try
			{

				mDbInterface.Requery(sqlCmd, out dbReader, true);

				if(dbReader.HasRows && dbReader.Read())
					return dbReader[0].ToString();

				return null;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				// Chiude la query
				mDbInterface.EndRequery(dbReader);
			}
		}

		/***********************************************************************
		 * CheckExistingCode
		 * Ritorna un nuovo codice
		 * Parametri:
		 *			code		: codice da controllare
		 *			table		: tabella da controllare
		 *			exist		: ritorna true se il codice esiste
		 * Ritorna:
		 *			true	controllo eseguito
		 *			false	errore durante il controllo
		 ************************************************************************/
		public virtual bool CheckExistingCode(string code, string table, out bool exist)
		{
			// Definisce i parametri della stored procedure
			// sp_check_existing_code (@code varchar(18), @table varchar(32), @codeField varchar(32) = 'F_CODE')

			OleDbParameter par;
			ArrayList parameters = new ArrayList();
			exist = false;

			try
			{
				par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
				par.Direction = System.Data.ParameterDirection.ReturnValue;
				parameters.Add(par);

				par = new OleDbParameter("@code", OleDbType.VarChar, 18);
				par.Value = code;
				parameters.Add(par);

				par = new OleDbParameter("@table", OleDbType.VarChar, 32);
				par.Value = table;
				parameters.Add(par);

				// Lancia la stored procedure
				if (!mDbInterface.Execute("sp_check_existing_code", ref parameters))
					return false;

				// Codice esiste se valore ritornato > 0
				par = (OleDbParameter) parameters[0];
				exist =((int) par.Value) > 0;
				
				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("DbCollection.CheckExistingCode Error", ex);
				return false;
			}
		}

		#endregion

		#region Private Methods

		//**********************************************************************
		// IsIndexOk
		// Verifica se l'indice passato � all'interno del vettore di oggetti
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		protected bool IsIndexOk(int index)
		{
			return (index >= 0 && index < mRecordset.Count);
		}

		//**********************************************************************
		// TableIdSearch
		// Cerca l'indice dell'oggetto avente l'id tabella indicato
		// Ritorna:
		//			>=0		indice dell'oggetto trovato
		//			-1		indice non trovato
		//**********************************************************************
		protected int TableIdSearch(int tableId)
		{
			//Cerca l'id dell'oggetto
			for (int i=0; i < mRecordset.Count; i++)
			{
				DbObject obj = (DbObject) mRecordset[i];
				if (obj.gTableId == tableId)
					return i;
			}
			//oggetto non trovato
			return -1;
		}
		#endregion
	}
}
