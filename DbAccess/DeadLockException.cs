using System;

namespace DbAccess
{
	/// <summary>
	/// Classe che implementa l'eccezione di deadlock.
	/// Viene sollevata in genere dalla DbInterface nel caso in cui avvenga un deadlock con una transazione aperta,
	/// per cui la funzione chiamante deve intercettare questa eccezione e ripetere tutte le operazioni eseguite
	/// dentro la transazione.
	/// </summary>
	public class DeadLockException: ApplicationException
	{
		public DeadLockException()
		{
		}
		public DeadLockException(string message): base(message)
		{
		}
		public DeadLockException(string message, Exception inner): base(message, inner)
		{
		}
	}
}
