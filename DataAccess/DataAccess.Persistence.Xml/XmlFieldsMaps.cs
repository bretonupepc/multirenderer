﻿using System;
using System.Collections.Generic;
using DataAccess.Business.BusinessBase;

namespace DataAccess.Persistence.Xml
{
    //[Obsolete]
    //public class XmlFieldsMaps
    //{
    //    private static Dictionary<Type, Dictionary<string, string>> _entitiesFieldMaps = null;


    //    private static Dictionary<Type, Dictionary<string, string>> EntitiesFieldMaps
    //    {
    //        get
    //        {
    //            if (_entitiesFieldMaps == null)
    //            {
    //                _entitiesFieldMaps = new Dictionary<Type, Dictionary<string, string>>();
    //            }
    //            return _entitiesFieldMaps;
    //        }
    //    }

    //    private static object _staticLockObj = new object();

    //    public static Dictionary<string, string> GetEntityFieldsMap(Type entityType)
    //    {
    //        lock (_staticLockObj)
    //        {
    //            if (!EntitiesFieldMaps.ContainsKey(entityType))
    //            {
    //                AddEntityFieldMap(entityType);
    //            }

    //        }

    //        return (EntitiesFieldMaps.ContainsKey(entityType)
    //            ? EntitiesFieldMaps[entityType]
    //            : null);
    //    }

    //    private static void AddEntityFieldMap(Type entityType)
    //    {

    //        var entityFieldMap = new Dictionary<string, string>();
    //        var requestedDict = new Dictionary<string, string>();

    //        List<FieldItemBaseInfo> fieldsInfo = PersistableEntityBaseInfos.GetBaseInfos(entityType);

    //        if (fieldsInfo == null)
    //            return;

    //        switch (entityType.Name)
    //        {
    //            #region MATERIAL

    //            case "MaterialEntity":
    //                requestedDict = new Dictionary<string, string>()
    //                {
    //                    {"Id", "F_ID"},
    //                    {"MaterialClassId", "F_ID_T_LS_MATERIAL_CLASSES"},
    //                    {"StationId", "F_ID_T_AN_STATIONS"},
    //                    {"ImageId", "F_ID_T_AN_MATERIAL_IMAGES"},
    //                    {"SupplierId", "F_ID_T_AN_SUPPLIERS"},
    //                    {"StatusId", "F_ID_T_CO_MATERIAL_STATES"},
    //                    {"Code", "F_CODE"},
    //                    {"Description", "F_DESCRIPTION"},
    //                    {"SpecificGravity", "F_SPECIFIC_GRAVITY"},
    //                    {"FinishingSurface", "F_FINISHING_SURFACE"},
    //                    {"CostM2", "F_COST_M2"},
    //                    {"CostM3", "F_COST_M3"},
    //                    {"LinearCost", "F_LINEAR_COST"},
    //                    {"SlabCost", "F_SLAB_COST"},
    //                    {"Origin", "F_ORIGIN"},
    //                    {"BlockSize", "F_BLOCK_SIZE"},
    //                    {"Color", "F_COLOR"},
    //                    {"Availability", "F_AVAILABILITY"},
    //                    {"PrevailingUse", "F_PREVAILING_USE"},
    //                    {"Polish", "F_POLISH"},
    //                    {"Comments", "F_COMMENTS"},
    //                    {"Tonality", "F_TONALITY"},
    //                    {"Scale", "F_SCALE"},
    //                    {"InsertDate", "F_INSERT_DATE"},
    //                };

    //                break; 
    //            #endregion

    //            #region SLAB FACE
    //            case "SlabFaceEntity":
    //                requestedDict = new Dictionary<string, string>()
    //                {
    //                    {"Id", "F_ID"},
    //                    {"SlabId", "F_ID_T_AN_ARTICLES"},
    //                    {"FaceId", "F_FACE"},
    //                    {"Used", "F_USED"},
    //                    {"DimX", "F_DIM_X"},
    //                    {"DimY", "F_DIM_Y"},
    //                    {"DimZ", "F_DIM_Z"},
    //                    {"OffsetX", "F_OFFSET_X"},
    //                    {"OffsetY", "F_OFFSET_Y"},
    //                    {"Surface", "F_SURFACE"},
    //                    {"CommercialSurface", "F_COMMERCIAL_SURFACE"},
    //                    {"InternalRectLeft", "F_INTERNAL_RECT_LEFT"},
    //                    {"InternalRectTop", "F_INTERNAL_RECT_TOP"},
    //                    {"InternalRectWidth", "F_INTERNAL_RECT_WIDTH"},
    //                    {"InternalRectHeight", "F_INTERNAL_RECT_HEIGHT"},
    //                    {"Tone", "F_TONE"},
    //                    {"XmlParameter", "F_XML_PARAMETER"},
    //                    {"PositionStopX", "F_POSITION_STOP_X"},
    //                    {"PositionStopY", "F_POSITION_STOP_Y"},
    //                    {"UnitCost", "F_UNIT_COST"},
    //                    {"Defects", "F_DEFECTS"},
    //                    {"ImageId", "F_ID_T_AN_ARTICLE_IMAGES"},
    //                    {"QualityId", "F_ID_T_LS_ARTICLE_QUALITIES"}
    //                };

    //                break;
    //            #endregion

    //            #region ARTICLE
    //            case "ArticleEntity":
    //                requestedDict = new Dictionary<string, string>()
    //                {
    //                    {"Id", "F_ID"},
    //                    {"DimX", "F_DIM_X"},
    //                    {"DimY", "F_DIM_Y"},
    //                    {"DimZ", "F_DIM_Z"},
    //                    {"OffsetX", "F_OFFSET_X"},
    //                    {"OffsetY", "F_OFFSET_Y"},
    //                    {"Surface", "F_SURFACE"},
    //                    {"CommercialSurface", "F_COMMERCIAL_SURFACE"},
    //                    {"InternalRectLeft", "F_INTERNAL_RECT_LEFT"},
    //                    {"InternalRectTop", "F_INTERNAL_RECT_TOP"},
    //                    {"InternalRectWidth", "F_INTERNAL_RECT_WIDTH"},
    //                    {"InternalRectHeight", "F_INTERNAL_RECT_HEIGHT"},
    //                    {"Tone", "F_TONE"},
    //                    {"XmlParameter", "F_XML_PARAMETER"},
    //                    {"PositionStopX", "F_POSITION_STOP_X"},
    //                    {"PositionStopY", "F_POSITION_STOP_Y"},
    //                    {"UnitCost", "F_UNIT_COST"},
    //                    {"Defects", "F_DEFECTS"},
    //                    {"ImageId", "F_ID_T_AN_ARTICLE_IMAGES"},
    //                    {"QualityId", "F_ID_T_LS_ARTICLE_QUALITIES"},

    //                    {"Code", "F_CODE"},
    //                    {"Description", "F_DESCRIPTION"},
    //                    {"BatchCode", "F_BATCH_CODE"},
    //                    {"Qty", "F_QTY"},

    //                    {"ArticleType", "F_ID_T_CO_ARTICLE_TYPES"},
    //                    {"ArticleStatus", "F_ID_T_CO_ARTICLE_STATES"},
    //                    {"MaterialId", "F_ID_T_AN_MATERIALS"},
    //                    {"StockHusId", "F_ID_T_AN_STOCK_HUS"},
    //                    {"DdtaDetailsId", "F_ID_T_AN_DDTA_DETAILS"},
    //                    {"SupplierId", "F_ID_T_AN_SUPPLIERS"},
    //                    {"CustomerProjectId", "F_ID_T_AN_SUPPLIERS"},
    //                    {"InsertDate", "F_INSERT_DATE"},
    //                    {"HusPosition", "F_HUS_POSITION"},
    //                    {"BlobsPartProgramId", "F_ID_T_AN_BLOBS_PART_PROGRAM"},
    //                    {"BlobsPolygonId", "F_ID_T_AN_BLOBS_POLYGON"},
    //                    {"Weight", "F_WEIGHT"}


    //                };

    //                break;
    //            #endregion

    //            default:
    //                break;
    //        }


    //        foreach (var kvp in requestedDict)
    //        {
    //            if (fieldsInfo.Exists(i => i.PropertyName == kvp.Key))
    //            {
    //                entityFieldMap.Add(kvp.Key, kvp.Value);
    //            }
    //        }


    //        EntitiesFieldMaps.Add(entityType, entityFieldMap);

    //    }

    //}
}
