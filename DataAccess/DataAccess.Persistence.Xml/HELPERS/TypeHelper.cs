﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Persistence.Xml.HELPERS
{
    /// <summary>
    /// Helper per Tipi oggetto
    /// </summary>
    internal class TypeHelper
    {
        /// <summary>
        /// Restituisce il Type analizzando il nome completo del tipo oggetto
        /// </summary>
        /// <param name="fullName">Nome completo del Tipo oggetto</param>
        /// <returns>Type corrispondente; null se non riconosciuto</returns>
        public static Type GetType(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
                return null;
            Type type = Type.GetType(fullName);
            if (type == null)
            {
                string targetAssembly = fullName;
                while (type == null && targetAssembly.Length > 0)
                {
                    try
                    {
                        int dotInd = targetAssembly.LastIndexOf('.');
                        targetAssembly = dotInd >= 0 ? targetAssembly.Substring(0, dotInd) : "";
                        if (targetAssembly.Length > 0)
                            type = Type.GetType(fullName + ", " + targetAssembly);
                    }
                    catch { }
                }
            }
            return type;
        }
    }
}
