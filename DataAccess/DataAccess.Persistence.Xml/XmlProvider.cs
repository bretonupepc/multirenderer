﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Xml.Linq;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.COMMON;
using DataAccess.Business.Persistence;

namespace DataAccess.Persistence.Xml
{
    /// <summary>
    /// Classe di implementazione provider Xml
    /// </summary>
    [PersistenceProvider(PersistenceProviderType.Xml)]
    public class XmlProvider : IPersistenceProvider
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _mainFolder = string.Empty;

        private string _currentUserName = string.Empty;

        private PersistenceProviderType _providerType = PersistenceProviderType.Xml;

        #region Workers

        private readonly SynchronizationContext _synchroContext;

        private bool _canStartWorkers = true;

        private SafeCollection<Thread> _workers = new SafeCollection<Thread>();

        #endregion Workers


        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore base
        /// </summary>
        public XmlProvider()
        {
            _synchroContext = SynchronizationContext.Current;

        }

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="mainFolder">Directory di lavoro</param>
        public XmlProvider(string mainFolder): this()
        {
            MainFolder = mainFolder;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties


        /// <summary>
        /// Nome utente corrente
        /// </summary>
        public string CurrentUserName
        {
            get { return _currentUserName; }
            set { _currentUserName = value; }
        }

        /// <summary>
        /// COntesto di sincronizzazione
        /// </summary>
        public SynchronizationContext SynchroContext
        {
            get { return _synchroContext; }
        }

        /// <summary>
        /// Directory di lavoro
        /// </summary>
        public string MainFolder
        {
            get { return _mainFolder; }
            set { _mainFolder = value; }
        }


        public bool CanStartWorkers
        {
            get { return _canStartWorkers; }
            set { _canStartWorkers = value; }
        }

        public SafeCollection<Thread> Workers
        {
            get { return _workers; }
            set { _workers = value; }
        }

        public PersistenceProviderType ProviderType
        {
            get { return _providerType; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Salva la gerarchia di un nodo xml su un file
        /// </summary>
        /// <param name="filepath">Percorso del file</param>
        /// <param name="rootNode">Nodo da salvare</param>
        /// <returns>True se operazione eseguita con successo</returns>
        public bool WriteXmlFile(string filepath, XElement rootNode)
        {
            bool retVal = false;

            return retVal;
        }

        /// <summary>
        /// Carica il contenuto di un file xml su un nodo xml
        /// </summary>
        /// <param name="filepath">Percorso file</param>
        /// <param name="rootNode">Nodo xml</param>
        /// <returns>True se operazione eseguita con successo</returns>
        public bool ReadXmlFile(string filepath, out XElement rootNode)
        {
            bool retVal = false;
            rootNode = null;

            return retVal;
        }

        public void StopWorkers()
        {
            _canStartWorkers = false;

            while (_workers.Count > 0)
            {
                for (int i = _workers.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        Thread thread = _workers[i];
                        if (thread != null && thread.IsAlive)
                        {
                            thread.Abort();
                        }
                        else
                        {
                            _workers.RemoveAt(i);
                        }

                    }
                    catch { }

                }
                Thread.Sleep(10);
            }
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private static object IsDbNull(object value, object nullValueReplacement)
        {
            if (value == DBNull.Value)
                return nullValueReplacement;

            return value;
        }

        private static string NotNullString(object dataValue)
        {
            if (dataValue.Equals(DBNull.Value))
            {
                return string.Empty;
            }
            else
            {
                return dataValue.ToString();
            }
        }

        //public static string DateSqlFormat(DateTime date)
        //{
        //    return date.ToString("yyyyMMdd HH:mm:ss");
        //}

        //public static string FixSqlString(string inputStr)
        //{
        //    return inputStr.Replace("\'", "\'\'");
        //}

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        //private 



        #region IPersistenceProvider Members

        //public PersistableCollectionProxy<T> GetCollectionProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity
        //{
        //    XmlCollectionProxy<T> proxy = null;
        //    switch (typeof(T).Name)
        //    {
        //        case "MaterialEntity":
        //            //proxy = new SqlMaterialCollectionProxy(collection as MaterialCollection, DataAccessInterface.Clone()) as SqlCollectionProxy<T>;
        //            break;

        //        case "ArticleEntity":
        //            proxy = new XmlArticleCollectionProxy(collection as ArticleCollection) as XmlCollectionProxy<T>;
        //            break;

        //        case "SlabEntity":
        //            proxy = new XmlSlabCollectionProxy(collection as SlabCollection) as XmlCollectionProxy<T>;
        //            break;

        //        case "SlabFaceEntity":
        //            proxy = new XmlSlabFaceCollectionProxy(collection as SlabFaceCollection) as XmlCollectionProxy<T>;
        //            break;

        //        default:
        //            proxy = new XmlCollectionProxy<T>(collection);
        //            break;
        //    }
        //    return proxy;

        //}
        
        public PersistableCollectionProxy<T> GetDefaultCollectionProxy<T>(PersistableEntityCollection<T> collection, bool saveMode = false) where T : BasePersistableEntity
        {
            XmlCollectionProxy<T> proxy = new XmlCollectionProxy<T>(collection);

            return proxy;

        }

        //public PersistableEntityProxy GetEntityProxy<T>(T entity) where T : BasePersistableEntity
        //{
        //    PersistableEntityProxy proxy = null;
        //    switch (entity.GetType().Name)
        //    {
        //        case "MaterialEntity":
        //            //return new SqlMaterialProxy(entity as MaterialEntity, this);
        //            break;

        //        case "MaterialClassEntity":

        //            break;

        //        case "ArticleEntity":
        //            return new XmlArticleProxy(entity as ArticleEntity, this);
        //            break;

        //        case "SlabEntity":
        //            return new XmlSlabProxy(entity as SlabEntity, this);
        //            break;

        //        case "SlabFaceEntity":
        //            return new XmlSlabFaceProxy(entity as SlabFaceEntity, this);
        //            break;


        //        default:
        //            return new XmlEntityProxy(entity, this);
        //            break;
        //    }

        //    return proxy;
        //}
        
        public PersistableEntityProxy GetDefaultEntityProxy<T>(T entity, bool saveMode = false) where T : BasePersistableEntity
        {
            PersistableEntityProxy proxy = new XmlEntityProxy(entity, this);

            return proxy;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion



        public bool BeginTransaction()
        {
            return true;
        }

        public bool EndTransaction(bool commit)
        {
            return true;
        }


        public bool SaveMode
        {
            get { return false; }
        }


        public string GetMainDbTableName(Type type)
        {
            return string.Empty;
        }

        public Dictionary<string, DbField> GetEntityFieldsMap(Type type)
        {
            return new Dictionary<string, DbField>();
        }
    }

}
