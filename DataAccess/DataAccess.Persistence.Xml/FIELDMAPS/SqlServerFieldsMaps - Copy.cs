﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BrSm.Business.BusinessBase;
using BrSm.Business.Persistence;

namespace BrSm.Persistence.SqlServer.FIELDMAPS
{
    public class SqlServerFieldsMaps_ORI
    {
        private static Dictionary<Type, Dictionary<string, DbField>> _entitiesFieldMaps = null;


        private static Dictionary<Type, Dictionary<string, DbField>> EntitiesFieldMaps
        {
            get
            {
                if (_entitiesFieldMaps == null)
                {
                    _entitiesFieldMaps = new Dictionary<Type, Dictionary<string, DbField>>();
                }
                return _entitiesFieldMaps;
            }
        }

        private static object _staticLockObj = new object();

        public static Dictionary<string, DbField> GetEntityFieldsMap(Type entityType)
        {
            lock (_staticLockObj)
            {
                if (!EntitiesFieldMaps.ContainsKey(entityType))
                {
                    AddEntityFieldMap(entityType);
                }

            }

            return (EntitiesFieldMaps.ContainsKey(entityType)
                ? EntitiesFieldMaps[entityType]
                : null);
        }

        private static void AddEntityFieldMap(Type entityType)
        {

            var entityFieldMap = new Dictionary<string, DbField>();
            var requestedDict = new Dictionary<string, DbField>();

            List<FieldItemBaseInfo> fieldsInfo = PersistableEntityBaseInfos.GetBaseInfos(entityType);

            if (fieldsInfo == null)
                return;

            requestedDict = GetFieldMapDictionary(entityType);


            if (requestedDict.Count == 0)
            switch (entityType.Name)
            {
                #region MATERIAL

                case "MaterialEntity":
                    requestedDict = FIELDMAPS.FieldMapDictionaries.MaterialEntityMap;
                    break; 
                #endregion

                #region SLAB FACE
                case "SlabFaceEntity":
                    requestedDict = FIELDMAPS.FieldMapDictionaries.SlabFaceEntityMap;

                    break;
                #endregion

                #region ARTICLE
                case "ArticleEntity":
                    requestedDict = FIELDMAPS.FieldMapDictionaries.ArticleEntityMap;

                    break;
                #endregion

                case "AreaAttribute":
                    requestedDict = FIELDMAPS.FieldMapDictionaries.AreaAttributeMap;

                    break;

                case "QualityClass":
                    requestedDict = FIELDMAPS.FieldMapDictionaries.QualityClassMap;

                    break;

                case "QualityClassAreaAttribute":
                    requestedDict = FIELDMAPS.FieldMapDictionaries.QualityClassAreaAttributeMap;

                    break;

                default:
                    break;
            }


            foreach (var kvp in requestedDict)
            {
                if (fieldsInfo.Exists(i => i.PropertyName == kvp.Key))
                {
                    entityFieldMap.Add(kvp.Key, kvp.Value);
                }
            }


            EntitiesFieldMaps.Add(entityType, entityFieldMap);

        }

        private static Dictionary<string, DbField> GetFieldMapDictionary(Type entityType)
        {
            var props = entityType.GetProperties().Where(prop => Attribute.IsDefined((MemberInfo) prop, typeof(DbFieldAttribute)));
            var dict = new Dictionary<string, DbField>();

            foreach (var prop in props)
            {
                var attribute =
                    prop.GetCustomAttributes(typeof (DbFieldAttribute), true)
                        .FirstOrDefault(a => ((DbFieldAttribute) a).ProviderType == PersistenceProviderType.SqlServer);
                if (attribute != null)
                {
                    dict.Add(prop.Name, ((DbFieldAttribute)attribute).Field);
                }
            }

            return dict;
        } 

    }
}
