﻿using System.Linq;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace DataAccess.Business.DATASOURCES
{
    /// <summary>
    /// Collezione tipizzata per caching dati da database
    /// </summary>
    /// <typeparam name="T">Tipizzazione</typeparam>
    public class BaseDataSource<T>:PersistableEntityCollection<T> where T:BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Estrae tutti gli elementi dal repository
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <param name="sortSet">Criterio di ordinamento</param>
        /// <param name="suppressNotifications"></param>
        public void GetAllItemsFromRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer, FieldsSortSet sortSet = null, bool suppressNotifications = false)
        {
            GetItemsFromRepository(providerType, sortSet:sortSet, suppressNotifications:suppressNotifications);
        }

        /// <summary>
        /// Aggiunge o aggiorna un elemento per codice
        /// </summary>
        /// <param name="code">Codice</param>
        /// <param name="providerType">Tipo provider</param>
        public void AddOrRefreshByCode(string code, PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            
        }

        /// <summary>
        /// Aggiunge o aggiorna un elemento per Id
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="providerType">Tipo provider</param>
        public void AddOrRefreshById(int id, PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            T item;
            var itemList = new PersistableEntityCollection<T>(Scope);

            var filter = new FilterClause(new FieldItemBaseInfo("Id", typeof(int)), ConditionOperator.Eq, id);
            if (itemList.GetItemsFromRepository(providerType, null, filter))
            {
                if (itemList.Count > 0)
                {
                    item = itemList[0];
                    var current = this.FirstOrDefault(i => i.UniqueId == item.UniqueId);
                    if (current != null)
                    {
                        var currentIndex = this.IndexOf(current);
                        this.RemoveAt(currentIndex, false);
                        Insert(currentIndex,item);
                    }
                    else
                    {
                        Add(item);
                    }
                }
            }

        }

        /// <summary>
        /// Rimuove un elemento per codice
        /// </summary>
        /// <param name="code">Codice</param>
        public void RemoveByCode(string code)
        {

        }

        /// <summary>
        /// Rimuove un elemento per Id
        /// </summary>
        /// <param name="id">Id</param>
        public void RemoveById(int id)
        {
            var current = this.FirstOrDefault(i => i.UniqueId == id);
            if (current != null)
            {
                Remove(current);
            }
        }

        /// <summary>
        /// Ottiene un elemento per Id
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>Elemento tipizzato</returns>
        public T GetById(int id, PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            T item = default(T);

            item = this.Items.FirstOrDefault(i => i.UniqueId == id);

            if (item == null) 
            {
                var itemList = new PersistableEntityCollection<T>(Scope);

                var filter = new FilterClause(new FieldItemBaseInfo("Id", typeof(int)), ConditionOperator.Eq, id);
                if (itemList.GetItemsFromRepository(providerType, null, filter))
                {
                    if (itemList.Count > 0)
                    {
                        item = itemList[0];
                        Add(item);
                    }
                }
            }

            return item;
        }

        /// <summary>
        /// Ottiene un elemento per codice
        /// </summary>
        /// <param name="code">Codice</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>Elemento tipizzato</returns>
        public T GetByCode(string code, PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            T item = default(T);

            item = this.Items.FirstOrDefault(i => i.UniqueCode == code);

            if (item == null)
            {
                var itemList = new PersistableEntityCollection<T>(Scope);

                var filter = new FilterClause(new FieldItemBaseInfo("Code", typeof(string)), ConditionOperator.Eq, code);
                if (itemList.GetItemsFromRepository(providerType, null, filter))
                {
                    if (itemList.Count > 0)
                    {
                        item = itemList[0];
                        Add(item);
                    }
                }
            }

            return item;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
