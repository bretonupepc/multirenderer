﻿using System;
using System.Collections.Generic;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace DataAccess.Business.DATASOURCES
{
    /// <summary>
    /// Dizionari per caching dati da database
    /// </summary>
    public class DataSources
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        protected PersistenceScope _scope;

        private readonly Dictionary<string, object> Dict = new Dictionary<string, object>();

        protected readonly Dictionary<PersistenceProviderType, Dictionary<string, object>> Dictionaries = new Dictionary<PersistenceProviderType, Dictionary<string, object>>();

        public PersistenceScope Scope
        {
            get { return _scope; }
        }

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public DataSources(PersistenceScope scope)
        {
            _scope = scope;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        //public static BaseDataSource<Customer> Customers
        //{
        //    get { return GetDataSource<Customer>(typeof (Customer).ToString()); }
        //}

        //public static BaseDataSource<MaterialEntity> Materials
        //{
        //    get { return GetDataSource<MaterialEntity>(typeof(MaterialEntity).ToString()); }
        //} 

        //public static BaseDataSource<MaterialClassEntity> MaterialClasses
        //{
        //    get { return GetDataSource<MaterialClassEntity>(typeof(MaterialClassEntity).ToString()); }
        //} 

        //public static BaseDataSource<QualityClass> QualityClasses
        //{
        //    get { return GetDataSource<QualityClass>(typeof(QualityClass).ToString()); }
        //} 

        //public static BaseDataSource<AreaAttribute> AreaAttributes
        //{
        //    get { return GetDataSource<AreaAttribute>(typeof(AreaAttribute).ToString()); }
        //} 

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Clear all internal dictionaries
        /// </summary>
        public void ClearDictionaries()
        {
            Dictionaries.Clear();
        }

        /// <summary>
        /// Recupera un datasource per tipo oggetto e tipo provider
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="typeName">Nome del tipo oggetto</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>Datasource tipizzato</returns>
        public BaseDataSource<T> GetDataSource<T>(string typeName, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T:BasePersistableEntity
        {
            BaseDataSource<T> dictionary;

            if (!Dictionaries.ContainsKey(providerType))
            {
                Dictionaries.Add(providerType, new Dictionary<string, object>());
            }
            var dict = Dictionaries[providerType];

            if (dict.ContainsKey(typeName))
            {
                dictionary = dict[typeName] as BaseDataSource<T>;
            }
            else
            {
                dictionary = new BaseDataSource<T>();
                dict.Add(typeName, dictionary);
            }

            if (dictionary != null)
            {
                dictionary.Scope = Scope;
            }

            return dictionary;
        }

        /// <summary>
        /// Recupera un oggetto per Id
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="id">Id oggetto</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>Oggetto tipizzato</returns>
        public T GetById<T>(int id, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T:BasePersistableEntity
        {
            BaseDataSource<T> dictionary;

            string typeName = typeof (T).ToString();

            if (!Dictionaries.ContainsKey(providerType))
            {
                Dictionaries.Add(providerType, new Dictionary<string, object>());
            }

            var dict = Dictionaries[providerType];


            if (dict.ContainsKey(typeName))
            {
                dictionary = dict[typeName] as BaseDataSource<T>;
            }
            else
            {
                dictionary = new BaseDataSource<T>();
                dict.Add(typeName, dictionary);
            }

            if (dictionary != null)
                dictionary.Scope = Scope;

            return dictionary.GetById(id);
        }

        /// <summary>
        /// Recupera un oggetto per Code
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="code">Id oggetto</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>Oggetto tipizzato</returns>
        public T GetByCode<T>(string code, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T : BasePersistableEntity
        {
            BaseDataSource<T> dictionary;

            string typeName = typeof(T).ToString();

            if (!Dictionaries.ContainsKey(providerType))
            {
                Dictionaries.Add(providerType, new Dictionary<string, object>());
            }

            var dict = Dictionaries[providerType];


            if (dict.ContainsKey(typeName))
            {
                dictionary = dict[typeName] as BaseDataSource<T>;
            }
            else
            {
                dictionary = new BaseDataSource<T>();
                dict.Add(typeName, dictionary);
            }

            if (dictionary != null)
                dictionary.Scope = Scope;


            return dictionary.GetByCode(code);
        }
        //TODO
        //public static T GetById<T>(int id, Type type, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T : BasePersistableEntity
        //{
        //    BaseDataSource<T> dictionary;

        //    string typeName = type.Name;

        //    if (!Dictionaries.ContainsKey(providerType))
        //    {
        //        Dictionaries.Add(providerType, new Dictionary<string, object>());
        //    }

        //    var dict = Dictionaries[providerType];


        //    if (dict.ContainsKey(typeName))
        //    {
        //        dictionary = dict[typeName] as BaseDataSource<T>;
        //    }
        //    else
        //    {
        //        dictionary = new BaseDataSource<T>();
        //        dict.Add(typeName, dictionary);
        //    }

        //    return dictionary.GetById(id);
        //}


        /// <summary>
        /// Recupera un oggetto per Id
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="id">Id oggetto</param>
        /// <param name="type">Tipo oggetto</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>Oggetto tipizzato</returns>
        public T GetById<T>(int id, T type, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T : BasePersistableEntity
        {
            return GetById<T>(id,providerType);
        }

        /// <summary>
        /// Recupera un oggetto per Code
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="code">Id oggetto</param>
        /// <param name="type">Tipo oggetto</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>Oggetto tipizzato</returns>
        public T GetByCode<T>(string code, T type, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T : BasePersistableEntity
        {
            return GetByCode<T>(code, providerType);
        }


        ///// <summary>
        ///// Recupera un oggetto per Id
        ///// </summary>
        ///// <typeparam name="T">Tipizzazione</typeparam>
        ///// <param name="id">Id oggetto</param>
        ///// <param name="providerType">Tipo provider</param>
        ///// <returns>Oggetto tipizzato</returns>
        //public static T GetById<T>(int x, int id, Type type, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T : BasePersistableEntity
        //{
        //    return GetById<T>(id, providerType);
        //}

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
