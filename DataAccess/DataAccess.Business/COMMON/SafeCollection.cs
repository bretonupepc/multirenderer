﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Data;
using System.Windows.Threading;

namespace DataAccess.Business.COMMON
{
    /// <summary>
    /// Collezione tipizzata sincronizzata
    /// </summary>
    /// <typeparam name="T">Tipizzazione</typeparam>
    public class SafeCollection<T> : ObservableCollection<T>
    {
        private readonly object _lockObject = new object();
        private readonly object _syncLock = new object();

        protected readonly ObservableCollection<T> _removedItems = new ObservableCollection<T>();

        protected bool _suppressNotification = false;

        /// <summary>
        /// Aggiunge un blocco di elementi
        /// </summary>
        /// <param name="items"></param>
        /// <param name="suppressNotifications"></param>
        public void AddRange(IEnumerable<T> items, bool suppressNotifications = true)
        {
            if (items == null)
                throw new ArgumentNullException("items");

            _suppressNotification = suppressNotifications;

            lock (_lockObject)
            {
                foreach (T item in items)
                {
                    base.Add(item);
                }
            }

            _suppressNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }


        /// <summary>
        /// Override evento CollectionChanged
        /// </summary>
        public override event NotifyCollectionChangedEventHandler CollectionChanged;
 
        /// <summary>
        /// Costruttore da IEnumerable
        /// </summary>
        /// <param name="collection">Oggetto IEnumerable originale</param>
        public SafeCollection(IEnumerable<T> collection) : base(collection) { }

        /// <summary>
        /// Costruttore da List
        /// </summary>
        /// <param name="collection">Oggetto List originale</param>
        public SafeCollection(List<T> collection) : base(collection) { }

        /// <summary>
        /// Costruttore base
        /// </summary>
        public SafeCollection():base()
        {
            EnableCollectionSynchronization(this, _syncLock);
        }

        //protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        //{
        //    lock (_lockObject)
        //    {
        //        base.OnCollectionChanged(e);
        //    }

        //    //// Be nice - use BlockReentrancy like MSDN said
        //    //using (BlockReentrancy())
        //    //{
        //    //    var eventHandler = CollectionChanged;
        //    //    if (eventHandler != null)
        //    //    {
        //    //        Delegate[] delegates = eventHandler.GetInvocationList();
        //    //        // Walk thru invocation list
        //    //        foreach (NotifyCollectionChangedEventHandler handler in delegates)
        //    //        {
        //    //            var dispatcherObject = handler.Target as DispatcherObject;
        //    //            // If the subscriber is a DispatcherObject and different thread
        //    //            if (dispatcherObject != null && dispatcherObject.CheckAccess() == false)
        //    //                // Invoke handler in the target dispatcher's thread
        //    //                dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind,
        //    //                            handler, this, e);
        //    //            else // Execute handler as is
        //    //                handler(this, e);
        //    //        }
        //    //    }
        //    //}
        //}

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (_suppressNotification) return;

            using (BlockReentrancy())
            {
                var eh = CollectionChanged;
                if (eh == null) return;

                var dispatcher = (from NotifyCollectionChangedEventHandler nh in eh.GetInvocationList()
                                  let dpo = nh.Target as DispatcherObject
                                  where dpo != null
                                  select dpo.Dispatcher).FirstOrDefault();

                if (dispatcher != null && dispatcher.CheckAccess() == false)
                {
                    dispatcher.Invoke(DispatcherPriority.DataBind, (Action)(() => OnCollectionChanged(e)));
                }
                else
                {
                    foreach (var nh in eh.GetInvocationList())
                    {
                        var nch = nh as NotifyCollectionChangedEventHandler;
                        if (nch != null)
                            nch.Invoke(this, e);
                    }
                }
            }
        }

        /// <summary>
        /// Indexer
        /// </summary>
        /// <param name="index">Posizione</param>
        /// <returns>Elemento alla posizione specificata</returns>
        public new T this[int index]
        {
            get
            {
                lock (_lockObject)
                {
                    return base[index];
                }
            }
            set
            {
                lock (_lockObject)
                {
                    base[index] = value;
                }

            }
        }

        /// <summary>
        /// Aggiunge un elemento
        /// </summary>
        /// <param name="item">Elemento da aggiungere</param>
        //public new void Add(T item)
        //{
        //    lock (_lockObject)
        //    {
        //        base.Add(item);
        //    }
        //}

        protected override void InsertItem(int index, T item)
        {
            ////lock (_lockObject)
            {
                base.InsertItem(index, item);
            }
        }

        /// <summary>
        /// Rimuove un elemento
        /// </summary>
        /// <param name="item">Elemento da rimuovere</param>
        public new void Remove(T item)
        {
            ////lock (_lockObject)
            {
                _removedItems.Add(item);
                base.Remove(item);
            }
        }

        /// <summary>
        /// Rimuove un elemento alla posizione specificata
        /// </summary>
        /// <param name="index">Posizione</param>
        public new void RemoveAt(int index, bool saveInRemovedItems = true)
        {
            ////lock (_lockObject)
            {
                if (saveInRemovedItems)
                    _removedItems.Add(this[index]);
                base.RemoveAt(index);
            }

        }

        protected override void RemoveItem(int index)
        {
            lock (_lockObject)
            {
                _removedItems.Add(this[index]);
                base.RemoveItem(index);
            }
        }

        protected override void ClearItems()
        {
            lock (_lockObject)
            {
                _removedItems.Clear();
                base.ClearItems();
            }
        }

        /// <summary>
        /// Svuota la collezione
        /// </summary>
        public new void Clear()
        {
            ClearItems();

        }

        private static void EnableCollectionSynchronization(IEnumerable collection, object lockObject)
        {
            var method = typeof(BindingOperations).GetMethod("EnableCollectionSynchronization",
                                    new Type[] { typeof(IEnumerable), typeof(object) });
            if (method != null)
            {
                // It's .NET 4.5
                method.Invoke(null, new object[] { collection, lockObject });
            }
        }

    }
}
