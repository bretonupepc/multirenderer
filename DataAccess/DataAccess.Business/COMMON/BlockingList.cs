﻿using System.Collections.Generic;

namespace DataAccess.Business.COMMON
{
    /// <summary>
    /// Lista tipizzata protetta
    /// </summary>
    /// <typeparam name="T">Tipizzazione</typeparam>
    public class BlockingList<T> : List<T>
    {
        private readonly object _lockObject = new object();

        /// <summary>
        /// Indexer
        /// </summary>
        /// <param name="index">Posizione</param>
        /// <returns>Elemento alla posizione specificata</returns>
        public new T this[int index]
        {
            get
            {
                lock (_lockObject)
                {
                    return base[index];
                }
            }
            set
            {
                lock (_lockObject)
                {
                    base[index] = value;
                }

            }
        }

        /// <summary>
        /// Aggiunge un elemento
        /// </summary>
        /// <param name="item">Elemento da aggiungere</param>
        public new void Add(T item)
        {
            lock (_lockObject)
            {
                if (!Contains(item))
                    base.Add(item);
            }
        }

        /// <summary>
        /// Rimuove un elemento
        /// </summary>
        /// <param name="item">Elemento da rimuovere</param>
        public new void Remove(T item)
        {
            lock (_lockObject)
            {
                if (Contains(item))
                    base.Remove(item);
            }
        }

        /// <summary>
        /// Rimuove un elemento alla posizione specificata
        /// </summary>
        /// <param name="index">Posizione</param>
        public new void RemoveAt(int index)
        {
            lock (_lockObject)
            {
                if (Count > index)
                    RemoveAt(index);
            }

        }

    }
}
