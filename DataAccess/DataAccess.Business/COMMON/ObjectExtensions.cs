﻿using System;
using System.Collections.Generic;
using System.Reflection;


namespace DataAccess.Business.COMMON
{
    /// <summary>
    /// Metodi di estensione per Objects
    /// </summary>
    public static class ObjectExtensions
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private static readonly MethodInfo CloneMethod = typeof(Object).GetMethod("MemberwiseClone", BindingFlags.NonPublic | BindingFlags.Instance);

        private static Object InternalCopy(Object originalObject, IDictionary<Object, Object> visited, bool deepCopy)
        {
            if (originalObject == null) return null;

            var typeToReflect = originalObject.GetType();

            if (IsPrimitive(typeToReflect)) return originalObject;

            if (visited.ContainsKey(originalObject)) return visited[originalObject];

            if (typeof(Delegate).IsAssignableFrom(typeToReflect)) return null;
            var cloneObject = CloneMethod.Invoke(originalObject, null);

            if (typeToReflect.IsArray)
            {
                var arrayType = typeToReflect.GetElementType();
                if ((IsPrimitive(arrayType) == false) && deepCopy)
                {
                    Array clonedArray = (Array)cloneObject;
                    clonedArray.ForEach((array, indices) => array.SetValue(InternalCopy(clonedArray.GetValue(indices), visited, true), indices));
                }

            }

            visited.Add(originalObject, cloneObject);

            if (deepCopy)
            {
                CopyFields(originalObject, visited, cloneObject, typeToReflect, deepCopy:deepCopy);
                RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect, deepCopy);
            }

            return cloneObject;
        }

        private static void RecursiveCopyBaseTypePrivateFields(object originalObject, IDictionary<object, object> visited, object cloneObject, Type typeToReflect, bool deepCopy)
        {
            if (typeToReflect.BaseType != null)
            {
                RecursiveCopyBaseTypePrivateFields(originalObject, visited, cloneObject, typeToReflect.BaseType, deepCopy);
                CopyFields(originalObject, visited, cloneObject, typeToReflect.BaseType, BindingFlags.Instance | BindingFlags.NonPublic, info => info.IsPrivate, deepCopy);
            }
        }

        private static void CopyFields(object originalObject, IDictionary<object, object> visited, object cloneObject, Type typeToReflect, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy, Func<FieldInfo, bool> filter = null, bool deepCopy = true)
        {
            foreach (FieldInfo fieldInfo in typeToReflect.GetFields(bindingFlags))
            {
                if (filter != null && filter(fieldInfo) == false) continue;
                if (IsPrimitive(fieldInfo.FieldType)) continue;
                var originalFieldValue = fieldInfo.GetValue(originalObject);
                var clonedFieldValue = InternalCopy(originalFieldValue, visited, deepCopy);
                fieldInfo.SetValue(cloneObject, clonedFieldValue);
            }
        }

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Identifica se un Tipo è primitivo
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsPrimitive(this Type type)
        {
            if (type == typeof(String)) return true;
            return (type.IsValueType & type.IsPrimitive);
        }

        /// <summary>
        /// Ritorna una copia completa (deep copy) dell'oggetto
        /// </summary>
        /// <param name="originalObject">Oggetto da copiare</param>
        /// <returns>Oggetto copiato</returns>
        public static Object GetDeepClone(this Object originalObject)
        {
            return InternalCopy(originalObject, new Dictionary<Object, Object>(new ReferenceEqualityComparer()), true);
        }

        /// <summary>
        /// Ritorna una copia completa (deep copy) tipizzata dell'oggetto
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="original">Oggetto da copiare</param>
        /// <returns>Copia tipizzata dell'oggetto</returns>
        public static T GetDeepClone<T>(this T original)
        {
            return (T)GetDeepClone((Object)original);
        }


        /// <summary>
        /// Ritorna una copia semplice dell'oggetto
        /// </summary>
        /// <param name="originalObject">Oggetto da copiare</param>
        /// <returns>Oggetto copiato</returns>
        public static Object GetLightClone(this Object originalObject)
        {
            return InternalCopy(originalObject, new Dictionary<Object, Object>(new ReferenceEqualityComparer()), false);
        }

        /// <summary>
        /// Ritorna una copia semplice tipizzata dell'oggetto
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="original">Oggetto da copiare</param>
        /// <returns>Copia tipizzata dell'oggetto</returns>
        public static T GetLightClone<T>(this T original)
        {
            return (T)GetLightClone((Object)original);
        }

        /// <summary>
        /// Ritorna una copia semplice dell'oggetto
        /// </summary>
        /// <param name="originalObject">Oggetto da copiare</param>
        /// <returns>Oggetto copiato</returns>
        public static Object GetClone(this Object originalObject, bool deepCopy = true)
        {
            return InternalCopy(originalObject, new Dictionary<Object, Object>(new ReferenceEqualityComparer()), deepCopy);
        }

        /// <summary>
        /// Ritorna una copia semplice tipizzata dell'oggetto
        /// </summary>
        /// <typeparam name="T">Tipizzazione</typeparam>
        /// <param name="original">Oggetto da copiare</param>
        /// <returns>Copia tipizzata dell'oggetto</returns>
        public static T GetClone<T>(this T original, bool deepCopy = true)
        {
            return (T)GetClone((Object)original, deepCopy);
        }




        //public static void UpdateTo(this Object originalObject, Object updatedObject)
        //{
            
        //}

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }

    /// <summary>
    /// Comparatore di uguaglianza per Objects
    /// </summary>
    public class ReferenceEqualityComparer : EqualityComparer<object>
    {
        /// <summary>
        /// Confronto di uguaglianza tra oggetti
        /// </summary>
        /// <param name="x">Primo oggetto</param>
        /// <param name="y">Secondo oggetto</param>
        /// <returns>True se gli oggetti sono uguali</returns>
        public override bool Equals(object x, object y)
        {
            return ReferenceEquals(x, y);
        }

        /// <summary>
        /// Ritorna un integer come Hash di un oggetto
        /// </summary>
        /// <param name="obj">Oggetto di input</param>
        /// <returns>Hash code</returns>
        public override int GetHashCode(object obj)
        {
            if (obj == null) return 0;
            return obj.GetHashCode();
        }
    }

    /// <summary>
    /// Metodi di estensione per Arrays
    /// </summary>
    public static class ArrayExtensions
    {
        /// <summary>
        /// Itera un'azione su ogni elemento dell'array
        /// </summary>
        /// <param name="array">Array</param>
        /// <param name="action">Azione</param>
        public static void ForEach(this Array array, Action<Array, int[]> action)
        {
            if (array.LongLength == 0) return;
            ArrayTraverse walker = new ArrayTraverse(array);
            do action(array, walker.Position);
            while (walker.Step());
        }
    }

    internal class ArrayTraverse
    {
        public int[] Position;
        private int[] maxLengths;

        /// <summary>
        /// Percorre l'array
        /// </summary>
        /// <param name="array">Array</param>
        public ArrayTraverse(Array array)
        {
            maxLengths = new int[array.Rank];
            for (int i = 0; i < array.Rank; ++i)
            {
                maxLengths[i] = array.GetLength(i) - 1;
            }
            Position = new int[array.Rank];
        }

        public bool Step()
        {
            for (int i = 0; i < Position.Length; ++i)
            {
                if (Position[i] < maxLengths[i])
                {
                    Position[i]++;
                    for (int j = 0; j < i; j++)
                    {
                        Position[j] = 0;
                    }
                    return true;
                }
            }
            return false;
        }
    }


}
