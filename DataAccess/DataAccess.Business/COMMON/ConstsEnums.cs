﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Business.COMMON
{
    /// <summary>
    /// Tipo articolo
    /// </summary>
    public enum ArticleType 
    {  
        UNDEFINED = 0, 
        SLAB = 1, 
        RESIDUE = 2, 
        SEMIFINISHED = 3, 
        COMPLETED = 4 
    }

    /// <summary>
    /// Stato articolo
    /// </summary>
    public enum ArticleStatus
    {
        /// <summary>
        /// Non definito
        /// </summary>
        UNDEFINED = 0,

        /// <summary>
        /// In magazzino
        /// </summary>
        START_STOCK = 1,		// In magazzino

        /// <summary>
        /// Disponibile
        /// </summary>
        START_AVAILABLE = 2,	// Disponibile

        /// <summary>
        /// Prenotato
        /// </summary>
        START_LOCKED = 3,		// Prenotato

        /// <summary>
        /// In programmazione
        /// </summary>
        START_PROGRAMMING = 4,	// In programmazione

        /// <summary>
        /// Programmato
        /// </summary>
        START_PROGRAMMED = 5,	// Programmato

        /// <summary>
        /// Confermato
        /// </summary>
        START_CONFIRMED = 6,	// Confermato

        /// <summary>
        /// In lavoro
        /// </summary>
        START_WORKING = 7,		// In lavoro

        /// <summary>
        /// Completato
        /// </summary>
        START_COMPLETED = 8,	// Completato

        /// <summary>
        /// Sospeso
        /// </summary>
        START_SUSPENDED = 9,	// Sospeso

        /// <summary>
        /// Non disponibile
        /// </summary>
		START_UNAVAILABLE = 10,	// Non disponibile

        /// <summary>
        /// Scartato
        /// </summary>
        START_DISCARDED = 11,    // Scartato

        /// <summary>
        /// Distrutto
        /// </summary>
        START_DESTROYED = 12     // Distrutto
    }

    class ConstsEnums
    {
    }
}
