﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Classe di base che implementa l'interfaccia INotifyPropertyChanged
    /// per utilizzo in binding WPF
    /// </summary>
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        /// <summary>
        /// Solleva l'evento PropertyChanged per una specifica Property
        /// </summary>
        /// <param name="propertyName">Nome Property</param>
        protected  void OnPropertyChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);

            var handler = PropertyChanged;

            if (handler != null)
            {
                RaisePropertyChanged(propertyName, handler);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="handler"></param>
        protected virtual void RaisePropertyChanged(string propertyName, PropertyChangedEventHandler handler)
        {
            handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // If you raise PropertyChanged and do not specify a property name,
            // all properties on the object are considered to be changed by the binding system.
            if (String.IsNullOrEmpty(propertyName))
                return;

            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                    throw new ArgumentException(msg);
                else
                    Debug.Fail(msg);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }



        /// <summary>
        /// Evento di notifica Property modificata
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
