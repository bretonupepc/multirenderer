﻿using System;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Informazioni di base associate ad un campo da trattare come "persistable"
    /// </summary>
    public class FieldItemBaseInfo
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _propertyName = string.Empty;

        private Type _propertyType;

        private FieldRetrieveMode _defaultRetrieveMode;

        private string _filenamePrefix = string.Empty;

        private string _filenameExt = string.Empty;

        private FieldManagementType _defaultManagementType;

        private RelatedEntity _relation = null;

        

        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="propertyName">Nome campo</param>
        /// <param name="propertyType">Tipo campo</param>
        /// <param name="defaultRetrieveMode">Modalità di recupero del valore del campo</param>
        /// <param name="defaultManagementType">Modalità di gestione del valore del campo</param>
        /// <param name="filenamePrefix">Prefisso da utilizzare per l'eventuale nome file</param>
        /// <param name="filenameExt">Estensione da utilizzare per l'eventuale nome file</param>
        public FieldItemBaseInfo(string propertyName, Type propertyType, 
            FieldRetrieveMode defaultRetrieveMode = FieldRetrieveMode.Deferred,
            FieldManagementType defaultManagementType = FieldManagementType.Direct,
            string filenamePrefix = "",
            string filenameExt = "")
        {
            PropertyName = propertyName;
            PropertyType = propertyType;
            _defaultRetrieveMode = defaultRetrieveMode;
            _defaultManagementType = defaultManagementType;
            _filenamePrefix = filenamePrefix;
            _filenameExt = filenameExt;

        }

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="propertyName">Nome campo</param>
        /// <param name="defaultRetrieveMode">Modalità di recupero del valore del campo</param>
        /// <param name="defaultManagementType">Modalità di gestione del valore del campo</param>
        /// <param name="filenamePrefix">Prefisso da utilizzare per l'eventuale nome file</param>
        /// <param name="filenameExt">Estensione da utilizzare per l'eventuale nome file</param>
        public FieldItemBaseInfo(string propertyName, 
            FieldRetrieveMode defaultRetrieveMode = FieldRetrieveMode.Deferred,
            FieldManagementType defaultManagementType = FieldManagementType.Direct,
            string filenamePrefix = "",
            string filenameExt = "")
        {
            PropertyName = propertyName;
            _defaultRetrieveMode = defaultRetrieveMode;
            _defaultManagementType = defaultManagementType;
            _filenamePrefix = filenamePrefix;
            _filenameExt = filenameExt;

        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Nome campo
        /// </summary>
        public string PropertyName
        {
            get { return _propertyName; }
            set { _propertyName = value; }
        }

        /// <summary>
        /// Tipo campo
        /// </summary>
        public Type PropertyType
        {
            get { return _propertyType; }
            set { _propertyType = value; }
        }

        /// <summary>
        /// Modalità di recupero del valore del campo
        /// </summary>
        public FieldRetrieveMode DefaultRetrieveMode
        {
            get { return _defaultRetrieveMode; }
        }

        /// <summary>
        /// Prefisso da utilizzare per l'eventuale nome file
        /// </summary>
        public string FilenamePrefix
        {
            get { return _filenamePrefix; }
        }

        /// <summary>
        /// Estensione da utilizzare per l'eventuale nome file
        /// </summary>
        public string FilenameExt
        {
            get { return _filenameExt; }
        }

        /// <summary>
        /// Modalità di gestione del valore del campo
        /// </summary>
        public FieldManagementType DefaultManagementType
        {
            get { return _defaultManagementType; }
        }

        /// <summary>
        /// Relazione del campo
        /// </summary>
        public RelatedEntity Relation
        {
            get { return _relation; }
            set { _relation = value; }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
