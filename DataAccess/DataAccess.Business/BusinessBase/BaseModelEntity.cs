﻿using System;
using System.ComponentModel;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Stato dell'entità 
    /// </summary>
    public enum EntityStatus
    {
        /// <summary>
        /// Stato iniziale pre-lettura da repository
        /// </summary>
        Unknown,

        /// <summary>
        /// Nuova entità
        /// </summary>
        New,

        /// <summary>
        /// Entità non modificata
        /// </summary>
        Unchanged,

        /// <summary>
        /// Entità modificata
        /// </summary>
        Changed
    }

    /// <summary>
    /// Classe base per entità model; implementa interfaccia IDataErrorInfo
    /// </summary>
    public abstract class BaseModelEntity : ObservableObject, IDataErrorInfo
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private EntityStatus _status = EntityStatus.Unknown;


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields

        protected string _error = string.Empty;

        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Stato entità
        /// </summary>
        public virtual EntityStatus Status
        {
            get { return _status; }
            set
            {
                bool hasChanged = (_status != value);
                _status = value;
                if (hasChanged)
                {
                    OnPropertyChanged("Status");
                }
            }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<






        #region IDataErrorInfo Members

        /// <summary>
        /// Ritorna la stringa di errore relativa all'intero oggetto
        /// </summary>
        public virtual string Error
        {
            get
            {
                return _error;
            }
        }

        /// <summary>
        /// Ritorna la stringa di errore relativa a una specifica proprietà
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public virtual string this[string columnName]
        {
            get
            {
                return string.Empty;
            }
        }

        #endregion


    }
}
