﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using TraceLoggers;
using DataAccess.Business.DATASOURCES;
using DataAccess.Business.Persistence;
using Wpf.Common;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Classe astratta per entità di tipo persistable
    /// </summary>
    public abstract class BasePersistableEntity: BaseModelEntity
    {

        #region >-------------- Constants and Enums

        //public enum PersistableFields
        //{
            
        //}

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration

        /// <summary>
        /// Segnala la variazione di stato di un Property dell'entità
        /// </summary>
        public event EventHandler<EventArgs<FieldItemInfo>> PropertyStatusChanged;

        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private List<FieldItemInfo> _fieldsInfos = null;

        private PersistableEntityProxy _repositoryProxy = null;

        private bool _hasIdentityId = true;

        private static string _baseTempFolder = string.Empty;

        private PersistenceScope _scope;

        private Dictionary<string, string> _errorDictionary; 

        #endregion Private Fields --------<

        #region >-------------- Constructors

        protected BasePersistableEntity()
        {
            //BuildFieldsInfos();
            //CheckEnumContent();
        }

        protected BasePersistableEntity(PersistenceScope scope)
        {
            Scope = scope;
            //BuildFieldsInfos();
            //CheckEnumContent();
        }

        //private void CheckEnumContent()
        //{
        //    if (Enum.GetValues(typeof (PersistableFields)).Length == 0)
        //    {
        //        EnumBuilder eb = 
        //        foreach (FieldItemBaseInfo baseInfo in PersistableEntityBaseInfos.GetBaseInfos(this.GetType()))
        //        {
        //            FieldItemInfo info = new FieldItemInfo(baseInfo);
        //            FieldsInfos.Add(info);
        //        }

        //    }
        //}

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Restituisce un integer come identificativo univoco
        /// </summary>
        public virtual int UniqueId
        {
            get { return -1; }
        }

        /// <summary>
        /// Restituisce una string come identificativo univoco
        /// </summary>
        public virtual string UniqueCode
        {
            get { return string.Empty; }
        }



        /// <summary>
        /// Identifica l'entità come nuova oppure no
        /// </summary>
        public virtual bool IsNewItem
        {
            get
            {
                return UniqueId  == - 1;
            }
        }


        public override EntityStatus Status
        {
            get
            {
                if (IsNewItem)
                    return EntityStatus.New;
                if (FieldsInfos.Any(i => i.Status == FieldStatus.Set))
                    return EntityStatus.Changed;
                if (FieldsInfos.All(i => i.Status == FieldStatus.Unknown))
                    return EntityStatus.Unknown;
                return EntityStatus.Unchanged;
            }
        }

        /// <summary>
        /// Elenco delle informazioni campi dell'entità
        /// </summary>
        public List<FieldItemInfo> FieldsInfos
        {
            get
            {
                if (_fieldsInfos == null)
                {
                    _fieldsInfos = new List<FieldItemInfo>();
                }
                return _fieldsInfos;
            }
        }

        /// <summary>
        /// Identifica se l'entità prevede una Identity come autonumerazione
        /// </summary>
        public bool HasIdentityId
        {
            get { return _hasIdentityId; }
            set { _hasIdentityId = value; }
        }

        /// <summary>
        /// Definisce il folder da usare per i dati temporanei dell'entità
        /// </summary>
        protected string BaseTempFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_baseTempFolder))
                {
                    string tempPath = System.IO.Path.GetTempPath();

                    string mainFolder = "DataAccess.Objects";
                    if (Scope!= null && !string.IsNullOrEmpty(Scope.Name))
                    {
                        mainFolder = string.Format("{0}.{1}", mainFolder, Scope.Name);
                    }

                    tempPath = Path.Combine(tempPath, mainFolder);
                    if (!Directory.Exists(tempPath))
                        Directory.CreateDirectory(tempPath);
                    _baseTempFolder = tempPath;
                }
                return _baseTempFolder;
            }
        }

        /// <summary>
        /// Restituisce il folder temporaneo usato per le entità della classe
        /// </summary>
        protected virtual string ClassTempFolder
        {
            get
            {
                string path = Path.Combine(BaseTempFolder, GetType().Name);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return path;
            }
        }

        public PersistenceScope Scope
        {
            get { return _scope; }
            set { _scope = value; }
        }

        public Dictionary<string, string> ErrorDictionary
        {
            get
            {
                if (_errorDictionary == null)
                {
                    _errorDictionary = new Dictionary<string, string>();
                }
                return _errorDictionary;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool IsValid
        {
            get
            {
                return ErrorDictionary.Count == 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CheckIsValid()
        {
            OnPropertyChanged("IsValid");
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties

        //protected PersistableEntityProxy RepositoryProxy
        //{
        //    get
        //    {
        //        if (_repositoryProxy == null)
        //        {
        //            //Scope.Manager.
        //        }
        //        return _repositoryProxy;
        //    }
        //}

        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Recupera le informazioni di campo per una specifica Property
        /// </summary>
        /// <param name="propertyName">Nome Property</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public FieldItemInfo GetFieldItemInfo(string propertyName)
        {
            FieldItemInfo info = FieldsInfos.FirstOrDefault(i => i.BaseInfo.PropertyName == propertyName);
            if (info == null)
            {
                FieldItemBaseInfo baseInfo =
                    PersistableEntityBaseInfos.GetBaseInfos(this.GetType())
                        .FirstOrDefault(bi => bi.PropertyName == propertyName);
                if (baseInfo != null)
                {
                    info = new FieldItemInfo(baseInfo);
                    FieldsInfos.Add(info);
                }
            }

            return info;
        }


        /// <summary>
        /// Salva l'entità nel repository
        /// </summary>
        /// <param name="providerType">Tipo provider di salvataggio</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool SaveToRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            if (CanSave())
            {
                if (IsNewItem || (Status != EntityStatus.Unchanged && Status != EntityStatus.Unknown))
                {
                    var proxy = GetProxy(providerType, true);
                    if (proxy != null)
                    {
                        return proxy.Save();
                    }
                }
                else
                {
                    return true;
                }

            }
            return false;
        }

        /// <summary>
        /// Salva l'entità nel nodo Xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <param name="providerType">Tipo provider di salvataggio (di classe Xml)</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool SaveToRepository(out XElement node, PersistenceProviderType providerType = PersistenceProviderType.Xml)
        {
            node = null;

            if (CanSave())
            {
                var proxy = GetProxy(providerType);
                if (proxy != null)
                {
                    return proxy.Save(out node);
                }

                //return true;
            }
            return false;
        }

        /// <summary>
        /// Salva l'entità in un file Xml
        /// </summary>
        /// <param name="filepath">Percorso del file</param>
        /// <param name="overwriteIfExistent">Flag di sovrascrittura file</param>
        /// <param name="providerType">Tipo provider di salvataggio (di classe Xml)</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool SaveToRepository(string filepath, bool overwriteIfExistent = true, PersistenceProviderType providerType = PersistenceProviderType.Xml)
        {
            if (CanSave())
            {
                var proxy = GetProxy(providerType);
                if (proxy != null)
                {
                    return proxy.Save(filepath, overwriteIfExistent);
                }

                //return true;
            }
            return false;
        }

        /// <summary>
        /// Rilegge i valori da repository
        /// </summary>
        /// <returns>True se rilettura eseguita con successo; false altrimenti</returns>
        public virtual bool RefreshFromRepository()
        {
            return false;
        }

        /// <summary>
        /// Rilegge i valori da nodo xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <returns>True se rilettura eseguita con successo; false altrimenti</returns>
        public virtual bool RefreshFromRepository(XElement node)
        {
            return false;
        }

        /// <summary>
        /// Legge l'entità da nodo xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <param name="providerType">Tipo provider di lettura (di classe Xml)</param>
        /// <returns>True se lettura eseguita con successo; false altrimenti</returns>
        public virtual bool GetFromRepository(XElement node, PersistenceProviderType providerType = PersistenceProviderType.Xml)
        {
            var proxy = GetProxy(providerType);
            if (proxy != null)
            {
                return proxy.GetFields(node);
            }


            return false;
        }

        /// <summary>
        /// Legge l'entità da file xml
        /// </summary>
        /// <param name="filepath">Percorso file</param>
        /// <param name="providerType">Tipo provider di lettura (di classe Xml)</param>
        /// <returns>True se lettura eseguita con successo; false altrimenti</returns>
        public virtual bool GetFromRepository(string filepath, PersistenceProviderType providerType = PersistenceProviderType.Xml)
        {
            var proxy = GetProxy(providerType);
            if (proxy != null)
            {
                return proxy.GetFields(filepath);
            }
            return false;
        }

        /// <summary>
        /// Verifica l'esistenza delle condizioni necessarie per salvare l'entità
        /// </summary>
        /// <returns>true = l'entità può essere salvata</returns>
        public virtual bool CanSave()
        {
            return (Error == string.Empty);
        }

        /// <summary>
        /// Elimina l'entità dal repository
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool DeleteFromRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            bool retVal = false;

            if (CanDelete())
            {
                retVal = DeleteChildrenItemsFromRepository(providerType);
                if (retVal)
                {
                    var proxy = GetProxy(providerType);
                    if (proxy != null)
                    {
                        retVal = proxy.Delete();
                    }
                }

            }
            return retVal;
        }

        /// <summary>
        /// Elimina le collezioni figlie
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>true if successful</returns>
        public virtual bool DeleteChildrenItemsFromRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            return true;
        }


        /// <summary>
        /// Verifica l'esistenza delle condizioni necessarie per eliminare l'entità
        /// </summary>
        /// <returns>true = l'entità può essere eliminata</returns>
        public virtual bool CanDelete()
        {
            return true;
        }

        /// <summary>
        /// Verifica se l'entità esiste nel repository
        /// </summary>
        /// <returns>true = l'entità esiste nel repository</returns>
        public virtual bool ExistsInRepository()
        {
            return true;
        }

        /// <summary>
        /// Imposta la Property dell'entità (Generic)
        /// </summary>
        /// <typeparam name="T">Property type (Generic)</typeparam>
        /// <param name="propertyName">Nome property</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="target">Campo di destinazione per il valore della Property</param>
        /// <param name="forceStatus">Forza lo stato della Property</param>
        /// <param name="newStatus">Nuovo stato da impostare</param>
        /// <param name="forceOnChanged">Forza l'evento PropertyChanged</param>
        /// <param name="avoidOnChanged">Inibisce l'evento PropertyChanged</param>
        /// <param name="avoidOnStatusChanged">Inibisce l'evento di variazione di stato</param>
        protected virtual void SetProperty<T>(string propertyName, T value, ref T target, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set, 
            bool forceOnChanged = false, 
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false) 
        {
            //bool hasChanged = value != target;
            bool hasChanged = !(EqualityComparer<T>.Default.Equals(value, target));

            //bool hasChanged = !object.Equals(value, target);

            bool statusChanged = false;

            target = value;

            FieldItemInfo info = GetFieldItemInfo(propertyName);
            if (info != null)
            {
                if ((forceStatus && newStatus != info.Status)
                    || (!forceStatus && info.Status != FieldStatus.Set))
                {
                    statusChanged = true;
                }
                info.Status = forceStatus ? newStatus : FieldStatus.Set;

                // reset related fields
                if (hasChanged)
                {
                    var relatedFields =
                        this.FieldsInfos.Where(
                            i => i.BaseInfo.Relation != null && i.BaseInfo.Relation.SourceKey == info.BaseInfo.PropertyName).ToList();
                    foreach (var fieldInfo in relatedFields)
                    {
                        fieldInfo.Status = FieldStatus.Unknown;
                    }
                }


            }

            if ((!avoidOnChanged) && (hasChanged || forceOnChanged))
            {
                OnPropertyChanged(propertyName);
            }

            if ((!avoidOnStatusChanged) && statusChanged)
            {
                OnPropertyStatusChanged(info);
            }

        }


        //public abstract void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
        //    FieldStatus newStatus = FieldStatus.Set,
        //    bool forceOnChanged = false,
        //    bool avoidOnChanged = false,
        //    bool avoidOnStatusChanged = false);


        /// <summary>
        /// Imposta la Property dell'entità (Generic)
        /// </summary>
        /// <typeparam name="T">Property type (Generic)</typeparam>
        /// <param name="propertyName">Nome Property</param>
        /// <param name="value">Valore da impostare</param>
        /// <param name="forceStatus">Forza lo stato della Property</param>
        /// <param name="newStatus">Nuovo stato da impostare</param>
        /// <param name="forceOnChanged">Forza l'evento PropertyChanged</param>
        /// <param name="avoidOnChanged">Inibisce l'evento PropertyChanged</param>
        /// <param name="avoidOnStatusChanged">Inibisce l'evento di variazione di stato</param>
        /// <param name="index">Indice interno per Property di tipo ICollection</param>
        public virtual void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false,
            int index = -1)
        {

            Type thisType = this.GetType();
            PropertyInfo propertyInfo = thisType.GetProperty(propertyName);

            bool statusChanged = false;

            propertyInfo.SetValue(this, value, ((index == -1) ? null : new object[]{index}));

            FieldItemInfo info = GetFieldItemInfo(propertyName);
            if (info != null)
            {
                if ((forceStatus && newStatus != info.Status)
                    || (!forceStatus && info.Status != FieldStatus.Set))
                {
                    statusChanged = true;
                }
                info.Status = forceStatus ? newStatus : FieldStatus.Set;
            }

            if (!avoidOnChanged) 
            {
                OnPropertyChanged(propertyName);
            }

            //if (!avoidOnChanged)
            //{
            //    if (forceOnChanged)
            //        OnPropertyChanged(propertyName);
            //    else
            //    {
            //        bool hasChanged = !(EqualityComparer<T>.Default.Equals(value, (T)propertyInfo.GetValue(this, null)));
            //        if (hasChanged)
            //            OnPropertyChanged(propertyName);
            //    }
            //}

            if ((!avoidOnStatusChanged) && statusChanged)
            {
                OnPropertyStatusChanged(info);
            }
        }

        /// <summary>
        /// Imposta lo stato di una Property
        /// </summary>
        /// <param name="propertyName">Nome property</param>
        /// <param name="newStatus">Stato da impostare</param>
        /// <param name="forceOnStatusChanged">Forza l'evento di cambio stato</param>
        public virtual void SetPropertyStatus(string propertyName, FieldStatus newStatus, bool forceOnStatusChanged = false)
        {
            bool statusChanged = false;

            FieldItemInfo info = GetFieldItemInfo(propertyName);
            if (info != null)
            {
                statusChanged = newStatus != info.Status;
                info.Status = newStatus;
            }

            if (statusChanged || forceOnStatusChanged)
            {
                OnPropertyStatusChanged(info);
            }
        }

        //public abstract T GetProperty<T>(string propertyName);

        /// <summary>
        /// Imposta uno specifico stato per tutte le Property dell'entità
        /// </summary>
        /// <param name="newStatus">Stato da impostare</param>
        /// <param name="forceOnStatusChanged">Forza l'evento di cambio stato</param>
        public virtual void SetOverallPropertiesStatus(FieldStatus newStatus, bool forceOnStatusChanged = false)
        {
            if (FieldsInfos == null || FieldsInfos.Count == 0)
            {
                BuildCompleteFieldsInfos();
            }
            if (FieldsInfos != null)
                foreach (var fieldInfo in FieldsInfos)
                {
                    SetPropertyStatus(fieldInfo.BaseInfo.PropertyName, newStatus, forceOnStatusChanged);
                }
        }

        /// <summary>
        /// Restituisce il valore di una Property
        /// </summary>
        /// <typeparam name="T">Property type (Generic)</typeparam>
        /// <param name="propertyName">Nome property</param>
        /// <returns>Valore attuale</returns>
        public virtual T GetProperty<T>(string propertyName)
        {
            Type thisType = this.GetType();
            PropertyInfo propertyInfo = thisType.GetProperty(propertyName);
            return (T) propertyInfo.GetValue(this, null);
        }

        /// <summary>
        /// Restituisce il valore di una Property
        /// </summary>
        /// <typeparam name="T">Property type (Generic)</typeparam>
        /// <param name="propertyName">Nome property</param>
        /// <param name="type">Property type</param>
        /// <returns>Valore attuale</returns>
        public virtual T GetProperty<T>(string propertyName, T type)
        {
            return GetProperty<T>(propertyName);
        }

        /// <summary>
        /// Restituisce lo stato di una Property
        /// </summary>
        /// <param name="propertyName">Nome property</param>
        /// <param name="status">Stato attuale</param>
        /// <returns>Lettura eseguita/Non eseguita</returns>
        public virtual bool GetPropertyStatus(string propertyName, out FieldStatus status)
        {
            bool retVal = false;

            status = FieldStatus.Unknown;

            FieldItemInfo info = GetFieldItemInfo(propertyName);
            if (info != null)
            {
                status = info.Status;
                retVal = true;
            }

            return retVal;
        }


        /// <summary>
        /// Restituisce tipo e stato di una Property
        /// </summary>
        /// <param name="propertyName">Nome property</param>
        /// <param name="status">Stato attuale</param>
        /// <param name="propertyType">Property type</param>
        /// <returns>Lettura eseguita/Non eseguita</returns>
        public virtual bool GetPropertyStatusAndType(string propertyName, out FieldStatus status, out Type propertyType)
        {
            bool retVal = false;

            status = FieldStatus.Unknown;

            propertyType = typeof (object);

            FieldItemInfo info = GetFieldItemInfo(propertyName);
            if (info != null)
            {
                status = info.Status;
                propertyType = info.BaseInfo.PropertyType;
                retVal = true;
            }

            return retVal;
        }

        /// <summary>
        /// Restituisce il nome tabella repository per uno specifico tipo Provider
        /// </summary>
        /// <param name="persistenceProviderType">Tipo provider</param>
        /// <returns>Nome tabella</returns>
        public string GetMainDbTableName(PersistenceProviderType persistenceProviderType)
        {
            string tableName = string.Empty;

            MemberInfo info = this.GetType();
            var attribute = info.GetCustomAttributes(true).Where(a => a is MainDbTableAttribute)
                .FirstOrDefault(a => ((MainDbTableAttribute)a).ProviderType == persistenceProviderType);
            if (attribute != null)
                tableName = ((MainDbTableAttribute)attribute).MainDbTable;

            return tableName;
        }

        /// <summary>
        /// Crea il set di informazione campi dell'entità
        /// </summary>
        public virtual void BuildCompleteFieldsInfos()
        {
            //_fieldsInfos = new List<FieldItemInfo>();
            foreach (FieldItemBaseInfo baseInfo in PersistableEntityBaseInfos.GetBaseInfos(this.GetType()))
            {
                FieldItemInfo info = new FieldItemInfo(baseInfo);
                if (FieldsInfos.FirstOrDefault(i => i.BaseInfo.PropertyName == info.BaseInfo.PropertyName) == null)
                {
                    FieldsInfos.Add(info);
                }
            }
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        //[Obsolete]
        //protected T GetProperty_<T>(string propertyName, ref T actualValue)
        //{
        //    FieldStatus status;

        //    if (GetPropertyStatus(propertyName, out status))
        //    {
        //        if (status == FieldStatus.Unknown)
        //        {
        //            SetPropertyStatus(propertyName, FieldStatus.Reading);

        //            GetSingleFieldFromRepository(propertyName);

        //        }
        //    }

        //    return (T)actualValue;

        //}

        /// <summary>
        /// Restituisce il valore di una Property
        /// </summary>
        /// <typeparam name="T">Propety type (Generic)</typeparam>
        /// <param name="propertyName">Nome property</param>
        /// <param name="actualValue">Valore attuale</param>
        /// <returns>Lettura eseguita/Non eseguita</returns>
        protected T GetProperty<T>(string propertyName, ref T actualValue)
        {
            FieldItemInfo info = GetFieldItemInfo(propertyName);


            if (info != null)
            {
                if (info.Status == FieldStatus.Unknown)
                {
                    SetPropertyStatus(propertyName, FieldStatus.Reading);

                    if (info.BaseInfo.Relation == null)
                    {
                        //TODO: Verificare
                        if (Scope != null && Scope.Manager != null )
                        GetSingleFieldFromRepository(propertyName, Scope.Manager.DefaultRepositoryProviderType);

                    }

                    else if (info.BaseInfo.PropertyType.IsSubclassOf(typeof (BasePersistableEntity)))
                    {
                        GetSingleObjectFromRepository(info);
                        //switch (info.RetrieveMode)
                        //{
                        //    case FieldRetrieveMode.Deferred:
                        //    case FieldRetrieveMode.Immediate:
                        //        GetSingleObjectFromRepository(info);
                        //        break;

                        //    case FieldRetrieveMode.DeferredAsync:
                        //        Thread th = new Thread(() => GetSingleObjectFromRepository(info));
                        //        th.Start();
                        //        break;
                        //}
                    }
                    //else if (info.BaseInfo.PropertyType.IsSubclassOf(typeof (PersistableEntityCollection<>)))
                    //{
                    //    GetSingleCollectionFromRepository(info);
                    //}
                    else if (info.BaseInfo.PropertyType.BaseType != null
                        && info.BaseInfo.PropertyType.BaseType.Name.Contains("PersistableEntityCollection"))
                    {
                        GetSingleCollectionFromRepository(info);
                    }
                    else if (info.BaseInfo.PropertyType.GetGenericArguments().Any())
                    {
                        Type arg = info.BaseInfo.PropertyType.GetGenericArguments().First();
                        if (arg.IsSubclassOf(typeof (BasePersistableEntity)))
                        {
                            GetSingleCollectionFromRepository(info);
                            //switch (info.RetrieveMode)
                            //{
                            //    case FieldRetrieveMode.Deferred:
                            //    case FieldRetrieveMode.Immediate:
                            //        GetSingleCollectionFromRepository(info);
                            //        break;

                            //    case FieldRetrieveMode.DeferredAsync:
                            //        Thread th = new Thread(() => GetSingleCollectionFromRepository(info));
                            //        th.Start();
                            //        break;
                            //}
                        }
                    }
                }
            }

            return (T)actualValue;

        }

        /// <summary>
        /// Recupera da repository un campo relazionato di tipo Collection
        /// </summary>
        /// <param name="info">Informazioni del campo</param>
        private void GetSingleCollectionFromRepository(FieldItemInfo info)
        {
            try
            {
                RelatedEntity relatedEntity = info.BaseInfo.Relation;

                if (relatedEntity == null || relatedEntity.Relation != RelationType.OneToMany) return;

                //bool foundOnDatasource = false;

                //if (info.BaseInfo.Relation.UseDataSource)
                //{
                //    int id = GetProperty<int>(relatedEntity.SourceKey);
                //    dynamic innerType = Activator.CreateInstance(info.BaseInfo.PropertyType);
                //    var item = DataSources.GetById(id, innerType);
                //    if (item != null)
                //    {
                //        foundOnDatasource = true;
                //        SetProperty(info.BaseInfo.PropertyName, item, true, FieldStatus.Read);
                //    }
                //}

                //if (foundOnDatasource) return;

                var propertyType = info.BaseInfo.PropertyType;

                dynamic collection = Activator.CreateInstance(propertyType);



                //var instance = Activator.CreateInstance(propertyType);

                //if (instance != null)
                //dynamic collection = PersistableEntityCollectionFactory.GetNewEntityCollection(instance.GetType().gett);

                var sourceKeyInfo = FieldsInfos.FirstOrDefault(i => i.BaseInfo.PropertyName == relatedEntity.SourceKey);

                if (sourceKeyInfo != null)
                {
                    FilterClause clause =
                        new FilterClause(
                            new FieldItemBaseInfo(relatedEntity.RelatedKey, sourceKeyInfo.BaseInfo.PropertyType),
                            ConditionOperator.Eq,
                            GetProperty<int>(relatedEntity.SourceKey));

                    collection.GetItemsFromRepository(clause: clause, persistenceProviderType: Scope.Manager.DefaultRepositoryProviderType);

                    SetProperty(info.BaseInfo.PropertyName, collection, true, FieldStatus.Read);
                    //if (collection.Count > 0)
                    //{
                    //    int i = 0;
                    //    foreach(var item in collection)
                    //        SetProperty(info.BaseInfo.PropertyName, item, true, FieldStatus.Read,index:i++ );
                    //}
                    //else
                    //{
                    //    SetPropertyStatus(info.BaseInfo.PropertyName, FieldStatus.Read);
                    //}
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("BasePersistableEntity GetSingleObjectFromRepository error ", ex);
            }
        }


        /// <summary>
        /// Recupera da repository un campo relazionato di tipo singolo
        /// </summary>
        /// <param name="info">Informazioni del campo</param>
        private void GetSingleObjectFromRepository(FieldItemInfo info)
        {
            try
            {
                RelatedEntity relatedEntity = info.BaseInfo.Relation;

                if (relatedEntity == null || relatedEntity.Relation != RelationType.OneToOne) return;

                bool foundOnDatasource = false;

                var propertyType = info.BaseInfo.PropertyType;

                Type specializedType = null;

                if (!string.IsNullOrEmpty(relatedEntity.FactoryParameter))
                {
                    object parameterValue = GetProperty<object>(relatedEntity.FactoryParameter);

                    var parameter = new[] { parameterValue };

                    var factoryMethod = propertyType.GetMethod("GetSpecializedType", BindingFlags.Public|BindingFlags.Static);
                    if (factoryMethod != null)
                    {
                        specializedType = (Type)factoryMethod.Invoke(null, parameter);
                    }
                }

                if (info.BaseInfo.Relation.UseDataSource)
                {
                    if (relatedEntity.RelatedKey == "Id")
                    {
                        int id = GetProperty<int>(relatedEntity.SourceKey);

                        //var pType = typeof(info.BaseInfo.PropertyType)

                        dynamic innerType = Activator.CreateInstance(specializedType ?? propertyType);
                        innerType.Scope = Scope;

                        var item = Scope.DataSources.GetById(id, innerType, Scope.Manager.DefaultRepositoryProviderType);
                        if (item != null)
                        {
                            foundOnDatasource = true;
                            SetProperty(info.BaseInfo.PropertyName, item, true, FieldStatus.Read);
                        }
                    }

                    else if (relatedEntity.RelatedKey == "Code")
                    {
                        string code = GetProperty<string>(relatedEntity.SourceKey);

                        dynamic innerType = Activator.CreateInstance(specializedType ?? propertyType);
                        innerType.Scope = Scope;

                        var item = Scope.DataSources.GetByCode(code, innerType, Scope.Manager.DefaultRepositoryProviderType);
                        if (item != null)
                        {
                            foundOnDatasource = true;
                            SetProperty(info.BaseInfo.PropertyName, item, true, FieldStatus.Read);
                        }
                    }

                }



                if (foundOnDatasource) return;

                

                var listType = typeof (PersistableEntityCollection<>);
                var constructedListType = listType.MakeGenericType(specializedType ?? propertyType);

                dynamic collection = Activator.CreateInstance(constructedListType);
                collection.Scope = Scope;


                //var instance = Activator.CreateInstance(propertyType);

                //if (instance != null)
                //dynamic collection = PersistableEntityCollectionFactory.GetNewEntityCollection(instance.GetType().gett);

                var sourceKeyInfo = FieldsInfos.FirstOrDefault(i => i.BaseInfo.PropertyName == relatedEntity.SourceKey);

                if (sourceKeyInfo != null)
                {
                    //FilterClause clause =
                    //    new FilterClause(
                    //        new FieldItemBaseInfo(relatedEntity.RelatedKey, sourceKeyInfo.BaseInfo.PropertyType),
                    //        ConditionOperator.Eq,
                    //        GetProperty(relatedEntity.SourceKey, sourceKeyInfo.BaseInfo.PropertyType));
                    FilterClause clause =
                        new FilterClause(
                            new FieldItemBaseInfo(relatedEntity.RelatedKey, sourceKeyInfo.BaseInfo.PropertyType),
                            ConditionOperator.Eq,
                            GetProperty<object>(relatedEntity.SourceKey));
                    collection.GetItemsFromRepository(clause: clause, persistenceProviderType: Scope.Manager.DefaultRepositoryProviderType);
                    if (collection.Count > 0)
                    {
                        SetProperty(info.BaseInfo.PropertyName, collection[0], true, FieldStatus.Read);
                    }
                    else
                    {
                        SetPropertyStatus(info.BaseInfo.PropertyName, FieldStatus.Read);
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("BasePersistableEntity GetSingleObjectFromRepository error ", ex);
            }
        }

        /// <summary>
        /// Legge un singolo campo da repository usando lo specifico tipo Provider
        /// </summary>
        /// <param name="propertyName">Nome property</param>
        /// <param name="providerType">Tipo provider</param>
        protected virtual void GetSingleFieldFromRepository(string propertyName, PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            var proxy = GetProxy(providerType);
            if (proxy != null)
            {
                proxy.GetSingleField(propertyName);
            }
        }

        /// <summary>
        /// Legge un singolo campo da nodo xml usando lo specifico tipo provider (di classe Xml) 
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <param name="propertyName">Nome property</param>
        /// <param name="providerType">Tipo provider</param>
        protected virtual void GetSingleFieldFromRepository(XElement node, string propertyName, PersistenceProviderType providerType = PersistenceProviderType.Xml)
        {
            var proxy = GetProxy(providerType);
            if (proxy != null)
            {
                proxy.GetSingleField(node, propertyName);
            }
        }

        /// <summary>
        /// Solleva l'evento di cambio stato per un campo
        /// </summary>
        /// <param name="info">Informazioni campo</param>
        protected void OnPropertyStatusChanged(FieldItemInfo info)
        {
            if (PropertyStatusChanged != null)
            {
                PropertyStatusChanged(this, new EventArgs<FieldItemInfo>(info));
            }
        }

        /// <summary>
        /// Restituisce un percorso file temporaneo per la property, basato su un codice.
        /// Se il codice è omesso viene utilizzato un nuovo GUID
        /// </summary>
        /// <param name="propertyName">Nome property</param>
        /// <param name="coreCode">Codice da utilizzare per generare il nome file</param>
        /// <returns>Percorso file temporaneo</returns>
        public virtual string GetTempFilepath(string propertyName, string coreCode)
        {

            string filename = string.Empty;

            string core = (!string.IsNullOrEmpty(coreCode)) ? coreCode : Guid.NewGuid().ToString();

            //if (!Directory.Exists(tempPath))
            //    Directory.CreateDirectory(tempPath);

            string tempPath = Path.Combine(BaseTempFolder, this.GetType().Name);
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);

            var fieldInfo = FieldsInfos.FirstOrDefault(i => i.BaseInfo.PropertyName == propertyName);
            if (fieldInfo != null)
            {
                filename = string.Format("{0}{1}.{2}", fieldInfo.BaseInfo.FilenamePrefix, core, fieldInfo.BaseInfo.FilenameExt);
            }
            else
            {
                filename = core;
            }

            tempPath = Path.Combine(tempPath, filename);

            return tempPath;

        }

        /// <summary>
        /// Restituisce il proxy da usare per l'accesso al repository
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <param name="saveMode">Modalità salvataggio dati</param>
        /// <returns>Proxy richiesto. Null se indisponibile</returns>
        protected virtual PersistableEntityProxy GetProxy(PersistenceProviderType providerType, bool saveMode = false)
        {
            if (providerType != PersistenceProviderType.Xml)
                return Scope.Manager.GetDefaultProxy(this, providerType, saveMode);
            else
            {
                return (new PersistenceScope("").Manager.GetDefaultProxy(this, providerType, saveMode));
            }
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates

        #endregion Delegates  -----------<
    }
}
