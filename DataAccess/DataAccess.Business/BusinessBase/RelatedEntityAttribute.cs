﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Business.Persistence;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Classe attributo per associare a un campo le informazioni di relazione
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class RelatedEntityAttribute : Attribute
    {
        private readonly RelatedEntity _related;

        private string _factoryParameter = null;

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="sourceKey">Nome campo nella classe corrente</param>
        /// <param name="relatedKey">Nome campo nella classe relazionata</param>
        /// <param name="relationType">Tipo relazione</param>
        /// <param name="useDataSource">Utilizzare caching con datasources</param>
        public RelatedEntityAttribute(string sourceKey, string relatedKey, RelationType relationType, bool useDataSource = false)
        {
            _related = new RelatedEntity(sourceKey, relatedKey, relationType, useDataSource);
        }

        /// <summary>
        /// Informazioni di relazione
        /// </summary>
        public RelatedEntity Related
        {
            get { return _related; }
        }

        public string FactoryParameter
        {
            get
            {
                if (_related != null)
                    return _related.FactoryParameter;
                return string.Empty;
            }
            set
            {
                if (_related != null)
                    _related.FactoryParameter = value;
            }
        }
    }


}
