﻿using System;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Attributo associabile a una property per definire le informazioni di persistenza del campo
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PersistableFieldAttribute: Attribute
    {
        private FieldRetrieveMode _defaultRetrieveMode;

        private FieldManagementType _defaultManagementType;

        private string _filenamePrefix = string.Empty;

        private string _filenameExt = string.Empty;


        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="defaultRetrieveMode">Modalità di recupero del valore del campo</param>
        /// <param name="defaultManagementType">Modalità di gestione del valore del campo</param>
        /// <param name="filenamePrefix">Prefisso da utilizzare per l'eventuale nome file</param>
        /// <param name="filenameExt">Estensione da utilizzare per l'eventuale nome file</param>
        public PersistableFieldAttribute(FieldRetrieveMode defaultRetrieveMode = FieldRetrieveMode.Deferred,
            FieldManagementType defaultManagementType = FieldManagementType.Direct,
            string filenamePrefix = "",
            string filenameExt = "")
            : base()
        {
            _defaultRetrieveMode = defaultRetrieveMode;
            _defaultManagementType = defaultManagementType;
            _filenamePrefix = filenamePrefix;
            _filenameExt = filenameExt;

        }

        /// <summary>
        /// Modalità di recupero del valore del campo
        /// </summary>
        public FieldRetrieveMode DefaultRetrieveMode
        {
            get { return _defaultRetrieveMode; }
        }


        /// <summary>
        /// Modalità di gestione del valore del campo
        /// </summary>
        public FieldManagementType DefaultManagementType
        {
            get { return _defaultManagementType; }
        }

        /// <summary>
        /// Prefisso da utilizzare per l'eventuale nome file
        /// </summary>
        public string FilenamePrefix
        {
            get { return _filenamePrefix; }
        }

        /// <summary>
        /// Estensione da utilizzare per l'eventuale nome file
        /// </summary>
        public string FilenameExt
        {
            get { return _filenameExt; }
        }


    }
}
