﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Linq;
using DataAccess.Business.COMMON;
using DataAccess.Business.Persistence;
using System.Collections;
using TraceLoggers;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Collezione tipizzata di entità persistibili
    /// </summary>
    /// <typeparam name="T">Tipo degli elementi in collezione</typeparam>
    public class PersistableEntityCollection<T> : SafeCollection<T> where T : BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration

        /// <summary>
        /// Override dell'evento CollectionChanged
        /// </summary>
        public override event System.Collections.Specialized.NotifyCollectionChangedEventHandler CollectionChanged;

        public PersistenceScope Scope
        {
            get { return _scope; }
            set { _scope = value; }
        }

        #endregion Events declaration  -----------<

        #region >-------------- Overrides

        /// <summary>
        /// Override dello scatenamento evento CollectionChanged
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_suppressNotification) return;

            // Be nice - use BlockReentrancy like MSDN said
            using (BlockReentrancy())
            {
                System.Collections.Specialized.NotifyCollectionChangedEventHandler eventHandler = CollectionChanged;
                if (eventHandler == null)
                    return;

                Delegate[] delegates = eventHandler.GetInvocationList();
                // Walk thru invocation list
                foreach (System.Collections.Specialized.NotifyCollectionChangedEventHandler handler in delegates)
                {
                    DispatcherObject dispatcherObject = handler.Target as DispatcherObject;
                    // If the subscriber is a DispatcherObject and different thread
                    if (dispatcherObject != null && dispatcherObject.CheckAccess() == false)
                    {
                        // Invoke handler in the target dispatcher's thread
                        dispatcherObject.Dispatcher.Invoke(DispatcherPriority.DataBind, handler, this, e);
                    }
                    else // Execute handler as is
                        handler(this, e);
                }
            }
        }

        

        #endregion Overrides -----------<


        #region >-------------- Private Fields

        //private Dictionary<PersistenceProviderType, string> _baseTableDict = new Dictionary<PersistenceProviderType, string>();

        private PersistenceScope _scope;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// 
        /// </summary>
        public PersistableEntityCollection():base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public PersistableEntityCollection(PersistenceScope scope)
            : base()
        {
            _scope = scope;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumerable"></param>
        public PersistableEntityCollection(IEnumerable<T> enumerable)
            : base(enumerable)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="scope"></param>
        public PersistableEntityCollection(IEnumerable<T> enumerable, PersistenceScope scope)
            : base(enumerable)
        {
            _scope = scope;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumerable"></param>
        public PersistableEntityCollection(IOrderedEnumerable<T> enumerable)
            : base(enumerable)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="scope"></param>
        public PersistableEntityCollection(IOrderedEnumerable<T> enumerable, PersistenceScope scope)
            : base(enumerable)
        {
            _scope = scope;
        } 

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        //[Obsolete]
        //public Dictionary<PersistenceProviderType, string> BaseTableDict
        //{
        //    get { return _baseTableDict; }
        //}

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Recupera gli elementi della collezione
        /// </summary>
        /// <param name="persistenceProviderType">Tipo provider</param>
        /// <param name="set">Set campi da recuperare</param>
        /// <param name="clause">Clausola di filtro per l'estrazione contenuti</param>
        /// <param name="sortSet">Criteri di ordinamento per gli elementi estratti</param>
        /// <returns>True se la lettura è andata a buon fine</returns>
        public virtual bool GetItemsFromRepository(PersistenceProviderType persistenceProviderType = PersistenceProviderType.SqlServer, FieldsSet set = null, FilterClause clause = null, FieldsSortSet sortSet = null, bool suppressNotifications = false)
        {
            _suppressNotification = suppressNotifications;

            bool retVal = false;
            try
            {
                var proxy = GetProxy(persistenceProviderType);
                if (proxy != null)
                {
                    retVal = proxy.GetItems(set, clause, sortSet);
                    foreach (var item in Items)
                        item.Scope = Scope;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("GetItemsFromRepository error ", ex);
            }
            finally
            {
                _suppressNotification = false;
            }
            return retVal;
        }

        /// <summary>
        /// Recupera gli elementi della collezione da un nodo xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <param name="persistenceProviderType">Tipo provider (di classe xml)</param>
        /// <param name="set">Set campi da recuperare</param>
        /// <param name="clause">Clausola di filtro per l'estrazione contenuti</param>
        /// <param name="sortSet">Criteri di ordinamento per gli elementi estratti</param>
        /// <returns>True se la lettura è andata a buon fine</returns>
        public virtual bool GetItemsFromRepository(XElement node, PersistenceProviderType persistenceProviderType = PersistenceProviderType.Xml, FieldsSet set = null, FilterClause clause = null, FieldsSortSet sortSet = null)
        {
            var proxy = GetProxy(persistenceProviderType);
            if (proxy != null)
            {
                return proxy.GetItems(node, set,clause,sortSet);
            }
            return false;
        }

        /// <summary>
        /// Recupera gli elementi della collezione da un file xml
        /// </summary>
        /// <param name="filepath">Percorso file xml</param>
        /// <param name="persistenceProviderType">Tipo provider (di classe xml)</param>
        /// <param name="set">Set campi da recuperare</param>
        /// <param name="clause">Clausola di filtro per l'estrazione contenuti</param>
        /// <param name="sortSet">Criteri di ordinamento per gli elementi estratti</param>
        /// <returns>True se la lettura è andata a buon fine</returns>
        public virtual bool GetItemsFromRepository(string filepath, PersistenceProviderType persistenceProviderType = PersistenceProviderType.Xml, FieldsSet set = null, FilterClause clause = null, FieldsSortSet sortSet = null)
        {
            var proxy = GetProxy(persistenceProviderType);
            if (proxy != null)
            {
                return proxy.GetItems(filepath, set, clause, sortSet);
            }
            return false;
        }

        /// <summary>
        /// Salva gli elementi della collezione su un nodo xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <param name="persistenceProviderType">Tipo provider (di classe xml)</param>
        /// <returns>True se il salvataggio è andato a buon fine</returns>
        public virtual bool SaveItemsToRepository(out XElement node, PersistenceProviderType persistenceProviderType = PersistenceProviderType.Xml)
        {
            node = null;
            var proxy = GetProxy(persistenceProviderType);
            if (proxy != null)
            {
                return proxy.SaveItems(out node);
            }
            return false;
        }

        /// <summary>
        /// Salva gli elementi della collezione su un file xml
        /// </summary>
        /// <param name="filepath">Percorso file xml</param>
        /// <param name="overwriteIfExistent">Sovrascrittura file esistente</param>
        /// <param name="persistenceProviderType">Tipo provider (di classe xml)</param>
        /// <returns>True se il salvataggio è andato a buon fine</returns>
        public virtual bool SaveItemsToRepository(string filepath, bool overwriteIfExistent = true, PersistenceProviderType persistenceProviderType = PersistenceProviderType.Xml)
        {
            var proxy = GetProxy(persistenceProviderType);
            if (proxy != null)
            {
                return proxy.SaveItems(filepath, overwriteIfExistent);
            }
            return false;
        }

        /// <summary>
        /// Salva gli elementi della collezione
        /// </summary>
        /// <param name="persistenceProviderType">Tipo provider</param>
        /// <returns>True se il salvataggio è andato a buon fine</returns>
        public virtual bool SaveItemsToRepository(PersistenceProviderType persistenceProviderType = PersistenceProviderType.SqlServer)
        {
            bool retval = true;

            try
            {
                
                // Deleted first
                foreach (var item in _removedItems)
                {
                    if (Items.All(i => i.UniqueId != item.UniqueId))
                    {
                        if (!item.IsNewItem)
                        {

                            if (!item.DeleteFromRepository(persistenceProviderType))
                            {
                                retval = false;
                                break;
                            }
                        }
                    }
                }
                if (retval)
                {
                    //var modifiedItems = Items.Where(i => i.Status != EntityStatus.Unchanged && i.Status != EntityStatus.Unknown);
                    foreach (var item in Items)
                    {
                        if (!item.SaveToRepository(persistenceProviderType))
                        {
                            retval = false;
                            break;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SaveItemsToRepository error ", ex);
            }
            return retval;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errors"></param>
        /// <param name="persistenceProviderType"></param>
        /// <returns></returns>
        public virtual bool SaveItemsToRepository(out string errors, PersistenceProviderType persistenceProviderType = PersistenceProviderType.SqlServer)
        {
            errors = string.Empty;

            bool retval = true;

            try
            {
                
                // Deleted first
                foreach (var item in _removedItems)
                {
                    if (Items.All(i => i.UniqueId != item.UniqueId))
                    {
                        if (!item.IsNewItem)
                        {

                            if (!item.DeleteFromRepository(persistenceProviderType))
                            {
                                retval = false;
                                break;
                            }
                        }
                    }
                }
                if (retval)
                {
                    //var modifiedItems = Items.Where(i => i.Status != EntityStatus.Unchanged && i.Status != EntityStatus.Unknown);
                    foreach (var item in Items)
                    {
                        if (!item.SaveToRepository(persistenceProviderType))
                        {
                            retval = false;
                            break;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SaveItemsToRepository error ", ex);
            }
            return retval;
        }



        //TODO: Delete con whereClause su proxyCollection
        /// <summary>
        /// Elimina gli elementi della collezione
        /// </summary>
        /// <param name="filterclause">Filtro di eliminazione</param>
        /// <param name="persistenceProviderType">Tipo provider</param>
        /// <returns>True se il salvataggio è andato a buon fine</returns>
        public virtual bool DeleteItemsFromRepository(FilterClause filterclause, PersistenceProviderType persistenceProviderType = PersistenceProviderType.SqlServer)
        {
            bool retVal = true;

            try
            {
                var proxy = GetProxy(persistenceProviderType);
                if (proxy != null)
                {
                    retVal = proxy.DeleteItems(filterclause);
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DeleteItemsFromRepository error ", ex);
            }

            return retVal;
        }



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        /// <summary>
        /// Restituisce il proxy per le operazioni di persistence
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <param name="saveMode">Modalità salvataggio</param>
        /// <returns>Proxy di persistence</returns>
        protected virtual PersistableCollectionProxy<T> GetProxy(PersistenceProviderType providerType, bool saveMode = false) 
        {
            if (providerType != PersistenceProviderType.Xml)
                return Scope.Manager.GetDefaultProxy(this, providerType);
            else
                return new PersistenceScope("").Manager.GetDefaultProxy(this, providerType);
        }
    }

    /// <summary>
    /// Generatore di nuove collezioni
    /// </summary>
    public static class PersistableEntityCollectionFactory
    {
        /// <summary>
        ///// Genera una nuova collezione tipizzata
        ///// </summary>
        ///// <typeparam name="T">Tipizzazione</typeparam>
        ///// <param name="type">Tipo elementi</param>
        ///// <returns>Collezione tipizzata</returns>
        //public static PersistableEntityCollection<T> GetNewEntityCollection<T>(T type) where T : BasePersistableEntity
        //{
        //    return new PersistableEntityCollection<T>();
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        public static PersistableEntityCollection<T> GetNewEntityCollection<T>(T type, PersistenceScope scope) where T : BasePersistableEntity
        {
            return new PersistableEntityCollection<T>(scope);
        }

        //public static object GetNewCollection<T>(T type) where T : Type
        //{
        //    if (type.IsSubclassOf(typeof(BasePersistableEntity)))
        //        return GetNewEntityCollection(type);
        //}
    }
}
