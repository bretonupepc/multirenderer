﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DataAccess.Business.Persistence;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Classe statica di helper per gestione informazioni dei campi delle entità
    /// </summary>
    public static class PersistableEntityBaseInfos
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private static Dictionary<Type, List<FieldItemBaseInfo>> _baseInfoDict = null;

        private static object _staticLockObj = new object();

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Private Properties

        private static Dictionary<Type, List<FieldItemBaseInfo>> BaseInfoDict
        {
            get
            {
                lock (_staticLockObj)
                {
                    if (_baseInfoDict == null)
                    {
                        _baseInfoDict = new Dictionary<Type, List<FieldItemBaseInfo>>();
                    }
                }
                return _baseInfoDict;
            }
        }

        #endregion Private Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Estrae il set di informazioni di base dei campi per una classe di un tipo specifico
        /// </summary>
        /// <param name="classType">Tipo della classe</param>
        /// <returns>Set informazioni di base</returns>
        public static List<FieldItemBaseInfo> GetBaseInfos(Type classType)
        {
            lock (_staticLockObj)
            {
                if (!BaseInfoDict.ContainsKey(classType))
                {
                    AddClassInfos(classType);
                }
            }

            return (BaseInfoDict.ContainsKey(classType)
                ? BaseInfoDict[classType]
                : null);
        }

        /// <summary>
        /// Estrae il nome tabella database di riferimento per una data classe e tipo provider
        /// </summary>
        /// <param name="type">Tipo della classe</param>
        /// <param name="persistenceProviderType">Tipo provider</param>
        /// <returns>Nome tabella database</returns>
        public static string GetMainDbTableName(Type type, PersistenceProviderType persistenceProviderType)
        {
            string tableName = string.Empty;

            MemberInfo info = type;
            var attribute = info.GetCustomAttributes(true).Where(a => a is MainDbTableAttribute)
                .FirstOrDefault(a => ((MainDbTableAttribute)a).ProviderType == persistenceProviderType);
            if (attribute != null)
                tableName = ((MainDbTableAttribute)attribute).MainDbTable;

            return tableName;
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private static void AddClassInfos(Type classType)
        {
            var itemBaseInfos = new List<FieldItemBaseInfo>();

            var persistableProperties = classType.GetProperties().Where(
                prop => Attribute.IsDefined((MemberInfo) prop, typeof(PersistableFieldAttribute))).ToList();


            foreach (var prop in persistableProperties)
            {
                PersistableFieldAttribute attribute = (PersistableFieldAttribute)prop.GetCustomAttributes(typeof (PersistableFieldAttribute), false).FirstOrDefault();
                FieldItemBaseInfo info = null;
                if (attribute != null)
                {
                    info = new FieldItemBaseInfo(prop.Name, prop.PropertyType, attribute.DefaultRetrieveMode, attribute.DefaultManagementType, attribute.FilenamePrefix, attribute.FilenameExt);
                }
                else
                {
                    info = new FieldItemBaseInfo(prop.Name, prop.PropertyType);
                }

                RelatedEntityAttribute relationAttribute = (RelatedEntityAttribute)prop.GetCustomAttributes(typeof(RelatedEntityAttribute), false).FirstOrDefault();
                if (relationAttribute != null)
                {
                    info.Relation = relationAttribute.Related;
                }

                itemBaseInfos.Add(info);
            }

            BaseInfoDict.Add(classType, itemBaseInfos);

        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
