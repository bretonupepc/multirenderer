﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Tipo relazione campo
    /// </summary>
    public enum RelationType
    {
        /// <summary>
        /// Relazione Uno-a-Uno
        /// </summary>
        OneToOne,

        /// <summary>
        /// Relazione Uno-a-Molti
        /// </summary>
        OneToMany
    }

    /// <summary>
    /// Informazioni di relazione per un campo
    /// </summary>
    public class RelatedEntity
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private readonly string _sourceKey = string.Empty;

        private readonly string _relatedKey = string.Empty;

        private readonly RelationType _relation;

        private bool _useDataSource = false;

        private string _factoryParameter = string.Empty;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="sourceKey">Nome campo nella classe corrente</param>
        /// <param name="relatedKey">Nome campo nella classe relazionata</param>
        /// <param name="relationType">Tipo relazione</param>
        /// <param name="useDataSource">Utilizzare caching con datasources</param>
        public RelatedEntity(string sourceKey, string relatedKey, RelationType relationType, bool useDataSource = false)
        {
            _sourceKey = sourceKey;

            _relatedKey = relatedKey;

            _relation = relationType;

            _useDataSource = useDataSource;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Nome campo nella classe corrente
        /// </summary>
        public string SourceKey
        {
            get { return _sourceKey; }
        }

        /// <summary>
        /// Nome campo nella classe relazionata
        /// </summary>
        public string RelatedKey
        {
            get { return _relatedKey; }
        }

        /// <summary>
        /// Informazioni di relazione
        /// </summary>
        public RelationType Relation
        {
            get { return _relation; }
        }

        /// <summary>
        /// Utilizzare caching con datasources
        /// </summary>
        public bool UseDataSource
        {
            get { return _useDataSource; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FactoryParameter
        {
            get { return _factoryParameter; }
            set { _factoryParameter = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
