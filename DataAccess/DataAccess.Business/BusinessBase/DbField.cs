﻿using System;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Classe di definizione specifiche di un campo connesso a database
    /// </summary>
    public class DbField
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private readonly string _name;

        private readonly bool _isRequired;

        private readonly string _foreignKeyTable = string.Empty;

        private readonly string _foreignKeyField = string.Empty;

        private readonly bool _isBlobField = false;

        private bool _isXmlAttribute = false;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="name">Nome colonna su tabella database</param>
        /// <param name="isRequired">Campo obbligatorio</param>
        /// <param name="foreignKeyTable">Nome tabella di relazione</param>
        /// <param name="foreigKeyField">Nome colonna su tabella di relazione</param>
        /// <param name="isBlobField">Campo blob</param>
        public DbField(string name, bool isRequired = false, string foreignKeyTable = "", 
            string foreigKeyField = "", bool isBlobField = false, bool isXmlAttribute = false)
        {
            _name = name;
            _isRequired = isRequired;
            _foreignKeyTable = foreignKeyTable;
            _foreignKeyField = foreigKeyField;
            _isBlobField = isBlobField;
            _isXmlAttribute = IsXmlAttribute;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Nome colonna su tabella database
        /// </summary>
        public string Name
        {
            get { return _name; }
        }

        /// <summary>
        /// Campo obbligatorio nel database
        /// </summary>
        public bool IsRequired
        {
            get { return _isRequired; }
        }

        /// <summary>
        /// Nome tabella di relazione
        /// </summary>
        public string ForeignKeyTable
        {
            get { return _foreignKeyTable; }
        }

        /// <summary>
        /// Nome colonna su tabella di relazione
        /// </summary>
        public string ForeignKeyField
        {
            get { return _foreignKeyField; }
        }

        /// <summary>
        /// Campo blob nel database
        /// </summary>
        public bool IsBlobField
        {
            get { return _isBlobField; }
        }

        /// <summary>
        /// Campo gestito come Attribute anzichè come Node
        /// </summary>
        public bool IsXmlAttribute
        {
            get { return _isXmlAttribute; }
            set { _isXmlAttribute = value; }
        }

        /// <summary>
        /// Costituisce chiave esterna
        /// </summary>
        [Obsolete]
        public bool IsForeignKey
        {
            get { return !string.IsNullOrEmpty(_foreignKeyTable); }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
