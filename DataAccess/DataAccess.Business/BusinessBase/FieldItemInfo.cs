﻿using System;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Informazioni di base associate ad un campo da trattare come "persistable"
    /// </summary>
    public class FieldItemInfo
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private FieldItemBaseInfo _baseInfo;

        private FieldRetrieveMode _retrieveMode;

        private FieldStatus _status = FieldStatus.Unknown;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="baseInfo">Informazioni di base</param>
        /// <param name="retrieveMode">Modalità di recupero del valore del campo</param>
        /// <param name="status">Stato del campo</param>
        public FieldItemInfo(FieldItemBaseInfo baseInfo, 
            FieldRetrieveMode retrieveMode,
            FieldStatus status = FieldStatus.Unknown)
        {
            _baseInfo = baseInfo;
            RetrieveMode = retrieveMode;
            Status = status;
        }

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="baseInfo">Informazioni di base</param>
        /// <param name="status">Stato del campo</param>
        public FieldItemInfo(FieldItemBaseInfo baseInfo,
            FieldStatus status = FieldStatus.Unknown)
        {
            _baseInfo = baseInfo;
            RetrieveMode = baseInfo.DefaultRetrieveMode;
            Status = status;
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Set informazioni di base
        /// </summary>
        public FieldItemBaseInfo BaseInfo
        {
            get { return _baseInfo; }
        }

        /// <summary>
        /// Modalità di recupero del valore del campo
        /// </summary>
        public FieldRetrieveMode RetrieveMode
        {
            get { return _retrieveMode; }
            set { _retrieveMode = value; }
        }

        /// <summary>
        /// Stato
        /// </summary>
        public FieldStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
