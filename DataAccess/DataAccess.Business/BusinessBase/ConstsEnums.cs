﻿using System;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Modalità lettura campo da repository
    /// </summary>
    public enum FieldRetrieveMode
    {
        /// <summary>
        /// Lettura immediata
        /// </summary>
        Immediate,

        /// <summary>
        /// Lettura differita (su richiesta) sincrona
        /// </summary>
        Deferred,

        /// <summary>
        /// Lettura differita (su richiesta) asincrona
        /// </summary>
        DeferredAsync
    }

    /// <summary>
    /// Stato del campo 
    /// </summary>
    [Flags]
    public enum FieldStatus
    {
        /// <summary>
        /// Non definito (non ancora acquisito)
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Letto da repository e invariato
        /// </summary>
        Read = 1,

        /// <summary>
        /// Impostato esplicitamente
        /// </summary>
        Set = 2,

        /// <summary>
        /// Lettura in corso (asincrona) da repository
        /// </summary>
        Reading = 4
    }

    /// <summary>
    /// Tipo gestione del campo tramite repository
    /// </summary>
    public enum FieldManagementType
    {
        /// <summary>
        /// Diretta (normale)
        /// </summary>
        Direct,

        /// <summary>
        /// Indiretta (campo relazionato)
        /// </summary>
        Indirect,

        /// <summary>
        /// Tramite file su disco
        /// </summary>
        ToFilepath
    }



    class ConstsEnums
    {
    }
}
