﻿using System;
using DataAccess.Business.Persistence;

namespace DataAccess.Business.BusinessBase
{
    /// <summary>
    /// Classe attributo per associare a un campo entità le informazioni di connessione 
    /// per uno specifico tipo provider (di classe Database) 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class DbFieldAttribute : Attribute
    {
        private readonly PersistenceProviderType _providerType;

        private readonly DbField _field;

        

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <param name="fieldName">Nome colonna su tabella database</param>
        /// <param name="isRequired">Obbligatorio</param>
        /// <param name="foreignKeyTable">Nome tabella di relazione</param>
        /// <param name="foreigKeyFieldName">Nome colonna di relazione</param>
        /// <param name="isBlobField">Campo blob</param>
        /// <param name="isXmlAttribute">Gestire come attributo xml</param>
        public DbFieldAttribute(PersistenceProviderType providerType,
                    string fieldName, 
                    bool isRequired = false, 
                    string foreignKeyTable = "", 
                    string foreigKeyFieldName = "", 
                    bool isBlobField = false
                    )
        {
            _providerType = providerType;
            _field = new DbField(fieldName, isRequired, foreignKeyTable, foreigKeyFieldName, isBlobField);
        }

        /// <summary>
        /// Xml attribute
        /// </summary>
        public bool IsXmlAttribute
        {
            get
            {
                if (_field != null)
                {
                    return _field.IsXmlAttribute;
                }
                return false;
            }
            set
            {
                if (_field != null)
                {
                    _field.IsXmlAttribute = value;
                }
            }
        }

        /// <summary>
        /// Tipo provider
        /// </summary>
        public PersistenceProviderType ProviderType
        {
            get { return _providerType; }
        }

        /// <summary>
        /// Specifiche del campo connesso a database
        /// </summary>
        public DbField Field
        {
            get { return _field; }
        }
    }

}
