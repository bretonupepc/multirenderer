﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using Breton.DbAccess;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.ARTICLES;
using BrSm.Business.ENTITIES.ARTICLES.Persistence.Xml;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.ENTITIES.ARTICLES.SLABS.Persistence.SqlServer;
using BrSm.Business.ENTITIES.ARTICLES.SLABS.Persistence.Xml;
using BrSm.Business.ENTITIES.MATERIALS;
using BrSm.Business.ENTITIES.MATERIALS.Persistence.SqlServer;

namespace BrSm.Business.Persistence.Xml
{
    public class XmlProvider: IPersistenceProvider
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _mainFolder = string.Empty;

        private string _currentUserName = string.Empty;

        #region Workers

        private readonly SynchronizationContext _synchroContext;

        private bool _canStartWorkers = true;

        private BlockingList<Thread> _workers = new BlockingList<Thread>();

        #endregion Workers


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public XmlProvider()
        {
            _synchroContext = SynchronizationContext.Current;

        }

        public XmlProvider(string mainFolder): this()
        {
            MainFolder = mainFolder;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        public string CurrentUserName
        {
            get { return _currentUserName; }
            set { _currentUserName = value; }
        }

        public SynchronizationContext SynchroContext
        {
            get { return _synchroContext; }
        }

        public bool CanStartWorkers
        {
            get { return _canStartWorkers; }
            set { _canStartWorkers = value; }
        }

        public BlockingList<Thread> Workers
        {
            get { return _workers; }
            set { _workers = value; }
        }

        public string MainFolder
        {
            get { return _mainFolder; }
            set { _mainFolder = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public bool WriteXmlFile(string filepath, XElement rootNode)
        {
            bool retVal = false;

            return retVal;
        }

        public bool ReadXmlFile(string filepath, out XElement rootNode)
        {
            bool retVal = false;
            rootNode = null;

            return retVal;
        }

        public void StopWorkers()
        {
            _canStartWorkers = false;

            while (_workers.Count > 0)
            {
                for (int i = _workers.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        Thread thread = _workers[i];
                        if (thread != null && thread.IsAlive)
                        {
                            thread.Abort();
                        }
                        else
                        {
                            _workers.RemoveAt(i);
                        }

                    }
                    catch { }

                }
                Thread.Sleep(10);
            }
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private static object IsDbNull(object value, object nullValueReplacement)
        {
            if (value == DBNull.Value)
                return nullValueReplacement;

            return value;
        }

        private static string NotNullString(object dataValue)
        {
            if (dataValue.Equals(DBNull.Value))
            {
                return string.Empty;
            }
            else
            {
                return dataValue.ToString();
            }
        }

        public static string DateSqlFormat(DateTime date)
        {
            return date.ToString("yyyyMMdd HH:mm:ss");
        }

        public static string FixSqlString(string inputStr)
        {
            return inputStr.Replace("\'", "\'\'");
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        //private 



        #region IPersistenceProvider Members

        public PersistableCollectionProxy<T> GetCollectionProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity
        {
            XmlCollectionProxy<T> proxy = null;
            switch (typeof(T).Name)
            {
                case "MaterialEntity":
                    //proxy = new SqlMaterialCollectionProxy(collection as MaterialCollection, BretonInterface.Clone()) as SqlCollectionProxy<T>;
                    break;

                case "ArticleEntity":
                    proxy = new XmlArticleCollectionProxy(collection as ArticleCollection) as XmlCollectionProxy<T>;
                    break;

                case "SlabEntity":
                    proxy = new XmlSlabCollectionProxy(collection as SlabCollection) as XmlCollectionProxy<T>;
                    break;

                case "SlabFaceEntity":
                    proxy = new XmlSlabFaceCollectionProxy(collection as SlabFaceCollection) as XmlCollectionProxy<T>;
                    break;

                default:
                    proxy = new XmlCollectionProxy<T>(collection);
                    break;
            }
            return proxy;

        }

        public PersistableEntityProxy GetEntityProxy<T>(T entity) where T : BasePersistableEntity
        {
            PersistableEntityProxy proxy = null;
            switch (entity.GetType().Name)
            {
                case "MaterialEntity":
                    //return new SqlMaterialProxy(entity as MaterialEntity, this);
                    break;

                case "MaterialClassEntity":

                    break;

                case "ArticleEntity":
                    return new XmlArticleProxy(entity as ArticleEntity, this);
                    break;

                case "SlabEntity":
                    return new XmlSlabProxy(entity as SlabEntity, this);
                    break;

                case "SlabFaceEntity":
                    return new XmlSlabFaceProxy(entity as SlabFaceEntity, this);
                    break;


                default:
                    return new XmlEntityProxy(entity, this);
                    break;
            }

            return proxy;
        }

        #endregion
    }

}
