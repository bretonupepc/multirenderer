﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using Breton.DbAccess;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.Persistence.Xml
{
    public class XmlEntityProxy:PersistableEntityProxy
    {

        #region >-------------- Constants and Enums

        protected const string BASE_SELECT = " SELECT ";
        protected const string BASE_UPDATE = " UPDATE ";
        protected const string BASE_WHERE = " WHERE ";
        protected const string BASE_INSERT = " INSERT INTO ";


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private XmlProvider _provider = new XmlProvider();

        protected string _mainTableName = string.Empty;

        protected string _rootNodeName = string.Empty;


        protected Dictionary<string, string> _fieldsMap;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public XmlEntityProxy(BasePersistableEntity entity, XmlProvider provider) : base(entity)
        {
            _provider = provider;
            _rootNodeName = entity.GetType().FullName;
        }

        public XmlEntityProxy(BasePersistableEntity entity, XmlProvider provider, string mainTableName)
            : this(entity, provider)
        {
            _mainTableName = mainTableName;
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public virtual string UniqueIdFieldName
        {
            get
            {
                return "Id";
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties

        protected internal Dictionary<string, string> FieldsMap
        {
            get
            {
                var map = XmlFieldsMaps.GetEntityFieldsMap(Entity.GetType());
                if (map == null || map.Count == 0)
                {
                    map = XmlFieldsMaps.GetEntityFieldsMap(Entity.GetType().BaseType);
                }
                return map;
            }
        }

        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database
        /// </summary>
        /// <param name="fieldName">Nome campo</param>
        public override void GetSingleField(string fieldName)
        {
            FieldRetrieveMode mode = FieldRetrieveMode.Deferred;

            FieldItemInfo info = Entity.GetFieldItemInfo(fieldName);
            if (info != null)
            {
                mode = info.RetrieveMode;
            }

            switch (mode)
            {
                case FieldRetrieveMode.DeferredAsync:
                    AsyncGetSingleField(fieldName);
                    break;

                default:
                    SyncGetSingleField(fieldName);
                    break;
            }

            return;

        }

        /// <summary>
        /// Salva l'oggetto su gerarchia xml
        /// </summary>
        /// <returns></returns>
        public override bool Save(out XElement rootNode)
        {
            bool retVal = false;

            rootNode = null;

            if (Entity.FieldsInfos != null && Entity.FieldsInfos.Count > 0)
            {
                try
                {
                    rootNode = new XElement(Entity.GetType().FullName);

                    XElement dummyNode = null;
                    XElement childNode = null;

                    foreach (var info in Entity.FieldsInfos)
                    {
                        if (info.Status == FieldStatus.Read || info.Status == FieldStatus.Set)
                        {
                            childNode = new XElement(info.BaseInfo.PropertyName);
                            rootNode.Add(childNode);
                            //if (info.BaseInfo.PropertyType.IsAssignableFrom(typeof (BasePersistableEntity)))
                            if (info.BaseInfo.PropertyType.IsSubclassOf(typeof (BasePersistableEntity)))
                            {
                                var dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType);

                                var fieldValue = Entity.GetProperty(info.BaseInfo.PropertyName, dummyEntity);
                                BasePersistableEntity ent = fieldValue as BasePersistableEntity;
                                if (ent != null && ent.SaveToRepository(out dummyNode))
                                {
                                    //childNode.Add(dummyNode.Descendants());
                                    childNode.Add(dummyNode.Elements());
                                }
                            }
                            else if (info.BaseInfo.PropertyType.GetGenericArguments().Any())
                            {
                                Type arg = info.BaseInfo.PropertyType.BaseType.GetGenericArguments().First();
                                {
                                    if (arg.IsSubclassOf(typeof(BasePersistableEntity)))
                                    {
                                        var dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType);


                                        dynamic fieldValue = Entity.GetProperty(info.BaseInfo.PropertyName, dummyEntity);
                                        if (fieldValue != null)
                                        {
                                            foreach (dynamic item in fieldValue)
                                            {
                                                if (item.SaveToRepository(out dummyNode))
                                                {
                                                    childNode.Add(dummyNode);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (info.BaseInfo.PropertyType.BaseType != null
                                && info.BaseInfo.PropertyType.BaseType.GetGenericArguments().Any())
                            {
                                Type arg = info.BaseInfo.PropertyType.BaseType.GetGenericArguments().First();
                                {
                                    if (arg.IsSubclassOf(typeof (BasePersistableEntity)))
                                    {
                                        var dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType);

                                        dynamic fieldValue = Entity.GetProperty(info.BaseInfo.PropertyName, dummyEntity) ;
                                        if (fieldValue != null)
                                        {
                                            foreach (dynamic item in fieldValue)
                                            {
                                                if (item.SaveToRepository(out dummyNode))
                                                {
                                                    childNode.Add(dummyNode);
                                                }
                                            }
                                        }
                                        //fieldValue.SaveToRepository(out dummyNode))}
                                        //{
                                        //    childNode.Add(dummyNode.Elements());
                                        //}

                                        

                                        //var method = info.BaseInfo.PropertyType.GetMethod("SaveToRepository",
                                        //        new Type[] { typeof(XElement) , typeof(PersistenceProviderType)});
                                        //if (method != null)
                                        //{
                                        //    // It's .NET 4.5

                                        //    //if ((bool) method.Invoke(null, new object[] {dummyNode}))
                                        //    //{
                                        //    //    childNode.Add(dummyNode.Elements());
                                        //    //}
                                        //}



                                        ////var ent = fieldValue as dummyEntity.GetType();
                                        //if (fieldValue != null && fieldValue.SaveToRepository(out dummyNode))
                                        //{
                                        //    //childNode.Add(dummyNode.Descendants());
                                        //    childNode.Add(dummyNode.Elements());
                                        //}
        
                                    }
                                }
                            }
                            ////if (info.BaseInfo.PropertyType.IsSubclassOf(typeof(PersistableEntityCollection<>)))
                            //{
                            //    var dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType);

                            //    var fieldValue = Entity.GetProperty(info.BaseInfo.PropertyName, dummyEntity);
                            //    BasePersistableEntity ent = fieldValue as BasePersistableEntity;
                            //    if (ent != null && ent.SaveToRepository(out dummyNode))
                            //    {
                            //        //childNode.Add(dummyNode.Descendants());
                            //        childNode.Add(dummyNode.Elements());
                            //    }
                            //}

                            else
                            {
                                string value = SetXmlValue(info);
                                if (!string.IsNullOrEmpty(value))
                                    childNode.Value = value;
                            }


                        }
                    }

                    retVal = childNode != null;
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("XmlEntityProxy Save error ", ex);
                }
            }

            return retVal;

        }

        public override bool Save(string filepath, bool overwriteIfExistent = true)
        {
            bool retVal = false;

            try
            {
                if (File.Exists(filepath) && !overwriteIfExistent)
                {
                    Trace.WriteLine("XmlEntityProxy Save(filepath) error: File exists.");
                    return retVal;
                }

                XElement rootNode = null;
                if (Save(out rootNode))
                {
                    XDocument xDocument = new XDocument();

                    xDocument.AddFirst(rootNode);

                    XComment versionComment = null;
                    XmlHelper.BuildAssembliesInfoNode(out versionComment);
                    if (versionComment != null)
                    {
                        xDocument.AddFirst(versionComment);
                    }

                    xDocument.Save(filepath);

                    retVal = true;
                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlEntityProxy Save(filepath) error ", ex);
                retVal = false;
            }
            return retVal;
        }

        /// <summary>
        /// Rilegge l'oggetto da database
        /// </summary>
        /// <returns></returns>
        public override bool Refresh()
        {
            bool retVal = false;

            string sqlCommand;

            retVal = BuildRefreshingSelectCommand(out sqlCommand);

            if (retVal)
            {
                //bool transaction = !BretonInterface.TransactionActive();


                int exception_step = 0;
                OleDbDataReader ord = null;


                try
                {
                    //if (transaction) BretonInterface.BeginTransaction();

                    //retVal = (BretonInterface.Requery(sqlCommand, out ord)) && ord.HasRows;
                    

                    
                    if ( retVal )
                    {
                        ord.Read();

                        foreach (FieldItemInfo field in Entity.FieldsInfos)
                        {
                            if ((field.Status == FieldStatus.Set || field.Status == FieldStatus.Read) 
                                && (FieldsMap.ContainsKey(field.BaseInfo.PropertyName)))
                            {
                                object fieldValue = ord[FieldsMap[field.BaseInfo.PropertyName]];

                                if (fieldValue != DBNull.Value)
                                {
                                    Entity.SetProperty(field.BaseInfo.PropertyName, fieldValue, true, FieldStatus.Read);

                                }

                            }
                        }


                    }

                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("EntityProxy Refresh error ", ex);
                    retVal = false;
                    ord = null;

                }

                finally
                {
                    //if (transaction) BretonInterface.EndTransaction(retVal);
                }

            }



            return retVal;

        }

        public override  bool GetFields(XElement node)
        {
            bool retVal = false;

            if (node == null || node.IsEmpty)
                return false;

            try
            {
                Entity.BuildCompleteFieldsInfos();
                //var descendants = node.Descendants();
                //foreach (var fieldNode in descendants)
                var elements = node.Elements();
                foreach (var fieldNode in elements)
                {
                    var info = Entity.FieldsInfos.FirstOrDefault(i => i.BaseInfo.PropertyName == fieldNode.Name);
                    if (info != null && info.RetrieveMode != FieldRetrieveMode.DeferredAsync)
                    {
                            if (info.BaseInfo.PropertyType.IsSubclassOf(typeof (BasePersistableEntity)))
                            {
                                var dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType) as BasePersistableEntity;
                                //var proxy = _provider.GetEntityProxy(dummyEntity) as XmlEntityProxy;
                                //if (dummyEntity != null && dummyEntity.GetFields(fieldNode))
                                if (dummyEntity != null && dummyEntity.GetFromRepository(fieldNode))
                                {
                                    Entity.SetProperty(info.BaseInfo.PropertyName, dummyEntity, true, FieldStatus.Read);
                                }
                            }



                            else if (info.BaseInfo.PropertyType.GetGenericArguments().Any())
                            {
                                Type arg = info.BaseInfo.PropertyType.GetGenericArguments().First();
                                if (arg.IsSubclassOf(typeof (BasePersistableEntity)))
                                {
                                    dynamic dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType);

                                    if (dummyEntity != null && dummyEntity.GetItemsFromRepository(fieldNode))
                                    {
                                        Entity.SetProperty(info.BaseInfo.PropertyName, dummyEntity, true,
                                            FieldStatus.Read);
                                    }
                                }
                                    //Type arg = info.BaseInfo.PropertyType.GetGenericArguments().First();
                                //{
                                //    if (arg.IsSubclassOf(typeof(BasePersistableEntity)))
                                //    {
                                //        dynamic dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType);

                                //        foreach (XElement itemNode in fieldNode.Elements())
                                //        {
                                //            var dummyChildEntity =
                                //                Activator.CreateInstance(arg) as BasePersistableEntity;
                                //            if (dummyChildEntity != null && dummyChildEntity.GetFromRepository(itemNode))
                                //            {
                                //                dummyEntity.Add(dummyChildEntity);
                                //            }
                                //        }

                                //        Entity.SetProperty(info.BaseInfo.PropertyName, dummyEntity, true, FieldStatus.Read);
                                //    }
                                //}
                            }
                            else if (info.BaseInfo.PropertyType.BaseType != null
                                && info.BaseInfo.PropertyType.BaseType.GetGenericArguments().Any())
                            {
                                Type arg = info.BaseInfo.PropertyType.BaseType.GetGenericArguments().First();
                                {
                                    if (arg.IsSubclassOf(typeof(BasePersistableEntity)))
                                    {
                                        if (arg.IsSubclassOf(typeof(BasePersistableEntity)))
                                        {
                                            dynamic dummyEntity = Activator.CreateInstance(info.BaseInfo.PropertyType);

                                            if (dummyEntity != null && dummyEntity.GetItemsFromRepository(fieldNode))
                                            {
                                                Entity.SetProperty(info.BaseInfo.PropertyName, dummyEntity, true,
                                                    FieldStatus.Read);
                                            }
                                        }

                                    }
                                }
                            }




                            else
                            {
                                object nodeValue = GetFieldValueFromXmlString(info, fieldNode.Value);
                                Entity.SetProperty(info.BaseInfo.PropertyName, nodeValue, true, FieldStatus.Read);
                            }

                    }
                }

                retVal = true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlEntityProxy:GetFields error ", ex);
                retVal = false;
            }

            return retVal;
        }

        public override bool GetFields(string filepath)
        {
            bool retVal = false;

            if (!File.Exists(filepath))
            {
                TraceLog.WriteLine("XmlEntityProxy: GetFields(filepath) error: file does not exists.");
                return retVal;
            }

            try
            {
                XDocument xDocument = XDocument.Load(filepath);

                retVal = GetFields(xDocument.Root);
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlEntityProxy:GetFields(filepath) error ", ex);
                retVal = false;
            }

            return retVal;

        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// Costruisce la stringa SQL per il refresh dell'oggetto
        /// </summary>
        /// <param name="sqlCommand">Stringa SQL prodotta</param>
        /// <returns></returns>
        protected virtual bool BuildRefreshingSelectCommand(out string sqlCommand)
        {
            string selectStr = BASE_SELECT;

            bool isValid = false;

            string fieldsPart = string.Empty;

            if (Entity.FieldsInfos != null && Entity.FieldsInfos.Count > 0)
            {

                foreach (FieldItemInfo field in Entity.FieldsInfos)
                {
                    if ((field.Status == FieldStatus.Set || field.Status == FieldStatus.Read)
                        && (FieldsMap.ContainsKey(field.BaseInfo.PropertyName)))
                    {
                        fieldsPart += FieldsMap[field.BaseInfo.PropertyName] + ", ";

                    }
                }

                isValid = !string.IsNullOrEmpty(fieldsPart);
                fieldsPart = fieldsPart.Substring(0, fieldsPart.Length - 2) + " ";
            }

            if (isValid)
            {
                selectStr += fieldsPart +
                             " FROM [" + _mainTableName + "] " +
                             " WHERE " +
                             FieldsMap[UniqueIdFieldName] + " = " + Entity.UniqueId;

            }

            sqlCommand = selectStr;
            return isValid ;
        }

        /// <summary>
        /// Costruisce la stringa SQL per il Save dell'oggetto
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected virtual bool BuildSaveCommand(out string sqlCommand)
        {
            sqlCommand = string.Empty;

            bool retVal = false;

            if (Entity.FieldsInfos != null && Entity.FieldsInfos.Count > 0)
            {
                try
                {
                    string actionPart = (Entity.IsNewItem)
                        ? BuildSaveInsertPart()
                        : BuildSaveUpdatePart();


                    sqlCommand = actionPart;
                    retVal = !string.IsNullOrEmpty(sqlCommand);
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("XmlEntityProxy Save error ", ex);
                    sqlCommand = string.Empty;
                    retVal = false;
                }
            }
            return retVal;
        }

        /// <summary>
        /// Costruisce la stringa SQL per l'insert dell'oggetto nel database
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildSaveInsertPart()
        {
            string sqlInsert = BASE_INSERT;

            bool isValid = false;

            string fieldsPart = string.Empty;
            string valuesPart = string.Empty;

            foreach (FieldItemInfo field in Entity.FieldsInfos)
            {
                if (field.Status == FieldStatus.Set &&
                    FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                {
                    fieldsPart += FieldsMap[field.BaseInfo.PropertyName] + ", ";
                    valuesPart += BuildSqlValue(field) + ", ";
                }
            }

            isValid = !string.IsNullOrEmpty(fieldsPart);

            if (isValid)
            {
                sqlInsert += " [" + _mainTableName + "] " +
                             " (" + fieldsPart.Substring(0, fieldsPart.Length - 2) + ") " + 
                             " VALUES " +
                             " (" + valuesPart.Substring(0, valuesPart.Length - 2) + ") ";

                sqlInsert += "; SELECT SCOPE_IDENTITY()";
            }

            return isValid ? sqlInsert : string.Empty;
        }

        /// <summary>
        /// Costruisce la stringa SQL per l'update dell'oggetto nel database
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildSaveUpdatePart()
        {
            string sqlUpdate = BASE_UPDATE;

            bool isValid = false;

            string fieldsPart = string.Empty;

            foreach (FieldItemInfo field in Entity.FieldsInfos)
            {
                if (field.Status == FieldStatus.Set &&
                    FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                {
                    fieldsPart += FieldsMap[field.BaseInfo.PropertyName] + " = ";
                    fieldsPart += BuildSqlValue(field) + ", ";
                }
            }

            isValid = !string.IsNullOrEmpty(fieldsPart);

            if (isValid)
            {
                sqlUpdate += " [" + _mainTableName + "] " +
                             " SET " + fieldsPart.Substring(0, fieldsPart.Length - 2) +
                             " WHERE " +
                             FieldsMap[UniqueIdFieldName] + " = " + Entity.UniqueId;
            }

            return isValid ? sqlUpdate : string.Empty;
        }

        /// <summary>
        /// Traduce il valore di un campo in formato SQL
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected virtual string BuildSqlValue(FieldItemInfo info)
        {
            string clauseValue = string.Empty;
            if (info.BaseInfo.PropertyType == typeof(string))
            {
                return string.Format(" '{0}' ", XmlProvider.FixSqlString(Entity.GetProperty <string>(info.BaseInfo.PropertyName)));
            }
            else if (info.BaseInfo.PropertyType == typeof(DateTime))
            {
                return string.Format(" '{0}' ", XmlProvider.DateSqlFormat(Entity.GetProperty<DateTime>(info.BaseInfo.PropertyName)));
            }

            else if (info.BaseInfo.PropertyType == typeof(bool))
            {
                return string.Format(" {0} ", Entity.GetProperty<bool>(info.BaseInfo.PropertyName) ? 1 : 0);
            }
            else if (info.BaseInfo.PropertyType.BaseType == typeof(Enum))
            {
                return string.Format(" {0} ", (int)Entity.GetProperty<object>(info.BaseInfo.PropertyName));
            }
            else
            {
                return string.Format(" {0} ", Entity.GetProperty<object>(info.BaseInfo.PropertyName));
            }

        }


        protected virtual object GetFieldValueFromXmlString(FieldItemInfo info, string value)
        {

            if (info.BaseInfo.PropertyType == typeof(string))
            {
                return value;
            }
            else if (info.BaseInfo.PropertyType == typeof(DateTime))
            {
                DateTime dateValue;
                //if (DateTime.TryParse(value, out dateValue))
                if (DateTime.TryParseExact(value,"MM/dd/yyyy HH:mm:ss.fff",CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
                {
                    return dateValue;
                }
                return default(DateTime);
            }

            else if (info.BaseInfo.PropertyType == typeof(bool))
            {
                bool boolValue;
                if (bool.TryParse(value, out boolValue))
                {
                    return boolValue;
                }
                return default(bool);
            }

            else if (info.BaseInfo.PropertyType.BaseType == typeof(Enum))
            {
                if (Enum.IsDefined(info.BaseInfo.PropertyType, value))
                {
                    return (int.Parse(value));
                }
                return value;
            }
            else
            {
                return value;
            }

        }

        protected virtual string SetXmlValue(FieldItemInfo info)
        {

            if (info.BaseInfo.PropertyType == typeof(string))
            {
                return Entity.GetProperty<string>(info.BaseInfo.PropertyName);
            }
            else if (info.BaseInfo.PropertyType == typeof(DateTime))
            {
                return Entity.GetProperty<DateTime>(info.BaseInfo.PropertyName)
                    .ToString("MM/dd/yyyy HH:mm:ss.fff", CultureInfo.InvariantCulture);
                //return Entity.GetProperty<DateTime>(info.BaseInfo.PropertyName)
                //    .ToString(CultureInfo.InvariantCulture);
            }

            else if (info.BaseInfo.PropertyType == typeof(bool))
            {
                return Entity.GetProperty<bool>(info.BaseInfo.PropertyName).ToString(CultureInfo.InvariantCulture);
            }
            else if (info.BaseInfo.PropertyType.BaseType == typeof(Enum))
            {
                return Entity.GetProperty<int>(info.BaseInfo.PropertyName).ToString();
            }
            else
            {
                return Entity.GetProperty<object>(info.BaseInfo.PropertyName).ToString();
            }

        }


        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database in modalità sincrona
        /// </summary>
        /// <param name="fieldName"></param>
        protected virtual void SyncGetSingleField(string fieldName)
        {
            if (FieldsMap == null || !FieldsMap.ContainsKey(fieldName))
            {
                return;
            }

            string sqlCommand;

            sqlCommand = BuildSingleFieldSelect(fieldName);

            bool rc = true;

            int exception_step = 0;
            OleDbDataReader ord = null;

            try
            {

                //if (!IsConnected())
                //    if (!Connect(sConnectionString))
                //        return false;

                exception_step = 1;
                //if (!_bretonInterface.Requery(sqlCommand, out ord))
                //    return;

                exception_step = 2;
                if (ord.HasRows)
                {
                    ord.Read();

                    object fieldValue = ord[0];

                    if (fieldValue != DBNull.Value)
                    {
                        Entity.SetProperty(fieldName, fieldValue, true, FieldStatus.Read);

                    }
                    else
                    {
                        Entity.SetPropertyStatus(fieldName,  FieldStatus.Read);
                    }

                }
                else
                    rc = false;
            }
            catch (Exception ex)
            {
                //TraceLog.WriteLine(this + "ExtractInfo (" + slabcode + "): Exception: " + ex.Message + "; step = " + exception_step.ToString());
                //slabinfo = null;
                rc = false;
            }

            finally
            {
                //_bretonInterface.EndRequery(ord);
                ord = null;
            }

            return;

        }

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database in modalità asincrona
        /// </summary>
        /// <param name="fieldName"></param>
        protected virtual void AsyncGetSingleField(string fieldName)
        {
            if (_provider != null && _provider.CanStartWorkers)
            {
                ThreadPool.QueueUserWorkItem(AsyncGetSingleFieldCore, new object[] {fieldName });
            }

        }

        /// <summary>
        /// Core dell'estrazione di un singolo campo dell'oggetto dal database in modalità asincrona
        /// </summary>
        /// <param name="args"></param>
        protected virtual void AsyncGetSingleFieldCore(object args)
        {
            if (_provider != null)
            {
                if (!_provider.CanStartWorkers)
                    return;

                _provider.Workers.Add(Thread.CurrentThread);
            }

            try
            {
                object[] array = args as object[];

                string fieldName = array[0].ToString();
                SyncGetSingleField(fieldName);
            }
            catch (Exception ex)
            {
                TraceLog.WriteException(ex);
            }
            finally
            {
                if (_provider != null)
                    _provider.Workers.Remove(Thread.CurrentThread);
            }

        }

        protected string BuildSingleFieldSelect(string fieldName)
        {
            string sqlCommand;
            sqlCommand = "SELECT ";
            sqlCommand += FieldsMap[fieldName] + " ";
            sqlCommand += " FROM " + _mainTableName + " ";
            sqlCommand += " WHERE F_ID = " + Entity.UniqueId;
            return sqlCommand;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<


        public XmlProvider Provider
        {
            get { return _provider; }
        }
    }
}
