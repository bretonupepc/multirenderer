﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Policy;
using System.Xml;
using System.Xml.Linq;

namespace BrSm.Business.Persistence.Xml
{
    public class XmlHelper
    {
        private static List<FilterOperator> _filterOperators = null;


        public static List<FilterOperator> FilterOperators
        {
            get
            {
                if (_filterOperators == null)
                {
                    BuildOperatorsList();
                }
                return _filterOperators;
            }
        }

        private static void BuildOperatorsList()
        {
            _filterOperators = new List<FilterOperator>()
            {
                new FilterOperator(ConditionOperator.Eq, ConditionOperatorValueType.SingleValueRequired, " = "),
                new FilterOperator(ConditionOperator.Ge, ConditionOperatorValueType.SingleValueRequired, " >= "),
                new FilterOperator(ConditionOperator.Gt, ConditionOperatorValueType.SingleValueRequired, " > "),
                new FilterOperator(ConditionOperator.In, ConditionOperatorValueType.MultipleValuesRequired, " IN "),
                new FilterOperator(ConditionOperator.IsNotNull, ConditionOperatorValueType.NoValueRequired, " IS NOT NULL "),
                new FilterOperator(ConditionOperator.IsNull, ConditionOperatorValueType.NoValueRequired, " IS NULL "),
                new FilterOperator(ConditionOperator.Le, ConditionOperatorValueType.SingleValueRequired, " <= "),
                new FilterOperator(ConditionOperator.Like, ConditionOperatorValueType.SingleValueRequired, " LIKE "),
                new FilterOperator(ConditionOperator.Lt, ConditionOperatorValueType.SingleValueRequired, " < "),
                new FilterOperator(ConditionOperator.Ne, ConditionOperatorValueType.SingleValueRequired, " <> ")
            };
        }


        public static void BuildAssembliesInfoNode(out XComment commentNode)
        {
            commentNode = null;

            string assembliesData = "Current loaded BrSm assemblies versions:\n";

            AppDomain currentDomain = AppDomain.CurrentDomain;
		    //Provide the current application domain evidence for the assembly.

		    //Make an array for the list of assemblies.
		    Assembly[] assemblies = currentDomain.GetAssemblies();
            foreach (var item in assemblies)
            {
                if (item.FullName.StartsWith("BrSm."))
                    assembliesData += string.Format("- {0} - ver {1}\n", item.FullName, item.ImageRuntimeVersion);
            }

            commentNode= new XComment(assembliesData);

        }
    }
}
