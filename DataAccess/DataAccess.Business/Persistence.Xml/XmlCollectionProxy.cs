﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Breton.DbAccess;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.Persistence.Xml
{
    public class XmlCollectionProxy<T> : PersistableCollectionProxy<T> where T : BasePersistableEntity
    {

        #region >-------------- Constants and Enums

        protected  const string BASE_SELECT = " SELECT ";
        protected const string BASE_WHERE = " WHERE ";
        protected const string BASE_ORDERBY = " ORDER BY ";

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        protected string _mainTableName = string.Empty;

        protected XmlProvider _provider = new XmlProvider();

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public XmlCollectionProxy(PersistableEntityCollection<T> collection)
            : base(collection)
        {
        }

        public XmlCollectionProxy(PersistableEntityCollection<T> collection, string mainTableName) : this(collection)
        {
            _mainTableName = mainTableName;
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields

        protected Dictionary<string, string> _fieldsMap = null;

        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties

        protected internal Dictionary<string, string> FieldsMap
        {
            get
            {
                if (_fieldsMap == null)
                {
                    _fieldsMap = SqlServerFieldsMaps.GetEntityFieldsMap(typeof (T));
                }
                if (_fieldsMap == null || _fieldsMap.Count == 0)
                {
                    _fieldsMap = SqlServerFieldsMaps.GetEntityFieldsMap(typeof(T).BaseType);
                }

                return _fieldsMap;
            }
        }

        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Recupera una collection di items da database
        /// tramite elenco campi, filtro di selezione e criterio di ordinamento
        /// </summary>
        /// <param name="set">Set di campi da estrarre</param>
        /// <param name="filterClause">Filtro di selezione</param>
        /// <param name="sortSet">Criterio di ordinamento</param>
        /// <returns></returns>
        //public override bool GetItems(FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
        //{

        //    if (!CanGetItems())
        //    {
        //        return false;
        //    }

        //    string sqlCommand = string.Empty;

        //    if (!BuildSelectCommand(set, filterClause, sortSet, ref sqlCommand)) return false;

        //    this.Collection.Clear();


        //    bool retVal = true;

        //    int exception_step = 0;
        //    OleDbDataReader ord = null;

        //    try
        //    {

        //        if (!_bretonInterface.Requery(sqlCommand, out ord))
        //            return false;

        //        if (ord.HasRows )
        //        {
        //            while (ord.Read())
        //            {
        //                T newItem = CreateNewItemFromDbRecord<T>(ord);

        //                if (newItem != default(T))
        //                {
        //                    _collection.Add(newItem);
        //                }
        //            }

        //        }
        //        else
        //            retVal = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine(this + "GetItems error: " , ex);
        //        retVal = false;
        //    }

        //    finally
        //    {
        //        _bretonInterface.EndRequery(ord);
        //        ord = null;
        //    }

        //    return retVal;


        //}

//        public override bool GetItems(FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
//        {
//            /*
             


//XDocument xdoc = new XDocument();
//xdoc.Load(path_to_file or another overload);

//var groupElements = from el in xdoc.Descendants().Elements("Group") select el;

////groupElements is now a collection of XElements

//foreach(XElement xe in groupElements)
//{
//  //do something
//}
//             * 
//             */


//            if (!CanGetItems())
//            {
//                return false;
//            }

//            string sqlCommand = string.Empty;

//            if (!BuildSelectCommand(set, filterClause, sortSet, ref sqlCommand)) return false;

//            this.Collection.Clear();


//            bool retVal = true;

//            int exception_step = 0;
//            OleDbDataReader ord = null;

//            try
//            {

//                //if (!_bretonInterface.Requery(sqlCommand, out ord))
//                //    return false;

//                if (ord.HasRows)
//                {
//                    while (ord.Read())
//                    {
//                        T newItem = CreateNewItemFromDbRecord<T>(ord);

//                        if (newItem != default(T))
//                        {
//                            Collection.Add(newItem);
//                        }
//                    }

//                }
//                else
//                    retVal = false;
//            }
//            catch (Exception ex)
//            {
//                TraceLog.WriteLine(this + "GetItems error: ", ex);
//                retVal = false;
//            }

//            finally
//            {
//                //_bretonInterface.EndRequery(ord);
//                ord = null;
//            }

//            return retVal;


//        }

        public override bool GetItems(XElement node, FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
        {
            /*
             


XDocument xdoc = new XDocument();
xdoc.Load(path_to_file or another overload);

var groupElements = from el in xdoc.Descendants().Elements("Group") select el;

//groupElements is now a collection of XElements

foreach(XElement xe in groupElements)
{
  //do something
}
             * 
             */


            if (!CanGetItems() || node == null )
            {
                return false;
            }

            //string sqlCommand = string.Empty;

            //if (!BuildSelectCommand(set, filterClause, sortSet, ref sqlCommand)) return false;

            this.Collection.Clear();


            bool retVal = true;


            try
            {

                //if (!_bretonInterface.Requery(sqlCommand, out ord))
                //    return false;
                string searchedType = typeof (T).FullName;
                if (!node.IsEmpty )
                {
                    //var items = node.Descendants(searchedType);
                    var items = node.Elements(searchedType);
                    if (items != null)
                    {
                        foreach (var item in items)
                        {
                            T newItem = Activator.CreateInstance<T>();
                            var proxy = _provider.GetEntityProxy(newItem) as XmlEntityProxy;
                            if (proxy != null && proxy.GetFields(item))
                            {
                                Collection.Add(newItem);
                            }

                        }
                    }
                }

                else
                    retVal = false; // Forse anche no, potrebbe essere semplicemente vuota...
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this + "GetItems error: ", ex);
                retVal = false;
            }

            finally
            {
                //_bretonInterface.EndRequery(ord);
                //ord = null;
            }

            return retVal;


        }

        public override bool GetItems(string filepath, FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
        {

            bool retVal = false;
            /*
             


XDocument xdoc = new XDocument();
xdoc.Load(path_to_file or another overload);

var groupElements = from el in xdoc.Descendants().Elements("Group") select el;

//groupElements is now a collection of XElements

foreach(XElement xe in groupElements)
{
  //do something
}
             * 
             */


            if (!CanGetItems())
            {
                return retVal;
            }

            if (!File.Exists(filepath))
            {
                TraceLog.WriteLine("XmlCollectionProxy GetItems(filepath) error: file does not exists.");
                return retVal;
            }


            try 
	        {	        
		        XDocument xDocument = XDocument.Load(filepath);

	            retVal = GetItems(xDocument.Root);

	        }
	        catch (Exception ex)
	        {
                TraceLog.WriteLine("XmlCollectionProxy GetItems(filepath) error ", ex);
	            retVal = false;
	        }

            finally
            {
            }

            return retVal;


        }

        public override bool SaveItems(out XElement rootNode)
        {
            bool retVal = false;

            rootNode = null;

            if (Collection == null || _provider == null)
            {
                return retVal;
            }

            try
            {
                rootNode = new XElement(typeof(T).FullName + "_Collection");

                XElement dummyNode = null;
                XElement childNode = null;

                foreach (var item in Collection)
                {
                    if (item.SaveToRepository(out childNode, PersistenceProviderType.Xml))
                    {
                        rootNode.Add(childNode);
                    }
                }


                retVal = true;
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlCollectionProxy Save error ", ex);
                retVal = false;
            }

            return retVal;

        }

        public override bool SaveItems(string filepath, bool overwriteIfExistent = true)
        {
            bool retVal = false;

            try
            {
                if (File.Exists(filepath) && !overwriteIfExistent)
                {
                    Trace.WriteLine("XmlCollectionProxy SaveItems(filepath) error: File exists.");
                    return retVal;
                }

                XElement rootNode = null;
                if (SaveItems(out rootNode))
                {
                    XDocument xDocument = new XDocument();

                    xDocument.AddFirst(rootNode);


                    XComment versionComment = null;
                    XmlHelper.BuildAssembliesInfoNode(out versionComment);
                    if (versionComment != null)
                    {
                        xDocument.AddFirst(versionComment);
                    }


                    xDocument.Save(filepath);

                    retVal = true;
                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlCollectionProxy SaveItems(filepath) error ", ex);
                retVal = false;
            }
            return retVal;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// Crea un nuovo oggetto a partire dal record database
        /// </summary>
        /// <typeparam name="T">Tipo oggetto</typeparam>
        /// <param name="ord">Oggetto [Reader] da database</param>
        /// <returns></returns>
        //protected virtual T CreateNewItemFromDbRecord<T>(OleDbDataReader ord) where T : BasePersistableEntity
        //{

        //    T newItem = Activator.CreateInstance<T>();
        //    try
        //    {

        //        foreach (FieldItemInfo field in FieldsInfos)
        //        {
        //            if (field.RetrieveMode == FieldRetrieveMode.Immediate &&
        //                FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
        //            {
        //                object fieldValue = ord[FieldsMap[field.BaseInfo.PropertyName]];

        //                if (fieldValue != DBNull.Value)
        //                {
        //                    newItem.SetProperty(field.BaseInfo.PropertyName, fieldValue, true, FieldStatus.Read);
        //                }
        //                else
        //                {
        //                    newItem.SetPropertyStatus(field.BaseInfo.PropertyName, FieldStatus.Read);
        //                }


        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
                
        //        TraceLog.WriteLine("CreateNewItem error ", ex);
        //        newItem = default(T);
        //    }

        //    return newItem ;
        //}

        /// <summary>
        /// Costruisce la stringa SQL di estrazione dati
        /// </summary>
        /// <param name="set">Set campi</param>
        /// <param name="filterClause">Filtro di estrazione</param>
        /// <param name="sortSet">Criteri ordinamento</param>
        /// <param name="sqlCommand">Stringa SQL prodotta</param>
        /// <returns></returns>
        //protected virtual bool BuildSelectCommand(FieldsSet set, FilterClause filterClause, FieldsSortSet sortSet, ref string sqlCommand)
        //{
        //    if (set == null)
        //    {
        //        if (_fieldsInfos == null)
        //        {
        //            BuildDefaultFieldsInfos();
        //        }
        //    }
        //    else
        //    {
        //        _fieldsInfos = set;
        //    }

        //    string selectPart = BuildSelectPart(_fieldsInfos);
        //    if (string.IsNullOrEmpty(selectPart))
        //        return false;

        //    string fromPart = BuildFromPart();
        //    if (string.IsNullOrEmpty(fromPart))
        //        return false;

        //    string wherePart = string.Empty;
        //    //if (filterClause != null)
        //    {
        //        wherePart = BuildWherePart(filterClause);
        //    }


        //    string sortPart = string.Empty;
        //    if (sortSet != null)
        //    {
        //        sortPart = BuildOrderByPart(sortSet);
        //    }

        //    sqlCommand = string.Concat(selectPart, fromPart, wherePart, sortPart);
        //    return true;
        //}

        protected virtual bool CanGetItems()
        {
            //return (_bretonInterface != null && _collection != null);
            return (Collection != null);
        }

        /// <summary>
        /// Costruisce la parte SELECT della stringa SQL
        /// </summary>
        /// <param name="set">Set campi</param>
        /// <returns></returns>
        protected virtual string BuildSelectPart(FieldsSet set)
        {
            string selectStr = BASE_SELECT;

            bool isValid = false;

            if (set != null)
            {
                foreach (FieldItemInfo field in set)
                {
                    if (field.RetrieveMode == FieldRetrieveMode.Immediate &&
                        FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                    {
                        selectStr += FieldsMap[field.BaseInfo.PropertyName] + ", ";

                    }
                }

                isValid = selectStr != BASE_SELECT;
                selectStr = selectStr.Substring(0, selectStr.Length - 2) + " ";
            }

            else if (FieldsMap.Count > 0)
            {
                foreach (KeyValuePair<string, string> kvp in FieldsMap)
                {
                    selectStr += kvp.Value + ", ";
                }

                isValid = !string.IsNullOrEmpty(selectStr);
                selectStr = selectStr.Substring(0, selectStr.Length - 2) + " ";
            }

            return isValid ? selectStr : string.Empty;
        }

        /// <summary>
        /// Costruisce la parte FROM della stringa SQL
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildFromPart()
        {
            string fromStr = string.Empty;

            if (!string.IsNullOrEmpty(_mainTableName))
            {
                fromStr = string.Format(" FROM {0} ", _mainTableName);
            }

            return fromStr;
        }

        /// <summary>
        /// Costruisce la parte WHERE della stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        protected virtual string BuildWherePart(FilterClause clause)
        {
            string whereStr = BASE_WHERE;

            bool isValid = false;

            if (clause != null)
            {
                string whereContent;
                isValid = ParseClause(clause, out whereContent);

                if (isValid)
                {
                    whereStr += whereContent;
                }

            }

            return isValid ? whereStr : string.Empty;
        }

        /// <summary>
        /// Analizza il filtro di selezione creando la relativa stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <param name="sqlClauseString"></param>
        /// <returns></returns>
        protected bool ParseClause(FilterClause clause, out string sqlClauseString)
        {
            sqlClauseString = string.Empty;

            bool retVal = true;

            bool isValid = true;

            if (clause.ParentClause != null)
            {
                string logicalConnector = string.Empty;

                switch (clause.ParentConnector)
                {
                    case LogicalConnector.And:
                        logicalConnector = " AND ";
                        break;

                    case LogicalConnector.AndNot:
                        logicalConnector = " AND NOT ";
                        break;

                    case LogicalConnector.Or:
                        logicalConnector = " OR ";
                        break;

                    case LogicalConnector.OrNot:
                        logicalConnector = " OR NOT ";
                        break;

                }
                sqlClauseString += logicalConnector;
            }

            // Main clause

            if (FieldsMap.ContainsKey(clause.FieldInfo.PropertyName))
            {
                string mainClauseString;


                isValid = BuildMainClauseString(clause, out mainClauseString);
                if (isValid)
                {
                    if (clause.ChildrenClauses.Count > 0)
                    {
                        sqlClauseString += " ( ";
                    }

                    sqlClauseString += mainClauseString;

                    string childrenClausesString;

                    isValid = BuildChildrenClausesString(clause, out childrenClausesString);
                    if (isValid)
                    {
                        sqlClauseString += childrenClausesString;
                    }

                    if (clause.ChildrenClauses.Count > 0)
                    {
                        sqlClauseString += " ) ";
                    }

                }

            }


            return isValid;
        }

        /// <summary>
        /// Costruisce la parte ORDER BY della stringa SQL
        /// </summary>
        /// <param name="set">Set criteri di ordinamento</param>
        /// <returns></returns>
        protected virtual string BuildOrderByPart(FieldsSortSet set)
        {
            string orderByStr = string.Empty;

            bool isValid = true;

            if (set != null)
            {
                orderByStr = BASE_ORDERBY;
                foreach (var item in set)
                {
                    if (FieldsMap.ContainsKey(item.FieldName))
                    {
                        orderByStr += string.Format(" {0} {1}, ", FieldsMap[item.FieldName],
                            item.Sort == SortOrder.Ascending ? "ASC" : "DESC");
                    }

                }
                isValid = orderByStr != BASE_ORDERBY;
                orderByStr = orderByStr.Substring(0, orderByStr.Length - 2) + " ";

            }

            return isValid ? orderByStr : string.Empty;

        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        /// <summary>
        /// Analizza la clausola principale di un filtro di selezione creando la relativa stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <param name="clauseContent"></param>
        /// <returns></returns>
        private bool BuildMainClauseString(FilterClause clause, out string clauseContent)
        {
            bool isValid = true;

            clauseContent = string.Empty;

            if (FieldsMap.ContainsKey(clause.FieldInfo.PropertyName))
            {
                clauseContent += " (";

                clauseContent += FieldsMap[clause.FieldInfo.PropertyName];

                FilterOperator fo = SqlHelper.FilterOperators.FirstOrDefault(f => f.Operator == clause.Operator);

                if (fo != null)
                {
                    clauseContent += fo.OperatorRender;

                    switch (fo.ValueType)
                    {
                        case ConditionOperatorValueType.MultipleValuesRequired:
                            //clauseContent += " ( ";

                            clauseContent += BuildClauseValues(clause);

                            //clauseContent += " ) ";
                            break;

                        case ConditionOperatorValueType.SingleValueRequired:
                            clauseContent += BuildClauseValue(clause);

                            break;

                        case ConditionOperatorValueType.NoValueRequired:
                            break;
                    }
                }

                clauseContent += ") ";



            }
            else
            {
                isValid = false;
            }


            return isValid;

        }

        /// <summary>
        /// Traduce un valore di una clausola in formato SQL
        /// </summary>
        /// <param name="clause">Clausola di filtro</param>
        /// <param name="valueIndex">Progressivo all'interno dell'elenco valori della clausola</param>
        /// <returns></returns>
        private string BuildClauseValue(FilterClause clause, int valueIndex = 0)
        {
            string clauseValue = string.Empty;
            if (clause.FieldInfo.PropertyType == typeof (string))
            {
                return clause.Values[0] == null
                    ? " '' "
                    : string.Format(" '{0}' ", SqlServerProvider.FixSqlString(clause.Values[valueIndex].ToString()));
            }
            else if (clause.FieldInfo.PropertyType == typeof (DateTime))
            {
                return clause.Values[0] == null
                    ? " '' "
                    : string.Format(" '{0}' ", SqlServerProvider.DateSqlFormat((DateTime)clause.Values[valueIndex]));
            }
            else
            {
                return clause.Values[0] == null
                    ? " "
                    : string.Format(" {0} ", clause.Values[valueIndex].ToString());
            }

        }

        /// <summary>
        /// Traduce il set di valori di una clausola in formato SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        private string BuildClauseValues(FilterClause clause)
        {
            string clauseValue = " (";

            for (int i = 0; i < clause.Values.Count; i++)
            {
                clauseValue += BuildClauseValue(clause, i);
                clauseValue += ", ";
            }
            if (clauseValue.Length > 2)
            {
                clauseValue = clauseValue.Substring(0, clauseValue.Length - 2) + " ";
            }

            clauseValue += ") ";

            return clauseValue;

        }

        /// <summary>
        /// Analizza le clausole figlie di un filtro di selezione creando la relativa stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <param name="childrenClausesString"></param>
        /// <returns></returns>
        private bool BuildChildrenClausesString(FilterClause clause, out string childrenClausesString)
        {
            bool isValid = true;
            childrenClausesString = string.Empty;

            if (clause.ChildrenClauses.Count > 0)
            {
                foreach (var singleClause in clause.ChildrenClauses)
                {
                    string childClauseString;
                    if (ParseClause(singleClause, out childClauseString))
                    {
                        childrenClausesString += childClauseString;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
            }

            return isValid;
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
