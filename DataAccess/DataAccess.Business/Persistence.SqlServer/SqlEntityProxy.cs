﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Threading;
using Breton.DbAccess;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.Persistence.SqlServer
{
    public class SqlEntityProxy:PersistableEntityProxy
    {

        #region >-------------- Constants and Enums

        protected const string BASE_SELECT = " SELECT ";
        protected const string BASE_UPDATE = " UPDATE ";
        protected const string BASE_WHERE = " WHERE ";
        protected const string BASE_INSERT = " INSERT INTO ";


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private DbInterface _bretonInterface;

        private SqlServerProvider _provider;

        protected string _mainTableName = string.Empty;


        protected Dictionary<string, string> _fieldsMap;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlEntityProxy(BasePersistableEntity entity, SqlServerProvider provider) : base(entity)
        {
            _provider = provider;
            _bretonInterface = provider.BretonInterface.Clone();
        }

        public SqlEntityProxy(BasePersistableEntity entity, SqlServerProvider provider, string mainTableName)
            : this(entity, provider)
        {
            _mainTableName = mainTableName;
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public virtual string UniqueIdFieldName
        {
            get
            {
                return "Id";
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties

        protected internal Dictionary<string, string> FieldsMap
        {
            get
            {
                if (_fieldsMap == null || _fieldsMap.Count == 0)
                {
                    var map = SqlServerFieldsMaps.GetEntityFieldsMap(Entity.GetType());
                    if (map == null || map.Count == 0)
                    {
                        Type innerType = Entity.GetType().BaseType;
                        while (innerType != null && innerType != typeof(BasePersistableEntity))
                        {
                            map = SqlServerFieldsMaps.GetEntityFieldsMap(innerType);
                            if (map != null && map.Count > 0)
                            {
                                _fieldsMap = map;
                                break;
                            }
                            innerType = innerType.BaseType;
                        }
                    }
                    else
                    {
                        _fieldsMap = map;
                    }
                }

                return _fieldsMap;
            }
        }

        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database
        /// </summary>
        /// <param name="fieldName">Nome campo</param>
        public override void GetSingleField(string fieldName)
        {
            FieldRetrieveMode mode = FieldRetrieveMode.Deferred;

            FieldItemInfo info = _entity.GetFieldItemInfo(fieldName);
            if (info != null)
            {
                mode = info.RetrieveMode;
            }

            switch (mode)
            {
                case FieldRetrieveMode.DeferredAsync:
                    AsyncGetSingleField(fieldName);
                    break;

                default:
                    SyncGetSingleField(fieldName);
                    break;
            }

            return;

        }

        /// <summary>
        /// Salva l'oggetto su database
        /// </summary>
        /// <returns></returns>
        public override bool Save()
        {
            bool retVal = false;

            string sqlCommand;

            retVal = BuildSaveCommand(out sqlCommand);

            if (retVal)
            {
                bool transaction = !BretonInterface.TransactionActive();

                try
                {
                    if (transaction) BretonInterface.BeginTransaction();

                    int id;
                    if (_entity.IsNewItem)
                    {
                        retVal = BretonInterface.Execute(sqlCommand, out id);
                        if (retVal)
                        {
                            _entity.SetProperty(UniqueIdFieldName, id, true, FieldStatus.Read);
                        }
                    }
                    else
                    {
                        retVal = BretonInterface.Execute(sqlCommand);
                    }


                    if (retVal)
                    {
                        retVal = Refresh();
                    }
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("EntityProxy Save error ", ex);
                    retVal = false;


                }

                finally
                {
                    if (transaction) BretonInterface.EndTransaction(retVal);
                }
                
            }



            return retVal;

        }

        /// <summary>
        /// Rilegge l'oggetto da database
        /// </summary>
        /// <returns></returns>
        public override bool Refresh()
        {
            bool retVal = false;

            string sqlCommand;

            retVal = BuildRefreshingSelectCommand(out sqlCommand);

            if (retVal)
            {
                bool transaction = !BretonInterface.TransactionActive();


                int exception_step = 0;
                OleDbDataReader ord = null;


                try
                {
                    if (transaction) BretonInterface.BeginTransaction();

                    retVal = (BretonInterface.Requery(sqlCommand, out ord)) && ord.HasRows;
                    

                    
                    if ( retVal )
                    {
                        ord.Read();

                        foreach (FieldItemInfo field in _entity.FieldsInfos)
                        {
                            if ((field.Status == FieldStatus.Set || field.Status == FieldStatus.Read) 
                                && (FieldsMap.ContainsKey(field.BaseInfo.PropertyName)))
                            {
                                object fieldValue = ord[FieldsMap[field.BaseInfo.PropertyName]];

                                if (fieldValue != DBNull.Value)
                                {
                                    _entity.SetProperty(field.BaseInfo.PropertyName, fieldValue, true, FieldStatus.Read);

                                }

                            }
                        }


                    }

                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("EntityProxy Refresh error ", ex);
                    retVal = false;
                    ord = null;

                }

                finally
                {
                    if (transaction) BretonInterface.EndTransaction(retVal);
                }

            }



            return retVal;

        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// Costruisce la stringa SQL per il refresh dell'oggetto
        /// </summary>
        /// <param name="sqlCommand">Stringa SQL prodotta</param>
        /// <returns></returns>
        protected virtual bool BuildRefreshingSelectCommand(out string sqlCommand)
        {
            string selectStr = BASE_SELECT;

            bool isValid = false;

            string fieldsPart = string.Empty;

            if (_entity.FieldsInfos != null && _entity.FieldsInfos.Count > 0)
            {

                foreach (FieldItemInfo field in _entity.FieldsInfos)
                {
                    if ((field.Status == FieldStatus.Set || field.Status == FieldStatus.Read)
                        && (FieldsMap.ContainsKey(field.BaseInfo.PropertyName)))
                    {
                        fieldsPart += FieldsMap[field.BaseInfo.PropertyName] + ", ";

                    }
                }

                isValid = !string.IsNullOrEmpty(fieldsPart);
                fieldsPart = fieldsPart.Substring(0, fieldsPart.Length - 2) + " ";
            }

            if (isValid)
            {
                selectStr += fieldsPart +
                             " FROM [" + _mainTableName + "] " +
                             " WHERE " +
                             FieldsMap[UniqueIdFieldName] + " = " + _entity.UniqueId;

            }

            sqlCommand = selectStr;
            return isValid ;
        }

        /// <summary>
        /// Costruisce la stringa SQL per il Save dell'oggetto
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected virtual bool BuildSaveCommand(out string sqlCommand)
        {
            sqlCommand = string.Empty;

            bool retVal = false;

            if (_entity.FieldsInfos != null && _entity.FieldsInfos.Count > 0)
            {
                try
                {
                    string actionPart = (_entity.IsNewItem)
                        ? BuildSaveInsertPart()
                        : BuildSaveUpdatePart();


                    sqlCommand = actionPart;
                    retVal = !string.IsNullOrEmpty(sqlCommand);
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("SqlEntityProxy Save error ", ex);
                    sqlCommand = string.Empty;
                    retVal = false;
                }
            }
            return retVal;
        }

        /// <summary>
        /// Costruisce la stringa SQL per l'insert dell'oggetto nel database
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildSaveInsertPart()
        {
            string sqlInsert = BASE_INSERT;

            bool isValid = false;

            string fieldsPart = string.Empty;
            string valuesPart = string.Empty;

            foreach (FieldItemInfo field in _entity.FieldsInfos)
            {
                if (field.Status == FieldStatus.Set &&
                    FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                {
                    fieldsPart += FieldsMap[field.BaseInfo.PropertyName] + ", ";
                    valuesPart += BuildSqlValue(field) + ", ";
                }
            }

            isValid = !string.IsNullOrEmpty(fieldsPart);

            if (isValid)
            {
                sqlInsert += " [" + _mainTableName + "] " +
                             " (" + fieldsPart.Substring(0, fieldsPart.Length - 2) + ") " + 
                             " VALUES " +
                             " (" + valuesPart.Substring(0, valuesPart.Length - 2) + ") ";

                sqlInsert += "; SELECT SCOPE_IDENTITY()";
            }

            return isValid ? sqlInsert : string.Empty;
        }

        /// <summary>
        /// Costruisce la stringa SQL per l'update dell'oggetto nel database
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildSaveUpdatePart()
        {
            string sqlUpdate = BASE_UPDATE;

            bool isValid = false;

            string fieldsPart = string.Empty;

            foreach (FieldItemInfo field in _entity.FieldsInfos)
            {
                if (field.Status == FieldStatus.Set &&
                    FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                {
                    fieldsPart += FieldsMap[field.BaseInfo.PropertyName] + " = ";
                    fieldsPart += BuildSqlValue(field) + ", ";
                }
            }

            isValid = !string.IsNullOrEmpty(fieldsPart);

            if (isValid)
            {
                sqlUpdate += " [" + _mainTableName + "] " +
                             " SET " + fieldsPart.Substring(0, fieldsPart.Length - 2) +
                             " WHERE " +
                             FieldsMap[UniqueIdFieldName] + " = " + _entity.UniqueId;
            }

            return isValid ? sqlUpdate : string.Empty;
        }

        /// <summary>
        /// Traduce il valore di un campo in formato SQL
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected virtual string BuildSqlValue(FieldItemInfo info)
        {
            string clauseValue = string.Empty;
            if (info.BaseInfo.PropertyType == typeof(string))
            {
                return string.Format(" '{0}' ", SqlServerProvider.FixSqlString(_entity.GetProperty <string>(info.BaseInfo.PropertyName)));
            }
            else if (info.BaseInfo.PropertyType == typeof(DateTime))
            {
                return string.Format(" '{0}' ", SqlServerProvider.DateSqlFormat(_entity.GetProperty<DateTime>(info.BaseInfo.PropertyName)));
            }

            else if (info.BaseInfo.PropertyType == typeof(bool))
            {
                return string.Format(" {0} ", _entity.GetProperty<bool>(info.BaseInfo.PropertyName) ? 1 : 0);
            }
            else if (info.BaseInfo.PropertyType.BaseType == typeof(Enum))
            {
                return string.Format(" {0} ", (int)_entity.GetProperty<object>(info.BaseInfo.PropertyName));
            }
            else
            {
                return string.Format(" {0} ", _entity.GetProperty<object>(info.BaseInfo.PropertyName));
            }

        }

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database in modalità sincrona
        /// </summary>
        /// <param name="fieldName"></param>
        protected virtual void SyncGetSingleField(string fieldName)
        {
            if (FieldsMap == null || !FieldsMap.ContainsKey(fieldName))
            {
                return;
            }

            string sqlCommand;

            sqlCommand = BuildSingleFieldSelect(fieldName);

            bool rc = true;

            int exception_step = 0;
            OleDbDataReader ord = null;

            try
            {

                //if (!IsConnected())
                //    if (!Connect(sConnectionString))
                //        return false;

                exception_step = 1;
                if (!_bretonInterface.Requery(sqlCommand, out ord))
                    return;

                exception_step = 2;
                if (ord.HasRows)
                {
                    ord.Read();

                    object fieldValue = ord[0];

                    if (fieldValue != DBNull.Value)
                    {
                        Entity.SetProperty(fieldName, fieldValue, true, FieldStatus.Read);

                    }
                    else
                    {
                        Entity.SetPropertyStatus(fieldName,  FieldStatus.Read);
                    }

                }
                else
                    rc = false;
            }
            catch (Exception ex)
            {
                //TraceLog.WriteLine(this + "ExtractInfo (" + slabcode + "): Exception: " + ex.Message + "; step = " + exception_step.ToString());
                //slabinfo = null;
                rc = false;
            }

            finally
            {
                _bretonInterface.EndRequery(ord);
                ord = null;
            }

            return;

        }

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database in modalità asincrona
        /// </summary>
        /// <param name="fieldName"></param>
        protected virtual void AsyncGetSingleField(string fieldName)
        {
            if (_provider != null && _provider.CanStartWorkers)
            {
                ThreadPool.QueueUserWorkItem(AsyncGetSingleFieldCore, new object[] {fieldName });
            }

        }

        /// <summary>
        /// Core dell'estrazione di un singolo campo dell'oggetto dal database in modalità asincrona
        /// </summary>
        /// <param name="args"></param>
        protected virtual void AsyncGetSingleFieldCore(object args)
        {
            if (_provider != null)
            {
                if (!_provider.CanStartWorkers)
                    return;

                _provider.Workers.Add(Thread.CurrentThread);
            }

            try
            {
                object[] array = args as object[];

                string fieldName = array[0].ToString();
                SyncGetSingleField(fieldName);
            }
            catch (Exception ex)
            {
                TraceLog.WriteException(ex);
            }
            finally
            {
                if (_provider != null)
                    _provider.Workers.Remove(Thread.CurrentThread);
            }

        }

        protected string BuildSingleFieldSelect(string fieldName)
        {
            string sqlCommand;
            sqlCommand = "SELECT ";
            sqlCommand += FieldsMap[fieldName] + " ";
            sqlCommand += " FROM " + _mainTableName + " ";
            sqlCommand += " WHERE F_ID = " + Entity.UniqueId;
            return sqlCommand;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        public DbInterface BretonInterface
        {
            get { return _bretonInterface; }
            set { _bretonInterface = value; }
        }

        public SqlServerProvider Provider
        {
            get { return _provider; }
        }
    }
}
