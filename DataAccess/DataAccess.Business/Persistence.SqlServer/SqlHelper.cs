﻿using System.Collections.Generic;

namespace BrSm.Business.Persistence.SqlServer
{
    public class SqlHelper
    {
        private static List<FilterOperator> _filterOperators = null;


        public static List<FilterOperator> FilterOperators
        {
            get
            {
                if (_filterOperators == null)
                {
                    BuildOperatorsList();
                }
                return _filterOperators;
            }
        }

        private static void BuildOperatorsList()
        {
            _filterOperators = new List<FilterOperator>()
            {
                new FilterOperator(ConditionOperator.Eq, ConditionOperatorValueType.SingleValueRequired, " = "),
                new FilterOperator(ConditionOperator.Ge, ConditionOperatorValueType.SingleValueRequired, " >= "),
                new FilterOperator(ConditionOperator.Gt, ConditionOperatorValueType.SingleValueRequired, " > "),
                new FilterOperator(ConditionOperator.In, ConditionOperatorValueType.MultipleValuesRequired, " IN "),
                new FilterOperator(ConditionOperator.IsNotNull, ConditionOperatorValueType.NoValueRequired, " IS NOT NULL "),
                new FilterOperator(ConditionOperator.IsNull, ConditionOperatorValueType.NoValueRequired, " IS NULL "),
                new FilterOperator(ConditionOperator.Le, ConditionOperatorValueType.SingleValueRequired, " <= "),
                new FilterOperator(ConditionOperator.Like, ConditionOperatorValueType.SingleValueRequired, " LIKE "),
                new FilterOperator(ConditionOperator.Lt, ConditionOperatorValueType.SingleValueRequired, " < "),
                new FilterOperator(ConditionOperator.Ne, ConditionOperatorValueType.SingleValueRequired, " <> ")
            };
        }
    }
}
