﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Threading;
using Breton.DbAccess;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.COMMON;
using BrSm.Business.ENTITIES.ARTICLES;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.ENTITIES.ARTICLES.SLABS.Persistence.SqlServer;
using BrSm.Business.ENTITIES.MATERIALS;
using BrSm.Business.ENTITIES.MATERIALS.Persistence.SqlServer;
using BrSm.Business.ENTITIES.PreOptimizer;
using BrSm.Business.ENTITIES.PreOptimizer.Persistence.SqlServer;

namespace BrSm.Business.Persistence.SqlServer
{
    public class SqlServerProvider: IPersistenceProvider, INotifyPropertyChanged
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        private  string _udlFilename = "DbConnection.udl";

        private  DbInterface _bretonInterface;

        private string _currentUserName = string.Empty;

        #region Workers

        private readonly SynchronizationContext _synchroContext;

        private bool _canStartWorkers = true;

        private SafeCollection<Thread> _workers = new SafeCollection<Thread>();

        #endregion Workers


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlServerProvider()
        {
            _synchroContext = SynchronizationContext.Current;
        }

        public SqlServerProvider(string udlFilename): this()
        {
            UdlFilename = udlFilename;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties


        public DbInterface BretonInterface
        {
            get
            {
                if (_bretonInterface == null)
                {
                    _bretonInterface = new DbInterface();

                    TraceLog.WriteLine(string.Format("Tentativo apertura file udl {0}", UdlFilename));

                    if (_bretonInterface.Open("", UdlFilename))
                    {
                        TraceLog.WriteLine(string.Format("Tentativo apertura file udl {0} riuscito.", UdlFilename));
                    }
                    else
                    {
                        TraceLog.WriteLine(string.Format("Tentativo apertura file udl {0} fallito.", UdlFilename));
                    }
                }
                return _bretonInterface;
            }
        }


        public string UdlFilename
        {
            get { return _udlFilename; }
            set { _udlFilename = value; }
        }

        public string CurrentUserName
        {
            get { return _currentUserName; }
            set { _currentUserName = value; }
        }

        public SynchronizationContext SynchroContext
        {
            get { return _synchroContext; }
        }

        public bool CanStartWorkers
        {
            get { return _canStartWorkers; }
            set { _canStartWorkers = value; }
        }

        public SafeCollection<Thread> Workers
        {
            get { return _workers; }
            set
            {
                _workers = value;
                OnPropertyChanged("Workers");
            }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public DataTable GetDataTable(string selectSql)
        {
            TraceLog.WriteLine(string.Format("GetDataTable avviata [{0}]", selectSql));

            DataTable dTable = new DataTable();

            OleDbDataReader reader;

            BretonInterface.Requery(selectSql, out reader);

            if (reader != null)
            {
                dTable.Load(reader);
            }

            BretonInterface.EndRequery(reader);

            TraceLog.WriteLine(string.Format("GetDataTable conclusa [{0}]", selectSql));


            return dTable;

        }

        //[Obsolete("Please don't use anymore")]
        //public SqlEntityProxy GetProxy(BasePersistableEntity entity)
        //{
        //    switch (entity.GetType().ToString())
        //    {
        //        case "MaterialEntity":
        //            return new SqlMaterialProxy(entity as MaterialEntity, this);

        //        default:
        //            return null;
        //    }
        //}

        //[Obsolete("Please don't use anymore")]
        //public SqlCollectionProxy<T> GetProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity
        //{
        //    SqlCollectionProxy<T> proxy = null;
        //    switch (typeof (T).Name)
        //    {
        //        case "MaterialEntity":
        //            proxy = new SqlMaterialCollectionProxy(collection as MaterialCollection, BretonInterface) as SqlCollectionProxy<T>;
        //            break;

        //    }
        //    return proxy;

        //}

        public void StopWorkers()
        {
            _canStartWorkers = false;

            while (_workers.Count > 0)
            {
                for (int i = _workers.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        Thread thread = _workers[i];
                        if (thread != null && thread.IsAlive)
                        {
                            thread.Abort();
                        }
                        else
                        {
                            _workers.RemoveAt(i);
                        }

                    }
                    catch { }

                }
                Thread.Sleep(10);
            }
        }


        //public SqlCollectionProxy<T> GetProxy<T>(PersistableEntityCollection<T> collection ) where T:BasePersistableEntity
        //{
        //    SqlMaterialCollectionProxy proxy = new SqlMaterialCollectionProxy(new MaterialCollection());

        //    return proxy;
        //    //switch (typeof(T).Name)
        //    //{
        //    //    case "MaterialEntity":

        //    //        //return new SqlMaterialCollectionProxy(collection as MaterialCollection);

        //    //    default:
        //    //        return null;
        //    //}
        //}


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private static object IsDbNull(object value, object nullValueReplacement)
        {
            if (value == DBNull.Value)
                return nullValueReplacement;

            return value;
        }

        private static string NotNullString(object dataValue)
        {
            if (dataValue.Equals(DBNull.Value))
            {
                return string.Empty;
            }
            else
            {
                return dataValue.ToString();
            }
        }

        public static string DateSqlFormat(DateTime date)
        {
            return date.ToString("yyyyMMdd HH:mm:ss");
        }

        public static string FixSqlString(string inputStr)
        {
            return inputStr.Replace("\'", "\'\'");
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        //private 



        #region IPersistenceProvider Members

        public PersistableCollectionProxy<T> GetCollectionProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity
        {
            SqlCollectionProxy<T> proxy = null;
            switch (typeof(T).Name)
            {
                case "MaterialEntity":
                    proxy = new SqlMaterialCollectionProxy(collection as MaterialCollection, BretonInterface.Clone()) as SqlCollectionProxy<T>;
                    break;

                case "ArticleEntity":
                    proxy = new SqlArticleCollectionProxy(collection as ArticleCollection, BretonInterface.Clone()) as SqlCollectionProxy<T>;
                    break;

                case "SlabEntity":
                    proxy = new SqlSlabCollectionProxy(collection as SlabCollection, BretonInterface.Clone()) as SqlCollectionProxy<T>;
                    break;

                case "SlabFaceEntity":
                    proxy = new SqlSlabFaceCollectionProxy(collection as SlabFaceCollection, BretonInterface.Clone()) as SqlCollectionProxy<T>;
                    break;

                case "OptimizableSlab":
                    proxy = new SqlOptimizableSlabCollectionProxy(collection as OptimizableSlabCollection, BretonInterface.Clone()) as SqlCollectionProxy<T>;
                    break;


            }
            return proxy;

            return proxy;
        }

        public PersistableEntityProxy GetEntityProxy<T>(T entity) where T : BasePersistableEntity
        {
            PersistableEntityProxy proxy = null;
            switch (entity.GetType().Name)
            {
                case "MaterialEntity":
                    return new SqlMaterialProxy(entity as MaterialEntity, this);
                    break;

                case "MaterialClassEntity":

                    break;

                case "ArticleEntity":
                    return new SqlArticleProxy(entity as ArticleEntity, this);
                    break;

                case "SlabEntity":
                    return new SqlSlabProxy(entity as SlabEntity, this);
                    break;

                case "SlabFaceEntity":
                    return new SqlSlabFaceProxy(entity as SlabFaceEntity, this);
                    break;

                case "OptimizableSlab":
                    return new SqlOptimizableSlabProxy(entity as OptimizableSlab, this);
                    break;



                default:

                    break;
            }

            return proxy;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }

}
