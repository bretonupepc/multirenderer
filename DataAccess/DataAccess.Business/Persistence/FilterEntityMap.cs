﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Business.BusinessBase;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Classe di supporto per creazione mappe di filtro entità 
    /// </summary>
    public class FilterEntityMap
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _mainTableName = string.Empty;

        private string _entityName = string.Empty;

        private string _alias = string.Empty;

        private Dictionary<string, DbField> _entitiesFieldMaps = null;

        private List<string> _relations = new List<string>(); 

        private List<string> _fieldClauses = new List<string>(); 

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Nome tabella di riferimento su database
        /// </summary>
        public string MainTableName
        {
            get { return _mainTableName; }
            set { _mainTableName = value; }
        }

        /// <summary>
        /// Nome entità
        /// </summary>
        public string EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }

        /// <summary>
        /// Alias da utilizzare nella costruzione query
        /// </summary>
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        /// <summary>
        /// Mappe campi dell'entità
        /// </summary>
        public Dictionary<string, DbField> EntitiesFieldMaps
        {
            get { return _entitiesFieldMaps; }
            set { _entitiesFieldMaps = value; }
        }

        /// <summary>
        /// Relazioni
        /// </summary>
        public List<string> Relations
        {
            get { return _relations; }
        }

        /// <summary>
        /// Clausole di filtro
        /// </summary>
        public List<string> FieldClauses
        {
            get { return _fieldClauses; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
