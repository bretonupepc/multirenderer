﻿namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Classe per definire il criterio di ordinamento su un campo
    /// </summary>
    public class FieldSort
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _fieldName;
        private SortOrder _sort = SortOrder.Ascending;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="fieldName">Nome campo</param>
        /// <param name="sort">Tipo ordinamento</param>
        public FieldSort(string fieldName, SortOrder sort)
        {
            _fieldName = fieldName;
            _sort = sort;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Nome campo
        /// </summary>
        public string FieldName
        {
            get { return _fieldName; }
        }

        /// <summary>
        /// Tipoordinamento
        /// </summary>
        public SortOrder Sort
        {
            get { return _sort; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
