﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Classe attributo per definizione tipo Provider da usare nel discovery
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class PersistenceProviderAttribute : Attribute
    {
        private PersistenceProviderType _providerType;

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        public PersistenceProviderAttribute(PersistenceProviderType providerType)
            : base()
        {
            _providerType = providerType;
        }

        /// <summary>
        /// Tipo provider
        /// </summary>
        public PersistenceProviderType ProviderType
        {
            get { return _providerType; }
        }
    }

}
