﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using DataAccess.Business.BusinessBase;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Tipo provider di accesso dati
    /// </summary>
    public enum PersistenceProviderType
    {
        /// <summary>
        /// ShopMaster Sql Server
        /// </summary>
        SqlServer,  // ShopMaster Sql Server

        /// <summary>
        /// Generico Xml
        /// </summary>
        Xml,        // Generico Xml

        /// <summary>
        /// FkSql Sql Server
        /// </summary>
        FkSql       // FkSql Sql Server
    }

    /// <summary>
    /// Gestore di accesso ai dati
    /// </summary>
    public class PersistenceManager:INotifyPropertyChanged
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private static PersistenceManager _default = null;

        private List<PersistenceProviderType> _providerTypes;

        private readonly List<IPersistenceProvider> _availableProviders = new List<IPersistenceProvider>(); 

        //private SqlServerProvider _sqlServerProvider = null;

        //private XmlProvider _defaultXmlProvider = null;

        private PersistenceProviderType _defaultRepositoryProviderType = PersistenceProviderType.SqlServer;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// 
        /// </summary>
        public PersistenceManager()
        {
            GetProviderTypes();
            DiscoverProviders();
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// 
        /// </summary>
        public static PersistenceManager Default
        {
            get
            {
                if (_default == null)
                {
                    _default = new PersistenceManager();
                }
                return _default;
            }
        }

        /// <summary>
        /// Elenco tipi provider 
        /// </summary>
        public List<PersistenceProviderType> ProviderTypes
        {
            get { return _providerTypes; }
        }

        /// <summary>
        /// Numero thread asincroni attivi
        /// </summary>
        public int AsyncSqlThreadsCount
        {
            get
            {
                var provider = GetProvider(PersistenceProviderType.SqlServer);
                if (provider != null && provider.Workers != null)
                {
                    return provider.Workers.Count;
                }

                return 0;
            }
        }

        /// <summary>
        /// Elenco tipi provider disponibili
        /// </summary>
        public List<IPersistenceProvider> AvailableProviders
        {
            get { return _availableProviders; }
        }

        /// <summary>
        /// Tipo provider default 
        /// </summary>
        public PersistenceProviderType DefaultRepositoryProviderType
        {
            get { return _defaultRepositoryProviderType; }
            set { _defaultRepositoryProviderType = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Restituisce il proxy default per singola entità
        /// </summary>
        /// <param name="entity">Entità</param>
        /// <param name="providerType">Tipo provider richiesto</param>
        /// <param name="saveMode">Modalità salvataggio dati (true= salvataggio dati, false = lettura dati)</param>
        /// <returns>Proxy per entità</returns>
        public PersistableEntityProxy GetDefaultProxy(BasePersistableEntity entity, PersistenceProviderType providerType = PersistenceProviderType.SqlServer, bool saveMode = false)
        {
            PersistableEntityProxy proxy = null;

            var provider = GetProvider(providerType);
            if (provider != null)
            {
                proxy = provider.GetDefaultEntityProxy(entity, provider.SaveMode);
            }
            //switch (providerType)
            //{
            //    case PersistenceProviderType.SqlServer:
            //        proxy = SqlServerProvider.GetDefaultEntityProxy(entity);

            //        break;

            //    case PersistenceProviderType.Xml:
            //        proxy = DefaultXmlProvider.GetDefaultEntityProxy(entity);

            //        break;

            //    default:
            //        break;

            //}

            return proxy;
        }
        
        //public PersistableCollectionProxy<T> GetProxy<T>(PersistableEntityCollection<T> collection, PersistenceProviderType providerType = PersistenceProviderType.SqlServer) where T:BasePersistableEntity
        //{
        //    PersistableCollectionProxy<T> proxy = null;
        //    switch (providerType)
        //    {
        //        case PersistenceProviderType.SqlServer:
        //            proxy = SqlServerProvider.GetCollectionProxy(collection);

        //            break;

        //        case PersistenceProviderType.Xml:
        //            proxy = DefaultXmlProvider.GetCollectionProxy(collection);

        //            break;

        //        default:
        //            break;

        //    }

        //    return proxy;
        //}

        /// <summary>
        /// Recupera il proxy default di gestione dati per collezioni di entità
        /// </summary>
        /// <typeparam name="T">Tipizzazione entità collezione</typeparam>
        /// <param name="collection">Collezione collegata</param>
        /// <param name="providerType">Tipo provider richiesto</param>
        /// <param name="saveMode">Modalità salvataggio dati (true= salvataggio dati, false = lettura dati)</param>
        /// <returns>Proxy di gestione dati collezione</returns>
        public PersistableCollectionProxy<T> GetDefaultProxy<T>(PersistableEntityCollection<T> collection, PersistenceProviderType providerType = PersistenceProviderType.SqlServer, bool saveMode = false) where T : BasePersistableEntity
        {
            PersistableCollectionProxy<T> proxy = null;

            var provider = GetProvider(providerType);
            if (provider != null)
            {
                proxy = provider.GetDefaultCollectionProxy(collection, saveMode);
            }

            //switch (providerType)
            //{
            //    case PersistenceProviderType.SqlServer:
            //        proxy = SqlServerProvider.GetDefaultCollectionProxy(collection);

            //        break;

            //    case PersistenceProviderType.Xml:
            //        proxy = DefaultXmlProvider.GetDefaultCollectionProxy(collection);

            //        break;

            //    default:
            //        break;

            //}

            return proxy;
        }

        /// <summary>
        /// Avvia transazione
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public bool BeginTransaction(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            bool retVal = false;
            var provider = GetProvider(providerType);
            if (provider != null)
            {
                retVal = provider.BeginTransaction();
            }

            return retVal;
        }

        /// <summary>
        /// Chiude transazione
        /// </summary>
        /// <param name="commit">true = commit, false = rollback</param>
        /// <param name="providerType">Tipo provider</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public bool EndTransaction(bool commit, PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            bool retVal = false;
            var provider = GetProvider(providerType);
            if (provider != null)
            {
                retVal = provider.EndTransaction(commit);
            }

            return retVal;
        }

        /// <summary>
        /// Arresta tutti gli working threads
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        public void StopWorkers(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            var provider = GetProvider(providerType);
            if (provider != null)
            {
                provider.StopWorkers();
            }
            //if (_sqlServerProvider != null)
            //{
            //    _sqlServerProvider.StopWorkers();
            //}
        }

        /// <summary>
        /// Amette l'impiego di working threads
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        public void AdmitWorkers(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            var provider = GetProvider(providerType);
            if (provider != null)
            {
                provider.CanStartWorkers = true;
            }
            //if (_sqlServerProvider != null)
            //{
            //    _sqlServerProvider.CanStartWorkers = true;
            //}
        }

        /// <summary>
        /// Restituisce il provider per il tipo richiesto
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <returns></returns>
        public IPersistenceProvider GetProvider(PersistenceProviderType providerType)
        {
            var provider = AvailableProviders.FirstOrDefault(p => p.ProviderType == providerType);

            return provider;
            //switch (providerType)
            //{
            //    case PersistenceProviderType.SqlServer:
            //        return SqlServerProvider;
            //        break;

            //    case PersistenceProviderType.Xml:
            //        return DefaultXmlProvider;
            //        break;

            //}
            //return null;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void GetProviderTypes()
        {
            //TODO: Get from configuration/ Reflection through class attributes
            _providerTypes = new List<PersistenceProviderType>()
            {
                PersistenceProviderType.SqlServer,
                PersistenceProviderType.Xml
            };
        }

        private void DiscoverProviders()
        {
            var providersClasses = GetTypesWithAttribute<PersistenceProviderAttribute>();

            foreach (var item in providersClasses)
            {
                Type t = item.Assembly.GetType(item.FullName);
                if (t != null)
                {
                    var maybeProvider = Activator.CreateInstance(t);
                    if (maybeProvider != null)
                    {
                        var provider = maybeProvider as IPersistenceProvider;
                        if (provider != null)
                        {
                            AvailableProviders.Add(provider);
                            if (provider.ProviderType == PersistenceProviderType.SqlServer)
                            {
                                provider.Workers.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Workers_CollectionChanged);
                            }
                        }
                    }
                }
            }
        }

        private static IEnumerable<Type> GetTypesWithAttribute<TAttribute>(bool inherit = true)
            where TAttribute : System.Attribute
        {
            var x = AppDomain.CurrentDomain.GetAssemblies();

            string path = Assembly.GetExecutingAssembly().Location;

            string folderPath = System.IO.Path.GetDirectoryName(path);

            string[] persistenceDlls = Directory.GetFiles(folderPath, "DataAccess.Persistence*.dll");

            foreach (var item in persistenceDlls)
            {
                var existent = x.FirstOrDefault(a => !a.FullName.StartsWith("Anonym") && a.Location == item);
                if (existent == null)
                {
                    Assembly assembly = Assembly.LoadFrom(item);
                }
            }


            return from a in AppDomain.CurrentDomain.GetAssemblies()
                where a.FullName.StartsWith("DataAccess")
                from t in a.GetTypes()
                where t.IsDefined(typeof(TAttribute), inherit)
                select t;

            //Assembly dummy = Assembly.GetExecutingAssembly();

            //return from t in Assembly.GetExecutingAssembly().GetTypes()
            //       where t.IsDefined(typeof(TAttribute), inherit)
            //       select t;

        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates

        void Workers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("AsyncSqlThreadsCount");
        }

        #endregion Delegates  -----------<

        //public SqlServerProvider SqlServerProvider
        //{
        //    get
        //    {


        //        if (_sqlServerProvider == null)
        //        {
        //            _sqlServerProvider = new SqlServerProvider();
        //        }
        //        return _sqlServerProvider;
        //    }
        //}

        //public XmlProvider DefaultXmlProvider
        //{
        //    get
        //    {
        //        if (_defaultXmlProvider == null)
        //        {
        //            _defaultXmlProvider = new XmlProvider();
        //        }
        //        return _defaultXmlProvider;
        //    }
        //}


        //public PersistableEntityProxy GetProxy(BasePersistableEntity entity, PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        //{
        //    PersistableEntityProxy proxy = null;
        //    switch (providerType)
        //    {
        //        case PersistenceProviderType.SqlServer:
        //            proxy = SqlServerProvider.GetEntityProxy(entity);

        //            break;

        //        case PersistenceProviderType.Xml:
        //            proxy = DefaultXmlProvider.GetEntityProxy(entity);

        //            break;

        //        default:
        //            break;

        //    }

        //    return proxy;
        //}

        #region INotifyPropertyChanged Members

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
