﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using DataAccess.Business.BusinessBase;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Proxy gestione accesso dati per collezioni
    /// </summary>
    /// <typeparam name="T">Tipizzazione entità</typeparam>
    public class PersistableCollectionProxy<T> where T : BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="collection">Collezione di entità</param>
        public PersistableCollectionProxy(PersistableEntityCollection<T> collection)
        {
            _collection = collection;
        }

        public PersistableCollectionProxy()
        {
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields

        protected PersistableEntityCollection<T> _collection;

        protected FieldsSet _fieldsInfos = null;

        protected string _mainTableName = string.Empty;


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Collezione collegata
        /// </summary>
        public PersistableEntityCollection<T> Collection
        {
            get { return _collection; }
            set { _collection = value; }
        }

        /// <summary>
        /// Insieme informazioni di campo
        /// </summary>
        public FieldsSet FieldsInfos
        {
            get
            {
                if (_fieldsInfos == null)
                {
                    _fieldsInfos = new FieldsSet();
                }
                return _fieldsInfos;
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Popola la collezione
        /// </summary>
        /// <param name="set">Set impostazioni campi da estrarre</param>
        /// <param name="clause">Set clausole di filtro</param>
        /// <param name="sortSet">Set calusole di ordinamento</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetItems(FieldsSet set = null, FilterClause clause = null, FieldsSortSet sortSet = null)
        {
            return false;
        }

        /// <summary>
        /// Popola la collezione
        /// </summary>
        /// <param name="errors"></param>
        /// <param name="set">Set impostazioni campi da estrarre</param>
        /// <param name="clause">Set clausole di filtro</param>
        /// <param name="sortSet">Set calusole di ordinamento</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetItems(out string errors, FieldsSet set = null, FilterClause clause = null, FieldsSortSet sortSet = null )
        {
            errors = string.Empty;

            return false;
        }

        /// <summary>
        /// Popola la collezione da un nodo xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <param name="set">Set impostazioni campi da estrarre</param>
        /// <param name="clause">Set clausole di filtro</param>
        /// <param name="sortSet">Set calusole di ordinamento</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetItems(XElement node, FieldsSet set = null, FilterClause clause = null, FieldsSortSet sortSet = null)
        {

            return false;
        }

        /// <summary>
        /// Popola la collezione da un file xml
        /// </summary>
        /// <param name="filepath">Percorso file xml</param>
        /// <param name="set">Set impostazioni campi da estrarre</param>
        /// <param name="clause">Set clausole di filtro</param>
        /// <param name="sortSet">Set calusole di ordinamento</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetItems(string filepath, FieldsSet set = null, FilterClause clause = null, FieldsSortSet sortSet = null)
        {

            return false;
        }

        /// <summary>
        /// Aggiorna gli elementi della collezione
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool UpdateItems()
        {
            return true;
        }

        /// <summary>
        /// Aggiorna gli elementi della collezione
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool UpdateItems(out string errors)
        {
            errors = string.Empty;

            return true;
        }

        /// <summary>
        /// Aggiorna gli elementi della collezione
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool SaveItems()
        {
            return false;
        }

        /// <summary>
        /// Aggiorna gli elementi della collezione
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool SaveItems(out string errors)
        {
            errors = string.Empty;

            return false;
        }


        /// <summary>
        /// Salva gli elementi della collezione su un nodo xml
        /// </summary>
        /// <param name="rootNode">Nodo xml di destinazione</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool SaveItems(out XElement rootNode)
        {
            rootNode = null;
            return false;
        }

        /// <summary>
        /// Salva gli elementi della collezione su un file xml
        /// </summary>
        /// <param name="filepath">Percorso del file xml</param>
        /// <param name="overwriteIfExistent">Flag di sovrascrittura</param>
        /// <returns></returns>
        public virtual bool SaveItems(string filepath, bool overwriteIfExistent = true)
        {

            return false;
        }

        /// <summary>
        /// Elimina un elemento della collezione
        /// </summary>
        /// <param name="item">Elemento da eliminare</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool DeleteItem(T item)
        {
            return true;
        }

        /// <summary>
        /// Elimina un elemento della collezione
        /// </summary>
        /// <param name="item">Elemento da eliminare</param>
        /// <param name="errors"></param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool DeleteItem(T item, out string errors)
        {
            errors = string.Empty;

            return true;
        }

        /// <summary>
        /// Elimina un insieme di elementi 
        /// </summary>
        /// <param name="items">Elementi da eliminare</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool DeleteItems(IEnumerable<T> items)
        {
            return true;
        }

        /// <summary>
        /// Elimina un insieme di elementi 
        /// </summary>
        /// <param name="items">Elementi da eliminare</param>
        /// <param name="errors"></param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool DeleteItems(IEnumerable<T> items, out string errors)
        {
            errors = string.Empty;

            return true;
        }

        /// <summary>
        /// Elimina un insieme di elementi secondo il filtro specificato
        /// </summary>
        /// <param name="filterclause">Filtro di selezione</param>
        /// <returns>True if successful</returns>
        public virtual bool DeleteItems(FilterClause filterclause)
        {
            return false;
        }

        /// <summary>
        /// Elimina un insieme di elementi secondo il filtro specificato
        /// </summary>
        /// <param name="filterclause">Filtro di selezione</param>
        /// <param name="errors"></param>
        /// <returns>True if successful</returns>
        public virtual bool DeleteItems(FilterClause filterclause, out string errors)
        {
            errors = string.Empty;

            return false;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// Restituisce il nome tabella di riferimento su database
        /// </summary>
        /// <param name="type">Tipo entità</param>
        /// <param name="persistenceProviderType">Tipo provider</param>
        /// <returns>Nome tabella</returns>
        protected string GetMainDbTableName(Type type, PersistenceProviderType persistenceProviderType)
        {
            string tableName = string.Empty;

            MemberInfo info = type;
            var attribute = info.GetCustomAttributes(true).Where(a => a is MainDbTableAttribute)
                .FirstOrDefault(a => ((MainDbTableAttribute)a).ProviderType == persistenceProviderType);
            if (attribute != null)
                tableName = ((MainDbTableAttribute)attribute).MainDbTable;

            return tableName;
        }


        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        /// <summary>
        /// Costruisce l'insieme definizione campi di default
        /// </summary>
        protected virtual void BuildDefaultFieldsInfos()
        {
            _fieldsInfos = new FieldsSet();
            foreach (FieldItemBaseInfo baseInfo in PersistableEntityBaseInfos.GetBaseInfos(typeof(T)))
            {
                FieldItemInfo info = new FieldItemInfo(baseInfo);
                FieldsInfos.Add(info);
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
