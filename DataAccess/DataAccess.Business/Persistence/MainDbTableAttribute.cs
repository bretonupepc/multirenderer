﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Attributo per associazione tabella di riferimento su database ad una specifica entità
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class MainDbTableAttribute : Attribute
    {
        private PersistenceProviderType _providerType;

        private string _mainDbTable;

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="providerType">Tipo provider</param>
        /// <param name="mainDbTable">Nome tabella su database</param>
        public MainDbTableAttribute(PersistenceProviderType providerType, string mainDbTable)
            : base()
        {
            _providerType = providerType;
            _mainDbTable = mainDbTable;
        }

        /// <summary>
        /// Tipo provider
        /// </summary>
        public PersistenceProviderType ProviderType
        {
            get { return _providerType; }
        }

        /// <summary>
        /// Nome tabella su database
        /// </summary>
        public string MainDbTable
        {
            get { return _mainDbTable; }
        }
    }

}
