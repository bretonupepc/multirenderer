﻿using System.Collections.Generic;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Tipo ordinamento
    /// </summary>
    public enum SortOrder
    {
        /// <summary>
        /// Crescente
        /// </summary>
        Ascending,

        /// <summary>
        /// Descrescente
        /// </summary>
        Descending
    }

    /// <summary>
    /// Lista specializzata di ordinamento campi
    /// </summary>
    public class FieldsSortSet:List<FieldSort>
    {
    }
}
