﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TraceLoggers;
using DataAccess.Business.BusinessBase;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Operatore condizionale per clausole di filtro
    /// </summary>
    public enum ConditionOperator
    {
        /// <summary>
        /// Uguale a
        /// </summary>
        Eq,

        /// <summary>
        /// Diverso da
        /// </summary>
        Ne,

        /// <summary>
        /// Minore di
        /// </summary>
        Lt,

        /// <summary>
        /// Minore o uguale a
        /// </summary>
        Le,
        
        /// <summary>
        /// Maggiore di 
        /// </summary>
        Gt,

        /// <summary>
        /// Maggiore o uguale a
        /// </summary>
        Ge,

        /// <summary>
        /// Like
        /// </summary>
        Like,

        /// <summary>
        /// Interno a insieme di valori ("In")
        /// </summary>
        In,

        /// <summary>
        /// Valore nullo
        /// </summary>
        IsNull,

        /// <summary>
        /// Valore non nullo
        /// </summary>
        IsNotNull
    }

    /// <summary>
    /// Connettore logico per clausole di filtro
    /// </summary>
    public enum LogicalConnector
    {
        /// <summary>
        /// AND
        /// </summary>
        And,

        /// <summary>
        /// AND NOT
        /// </summary>
        AndNot,

        /// <summary>
        /// OR
        /// </summary>
        Or,

        /// <summary>
        /// OR NOT
        /// </summary>
        OrNot
    }

    /// <summary>
    /// Classe di definizione clausola di filtro
    /// </summary>
    public class FilterClause
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private FieldItemBaseInfo _fieldInfo;

        private ConditionOperator _operator;

        private List<object> _values;



        private FilterClause _parentClause = null;

        private LogicalConnector _parentConnector;

        private List<FilterClause> _childrenClauses = null;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// Costruttore con parametri per valore confronto singolo
        /// </summary>
        /// <param name="fieldInfo">Informazioni del campo</param>
        /// <param name="operator">Operatore condizionale</param>
        /// <param name="value">Valore di confronto</param>
        public FilterClause(FieldItemBaseInfo fieldInfo, ConditionOperator @operator, object value)
        {
            _fieldInfo = fieldInfo;
            _operator = @operator;
            _values = new List<object>(){value};
        }

        /// <summary>
        /// Costruttore con parametri per valori confronto multipli
        /// </summary>
        /// <param name="fieldInfo">Informazioni del campo</param>
        /// <param name="operator">Operatore condizionale</param>
        /// <param name="values">Valori di confronto</param>
        public FilterClause(FieldItemBaseInfo fieldInfo, ConditionOperator @operator, List<object> values)
        {
            _fieldInfo = fieldInfo;
            _operator = @operator;
            _values = new List<object>(){values};
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Informazioni del campo
        /// </summary>
        public FieldItemBaseInfo FieldInfo
        {
            get { return _fieldInfo; }
        }

        /// <summary>
        /// Operatore condizionale
        /// </summary>
        public ConditionOperator Operator
        {
            get { return _operator; }
        }

        public List<object> Values
        {
            get { return _values; }
        }

        /// <summary>
        /// Clausola genitore
        /// </summary>
        public FilterClause ParentClause
        {
            get { return _parentClause; }
            set { _parentClause = value; }
        }

        /// <summary>
        /// Connettore logico al genitore
        /// </summary>
        public LogicalConnector ParentConnector
        {
            get { return _parentConnector; }
            set { _parentConnector = value; }
        }

        /// <summary>
        /// Clausole figlie
        /// </summary>
        public List<FilterClause> ChildrenClauses
        {
            get
            {
                if (_childrenClauses == null)
                {
                    _childrenClauses = new List<FilterClause>();
                }
                return _childrenClauses;
            }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Aggiunge una clausola figlia
        /// </summary>
        /// <param name="connector">Connettore logico</param>
        /// <param name="clause">Clausola da aggiungere</param>
        /// <returns>Clausola aggiunta</returns>
        public FilterClause AddClause(LogicalConnector connector, FilterClause clause)
        {
            if (clause != null)
            {
                clause.ParentClause = this;
                clause.ParentConnector = connector;
                ChildrenClauses.Add(clause);
            }
            return clause;
        }

        /// <summary>
        /// Popola la mappa delle clausole di filtro per tipo entità e tipo provider
        /// </summary>
        /// <param name="classType">Tipo entità</param>
        /// <param name="maps">Elenco mappe clausole filtro da popolare</param>
        /// <param name="providerType">Tipo provider</param>
        public void BuildFilterEntityMaps(Type classType, List<FilterEntityMap> maps, PersistenceProviderType providerType)
        {
            IPersistenceProvider provider = PersistenceManager.Default.GetProvider(providerType);

            var masterTablename = provider.GetMainDbTableName(classType);
            var masterFieldsMap = provider.GetEntityFieldsMap(classType);

            FilterEntityMap masterFem;
            if (!maps.Any(m => m.MainTableName == masterTablename))
            {
                masterFem = new FilterEntityMap();
                masterFem.MainTableName = masterTablename;
                masterFem.Alias = GetNewAlias(maps);
                maps.Add(masterFem);
            }

            string clausePropertyName = FieldInfo.PropertyName;
            if (clausePropertyName.Contains("."))
            {
                var elements = clausePropertyName.Split(".".ToCharArray()[0]);

                Type parentClassType = classType;
                for (int i = 0; i < elements.Length - 1; i++)
                {
                    AddRelations(elements[i], elements[i + 1], maps, parentClassType, provider);
                    //parentClassType = GetParentClassType(classType, elements[i], clausePropertyName);
                    parentClassType = GetParentClassType(parentClassType, elements[i], clausePropertyName);
                }

            }

            //Iteration+recursion

            foreach (var clause in ChildrenClauses)
            {
                clause.BuildFilterEntityMaps(classType, maps, providerType);
            }

        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void AddValueConstrain(string p1, string p2, List<FilterEntityMap> maps, Type parentClassType, IPersistenceProvider provider)
        {
            //throw new NotImplementedException();
        }

        private void AddRelations(string master, string slave, List<FilterEntityMap> maps, Type classType, IPersistenceProvider provider)
        {

            var masterTablename = provider.GetMainDbTableName(classType);
            var masterFieldsMap = provider.GetEntityFieldsMap(classType);

            FilterEntityMap masterFem;
            if (!maps.Any(m => m.MainTableName == masterTablename))
            {
                masterFem = new FilterEntityMap();
                masterFem.MainTableName = masterTablename;
                masterFem.Alias = GetNewAlias(maps);
                masterFem.EntityName = this.FieldInfo.PropertyName.Substring(0,
                    FieldInfo.PropertyName.IndexOf(master) + master.Length);
                masterFem.EntitiesFieldMaps = masterFieldsMap;
                maps.Add(masterFem);
            }
            else
            {
                masterFem = maps.FirstOrDefault(m => m.MainTableName == masterTablename);
            }


            var masterPropertyName = master;
            var slaveClassInfos = PersistableEntityBaseInfos.GetBaseInfos(classType);
            var slavePropertyInfo = slaveClassInfos.FirstOrDefault(i => i.PropertyName == masterPropertyName);

            //if (slavePropertyInfo == null)
            //{
            //    TraceLog.WriteLine(string.Format("BuildFilterEntityMaps: info not found for property {0}, hierarchy {1} in type {2}",
            //        masterPropertyName, clausePropertyName, classType.ToString()));
            //}
            //else if (slavePropertyInfo.Relation == null)
            //{
            //    TraceLog.WriteLine(string.Format("BuildFilterEntityMaps: relation info not found for property {0}, hierarchy {1} in type {2}",
            //        masterPropertyName, clausePropertyName, classType.ToString()));
            //}
            if (slavePropertyInfo != null)
            {
                Type slavePropertyType = slavePropertyInfo.PropertyType;

                var slaveTablename = provider.GetMainDbTableName(slavePropertyType);
                var slaveFieldsMap = provider.GetEntityFieldsMap(slavePropertyType);

                FilterEntityMap slaveFem;
                if (!maps.Any(m => m.MainTableName == slaveTablename))
                {
                    slaveFem = new FilterEntityMap();
                    slaveFem.MainTableName = slaveTablename;
                    slaveFem.Alias = GetNewAlias(maps);
                    slaveFem.EntityName = this.FieldInfo.PropertyName.Substring(0,
                        FieldInfo.PropertyName.IndexOf(master) + master.Length);
                    slaveFem.EntitiesFieldMaps = slaveFieldsMap;
                    maps.Add(slaveFem);
                }
                else
                {
                    slaveFem = maps.FirstOrDefault(m => m.MainTableName == slaveTablename);
                }
                string relationClause = string.Format("{0}.{1} = {2}.{3}",
                    masterFem.Alias, masterFieldsMap[slavePropertyInfo.Relation.SourceKey].Name,
                    slaveFem.Alias, slaveFieldsMap[slavePropertyInfo.Relation.RelatedKey].Name);

                if (!slaveFem.Relations.Contains(relationClause))
                    slaveFem.Relations.Add(relationClause);

            }
        }

        private static Type GetParentClassType(Type classType, string element, string clausePropertyName)
        {
            Type propertyType = default(Type);

            var classInfos = PersistableEntityBaseInfos.GetBaseInfos(classType);

            var slavePropertyInfo = classInfos.FirstOrDefault(i => i.PropertyName == element);
            if (slavePropertyInfo == null)
            {
                TraceLog.WriteLine(
                    string.Format(
                        "BuildFilterEntityMaps: info not found for property {0}, hierarchy {1} in type {2}",
                        element, clausePropertyName, classType.ToString()));
            }
            else if (slavePropertyInfo.Relation == null)
            {
                TraceLog.WriteLine(
                    string.Format(
                        "BuildFilterEntityMaps: relation info not found for property {0}, hierarchy {1} in type {2}",
                        element, clausePropertyName, classType.ToString()));
            }
            else
            {
                propertyType = slavePropertyInfo.PropertyType;
            }

            return propertyType;
        }

        private string GetNewAlias(List<FilterEntityMap> maps)
        {
            int index = (int)'A' + maps.Count;
            return ((char) index).ToString(CultureInfo.InvariantCulture);
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
    
}
