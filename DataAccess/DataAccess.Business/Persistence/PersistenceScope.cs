﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Business.DATASOURCES;

namespace DataAccess.Business.Persistence
{
    public class PersistenceScope
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _name;

        private PersistenceManager _manager;

        private DataSources _dataSources;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        /// <summary>
        /// 
        /// </summary>
        public PersistenceScope(string name)
        {
            _name = name;
            _manager = new PersistenceManager();
            _dataSources = new DataSources(this);
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        /// <summary>
        /// 
        /// </summary>
        public PersistenceManager Manager
        {
            get { return _manager; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DataSources DataSources
        {
            get { return _dataSources; }
        }

        public string Name
        {
            get { return _name; }
        }
    }
}
