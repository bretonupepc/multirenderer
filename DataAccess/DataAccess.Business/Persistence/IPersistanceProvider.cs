﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.COMMON;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Interfaccia funzionale per Provider di persistenza
    /// </summary>
    public interface IPersistenceProvider:  INotifyPropertyChanged
    {
        //PersistableCollectionProxy<T> GetCollectionProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity;

        /// <summary>
        /// Recupera il proxy default di gestione dati per collezioni di entità
        /// </summary>
        /// <typeparam name="T">Tipizzazione entità</typeparam>
        /// <param name="collection">Collezione di entità</param>
        /// <param name="saveMode">Modalità salvataggio dati (true= salvataggio dati, false = lettura dati)</param>
        /// <returns>Proxy di gestione dati</returns>
        PersistableCollectionProxy<T> GetDefaultCollectionProxy<T>(PersistableEntityCollection<T> collection, bool saveMode = false) where T : BasePersistableEntity;
        
        //PersistableEntityProxy GetEntityProxy<T>(T entity) where T:BasePersistableEntity;

        /// <summary>
        /// Recupera il proxy default di gestione dati per singola entità
        /// </summary>
        /// <typeparam name="T">Tipizzazione entità</typeparam>
        /// <param name="entity">Entità</param>
        /// <param name="saveMode">Modalità salvataggio dati (true= salvataggio dati, false = lettura dati)</param>
        /// <returns>Proxy di gestione dati</returns>
        PersistableEntityProxy GetDefaultEntityProxy<T>(T entity, bool saveMode = false) where T : BasePersistableEntity;

        /// <summary>
        /// Working threads
        /// </summary>
        SafeCollection<Thread> Workers { get; set; }

        /// <summary>
        /// Definisce se gli working threads possono essere avviati
        /// </summary>
        bool CanStartWorkers { get; set; }

        /// <summary>
        /// Modalità salvataggio dati (true= salvataggio dati, false = lettura dati)
        /// </summary>
        bool SaveMode { get; }

        /// <summary>
        /// Arresta tutti gli working threads
        /// </summary>
        void StopWorkers();

        /// <summary>
        /// Tipo provider
        /// </summary>
        PersistenceProviderType ProviderType { get; }

        /// <summary>
        /// Avvia transazione
        /// </summary>
        /// <returns>True se transazione avviata con successo</returns>
        bool BeginTransaction();

        /// <summary>
        /// Chiude transazione
        /// </summary>
        /// <param name="commit">true=commit, false=rollback</param>
        /// <returns>true = operazione eseguita con successo, false = operazione fallita</returns>
        bool EndTransaction(bool commit);

        /// <summary>
        /// Recupera il nome tabella di riferimento su database per un Tipo specifico
        /// </summary>
        /// <param name="type">Tipo entità</param>
        /// <returns>Nome tabella</returns>
        string GetMainDbTableName(Type type);

        /// <summary>
        /// Estrae la mappa campi per una specifica entità
        /// </summary>
        /// <param name="type">Tipo entità</param>
        /// <returns>Mappa campi</returns>
        Dictionary<string, DbField> GetEntityFieldsMap(Type type);
    }
}
