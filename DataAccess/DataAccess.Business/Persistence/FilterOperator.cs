﻿namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Tipo valore per operatori condizionali di filtro
    /// </summary>
    public enum ConditionOperatorValueType
    {
        /// <summary>
        /// Richiede valore singolo
        /// </summary>
        SingleValueRequired,

        /// <summary>
        /// Richiede valori multipli
        /// </summary>
        MultipleValuesRequired,

        /// <summary>
        /// Non richiede valori
        /// </summary>
        NoValueRequired
    }

    /// <summary>
    /// Classe per operatori di filtro
    /// </summary>
    public class FilterOperator
    {
        private ConditionOperator _operator;
        private ConditionOperatorValueType _valueType;
        private string _operatorRender;

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="operator">Operatore logico di filtro</param>
        /// <param name="valueType">Tipo valore richiesto dall'operatore</param>
        /// <param name="operatorRender">Restituzione in formato stringa dell'operatore condizionale</param>
        public FilterOperator(ConditionOperator @operator, ConditionOperatorValueType valueType, string operatorRender)
        {
            _operator = @operator;
            _valueType = valueType;
            _operatorRender = operatorRender;
        }

        /// <summary>
        /// Operatore condizionale
        /// </summary>
        public ConditionOperator Operator
        {
            get { return _operator; }
        }

        /// <summary>
        /// Tipo valore richiesto
        /// </summary>
        public ConditionOperatorValueType ValueType
        {
            get { return _valueType; }
        }

        /// <summary>
        /// Restituzione in formato stringa dell'operatore condizionale
        /// </summary>
        public string OperatorRender
        {
            get { return _operatorRender; }
        }
    }
}
