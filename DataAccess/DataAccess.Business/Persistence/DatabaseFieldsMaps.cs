﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Business.BusinessBase;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Classe di definizione mappature campi entità su campi database
    /// </summary>
    public class DatabaseFieldsMaps
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        protected PersistenceProviderType _providerType;

        private Dictionary<Type, Dictionary<string, DbField>> _entitiesFieldMaps = null;

        private readonly object _lockObj = new object();

        #endregion Private Fields --------<

        #region >-------------- Constructors

        //public DatabaseFieldsMaps(PersistenceProviderType providerType)
        //{
        //    _providerType = providerType;
        //}

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Mappa interna per tipi entità
        /// </summary>
        protected Dictionary<Type, Dictionary<string, DbField>> EntitiesFieldMaps
        {
            get
            {
                if (_entitiesFieldMaps == null)
                {
                    _entitiesFieldMaps = new Dictionary<Type, Dictionary<string, DbField>>();
                }
                return _entitiesFieldMaps;
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Estrae la mappa campi per una specifica entità
        /// </summary>
        /// <param name="entityType">Tipo entità</param>
        /// <returns>Mappa campi</returns>
        public Dictionary<string, DbField> GetEntityFieldsMap(Type entityType)
        {
            lock (_lockObj)
            {
                if (!EntitiesFieldMaps.ContainsKey(entityType))
                {
                    AddEntityFieldMap(entityType);
                }

            }

            return (EntitiesFieldMaps.ContainsKey(entityType)
                ? EntitiesFieldMaps[entityType]
                : null);
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// Aggiunge la mappa campi per una specifica entità
        /// </summary>
        /// <param name="entityType">Tipo entità</param>
        protected virtual void AddEntityFieldMap(Type entityType)
        {

            var entityFieldMap = new Dictionary<string, DbField>();
            var requestedDict = new Dictionary<string, DbField>();

            List<FieldItemBaseInfo> fieldsInfo = PersistableEntityBaseInfos.GetBaseInfos(entityType);

            if (fieldsInfo == null)
                return;

            requestedDict = GetFieldMapDictionary(entityType);


            foreach (var kvp in requestedDict)
            {
                if (fieldsInfo.Exists(i => i.PropertyName == kvp.Key))
                {
                    entityFieldMap.Add(kvp.Key, kvp.Value);
                }
            }


            EntitiesFieldMaps.Add(entityType, entityFieldMap);

        }

        /// <summary>
        /// Fornisce la mappa campi per una specifica entità
        /// </summary>
        /// <param name="entityType">Tipo entità</param>
        /// <returns>Mappa campi</returns>
        protected virtual  Dictionary<string, DbField> GetFieldMapDictionary(Type entityType)
        {
            var props = entityType.GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(DbFieldAttribute)));
            var dict = new Dictionary<string, DbField>();

            foreach (var prop in props)
            {
                var attribute =
                    prop.GetCustomAttributes(typeof(DbFieldAttribute), true)
                        .FirstOrDefault(a => ((DbFieldAttribute)a).ProviderType == _providerType);
                if (attribute != null)
                {
                    dict.Add(prop.Name, ((DbFieldAttribute)attribute).Field);
                }
            }

            return dict;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }

}
