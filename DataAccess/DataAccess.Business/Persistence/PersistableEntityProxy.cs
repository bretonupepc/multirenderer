﻿using System;
using System.Xml;
using System.Xml.Linq;
using DataAccess.Business.BusinessBase;

namespace DataAccess.Business.Persistence
{
    /// <summary>
    /// Proxy gestione accesso dati per singola entità
    /// </summary>
    public class PersistableEntityProxy
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        //protected Dictionary<string, string> _fieldsMap; 

        //protected internal Dictionary<string, string> FieldsMap
        //{
        //    get { return SqlServerFieldsMaps.GetEntityFieldsMap(this.GetType()); }
        //}

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public PersistableEntityProxy(BasePersistableEntity entity)
        {
            _entity = entity;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields

        protected BasePersistableEntity _entity;

        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Entità collegata
        /// </summary>
        public BasePersistableEntity Entity
        {
            get { return _entity; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Salva l'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Save()
        {
            return false;
        }

        /// <summary>
        /// Salva l'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Save(out string errors )
        {
            errors = string.Empty;
            return Save();
        }

        /// <summary>
        /// Salva l'entità su un nodo xml
        /// </summary>
        /// <param name="rootNode">Nodo xml</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Save(out XElement rootNode)
        {
            rootNode = null;
            return false;
        }

        /// <summary>
        /// Salva l'entità su un file xml
        /// </summary>
        /// <param name="filepath">Percorso file xml</param>
        /// <param name="overwriteIfExistent">Flag sovrascrittura</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Save(string filepath, bool overwriteIfExistent = true)
        {
            
            return false;
        }

        /// <summary>
        /// Rilegge l'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Refresh()
        {
            return false;
        }

        /// <summary>
        /// Rilegge l'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Refresh(out string errors)
        {
            errors = string.Empty;
            return Refresh();
        }


        /// <summary>
        /// Recupera i valori per i campi dell'entità da nodo xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetFields(XElement node)
        {
            return false;

        }

        /// <summary>
        /// Recupera i valori per i campi dell'entità da file xml
        /// </summary>
        /// <param name="filepath">Percorso file xml</param>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetFields(string filepath)
        {
            return false;

        }

        /// <summary>
        /// Recupera i valori per i campi dell'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetFields()
        {
            return false;

        }

        /// <summary>
        /// Recupera i valori per i campi dell'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool GetFields(out string errors)
        {
            errors = string.Empty;
            return GetFields();
        }

        /// <summary>
        /// Elimina l'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Delete()
        {
            return false;
        }

        /// <summary>
        /// Elimina l'entità
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Delete(out string errors)
        {
            errors = string.Empty;

            return Delete();
        }

        /// <summary>
        /// Verifica se l'entità esiste nel repository
        /// </summary>
        /// <returns>true = operazione conclusa con successo, false = operazione fallita</returns>
        public virtual bool Exists()
        {
            return false;
        }

        /// <summary>
        /// Recupera un singolo campo
        /// </summary>
        /// <param name="fieldName">Nome campo</param>
        public virtual void GetSingleField(string fieldName)
        {
            
        }

        /// <summary>
        /// Recupera un singolo campo
        /// </summary>
        /// <param name="fieldName">Nome campo</param>
        /// <param name="errors"></param>
        public virtual void GetSingleField(string fieldName, out string errors)
        {
            errors = string.Empty;

        }

        /// <summary>
        /// Recupera un singolo campo da nodo xml
        /// </summary>
        /// <param name="node">Nodo xml</param>
        /// <param name="fieldName">Nome campo</param>
        public virtual void GetSingleField(XElement node, string fieldName)
        {
            
        }


        #endregion Public Methods -----------<



        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
