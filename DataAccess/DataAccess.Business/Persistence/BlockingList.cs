﻿using System.Collections.Generic;

namespace BrSm.Business.Persistence
{
    public class BlockingList<T> : List<T>
    {
        private readonly object _lockObject = new object();

        public new T this[int index]
        {
            get
            {
                lock (_lockObject)
                {
                    return base[index];
                }
            }
            set
            {
                lock (_lockObject)
                {
                    base[index] = value;
                }

            }
        }
        public new void Add(T item)
        {
            lock (_lockObject)
            {
                if (!Contains(item))
                    base.Add(item);
            }
        }

        public new void Remove(T item)
        {
            lock (_lockObject)
            {
                if (Contains(item))
                    base.Remove(item);
            }
        }

        public new void RemoveAt(int index)
        {
            lock (_lockObject)
            {
                if (Count > index)
                    RemoveAt(index);
            }

        }

    }
}
