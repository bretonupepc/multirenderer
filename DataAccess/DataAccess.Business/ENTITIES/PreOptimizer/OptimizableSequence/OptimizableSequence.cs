﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.ENTITIES.PreOptimizer.Common;
using BrSm.Business.ENTITIES.PreOptimizer;
using BrSm.Business.ENTITIES.SHAPES;

namespace BrSm.Business.ENTITIES.PreOptimizer.OptimizableSequence
{
    public class OptimizableSequence: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _name = string.Empty;

        private OptimizableSlabCollection _slabs;

        private ShapeCollection _shapes;

        //private QualityClassRange _qualityRange;

        private int _minQualityId = 0;

        private int _maxQualityId = int.MaxValue;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Name
        {
            get { return GetProperty("Name", ref _name); }
            set { SetProperty("Name", value, ref _name); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public OptimizableSlabCollection Slabs
        {
            get { return GetProperty("Slabs", ref _slabs); }
            set { SetProperty("Slabs", value, ref _slabs); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public ShapeCollection Shapes
        {
            get { return GetProperty("Shapes", ref _shapes); }
            set { SetProperty("Shapes", value, ref _shapes); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        public int MinQualityId
        {
            get { return GetProperty("MinQualityId", ref _minQualityId); }
            set { SetProperty("MinQualityId", value, ref _minQualityId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int MaxQualityId
        {
            get { return GetProperty("MaxQualityId", ref _maxQualityId); }
            set { SetProperty("MaxQualityId", value, ref _maxQualityId); }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<


        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {

            try
            {
                switch (propertyName)
                {
                    case "Name":
                        base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                            ref _name,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Slabs":
                        base.SetProperty(propertyName, (OptimizableSlabCollection)Convert.ChangeType(value, typeof(OptimizableSlabCollection)),
                            ref _slabs,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Shapes":
                        base.SetProperty(propertyName, (ShapeCollection)Convert.ChangeType(value, typeof(ShapeCollection)),
                            ref _shapes,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "MinQualityId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _minQualityId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "MaxQualityId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _maxQualityId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;



                    default:
                        //base.SetProperty<T>(propertyName, value, forceStatus, newStatus, forceOnChanged, avoidOnChanged,
                        //    avoidOnStatusChanged);
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {

            switch (propertyName)
            {
                case "Name":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _name), typeof(T));
                    break;

                case "Slabs":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _slabs), typeof(OptimizableSlabCollection));
                    break;

                case "Shapes":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _shapes), typeof(ShapeCollection));
                    break;

                case "MinQualityId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _minQualityId), typeof(T));
                    break;

                case "MaxQualityId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _maxQualityId), typeof(T));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }

    }
}
