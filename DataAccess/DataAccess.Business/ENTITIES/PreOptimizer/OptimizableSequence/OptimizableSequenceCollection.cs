﻿using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.OptimizableSequence
{
    public class OptimizableSequenceCollection : PersistableEntityCollection<OptimizableSequence>
    {
    }
}
