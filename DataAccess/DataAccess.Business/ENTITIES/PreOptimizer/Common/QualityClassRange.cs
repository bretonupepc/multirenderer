﻿using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.Common
{
    public class QualityClassRange:BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _minQualityClassId;

        private int _maxQualityClassId;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public int MinQualityClassId
        {
            get { return _minQualityClassId; }
            set
            {
                _minQualityClassId = value;
                OnPropertyChanged("MinQualityClass");
            }
        }

        public int MaxQualityClassId
        {
            get { return _maxQualityClassId; }
            set
            {
                _maxQualityClassId = value;
                OnPropertyChanged("MaxQualityClass");
                
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<


        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false, FieldStatus newStatus = FieldStatus.Set, bool forceOnChanged = false, bool avoidOnChanged = false, bool avoidOnStatusChanged = false)
        {
            throw new System.NotImplementedException();
        }

        public override T GetProperty<T>(string propertyName)
        {
            throw new System.NotImplementedException();
        }
    }
}
