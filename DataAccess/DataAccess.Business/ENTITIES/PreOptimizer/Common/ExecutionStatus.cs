﻿using System;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.Common
{
    public class ExecutionStatus: ObservableObject
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private double _percentageDone;

        private TimeSpan _eta = new TimeSpan();

        private TimeSpan _duration = new TimeSpan();

        private RunStatus _status = RunStatus.Unknown;

        private string _errors = string.Empty;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ExecutionStatus()
        {
            
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public double PercentageDone
        {
            get { return _percentageDone; }
            set
            {
                _percentageDone = value;
                OnPropertyChanged("PercentageDone");

            }
        }


        public TimeSpan Duration
        {
            get { return _duration; }
            set
            {
                _duration = value;
                OnPropertyChanged("Duration");

            }
        }

        public RunStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");

            }
        }

        public string Errors
        {
            get { return _errors; }
            set
            {
                _errors = value;
                OnPropertyChanged("Errors");

            }
        }

        public TimeSpan Eta
        {
            get { return _eta; }
            set
            {
                _eta = value;
                OnPropertyChanged("Eta");
            }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
