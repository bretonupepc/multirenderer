﻿namespace BrSm.Business.ENTITIES.PreOptimizer.Common
{
    public enum RunStatus
    {
        Unknown = 0,
        Awaiting = 1,
        Running = 2,
        Completed = 3,
        StoppedByError = 4,
        AbortedByUser = 5
    }

    public class ConstEnums
    {

    }
}
