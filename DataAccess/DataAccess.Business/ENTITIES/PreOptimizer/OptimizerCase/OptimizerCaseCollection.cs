﻿using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.OptimizerCase
{
    public class OptimizerCaseCollection : PersistableEntityCollection<OptimizerCase>
    {
    }
}
