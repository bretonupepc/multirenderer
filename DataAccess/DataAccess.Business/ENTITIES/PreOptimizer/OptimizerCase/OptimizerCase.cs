﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.ENTITIES.GEOMETRY.MATRICES;
using  BrSm.Business.ENTITIES.PreOptimizer.OptimizableSequence;
using BrSm.Business.ENTITIES.PreOptimizer.RESULTS;
using BrSm.Business.ENTITIES.SHAPES;


namespace BrSm.Business.ENTITIES.PreOptimizer.OptimizerCase
{
    public class OptimizerCase:BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _title = string.Empty;

        private OptimizableSequence.OptimizableSequence _sequence;

        private OptimizerParameters _parameters;

        private OptimizerResult _result;


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Title
        {
            get { return GetProperty("Title", ref _title); }
            set { SetProperty("Title", value, ref _title); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public OptimizableSequence.OptimizableSequence Sequence
        {
            get { return GetProperty("Sequence", ref _sequence); }
            set { SetProperty("Sequence", value, ref _sequence); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public OptimizerParameters Parameters
        {
            get { return GetProperty("Parameters", ref _parameters); }
            set { SetProperty("Parameters", value, ref _parameters); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public OptimizerResult Result
        {
            get { return GetProperty("Result", ref _result); }
            set { SetProperty("Result", value, ref _result); }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "Title":
                        base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                            ref _title,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Sequence":
                        base.SetProperty(propertyName, (OptimizableSequence.OptimizableSequence)Convert.ChangeType(value, typeof(OptimizableSequence.OptimizableSequence)),
                            ref _sequence,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Parameters":
                        base.SetProperty(propertyName, (OptimizerParameters)Convert.ChangeType(value, typeof(OptimizerParameters)),
                            ref _parameters,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Result":
                        base.SetProperty(propertyName, (OptimizerResult)Convert.ChangeType(value, typeof(OptimizerResult)),
                            ref _result,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;


                    default:
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Title":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _title), typeof(T));
                    break;

                case "Sequence":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _sequence), typeof(OptimizableSequence.OptimizableSequence));
                    break;

                case "Parameters":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _parameters), typeof(OptimizerParameters));
                    break;

                case "Result":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _result), typeof(OptimizerResult));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }
    }
}
