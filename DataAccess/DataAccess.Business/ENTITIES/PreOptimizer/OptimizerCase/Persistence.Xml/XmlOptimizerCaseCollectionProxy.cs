﻿using BrSm.Business.ENTITIES.PreOptimizer;
using BrSm.Business.Persistence.Xml;

namespace BrSm.Business.ENTITIES.PreOptimizer.OptimizerCase.Persistence.Xml
{
    public class XmlOptimizerCaseCollectionProxy:XmlCollectionProxy<OptimizerCase> 
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors


        public XmlOptimizerCaseCollectionProxy(OptimizerCaseCollection collection, string mainTableName = "")
            : base(collection, mainTableName)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods


        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
