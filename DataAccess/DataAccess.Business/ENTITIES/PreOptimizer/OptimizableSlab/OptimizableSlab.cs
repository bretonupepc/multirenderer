﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;

namespace BrSm.Business.ENTITIES.PreOptimizer
{
    public class OptimizableSlab: SlabEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        //private int _id = -1;

        private int _priority;

        //private BretonSlabFace _frontFace = null;

        //private BretonSlabFace _rearFace = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public OptimizableSlab()
        {
            
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Priority
        {
            get { return GetProperty("Priority", ref _priority); }
            set { SetProperty("Priority", value, ref _priority); }
        }

        public override T GetProperty<T>(string propertyName)
        {
            {
                switch (propertyName)
                {
                    case "Priority":
                        return (T)Convert.ChangeType(GetProperty(propertyName, ref _priority), typeof(T));
                        break;

                    default:
                        return base.GetProperty<T>(propertyName);
                        break;
                }

            }
            return default(T);
        }

        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
                FieldStatus newStatus = FieldStatus.Set,
                bool forceOnChanged = false,
                bool avoidOnChanged = false,
                bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "Priority":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _priority,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    default:
                        base.SetProperty<T>(propertyName, value, forceStatus, newStatus, forceOnChanged, avoidOnChanged,
                            avoidOnStatusChanged);
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }


    }
}
