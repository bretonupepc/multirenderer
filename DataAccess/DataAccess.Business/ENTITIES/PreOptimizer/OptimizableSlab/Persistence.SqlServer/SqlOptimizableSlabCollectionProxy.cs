﻿using Breton.DbAccess;
using BrSm.Business.BusinessBase;
using BrSm.Business.COMMON;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.Persistence;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.PreOptimizer.Persistence.SqlServer
{
    public class SqlOptimizableSlabCollectionProxy:SqlCollectionProxy<OptimizableSlab> 
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlOptimizableSlabCollectionProxy(OptimizableSlabCollection collection, DbInterface dbInterface, string mainTableName = "T_AN_ARTICLES")
            : base(collection, dbInterface, mainTableName)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods


        private void GetSlabFaces(SlabEntity slab)
        {
            if (slab.IsNewItem)
            {
                return;
            }

            SlabFaceCollection faces = new SlabFaceCollection();
            FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("SlabId", typeof(int)), ConditionOperator.Eq, slab.UniqueId);
            if (faces.GetItemsFromRepository(clause: filterClause))
            {
                if (faces.Count > 0)
                {
                    foreach (var face in faces)
                    {
                        slab.SetProperty(face.FaceId == 0 ? "FrontFace" : "RearFace",
                            face, true, FieldStatus.Read);
                        face.ParentSlab = slab;
                    }
                }
            }


        }



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override string BuildWherePart(FilterClause clause)
        {
            string whereStr = BASE_WHERE;

            bool isValid = false;

            if (clause != null)
            {
                string whereContent;
                isValid = ParseClause(clause, out whereContent);

                if (isValid)
                {
                    whereStr += string.Format(" {0} = {1} AND ", FieldsMap["ArticleType"], (int) ArticleType.SLAB);
                    whereStr += whereContent;
                }
                else
                {
                    whereStr += string.Format(" {0} = {1} ", FieldsMap["ArticleType"], (int)ArticleType.SLAB);
                    isValid = true;
                }

            }
            else
            {
                whereStr += string.Format(" {0} = {1} ", FieldsMap["ArticleType"], (int)ArticleType.SLAB);
                isValid = true;

            }


            return isValid ? whereStr : string.Empty;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
