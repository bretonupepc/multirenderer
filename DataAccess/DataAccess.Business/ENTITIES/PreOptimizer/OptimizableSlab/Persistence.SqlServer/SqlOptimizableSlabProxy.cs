﻿using System;
using System.Drawing;
using System.IO;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.PreOptimizer;
using BrSm.Business.Persistence;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.ARTICLES.SLABS.Persistence.SqlServer
{
    public class SqlOptimizableSlabProxy: SqlEntityProxy
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlOptimizableSlabProxy(OptimizableSlab slab, SqlServerProvider provider, string mainTableName = "T_AN_ARTICLES") 
            : base(slab, provider, mainTableName)
        {
            
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        public override void GetSingleField(string fieldName)
        {
            //switch (fieldName)
            //{
            //    case "ImagePath":
            //    case "ImageThumbnailPath":
            //        Entity.SetProperty(fieldName, "NoImage.png", true, FieldStatus.Read);
            //        break;
            //    default:
            //        break;

            //}

            base.GetSingleField(fieldName);
        }

        protected override void SyncGetSingleField(string fieldName)
        {
            switch (fieldName)
            {
                case "ImagePath":
                    GetImage(fieldName);
                    break;

                case "ImageThumbnailPath":
                    GetThumbnailImage(fieldName);
                    break;

                case "FrontFace":
                    GetFace(0);
                    break;

                case "RearFace":
                    GetFace(1);
                    break;

                default:
                    base.SyncGetSingleField(fieldName);
                    break;
            }

        }

        private void GetFace(int faceProg)
        {
            if (Entity.IsNewItem)
            {
                return;
            }
            SlabFaceCollection faces = new SlabFaceCollection();
            FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("SlabId", typeof(int)), ConditionOperator.Eq, Entity.UniqueId);
            filterClause.AddClause(LogicalConnector.And,
                new FilterClause(new FieldItemBaseInfo("FaceId", typeof(int)), ConditionOperator.Eq, faceProg));
            if (faces.GetItemsFromRepository(clause: filterClause))
            {
                if (faces.Count > 0)
                {
                    (Entity as SlabEntity).SetProperty(faceProg == 0 ? "FrontFace" : "RearFace",
                        faces[0], true, FieldStatus.Read);
                    faces[0].ParentSlab = Entity as SlabEntity;
                }
                else
                {
                    
                }
            }
            else
            {
                
            }



        }

        private void GetImage(string fieldName)
        {
            try
            {
                int imageId = (_entity as ArticleEntity).ImageId;
                if (imageId != -1)
                {
                    if (!Directory.Exists("SLAB_IMAGES"))
                    {
                        Directory.CreateDirectory("SLAB_IMAGES");
                    }
                    string fileName = string.Format(@"SLAB_IMAGES\Article_{0}.jpg", imageId);
                    if (File.Exists(fileName))
                    {
                        //File.Delete(fileName);
                    }

                    if (!File.Exists(fileName))
                    {
                        BretonInterface.GetBlobField("T_AN_ARTICLE_IMAGES", "F_IMAGE",
                            string.Format("F_ID = {0}", imageId), fileName);
                    }
                    //if (BretonInterface.GetImage("T_AN_ARTICLE_IMAGES", "F_IMAGE",
                    //    string.Format("F_ID = {0}", imageId), fileName))
                    {
                        if (File.Exists(fileName))
                        {
                            //using (var stream = new FileStream(fileName, FileMode.Open))
                            //{
                            //    BitmapImage image = new BitmapImage();
                            //    image.BeginInit();
                            //    // According to MSDN, "The default OnDemand cache option retains access to the stream until the image is needed."
                            //    // Force the bitmap to load right now so we can dispose the stream.
                            //    image.CacheOption = BitmapCacheOption.OnLoad;
                            //    image.StreamSource = stream;
                            //    image.EndInit();
                            //    image.Freeze();

                            //    Entity.SetProperty(fieldName, image, true, FieldStatus.Read);
                            //}

                            FileInfo info = new FileInfo(fileName);
                            Entity.SetProperty(fieldName, info.FullName, true, FieldStatus.Read);

                            //using (var bmpTemp = new Bitmap(fileName))
                            //{
                            //    BitmapImage source = (BitmapImage)bmpTemp.ToWpfBitmap();

                            //    Entity.SetProperty(fieldName, source, true, FieldStatus.Read);

                            //}

                            //File.Delete(fileName);

                        }

                    }

                }
                else
                {

                    Entity.SetPropertyStatus(fieldName, FieldStatus.Read);

                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("ArticleEntity GetImage error ", ex);
            }

        }

        private void GetThumbnailImage(string fieldName)
        {
            //return;
            try
            {
                Entity.SetProperty(fieldName, "NoImage.png", true, FieldStatus.Read);

                int imageId = (_entity as ArticleEntity).ImageId;
                if (imageId != -1)
                {
                    if (!Directory.Exists("SLAB_IMAGES"))
                    {
                        Directory.CreateDirectory("SLAB_IMAGES");
                    }
                    string imageFilename = string.Format(@"SLAB_IMAGES\Article_{0}.jpg", imageId);
                    string thumbFilename = string.Format(@"SLAB_IMAGES\Th_Article_{0}.jpg", imageId);


                    if (!File.Exists(thumbFilename))
                    {
                        if (!File.Exists(imageFilename))
                            BretonInterface.GetBlobField("T_AN_ARTICLE_IMAGES", "F_IMAGE",
                                string.Format("F_ID = {0}", imageId), imageFilename);
                        if (File.Exists(imageFilename))
                        {
                            using (var bmpTemp = new Bitmap(imageFilename))
                            {
                                var thumbnail = new Bitmap(bmpTemp, new System.Drawing.Size(300, 200));
                                thumbnail.Save(thumbFilename);
                            }
                        }

                    }
                    //if (BretonInterface.GetImage("T_AN_ARTICLE_IMAGES", "F_IMAGE",
                    //    string.Format("F_ID = {0}", imageId), fileName))
                    {
                        if (File.Exists(thumbFilename))
                        {
                            //using (var stream = new FileStream(fileName, FileMode.Open))
                            //{
                            //    BitmapImage image = new BitmapImage();
                            //    image.BeginInit();
                            //    // According to MSDN, "The default OnDemand cache option retains access to the stream until the image is needed."
                            //    // Force the bitmap to load right now so we can dispose the stream.
                            //    image.CacheOption = BitmapCacheOption.OnLoad;
                            //    image.StreamSource = stream;
                            //    image.EndInit();
                            //    image.Freeze();

                            //    Entity.SetProperty(fieldName, image, true, FieldStatus.Read);
                            //}

                            FileInfo info = new FileInfo(thumbFilename);
                            Entity.SetProperty(fieldName, info.FullName, true, FieldStatus.Read);

                            //using (var bmpTemp = new Bitmap(fileName))
                            //{
                            //    BitmapImage source = (BitmapImage)bmpTemp.ToWpfBitmap();

                            //    Entity.SetProperty(fieldName, source, true, FieldStatus.Read);

                            //}

                            //File.Delete(fileName);

                        }

                    }

                }
                else
                {

                    Entity.SetPropertyStatus(fieldName, FieldStatus.Read);

                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("ArticleEntity GetImage error ", ex);
            }

        }


        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
