﻿using System;
using System.Collections.Generic;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.RESULTS
{
    public class OptimizerResultStatistics: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private double _usedSurfacePercentage;

        private PersistableEntityCollection<QualityClassPercentage> _qualityClassesPercentages = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public OptimizerResultStatistics()
        {
            QualityClassesPercentages = new PersistableEntityCollection<QualityClassPercentage>();
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double UsedSurfacePercentage
        {
            get { return GetProperty("UsedSurfacePercentage", ref _usedSurfacePercentage); }
            set { SetProperty("UsedSurfacePercentage", value, ref _usedSurfacePercentage); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public PersistableEntityCollection<QualityClassPercentage> QualityClassesPercentages
        {
            get { return GetProperty("QualityClassesPercentages", ref _qualityClassesPercentages); }
            set { SetProperty("QualityClassesPercentages", value, ref _qualityClassesPercentages); }
        }

        #region Overrides

        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "UsedSurfacePercentage":
                        base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                            ref _usedSurfacePercentage,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "QualityClassesPercentages":
                        base.SetProperty(propertyName, (PersistableEntityCollection<QualityClassPercentage>)Convert.ChangeType(value, typeof(PersistableEntityCollection<QualityClassPercentage>)),
                            ref _qualityClassesPercentages,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;


                    default:
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "UsedSurfacePercentage":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _usedSurfacePercentage), typeof(T));
                    break;

                case "QualityClassesPercentages":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _qualityClassesPercentages), typeof(PersistableEntityCollection<QualityClassPercentage>));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }
 
	    #endregion    Overrides
    }
}
