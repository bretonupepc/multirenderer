﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.RESULTS
{
    public class QualityClassPercentage: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _qualityClassId = -1;

        private double _percentage;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public QualityClassPercentage()
        {
            
        }

        public QualityClassPercentage(int qualityClassId, double percentage) : this()
        {
            QualityClassId = qualityClassId;
            Percentage = percentage;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int QualityClassId
        {
            get { return GetProperty("QualityClassId", ref _qualityClassId); }
            set { SetProperty("QualityClassId", value, ref _qualityClassId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double Percentage
        {
            get { return GetProperty("Percentage", ref _percentage); }
            set { SetProperty("Percentage", value, ref _percentage); }
        }

        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "QualityClassId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _qualityClassId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Percentage":
                        base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                            ref _percentage,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;


                    default:
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "QualityClassId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _qualityClassId), typeof(T));
                    break;

                case "Percentage":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _percentage), typeof(T));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }
    }
}
