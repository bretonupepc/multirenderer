﻿using System;
using System.Collections.ObjectModel;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.RESULTS
{
    public class OptimizerResult: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private PersistableEntityCollection<OptimizerSlabResult> _slabResults;

        private OptimizerResultStatistics _statistics;

        private DateTime _creationDate;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public OptimizerResult()
        {
            CreationDate = DateTime.Now;
            SlabResults = new PersistableEntityCollection<OptimizerSlabResult>();
            Statistics = new OptimizerResultStatistics();
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public PersistableEntityCollection<OptimizerSlabResult> SlabResults
        {
            get { return GetProperty("SlabResults", ref _slabResults); }
            set { SetProperty("SlabResults", value, ref _slabResults); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public OptimizerResultStatistics Statistics
        {
            get { return GetProperty("Statistics", ref _statistics); }
            set { SetProperty("Statistics", value, ref _statistics); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public DateTime CreationDate
        {
            get { return GetProperty("CreationDate", ref _creationDate); }
            set { SetProperty("CreationDate", value, ref _creationDate); }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<


        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "SlabResults":
                        base.SetProperty(propertyName, (PersistableEntityCollection<OptimizerSlabResult>)Convert.ChangeType(value, typeof(PersistableEntityCollection<OptimizerSlabResult>)),
                            ref _slabResults,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Statistics":
                        base.SetProperty(propertyName, (OptimizerResultStatistics)Convert.ChangeType(value, typeof(OptimizerResultStatistics)),
                            ref _statistics,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "CreationDate":
                        base.SetProperty(propertyName, (DateTime)Convert.ChangeType(value, typeof(DateTime)),
                            ref _creationDate,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    default:
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "SlabResults":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _slabResults), typeof(PersistableEntityCollection<OptimizerSlabResult>));
                    break;

                case "Statistics":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _statistics), typeof(OptimizerResultStatistics));
                    break;

                case "CreationDate":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _creationDate), typeof(T));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }
    }
}
