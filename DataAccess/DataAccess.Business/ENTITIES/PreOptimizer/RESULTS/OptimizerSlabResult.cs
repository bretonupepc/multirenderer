﻿using System;
using System.Collections.ObjectModel;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.PreOptimizer.RESULTS
{
    public class OptimizerSlabResult: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private OptimizableSlab _slab;

        private PersistableEntityCollection<OptimizablePiece> _pieces;

        private OptimizerResultStatistics _statistics;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public OptimizerSlabResult()
        {
            //Slab = new OptimizableSlab();
            Pieces = new PersistableEntityCollection<OptimizablePiece>();
            Statistics = new OptimizerResultStatistics();
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        [PersistableField(FieldRetrieveMode.Immediate)]
        public OptimizableSlab Slab
        {
            get { return GetProperty("Slab", ref _slab); }
            set { SetProperty("Slab", value, ref _slab); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public PersistableEntityCollection<OptimizablePiece> Pieces
        {
            get { return GetProperty("Pieces", ref _pieces); }
            set { SetProperty("Pieces", value, ref _pieces); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public OptimizerResultStatistics Statistics
        {
            get { return GetProperty("Statistics", ref _statistics); }
            set { SetProperty("Statistics", value, ref _statistics); }
        }

        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "Slab":
                        base.SetProperty(propertyName, (OptimizableSlab)Convert.ChangeType(value, typeof(OptimizableSlab)),
                            ref _slab,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Pieces":
                        base.SetProperty(propertyName, (PersistableEntityCollection<OptimizablePiece>)Convert.ChangeType(value, typeof(PersistableEntityCollection<OptimizablePiece>)),
                            ref _pieces,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Statistics":
                        base.SetProperty(propertyName, (OptimizerResultStatistics)Convert.ChangeType(value, typeof(OptimizerResultStatistics)),
                            ref _statistics,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    default:
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Slab":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _slab), typeof(OptimizableSlab));
                    break;

                case "Pieces":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _pieces), typeof(PersistableEntityCollection<OptimizablePiece>));
                    break;

                case "Statistics":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _statistics), typeof(OptimizerResultStatistics));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }
    }
}
