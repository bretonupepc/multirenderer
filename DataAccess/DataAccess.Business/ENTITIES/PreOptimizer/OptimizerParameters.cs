﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.PreOptimizer.RESULTS;

namespace BrSm.Business.ENTITIES.PreOptimizer
{
    public  class OptimizerParameters: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _parameter1;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Parameter1
        {
            get { return GetProperty("Parameter1", ref _parameter1); }
            set { SetProperty("Parameter1", value, ref _parameter1); }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "Parameter1":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _parameter1,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    default:
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Parameter1":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _parameter1), typeof(T));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }
    }
}
