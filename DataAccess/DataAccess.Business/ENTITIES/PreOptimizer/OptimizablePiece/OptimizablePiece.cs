﻿
using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.ENTITIES.GEOMETRY.MATRICES;
using BrSm.Business.ENTITIES.SHAPES;

namespace BrSm.Business.ENTITIES.PreOptimizer
{
    public class OptimizablePiece: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private string _code = string.Empty;


        private int _parentShapeId = -1;
        private ShapeEntity _parentShape = null;

        private int _parentSlabFaceId = -1;
        private SlabFaceEntity _parentSlabFace = null;

        private TransformationMatrixEntity _matrixRt = null;

        private int _qualityId = -1;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public OptimizablePiece()
        {
            MatrixRt = new TransformationMatrixEntity();
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Code
        {
            get { return GetProperty("Code", ref _code); }
            set { SetProperty("Code", value, ref _code); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int ParentShapeId
        {
            get { return GetProperty("ParentShapeId", ref _parentShapeId); }
            set { SetProperty("ParentShapeId", value, ref _parentShapeId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int ParentSlabFaceId
        {
            get { return GetProperty("ParentSlabFaceId", ref _parentSlabFaceId); }
            set { SetProperty("ParentSlabFaceId", value, ref _parentSlabFaceId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public TransformationMatrixEntity MatrixRt
        {
            get { return GetProperty("MatrixRt", ref _matrixRt); }
            set { SetProperty("MatrixRt", value, ref _matrixRt); }
        }

        [PersistableField]
        public ShapeEntity ParentShape
        {
            get { return GetProperty("ParentShape", ref _parentShape); }
            set { SetProperty("ParentShape", value, ref _parentShape); }
        }

        [PersistableField]
        public SlabFaceEntity ParentSlabFace
        {
            get { return GetProperty("ParentSlabFace", ref _parentSlabFace); }
            set { SetProperty("ParentSlabFace", value, ref _parentSlabFace); }
        }

        [PersistableField]
        public int QualityId
        {
            get { return GetProperty("QualityId", ref _qualityId); }
            set { SetProperty("QualityId", value, ref _qualityId); }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "Id":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _id,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Code":
                        base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                            ref _code,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ParentShapeId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _parentShapeId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ParentShape":
                        base.SetProperty(propertyName, (ShapeEntity)Convert.ChangeType(value, typeof(ShapeEntity)),
                            ref _parentShape,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ParentSlabFaceId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _parentSlabFaceId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ParentSlabFace":
                        base.SetProperty(propertyName, (SlabFaceEntity)Convert.ChangeType(value, typeof(SlabFaceEntity)),
                            ref _parentSlabFace,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "MatrixRt":
                        base.SetProperty(propertyName, (TransformationMatrixEntity)Convert.ChangeType(value, typeof(TransformationMatrixEntity)),
                            ref _matrixRt,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "QualityId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _qualityId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    default:
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _id), typeof(T));
                    break;


                case "Code":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _code), typeof(T));
                    break;

                case "ParentShapeId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _parentShapeId), typeof(T));
                    break;

                case "ParentShape":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _parentShape), typeof(T));
                    break;

                case "ParentSlabFaceId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _parentSlabFaceId), typeof(T));
                    break;

                case "ParentSlabFace":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _parentSlabFace), typeof(T));
                    break;

                case "MatrixRt":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _matrixRt), typeof(T));
                    break;

                case "QualityId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _qualityId), typeof(T));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }
    }
}
