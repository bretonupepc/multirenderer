﻿using System;
using System.Drawing;
using System.IO;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.Persistence.SqlServer;
using BrSm.Business.Persistence.Xml;

namespace BrSm.Business.ENTITIES.ARTICLES.Persistence.Xml
{
    public class XmlArticleProxy: XmlEntityProxy
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public XmlArticleProxy(ArticleEntity article, XmlProvider provider, string mainTableName = "") 
            : base(article, provider, mainTableName)
        {
            
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods




        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
