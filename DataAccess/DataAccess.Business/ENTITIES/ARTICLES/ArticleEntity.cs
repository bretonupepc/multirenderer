﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.COMMON;

namespace BrSm.Business.ENTITIES.ARTICLES
{
    public class ArticleEntity: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private string _code = string.Empty;

        private string _description = string.Empty;

        private string _batchCode = string.Empty;

        private double _dimX;
        private double _dimY;
        private double _dimZ;

        private double _offsetX;
        private double _offsetY;

        private double _surface;
        private double _commercialSurface;

        private double _internalRectLeft;
        private double _internalRectTop;
        private double _internalRectWidth;
        private double _internalRectHeight;

        private string _tone = string.Empty;

        private string _xmlParameter = string.Empty;

        private double _positionStopX;
        private double _positionStopY;

        private double _unitCost;

        private int _defects;

        private int _imageId = -1;
        private string _imagePath;
        private BitmapImage _imageSource = null;
        private string _imageThumbnailPath = @"E:\Progetti\BRETON VSS\BrSm\SlabPlanning\bin\Debug\NoImage.png";

        private int _qualityId = -1;


        // 
        private ArticleType _articleType = ArticleType.UNDEFINED;

        private ArticleStatus _articleStatus = ArticleStatus.START_PROGRAMMING;

        private int _materialId = -1;

        private int _stockHusId = -1;

        private int _ddtaDetailsId = -1;

        //private int _stationId;

        private int _supplierId = -1;

        private int _customerProjectId = -1;

        private double _qty = 0;

        private DateTime _insertDate;

        private int _husPosition = -1;

        private int _blobsPartProgramId = -1;

        private int _blobsPolygonId = -1;

        private double _weight;
 


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Id
        {
            get {return GetProperty("Id", ref _id);}
            set { SetProperty("Id", value, ref _id);}
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Code
        {
            get
            {
                return GetProperty("Code", ref _code);
            }
            set
            {
                SetProperty("Code", value, ref _code);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Description
        {
            get
            {
                return GetProperty("Description", ref _description);
            }
            set
            {
                SetProperty("Description", value, ref _description);
            }
        }

        [PersistableField]
        public string BatchCode
        {
            get
            {
                return GetProperty("BatchCode", ref _batchCode);
            }
            set
            {
                SetProperty("BatchCode", value, ref _batchCode);
            }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        public double DimX
        {
            get
            {
                return GetProperty("DimX", ref _dimX);
            }
            set
            {
                SetProperty("DimX", value, ref _dimX);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double DimY
        {
            get
            {
                return GetProperty("DimY", ref _dimY);
            }
            set
            {
                SetProperty("DimY", value, ref _dimY);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double DimZ
        {
            get
            {
                return GetProperty("DimZ", ref _dimZ);
            }
            set
            {
                SetProperty("DimZ", value, ref _dimZ);
            }
        }

        [PersistableField]
        public double OffsetX
        {
            get
            {
                return GetProperty("OffsetX", ref _offsetX);
            }
            set
            {
                SetProperty("OffsetX", value, ref _offsetX);
            }
        }

        [PersistableField]
        public double OffsetY
        {
            get
            {
                return GetProperty("OffsetY", ref _offsetY);
            }
            set
            {
                SetProperty("OffsetY", value, ref _offsetY);
            }
        }

        [PersistableField]
        public double Surface
        {
            get
            {
                return GetProperty("Surface", ref _surface);
            }
            set
            {
                SetProperty("Surface", value, ref _surface);
            }
        }

        [PersistableField]
        public double CommercialSurface
        {
            get
            {
                return GetProperty("CommercialSurface", ref _commercialSurface);
            }
            set
            {
                SetProperty("CommercialSurface", value, ref _commercialSurface);
            }
        }

        [PersistableField]
        public double InternalRectLeft
        {
            get
            {
                return GetProperty("InternalRectLeft", ref _internalRectLeft);
            }
            set
            {
                SetProperty("InternalRectLeft", value, ref _internalRectLeft);
            }
        }

        [PersistableField]
        public double InternalRectTop
        {
            get
            {
                return GetProperty("InternalRectTop", ref _internalRectTop);
            }
            set
            {
                SetProperty("InternalRectTop", value, ref _internalRectTop);
            }
        }

        [PersistableField]
        public double InternalRectWidth
        {
            get
            {
                return GetProperty("InternalRectWidth", ref _internalRectWidth);
            }
            set
            {
                SetProperty("InternalRectWidth", value, ref _internalRectWidth);
            }
        }

        [PersistableField]
        public double InternalRectHeight
        {
            get
            {
                return GetProperty("InternalRectHeight", ref _internalRectHeight);
            }
            set
            {
                SetProperty("InternalRectHeight", value, ref _internalRectHeight);
            }
        }

        [PersistableField]
        public string Tone
        {
            get
            {
                return GetProperty("Tone", ref _tone);
            }
            set
            {
                SetProperty("Tone", value, ref _tone);
            }
        }

        [PersistableField]
        public string XmlParameter
        {
            get
            {
                return GetProperty("XmlParameter", ref _xmlParameter);
            }
            set
            {
                SetProperty("XmlParameter", value, ref _xmlParameter);
            }
        }

        [PersistableField]
        public double PositionStopX
        {
            get
            {
                return GetProperty("PositionStopX", ref _positionStopX);
            }
            set
            {
                SetProperty("PositionStopX", value, ref _positionStopX);
            }
        }

        [PersistableField]
        public double PositionStopY
        {
            get
            {
                return GetProperty("PositionStopY", ref _positionStopY);
            }
            set
            {
                SetProperty("PositionStopY", value, ref _positionStopY);
            }
        }

        [PersistableField]
        public double UnitCost
        {
            get
            {
                return GetProperty("UnitCost", ref _unitCost);
            }
            set
            {
                SetProperty("UnitCost", value, ref _unitCost);
            }
        }

        [PersistableField]
        public int Defects
        {
            get
            {
                return GetProperty("Defects", ref _defects);
            }
            set
            {
                SetProperty("Defects", value, ref _defects);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int ImageId
        {
            get
            {
                return GetProperty("ImageId", ref _imageId);
            }
            set
            {
                SetProperty("ImageId", value, ref _imageId);
            }
        }


        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public string ImagePath
        {
            get
            {
                return GetProperty("ImagePath", ref _imagePath);
            }
            set
            {
                SetProperty("ImagePath", value, ref _imagePath);
            }
        }

        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public string ImageThumbnailPath
        {
            get
            {
                return GetProperty("ImageThumbnailPath", ref _imageThumbnailPath);
            }
            set
            {
                SetProperty("ImageThumbnailPath", value, ref _imageThumbnailPath);
            }
        }



        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public BitmapImage ImageSource
        {
            get
            {
                return GetProperty("ImageSource", ref _imageSource);
            }
            set
            {
                SetProperty("ImageSource", value, ref _imageSource);
            }
        }

        [PersistableField]
        public int QualityId
        {
            get
            {
                return GetProperty("QualityId", ref _qualityId);
            }
            set
            {
                SetProperty("QualityId", value, ref _qualityId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public ArticleType ArticleType
        {
            get
            {
                return GetProperty("ArticleType", ref _articleType);
            }
            set
            {
                SetProperty("ArticleType", value, ref _articleType);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public ArticleStatus ArticleStatus
        {
            get
            {
                return GetProperty("ArticleStatus", ref _articleStatus);
            }
            set
            {
                SetProperty("ArticleStatus", value, ref _articleStatus);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int MaterialId
        {
            get
            {
                return GetProperty("MaterialId", ref _materialId);
            }
            set
            {
                SetProperty("MaterialId", value, ref _materialId);
            }
        }

        [PersistableField]
        public int StockHusId
        {
            get
            {
                return GetProperty("StockHusId", ref _stockHusId);
            }
            set
            {
                SetProperty("StockHusId", value, ref _stockHusId);
            }
        }

        [PersistableField]
        public int DdtaDetailsId
        {
            get
            {
                return GetProperty("DdtaDetailsId", ref _ddtaDetailsId);
            }
            set
            {
                SetProperty("DdtaDetailsId", value, ref _ddtaDetailsId);
            }
        }

        [PersistableField]
        public int SupplierId
        {
            get
            {
                return GetProperty("SupplierId", ref _supplierId);
            }
            set
            {
                SetProperty("SupplierId", value, ref _supplierId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int CustomerProjectId
        {
            get
            {
                return GetProperty("CustomerProjectId", ref _customerProjectId);
            }
            set
            {
                SetProperty("CustomerProjectId", value, ref _customerProjectId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double Qty
        {
            get
            {
                return GetProperty("Qty", ref _qty);
            }
            set
            {
                SetProperty("Qty", value, ref _qty);
            }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        public DateTime InsertDate
        {
            get
            {
                return GetProperty("InsertDate", ref _insertDate);
            }
            set
            {
                SetProperty("InsertDate", value, ref _insertDate);
            }
        }

        [PersistableField]
        public int HusPosition
        {
            get
            {
                return GetProperty("HusPosition", ref _husPosition);
            }
            set
            {
                SetProperty("HusPosition", value, ref _husPosition);
            }
        }

        [PersistableField]
        public int BlobsPartProgramId
        {
            get
            {
                return GetProperty("BlobsPartProgramId", ref _blobsPartProgramId);
            }
            set
            {
                SetProperty("BlobsPartProgramId", value, ref _blobsPartProgramId);
            }
        }

        [PersistableField]
        public int BlobsPolygonId
        {
            get
            {
                return GetProperty("BlobsPolygonId", ref _blobsPolygonId);
            }
            set
            {
                SetProperty("BlobsPolygonId", value, ref _blobsPolygonId);
            }
        }

        [PersistableField]
        public double Weight
        {
            get
            {
                return GetProperty("Weight", ref _weight);
            }
            set
            {
                SetProperty("Weight", value, ref _weight);
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "Id":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _id,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Code":
                        base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
                            ref _code,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Description":
                        base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
                            ref _description,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "BatchCode":
                        base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
                            ref _batchCode,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;


                    case "DimX":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _dimX,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "DimY":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _dimY,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "DimZ":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _dimZ,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "OffsetX":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _offsetX,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "OffsetY":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _offsetY,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Surface":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _surface,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "CommercialSurface":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _commercialSurface,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "InternalRectLeft":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _internalRectLeft,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "InternalRectTop":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _internalRectTop,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "InternalRectWidth":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _internalRectWidth,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "InternalRectHeight":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _internalRectHeight,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Tone":
                        base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
                            ref _tone,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "XmlParameter":
                        base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
                            ref _xmlParameter,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "PositionStopX":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _positionStopX,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "PositionStopY":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _positionStopY,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "UnitCost":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _unitCost,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Defects":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _defects,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "QualityId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _qualityId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ImageId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _imageId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ImagePath":
                        base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
                            ref _imagePath,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ImageThumbnailPath":
                        base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
                            ref _imageThumbnailPath,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ImageSource":
                        base.SetProperty(propertyName, (BitmapImage) Convert.ChangeType(value, typeof (BitmapImage)),
                            ref _imageSource,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ArticleType":
                        base.SetProperty(propertyName, (ArticleType) Convert.ChangeType(value, typeof (int)),
                            ref _articleType,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "ArticleStatus":
                        base.SetProperty(propertyName, (ArticleStatus) Convert.ChangeType(value, typeof (int)),
                            ref _articleStatus,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "MaterialId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _materialId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "StockHusId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _stockHusId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "DdtaDetailsId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _ddtaDetailsId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "SupplierId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _supplierId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "CustomerProjectId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _customerProjectId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Qty":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _qty,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "InsertDate":
                        base.SetProperty(propertyName, (DateTime) Convert.ChangeType(value, typeof (DateTime)),
                            ref _insertDate,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "HusPosition":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _husPosition,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "BlobsPartProgramId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _blobsPartProgramId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "BlobsPolygonId":
                        base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
                            ref _blobsPolygonId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Weight":
                        base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
                            ref _weight,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }
        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _id), typeof(T));
                    break;

                case "Code":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _code), typeof(T));
                    break;

                case "Description":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _description), typeof(T));
                    break;

                case "BatchCode":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _batchCode), typeof(T));
                    break;

                case "DimX":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimX), typeof(T));
                    break;

                case "DimY":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimY), typeof(T));
                    break;

                case "DimZ":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimZ), typeof(T));
                    break;

                case "OffsetX":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _offsetX), typeof(T));
                    break;

                case "OffsetY":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _offsetY), typeof(T));
                    break;

                case "Surface":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _surface), typeof(T));
                    break;

                case "CommercialSurface":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _commercialSurface), typeof(T));
                    break;

                case "InternalRectLeft":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectLeft), typeof(T));
                    break;

                case "InternalRectTop":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectTop), typeof(T));
                    break;

                case "InternalRectWidth":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectWidth), typeof(T));
                    break;

                case "InternalRectHeight":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectHeight), typeof(T));
                    break;

                case "Tone":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _tone), typeof(T));
                    break;

                case "XmlParameter":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _xmlParameter), typeof(T));
                    break;

                case "PositionStopX":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _positionStopX), typeof(T));
                    break;

                case "PositionStopY":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _positionStopY), typeof(T));
                    break;

                case "UnitCost":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _unitCost), typeof(T));
                    break;

                case "Defects":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _defects), typeof(T));
                    break;

                case "QualityId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _qualityId), typeof(T));
                    break;

                case "ImageId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageId), typeof(T));
                    break;

                case "ImagePath":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imagePath), typeof(T));
                    break;

                case "ImageThumbnailPath":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageThumbnailPath), typeof(T));
                    break;

                case "ImageSource":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageSource), typeof(T));
                    break;

                case "ArticleType":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _articleType), typeof(T));
                    break;

                case "ArticleStatus":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _articleStatus), typeof(T));
                    break;

                case "MaterialId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _materialId), typeof(T));
                    break;

                case "StockHusId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _stockHusId), typeof(T));
                    break;

                case "DdtaDetailsId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _ddtaDetailsId), typeof(T));
                    break;

                case "SupplierId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _supplierId), typeof(T));
                    break;

                case "CustomerProjectId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _customerProjectId), typeof(T));
                    break;

                case "Qty":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _qty), typeof(T));
                    break;

                case "InsertDate":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _insertDate), typeof(T));
                    break;

                case "HusPosition":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _husPosition), typeof(T));
                    break;

                case "BlobsPartProgramId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _blobsPartProgramId), typeof(T));
                    break;

                case "BlobsPolygonId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _blobsPolygonId), typeof(T));
                    break;

                case "Weight":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _weight), typeof(T));
                    break;

            }

            return default(T);
        }

        public override int UniqueId
        {
            get { return Id; }
        }

    }
}
