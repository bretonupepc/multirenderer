﻿using Breton.DbAccess;
using BrSm.Business.ENTITIES.ARTICLES;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.MATERIALS.Persistence.SqlServer
{
    public class SqlArticleCollectionProxy:SqlCollectionProxy<ArticleEntity> 
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors

        //public SqlMaterialCollectionProxy( MaterialCollection materialCollection, DbInterface dbInterface)
        //    : base(materialCollection, dbInterface)
        //{
        //    _mainTableName = "T_AN_MATERIALS";
        //}

        public SqlArticleCollectionProxy(ArticleCollection articleCollection, DbInterface dbInterface, string mainTableName = "T_AN_ARTICLES")
            : base(articleCollection, dbInterface, mainTableName)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        //public override 

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        //protected override T CreateNewItemFromDbRecord<T>(OleDbDataReader ord)
        //{
        //    T newItem = Activator.CreateInstance<T>();

        //    foreach (FieldItemInfo field in FieldsInfos)
        //    {
        //        if (field.RetrieveMode == FieldRetrieveMode.Immediate &&
        //            FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
        //        {
        //            object fieldValue = ord[FieldsMap[field.BaseInfo.PropertyName]];

        //            if (fieldValue != DBNull.Value)
        //            {
        //                newItem.SetProperty(field.BaseInfo.PropertyName, fieldValue, true, FieldStatus.Read);

        //            }

        //        }
        //    }

        //    return newItem as T;
        //}

        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
