﻿using Breton.DbAccess;
using BrSm.Business.BusinessBase;
using BrSm.Business.COMMON;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.Persistence;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.ARTICLES.PIECES.Persistence.SqlServer
{
    public class SqlPieceCollectionProxy:SqlCollectionProxy<PieceEntity> 
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlPieceCollectionProxy(PieceCollection pieceCollection, DbInterface dbInterface, string mainTableName = "T_AN_ARTICLES")
            : base(pieceCollection, dbInterface, mainTableName)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods





        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override string BuildWherePart(FilterClause clause)
        {
            string whereStr = BASE_WHERE;

            bool isValid = false;

            if (clause != null)
            {
                string whereContent;
                isValid = ParseClause(clause, out whereContent);

                if (isValid)
                {
                    whereStr += string.Format(" {0} = {1} AND ", FieldsMap["ArticleType"], (int) ArticleType.COMPLETED);
                    whereStr += whereContent;
                }
                else
                {
                    whereStr += string.Format(" {0} = {1} ", FieldsMap["ArticleType"], (int)ArticleType.COMPLETED);
                    isValid = true;
                }

            }
            else
            {
                whereStr += string.Format(" {0} = {1} ", FieldsMap["ArticleType"], (int)ArticleType.COMPLETED);
                isValid = true;

            }


            return isValid ? whereStr : string.Empty;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
