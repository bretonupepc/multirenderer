﻿using System;
using System.Drawing;
using System.IO;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;
using BrSm.Business.ENTITIES.ARTICLES.SLABS;
using BrSm.Business.Persistence;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.ARTICLES.PIECES.Persistence.SqlServer
{
    public class SqlPieceProxy: SqlEntityProxy
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlPieceProxy(PieceEntity piece, SqlServerProvider provider, string mainTableName = "T_AN_ARTICLES") 
            : base(piece, provider, mainTableName)
        {
            
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods




        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
