﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.ARTICLES.PIECES
{
    public class PieceEntity : ArticleEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<


        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {

                    default:
                        base.SetProperty<T>(propertyName, value, forceStatus, newStatus, forceOnChanged, avoidOnChanged,
                            avoidOnStatusChanged);
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                default:
                    return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }



    }
}
