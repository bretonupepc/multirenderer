﻿using System;
using System.Windows.Media.Imaging;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.ARTICLES.SLABS
{
    public class SlabFaceEntity: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;
        private int _slabId = -1;
        private int _faceId = -1;

        private int _used = 0;

        private double _dimX;
        private double _dimY;
        private double _dimZ;

        private double _offsetX;
        private double _offsetY;

        private double _surface;
        private double _commercialSurface;

        private double _internalRectLeft;
        private double _internalRectTop;
        private double _internalRectWidth;
        private double _internalRectHeight;

        private string _tone = string.Empty;

        private string _xmlParameter = string.Empty;

        private double _positionStopX;
        private double _positionStopY;

        private double _unitCost;

        private int _defects;

        private int _imageId = -1;
        private string _imagePath;
        private BitmapImage _imageSource = null;

        private string _imageThumbnailPath = @"E:\Progetti\BRETON VSS\BrSm\SlabPlanning\bin\Debug\NoImage.png";


        private int _qualityId = -1;

        private SlabEntity _parentSlab = null;


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Id
        {
            get
            {
                return GetProperty("Id", ref _id);
            }
            set
            {
                SetProperty("Id", value, ref _id);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int SlabId
        {
            get
            {
                return GetProperty("SlabId", ref _slabId);
            }
            set
            {
                SetProperty("SlabId", value, ref _slabId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int FaceId
        {
            get
            {
                return GetProperty("FaceId", ref _faceId);
            }
            set
            {
                SetProperty("FaceId", value, ref _faceId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Used
        {
            get
            {
                return GetProperty("Used", ref _used);
            }
            set
            {
                SetProperty("Used", value, ref _used);
            }
        }

        public double DimX
        {
            get
            {
                return GetProperty("DimX", ref _dimX);
            }
            set
            {
                SetProperty("DimX", value, ref _dimX);
            }
        }

        public double DimY
        {
            get
            {
                return GetProperty("DimY", ref _dimY);
            }
            set
            {
                SetProperty("DimY", value, ref _dimY);
            }
        }

        public double DimZ
        {
            get
            {
                return GetProperty("DimZ", ref _dimZ);
            }
            set
            {
                SetProperty("DimZ", value, ref _dimZ);
            }
        }

        public double OffsetX
        {
            get
            {
                return GetProperty("OffsetX", ref _offsetX);
            }
            set
            {
                SetProperty("OffsetX", value, ref _offsetX);
            }
        }

        public double OffsetY
        {
            get
            {
                return GetProperty("OffsetY", ref _offsetY);
            }
            set
            {
                SetProperty("OffsetY", value, ref _offsetY);
            }
        }

        public double Surface
        {
            get
            {
                return GetProperty("Surface", ref _surface);
            }
            set
            {
                SetProperty("Surface", value, ref _surface);
            }
        }

        public double CommercialSurface
        {
            get
            {
                return GetProperty("CommercialSurface", ref _commercialSurface);
            }
            set
            {
                SetProperty("CommercialSurface", value, ref _commercialSurface);
            }
        }

        public double InternalRectLeft
        {
            get
            {
                return GetProperty("InternalRectLeft", ref _internalRectLeft);
            }
            set
            {
                SetProperty("InternalRectLeft", value, ref _internalRectLeft);
            }
        }

        public double InternalRectTop
        {
            get
            {
                return GetProperty("InternalRectTop", ref _internalRectTop);
            }
            set
            {
                SetProperty("InternalRectTop", value, ref _internalRectTop);
            }
        }

        public double InternalRectWidth
        {
            get
            {
                return GetProperty("InternalRectWidth", ref _internalRectWidth);
            }
            set
            {
                SetProperty("InternalRectWidth", value, ref _internalRectWidth);
            }
        }

        public double InternalRectHeight
        {
            get
            {
                return GetProperty("InternalRectHeight", ref _internalRectHeight);
            }
            set
            {
                SetProperty("InternalRectHeight", value, ref _internalRectHeight);
            }
        }

        public string Tone
        {
            get
            {
                return GetProperty("Tone", ref _tone);
            }
            set
            {
                SetProperty("Tone", value, ref _tone);
            }
        }

        public string XmlParameter
        {
            get
            {
                return GetProperty("XmlParameter", ref _xmlParameter);
            }
            set
            {
                SetProperty("XmlParameter", value, ref _xmlParameter);
            }
        }

        public double PositionStopX
        {
            get
            {
                return GetProperty("PositionStopX", ref _positionStopX);
            }
            set
            {
                SetProperty("PositionStopX", value, ref _positionStopX);
            }
        }

        public double PositionStopY
        {
            get
            {
                return GetProperty("PositionStopY", ref _positionStopY);
            }
            set
            {
                SetProperty("PositionStopY", value, ref _positionStopY);
            }
        }

        public double UnitCost
        {
            get
            {
                return GetProperty("UnitCost", ref _unitCost);
            }
            set
            {
                SetProperty("UnitCost", value, ref _unitCost);
            }
        }

        public int Defects
        {
            get
            {
                return GetProperty("Defects", ref _defects);
            }
            set
            {
                SetProperty("Defects", value, ref _defects);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int ImageId
        {
            get
            {
                return GetProperty("ImageId", ref _imageId);
            }
            set
            {
                SetProperty("ImageId", value, ref _imageId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int QualityId
        {
            get
            {
                return GetProperty("QualityId", ref _qualityId);
            }
            set
            {
                SetProperty("QualityId", value, ref _qualityId);
            }
        }


        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public string ImagePath
        {
            get
            {
                return GetProperty("ImagePath", ref _imagePath);
            }
            set
            {
                SetProperty("ImagePath", value, ref _imagePath);
            }
        }

        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public string ImageThumbnailPath
        {
            get
            {
                return GetProperty("ImageThumbnailPath", ref _imageThumbnailPath);
            }
            set
            {
                SetProperty("ImageThumbnailPath", value, ref _imageThumbnailPath);
            }
        }


        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public BitmapImage ImageSource
        {
            get
            {
                return GetProperty("ImageSource", ref _imageSource);
            }
            set
            {
                SetProperty("ImageSource", value, ref _imageSource);
            }
        }

        public SlabEntity ParentSlab
        {
            get { return _parentSlab; }
            set
            {
                _parentSlab = value;
                OnPropertyChanged("ParentSlab");
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        #region >-------------- Overrides

        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            switch (propertyName)
            {
                case "Id":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _id,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "SlabId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _slabId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "FaceId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _faceId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Used":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _used,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "DimX":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _dimX,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "DimY":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _dimY,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "DimZ":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _dimZ,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "OffsetX":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _offsetX,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "OffsetY":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _offsetY,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Surface":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _surface,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "CommercialSurface":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _commercialSurface,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "InternalRectLeft":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _internalRectLeft,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "InternalRectTop":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _internalRectTop,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "InternalRectWidth":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _internalRectWidth,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "InternalRectHeight":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _internalRectHeight,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Tone":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _tone,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "XmlParameter":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _xmlParameter,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "PositionStopX":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _positionStopX,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "PositionStopY":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _positionStopY,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "UnitCost":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _unitCost,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Defects":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _defects,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "QualityId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _qualityId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "ImageId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _imageId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "ImagePath":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _imagePath,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "ImageSource":
                    base.SetProperty(propertyName, (BitmapImage)Convert.ChangeType(value, typeof(BitmapImage)),
                        ref  _imageSource,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "ImageThumbnailPath":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref _imageThumbnailPath,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;



            }
        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _id), typeof(T));
                    break;

                case "SlabId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _slabId), typeof(T));
                    break;

                case "FaceId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _faceId), typeof(T));
                    break;

                case "Used":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _used), typeof(T));
                    break;

                case "DimX":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimX), typeof(T));
                    break;

                case "DimY":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimY), typeof(T));
                    break;

                case "DimZ":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimZ), typeof(T));
                    break;

                case "OffsetX":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _offsetX), typeof(T));
                    break;

                case "OffsetY":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _offsetY), typeof(T));
                    break;

                case "Surface":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _surface), typeof(T));
                    break;

                case "CommercialSurface":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _commercialSurface), typeof(T));
                    break;

                case "InternalRectLeft":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectLeft), typeof(T));
                    break;

                case "InternalRectTop":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectTop), typeof(T));
                    break;

                case "InternalRectWidth":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectWidth), typeof(T));
                    break;

                case "InternalRectHeight":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectHeight), typeof(T));
                    break;

                case "Tone":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _tone), typeof(T));
                    break;

                case "XmlParameter":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _xmlParameter), typeof(T));
                    break;

                case "PositionStopX":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _positionStopX), typeof(T));
                    break;

                case "PositionStopY":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _positionStopY), typeof(T));
                    break;

                case "UnitCost":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _unitCost), typeof(T));
                    break;

                case "Defects":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _defects), typeof(T));
                    break;

                case "QualityId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _qualityId), typeof(T));
                    break;

                case "ImageId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageId), typeof(T));
                    break;

                case "ImagePath":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imagePath), typeof(T));
                    break;

                case "ImageSource":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageSource), typeof(T));
                    break;
            }

            return default(T);
        }

        public override string this[string columnName]
        {
            get
            {
                string retError = string.Empty;
                switch (columnName)
                {
                    case "SlabId":
                        if (SlabId == -1)
                            retError = "SlabId value missing.";
                        break;

                    default:
                        break;

                }
                return retError;
            }
        }

        public override bool CanSave()
        {
            return _slabId != -1;
        }

        #endregion Overrides  -----------<
    }
}
