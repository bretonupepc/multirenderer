﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.ARTICLES.SLABS
{
    public class SlabEntity : ArticleEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private SlabFaceEntity _frontFace = null;

        private SlabFaceEntity _rearFace = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        [PersistableField(FieldRetrieveMode.DeferredAsync)]
        public SlabFaceEntity FrontFace
        {
            get
            {
                return GetProperty("FrontFace", ref _frontFace);
            }
            set { SetProperty("FrontFace", value, ref _frontFace); }
        }


        [PersistableField(FieldRetrieveMode.DeferredAsync)]
        public SlabFaceEntity RearFace
        {
            get { return GetProperty("RearFace", ref _rearFace); }
            set { SetProperty("RearFace", value, ref _rearFace); }
        }

        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "RearFace":
                        base.SetProperty(propertyName,
                            (SlabFaceEntity) Convert.ChangeType(value, typeof (SlabFaceEntity)),
                            ref _rearFace,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "FrontFace":
                        base.SetProperty(propertyName,
                            (SlabFaceEntity) Convert.ChangeType(value, typeof (SlabFaceEntity)),
                            ref _frontFace,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    default:
                        base.SetProperty<T>(propertyName, value, forceStatus, newStatus, forceOnChanged, avoidOnChanged,
                            avoidOnStatusChanged);
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            //try
            {
                switch (propertyName)
                {
                    case "FrontFace":
                        return (T) Convert.ChangeType(GetProperty(propertyName, ref _frontFace), typeof (T));
                        break;

                    case "RearFace":
                        return (T) Convert.ChangeType(GetProperty(propertyName, ref _rearFace), typeof (T));
                        break;

                    default:
                        return base.GetProperty<T>(propertyName);
                        break;
                }

            }
            return default(T);
        }

        public override T GetProperty<T>(string propertyName, T typeObject)
        {
            //try
            {
                switch (propertyName)
                {
                    case "FrontFace":
                        return (T)Convert.ChangeType(GetProperty(propertyName, ref _frontFace), typeObject.GetType());
                        break;

                    case "RearFace":
                        return (T)Convert.ChangeType(GetProperty(propertyName, ref _rearFace), typeObject.GetType());
                        break;

                    default:
                        return base.GetProperty<T>(propertyName, typeObject);
                        break;
                }

            }
            return default(T);
        }


    }
}
