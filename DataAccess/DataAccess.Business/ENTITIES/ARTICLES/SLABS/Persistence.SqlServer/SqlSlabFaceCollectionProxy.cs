﻿using Breton.DbAccess;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.ARTICLES.SLABS.Persistence.SqlServer
{
    public class SqlSlabFaceCollectionProxy:SqlCollectionProxy<SlabFaceEntity> 
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors

        //public SqlMaterialCollectionProxy( MaterialCollection materialCollection, DbInterface dbInterface)
        //    : base(materialCollection, dbInterface)
        //{
        //    _mainTableName = "T_AN_MATERIALS";
        //}

        public SqlSlabFaceCollectionProxy(SlabFaceCollection slabFaceCollection, DbInterface dbInterface, string mainTableName = "T_AN_SLAB_FACES")
            : base(slabFaceCollection, dbInterface, mainTableName)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        //public override 

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        //protected override string BuildWherePart(FilterClause clause)
        //{
        //    string whereStr = BASE_WHERE;

        //    bool isValid = false;

        //    if (clause != null)
        //    {
        //        string whereContent;
        //        isValid = ParseClause(clause, out whereContent);

        //        if (isValid)
        //        {
        //            whereStr += string.Format(" {0} = {1} AND ", FieldsMap["ArticleType"], (int) ArticleType.SLAB);
        //            whereStr += whereContent;
        //        }
        //        else
        //        {
        //            whereStr += string.Format(" {0} = {1} ", FieldsMap["ArticleType"], (int)ArticleType.SLAB);
        //        }

        //    }

        //    return isValid ? whereStr : string.Empty;
        //}

        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
