﻿using Breton.DbAccess;
using BrSm.Business.BusinessBase;
using BrSm.Business.COMMON;
using BrSm.Business.Persistence;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.ARTICLES.SLABS.Persistence.SqlServer
{
    public class SqlSlabCollectionProxy:SqlCollectionProxy<SlabEntity> 
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors

        //public SqlMaterialCollectionProxy( MaterialCollection materialCollection, DbInterface dbInterface)
        //    : base(materialCollection, dbInterface)
        //{
        //    _mainTableName = "T_AN_MATERIALS";
        //}

        public SqlSlabCollectionProxy(SlabCollection slabCollection, DbInterface dbInterface, string mainTableName = "T_AN_ARTICLES")
            : base(slabCollection, dbInterface, mainTableName)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Recupera una collection di items da database
        /// tramite elenco campi, filtro di selezione e criterio di ordinamento
        /// </summary>
        /// <param name="set">Set di campi da estrarre</param>
        /// <param name="filterClause">Filtro di selezione</param>
        /// <param name="sortSet">Criterio di ordinamento</param>
        /// <returns></returns>
        //public override bool GetItems(FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
        //{

        //    if (!CanGetItems())
        //    {
        //        return false;
        //    }

        //    string sqlCommand = string.Empty;

        //    if (!BuildSelectCommand(set, filterClause, sortSet, ref sqlCommand)) return false;

        //    this.Collection.Clear();


        //    bool retVal = true;

        //    int exception_step = 0;
        //    OleDbDataReader ord = null;

        //    try
        //    {

        //        if (!BretonInterface.Requery(sqlCommand, out ord))
        //            return false;

        //        if (ord.HasRows)
        //        {
        //            while (ord.Read())
        //            {
        //                SlabEntity newItem = CreateNewItemFromDbRecord<SlabEntity>(ord);

        //                if (newItem != default(SlabEntity))
        //                {
        //                    _collection.Add(newItem);
        //                }
        //            }
        //            //BretonInterface.EndRequery(ord);
        //            //var fieldinfo = _fieldsInfos.FirstOrDefault(i => i.RetrieveMode == FieldRetrieveMode.Immediate
        //            //                 &&
        //            //                 (i.BaseInfo.PropertyName == "FrontFace" ||
        //            //                  i.BaseInfo.PropertyName == "RearFace"));
        //            //if (fieldinfo != null)
        //            //{
        //            //    foreach (var slab in _collection)
        //            //    {
        //            //        GetSlabFaces(slab);
        //            //    }
        //            //}


        //        }
        //        else
        //            retVal = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine(this + "GetItems error: ", ex);
        //        retVal = false;
        //    }

        //    finally
        //    {
        //        BretonInterface.EndRequery(ord);
        //        ord = null;
        //    }

        //    return retVal;


        //}

        private void GetSlabFaces(SlabEntity slab)
        {
            if (slab.IsNewItem)
            {
                return;
            }

            SlabFaceCollection faces = new SlabFaceCollection();
            FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("SlabId", typeof(int)), ConditionOperator.Eq, slab.UniqueId);
            if (faces.GetItemsFromRepository(clause: filterClause))
            {
                if (faces.Count > 0)
                {
                    foreach (var face in faces)
                    {
                        slab.SetProperty(face.FaceId == 0 ? "FrontFace" : "RearFace",
                            face, true, FieldStatus.Read);
                        face.ParentSlab = slab;
                    }
                }
            }


        }



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override string BuildWherePart(FilterClause clause)
        {
            string whereStr = BASE_WHERE;

            bool isValid = false;

            if (clause != null)
            {
                string whereContent;
                isValid = ParseClause(clause, out whereContent);

                if (isValid)
                {
                    whereStr += string.Format(" {0} = {1} AND ", FieldsMap["ArticleType"], (int) ArticleType.SLAB);
                    whereStr += whereContent;
                }
                else
                {
                    whereStr += string.Format(" {0} = {1} ", FieldsMap["ArticleType"], (int)ArticleType.SLAB);
                    isValid = true;
                }

            }
            else
            {
                whereStr += string.Format(" {0} = {1} ", FieldsMap["ArticleType"], (int)ArticleType.SLAB);
                isValid = true;

            }


            return isValid ? whereStr : string.Empty;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
