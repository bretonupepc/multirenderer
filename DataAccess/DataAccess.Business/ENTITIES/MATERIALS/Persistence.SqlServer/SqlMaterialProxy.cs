﻿using System;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using BrRh.Wpf.Common;
using BrSm.Business.BusinessBase;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.MATERIALS.Persistence.SqlServer
{
    public class SqlMaterialProxy: SqlEntityProxy
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlMaterialProxy(MaterialEntity material, SqlServerProvider provider, string mainTableName = "T_AN_MATERIALS") 
            : base(material, provider, mainTableName)
        {
            
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override void SyncGetSingleField(string fieldName)
        {
            switch (fieldName)
            {
                case "ImageSource":
                    //base.GetSingleField("ImageId");
                    GetImage(fieldName);
                    break;

                default:
                    base.SyncGetSingleField(fieldName);
                    break;
            }

        }

        private void GetImage(string fieldName)
        {
            int imageId = (_entity as MaterialEntity).ImageId;
            if (imageId != -1)
            {
                string fileName = string.Format("Material_{0}.jpg", imageId);
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                if (BretonInterface.GetBlobField("T_AN_MATERIAL_IMAGES", "F_IMAGE", string.Format("F_ID = {0}", imageId), fileName))
                {
                    if (File.Exists(fileName))
                    {
                        using (var bmpTemp = new Bitmap(fileName))
                        {
                            BitmapImage source = (BitmapImage) bmpTemp.ToWpfBitmap();

                            Entity.SetProperty(fieldName, source, true, FieldStatus.Read);

                        }

                        File.Delete(fileName);

                    }

                }

            }
            else
            {

                Entity.SetPropertyStatus(fieldName,  FieldStatus.Read);

            }

        }


        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
