﻿using System;
using System.Drawing;
using BrSm.Business.BusinessBase;
using System.Windows.Media.Imaging;


namespace BrSm.Business.ENTITIES.MATERIALS
{
    public class MaterialEntity: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private int _materialClassId = -1;
        private int _stationId = -1;
        private int _imageId = -1;
        private int _supplierId = -1;
        private int _statusId = -1;

        private string _code = string.Empty;
        private string _description = string.Empty;

        private double _specificGravity;
        private string _finishingSurface;

        private double _costM2;
        private double _costM3;
        private double _linearCost;
        private double _slabCost;

        private string _origin;
        private string _blockSize = null;
        private string _color;
        private string _availability;
        private string _prevailingUse;
        private string _polish;

        private string _comments;
        private string _tonality;
        private string _scale;
        private DateTime _insertDate;

        private Image _mainImage;
        private string _imagePath;
        private BitmapImage _imageSource = null; 

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Id
        {
            get
            {
                return GetProperty("Id", ref _id);
            }
            set
            {
                SetProperty("Id", value, ref _id);
            }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        public int MaterialClassId
        {
            get
            {
                return GetProperty("MaterialClassId", ref _materialClassId);
            }
            set
            {
                SetProperty("MaterialClassId", value, ref _materialClassId);
            }
        }



        [PersistableField]
        public int StationId
        {
            get
            {
                return GetProperty("StationId",ref  _stationId);
            }
            set
            {
                SetProperty("StationId", value, ref _stationId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int ImageId
        {
            get
            {
                return GetProperty("ImageId", ref _imageId);
            }
            set
            {
                SetProperty("ImageId", value, ref _imageId);
            }
        }

        [PersistableField]
        public int SupplierId
        {
            get
            {
                return GetProperty("SupplierId", ref _supplierId);
            }
            set
            {
                SetProperty("SupplierId", value, ref _supplierId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int StatusId
        {
            get
            {
                return GetProperty("StatusId", ref _statusId);
            }
            set
            {
                SetProperty("StatusId", value, ref _statusId);
            }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Code
        {
            get
            {
                return GetProperty("Code", ref _code);
            }
            set
            {
                SetProperty("Code", value, ref _code);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Description
        {
            get
            {
                return GetProperty("Description", ref _description);
            }
            set
            {
                SetProperty("Description", value, ref _description);
            }
        }

        [PersistableField]
        public double SpecificGravity
        {
            get
            {
                return GetProperty("SpecificGravity", ref _specificGravity);
            }
            set
            {
                SetProperty("SpecificGravity", value, ref _specificGravity);
            }
        }

        [PersistableField]
        public string FinishingSurface
        {
            get
            {
                return GetProperty("FinishingSurface", ref _finishingSurface);
            }
            set
            {
                SetProperty("FinishingSurface", value, ref _finishingSurface);
            }
        }

        [PersistableField]
        public double CostM2
        {
            get
            {
                return GetProperty("CostM2", ref _costM2);
            }
            set
            {
                SetProperty("CostM2", value, ref _costM2);
            }
        }

        [PersistableField]
        public double CostM3
        {
            get
            {
                return GetProperty("CostM3", ref _costM3);
            }
            set
            {
                SetProperty("CostM3", value, ref _costM3);
            }
        }

        [PersistableField]
        public double LinearCost
        {
            get
            {
                return GetProperty("LinearCost", ref _linearCost);
            }
            set
            {
                SetProperty("LinearCost", value, ref _linearCost);
            }
        }

        [PersistableField]
        public double SlabCost
        {
            get
            {
                return GetProperty("SlabCost", ref _slabCost);
            }
            set
            {
                SetProperty("SlabCost", value, ref _slabCost);
            }
        }

        [PersistableField]
        public string Origin
        {
            get
            {
                return GetProperty("Origin", ref _origin);
            }
            set
            {
                SetProperty("Origin", value, ref _origin);
            }
        }

        [PersistableField]
        public string BlockSize
        {
            get
            {
                return GetProperty("BlockSize", ref _blockSize);
            }
            set
            {
                SetProperty("BlockSize", value, ref _blockSize);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Color
        {
            get
            {
                return GetProperty("Color", ref _color);
            }
            set
            {
                SetProperty("Color", value, ref _color);
            }
        }

        [PersistableField]
        public string Availability
        {
            get
            {
                return GetProperty("Availability", ref _availability);
            }
            set
            {
                SetProperty("Availability", value, ref _availability);
            }
        }

        [PersistableField]
        public string PrevailingUse
        {
            get
            {
                return GetProperty("PrevailingUse", ref _prevailingUse);
            }
            set
            {
                SetProperty("PrevailingUse", value, ref _prevailingUse);
            }
        }

        [PersistableField]
        public string Polish
        {
            get
            {
                return GetProperty("Polish", ref _polish);
            }
            set
            {
                SetProperty("Polish", value, ref _polish);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Comments
        {
            get
            {
                return GetProperty("Comments", ref _comments);
            }
            set
            {
                SetProperty("Comments", value, ref _comments);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Tonality
        {
            get
            {
                return GetProperty("Tonality", ref _tonality);
            }
            set
            {
                SetProperty("Tonality", value, ref _tonality);
            }
        }

        [PersistableField]
        public string Scale
        {
            get
            {
                return GetProperty("Scale", ref _scale);
            }
            set
            {
                SetProperty("Scale", value, ref _scale);
            }
        }

        [PersistableField(FieldRetrieveMode.DeferredAsync)]
        public DateTime InsertDate
        {
            get
            {
                return GetProperty("InsertDate", ref _insertDate);
            }
            set
            {
                SetProperty("InsertDate", value, ref _insertDate);
            }
        }

        public override  int UniqueId
        {
            get { return _id; }
        }

        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public string ImagePath
        {
            get
            {
                return GetProperty("ImagePath", ref _imagePath);
            }
            set
            {
                SetProperty("ImagePath", value, ref _imagePath);
            }
        }

        [PersistableField(FieldRetrieveMode.DeferredAsync, FieldManagementType.Indirect)]
        public BitmapImage ImageSource
        {
            get
            {
                return GetProperty("ImageSource", ref _imageSource);
            }
            set
            {
                SetProperty("ImageSource", value, ref _imageSource);
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        #region >-------------- Overrides

        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            switch (propertyName)
            {
                case "Id":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)), 
                        ref  _id, 
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged,avoidOnStatusChanged);
                    break;

                case "MaterialClassId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _materialClassId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "StationId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _stationId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "ImageId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _imageId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "SupplierId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _supplierId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "StatusId":
                    base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                        ref  _statusId,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Code":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _code,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Description":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _description,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "SpecificGravity":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _specificGravity,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "FinishingSurface":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _finishingSurface,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "CostM2":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _costM2,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "CostM3":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _costM3,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "LinearCost":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _linearCost,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "SlabCost":
                    base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                        ref  _slabCost,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Origin":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _origin,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "BlockSize":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _blockSize,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Color":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _color,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Availability":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _availability,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "PrevailingUse":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _prevailingUse,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Polish":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _polish,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Comments":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _comments,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Tonality":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _tonality,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "Scale":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _scale,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "InsertDate":
                    base.SetProperty(propertyName, (DateTime)Convert.ChangeType(value, typeof(DateTime)),
                        ref  _insertDate,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "ImagePath":
                    base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                        ref  _imagePath,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;

                case "ImageSource":
                    base.SetProperty(propertyName, (BitmapImage)Convert.ChangeType(value, typeof(BitmapImage)),
                        ref  _imageSource,
                        forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                    break;


            }
        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _id), typeof(T));
                    break;

                case "MaterialClassId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _materialClassId), typeof(T));
                    break;

                case "StationId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _stationId), typeof(T));
                    break;

                case "ImageId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageId), typeof(T));
                    break;

                case "SupplierId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _supplierId), typeof(T));
                    break;

                case "StatusId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _statusId), typeof(T));
                    break;

                case "Code":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _code), typeof(T));
                    break;

                case "Description":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _description), typeof(T));
                    break;

                case "SpecificGravity":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _specificGravity), typeof(T));
                    break;

                case "FinishingSurface":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _finishingSurface), typeof(T));
                    break;

                case "CostM2":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _costM2), typeof(T));
                    break;

                case "CostM3":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _costM3), typeof(T));
                    break;

                case "LinearCost":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _linearCost), typeof(T));
                    break;

                case "SlabCost":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _slabCost), typeof(T));
                    break;

                case "Origin":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _origin), typeof(T));
                    break;

                case "BlockSize":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _blockSize), typeof(T));
                    break;

                case "Color":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _color), typeof(T));
                    break;

                case "Availability":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _availability), typeof(T));
                    break;

                case "PrevailingUse":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _prevailingUse), typeof(T));
                    break;

                case "Polish":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _polish), typeof(T));
                    break;

                case "Comments":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _comments), typeof(T));
                    break;

                case "Tonality":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _tonality), typeof(T));
                    break;

                case "Scale":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _scale), typeof(T));
                    break;

                case "InsertDate":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _insertDate), typeof(T));
                    break;

                case "MainImage":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _mainImage), typeof(T));
                    break;

                case "ImagePath":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _mainImage), typeof(T));
                    break;

                case "ImageSource":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageSource), typeof(T));
                    break;


            }

            return default(T);
        }

        public override string this[string columnName]
        {
            get
            {
                string retError = string.Empty;
                switch (columnName)
                {
                    case "MaterialClassId":
                        if (_materialClassId == -1)
                            retError = "MaterialClassId value missing.";
                        break;

                    default:
                        break;

                }
                return retError;
            }
        }

        public override bool CanSave()
        {
            return _materialClassId != -1;
        }

        #endregion Overrides  -----------<
    }
}
