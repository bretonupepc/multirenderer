﻿using BrSm.Business.ENTITIES.ARTICLES.PIECES;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.SHAPES.Persistence.SqlServer
{
    public class SqlShapeProxy: SqlEntityProxy
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlShapeProxy(ShapeEntity shape, SqlServerProvider provider, string mainTableName = "T_WK_CUSTOMER_ORDER_DETAILS")
            : base(shape, provider, mainTableName)
        {
            
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods




        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
