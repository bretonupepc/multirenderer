﻿using Breton.DbAccess;
using BrSm.Business.COMMON;
using BrSm.Business.Persistence;
using BrSm.Business.Persistence.SqlServer;

namespace BrSm.Business.ENTITIES.SHAPES.Persistence.SqlServer
{
    public class SqlShapeCollectionProxy:SqlCollectionProxy<ShapeEntity> 
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlShapeCollectionProxy(ShapeCollection shapeCollection, DbInterface dbInterface, string mainTableName = "T_WK_CUSTOMER_ORDER_DETAILS")
            : base(shapeCollection, dbInterface, mainTableName)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields


        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods





        #endregion Public Methods -----------<

        #region >-------------- Protected Methods


        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
