﻿using System;
using Breton.TraceLoggers;
using BrSm.Business.BusinessBase;

namespace BrSm.Business.ENTITIES.SHAPES
{
    public class ShapeEntity : BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private string _code = string.Empty;

        private string _description = string.Empty;

        private double _dimX;
        private double _dimY;
        private double _dimZ;

        private double _qty = 0;

        private int _minQualityId = -1;
        private int _maxQualityId = -1;

        private int _materialId = -1;

        private int _customerProjectId = -1;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Code
        {
            get
            {
                return GetProperty("Code", ref _code);
            }
            set
            {
                SetProperty("Code", value, ref _code);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public string Description
        {
            get
            {
                return GetProperty("Description", ref _description);
            }
            set
            {
                SetProperty("Description", value, ref _description);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double DimX
        {
            get
            {
                return GetProperty("DimX", ref _dimX);
            }
            set
            {
                SetProperty("DimX", value, ref _dimX);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double DimY
        {
            get
            {
                return GetProperty("DimY", ref _dimY);
            }
            set
            {
                SetProperty("DimY", value, ref _dimY);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double DimZ
        {
            get
            {
                return GetProperty("DimZ", ref _dimZ);
            }
            set
            {
                SetProperty("DimZ", value, ref _dimZ);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double Qty
        {
            get
            {
                return GetProperty("Qty", ref _qty);
            }
            set
            {
                SetProperty("Qty", value, ref _qty);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int MinQualityId
        {
            get { return GetProperty("MinQualityId", ref _minQualityId);}
            set { SetProperty("MinQualityId", value, ref _minQualityId);}
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int MaxQualityId
        {
            get { return GetProperty("MaxQualityId", ref _maxQualityId); }
            set { SetProperty("MaxQualityId", value, ref _maxQualityId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int MaterialId
        {
            get
            {
                return GetProperty("MaterialId", ref _materialId);
            }
            set
            {
                SetProperty("MaterialId", value, ref _materialId);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public int CustomerProjectId
        {
            get
            {
                return GetProperty("CustomerProjectId", ref _customerProjectId);
            }
            set
            {
                SetProperty("CustomerProjectId", value, ref _customerProjectId);
            }
        }



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<


        public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
            FieldStatus newStatus = FieldStatus.Set,
            bool forceOnChanged = false,
            bool avoidOnChanged = false,
            bool avoidOnStatusChanged = false)
        {
            try
            {
                switch (propertyName)
                {
                    case "Id":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _id,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Code":
                        base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                            ref _code,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Description":
                        base.SetProperty(propertyName, (string)Convert.ChangeType(value, typeof(string)),
                            ref _description,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "DimX":
                        base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                            ref _dimX,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "DimY":
                        base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                            ref _dimY,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "DimZ":
                        base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                            ref _dimZ,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "Qty":
                        base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
                            ref _qty,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "MinQualityId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _minQualityId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "MaxQualityId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _maxQualityId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "MaterialId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _materialId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;

                    case "CustomerProjectId":
                        base.SetProperty(propertyName, (int)Convert.ChangeType(value, typeof(int)),
                            ref _customerProjectId,
                            forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
                        break;


                    default:
                        //base.SetProperty<T>(propertyName, value, forceStatus, newStatus, forceOnChanged, avoidOnChanged,
                        //    avoidOnStatusChanged);
                        break;
                }


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetProperty error ", ex);
            }

        }

        public override T GetProperty<T>(string propertyName)
        {
            switch (propertyName)
            {
                case "Id":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _id), typeof(T));
                    break;

                case "Code":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _code), typeof(T));
                    break;

                case "Description":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _description), typeof(T));
                    break;

                case "DimX":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimX), typeof(T));
                    break;

                case "DimY":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimY), typeof(T));
                    break;

                case "DimZ":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimZ), typeof(T));
                    break;

                case "Qty":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _qty), typeof(T));
                    break;

                case "MinQualityId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _minQualityId), typeof(T));
                    break;

                case "MaxQualityId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _maxQualityId), typeof(T));
                    break;

                case "MaterialId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _materialId), typeof(T));
                    break;

                case "CustomerProjectId":
                    return (T)Convert.ChangeType(GetProperty(propertyName, ref _customerProjectId), typeof(T));
                    break;

                default:
                    //return base.GetProperty<T>(propertyName);
                    break;
            }

            return default(T);
        }



    }
}
