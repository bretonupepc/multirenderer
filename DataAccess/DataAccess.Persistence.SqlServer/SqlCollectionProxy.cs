﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using DbAccess;
using TraceLoggers;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer.FIELDMAPS;

namespace DataAccess.Persistence.SqlServer
{
    public class SqlCollectionProxy<T> : PersistableCollectionProxy<T> where T : BasePersistableEntity
    {

        #region >-------------- Constants and Enums

        protected const string BASE_SELECT = " SELECT ";
        protected const string BASE_WHERE = " WHERE ";
        protected const string BASE_ORDERBY = " ORDER BY ";

        protected const string BASE_DELETE = " DELETE ";

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private DbInterface _dataAccessInterface;

        private List<FilterEntityMap> _filterEntitymaps = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlCollectionProxy(PersistableEntityCollection<T> collection, DbInterface dbInterface)
            : base(collection)
        {
            DataAccessInterface = dbInterface;
            _mainTableName = GetMainDbTableName(typeof(T), PersistenceProviderType.SqlServer);
        }


        public SqlCollectionProxy(PersistableEntityCollection<T> collection, DbInterface dbInterface, string mainTableName) : this(collection, dbInterface)
        {
            _mainTableName = mainTableName;
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields

        protected Dictionary<string, DbField> _fieldsMap = null;

        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public DbInterface DataAccessInterface
        {
            get { return _dataAccessInterface; }
            set { _dataAccessInterface = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties

        protected internal Dictionary<string, DbField> FieldsMap
        {
            get
            {
                if (_fieldsMap == null)
                {
                    _fieldsMap = SqlServerFieldsMaps.Instance.GetEntityFieldsMap(typeof (T));
                }
                if (_fieldsMap == null || _fieldsMap.Count == 0)
                {
                    Type innerType = typeof (T).BaseType;
                    while (innerType!= null && innerType != typeof (BasePersistableEntity))
                    {
                        _fieldsMap = SqlServerFieldsMaps.Instance.GetEntityFieldsMap(innerType);
                        if (_fieldsMap != null && _fieldsMap.Count > 0)
                        {
                            break;
                        }
                        innerType = innerType.BaseType;
                    }
                }

                return _fieldsMap;
            }
        }

        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Recupera una collection di items da database
        /// tramite elenco campi, filtro di selezione e criterio di ordinamento
        /// </summary>
        /// <param name="set">Set di campi da estrarre</param>
        /// <param name="filterClause">Filtro di selezione</param>
        /// <param name="sortSet">Criterio di ordinamento</param>
        /// <returns></returns>
        //public override bool GetItems(FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
        //{

        //    if (!CanGetItems())
        //    {
        //        return false;
        //    }

        //    string sqlCommand = string.Empty;

        //    if (!BuildSelectCommand(set, filterClause, sortSet, ref sqlCommand)) return false;

        //    this.Collection.Clear();


        //    bool retVal = true;

        //    int exception_step = 0;
        //    OleDbDataReader ord = null;

        //    try
        //    {

        //        if (!_dataAccessInterface.Requery(sqlCommand, out ord))
        //            return false;

        //        if (ord.HasRows )
        //        {
        //            while (ord.Read())
        //            {
        //                T newItem = CreateNewItemFromDbRecord<T>(ord);

        //                if (newItem != default(T))
        //                {
        //                    _collection.Add(newItem);
        //                }
        //            }

        //        }
        //        else
        //            retVal = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine(this + "GetItems error: " , ex);
        //        retVal = false;
        //    }

        //    finally
        //    {
        //        _dataAccessInterface.EndRequery(ord);
        //        ord = null;
        //    }

        //    return retVal;


        //}

        public override bool GetItems(FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
        {

            if (!CanGetItems())
            {
                return false;
            }

            string sqlCommand = string.Empty;

            if (!BuildSelectCommand(set, filterClause, sortSet, ref sqlCommand)) return false;

            this.Collection.Clear();


            bool retVal = true;

            int exception_step = 0;
            OleDbDataReader ord = null;
            
            try
            {

                if (!_dataAccessInterface.Requery(sqlCommand, out ord))
                    return false;

                if (ord.HasRows)
                {
                    while (ord.Read())
                    {
                        T newItem = CreateNewItemFromDbRecord<T>(ord);

                        if (newItem != default(T))
                        {
                            _collection.Add(newItem);
                        }
                    }


                }

                retVal = true;
                //else
                //    retVal = false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this + "GetItems error: ", ex);
                retVal = false;
            }

            finally
            {
                _dataAccessInterface.EndRequery(ord);
                ord = null;
            }

            return retVal;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errors"></param>
        /// <param name="set"></param>
        /// <param name="filterClause"></param>
        /// <param name="sortSet"></param>
        /// <returns></returns>
        public override bool GetItems(out string errors, FieldsSet set = null, FilterClause filterClause = null, FieldsSortSet sortSet = null)
        {
            errors = string.Empty;

            if (!CanGetItems())
            {
                errors += "Failed CanGetItems()";
                return false;
            }

            string sqlCommand = string.Empty;

            if (!BuildSelectCommand(set, filterClause, sortSet, ref sqlCommand))
            {
                errors += string.Format("Failed to build SQL Select command\n{0}", sqlCommand);
                return false;
            }

            this.Collection.Clear();


            bool retVal = true;

            int exception_step = 0;
            OleDbDataReader ord = null;

            try
            {

                if (!_dataAccessInterface.Requery(sqlCommand, out ord))
                {
                    errors += string.Format("Failed to execute SQL Select command\n{0}", sqlCommand);
                    return false;
                }

                if (ord.HasRows)
                {
                    while (ord.Read())
                    {
                        T newItem = CreateNewItemFromDbRecord<T>(ord);

                        if (newItem != default(T))
                        {
                            _collection.Add(newItem);
                        }
                    }


                }

                retVal = true;
                //else
                //    retVal = false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this + "GetItems error: ", ex);
                errors += string.Format("SqlCollection GetItems exception:\n{0}", ex.Message);
                retVal = false;
            }

            finally
            {
                _dataAccessInterface.EndRequery(ord);
                ord = null;
            }

            return retVal;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterClause"></param>
        /// <returns></returns>
        public override bool DeleteItems(FilterClause filterClause)
        {
            bool retVal = false;

            if (!CanGetItems())
            {
                return retVal;
            }

            string sqlCommand = string.Empty;

            try
            {
                if (!BuildDeleteCommand(filterClause, ref sqlCommand)) return false;



                int affectedRows = 0;

                retVal = _dataAccessInterface.ExecuteNonQuery(sqlCommand, out affectedRows);
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DeleteItems error ", ex);
                retVal = false;
            }


            return retVal;
        }


        public override bool DeleteItems(FilterClause filterClause, out string errors)
        {
            errors = string.Empty;

            bool retVal = false;

            if (!CanGetItems())
            {
                errors += "Failed CanGetItems() in DeleteItems()";
                return retVal;
            }

            string sqlCommand = string.Empty;

            try
            {
                if (!BuildDeleteCommand(filterClause, ref sqlCommand))
                {
                    errors += string.Format("Failed to build SQL Delete command\n{0}", sqlCommand);
                    return false;
                }



                int affectedRows = 0;

                retVal = _dataAccessInterface.ExecuteNonQuery(sqlCommand, out affectedRows);
                if (!retVal)
                {
                    errors += string.Format("Failed to execute SQL Delete command\n{0}", sqlCommand);

                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DeleteItems error ", ex);
                errors += string.Format("SqlCollection DeleteItems exception:\n{0}", ex.Message);
                retVal = false;
            }


            return retVal;
        }

        private bool BuildDeleteCommand(FilterClause filterClause, ref string sqlCommand)
        {
            if (filterClause == null)
            {
                return false;
            }

            _filterEntitymaps = new List<FilterEntityMap>();

            filterClause.BuildFilterEntityMaps(typeof(T), _filterEntitymaps, PersistenceProviderType.SqlServer);

            string deletePart = BuildDeletePart(_fieldsInfos);

            if (string.IsNullOrEmpty(deletePart))
                return false;

            string fromPart = BuildFromPart(filterClause);
            if (string.IsNullOrEmpty(fromPart))
                return false;

            string wherePart = BuildWherePart(filterClause);


            sqlCommand = string.Concat(deletePart, fromPart, wherePart);
            return true;
        }

        private string BuildDeletePart(FieldsSet fieldsInfos)
        {
            string deleteStr = BASE_DELETE;

            bool isValid = true;

            string mainAlias = "A";

            FilterEntityMap mainFem = _filterEntitymaps.FirstOrDefault(f => f.MainTableName == _mainTableName);

            if (mainFem != null)
            {
                mainAlias = mainFem.Alias;
            }

            deleteStr = string.Format("{0} {1} ", deleteStr, mainAlias);

            isValid = deleteStr != BASE_SELECT;

            return isValid ? deleteStr : string.Empty;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// Crea un nuovo oggetto a partire dal record database
        /// </summary>
        /// <typeparam name="T">Tipo oggetto</typeparam>
        /// <param name="ord">Oggetto [Reader] da database</param>
        /// <returns></returns>
        protected virtual T CreateNewItemFromDbRecord<T>(OleDbDataReader ord) where T : BasePersistableEntity
        {

            T newItem = Activator.CreateInstance<T>();
            try
            {

                foreach (FieldItemInfo field in FieldsInfos)
                {
                    if (field.RetrieveMode == FieldRetrieveMode.Immediate &&
                        FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                    {
                        object fieldValue = ord[FieldsMap[field.BaseInfo.PropertyName].Name];

                        if (fieldValue != DBNull.Value)
                        {
                            newItem.SetProperty(field.BaseInfo.PropertyName, fieldValue, true, FieldStatus.Read);
                        }
                        else
                        {
                            newItem.SetPropertyStatus(field.BaseInfo.PropertyName, FieldStatus.Read);
                        }


                    }
                }

            }
            catch (Exception ex)
            {
                
                TraceLog.WriteLine("CreateNewItem error ", ex);
                newItem = default(T);
            }

            return newItem ;
        }

        /// <summary>
        /// Costruisce la stringa SQL di estrazione dati
        /// </summary>
        /// <param name="set">Set campi</param>
        /// <param name="filterClause">Filtro di estrazione</param>
        /// <param name="sortSet">Criteri ordinamento</param>
        /// <param name="sqlCommand">Stringa SQL prodotta</param>
        /// <returns></returns>
        protected virtual bool BuildSelectCommand(FieldsSet set, FilterClause filterClause, FieldsSortSet sortSet, ref string sqlCommand)
        {
            if (set == null)
            {
                if (_fieldsInfos == null || _fieldsInfos.Count == 0)
                {
                    BuildDefaultFieldsInfos();
                }
            }
            else
            {
                _fieldsInfos = set;
            }

            _filterEntitymaps = new List<FilterEntityMap>();

            if (filterClause != null)
            {
                filterClause.BuildFilterEntityMaps(typeof(T), _filterEntitymaps, PersistenceProviderType.SqlServer);
            }

            string selectPart = BuildSelectPart(_fieldsInfos);
            if (string.IsNullOrEmpty(selectPart))
                return false;

            string fromPart = BuildFromPart(filterClause);
            if (string.IsNullOrEmpty(fromPart))
                return false;

            string wherePart = string.Empty;
            //if (filterClause != null)
            {
                wherePart = BuildWherePart(filterClause);
            }


            string sortPart = string.Empty;
            if (sortSet != null)
            {
                sortPart = BuildOrderByPart(sortSet);
            }

            sqlCommand = string.Concat(selectPart, fromPart, wherePart, sortPart);
            return true;
        }

        protected virtual bool CanGetItems()
        {
            return (_dataAccessInterface != null && _collection != null);
        }

        /// <summary>
        /// Costruisce la parte SELECT della stringa SQL
        /// </summary>
        /// <param name="set">Set campi</param>
        /// <returns></returns>
        protected virtual string BuildSelectPart(FieldsSet set)
        {
            string selectStr = BASE_SELECT;

            bool isValid = false;

            string mainAlias = "A";
            FilterEntityMap mainFem = _filterEntitymaps.FirstOrDefault(f => f.MainTableName == _mainTableName);

            if (mainFem != null)
            {
                mainAlias = mainFem.Alias;
            }

            if (set != null)
            {
                foreach (FieldItemInfo field in set)
                {
                    if (field.RetrieveMode == FieldRetrieveMode.Immediate &&
                        FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                    {
                        selectStr += string.Format("{0}.[{1}], ", mainAlias, FieldsMap[field.BaseInfo.PropertyName].Name);

                    }
                }

                isValid = selectStr != BASE_SELECT;
                selectStr = selectStr.Substring(0, selectStr.Length - 2) + " ";
            }

            else if (FieldsMap.Count > 0)
            {
                foreach (KeyValuePair<string, DbField> kvp in FieldsMap)
                {
                    selectStr += kvp.Value.Name + ", ";
                }

                isValid = !string.IsNullOrEmpty(selectStr);
                selectStr = selectStr.Substring(0, selectStr.Length - 2) + " ";
            }

            return isValid ? selectStr : string.Empty;
        }

        /// <summary>
        /// Costruisce la parte FROM della stringa SQL
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildFromPart(FilterClause filterClause)
        {
            string fromStr = string.Empty;

            string mainAlias = "A";
            FilterEntityMap mainFem = _filterEntitymaps.FirstOrDefault(f => f.MainTableName == _mainTableName);

            if (mainFem != null)
            {
                mainAlias = mainFem.Alias;
            }

            if (filterClause == null)
            {
                if (!string.IsNullOrEmpty(_mainTableName))
                {
                    fromStr = string.Format(" FROM [{0}] AS {1} ", _mainTableName, mainAlias);
                }
            }
            else
            {
                fromStr = " FROM ";
                foreach (var item in _filterEntitymaps)
                {
                    fromStr += string.Format("[{0}] AS {1} , ", item.MainTableName, item.Alias);
                }
                fromStr = fromStr.Substring(0, fromStr.Length - 2);
            }



            return fromStr;
        }

        /// <summary>
        /// Costruisce la parte WHERE della stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        protected virtual string BuildWherePart(FilterClause clause)
        {
            string whereStr = BASE_WHERE;

            string mainAlias = "A";

            FilterEntityMap mainFem = _filterEntitymaps.FirstOrDefault(f => f.MainTableName == _mainTableName);

            if (mainFem != null)
            {
                mainAlias = mainFem.Alias;
            }


            bool isValid = false;

            if (clause != null)
            {
                string whereContent = string.Empty;

                //Table joins
                string whereJoins = string.Empty;
                if (_filterEntitymaps.Count > 0)
                {
                    foreach (var item in _filterEntitymaps)
                    {
                        foreach (var relation in item.Relations)
                        {
                            whereJoins += string.Format("({0}) AND ", relation.Trim());
                        }
                    }
                }
                

                isValid = ParseClause(clause, out whereContent);

                if (isValid)
                {
                    if (!string.IsNullOrEmpty(whereJoins))
                    {
                        whereContent = whereJoins + whereContent;
                    }
                    whereStr += whereContent;
                }
                else
                {
                    if (!string.IsNullOrEmpty(whereJoins))
                    {
                        whereJoins = whereJoins.Substring(0, whereJoins.Length - 4);
                        whereStr += whereJoins;
                        isValid = true;
                    }

                }

            }

            return isValid ? whereStr : string.Empty;
        }

        /// <summary>
        /// Analizza il filtro di selezione creando la relativa stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <param name="sqlClauseString"></param>
        /// <returns></returns>
        protected bool ParseClause(FilterClause clause, out string sqlClauseString)
        {
            sqlClauseString = string.Empty;

            bool retVal = true;

            bool isValid = true;

            if (clause.ParentClause != null)
            {
                string logicalConnector = string.Empty;

                switch (clause.ParentConnector)
                {
                    case LogicalConnector.And:
                        logicalConnector = " AND ";
                        break;

                    case LogicalConnector.AndNot:
                        logicalConnector = " AND NOT ";
                        break;

                    case LogicalConnector.Or:
                        logicalConnector = " OR ";
                        break;

                    case LogicalConnector.OrNot:
                        logicalConnector = " OR NOT ";
                        break;

                }
                sqlClauseString += logicalConnector;
            }

            // Main clause
            //if (!clause.FieldInfo.PropertyName.Contains("."))
            
            {
                //if (FieldsMap.ContainsKey(clause.FieldInfo.PropertyName)) 
                {
                    string mainClauseString;


                    isValid = BuildMainClauseString(clause, out mainClauseString);
                    if (isValid)
                    {
                        if (clause.ChildrenClauses.Count > 0)
                        {
                            sqlClauseString += " ( ";
                        }

                        sqlClauseString += mainClauseString;

                        string childrenClausesString;

                        isValid = BuildChildrenClausesString(clause, out childrenClausesString);
                        if (isValid)
                        {
                            sqlClauseString += childrenClausesString;
                        }

                        if (clause.ChildrenClauses.Count > 0)
                        {
                            sqlClauseString += " ) ";
                        }

                    }

                }
            }
            //else
            //{
            //    //TODO:composite property
            //}



            return isValid;
        }

        /// <summary>
        /// Costruisce la parte ORDER BY della stringa SQL
        /// </summary>
        /// <param name="set">Set criteri di ordinamento</param>
        /// <returns></returns>
        protected virtual string BuildOrderByPart(FieldsSortSet set)
        {
            string orderByStr = string.Empty;

            bool isValid = true;

            if (set != null)
            {
                orderByStr = BASE_ORDERBY;
                foreach (var item in set)
                {
                    if (FieldsMap.ContainsKey(item.FieldName))
                    {
                        orderByStr += string.Format(" {0} {1}, ", FieldsMap[item.FieldName].Name,
                            item.Sort == SortOrder.Ascending ? "ASC" : "DESC");
                    }

                }
                isValid = orderByStr != BASE_ORDERBY;
                orderByStr = orderByStr.Substring(0, orderByStr.Length - 2) + " ";

            }

            return isValid ? orderByStr : string.Empty;

        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        /// <summary>
        /// Analizza la clausola principale di un filtro di selezione creando la relativa stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <param name="clauseContent"></param>
        /// <returns></returns>
        private bool BuildMainClauseString(FilterClause clause, out string clauseContent)
        {
            bool isValid = true;

            clauseContent = string.Empty;

            string mainAlias = "A";
            FilterEntityMap mainFem = _filterEntitymaps.FirstOrDefault(f => f.MainTableName == _mainTableName);

            if (mainFem != null)
            {
                mainAlias = mainFem.Alias;
            }


            if (FieldsMap.ContainsKey(clause.FieldInfo.PropertyName)) // è campo diretto, interno alla classe
            {
                clauseContent += " (";

                clauseContent += string.Format("{0}.[{1}]", mainAlias, FieldsMap[clause.FieldInfo.PropertyName].Name);

                FilterOperator fo = SqlHelper.FilterOperators.FirstOrDefault(f => f.Operator == clause.Operator);

                if (fo != null)
                {
                    clauseContent += fo.OperatorRender;

                    switch (fo.ValueType)
                    {
                        case ConditionOperatorValueType.MultipleValuesRequired:
                            //clauseContent += " ( ";

                            clauseContent += BuildClauseValues(clause);

                            //clauseContent += " ) ";
                            break;

                        case ConditionOperatorValueType.SingleValueRequired:
                            clauseContent += BuildClauseValue(clause);

                            break;

                        case ConditionOperatorValueType.NoValueRequired:
                            break;
                    }
                }

                clauseContent += ") ";

            }
            else if (clause.FieldInfo.PropertyName.Contains(".")) //inner object
            {
                clauseContent += " (";

                var entityName = clause.FieldInfo.PropertyName.Substring(0,
                    clause.FieldInfo.PropertyName.LastIndexOf(".") );
                var propertyName = clause.FieldInfo.PropertyName.Substring(entityName.Length +1);
                var fem = _filterEntitymaps.FirstOrDefault(m => m.EntityName == entityName);
                var alias = (fem != null) ? fem.Alias : "NO_TABLE";

                clauseContent += string.Format("{0}.[{1}]", alias, fem.EntitiesFieldMaps[propertyName].Name);

                FilterOperator fo = SqlHelper.FilterOperators.FirstOrDefault(f => f.Operator == clause.Operator);

                if (fo != null)
                {
                    clauseContent += fo.OperatorRender;

                    switch (fo.ValueType)
                    {
                        case ConditionOperatorValueType.MultipleValuesRequired:
                            //clauseContent += " ( ";

                            clauseContent += BuildClauseValues(clause);

                            //clauseContent += " ) ";
                            break;

                        case ConditionOperatorValueType.SingleValueRequired:
                            clauseContent += BuildClauseValue(clause);

                            break;

                        case ConditionOperatorValueType.NoValueRequired:
                            break;
                    }
                }

                clauseContent += ") ";
            }
            else
            {
                isValid = false;
            }


            return isValid;

        }

        /// <summary>
        /// Traduce un valore di una clausola in formato SQL
        /// </summary>
        /// <param name="clause">Clausola di filtro</param>
        /// <param name="valueIndex">Progressivo all'interno dell'elenco valori della clausola</param>
        /// <returns></returns>
        private string BuildClauseValue(FilterClause clause, int valueIndex = 0)
        {
            string value = string.Empty;

            Type valueType = clause.Values[valueIndex].GetType();
            string clauseValue = string.Empty;

            //if (clause.FieldInfo.PropertyType == typeof (string))
            if (valueType == typeof(string))
            {
                return clause.Values[0] == null
                    ? " '' "
                    : string.Format(" '{0}' ", SqlServerProvider.FixSqlString(clause.Values[valueIndex].ToString()));
            }
            //else if (clause.FieldInfo.PropertyType == typeof (DateTime))
            else if (valueType == typeof(DateTime))
            {
                return clause.Values[0] == null
                    ? " '' "
                    : string.Format(" '{0}' ", SqlServerProvider.DateSqlFormat((DateTime)clause.Values[valueIndex]));
            }
            else if (clause.Values[0] is IEnumerable<string>)
            {
                value = string.Join(",", (clause.Values[0] as IEnumerable<string>).Select(p => "'" + p.ToString() +"'"));
            }
            else if (clause.Values[0] is IEnumerable<int>)
            {
                value = string.Join(",", (clause.Values[0] as IEnumerable<int>).Select(p =>  p.ToString(CultureInfo.InvariantCulture) ));
            }
            else if (clause.Values[0] is IEnumerable<double>)
            {
                value = string.Join(",", (clause.Values[0] as IEnumerable<double>).Select(p =>  p.ToString(CultureInfo.InvariantCulture) ));
            }
            else if (clause.Values[0] is IEnumerable<float>)
            {
                value = string.Join(",", (clause.Values[0] as IEnumerable<float>).Select(p =>  p.ToString(CultureInfo.InvariantCulture) ));
            }
            else
            {
                return clause.Values[0] == null
                    ? " "
                    : string.Format(" {0} ", clause.Values[valueIndex].ToString());
            }

            return value;
        }

        /// <summary>
        /// Traduce il set di valori di una clausola in formato SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        private string BuildClauseValues(FilterClause clause)
        {
            string clauseValue = " (";

            for (int i = 0; i < clause.Values.Count; i++)
            {
                clauseValue += BuildClauseValue(clause, i);
                clauseValue += ", ";
            }
            if (clauseValue.Length > 2)
            {
                clauseValue = clauseValue.Substring(0, clauseValue.Length - 2) + " ";
            }

            clauseValue += ") ";

            return clauseValue;

        }

        /// <summary>
        /// Analizza le clausole figlie di un filtro di selezione creando la relativa stringa SQL
        /// </summary>
        /// <param name="clause"></param>
        /// <param name="childrenClausesString"></param>
        /// <returns></returns>
        private bool BuildChildrenClausesString(FilterClause clause, out string childrenClausesString)
        {
            bool isValid = true;
            childrenClausesString = string.Empty;

            if (clause.ChildrenClauses.Count > 0)
            {
                foreach (var singleClause in clause.ChildrenClauses)
                {
                    string childClauseString;
                    if (ParseClause(singleClause, out childClauseString))
                    {
                        childrenClausesString += childClauseString;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
            }

            return isValid;
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
