﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using DbAccess;
using TraceLoggers;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.COMMON;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer.FIELDMAPS;

namespace DataAccess.Persistence.SqlServer
{
    [PersistenceProvider(PersistenceProviderType.SqlServer)]
    public class SqlServerProvider: IPersistenceProvider
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private PersistenceProviderType _providerType = PersistenceProviderType.SqlServer;

        private  string _udlFilename = "DbConnection.udl";

        private  DbInterface _dataAccessInterface;

        private string _currentUserName = string.Empty;

        private bool _saveMode = false;

        #region Workers

        private readonly SynchronizationContext _synchroContext;

        private bool _canStartWorkers = true;

        private SafeCollection<Thread> _workers = new SafeCollection<Thread>();

        #endregion Workers


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlServerProvider()
        {
            _synchroContext = SynchronizationContext.Current;
        }

        public SqlServerProvider(string udlFilename): this()
        {
            UdlFilename = udlFilename;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties


        public DbInterface DataAccessInterface
        {
            get
            {
                if (_dataAccessInterface == null)
                {
                    _dataAccessInterface = new DbInterface();

                    TraceLog.WriteLine(string.Format("Tentativo apertura file udl {0}", UdlFilename));

                    if (_dataAccessInterface.Open("", UdlFilename))
                    {
                        TraceLog.WriteLine(string.Format("Tentativo apertura file udl {0} riuscito.", UdlFilename));
                    }
                    else
                    {
                        TraceLog.WriteLine(string.Format("Tentativo apertura file udl {0} fallito.", UdlFilename));
                    }
                }
                return _dataAccessInterface;
            }
        }


        public string UdlFilename
        {
            get { return _udlFilename; }
            set { _udlFilename = value; }
        }

        public string CurrentUserName
        {
            get { return _currentUserName; }
            set { _currentUserName = value; }
        }

        public SynchronizationContext SynchroContext
        {
            get { return _synchroContext; }
        }


        #region >---------------------> IPersistenceProvider Members

        public bool SaveMode
        {
            get {return _saveMode; }
        }

        public bool CanStartWorkers
        {
            get { return _canStartWorkers; }
            set { _canStartWorkers = value; }
        }

        public SafeCollection<Thread> Workers
        {
            get { return _workers; }
            set
            {
                _workers = value;
                OnPropertyChanged("Workers");
            }
        }

        public PersistenceProviderType ProviderType
        {
            get { return _providerType; }
        }

        #endregion <---------------------< IPersistenceProvider Members


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods


        #region >---------------------> IPersistenceProvider Members

        public bool BeginTransaction()
        {
            bool retVal = false;
            if (DataAccessInterface.BeginTransaction())
            {
                retVal = true;
                _saveMode = true;
            }

            return retVal;
        }

        public bool EndTransaction(bool commit)
        {
            bool retVal = false;
            if (DataAccessInterface.TransactionActive())
            {
                retVal = true;
                DataAccessInterface.EndTransaction(commit);
                _saveMode = false;
            }
            
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="machineId"></param>
        /// <param name="id"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        public bool CloneMachineAndAlarmsOnDb(int machineId, out int id, out string errors )
        {
            bool retVal = false;
            id = -1;
            errors = string.Empty;

            ArrayList parameters = new ArrayList();
            OleDbParameter parameter = new OleDbParameter("@MachineId", OleDbType.Integer);
            parameter.Value = machineId;
            parameters.Add(parameter);

            retVal = DataAccessInterface.Execute("ST_CLONE_MACHINE", out id, ref parameters);

            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool ClearPlantLayout()
        {
            bool retVal = false;


            retVal = DataAccessInterface.Execute("TRUNCATE TABLE T_AN_PLANT_LAYOUT");

            return retVal;
        }

        public void StopWorkers()
        {
            _canStartWorkers = false;

            while (_workers.Count > 0)
            {
                for (int i = _workers.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        Thread thread = _workers[i];
                        if (thread != null && thread.IsAlive)
                        {
                            thread.Abort();
                        }
                        else
                        {
                            _workers.RemoveAt(i);
                        }

                    }
                    catch { }

                }
                Thread.Sleep(10);
            }
        }

        public PersistableCollectionProxy<T> GetDefaultCollectionProxy<T>(PersistableEntityCollection<T> collection, bool saveMode = false) where T : BasePersistableEntity
        {
            SqlCollectionProxy<T> proxy;
            if (SaveMode)
            {
                proxy = new SqlCollectionProxy<T>(collection, DataAccessInterface);
            }
            else
            {
                proxy = new SqlCollectionProxy<T>(collection, DataAccessInterface.Clone());
            }

            return proxy;
        }

        //public PersistableCollectionProxy<T> GetDefaultCollectionProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity
        //{
        //    throw new NotImplementedException();
        //}


        //public PersistableEntityProxy GetEntityProxy<T>(T entity) where T : BasePersistableEntity
        //{
        //    PersistableEntityProxy proxy = null;
        //    switch (entity.GetType().Name)
        //    {
        //        case "MaterialEntity":
        //            return new SqlMaterialProxy(entity as MaterialEntity, this);
        //            break;

        //        case "MaterialClassEntity":

        //            break;

        //        case "ArticleEntity":
        //            return new SqlArticleProxy(entity as ArticleEntity, this);
        //            break;

        //        case "SlabEntity":
        //            return new SqlSlabProxy(entity as SlabEntity, this);
        //            break;

        //        case "SlabFaceEntity":
        //            return new SqlSlabFaceProxy(entity as SlabFaceEntity, this);
        //            break;

        //        case "OptimizableSlab":
        //            return new SqlOptimizableSlabProxy(entity as OptimizableSlab, this);
        //            break;



        //        default:

        //            break;
        //    }

        //    return proxy;
        //}
        
        public PersistableEntityProxy GetDefaultEntityProxy<T>(T entity, bool saveMode = false) where T : BasePersistableEntity
        {
            PersistableEntityProxy proxy = new SqlEntityProxy(entity, this, SaveMode);


            return proxy;
        }

        public string GetMainDbTableName(Type type)
        {
            return PersistableEntityBaseInfos.GetMainDbTableName(type, PersistenceProviderType.SqlServer);
        }

        public Dictionary<string, DbField> GetEntityFieldsMap(Type type)
        {
            var fieldsMap = new Dictionary<string, DbField>();

            var map = SqlServerFieldsMaps.Instance.GetEntityFieldsMap(type);
            if (map == null || map.Count == 0)
            {
                Type innerType = type.BaseType;
                while (innerType != null && innerType != typeof(BasePersistableEntity))
                {
                    map = SqlServerFieldsMaps.Instance.GetEntityFieldsMap(innerType);
                    if (map != null && map.Count > 0)
                    {
                        fieldsMap = map;
                        break;
                    }
                    innerType = innerType.BaseType;
                }
            }
            else
            {
                fieldsMap = map;
            }

            return fieldsMap;
        }

        #endregion <---------------------< IPersistenceProvider Members

        public DataTable GetDataTable(string selectSql)
        {
            //TraceLog.WriteLine(string.Format("GetDataTable avviata [{0}]", selectSql));

            DataTable dTable = new DataTable();

            OleDbDataReader reader;

            DataAccessInterface.Requery(selectSql, out reader);

            if (reader != null)
            {
                dTable.Load(reader);
            }

            DataAccessInterface.EndRequery(reader);

            //TraceLog.WriteLine(string.Format("GetDataTable conclusa [{0}]", selectSql));


            return dTable;

        }

        //[Obsolete("Please don't use anymore")]
        //public SqlEntityProxy GetProxy(BasePersistableEntity entity)
        //{
        //    switch (entity.GetType().ToString())
        //    {
        //        case "MaterialEntity":
        //            return new SqlMaterialProxy(entity as MaterialEntity, this);

        //        default:
        //            return null;
        //    }
        //}

        //[Obsolete("Please don't use anymore")]
        //public SqlCollectionProxy<T> GetProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity
        //{
        //    SqlCollectionProxy<T> proxy = null;
        //    switch (typeof (T).Name)
        //    {
        //        case "MaterialEntity":
        //            proxy = new SqlMaterialCollectionProxy(collection as MaterialCollection, DataAccessInterface) as SqlCollectionProxy<T>;
        //            break;

        //    }
        //    return proxy;

        //}


        //public SqlCollectionProxy<T> GetProxy<T>(PersistableEntityCollection<T> collection ) where T:BasePersistableEntity
        //{
        //    SqlMaterialCollectionProxy proxy = new SqlMaterialCollectionProxy(new MaterialCollection());

        //    return proxy;
        //    //switch (typeof(T).Name)
        //    //{
        //    //    case "MaterialEntity":

        //    //        //return new SqlMaterialCollectionProxy(collection as MaterialCollection);

        //    //    default:
        //    //        return null;
        //    //}
        //}


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private static object IsDbNull(object value, object nullValueReplacement)
        {
            if (value == DBNull.Value)
                return nullValueReplacement;

            return value;
        }

        private static string NotNullString(object dataValue)
        {
            if (dataValue.Equals(DBNull.Value))
            {
                return string.Empty;
            }
            else
            {
                return dataValue.ToString();
            }
        }

        public static string DateSqlFormat(DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string FixSqlString(string inputStr)
        {
            if (!string.IsNullOrEmpty(inputStr))
                return inputStr.Replace("\'", "\'\'");
            return inputStr;
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        //private 



        #region IPersistenceProvider Members

        //public PersistableCollectionProxy<T> GetCollectionProxy<T>(PersistableEntityCollection<T> collection) where T : BasePersistableEntity
        //{
        //    SqlCollectionProxy<T> proxy = null;
        //    switch (typeof(T).Name)
        //    {
        //        case "MaterialEntity":
        //            proxy = new SqlMaterialCollectionProxy(collection as MaterialCollection, DataAccessInterface.Clone()) as SqlCollectionProxy<T>;
        //            break;

        //        case "ArticleEntity":
        //            proxy = new SqlArticleCollectionProxy(collection as ArticleCollection, DataAccessInterface.Clone()) as SqlCollectionProxy<T>;
        //            break;

        //        case "SlabEntity":
        //            proxy = new SqlSlabCollectionProxy(collection as SlabCollection, DataAccessInterface.Clone()) as SqlCollectionProxy<T>;
        //            break;

        //        case "SlabFaceEntity":
        //            proxy = new SqlSlabFaceCollectionProxy(collection as SlabFaceCollection, DataAccessInterface.Clone()) as SqlCollectionProxy<T>;
        //            break;

        //        case "OptimizableSlab":
        //            proxy = new SqlOptimizableSlabCollectionProxy(collection as OptimizableSlabCollection, DataAccessInterface.Clone()) as SqlCollectionProxy<T>;
        //            break;


        //    }
        //    return proxy;

        //    return proxy;
        //}

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }

}
