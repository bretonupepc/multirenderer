﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrSm.Business.BusinessBase;

namespace BrSm.Persistence.SqlServer.FIELDMAPS
{
    internal class FieldMapDictionaries
    {

        internal static readonly Dictionary<string, DbField> ArticleEntityMap = new Dictionary<string, DbField>()
        {
            {"Id", new DbField("F_ID", true)},
            {"DimX", new DbField("F_DIM_X", true)},
            {"DimY", new DbField("F_DIM_Y", true)},
            {"DimZ", new DbField("F_DIM_Z", true)},
            {"OffsetX", new DbField("F_OFFSET_X")},
            {"OffsetY", new DbField("F_OFFSET_Y")},
            {"Surface", new DbField("F_SURFACE")},
            {"CommercialSurface", new DbField("F_COMMERCIAL_SURFACE")},
            {"InternalRectLeft",new DbField("F_INTERNAL_RECT_LEFT")},
            {"InternalRectTop",new DbField("F_INTERNAL_RECT_TOP")},
            {"InternalRectWidth",new DbField("F_INTERNAL_RECT_WIDTH")},
            {"InternalRectHeight",new DbField("F_INTERNAL_RECT_HEIGHT")},
            {"Tone",new DbField("F_TONE")},
            {"XmlParameter",new DbField("F_XML_PARAMETER")},
            {"PositionStopX",new DbField("F_POSITION_STOP_X")},
            {"PositionStopY",new DbField("F_POSITION_STOP_Y")},
            {"UnitCost",new DbField("F_UNIT_COST")},
            {"Defects",new DbField("F_DEFECTS")},
            {"ImageId",new DbField("F_ID_T_AN_ARTICLE_IMAGES")},
            {"QualityId",new DbField("F_ID_T_LS_ARTICLE_QUALITIES")},

            {"Code",new DbField("F_CODE", true)},
            {"Description",new DbField("F_DESCRIPTION")},
            {"BatchCode",new DbField("F_BATCH_CODE")},
            {"Qty",new DbField("F_QTY", true)},

            {"ArticleType",new DbField("F_ID_T_CO_ARTICLE_TYPES", true, "T_CO_ARTICLE_TYPES", "F_ID")},
            {"ArticleStatus",new DbField("F_ID_T_CO_ARTICLE_STATES", true, "T_CO_ARTICLE_STATES", "F_ID")},
            {"MaterialId",new DbField("F_ID_T_AN_MATERIALS", true, "T_AN_MATERIALS", "F_ID")},
            {"StockHusId",new DbField("F_ID_T_AN_STOCK_HUS")},
            {"DdtaDetailsId",new DbField("F_ID_T_AN_DDTA_DETAILS")},
            {"SupplierId",new DbField("F_ID_T_AN_SUPPLIERS")},
            {"CustomerProjectId",new DbField("F_ID_T_AN_SUPPLIERS")},
            {"InsertDate",new DbField("F_INSERT_DATE")},
            {"HusPosition",new DbField("F_HUS_POSITION")},
            {"BlobsPartProgramId",new DbField("F_ID_T_AN_BLOBS_PART_PROGRAM")},
            {"BlobsPolygonId",new DbField("F_ID_T_AN_BLOBS_POLYGON")},
            {"Weight",new DbField("F_WEIGHT")}
        };

        internal static readonly Dictionary<string, DbField> SlabFaceEntityMap = new Dictionary<string, DbField>()
        {
            {"Id",new DbField("F_ID")},
            {"SlabId",new DbField("F_ID_T_AN_ARTICLES")},
            {"FaceId",new DbField("F_FACE")},
            {"Used",new DbField("F_USED")},
            {"DimX",new DbField("F_DIM_X")},
            {"DimY",new DbField("F_DIM_Y")},
            {"DimZ",new DbField("F_DIM_Z")},
            {"OffsetX",new DbField("F_OFFSET_X")},
            {"OffsetY",new DbField("F_OFFSET_Y")},
            {"Surface",new DbField("F_SURFACE")},
            {"CommercialSurface",new DbField("F_COMMERCIAL_SURFACE")},
            {"InternalRectLeft",new DbField("F_INTERNAL_RECT_LEFT")},
            {"InternalRectTop",new DbField("F_INTERNAL_RECT_TOP")},
            {"InternalRectWidth",new DbField("F_INTERNAL_RECT_WIDTH")},
            {"InternalRectHeight",new DbField("F_INTERNAL_RECT_HEIGHT")},
            {"Tone",new DbField("F_TONE")},
            {"XmlParameter",new DbField("F_XML_PARAMETER")},
            {"PositionStopX",new DbField("F_POSITION_STOP_X")},
            {"PositionStopY",new DbField("F_POSITION_STOP_Y")},
            {"UnitCost",new DbField("F_UNIT_COST")},
            {"Defects",new DbField("F_DEFECTS")},
            {"ImageId",new DbField("F_ID_T_AN_ARTICLE_IMAGES")},
            {"QualityId",new DbField("F_ID_T_LS_ARTICLE_QUALITIES")}
        };

        internal static readonly Dictionary<string, DbField> MaterialEntityMap = new Dictionary<string, DbField>()
        {
            {"Id",new DbField("F_ID")},
            {"MaterialClassId",new DbField("F_ID_T_LS_MATERIAL_CLASSES")},
            {"StationId",new DbField("F_ID_T_AN_STATIONS")},
            {"ImageId",new DbField("F_ID_T_AN_MATERIAL_IMAGES")},
            {"SupplierId",new DbField("F_ID_T_AN_SUPPLIERS")},
            {"StatusId",new DbField("F_ID_T_CO_MATERIAL_STATES")},
            {"Code",new DbField("F_CODE")},
            {"Description",new DbField("F_DESCRIPTION")},
            {"SpecificGravity",new DbField("F_SPECIFIC_GRAVITY")},
            {"FinishingSurface",new DbField("F_FINISHING_SURFACE")},
            {"CostM2",new DbField("F_COST_M2")},
            {"CostM3",new DbField("F_COST_M3")},
            {"LinearCost",new DbField("F_LINEAR_COST")},
            {"SlabCost",new DbField("F_SLAB_COST")},
            {"Origin",new DbField("F_ORIGIN")},
            {"BlockSize",new DbField("F_BLOCK_SIZE")},
            {"Color",new DbField("F_COLOR")},
            {"Availability",new DbField("F_AVAILABILITY")},
            {"PrevailingUse",new DbField("F_PREVAILING_USE")},
            {"Polish",new DbField("F_POLISH")},
            {"Comments",new DbField("F_COMMENTS")},
            {"Tonality",new DbField("F_TONALITY")},
            {"Scale",new DbField("F_SCALE")},
            {"InsertDate",new DbField("F_INSERT_DATE")},
        };

        internal static readonly Dictionary<string, DbField> AreaAttributeMap = new Dictionary<string, DbField>()
        {
            {"Id",new DbField("F_ID",true)},
            {"Code",new DbField("F_CODE", true)},
            {"ShortCode",new DbField("F_SHORT_CODE")},
            {"Description",new DbField("F_DESCRIPTION", true)},
            {"BothFaces",new DbField("F_BOTH_FACES", true)},
            {"DefaultRelevance",new DbField("F_DEFAULT_RELEVANCE", true)},
        };


        internal static readonly Dictionary<string, DbField> QualityClassMap = new Dictionary<string, DbField>()
        {
            {"Id",new DbField("F_ID",true)},
            {"Code",new DbField("F_CODE", true)},
            {"Description",new DbField("F_DESCRIPTION", true)},
            {"MaterialId",new DbField("F_ID_T_AN_MATERIALS")},
            {"MaterialClassId",new DbField("F_ID_T_LS_MATERIAL_CLASSES")},
            {"MinThickness",new DbField("F_MIN_THICKNESS", true)},
            {"MaxThickness",new DbField("F_MAX_THICKNESS", true)},
            {"MaxDefectsNum",new DbField("F_MAX_DEFECTS", true)},
            {"MaxDefectsArea",new DbField("F_MAX_DEFECTS_AREA", true)},
            {"QualityLevel",new DbField("F_QUALITY_LEVEL", true)},
        };

        internal static readonly Dictionary<string, DbField> QualityClassAreaAttributeMap = new Dictionary<string, DbField>()
        {
            {"Id",new DbField("F_ID",true)},
            {"QualityClassId",new DbField("F_ID_T_LS_QUALITY_CLASSES", true)},
            {"AreaAttributeId",new DbField("F_ID_T_LS_ATTRIBUTES", true)},
            {"MinAttributeNum",new DbField("F_MIN_ATTRIBUTE_NUMBER")},
            {"MinAttributeArea",new DbField("F_MIN_ATTRIBUTE_AREA")},
            {"MaxAttributeNum",new DbField("F_MAX_ATTRIBUTE_NUMBER")},
            {"MaxAttributeArea",new DbField("F_MAX_ATTRIBUTE_AREA")},
        };

    }
}
