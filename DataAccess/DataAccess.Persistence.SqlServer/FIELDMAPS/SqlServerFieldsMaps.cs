﻿using System;
using System.Collections.Generic;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace DataAccess.Persistence.SqlServer.FIELDMAPS
{
    public class SqlServerFieldsMaps:DatabaseFieldsMaps
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private static SqlServerFieldsMaps _instance = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        private SqlServerFieldsMaps()
        {
            _providerType = PersistenceProviderType.SqlServer;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public static SqlServerFieldsMaps Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SqlServerFieldsMaps();
                }
                return _instance;
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override void AddEntityFieldMap(Type entityType)
        {

            var entityFieldMap = new Dictionary<string, DbField>();
            var requestedDict = new Dictionary<string, DbField>();

            List<FieldItemBaseInfo> fieldsInfo = PersistableEntityBaseInfos.GetBaseInfos(entityType);

            if (fieldsInfo == null)
                return;

            requestedDict = GetFieldMapDictionary(entityType);


            //if (requestedDict.Count == 0)
            //    switch (entityType.Name)
            //    {
            //            #region MATERIAL

            //        case "MaterialEntity":
            //            requestedDict = FIELDMAPS.FieldMapDictionaries.MaterialEntityMap;
            //            break; 
            //            #endregion

            //            #region SLAB FACE
            //        case "SlabFaceEntity":
            //            requestedDict = FIELDMAPS.FieldMapDictionaries.SlabFaceEntityMap;

            //            break;
            //            #endregion

            //            #region ARTICLE
            //        case "ArticleEntity":
            //            requestedDict = FIELDMAPS.FieldMapDictionaries.ArticleEntityMap;

            //            break;
            //            #endregion

            //        case "AreaAttribute":
            //            requestedDict = FIELDMAPS.FieldMapDictionaries.AreaAttributeMap;

            //            break;

            //        case "QualityClass":
            //            requestedDict = FIELDMAPS.FieldMapDictionaries.QualityClassMap;

            //            break;

            //        case "QualityClassAreaAttribute":
            //            requestedDict = FIELDMAPS.FieldMapDictionaries.QualityClassAreaAttributeMap;

            //            break;

            //        default:
            //            break;
            //    }


            foreach (var kvp in requestedDict)
            {
                if (fieldsInfo.Exists(i => i.PropertyName == kvp.Key))
                {
                    entityFieldMap.Add(kvp.Key, kvp.Value);
                }
            }


            EntitiesFieldMaps.Add(entityType, entityFieldMap);

        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
