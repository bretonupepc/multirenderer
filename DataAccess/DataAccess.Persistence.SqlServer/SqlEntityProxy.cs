﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Threading;
using System.Windows.Media.Imaging;
using DbAccess;
using TraceLoggers;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer.FIELDMAPS;
using Wpf.Common;

namespace DataAccess.Persistence.SqlServer
{
    public class SqlEntityProxy:PersistableEntityProxy
    {

        #region >-------------- Constants and Enums

        protected const string BASE_SELECT = " SELECT ";
        protected const string BASE_UPDATE = " UPDATE ";
        protected const string BASE_WHERE = " WHERE ";
        protected const string BASE_INSERT = " INSERT INTO ";
        protected const string BASE_DELETE = " DELETE ";


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private DbInterface _dataAccessInterface;

        private SqlServerProvider _provider;

        protected string _mainTableName = string.Empty;


        protected Dictionary<string, DbField> _fieldsMap;

        private static object _lockObj = new object();

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SqlEntityProxy(BasePersistableEntity entity, SqlServerProvider provider, bool saveMode = false) : base(entity)
        {
            _provider = provider;
            if (provider.SaveMode)
            {
                _dataAccessInterface = provider.DataAccessInterface;
            }
            else
            {
                _dataAccessInterface = provider.DataAccessInterface.Clone();
            }


            _mainTableName =  entity.GetMainDbTableName(PersistenceProviderType.SqlServer);

        }

        public SqlEntityProxy(BasePersistableEntity entity, SqlServerProvider provider, string mainTableName, bool saveMode = false)
            : base(entity)
        {
            _provider = provider;
            if (provider.SaveMode)
            {
                _dataAccessInterface = provider.DataAccessInterface;
            }
            else
            {
                _dataAccessInterface = provider.DataAccessInterface.Clone();
            }

            _mainTableName = mainTableName;
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public DbInterface DataAccessInterface
        {
            get { return _dataAccessInterface; }
            set { _dataAccessInterface = value; }
        }

        public SqlServerProvider Provider
        {
            get { return _provider; }
        }

        public virtual string UniqueIdFieldName
        {
            get
            {
                return "Id";
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties

        protected internal Dictionary<string, DbField> FieldsMap
        {
            get
            {
                if (_fieldsMap == null || _fieldsMap.Count == 0)
                {
                    var map = SqlServerFieldsMaps.Instance.GetEntityFieldsMap(Entity.GetType());
                    if (map == null || map.Count == 0)
                    {
                        Type innerType = Entity.GetType().BaseType;
                        while (innerType != null && innerType != typeof(BasePersistableEntity))
                        {
                            map = SqlServerFieldsMaps.Instance.GetEntityFieldsMap(innerType);
                            if (map != null && map.Count > 0)
                            {
                                _fieldsMap = map;
                                break;
                            }
                            innerType = innerType.BaseType;
                        }
                    }
                    else
                    {
                        _fieldsMap = map;
                    }
                }

                return _fieldsMap;
            }
        }

        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database
        /// </summary>
        /// <param name="fieldName">Nome campo</param>
        public override void GetSingleField(string fieldName)
        {
            FieldRetrieveMode mode = FieldRetrieveMode.Deferred;

            FieldManagementType managementType = FieldManagementType.Direct;

            FieldItemInfo info = _entity.GetFieldItemInfo(fieldName);
            if (info != null)
            {
                mode = info.RetrieveMode;
                managementType = info.BaseInfo.DefaultManagementType;
            }

            bool isBlob = false;
            if (FieldsMap.ContainsKey(fieldName))
            {
                isBlob = FieldsMap[fieldName].IsBlobField;
            }

            if (!isBlob)
            {
                switch (mode)
                {
                    case FieldRetrieveMode.DeferredAsync:
                        AsyncGetSingleField(fieldName);
                        break;

                    default:

                        SyncGetSingleField(fieldName);
                        break;
                }
            }
            else
            {
                switch (mode)
                {
                    case FieldRetrieveMode.DeferredAsync:
                        AsyncGetSingleBlobField(fieldName, managementType);
                        break;

                    default:

                        SyncGetSingleBlobField(fieldName, managementType);
                        break;
                }
            }

            return;

        }

        /// <summary>
        /// Salva l'oggetto su database
        /// </summary>
        /// <returns></returns>
        public override bool Save()
        {
            bool retVal = false;

            string sqlCommand;

            if (_entity.IsNewItem)
            {
                _entity.BuildCompleteFieldsInfos();
                var code = _entity.FieldsInfos.FirstOrDefault(f => f.BaseInfo.PropertyName == "Code");

                if (code != null && FieldsMap.ContainsKey("Code") &&
                    string.IsNullOrEmpty(_entity.GetProperty<string>("Code"))
                    && !string.IsNullOrEmpty(_mainTableName))
                {
                    //sqlCommand = "sp_get_new_code_by_TableName";
                    //ArrayList parameters = new ArrayList();
                    //OleDbParameter param = new OleDbParameter("@TableName",OleDbType.VarChar,50);
                    //param.Value = _mainTableName;
                    //parameters.Add(param);

                    //param = new OleDbParameter("@Code", OleDbType.VarChar, 18);
                    
                    //param.Direction = ParameterDirection.Output;
                    //parameters.Add(param);

                    //if (DataAccessInterface.Execute(sqlCommand, ref parameters))
                    //{
                    //    _entity.SetProperty("Code", ((OleDbParameter)parameters[1]).Value.ToString());
                    //}
                }

            }



            retVal = BuildSaveCommand(out sqlCommand);

            bool areBlobFieldsToUpdate = _entity.FieldsInfos.Any(f => f.Status == FieldStatus.Set
                                                                      && FieldsMap.ContainsKey(f.BaseInfo.PropertyName)
                                                                      && FieldsMap[f.BaseInfo.PropertyName].IsBlobField);

            if (retVal || areBlobFieldsToUpdate)
            {
                bool transaction = !DataAccessInterface.TransactionActive();

                try
                {
                    if (transaction) DataAccessInterface.BeginTransaction();

                    int id;

                    if (retVal)
                    {
                        if (_entity.IsNewItem)
                        {
                            retVal = DataAccessInterface.Execute(sqlCommand, out id);
                            if (retVal)
                            {
                                _entity.SetProperty(UniqueIdFieldName, id, true, FieldStatus.Read);
                            }

                        }
                        else
                        {
                            retVal = DataAccessInterface.Execute(sqlCommand);
                        }
                    }


                    if (areBlobFieldsToUpdate)
                    {
                        //TODO:Update Blob fields
                        retVal = true;

                        foreach (FieldItemInfo field in _entity.FieldsInfos)
                        {
                            if (field.Status == FieldStatus.Set &&
                                FieldsMap.ContainsKey(field.BaseInfo.PropertyName)
                                && FieldsMap[field.BaseInfo.PropertyName].IsBlobField)
                            {
                                if (!SaveBlobField(field))
                                {
                                    retVal = false;
                                    break;
                                }
                            }
                        }


                        if (retVal)
                            retVal = Refresh();
                    }

                    if (!retVal)
                    {
                        TraceLog.WriteLine("EntityProxy Save failed.");
                    }
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("EntityProxy Save error ", ex);
                    retVal = false;


                }

                finally
                {
                    if (transaction) DataAccessInterface.EndTransaction(retVal);
                }

            }



            return retVal || (string.IsNullOrEmpty(sqlCommand));

        }


        /// <summary>
        /// Salva l'oggetto su database
        /// </summary>
        /// <returns></returns>
        public override bool Save(out string errors)
        {
            bool retVal = false;

            errors = string.Empty;

            string sqlCommand;

            if (_entity.IsNewItem)
            {
                _entity.BuildCompleteFieldsInfos();
                var code = _entity.FieldsInfos.FirstOrDefault(f => f.BaseInfo.PropertyName == "Code");

                if (code != null && FieldsMap.ContainsKey("Code") &&
                    string.IsNullOrEmpty(_entity.GetProperty<string>("Code"))
                    && !string.IsNullOrEmpty(_mainTableName))
                {
                    //sqlCommand = "sp_get_new_code_by_TableName";
                    //ArrayList parameters = new ArrayList();
                    //OleDbParameter param = new OleDbParameter("@TableName",OleDbType.VarChar,50);
                    //param.Value = _mainTableName;
                    //parameters.Add(param);

                    //param = new OleDbParameter("@Code", OleDbType.VarChar, 18);
                    
                    //param.Direction = ParameterDirection.Output;
                    //parameters.Add(param);

                    //if (DataAccessInterface.Execute(sqlCommand, ref parameters))
                    //{
                    //    _entity.SetProperty("Code", ((OleDbParameter)parameters[1]).Value.ToString());
                    //}
                }

            }



            retVal = BuildSaveCommand(out sqlCommand);
            if (!retVal)
            {
                errors += string.Format("Failed to build SQL Save command\n{0}", sqlCommand);
            }

            bool areBlobFieldsToUpdate = _entity.FieldsInfos.Any(f => f.Status == FieldStatus.Set
                                                                      && FieldsMap.ContainsKey(f.BaseInfo.PropertyName)
                                                                      && FieldsMap[f.BaseInfo.PropertyName].IsBlobField);

            if (retVal || areBlobFieldsToUpdate)
            {
                bool transaction = !DataAccessInterface.TransactionActive();

                try
                {
                    if (transaction) DataAccessInterface.BeginTransaction();

                    int id;

                    if (retVal)
                    {
                        if (_entity.IsNewItem)
                        {
                            retVal = DataAccessInterface.Execute(sqlCommand, out id);
                            if (retVal)
                            {
                                _entity.SetProperty(UniqueIdFieldName, id, true, FieldStatus.Read);
                            }
                            else
                            {
                                errors += string.Format("Failed to execute SQL:\n{0}\n", sqlCommand);
                            }

                        }
                        else
                        {
                            retVal = DataAccessInterface.Execute(sqlCommand);
                            if (!retVal)
                            {
                                errors += string.Format("Failed to execute SQL:\n{0}\n", sqlCommand);
                            }
                        }
                    }


                    if (areBlobFieldsToUpdate)
                    {
                        //TODO:Update Blob fields
                        retVal = true;

                        foreach (FieldItemInfo field in _entity.FieldsInfos)
                        {
                            if (field.Status == FieldStatus.Set &&
                                FieldsMap.ContainsKey(field.BaseInfo.PropertyName)
                                && FieldsMap[field.BaseInfo.PropertyName].IsBlobField)
                            {
                                if (!SaveBlobField(field))
                                {
                                    errors += string.Format("Failed to save BLOB field:\n{0}\n", field.BaseInfo.PropertyName);

                                    retVal = false;
                                    break;
                                }
                            }
                        }


                        if (retVal)
                            retVal = Refresh();
                    }

                    if (!retVal)
                    {
                        TraceLog.WriteLine("EntityProxy Save failed.");
                        errors += "EntityProxy Save failed.";
                    }
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("EntityProxy Save error ", ex);
                    errors += string.Format("EntityProxy Save exception:\n{0}", ex.Message);
                    retVal = false;


                }

                finally
                {
                    if (transaction) DataAccessInterface.EndTransaction(retVal);
                }

            }



            return retVal || (string.IsNullOrEmpty(sqlCommand));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Delete()
        {
            bool retVal = false;

            if (_entity.IsNewItem)
            {
                retVal = true;
            }
            else
            {
                try
                {
                    string sqlCommand;

                    retVal = BuildDeleteCommand(out sqlCommand);

                    int affectedRows;

                    retVal = DataAccessInterface.ExecuteNonQuery(sqlCommand, out affectedRows);

                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("SqlEntityProxy Delete error", ex);
                }

            }

            return retVal;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Delete(out string errors)
        {
            errors = string.Empty;

            bool retVal = false;

            if (_entity.IsNewItem)
            {
                retVal = true;
            }
            else
            {
                try
                {
                    string sqlCommand;

                    retVal = BuildDeleteCommand(out sqlCommand);
                    if (!retVal)
                    {
                        errors += string.Format("Failed to build SQL delete command\n{0}", sqlCommand);
                    }

                    int affectedRows;

                    retVal = DataAccessInterface.ExecuteNonQuery(sqlCommand, out affectedRows);
                    if (!retVal)
                    {
                        errors += string.Format("Failed to execute SQL delete command\n{0}", sqlCommand);
                    }

                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("SqlEntityProxy Delete error", ex);
                    errors += string.Format("EntityProxy Delete exception:\n{0}", ex.Message);
                }

            }

            return retVal;
        }


        /// <summary>
        /// Rilegge l'oggetto da database
        /// </summary>
        /// <returns></returns>
        public override bool Refresh()
        {
            bool retVal = false;

            string sqlCommand;

            retVal = BuildRefreshingSelectCommand(out sqlCommand);

            if (retVal)
            {
                bool transaction = !DataAccessInterface.TransactionActive();


                int exception_step = 0;

                OleDbDataReader ord = null;


                try
                {
                    if (transaction) DataAccessInterface.BeginTransaction();

                    retVal = (DataAccessInterface.Requery(sqlCommand, out ord)) && ord.HasRows;



                    if (retVal)
                    {
                        ord.Read();

                        foreach (FieldItemInfo field in _entity.FieldsInfos)
                        {
                            if ((field.Status == FieldStatus.Set || field.Status == FieldStatus.Read)
                                && (FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                                && !FieldsMap[field.BaseInfo.PropertyName].IsBlobField)
                            {
                                object fieldValue = ord[FieldsMap[field.BaseInfo.PropertyName].Name];

                                if (fieldValue != DBNull.Value)
                                {
                                    _entity.SetProperty(field.BaseInfo.PropertyName, fieldValue, true, FieldStatus.Read);

                                }

                            }
                        }


                    }

                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("EntityProxy Refresh error ", ex);
                    retVal = false;
                    ord = null;

                }

                finally
                {
                    if (transaction) DataAccessInterface.EndTransaction(retVal);
                }

            }



            return retVal;

        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="managementType"></param>
        protected virtual void AsyncGetSingleBlobField(string fieldName, FieldManagementType managementType)
        {
            if (_provider != null && _provider.CanStartWorkers)
            {
                ThreadPool.QueueUserWorkItem(AsyncGetSingleBlobFieldCore, new object[] { fieldName, managementType });
            }

        }

        /// <summary>
        /// Costruisce la stringa SQL per il refresh dell'oggetto
        /// </summary>
        /// <param name="sqlCommand">Stringa SQL prodotta</param>
        /// <returns></returns>
        protected virtual bool BuildRefreshingSelectCommand(out string sqlCommand)
        {
            string selectStr = BASE_SELECT;

            bool isValid = false;

            string fieldsPart = string.Empty;

            if (_entity.FieldsInfos != null && _entity.FieldsInfos.Count > 0)
            {

                foreach (FieldItemInfo field in _entity.FieldsInfos)
                {
                    if ((field.Status == FieldStatus.Set || field.Status == FieldStatus.Read)
                        && (FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                        && !FieldsMap[field.BaseInfo.PropertyName].IsBlobField)
                    {
                        fieldsPart += FieldsMap[field.BaseInfo.PropertyName].Name + ", ";

                    }
                }

                isValid = !string.IsNullOrEmpty(fieldsPart);
                fieldsPart = fieldsPart.Substring(0, fieldsPart.Length - 2) + " ";
            }

            if (isValid)
            {
                selectStr += fieldsPart +
                             " FROM [" + _mainTableName + "] " +
                             " WHERE " +
                             FieldsMap[UniqueIdFieldName].Name + " = " + _entity.UniqueId;

            }

            sqlCommand = selectStr;
            return isValid ;
        }

        /// <summary>
        /// Costruisce la stringa SQL per il Save dell'oggetto
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected virtual bool BuildSaveCommand(out string sqlCommand)
        {
            sqlCommand = string.Empty;

            bool retVal = false;

            if (_entity.FieldsInfos != null && _entity.FieldsInfos.Count > 0)
            {
                try
                {
                    string actionPart = (_entity.IsNewItem)
                        ? BuildSaveInsertPart()
                        : BuildSaveUpdatePart();


                    sqlCommand = actionPart;
                    retVal = !string.IsNullOrEmpty(sqlCommand);
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("SqlEntityProxy Save error ", ex);
                    sqlCommand = string.Empty;
                    retVal = false;
                }
            }
            return retVal;
        }

        /// <summary>
        /// Costruisce la stringa SQL per il Delete dell'oggetto
        /// </summary>
        /// <param name="sqlCommand"></param>
        /// <returns></returns>
        protected virtual bool BuildDeleteCommand(out string sqlCommand)
        {
            sqlCommand = string.Empty;

            bool retVal = false;

            if (_entity.FieldsInfos != null && _entity.FieldsInfos.Count > 0)
            {
                try
                {
                    sqlCommand += BASE_DELETE +
                         " FROM [" + _mainTableName + "] " +
                         " WHERE " +
                         FieldsMap[UniqueIdFieldName].Name + " = " + _entity.UniqueId;


                    retVal = !string.IsNullOrEmpty(sqlCommand);
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("SqlEntityProxy BuildDeleteCommand error ", ex);
                    sqlCommand = string.Empty;
                    retVal = false;
                }
            }
            return retVal;
        }


        /// <summary>
        /// Costruisce la stringa SQL per l'insert dell'oggetto nel database
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildSaveInsertPart()
        {
            string sqlInsert = BASE_INSERT;

            bool isValid = false;

            string fieldsPart = string.Empty;
            string valuesPart = string.Empty;

            foreach (FieldItemInfo field in _entity.FieldsInfos)
            {
                //if (field.Status == FieldStatus.Set &&
                //    FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
                if (FieldsMap.ContainsKey(field.BaseInfo.PropertyName)
                    && !FieldsMap[field.BaseInfo.PropertyName].IsBlobField
                    && field.BaseInfo.Relation == null
                    && ((field.Status == FieldStatus.Set) || FieldsMap[field.BaseInfo.PropertyName].IsRequired))
                {
                    if (!(_entity.HasIdentityId && field.BaseInfo.PropertyName == UniqueIdFieldName))
                    {
                        fieldsPart += FieldsMap[field.BaseInfo.PropertyName].Name + ", ";
                        valuesPart += BuildSqlValue(field) + ", ";
                    }
                }
            }

            isValid = !string.IsNullOrEmpty(fieldsPart);

            if (isValid)
            {
                sqlInsert += " [" + _mainTableName + "] " +
                             " (" + fieldsPart.Substring(0, fieldsPart.Length - 2) + ") " + 
                             " VALUES " +
                             " (" + valuesPart.Substring(0, valuesPart.Length - 2) + ") ";

                if (_entity.HasIdentityId)
                    sqlInsert += "; SELECT SCOPE_IDENTITY()";
            }

            return isValid ? sqlInsert : string.Empty;
        }

        /// <summary>
        /// Costruisce la stringa SQL per l'update dell'oggetto nel database
        /// </summary>
        /// <returns></returns>
        protected virtual string BuildSaveUpdatePart()
        {
            string sqlUpdate = BASE_UPDATE;

            bool isValid = false;

            string fieldsPart = string.Empty;

            foreach (FieldItemInfo field in _entity.FieldsInfos)
            {
                if (field.Status == FieldStatus.Set 
                    && field.BaseInfo.Relation == null
                    && FieldsMap.ContainsKey(field.BaseInfo.PropertyName)
                    && !FieldsMap[field.BaseInfo.PropertyName].IsBlobField)
                {
                    fieldsPart += FieldsMap[field.BaseInfo.PropertyName].Name + " = ";
                    fieldsPart += BuildSqlValue(field) + ", ";
                }
            }

            isValid = !string.IsNullOrEmpty(fieldsPart);

            if (isValid)
            {
                sqlUpdate += " [" + _mainTableName + "] " +
                             " SET " + fieldsPart.Substring(0, fieldsPart.Length - 2) +
                             " WHERE " +
                             FieldsMap[UniqueIdFieldName].Name + " = " + _entity.UniqueId;
            }

            return isValid ? sqlUpdate : string.Empty;
        }

        /// <summary>
        /// Traduce il valore di un campo in formato SQL
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected virtual string BuildSqlValue(FieldItemInfo info)
        {
            string clauseValue = string.Empty;

            var propertyType = info.BaseInfo.PropertyType;

            if (propertyType == typeof(string) || propertyType.UnderlyingSystemType == typeof(System.String) || propertyType.Name == "String")
            {
                return string.Format(" '{0}' ", SqlServerProvider.FixSqlString(_entity.GetProperty <string>(info.BaseInfo.PropertyName)));
            }
            else if (propertyType == typeof(DateTime) || propertyType.Name == "DateTime")
            {
                return string.Format(" '{0}' ", SqlServerProvider.DateSqlFormat(_entity.GetProperty<DateTime>(info.BaseInfo.PropertyName)));
            }

            else if (propertyType == typeof(bool) || propertyType.Name == "Boolean")
            {
                return string.Format(" {0} ", _entity.GetProperty<bool>(info.BaseInfo.PropertyName) ? 1 : 0);
            }

            else if (propertyType == typeof(int) || propertyType.Name == "Int32")
            {
                return string.Format(" {0} ", _entity.GetProperty<int>(info.BaseInfo.PropertyName));
            }

            else if (propertyType == typeof(double) || propertyType.Name == "Double")
            {
                return string.Format(" {0} ", _entity.GetProperty<double>(info.BaseInfo.PropertyName).ToString(CultureInfo.InvariantCulture));
            }


            else if (propertyType.BaseType == typeof(Enum))
            {
                return string.Format(" {0} ", (int)_entity.GetProperty<object>(info.BaseInfo.PropertyName));
            }

            else
            {
                return string.Format(" {0} ", _entity.GetProperty<object>(info.BaseInfo.PropertyName));
            }

        }

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database in modalità sincrona
        /// </summary>
        /// <param name="fieldName"></param>
        protected virtual void SyncGetSingleField(string fieldName)
        {
            if (FieldsMap == null || !FieldsMap.ContainsKey(fieldName))
            {
                return;
            }

            string sqlCommand;

            sqlCommand = BuildSingleFieldSelect(fieldName);

            bool rc = true;

            int exception_step = 0;
            OleDbDataReader ord = null;


            try
            {
                

                //if (!IsConnected())
                //    if (!Connect(sConnectionString))
                //        return false;

                exception_step = 1;
                if (!_dataAccessInterface.Requery(sqlCommand, out ord))
                    return;

                exception_step = 2;
                if (ord.HasRows)
                {
                    ord.Read();

                    object fieldValue = ord[0];

                    if (fieldValue != DBNull.Value)
                    {
                        Entity.SetProperty(fieldName, fieldValue, true, FieldStatus.Read);

                    }
                    else
                    {
                        Entity.SetPropertyStatus(fieldName,  FieldStatus.Read);
                    }

                }
                else
                    rc = false;
            }
            catch (Exception ex)
            {
                //TraceLog.WriteLine(this + "ExtractInfo (" + slabcode + "): Exception: " + ex.Message + "; step = " + exception_step.ToString());
                //slabinfo = null;
                rc = false;
            }

            finally
            {
                _dataAccessInterface.EndRequery(ord);
                ord = null;
            }

            return;

        }

        /// <summary>
        /// Estrae un singolo campo dell'oggetto dal database in modalità asincrona
        /// </summary>
        /// <param name="fieldName"></param>
        protected virtual void AsyncGetSingleField(string fieldName)
        {
            if (_provider != null && _provider.CanStartWorkers)
            {
                ThreadPool.QueueUserWorkItem(AsyncGetSingleFieldCore, new object[] {fieldName });
            }

        }

        /// <summary>
        /// Core dell'estrazione di un singolo campo dell'oggetto dal database in modalità asincrona
        /// </summary>
        /// <param name="args"></param>
        protected virtual void AsyncGetSingleFieldCore(object args)
        {
            if (_provider != null)
            {
                if (!_provider.CanStartWorkers)
                    return;

                _provider.Workers.Add(Thread.CurrentThread);
            }

            try
            {
                object[] array = args as object[];

                string fieldName = array[0].ToString();
                SyncGetSingleField(fieldName);
            }
            catch (Exception ex)
            {
                TraceLog.WriteException(ex);
            }
            finally
            {
                if (_provider != null)
                    _provider.Workers.Remove(Thread.CurrentThread);
            }

        }

        protected string BuildSingleFieldSelect(string fieldName)
        {
            string sqlCommand;
            sqlCommand = "SELECT ";
            sqlCommand += FieldsMap[fieldName].Name + " ";
            sqlCommand += " FROM " + _mainTableName + " ";
            sqlCommand += " WHERE F_ID = " + Entity.UniqueId;
            return sqlCommand;
        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private bool SaveBlobField(FieldItemInfo field)
        {
            bool retVal = false;

            if (!FieldsMap.ContainsKey(field.BaseInfo.PropertyName))
            {
                TraceLog.WriteLine(string.Format("SqlEntityProxy SaveBlobField: FieldsMap not found for field <{0}>", field.BaseInfo.PropertyName));
                return false;
            }

            string idFieldName = "F_ID";
            if (FieldsMap.ContainsKey("Id"))
            {
                idFieldName = FieldsMap["Id"].Name;
            }

            string whereCondition = string.Format("{0} = {1}", idFieldName, _entity.UniqueId);

            if (field.BaseInfo.DefaultManagementType == FieldManagementType.ToFilepath)
            {
                string filepath = _entity.GetProperty<string>(field.BaseInfo.PropertyName);
                if (File.Exists(filepath))
                {
                    //retVal = DataAccessInterface.WriteBlobField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name, "",
                    //    whereCondition, filepath, "");
                    retVal = DataAccessInterface.WriteSingleBlobField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name,
                        whereCondition, filepath);
                    if (retVal) //Copio il file nel folder temporaneo, con il nuovo nome, e aggiorno la property
                    {
                        string tempFilepath = _entity.GetTempFilepath(field.BaseInfo.PropertyName, _entity.UniqueId.ToString("00000000"));
                        if (filepath != tempFilepath)
                        {
                            File.Copy(filepath, tempFilepath, true);
                            _entity.SetProperty(field.BaseInfo.PropertyName,tempFilepath,true,FieldStatus.Read,false,true,true);
                        }
                    }

                }
            }
            else
            {
                
                Type propertyType = field.BaseInfo.PropertyType;
                if (propertyType == typeof (string))
                {
                    string value = _entity.GetProperty<string>(field.BaseInfo.PropertyName);
                    if (string.IsNullOrEmpty(value))
                    {
                        retVal = SetNullField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name,
                            whereCondition);
                    }
                    else
                    {
                        string filepath = _entity.GetTempFilepath(field.BaseInfo.PropertyName, _entity.UniqueId.ToString("00000000"));
                        File.WriteAllText(filepath, value);
                        retVal = DataAccessInterface.WriteBlobField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name,
                            whereCondition, filepath);
                    }
                    
                }
                else if (propertyType == typeof(Bitmap))
                {
                    Bitmap value = _entity.GetProperty<Bitmap>(field.BaseInfo.PropertyName);
                    if (value == null)
                    {
                        retVal = SetNullField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name,
                            whereCondition);
                    }
                    else
                    {
                        string filepath = _entity.GetTempFilepath(field.BaseInfo.PropertyName, _entity.UniqueId.ToString("00000000"));
                        value.Save(filepath);
                        retVal = DataAccessInterface.WriteBlobField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name,
                            whereCondition, filepath);

                    }

                }
                if (propertyType == typeof(BitmapSource))
                {
                    BitmapSource value = _entity.GetProperty<BitmapSource>(field.BaseInfo.PropertyName);
                    if (value == null)
                    {
                        retVal = SetNullField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name,
                            whereCondition);
                    }
                    else
                    {
                        string filepath = _entity.GetTempFilepath(field.BaseInfo.PropertyName, _entity.UniqueId.ToString("00000000"));
                        value.ToWinFormsBitmap().Save(filepath);
                        retVal = DataAccessInterface.WriteBlobField(_mainTableName, FieldsMap[field.BaseInfo.PropertyName].Name,
                            whereCondition, filepath);

                    }
                }
                
            }
            if (!retVal)
            {
                TraceLog.WriteLine(string.Format("SqlEntityProxy SaveBlobField error: field <{0}>", field.BaseInfo.PropertyName));
            }

            return retVal;
        }

        private bool SetNullField(string mainTableName, string columnName, string whereCondition)
        {
            bool retVal = false;

            try
            {
                string sqlCommand = "UPDATE " + "[" + mainTableName + "]" +
                                    " SET " + "[" + columnName + "] = NULL " +
                                    " WHERE " + whereCondition;
                int affectedRows;
                retVal = DataAccessInterface.ExecuteNonQuery(sqlCommand, out affectedRows);
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SqlEntityProxy SetNullField error ", ex);
                retVal = false;
            }
            
            
            return retVal;

        }

        private void SyncGetSingleBlobField(string fieldName, FieldManagementType managementType)
        {
            string idFieldName = "F_ID";
            if (FieldsMap.ContainsKey("Id"))
            {
                idFieldName = FieldsMap["Id"].Name;
            }
            string filepath = _entity.GetTempFilepath(fieldName, _entity.UniqueId.ToString("00000000"));

            bool hasBlobWritten = false;
            lock (_lockObj)
            {
                if (File.Exists(filepath))
                {
                    //File.Delete(filepath);
                    hasBlobWritten = true;
                }
                else
                {
                    hasBlobWritten = DataAccessInterface.GetBlobField(_mainTableName, FieldsMap[fieldName].Name,
                        string.Format("{0} = {1}", idFieldName, _entity.UniqueId), filepath);
                }
            }

            if (hasBlobWritten)
            {
                if (managementType == FieldManagementType.ToFilepath)
                {
                    FileInfo info = new FileInfo(filepath);
                    Entity.SetProperty(fieldName, info.FullName, true, FieldStatus.Read);
                }
                else
                {
                    if (Entity.GetFieldItemInfo(fieldName).BaseInfo.PropertyType == typeof (string))
                    {
                        string content = File.ReadAllText(filepath);
                        Entity.SetProperty(fieldName, content, true, FieldStatus.Read);
                    }
                    else if (Entity.GetFieldItemInfo(fieldName).BaseInfo.PropertyType == typeof(Bitmap))
                    {
                        using (Bitmap readBitmap = (Bitmap)Image.FromFile(filepath))
                        {
                            Entity.SetProperty(fieldName, (Bitmap)readBitmap.Clone(), true, FieldStatus.Read);
                        }
                    }
                    else if (Entity.GetFieldItemInfo(fieldName).BaseInfo.PropertyType == typeof(BitmapSource))
                    {
                        using (Bitmap readBitmap = (Bitmap)Image.FromFile(filepath))
                        {
                            Entity.SetProperty(fieldName, ((Bitmap)readBitmap.Clone()).ToWpfBitmap(), true, FieldStatus.Read);
                        }
                    }

                }
            }
            else
            {
                TraceLog.WriteLine("SyncGetSingleBlobField error ");
            }
        }

        private void AsyncGetSingleBlobFieldCore(object args)
        {
            if (_provider != null)
            {
                if (!_provider.CanStartWorkers)
                    return;

                _provider.Workers.Add(Thread.CurrentThread);
            }

            try
            {
                object[] array = args as object[];

                string fieldName = array[0].ToString();
                FieldManagementType managementType = (FieldManagementType) array[1];
                SyncGetSingleBlobField(fieldName, managementType);
            }
            catch (Exception ex)
            {
                TraceLog.WriteException(ex);
            }
            finally
            {
                if (_provider != null)
                    _provider.Workers.Remove(Thread.CurrentThread);
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
