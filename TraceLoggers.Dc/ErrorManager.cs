﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using Wpf.Common;
using Wpf.Common.Commands;

namespace TraceLoggers
{
    public class ErrorManager : ObservableObject
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration

        public event EventHandler<EventArgs<ErrorItem>> NewError;

        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private ObservableCollection<ErrorItem> _errorList = new ObservableCollection<ErrorItem>();

        private bool _showOnNewError = true;

        private bool _showErrors = false;

        private object _objLocker = new object();

        private ErrorItem _selectedItem = null;

        private DelegateCommand _closePopupCommand = null;

        private DelegateCommand _removeErrorCommand = null;

        private DelegateCommand _removeAllErrorsCommand = null;

        private bool _manageOwnList = true;

        private bool _raiseEvents = true;

        private bool _viewErrors = true;

        private bool _viewWarnings = true;

        private bool _viewInfo = true;

        private int _filterMask = 7;

        private ICollectionView _traceViewSource;

        private static ErrorManager _instance = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        private ErrorManager()
        {
            TraceViewSource = CollectionViewSource.GetDefaultView(ErrorList);
            TraceViewSource.Filter = TraceViewFilter;

        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties


        private bool TraceViewFilter(object errorItem)
        {
            var item = errorItem as ErrorItem;
            var gravityValue = (int)item.Gravity;
            return (gravityValue & _filterMask) == gravityValue;
        }

        public ICollectionView TraceViewSource
        {
            get { return _traceViewSource; }
            set
            {
                _traceViewSource = value;
                OnPropertyChanged("TraceViewSource");
            }
        }

        public bool ViewErrors
        {
            get { return _viewErrors; }
            set
            {
                _viewErrors = value;
                UpdateFilter();
                OnPropertyChanged("ViewErrors");
            }
        }

        public bool ViewWarnings
        {
            get { return _viewWarnings; }
            set
            {
                _viewWarnings = value;
                UpdateFilter();
                OnPropertyChanged("ViewWarnings");

            }
        }

        public bool ViewInfo
        {
            get { return _viewInfo; }
            set
            {
                _viewInfo = value;
                UpdateFilter();
                OnPropertyChanged("ViewInfo");

            }
        }

        private void UpdateFilter()
        {
            _filterMask = (ViewInfo ? (int)ErrorGravity.Info : 0)
                          | (ViewErrors ? (int)ErrorGravity.Error : 0)
                          | (ViewWarnings ? (int)ErrorGravity.Warning : 0);
            TraceViewSource.Refresh();
        }


        public ObservableCollection<ErrorItem> ErrorList
        {
            get { return _errorList; }
        }

        public bool ShowOnNewError
        {
            get { return _showOnNewError; }
            set
            {
                _showOnNewError = value;
                OnPropertyChanged("ShowOnNewError");
            }
        }

        public bool ShowErrors
        {
            get { return _showErrors; }
            set
            {
                _showErrors = value;
                OnPropertyChanged("ShowErrors");

            }
        }

        public ErrorItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public DelegateCommand ClosePopupCommand
        {
            get
            {
                if (_closePopupCommand == null)
                {
                    _closePopupCommand = new DelegateCommand(ClosePopup, CanClosePopup);
                }
                return _closePopupCommand;
            }
        }

        public DelegateCommand RemoveErrorCommand
        {
            get
            {
                if (_removeErrorCommand == null)
                {
                    _removeErrorCommand = new DelegateCommand(RemoveError, CanRemoveError);
                }
                return _removeErrorCommand;
            }
        }

        private void RemoveError(object obj)
        {
            ErrorItem item = obj as ErrorItem;
            if (item != null)
            {
                RemoveError(item);
            }
        }

        private bool CanRemoveError(object obj)
        {
            return true;
        }


        public DelegateCommand RemoveAllErrorsCommand
        {
            get
            {
                if (_removeAllErrorsCommand == null)
                {
                    _removeAllErrorsCommand = new DelegateCommand(RemoveAllErrors, CanRemoveAllErrors);
                }
                return _removeAllErrorsCommand;
            }
        }

        public bool ManageOwnList
        {
            get { return _manageOwnList; }
            set { _manageOwnList = value; }
        }

        public bool RaiseEvents
        {
            get { return _raiseEvents; }
            set { _raiseEvents = value; }
        }

        public static ErrorManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ErrorManager();
                }
                return _instance;
            }
        }

        private void RemoveAllErrors(object obj)
        {
            TryCleanErrors();
        }

        private void TryCleanErrors()
        {
            lock (_objLocker)
            {
                ErrorList.Clear();
                SelectedItem = null;
                ShowErrors = false;
            }
        }

        private bool CanRemoveAllErrors(object obj)
        {
            return true;
        }




        private void ClosePopup(object obj)
        {
            ShowErrors = false;
        }

        private bool CanClosePopup(object obj)
        {
            return true;
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public void AddError(string errorContent)
        {
            AddItem(errorContent, ErrorGravity.Error);
        }

        public void AddWarning(string errorContent)
        {
            AddItem(errorContent, ErrorGravity.Warning);
        }

        public void AddInfo(string errorContent)
        {
            AddItem(errorContent, ErrorGravity.Info);
        }

        public void AddItem(ErrorItem errorItem)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(new Action(() => AddItem(errorItem)));
            }
            else
            {
                if (_manageOwnList)
                {
                    ErrorList.Insert(0, errorItem);
                    if (_showOnNewError)
                    {

                        ShowErrors = true;
                    }
                    SelectedItem = errorItem;
                }

                if (_raiseEvents)
                    OnNewError(errorItem);
            }
        }

        public void AddItem(string errorContent, ErrorGravity errorGravity)
        {
            var errorItem = new ErrorItem(errorContent, errorGravity);

            AddItem(errorItem);
        }


        public void RemoveError(ErrorItem errorItem)
        {
            lock (_objLocker)
            {
                if (ErrorList.Contains(errorItem))
                {
                    int position = ErrorList.IndexOf(errorItem);

                    ErrorList.Remove(errorItem);
                    if (ErrorList.Count > position)
                    {
                        SelectedItem = ErrorList[position];
                    }
                    else if (position > 0)
                    {
                        SelectedItem = ErrorList[position - 1];
                    }
                    else
                    {
                        SelectedItem = null;
                        ShowErrors = false;
                    }
                }
            }
        }

        public void TryRemoveErrorByContent(string errorItemContent)
        {
            lock (_objLocker)
            {
                var errorItem = ErrorList.FirstOrDefault(i => i.Content == errorItemContent);
                if (errorItem != null)
                {
                    int position = ErrorList.IndexOf(errorItem);

                    ErrorList.Remove(errorItem);
                    if (ErrorList.Count > position)
                    {
                        SelectedItem = ErrorList[position];
                    }
                    else if (position > 0)
                    {
                        SelectedItem = ErrorList[position - 1];
                    }
                    else
                    {
                        SelectedItem = null;
                        ShowErrors = false;
                    }
                }
            }
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void OnNewError(ErrorItem errorItem)
        {
            var handler = NewError;
            if (handler != null)
            {
                NewError(this, new EventArgs<ErrorItem>(errorItem));
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
