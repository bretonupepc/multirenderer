﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace TraceLoggers
{
    /// <summary>
    /// Classe di base che implementa l'interfaccia INotifyPropertyChanged
    /// per utilizzo in binding WPF
    /// </summary>
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        /// <summary>
        /// Solleva l'evento PropertyChanged per una specifica Property
        /// </summary>
        /// <param name="propertyName">Nome Property</param>
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Evento di notifica Property modificata
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
