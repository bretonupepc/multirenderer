﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TraceLoggers
{
    public enum ErrorGravity
    {
        Info = 1,
        Warning = 2,
        Error = 4
    }

    public class ErrorItem : ObservableObject
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private DateTime _errorDateTime;

        private string _content = string.Empty;

        private ErrorGravity _gravity = ErrorGravity.Error;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ErrorItem(string content, ErrorGravity gravity = ErrorGravity.Error)
        {
            _errorDateTime = DateTime.Now;
            _content = content;
            _gravity = gravity;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public DateTime ErrorDateTime
        {
            get { return _errorDateTime; }
        }

        public string ErrorTimeValue
        {
            get { return ErrorDateTime.ToString("T"); }
        }


        public string Content
        {
            get { return _content; }
        }

        public ErrorGravity Gravity
        {
            get { return _gravity; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
