using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

namespace TraceLoggers
{
    /// <summary>
    /// Defines different log levels for messages to be logged.
    /// </summary>
    public enum LogLevel
    {
        //order DOES matter
        Debug,
        Info,
        Warning,
        Error,
        None,
    }

    /// <summary>
    /// Summary description for TraceLog.
    /// </summary>
    public class TraceLog
    {
        public static event EventHandler<WriteLineEventArgs> NewContent; 


        public const string DEFAULT_LOGS_DIR = @"C:\Logs";

        static int MAX_CONCURRENT_FILES = 5;
        static string LOG_FILE_EXTENSION = ".TXT";
        static LogLevel mLogLevel = LogLevel.Debug;
        static DateTime mDate;
        static DateTime mLogDate = DateTime.Now;
        static int mNameMode;
        static string mPath;
        static string mProcessName;
        static int mProcessId;
        static bool mInitExecuted = false;
        static object lockObject = new object();
        static Dictionary<int, TextWriterTraceListener> traceListeners;
        static int listenersProg = 0;

        private static bool _raiseEventOnWriteline = false;

        public static bool InitExecuted
        {
            get
            {
                return mInitExecuted;
            }
        }

        public static string ProcessName
        {
            get
            {
                return mProcessName;
            }

            set
            {
                mProcessName = value;
            }
        }

        public static int ProcessId
        {
            get
            {
                return mProcessId;
            }

            set
            {
                mProcessId = value;
            }
        }

        /// <summary>
        /// Livello di log
        /// </summary>
        public static LogLevel Level
        {
            get
            {
                return mLogLevel;
            }

            set
            {
                mLogLevel = value;
            }
        }

        public static bool RaiseEventOnWriteline
        {
            get { return _raiseEventOnWriteline; }
            set { _raiseEventOnWriteline = value; }
        }

        //**************************************************************************
        // TraceLog
        // Apre il log
        //**************************************************************************
        static TraceLog()
        {
            traceListeners = new Dictionary<int, TextWriterTraceListener>();
            //Init();
        }

        //**************************************************************************
        // Function msFileName
        // Restituisce il nome del file di log
        // Parametri:
        //           vdtDate     : data del file
        //           vnNameMode  : modalit� definizione del nome file
        //                              -1 default
        //                               0 giorno della settimana
        //                               1 giorno del mese
        //                               2 giorno dell'anno (mese + giorno)
        //                               3 data completa
        // Ritorna:
        //           nome del file
        //**************************************************************************
        private static string FileName(string path, DateTime date, int nameMode)
        {
            string fname = "";

            if (ProcessName != null)
            {
                fname = fname + ProcessName + "_";
            }

            switch (nameMode)
            {
                case 0:
                    fname += date.ToString("dddd");
                    break;
                case 1:
                    fname += date.ToString("dd");
                    break;
                case 2:
                    fname += date.ToString("MMdd");
                    break;
                case 3:
                    fname += date.ToString("yyyyMMdd");
                    break;
                default:
                    fname += date.ToString("dddd");
                    break;
            }

            if (ProcessId > 0)
            {
                fname = fname + "_" + ProcessId;
            }

            if (path == null)
            {
                path = "";
            }

            if (!string.IsNullOrEmpty(path) &&
                !Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string returnFilename = Path.Combine(path, fname);

            if (ProcessId <= 0)
            {
                // try to guess a unique "process id"
                for (int i = 1; i <= MAX_CONCURRENT_FILES; i++)
                {
                    string actualFilename = returnFilename + "_" + i + LOG_FILE_EXTENSION;

                    if (!IsFileLocked(new FileInfo(actualFilename)))
                    {
                        returnFilename = actualFilename;
                        break;
                    }
                }
            }
            else
            {
                // trust process id and just append extension
                returnFilename += LOG_FILE_EXTENSION;
            }

            return returnFilename;
        }

        //**************************************************************************
        // ShouldLog
        // Verifica se il livello di log richiesto e' compatibile con quello attuale
        // Parametri:
        //			wanted		: livello di log richiesto per la prossima operazione
        // Risultato:
        //          true = livello richiesto compatibile con quello attuale
        //          false = altrimenti
        // 
        //**************************************************************************
        private static bool ShouldLog(LogLevel wanted)
        {
            return wanted >= mLogLevel;
        }

        
        //**************************************************************************
        // Sub PrivateWriteLine
        // Scrive un evento nel file di log
        // Parametri:
        //           str		: stringa di descrizione dell'evento
        //**************************************************************************
        private static void PrivateWriteLine(LogLevel level, string str)
        {
            if (!ShouldLog(level))
                return;

            lock (lockObject)
            {
                if (mInitExecuted == false)
                {
                    StartTraceLog();
                }
        
                //Verifica se � cambiato il giorno
                if (mDate != DateTime.Today)
                {
                    Close();
                    while (Trace.Listeners.Count > 1)
                        Trace.Listeners.RemoveAt(1);
                    StartTraceLog(mPath, mNameMode);
                }

                //Scrive il messaggio nel log
                Trace.WriteLine("\r\n" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff: ") + str);
            }
            if (RaiseEventOnWriteline)
            {
                OnNewContent(str);
            }
            ErrorManager.Instance.AddError(str);
        }

        private static void OnNewContent(string content)
        {
            var handler = NewContent;
            if (handler != null)
            {
                handler(null, new WriteLineEventArgs(content));
            }
        }

        private static void Init()
        {
            mInitExecuted = true;
            Trace.AutoFlush = true;
            // trace intestazione di inizio
            Trace.WriteLine("**************************************************");
            Trace.WriteLine("START LOG TRACE: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff"));
            Trace.WriteLine("**************************************************");
        }

        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException ex)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                return !(ex is FileNotFoundException);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        ~TraceLog()
        {
            Close();
        }

        public static void Close()
        {
            try
            {
                Trace.WriteLine("**************************************************");
                Trace.WriteLine("END LOG TRACE: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff"));
                Trace.WriteLine("**************************************************");

                Trace.Flush();
                Trace.Close(); 
            }
            catch
            {}
        }

        //**************************************************************************
        // TraceLog
        // Apre il log
        // Parametri:
        //			path		: percorso del file
        //          nameMode	: modalit� definizione del nome file
        //                              -1 default
        //                               0 giorno della settimana
        //                               1 giorno del mese
        //                               2 giorno dell'anno (mese + giorno)
        //                               3 data completa
        //**************************************************************************
        public static void StartTraceLog()
        {
            StartTraceLog("", 0);
        }

        //**************************************************************************
        // TraceLog
        // Apre il log
        // Parametri:
        //			path		: percorso del file
        //          nameMode	: modalit� definizione del nome file
        //                              -1 default
        //                               0 giorno della settimana
        //                               1 giorno del mese
        //                               2 giorno dell'anno (mese + giorno)
        //                               3 data completa
        //**************************************************************************
        public static void StartTraceLog(string path, int nameMode)
        {
            mDate = DateTime.Today;
            mNameMode = nameMode;
            mPath = path;
            string fileName = "";
            try
            {
                //Crea il nome del file di log
                fileName = FileName(mPath, mDate, mNameMode);
                CheckFile(fileName);
            }
            catch
            { }

            Trace.Listeners.Add(new TextWriterTraceListener(fileName));
            Init();
        }

        /// <summary>
        /// Apre il log e gli assegna un id per identificarlo
        /// </summary>
        /// <param name="path">percorso del file</param>
        /// <param name="nameMode">modalit� definizione del nome file
        ///                              -1 default
        ///                               0 giorno della settimana
        ///                               1 giorno del mese
        ///                               2 giorno dell'anno (mese + giorno)
        ///                               3 data completa</param>
        /// <returns>Id del trace avviato</returns>
        public static int StartTraceLogWithId(string path, int nameMode)
        {
            mDate = DateTime.Today;
            mNameMode = nameMode;
            mPath = path;
            string fileName = "";
            try
            {
                //Crea il nome del file di log
                fileName = FileName(mPath, mDate, mNameMode);
                CheckFile(fileName);
            }
            catch
            { }

            TextWriterTraceListener listener = new TextWriterTraceListener(fileName);
            Trace.Listeners.Add(listener);
            int listenerId = AddToListenersWithId(listener);
            Init();

            return listenerId;
        }

        private static void CheckFile(string fileName)
        {
            //Verifica se il file esiste 
            FileInfo file = new FileInfo(fileName);
            if (file.Exists)
                //Se ha una data di creazione diversa, lo cancella
                if (file.LastWriteTime.Date != mDate)
                    file.Delete();
        }

        /// <summary>
        /// Apre il log e gli assegna un id per identificarlo
        /// </summary>
        /// <returns>Id del trace avviato</returns>
        public static int StartTraceLogWithId()
        {
            return StartTraceLogWithId("", 0);
        }

        /// <summary>
        /// Chiude il log con un determinato id
        /// </summary>
        /// <param name="listenerId"></param>
        public static void Close(int listenerId)
        {
            Close();

            RemoveTraceListener(listenerId);
        }

        /// <summary>
        /// Rimuove un trace listener senza chiudere il log
        /// </summary>
        public static void RemoveTraceListener(int listenerId)
        {
            try
            {
                TextWriterTraceListener listener;
                lock (lockObject)
                {
                    if (!traceListeners.TryGetValue(listenerId, out listener))
                        listener = null;
                    traceListeners.Remove(listenerId);
                }
                if (listener != null)
                    Trace.Listeners.Remove(listener);
            }
            catch
            {
            }
        }

        private static int AddToListenersWithId(TextWriterTraceListener listener)
        {
            int listenerId;
            try
            {
                lock (lockObject)
                {
                    listenersProg++;
                    listenerId = listenersProg;
                    traceListeners.Add(listenerId, listener);
                }
            }
            catch
            {
                listenerId = -1;
            }
            return listenerId;
        }

        public static void RestartTraceLog(string path, int nameMode)
        {
            Close();

            while (Trace.Listeners.Count > 1)
                Trace.Listeners.RemoveAt(1);

            StartTraceLog(path, nameMode);
        }

        public static void RestartTraceLog(string path)
        {
            RestartTraceLog(path, mNameMode);
        }

        public static void RestartTraceLog()
        {
            RestartTraceLog(mPath);
        }

        public static int RestartTraceLog(string path, int nameMode, int listenerId)
        {
            Close(listenerId);

            return StartTraceLogWithId(path, nameMode);
        }

        public static int RestartTraceLog(int listenerId)
        {
            return RestartTraceLog(mPath, mNameMode, listenerId);
        }

        //**************************************************************************
        // Sub WriteLine
        // Scrive un evento nel file di log
        // Parametri:
        //           str		: stringa di descrizione dell'evento
        //**************************************************************************
        public static string WriteException(Exception ex, bool logException = true)
        {
            System.Diagnostics.StackTrace stk = new StackTrace(true);
            string msg = "Exception: ";
            msg += Environment.NewLine + "\tException" + Environment.NewLine + ex.ToString();
            msg += Environment.NewLine + "\tStackTrace" + Environment.NewLine + stk.ToString();

            if (ex is System.Reflection.ReflectionTypeLoadException)
            {
                var typeLoadException = ex as System.Reflection.ReflectionTypeLoadException;
                var loaderExceptions = typeLoadException.LoaderExceptions;
                if (loaderExceptions != null && loaderExceptions.Length > 0)
                    foreach (var lex in loaderExceptions)
                        msg += "\t" + lex.ToString().Replace("\r\n", "_");
            }

            if (logException)
                WriteLine(msg);

            return msg;
        }
        public static string WriteException(Exception ex, string message)
        {
            return WriteException(CreateException(ex, message));
        }
        public static string WriteException(ref Logs log, Exception ex)
        {
            string msg = WriteException(ex);
            TraceLog.Append(ref log, msg);
            return msg;
        }
        public static string WriteException(ref string log, Exception ex)
        {
            string msg = WriteException(ex);
            TraceLog.AppendToLog(ref log, msg);
            return msg;
        }
        public static Exception CreateException(Exception ex, string msg)
        {
            if (ex is Exception)
            {
                var type = ex.GetType();
                var constructor = type.GetConstructor(Type.EmptyTypes);
                if (constructor != null)
                {
                    return (Exception)Activator.CreateInstance(type, msg, ex);
                }
                else
                {
                    string msgException = msg;
                    AppendToLog(ref msgException, ex.ToString());
                    return new Exception(msgException);
                }
            }
            else
                return new Exception(ConvertToVisual(msg));
        }

        //**************************************************************************
        // Sub WriteLine
        // Scrive un evento nel file di log
        // Parametri:
        //           str		: stringa di descrizione dell'evento
        //**************************************************************************
        public static void WriteLine(string str)
        {
            PrivateWriteLine(mLogLevel, str);
        }

        //**************************************************************************
        // Sub WriteLine
        // Scrive un evento nel file di log
        // Parametri:
        //          str		: stringa di descrizione dell'evento
        //			ex		: eccezione
        //**************************************************************************
        public static void WriteLine(string str, Exception ex)
        {
            WriteLine(mLogLevel, str, ex );
        }

        //**************************************************************************
        // Sub WriteLog
        // Scrive un evento nel file di log
        // Parametri:
        //           str		: stringa di descrizione dell'evento
        //**************************************************************************
        public static void WriteLine(LogLevel level, string str)
        {
           PrivateWriteLine(level, str);
        }

        //**************************************************************************
        // Sub WriteLog
        // Scrive un evento nel file di log
        // Parametri:
        //          str		: stringa di descrizione dell'evento
        //			ex		: eccezione
        //**************************************************************************
        public static void WriteLine(LogLevel level, string str, Exception ex)
        {
            str += "\r\n Message: " + ex.Message
                + "\r\n Source: " + ex.Source
                + "\r\n StackTrace:" + ex.StackTrace;
            if (ex.InnerException != null)
                str += "\r\n Inner exception: " + ex.InnerException.ToString();

            PrivateWriteLine(level, str);
        }

        //**************************************************************************
        // Sub WriteLog
        // Scrive time e span rispetto all'ultima chiamata
        // Parametri:
        //          str		: stringa di descrizione dell'evento
        //**************************************************************************
        public static void WriteTime(string str)
        {
            DateTime t = DateTime.Now;
            TimeSpan span = t - mLogDate;
            mLogDate = t;

           
            //System.Diagnostics.Debug.WriteLine("Time: " + t.ToLongTimeString()+"."+t.Millisecond.ToString() + "\tSpan: " + span.TotalMilliseconds.ToString()+ "\t\t" + str);
        }

        //**************************************************************************
        // Sub AppendToLog
        // Aggiunta in log di message
        //**************************************************************************
        public static void AppendToLog(ref string log, string message, bool insertAtStart = false)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (string.IsNullOrEmpty(log))
                log = message;
            else
            {
                if (insertAtStart)
                    log = message + Environment.NewLine + log;
                else
                    log += Environment.NewLine + message;
            }
        }
        //public static void AppendToLog(ref Logs log, Exception ex)
        //{
        //    if (string.IsNullOrEmpty(message))
        //        return;

        //    if (string.IsNullOrEmpty(log))
        //        log = message;
        //    else
        //    {
        //        if (insertAtStart)
        //            log = message + Environment.NewLine + log;
        //        else
        //            log += Environment.NewLine + message;
        //    }
        //}

        //**************************************************************************
        // Sub AppendToLog
        // Aggiunta in log di ex
        //**************************************************************************
        public static void AppendToLog(ref string log, Exception ex)
        {
            TraceLog.AppendToLog(ref log, "\t\tException" + Environment.NewLine + ex.ToString());
            if (ex is TracedException)
            {
                var stk = ((TracedException)ex).OuterStackTrace;
                if (stk != null)
                    TraceLog.AppendToLog(ref log, "\tStackTrace" + Environment.NewLine + stk.ToString());
            }
        }
        public static void Append(ref Logs log, Logs logToAppend)
        {
            if (logToAppend != null && logToAppend.Count > 0)
            {
                if (log == null)
                    log = new Logs();

                for (int i = 0; i < logToAppend.Count; i++)
                    log.Add(logToAppend[i]);
            }
        }
        public static void Prepend(ref Logs log, Logs logsToPrepend)
        {
            if (logsToPrepend != null && logsToPrepend.Count > 0)
            {
                if (log == null)
                    log = new Logs();

                for (int i = logsToPrepend.Count - 1; i >= 0 ; i--)
                    log.Add(logsToPrepend[i]);
            }
        }
        public static void Append(ref Logs log, string error)
        {
            if (!string.IsNullOrEmpty(error))
            {
                if (log == null)
                    log = new Logs();

                log.Add(new Log(LogLevel.Error, error));
            }
        }
        public static void Append(ref Logs log, string logString, LogLevel level)
        {
            if (!string.IsNullOrEmpty(logString))
            {
                if (log == null)
                    log = new Logs();

                log.Add(new Log(level, logString));
            }
        }
        public static void Prepend(ref Logs log, string error)
        {
            if (!string.IsNullOrEmpty(error))
            {
                if (log == null)
                    log = new Logs();

                log.Insert(0, new Log(LogLevel.Error, error));
            }
        }
        public static void Prepend(ref Logs log, string logString, LogLevel level)
        {
            if (!string.IsNullOrEmpty(logString))
            {
                if (log == null)
                    log = new Logs();

                log.Insert(0, new Log(level, logString));
            }
        }
        public static void Append(ref string log, Logs llog)
        {
            Append(ref log, llog, LogLevel.Error);
        }
        public static void Append(ref string log, Logs llog, LogLevel level)
        {
            if (llog != null && llog.Count > 0)
            {
                for (int i = 0; i < llog.Count; i++)
                {
                    var l = llog[i];
                    if (l.LogLevel >= level)
                    {
                        TraceLog.AppendToLog(ref log, l.LogString);
                    }
                }
            }
        }
        public static void AppendLevel(ref string log, Logs llog, LogLevel level, string prependingToEachLog = "")
        {
            if (llog != null && llog.Count > 0)
            {
                for (int i = 0; i < llog.Count; i++)
                {
                    var l = llog[i];
                    if (l.LogLevel == level)
                    {
                        if (string.IsNullOrEmpty(prependingToEachLog))
                            TraceLog.AppendToLog(ref log, l.LogString);
                        else
                            TraceLog.AppendToLog(ref log, prependingToEachLog + l.LogString);
                    }
                }
            }
        }

        //**************************************************************************
        // Sub ConvertToVisual
        // Conversione di una stringa in un valore visibile
        //**************************************************************************
        public static string ConvertToVisual(string value)
        {
            string name;
            if (value == null)
                name = NULL_VALUE;
            else if (value == string.Empty)
                name = EMPTY_VALUE;
            else
                name = value;
            return name;
        }
        public const string NULL_VALUE = "<NULL VALUE>";
        public const string EMPTY_VALUE = "<EMPTY VALUE>";
        public static string ConvertToVisual(int value)
        {
            return ConvertToVisual(value.ToString());
        }
        public static string ConvertToVisual(double value)
        {
            return ConvertToVisual(value.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }
        public static string ConvertToVisual(System.DateTime d, bool fromHours = false, bool addMilliseconds = true)
        {
            string s;
            if (fromHours)
                s = d.Hour.ToString("00") + "_" + d.Minute.ToString("00") + "_" + d.Second.ToString("00");
            else
                s = d.Year + "_" + d.Month.ToString("00") + "_" + d.Day.ToString("00") + "_" + d.Hour.ToString("00") + "_" + d.Minute.ToString("00") + "_" + d.Second.ToString("00");
            if (addMilliseconds)
                s += "_" + d.Millisecond.ToString("000");
            return s;
        }

        //**************************************************************************
        // Sub ValidFullPath
        // Controllo se esiste cartella e nome file valido
        //**************************************************************************
        public static string ValidFullPath(string fullpath)
        {
            var _directory = System.IO.Path.GetDirectoryName(fullpath);
            var _name = System.IO.Path.GetFileName(fullpath);
            if (string.IsNullOrEmpty(_directory) || !System.IO.Directory.Exists(_directory))
                return "directory " + TraceLog.ConvertToVisual(_directory) + " not existing";
            if (_name == null || _name.Trim() == null)
                return "filename " + TraceLog.ConvertToVisual(_name) + " not valid";
            return string.Empty;
        }
    }

    public class TracedException : Exception
    {
        public System.Diagnostics.StackTrace OuterStackTrace = null;
        public TracedException(string message, Exception innerException, System.Diagnostics.StackTrace outerStackTrace)
            : base(message, innerException)
        {
            OuterStackTrace = outerStackTrace;
        }
    }

    public class Log
    {
        public Log()
        {

        }

        public Log(LogLevel logLevel, string log)
        {
            LogLevel = logLevel;
            LogString = log;
        }

        public LogLevel LogLevel = LogLevel.Error;
        public string LogString = string.Empty;

        public override string ToString()
        {
            return TraceLog.ConvertToVisual(LogLevel.ToString()) + "='" + TraceLog.ConvertToVisual(LogString) + "'";
        }
    }

    public class Logs : System.Collections.Generic.List<Log>
    {
        public static void Append(ref Logs logs, Logs logsToAppend)
        {
            TraceLoggers.TraceLog.Append(ref logs, logsToAppend);
        }
        public static void Prepend(ref Logs logs, Logs logsToPrepend)
        {
            TraceLoggers.TraceLog.Prepend(ref logs, logsToPrepend);
        }
        public static void Append(ref Logs logs, Exception ex)
        {
            string s = null;
            TraceLoggers.TraceLog.AppendToLog(ref s, ex);
            TraceLog.Append(ref logs, s);
        }
        public static void Append(ref Logs logs, string s)
        {
            if(!string.IsNullOrEmpty(s))
                TraceLog.Append(ref logs, s);
        }
        public static void Append(ref Logs logs, LogLevel level, string s)
        {
            if(!string.IsNullOrEmpty(s))
                TraceLog.Append(ref logs, s, level);
        }
        public static void Prepend(ref Logs logs, string s)
        {
            if(!string.IsNullOrEmpty(s))
                TraceLog.Prepend(ref logs, s);
        }
        public static void Prepend(ref Logs logs, LogLevel level, string s)
        {
            if(!string.IsNullOrEmpty(s))
                TraceLog.Prepend(ref logs, s, level);
        }
        public static string GetErrors(Logs logs)
        {
            string log = null;
            TraceLog.Append(ref log, logs, LogLevel.Error);
            return log;
        }
        public static string GetString(Logs logs, LogLevel level, string prependingToEachLog = "")
        {
            string log = null;
            var values = ((IList<LogLevel>)Enum.GetValues(typeof(LogLevel)));
            for (int i = values.Count - 1; i >= 0; i--)
            {
                if (level <= values[i])
                    TraceLog.AppendLevel(ref log, logs, values[i], prependingToEachLog);
            }
            return log;
        }
        public static string GetString(Logs logs, LogLevel level, out LogLevel worstLevelFound, string prependingToEachLog = "")
        {
            worstLevelFound = LogLevel.None;
            string log = null;
            var values = ((IList<LogLevel>)Enum.GetValues(typeof(LogLevel)));
            for (int i = values.Count - 1; i >= 0; i--)
            {
                if (level <= values[i])
                {
                    TraceLog.AppendLevel(ref log, logs, values[i], prependingToEachLog);
                    if (!string.IsNullOrEmpty(log))
                    {
                        worstLevelFound = values[i];
                        break;
                    }
                }
            }
            return log;
        }
        public static bool GetReturn(Logs logs)
        {
            return string.IsNullOrEmpty(GetErrors(logs));
        }
        public static string WriteToDisk(Exception ex, string message = "")
        {
            return TraceLoggers.TraceLog.WriteException(ex, message);
        }
        public static string WriteToDisk(Logs logs, LogLevel level = LogLevel.Error, string prependingToEachLog = "� ")
        {
            if (logs == null)
                return null;
            string s = GetString(logs, level, prependingToEachLog);
            if (!string.IsNullOrEmpty(s))
            {
                TraceLoggers.TraceLog.WriteLine(s);
                return s;
            }
            else
            {
                return null;
            }
        }
    }

    public class WriteLineEventArgs : EventArgs
    {
       public string Content {get; set; }

       public WriteLineEventArgs(string content)
       {
           Content = content;
       }
    }
}