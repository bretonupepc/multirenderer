﻿namespace Breton.DesignCenterInterface.Printing
{
    partial class PrintPreviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.printPreviewControl1 = new System.Windows.Forms.PrintPreviewControl();
            this.pnlCommands = new System.Windows.Forms.Panel();
            this.btnExit = new WpfButtons.StandardButton.WpfShadowedImageButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSetupPage = new WpfButtons.StandardButton.WpfShadowedImageButton();
            this.btnPrint = new WpfButtons.StandardButton.WpfShadowedImageButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkBlackWhite = new WpfButtons.CheckButton.WpfShadowedImageCheckButton();
            this.chkColor = new WpfButtons.CheckButton.WpfShadowedImageCheckButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnZoomFit = new WpfButtons.StandardButton.WpfShadowedImageButton();
            this.btnZoomOut = new WpfButtons.StandardButton.WpfShadowedImageButton();
            this.btnZoomIn = new WpfButtons.StandardButton.WpfShadowedImageButton();
            this.btnZoomDimension = new WpfButtons.StandardButton.WpfShadowedImageButton();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pnlCommands.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // printPreviewControl1
            // 
            this.printPreviewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printPreviewControl1.Location = new System.Drawing.Point(0, 0);
            this.printPreviewControl1.Name = "printPreviewControl1";
            this.printPreviewControl1.Size = new System.Drawing.Size(653, 592);
            this.printPreviewControl1.TabIndex = 0;
            // 
            // pnlCommands
            // 
            this.pnlCommands.BackColor = System.Drawing.Color.LightGray;
            this.pnlCommands.Controls.Add(this.btnExit);
            this.pnlCommands.Controls.Add(this.groupBox3);
            this.pnlCommands.Controls.Add(this.groupBox2);
            this.pnlCommands.Controls.Add(this.groupBox1);
            this.pnlCommands.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlCommands.Location = new System.Drawing.Point(653, 0);
            this.pnlCommands.Name = "pnlCommands";
            this.pnlCommands.Size = new System.Drawing.Size(82, 592);
            this.pnlCommands.TabIndex = 1;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExit.BackColor = System.Drawing.Color.Transparent;
            this.btnExit.ForeColor = System.Drawing.Color.Transparent;
            this.btnExit.Image = global::BretonViewportLayout.Properties.Resources.system_log_out;
            this.btnExit.ImageShadowBlurRadius = 5D;
            this.btnExit.ImageShadowColor = System.Drawing.Color.DimGray;
            this.btnExit.ImageShadowDepth = 6D;
            this.btnExit.ImageShadowDirection = 320D;
            this.btnExit.ImageShadowOpacity = 0.5D;
            this.btnExit.Location = new System.Drawing.Point(12, 527);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(60, 60);
            this.btnExit.TabIndex = 11;
            this.btnExit.ButtonClick += new System.EventHandler<System.EventArgs>(this.btnExit_ButtonClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnSetupPage);
            this.groupBox3.Controls.Add(this.btnPrint);
            this.groupBox3.Location = new System.Drawing.Point(6, 364);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(72, 138);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // btnSetupPage
            // 
            this.btnSetupPage.BackColor = System.Drawing.Color.Transparent;
            this.btnSetupPage.ForeColor = System.Drawing.Color.Transparent;
            this.btnSetupPage.Image = global::BretonViewportLayout.Properties.Resources.documentinfo_koffice;
            this.btnSetupPage.ImageShadowBlurRadius = 5D;
            this.btnSetupPage.ImageShadowColor = System.Drawing.Color.DimGray;
            this.btnSetupPage.ImageShadowDepth = 6D;
            this.btnSetupPage.ImageShadowDirection = 320D;
            this.btnSetupPage.ImageShadowOpacity = 0.5D;
            this.btnSetupPage.Location = new System.Drawing.Point(11, 19);
            this.btnSetupPage.Name = "btnSetupPage";
            this.btnSetupPage.Size = new System.Drawing.Size(48, 48);
            this.btnSetupPage.TabIndex = 2;
            this.btnSetupPage.ButtonClick += new System.EventHandler<System.EventArgs>(this.btnSetupPage_ButtonClick);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.Transparent;
            this.btnPrint.ForeColor = System.Drawing.Color.Transparent;
            this.btnPrint.Image = global::BretonViewportLayout.Properties.Resources.fileprint;
            this.btnPrint.ImageShadowBlurRadius = 5D;
            this.btnPrint.ImageShadowColor = System.Drawing.Color.DimGray;
            this.btnPrint.ImageShadowDepth = 6D;
            this.btnPrint.ImageShadowDirection = 320D;
            this.btnPrint.ImageShadowOpacity = 0.5D;
            this.btnPrint.Location = new System.Drawing.Point(11, 73);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(48, 48);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.ButtonClick += new System.EventHandler<System.EventArgs>(this.btnPrint_ButtonClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkBlackWhite);
            this.groupBox2.Controls.Add(this.chkColor);
            this.groupBox2.Location = new System.Drawing.Point(6, 230);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(72, 128);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            // 
            // chkBlackWhite
            // 
            this.chkBlackWhite.BackColor = System.Drawing.Color.Transparent;
            this.chkBlackWhite.Checked = false;
            this.chkBlackWhite.ForeColor = System.Drawing.Color.Transparent;
            this.chkBlackWhite.Image = global::BretonViewportLayout.Properties.Resources.Black_White;
            this.chkBlackWhite.ImageShadowBlurRadius = 5D;
            this.chkBlackWhite.ImageShadowColor = System.Drawing.Color.DimGray;
            this.chkBlackWhite.ImageShadowDepth = 6D;
            this.chkBlackWhite.ImageShadowDirection = 320D;
            this.chkBlackWhite.ImageShadowOpacity = 0.5D;
            this.chkBlackWhite.Location = new System.Drawing.Point(11, 16);
            this.chkBlackWhite.Name = "chkBlackWhite";
            this.chkBlackWhite.Size = new System.Drawing.Size(48, 48);
            this.chkBlackWhite.TabIndex = 6;
            this.chkBlackWhite.ButtonCheckedChanged += new System.EventHandler<System.EventArgs>(this.chkBlackWhite_ButtonCheckedChanged);
            // 
            // chkColor
            // 
            this.chkColor.BackColor = System.Drawing.Color.Transparent;
            this.chkColor.Checked = false;
            this.chkColor.ForeColor = System.Drawing.Color.Transparent;
            this.chkColor.Image = global::BretonViewportLayout.Properties.Resources.color_line;
            this.chkColor.ImageShadowBlurRadius = 5D;
            this.chkColor.ImageShadowColor = System.Drawing.Color.DimGray;
            this.chkColor.ImageShadowDepth = 6D;
            this.chkColor.ImageShadowDirection = 320D;
            this.chkColor.ImageShadowOpacity = 0.5D;
            this.chkColor.Location = new System.Drawing.Point(11, 64);
            this.chkColor.Name = "chkColor";
            this.chkColor.Size = new System.Drawing.Size(48, 48);
            this.chkColor.TabIndex = 7;
            this.chkColor.ButtonCheckedChanged += new System.EventHandler<System.EventArgs>(this.chkColor_ButtonCheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnZoomFit);
            this.groupBox1.Controls.Add(this.btnZoomOut);
            this.groupBox1.Controls.Add(this.btnZoomIn);
            this.groupBox1.Controls.Add(this.btnZoomDimension);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(72, 223);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // btnZoomFit
            // 
            this.btnZoomFit.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomFit.ForeColor = System.Drawing.Color.Transparent;
            this.btnZoomFit.Image = global::BretonViewportLayout.Properties.Resources.zoom_fit_best;
            this.btnZoomFit.ImageShadowBlurRadius = 5D;
            this.btnZoomFit.ImageShadowColor = System.Drawing.Color.DimGray;
            this.btnZoomFit.ImageShadowDepth = 6D;
            this.btnZoomFit.ImageShadowDirection = 320D;
            this.btnZoomFit.ImageShadowOpacity = 0.5D;
            this.btnZoomFit.Location = new System.Drawing.Point(11, 16);
            this.btnZoomFit.Name = "btnZoomFit";
            this.btnZoomFit.Size = new System.Drawing.Size(48, 48);
            this.btnZoomFit.TabIndex = 4;
            this.btnZoomFit.ButtonClick += new System.EventHandler<System.EventArgs>(this.btnZoomFit_ButtonClick);
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomOut.ForeColor = System.Drawing.Color.Transparent;
            this.btnZoomOut.Image = global::BretonViewportLayout.Properties.Resources.zoom_out;
            this.btnZoomOut.ImageShadowBlurRadius = 5D;
            this.btnZoomOut.ImageShadowColor = System.Drawing.Color.DimGray;
            this.btnZoomOut.ImageShadowDepth = 6D;
            this.btnZoomOut.ImageShadowDirection = 320D;
            this.btnZoomOut.ImageShadowOpacity = 0.5D;
            this.btnZoomOut.Location = new System.Drawing.Point(11, 160);
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.Size = new System.Drawing.Size(48, 48);
            this.btnZoomOut.TabIndex = 3;
            this.btnZoomOut.ButtonClick += new System.EventHandler<System.EventArgs>(this.btnZoomOut_ButtonClick);
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomIn.ForeColor = System.Drawing.Color.Transparent;
            this.btnZoomIn.Image = global::BretonViewportLayout.Properties.Resources.zoom_in;
            this.btnZoomIn.ImageShadowBlurRadius = 5D;
            this.btnZoomIn.ImageShadowColor = System.Drawing.Color.DimGray;
            this.btnZoomIn.ImageShadowDepth = 6D;
            this.btnZoomIn.ImageShadowDirection = 320D;
            this.btnZoomIn.ImageShadowOpacity = 0.5D;
            this.btnZoomIn.Location = new System.Drawing.Point(11, 112);
            this.btnZoomIn.Name = "btnZoomIn";
            this.btnZoomIn.Size = new System.Drawing.Size(48, 48);
            this.btnZoomIn.TabIndex = 1;
            this.btnZoomIn.ButtonClick += new System.EventHandler<System.EventArgs>(this.btnZoomIn_ButtonClick);
            // 
            // btnZoomDimension
            // 
            this.btnZoomDimension.BackColor = System.Drawing.Color.Transparent;
            this.btnZoomDimension.ForeColor = System.Drawing.Color.Transparent;
            this.btnZoomDimension.Image = global::BretonViewportLayout.Properties.Resources.zoom_original;
            this.btnZoomDimension.ImageShadowBlurRadius = 5D;
            this.btnZoomDimension.ImageShadowColor = System.Drawing.Color.DimGray;
            this.btnZoomDimension.ImageShadowDepth = 6D;
            this.btnZoomDimension.ImageShadowDirection = 320D;
            this.btnZoomDimension.ImageShadowOpacity = 0.5D;
            this.btnZoomDimension.Location = new System.Drawing.Point(11, 64);
            this.btnZoomDimension.Name = "btnZoomDimension";
            this.btnZoomDimension.Size = new System.Drawing.Size(48, 48);
            this.btnZoomDimension.TabIndex = 2;
            this.btnZoomDimension.ButtonClick += new System.EventHandler<System.EventArgs>(this.btnZoomDimension_ButtonClick);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // PrintPreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 592);
            this.Controls.Add(this.printPreviewControl1);
            this.Controls.Add(this.pnlCommands);
            this.Name = "PrintPreviewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Print preview";
            this.pnlCommands.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PrintPreviewControl printPreviewControl1;
        private System.Windows.Forms.Panel pnlCommands;
        private System.Windows.Forms.PrintDialog printDialog1;
        private WpfButtons.StandardButton.WpfShadowedImageButton btnPrint;
        private WpfButtons.StandardButton.WpfShadowedImageButton btnSetupPage;
        private WpfButtons.StandardButton.WpfShadowedImageButton btnZoomIn;
        private WpfButtons.StandardButton.WpfShadowedImageButton btnZoomFit;
        private WpfButtons.StandardButton.WpfShadowedImageButton btnZoomOut;
        private WpfButtons.StandardButton.WpfShadowedImageButton btnZoomDimension;
        private System.Windows.Forms.GroupBox groupBox2;
        private WpfButtons.CheckButton.WpfShadowedImageCheckButton chkBlackWhite;
        private WpfButtons.CheckButton.WpfShadowedImageCheckButton chkColor;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private WpfButtons.StandardButton.WpfShadowedImageButton btnExit;
    }
}