﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesignCenterInterface.Interfaces;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using Point = Breton.Polygons.Point;

namespace Breton.DesignCenterInterface
{
    public class EyeSelection : EntityList, IEntitiesSelection
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _name = string.Empty;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public EyeSelection():base()
        {
            
        }

        public EyeSelection(string name):this()
        {
            _name = name;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public EyeSelection Clone()
        {
            EyeSelection selection = (EyeSelection)this.MemberwiseClone();
            selection.Name = this.Name;
            return selection;
        }

        public void AddItem(Entity entity, bool checkIfAlreadyExists)
        {
            if (!checkIfAlreadyExists )
            {
                Add(entity);
            }
            else
            {
                if (!Contains(entity))
                {
                    Add(entity);
                }
            }
        }
        public void AddItem(Entity entity)
        {
            Add(entity);
        }

        public void FilterSelect(ViewportLayout layout, EntitiesFilter filter, bool clearBefore = true)
        {
            if (clearBefore && Count > 0)
            {
                Clear();
            }
            if (layout == null)
            {
                return;
            }

            foreach (var entity in layout.Entities)
            {
                bool filteredOut = true;
                if (filter.EntityDataList.Count > 0)
                {
                    string entData = entity.EntityData as string;
                    if (entData != null && filter.EntityDataList.Contains(entData))
                    {
                        filteredOut = false;
                    }
                }
                if (!filteredOut)
                {
                    filteredOut = true;
                    if (filter.TypeNames.Count > 0)
                    {
                        if (filter.TypeNames.Contains(entity.GetType().Name))
                        {
                            filteredOut = false;
                        }
                    }
                }
                if (!filteredOut)
                {
                    AddItem(entity);
                }

            }
        }

        public void ApplyFilter(EntitiesFilter filter)
        {
            for (int i = Count - 1; i >= 0; i--)
            {
                bool filteredOut = true;
                Entity ent = this[i];
                if (filter.EntityDataList.Count > 0)
                {
                    string entData = ent.EntityData as string;
                    if (entData != null && filter.EntityDataList.Contains(entData))
                    {
                        filteredOut = false;
                    }
                }

                if (!filteredOut)
                {
                    filteredOut = true;
                    if (filter.TypeNames.Count > 0)
                    {
                        if (filter.TypeNames.Contains(ent.GetType().Name))
                        {
                            filteredOut = false;
                        }
                    }
                }
                if (filteredOut)
                {
                    RemoveAt(i);
                }
            }
        }

        public new void Clear()
        {
            try
            {
                if (Count > 0)
                {
                    base.Clear();
                }
            }
            catch
            {
                
            }
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        #region IEntitiesSelection Members


        IEntitiesSelection IEntitiesSelection.Clone()
        {
            return this.Clone();
        }

        public void AddItem(object entity, bool checkIfAlreadyExists)
        {
            Entity eyeshotEntity = entity as Entity;
            if (eyeshotEntity != null)
                AddItem(eyeshotEntity, checkIfAlreadyExists);
        }

        public void AddItem(object entity)
        {
            Entity eyeshotEntity = entity as Entity;
            if (eyeshotEntity != null)
                AddItem(eyeshotEntity);
        }

        public void FilterSelect(object layout, EntitiesFilter filter, bool clearBefore = true)
        {
            ViewportLayout viewportLayout = layout as ViewportLayout;
            if (viewportLayout != null)
            {
                FilterSelect(viewportLayout, filter, clearBefore);
            }
        }

        public void Rotate(double angle, Point rotationCenter)
        {
            Rotate(angle, Vector3D.AxisZ, new Point3D(rotationCenter.X, rotationCenter.Y));
        }

        public void Traslate(double deltaX, double deltaY)
        {
            Translate(deltaX, deltaY, 0);
        }


        #endregion
    }
}
