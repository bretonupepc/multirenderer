﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using devDept.Graphics;

namespace BretonViewportLayout
{
    public class ZTopLine : Line
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ZTopLine(Line another) : base(another)
        {
        }

        public ZTopLine(ZTopLine another) : base(another)
        {
        }

        public ZTopLine(Plane sketchPlane, Point2D startPoint, Point2D endPoint) : base(sketchPlane, startPoint, endPoint)
        {}

        public ZTopLine(Plane sketchPlane, double x1, double y1, double x2, double y2)
            : base(sketchPlane, x1, y1, x2, y2)
        {}

        public ZTopLine(Point3D startPoint, Point3D endPoint)
            : base(startPoint, endPoint)
        {}
        
        public ZTopLine(Segment3D seg) : base(seg)
        {}

        public ZTopLine(Segment2D seg)
            : base(seg)
        { }

        public ZTopLine(SerializationInfo info, StreamingContext context) : base(info, context)
        {}

        public ZTopLine(double x1, double y1, double x2, double y2): base(x1, y1, x2, y2)
        {}

        public ZTopLine(double x1, double y1, double z1, double x2, double y2, double z2): base(x1, y1, z1, x2, y2, z2)
        {}

        #endregion Constructors  -----------<


        #region >-------------- Overrides

        protected override void Draw(DrawParams data)
        {
            data.RenderContext.PushRasterizerState();
            data.RenderContext.SetState(rasterizerStateType.CCW_PolygonLine_CullFace_Front);

            base.Draw(data);

            data.RenderContext.PopRasterizerState();
        }



        #endregion Overrides --------<

        #region >-------------- Protected Fields



        #endregion Protected Fields --------<
        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
