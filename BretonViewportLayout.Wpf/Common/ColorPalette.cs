﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;

namespace Breton.DesignCenterInterface
{
    public class ColorPalette : List<Color>
    {
        private Color _defaultColor = Color.Black;

        public ColorPalette()
        {
            BuildDefault();
        }

        public Color DefaultColor
        {
            get { return _defaultColor; }
            set { _defaultColor = value; }
        }

        public new Color this[int index]
        {
            get
            {
                if (Count > index)
                    return base[index];
                return _defaultColor;
            }
            set { base[index] = value; }
        }

        private void BuildDefault()
        {
            Add(Color.Blue);
            Add(Color.White);
            Add(Color.Red);
            Add(Color.Yellow);
            Add(Color.Green);
            Add(Color.Magenta);
            Add(Color.Cyan);

        }
    }
}
