﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using devDept.Eyeshot.Entities;

namespace BretonViewportLayout.Common
{
    public static class EntityExtension
    {
        private static readonly Color HIGHLIGHT_COLOR = Color.Fuchsia;

        public static void SetColor(this Entity entity, Color color)
        {
            if (entity.XData == null)
            {
                entity.XData = new List<KeyValuePair<short, object>>();
            }
            var updatedItem = new KeyValuePair<short, object>(0, color);
            if (entity.XData.Count == 0)
            {
                entity.XData.Add(updatedItem);
            }
            else
            {
                entity.XData[0] = updatedItem;
            }
        }

        public static Color GetColor(this Entity entity)
        {
            Color retValue = Color.Empty;

            if ((entity.XData != null)
                && (entity.XData.Count > 0))
            {
                var item = entity.XData.FirstOrDefault(x => x.Key == 0);
                if (item.Key == 0)
                {
                    retValue = (Color) item.Value;
                }
            }

            return retValue;
        }

        public static void HiLight(this Entity entity, bool on)
        {
            entity.Color = on ? HIGHLIGHT_COLOR : entity.GetColor();
        }
    }
}
