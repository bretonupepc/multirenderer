﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.PropertyGridInternal;
using devDept.Eyeshot;
using devDept.Eyeshot.Translators;
using devDept.Graphics;

namespace Breton.DesignCenterInterface
{
    public enum ViewportBackgroundMode
    {
        Solid = backgroundStyleType.Solid,
        LinearGradient = backgroundStyleType.LinearGradient
    }

    public enum GraphicCursorMode
    {
        Cross,
        ViewFinder
    }

    public partial class Breton2DViewer : UserControl
    {
        #region >-------------- Constants and Enums

        //private const string SERIAL_NUMBER = "EYEPRO-812M-F9J2L-A0HA1-7HWG2";

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        protected override void OnHandleDestroyed(EventArgs e)
        {
            base.OnHandleDestroyed(e);
            ReadAutodesk.OnApplicationExit(null, new EventArgs());
        }

        #region >-------------- Private Fields

        private bool _showProperties = false;

        ////private Bitmap _viewerBitmap = null;

        private Color _cursorColor = Color.Black;

        private Color _cursorCoordinatesColor = Color.Black;

        private Color _cursorTipsColor = Color.Black;

        private Color _zoomWindowColor = Color.Orange;

        private bool _zoomWindowDottedBorder = false;

        private bool _zoomWindowShowBorder = true;

        private bool _zoomWindowFill = true;

        private bool _keyShortcutsEnabled = false;

        private Cursor _voidCursor;

        private Cursor _defaultCursor;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public Breton2DViewer()
        {
            InitializeComponent();

            draftingViewportLayout1.UseShaders = false;

            draftingViewportLayout1.Dock = DockStyle.None;



            if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
            {
                SetStyle(ControlStyles.ResizeRedraw | ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
                LoadVoidCursor();
                LoadPanCursor();


            }

            SetupControls();
            propertyGrid1.SelectedObject = null;

            ResizeRedraw = true;

        }

        protected override void InitLayout()
        {
            base.InitLayout();

            draftingViewportLayout1.Dock = DockStyle.Fill;

        }

        private void LoadVoidCursor()
        {
            try
            {
                _defaultCursor = draftingViewportLayout1.GetDefaultCursor();
                System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();

                using (System.IO.Stream imgStream = a.GetManifestResourceStream("BretonViewportLayout.VoidCursor.cur"))
                {
                    _voidCursor = 
                    draftingViewportLayout1.VoidCursor = new Cursor(imgStream);
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void LoadPanCursor()
        {
            try
            {
                System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();

                using (System.IO.Stream imgStream = a.GetManifestResourceStream("BretonViewportLayout.PanCursor.cur"))
                {
                    draftingViewportLayout1.PanCursor = new Cursor(imgStream);
                }
            }
            catch (Exception ex)
            {

            }
        }




        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [Browsable(false)]
        public DraftingViewportLayout BretonLayout
        {
            get { return draftingViewportLayout1; }
        }

        public bool ShowProperties
        {
            get
            {
                return _showProperties; 
                
            }
            set
            {
                _showProperties = value;
                SetupControls();
            }
        }


        [Category("Viewer background")]
        public Color BackgroundTopColor
        {
            get { return BretonLayout.Viewports[0].Background.TopColor; }
            set
            {
                BretonLayout.Viewports[0].Background.TopColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer background")]
        public Color BackgroundIntermediateColor
        {
            get { return BretonLayout.Viewports[0].Background.IntermediateColor; }
            set
            {
                BretonLayout.Viewports[0].Background.IntermediateColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer background")]
        public Color BackgroundBottomColor
        {
            get { return BretonLayout.Viewports[0].Background.BottomColor; }
            set
            {
                BretonLayout.Viewports[0].Background.BottomColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer background")]
        public double BackgroundIntermediateColorPosition
        {
            get { return BretonLayout.Viewports[0].Background.IntermediateColorPosition; }
            set
            {
                BretonLayout.Viewports[0].Background.IntermediateColorPosition = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public Color AxisLabelsColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelColor; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelColor = value;
                BretonLayout.UpdateViewportLayout();
            }

        }

        [Category("Viewer Axes")]
        public Color AxisXColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.ArrowColorX; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.ArrowColorX = value;
                BretonLayout.UpdateViewportLayout();
            }
        }
        [Category("Viewer Axes")]
        public Color AxisYColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.ArrowColorY; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.ArrowColorY = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public Color AxisZColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.ArrowColorZ; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.ArrowColorZ = value;
                BretonLayout.UpdateViewportLayout();
            }
        }


        [Category("Viewer Axes")]
        public string AxisXLabel
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelAxisX; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelAxisX = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public string AxisYLabel
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelAxisY; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelAxisY = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public string AxisZLabel
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelAxisZ; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelAxisZ = value;
                BretonLayout.UpdateViewportLayout();
            }
        }




        [Category("Viewer background")]
        public ViewportBackgroundMode BackgroundMode
        {
            get
            {
                return (ViewportBackgroundMode)BretonLayout.Viewports[0].Background.Style;
            }
            set
            {
                BretonLayout.Viewports[0].Background.Style = (backgroundStyleType)value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer misc colors")]
        public Color CursorColor
        {
            get { return BretonLayout.CursorColor; }
            set
            {
                BretonLayout.CursorColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer misc colors")]
        public Color CursorCoordinatesColor
        {
            get { return BretonLayout.CursorCoordinatesColor; }
            set
            {
                BretonLayout.CursorCoordinatesColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer misc colors")]
        public Color CursorTipsColor
        {
            get { return BretonLayout.CursorTipsColor; }
            set
            {
                BretonLayout.CursorTipsColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Zoom window")]
        public Color ZoomWindowColor
        {
            get { return _zoomWindowColor; }
            set
            {
                _zoomWindowColor = value; 
                
            }
        }

        [Category("Zoom window")]
        public bool ZoomWindowShowBorder
        {
            get { return _zoomWindowShowBorder; }
            set { _zoomWindowShowBorder = value; }
        }

        [Category("Zoom window")]
        public bool ZoomWindowDottedBorder
        {
            get { return _zoomWindowDottedBorder; }
            set
            {
                _zoomWindowDottedBorder = value;

            }
        }


        [Category("Zoom window")]
        public bool ZoomWindowFill
        {
            get { return _zoomWindowFill; }
            set { _zoomWindowFill = value; }
        }

        public bool KeyShortcutsEnabled
        {
            get { return _keyShortcutsEnabled; }
            set { _keyShortcutsEnabled = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public  void ShowCrossCursor(bool show)
        {
            ////draftingViewportLayout1.Cursor = show ? _defaultCursor : _voidCursor;
        }

        public void Clear()
        {
            this.draftingViewportLayout1.Clear();
        }
        #endregion Public Methods -----------<

        #region >-------------- Protected Methods


        ////protected override void OnEnabledChanged(EventArgs e)
        ////{
        ////    if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
        ////    {
        ////        if (!Enabled)
        ////        {
        ////            _viewerBitmap = (Bitmap)SetImageOpacity(BretonLayout.RenderToBitmap(this.Size), 0.7F);
        ////        }
        ////        BretonLayout.Visible = Enabled;
        ////    }

        ////    base.OnEnabledChanged(e);
        ////}

        ////protected override void OnPaint(PaintEventArgs e)
        ////{
        ////    if (DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime)
        ////    {
        ////        base.OnPaint(e);
        ////        return;
        ////    }


        ////    if (!Enabled)
        ////    {
        ////        using (var bitmap = new Bitmap(Width, Height))
        ////        {
        ////            using (var g = Graphics.FromImage(bitmap))
        ////            {
        ////                g.Clear(Color.White);
        ////                Bitmap transparentBckg = null;

        ////                if (_viewerBitmap != null)
        ////                {
        ////                    g.DrawImage(_viewerBitmap, new Rectangle(0,0,Width, Height));
        ////                }
        ////                if (BretonViewportLayout.Properties.Resources.Lucchetto != null)
        ////                {
        ////                    transparentBckg = (Bitmap)SetImageOpacity(BretonViewportLayout.Properties.Resources.Locker, 0.8F);
        ////                    Point topLeft = new Point((Width - transparentBckg.Width) / 2, (Height - transparentBckg.Height) / 2);
        ////                    g.DrawImage(transparentBckg, topLeft);
        ////                }
        ////                if (transparentBckg != null)
        ////                    transparentBckg.Dispose();

        ////            }

        ////            e.Graphics.DrawImage(bitmap, new Point(0, 0));
        ////        }

        ////    }
        ////    else
        ////    {
        ////        base.OnPaint(e);

        ////    }

        ////}

        ////protected override void OnPaintBackground(PaintEventArgs e)
        ////{
        ////    if (DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime)
        ////        base.OnPaintBackground(e);
        ////}

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void draftingViewportLayout1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //if (!_keyShortcutsEnabled)
            //{
                
            //    return;
            //}

            //if (e.Alt && e.KeyCode == Keys.F7)
            //{
            //    ShowProperties = !ShowProperties;
            //}
            //if (e.Alt && e.KeyCode == Keys.F8)
            //{
            //    BretonLayout.TryExportToDxf();
            //}

            //if (e.KeyCode == Keys.F12)
            //{
            //    BretonLayout.Entities.Regen();
            //    BretonLayout.Invalidate();
            //}

        }

        protected override void OnPreviewKeyDown(PreviewKeyDownEventArgs e)
        {
            if (!_keyShortcutsEnabled)
            {
                return;
            }

            if (e.Alt && e.KeyCode == Keys.F7)
            {
                ShowProperties = !ShowProperties;
            }
            if (e.Alt && e.KeyCode == Keys.F8)
            {
                BretonLayout.TryExportToDxf();
            }

            if (e.KeyCode == Keys.F12)
            {
                BretonLayout.Entities.Regen();
                BretonLayout.Invalidate();
            }

        }


        protected override void OnLoad(EventArgs e)
        {
            DoubleBuffered = true;

            base.OnLoad(e);

        }

        private void SetupControls()
        {
            splitter1.Visible = _showProperties;
            pnlProperties.Visible = _showProperties;
            BretonLayout.BringToFront();
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        public bool UseTouchLayout
        {
            get { return BretonLayout.UseTouchLayout; }
            set { BretonLayout.UseTouchLayout = value; }
        }
    }
}
