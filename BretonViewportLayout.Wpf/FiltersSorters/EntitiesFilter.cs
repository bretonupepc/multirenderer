﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using devDept.Eyeshot.Entities;

namespace Breton.DesignCenterInterface
{
    public class EntitiesFilter
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private List<string> _typeNames = new List<string>();
        private List<string> _entityDataList = new List<string>(); 


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public List<string> TypeNames
        {
            get { return _typeNames; }
            set { _typeNames = value; }
        }

        public List<string> EntityDataList
        {
            get { return _entityDataList; }
            set { _entityDataList = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
