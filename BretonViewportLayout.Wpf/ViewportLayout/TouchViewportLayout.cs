﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using devDept.Eyeshot;
using devDept.Eyeshot.Multitouch;
using devDept.Geometry;
using devDept.Graphics;
using TraceLoggers;
using ToolBarButton = devDept.Eyeshot.ToolBarButton;

namespace Breton.DesignCenterInterface
{
    public enum UM
    {
        Millimeters,
        Inches
    }

    public enum AxisOrientation
    {
        LeftBottom,
        LeftTop,
        RightBottom,
        RightTop
    }
    public partial class DraftingViewportLayout : devDept.Eyeshot.ViewportLayout
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private bool _actionModeActive = false;

        private bool _actionModeJustStarted = false;

        private ToolBarButton _activeButton = null;

        private readonly Dictionary<ToolBarButton, actionType> _buttonAction = new Dictionary<ToolBarButton, actionType>();

        private actionType _defaultActionType = actionType.None;

        private double _viewTopRotationAngle = 0;

        #region Ruler

        private Point2D _lowerLeftWorldCoord, _upperRightWorldCoord;
        private Point2D _originScreen;

        private Plane _rulerPlane = Plane.XY;

        private int _rulerSize = 35;

        private UM _measureUnit = UM.Millimeters;

        private double  _umRatio = 1;

        private bool _showRuler = false;

        private bool _showRulerCoordinates = false;

        private AxisOrientation _axisMode = AxisOrientation.LeftBottom;

        #endregion

        #endregion Private Fields --------<

        #region >-------------- Constructors


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public actionType DefaultActionType
        {
            get { return _defaultActionType; }
            set { _defaultActionType = value; }
        }

        public UM MeasureUnit
        {
            get { return _measureUnit; }
            set
            {
                _measureUnit = value;
                _umRatio = (_measureUnit == UM.Millimeters) ? 1 : 25.4;
            }
        }


        public AxisOrientation AxisMode
        {
            get { return _axisMode; }
            set { _axisMode = value; }
        }

        public bool ShowRulerCoordinates
        {
            get { return _showRulerCoordinates; }
            set { _showRulerCoordinates = value; }
        }

        public double ViewTopRotationAngle
        {
            get { return _viewTopRotationAngle; }
            set
            {
                int invertRotationFactor = (AxisMode == AxisOrientation.LeftTop ||
                                            AxisMode == AxisOrientation.RightBottom)
                                            ? -1
                                            : 1;
                _viewTopRotationAngle = value;
                this.RotateCamera(new Vector3D(0, 0, 1), _viewTopRotationAngle * invertRotationFactor, false);
            }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties


        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override void DrawEntities(DrawSceneParams myParams)
        {
            base.DrawEntities(myParams);

            //if (!IsHandleCreated) return;

            Point3D ptUpperRight, ptLowerLeft;
            if ( ScreenToPlane(new Point(Width, _rulerSize), _rulerPlane, out ptUpperRight)
                 &&
                 ScreenToPlane(new Point(_rulerSize, Height), _rulerPlane, out ptLowerLeft)
                )
            {
                _originScreen = WorldToScreen(0, 0, 0);


                _upperRightWorldCoord = new Point2D(ptUpperRight.X, ptUpperRight.Y);
                _lowerLeftWorldCoord = new Point2D(ptLowerLeft.X, ptLowerLeft.Y);
            }



        }

        private void DrawRuler()
        {
            bool isRotated = (ViewTopRotationAngle == 90 || ViewTopRotationAngle == 270);

            bool isMirrored = (ViewTopRotationAngle == 180 || ViewTopRotationAngle == 270);

            var currentWireframeColor = renderContext.CurrentWireColor;

            renderContext.SetState(depthStencilStateType.DepthTestOff);
            renderContext.SetState(blendStateType.Blend);

            // Draw the transparent ruler
            renderContext.SetColorWireframe(Color.FromArgb((int)(0.4 * 255), 255, 255, 255));
            renderContext.SetState(rasterizerStateType.CCW_PolygonFill_NoCullFace_NoPolygonOffset);

            // Vertical Ruler
            renderContext.DrawQuad(new RectangleF(0, 0, _rulerSize, Height - _rulerSize));

            // Horizontal Ruler
            renderContext.DrawQuad(new RectangleF(_rulerSize, Height - _rulerSize, Width - _rulerSize, _rulerSize));


            // Coordinates Rect
            if (_showRulerCoordinates)
            {
                renderContext.SetColorWireframe(Color.FromArgb((int)(0.5 * 255), 255, 255, 255));
                renderContext.DrawQuad(new RectangleF(Width - 90, 0, 90, 40));

                renderContext.SetColorWireframe(Color.FromArgb((int)(1 * 255), 0, 0, 0));
                renderContext.DrawQuad(new RectangleF(Width - 88, 2, 90 - 4, 36));
            }

            renderContext.SetState(blendStateType.NoBlend);





            // choose a string format with 1 decimal numbers

            string formatString = (_measureUnit == UM.Millimeters) ? "{0:0.#}" : "{0:0.#}\"";
            string coordinatesFormatString = (_measureUnit == UM.Millimeters) ? "{0:##0.0} mm" : "{0:0.000}\"";

            double linesLeftPosition = 5;
            double stepWorldX = 5, stepWorldY = 5;

            double worldHeight = isRotated
                                ? _upperRightWorldCoord.X - _lowerLeftWorldCoord.X
                                : _upperRightWorldCoord.Y - _lowerLeftWorldCoord.Y;
            //double nlinesH = (worldHeight / stepWorldY);

            //double worldWidth = _upperRightWorldCoord.X - _lowerLeftWorldCoord.X;
            //double nlinesW = (worldWidth / stepWorldX);



            double worldToScreen = (Height - _rulerSize) / (worldHeight);

            double worldToScreenForX = (_axisMode == AxisOrientation.LeftBottom
                                        || _axisMode == AxisOrientation.RightBottom)
                ? worldToScreen
                : -worldToScreen;

            ////RefineStep(nlinesH, worldHeight, ref stepWorldY);
            ////RefineStep(nlinesW, worldWidth, ref stepWorldX);

            //double stepWorld = Math.Min(stepWorldX, stepWorldY);


            double stepWorld = (_measureUnit == UM.Millimeters) ?  200  : 254;

            double stepScreen = stepWorld * worldToScreen;
            double stepScreenForX = stepWorld * worldToScreenForX;

            double reverseFactor = 1.0;

            double mirrorFactor = 1.0;

            #region Not rotated (horizontal = X, Vertical = Y

            if (!isRotated)
            {

                ///////////////////////////
                // Vertical ruler
                ///////////////////////////

                // First line Y world coordinate
                double startYWorld = stepWorld * Math.Floor(_lowerLeftWorldCoord.Y / stepWorld);

                Point2D firstLineScreenPositionY = new Point2D(linesLeftPosition, _originScreen.Y + startYWorld * worldToScreen);
                double currentScreenY = firstLineScreenPositionY.Y;
                double shorterLineXPos = (firstLineScreenPositionY.X + _rulerSize) / 2;

                // draw a longer line each 5 lines. The origin must be a longer line.
                int countShortLinesY = (int)(Math.Round((currentScreenY - _originScreen.Y) / stepScreen)) % 5;

                // Draw the ruler lines
                renderContext.SetLineSize(1);

                double left;

                bool yReverted =
                    !(((_axisMode == AxisOrientation.LeftBottom || _axisMode == AxisOrientation.RightBottom) && (!isMirrored))
                      ||
                      ((_axisMode == AxisOrientation.LeftTop || _axisMode == AxisOrientation.RightTop) && (isMirrored)));

                reverseFactor = yReverted ? -1 : 1;
                mirrorFactor = isMirrored ? -1 : 1;


                if (!yReverted)
                {
                    for (double y = startYWorld; y < _upperRightWorldCoord.Y; y += stepWorld, currentScreenY += stepScreen)
                    {
                        if (countShortLinesY % 5 == 0)
                            left = firstLineScreenPositionY.X;
                        else
                            left = shorterLineXPos; ;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(left, currentScreenY), new Point2D(_rulerSize, currentScreenY));

                        DrawText((int)firstLineScreenPositionY.X, (int)currentScreenY, string.Format(formatString, y / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesY++;
                    }
                }
                else
                {
                    for (double y = startYWorld; y > _upperRightWorldCoord.Y; y -= stepWorld, currentScreenY -= stepScreen)
                    {
                        if (countShortLinesY % 5 == 0)
                            left = firstLineScreenPositionY.X;
                        else
                            left = shorterLineXPos; ;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(left, currentScreenY), new Point2D(_rulerSize, currentScreenY));

                        DrawText((int)firstLineScreenPositionY.X, (int)currentScreenY, string.Format(formatString, y / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesY++;
                    }
                }



                ///////////////////////////
                // Horizontal ruler
                ///////////////////////////

                // First line X world coordinate
                double startXWorld = stepWorld *
                    (isRotated
                    ? Math.Ceiling(_lowerLeftWorldCoord.Y / stepWorld)
                    : Math.Ceiling(_lowerLeftWorldCoord.X / stepWorld));

                //Point2D firstLineScreenPositionX = isRotated
                //            ? new Point2D(_originScreen.Y + startXWorld * worldToScreenForX, linesLeftPosition)
                //            : new Point2D(_originScreen.X + startXWorld * worldToScreenForX, linesLeftPosition);
                Point2D firstLineScreenPositionX = isRotated
                            ? new Point2D(_originScreen.Y + startXWorld * worldToScreenForX, linesLeftPosition)
                            : new Point2D(_originScreen.X + startXWorld * worldToScreenForX, linesLeftPosition);

                bool xReverted =
                    !(((_axisMode == AxisOrientation.LeftBottom || _axisMode == AxisOrientation.LeftTop) && (!isMirrored))
                      ||
                      ((_axisMode == AxisOrientation.RightBottom || _axisMode == AxisOrientation.RightTop) && (isMirrored)));

                double currentScreenX = !isMirrored ? firstLineScreenPositionX.X : _originScreen.X - startXWorld * worldToScreenForX;

                double shorterLineYPos = (firstLineScreenPositionX.Y + _rulerSize) / 2;

                int countShortLinesX = (int)(Math.Round((currentScreenX - _originScreen.X) / stepScreenForX)) % 5;

                double top;


                reverseFactor = xReverted ? -1 : 1;
                mirrorFactor = isMirrored ? -1 : 1;

                if (!xReverted)
                {
                    stepScreenForX = stepScreenForX * mirrorFactor;
                    for (double x = startXWorld; x < _upperRightWorldCoord.X; x += stepWorld, currentScreenX += stepScreenForX)
                    {
                        if (countShortLinesX % 5 == 0)
                            top = firstLineScreenPositionX.Y;
                        else
                            top = shorterLineYPos;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(currentScreenX, Height - top), new Point2D(currentScreenX, Height - _rulerSize));

                        DrawText((int)currentScreenX, (int)(Height - _rulerSize - firstLineScreenPositionX.Y), string.Format(formatString, x / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesX++;
                    }
                }
                else

                {
                    startXWorld = stepWorld * Math.Floor(_lowerLeftWorldCoord.X / stepWorld);
                    firstLineScreenPositionX = new Point2D(_originScreen.X - startXWorld * worldToScreenForX * mirrorFactor, linesLeftPosition);
                    currentScreenX = firstLineScreenPositionX.X;
                    shorterLineYPos = (firstLineScreenPositionX.Y + _rulerSize) / 2;
                    countShortLinesX = (int)(Math.Round((currentScreenX - _originScreen.X) / stepScreenForX)) % 5;

                    for (double x = startXWorld; x > _upperRightWorldCoord.X; x -= stepWorld, currentScreenX += stepScreenForX * mirrorFactor)
                    {
                        if (countShortLinesX % 5 == 0)
                            top = firstLineScreenPositionX.Y;
                        else
                            top = shorterLineYPos;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(currentScreenX, Height - top), new Point2D(currentScreenX, Height - _rulerSize));

                        DrawText((int)currentScreenX, (int)(Height - _rulerSize - firstLineScreenPositionX.Y), string.Format(formatString, x / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesX++;
                    }
                }


            }
            #endregion

            else

            #region Rotated (horizontal = Y, Vertical = X

            {
                #region Vertical ruler
                ///////////////////////////
                // Vertical ruler
                ///////////////////////////

                bool yReverted = (_upperRightWorldCoord.X - _lowerLeftWorldCoord.X) < 0;

                // First line Y world coordinate
                double startYWorld = stepWorld * Math.Floor(_lowerLeftWorldCoord.X / stepWorld);

                Point2D firstLineScreenPositionY = new Point2D(linesLeftPosition, _originScreen.Y + startYWorld * worldToScreen);

                double currentScreenY = firstLineScreenPositionY.Y;

                double shorterLineXPos = (firstLineScreenPositionY.X + _rulerSize) / 2;

                // draw a longer line each 5 lines. The origin must be a longer line.
                int countShortLinesY = (int)(Math.Round((currentScreenY - _originScreen.Y) / stepScreen)) % 5;

                // Draw the ruler lines
                renderContext.SetLineSize(1);

                double left;


                reverseFactor = yReverted ? -1 : 1;
                mirrorFactor = isMirrored ? -1 : 1;


                if (!yReverted)
                {
                    for (double y = startYWorld; y < _upperRightWorldCoord.X; y += stepWorld, currentScreenY += stepScreen)
                    {
                        if (countShortLinesY % 5 == 0)
                            left = firstLineScreenPositionY.X;
                        else
                            left = shorterLineXPos; ;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(left, currentScreenY), new Point2D(_rulerSize, currentScreenY));

                        DrawText((int)firstLineScreenPositionY.X, (int)currentScreenY, string.Format(formatString, y / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesY++;
                    }
                }
                else
                {
                    for (double y = startYWorld; y > _upperRightWorldCoord.X; y -= stepWorld, currentScreenY -= stepScreen)
                    {
                        if (countShortLinesY % 5 == 0)
                            left = firstLineScreenPositionY.X;
                        else
                            left = shorterLineXPos; ;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(left, currentScreenY), new Point2D(_rulerSize, currentScreenY));

                        DrawText((int)firstLineScreenPositionY.X, (int)currentScreenY, string.Format(formatString, y / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesY++;
                    }
                }

                #endregion

                #region Horizontal ruler


                ///////////////////////////
                // Horizontal ruler
                ///////////////////////////

                // First line X world coordinate
                double startXWorld = stepWorld * Math.Ceiling(_lowerLeftWorldCoord.Y / stepWorld);

                //Point2D firstLineScreenPositionX = isRotated
                //            ? new Point2D(_originScreen.Y + startXWorld * worldToScreenForX, linesLeftPosition)
                //            : new Point2D(_originScreen.X + startXWorld * worldToScreenForX, linesLeftPosition);

                bool xReverted = (_upperRightWorldCoord.Y - _lowerLeftWorldCoord.Y) < 0;

                worldToScreenForX = - Math.Abs(worldToScreen);


                Point2D firstLineScreenPositionX = new Point2D(_originScreen.X - startXWorld * worldToScreenForX, linesLeftPosition);

                //bool xReverted =
                //    !(((_axisMode == AxisOrientation.LeftBottom || _axisMode == AxisOrientation.RightBottom) && (!isMirrored))
                //      ||
                //      ((_axisMode == AxisOrientation.LeftTop || _axisMode == AxisOrientation.RightTop) && (isMirrored)));

                double currentScreenX = !xReverted ? firstLineScreenPositionX.X : _originScreen.X - startXWorld * worldToScreen;

                double shorterLineYPos = (firstLineScreenPositionX.Y + _rulerSize) / 2;

                int countShortLinesX = (int)(Math.Round((currentScreenX - _originScreen.X) / stepScreenForX)) % 5;

                double top;


                reverseFactor = xReverted ? -1 : 1;
                mirrorFactor = isMirrored ? -1 : 1;

                if (!xReverted)
                {
                    stepScreenForX = Math.Abs(stepScreenForX);
                    for (double x = startXWorld; x < _upperRightWorldCoord.Y; x += stepWorld, currentScreenX += stepScreenForX)
                    {
                        if (countShortLinesX % 5 == 0)
                            top = firstLineScreenPositionX.Y;
                        else
                            top = shorterLineYPos;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(currentScreenX, Height - top), new Point2D(currentScreenX, Height - _rulerSize));

                        DrawText((int)currentScreenX, (int)(Height - _rulerSize - firstLineScreenPositionX.Y), string.Format(formatString, x / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesX++;
                    }
                }
                else
                {
                    worldToScreenForX = Math.Abs(worldToScreen);
                    startXWorld = stepWorld * Math.Floor(_lowerLeftWorldCoord.Y / stepWorld);
                    stepScreenForX = Math.Abs(stepScreenForX);

                    firstLineScreenPositionX = new Point2D(_originScreen.X - startXWorld * worldToScreenForX , linesLeftPosition);

                    currentScreenX = firstLineScreenPositionX.X;
                    shorterLineYPos = (firstLineScreenPositionX.Y + _rulerSize) / 2;
                    countShortLinesX = (int)(Math.Round((currentScreenX - _originScreen.X) / stepScreenForX)) % 5;

                    for (double x = startXWorld; x > _upperRightWorldCoord.Y; x -= stepWorld, currentScreenX += stepScreenForX )
                    {
                        if (countShortLinesX % 5 == 0)
                            top = firstLineScreenPositionX.Y;
                        else
                            top = shorterLineYPos;

                        renderContext.SetColorWireframe(Color.White);

                        renderContext.DrawLine(new Point2D(currentScreenX, Height - top), new Point2D(currentScreenX, Height - _rulerSize));

                        DrawText((int)currentScreenX, (int)(Height - _rulerSize - firstLineScreenPositionX.Y), string.Format(formatString, x / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);

                        countShortLinesX++;
                    }
                }
                #endregion
            }
            
            #endregion




            // Draw a red line in correspondance with the mouse position
            Point mousePos = PointToClient(MousePosition);
            Point3D mouseWorldPos;
            ScreenToPlane(mousePos, Plane.XY, out mouseWorldPos);

            //Draw coordinates

            if (_showRulerCoordinates)
            {
                DrawText(Width - 85, 20, string.Format("X = " + coordinatesFormatString, mouseWorldPos.X / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);
                DrawText(Width - 85, 0, string.Format("Y = " + coordinatesFormatString, mouseWorldPos.Y / _umRatio), Font, Color.White, ContentAlignment.BottomLeft);
            }


            renderContext.SetColorWireframe(Color.Red);

            if (mousePos.Y > _rulerSize)
            {
                renderContext.DrawLine(new Point3D(linesLeftPosition, Height - mousePos.Y, 0), new Point3D(_rulerSize, Height - mousePos.Y, 0));
            }

            if (mousePos.X > _rulerSize)
            {
                renderContext.DrawLine(new Point3D(mousePos.X, Height - linesLeftPosition, 0), new Point3D(mousePos.X, Height - _rulerSize, 0));
            }

            renderContext.SetColorWireframe(currentWireframeColor);

        }


        protected override void OnMouseUp(MouseEventArgs e)
        {
            try
            {
                base.OnMouseUp(e);
                ProcessMouseUp();
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("TouchViewportLayout OnMouseUp error ", ex);
            }
        }

        

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void ProcessMouseUp()
        {
            if (_actionModeActive)
            {
                // Check if mouseUp on button click (Eyeshot triggers mouseUp anyway!)
                if (_actionModeJustStarted)
                {
                    _actionModeJustStarted = false;
                }
                else
                {
                    EndActionMode();
                }
            }
        }

        private void EndActionMode()
        {
            if (_actionModeActive)
            {
                base.ActionMode = _defaultActionType;
                if (_activeButton != null && _activeButton.StyleMode == toolBarButtonStyleType.ToggleButton && _activeButton.Pushed)
                {
                    _activeButton.Pushed = false;
                    base.Viewports[0].Invalidate(); // to refresh toolbar
                    _activeButton = null;
                }
                _actionModeActive = false;
            }
        }


        private void BuildToolbar()
        {
            List<ToolBarButton> buttons = new List<ToolBarButton>();

            //ToolBarButton button = CreateToggleButton("Rotate", BretonViewportLayout.Properties.Resources.rotazione2, BretonViewportLayout.Properties.Resources.rotazione2B);
            //button.Click += button_Click;
            //_buttonAction.Add(button, actionType.Rotate);

            ToolBarButton button = CreateToggleButton("Pan", BretonViewportLayout.Properties.Resources.Pan, BretonViewportLayout.Properties.Resources.PanB);
            button.Click += button_Click;
            _buttonAction.Add(button, actionType.Pan);

            button = CreatePushButton("ZoomFit", BretonViewportLayout.Properties.Resources.ZoomFit, BretonViewportLayout.Properties.Resources.ZoomFitB);
            button.Click += button_Click;
            _buttonAction.Add(button, actionType.None);

            button = CreateToggleButton("ZoomWindow", BretonViewportLayout.Properties.Resources.ZoomSele, BretonViewportLayout.Properties.Resources.ZoomSeleB);
            button.Click += button_Click;
            _buttonAction.Add(button, actionType.ZoomWindow);

            button = CreateToggleButton("ZoomDynamic", BretonViewportLayout.Properties.Resources.ZoomRotella, BretonViewportLayout.Properties.Resources.ZoomRotellaB);
            button.Click += button_Click;
            _buttonAction.Add(button, actionType.Zoom);

            base.ToolBar.Buttons = _buttonAction.Keys.ToArray();

            ////base.ToolBar.ButtonClick += ToolBar_ButtonClick;


        }

        void button_Click(object sender, EventArgs e)
        {
            ToolBarButton button = sender as ToolBarButton;
            if (button == null) return;

            if (_actionModeActive)
            {
                base.ActionMode = _defaultActionType;
                _actionModeActive = false;
            }
            if (_activeButton != null && _activeButton.StyleMode == toolBarButtonStyleType.ToggleButton)
            {
                _activeButton.Pushed = false;
            }

            if (_activeButton != null && _activeButton == button)
            {
                _activeButton = null;
                return;
            }

            _activeButton = null;


            if (button.StyleMode.Equals(toolBarButtonStyleType.ToggleButton) && button.Pushed)
            {
                actionType action = _buttonAction[button];
                _activeButton = button;
                base.ActionMode = action;
                _actionModeActive = true;
                _actionModeJustStarted = true;

            }
            else if (button.StyleMode.Equals(toolBarButtonStyleType.PushButton))
            {
                switch (button.Name)
                {
                    case "ZoomFit":
                        base.Viewports[0].ZoomFit();
                        base.Viewports[0].Invalidate();
                        break;
                }
            }
        }

        //void ToolBar_ButtonClick(object sender, devDept.Eyeshot.ToolBarButtonClickEventArgs e)
        //{
        //    if (_actionModeActive)
        //    {
        //        base.ActionMode = _defaultActionType;
        //        _actionModeActive = false;
        //    }
        //    if (_activeButton != null && _activeButton.StyleMode == toolBarButtonStyleType.ToggleButton)
        //    {
        //        _activeButton.Pushed = false;
        //    }

        //    if (_activeButton != null && _activeButton == e.Button)
        //    {
        //        _activeButton = null;
        //        return;
        //    }

        //    _activeButton = null;


        //    if (e.Button.StyleMode.Equals(toolBarButtonStyleType.ToggleButton) && e.Button.Pushed)
        //    {
        //        actionType action = _buttonAction[e.Button];
        //        _activeButton = e.Button;
        //        base.ActionMode = action;
        //        _actionModeActive = true;
        //        _actionModeJustStarted = true;

        //    }
        //    else if (e.Button.StyleMode.Equals(toolBarButtonStyleType.PushButton))
        //    {
        //        switch (e.Button.Name)
        //        {
        //            case "ZoomFit":
        //                base.Viewports[0].ZoomFit();
        //                base.Viewports[0].Invalidate();
        //                break;
        //        }
        //    }


        //}


        public void SetupUserInterface(bool showRuler = true, bool showRulerCoordinates = true)
        {
            if (_useTouchLayout)
            {
                Color faceColor = Color.DimGray;
                Color highlightColor = Color.FromArgb(41, 212, 229);
                Color edgeColor = Color.Black;

                base.ViewportBorder.Visible = false;
                base.Margin = new Padding(0);

                base.Viewports[0].ToolBar.Position = toolBarPositionType.VerticalMiddleRight;

                //base.Viewports[0].ViewCubeIcon.BackFaceColor =
                //    base.Viewports[0].ViewCubeIcon.FrontFaceColor =
                //        base.Viewports[0].ViewCubeIcon.TopFaceColor =
                //            base.Viewports[0].ViewCubeIcon.RightFaceColor =
                //                base.Viewports[0].ViewCubeIcon.LeftFaceColor =
                //                    base.Viewports[0].ViewCubeIcon.BottomFaceColor = faceColor;

                //base.Viewports[0].ViewCubeIcon.HighlightColor = highlightColor;
                //base.Viewports[0].ViewCubeIcon.EdgeColor = edgeColor;
                //base.Viewports[0].ViewCubeIcon.Position = coordinateSystemPositionType.BottomRight;

                base.Viewports[0].Background.Style = backgroundStyleType.LinearGradient;
                base.Viewports[0].Background.TopColor = Color.Black;
                base.Viewports[0].Background.IntermediateColor = Color.FromArgb(40, 40, 40);
                base.Viewports[0].Background.IntermediateColorPosition = .50;
                base.Viewports[0].Background.BottomColor = Color.FromArgb(127, 127, 127);

                base.Viewports[0].Camera.ProjectionMode = projectionType.Orthographic;
                base.SelectionColor = Color.White;


                base.ButtonStyle.HighlightColor = highlightColor;
                base.ButtonStyle.Size = 48;

                base.Viewports[0].Grid.MajorLinesEvery = 10;
                base.Viewports[0].Grid.Visible = true;
                base.Viewports[0].Grid.LineColor = Color.FromArgb(100, Color.LightCyan.R, Color.LightCyan.G, Color.LightCyan.B);
                base.Viewports[0].Grid.MajorLineColor = Color.LightCyan;
                base.Viewports[0].Grid.ColorAxisX = Color.Cyan;
                base.Viewports[0].Grid.ColorAxisY = Color.Cyan;
                base.Viewports[0].Grid.Step = 100;
                base.Viewports[0].Grid.AlwaysBehind = true;
                base.Viewports[0].Grid.Max = new Point2D(6000, 3000);
                base.Viewports[0].Grid.Min = new Point2D(-1000, -1000);
                base.Viewports[0].Grid.AutoStep = false;

                base.Light1.Color = Color.White;
                base.Light1.Direction = new Vector3D(1, 1, 0);

                BuildToolbar();
                base.CompileUserInterfaceElements();
                base.ToolBar.Visible = true;

            }

            _showRuler = showRuler;
            _showRulerCoordinates = showRulerCoordinates;
            if (_showRuler)
                Zoom.FitMargin = (int)(_rulerSize * 1.1);
        }



        private static ToolBarButton CreateToggleButton(string name, Image defaultImage, Image pressedImage)
        {
            ToolBarButton button = new ToolBarButton();
            button.Name = name;
            button.ToolTipText = button.Name;
            button.StyleMode = toolBarButtonStyleType.ToggleButton;
            button.Image = defaultImage;
            button.DisabledDownImage = pressedImage;
            button.DownImage = pressedImage;
            button.HoverImage = pressedImage;
            button.Enabled = true;
            return button;
        }

        private static ToolBarButton CreatePushButton(string name, Image defaultImage, Image pressedImage)
        {
            ToolBarButton button = new ToolBarButton();
            button.Name = name;
            button.ToolTipText = button.Name;
            button.StyleMode = toolBarButtonStyleType.PushButton;
            button.Image = defaultImage;
            button.DownImage = pressedImage;
            button.HoverImage = pressedImage;
            button.Enabled = true;
            return button;
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
