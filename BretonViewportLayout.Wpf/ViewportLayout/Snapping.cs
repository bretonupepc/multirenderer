using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Drawing;
using Breton.MathUtils;
using Breton.Polygons;
using devDept.Geometry;
using devDept.Eyeshot.Entities;
using devDept.Graphics;
using Arc = devDept.Eyeshot.Entities.Arc;
using Ellipse = devDept.Eyeshot.Entities.Ellipse;
using Point = Breton.Polygons.Point;

namespace Breton.DesignCenterInterface
{
    /// <summary>
    /// Contains utilities required for grid snapping or model vertex snapping.
    /// </summary>
    partial class DraftingViewportLayout
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private List<ObjectSnapType> _snapModes = null;

        private List<string> _snapLayers = null;
        // Current snapped point, which is one of the vertex from model
        private Point3D snapPoint = null;

        private int _snapBoxSize = 20;

        private Color _snapSymbolColor = Color.Red;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields

        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [Browsable(false)]
        public bool ObjectSnapEnabled { get; set; }

        [Browsable(false)]
        public bool GridSnapEnabled { get; set; }

        [Browsable(false)]
        public bool WaitingForSelection { get; set; }


        [Browsable(false)]
        [DefaultValue(null)]
        public List<ObjectSnapType> SnapModes
        {
            get
            {
                if(_snapModes == null)
                    _snapModes = new List<ObjectSnapType>();
                return _snapModes;
            }
            set { _snapModes = value; }
        }

        [Browsable(false)]
        [DefaultValue(null)]
        public List<string> SnapLayers
        {
            get
            {
                if (_snapLayers == null)
                    _snapLayers = new List<string>();
                return _snapLayers;
            }
            set { _snapLayers = value; }
        }

        public Color SnapSymbolColor
        {
            get { return _snapSymbolColor; }
            set { _snapSymbolColor = value; }
        }

        public int SnapBoxSize
        {
            get { return _snapBoxSize; }
            set { _snapBoxSize = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public SnapPoint FindClosestPoint(List<SnapPoint> snapPoints, System.Drawing.Point mousePosition)
        {
            _lastSnapPoint = null;
            if (snapPoints.Count == 0)
                return null;

            double minDist = double.MaxValue;

            int i = 0;
            int index = 0;

            for (i=0; i< snapPoints.Count; i++)
            {
                SnapPoint vertex = snapPoints[i];
                Point3D vertexScreen = WorldToScreen(vertex);
                Point2D currentScreen = new Point2D(mousePosition.X, Height - mousePosition.Y);

                double dist = Point2D.Distance(vertexScreen, currentScreen);

                if (dist < minDist)
                {
                    index = i;
                    minDist = dist;
                }
            }

            //SnapPoint closestPoint = snapPoints[index];
            _lastSnapPoint = snapPoints[index];
            //DisplaySnappedVertex(_lastSnapPoint);

            return _lastSnapPoint;
        }

        
        /// <summary>
        /// Displays symbols associated with the snapped vertex type
        /// </summary>
        public void DisplaySnappedVertex(SnapPoint snapP)
        {
            renderContext.SetLineSize(2);

            Color originalContextColor = renderContext.CurrentWireColor;

            renderContext.SetColorWireframe(_snapSymbolColor);
            renderContext.SetState(depthStencilStateType.DepthTestOff);

            Point2D onScreen = WorldToScreen(snapP);

            this.snapPoint = snapP;

            switch (snapP.Type)
            {
                case ObjectSnapType.Point:
                    DrawCircle(new System.Drawing.Point((int)onScreen.X, (int)(onScreen.Y)));
                    DrawCross(new System.Drawing.Point((int)onScreen.X, (int)(onScreen.Y)));
                    break;

                case ObjectSnapType.Near:
                    DrawHourglass(new System.Drawing.Point((int)onScreen.X, (int)(onScreen.Y)));
                    break;

                case ObjectSnapType.Center:
                    DrawCircle(new System.Drawing.Point((int)onScreen.X, (int)(onScreen.Y)));
                    break;

                case ObjectSnapType.End:
                    DrawQuad(new System.Drawing.Point((int)onScreen.X, (int)(onScreen.Y)));
                    break;

                case ObjectSnapType.Mid:
                    DrawTriangle(new System.Drawing.Point((int)onScreen.X, (int)(onScreen.Y)));
                    break;

            }

            renderContext.SetColorWireframe(originalContextColor);
            renderContext.SetLineSize(1);            
        }

        /// <summary>
        /// identify snapPoints of the entity under mouse cursor in that moment, using PickBoxSize as tolerance
        /// </summary>
        public List<SnapPoint> GetSnapPoints(System.Drawing.Point mousePosition)
        {
            //changed PickBoxSize to define a range for display snapPoints
            int oldSize = PickBoxSize;
            PickBoxSize = SnapBoxSize;

            //select the entity under mouse cursor
            //int index = GetEntityUnderMouseCursor(mouseLocation);
            Point3D pointOnPlane;
            ScreenToPlane(mousePosition, Plane.XY, out pointOnPlane);

            var entitiesIndexes = GetEntitiesUnderCursor(mousePosition);
            //Rectangle selectionRect = new Rectangle(mousePosition.X - PickBoxSize / 2, mousePosition.Y - PickBoxSize / 2, PickBoxSize, PickBoxSize); 

            //var entitiesIndexes = GetAllCrossingEntities(selectionRect);

            PickBoxSize = oldSize;

            _lastSnapPoint = null;

            List<SnapPoint> validPoints = new List<SnapPoint>();

            if (entitiesIndexes.Length > 0)
            {
                foreach (var index in entitiesIndexes)
                {
                    Entity ent = Entities[index];

                    if (_snapLayers != null)
                    {
                        if (!_snapLayers.Contains(Layers[ent.LayerIndex].Name)
                            || !Layers[ent.LayerIndex].Visible)
                        {
                            continue;
                        }
                    }

                    //check wich type of entity is it and then,identify snap points
                    if (ent is devDept.Eyeshot.Entities.Point)
                    {
                        devDept.Eyeshot.Entities.Point point = (devDept.Eyeshot.Entities.Point) ent;

                        foreach(var snapMode in _snapModes)
                            switch (snapMode)
                            {
                                case ObjectSnapType.Point:                    
                                    Point3D point3d = point.Vertices[0];
                                    validPoints.Add(new SnapPoint(point3d, ObjectSnapType.Point));
                                    break;
                            }
                    } 
                    else if (ent is Line) //line
                    {
                        Line line = (Line) ent;

                        foreach (var snapMode in _snapModes)
                        {
                            switch (snapMode)
                            {
                                case ObjectSnapType.End:
                                    validPoints.Add(new SnapPoint(line.StartPoint, ObjectSnapType.End));
                                    validPoints.Add(new SnapPoint(line.EndPoint, ObjectSnapType.End));

                                    break;
                                case ObjectSnapType.Mid:
                                    validPoints.Add(new SnapPoint(line.MidPoint, ObjectSnapType.Mid));
                                    break;

                                case ObjectSnapType.Near:
                                    // Calcola la proiezione del punto sul segmento per trovare il punto interessato
                                    Point3D n = new Point3D();
                                    double x, y, a, b, c;
                                    Segment seg = new Segment(line.StartPoint.X, line.StartPoint.Y, line.EndPoint.X,
                                        line.EndPoint.Y);
                                    seg.StraightLine(out a, out b, out c);
                                    MathUtil.ProjectedPointToStraight(a, b, c, pointOnPlane.X, pointOnPlane.Y, out x,
                                        out y);
                                    if (seg.IsInSide(new Point(x, y)))
                                    {
                                        validPoints.Add(new SnapPoint(new Point3D(x, y), ObjectSnapType.Near));
                                    }
                                    break;

                            }
                        }
                    }
                    else if (ent is LinearPath)//polyline
                    {
                        LinearPath polyline = (LinearPath) ent;

                        foreach (var snapMode in _snapModes)
                        {
                            switch (snapMode)
                            {
                                case ObjectSnapType.End:
                                    foreach (Point3D point in polyline.Vertices)
                                    {
                                        validPoints.Add(new SnapPoint(point, ObjectSnapType.End));
                                    }
                                    break;
                            }
                        }
                    }
                    else if (ent is CompositeCurve)//composite
                    {
                        CompositeCurve composite = (CompositeCurve)ent;

                        foreach (var snapMode in _snapModes)
                        {
                            switch (snapMode)
                            {
                                case ObjectSnapType.End:
                                    foreach (ICurve curveSeg in composite.CurveList)
                                        validPoints.Add(new SnapPoint(curveSeg.EndPoint, ObjectSnapType.End));
                                    validPoints.Add(new SnapPoint(composite.CurveList[0].StartPoint, ObjectSnapType.End));

                                    break;
                            }
                        }
                    }
                    else if (ent is Arc) //Arc
                    {
                        Arc arc = (Arc) ent;

                        foreach (var snapMode in _snapModes)
                        {
                            switch (snapMode)
                            {
                                case ObjectSnapType.End:
                                    validPoints.Add(new SnapPoint(arc.StartPoint, ObjectSnapType.End));
                                    validPoints.Add(new SnapPoint(arc.EndPoint, ObjectSnapType.End));
                                    break;

                                case ObjectSnapType.Mid:
                                    validPoints.Add(new SnapPoint(arc.MidPoint, ObjectSnapType.Mid));
                                    break;

                                case ObjectSnapType.Center:
                                    validPoints.Add(new SnapPoint(arc.Center, ObjectSnapType.Center));
                                    break;

                                case ObjectSnapType.Near:
                                    // Crea un segmento che va dal centro dell'arco al punto del mouse e lo estende per intersecare l'arco toccato
                                    Point[] intersectionPoint;
                                    Breton.Polygons.Arc bretonArc = new Breton.Polygons.Arc(arc.StartPoint.X, arc.StartPoint.Y, arc.MidPoint.X, arc.MidPoint.Y, arc.EndPoint.X, arc.EndPoint.Y);
                                    Segment seg = new Segment(pointOnPlane.X, pointOnPlane.Y, arc.Center.X, arc.Center.Y);
                                    seg.Extend(Side.EN_VERTEX.EN_VERTEX_START, 10000);
                                    seg.Intersect(bretonArc, out intersectionPoint);

                                    if (intersectionPoint.Length > 0) // sempre vero, in realt�...
                                    {
                                        if (intersectionPoint[0] != null)
                                            validPoints.Add(new SnapPoint(new Point3D(intersectionPoint[0].X, intersectionPoint[0].Y), ObjectSnapType.Near));
                                        else if (intersectionPoint.Length > 1 && (intersectionPoint[1] != null))
                                        {
                                            validPoints.Add(
                                                new SnapPoint(
                                                    new Point3D(intersectionPoint[1].X, intersectionPoint[1].Y),
                                                    ObjectSnapType.Near));
                                        }
                                        else // il pi� vicino tra gli estremi
                                        {
                                            if (pointOnPlane.DistanceTo(arc.StartPoint) >
                                                pointOnPlane.DistanceTo(arc.EndPoint))
                                            {
                                                validPoints.Add(
                                                    new SnapPoint(
                                                        new Point3D(arc.EndPoint.X, arc.EndPoint.Y),
                                                        ObjectSnapType.Near));
                                            }
                                            else
                                            {
                                                validPoints.Add(
                                                    new SnapPoint(
                                                        new Point3D(arc.StartPoint.X, arc.StartPoint.Y),
                                                        ObjectSnapType.Near));
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    else if (ent is Circle) //Circle
                    {
                        Circle circle = (Circle) ent;

                        foreach (var snapMode in _snapModes)
                        {
                            switch (snapMode)
                            {
                                case ObjectSnapType.End:
                                    validPoints.Add(new SnapPoint(circle.EndPoint, ObjectSnapType.End));
                                    break;

                                case ObjectSnapType.Mid:
                                    validPoints.Add(new SnapPoint(circle.PointAt(circle.Domain.Mid), ObjectSnapType.Mid));
                                    break;

                                case ObjectSnapType.Center:
                                    validPoints.Add(new SnapPoint(circle.Center, ObjectSnapType.Center));
                                    break;

                            }
                        }
                    }

                    else if (ent is EllipticalArc) //Elliptical Arc
                    {
                        EllipticalArc elArc = (EllipticalArc) ent;

                        foreach (var snapMode in _snapModes)
                        {
                            switch (snapMode)
                            {
                                case ObjectSnapType.End:
                                    validPoints.Add(new SnapPoint(elArc.StartPoint, ObjectSnapType.End));
                                    validPoints.Add(new SnapPoint(elArc.EndPoint, ObjectSnapType.End));
                                    break;

                                case ObjectSnapType.Center:
                                    validPoints.Add(new SnapPoint(elArc.Center, ObjectSnapType.Center));
                                    break;
                            }
                        }
                    }
                    else if (ent is Ellipse) //Ellipse
                    {
                        Ellipse ellipse = (Ellipse) ent;

                        foreach (var snapMode in _snapModes)
                        {
                            switch (snapMode)
                            {
                                case ObjectSnapType.End:
                                    validPoints.Add(new SnapPoint(ellipse.EndPoint, ObjectSnapType.End));
                                    break;
                                case ObjectSnapType.Mid:
                                    validPoints.Add(new SnapPoint(ellipse.PointAt(ellipse.Domain.Mid), ObjectSnapType.Mid));
                                    break;
                                case ObjectSnapType.Center:
                                    validPoints.Add(new SnapPoint(ellipse.Center, ObjectSnapType.Center));
                                    break;
                            }
                        }
                    }
                }  
            }

            return validPoints.OrderBy(p=>(int)p.Type).ToList();
        }

        public int[] GetEntitiesUnderCursor(System.Drawing.Point mousePosition)
        {
            var entitiesIndexes = GetAllEntitiesUnderMouseCursor(mousePosition);
            if (entitiesIndexes.Length > 0)
            {
                Rectangle selectionRect = new Rectangle(mousePosition.X - PickBoxSize / 2, mousePosition.Y - PickBoxSize / 2, PickBoxSize, PickBoxSize);

                entitiesIndexes = GetAllCrossingEntities(selectionRect);
            }

            return entitiesIndexes;

        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        /// <summary>
        /// Adds entity to scene on active layer and refresh the screen. 
        /// </summary>
        private void AddAndRefresh(Entity entity, int layerIndex)
        {
            // increase dimension of points
            if(entity is devDept.Eyeshot.Entities.Point)
            {
                entity.LineWeightMethod = colorMethodType.byEntity;
                entity.LineWeight = 3;
            }

            // avoid dimensions with width bigger than one
            if (entity is Dimension)
            {
                entity.LayerIndex = layerIndex;
                entity.LineWeightMethod = colorMethodType.byEntity;

                Entities.Add(entity);
            }
            else
            {
                Entities.Add(entity, layerIndex);
            }

            Entities.Regen();
            Invalidate();
        }

        /// <summary>
        /// Tries to snap grid vertex for the current mouse point
        /// </summary>
        private bool SnapToGrid(ref Point3D ptToSnap)
        {
            Point2D gridPoint = new Point2D(Math.Round(ptToSnap.X / 10) * 10, Math.Round(ptToSnap.Y / 10) * 10);

            if (Point2D.Distance(gridPoint, ptToSnap) < magnetRange)
            {
                ptToSnap.X = gridPoint.X;
                ptToSnap.Y = gridPoint.Y;

                return true;
            }

            return false;
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        // Flags to indicate current snapping mode

        #region SnappingData

        /// <summary>
        /// Represents a 3D point from model vertices and associated snap type.
        /// </summary>
        public class SnapPoint : devDept.Geometry.Point3D
        {
            public ObjectSnapType Type;

            public SnapPoint()
                : base()
            {
                Type = ObjectSnapType.End;
            }
            
            public SnapPoint(Point3D point3D, ObjectSnapType objectSnapType) : base(point3D.X, point3D.Y, point3D.Z)
            {             
                this.Type = objectSnapType;
            }

            public override string ToString()
            {
                return base.ToString() + " | " + Type;
            }
        }
        #endregion

        #region Snapping symbols

        private  int SNAP_SYMBOL_SIZE = 20;
        public readonly  int RESIZER_SYMBOL_SIZE = 16;

        /// <summary>
        /// Draw cross. Drawn with a circle identifies a single point
        /// </summary>
        private void DrawCross(System.Drawing.Point onScreen)
        {
            double dim1 = onScreen.X + (SNAP_SYMBOL_SIZE / 2);
            double dim2 = onScreen.Y + (SNAP_SYMBOL_SIZE / 2);
            double dim3 = onScreen.X - (SNAP_SYMBOL_SIZE / 2);
            double dim4 = onScreen.Y - (SNAP_SYMBOL_SIZE / 2);

            Point3D topLeftVertex   = new Point3D(dim3, dim2);
            Point3D topRightVertex  = new Point3D(dim1, dim2);
            Point3D bottomRightVertex   = new Point3D(dim1, dim4);
            Point3D bottomLeftVertex    = new Point3D(dim3, dim4);

            var currentColor = renderContext.CurrentWireColor;
            renderContext.SetColorWireframe(_snapSymbolColor);

            renderContext.DrawLines(
                new Point3D[]
                {
                    bottomLeftVertex,
                    topRightVertex,

                    topLeftVertex,
                    bottomRightVertex,

                });

            renderContext.SetColorWireframe(currentColor);

        }

        /// <summary>
        /// Draw hourglass. Identifies a near point
        /// </summary>
        /// <param name="onScreen"></param>
        private void DrawHourglass(System.Drawing.Point onScreen)
        {
            double dim1 = onScreen.X + (SNAP_SYMBOL_SIZE / 2);
            double dim2 = onScreen.Y + (SNAP_SYMBOL_SIZE / 2);
            double dim3 = onScreen.X - (SNAP_SYMBOL_SIZE / 2);
            double dim4 = onScreen.Y - (SNAP_SYMBOL_SIZE / 2);

            Point3D topLeftVertex = new Point3D(dim3, dim4);
            Point3D topRightVertex = new Point3D(dim1, dim4);
            Point3D bottomRightVertex = new Point3D(dim1, dim2);
            Point3D bottomLeftVertex = new Point3D(dim3, dim2);


            var currentColor = renderContext.CurrentWireColor;
            renderContext.SetColorWireframe(_snapSymbolColor);

            renderContext.DrawLines(
                new Point3D[]
                {
                    topLeftVertex,
                    topRightVertex,
                    topRightVertex,
                    bottomLeftVertex,
                    bottomLeftVertex,
                    bottomRightVertex,
                    bottomRightVertex,
                    topLeftVertex
                });

            renderContext.SetColorWireframe(currentColor);

        }

        /// <summary>
        /// Draw circle with openGl to rapresent CENTER snap point
        /// </summary>
        private void DrawCircle(System.Drawing.Point onScreen)
        {
            double radius = SNAP_SYMBOL_SIZE /2; 

            double x2 = 0, y2 = 0;

            List<Point3D> pts = new List<Point3D>();

            for (int angle = 0; angle < 360; angle += 10)
            {
                double rad_angle = Utility.DegToRad(angle);

                x2 = onScreen.X + radius * Math.Cos(rad_angle);
                y2 = onScreen.Y + radius * Math.Sin(rad_angle);

                Point3D circlePoint = new Point3D(x2, y2);
                pts.Add(circlePoint);
            }

            renderContext.DrawLineLoop(pts.ToArray());
        }


        /// <summary>
        /// Draw quad with openGl to rapresent END snap point
        /// </summary>
        private void DrawQuad(System.Drawing.Point onScreen)
        {
            double dim1 = onScreen.X + (SNAP_SYMBOL_SIZE / 2);
            double dim2 = onScreen.Y + (SNAP_SYMBOL_SIZE / 2);
            double dim3 = onScreen.X - (SNAP_SYMBOL_SIZE / 2);
            double dim4 = onScreen.Y - (SNAP_SYMBOL_SIZE / 2);

            Point3D topLeftVertex = new Point3D(dim3, dim2);
            Point3D topRightVertex = new Point3D(dim1, dim2);
            Point3D bottomRightVertex = new Point3D(dim1, dim4);
            Point3D bottomLeftVertex = new Point3D(dim3, dim4);

            var currentColor = renderContext.CurrentWireColor;
            renderContext.SetColorWireframe(_snapSymbolColor);

            renderContext.DrawLineLoop(new Point3D[]
            {
                bottomLeftVertex,
                bottomRightVertex,
                topRightVertex,
                topLeftVertex
            });

            renderContext.SetColorWireframe(currentColor);
        }

        /// <summary>
        /// Draw triangle with openGl to rapresent MID snap point
        /// </summary>
        private void DrawTriangle(System.Drawing.Point onScreen)
        {
            double dim1 = onScreen.X + (SNAP_SYMBOL_SIZE / 2);
            double dim2 = onScreen.Y + (SNAP_SYMBOL_SIZE / 2);
            double dim3 = onScreen.X - (SNAP_SYMBOL_SIZE / 2);
            double dim4 = onScreen.Y - (SNAP_SYMBOL_SIZE / 2);
            double dim5 = onScreen.X;

            Point3D topCenter = new Point3D(dim5, dim2);

            Point3D bottomRightVertex = new Point3D(dim1, dim4);
            Point3D bottomLeftVertex  = new Point3D(dim3, dim4);

            renderContext.DrawLineLoop(new Point3D[]
            {
                bottomLeftVertex,
                bottomRightVertex,
                topCenter
            });
        }


        private void DrawRhombus(System.Drawing.Point onScreen)
        {
            double dim1 = onScreen.X + (SNAP_SYMBOL_SIZE / 1.5);
            double dim2 = onScreen.Y + (SNAP_SYMBOL_SIZE / 1.5);
            double dim3 = onScreen.X - (SNAP_SYMBOL_SIZE / 1.5);
            double dim4 = onScreen.Y - (SNAP_SYMBOL_SIZE / 1.5);

            Point3D topVertex    = new Point3D(onScreen.X, dim2);
            Point3D bottomVertex = new Point3D(onScreen.X, dim4);
            Point3D rightVertex  = new Point3D(dim1, onScreen.Y);
            Point3D leftVertex   = new Point3D(dim3, onScreen.Y);

            renderContext.DrawLineLoop(new Point3D[]
            {
                bottomVertex,
                rightVertex,
                topVertex,
                leftVertex,
            });
        }

        #endregion
    }

}