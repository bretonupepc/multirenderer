﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Wpf.Presentation.UserControls.BUTTONS
{
    public class ImageThreeStateButton : Button
    {
        static ImageThreeStateButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof (ImageThreeStateButton),
                new FrameworkPropertyMetadata(typeof (ImageThreeStateButton)));
        }

        #region Dependency Properties

        public double ImageSize
        {
            get { return (double) GetValue(ImageSizeProperty); }
            set
            {
                SetValue(ImageSizeProperty, value);
            }
        }

        public static readonly DependencyProperty ImageSizeProperty =
            DependencyProperty.Register("ImageSize", typeof (double), typeof (ImageThreeStateButton),
                new FrameworkPropertyMetadata(32.0, FrameworkPropertyMetadataOptions.AffectsRender));

        public ImageSource NormalImage
        {
            get
            {
                return (ImageSource)GetValue(NormalImageProperty);
            }
            set
            {
                SetValue(NormalImageProperty, value);
            }
        }

        public static readonly DependencyProperty NormalImageProperty =
            DependencyProperty.Register("NormalImage", typeof(ImageSource), typeof(ImageThreeStateButton),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender, ImageSourceChanged));

        public ImageSource HoverImage
        {
            get
            {
                //var hover = (ImageSource) GetValue(HoverImageProperty);
                //var normal = (ImageSource) GetValue(NormalImageProperty);
                return (ImageSource)GetValue(HoverImageProperty);
            }
            set { SetValue(HoverImageProperty, value); }
        }

        public static readonly DependencyProperty HoverImageProperty =
            DependencyProperty.Register("HoverImage", typeof(ImageSource), typeof(ImageThreeStateButton),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        public ImageSource PressedImage
        {
            get { return (ImageSource)GetValue(PressedImageProperty) ; }
            set { SetValue(PressedImageProperty, value); }
        }

        public static readonly DependencyProperty PressedImageProperty =
            DependencyProperty.Register("PressedImage", typeof(ImageSource), typeof(ImageThreeStateButton),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        private static object DefaultToNormal(DependencyObject d, object baseValue)
        {
            if (baseValue == null) return ((ImageThreeStateButton) d).NormalImage;
            return baseValue;
        }

        public ImageSource DisabledImage
        {
            get { return (ImageSource)GetValue(DisabledImageProperty); }
            set { SetValue(DisabledImageProperty, value); }
        }

        public static readonly DependencyProperty DisabledImageProperty =
            DependencyProperty.Register("DisabledImage", typeof(ImageSource), typeof(ImageThreeStateButton),
                new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender));

        private static void ImageSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (((ImageThreeStateButton)sender).PressedImage == null)
            {
                ((ImageThreeStateButton)sender).PressedImage = (ImageSource)e.NewValue;
            }
            if (((ImageThreeStateButton)sender).HoverImage == null)
            {
                ((ImageThreeStateButton)sender).HoverImage = (ImageSource)e.NewValue;
            }
            //if (((ImageThreeStateButton)sender).DisabledImage == null)
            //{
            //    ((ImageThreeStateButton)sender).DisabledImage = (ImageSource)e.NewValue;

            //}
            //PressedImageProperty.OverrideMetadata(typeof(ImageSource), new FrameworkPropertyMetadata(e.NewValue, FrameworkPropertyMetadataOptions.AffectsRender));
            //Application.GetResourceStream(new Uri("pack://application:,,," + (string) e.NewValue));
        }

        #endregion
    }
}
