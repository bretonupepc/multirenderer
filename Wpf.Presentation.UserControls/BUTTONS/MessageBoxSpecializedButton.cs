﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Wpf.Presentation.UserControls.BUTTONS
{
    public class MessageBoxSpecializedButton:ImageThreeStateButton
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        static MessageBoxSpecializedButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MessageBoxSpecializedButton),
                new FrameworkPropertyMetadata(typeof(MessageBoxSpecializedButton)));
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<

        #region Dependency Properties

        public bool IsDefaultButton
        {
            get { return (bool)GetValue(IsDefaultButtonProperty); }
            set
            {
                SetValue(IsDefaultButtonProperty, value);
            }
        }

        public static readonly DependencyProperty IsDefaultButtonProperty =
            DependencyProperty.Register("IsDefaultButton", typeof(bool), typeof(MessageBoxSpecializedButton),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        public bool IsTimedButton
        {
            get { return (bool)GetValue(IsTimedButtonProperty); }
            set
            {
                SetValue(IsTimedButtonProperty, value);
            }
        }

        public static readonly DependencyProperty IsTimedButtonProperty =
            DependencyProperty.Register("IsTimedButton", typeof(bool), typeof(MessageBoxSpecializedButton),
                new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender));

        public int TimerSeconds
        {
            get { return (int)GetValue(TimerSecondsProperty); }
            set
            {
                SetValue(TimerSecondsProperty, value);
            }
        }

        public static readonly DependencyProperty TimerSecondsProperty =
            DependencyProperty.Register("TimerSeconds", typeof(int), typeof(MessageBoxSpecializedButton),
                new FrameworkPropertyMetadata(10, FrameworkPropertyMetadataOptions.AffectsRender));

        public int TimerRemainingSeconds
        {
            get { return (int)GetValue(TimerRemainingSecondsProperty); }
            set
            {
                SetValue(TimerRemainingSecondsProperty, value);
            }
        }

        public static readonly DependencyProperty TimerRemainingSecondsProperty =
            DependencyProperty.Register("TimerRemainingSeconds", typeof(int), typeof(MessageBoxSpecializedButton),
                new FrameworkPropertyMetadata(10, FrameworkPropertyMetadataOptions.AffectsRender));




        #endregion


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
