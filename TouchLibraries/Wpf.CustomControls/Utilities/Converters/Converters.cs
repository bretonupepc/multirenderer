﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Wpf.CustomControls.Utilities.Converters
{
    ///// <summary>
    ///// https://stackoverflow.com/questions/5399601/imagesourceconverter-error-for-source-null
    ///// </summary>
    //public class ImageNotExistingConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if (value == null)
    //            return new Uri("pack://application:,,,/Wpf.Touch.Base;component/RESOURCES/IMAGES/FileLoadingNotExisting.png");
    //            //return DependencyProperty.UnsetValue;
    //        return value;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        // According to https://msdn.microsoft.com/en-us/library/system.windows.data.ivalueconverter.convertback(v=vs.110).aspx#Anchor_1
    //        // (kudos Scott Chamberlain), if you do not support a conversion 
    //        // back you should return a Binding.DoNothing or a 
    //        // DependencyProperty.UnsetValue
    //        return Binding.DoNothing;
    //        // Original code:
    //        // throw new NotImplementedException();
    //    }
    //}

    public class ThumbnailBitmapImageFromImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Uri uri = value as Uri;
            if(uri == null)
            {
                string s = value as string;
                if(s != null)
                {
                    if(System.IO.File.Exists(s))
                        uri = new Uri(s);
                }
            }
            if (uri == null)
                uri = new Uri("pack://application:,,,/Wpf.CustomControls;component/Images/FileLoadingNotExisting.png");
            var bi = new System.Windows.Media.Imaging.BitmapImage();
            bi.BeginInit();
            bi.UriSource = uri;
            bi.DecodePixelWidth = 100;
            bi.EndInit();
            return bi;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class IsSelectionModeSingleOrMultipleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? System.Windows.Controls.SelectionMode.Multiple : System.Windows.Controls.SelectionMode.Single;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class WindowSizeToMediumSquareConverter : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values != null && values.Length == 2 && values[0] is double && values[1] is double)
            {
                var width = (double)values[0];
                var height = (double)values[1];
                var minDimension = Math.Min(width, height);
                var multiplier = 0.03;
                double value = Math.Max(0, minDimension * multiplier);
                return value;
            }

            return Binding.DoNothing;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            return new object[] { Binding.DoNothing, Binding.DoNothing };
        }
    }

    public class ValidationYesFromUserToNullableBoolConverter : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values != null && values.Length == 2 && values[0] is Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode && values[1] is bool)
            {
                Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode mode = (Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode)values[0];
                bool hasValue = (bool)values[1];
                if (hasValue)
                    return mode == Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode.Yes;
                else
                    return false;
            }

            return Binding.DoNothing;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                bool isYes = (bool)value;
                if(isYes)
                    return new object[] { Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode.Yes, true};
            }
            return new object[] { Binding.DoNothing, Binding.DoNothing };
        }
    }
    public class ValidationNoFromUserToNullableBoolConverter : IMultiValueConverter
    {
        public object Convert(
            object[] values,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (values != null && values.Length == 2 && values[0] is Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode && values[1] is bool)
            {
                Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode mode = (Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode)values[0];
                bool hasValue = (bool)values[1];
                if (hasValue)
                    return mode == Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode.No;
                else
                    return false;
            }

            return Binding.DoNothing;
        }

        public object[] ConvertBack(
            object value,
            Type[] targetTypes,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                bool isNo = (bool)value;
                if (isNo)
                    return new object[] { Wpf.CustomControls.ObservableItems.WtbValidationFromUserMode.No, true };
            }
            return new object[] { Binding.DoNothing, Binding.DoNothing };
        }
    }


    //public class ValidationYesFromUserToNullableBoolConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if(value is WtbValidationFromUserMode)
    //        {
    //            var eValue = (WtbValidationFromUserMode)value;
    //            switch(eValue)
    //            {
    //                case WtbValidationFromUserMode.Yes:
    //                    return true;
    //                case WtbValidationFromUserMode.No:
    //                    return false;
    //                case WtbValidationFromUserMode.Cancel:
    //                    return Binding.DoNothing;
    //            }
    //        }
    //        return Binding.DoNothing;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if (value is bool?)
    //        {
    //            var bValue = (bool?)value;
    //            switch (bValue)
    //            {
    //                case true:
    //                    return WtbValidationFromUserMode.Yes;
    //                case false:
    //                    return WtbValidationFromUserMode.No;
    //            }
    //        }
    //        return Binding.DoNothing;
    //    }
    //}

    //public class ValidationNoFromUserToNullableBoolConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if (value is WtbValidationFromUserMode)
    //        {
    //            var eValue = (WtbValidationFromUserMode)value;
    //            switch (eValue)
    //            {
    //                case WtbValidationFromUserMode.No:
    //                    return true;
    //                case WtbValidationFromUserMode.Yes:
    //                    return false;
    //                case WtbValidationFromUserMode.Cancel:
    //                    return Binding.DoNothing;
    //            }
    //        }
    //        return Binding.DoNothing;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if (value is bool?)
    //        {
    //            var bValue = (bool?)value;
    //            switch (bValue)
    //            {
    //                case true:
    //                    return WtbValidationFromUserMode.No;
    //                case false:
    //                    return WtbValidationFromUserMode.Yes;
    //            }
    //        }
    //        return Binding.DoNothing;
    //    }
    //}

    public class ObjectToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is string ? (string)value : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class ObjectToEnabledVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class StringToEnabledVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !string.IsNullOrWhiteSpace((string)value) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class BooleanToEnabledVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value == Visibility.Visible;
        }
    }
    public class BooleanToDisabledVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility)value == Visibility.Collapsed;
        }
    }
    public class IsUIDesignToHiddenVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class IsComposingToEnabledVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class IsComposingToDisabledVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class IsComposingToHiddenVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? Visibility.Hidden : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class IsProgramRunningToDisabledIsHitTestVisible : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !((bool)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class IsProgramRunningToDisabledOpacity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? 0.75 : 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }

    public class IsUILoadingToDisabledIsHitTestVisible : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !((bool)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class IsUILoadingToDisabledOpacity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? 0.8 : 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class EnabledToOpacityCN : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? 1 : 0.75;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class IsComposingToGridLength0ToAuto : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((bool)value) ? GridLength.Auto : new GridLength(150, GridUnitType.Pixel);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
