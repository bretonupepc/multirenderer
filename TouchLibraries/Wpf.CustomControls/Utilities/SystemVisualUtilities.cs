﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.IO;
using System.Windows.Media.Imaging;

namespace Wpf.CustomControls.Utilities
{

    public static class ObservableStringUtilities
    {
        /// <summary>
        /// Determines if a type is numeric. Nullable numeric types are considered numeric.
        /// </summary>
        /// <remarks>
        /// Boolean is not considered numeric.
        /// </remarks>
        public static bool IsNumericType(object o)
        {
            return IsNumericType(o != null ? o.GetType() : null);
        }
        public static bool IsNumericType(Type type)
        {
            if (type == null)
            {
                return false;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return IsNumericType(Nullable.GetUnderlyingType(type));
                    }
                    return false;
            }
            return false;
        }

        public static string LogException(Exception ex, bool writeToDisk = true)
        {
            try
            {
                string msg = ConvertToVisual.GetFrom(ex);
                if (writeToDisk)
                    ;// Breton.TraceLoggers.TraceLog.WriteLine(Breton.TraceLoggers.LogLevel.Error, msg);
                return msg;
            }
            catch (Exception ex1)
            {
                throw;
            }
        }

        public static string Format(string formattingMessage, params object[] formattingArguments)
        {
            string result;
            //try
            //{
            result = string.Format(formattingMessage, formattingArguments);
            //}
            //catch (Exception ex)
            //{
            //    result = LogException(new BrMillCore.Utilities.Exceptions.FormatException(string.Format("Exception formatting formattingMessage='{0}' with formattingArguments='{1}'", ToStringVisual(formattingMessage), ToStringVisual(formattingArguments)), ex));
            //}
            return result;
        }
        public static string FormatVisual(string formattingMessage, params object[] formattingArguments)
        {
            return FormatVisualImpl(false, formattingMessage, formattingArguments);
        }
        public static string FormatVisualShort(string formattingMessage, params object[] formattingArguments)
        {
            return FormatVisualImpl(true, formattingMessage, formattingArguments);
        }
        public static string FormatVisualImpl(bool useShortFormat, string formattingMessage, params object[] formattingArguments)
        {
            string result;
            //try
            //{
            object[] formattingArgumentsVisual;
            if (formattingArguments == null || formattingArguments.Length == 0)
            {
                formattingArgumentsVisual = formattingArguments;
            }
            else
            {
                int count = formattingArguments.Length;
                formattingArgumentsVisual = new object[count];
                for (int i = 0; i < count; i++)
                {
                    if (IsNumericType(formattingArguments[i]))
                        formattingArgumentsVisual[i] = formattingArguments[i];
                    else
                        formattingArgumentsVisual[i] = ToStringVisualImpl(useShortFormat, formattingArguments[i]);
                }
            }

            result = string.Format(formattingMessage, formattingArgumentsVisual);
            //}
            //catch (Exception ex)
            //{
            //    result = LogException(new BrMillCore.Utilities.Exceptions.FormatVisualException(Format("Exception formatting to visual formattingMessage='{0}' with formattingArguments='{1}' useShortFormat='{2}'", ToStringVisual(formattingMessage), ToStringVisual(formattingArguments), useShortFormat), ex));
            //}
            return result;
        }
        public static string ToStringVisual(params object[] lo)
        {
            return ToStringVisualImpl(false, lo);
        }
        private static string ToStringVisualImpl(bool useShortFormat, params object[] lo)
        {
            StringBuilder sbToStringVisualImpl = null;

            //try
            //{
            if (lo == null)
                return "<NULL>";
            if (lo.Length == 0)
                return "<EMPTY>";
            foreach (var o in lo)
            {
                if (o is IEnumerable<char>)
                {
                    AppendToLogImpl(ref sbToStringVisualImpl, ConvertToVisual.GetFrom(o));
                }
                else
                {
                    string convertedToVisual;
                    if (useShortFormat)
                        convertedToVisual = ConvertToVisual.GetShortFrom(o);
                    else
                        convertedToVisual = ConvertToVisual.GetFrom(o);
                    AppendToLogImpl(ref sbToStringVisualImpl, convertedToVisual);
                    if (o is System.Collections.IEnumerable)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var oo in o as System.Collections.IEnumerable)
                        {
                            if (useShortFormat)
                                convertedToVisual = ConvertToVisual.GetShortFrom(oo);
                            else
                                convertedToVisual = ConvertToVisual.GetFrom(oo);

                            string s = ConvertToVisual.IncreaseIndent(convertedToVisual);
                            if (sb.Length == 0)
                                sb.Append(s);
                            else
                                sb.Append(Environment.NewLine + s);
                        }
                        if (sb.Length > 0)
                            AppendToLogImpl(ref sbToStringVisualImpl, sb.ToString());
                    }
                }
            }
            //}
            //catch (Exception ex)
            //{
            //    l = LogException(new BrMillCore.Utilities.Exceptions.ToStringVisualException("Error formatting object", ex));
            //}

            if (sbToStringVisualImpl != null)
                return sbToStringVisualImpl.ToString();
            else
                return null;
        }
        static void AppendToLogImpl(ref StringBuilder sb, string message, bool insertAtStart = false)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (sb == null)
            {
                sb = new StringBuilder(message);
            }
            else
            {
                if (insertAtStart)
                {
                    sb.Insert(0, message + Environment.NewLine);
                }
                else
                {
                    sb.Append(Environment.NewLine + message);
                }
            }
        }

        public static string AppendSubLevel(string previous, string appending)
        {
            return previous + Environment.NewLine + ConvertToVisual.IncreaseIndent(appending);
        }
        public static int GetArrayLength(string s)
        {
            if (s != null)
                return s.Length;
            else
                return -1;
        }
        public static string GetSubstringFromStart(string s, int length)
        {
            if (string.IsNullOrWhiteSpace(s))
                return s;
            return s.Substring(0, Math.Min(length, s.Length));
        }

        private class ConvertToVisual
        {
            public static string IncreaseIndent(string formatting)
            {
                if (formatting != null)
                {
                    var spl = formatting.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    if (spl.Length == 1)
                    {
                        return "\t" + spl[0];
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var s in spl)
                        {
                            if (sb.Length > 0)
                                sb.Append(Environment.NewLine);
                            sb.Append("\t" + s);
                        }
                        return sb.ToString();
                    }
                }
                else
                {
                    return null;
                }
            }

            private static string pGetFrom(System.DateTime dt)
            {
                return string.Format("{0:00}_{1:00}_{2:00}_{3:00}:{4:00}:{5:00}.{6:000}", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
            }

            private static string pGetFrom(System.Exception ex)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("EXCEPTION");
                var spl = ex.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var s in spl)
                    sb.Append(Environment.NewLine + ConvertToVisual.IncreaseIndent(ToStringVisual(s)));

                System.Diagnostics.StackTrace stk = new System.Diagnostics.StackTrace(1, true);
                sb.Append(Environment.NewLine + ConvertToVisual.GetFrom(stk));

                string sException = sb.ToString();
                return sException;
            }

            private static string pGetFrom(System.Diagnostics.StackTrace stk)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("STACKTRACE");
                for (int i = 0; i < stk.FrameCount && i < 5; i++)
                {
                    var stkf = stk.GetFrame(i);
                    var filename = stkf.GetFileName();

                    sb.Append(Environment.NewLine + ConvertToVisual.IncreaseIndent(ToStringVisual(stkf)));

                    //var method = stkf.GetMethod();
                    //if (method == null)
                    //    break;
                    //var reflectedType = method.ReflectedType;
                    //if (reflectedType == null)
                    //    break;
                    //var className = reflectedType.FullName;
                    //if (string.IsNullOrWhiteSpace(className))
                    //    break;
                    //if (className.StartsWith("System."))
                    //    break;
                    if (string.IsNullOrWhiteSpace(filename))
                        break;
                }
                var sStackTrace = sb.ToString();
                return sStackTrace;
            }
            private static string pGetFrom(System.Diagnostics.StackFrame stkf)
            {
                var method = stkf.GetMethod();
                var filename = stkf.GetFileName();
                var fileLineNumber = stkf.GetFileLineNumber();
                var fileColumnNumber = stkf.GetFileColumnNumber();

                var sMethod = ToStringVisual(method);
                var sFilename = ToStringVisual(filename);
                var sFileLineNumber = ToStringVisual(fileLineNumber);
                var sFileColumnNumber = ToStringVisual(fileColumnNumber);

                return string.Format("method='{0}' filename='{1}' fileLineNumber='{2}' fileColumnNumber='{3}'", sMethod, sFilename, sFileLineNumber, sFileColumnNumber);
            }
            private static string pGetFrom(System.Delegate d)
            {
                if (d != null)
                    return pGetFrom(d.Method);
                else
                    return string.Format("delegate='{0}''", NULL_VALUE);
            }
            private static string pGetFrom(System.Reflection.MethodBase method)
            {
                string sMethod = null;
                if (method is System.Reflection.MethodInfo)
                {
                    var mi = method as System.Reflection.MethodInfo;
                    if (mi.DeclaringType != null)
                        sMethod = string.Format("MethodInfo className='{0}' methodName='{1}' method='{2}'", ToStringVisual(mi.DeclaringType.Name), ToStringVisual(mi.Name), ToStringVisual(mi.ToString()));
                }
                else if (sMethod == null && method is System.Reflection.MethodBase)
                {
                    var mb = method as System.Reflection.MethodBase;
                    sMethod = string.Format("MethodBase methodName='{0}' method='{1}'", ToStringVisual(mb.Name), ToStringVisual(mb.ToString()));
                }
                else if (sMethod == null)
                {
                    sMethod = string.Format("method='{0}''", NULL_VALUE);
                }
                return sMethod;
            }
            private static string pGetFrom(System.Reflection.PropertyInfo p)
            {
                if (p != null)
                    return p.Name.ToString();
                else
                    return NULL_VALUE;
            }
            private static string pGetFrom(System.Collections.IDictionary d)
            {
                if (d != null)
                    return string.Format("Dictionary='{0}' Count='{1}'", d.GetType(), d.Count);
                else
                    return NULL_VALUE;
            }
            private static string pGetFrom(string value)
            {
                string name;
                if (value == null)
                    name = NULL_VALUE;
                else if (value == string.Empty)
                    name = EMPTY_VALUE;
                else
                    name = value;
                return name;
            }
            public static string GetFrom(object o)
            {
                if (o != null)
                {
                    var piStringVisual = o.GetType().GetProperty("SystemUtilitiesStringVisual");
                    if (piStringVisual != null && piStringVisual.PropertyType == typeof(String))
                        return pGetFrom(piStringVisual.GetValue(o, null) as String);
                    else if (o is System.Exception)
                        return pGetFrom(o as System.Exception);
                    else if (o is System.Diagnostics.StackTrace)
                        return pGetFrom(o as System.Diagnostics.StackTrace);
                    else if (o is System.Diagnostics.StackFrame)
                        return pGetFrom(o as System.Diagnostics.StackFrame);
                    else if (o is System.Reflection.PropertyInfo)
                        return pGetFrom(o as System.Reflection.PropertyInfo);
                    else if (o is System.Reflection.MethodBase)
                        return pGetFrom(o as System.Reflection.MethodBase);
                    else if (o is System.Delegate)
                        return pGetFrom(o as System.Delegate);
                    else if (o is System.DateTime)
                        return pGetFrom((System.DateTime)o);
                    else if (o is String)
                        return pGetFrom(o as String);
                    else if (o is System.Collections.IDictionary)
                        return pGetFrom(o as System.Collections.IDictionary);
                    else
                        return GetFrom(o.ToString());
                }
                else
                    return NULL_OBJECT;
            }
            public const string NULL_OBJECT = "<NULL OBJECT>";
            public const string NULL_VALUE = "<NULL VALUE>";
            public const string EMPTY_VALUE = "<EMPTY VALUE>";


            private static string pGetShortFrom(System.Type t)
            {
                if (t != null)
                    return t.Name;
                else
                    return NULL_OBJECT;
            }
            private static string pGetShortFrom(System.DateTime dt)
            {
                return string.Format("{0:000}_{1:00}:{2:00}:{3:00}.{4:000}", dt.DayOfYear, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
            }
            public static string GetShortFrom(object o)
            {
                if (o is Type)
                    return pGetShortFrom(o as Type);
                else if (o is DateTime)
                    return pGetShortFrom((DateTime)o);
                else
                    return GetFrom(o);
            }
        }
    }

    public static class SystemVisualUtilities
    {
        public static bool GetIsInDesignMode(DependencyObject element)
        {
            return System.ComponentModel.DesignerProperties.GetIsInDesignMode(element);
        }
        public static bool GetIsInRuntimeMode(DependencyObject element)
        {
            return !System.ComponentModel.DesignerProperties.GetIsInDesignMode(element);
        }

        //public static System.Windows.MessageBoxResult Show(string messageBoxText, string title, System.Windows.MessageBoxButton button, System.Windows.MessageBoxImage icon)
        //{
        //    if (WindowHandler.Instance.ApplicationExiting)
        //        return MessageBoxResult.None;

        //    Breton.Wpf.MessageBox.Result result = Breton.Wpf.MessageBox.MessageBoxNotifier.MessageBox.Show(messageBoxText, title, button, icon);

        //    System.Windows.MessageBoxResult messageBoxResult = VisualUtils.GetMessageBoxResult(result);

        //    return messageBoxResult;
        //}

        //public static bool TryFileSelect(string initialDirectory, out string fullpath, string filter = null, string title = null)
        //{
        //    try
        //    {
        //        fullpath = null;

        //        string directory;
        //        if (SystemUtilities.DirectoryExists(initialDirectory))
        //            directory = initialDirectory;
        //        else
        //            directory = SystemUtilities.GetDirectoryBin();

        //        var dialog = new Microsoft.Win32.OpenFileDialog();
        //        dialog.Multiselect = false;
        //        if (filter != null)
        //            dialog.Filter = filter;
        //        if (!string.IsNullOrWhiteSpace(title))
        //            dialog.Title = title;
        //        dialog.InitialDirectory = directory;

        //        bool? result = dialog.ShowDialog(WindowHandler.Instance.CurrentWindow);

        //        if (!SystemUtilities.FileExists(fullpath))
        //            fullpath = null;

        //        return fullpath != null;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new CriticalException();
        //    }
        //}
        //public static bool TryFileSave(string initialDirectory, out string fullpath, string filter = null, string title = null)
        //{
        //    try
        //    {
        //        fullpath = null;

        //        string directory;
        //        if (SystemUtilities.DirectoryExists(initialDirectory))
        //            directory = initialDirectory;
        //        else
        //            directory = SystemUtilities.GetDirectoryBin();

        //        var dialog = new Microsoft.Win32.SaveFileDialog();
        //        if(filter != null)
        //            dialog.Filter = filter;
        //        if (!string.IsNullOrWhiteSpace(title))
        //            dialog.Title = title;
        //        dialog.InitialDirectory = directory;
        //        dialog.ValidateNames = true;
        //        dialog.CheckPathExists = true;

        //        bool? result = dialog.ShowDialog(WindowHandler.Instance.CurrentWindow);

        //        if (result == true)
        //            fullpath = dialog.FileName;

        //        return fullpath != null;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public static Window FindWindow(DependencyObject current)
        {
            return FindAncestor<Window>(current);
        }

        public static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            return VisualUtils.FindAncestor<T>(current);
        }
        public static T FindImplementation<T>(DependencyObject current) where T : class
        {
            return VisualUtils.FindImplementation<T>(current);
        }

        public static UIElement FindUid(DependencyObject parent, string uid)
        {
            return VisualUtils.FindUid(parent, uid);
        }

        private static class VisualUtils
        {

            //public static System.Windows.MessageBoxResult GetMessageBoxResult(Breton.Wpf.MessageBox.Result result)
            //{
            //    System.Windows.MessageBoxResult messageBoxResult = System.Windows.MessageBoxResult.None;
            //    switch (result)
            //    {
            //        case Breton.Wpf.MessageBox.Result.Yes:
            //            messageBoxResult = System.Windows.MessageBoxResult.Yes;
            //            break;
            //        case Breton.Wpf.MessageBox.Result.No:
            //            messageBoxResult = System.Windows.MessageBoxResult.No;
            //            break;
            //        case Breton.Wpf.MessageBox.Result.Cancel:
            //            messageBoxResult = System.Windows.MessageBoxResult.Cancel;
            //            break;
            //        case Breton.Wpf.MessageBox.Result.Ok:
            //            messageBoxResult = System.Windows.MessageBoxResult.OK;
            //            break;
            //        case Breton.Wpf.MessageBox.Result.Undefined:
            //        case Breton.Wpf.MessageBox.Result.None:
            //        default:
            //            messageBoxResult = System.Windows.MessageBoxResult.None;
            //            break;
            //    }
            //    return messageBoxResult;
            //}

            public static UIElement FindUid(DependencyObject parent, string uid)
            {
                UIElement found = null;

                var uielement = parent as UIElement;
                if (uielement != null && uielement.Uid == uid)
                    found = uielement;
                if (found != null)
                    return found;

                found = FindUidVisualTreeHelper(parent, uid);
                if (found != null)
                    return found;

                found = FindUidLogicalTreeHelper(parent, uid);
                if (found != null)
                    return found;

                return null;
            }

            private static UIElement FindUidVisualTreeHelper(DependencyObject parent, string uid)
            {
                var count = VisualTreeHelper.GetChildrenCount(parent);
                if (count == 0) return null;

                for (int i = 0; i < count; i++)
                {
                    var el = VisualTreeHelper.GetChild(parent, i) as UIElement;
                    if (el == null) continue;

                    if (el.Uid == uid) 
                        return el;

                    el = FindUidVisualTreeHelper(el, uid);
                    if (el != null) 
                        return el;
                }

                return null;
            }

            private static UIElement FindUidLogicalTreeHelper(DependencyObject parent, string name)
            {
                var l = new List<FrameworkElement>();
                GetLogicalChildCollection<FrameworkElement>(parent, l);

                if (l != null && l.Count > 0)
                {
                    return l.FirstOrDefault(o => o.Uid == name);
                }
                else
                {
                    return null;
                }
            }

            private static void GetLogicalChildCollection<T>(DependencyObject parent, List<T> logicalCollection) where T : DependencyObject
            {
                System.Collections.IEnumerable children = LogicalTreeHelper.GetChildren(parent);
                foreach (object child in children)
                {
                    if (child is DependencyObject)
                    {
                        DependencyObject depChild = child as DependencyObject;
                        if (child is T)
                        {
                            logicalCollection.Add(child as T);
                        }

                        var fChild = child as FrameworkElement;
                        if (fChild != null)
                        {
                            var tp = fChild.TemplatedParent;
                            if (tp != null)
                            {
                                if (tp is T)
                                {
                                    logicalCollection.Add(tp as T);
                                }
                                GetLogicalChildCollection(tp, logicalCollection);
                            }
                        }

                        GetLogicalChildCollection(depChild, logicalCollection);
                    }
                }
            }

            public static childItem FindVisualChild<childItem>(DependencyObject obj)
                where childItem : DependencyObject
            {

                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
                {

                    DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                    if (child != null && child is childItem)

                        return (childItem)child;

                    else
                    {

                        childItem childOfChild = FindVisualChild<childItem>(child);

                        if (childOfChild != null)

                            return childOfChild;

                    }

                }

                return null;

            }

            public static T FindAncestor<T>(DependencyObject current)
                where T : DependencyObject
            {
                do
                {
                    if (current is T)
                    {
                        return (T)current;
                    }
                    current = VisualTreeHelper.GetParent(current);
                }
                while (current != null);
                return null;
            }
            public static T FindImplementation<T>(DependencyObject current) where T : class
            {
                if (current != null)
                {
                    var typeInterface = typeof(T);
                    if (typeInterface.IsInterface)
                    {
                        do
                        {
                            var currentType = current.GetType();
                            if (typeInterface.IsAssignableFrom(currentType))
                            {
                                return current as T;
                            }
                            current = VisualTreeHelper.GetParent(current);
                        }
                        while (current != null);
                    }
                }

                return default(T);
            }
            
            public static Stream GetCursorFromBitmap(string bitmapPath, byte hotspotx, byte hotspoty)
            {
                //StreamResourceInfo sri = Application.GetResourceStream(bitmapPath);

                Stream s = new FileStream(bitmapPath, FileMode.Open);

                byte[] buffer = new byte[s.Length];

                s.Read(buffer, 0, (int)s.Length);

                MemoryStream ms = new MemoryStream();

                buffer[2] = 2; // change to CUR file type
                buffer[10] = hotspotx;
                buffer[12] = hotspoty;

                ms.Write(buffer, 0, (int)s.Length);

                ms.Position = 0;

                return ms;
            }

            public static int GetGridRowByMousePos(Grid grid, Point position)
            {
                Point gridMousePos = grid.PointFromScreen(position);
                double progrHeight = 0;
                int currentRow = 0;
                for (currentRow = 0; currentRow < grid.RowDefinitions.Count; currentRow++)
                {
                    progrHeight += grid.RowDefinitions[currentRow].ActualHeight;
                    if (gridMousePos.Y <= progrHeight)
                        break;
                }
                return currentRow;
            }

            public static int GetGridColByMousePos(Grid grid, Point position)
            {
                Point gridMousePos = grid.PointFromScreen(position);
                double progrLeft = 0;
                int currentCol = 0;
                for (currentCol = 0; currentCol < grid.ColumnDefinitions.Count; currentCol++)
                {
                    progrLeft += grid.ColumnDefinitions[currentCol].ActualWidth;
                    if (gridMousePos.X <= progrLeft)
                        break;
                }
                return currentCol;
            }

            /// <summary>
            /// Collapses all treeview elements
            /// </summary>
            /// <param name="t">treeview to collapse</param>
            public static void CollapseTreeview(TreeView t)
            {
                t.InvalidateMeasure();
                t.InvalidateArrange();
                t.InvalidateVisual();

                foreach (object o in t.ItemsSource)
                {
                    var ti = GetTreeViewItem(t, o, true);
                    if (ti == null)
                        continue;
                    ti.IsExpanded = false;
                }
            }

            /// <summary>
            /// Recursively search for an item in this subtree.
            /// </summary>
            /// <param name="container">
            /// The parent ItemsControl. This can be wh TreeView or wh TreeViewItem.
            /// </param>
            /// <param name="item">
            /// The item to search for.
            /// </param>
            /// <returns>
            /// The TreeViewItem that contains the specified item.
            /// </returns> 
            public static TreeViewItem GetTreeViewItem(ItemsControl container, object item, bool collapseSubItems)
            {
                TreeViewItem found = null;
                GetTreeViewItem(container, item, collapseSubItems, ref found);

                if (container.IsLoaded && found != null)
                {
                    found.BringIntoView();
                }

                return found;
            }

            private static bool GetTreeViewItem(ItemsControl container, object item, bool collapseSubItems, ref TreeViewItem found)
            {
                bool iscurrent = false;

                if (container != null)
                {
                    // Expand the current container
                    if (container is TreeViewItem && !((TreeViewItem)container).IsExpanded)
                    {
                        container.SetValue(TreeViewItem.IsExpandedProperty, true);
                    }

                    if (container.DataContext == item)
                    {
                        found = container as TreeViewItem;
                        iscurrent = true;
                    }

                    // Try to generate the ItemsPresenter and the ItemsPanel.
                    // by calling ApplyTemplate.  Note that in the 
                    // virtualizing case even if the item is marked 
                    // expanded we still need to do this step in order to 
                    // regenerate the visuals because they may have been virtualized away.

                    //container.ApplyTemplate();
                    //ItemsPresenter itemsPresenter =
                    //    (ItemsPresenter)container.Template.FindName("ItemsHost", container);
                    //if (itemsPresenter != null)
                    //{
                    //    itemsPresenter.ApplyTemplate();
                    //}
                    //else
                    //{
                    // The Tree template has not named the ItemsPresenter, 
                    // so walk the descendents and find the child.
                    ItemsPresenter itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    if (itemsPresenter == null)
                    {
                        container.UpdateLayout();

                        itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    }
                    //}

                    if (itemsPresenter == null)
                        return iscurrent;

                    if (VisualTreeHelper.GetChildrenCount(itemsPresenter) == 0)
                        return iscurrent;

                    Panel itemsHostPanel = (Panel)VisualTreeHelper.GetChild(itemsPresenter, 0);

                    // Ensure that the generator for this panel has been created.
                    UIElementCollection children = itemsHostPanel.Children;

                    VirtualizingStackPanel virtualizingPanel = itemsHostPanel as VirtualizingStackPanel;

                    bool isChildFound = false;
                    int lev = 0;
                    if (found != null && container is TreeViewItem)
                    {
                        var tvi = container as TreeViewItem;
                        while (tvi != null)
                        {
                            ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(tvi);
                            lev++;

                            if (parent is TreeViewItem)
                            {
                                if (parent == found)
                                {
                                    isChildFound = true;
                                    break;
                                }
                                tvi = parent as TreeViewItem;
                                tvi.IsExpanded = true;
                            }
                            else
                                tvi = null;

                        }
                    }
                    if (isChildFound && lev == 1)
                    {
                        var tvi = container as TreeViewItem;
                        tvi.IsExpanded = false;
                    }
                    else
                        for (int i = 0, count = container.Items.Count; i < count; i++)
                        {
                            TreeViewItem subContainer;
                            if (virtualizingPanel != null)
                            {
                                // Bring the item into view so 
                                // that the container will be generated.
                                virtualizingPanel.BringIntoView();

                                subContainer = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(i);
                            }
                            else
                            {
                                subContainer = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(i);

                                if (subContainer != null)
                                {
                                    // Bring the item into view to maintain the 
                                    // same behavior as with wh virtualizing panel.
                                    subContainer.BringIntoView();
                                }
                            }

                            if (subContainer != null)
                            {
                                //subContainer.IsExpanded = true;

                                //if (container.DataContext != item)
                                //{
                                // Search the next level for the object.
                                bool res = GetTreeViewItem(subContainer, item, collapseSubItems, ref found);
                                if (res)
                                {
                                    //selected item is always expanded
                                }
                                else if (collapseSubItems)
                                {
                                    // The object is not under this TreeViewItem
                                    // so collapse it.
                                    if (!iscurrent)
                                    {
                                        if (found != null)
                                        {
                                            bool isParentFound = false;
                                            int levParent = 0;
                                            if (found != null && subContainer is TreeViewItem)
                                            {
                                                var tvi = found as TreeViewItem;
                                                while (tvi != null)
                                                {
                                                    ItemsControl parent = ItemsControl.ItemsControlFromItemContainer(tvi);
                                                    levParent++;

                                                    if (parent is TreeViewItem)
                                                    {
                                                        if (parent == subContainer)
                                                        {
                                                            isParentFound = true;
                                                            break;
                                                        }
                                                        tvi = parent as TreeViewItem;
                                                        tvi.IsExpanded = true;
                                                    }
                                                    else
                                                        tvi = null;

                                                }
                                            }
                                            if (!isParentFound)
                                                subContainer.IsExpanded = false;
                                        }
                                        else
                                            subContainer.IsExpanded = false;
                                    }
                                }
                                //}
                            }
                        }
                }

                return iscurrent;
            }

            /// <summary>
            /// Recursively search for an item in this subtree.
            /// </summary>
            /// <param name="container">
            /// The parent ItemsControl. This can be wh TreeView or wh TreeViewItem.
            /// </param>
            /// <param name="item">
            /// The item to search for.
            /// </param>
            /// <returns>
            /// The TreeViewItem that contains the specified item.
            /// </returns>
            public static bool ExpandAll(ItemsControl container)
            {
                var result = true;

                if (container != null)
                {
                    // Expand the current container
                    if (container is TreeViewItem && !((TreeViewItem)container).IsExpanded)
                    {
                        container.SetValue(TreeViewItem.IsExpandedProperty, true);
                    }

                    ItemsPresenter itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    if (itemsPresenter == null)
                    {
                        container.UpdateLayout();

                        itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    }

                    if (itemsPresenter == null)
                        return false;

                    if (VisualTreeHelper.GetChildrenCount(itemsPresenter) == 0)
                        return true;

                    Panel itemsHostPanel = (Panel)VisualTreeHelper.GetChild(itemsPresenter, 0);

                    // Ensure that the generator for this panel has been created.
                    UIElementCollection children = itemsHostPanel.Children;

                    VirtualizingStackPanel virtualizingPanel =
                        itemsHostPanel as VirtualizingStackPanel;

                    for (int i = 0, count = container.Items.Count; i < count; i++)
                    {
                        TreeViewItem subContainer;
                        if (virtualizingPanel != null)
                        {
                            // Bring the item into view so that the container will be generated.
                            virtualizingPanel.BringIntoView();

                            subContainer = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(i);
                        }
                        else
                        {
                            subContainer = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(i);

                            if (subContainer != null)
                            {
                                // Bring the item into view to maintain the same behavior as with wh virtualizing panel.
                                subContainer.BringIntoView();
                            }
                        }

                        if (subContainer != null)
                        {
                            // Search the next level for the object.
                            result &= ExpandAll(subContainer);
                            if (result)
                            {
                                //item is expanded correctly
                            }
                        }
                    }
                }

                return true;
            }

            /// <summary>
            /// Recursively search for an item in this subtree.
            /// </summary>
            /// <param name="container">
            /// The parent ItemsControl. This can be wh TreeView or wh TreeViewItem.
            /// </param>
            /// <param name="item">
            /// The item to search for.
            /// </param>
            /// <returns>
            /// The TreeViewItem that contains the specified item.
            /// </returns>
            public static TreeViewItem GetTreeViewItem(ItemsControl container, object item)
            {
                if (container != null)
                {
                    if (container.DataContext == item)
                    {
                        return container as TreeViewItem;
                    }

                    ItemsPresenter itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    if (itemsPresenter == null)
                    {
                        container.UpdateLayout();

                        itemsPresenter = FindVisualChild<ItemsPresenter>(container);
                    }

                    if (itemsPresenter == null)
                        return null;

                    if (VisualTreeHelper.GetChildrenCount(itemsPresenter) == 0)
                        return null;

                    Panel itemsHostPanel = (Panel)VisualTreeHelper.GetChild(itemsPresenter, 0);

                    // Ensure that the generator for this panel has been created.
                    UIElementCollection children = itemsHostPanel.Children;

                    VirtualizingStackPanel virtualizingPanel = itemsHostPanel as VirtualizingStackPanel;

                    for (int i = 0, count = container.Items.Count; i < count; i++)
                    {
                        TreeViewItem subContainer;
                        if (virtualizingPanel != null)
                            subContainer = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(i);
                        else
                            subContainer = (TreeViewItem)container.ItemContainerGenerator.ContainerFromIndex(i);

                        if (subContainer != null)
                        {
                            // Search the next level for the object.
                            TreeViewItem resultContainer = GetTreeViewItem(subContainer, item);
                            if (resultContainer != null)
                                return resultContainer;
                        }
                    }
                }

                return null;
            }

            /// <summary>
            /// Search for an element of wh certain type in the visual tree.
            /// </summary>
            /// <typeparam name="T">The type of element to find.</typeparam>
            /// <param name="visual">The parent element.</param>
            /// <returns></returns>
            private static T FindVisualChild<T>(Visual visual) where T : Visual
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(visual); i++)
                {
                    Visual child = (Visual)VisualTreeHelper.GetChild(visual, i);
                    if (child != null)
                    {
                        T correctlyTyped = child as T;
                        if (correctlyTyped != null)
                        {
                            return correctlyTyped;
                        }

                        T descendent = FindVisualChild<T>(child);
                        if (descendent != null)
                        {
                            return descendent;
                        }
                    }
                }

                return null;
            }

            /// <summary>
            /// Finds wh Child of wh given item in the visual tree. 
            /// </summary>
            /// <param name="parent">A direct parent of the queried item.</param>
            /// <typeparam name="T">The type of the queried item.</typeparam>
            /// <param name="childName">x:Name or Name of child. </param>
            /// <returns>The first parent item that matches the submitted type parameter. 
            /// If not matching item can be found, 
            /// wh null parent is being returned.</returns>
            public static T FindChild<T>(DependencyObject parent, string childName)
               where T : DependencyObject
            {
                // Confirm parent and childName are valid. 
                if (parent == null) return null;

                T foundChild = null;

                int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
                for (int i = 0; i < childrenCount; i++)
                {
                    var child = VisualTreeHelper.GetChild(parent, i);
                    // If the child is not of the request child type child
                    T childType = child as T;
                    if (childType == null)
                    {
                        // recursively drill down the tree
                        foundChild = FindChild<T>(child, childName);

                        // If the child is found, break so we do not overwrite the found child. 
                        if (foundChild != null) break;
                    }
                    else if (!string.IsNullOrEmpty(childName))
                    {
                        var frameworkElement = child as FrameworkElement;
                        // If the child's name is set for search
                        if (frameworkElement != null && frameworkElement.Name == childName)
                        {
                            // if the child's name is of the request name
                            foundChild = (T)child;
                            break;
                        }
                    }
                    else
                    {
                        // child element found.
                        foundChild = (T)child;
                        break;
                    }
                }

                return foundChild;
            }

            /// <summary>
            /// Get wh bitmap image from wh Control view
            /// </summary>
            /// <param name="target"></param>
            /// <param name="dpiX"></param>
            /// <param name="dpiY"></param>
            /// <returns></returns>
            public static BitmapSource CaptureScreen(Visual target, double dpiX, double dpiY)
            {
                if (target == null)
                {
                    return null;
                }
                Rect bounds = VisualTreeHelper.GetDescendantBounds(target);
                return CaptureScreen(bounds, target, dpiX, dpiY);
            }
            public static BitmapSource CaptureScreen(Rect bounds, Visual target, double dpiX, double dpiY)
            {
                if (target == null)
                {
                    return null;
                }
                RenderTargetBitmap rtb = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                                (int)(bounds.Height * dpiY / 96.0),
                                                                dpiX,
                                                                dpiY,
                                                                PixelFormats.Pbgra32);
                DrawingVisual dv = new DrawingVisual();
                using (DrawingContext ctx = dv.RenderOpen())
                {
                    VisualBrush vb = new VisualBrush(target);
                    ctx.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
                }
                rtb.Render(dv);
                return rtb;
            }

            /// <summary>
            /// HitTest considerando UIElement.IsVisible e UIElement.IsHitTestVisible
            /// </summary>
            /// <param name="reference">elemento in cui esegurie l'hittest</param>
            /// <param name="point">punto risp. riferimento di <paramref name="reference"/></param>
            /// <param name="hitTestResult">primo elemento trovato</param>
            /// <returns>numero di elementi trovati</returns>
            public static int HitTest(Visual reference, Point point, out HitTestResult hitTestResult)
            {
                HitTestData htd = new HitTestData();

                // Call example
                VisualTreeHelper.HitTest(
                reference,
                HitTestFilter_Visible_IsHitTestVisible,
                htd.MyHitTestResult,
                new PointHitTestParameters(point));

                if (htd.ListHitTestResult != null)
                {
                    hitTestResult = htd.ListHitTestResult[0];
                    return htd.ListHitTestResult.Count;
                }
                else
                {
                    hitTestResult = null;
                    return 0;
                }
            }
            public static HitTestFilterBehavior HitTestFilter_Visible_IsHitTestVisible(DependencyObject potentialHitTestTarget)
            {
                bool isVisible = false;
                bool isHitTestVisible = false;

                var uiElement = potentialHitTestTarget as UIElement;
                if (uiElement != null)
                {
                    isVisible = uiElement.IsVisible;
                    if (isVisible)
                    {
                        isHitTestVisible = uiElement.IsHitTestVisible;
                    }
                }
                else
                {
                    UIElement3D uiElement3D = potentialHitTestTarget as UIElement3D;
                    if (uiElement3D != null)
                    {
                        isVisible = uiElement3D.IsVisible;
                        if (isVisible)
                        {
                            isHitTestVisible = uiElement3D.IsHitTestVisible;
                        }
                    }
                }

                if (isVisible)
                {
                    return isHitTestVisible ? HitTestFilterBehavior.Continue : HitTestFilterBehavior.ContinueSkipSelfAndChildren;
                }

                return HitTestFilterBehavior.ContinueSkipSelfAndChildren;
            }
            public class HitTestData
            {
                public List<HitTestResult> ListHitTestResult = null;

                // Returns the result of the hit test to the callback.
                public HitTestResultBehavior MyHitTestResult(HitTestResult result)
                {
                    if (result.VisualHit is Visual)
                    {
                        if (ListHitTestResult == null)
                            ListHitTestResult = new List<HitTestResult>();
                        // Add the hit test result to the list that will be processed after the enumeration.
                        ListHitTestResult.Add(result);
                    }
                    // Set the behavior to return visuals at all z-order levels.
                    return HitTestResultBehavior.Continue;
                }
            }

            [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto, ExactSpelling = true)]
            public static extern bool SetForegroundWindow(IntPtr hWnd);

            public static bool SetForegroundWindow(System.Windows.DependencyObject chWindow, IntPtr hWndBackground)
            {
                bool rc = VisualUtils.SetForegroundWindow(hWndBackground);
                if (rc)
                {
                    var w = FindAncestor<Window>(chWindow);
                    if (w != null)
                    {
                        var wih = new System.Windows.Interop.WindowInteropHelper(w);

                        if (wih != null)
                        {
                            rc &= VisualUtils.SetForegroundWindow(wih.Handle);
                        }
                    }
                }
                return rc;
            }
        }
    }
}
