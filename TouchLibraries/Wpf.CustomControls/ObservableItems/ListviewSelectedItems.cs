﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Wpf.CustomControls.ObservableItems
{
    public class ListViewSelectedItemsBinder
    {
        private ListView _listBox;
        private IList _collection;

        public ListViewSelectedItemsBinder(ListView listBox, IList collection)
        {
            _listBox = listBox;
            _collection = collection;

            _listBox.SelectedItems.Clear();

            foreach (var item in _collection)
            {
                _listBox.SelectedItems.Add(item);
            }
        }

        public void Bind()
        {
            _listBox.SelectionChanged += ListBox_SelectionChanged;

            if (_collection is INotifyCollectionChanged)
            {
                var observable = (INotifyCollectionChanged)_collection;
                observable.CollectionChanged += Collection_CollectionChanged;
            }
        }

        public void UnBind()
        {
            if (_listBox != null)
                _listBox.SelectionChanged -= ListBox_SelectionChanged;

            if (_collection != null && _collection is INotifyCollectionChanged)
            {
                var observable = (INotifyCollectionChanged)_collection;
                observable.CollectionChanged -= Collection_CollectionChanged;
            }
        }

        private void Collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                if (_listBox.SelectionMode == SelectionMode.Single)
                    _listBox.SelectedItem = null;
                else
                    _listBox.SelectedItems.Clear();
            }
            else
            {
                if (_listBox.SelectionMode == SelectionMode.Single)
                {
                    foreach (var item in e.NewItems ?? new object[0])
                        _listBox.SelectedItem = item;
                }
                else
                {
                    foreach (var item in e.NewItems ?? new object[0])
                    {
                        if (!_listBox.SelectedItems.Contains(item))
                            _listBox.SelectedItems.Add(item);
                    }
                    foreach (var item in e.OldItems ?? new object[0])
                    {
                        _listBox.SelectedItems.Remove(item);
                    }
                }
            }
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems ?? new object[0])
            {
                if (!_collection.Contains(item))
                    _collection.Add(item);
            }

            foreach (var item in e.RemovedItems ?? new object[0])
            {
                _collection.Remove(item);
            }
        }


    }

    public class ListViewExtensions
    {
        private static ListViewSelectedItemsBinder GetSelectedValueBinder(DependencyObject obj)
        {
            return (ListViewSelectedItemsBinder)obj.GetValue(SelectedValueBinderProperty);
        }

        private static void SetSelectedValueBinder(DependencyObject obj, ListViewSelectedItemsBinder items)
        {
            obj.SetValue(SelectedValueBinderProperty, items);
        }

        private static readonly DependencyProperty SelectedValueBinderProperty = 
            DependencyProperty.RegisterAttached(
            "SelectedValueBinder", 
            typeof(ListViewSelectedItemsBinder), 
            typeof(ListViewExtensions));

        public static readonly DependencyProperty SelectedValuesProperty = 
            DependencyProperty.RegisterAttached(
            "SelectedValues", 
            typeof(IList), 
            typeof(ListViewExtensions), 
            new FrameworkPropertyMetadata(null, OnSelectedValuesChanged));

        private static void OnSelectedValuesChanged(DependencyObject o, DependencyPropertyChangedEventArgs value)
        {
            var oldBinder = GetSelectedValueBinder(o);
            if (oldBinder != null)
                oldBinder.UnBind();

            SetSelectedValueBinder(o, new ListViewSelectedItemsBinder((ListView)o, (IList)value.NewValue));
            GetSelectedValueBinder(o).Bind();
        }

        public static void SetSelectedValues(Selector elementName, IEnumerable value)
        {
            elementName.SetValue(SelectedValuesProperty, value);
        }

        public static IEnumerable GetSelectedValues(Selector elementName)
        {
            return (IEnumerable)elementName.GetValue(SelectedValuesProperty);
        }
    }
}
