﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.CustomControls.ObservableItems
{
    public abstract class ObservableNpcObject : ObservableBase
    {
        private INotifyPropertyChanged _NpcObject = null;
        public INotifyPropertyChanged NpcObject
        {
            get { return _NpcObject; }
            set { SetProperty(ref _NpcObject, value); }
        }

        private bool _IsVisibleRequestActive = false;
        public bool IsVisibleRequestActive
        {
            get { return _IsVisibleRequestActive; }
            set { SetProperty(ref _IsVisibleRequestActive, value); }
        }

        private string _KeyObservableNpcObject = null;
        public string KeyObservableNpcObject
        {
            get { return _KeyObservableNpcObject; }
            set { SetProperty(ref _KeyObservableNpcObject, value); }
        }

        protected static void SetValues(ObservableNpcObject oncpo, INotifyPropertyChanged npcObject, string key)
        {
            oncpo.NpcObject = npcObject;
            var sKey = key;
            if (string.IsNullOrWhiteSpace(sKey))
                sKey = Guid.NewGuid().ToString();
            oncpo.KeyObservableNpcObject = sKey;
        }

        public bool IsSameObservableNpcObject(string key)
        {
            var sKey = GetKeyForComparisionOfSameItem(key);
            var sThis = GetKeyForComparisionOfSameItem(_KeyObservableNpcObject);
            return sThis == key;
        }

        static string GetKeyForComparisionOfSameItem(string key)
        {
            var sKey = key;
            if(sKey != null)
                sKey = sKey.Trim();
            return sKey;
        }
    }

    public class WtbProxyLinks
    {
        public static bool TryCopy(INotifyPropertyChanged oFrom, string fromPropertyName, INotifyPropertyChanged oTo, string toPropertyName)
        {
            object value;
            if (!WtbProxyLinks.TryGet(oFrom, fromPropertyName, out value))
                return false;

            WtbProxyLinks.TrySet(oTo, toPropertyName, value);

            return true;
        }

        public static void SetToPropertyName(object oTo, string toPropertyName, object valueToSet)
        {
            WtbProxyLinks.TryGet(oTo, toPropertyName, out valueToSet);
        }

        public static bool TrySet(object oTo, string toSubsequentPath, object valueToSet)
        {
            if (oTo == null) throw new ArgumentNullException("value");
            if (toSubsequentPath == null) throw new ArgumentNullException("path");
            if (toSubsequentPath.Contains("[")) throw new ArgumentNullException("Wpf.Touch.Base.ObservableItems ReflectorUtil value not implemented for IDictionary and IList");

            var spl = toSubsequentPath.Split('.');
            if (spl.Length == 0)
            {
                return false;
            }

            Type currentTypeCycling = oTo.GetType();
            object obj = oTo;
            System.Reflection.PropertyInfo pi = null;
            for (int i = 0; i < spl.Length; i++)
            {
                string propertyName = spl[i];

                if (currentTypeCycling != null)
                {
                    System.Reflection.PropertyInfo property = null;

                    property = currentTypeCycling.GetProperty(propertyName);
                    if (i < spl.Length - 1)
                    {
                        obj = property.GetValue(obj, null);
                    }
                    else
                    {
                        pi = property;
                    }
                    currentTypeCycling = obj != null ? obj.GetType() : null; //property.PropertyType;
                }
                else
                {
                    return false;
                }
            }
            var resultIsValid = obj != null && pi != null;
            if (resultIsValid)
            {
                pi.SetValue(obj, valueToSet);
            }
            return resultIsValid;
        }

        public static bool TryGet<T>(object oFrom, string fromSubsequentPath, out T result)
        {
            result = default(T);

            if (oFrom == null) throw new ArgumentNullException("value");
            if (fromSubsequentPath == null) throw new ArgumentNullException("path");

            var spl = fromSubsequentPath.Split('.');
            if (spl.Length == 0)
            {
                result = default(T);
                return false;
            }

            Type currentTypeCycling = oFrom.GetType();
            object obj = oFrom;
            foreach (string propertyName in spl)
            {
                if (currentTypeCycling != null)
                {
                    System.Reflection.PropertyInfo property = null;
                    int brackStart = propertyName.IndexOf("[");
                    int brackEnd = propertyName.IndexOf("]");

                    property = currentTypeCycling.GetProperty(brackStart > 0 ? propertyName.Substring(0, brackStart) : propertyName);
                    obj = property.GetValue(obj, null);

                    if (brackStart > 0)
                    {
                        string index = propertyName.Substring(brackStart + 1, brackEnd - brackStart - 1);
                        foreach (Type iType in obj.GetType().GetInterfaces())
                        {
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof(IDictionary<,>))
                            {
                                obj = typeof(WtbProxyLinks).GetMethod("GetDictionaryElement")
                                                     .MakeGenericMethod(iType.GetGenericArguments())
                                                     .Invoke(null, new object[] { obj, index });
                                break;
                            }
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof(IList<>))
                            {
                                obj = typeof(WtbProxyLinks).GetMethod("GetListElement")
                                                     .MakeGenericMethod(iType.GetGenericArguments())
                                                     .Invoke(null, new object[] { obj, index });
                                break;
                            }
                        }
                    }

                    currentTypeCycling = obj != null ? obj.GetType() : null; //property.PropertyType;
                }
                else
                {
                    result = default(T);
                    return false;
                }
            }
            var resultIsValid = currentTypeCycling != null;
            if (resultIsValid && obj != null)
                result = (T)obj;
            return resultIsValid;
        }

        public static TValue GetDictionaryElement<TKey, TValue>(IDictionary<TKey, TValue> dict, object index)
        {
            TKey key = (TKey)Convert.ChangeType(index, typeof(TKey), null);
            return dict[key];
        }

        public static T GetListElement<T>(IList<T> list, object index)
        {
            return list[Convert.ToInt32(index)];
        }
    }

}
