﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.CustomControls.ObservableItems;

namespace Wpf.CustomControls.ItemSelectors
{
    /// <summary>
    /// Interaction logic for ItemSelectorListView.xaml
    /// </summary>
    public partial class ItemSelectorListView : UserControl
    {
        ListViewSelectedItemsBinder _SelectedItemsBinder = null;

        public static readonly DependencyProperty ItemSelectorListViewItemBackgroundUnselectedProperty =
            DependencyProperty.Register(
            "ItemSelectorListViewItemBackgroundUnselected",
            typeof(Brush),
            typeof(ItemSelectorListView),
            new UIPropertyMetadata(Brushes.Red));
        public Brush ItemSelectorListViewItemBackgroundUnselected
        {
            get { return (Brush)GetValue(ItemSelectorListViewItemBackgroundUnselectedProperty); }
            set { SetValue(ItemSelectorListViewItemBackgroundUnselectedProperty, value); }
        }

        public static readonly DependencyProperty ItemSelectorListViewItemDataTemplateProperty = 
            DependencyProperty.Register(
            "ItemSelectorListViewItemDataTemplate", 
            typeof(DataTemplate), 
            typeof(ItemSelectorListView), 
            new UIPropertyMetadata(null));
        public DataTemplate ItemSelectorListViewItemDataTemplate
        {
            get { return (DataTemplate)GetValue(ItemSelectorListViewItemDataTemplateProperty); }
            set { SetValue(ItemSelectorListViewItemDataTemplateProperty, value); }
        }

        public ItemSelectorListView()
        {
            InitializeComponent();

            itemSelectorListView.ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;

            DataContextChanged += ItemSelectorListView_DataContextChanged;
        }

        void ItemSelectorListView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (_SelectedItemsBinder != null)
            {
                _SelectedItemsBinder.UnBind();
                _SelectedItemsBinder = null;
            }
            var vm = DataContext as IWtbObservableNpcObjectSelectedViewModel;
            if(vm != null)
            {
                _SelectedItemsBinder = new ListViewSelectedItemsBinder(itemSelectorListView, vm.SelectedItems);
                _SelectedItemsBinder.Bind();
            }
        }


        void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
        {
            d.Clear();

            var items = itemSelectorListView.Items;
            for (int i = 0; i < items.Count; i++)
            {
                var itm = items[i];
                var uiObj = itemSelectorListView.ItemContainerGenerator.ContainerFromItem(itm);
                var frameworkElement = uiObj as FrameworkElement;
                if (frameworkElement == null)
                    return;


                frameworkElement.SizeChanged += frameworkElement_SizeChanged;
                //var f = new SizeChangedEventHandler(delegate(object senderEv, SizeChangedEventArgs eEv) 
                //{
                //    var frameworkElementEv = (FrameworkElement)senderEv;
                //    frameworkElementEv.SizeChanged -= f;
                //    var itmEv = frameworkElement.DataContext;

                //    SetVisibilityToItem(itm, GetRectForVisibility());
                //});
                //    frameworkElement.SizeChanged += f;
            }
            //SetVisibilityToAllItems();
        }

        void frameworkElement_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var frameworkElement = (FrameworkElement)sender;
            frameworkElement.SizeChanged -= frameworkElement_SizeChanged;
            var itm = frameworkElement.DataContext;

            SetVisibilityToItem(itm, GetRectForVisibility());
        }

        private void itemSelectorListView_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            SetVisibilityToAllItems();
        }

        Dictionary<string, bool> d = new Dictionary<string, bool>();
        private void SetVisibilityToAllItems()
        {
            d.Clear();

            var items = itemSelectorListView.Items;

            var rectForVisibility = GetRectForVisibility();

            for (int i = 0; i < items.Count; i++)
            {
                var itm = items[i];
                SetVisibilityToItem(itm, rectForVisibility);
            }
        }

        void SetVisibilityToItem(object itm, Rect rectForVisibility)
        {
            var itmOB = itm as ObservableNpcObject;
            if (itmOB == null)
                return;
            var uiObj = itemSelectorListView.ItemContainerGenerator.ContainerFromItem(itm);
            var frameworkElement = uiObj as FrameworkElement;
            if (frameworkElement == null)
                return;

            var t = frameworkElement.TransformToAncestor(itemSelectorListView);

            var fw = frameworkElement.ActualWidth;
            var fh = frameworkElement.ActualHeight;
            var ptNeg = t.Transform(new Point(0, 0));
            var ptPos = t.Transform(new Point(fw, fh));

            bool isVisible = false;
            isVisible |= rectForVisibility.Contains(ptNeg);
            isVisible |= rectForVisibility.Contains(ptPos);

            itmOB.IsVisibleRequestActive |= isVisible;

            d[itmOB.ToString()] = isVisible;
        }

        Rect GetRectForVisibility()
        {
            var pv = itemSelectorListView;
            if (pv == null)
                return Rect.Empty;
            var pvw = pv.ActualWidth;
            var pvh = pv.ActualHeight;
            var rectForVisibility = new Rect(0, 0, pvw, pvh);
            rectForVisibility.Inflate(1, 1);
            return rectForVisibility;
        }
    }
}
