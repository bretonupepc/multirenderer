﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf.CustomControls
{
    public class ButtonImage : Button
    {
        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }
        public static readonly DependencyProperty ImageProperty =
            DependencyProperty.Register("Image", typeof(ImageSource), typeof(ButtonImage), new PropertyMetadata(null));

        public string ResourcesKey
        {
            get { return (string)GetValue(ResourcesKeyProperty); }
            set { SetValue(ResourcesKeyProperty, value); }
        }
        public static readonly DependencyProperty ResourcesKeyProperty =
            DependencyProperty.Register("ResourcesKey", typeof(string), typeof(ButtonImage), new PropertyMetadata(null));

        static ButtonImage()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ButtonImage), new FrameworkPropertyMetadata(typeof(ButtonImage)));
        }
    }
}
