﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.Touch.BusinessObjects.Serialization
{
    public abstract class DataSetSource
    {
        public class DataSetSourceEmpty : DataSetSource
        {
            public override bool IsEmpty { get { return true; } }

            protected override IEnumerable<System.ComponentModel.INotifyPropertyChanged> GetImpl(DataSetClauseRetrievingResult clause)
            {
                return null;
            }
        }
        public static readonly DataSetSource Empty = new DataSetSourceEmpty();

        public virtual bool IsEmpty { get { return false; } }
        public virtual bool IsMock { get { return false; } }

        public IEnumerable<System.ComponentModel.INotifyPropertyChanged> Get(DataSetClauseRetrievingResult clause)
        {
            var result = GetImpl(clause);
            if (result == null)
                result = new System.ComponentModel.INotifyPropertyChanged[0];
            return result;
        }
        protected abstract IEnumerable<System.ComponentModel.INotifyPropertyChanged> GetImpl(DataSetClauseRetrievingResult clause);
    }
}
