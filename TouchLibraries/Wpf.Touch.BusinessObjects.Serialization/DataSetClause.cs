﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.Touch.BusinessObjects.Serialization
{
    [System.Xml.Serialization.XmlInclude(typeof(DataSetClauseRetrievingResultEmpty))]
    public abstract class DataSetClauseRetrievingResult
    {
        public class DataSetClauseRetrievingResultEmpty : DataSetClauseRetrievingResult
        {
            public override bool IsEmpty { get { return true; } }

            protected override string GetMarkupStringImpl()
            {
                return string.Empty;
            }
            protected override void SetFromMarkupStringImpl(string markupString)
            {
            }
        }
        public static readonly DataSetClauseRetrievingResult Empty = new DataSetClauseRetrievingResultEmpty();

        public virtual bool IsEmpty { get { return false; } }

        public string GetMarkupString()
        {
            string markupString = GetMarkupStringImpl();
            if (string.IsNullOrWhiteSpace(markupString))
                markupString = string.Empty;
            return markupString;
        }
        protected abstract string GetMarkupStringImpl();

        public void SetFromMarkupString(string markupString)
        {
            SetFromMarkupStringImpl(markupString);
        }
        protected abstract void SetFromMarkupStringImpl(string markupString);
    }

    public abstract class DataSetClauseItem
    {
        public string GetMarkupString()
        {
            string markupString = GetMarkupStringImpl();
            if (string.IsNullOrWhiteSpace(markupString))
                markupString = string.Empty;
            return markupString;
        }
        protected abstract string GetMarkupStringImpl();

        public void SetFromMarkupString(string markupString)
        {
            SetFromMarkupStringImpl(markupString);
        }
        protected abstract void SetFromMarkupStringImpl(string markupString);
    }
}
