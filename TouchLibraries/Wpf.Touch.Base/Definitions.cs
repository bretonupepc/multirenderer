﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.Touch.Base
{
    public interface IWtbItemSelectorViewModel
    {
        System.Collections.IEnumerable Set { get; set; }
        void Load();
    }

    public interface IWtbObservableNpcObjectSelectedViewModel
    {
        bool IsSelectionMultipleEnabled {get; set;}
        System.Collections.ObjectModel.ObservableCollection<Wpf.Touch.Base.ObservableItems.ObservableNpcObject> SelectedItems { get; set; }
    }

    public interface IWtbValidationFromUser
    {
        WtbValidationFromUserMode ValueValidationFromUser { get; set; }
        bool HasValidationFromUser { get; set; }
        Wpf.Common.Commands.DelegateCommand CommandOnHasValidationFromUser { get; set; }
    }
    public enum WtbValidationFromUserMode
    {
        None = 0,
        Yes = 1,
        No = 2,
        Cancel = 3,
    }
}
