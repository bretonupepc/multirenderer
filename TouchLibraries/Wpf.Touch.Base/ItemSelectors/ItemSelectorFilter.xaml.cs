﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.Touch.Base;

namespace Wpf.Touch.Base.ItemSelectors
{
    /// <summary>
    /// Interaction logic for ItemSelectorFilter.xaml
    /// </summary>
    public partial class ItemSelectorFilter : UserControl
    {
        public ItemSelectorFilter()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var setViewModel = DataContext as IWtbItemSelectorViewModel;
            setViewModel.Load();
        }
    }
}
