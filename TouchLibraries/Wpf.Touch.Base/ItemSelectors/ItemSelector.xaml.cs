﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf.Touch.Base.ItemSelectors
{
    /// <summary>
    /// Interaction logic for ItemSelector.xaml
    /// </summary>
    public partial class ItemSelector : UserControl
    {
        public static readonly DependencyProperty ItemSelectorListViewItemDataTemplateProperty = 
            DependencyProperty.Register(
            "ItemSelectorListViewItemDataTemplate", 
            typeof(DataTemplate), 
            typeof(ItemSelector),
            new UIPropertyMetadata(null));
        public DataTemplate ItemSelectorListViewItemDataTemplate
        {
            get { return (DataTemplate)GetValue(ItemSelectorListViewItemDataTemplateProperty); }
            set { SetValue(ItemSelectorListViewItemDataTemplateProperty, value); }
        }

        public ItemSelector()
        {
            InitializeComponent();
        }
    }
}
