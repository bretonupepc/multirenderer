﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.Touch.Base.ObservableItems
{
    public static class ObservableStringUtilities
    {
        /// <summary>
        /// Determines if a type is numeric. Nullable numeric types are considered numeric.
        /// </summary>
        /// <remarks>
        /// Boolean is not considered numeric.
        /// </remarks>
        public static bool IsNumericType(object o)
        {
            return IsNumericType(o != null ? o.GetType() : null);
        }
        public static bool IsNumericType(Type type)
        {
            if (type == null)
            {
                return false;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return IsNumericType(Nullable.GetUnderlyingType(type));
                    }
                    return false;
            }
            return false;
        }

        public static string LogException(Exception ex, bool writeToDisk = true)
        {
            try
            {
                string msg = ConvertToVisual.GetFrom(ex);
                if (writeToDisk)
                    ;// Breton.TraceLoggers.TraceLog.WriteLine(Breton.TraceLoggers.LogLevel.Error, msg);
                return msg;
            }
            catch (Exception ex1)
            {
                throw;
            }
        }

        public static string Format(string formattingMessage, params object[] formattingArguments)
        {
            string result;
            //try
            //{
                result = string.Format(formattingMessage, formattingArguments);
            //}
            //catch (Exception ex)
            //{
            //    result = LogException(new BrMillCore.Utilities.Exceptions.FormatException(string.Format("Exception formatting formattingMessage='{0}' with formattingArguments='{1}'", ToStringVisual(formattingMessage), ToStringVisual(formattingArguments)), ex));
            //}
            return result;
        }
        public static string FormatVisual(string formattingMessage, params object[] formattingArguments)
        {
            return FormatVisualImpl(false, formattingMessage, formattingArguments);
        }
        public static string FormatVisualShort(string formattingMessage, params object[] formattingArguments)
        {
            return FormatVisualImpl(true, formattingMessage, formattingArguments);
        }
        public static string FormatVisualImpl(bool useShortFormat, string formattingMessage, params object[] formattingArguments)
        {
            string result;
            //try
            //{
                object[] formattingArgumentsVisual;
                if (formattingArguments == null || formattingArguments.Length == 0)
                {
                    formattingArgumentsVisual = formattingArguments;
                }
                else
                {
                    int count = formattingArguments.Length;
                    formattingArgumentsVisual = new object[count];
                    for (int i = 0; i < count; i++)
                    {
                        if (IsNumericType(formattingArguments[i]))
                            formattingArgumentsVisual[i] = formattingArguments[i];
                        else
                            formattingArgumentsVisual[i] = ToStringVisualImpl(useShortFormat, formattingArguments[i]);
                    }
                }

                result = string.Format(formattingMessage, formattingArgumentsVisual);
            //}
            //catch (Exception ex)
            //{
            //    result = LogException(new BrMillCore.Utilities.Exceptions.FormatVisualException(Format("Exception formatting to visual formattingMessage='{0}' with formattingArguments='{1}' useShortFormat='{2}'", ToStringVisual(formattingMessage), ToStringVisual(formattingArguments), useShortFormat), ex));
            //}
            return result;
        }
        public static string ToStringVisual(params object[] lo)
        {
            return ToStringVisualImpl(false, lo);
        }
        private static string ToStringVisualImpl(bool useShortFormat, params object[] lo)
        {
            StringBuilder sbToStringVisualImpl = null;

            //try
            //{
            if (lo == null)
                return "<NULL>";
            if (lo.Length == 0)
                return "<EMPTY>";
            foreach (var o in lo)
            {
                if (o is IEnumerable<char>)
                {
                    AppendToLogImpl(ref sbToStringVisualImpl, ConvertToVisual.GetFrom(o));
                }
                else
                {
                    string convertedToVisual;
                    if (useShortFormat)
                        convertedToVisual = ConvertToVisual.GetShortFrom(o);
                    else
                        convertedToVisual = ConvertToVisual.GetFrom(o);
                    AppendToLogImpl(ref sbToStringVisualImpl, convertedToVisual);
                    if (o is System.Collections.IEnumerable)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var oo in o as System.Collections.IEnumerable)
                        {
                            if (useShortFormat)
                                convertedToVisual = ConvertToVisual.GetShortFrom(oo);
                            else
                                convertedToVisual = ConvertToVisual.GetFrom(oo);

                            string s = ConvertToVisual.IncreaseIndent(convertedToVisual);
                            if (sb.Length == 0)
                                sb.Append(s);
                            else
                                sb.Append(Environment.NewLine + s);
                        }
                        if (sb.Length > 0)
                            AppendToLogImpl(ref sbToStringVisualImpl, sb.ToString());
                    }
                }
            }
            //}
            //catch (Exception ex)
            //{
            //    l = LogException(new BrMillCore.Utilities.Exceptions.ToStringVisualException("Error formatting object", ex));
            //}

            if (sbToStringVisualImpl != null)
                return sbToStringVisualImpl.ToString();
            else
                return null;
        }
        static void AppendToLogImpl(ref StringBuilder sb, string message, bool insertAtStart = false)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (sb == null)
            {
                sb = new StringBuilder(message);
            }
            else
            {
                if (insertAtStart)
                {
                    sb.Insert(0, message + Environment.NewLine);
                }
                else
                {
                    sb.Append(Environment.NewLine + message);
                }
            }
        }

        public static string AppendSubLevel(string previous, string appending)
        {
            return previous + Environment.NewLine + ConvertToVisual.IncreaseIndent(appending);
        }
        public static int GetArrayLength(string s)
        {
            if (s != null)
                return s.Length;
            else
                return -1;
        }
        public static string GetSubstringFromStart(string s, int length)
        {
            if (string.IsNullOrWhiteSpace(s))
                return s;
            return s.Substring(0, Math.Min(length, s.Length));
        }

        private class ConvertToVisual
        {
            public static string IncreaseIndent(string formatting)
            {
                if (formatting != null)
                {
                    var spl = formatting.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    if (spl.Length == 1)
                    {
                        return "\t" + spl[0];
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (var s in spl)
                        {
                            if (sb.Length > 0)
                                sb.Append(Environment.NewLine);
                            sb.Append("\t" + s);
                        }
                        return sb.ToString();
                    }
                }
                else
                {
                    return null;
                }
            }

            private static string pGetFrom(System.DateTime dt)
            {
                return string.Format("{0:00}_{1:00}_{2:00}_{3:00}:{4:00}:{5:00}.{6:000}", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
            }

            private static string pGetFrom(System.Exception ex)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("EXCEPTION");
                var spl = ex.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var s in spl)
                    sb.Append(Environment.NewLine + ConvertToVisual.IncreaseIndent(ToStringVisual(s)));

                System.Diagnostics.StackTrace stk = new System.Diagnostics.StackTrace(1, true);
                sb.Append(Environment.NewLine + ConvertToVisual.GetFrom(stk));

                string sException = sb.ToString();
                return sException;
            }

            private static string pGetFrom(System.Diagnostics.StackTrace stk)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("STACKTRACE");
                for (int i = 0; i < stk.FrameCount && i < 5; i++)
                {
                    var stkf = stk.GetFrame(i);
                    var filename = stkf.GetFileName();

                    sb.Append(Environment.NewLine + ConvertToVisual.IncreaseIndent(ToStringVisual(stkf)));

                    //var method = stkf.GetMethod();
                    //if (method == null)
                    //    break;
                    //var reflectedType = method.ReflectedType;
                    //if (reflectedType == null)
                    //    break;
                    //var className = reflectedType.FullName;
                    //if (string.IsNullOrWhiteSpace(className))
                    //    break;
                    //if (className.StartsWith("System."))
                    //    break;
                    if (string.IsNullOrWhiteSpace(filename))
                        break;
                }
                var sStackTrace = sb.ToString();
                return sStackTrace;
            }
            private static string pGetFrom(System.Diagnostics.StackFrame stkf)
            {
                var method = stkf.GetMethod();
                var filename = stkf.GetFileName();
                var fileLineNumber = stkf.GetFileLineNumber();
                var fileColumnNumber = stkf.GetFileColumnNumber();

                var sMethod = ToStringVisual(method);
                var sFilename = ToStringVisual(filename);
                var sFileLineNumber = ToStringVisual(fileLineNumber);
                var sFileColumnNumber = ToStringVisual(fileColumnNumber);

                return string.Format("method='{0}' filename='{1}' fileLineNumber='{2}' fileColumnNumber='{3}'", sMethod, sFilename, sFileLineNumber, sFileColumnNumber);
            }
            private static string pGetFrom(System.Delegate d)
            {
                if (d != null)
                    return pGetFrom(d.Method);
                else
                    return string.Format("delegate='{0}''", NULL_VALUE);
            }
            private static string pGetFrom(System.Reflection.MethodBase method)
            {
                string sMethod = null;
                if (method is System.Reflection.MethodInfo)
                {
                    var mi = method as System.Reflection.MethodInfo;
                    if (mi.DeclaringType != null)
                        sMethod = string.Format("MethodInfo className='{0}' methodName='{1}' method='{2}'", ToStringVisual(mi.DeclaringType.Name), ToStringVisual(mi.Name), ToStringVisual(mi.ToString()));
                }
                else if (sMethod == null && method is System.Reflection.MethodBase)
                {
                    var mb = method as System.Reflection.MethodBase;
                    sMethod = string.Format("MethodBase methodName='{0}' method='{1}'", ToStringVisual(mb.Name), ToStringVisual(mb.ToString()));
                }
                else if (sMethod == null)
                {
                    sMethod = string.Format("method='{0}''", NULL_VALUE);
                }
                return sMethod;
            }
            private static string pGetFrom(System.Reflection.PropertyInfo p)
            {
                if (p != null)
                    return p.Name.ToString();
                else
                    return NULL_VALUE;
            }
            private static string pGetFrom(System.Collections.IDictionary d)
            {
                if (d != null)
                    return string.Format("Dictionary='{0}' Count='{1}'", d.GetType(), d.Count);
                else
                    return NULL_VALUE;
            }
            private static string pGetFrom(string value)
            {
                string name;
                if (value == null)
                    name = NULL_VALUE;
                else if (value == string.Empty)
                    name = EMPTY_VALUE;
                else
                    name = value;
                return name;
            }
            public static string GetFrom(object o)
            {
                if (o != null)
                {
                    var piStringVisual = o.GetType().GetProperty("SystemUtilitiesStringVisual");
                    if (piStringVisual != null && piStringVisual.PropertyType == typeof(String))
                        return pGetFrom(piStringVisual.GetValue(o, null) as String);
                    else if (o is System.Exception)
                        return pGetFrom(o as System.Exception);
                    else if (o is System.Diagnostics.StackTrace)
                        return pGetFrom(o as System.Diagnostics.StackTrace);
                    else if (o is System.Diagnostics.StackFrame)
                        return pGetFrom(o as System.Diagnostics.StackFrame);
                    else if (o is System.Reflection.PropertyInfo)
                        return pGetFrom(o as System.Reflection.PropertyInfo);
                    else if (o is System.Reflection.MethodBase)
                        return pGetFrom(o as System.Reflection.MethodBase);
                    else if (o is System.Delegate)
                        return pGetFrom(o as System.Delegate);
                    else if (o is System.DateTime)
                        return pGetFrom((System.DateTime)o);
                    else if (o is String)
                        return pGetFrom(o as String);
                    else if (o is System.Collections.IDictionary)
                        return pGetFrom(o as System.Collections.IDictionary);
                    else
                        return GetFrom(o.ToString());
                }
                else
                    return NULL_OBJECT;
            }
            public const string NULL_OBJECT = "<NULL OBJECT>";
            public const string NULL_VALUE = "<NULL VALUE>";
            public const string EMPTY_VALUE = "<EMPTY VALUE>";


            private static string pGetShortFrom(System.Type t)
            {
                if (t != null)
                    return t.Name;
                else
                    return NULL_OBJECT;
            }
            private static string pGetShortFrom(System.DateTime dt)
            {
                return string.Format("{0:000}_{1:00}:{2:00}:{3:00}.{4:000}", dt.DayOfYear, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
            }
            public static string GetShortFrom(object o)
            {
                if (o is Type)
                    return pGetShortFrom(o as Type);
                else if (o is DateTime)
                    return pGetShortFrom((DateTime)o);
                else
                    return GetFrom(o);
            }
        }
    }
}
