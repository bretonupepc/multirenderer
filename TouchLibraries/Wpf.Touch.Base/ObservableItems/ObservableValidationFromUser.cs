﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.Touch.Base.ObservableItems
{
    public class ObservableValidationFromUserViewModel : ObservableBase, IWtbValidationFromUser
    {
        private Wpf.Common.Commands.DelegateCommand _CommandOnHasValidationFromUser = null;
        public Wpf.Common.Commands.DelegateCommand CommandOnHasValidationFromUser
        {
            get { return _CommandOnHasValidationFromUser; }
            set { SetProperty(ref _CommandOnHasValidationFromUser, value); }
        }

        private WtbValidationFromUserMode _ValueValidationFromUser = WtbValidationFromUserMode.None;
        public WtbValidationFromUserMode ValueValidationFromUser
        {
            get { return _ValueValidationFromUser; }
            set { SetProperty(ref _ValueValidationFromUser, value); }
        }

        bool _HasValidationFromUser = false;
        public bool HasValidationFromUser
        {
            get { return _HasValidationFromUser; }
            set { SetProperty(ref _HasValidationFromUser, value); }
        }
    }
}
