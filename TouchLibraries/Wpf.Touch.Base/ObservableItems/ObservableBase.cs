﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpf.Touch.Base.ObservableItems
{
    public abstract class ObservableBase : System.ComponentModel.INotifyPropertyChanged
    {
        #region Implementation of INotifyPropertyChanged

        public bool FireOnPropertyChanged = true;
        public bool PropertyDirty;

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            if (!FireOnPropertyChanged)
            {
                PropertyDirty = true;
                return;
            }

            System.ComponentModel.PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                PropertyDirty = false;

                var e = new System.ComponentModel.PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }

            PropertyChangedInternal(propertyName);
        }

        public virtual void OnPropertyChanged<T>(System.Linq.Expressions.Expression<Func<T>> propertyNameExpression)
        {
            OnPropertyChanged(GetNameProperty<T>(propertyNameExpression));
        }

        protected string GetNameProperty<T>(System.Linq.Expressions.Expression<Func<T>> propertyNameExpression)
        {
            return ((System.Linq.Expressions.MemberExpression)propertyNameExpression.Body).Member.Name;
        }

        protected System.Reflection.PropertyInfo GetPropertyInfo<T>(System.Linq.Expressions.Expression<Func<T>> propertyNameExpression)
        {
            Type type = GetType();
            var name = GetNameProperty<T>(propertyNameExpression);
            var propertyInfo = type.GetProperty(name);
            return propertyInfo;
        }

        protected virtual void PropertyChangedInternal(string name)
        {

        }

        //public static string GetProperty<T>(System.Linq.Expressions.Expression<Func<T>> propertyNameExpression)
        //{
        //    return ((System.Linq.Expressions.MemberExpression)propertyNameExpression.Body).Member.Name;
        //}

        /// <summary>
        /// Checks if a property already matches a desired value. Sets the property and
        /// notifies listeners only when necessary.
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="storage">Reference to a property with both getter and setter.</param>
        /// <param name="value">Desired value for the property.</param>
        /// <param name="propertyName">Name of the property used to notify listeners. This
        /// value is optional and can be provided automatically when invoked from compilers that
        /// support CallerMemberName.</param>
        /// <returns>True if the value was changed, false if the existing value matched the
        /// desired value.</returns>
        protected virtual bool SetProperty<T>(ref T storage, T value, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
        {
            if (object.Equals(storage, value)) return false;

            storage = value;
            this.OnPropertyChanged(propertyName);

            return true;
        }

        #endregion

        //public virtual T GetPropertyFromProxy<T>(ref T storage, [System.Runtime.CompilerServices.CallerMemberName] string propertyName = null)
        //{
        //    return storage;
        //}

        public void Test1()
        {
            OnPropertyChanged("");
        }
    }
}
