﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Collections.Specialized;
using System.Reflection;
using System.Windows.Markup;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Wpf.Touch.Base.RESOURCES
{
    public partial class SharedBase : ResourceDictionary
    {

        System.Windows.Media.Animation.Storyboard sb = new System.Windows.Media.Animation.Storyboard();

        //public void TextBoxCallKeyboard(object sender, RoutedEventArgs e)
        //{
        //    if (sender is TextBox)
        //    {
        //        var t = sender as TextBox;
        //        var r = t.FindRoot();

        //        bool numeric = Breton.BrRh_Libs.Br_DependencyProperties.GetTextBoxNumeric(t);
        //        if (r != null)
        //            r.EditText(t, numeric);
        //    }
        //}

        void ListView_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ListViewScroll(sender, e);
        }

        void ListView_UpdateViewPort(object sender, SizeChangedEventArgs e)
        {
            ListViewViewPort(sender, e);
        }

        public static void ListViewScroll(object sender, ScrollChangedEventArgs e)
        {
            if (sender is ScrollViewer == false)
                return;
            var s = sender as ScrollViewer;

            //if (!s.IsVisible)
            //    return;

            if (e == null || e.ExtentHeightChange != 0 || e.ExtentWidthChange != 0 || e.ViewportHeightChange != 0 || e.ViewportWidthChange != 0)
            {
                var v = s.Template.FindName("PART_VerticalScrollBar", s) as ScrollBar;
                if (v != null)
                    ListViewViewPort(v.Track, null);

                var h = s.Template.FindName("PART_HorizontalScrollBar", s) as ScrollBar;
                if (h != null)
                    ListViewViewPort(h.Track, null);
            }

            //if (e.VerticalChange != 0)
            //{
            //    if (v.TouchesOver.Count() > 0 || v.IsMouseOver)
            //    {
            //        sb.Stop();
            //        sb.Children.Clear();
            //        v.Opacity = 1;
            //    }
            //    else
            //    {
            //        sb.Stop();
            //        sb.Children.Clear();
            //        v.Opacity = 1;

            //        System.Windows.Media.Animation.DoubleAnimationUsingKeyFrames a = new System.Windows.Media.Animation.DoubleAnimationUsingKeyFrames();
            //        a.KeyFrames.Add(new System.Windows.Media.Animation.SplineDoubleKeyFrame(1, System.Windows.Media.Animation.KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(100))));
            //        a.KeyFrames.Add(new System.Windows.Media.Animation.SplineDoubleKeyFrame(1, System.Windows.Media.Animation.KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(2000))));
            //        a.KeyFrames.Add(new System.Windows.Media.Animation.SplineDoubleKeyFrame(0, System.Windows.Media.Animation.KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(2200))));
            //        System.Windows.Media.Animation.Storyboard.SetTarget(a, v);
            //        System.Windows.Media.Animation.Storyboard.SetTargetProperty(a, new PropertyPath(CmdButton.OpacityProperty));
            //        a.Completed += a_Completed;
            //        sb.Children.Add(a);
            //        sb.Begin();
            //    }
            //}
        }
        public static void ListViewViewPort(object sender, SizeChangedEventArgs e)
        {
            double MinThumbLength = 100;

            if (sender is Track)
            {
                var t = sender as Track;

                var s = Wpf.Touch.Base.RESOURCES.BASE.SystemVisualUtilities.FindAncestor<ScrollBar>(t);
                var sv = Wpf.Touch.Base.RESOURCES.BASE.SystemVisualUtilities.FindAncestor<ScrollViewer>(s);
                double trackLength = s.Orientation == Orientation.Vertical ? t.ActualHeight : t.ActualWidth;
                double m_originalViewportSize = s.Orientation == Orientation.Vertical ? sv.ViewportHeight : sv.ViewportWidth;
                double thumbHeight = m_originalViewportSize / (s.Maximum - s.Minimum + m_originalViewportSize) * trackLength;

                if (!double.IsNaN(thumbHeight) && s.Maximum != s.Minimum)
                {
                    double ts = Math.Max(thumbHeight, MinThumbLength);
                    if (trackLength - ts > 0)
                        s.ViewportSize = (ts * (s.Maximum - s.Minimum)) / (trackLength - ts);
                }
                if (s.Orientation == Orientation.Vertical)
                    s.MinHeight = 50;
                else
                    s.MinWidth = 50;
            }
        }

        void a_Completed(object sender, EventArgs e)
        {
           // a.Completed -= a_Completed;


        }
        void scrollbarVertical_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is ScrollBar == false)
                return;
            var scrollbar = sender as ScrollBar;
            //sb.Stop();
            sb.Children.Clear();
            scrollbar.Opacity = 1;
        }
        void scrollbarVertical_PreviewMouseMove(object sender, MouseEventArgs e)
        {
        }
        void scrollbarVertical_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is ScrollBar == false)
                return;
            var scrollbar = sender as ScrollBar;
            System.Windows.Media.Animation.DoubleAnimationUsingKeyFrames a = new System.Windows.Media.Animation.DoubleAnimationUsingKeyFrames();
            a.KeyFrames.Add(new System.Windows.Media.Animation.SplineDoubleKeyFrame(1, System.Windows.Media.Animation.KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(2000))));
            a.KeyFrames.Add(new System.Windows.Media.Animation.SplineDoubleKeyFrame(0, System.Windows.Media.Animation.KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(2200))));
            System.Windows.Media.Animation.Storyboard.SetTarget(a, scrollbar);
            System.Windows.Media.Animation.Storyboard.SetTargetProperty(a, new PropertyPath(UIElement.OpacityProperty));
            sb.Children.Add(a);
            sb.Begin();
        }    
        void scrollbar_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (sender is UIElement)
            {

            }
        }

        void ListView_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (sender is Control)
            {
                var l = sender as Control;
                var o = l.Template.FindName("PART_ScrollViewer", l);
                if (o is ScrollViewer)
                {
                    ScrollViewer s = o as ScrollViewer;
                    //se solo orizzontale oppure orizzontale e verticale + left shift
                    if (s.ComputedHorizontalScrollBarVisibility == scrollbarVisible && (Keyboard.IsKeyDown(Key.LeftShift) || s.ComputedVerticalScrollBarVisibility == scrollbarNotVisible))
                        s.ScrollToHorizontalOffset(s.HorizontalOffset - e.Delta);
                    else if (s.ComputedVerticalScrollBarVisibility == scrollbarVisible)
                        s.ScrollToVerticalOffset(s.VerticalOffset - e.Delta);
                }
            }
        }
        static Visibility scrollbarVisible = Visibility.Visible;
        static Visibility scrollbarNotVisible = Visibility.Collapsed;
    }
}
