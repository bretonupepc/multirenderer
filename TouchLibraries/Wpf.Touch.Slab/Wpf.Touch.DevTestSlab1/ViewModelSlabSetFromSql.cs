﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf.Touch.Slab.ObservableItems;

namespace Wpf.Touch.DevTestSlab1
{
    public class ViewModelSlabSetFromSql
    {
        //info di connessione
        Wpf.Touch.BusinessObjects.SqlAccess.SqlAccessInfo _SqlAccessInfo;
        //T_SLAB
        Wpf.Touch.BusinessObjects.SqlAccess.SlabSetSource _SlabSetSource;
        Wpf.Touch.BusinessObjects.SqlAccess.SlabSetClauseRetrievingResult _SlabSetClauseRetrievingResult;
        //T_PARAMETERS
        Wpf.Touch.BusinessObjects.SqlAccess.ParameterSetSource _ParameterSetSource;

        Wpf.Touch.BusinessObjects.SqlAccess.ParameterSetForCutScheme _CutSchemeSqlParameters;
        public Wpf.Touch.BusinessObjects.SqlAccess.ParameterSetForCutScheme CutSchemeSqlParameters { get { return _CutSchemeSqlParameters; } }

        ViewModelSlabItemSelector _ViewModelSlabItemSelector;
        public ViewModelSlabItemSelector VMSlabItemSelector { get { return _ViewModelSlabItemSelector; } }

        public void Initialize()
        {
            _SqlAccessInfo = new BusinessObjects.SqlAccess.SqlAccessInfo();

            _SlabSetSource = new BusinessObjects.SqlAccess.SlabSetSource(_SqlAccessInfo);
            _SlabSetClauseRetrievingResult = new BusinessObjects.SqlAccess.SlabSetClauseRetrievingResult();

            _ParameterSetSource = new BusinessObjects.SqlAccess.ParameterSetSource(_SqlAccessInfo);
            _CutSchemeSqlParameters = _ParameterSetSource.CreateCutSchemeSqlParameters();

            _ViewModelSlabItemSelector = FactorySlabSetObserved.CreateNewViewModel(_SlabSetSource, _SlabSetClauseRetrievingResult);
        }
    }
}
