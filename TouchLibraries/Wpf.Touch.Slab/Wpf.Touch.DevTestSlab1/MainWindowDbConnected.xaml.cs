﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.Touch.Slab;
using Wpf.Touch.Slab.ObservableItems;

namespace Wpf.Touch.DevTestSlab1
{
    /// <summary>
    /// Interaction logic for MainWindowDbConnected.xaml
    /// </summary>
    public partial class MainWindowDbConnected : Window
    {
        StandardSlabResources _StandardSlabResources;
        ViewModelSlabSetFromSql _VMSlabSetFromSql;
        public ViewModelSlabSetFromSql VMSlabSetFromSql { get { return _VMSlabSetFromSql; } }

        public MainWindowDbConnected()
        {
            InitializeComponent();

            _StandardSlabResources = new StandardSlabResources();
            _StandardSlabResources.LoadStandardResourceDictionary();

            _VMSlabSetFromSql = new ViewModelSlabSetFromSql();
            _VMSlabSetFromSql.Initialize();

            var vm = _VMSlabSetFromSql.VMSlabItemSelector;
            vm.CommandOnHasValidationFromUser = new Wpf.Common.Commands.DelegateCommand(new Action<object>(OnSlabSelectionDefinedFromUser));

            btnOpenSlabs.DataContext = vm;
            popupOpenSlabs.DataContext = vm;
        }

        void OnSlabSelectionDefinedFromUser(object parameter)
        {
            List<string> listValidated;
            if (_VMSlabSetFromSql.VMSlabItemSelector.TryGetSelectionValidatedFromUser(out listValidated))
            {
                MessageBox.Show(Wpf.CustomControls.Utilities.ObservableStringUtilities.FormatVisual("New selection={0}", listValidated));
            }
        }

        private void GridWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void btnLoadStandardDataTemplates_Click(object sender, RoutedEventArgs e)
        {
            _StandardSlabResources.TestAddOrRemove();
        }

        private void btnTestPropertyChange_Click(object sender, RoutedEventArgs e)
        {
            var vm = _VMSlabSetFromSql.VMSlabItemSelector;
            if (vm == null)
                return;
            var s = vm.Set;
            d--;
            foreach (var itm in s)
            {
                var sItem = itm as SlabItem;
                if (sItem == null)
                    continue;
                int slabDatabaseId;
                if (Wpf.CustomControls.ObservableItems.WtbProxyLinks.TryGet(sItem.NpcObject, "Id", out slabDatabaseId))
                {
                    int index = Math.Max(0, slabDatabaseId + d);
                    string imagePath = string.Format(@"C:\testSlabSelector\{0}.jpg", index);
                    Wpf.CustomControls.ObservableItems.WtbProxyLinks.TrySet(sItem.NpcObject, "SlabImage.ImagePath", imagePath);
                }
            }
        }
        int d = 0;

        private void btnOpenSlabs_Click(object sender, RoutedEventArgs e)
        {
            var vm = _VMSlabSetFromSql.VMSlabItemSelector;
            if (vm.IsSlabItemSelectorEnabledForVisualization)
            {
                vm.LoadSlabSelectionFromShapesToCreate(new List<string>() { "cas" }, "MAT00000001", 35, VMSlabSetFromSql.CutSchemeSqlParameters.ToleranceRangeDeviationForSlabRetrievalFromShapeDimZ, true);
            }
            else
            {
                MessageBox.Show("Use this.OnSlabSelectionDefinedFromUser after ok");
                //List<string> listValidated;
                //if(_ViewModelSlabItemSelector.TryGetSelectionValidatedFromUser(out listValidated))
                //{
                //    _ViewModelSlabItemSelector.PreviousSelectedItems = listValidated;
                //    MessageBox.Show(Wpf.Touch.Base.ObservableItems.ObservableStringUtilities.FormatVisual("New selection={0}", listValidated));
                //}
                //else
                //{
                //    MessageBox.Show(Wpf.Touch.Base.ObservableItems.ObservableStringUtilities.FormatVisual("New selection={0} Not validated", listValidated));
                //}
            }
        }
    }
}
