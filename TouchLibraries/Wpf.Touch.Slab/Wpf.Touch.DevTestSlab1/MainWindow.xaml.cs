﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.Touch.Base;
using Wpf.Touch.Base.ObservableItems;
using Wpf.Touch.Slab;
using Wpf.Touch.Slab.ObservableItems;

namespace Wpf.Touch.DevTestSlab1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModelSlabItemSelector _ViewModelSlabItemSelector;
        StandardSlabResources _StandardSlabResources;

        public MainWindow()
        {
            InitializeComponent();

            _StandardSlabResources = new StandardSlabResources();
            _StandardSlabResources.LoadStandardResourceDictionary();


            _ViewModelSlabItemSelector = FactorySlabSetObserved.CreateNewMockViewModel();
            btnOpenSlabs.DataContext = _ViewModelSlabItemSelector;
            SlabSelector1.DataContext = _ViewModelSlabItemSelector;

            var testObservableValidationFromUserViewModel = new ObservableValidationFromUserViewModel();
            testObservableValidationFromUserViewModel.ValueValidationFromUser = WtbValidationFromUserMode.None;
            gridTestValidationFromUser.DataContext = testObservableValidationFromUserViewModel;
        }

        private void GridWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void btnLoadStandardDataTemplates_Click(object sender, RoutedEventArgs e)
        {
            _StandardSlabResources.TestAddOrRemove();
        }

        private void btnTestPropertyChange_Click(object sender, RoutedEventArgs e)
        {
            var v = _ViewModelSlabItemSelector;
            if (v == null)
                return;
            var s = v.Set;
            d--;
            foreach (var itm in s)
            {
                var sItem = itm as SlabItem;
                if (sItem == null)
                    continue;
                int slabDatabaseId;
                if(Wpf.Touch.Base.ObservableItems.WtbProxyLinks.TryGet(sItem.NpcObject, "Id", out slabDatabaseId))
                {
                    int index = Math.Max(0, slabDatabaseId + d);
                    string imagePath = string.Format(@"C:\testSlabSelector\{0}.jpg", index);
                    Wpf.Touch.Base.ObservableItems.WtbProxyLinks.TrySet(sItem.NpcObject, "SlabImage.ImagePath", imagePath);
                }
            }
        }
        int d = 0;

        private void btnOpenSlabs_Click(object sender, RoutedEventArgs e)
        {
            if(_ViewModelSlabItemSelector.IsSlabItemSelectorEnabledForVisualization)
            {
                _ViewModelSlabItemSelector.LoadSlabSelectionFromShapesToCreate(new List<string>() { "key0" }, null, 0, 100);
            }
            //else
            //{
            //    List<string> listValidated;
            //    if (_ViewModelSlabItemSelector.TryGetSelectionValidatedFromUser(out listValidated))
            //    {
            //        _ViewModelSlabItemSelector.PreviousSelectedItems = listValidated;
            //        MessageBox.Show(Wpf.Touch.Base.ObservableItems.ObservableStringUtilities.FormatVisual("New selection={0}", listValidated));
            //    }
            //    else
            //    {
            //        MessageBox.Show(Wpf.Touch.Base.ObservableItems.ObservableStringUtilities.FormatVisual("New selection={0} Not validated", listValidated));
            //    }
            //}
        }
    }
}
