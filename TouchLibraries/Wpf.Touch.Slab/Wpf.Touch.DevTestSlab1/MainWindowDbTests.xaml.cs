﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf.Touch.Slab;
using Wpf.Touch.Slab.ObservableItems;

namespace Wpf.Touch.DevTestSlab1
{
    /// <summary>
    /// Interaction logic for MainWindowDbTests.xaml
    /// </summary>
    public partial class MainWindowDbTests : Window
    {
        StandardSlabResources _StandardSlabResources;

        ViewModelSlabSetFromSql _VMSlabSetFromSql;
        ViewModelSlabSetFromSql VMSlabSetFromSql { get { return _VMSlabSetFromSql; } }
        ViewModelSlabItemSelector VMSlabItemSelector { get { return _VMSlabSetFromSql.VMSlabItemSelector; } }

        public MainWindowDbTests()
        {
            InitializeComponent();

            _StandardSlabResources = new StandardSlabResources();
            _StandardSlabResources.LoadStandardResourceDictionary();

            _VMSlabSetFromSql = new ViewModelSlabSetFromSql();
            _VMSlabSetFromSql.Initialize();

            var vm = VMSlabItemSelector;
            vm.CommandOnHasValidationFromUser = new Wpf.Common.Commands.DelegateCommand(new Action<object>(OnSlabSelectionDefinedFromUser));

            btnOpenSlabs.DataContext = vm;
            pppOpenSlabs.DataContext = vm;
            SlabSelector1.DataContext = vm;
            DialogSlabSelector1.DataContext = vm;

            Loaded += MainWindowDbTests_Loaded;
        }

        void MainWindowDbTests_Loaded(object sender, RoutedEventArgs e)
        {
            TestLoadSlabSelectionToEdit();
        }

        void OnSlabSelectionDefinedFromUser(object parameter)
        {
            List<string> listValidated;
            if (VMSlabItemSelector.TryGetSelectionValidatedFromUser(out listValidated))
            {
                MessageBox.Show(Wpf.Touch.Base.ObservableItems.ObservableStringUtilities.FormatVisual("New selection={0}", listValidated));
            }
        }

        private void GridWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void btnLoadStandardDataTemplates_Click(object sender, RoutedEventArgs e)
        {
            _StandardSlabResources.TestAddOrRemove();
        }

        private void btnTestPropertyChange_Click(object sender, RoutedEventArgs e)
        {
            var vm = VMSlabItemSelector;
            if (vm == null)
                return;
            var s = vm.Set;
            d--;
            foreach (var itm in s)
            {
                var sItem = itm as SlabItem;
                if (sItem == null)
                    continue;
                sItem.Test1();
            }
        }
        int d = 0;

        void TestLoadSlabSelectionToEdit()
        {

            var vm = VMSlabItemSelector;
            vm.LoadSlabSelectionFromShapesToCreate(new List<string>() { "SLAB00000000000740" }, "MAT00000001", 35, VMSlabSetFromSql.CutSchemeSqlParameters.ToleranceRangeDeviationForSlabRetrievalFromShapeDimZ);
        }

        private void btnOpenSlabs_Click(object sender, RoutedEventArgs e)
        {
            var vm = VMSlabItemSelector;
            if (vm.IsSlabItemSelectorEnabledForVisualization)
            {
                vm.LoadSlabSelectionFromShapesToCreate(new List<string>() { "cas" }, "MAT00000001", 20, VMSlabSetFromSql.CutSchemeSqlParameters.ToleranceRangeDeviationForSlabRetrievalFromShapeDimZ);
            }
            else
            {
                MessageBox.Show("Use this.OnSlabSelectionDefinedFromUser after ok");
                //List<string> listValidated;
                //if(_ViewModelSlabItemSelector.TryGetSelectionValidatedFromUser(out listValidated))
                //{
                //    _ViewModelSlabItemSelector.PreviousSelectedItems = listValidated;
                //    MessageBox.Show(Wpf.Touch.Base.ObservableItems.ObservableStringUtilities.FormatVisual("New selection={0}", listValidated));
                //}
                //else
                //{
                //    MessageBox.Show(Wpf.Touch.Base.ObservableItems.ObservableStringUtilities.FormatVisual("New selection={0} Not validated", listValidated));
                //}
            }
        }
    }
}
