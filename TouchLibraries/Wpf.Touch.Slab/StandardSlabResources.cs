﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Wpf.Touch.Slab
{
    class WpfResourceDictionary
    {
        private string _AssemblyfullName = null;
        public string AssemblyfullName { get { return _AssemblyfullName; } }

        private string _ResourceName = null;
        public string ResourceName { get { return _ResourceName; } }

        private ResourceDictionary _ResourceDictionary = null;
        public ResourceDictionary ResourceDictionary { get { return _ResourceDictionary; } }

        public static bool TryCreateNew(string assemblyfullName, string resourceName, out WpfResourceDictionary wpfResourceDictionary)
        {
            wpfResourceDictionary = null;

            ResourceDictionary rd;
            if (TryLoadComponent(assemblyfullName, resourceName, out rd))
            {
                wpfResourceDictionary = new WpfResourceDictionary();
                wpfResourceDictionary._AssemblyfullName = assemblyfullName;
                wpfResourceDictionary._ResourceName = resourceName;
                wpfResourceDictionary._ResourceDictionary = rd;
            }

            return wpfResourceDictionary != null;
        }

        static bool TryLoadComponent(string assemblyfullName, string resourceName, out ResourceDictionary resourceDictionary)
        {
            resourceDictionary = null;
            var uriPath = "/" + assemblyfullName + ";component/" + resourceName;
            var oSkinResource = Application.LoadComponent(new Uri(uriPath, UriKind.RelativeOrAbsolute));
            if (oSkinResource != null)
            {
                resourceDictionary = oSkinResource as ResourceDictionary;
            }
            return resourceDictionary != null;
        }

        public bool IsSameDictionary(string assemblyfullName, string resourceName)
        {
            if (_AssemblyfullName.ToLowerInvariant() != assemblyfullName.ToLowerInvariant())
                return false;
            if (_ResourceName.ToLowerInvariant() != resourceName.ToLowerInvariant())
                return false;
            return true;
        }
    }

    public class StandardSlabResources
    {
        public void LoadStandardResourceDictionary()
        {
            SetCurrentResourceDictionary(KeyDictionary.GenericWpfCustomControls, "Wpf.CustomControls", "Themes/Generic.xaml");
            SetCurrentResourceDictionary(KeyDictionary.GenericSlabDatatemplate, "Wpf.Touch.Slab", "Themes/SlabObservableItemsDefault.xaml");
        }






        public enum KeyDictionary
        {
            None,
            GenericWpfCustomControls,
            GenericSlabDatatemplate,
        }

        object _LoadedResourcesLock = new object();
        Dictionary<KeyDictionary, WpfResourceDictionary> _LoadedResources = new Dictionary<KeyDictionary, WpfResourceDictionary>();

        public bool GetIsLoadedResourceDictionary(KeyDictionary keyDictionary)
        {
            var found = GetCurrentResourceDictionaryImpl(keyDictionary);
            return found != null;
        }
        public bool GetIsLoadedResourceDictionary(KeyDictionary keyDictionary, string assemblyfullName, string resourceName)
        {
            var found = GetCurrentResourceDictionaryImpl(keyDictionary);
            return found != null && found.IsSameDictionary(assemblyfullName, resourceName);
        }
        public void SetCurrentResourceDictionary(KeyDictionary keyDictionary, string assemblyfullName, string resourceName)
        {
            SetCurrentResourceDictionaryImpl(keyDictionary, assemblyfullName, resourceName);
        }

        WpfResourceDictionary GetCurrentResourceDictionaryImpl(KeyDictionary keyDictionary)
        {
            lock (_LoadedResourcesLock)
            {
                WpfResourceDictionary loadedResourceDictionary = null;
                if (_LoadedResources.ContainsKey(keyDictionary))
                {
                    loadedResourceDictionary = _LoadedResources[keyDictionary];
                }
                return loadedResourceDictionary;
            }
        }

        void SetCurrentResourceDictionaryImpl(KeyDictionary keyDictionary, string assemblyfullName, string resourceName)
        {
            lock (_LoadedResourcesLock)
            {
                if (GetIsLoadedResourceDictionary(keyDictionary, assemblyfullName, resourceName))
                    return;

                WpfResourceDictionary rd;
                if (WpfResourceDictionary.TryCreateNew(assemblyfullName, resourceName, out rd))
                {
                    Application.Current.Resources.MergedDictionaries.Add(rd.ResourceDictionary);
                    _LoadedResources[keyDictionary] = rd;
                }
            }
        }
        void RemoveLoadedResourceDictionaryImpl(KeyDictionary keyDictionary)
        {
            lock (_LoadedResourcesLock)
            {
                if (_LoadedResources.ContainsKey(keyDictionary))
                {
                    var p = _LoadedResources[keyDictionary];
                    _LoadedResources.Remove(keyDictionary);
                    Application.Current.Resources.MergedDictionaries.Remove(p.ResourceDictionary);
                }
            }
        }


        public void TestAddOrRemove()
        {
            lock (_LoadedResourcesLock)
            {
                if (GetIsLoadedResourceDictionary(KeyDictionary.GenericSlabDatatemplate))
                    RemoveLoadedResourceDictionaryImpl(KeyDictionary.GenericSlabDatatemplate);
                else
                    LoadStandardResourceDictionary();
            }
        }
    }
}
