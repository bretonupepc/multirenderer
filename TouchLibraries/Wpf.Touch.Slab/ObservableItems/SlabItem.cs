﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf.CustomControls.ObservableItems;
using Wpf.CustomControls.Utilities;

namespace Wpf.Touch.Slab.ObservableItems
{
    public class SlabItem : ObservableNpcObject
    {
        SlabItem()
        {

        }
        public static SlabItem CreateNew(System.ComponentModel.INotifyPropertyChanged npcObject, string key)
        {
            var oncpo = new SlabItem();
            SetValues(oncpo, npcObject, key);
            return oncpo;
        }
    }

    public class SlabImageMock : ObservableBase
    {
        private string _ImagePath = "pack://application:,,,/Wpf.Touch.Base;component/RESOURCES/IMAGES/FileLoadingNotExisting.png";
        public string ImagePath
        {
            get
            {
                return _ImagePath;
            }
            set
            {
                if (SetProperty(ref _ImagePath, value))
                {
                    OnPropertyChanged(() => ImagePath);
                }
            }
        }
        public string ImagePathIfExists
        {
            get
            {
                var p = ImagePath;
                if (System.IO.File.Exists(p))
                    return p;
                else
                    return "pack://application:,,,/Wpf.Touch.Base;component/RESOURCES/IMAGES/FileLoadingNotExisting.png";
            }
        }
    }

    public class SlabItemMock : ObservableBase
    {
        private string _Code = null;
        public string Code
        {
            get { return _Code; }
            set { SetProperty(ref _Code, value); }
        }

        private double _DimX = 0;
        public double DimX
        {
            get { return _DimX; }
            set { SetProperty(ref _DimX, value); }
        }

        private double _DimY = 0;
        public double DimY
        {
            get { return _DimY; }
            set { SetProperty(ref _DimY, value); }
        }

        private double _DimZ = 0;
        public double DimZ
        {
            get { return _DimZ; }
            set { SetProperty(ref _DimZ, value); }
        }

        private int _Id = -1;
        public int Id
        {
            get { return _Id; }
            set { SetProperty(ref _Id, value); }
        }

        private SlabImageMock _SlabImage = null;
        public SlabImageMock SlabImage
        {
            get { return _SlabImage; }
            set { SetProperty(ref _SlabImage, value); }
        }

        private DateTime _StartDate = DateTime.MinValue;
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { SetProperty(ref _StartDate, value); }
        }


        public override string ToString()
        {
            return ObservableStringUtilities.FormatVisual("Code='{0}' StartDate='{1}'", Code, StartDate);
        }
    }
}
