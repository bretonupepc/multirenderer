﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf.CustomControls.ObservableItems;
using Wpf.Touch.BusinessObjects.Serialization;

namespace Wpf.Touch.Slab.ObservableItems
{
    public class ViewModelSlabItemSelector : ObservableBase, IWtbItemSelectorViewModel, IWtbObservableNpcObjectSelectedViewModel, IWtbValidationFromUser
    {
        private System.Collections.IEnumerable _Set = SlabSet.CreateNew(new List<SlabItem>());
        public System.Collections.IEnumerable Set
        {
            get { return _Set; }
            set 
            { 
                if(SetProperty(ref _Set, value))
                {
                    TrySetSelection(_Set as SlabSet, _PreviousSelectedItems);
                }
            }
        }

        private List<string> _PreviousSelectedItems = new List<string>();
        public List<string> PreviousSelectedItems
        {
            get { return _PreviousSelectedItems; }
            set 
            {
                if(SetProperty(ref _PreviousSelectedItems, value))
                {
                    TrySetSelection(_Set as SlabSet, _PreviousSelectedItems);
                }
            }
        }

        private ObservableCollection<ObservableNpcObject> _SelectedItems = new ObservableCollection<ObservableNpcObject>();
        public ObservableCollection<ObservableNpcObject> SelectedItems
        {
            get { return _SelectedItems; }
            set { SetProperty(ref _SelectedItems, value); }
        }

        private DataSetClauseRetrievingResult _DataSetClauseRetrievingResult = DataSetClauseRetrievingResult.Empty;
        public DataSetClauseRetrievingResult DataSetClauseRetrievingResult
        {
            get { return _DataSetClauseRetrievingResult; }
            set { SetProperty(ref _DataSetClauseRetrievingResult, value); }
        }

        private DataSetSource _DataSetSource = DataSetSource.Empty;
        public DataSetSource DataSetSource
        {
            get { return _DataSetSource; }
            set { SetProperty(ref _DataSetSource, value); }
        }

        private bool _IsMock = false;
        public bool IsMock
        {
            get { return _IsMock; }
            set { SetProperty(ref _IsMock, value); }
        }

        private bool _IsSelectionMultipleEnabled = true;
        public bool IsSelectionMultipleEnabled
        {
            get { return _IsSelectionMultipleEnabled; }
            set { SetProperty(ref _IsSelectionMultipleEnabled, value); }
        }

        private bool _HasValidationFromUser = false;
        public bool HasValidationFromUser
        {
            get { return _HasValidationFromUser; }
            set 
            { 
                if(SetProperty(ref _HasValidationFromUser, value))
                {
                    IsSlabItemSelectorEnabledForVisualization = !value;
                }
            }
        }

        private WtbValidationFromUserMode _ValueValidationFromUser = WtbValidationFromUserMode.None;
        public WtbValidationFromUserMode ValueValidationFromUser
        {
            get { return _ValueValidationFromUser; }
            set { SetProperty(ref _ValueValidationFromUser, value); }
        }

        private Wpf.Common.Commands.DelegateCommand _CommandOnHasValidationFromUser = null;
        public Wpf.Common.Commands.DelegateCommand CommandOnHasValidationFromUser
        {
            get { return _CommandOnHasValidationFromUser; }
            set { SetProperty(ref _CommandOnHasValidationFromUser, value); }
        }

        private bool _IsSlabItemSelectorEnabledForVisualization = false;
        public bool IsSlabItemSelectorEnabledForVisualization
        {
            get { return _IsSlabItemSelectorEnabledForVisualization; }
            set 
            { 
                if(SetProperty(ref _IsSlabItemSelectorEnabledForVisualization, value))
                {
                    HasValidationFromUser = !value;
                }
            }
        }

        public void OnHasValidationFromUser(IWtbValidationFromUser vm)
        {

        }

        public void Load()
        {
            HasValidationFromUser = false;
            if (TryLoadMock())
                return;

            var source = DataSetSource.Get(DataSetClauseRetrievingResult);
            Set = FactorySlabSetObserved.CreateNewSlabSetFromSlabOperateEntity(source);
        }

        public void LoadSlabSelectionFromShapesToCreate(List<string> selectedCodes, string materialCode = null, double? shapeDimZ = null, double? toleranceRangeDeviationForSlabRetrievalFromShapeDimZ = null, bool? onlyIfHasImage = null)
        {
            LoadImpl(selectedCodes, DataSetClauseMarkupFactory.GetFilterForSlabSelectionFromShapesToCreate(materialCode, shapeDimZ, toleranceRangeDeviationForSlabRetrievalFromShapeDimZ, onlyIfHasImage));
        }
        void LoadImpl(List<string> selectedCodes, string filterMarkupString)
        {
            if (TryLoadMock(selectedCodes))
                return;

            PreviousSelectedItems = selectedCodes;
            Set = SlabSet.CreateNew(new List<SlabItem>());
            DataSetClauseRetrievingResult.SetFromMarkupString(filterMarkupString);
            var source = DataSetSource.Get(DataSetClauseRetrievingResult);
            Set = FactorySlabSetObserved.CreateNewSlabSetFromSlabOperateEntity(source);
        }

        bool TryLoadMock(List<string> selectedCodes = null)
        {
            if (_IsMock)
            {
                if (selectedCodes == null)
                    selectedCodes = new List<string>();
                PreviousSelectedItems = selectedCodes;

                Set = FactorySlabSetObserved.CreateNewMockSlabSet();
                return true;
            }
            return false;
        }


        bool TrySetSelection(SlabSet slabSet, List<string> selectedCodes)
        {
            if (selectedCodes == null)
                return false;

            List<ObservableNpcObject> listToSelect = new List<ObservableNpcObject>();
            foreach(var key in selectedCodes)
            {
                var found = slabSet.FirstOrDefault(itm => itm.IsSameObservableNpcObject(key));
                if(found != null)
                {
                    listToSelect.Add(found);
                }
            }

            SelectedItems.Clear();
            foreach (var itmToSelect in listToSelect)
                SelectedItems.Add(itmToSelect);

            return true;
        }
        public bool TryGetSelectionValidatedFromUser(out List<string> selectedCodes)
        {
            selectedCodes = new List<string>();
            if (!HasValidationFromUser || ValueValidationFromUser != WtbValidationFromUserMode.Yes)
                return false;

            var selected = SelectedItems;
            if (selected == null)
                return false;

            var list = selected.ToList();
            var l = new List<string>();
            foreach (var itm in list)
                l.Add(itm.KeyObservableNpcObject);
            selectedCodes = l;

            return true;
        }
    }


    public static class DataSetClauseMarkupFactory
    {
        public static string GetFilterForSlabSelectionFromShapesToCreate(string materialCode = null, double? shapeDimZ = null, double? toleranceRangeDeviationForSlabRetrievalFromShapeDimZ = null, bool? onlyIfHasImage = null)
        {
            double eps_mm = 0.001;

            StringBuilder sb = null;

            if(!string.IsNullOrWhiteSpace(materialCode))
            {
                AppendWithAndConnection(ref sb, string.Format("ThicknessObject.Material.Code == {0}", materialCode));
            }
            
            if(shapeDimZ.HasValue)
            {
                var dimZ = shapeDimZ ?? 0;
                var halfInterval = toleranceRangeDeviationForSlabRetrievalFromShapeDimZ ?? 0;
                double minDimZ = dimZ - halfInterval - eps_mm;
                double maxDimZ = dimZ + halfInterval + eps_mm;
                AppendWithAndConnection(ref sb, string.Format("DimZ > {0} && DimZ < {1}", minDimZ, maxDimZ));
            }

            if (onlyIfHasImage ?? false)
            {
                AppendWithAndConnection(ref sb, string.Format("SlabImageId > -1"));
            }

            string markupString = sb.ToString() ?? string.Empty;

            return markupString;
        }

        static void AppendWithAndConnection(ref StringBuilder sb, string expressionToAppend)
        {
            if (sb == null)
                sb = new StringBuilder();
            else
                sb.Append(" && ");
            sb.Append(expressionToAppend);
        }
    }
}
