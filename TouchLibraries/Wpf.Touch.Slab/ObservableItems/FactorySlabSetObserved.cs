﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf.CustomControls.ObservableItems;

namespace Wpf.Touch.Slab.ObservableItems
{
    public class FactorySlabSetObserved
    {
        public static ViewModelSlabItemSelector CreateNewMockViewModel()
        {
            ViewModelSlabItemSelector slabItemSelectorViewModel = new ViewModelSlabItemSelector()
            {
                IsMock = true,
                Set = CreateNewMockSlabSet(),
            };
            return slabItemSelectorViewModel;
        }
        public static ViewModelSlabItemSelector CreateNewViewModel(Wpf.Touch.BusinessObjects.Serialization.DataSetSource dataSetSource, Wpf.Touch.BusinessObjects.Serialization.DataSetClauseRetrievingResult dataSetClauseRetrievingResult)
        {
            ViewModelSlabItemSelector slabItemSelectorViewModel = new ViewModelSlabItemSelector()
            {
                DataSetSource = dataSetSource,
                DataSetClauseRetrievingResult = dataSetClauseRetrievingResult,
            };
            return slabItemSelectorViewModel;
        }

        public static SlabSet CreateNewMockSlabSet()
        {
            List<SlabItem> l = new List<SlabItem>();
            for (int i = 0; i < 1000; i++)
            {
                var s = new SlabItemMock()
                {
                    //IsVisibleRequestActive = true,
                    Id = i,
                    Code = "key" + i,
                    StartDate = DateTime.Now,
                };
                s.SlabImage = new SlabImageMock();
                s.SlabImage.ImagePath = string.Format(@"C:\testSlabSelector\{0}.jpg", i);
                var op = SlabItem.CreateNew(s, s.Code);
                op.NpcObject = s;
                l.Add(op);
            }
            var slabSet = SlabSet.CreateNew(l);
            return slabSet;
        }

        public static SlabSet CreateNewSlabSetFromSlabOperateEntity(IEnumerable<System.ComponentModel.INotifyPropertyChanged> source)
        {
            //Dictionary<string, string> sourceToProxy = new Dictionary<string, string>()
            //{
            //    {"Code", "SlabCode"},
            //    {"Length", "DimX"},
            //    {"Width", "DimY"},
            //    {"Thickness", "DimZ"},
            //    {"StartDate", "StartDate"},
            //    {"SlabImage.ImagePath", "ImagePath"},
            //};
            //Dictionary<string, string> proxyToSource = null;
            //var pl = Wpf.Touch.Base.ObservableItems.ProxyLinks.CreateNew(sourceToProxy, proxyToSource);

            var l = new List<SlabItem>();
            foreach (var itm in source)
            {
                string code;
                if(WtbProxyLinks.TryGet(itm, "Code", out code))
                {
                    SlabItem op = SlabItem.CreateNew(itm, code);
                    op.NpcObject = itm;
                    l.Add(op);
                }
            }
            var slabSet = SlabSet.CreateNew(l);
            return slabSet;
        }
    }
}
