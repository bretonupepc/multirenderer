﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf.CustomControls.ObservableItems;

namespace Wpf.Touch.Slab.ObservableItems
{
    public class SlabSet : IEnumerable<ObservableNpcObject>
    {
        private List<ObservableNpcObject> _SlabSet;
        public static SlabSet CreateNew(IEnumerable<ObservableNpcObject> collectionOfSlabs)
        {
            SlabSet slabSet = new SlabSet();
            slabSet._SlabSet = collectionOfSlabs.ToList();
            return slabSet;
        }

        public IEnumerator<ObservableNpcObject> GetEnumerator()
        {
            return _SlabSet.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    //public class SlabItemEnumerator : System.Collections.IEnumerator
    //{
    //    SlabItem[] _SlabSet;
    //    int _CurrentIndex;

    //    public static SlabItemEnumerator CreateNew(SlabSet slabSet)
    //    {
    //        SlabItemEnumerator slabItemEnumerator = new SlabItemEnumerator();
    //        slabItemEnumerator._SlabSet = slabSet._SlabSet;
    //        slabItemEnumerator._CurrentIndex = -1;
    //        return slabItemEnumerator;
    //    }

    //    SlabItem GetCurrentImpl()
    //    {
    //        var slabSet = _SlabSet;
    //        var i = _CurrentIndex;

    //        if (slabSet == null)
    //            return null;

    //        var c = slabSet.Length;
    //        if (c == 0)
    //            return null;

    //        if (i < c)
    //            return slabSet[i];

    //        return null;
    //    }

    //    bool TryMoveNextImpl(out int newValue)
    //    {
    //        var slabSet = _SlabSet;
    //        var i = _CurrentIndex;
    //        newValue = i;

    //        if (slabSet == null)
    //            return false;

    //        var c = slabSet.Length;
    //        if (c == 0)
    //            return false;

    //        if (i < c - 1)
    //        {
    //            newValue = i + 1;
    //            return true;
    //        }

    //        return false;
    //    }

    //    public object Current { get { return GetCurrentImpl(); } }

    //    public bool MoveNext()
    //    {
    //        int newValue;
    //        bool ok = TryMoveNextImpl(out newValue);
    //        _CurrentIndex = newValue;
    //        return ok;
    //    }

    //    public void Reset()
    //    {
    //        _CurrentIndex = -1;
    //    }

    //    public void Dispose()
    //    {

    //    }
    //}
}
