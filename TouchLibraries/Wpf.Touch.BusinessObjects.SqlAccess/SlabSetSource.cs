﻿using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer;
using SlabOperate.Business.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wpf.Touch.BusinessObjects.Serialization;

namespace Wpf.Touch.BusinessObjects.SqlAccess
{
    public class SqlAccessInfo
    {
        private string _UdlFilename = "DbConnection.udl";
        public string UdlFilename
        {
            get { return _UdlFilename; }
            set { _UdlFilename = value; }
        }

        public override string ToString()
        {
            return string.Format("Wpf.Touch.BusinessObjects.SqlAccess SqlAccessInfo.UdlFilename='{0}'", UdlFilename);
        }

        public static FilterClause CreateFilterClauseRetrievingNothing()
        {
            return new FilterClause(new FieldItemBaseInfo("1"), ConditionOperator.Eq, "2");
        }
    }

    public class SlabSetClauseRetrievingResult : Wpf.Touch.BusinessObjects.Serialization.DataSetClauseRetrievingResult
    {
        private FilterClause _FilterClauseRetrievingResult = null;
        public FilterClause FilterClauseRetrievingResult
        {
            get { return _FilterClauseRetrievingResult; }
            set { _FilterClauseRetrievingResult = value; }
        }

        public override bool IsEmpty
        {
            get
            {
                return string.IsNullOrWhiteSpace(_markupString);
            }
        }
        string _markupString = "";
        protected override string GetMarkupStringImpl()
        {
            return _markupString;
        }
        protected override void SetFromMarkupStringImpl(string markupString)
        {
            _markupString = markupString;

            //var filterClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Material.Code"), ConditionOperator.IsNotNull, null);
            //var secondClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"), ConditionOperator.Ge, 10);
            //secondClause.AddClause(LogicalConnector.And, new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"), ConditionOperator.Le, 40));
            //filterClause.AddClause(LogicalConnector.And, secondClause);
            //_FilterClauseRetrievingResult = filterClause;

            var sMarkupString = _markupString;

            FilterClause filterClause;
            if (!FilterClauseFromMarkup.TryGetClause(sMarkupString, out filterClause))
            {
                filterClause = SqlAccessInfo.CreateFilterClauseRetrievingNothing();
            }
            _FilterClauseRetrievingResult = filterClause;
        }
    }

    public class SlabSetSource : Wpf.Touch.BusinessObjects.Serialization.DataSetSource
    {
        private SqlAccessInfo _SqlAccessInfo;
        private PersistenceScope _dataScope;
        bool _initialized = false;

        public SlabSetSource(SqlAccessInfo sqlAccessInfo)
        {
            _SqlAccessInfo = sqlAccessInfo;
        }

        private void SetDataScope()
        {
            if (_initialized)
                return;

            _dataScope = new PersistenceScope("Main");
            var sqlProvider = _dataScope.Manager.AvailableProviders.FirstOrDefault(p => p.ProviderType == PersistenceProviderType.SqlServer);
            if (sqlProvider as SqlServerProvider != null)
            {
                (sqlProvider as SqlServerProvider).UdlFilename = _SqlAccessInfo.UdlFilename;
            }

            _initialized = true;
        }

        protected override IEnumerable<System.ComponentModel.INotifyPropertyChanged> GetImpl(DataSetClauseRetrievingResult dataSetClauseRetrievingResult)
        {
            SetDataScope();

            var slabs = new PersistableEntityCollection<SlabEntity>(_dataScope);

            ////Filtro per materiale con codice specifico
            ////var filterClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Material.Code"),
            ////    ConditionOperator.Eq, "MAT00000001");
            //var filterClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Material.Code"),
            //    ConditionOperator.IsNotNull, null);

            //// aggiungi condizione per spessore

            //var secondClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"),
            //    ConditionOperator.Ge, 10);
            //secondClause.AddClause(LogicalConnector.And,
            //    new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"),
            //        ConditionOperator.Le, 40));

            //filterClause.AddClause(LogicalConnector.And, secondClause);

            var filterClause = ((SlabSetClauseRetrievingResult)dataSetClauseRetrievingResult).FilterClauseRetrievingResult;

            SlabEntity[] result = null;
            if (slabs.GetItemsFromRepository(clause: filterClause))
                result = slabs.ToArray();
            if (result == null)
                result = new SlabEntity[0];
            return result;
        }
    }

    public class ParameterSetSource
    {
        private PersistableEntityCollection<ParameterEntity> _List;
        public PersistableEntityCollection<ParameterEntity> List { get { return _List; } }

        private SqlAccessInfo _SqlAccessInfo;
        public SqlAccessInfo SqlAccessInfo { get { return _SqlAccessInfo; } }

        private PersistenceScope _dataScope;
        bool _initialized = false;

        public ParameterSetSource(SqlAccessInfo sqlAccessInfo)
        {
            _SqlAccessInfo = sqlAccessInfo;
        }

        private void SetDataScope()
        {
            if (_initialized)
                return;

            _dataScope = new PersistenceScope("Main");
            var sqlProvider = _dataScope.Manager.AvailableProviders.FirstOrDefault(p => p.ProviderType == PersistenceProviderType.SqlServer);
            if (sqlProvider as SqlServerProvider != null)
            {
                (sqlProvider as SqlServerProvider).UdlFilename = _SqlAccessInfo.UdlFilename;
            }

            _initialized = true;
        }

        public ParameterSetForCutScheme CreateCutSchemeSqlParameters()
        {
            SetDataScope();
            var pec = new PersistableEntityCollection<ParameterEntity>(_dataScope);
            var filterClause = ParameterSetForCutScheme.CreateFilterClauseForAll();
            var retrieved = pec.GetItemsFromRepository(clause: filterClause);
            if (retrieved)
                _List = pec;
            else
                _List = null;
            var pss = ParameterSetForCutScheme.CreateNew(this);
            return pss;
        }
    }


    public class ParameterEntityNotFoundException : Exception
    {
        public ParameterEntityNotFoundException(string format, params object[] args) : base(string.Format(format, args)) { }

    }
    public class ParameterSetForCutScheme
    {
        List<ParameterEntity> _List;
        private SqlAccessInfo _SqlAccessInfo;

        public static ParameterSetForCutScheme CreateNew(ParameterSetSource pss)
        {
            var csp = new ParameterSetForCutScheme();
            csp._SqlAccessInfo = pss.SqlAccessInfo;
            var l = pss.List;
            if (l != null)
                csp._List = l.ToList();
            else
                csp._List = new List<ParameterEntity>();
            return csp;
        }


        static readonly List<string> Parameters = new List<string>()
        {
            "StepSpess",
        };

        static FilterClause CreateFilterClauseForItem(string parameterName)
        {
            var fc = new FilterClause(new FieldItemBaseInfo("Name"), ConditionOperator.Eq, parameterName);
            return fc;
        }
        public static FilterClause CreateFilterClauseForAll()
        {
            var l = Parameters.ToList();
            if (l.Count == 0)
                return SqlAccessInfo.CreateFilterClauseRetrievingNothing();
            FilterClause fc = CreateFilterClauseForItem(l[0]);
            for (int i = 1; i < l.Count; i++)
            {
                var parameterName = l[i];
                var fcc = CreateFilterClauseForItem(parameterName);
                fc.AddClause(LogicalConnector.Or, fcc);
            }
            return fc;
        }

        double GetDoubleOrException(string parameterName)
        {
            var found = FindItemOrException(parameterName);
            double dTmp;
            if (double.TryParse(found.Value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out dTmp))
                return dTmp;
            throw new ParameterEntityNotFoundException("Cannot GetDouble from _SqlAccessInfo='{0}' parameterName='{1}' parameter='{2}'", _SqlAccessInfo, parameterName, found);
        }

        ParameterEntity FindItemOrException(string parameterName)
        {
            var itm = FindItem(parameterName);
            if (itm == null)
                throw new ParameterEntityNotFoundException("Cannot retrieve _SqlAccessInfo='{0}' parameterName='{1}'", _SqlAccessInfo, parameterName);
            return itm;
        }

        ParameterEntity FindItem(string parameterName)
        {
            var l = _List;
            if (l == null)
                return null;
            var found = l.FirstOrDefault(itm => IsSameParameterName(itm.Name, parameterName));
            return found;
        }
        static bool IsSameParameterName(string s1, string s2)
        {
            return s1.Trim() == s2.Trim();
        }

        public double ToleranceRangeDeviationForSlabRetrievalFromShapeDimZ { get { return GetDoubleOrException("StepSpess") * 0.5; } }
    }
}
