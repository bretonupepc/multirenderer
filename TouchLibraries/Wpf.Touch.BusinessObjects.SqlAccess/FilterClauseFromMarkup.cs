﻿using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer;
using SlabOperate.Business.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wpf.Touch.BusinessObjects.Serialization;

namespace Wpf.Touch.BusinessObjects.SqlAccess
{
    public class ParseOfFilterClauseFromMarkupException : Exception { public ParseOfFilterClauseFromMarkupException(string stringToFormat, params object[] args) : base(string.Format(stringToFormat, args)) { } }
    internal static class FilterClauseDefinitions
    {
        public enum OperatorMode
        {
            None,
            And,
            Or,
        }
        public enum EqualityMode
        {
            None,
            Equal,
            NtEq,
            Gr,
            GrEq,
            Lw,
            LwEq,
        }
        public static List<EqualityMode> GetListEqualityModeHandled()
        {
            List<EqualityMode> l = new List<EqualityMode>();
            var values = Enum.GetValues(typeof(EqualityMode));
            for (int i = 0; i < values.Length; i++)
            {
                var eValue = (EqualityMode)values.GetValue(i);
                if (eValue != EqualityMode.None)
                    l.Add(eValue);
            }
            return l;
        }

        public static string GetToken(OperatorMode t)
        {
            switch (t)
            {
                case OperatorMode.And:
                    return "&&";
                case OperatorMode.Or:
                    return "||";
                default:
                    return null;
            }
        }
        public static string GetToken(EqualityMode t)
        {
            switch (t)
            {
                case EqualityMode.NtEq:
                    return "!=";
                case EqualityMode.Equal:
                    return "==";
                case EqualityMode.Gr:
                    return ">";
                case EqualityMode.GrEq:
                    return ">=";
                case EqualityMode.Lw:
                    return "<";
                case EqualityMode.LwEq:
                    return "<=";
                default:
                    return null;
            }
        }

        public static ConditionOperator GetConditionOperator(EqualityMode eMode)
        {
            switch (eMode)
            {
                case EqualityMode.Equal:
                    return ConditionOperator.Eq;
                case EqualityMode.NtEq:
                    return ConditionOperator.Ne;
                case EqualityMode.Gr:
                    return ConditionOperator.Gt;
                case EqualityMode.GrEq:
                    return ConditionOperator.Ge;
                case EqualityMode.Lw:
                    return ConditionOperator.Lt;
                case EqualityMode.LwEq:
                    return ConditionOperator.Le;
                default:
                    throw new ParseOfFilterClauseFromMarkupException("Cannot convert EqualityModeType='{0}'EqualityModeValue='{1}' to ConditionOperatorType='{2}'", typeof(EqualityMode), eMode, typeof(ConditionOperator));
            }
        }
    }
    internal static class FilterClauseParser
    {
        static bool TrySplit(string s, FilterClauseDefinitions.EqualityMode eMode, out string[] values)
        {
            return TrySplit(s, FilterClauseDefinitions.GetToken(eMode), out values);
        }
        static bool TrySplit(string s, FilterClauseDefinitions.OperatorMode oMode, out string[] values)
        {
            return TrySplit(s, FilterClauseDefinitions.GetToken(oMode), out values);
        }
        static bool TrySplit(string sToSplit, string splittingSeparator, out string[] values)
        {
            values = null;
            if (string.IsNullOrWhiteSpace(sToSplit))
                return false;

            var spl = sToSplit.Split(new string[] { splittingSeparator }, StringSplitOptions.None);
            if (spl != null && spl.Length > 0)
            {
                List<string> l = new List<string>();
                for (int i = 0; i < spl.Length; i++ )
                {
                    var s = spl[i];
                    if (string.IsNullOrWhiteSpace(s))
                        l.Add(null);
                    else
                        l.Add(s.Trim());
                }
                values = l.ToArray();
            }

            if (values.Length == 0)
                values = null;
            return values != null;
        }

        static bool TryGetFilterClause(string sToParse, out string leftParsed, out FilterClauseDefinitions.EqualityMode eModeParsed, out object oValue)
        {
            leftParsed = null;
            eModeParsed = FilterClauseDefinitions.EqualityMode.None;
            oValue = null;
            var l = FilterClauseDefinitions.GetListEqualityModeHandled();
            foreach (var eMode in l)
            {
                var teMode = FilterClauseDefinitions.GetToken(eMode);
                string[] splClause;
                if (sToParse.Contains(teMode))
                {
                    if (!TrySplit(sToParse, eMode, out splClause))
                        throw new ParseOfFilterClauseFromMarkupException("Cannot create filter clause with EqualityMode='{0}' for sToParse='{1}'", eMode, sToParse);

                    if (splClause.Length != 2)
                        throw new ParseOfFilterClauseFromMarkupException("Cannot create filter clause with EqualityMode='{0}' for sToParse='{1}' with resulting splClause='{2}'", eMode, sToParse, splClause);

                    leftParsed = splClause[0];
                    var rightParsed = splClause[1];

                    if (!TryParseValue(rightParsed, out oValue)) 
                        throw new ParseOfFilterClauseFromMarkupException("Operation of parse not completed on sToParse='{0}'", sToParse);

                    eModeParsed = eMode;
                    break;
                }
            }
            return eModeParsed != FilterClauseDefinitions.EqualityMode.None;
        }

        public static bool TryParseValue(string sToParse, out object valueParsed)
        {
            valueParsed = null;
            if (string.IsNullOrWhiteSpace(sToParse))
                return false;

            string sImplicit;
            
            sImplicit = "(double)";
            if (sToParse.StartsWith(sImplicit))
            {
                valueParsed = Convert.ToDouble(sToParse.Substring(sImplicit.Length, sToParse.Length - sImplicit.Length));
                return true;
            }
            sImplicit = "(int)";
            if (sToParse.StartsWith(sImplicit))
            {
                valueParsed = Convert.ToInt32(sToParse.Substring(sImplicit.Length, sToParse.Length - sImplicit.Length));
                return true;
            }
            sImplicit = "(bool)";
            if (sToParse.StartsWith(sImplicit))
            {
                valueParsed = Convert.ToBoolean(sToParse.Substring(sImplicit.Length, sToParse.Length - sImplicit.Length));
                return true;
            }
            sImplicit = "(string)";
            if (sToParse.StartsWith(sImplicit))
            {
                valueParsed = Convert.ToString(sToParse.Substring(sImplicit.Length, sToParse.Length - sImplicit.Length));
                return true;
            }

            if (sToParse.Contains("("))
                return false;

            valueParsed = sToParse;
            return true;
        }

        public static bool TryCreateFromMarkup(string markupString, out FilterClause filterClause)
        {
            filterClause = null;

            if (string.IsNullOrWhiteSpace(markupString))
                return false;

            var inputValue = markupString;

            FilterClause fcFromMarkup = null;
            string[] orItems;
            if (TrySplit(inputValue, FilterClauseDefinitions.OperatorMode.Or, out orItems))
            {
                List<FilterClause> lfcOr = new List<FilterClause>();
                for (int iOr = 0; iOr < orItems.Length; iOr++)
                {
                    var or = orItems[iOr];

                    string[] andItems;
                    if (TrySplit(or, FilterClauseDefinitions.OperatorMode.And, out andItems))
                    {
                        List<FilterClause> lfcAnd = new List<FilterClause>();
                        for (int iAnd = 0; iAnd < andItems.Length; iAnd++)
                        {
                            var and = andItems[iAnd];

                            string leftParsed;
                            object oValue;
                            FilterClauseDefinitions.EqualityMode eModeParsed;
                            if (TryGetFilterClause(and, out leftParsed, out eModeParsed, out oValue))
                            {
                                var fbi = new FieldItemBaseInfo(leftParsed);
                                var co = FilterClauseDefinitions.GetConditionOperator(eModeParsed);
                                var value = oValue;
                                var fc = new FilterClause(fbi, co, value);
                                lfcAnd.Add(fc);
                            }
                        }

                        if (lfcAnd.Count > 0)
                        {
                            var fcAnd = lfcAnd[0];
                            for (int i = 1; i < lfcAnd.Count; i++)
                            {
                                fcAnd.AddClause(LogicalConnector.And, lfcAnd[i]);
                            }
                            lfcOr.Add(fcAnd);
                        }
                    }
                }

                if (lfcOr.Count > 0)
                {
                    var fcOr = lfcOr[0];
                    for (int i = 1; i < lfcOr.Count; i++)
                    {
                        fcOr.AddClause(LogicalConnector.Or, lfcOr[i]);
                    }

                    fcFromMarkup = fcOr;
                }
            }

            filterClause = fcFromMarkup;

            return filterClause != null;
        }

    }
    public static class FilterClauseFromMarkup
    {
        public static bool TryGetClause(string markupString, out FilterClause filterClause)
        {
            return FilterClauseParser.TryCreateFromMarkup(markupString, out filterClause);
        }
    }
}
