﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Breton.ImportOffer;
using DbAccess;
using ImportOffer.Class;
using TraceLoggers;

namespace OfferManagerSimple
{
    public class Ord
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private bool _slabOperationShowing = false;
        //private BackgroundWorker _worker;

        #region Variables
        private DbInterface _dbInterface;
        private string mLanguage;
        private static bool formCreate = false;
        private bool mImperial;
        private OfferCollection mOffers;
        private Order oldO;
        private ArrayList mProjectsCode;
        private ArrayList mDetailsCode;
        private ArrayList mOldProjectsQty = new ArrayList();
        private ArrayList mNewProjectsQty = new ArrayList();
        private List<string> symbolsToImport = null;
        private OrderCollection mImportOrders;
        private OrderCollection mOrders;


        // Deferred instance

        private bool mProjectView = false;
        private System.IO.DirectoryInfo dirXml;
        private string tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
        private string mLastOrderMouseOver = "";
        private bool isModified = false;
        private int mTop = 30;
        //private FormOpenFileDialog controlex;

        private int mSlabWithoutPhoto = 0;
        private bool mOptimizerEnable = false;
        private bool mAllowStockMovement = true;

        private EventHandler leaveHandler;


        private bool _slabOperationsResizing = false;

        protected DbInterface DbInterface
        {
            get
            {
                if (_dbInterface == null)
                {
                    _dbInterface = new DbInterface();
                    _dbInterface.Open("", "DbConnection.udl");

                }
                return _dbInterface;
            }
        }

        #endregion Private Fields --------<
        #endregion Private fields  -----------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Crea l'ordine e tutti i progetti e pezzi che li compongono
        /// </summary>
        /// <param name="offer"></param>
        /// <param name="msg"></param>
        public bool MakeOrder(string offer, out Project.Message msg)
        {
            msg = Project.Message.NONE;
            Offer off = null;
            Order ord = new Order(DbInterface);
            mImportOrders = new OrderCollection(DbInterface, null);
            XmlDocument originalDoc = new XmlDocument();
            bool find = false;

            try
            {
                var offers = new OfferCollection(DbInterface, null);
                offers.GetSingleElementFromDb(offer);
                if (offers.Count == 0)
                    return false;
                else
                {
                    offers.MoveFirst();
                    offers.GetObject(out off);
                }

                //mOffers.MoveFirst();
                //while (!mOffers.IsEOF())
                //{
                //    mOffers.GetObject(out off);
                //    if (off.gCode.Trim() == offer.Trim())
                //    {
                //        find = true;
                //        break;
                //    }

                //    mOffers.MoveNext();
                //}
                //if (!find || off == null)
                //    return false;


                // Se l'offerta è da analizzare procedo con la semplice creazione dell'ordine da xml
                //if (off.gStateOff.Trim() == Offer.State.OFFER_TO_ANALYZE.ToString() || off.gStateOff.Trim() == Offer.State.OFFER_ERROR.ToString())
                {
                    offers.GetXmlParameterFromDb(off, tmpXmlDir + "\\" + off.gCode.Trim() + ".xml");
                    originalDoc.Load(tmpXmlDir + "\\" + off.gCode.Trim() + ".xml");
                    if (DeleteNodes(ref originalDoc))
                        originalDoc.Save(tmpXmlDir + "\\" + off.gCode.Trim() + "_mod.xml");

                    //Legge il file xml appena caricato dal db e imposta i valori interessati nell'oggetto off
                    if (ord.ReadFileXml(tmpXmlDir + "\\" + off.gCode.Trim() + ".xml", out msg))
                    {
                        // Creazione ordine eseguita con successo
                        // Genero il codice ordine modificando il prefisso del codice offerta                   
                        ord.gCode = Order.CODE_PREFIX + off.gCode.Replace(Offer.CODE_PREFIX, "");
                        ord.gDescription = off.gDescription;
                        ord.gStateOrd = Order.State.ORDER_LOADED.ToString();	// Stato ordine di default
                        ord.gOfferCode = off.gCode;							    // Offerta a cui è relativo l'ordine

                        mImportOrders.AddObject(ord);
                        if (!mImportOrders.UpdateDataToDb())
                            throw new ApplicationException("Error on order save");

                        if (msg == Project.Message.DA_CONFERMARE)
                            off.gNote = "Project to be confirmed" + Environment.NewLine;
                        off.gStateOff = Offer.State.OFFER_ANALYZED.ToString();
                        if (!offers.UpdateDataToDb())	//Aggiorno nel database l'offerta con lo stato modificato
                            throw new ApplicationException("Error on offer save");
                    }
                    else
                    {
                        throw new ApplicationException("Error on Orders Creation");
                    }
                }
                //else if (off.gStateOff.Trim() == Offer.State.OFFER_RELOADED.ToString())
                //{
                //    // Non è possibile trovare un ordine reloaded
                //}

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + ".MakeOrder: ", ex);

                //Creazione ordine fallita
                off.gStateOff = Offer.State.OFFER_ERROR.ToString();
                if (msg == Project.Message.NO_MATERIAL)
                    off.gNote += "Missing material" +  Environment.NewLine;
                if (msg == Project.Message.NO_CUSTOMER)
                    off.gNote += "Missing customer" + Environment.NewLine;
                if (msg == Project.Message.NO_CUT_TO_SIZE)
                    off.gNote += "Missing Cut size" + Environment.NewLine;

                mOffers.UpdateDataToDb();	//Aggiorno nel database l'offerta con lo stato modificato

                return false;
            }
            finally
            {
                originalDoc = null;
            }
        }

        /// <summary>
        /// Cancella tutti i nodi che non interessano a questo livello
        /// </summary>
        /// <param name="doc">xml document da modificare</param>
        /// <returns></returns>
        private bool DeleteNodes(ref XmlDocument doc)
        {
            ArrayList nodes = new ArrayList();
            XmlNode node = doc.SelectSingleNode("/EXPORT");

            try
            {
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (nd.Name.ToUpperInvariant() != "FABMASTER")
                        nodes.Add(nd);
                }

                node.RemoveAll();
                for (int i = 0; i < nodes.Count; i++)
                    node.AppendChild((XmlNode)nodes[i]);
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine(this.ToString() + ".DeleteNodes Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + ".DeleteNodes Error. ", ex);
                return false;
            }
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
