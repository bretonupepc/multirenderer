﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Breton.ImportOffer;
using DbAccess;
using ImportOffer.Class;
using TraceLoggers;

namespace OfferManagerSimple
{
    public class Import
    {
        #region >-------------- Constants and Enums

        /// <summary>
        /// Tipi di errori possibili
        /// </summary>
        public  enum ErrorType
        {
            NONE,
            NO_NEWER_FILE,
            RELOAD_OFFER,
            GENERAL,
            NO_ALL_EXECUTE
        }

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private DbInterface _dbInterface;

        private OfferCollection mOffers;

        private OrderCollection mImportOrders;

        private OrderCollection mOrders;

        private System.IO.DirectoryInfo dirXml;
        private string tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";

        protected DbInterface DbInterface
        {
            get
            {
                if (_dbInterface == null)
                {
                    _dbInterface = new DbInterface();
                    _dbInterface.Open("", "DbConnection.udl");

                }
                return _dbInterface;
            }
        }

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Importa l'offerta
        /// </summary>
        /// <param name="filename">Nome del file da importare</param>
        /// <param name="offercode"></param>
        /// <param name="reloaded"></param>
        /// <returns></returns>
        public bool ImportOffer(string filename, out string offercode, out bool reloaded, out ErrorType err)
        {
            XmlDocument originalDoc = new XmlDocument();
            Offer off = new Offer();
            Offer tmpO = new Offer();
            Offer.errorType error;

            err = ErrorType.NONE;
            offercode = null;
            reloaded = false;

            off = new Offer();
            off.ReadFileXml(filename, DbInterface, out error);

            //if (error == Offer.errorType.DUPLICATE_CODE)
            //{
            //    offercode = off.gCode;

            //    // Trova l'oggetto già esistente
            //    mOffers.MoveFirst();
            //    while (!mOffers.IsEOF())
            //    {
            //        mOffers.GetObject(out tmpO);
            //        if (tmpO.gCode.Trim() == off.gCode.Trim())
            //            break;
            //        mOffers.MoveNext();
            //    }

            //    string reloadingFile = tmpXmlDir + "\\" + off.gCode.Trim() + "_reload.xml";
            //    mOffers.GetXmlParameterFromDb(tmpO, reloadingFile);

            //    originalDoc.Load(filename);
            //    if (DeleteNodes(ref originalDoc))
            //        originalDoc.Save(tmpXmlDir + "\\" + off.gCode.Trim() + "_mod.xml");

            //    // Commentata la parte per il controllo dei file perchè se a volte viene reimportato un file non tutti i pezzi 
            //    // potrebbero essere pronti alla modifica e quindi dovrà essere possibile importarlo nuovamente

            //    //// Verifica se il nuovo file che si sta importando è identico a quello gia esistente
            //    //if (GenUtils.FileCompare(reloadingFile, tmpXmlDir + "\\" + off.gCode.Trim() + "_mod.xml"))
            //    //{
            //    //    err = ErrorType.NO_NEWER_FILE;
            //    //    return false;
            //    //}

            //    // L'utente ha deciso di sovrascrivere comunque l'offerta
            //    tmpO.gInsertDate = mDbInterface.DateTimeNow;

            //    if (tmpO.gStateOff.Trim() == Offer.State.OFFER_TO_ANALYZE.ToString() ||
            //        tmpO.gStateOff.Trim() == Offer.State.OFFER_ERROR.ToString() ||
            //        tmpO.gStateOff.Trim() == Offer.State.OFFER_RELOADED.ToString())
            //    {
            //        tmpO.gXmlParameterLink = tmpXmlDir + "\\" + off.gCode.Trim() + "_mod.xml";
            //        if (tmpO.gStateOff.Trim() == Offer.State.OFFER_ERROR.ToString())
            //        {
            //            tmpO.gStateOff = Offer.State.OFFER_TO_ANALYZE.ToString();
            //            tmpO.gNote = "";
            //        }
            //    }
            //    else if (tmpO.gStateOff.Trim() == Offer.State.OFFER_ANALYZED.ToString())
            //    {
            //        if (off.gRevision >= 0)
            //        {
            //            if (off.gRevision > tmpO.gRevision)
            //                tmpO.gRevision = off.gRevision;
            //        }
            //        else
            //            tmpO.gRevision++;

            //        // Ricarica l'offerta
            //        if (!ReloadOrder(off, ref err))
            //        {
            //            err = Orders.ErrorType.RELOAD_OFFER;
            //            return false;
            //        }

            //        reloaded = true;

            //        tmpO.gCode = off.gCode;
            //        tmpO.gXmlParameterLink = tmpXmlDir + "\\" + off.gCode.Trim() + "_mod.xml";
            //    }
            //}
            //else
            {
                

                mOffers = new OfferCollection(DbInterface, null);
                offercode = off.gCode;

                originalDoc.Load(filename);
                if (DeleteNodes(ref originalDoc))
                    originalDoc.Save(tmpXmlDir + "\\" + off.gCode.Trim() + "_mod.xml");

                off.gStateOff = Offer.State.OFFER_TO_ANALYZE.ToString();
                off.gXmlParameterLink = tmpXmlDir + "\\" + off.gCode.Trim() + "_mod.xml";
                off.gRevision = 0;
                mOffers.AddObject(off);
            }

            if (!mOffers.UpdateDataToDb())
            {
                err = ErrorType.GENERAL;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Cancella tutti i nodi che non interessano a questo livello
        /// </summary>
        /// <param name="doc">xml document da modificare</param>
        /// <returns></returns>
        private bool DeleteNodes(ref XmlDocument doc)
        {
            ArrayList nodes = new ArrayList();
            XmlNode node = doc.SelectSingleNode("/EXPORT");

            try
            {
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (nd.Name.ToUpperInvariant() != "FABMASTER")
                        nodes.Add(nd);
                }

                node.RemoveAll();
                for (int i = 0; i < nodes.Count; i++)
                    node.AppendChild((XmlNode)nodes[i]);
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine(this.ToString() + ".DeleteNodes Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + ".DeleteNodes Error. ", ex);
                return false;
            }
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
