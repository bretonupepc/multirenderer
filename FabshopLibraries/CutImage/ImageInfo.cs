﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breton.CutImages
{
    public class ImageInfo
    {
        private string _imagePath = string.Empty;
        private int _imageWidth;
        private int _imageHeight;

        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

        public int ImageWidth
        {
            get { return _imageWidth; }
            set { _imageWidth = value; }
        }

        public int ImageHeight
        {
            get { return _imageHeight; }
            set { _imageHeight = value; }
        }
    }
}
