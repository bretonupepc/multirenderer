﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Breton.MathUtils;

namespace Breton.CutImages
{
    internal class DekImageWrapper
    {

        #region Structs & Enums

        private struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        private struct point
        {
            public int x;
            public int y;
        }

        public struct DEK_PATTERN_RES
        {
            public int pattern_index;
            public int X;
            public int Y;
            private int Filler;				// campo fittizio di allineamento dati VB/C++
        }


        public enum EN_DEK_IMAGE_FORMATS
        {
            EN_DEK_IMAGE_FORMAT_UNKNOWN = 0,
            EN_DEK_IMAGE_FORMAT_BMP,
            EN_DEK_IMAGE_FORMAT_GIF,
            EN_DEK_IMAGE_FORMAT_JPG,
            EN_DEK_IMAGE_FORMAT_PNG,
            EN_DEK_IMAGE_FORMAT_ICO,
            EN_DEK_IMAGE_FORMAT_TIF,
            EN_DEK_IMAGE_FORMAT_TGA,
            EN_DEK_IMAGE_FORMAT_PCX,
            EN_DEK_IMAGE_FORMAT_WBMP,
            EN_DEK_IMAGE_FORMAT_WMF,
            EN_DEK_IMAGE_FORMAT_JP2,
            EN_DEK_IMAGE_FORMAT_JPC,
            EN_DEK_IMAGE_FORMAT_PGX,
            EN_DEK_IMAGE_FORMAT_PNM,
            EN_DEK_IMAGE_FORMAT_RAS
        }

        public enum EN_DEK_INT_METHODS
        {
            EN_DEK_INT_METHOD_NEAREST_NEIGHBOUR = 1,
            EN_DEK_INT_METHOD_BILINEAR,
            EN_DEK_INT_METHOD_BSPLINE,
            EN_DEK_INT_METHOD_BICUBIC,
            EN_DEK_INT_METHOD_BICUBIC2,
            EN_DEK_INT_METHOD_LANCZOS,
            EN_DEK_INT_METHOD_BOX,
            EN_DEK_INT_METHOD_HERMITE,
            EN_DEK_INT_METHOD_HAMMING,
            EN_DEK_INT_METHOD_SINC,
            EN_DEK_INT_METHOD_BLACKMAN,
            EN_DEK_INT_METHOD_BESSEL,
            EN_DEK_INT_METHOD_GAUSSIAN,
            EN_DEK_INT_METHOD_QUADRATIC,
            EN_DEK_INT_METHOD_MITCHELL,
            EN_DEK_INT_METHOD_CATROM
        }

        public enum EN_DEK_AUTOCORRECTION_METHODS
        {
            EN_DEK_AUTOCORRECTION_STRETCH = 0,
            EN_DEK_AUTOCORRECTION_EQUALIZE,
            EN_DEK_AUTOCORRECTION_NORMALIZE,
            EN_DEK_AUTOCORRECTION_LOG,
            EN_DEK_AUTOCORRECTION_ROOT
        }

        public enum EN_DEK_ERRORS
        {
            EN_DEK_ERR_OK = 0,
            EN_DEK_ERR_CREATE_IMAGE1,
            EN_DEK_ERR_CREATE_IMAGE2,
            EN_DEK_ERR_LOAD_IMAGE,
            EN_DEK_ERR_SIZE_IMAGE,
            EN_DEK_ERR_COPY_IMAGE,
            EN_DEK_ERR_SAVE_IMAGE,
            EN_DEK_ERR_PAINT_IMAGE,
            EN_DEK_ERR_LENS_CORRECTION,
            EN_DEK_ERR_LIGHT_AUTOCORRECTION,
            EN_DEK_ERR_LIGHT_SET,
            EN_DEK_ERR_COMPUTE_PERSPECTIVE,
            EN_DEK_ERR_PERSPECTIVE_CORRECTION,
            EN_DEK_ERR_ROTATE,
            EN_DEK_ERR_GENERIC,
            EN_DEK_ERR_NUM_OF
        }


        public enum DKI_DISPLAY
        {
            DKI_DISPLAY_CONF_IMAGE,
            DKI_DISPLAY_MOD_IMAGE,
            DKI_DISPLAY_MIX_IMAGE,
            DKI_DISPLAY_CROP_IMAGE
        }

        #endregion

        private const string DEK_IMAGE_DLL_FILE_NAME = "Dek_Image.dll";


        static DekImageWrapper()
        {
            // Caricamento delle dll COM in base alla piattaforma di esecuzione (32 o 64)
            // 1. si prova a cercare la libreria nelle cartelle lib o lib64, utilizzando lo stesso nome del file
            // 2. se non esiste la cartella si cerca la libreria nella directory root 
            string path = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string subfolder = Environment.Is64BitProcess ? "\\lib64\\" : "\\lib\\";
            string fileName = DEK_IMAGE_DLL_FILE_NAME;
            if (!Directory.Exists(path + subfolder))
            {
                subfolder = "\\";
                if (Environment.Is64BitProcess)
                    fileName = System.IO.Path.GetFileNameWithoutExtension(fileName) + "_64" + System.IO.Path.GetExtension(fileName);
            }
            LoadLibrary(path + subfolder + fileName);
        }


        #region DekImage.dll Imports

        [DllImport("kernel32.dll")]
        private static extern IntPtr LoadLibrary(string dllToLoad);


        //è stato cambiato il nome della variabile object --> obj
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Patterns")]
        private static extern int Dek_Image_Patterns(int obj,
            ref int pattern,
            int npatterns,
            ref DEK_PATTERN_RES results,
            int maxresults,
            int maxthreads);

        //è stato cambiato il nome della variabile object --> obj
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_AutoCorrection")]
        private static extern int Dek_Image_AutoCorrection(int obj,
            EN_DEK_AUTOCORRECTION_METHODS method);

        //è stato cambiato il nome della variabile object --> obj
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_ComputePerspective")]
        private static extern int Dek_Image_ComputePerspective(int obj,
            ref point wpoints,
            ref point ipoints,
            ref double pm,
            ref double dv);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Copy")]
        private static extern int Dek_Image_Copy(int from_object,
            int to_object);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Create")]
        private static extern int Dek_Image_Create(int from_object);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Destroy")]
        private static extern int Dek_Image_Destroy(int obj);

        //x,t default=0			dimx,dimy default=-1	pClipRett default=null	   bSmooth defaulft=0
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Display")]
        unsafe private static extern int Dek_Image_Display(int obj,
            IntPtr mhdc,
            int x,
            int y,
            int dimx,
            int dimy,
            RECT* pClipRett,
            int bSmooth);

        //è stato cambiato il nome della variabile object --> obj
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Lens_Correction")]
        private static extern int Dek_Image_Lens_Correction(int obj,
            EN_DEK_INT_METHODS method,
            double a_const,
            double b_const,
            double c_const,
            double d_const);

        //è stato cambiato il nome della variabile object --> obj
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Light")]
        private static extern int Dek_Image_Light(int obj,
            int brightness,
            int contrast);

        //è stato cambiato il nome della variabile object --> obj
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Load")]
        private static extern int Dek_Image_Load(int obj,
            EN_DEK_IMAGE_FORMATS img_format,
            string img_name);

        //è stato cambiato il nome della variabile object --> obj
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Perspective")]
        private static extern int Dek_Image_Perspective(int obj,
            EN_DEK_INT_METHODS method,
            ref point final_dimension,
            int offx,
            int offy,
            ref double pm,
            ref double dv);

        //è stato cambiato il nome della variabile object --> obj (quality dovrebbe essere
        //un parametro opzionale quality (default=0))
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Save")]
        private static extern int Dek_Image_Save(int obj,
            EN_DEK_IMAGE_FORMATS img_format,
            string img_name,
            int quality);

        //è stato cambiato il nome della variabile object --> obj 
        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Size")]
        private static extern int Dek_Image_Size(int obj,
            ref int length,
            ref int height);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Get_DPI")]
        private static extern int Dek_Image_Get_DPI(int obj,
            ref int dpi_x,
            ref int dpi_y);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Set_DPI")]
        private static extern int Dek_Image_Set_DPI(int obj,
            int dpi_x,
            int dpi_y);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Crop")]
        unsafe private static extern int Dek_Image_Crop(int src_obj,
            RECT* pClipRett,
            int dst_obj);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Mix")]
        private static extern int Dek_Image_Mix(int ref_object,
            int aux_object,
            int xoff, int yoff);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Vers")]
        private static extern int Dek_Image_Vers(StringBuilder sVers);

        [DllImport(@"dll\Dek_Image.dll", EntryPoint = "Dek_Image_Rotate")]
        private static extern int Dek_Image_Rotate(int obj,
            double angle);
        #endregion


        #region Variables

        private string msSrcFile;			//nome file immagine origine
        private string msDstFile;			//nome file immagine destinazione
        private int mlImage1;				//handle immagine originaria (o confermata)
        private int mlImage2;				//handle immagine trasformata
        private int mlImage3;				//handle immagine trasformata
        private int mlImage4;				//handle immagine trasformata
        private int mlBrightness;
        private int mlContrast;
        private double[] maPM = new double[9];
        private double[] maDV = new double[3];
        private int mlWidth1;				//larghezza immagine 1
        private int mlHeight1;				//altezza immagine 1
        private int mlWidth2;				//larghezza immagine 2
        private int mlHeight2;				//altezza immagine 2
        private int mlWidth3;				//larghezza immagine 3
        private int mlHeight3;				//altezza immagine 3
        private int mlWidth4;				//larghezza immagine 4
        private int mlHeight4;				//altezza immagine 4

        private int mlPerspCalcWidth;		//larghezza immagine calcolata per la correzione prospettica
        private int mlPerspCalcHeight;		//altezza immagine calcolata per la correzione prospettica		
        //private Mutex mutexDrawing = new Mutex();

        #endregion


        #region Constructors & Disposers

        //Costruttore
        public DekImageWrapper()
        {
            mlImage1 = mlImage2 = mlImage3 = mlImage4 = 0;
        }

        //Distruttore
        ~DekImageWrapper()
        {
            if (mlImage1 != 0) Dek_Image_Destroy(mlImage1);
            if (mlImage2 != 0) Dek_Image_Destroy(mlImage2);
            if (mlImage3 != 0) Dek_Image_Destroy(mlImage3);
            if (mlImage4 != 0) Dek_Image_Destroy(mlImage4);
        }

        #endregion


        //*************************************
        //	string gsVersion
        //	Ritorna la versione della DLL
        //	Parametri: no parametri
        //*************************************
        public static string gsVersion()
        {
            StringBuilder sVers = new StringBuilder(32);
            Dek_Image_Vers(sVers);
            return sVers.ToString().Trim();
        }

        ///***************************************************************************
        /// <summary>
        /// Ritorna l'handle dell'immagine richiesta
        /// </summary>
        /// <param name="veDkiDisplay">quale immagine considerare</param>
        /// <returns> maggiore di 0 = handle dell'immagine, 0 = immagine non esistente</returns>
        ///***************************************************************************
        public int glImage(DKI_DISPLAY veDkiDisplay)
        {
            int lImage;

            switch (veDkiDisplay)
            {
                case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                    lImage = mlImage1;
                    break;

                case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                    lImage = mlImage2;
                    break;

                case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                    lImage = mlImage3;
                    break;

                case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                    lImage = mlImage4;
                    break;

                default:
                    lImage = 0;
                    break;
            }

            return lImage;
        }

        //gbGetImageDimension:  sono stati modificati tutti i parametri che erano opzionali
        //						nella funzione VB6. Inserire il valore di default nelle chiamate
        //vbConfTransf(default=false)
        //**************************************************************************
        //  bool gbGetImageDimension
        //  Ritorna le dimensioni dell'immagine in pixel
        //  Parametri:
        //            rlWidth         : larghezza dell'immagine
        //            rlHeight        : altezza dell'immagine
        //            rnErr           : codice di errore ritornato
        //            veDkiDisplay    : quale immagine considerare
        //  Ritorna:
        //            TRUE    operazione eseguita correttamente
        //            FALSE   errore nel caricamento
        //'**************************************************************************
        public bool gbGetImageDimension(ref int rlWidth, ref int rlHeight,
            out EN_DEK_ERRORS rnErr, DKI_DISPLAY veDkiDisplay)
        {
            int lImage = 0;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;

            try
            {
                switch (veDkiDisplay)
                {
                    case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                        lImage = mlImage1;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                        lImage = mlImage2;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                        lImage = mlImage3;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                        lImage = mlImage4;
                        break;
                }

                //Legge le dimensioni dell'immagine iniziale
                if (Dek_Image_Size(lImage, ref rlWidth, ref rlHeight) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_SIZE_IMAGE;
                    throw new Exception();
                }
                else return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        //Variazione del tipo di parametro, da optional a val (default=false)
        //**************************************************************************
        //	int glWidth
        //	Ritorna la larghezza dell'immagine
        //	Parametri:
        //			veDkiDisplay    : quale immagine considerare
        //**************************************************************************
        public int glWidth(DKI_DISPLAY veDkiDisplay)
        {
            switch (veDkiDisplay)
            {
                case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                    return mlWidth1;

                case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                    return mlWidth2;

                case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                    return mlWidth3;

                case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                    return mlWidth4;
            }

            return 0;
        }

        //Variazione del tipo di parametro, da optional a val (default=false)
        //**************************************************************************
        // int glHeight
        // Ritorna l'altezza dell'immagine
        // Parametri:
        //			veDkiDisplay    : quale immagine considerare
        //**************************************************************************
        public int glHeight(DKI_DISPLAY veDkiDisplay)
        {
            switch (veDkiDisplay)
            {
                case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                    return mlHeight1;

                case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                    return mlHeight2;

                case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                    return mlHeight3;

                case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                    return mlHeight4;
            }

            return 0;
        }

        //**************************************************************************
        // int glBrightness
        // Ritorna la luminosità dell'immagine
        //**************************************************************************
        public int glBrightness()
        {
            return mlBrightness;
        }


        //**************************************************************************
        // int glContrast
        // Ritorna il contrasto dell'immagine
        //**************************************************************************
        public int glContrast()
        {
            return mlContrast;
        }

        //Variazione del tipo di parametro (rnErr), da optional byref a out
        //**************************************************************************
        // bool gbLoadImage
        // Carica l'immagine
        // Parametri:
        //           vsSrcFile   : nome file sorgente
        //           rnErr       : codice di errore ritornato
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nel caricamento
        //**************************************************************************
        public bool gbLoadImage(string vsSrcFile, out EN_DEK_ERRORS rnErr)
        {
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            try
            {
                //Distrugge l'immagine iniziale
                if (mlImage1 != 0)
                {
                    Dek_Image_Destroy(mlImage1);
                    mlImage1 = 0;
                }
                //Crea l'immagine iniziale
                mlImage1 = Dek_Image_Create(0);
                if (mlImage1 == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_CREATE_IMAGE1;
                    throw new Exception();
                }
                //Distrugge l'immagine trasformata
                if (mlImage2 != 0)
                {
                    Dek_Image_Destroy(mlImage2);
                    mlImage2 = 0;
                }
                //Crea l'immagine trasformata
                mlImage2 = Dek_Image_Create(0);
                if (mlImage2 == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_CREATE_IMAGE2;
                    throw new Exception();
                }
                //Carica l'immagine iniziale
                if (Dek_Image_Load(mlImage1, EN_DEK_IMAGE_FORMATS.EN_DEK_IMAGE_FORMAT_JPG, vsSrcFile) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_LOAD_IMAGE;
                    throw new Exception();
                }
                //Copia l'immagine iniziale su quella trasformata
                if (Dek_Image_Copy(mlImage1, mlImage2) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_COPY_IMAGE;
                    throw new Exception();
                }

                //Legge le dimensioni dell'immagine
                if (!gbGetImageDimension(ref mlWidth1, ref mlHeight1, out rnErr, DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE))
                {
                    throw new Exception();
                }

                mlWidth2 = mlWidth3 = mlWidth4 = mlWidth1;
                mlHeight2 = mlHeight3 = mlHeight4 = mlHeight1;

                mlBrightness = 0;
                mlContrast = 0;

                msSrcFile = vsSrcFile;

                return true;
            }
            catch (Exception ex)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        //Il parametro rnErr da Optional ByRef a out, vbConfTransf no Optional (default=false)
        //**************************************************************************
        // bool gbSaveImage
        // Salva l'immagine trasformata
        // Parametri:
        //			vsDstFile		: nome file destinazione
        //			quality			: qualità dell'immagine (0-100)
        //			rnErr			: codice di errore ritornato
        //			veDkiDisplay	: quale immagine considerare
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nel salvataggio
        //**************************************************************************
        public bool gbSaveImage(string vsDstFile, int quality, out EN_DEK_ERRORS rnErr, DKI_DISPLAY veDkiDisplay)
        {
            int lImage = 0;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;

            try
            {
                //Legge il nome del file di destinazione, se specificato
                if (vsDstFile != "") msDstFile = vsDstFile;

                switch (veDkiDisplay)
                {
                    case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                        lImage = mlImage1;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                        lImage = mlImage2;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                        lImage = mlImage3;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                        lImage = mlImage4;
                        break;
                }

                //Salva l'immagine
                if (Dek_Image_Save(lImage, EN_DEK_IMAGE_FORMATS.EN_DEK_IMAGE_FORMAT_JPG, msDstFile, quality) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_SAVE_IMAGE;
                    throw new Exception();
                }
                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        public bool gbSaveImage(string vsDstFile, out EN_DEK_ERRORS rnErr, DKI_DISPLAY veDkiDisplay)
        {
            return gbSaveImage(vsDstFile, 0, out rnErr, veDkiDisplay);
        }


        ////sono stati modificati i seguenti parametri da optional a non optional:
        ////rnErr(no default)
        ////vbConfTransf (defautl=false)
        ////vlLeft,vlTop,vlWidth,vlHeight (default=0)
        ////**************************************************************************
        //// unsafe bool gbPaintImage
        //// Disegna l'immagine trasformata
        //// Parametri:
        ////			vsDstFile		: nome file destinazione
        ////			rnErr			: codice di errore ritornato
        ////			veDkiDisplay	: quale immagine considerare
        //// Ritorna:
        ////           TRUE    operazione eseguita correttamente
        ////           FALSE   errore nel disegno
        ////**************************************************************************
        //unsafe public bool gbPaintImage(System.Windows.Forms.PictureBox rpctDest,
        //    System.Drawing.Graphics g,
        //    out EN_DEK_ERRORS rnErr,
        //    DKI_DISPLAY veDkiDisplay,
        //    int vlLeft,
        //    int vlTop,
        //    int vlWidth,
        //    int vlHeight)
        //{
        //    int lDimX = 0, lDimY = 0;
        //    int lImage = 0;
        //    RECT RectSrc;
        //    RECT* r;
        //    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
        //    int ret = 0;
        //    bool rc = true;


        //    IntPtr pctHdc = g.GetHdc();

        //    try
        //    {
        //        switch (veDkiDisplay)
        //        {
        //            case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
        //                lImage = mlImage1;
        //                break;

        //            case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
        //                lImage = mlImage2;
        //                break;

        //            case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
        //                lImage = mlImage3;
        //                break;

        //            case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
        //                lImage = mlImage4;
        //                break;
        //        }

        //        lDimX = rpctDest.Width;
        //        lDimY = rpctDest.Height;

        //        //Disegna solo il rettangolo specificato
        //        if (vlWidth > 0 && vlHeight > 0)
        //        {
        //            RectSrc.Left = vlLeft;
        //            RectSrc.Bottom = vlTop;
        //            RectSrc.Right = vlLeft + vlWidth;
        //            RectSrc.Top = vlTop + vlHeight;
        //            r = &RectSrc;
        //            if (Dek_Image_Display(lImage, pctHdc, 0, 0, lDimX, lDimY, r, 0) == 0)
        //            {
        //                rnErr = EN_DEK_ERRORS.EN_DEK_ERR_PAINT_IMAGE;
        //                throw new Exception();
        //            }
        //        }
        //        else	//Disegna tutta l'immagine
        //        {
        //            ret = Dek_Image_Display(lImage, pctHdc, 0, 0, lDimX, lDimY, null, 0);
        //            if (ret == 0)
        //            {
        //                rnErr = EN_DEK_ERRORS.EN_DEK_ERR_PAINT_IMAGE;
        //                throw new Exception();
        //            }
        //        }
        //        rc = true;
        //    }
        //    catch (Exception)
        //    {
        //        if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
        //        rc = false;
        //    }

        //    finally
        //    {
        //        g.ReleaseHdc(pctHdc);
        //    }

        //    return rc;
        //}




        unsafe public bool gbPaintImage(System.Drawing.Size rpctDest,
        System.Drawing.Graphics g,
        out EN_DEK_ERRORS rnErr,
        DKI_DISPLAY veDkiDisplay,
        int vlLeft,
        int vlTop,
        int vlWidth,
        int vlHeight)
        {
            int lDimX = 0, lDimY = 0;
            int lImage = 0;
            RECT RectSrc;
            RECT* r;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            int ret = 0;
            bool rc = true;


            IntPtr pctHdc = g.GetHdc();

            try
            {
                switch (veDkiDisplay)
                {
                    case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                        lImage = mlImage1;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                        lImage = mlImage2;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                        lImage = mlImage3;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                        lImage = mlImage4;
                        break;
                }

                lDimX = rpctDest.Width;
                lDimY = rpctDest.Height;

                //Disegna solo il rettangolo specificato
                if (vlWidth > 0 && vlHeight > 0)
                {
                    RectSrc.Left = vlLeft;
                    RectSrc.Bottom = vlTop;
                    RectSrc.Right = vlLeft + vlWidth;
                    RectSrc.Top = vlTop + vlHeight;
                    r = &RectSrc;
                    if (Dek_Image_Display(lImage, pctHdc, 0, 0, lDimX, lDimY, r, 0) == 0)
                    {
                        rnErr = EN_DEK_ERRORS.EN_DEK_ERR_PAINT_IMAGE;
                        throw new Exception();
                    }
                }
                else	//Disegna tutta l'immagine
                {
                    ret = Dek_Image_Display(lImage, pctHdc, 0, 0, lDimX, lDimY, null, 0);
                    if (ret == 0)
                    {
                        rnErr = EN_DEK_ERRORS.EN_DEK_ERR_PAINT_IMAGE;
                        throw new Exception();
                    }
                }
                rc = true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                rc = false;
            }

            finally
            {
                g.ReleaseHdc(pctHdc);
            }

            return rc;
        }


        //**************************************************************************
        // unsafe bool gbPaintImage
        // Disegna l'immagine trasformata
        // Parametri:
        //			vsDstFile		: nome file destinazione
        //			rnErr			: codice di errore ritornato
        //			veDkiDisplay	: quale immagine considerare
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nel disegno
        //**************************************************************************
        ////unsafe public bool gbPaintImage(System.Windows.Forms.PictureBox rpctDest, out EN_DEK_ERRORS rnErr,
        ////    DKI_DISPLAY veDkiDisplay, int vlLeft, int vlTop, int vlWidth, int vlHeight)
        ////{
        ////    System.Drawing.Graphics g = rpctDest.CreateGraphics();
        ////    if (gbPaintImage(rpctDest, g, out rnErr, veDkiDisplay, vlLeft, vlTop, vlWidth, vlHeight))
        ////        return true;
        ////    else
        ////        return false;

        ////}


        /**************************************************************************
         * gbGhostImage
         * Crea una copia dell'immagine
         * Parametri:
         *			rnErr	: codice di errore ritornato
         * Ritorna:
         *			true	operazione eseguita correttamente
         *			false	errore nella conferma dell'immagine
        '**************************************************************************/
        public bool gbGhostImage(out EN_DEK_ERRORS rnErr)
        {
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            try
            {
                if (mlImage4 == 0)
                {
                    mlImage4 = Dek_Image_Create(0);
                    if (mlImage4 == 0)
                    {
                        rnErr = EN_DEK_ERRORS.EN_DEK_ERR_CREATE_IMAGE2;
                        return false;
                    }
                }

                if (Dek_Image_Copy(mlImage2, mlImage4) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_COPY_IMAGE;
                    return false;
                }

                //Legge le dimensioni dell'immagine
                if (!gbGetImageDimension(ref mlWidth4, ref mlHeight4, out rnErr, DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE))
                    throw new Exception();

                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }

        }

        //Modificato il parametro da optional byref a out
        //**************************************************************************
        // bool gbConfirmImage
        // Conferma l'immagine trasformata
        // Parametri:
        //           rnErr           : codice di errore ritornato
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nella conferma dell'immagine
        //**************************************************************************
        public bool gbConfirmImage(out EN_DEK_ERRORS rnErr)
        {
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            try
            {
                //Copia l'immagine trasformata su quella iniziale
                if (Dek_Image_Copy(mlImage2, mlImage1) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_COPY_IMAGE;
                    throw new Exception();
                }
                //Legge le dimensioni dell'immagine
                if (!gbGetImageDimension(ref mlWidth1, ref mlHeight1, out rnErr, DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE)) throw new Exception();

                mlBrightness = 0;
                mlContrast = 0;

                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        //Modificato il parametro da optional byref a out
        //**************************************************************************
        // bool gbRestoreImage
        // Elimina le trasformazioni non confermate
        // Parametri:
        //           rnErr           : codice di errore ritornato
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nel ripristino dell'immagine
        //**************************************************************************
        public bool gbRestoreImage(out EN_DEK_ERRORS rnErr)
        {
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            try
            {
                //Copia l'immagine trasformata su quella iniziale
                if (Dek_Image_Copy(mlImage1, mlImage2) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_COPY_IMAGE;
                    throw new Exception();
                }
                //Legge le dimensioni dell'immagine
                if (!gbGetImageDimension(ref mlWidth2, ref mlHeight2, out rnErr, DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE)) throw new Exception();

                mlBrightness = 0;
                mlContrast = 0;

                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        // modificati i parametri opzionali. vbNormalize (default=true)
        // vnMethod=EN_DEK_INT_METHOD_BILINEAR
        //**************************************************************************
        // bool gbLensCorrection
        // Esegue la correzione della lente (effetto botte)
        // Parametri:
        //           vdA         : parametro A
        //           vdB         : parametro B
        //           vdC         : parametro C
        //           vdD         : parametro D
        //           vnMethod    : metodo di correzione
        //           vbNormalize : normalizza i parametri A, B, C, D
        //           rnErr       : codice di errore ritornato
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nella correzione della lente
        //**************************************************************************
        public bool gbLensCorrection(double vdA, double vdB, double vdC, double vdD,
            bool vbNormalize, EN_DEK_INT_METHODS vnMethod, out EN_DEK_ERRORS rnErr)
        {
            int rc;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;

            try
            {
                //Normalizza i valori a 1
                if (vbNormalize) vdD = 1 - vdA - vdB - vdC;
                //Esegue la correzione della lente
                rc = Dek_Image_Lens_Correction(mlImage2, vnMethod, vdA, vdB, vdC, vdD);
                if (rc == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_LENS_CORRECTION;
                    throw new Exception();
                }

                //Legge le dimensioni dell'immagine
                if (!gbGetImageDimension(ref mlWidth2, ref mlHeight2, out rnErr, DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE)) throw new Exception();

                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        //Entrambi i parametri erano opzionali:vnMethod (default=EN_DEK_AUTOCORRECTION_NORMALIZE)
        //rnErr --> out, (no default)
        //**************************************************************************
        // bool gbLightAutoCorrection
        // Esegue la correzione della luminosità
        // Parametri:
        //           vnMethod    : metodo di correzione
        //           rnErr       : codice di errore ritornato
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nella correzione della luminosità
        //**************************************************************************
        public bool gbLightAutoCorrection(EN_DEK_AUTOCORRECTION_METHODS vnMethod, out EN_DEK_ERRORS rnErr)
        {
            int rc;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;

            try
            {
                //Esegue la correzione della luminosità
                rc = Dek_Image_AutoCorrection(mlImage2, vnMethod);

                if (rc == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_LIGHT_AUTOCORRECTION;
                    throw new Exception();
                }
                mlBrightness = 0;
                mlContrast = 0;

                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        //il parametro rnErr è stato modificato da optional by ref a out
        //**************************************************************************
        // bool gbSetImageLight
        // Imposta la luminosità e il contrasto dell'immagine
        // Parametri:
        //           vlBrightness    : metodo di correzione
        //           vlContrast      : normalizza i parametri A, B, C, D
        //           rnErr           : codice di errore ritornato
        // Ritorna:
        //           TRUE    operazione eseguita correttamente
        //           FALSE   errore nel settaggio della luminosità
        //**************************************************************************
        public bool gbSetImageLight(int vlBrightness, int vlContrast, out EN_DEK_ERRORS rnErr)
        {
            return gbSetImageLight(vlBrightness, vlContrast, DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE, out rnErr);
        }
        public bool gbSetImageLight(int vlBrightness, int vlContrast, DKI_DISPLAY veDkiDisplay, out EN_DEK_ERRORS rnErr)
        {
            int rc;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            int lImage;

            try
            {
                switch (veDkiDisplay)
                {
                    case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                        lImage = mlImage1;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                        lImage = mlImage2;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                        lImage = mlImage3;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                        lImage = mlImage4;
                        break;

                    default:
                        return false;
                }

                //Setta la luminosità
                rc = Dek_Image_Light(lImage, vlBrightness, vlContrast);

                if (rc == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_LIGHT_SET;
                    throw new Exception();
                }

                mlBrightness = vlBrightness;
                mlContrast = vlContrast;
                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        //**************************************************************************
        // bool gbSetPerspective
        // Setta la prospettiva
        // Parametri:
        //			ralPtImage  : coordinate del rettangolo nell'immagine (in pixel)
        //			ralPtReal   : coordinate del rettangolo reale (in mm)
        //			vfPxRatio   : rapporto impostato fra pixel reali e pixel risultanti
        //			dpi			: dpi desiderato
        //			rnErr       : codice di errore ritornato
        // Ritorna:
        //			true	operazione eseguita correttamente
        //			false	errore nella correzione della lente
        //**************************************************************************
        public bool gbSetPerspective(ref int[] ralPtImage, ref int[] ralPtReal,
            float vfPxRatio, ref int dpi, out EN_DEK_ERRORS rnErr)
        {
            int rc;
            EN_DEK_ERRORS errVar;
            point[] PtImage = new point[8];
            point[] PtReal = new point[8];
            double[] adPM = new double[9];
            double[] adDV = new double[3];
            int i;
            int lWidth = 0, lHeight = 0;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            int lMaxX, lMaxY;
            int lMinX, lMinY;
            int newDpi = 0;

            try
            {
                gbGetImageDimension(ref lWidth, ref lHeight, out errVar, DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE);
                //internal computations assume origin (0,0) on lower left corner !!!!
                PtImage[0].x = ralPtImage[0];
                PtImage[0].y = ralPtImage[1];	//lHeight - ralPtImage[1] - 1&
                PtImage[1].x = ralPtImage[2];
                PtImage[1].y = ralPtImage[3];	//lHeight - ralPtImage[3] - 1&
                PtImage[2].x = ralPtImage[4];
                PtImage[2].y = ralPtImage[5];	//lHeight - ralPtImage[5] - 1&
                PtImage[3].x = ralPtImage[6];
                PtImage[3].y = ralPtImage[7];	//lHeight - ralPtImage(7) - 1&

                //internal computations assume origin (0,0) on lower left corner !!!!
                lMaxY = ralPtReal[1];
                if (lMaxY < ralPtReal[3]) lMaxY = ralPtReal[3];
                if (lMaxY < ralPtReal[5]) lMaxY = ralPtReal[5];
                if (lMaxY < ralPtReal[7]) lMaxY = ralPtReal[7];
                PtReal[0].x = ralPtReal[0];
                PtReal[0].y = ralPtReal[1];		//lMaxY - ralPtReal[1]
                PtReal[1].x = ralPtReal[2];
                PtReal[1].y = ralPtReal[3];		//lMaxY - ralPtReal[3]
                PtReal[2].x = ralPtReal[4];
                PtReal[2].y = ralPtReal[5];		//lMaxY - ralPtReal[5]
                PtReal[3].x = ralPtReal[6];
                PtReal[3].y = ralPtReal[7];		//lMaxY - ralPtReal(7)

                lMinX = 999999999;
                lMinY = 999999999;
                lMaxX = -999999999;
                lMaxY = -999999999;

                //Determina base e altezza del rettangolo in mm
                for (i = 0; i <= 3; i++)
                {
                    if (PtReal[i].x < lMinX) lMinX = PtReal[i].x;
                    if (PtReal[i].y < lMinY) lMinY = PtReal[i].y;
                    if (PtReal[i].x > lMaxX) lMaxX = PtReal[i].x;
                    if (PtReal[i].y > lMaxY) lMaxY = PtReal[i].y;
                }

                //Se è stato impostato un rapporto fra pixel reali e risultanti,
                //calcola i pixel risultanti
                mCalcResultPixels(ref PtImage, ref PtReal, vfPxRatio, dpi, ref mlPerspCalcWidth, ref mlPerspCalcHeight);

                // Calcola i dpi
                if (mlPerspCalcWidth != 0 && mlPerspCalcHeight != 0)
                    newDpi = (int)Math.Round((double)mlPerspCalcWidth / (double)lMaxX * 25.4);

                rc = Dek_Image_ComputePerspective(mlImage2, ref PtReal[0], ref PtImage[0], ref adPM[0], ref adDV[0]);
                if (rc == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_COMPUTE_PERSPECTIVE;
                    throw new Exception();
                }

                // Setta i dpi nell'immagine
                if (newDpi > 0)
                    dpi = newDpi;

                for (i = 0; i <= 8; i++) maPM[i] = adPM[i];

                for (i = 0; i <= 2; i++) maDV[i] = adDV[i];
                return true;
            }
            catch
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        //**************************************************************************
        // void mCalcResultPixels
        // Calcola i punti risultanti in base al rapporto tra pixel reali e pixel
        // risultanti
        // Parametri:
        //			rPtImage	: coordinate del rettangolo nell'immagine (in pixel)
        //			rPtReal		: coordinate del rettangolo reale (in mm)
        //			vfPxRatio	: rapporto impostato fra pixel reali e pixel risultanti
        //			dpi			: dpi desiderato
        //			rlFinalDimX	: dimensione finale X immagine in pixel
        //			rlFinalDimY	: dimensione finale Y immagine in pixel
        //'**************************************************************************
        private void mCalcResultPixels(ref point[] rPtImage, ref point[] rPtReal, float vfPxRatio, int dpi,
            ref int rlFinalDimX, ref int rlFinalDimY)
        {
            int i;
            double dAreaPxImg, dArea2, dAreaPxRes;
            int lMaxX, lMaxY;
            int lMinX, lMinY;
            double dWidth, dHeight;

            try
            {
                rlFinalDimX = 0;
                rlFinalDimY = 0;

                lMinX = 999999999;
                lMinY = 999999999;
                lMaxX = -999999999;
                lMaxY = -999999999;

                //Determina base e altezza del rettangolo in mm
                for (i = 0; i <= 3; i++)
                {
                    if (rPtReal[i].x < lMinX) lMinX = rPtReal[i].x;
                    if (rPtReal[i].y < lMinY) lMinY = rPtReal[i].y;
                    if (rPtReal[i].x > lMaxX) lMaxX = rPtReal[i].x;
                    if (rPtReal[i].y > lMaxY) lMaxY = rPtReal[i].y;
                }

                //Se è stato impostato un rapporto fra pixel reali e risultanti,
                //calcola i pixel risultanti
                if (vfPxRatio > 0)
                {
                    //Calcola l'area in pixel del quadrilatero dell'immagine
                    dAreaPxImg = 0;
                    for (i = 1; i < 3; i++)
                    {
                        dArea2 = (rPtImage[i].x - rPtImage[0].x) * (rPtImage[i + 1].y - rPtImage[0].y) -
                            (rPtImage[i + 1].x - rPtImage[0].x) * (rPtImage[i].y - rPtImage[0].y);

                        dAreaPxImg += dArea2;
                    }
                    dAreaPxImg = Math.Abs(dAreaPxImg / 2d);

                    //Calcola l'area in pixel risultante
                    dAreaPxRes = dAreaPxImg / vfPxRatio;

                    //Calcola la base del rettangolo risultante in pixel
                    dWidth = Math.Sqrt(dAreaPxRes * (lMaxX - lMinX) / (lMaxY - lMinY));

                    //Calcola l'altezza del rettangolo risultante in pixel
                    dHeight = dAreaPxRes / dWidth;

                    //Dimensioni finali immagine in pixel
                    rlFinalDimX = (int)(dWidth);
                    rlFinalDimY = (int)(dHeight);
                }

                //Se è stato impostato dpi desiderato, calcola i pixel risultanti
                else if (dpi > 0)
                {
                    rlFinalDimX = (int)Math.Round((double)lMaxX / 25.4 * (double)dpi);
                    rlFinalDimY = (int)Math.Round((double)lMaxY / 25.4 * (double)dpi);

                }

                //Modifica il rettangolo risultante
                if (rlFinalDimX != 0 && rlFinalDimY != 0)
                    for (i = 0; i <= 3; i++)
                    {
                        if (rPtReal[i].x == lMinX) rPtReal[i].x = 0;
                        if (rPtReal[i].y == lMinY) rPtReal[i].y = 0;
                        if (rPtReal[i].x == lMaxX) rPtReal[i].x = rlFinalDimX;
                        if (rPtReal[i].y == lMaxY) rPtReal[i].y = rlFinalDimY;
                    }
            }

            catch (Exception)
            {
            }
        }

        // sono stati modificati i 2 parametri vnMethod (default=EN_DEK_INT_METHOD_BILINEAR) da Optional --> No Optional
        // e rnErr da optional a out
        //**************************************************************************
        // Function gbPerspectiveCorrection
        // Esegue la correzione della prospettiva
        // Parametri:
        //			vlDimX		: dimensione X dell'immagine in mm
        //			vlDimY		: dimensione Y dell'immagine in mm
        //			vlFrameX	: cornice aggiuntiva X in mm
        //			vlFrameY	: cornice aggiuntiva Y in mm
        //			dpi			: dpi da impostare nella foto risultante
        //			vnMethod	: metodo di correzione
        //			rnErr		: codice di errore ritornato
        // Ritorna:
        //			true	operazione eseguita correttamente
        //			false	errore nella correzione della lente
        //'**************************************************************************
        public bool gbPerspectiveCorrection(int vlDimX, int vlDimY, int vlFrameX, int vlFrameY, int dpi,
            EN_DEK_INT_METHODS vnMethod, out EN_DEK_ERRORS rnErr)
        {
            int rc;
            point PtDim;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;

            try
            {
                //Prende le dimensioni calcolate precedentemente
                if (mlPerspCalcWidth > 0 && mlPerspCalcHeight > 0)
                {
                    PtDim.x = mlPerspCalcWidth + vlFrameX * 2;
                    PtDim.y = mlPerspCalcHeight + vlFrameY * 2;
                }
                else	//Oppure prende le dimensioni passate
                {
                    PtDim.x = vlDimX;
                    PtDim.y = vlDimY;
                }

                rc = Dek_Image_Perspective(mlImage2, vnMethod, ref PtDim, vlFrameX, vlFrameY, ref maPM[0], ref maDV[0]);
                if (rc == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_COMPUTE_PERSPECTIVE;
                    throw new Exception();
                }

                // Setta i DPI desiderati
                if (dpi > 0)
                    gbSetDPI(dpi);

                //Legge le dimensioni dell'immagine
                if (!gbGetImageDimension(ref mlWidth2, ref mlHeight2, out rnErr, DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE))
                {
                    throw new Exception();
                }
                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        /**************************************************************************
         * gbGetDPI
         * Restituisce la risoluzione delle foto
         * Parametri:
         *			vlDPI	: risoluzione in DPI
         * Ritorna:
         *			true	operazione eseguita correttamente
         *			false	errore durante l'operazione
         ***************************************************************************/
        public bool gbGetDPI(out int DPI)
        {
            int DPIx = 0, DPIy = 0;

            if (Dek_Image_Get_DPI(mlImage2, ref DPIx, ref DPIy) == 0)
            {
                DPI = 0;
                return false;
            }

            DPI = DPIx;
            return true;
        }

        /**************************************************************************
         * gbSetDPI
         * Imposta la risoluzione delle foto
         * Parametri:
         *			vlDPI	: risoluzione in DPI
         * Ritorna:
         *			true	operazione eseguita correttamente
         *			false	errore durante l'operazione
         ***************************************************************************/
        public bool gbSetDPI(int vlDPI)
        {
            if (Dek_Image_Set_DPI(mlImage2, vlDPI, vlDPI) == 0)
                return false;

            return true;
        }

        /**************************************************************************
         * gCropImage
         * Crea l'immagine ritagliata
         * Parametri:
         *			xmin	: ascissa minima
         *			ymin	: ordinata minima
         *			xmax	: ascissa massima
         *			ymax	: ordinata massima
         * Ritorna:
         *			true	operazione eseguita correttamente
         *			false	errore nella correzione della lente
         ***************************************************************************/
        unsafe public bool gCropImage(int xmin, int ymin, int xmax, int ymax)
        {
            EN_DEK_ERRORS rnErr;
            RECT rct;

            rct.Bottom = ymin;
            rct.Left = xmin;
            rct.Right = xmax;
            rct.Top = ymax;

            if (mlImage3 == 0)
                mlImage3 = Dek_Image_Create(0);

            if (mlImage3 == 0)
                return false;

            if (Dek_Image_Crop(mlImage2, &rct, mlImage3) == 0)
                return false;

            //Legge le dimensioni dell'immagine
            if (!gbGetImageDimension(ref mlWidth3, ref mlHeight3, out rnErr, DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE))
                return false;

            return true;
        }

        /**************************************************************************
         * gCropImageRestore
         * Reimposta l'immagine originale
         * Parametri:
         * Ritorna:
         *			true	operazione eseguita correttamente
         *			false	errore nella correzione della lente
         ***************************************************************************/
        public bool gCropImageRestore()
        {
            EN_DEK_ERRORS rnErr;

            if (Dek_Image_Copy(mlImage2, mlImage4) == 0)
                return false;

            //Legge le dimensioni dell'immagine
            if (!gbGetImageDimension(ref mlWidth4, ref mlHeight4, out rnErr, DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE))
                return false;

            return true;
        }

        /**************************************************************************
         * gMixImage
         * Crea l'immagine ritagliata
         * Parametri:
         *			xoff	: offset x
         *			yoff	: offset y
         * Ritorna:
         *			true	operazione eseguita correttamente
         *			false	errore nella correzione della lente
         ***************************************************************************/
        public bool gMixImage(int xoff, int yoff)
        {
            EN_DEK_ERRORS rnErr;

            if (xoff < 0)
                xoff = 0;
            if (xoff > mlWidth4)
                xoff = mlWidth4;

            if (yoff < 0)
                yoff = 0;
            if (yoff > mlHeight4)
                yoff = mlHeight4;

            if (Dek_Image_Mix(mlImage4, mlImage3, xoff, yoff) == 0)
                return false;

            //Legge le dimensioni dell'immagine
            if (!gbGetImageDimension(ref mlWidth4, ref mlHeight4, out rnErr, DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE))
                return false;

            return true;
        }

        ///*****************************************************************************
        /// <summary>
        /// Determina la presenza e la posizione di una serie di patterns all'interno
        /// dell'immagine
        /// </summary>
        /// <param name="patterns">elenco delle immagini</param>
        /// <param name="results">elenco delle immagini trovate e delle relative posizioni</param>
        /// <param name="maxresults">numero massimo di patterns da cercare</param>
        /// <param name="threads">numero di threads paralleli da attivare</param>
        /// <param name="dkiImage">immagine su cui cercare i patterns</param>
        /// <returns>
        ///		maggiore o uguale a 0	numero di patterns trovati
        ///		minore di 0				errore
        /// </returns>
        ///*****************************************************************************
        public int gPatterns(ref DekImageWrapper[] patterns,
                             out DEK_PATTERN_RES[] results,
                             int maxresults,
                             int threads,
                             DKI_DISPLAY dkiImage)
        {
            int lImage;
            EN_DEK_ERRORS dekErr;

            // Verifica che ci siano dei patterns da cercare
            if (patterns.Length <= 0)
            {
                results = new DEK_PATTERN_RES[0];
                return 0;
            }

            switch (dkiImage)
            {
                case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                    lImage = mlImage1;
                    break;

                case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                    lImage = mlImage2;
                    break;

                case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                    lImage = mlImage3;
                    break;

                case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                    lImage = mlImage4;
                    break;

                default:
                    lImage = mlImage1;
                    break;
            }

#if SAVE_SEARCH_IMAGE
			// Salva l'immagine su cui fare la ricerca
			gbSaveImage("SearchImage.jpg", out dekErr, dkiImage);
#endif
            // Prepara il vettore con gli Handle delle immagini dei patterns
            int[] apatterns = new int[patterns.Length];
            for (int i = 0; i < patterns.Length; i++)
                apatterns[i] = patterns[i].mlImage1;

            // Prepara il vettore per i risultati
            DEK_PATTERN_RES[] aresults = new DEK_PATTERN_RES[maxresults];

            // Lancia il ritrovamento dei patterns
            int rc = Dek_Image_Patterns(lImage, ref apatterns[0], apatterns.Length, ref aresults[0], maxresults, threads);

            // Copia i risultati nel vettore di ritorno
            if (rc >= 0)
            {
                results = new DEK_PATTERN_RES[rc];
                for (int i = 0; i < rc; i++)
                    results[i] = aresults[i];
            }
            // errore
            else
                results = null;

            return rc;
        }

        ///*********************************************************************
        /// <summary>
        /// Cancella i file di log attuali della ricerca pattern
        /// </summary>
        /// <param name="path">percorso da pulire</param>
        /// <returns>
        ///		true	operazione eseguita con successo
        ///		false	errore
        /// </returns>
        ///*********************************************************************
        public bool gLogClean(string path)
        {
            try
            {
                if (path == "")
                    path = Environment.CurrentDirectory;

                // Create a reference to the current directory.
                DirectoryInfo di = new DirectoryInfo(path);
                // Create an array representing the files in the current directory.
                FileInfo[] fi = di.GetFiles("Pattern_??.jpg");
                // Search the log file
                foreach (FileInfo fiTemp in fi)
                {
                    fiTemp.Delete();
                }

                // Create an array representing the files in the current directory.
                fi = di.GetFiles("Pattern??.txt");
                // Search the log file
                foreach (FileInfo fiTemp in fi)
                {
                    fiTemp.Delete();
                }

                // Create an array representing the files in the current directory.
                fi = di.GetFiles("Threads??.txt");
                // Search the log file
                foreach (FileInfo fiTemp in fi)
                {
                    fiTemp.Delete();
                }

                GenUtils.DeleteFile(di.FullName + "\\" + "Result.txt");
                GenUtils.DeleteFile(di.FullName + "\\" + "SearchImage.jpg");

                if (di.FullName != Environment.CurrentDirectory)
                {
                    GenUtils.DeleteFile(di.FullName + "\\" + "RefPoint1.jpg");
                    GenUtils.DeleteFile(di.FullName + "\\" + "RefPoint2.jpg");
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        ///*********************************************************************
        /// <summary>
        /// Salva i file di log
        /// </summary>
        /// <param name="log">numero di log da salvare</param>
        /// <returns>
        ///		true	operazione eseguita con successo
        ///		false	errore
        /// </returns>
        ///*********************************************************************
        public bool gLogSave(int log)
        {
            try
            {

                // Trova la directory sorgente
                DirectoryInfo diSrc = new DirectoryInfo(Environment.CurrentDirectory);

                // Trova la directory di log
                DirectoryInfo diDst = new DirectoryInfo(Environment.CurrentDirectory + "\\LOG");
                if (!diDst.Exists)
                    diDst.Create();

                // Trova la sottodirectory di log
                string subDir = log.ToString("000000");
                DirectoryInfo[] ddi_sub = diDst.GetDirectories(subDir);
                if (ddi_sub.Length == 0)
                    diDst = diDst.CreateSubdirectory(subDir);
                else
                    diDst = ddi_sub[0];

                // Pulisce la directory di destinazione
                gLogClean(diDst.FullName);

                // Create an array representing the files in the current directory.
                FileInfo[] fi = diSrc.GetFiles("Pattern_??.jpg");
                // Search the log file
                foreach (FileInfo fiTemp in fi)
                {
                    fiTemp.MoveTo(diDst.FullName + "\\" + fiTemp.Name);
                }

                // Create an array representing the files in the current directory.
                fi = diSrc.GetFiles("Pattern??.txt");
                // Search the log file
                foreach (FileInfo fiTemp in fi)
                {
                    fiTemp.MoveTo(diDst.FullName + "\\" + fiTemp.Name);
                }

                // Create an array representing the files in the current directory.
                fi = diSrc.GetFiles("Threads??.txt");
                // Search the log file
                foreach (FileInfo fiTemp in fi)
                {
                    fiTemp.MoveTo(diDst.FullName + "\\" + fiTemp.Name);
                }

                GenUtils.CopyFile(diSrc.FullName + "\\" + "Result.txt", diDst.FullName + "\\" + "Result.txt");
                GenUtils.CopyFile(diSrc.FullName + "\\" + "SearchImage.jpg", diDst.FullName + "\\" + "SearchImage.jpg");
                GenUtils.CopyFile(diSrc.FullName + "\\" + "RefPoint1.jpg", diDst.FullName + "\\" + "RefPoint1.jpg");
                GenUtils.CopyFile(diSrc.FullName + "\\" + "RefPoint2.jpg", diDst.FullName + "\\" + "RefPoint2.jpg");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// **************************************************************************
        /// <summary>
        /// Copia l'immagine sorgente su quella di destinazione
        /// </summary>
        /// <param name="src">immagine sorgente</param>
        /// <param name="dst">immagine destinazione</param>
        /// <param name="rnErr">errore</param>
        /// <returns>
        ///		true	operazione eseguita con successo
        ///		false	errore
        /// </returns>
        /// **************************************************************************
        public bool gbCopyImage(DKI_DISPLAY src, DKI_DISPLAY dst, out EN_DEK_ERRORS rnErr)
        {
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            try
            {
                int handleSrc = glImage(src);
                int handleDst = glImage(dst);

                // Copia l'immagine sorgente su quella di destinazione
                if (Dek_Image_Copy(handleSrc, handleDst) == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_COPY_IMAGE;
                    throw new Exception();
                }

                // Legge le dimensioni dell'immagine
                int width = 0, height = 0;
                if (!gbGetImageDimension(ref width, ref height, out rnErr, dst))
                    throw new Exception();
                switch (dst)
                {
                    case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                        mlWidth1 = width;
                        mlHeight1 = height;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                        mlWidth2 = width;
                        mlHeight2 = height;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                        mlWidth3 = width;
                        mlHeight3 = height;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                        mlWidth4 = width;
                        mlHeight4 = height;
                        break;
                }
                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }

        /// **************************************************************************
        /// <summary>
        /// Ruota l'immagine
        /// </summary>
        /// <param name="angle">angolo di rotazione</param>
        /// <param name="rnErr">errore</param>
        /// <returns>
        ///		true	operazione eseguita con successo
        ///		false	errore
        /// </returns>
        /// **************************************************************************
        public bool gbRotateImage(double angle, out EN_DEK_ERRORS rnErr)
        {
            return gbRotateImage(angle, DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE, out rnErr);
        }

        /// **************************************************************************
        /// <summary>
        /// Ruota l'immagine
        /// </summary>
        /// <param name="angle">angolo di rotazione</param>
        /// <param name="dkiImage">immagine </param>
        /// <param name="rnErr">errore</param>
        /// <returns>
        ///		true	operazione eseguita con successo
        ///		false	errore
        /// </returns>
        /// **************************************************************************
        public bool gbRotateImage(double angle, DKI_DISPLAY dkiImage, out EN_DEK_ERRORS rnErr)
        {
            int rc;
            rnErr = EN_DEK_ERRORS.EN_DEK_ERR_OK;
            int lImage;

            try
            {
                switch (dkiImage)
                {
                    case DKI_DISPLAY.DKI_DISPLAY_CONF_IMAGE:
                        lImage = mlImage1;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MOD_IMAGE:
                        lImage = mlImage2;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_CROP_IMAGE:
                        lImage = mlImage3;
                        break;

                    case DKI_DISPLAY.DKI_DISPLAY_MIX_IMAGE:
                        lImage = mlImage4;
                        break;

                    default:
                        return false;
                }

                //Ruota l'immagine
                rc = Dek_Image_Rotate(lImage, angle);

                if (rc == 0)
                {
                    rnErr = EN_DEK_ERRORS.EN_DEK_ERR_ROTATE;
                    throw new Exception();
                }

                return true;
            }
            catch (Exception)
            {
                if (rnErr == EN_DEK_ERRORS.EN_DEK_ERR_OK) rnErr = EN_DEK_ERRORS.EN_DEK_ERR_GENERIC;
                return false;
            }
        }
    }

}
