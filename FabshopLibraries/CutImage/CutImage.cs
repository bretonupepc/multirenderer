using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Text;
using System.Drawing;
using TraceLoggers;

namespace Breton.CutImages
{
    public class CutImage
    {
        public static event EventHandler<EventArgs> OutOfMemoryRaised;

        private static void OnOutOfMemoryRaised()
        {
            var handler = OutOfMemoryRaised;
            if (handler != null)
            {
                handler(null, new EventArgs());
            }
        }

        private bool mDisposed = false;

        private Bitmap lastra;
        private Bitmap bitmap;
        private int largImgResult;
        private int altImgResult;
        private RectangleF destinationRect;
        private int scala;
		private object _lockObj = new object();

        private ImageInfo _lastraImageInfo = null;

        private DekImageWrapper _dekImage;

        private DekImageWrapper.EN_DEK_ERRORS _dekImageError;


        #region Constructor

        public CutImage()
        {

        }

		public CutImage(string imageFile)
		{
            ////this.lastra = new Bitmap(imageFile);
            /// 
            _lastraImageInfo = new ImageInfo();
		    _lastraImageInfo.ImagePath = imageFile;

		    using (var image = Image.FromFile(imageFile))
		    {
		        _lastraImageInfo.ImageWidth = image.Width;
                _lastraImageInfo.ImageHeight = image.Height;
                //this.lastra = new Bitmap(image, image.Width, image.Height);

		    }

            _dekImage = new DekImageWrapper();

		    _dekImage.gbLoadImage(imageFile, out _dekImageError);

		}

        public CutImage(Bitmap lastra)
        {
            this.lastra = new Bitmap(lastra, lastra.Width, lastra.Height);
	    }

        public CutImage(Bitmap lastra, int scala)
        {
            this.scala = scala;
            this.lastra = new Bitmap(lastra, lastra.Width / scala, lastra.Height / scala);
        }

        public int Scala
        {
            set { scala = value; }
            get { return scala; }
        }


        public ImageInfo LastraImageInfo
        {
            get { return _lastraImageInfo; }
            set { _lastraImageInfo = value; }
        }

        public void SetBitmapOut(PointF[] BBoxSagoma)
        {

            try
            {
                //int start = System.DateTime.Now.Millisecond;
                //TraceLog.WriteLine("START SetBitmapOut: " + start.ToString());

                //dai quattro punti del bounding box in ingresso ricavo larghezza e altezza
                //dell'immagine da ritagliare. Questi punti devono essere stati calcolati
                //a partire da quello in basso a sx in senso antiorario
                largImgResult = (int) GetDistanza2Punti(BBoxSagoma[0].X/scala, BBoxSagoma[0].Y/scala,
                    BBoxSagoma[1].X/scala, BBoxSagoma[1].Y/scala);
                altImgResult = (int) GetDistanza2Punti(BBoxSagoma[1].X/scala, BBoxSagoma[1].Y/scala,
                    BBoxSagoma[2].X/scala, BBoxSagoma[2].Y/scala);


                using (var image = Image.FromFile(_lastraImageInfo.ImagePath))
                {
                    using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
                    {
                        bitmap = new Bitmap(bitmapImage, largImgResult, altImgResult);
                    }
                }


                ////bitmap = new Bitmap(lastra, largImgResult, altImgResult);

                destinationRect = new RectangleF(0, 0, largImgResult, altImgResult);

                //int end = System.DateTime.Now.Millisecond - start;
                //TraceLog.WriteLine("END SetBitmapOut: " + end.ToString());

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                if (ex is OutOfMemoryException)
                {
                    OnOutOfMemoryRaised();
                }

            }

        }

        public void SetBoundingBox(PointF[] BBoxSagoma)
        {

            try
            {
                //int start = System.DateTime.Now.Millisecond;
                //TraceLog.WriteLine("START SetBitmapOut: " + start.ToString());

                //dai quattro punti del bounding box in ingresso ricavo larghezza e altezza
                //dell'immagine da ritagliare. Questi punti devono essere stati calcolati
                //a partire da quello in basso a sx in senso antiorario
                largImgResult = (int)GetDistanza2Punti(BBoxSagoma[0].X / scala, BBoxSagoma[0].Y / scala,
                    BBoxSagoma[1].X / scala, BBoxSagoma[1].Y / scala);
                altImgResult = (int)GetDistanza2Punti(BBoxSagoma[1].X / scala, BBoxSagoma[1].Y / scala,
                    BBoxSagoma[2].X / scala, BBoxSagoma[2].Y / scala);


                ////using (var image = Image.FromFile(_lastraImageInfo.ImagePath))
                ////{
                ////    using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
                ////    {
                ////        bitmap = new Bitmap(bitmapImage, largImgResult, altImgResult);
                ////    }
                ////}


                ////bitmap = new Bitmap(lastra, largImgResult, altImgResult);

                destinationRect = new RectangleF(0, 0, largImgResult, altImgResult);

                //int end = System.DateTime.Now.Millisecond - start;
                //TraceLog.WriteLine("END SetBitmapOut: " + end.ToString());

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                if (ex is OutOfMemoryException)
                {
                    OnOutOfMemoryRaised();
                }

            }

        }

        
        public void SetSquareCenteredBitmapOut(ref PointF[] BBoxSagoma)
        {
            double squareDim ;
            try
            {
                //int start = System.DateTime.Now.Millisecond;
                //TraceLog.WriteLine("START SetBitmapOut: " + start.ToString());

                //dai quattro punti del bounding box in ingresso ricavo larghezza e altezza
                //dell'immagine da ritagliare. Questi punti devono essere stati calcolati
                //a partire da quello in basso a sx in senso antiorario
                largImgResult = (int) GetDistanza2Punti(BBoxSagoma[0].X/scala, BBoxSagoma[0].Y/scala,
                    BBoxSagoma[1].X/scala, BBoxSagoma[1].Y/scala);
                altImgResult = (int) GetDistanza2Punti(BBoxSagoma[1].X/scala, BBoxSagoma[1].Y/scala,
                    BBoxSagoma[2].X/scala, BBoxSagoma[2].Y/scala);

                squareDim = Math.Max(largImgResult, altImgResult);


                //bitmap = new Bitmap(lastra, (int)squareDim, (int)squareDim);
                ////using (var image = Image.FromFile(_lastraImageInfo.ImagePath))
                ////{
                ////    //int maxDim = Math.Max(image.Width, image.Height);
                ////    using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
                ////    {
                ////        bitmap = new Bitmap(bitmapImage, (int)squareDim, (int)squareDim);
                ////    //bitmap = new Bitmap(image, (int)maxDim, (int)maxDim);
                ////        //bitmap = new Bitmap(image);
                ////    //bitmap.Save(@"E:\Pippo.jpg");
                ////    }
                ////}

                destinationRect = new RectangleF(0, 0, (int) squareDim, (int) squareDim);


                // Estensione BBox
                if (largImgResult != (int) squareDim && largImgResult != 0)
                {
                    double sideExtension = (squareDim - largImgResult)/2;
                    double deltaX = (BBoxSagoma[0].X - BBoxSagoma[1].X)*sideExtension/largImgResult;
                    double deltaY = (BBoxSagoma[0].Y - BBoxSagoma[1].Y)*sideExtension/largImgResult;

                    BBoxSagoma[0].X += (int) deltaX;
                    BBoxSagoma[0].Y += (int) deltaY;

                    BBoxSagoma[1].X -= (int) deltaX;
                    BBoxSagoma[1].Y -= (int) deltaY;

                    BBoxSagoma[2].X -= (int) deltaX;
                    BBoxSagoma[2].Y -= (int) deltaY;

                    BBoxSagoma[3].X += (int) deltaX;
                    BBoxSagoma[3].Y += (int) deltaY;
                }
                else if (altImgResult != (int) squareDim && altImgResult != 0)
                {
                    double sideExtension = (squareDim - altImgResult)/2;
                    double deltaX = (BBoxSagoma[0].X - BBoxSagoma[3].X)*sideExtension/altImgResult;
                    double deltaY = (BBoxSagoma[0].Y - BBoxSagoma[3].Y)*sideExtension/altImgResult;

                    BBoxSagoma[0].X += (int) deltaX;
                    BBoxSagoma[0].Y += (int) deltaY;

                    BBoxSagoma[1].X += (int) deltaX;
                    BBoxSagoma[1].Y += (int) deltaY;

                    BBoxSagoma[2].X -= (int) deltaX;
                    BBoxSagoma[2].Y -= (int) deltaY;

                    BBoxSagoma[3].X -= (int) deltaX;
                    BBoxSagoma[3].Y -= (int) deltaY;
                }

                largImgResult = altImgResult = (int) squareDim;

                //int end = System.DateTime.Now.Millisecond - start;
                //TraceLog.WriteLine("END SetBitmapOut: " + end.ToString());

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("SetSquareCenterdBitmapOut ERROR: ", ex);

                if (ex is OutOfMemoryException)
                {
                    OnOutOfMemoryRaised();
                }

            }

        }


        public void SetSquareCenteredBoundingBox(ref PointF[] BBoxSagoma)
        {
            double squareDim;
            try
            {
                //int start = System.DateTime.Now.Millisecond;
                //TraceLog.WriteLine("START SetBitmapOut: " + start.ToString());

                //dai quattro punti del bounding box in ingresso ricavo larghezza e altezza
                //dell'immagine da ritagliare. Questi punti devono essere stati calcolati
                //a partire da quello in basso a sx in senso antiorario
                largImgResult = (int)GetDistanza2Punti(BBoxSagoma[0].X / scala, BBoxSagoma[0].Y / scala,
                    BBoxSagoma[1].X / scala, BBoxSagoma[1].Y / scala);
                altImgResult = (int)GetDistanza2Punti(BBoxSagoma[1].X / scala, BBoxSagoma[1].Y / scala,
                    BBoxSagoma[2].X / scala, BBoxSagoma[2].Y / scala);

                squareDim = Math.Max(largImgResult, altImgResult);


                destinationRect = new RectangleF(0, 0, (int)squareDim, (int)squareDim);


                // Estensione BBox
                if (largImgResult != (int)squareDim && largImgResult != 0)
                {
                    double sideExtension = (squareDim - largImgResult) / 2;
                    double deltaX = (BBoxSagoma[0].X - BBoxSagoma[1].X) * sideExtension / largImgResult;
                    double deltaY = (BBoxSagoma[0].Y - BBoxSagoma[1].Y) * sideExtension / largImgResult;

                    BBoxSagoma[0].X += (int)deltaX;
                    BBoxSagoma[0].Y += (int)deltaY;

                    BBoxSagoma[1].X -= (int)deltaX;
                    BBoxSagoma[1].Y -= (int)deltaY;

                    BBoxSagoma[2].X -= (int)deltaX;
                    BBoxSagoma[2].Y -= (int)deltaY;

                    BBoxSagoma[3].X += (int)deltaX;
                    BBoxSagoma[3].Y += (int)deltaY;
                }
                else if (altImgResult != (int)squareDim && altImgResult != 0)
                {
                    double sideExtension = (squareDim - altImgResult) / 2;
                    double deltaX = (BBoxSagoma[0].X - BBoxSagoma[3].X) * sideExtension / altImgResult;
                    double deltaY = (BBoxSagoma[0].Y - BBoxSagoma[3].Y) * sideExtension / altImgResult;

                    BBoxSagoma[0].X += (int)deltaX;
                    BBoxSagoma[0].Y += (int)deltaY;

                    BBoxSagoma[1].X += (int)deltaX;
                    BBoxSagoma[1].Y += (int)deltaY;

                    BBoxSagoma[2].X -= (int)deltaX;
                    BBoxSagoma[2].Y -= (int)deltaY;

                    BBoxSagoma[3].X -= (int)deltaX;
                    BBoxSagoma[3].Y -= (int)deltaY;
                }

                largImgResult = altImgResult = (int)squareDim;


            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("SetSquareCenteredBoundingBox ERROR: ", ex);

                if (ex is OutOfMemoryException)
                {
                    OnOutOfMemoryRaised();
                }

            }

        }

        #endregion

        #region Distructor

        public void Dispose()
        {
            Dispose(true);
            GC.Collect();
        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.mDisposed)
            {
                //lastra = null;
                mDisposed = true;
            }

        }

        ~CutImage()
        {
            Dispose(false);
        }

        #endregion

        /// <summary>
        /// Procedura di ritaglio di una immagine.
        /// </summary>
        /// <param name="lastra">Bitmap contenente la lastra da ritagliare.</param>
        /// <param name="larghezza">Larghezza in mm della lastra.</param>
        /// <param name="altezza">Altezza in mm della lastra.</param>
        /// <param name="angolo">Angolo di rotazione in gradi della sagoma sulla lastra.</param>
        /// <param name="BBoxSagoma">Array dei 4 punti finali del bounding box della sagoma.</param>
		public Bitmap GetImage_ORI(int larghezza, int altezza, float angolo, PointF[] BBoxSagomaOrig)
		{
			if (bitmap == null) return null;

			lock (_lockObj)
			{
				try
				{

					larghezza = larghezza / scala;
					altezza = altezza / scala;

					PointF[] BBoxSagoma = new PointF[4];

					for (int i = 0; i < BBoxSagomaOrig.Length; i++)
					{
						BBoxSagoma[i].X = BBoxSagomaOrig[i].X / scala;
						BBoxSagoma[i].Y = BBoxSagomaOrig[i].Y / scala;
					}

                    ////int p0X = (int)(lastra.Width * BBoxSagoma[3].X) / larghezza;
                    ////int p0Y = (int)(lastra.Height * (altezza - BBoxSagoma[3].Y)) / altezza;
                    ////int p1X = (int)(lastra.Width * largImgResult) / larghezza;
                    ////int p1Y = (int)(lastra.Height * altImgResult) / altezza;
					int p0X = (int)(_lastraImageInfo.ImageWidth * BBoxSagoma[3].X) / larghezza;
                    int p0Y = (int)(_lastraImageInfo.ImageHeight * (altezza - BBoxSagoma[3].Y)) / altezza;
                    int p1X = (int)(_lastraImageInfo.ImageWidth * largImgResult) / larghezza;
                    int p1Y = (int)(_lastraImageInfo.ImageHeight * altImgResult) / altezza;

					using (Graphics gr = Graphics.FromImage(bitmap))
					{
						// Usare Red per vedere bene le anomal�e
						//gr.Clear(Color.Red);
						gr.Clear(Color.Transparent);

						System.Drawing.Drawing2D.Matrix matriceSagoma = gr.Transform;

						Point[] destinationPoints = { new Point(p0X, p0Y) };

						System.Drawing.Drawing2D.Matrix matrice = gr.Transform;

						matrice.Rotate(angolo);

                    ////    PointF[] rotationPoints = {
                    ////new PointF(0, 0),                           // punto in alto a sx
                    ////new PointF(lastra.Width, 0),                // punto in alto a dx
                    ////new PointF(lastra.Width, lastra.Height),    // punto in basso a dx
                    ////new PointF(0, lastra.Height)};              // punto in basso a sx
						PointF[] rotationPoints = {
                    new PointF(0, 0),                           // punto in alto a sx
                    new PointF(_lastraImageInfo.ImageWidth, 0),                // punto in alto a dx
                    new PointF(_lastraImageInfo.ImageWidth, _lastraImageInfo.ImageHeight),    // punto in basso a dx
                    new PointF(0, _lastraImageInfo.ImageHeight)};              // punto in basso a sx

						matrice.TransformPoints(rotationPoints);

						//dopo averli ruotati prendo min e max per calcolare il rettangolo di ingombro
						float minX = 100000;
						float minY = 100000;
						float maxX = -100000;
						float maxY = -100000;

						for (int i = 0; i < rotationPoints.Length; i++)
						{
							minX = System.Math.Min(rotationPoints[i].X, minX);
							minY = System.Math.Min(rotationPoints[i].Y, minY);
							maxX = System.Math.Max(rotationPoints[i].X, maxX);
							maxY = System.Math.Max(rotationPoints[i].Y, maxY);
						}

						RectangleF maxRect = new RectangleF(
							0,
							0,
							maxX - minX,
							maxY - minY);

						matriceSagoma.Translate(Math.Abs(minX), Math.Abs(minY));

						//matriceSagoma.Translate((float)max / 2, (float)max);
						matriceSagoma.Rotate(angolo);
						matriceSagoma.TransformPoints(destinationPoints);

						RectangleF sourceRect = new RectangleF(destinationPoints[0].X, destinationPoints[0].Y, p1X, p1Y);

						if (angolo != 0)
						{

							using (Bitmap bRet = RotateImage(lastra, angolo, maxRect, minX, minY))
							{
								//bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

								gr.DrawImage(
									bRet,
									destinationRect,
									sourceRect,
									GraphicsUnit.Pixel);
							}

						}
						else
						{
							gr.DrawImage(
								lastra,
								destinationRect,
								sourceRect,
								GraphicsUnit.Pixel);
						}
					}
				}
				catch (Exception ex)
				{
					TraceLog.WriteLine("ERROR: ", ex);
				}
			}

			return new Bitmap(bitmap);
		}

        /// <summary>
        /// Procedura di ritaglio di una immagine.
        /// </summary>
        /// <param name="larghezza">Larghezza in mm della lastra.</param>
        /// <param name="altezza">Altezza in mm della lastra.</param>
        /// <param name="angolo">Angolo di rotazione in gradi della sagoma sulla lastra.</param>
        /// <param name="BBoxSagomaOrig">Array dei 4 punti finali del bounding box della sagoma.</param>
        public Bitmap GetImage(int larghezza, int altezza, float angolo, PointF[] BBoxSagomaOrig)
        {
            if (bitmap == null) return null;

            lock (_lockObj)
            {
                try
                {

                    larghezza = larghezza / scala;
                    altezza = altezza / scala;

                    PointF[] BBoxSagoma = new PointF[4];

                    for (int i = 0; i < BBoxSagomaOrig.Length; i++)
                    {
                        BBoxSagoma[i].X = BBoxSagomaOrig[i].X / scala;
                        BBoxSagoma[i].Y = BBoxSagomaOrig[i].Y / scala;
                    }

                    ////int p0X = (int)(lastra.Width * BBoxSagoma[3].X) / larghezza;
                    ////int p0Y = (int)(lastra.Height * (altezza - BBoxSagoma[3].Y)) / altezza;
                    ////int p1X = (int)(lastra.Width * largImgResult) / larghezza;
                    ////int p1Y = (int)(lastra.Height * altImgResult) / altezza;
                    int p0X = (int)(_lastraImageInfo.ImageWidth * BBoxSagoma[3].X) / larghezza;
                    int p0Y = (int)(_lastraImageInfo.ImageHeight * (altezza - BBoxSagoma[3].Y)) / altezza;
                    int p1X = (int)(_lastraImageInfo.ImageWidth * largImgResult) / larghezza;
                    int p1Y = (int)(_lastraImageInfo.ImageHeight * altImgResult) / altezza;

                    using (Graphics gr = Graphics.FromImage(bitmap))
                    {
                        // Usare Red per vedere bene le anomal�e
                        //gr.Clear(Color.Red);
                        gr.Clear(Color.Transparent);

                        System.Drawing.Drawing2D.Matrix matriceSagoma = gr.Transform;

                        Point[] destinationPoints = { new Point(p0X, p0Y) };

                        System.Drawing.Drawing2D.Matrix matrice = gr.Transform;

                        matrice.Rotate(angolo);

                        ////PointF[] rotationPoints = {
                        ////    new PointF(0, 0),                           // punto in alto a sx
                        ////    new PointF(lastra.Width, 0),                // punto in alto a dx
                        ////    new PointF(lastra.Width, lastra.Height),    // punto in basso a dx
                        ////    new PointF(0, lastra.Height)};              // punto in basso a sx
                        PointF[] rotationPoints = {
                            new PointF(0, 0),                           // punto in alto a sx
                            new PointF(_lastraImageInfo.ImageWidth, 0),                // punto in alto a dx
                            new PointF(_lastraImageInfo.ImageWidth, _lastraImageInfo.ImageHeight),    // punto in basso a dx
                            new PointF(0, _lastraImageInfo.ImageHeight)};              // punto in basso a sx

                        matrice.TransformPoints(rotationPoints);

                        //dopo averli ruotati prendo min e max per calcolare il rettangolo di ingombro
                        float minX = 100000;
                        float minY = 100000;
                        float maxX = -100000;
                        float maxY = -100000;

                        for (int i = 0; i < rotationPoints.Length; i++)
                        {
                            minX = System.Math.Min(rotationPoints[i].X, minX);
                            minY = System.Math.Min(rotationPoints[i].Y, minY);
                            maxX = System.Math.Max(rotationPoints[i].X, maxX);
                            maxY = System.Math.Max(rotationPoints[i].Y, maxY);
                        }

                        RectangleF maxRect = new RectangleF(
                            0,
                            0,
                            maxX - minX,
                            maxY - minY);

                        matriceSagoma.Translate(Math.Abs(minX), Math.Abs(minY));

                        //matriceSagoma.Translate((float)max / 2, (float)max);
                        matriceSagoma.Rotate(angolo);
                        matriceSagoma.TransformPoints(destinationPoints);

                        RectangleF sourceRect = new RectangleF(destinationPoints[0].X, destinationPoints[0].Y, p1X, p1Y);

                        using (var image = Image.FromFile(_lastraImageInfo.ImagePath))
                        using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
                        {
                            if (angolo != 0)
                            {
                                ////using (Bitmap bRet = rotateImage(lastra, angolo, maxRect, minX, minY))
                                ////{
                                ////    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                ////    gr.DrawImage(
                                ////        bRet,
                                ////        destinationRect,
                                ////        sourceRect,
                                ////        GraphicsUnit.Pixel);
                                ////}
                                using (Bitmap bRet = RotateImage(bitmapImage, angolo, maxRect, minX, minY))
                                {
                                    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                    gr.DrawImage(
                                        bRet,
                                        destinationRect,
                                        sourceRect,
                                        GraphicsUnit.Pixel);
                                }


                            }
                            else
                            {
                                //gr.DrawImage(
                                //lastra,
                                //destinationRect,
                                //sourceRect,
                                //GraphicsUnit.Pixel);
                                gr.DrawImage(
                                bitmapImage,
                                destinationRect,
                                sourceRect,
                                GraphicsUnit.Pixel);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("ERROR: ", ex);
                    if (ex is OutOfMemoryException)
                    {
                        OnOutOfMemoryRaised();
                    }
                }
            }

            return new Bitmap(bitmap);
        }

        public Bitmap GetBitmap(int larghezza, int altezza, float angolo, PointF[] BBoxSagomaOrig)
        {
            Bitmap outputBitmap = new Bitmap(largImgResult, altImgResult);

            lock (_lockObj)
            {
                try
                {

                    larghezza = larghezza / scala;
                    altezza = altezza / scala;

                    PointF[] BBoxSagoma = new PointF[4];

                    for (int i = 0; i < BBoxSagomaOrig.Length; i++)
                    {
                        BBoxSagoma[i].X = BBoxSagomaOrig[i].X / scala;
                        BBoxSagoma[i].Y = BBoxSagomaOrig[i].Y / scala;
                    }

                    ////int p0X = (int)(lastra.Width * BBoxSagoma[3].X) / larghezza;
                    ////int p0Y = (int)(lastra.Height * (altezza - BBoxSagoma[3].Y)) / altezza;
                    ////int p1X = (int)(lastra.Width * largImgResult) / larghezza;
                    ////int p1Y = (int)(lastra.Height * altImgResult) / altezza;
                    int p0X = (int)(_lastraImageInfo.ImageWidth * BBoxSagoma[3].X) / larghezza;
                    int p0Y = (int)(_lastraImageInfo.ImageHeight * (altezza - BBoxSagoma[3].Y)) / altezza;
                    int p1X = (int)(_lastraImageInfo.ImageWidth * largImgResult) / larghezza;
                    int p1Y = (int)(_lastraImageInfo.ImageHeight * altImgResult) / altezza;

                    using (Graphics gr = Graphics.FromImage(outputBitmap))
                    {
                        // Usare Red per vedere bene le anomal�e
                        //gr.Clear(Color.Red);
                        gr.Clear(Color.Transparent);

                        System.Drawing.Drawing2D.Matrix matriceSagoma = gr.Transform;

                        Point[] destinationPoints = { new Point(p0X, p0Y) };

                        System.Drawing.Drawing2D.Matrix matrice = gr.Transform;

                        matrice.Rotate(angolo);

                        ////PointF[] rotationPoints = {
                        ////    new PointF(0, 0),                           // punto in alto a sx
                        ////    new PointF(lastra.Width, 0),                // punto in alto a dx
                        ////    new PointF(lastra.Width, lastra.Height),    // punto in basso a dx
                        ////    new PointF(0, lastra.Height)};              // punto in basso a sx
                        PointF[] rotationPoints = {
                            new PointF(0, 0),                           // punto in alto a sx
                            new PointF(_lastraImageInfo.ImageWidth, 0),                // punto in alto a dx
                            new PointF(_lastraImageInfo.ImageWidth, _lastraImageInfo.ImageHeight),    // punto in basso a dx
                            new PointF(0, _lastraImageInfo.ImageHeight)};              // punto in basso a sx

                        matrice.TransformPoints(rotationPoints);

                        //dopo averli ruotati prendo min e max per calcolare il rettangolo di ingombro
                        float minX = 100000;
                        float minY = 100000;
                        float maxX = -100000;
                        float maxY = -100000;

                        for (int i = 0; i < rotationPoints.Length; i++)
                        {
                            minX = System.Math.Min(rotationPoints[i].X, minX);
                            minY = System.Math.Min(rotationPoints[i].Y, minY);
                            maxX = System.Math.Max(rotationPoints[i].X, maxX);
                            maxY = System.Math.Max(rotationPoints[i].Y, maxY);
                        }

                        RectangleF maxRect = new RectangleF(
                            0,
                            0,
                            maxX - minX,
                            maxY - minY);

                        matriceSagoma.Translate(Math.Abs(minX), Math.Abs(minY));

                        //matriceSagoma.Translate((float)max / 2, (float)max);
                        matriceSagoma.Rotate(angolo);
                        matriceSagoma.TransformPoints(destinationPoints);

                        RectangleF sourceRect = new RectangleF(destinationPoints[0].X, destinationPoints[0].Y, p1X, p1Y);


                        var image = Image.FromFile(_lastraImageInfo.ImagePath);
                        using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
                        {
                            image.Dispose();

                            if (angolo != 0)
                            {
                                ////using (Bitmap bRet = rotateImage(lastra, angolo, maxRect, minX, minY))
                                ////{
                                ////    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                ////    gr.DrawImage(
                                ////        bRet,
                                ////        destinationRect,
                                ////        sourceRect,
                                ////        GraphicsUnit.Pixel);
                                ////}
                                using (Bitmap bRet = RotateImage(bitmapImage, angolo, maxRect, minX, minY))
                                //using (Bitmap bRet = rotateImage(lastra, angolo, maxRect, minX, minY))
                                {
                                    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                    gr.DrawImage(
                                        bRet,
                                        destinationRect,
                                        sourceRect,
                                        GraphicsUnit.Pixel);
                                }


                            }
                            else
                            {
                                ////gr.DrawImage(
                                ////lastra,
                                ////destinationRect,
                                ////sourceRect,
                                ////GraphicsUnit.Pixel);
                                gr.DrawImage(
                                bitmapImage,
                                destinationRect,
                                sourceRect,
                                GraphicsUnit.Pixel);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("ERROR: ", ex);
                    if (ex is OutOfMemoryException)
                    {
                        OnOutOfMemoryRaised();
                    }
                }
            }

            return outputBitmap;
        }


        public Bitmap GetSquaredBitmap(int larghezza, int altezza, float angolo, PointF[] BBoxSagomaOrig)
        {
            Bitmap outputBitmap = new Bitmap(largImgResult, altImgResult);

            lock (_lockObj)
            {
                try
                {

                    larghezza = larghezza / scala;
                    altezza = altezza / scala;

                    PointF[] BBoxSagoma = new PointF[4];

                    for (int i = 0; i < BBoxSagomaOrig.Length; i++)
                    {
                        BBoxSagoma[i].X = BBoxSagomaOrig[i].X / scala;
                        BBoxSagoma[i].Y = BBoxSagomaOrig[i].Y / scala;
                    }

                    ////int p0X = (int)(lastra.Width * BBoxSagoma[3].X) / larghezza;
                    ////int p0Y = (int)(lastra.Height * (altezza - BBoxSagoma[3].Y)) / altezza;
                    ////int p1X = (int)(lastra.Width * largImgResult) / larghezza;
                    ////int p1Y = (int)(lastra.Height * altImgResult) / altezza;
                    int p0X = (int)(_lastraImageInfo.ImageWidth * BBoxSagoma[3].X) / larghezza;
                    int p0Y = (int)(_lastraImageInfo.ImageHeight * (altezza - BBoxSagoma[3].Y)) / altezza;
                    int p1X = (int)(_lastraImageInfo.ImageWidth * largImgResult) / larghezza;
                    int p1Y = (int)(_lastraImageInfo.ImageHeight * altImgResult) / altezza;

                    
                    using (Graphics gr = Graphics.FromImage(outputBitmap))
                    {
                        // Usare Red per vedere bene le anomal�e
                        //gr.Clear(Color.Red);
                        gr.Clear(Color.Transparent);

                        System.Drawing.Drawing2D.Matrix matriceSagoma = gr.Transform;

                        Point[] destinationPoints = { new Point(p0X, p0Y) };

                        System.Drawing.Drawing2D.Matrix matrice = gr.Transform;

                        matrice.Rotate(angolo);

                        ////PointF[] rotationPoints = {
                        ////    new PointF(0, 0),                           // punto in alto a sx
                        ////    new PointF(lastra.Width, 0),                // punto in alto a dx
                        ////    new PointF(lastra.Width, lastra.Height),    // punto in basso a dx
                        ////    new PointF(0, lastra.Height)};              // punto in basso a sx
                        PointF[] rotationPoints = {
                            new PointF(0, 0),                           // punto in alto a sx
                            new PointF(_lastraImageInfo.ImageWidth, 0),                // punto in alto a dx
                            new PointF(_lastraImageInfo.ImageWidth, _lastraImageInfo.ImageHeight),    // punto in basso a dx
                            new PointF(0, _lastraImageInfo.ImageHeight)};              // punto in basso a sx

                        matrice.TransformPoints(rotationPoints);

                        //dopo averli ruotati prendo min e max per calcolare il rettangolo di ingombro
                        float minX = float.MaxValue;
                        float minY = float.MaxValue;
                        float maxX = float.MinValue;
                        float maxY = float.MinValue;

                        for (int i = 0; i < rotationPoints.Length; i++)
                        {
                            minX = System.Math.Min(rotationPoints[i].X, minX);
                            minY = System.Math.Min(rotationPoints[i].Y, minY);
                            maxX = System.Math.Max(rotationPoints[i].X, maxX);
                            maxY = System.Math.Max(rotationPoints[i].Y, maxY);
                        }

                        RectangleF maxRect = new RectangleF(
                            0,
                            0,
                            maxX - minX,
                            maxY - minY);

                        matriceSagoma.Translate(Math.Abs(minX), Math.Abs(minY));

                        //matriceSagoma.Translate((float)max / 2, (float)max);
                        matriceSagoma.Rotate(angolo);
                        matriceSagoma.TransformPoints(destinationPoints);

                        RectangleF sourceRect = new RectangleF(destinationPoints[0].X, destinationPoints[0].Y, p1X, p1Y);

                        var image = Image.FromFile(_lastraImageInfo.ImagePath);
                        using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
                        {
                            image.Dispose();

                            if (angolo != 0)
                            {
                                ////using (Bitmap bRet = rotateImage(lastra, angolo, maxRect, minX, minY))
                                ////{
                                ////    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                ////    gr.DrawImage(
                                ////        bRet,
                                ////        destinationRect,
                                ////        sourceRect,
                                ////        GraphicsUnit.Pixel);
                                ////}
                                using (Bitmap bRet = RotateImage(bitmapImage, angolo, maxRect, minX, minY))
                                {
                                    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                    gr.DrawImage(
                                        bRet,
                                        destinationRect,
                                        sourceRect,
                                        GraphicsUnit.Pixel);
                                }


                            }
                            else
                            {
                                //gr.DrawImage(
                                //lastra,
                                //destinationRect,
                                //sourceRect,
                                //GraphicsUnit.Pixel);
                                gr.DrawImage(
                                bitmapImage,
                                destinationRect,
                                sourceRect,
                                GraphicsUnit.Pixel);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("ERROR: ", ex);
                    if (ex is OutOfMemoryException)
                    {
                        OnOutOfMemoryRaised();
                    }
                }
            }

            return outputBitmap;
        }

        public Bitmap GetSquaredBitmapFromBitmap(int larghezza, int altezza, float angolo, PointF[] BBoxSagomaOrig, Bitmap bitmapImage)
        {
            Bitmap outputBitmap = new Bitmap(largImgResult, altImgResult);

            lock (_lockObj)
            {
                try
                {

                    larghezza = larghezza / scala;
                    altezza = altezza / scala;

                    PointF[] BBoxSagoma = new PointF[4];

                    for (int i = 0; i < BBoxSagomaOrig.Length; i++)
                    {
                        BBoxSagoma[i].X = BBoxSagomaOrig[i].X / scala;
                        BBoxSagoma[i].Y = BBoxSagomaOrig[i].Y / scala;
                    }

                    ////int p0X = (int)(lastra.Width * BBoxSagoma[3].X) / larghezza;
                    ////int p0Y = (int)(lastra.Height * (altezza - BBoxSagoma[3].Y)) / altezza;
                    ////int p1X = (int)(lastra.Width * largImgResult) / larghezza;
                    ////int p1Y = (int)(lastra.Height * altImgResult) / altezza;
                    int p0X = (int)(_lastraImageInfo.ImageWidth * BBoxSagoma[3].X) / larghezza;
                    int p0Y = (int)(_lastraImageInfo.ImageHeight * (altezza - BBoxSagoma[3].Y)) / altezza;
                    int p1X = (int)(_lastraImageInfo.ImageWidth * largImgResult) / larghezza;
                    int p1Y = (int)(_lastraImageInfo.ImageHeight * altImgResult) / altezza;


                    using (Graphics gr = Graphics.FromImage(outputBitmap))
                    {
                        // Usare Red per vedere bene le anomal�e
                        //gr.Clear(Color.Red);
                        gr.Clear(Color.Transparent);

                        System.Drawing.Drawing2D.Matrix matriceSagoma = gr.Transform;

                        Point[] destinationPoints = { new Point(p0X, p0Y) };

                        System.Drawing.Drawing2D.Matrix matrice = gr.Transform;

                        matrice.Rotate(angolo);

                        ////PointF[] rotationPoints = {
                        ////    new PointF(0, 0),                           // punto in alto a sx
                        ////    new PointF(lastra.Width, 0),                // punto in alto a dx
                        ////    new PointF(lastra.Width, lastra.Height),    // punto in basso a dx
                        ////    new PointF(0, lastra.Height)};              // punto in basso a sx
                        PointF[] rotationPoints = {
                            new PointF(0, 0),                           // punto in alto a sx
                            new PointF(_lastraImageInfo.ImageWidth, 0),                // punto in alto a dx
                            new PointF(_lastraImageInfo.ImageWidth, _lastraImageInfo.ImageHeight),    // punto in basso a dx
                            new PointF(0, _lastraImageInfo.ImageHeight)};              // punto in basso a sx

                        matrice.TransformPoints(rotationPoints);

                        //dopo averli ruotati prendo min e max per calcolare il rettangolo di ingombro
                        float minX = float.MaxValue;
                        float minY = float.MaxValue;
                        float maxX = float.MinValue;
                        float maxY = float.MinValue;

                        for (int i = 0; i < rotationPoints.Length; i++)
                        {
                            minX = System.Math.Min(rotationPoints[i].X, minX);
                            minY = System.Math.Min(rotationPoints[i].Y, minY);
                            maxX = System.Math.Max(rotationPoints[i].X, maxX);
                            maxY = System.Math.Max(rotationPoints[i].Y, maxY);
                        }

                        RectangleF maxRect = new RectangleF(
                            0,
                            0,
                            maxX - minX,
                            maxY - minY);

                        matriceSagoma.Translate(Math.Abs(minX), Math.Abs(minY));

                        //matriceSagoma.Translate((float)max / 2, (float)max);
                        matriceSagoma.Rotate(angolo);
                        matriceSagoma.TransformPoints(destinationPoints);

                        RectangleF sourceRect = new RectangleF(destinationPoints[0].X, destinationPoints[0].Y, p1X, p1Y);

                        {
                            if (angolo != 0)
                            {
                                ////using (Bitmap bRet = rotateImage(lastra, angolo, maxRect, minX, minY))
                                ////{
                                ////    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                ////    gr.DrawImage(
                                ////        bRet,
                                ////        destinationRect,
                                ////        sourceRect,
                                ////        GraphicsUnit.Pixel);
                                ////}
                                using (Bitmap bRet = RotateImage(bitmapImage, angolo, maxRect, minX, minY))
                                {
                                    //bRet.Save("c:\\ritaglio1.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                                    gr.DrawImage(
                                        bRet,
                                        destinationRect,
                                        sourceRect,
                                        GraphicsUnit.Pixel);
                                }


                            }
                            else
                            {
                                //gr.DrawImage(
                                //lastra,
                                //destinationRect,
                                //sourceRect,
                                //GraphicsUnit.Pixel);
                                gr.DrawImage(
                                bitmapImage,
                                destinationRect,
                                sourceRect,
                                GraphicsUnit.Pixel);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("ERROR: ", ex);
                    if (ex is OutOfMemoryException)
                    {
                        OnOutOfMemoryRaised();
                    }
                }
            }

            return outputBitmap;
        }


        private Bitmap RotateImage(Bitmap b, float angle, int max)
        {
            ////creo una nuova bitmap su cui effettuare la rotazione
            ////if(returnBitmap == null)
            ////    returnBitmap = new Bitmap(2 * max, 2 * max);
            ////creo un nuovo oggetto Graphics dalla nuova bitmap
            //Graphics g = Graphics.FromImage(returnBitmap);
            ////setto come centro di rotazione il centro dell'immagine
            //g.TranslateTransform((float)max / 2, (float)max);
            ////ruoto
            //g.RotateTransform(angle);
            ////disegno l'immagine nel sistema Graphic ruotato
            //g.DrawImage(b, new Point(0, 0));
            //return returnBitmap;
            return null;
        }

        private Bitmap RotateImage_GOOD(Bitmap b, float angle, RectangleF max, float minX, float minY)
        {
            //creo una nuova bitmap su cui effettuare la rotazione
            Bitmap returnBitmap = new Bitmap((int)max.Width, (int)max.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

            try
            {
                //creo un nuovo oggetto Graphics dalla nuova bitmap
                using (Graphics g = Graphics.FromImage(returnBitmap))
                {
                    g.Clear(Color.DarkBlue);
                    g.InterpolationMode = InterpolationMode.Low;
                    //setto come centro di rotazione il centro dell'immagine
                    g.TranslateTransform(Math.Abs(minX), Math.Abs(minY));
                    //ruoto
                    g.RotateTransform(angle);
                    //disegno l'immagine nel sistema Graphic ruotato
                    g.DrawImage(b, new Point(0, 0));
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("CutImage-rotateImage error", ex);
				TraceLog.WriteLine("CutImage-rotateImage error Parameters. Angle: " + angle.ToString() + "Rectangle X:" + max.X.ToString() + " Y:" + max.Y.ToString()
					+ " Width:" + max.Width.ToString() + " Height:" + max.Height.ToString() + " minX: " + minX.ToString() + " minY: " + minY.ToString());
                
                if (ex is OutOfMemoryException)
                {
                    OnOutOfMemoryRaised();
                }

            }


            //returnBitmap.Save("c:\\ritaglio2.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return returnBitmap;
        }

        private Bitmap RotateImage(Bitmap b, float angle, RectangleF max, float minX, float minY)
        {
            //creo una nuova bitmap su cui effettuare la rotazione
            Bitmap returnBitmap = new Bitmap((int)max.Width, (int)max.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);

            try
            {
                //creo un nuovo oggetto Graphics dalla nuova bitmap
                using (Graphics g = Graphics.FromImage(returnBitmap))
                {
                    g.Clear(Color.DarkBlue);
                    g.InterpolationMode = InterpolationMode.Low;
                    //setto come centro di rotazione il centro dell'immagine
                    g.TranslateTransform(Math.Abs(minX), Math.Abs(minY));
                    //ruoto
                    g.RotateTransform(angle);
                    //disegno l'immagine nel sistema Graphic ruotato
                    g.DrawImage(b, new Point(0, 0));
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("CutImage-rotateImage error", ex);
				TraceLog.WriteLine("CutImage-rotateImage error Parameters. Angle: " + angle.ToString() + "Rectangle X:" + max.X.ToString() + " Y:" + max.Y.ToString()
					+ " Width:" + max.Width.ToString() + " Height:" + max.Height.ToString() + " minX: " + minX.ToString() + " minY: " + minY.ToString());
                
                if (ex is OutOfMemoryException)
                {
                    OnOutOfMemoryRaised();
                }

            }


            //returnBitmap.Save("c:\\ritaglio2.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            return returnBitmap;
        }


        static double GetDistanza2Punti(float x0, float y0, float x1, float y1) 
        { 
            return Math.Sqrt(((x1 - x0) * (x1 - x0)) + ((y1 - y0) * (y1 - y0)));
        }

        




    }
}
