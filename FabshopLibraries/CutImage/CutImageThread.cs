using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Drawing;
using TraceLoggers;

namespace Breton.CutImages
{
	public class CutImageThread
	{
		#region Variables
		private Thread thWorkerThread;
		private AutoResetEvent evShutDown = new AutoResetEvent(false);
		private AutoResetEvent evCut = new AutoResetEvent(false);
        private AutoResetEvent evDel = new AutoResetEvent(false);

		private Stack<CutParam> cutParameters;
        private Stack<int> delBlocks;
		protected bool mDisposed = false;

        private Bitmap mDefaultImage;

	    private bool _createSquareImage = false;

		private class CutParam
		{
			public CutImage cut;
			public int numSag;
			public int length;
			public int height;
			public float angle;
			public PointF[] boundingBox;
		}

		public delegate void CutChangeHandler(object sender, int numSag, Bitmap ritaglio);
		public event CutChangeHandler CutChange;
		#endregion

		#region Constructor
        public CutImageThread(Bitmap defaultImage)
		{
			cutParameters = new Stack<CutParam>();
            delBlocks = new Stack<int>();

            mDefaultImage = defaultImage;

			thWorkerThread = new Thread(new ThreadStart(ExecutionThread));
			thWorkerThread.Name = "CutImageThread_ExecutionThread";
			thWorkerThread.Start();
		}
		#endregion

		#region IDisposable Members

		/// *********************************************************************
		/// <summary>
		/// Distruttore chiamato dall'esterno
		/// </summary>
		/// *********************************************************************
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// *********************************************************************
		/// <summary>
		/// Distruttore interno effettivo
		/// </summary>
		/// <param name="disposing">true = chiamata esplicita / false = Garbage Collector</param>
		/// *********************************************************************
		protected virtual void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.mDisposed)
			{				
				if (thWorkerThread == null)
					return;

				evShutDown.Set();
				thWorkerThread.Join();
				thWorkerThread = null;

				mDisposed = true;
			}
		}

		public bool Disposed
		{
			get { return mDisposed; }
		}

	    public bool CreateSquareImage
	    {
	        get { return _createSquareImage; }
	        set { _createSquareImage = value; }
	    }

	    #endregion

		#region Public Methods

		public void AddCut(CutImage cut, int numSag, int length, int height, float angle, PointF[] boundingBox)
		{
			// Aggiunge i parametri per il ritaglio dell'immagine
			CutParam c = new CutParam();
			c.cut = cut;
			c.numSag = numSag;
			c.length = length;
			c.height = height;
			c.angle = angle;
			c.boundingBox = boundingBox;
			cutParameters.Push(c);

			// Risveglia il Thread dormiente
			evCut.Set();
		}

        public void AddDel(int delBlock)
        {
			// Aggiunge l'indice del blocco da togliere dalla visualizzazione
            delBlocks.Push(delBlock);

			// Risveglia il Thread dormiente
            evDel.Set();
        }
		#endregion

		#region Operations
		/// <summary>
		/// Thread per la gestione dei messaggi arrivati
		/// </summary>
		private void ExecutionThread()
		{
			WaitHandle[] evList = new WaitHandle[3];
			evList[0] = evShutDown;
			evList[1] = evCut;
            evList[2] = evDel;

			//execute job
			CutImage();

			//thread loop
			while (true)
			{
				//wait for an event
				int index = WaitHandle.WaitAny(evList, -1, false);

				if (index == WaitHandle.WaitTimeout)
				{
					//execute job
					CutImage();
				}
				else
				{
					//index 0 means shutdown request
					if (index == 0)
						break;

					//index 1 means inner db notification
					if (index == 1)
					{
						//execute job
						CutImage();
					}
                    else if (index == 2)
                    {
                        //execute job
                        DelImage();
                    }
				}
			}
        }

		private void CutImage()
		{
			CutParam c = null;
			List<CutParam> cuts = new List<CutParam>();

			

			lock (cutParameters)
			{
				if (cutParameters.Count > 0)
				{
					// Rimuove tutti gli altri valori per il pezzo in questione
					while (cutParameters.Count > 0)
					{
						c = cutParameters.Pop();

						if (!cuts.Exists(delegate(CutParam x) { return x.numSag == c.numSag; }))
							cuts.Add(c);
					}
				}
			}
		    TraceLog.WriteLine("Cuts.Count:" + cuts.Count);
			foreach (CutParam cp in cuts)
			{
				cp.cut.Scala = 1;

			    DateTime startTime = DateTime.Now;
			    DateTime partial = startTime;
			    TimeSpan overallDuration;
			    TimeSpan opDuration;

                if (_createSquareImage)
                {
                    double squareDim;
                    //cp.cut.SetSquareCenteredBitmapOut(ref cp.boundingBox);
                    cp.cut.SetSquareCenteredBoundingBox(ref cp.boundingBox);
                }
                else
                {
                    //cp.cut.SetBitmapOut(cp.boundingBox);
                    cp.cut.SetBoundingBox(cp.boundingBox);
                }

			    opDuration = DateTime.Now - partial;
			    partial = DateTime.Now;
                TraceLog.WriteLine("Definizione ritaglio: " + opDuration.TotalSeconds);
				
				//ritaglio immagine della lastra caricata
                using (Bitmap ritaglio = cp.cut.GetBitmap(cp.length, cp.height, cp.angle, cp.boundingBox))
                //Bitmap ritaglio = cp.cut.GetImage(cp.length, cp.height, cp.angle, cp.boundingBox);
			    {
                    opDuration = DateTime.Now - partial;
                    partial = DateTime.Now;
                    TraceLog.WriteLine("esecuzione ritaglio: " + opDuration.TotalSeconds);

                    //ritaglio.Save(@"e:\img014.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    //ritaglio.SetResolution(20, 20);
                    //ritaglio.Save(System.Windows.Forms.Application.StartupPath + "\\" + cp.numSag.ToString() + "_small.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    OnCutChange(cp.numSag, ritaglio);

                    opDuration = DateTime.Now - partial;
                    partial = DateTime.Now;
                    TraceLog.WriteLine("Conclusione evento: " + opDuration.TotalSeconds);

                }
				
			}
		}

        private void DelImage()
        {
            lock (delBlocks)
            {
                while (delBlocks.Count > 0)
                {
                    OnCutChange(delBlocks.Pop(), mDefaultImage);
                }
            }            
        }

		#endregion

		#region Eventi
		/// <summary>
		/// Evento per il cambio materiale nel Render grande
		/// </summary>
		/// <param name="numSag">numero della sagoma a cui cambiare il materiale</param>
		/// <param name="ritaglio">Immagine del ritaglio</param>
		protected virtual void OnCutChange(int numSag, Bitmap ritaglio)
		{
			try
			{
				if (CutChange != null)
					CutChange(this, numSag, new Bitmap(ritaglio, ritaglio.Width, ritaglio.Height));
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("CutImageThread.OnCutChange: ", ex);
			}
		}

		#endregion
	}
}
 