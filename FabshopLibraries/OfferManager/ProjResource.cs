using System;
using Breton.SmartResources;

namespace Breton.OfferManager
{
	public class ProjResource
	{
		internal static readonly SmartResource gResource;

		static ProjResource()
		{
			gResource = new SmartResource("Resources.OfferManagerResource");
		}
	}
}