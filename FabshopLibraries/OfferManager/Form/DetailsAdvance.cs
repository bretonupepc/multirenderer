using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.TraceLoggers;
using Breton.Materials;
using Breton.ImportOffer;
using Breton.DesignUtil;
using Breton.MgzLevel;
using Breton.KeyboardInterface;
using Breton.Phases;  
using Breton.Articles;
using Breton.Stocks;
using VDProLib5;

namespace Breton.OfferManager.Form
{
    public partial class DetailsAdvance : System.Windows.Forms.Form
    {
        #region Variables
        private static bool formCreate;
        private DbInterface mDbInterface;
        private User mUser;
        private string mLanguage;
        private string mProjectCode;
        private ParameterControlPositions mFormPosition;
        private ParameterCollection mParam, mParamApp;
        private DetailCollection mDetails;
        private MaterialCollection mMaterials;
        private WorkPhaseCollection mWorkPhases;
        private WorkOrderCollection mWorkOrders;
        private UsedObjectCollection mUsedObjects;
        private Stock mStock;
		private DirectoryInfo dirXml, dirXmlDet;
		private string tmpXmlDir;
		private string tmpXmlDetDir;
        private string mFileXml;
        private Painter mDraw;
        private FieldCollection mImpost = null;
        private Field fGridThickness, fGridFinalThickness, fGridLength, fGridWidth;
        private bool imperial = false;				// Sistema in pollici o mm
        private bool mDialog;
        private string[] allPiece;
        private bool coord_assolute;
        private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;
        private int clickNumber = 0;
        private bool mProjectDraw;

        private Breton.OfferManager.Form.ProjectsAdvance.SelectProjectAdvanceEventHandler prjHandler;
        private Breton.OfferManager.Form.OrdersAdvance.SelectOrderAdvanceEventHandler ordHandler;
        #endregion

        public DetailsAdvance(DbInterface db, User user, string language, string projectCode, bool dialog)
        {
            InitializeComponent();

            if (db == null)
            {
                mDbInterface = new DbInterface();
                mDbInterface.Open("", "DbConnection.udl");
            }
            else
                mDbInterface = db;

            // Carica la posizione del form
            mFormPosition = new ParameterControlPositions(this.Name);
            mFormPosition.LoadFormPosition(this);

            mUser = user;

            mLanguage = language;
            ProjResource.gResource.ChangeLanguage(mLanguage);

            mProjectCode = projectCode;

            mDialog = dialog;

            // Crea il delegato per la gestione del'evento SelectProject
            prjHandler = new ProjectsAdvance.SelectProjectAdvanceEventHandler(ProgettoSelezionato);
            Breton.OfferManager.Form.ProjectsAdvance.SelectProjectAdvanceEvent += prjHandler;

            ordHandler = new OrdersAdvance.SelectOrderAdvanceEventHandler(OrdineSelezionato);
            Breton.OfferManager.Form.OrdersAdvance.SelectOrderAdvanceEvent += ordHandler;
        }

        #region Properties

        public static bool gCreate
        {
            set { formCreate = value; }
            get { return formCreate; }
        }

        #endregion

		#region Open Form & Load Parameters
		public new void ShowDialog()
		{
			if (LoadParameters())
				base.ShowDialog();
		}

		public new void Show()
		{
			if (LoadParameters())
				base.Show();
		}

		private bool LoadParameters()
		{
			Parameter par;
			string message, caption;

			ProjResource.gResource.LoadMessageBox(3, out caption, out message);

			mParam = new ParameterCollection(mDbInterface, true);
			while (!mParam.GetDataFromDb(null, null, "NUMBER", "IMPERIAL"))
			{
				if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
					return false;
			}
			if (mParam.GetObject(null, null, "NUMBER", "IMPERIAL", out par))
				imperial = par.gBoolValue;
			else
			{
				par = new Parameter(null, null, "NUMBER", "IMPERIAL", Parameter.ParTypes.BOOL, "0", "Sistema metrico in uso");
				mParam.AddObject(par);
				mParam.UpdateDataToDb();	// Aggiorna il DB con il parametro inserito
			}

			return true;
		}

		#endregion

        #region Form Functions
        private void DetailsAdvance_Load(object sender, EventArgs e)
        {
            Parameter par;

            // Carica le lingue del form
            ProjResource.gResource.LoadStringForm(this);
            LoadContextMenuString();

            mDetails = new DetailCollection(mDbInterface);
            mMaterials = new MaterialCollection(mDbInterface);
            mParamApp = new ParameterCollection(mDbInterface, false);

            mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridDetails, dataTableDetails);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridDetails);

			tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
			tmpXmlDetDir = tmpXmlDir + "\\Detail";

			dirXml = new DirectoryInfo(tmpXmlDir);
			dirXmlDet = new DirectoryInfo(tmpXmlDetDir);
           
			mDraw = new Painter(VDPreview, imperial);

            SetKeyboardControl();

			if (mDialog)
				btnSelectDetail.Visible = true;
			else
				btnSelectDetail.Visible = false;

            // Carica tutti i materiali dal database
            mMaterials.GetDataFromDb();

            if (mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode))
                FillTable();

            if (!dirXml.Exists)
            {
                dirXml.Create();
                dirXml.Attributes = FileAttributes.Hidden;
            }

			if (!dirXmlDet.Exists)
			{
				dirXmlDet.Create();
				dirXmlDet.Attributes = FileAttributes.Hidden;
			}

            // Carica l'ultima scelta per quanto riguarda la visualizzazione o meno del progetto
            if (mParamApp.GetDataFromDb("", "", "GRAPHICS", "SHOW_PROJECT") &&
                    mParamApp.GetObject("", "", "GRAPHICS", "SHOW_PROJECT", out par))
                miProject.Checked = par.gBoolValue;

            

            gCreate = true;
        }

        private void DetailsAdvance_FormClosed(object sender, FormClosedEventArgs e)
        {
            miLinearQuote.Checked = false;
            miRadiusQuote.Checked = false;
            miAngleQuote.Checked = false;

            mFormPosition.SaveFormPosition(this);
            mSortGrid = null;
			mColumnView = null;

			if (dirXmlDet.Exists)
				dirXmlDet.Delete(true);

            if (dirXml.Exists &&
                !Breton.TechnoMaster.Form.Working.gCreate &&
                !Breton.OfferManager.Form.Orders.gCreate &&
                !Breton.OfferManager.Form.Projects.gCreate &&
                !Breton.OfferManager.Form.Details.gCreate &&
                !Breton.OfferManager.Form.Offers.gCreate)
                dirXml.Delete(true);


            Breton.OfferManager.Form.ProjectsAdvance.SelectProjectAdvanceEvent -= prjHandler;
            Breton.OfferManager.Form.OrdersAdvance.SelectOrderAdvanceEvent -= ordHandler;
            
            mDraw = null;

            gCreate = false;
        }
        #endregion

        #region Gestione tabelle

        private void FillTable()
        {
            Detail det;

            dataTableDetails.Clear();

            // Scorre tutti i dettagli letti
            mDetails.MoveFirst();
            while (!mDetails.IsEOF())
            {
                // Legge il progetto attuale
                mDetails.GetObject(out det);

                dataTableDetails.BeginInit();
                dataTableDetails.BeginLoadData();
                // Aggiunge una riga alla volta
                AddRow(ref det);

                dataTableDetails.EndInit();
                dataTableDetails.EndLoadData();
            }
        }

        private void AddRow(ref Detail det)
        {
            // Array con i dati della riga
            object[] myArray = new object[15];
            myArray[0] = dataTableDetails.Rows.Count;
            myArray[1] = det.gId;
            myArray[2] = det.gCode;
            myArray[3] = det.gCustomerProjectCode;
            myArray[4] = det.gMaterialCode;
            myArray[5] = FindMaterialDescr(det.gMaterialCode);
            myArray[6] = StateEnumstrToString(det.gStateDet);
            fGridThickness.DoubleValue = det.gThickness;
            myArray[7] = double.Parse(fGridThickness.ToString());
            myArray[8] = det.gArticleCode;
            myArray[9] = det.gCodeOriginalShape;
            myArray[10] = det.gDescription;
            myArray[11] = MagDescription(det.gArticleCode);
            fGridFinalThickness.DoubleValue = det.gFinalThickness;
            myArray[12] = double.Parse(fGridFinalThickness.ToString());
            fGridLength.DoubleValue = det.gLength;
            myArray[13] = double.Parse(fGridLength.ToString());
            fGridWidth.DoubleValue = det.gWidth;
            myArray[14] = double.Parse(fGridWidth.ToString());


            // Crea una nuova riga nella tabella e la riempie di dati
            DataRow r = dataTableDetails.NewRow();
            r.ItemArray = myArray;
            dataTableDetails.Rows.Add(r);

            // Assegna l'id della riga tabella al dettaglio
            det.gTableId = int.Parse(r.ItemArray[0].ToString());

            // Passa all'elemento successivo
            mDetails.MoveNext();
        }

        #endregion

        #region Toolbar
        private void toolBar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
             
			string caption, message;

             switch (toolBar.Buttons.IndexOf(e.Button))
             {
                 // Riapri pezzo
                 case 0:
                     string mgz_code;
                     Detail det;
                     Article art;
                     ArticleCollection mArticle = new ArticleCollection(mDbInterface);
                     Coordinate coord = new Coordinate();
                     mStock = new Stock(mDbInterface);                     

                     if (dataGridDetails.Bookmark >= 0)
                     {
                         ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
                         if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                         {
                             if (dataGridDetails.SelectedRows.Count == 0)
                             {
                                 if (mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.Bookmark, 0].ToString()), out det))
                                 {
                                     this.Cursor = Cursors.WaitCursor;
                                     mWorkOrders = new WorkOrderCollection(mDbInterface);
                                     mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                     mUsedObjects = new UsedObjectCollection(mDbInterface);

                                     LoadTecnoPhases(det.gCode);
                                     DeleteAll();
                                     //ResetPhases();

                                     mArticle.GetSingleElementFromDb(det.gArticleCode);
                                     mArticle.GetObject(out art);
                                     // controlla se l'articolo � presente in magazzino
                                     mgz_code = mStock.GetStockCode(art.gCode);
                                     // se si lo sposta nel magazzino di default, altrimenti lo inserisce
                                     if (mgz_code != null)
                                     {
                                         if (!mStock.MoveArticle(art, coord))
                                             throw new ApplicationException("Error on article move");
                                     }
                                     else
                                     {
                                         if (!mStock.InsertArticle(art, coord))
                                             throw new ApplicationException("Error on article insert");
                                     }

                                     this.Cursor = Cursors.Default;
                                 }
                             }
                             else
                             {
                                 this.Cursor = Cursors.WaitCursor;
                                 for (int i = 0; i < dataGridDetails.SelectedRows.Count; i++)
                                 {
                                     if (mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.SelectedRows[i], 0].ToString()), out det))
                                     {                                         
                                         mWorkOrders = new WorkOrderCollection(mDbInterface);
                                         mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                         mUsedObjects = new UsedObjectCollection(mDbInterface);

                                         LoadTecnoPhases(det.gCode);
                                         DeleteAll();
                                         //ResetPhases();

                                         mArticle.GetSingleElementFromDb(det.gArticleCode);
                                         mArticle.GetObject(out art);
                                         // controlla se l'articolo � presente in magazzino
                                         mgz_code = mStock.GetStockCode(art.gCode);
                                         // se si lo sposta nel magazzino di default, altrimenti lo inserisce
                                         if (mgz_code != null)
                                         {
                                             if (!mStock.MoveArticle(art, coord))
                                                 throw new ApplicationException("Error on article move");
                                         }
                                         else
                                         {
                                             if (!mStock.InsertArticle(art, coord))
                                                 throw new ApplicationException("Error on article insert");
                                         }

                                     }
                                 }
                                 this.Cursor = Cursors.Default;
                             }
                             mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode);
                             FillTable();
                         }
                     }
                     break;

                 // Esci
                 case 2:
                     Close();
                     break;

                 // Refresh
                 case 4:
                     this.Cursor = Cursors.WaitCursor;

					 if (dirXmlDet.Exists)
					 {
						 while (dirXmlDet.GetFiles().Length > 0)
							 dirXmlDet.GetFiles()[0].Delete();
					 }

					 if (mDetails.RefreshData())
						 FillTable();

					 mProjectDraw = false;
					 CellValue();

                     this.Cursor = Cursors.Default;
                     break;

				 // Seleziona dettaglio
				 case 5:
					 if (dataGridDetails.Bookmark >= 0)
					 {
						 mDetails.GetObjectTable((int)dataGridDetails[dataGridDetails.Bookmark, 0], out det);
						 this.Tag = det.gCode;
					 }
					 Close();
					 break;
             }
        }
        #endregion

        #region Eventi dei controlli
        private void dataGridDetails_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
        {
            CellValue();
        }

        private void CellValue()
        {
            VDProLib5.vdLayer layer;
            Detail det;
            int i;

            if (dataGridDetails.Bookmark >= 0)
                i = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
            else
                return;

            try
            {
                if (mDetails.GetObjectTable(i, out det))
                {   
                    lblShapeInfo.Text = det.gDescription;

                    if (miProject.Checked)
                    {
                        if (coord_assolute)
                        {
                            if (!mProjectDraw)
                                DrawProject();

							if (mDraw.gVDCom.gCurrentLayer != null)
								mDraw.gVDCom.DeselectLayer(mDraw.gVDCom.gCurrentLayer);
							mDraw.gVDCom.SelectLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), 4, 5);

							mDraw.QuotasOff();
							mDraw.LayersProcessing(true);
                        }
                        else
                        {
                            // Spegne tutti i layer
							for (int j = 0; j < mDraw.gVDCom.LayersCount; j++)
                            {
								layer = mDraw.gVDCom.Layer(j);
                                mDraw.OnOffLayer(layer.LayerName, true);
                            }
							mDraw.gVDCom.ActiveLayer = VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim();
                            // Accende il layer interessato
                            mDraw.OnOffLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), false);
                            mDraw.OnOffLayer(VDInterface.Layers.TEXT.ToString() + det.gCodeOriginalShape.Trim(), false);
                        }
                    }
                    else
                    {
                        // Estrae l'xml del pezzo interessato
						mFileXml = tmpXmlDetDir + "\\" + det.gCode.Trim() + ".xml";
                        if (!File.Exists(mFileXml) && !mDetails.GetXmlParameterFromDb(det, mFileXml))
                            throw new ApplicationException("Error getting xml");

                        mDraw.DrawXml(mFileXml, true, false, false, true, true); // Disegna il pezzo
                        mDraw.ZoomAll();
                    }

					miLinearQuote_Click(null, null);
					miRadiusQuote_Click(null, null);
					miAngleQuote_Click(null, null);

                    miAutoQuote.Checked = false;
					miDesignQuote.Checked = false;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DetailsAdvance.CellValue() :" + ex.Message);
            }
        }

        private void dataGridDetails_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Detail det;

            int i = (int)dataGridDetails[e.Row, "T_ID"];
            mDetails.GetObjectTable(i, out det);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (det.gStateDet.Trim() == Detail.State.SHAPE_LOADED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_IN_PROGRESS.ToString())
            {
                e.CellStyle.BackColor = Color.Yellow;
                e.CellStyle.BackColor2 = Color.LightYellow;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_COMPLETED.ToString())
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_STANDBY.ToString())
            {
                e.CellStyle.BackColor = Color.Yellow;
                e.CellStyle.BackColor2 = Color.LightYellow;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_RELOADED.ToString())
            {
                e.CellStyle.BackColor = Color.Orange;
                e.CellStyle.BackColor2 = Color.Gold;
            }
        }

        private void VDPreview_MouseWheelEvent(object sender, AxVDProLib5._DVdrawEvents_MouseWheelEvent e)
        {
            if (miLock.Checked)
                e.cancel = 1;
        }

        private void VDPreview_MouseDownEvent(object sender, AxVDProLib5._DVdrawEvents_MouseDownEvent e)
        {
            Detail det;
            string codeOriginalDet;
           

            // Se il click non avviene per posizionare una quota
            if (!miLinearQuote.Checked && !miRadiusQuote.Checked && !miAngleQuote.Checked)
            {
                codeOriginalDet = mDraw.gVDCom.SelectLayerClick();

				if (codeOriginalDet == null)
					return;

                    mDetails.MoveFirst();
                    while (!mDetails.IsEOF())
                    {
                        mDetails.GetObject(out det);
                        if (det.gCodeOriginalShape.Trim() == codeOriginalDet)
                        {
                            dataGridDetails.Bookmark = SelectBookmark(det);
                            break;
                        }
                        mDetails.MoveNext();
                    }
            }
            // Se � stato cliccato il tasto destro azzero il conteggio
            if (e.button == 2)
            {
                clickNumber = 0;
                return;
            }

            clickNumber++;
            // Se deve inserire una quota lineare si aspetta 3 click
            if (miLinearQuote.Checked)
            {
				if (clickNumber < 3)
				{
					if (!mDraw.gVDCom.GetSnapPoint())
						clickNumber = 0;
				}
				else
					clickNumber = 0;
            }
            // Se deve inserire una quota angolare si aspetta 4 click
            else if (miAngleQuote.Checked)
            {
				if (clickNumber < 4)
				{
					if (!mDraw.gVDCom.GetSnapPoint())
						clickNumber = 0;
				}
				else
					clickNumber = 0;
            }
        }
        
        private void tsslblOrtho_Click(object sender, EventArgs e)
        {
            // Imposta l'ortogonalit� al layer attivo
            if (mDraw.gVDCom.OrthoMode)
            {
                tsslblOrtho.Text = "ORTHO: Off";
				mDraw.gVDCom.OrthoMode = false;
            }
            else
            {
                tsslblOrtho.Text = "ORTHO: On";
				mDraw.gVDCom.OrthoMode = true;
            }
        }

        private void DetailsAdvance_Resize(object sender, EventArgs e)
        {
            if (mDraw != null)
                mDraw.ZoomAll();
        }   
        
        private void VDPreview_Resize(object sender, EventArgs e)
        {
            if (mDraw != null)
                mDraw.ZoomAll();
        }
        #endregion

        #region Gestione eventi
        private void ProgettoSelezionato(object sender, string ev)
        {
            mProjectCode = ev;
            if (mProjectCode == null)
            {
                dataTableDetails.Clear();
                return;
            }

			if (dirXmlDet.Exists)
			{
				while (dirXmlDet.GetFiles().Length > 0)
					dirXmlDet.GetFiles()[0].Delete();
			}

            if (mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode))
            {
                FillTable();
                mProjectDraw = false;
                CellValue();
            }
        }

        private void OrdineSelezionato(object sender, string ev)
        {
            Project prj;
            ProjectCollection mProjects = new ProjectCollection(mDbInterface);

			if (dirXmlDet.Exists)
			{
				while (dirXmlDet.GetFiles().Length > 0)
					dirXmlDet.GetFiles()[0].Delete();
			}

            if (mProjects.GetDataFromDb(Project.Keys.F_CODE, ev) &&
                mProjects.GetObject(out prj) &&
                mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
            {
                mProjectCode = prj.gCode;
                FillTable();
                mProjectDraw = false;
                CellValue();
            }
        }
        #endregion

        #region Private methods
        private string FindMaterialDescr(string matCode)
        {
            Material mat;
            mMaterials.MoveFirst();

            while (!mMaterials.IsEOF())
            {
                if (mMaterials.GetObject(out mat) && mat.gCode == matCode)
                    return mat.gDescription;

                mMaterials.MoveNext();
            }
            return "";
        }

        private void AttivaForm(string formName)
        {
            System.Windows.Forms.Form[] frm = new System.Windows.Forms.Form[this.ParentForm.MdiChildren.Length];
            frm = this.ParentForm.MdiChildren;
            for (int i = 0; i < frm.Length; i++)
            {
                if (frm[i].Name == formName)
                    frm[i].Activate();
            }
        }

        private string StateEnumToString(Detail.State st)
        {
            switch (st)
            {
                case Detail.State.SHAPE_LOADED:
                    return ProjResource.gResource.LoadFixString(this, 1);
                case Detail.State.SHAPE_IN_PROGRESS:
                    return ProjResource.gResource.LoadFixString(this, 2);
                case Detail.State.SHAPE_COMPLETED:
                    return ProjResource.gResource.LoadFixString(this, 3);
                case Detail.State.SHAPE_STANDBY:
                    return ProjResource.gResource.LoadFixString(this, 4);
                case Detail.State.SHAPE_ANALIZED:
                    return ProjResource.gResource.LoadFixString(this, 5);
                case Detail.State.SHAPE_RELOADED:
                    return ProjResource.gResource.LoadFixString(this, 6);
                default:
                    return "";
            }
        }

        private string StateEnumstrToString(string st)
        {
            if (st.Trim() == Detail.State.SHAPE_LOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 1);
            else if (st.Trim() == Detail.State.SHAPE_IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 2);
            else if (st.Trim() == Detail.State.SHAPE_COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 3);
            else if (st.Trim() == Detail.State.SHAPE_STANDBY.ToString())
                return ProjResource.gResource.LoadFixString(this, 4);
            else if (st.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 5);
            else if (st.Trim() == Detail.State.SHAPE_RELOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 6);
            else
                return "";
        }

        private int StateEnumstrToInt(string st)
        {
            if (st.Trim() == Detail.State.SHAPE_LOADED.ToString())
                return 0;
            else if (st.Trim() == Detail.State.SHAPE_IN_PROGRESS.ToString())
                return 1;
            else if (st.Trim() == Detail.State.SHAPE_COMPLETED.ToString())
                return 2;
            else if (st.Trim() == Detail.State.SHAPE_STANDBY.ToString())
                return 3;
            else if (st.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                return 4;
            else if (st.Trim() == Detail.State.SHAPE_RELOADED.ToString())
                return 5;
            else
                return -1;
        }

        private Detail.State StateStringToEnum(string state)
        {
            if (state == ProjResource.gResource.LoadFixString(this, 1))
                return Detail.State.SHAPE_LOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 2))
                return Detail.State.SHAPE_IN_PROGRESS;
            else if (state == ProjResource.gResource.LoadFixString(this, 3))
                return Detail.State.SHAPE_COMPLETED;
            else if (state == ProjResource.gResource.LoadFixString(this, 4))
                return Detail.State.SHAPE_STANDBY;
            else if (state == ProjResource.gResource.LoadFixString(this, 5))
                return Detail.State.SHAPE_ANALIZED;
            else if (state == ProjResource.gResource.LoadFixString(this, 6))
                return Detail.State.SHAPE_RELOADED;
            else
                return Detail.State.NONE;
        }

        /// ***********************************************************
        /// LoadTecnoPhases
        /// <summary>
        /// Carica le collection con gli ordini e le fasi di lavoro
        /// </summary>
        /// <param name="code">Codice del dettaglio</param>
        /// <returns></returns>
        /// ***********************************************************        
        private bool LoadTecnoPhases(string code)
        {
            int i = 0;
            string[] usObjs = new string[0];
            string[] wkOrds = new string[0];
            WorkPhase wk = new WorkPhase();

            if (!mWorkPhases.GetDataFromDb(WorkPhase.Keys.F_SEQUENCE, null, code, null, null, null))
                return false;

            mWorkPhases.MoveFirst();
            while (!mWorkPhases.IsEOF())
            {
                mWorkPhases.GetObject(out wk);

                string[] tmp = new string[wkOrds.Length + 1];
                wkOrds.CopyTo(tmp, 0);
                wkOrds = tmp;
                wkOrds[i] = wk.gWorkOrderCode;

                string[] tmp2 = new string[usObjs.Length + 1];
                usObjs.CopyTo(tmp2, 0);
                usObjs = tmp2;
                usObjs[i] = wk.gUsedObjectCode;

                i++;
                mWorkPhases.MoveNext();
            }
            string[] tmp3 = new string[usObjs.Length + 1];
            usObjs.CopyTo(tmp3, 0);
            usObjs = tmp3;
            usObjs[i] = wk.gPhaseDUsedObject;

            if (!mWorkOrders.GetDataFromDb(WorkOrder.Keys.F_SEQUENCE, null, null, null, null, null, wkOrds))
                return false;

            if (!mUsedObjects.GetDataFromDb(usObjs))
                return false;

            return true;
        }

        ///**********************************************************
        /// Delete All
        /// <summary>
        /// Elimina il processo produttivo (fase tecno) per il pezzo
        /// </summary>
        /// <returns></returns>
        /// *********************************************************
        private bool DeleteAll()
        {
            try
            {
                // Cancellazione di tutti i record caricati
                mWorkPhases.MoveFirst();
                while (!mWorkPhases.IsEmpty())
                    mWorkPhases.DeleteObject();
                if (!mWorkPhases.UpdateDataToDb())
                    throw new ApplicationException("Error on work phases updated");

                mUsedObjects.MoveFirst();
                while (!mUsedObjects.IsEmpty())
                    mUsedObjects.DeleteObject();
                if (!mUsedObjects.UpdateDataToDb())
                    throw new ApplicationException("Error on used objects updated");

                mWorkOrders.MoveFirst();
                while (!mWorkOrders.IsEmpty())
                    mWorkOrders.DeleteObject();
                if (!mWorkOrders.UpdateDataToDb())
                    throw new ApplicationException("Error on work orders updated");

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Details.DeleteAll Error: " + ex.Message);
                return false;
            }
            finally
            {
                mWorkPhases = null;
                mUsedObjects = null;
                mWorkOrders = null;
            }
        }

        private void SetKeyboardControl()
        {
            mImpost = new FieldCollection(imperial);

            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//DimZ
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//FinalThickenss
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//Length
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//Width

            fGridThickness = mImpost.GetField(0);
            fGridFinalThickness = mImpost.GetField(1);
            fGridLength = mImpost.GetField(2);
            fGridWidth = mImpost.GetField(3);

            dataGridDetails.Columns[7].Caption += " (" + fGridThickness.GetMeasureUnit() + ")";
            dataGridDetails.Columns[12].Caption += " (" + fGridFinalThickness.GetMeasureUnit() + ")";
            dataGridDetails.Columns[13].Caption += " (" + fGridLength.GetMeasureUnit() + ")";
            dataGridDetails.Columns[14].Caption += " (" + fGridWidth.GetMeasureUnit() + ")";    
        }

        private void ResetVal()
        {
            fGridThickness.EnterValue("");
            fGridFinalThickness.EnterValue("");
            fGridLength.EnterValue("");
            fGridWidth.EnterValue("");
        }

        /// <summary>
        /// Ricerca la riga nella griglia che identifica l'oggetto
        /// </summary>
        /// <param name="det">oggetto da ricercare nella griglia</param>
        /// <returns></returns>
        private int SelectBookmark(Detail det)
        {
            for (int i = 0; i < dataTableDetails.Rows.Count; i++)
            {
                if ((int)dataGridDetails[i, 0] == det.gTableId)
                    return i;
            }

            return -1;
        }

        private void LoadContextMenuString()
        {
            miProject.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 0);
            miZoomAll.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 1);
            miLock.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 2);
            miZoom.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 3);
            mi10.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 4);
            mi50.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 5);
            mi100.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 6);
            mi150.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 7);
            mi200.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 8);
            mi300.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 9);
            miQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 10);
            miLinearQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 11);
            miRadiusQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 12);
            miAngleQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 13);
            miAutoQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 14);
            miReset.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 15);
			miDesignQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 16);

        }
        #endregion

        #region Gestione magazzino

        private string MagDescription(string articleCode)
        {
            Breton.Mgz.MgzUtility magUt = new Breton.Mgz.MgzUtility(mDbInterface);
            return magUt.MagDescription(articleCode);
        }

        #endregion

        #region Context Menu
        private void mi10_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(0.10);
        }

        private void mi50_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(0.50);
        }

        private void mi100_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(1.00);
        }

        private void mi150_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(1.50);
        }

        private void mi200_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(2.00);
        }

        private void mi300_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(3.00);
        }

        private void miLock_Click(object sender, EventArgs e)
        {
            // Verifica se � stato richiesto di bloccare il pannello di disegno
            if (miLock.Checked)
				mDraw.gVDCom.DisableMouseWheelPan = true; // Disabilita l'uso della ruota del mouse
            else
				mDraw.gVDCom.DisableMouseWheelPan = false; // Abilita l'uso della ruota del mouse
        }

        private void miZoomAll_Click(object sender, EventArgs e)
        {
            mDraw.ZoomAll();
        }

        private void miProject_CheckedChanged(object sender, EventArgs e)
        {
            Parameter par;
            Detail det;
            string mFileXml;
            int i = 0;

            miLinearQuote.Checked = false;
            miRadiusQuote.Checked = false;
            miAngleQuote.Checked = false;
            tsslOperation.Text = "";

            // Disegna tutto il progetto
            if (miProject.Checked)
            {
                DrawProject();
            }
            
            CellValue();

            if (mParamApp.GetDataFromDb("", "", "GRAPHICS", "SHOW_PROJECT") &&
                 mParamApp.GetObject("", "", "GRAPHICS", "SHOW_PROJECT", out par))
            {
                par.gBoolValue = miProject.Checked;
            }
            else
            {
                par = new Parameter(Environment.MachineName, Application.ProductName, "GRAPHICS", "SHOW_PROJECT", Parameter.ParTypes.INT, miProject.Checked.ToString(), "");
                mParamApp.AddObject(par);
            }
            mParamApp.UpdateDataToDb();
        }

        private void DrawProject()
        {
			string fileOffer;
			OfferCollection offers = new OfferCollection(mDbInterface, mUser);
			Offer off;
			OrderCollection orders = new OrderCollection(mDbInterface, mUser);
			Order ord;
			ProjectCollection projects = new ProjectCollection(mDbInterface);
			Project prj;
            Detail det;
            string mFileXml;
            int i = 0;

            allPiece = new string[0];
            mDetails.MoveFirst();
            while (!mDetails.IsEOF())
            {
                mDetails.GetObject(out det);

				mFileXml = tmpXmlDetDir + "\\" + det.gCode.Trim() + ".xml";

                if (!File.Exists(mFileXml) && !mDetails.GetXmlParameterFromDb(det, mFileXml))
                    throw new ApplicationException("Error getting xml");

                string[] tmp = new string[allPiece.Length + 1];
                allPiece.CopyTo(tmp, 0);
                allPiece = tmp;
                allPiece[i] = mFileXml;
                mDetails.MoveNext();
                i++;
            }

			Shape shp = new Shape();

            // Verifica che il pezzo che si sta disegnando abbia le coordinate assolute
            if (allPiece != null && allPiece.Length > 0)
            {
                XmlAnalizer.ReadShapeXml(allPiece[0], ref shp, true);

                if (shp.gContours.Count > 0)
                {
                    // Disegna tutto il progetto con le coordinate assolute
                    mDraw.DrawXml(allPiece, true, false, false, true, true);
                    mDraw.ZoomAll();
                    coord_assolute = true;
                }
                else
                {
                    coord_assolute = false;
                }
            }

			// Cerca l'xml dell'intera offerta
			mDetails.MoveFirst();
			mDetails.GetObject(out det);
			projects.GetSingleElementFromDb(det.gCustomerProjectCode);
			projects.GetObject(out prj);
			orders.GetSingleElementFromDb(prj.gCustomerOrderCode);
			orders.GetObject(out ord);
			offers.GetSingleElementFromDb(ord.gOfferCode);
			offers.GetObject(out off);
			fileOffer = tmpXmlDetDir + "\\" + off.gCode.Trim() + ".xml";

			if (!File.Exists(fileOffer))
				offers.GetXmlParameterFromDb(off, tmpXmlDetDir + "\\" + off.gCode.Trim() + ".xml");

			mDraw.DrawTopXml(fileOffer, prj.gCodeOriginalProject.Trim());

            mProjectDraw = true;
        }

        private void miLinearQuote_Click(object sender, EventArgs e)
        {
            if (miLinearQuote.Checked)
            {
                tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 7);

                // Disattiva pulsanti eventualmente attive
                miRadiusQuote.Checked = false;
                miAngleQuote.Checked = false;

                clickNumber = 0;

				mDraw.gVDCom.LinearQuote();
            }
            else
            {
                // Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
                tsslOperation.Text = "";
            }
        }

        private void miRadiusQuote_Click(object sender, EventArgs e)
        {
            if (miRadiusQuote.Checked)
            {
                tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 8);

                // Disattiva pulsanti eventualmente attive
                miLinearQuote.Checked = false;
                miAngleQuote.Checked = false;

                clickNumber = 0;
			
				mDraw.gVDCom.RadiusQuote();
			}
            else
            {
                // Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
                tsslOperation.Text = "";
            }
        }

        private void miAngleQuote_Click(object sender, EventArgs e)
        {
            if (miAngleQuote.Checked)
            {
                tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 9);

                // Disattiva pulsanti eventualmente attive
                miLinearQuote.Checked = false;
                miRadiusQuote.Checked = false;

                clickNumber = 0;

				mDraw.gVDCom.AngleQuote();
            }
            else
            {
                // Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
                tsslOperation.Text = "";
            }
        }


		private void miLinearQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.LinearQuoting = miLinearQuote.Checked;
		}

		private void miRadiusQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.RadiusQuoting = miRadiusQuote.Checked;
		}

		private void miAngleQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.AngleQuoting = miAngleQuote.Checked;
		}

        private void miAutoQuote_Click(object sender, EventArgs e)
        {
            Detail det;
            int i;

            if (dataGridDetails.Bookmark >= 0)
                i = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
            else
                return;

            mDetails.GetObjectTable(i, out det);

            // Verifica se il pulsante � cliccato
            if (miAutoQuote.Checked)
            {
                mDraw.OnOffLayer(VDInterface.Layers.PROCESSING.ToString() + det.gCodeOriginalShape.Trim(), true);
                mDraw.OnOffLayer(VDInterface.Layers.QUOTAS.ToString() + det.gCodeOriginalShape.Trim(), false);
            }
            else
            {
                mDraw.OnOffLayer(VDInterface.Layers.PROCESSING.ToString() + det.gCodeOriginalShape.Trim(), false);
                mDraw.OnOffLayer(VDInterface.Layers.QUOTAS.ToString() + det.gCodeOriginalShape.Trim(), true);
            }
        }

        private void miReset_Click(object sender, EventArgs e)
        {
            miLinearQuote.Checked = false;
            miRadiusQuote.Checked = false;
            miAngleQuote.Checked = false;

            // Ridisegna tutto
            if (miProject.Checked)
            {
                DrawProject();
            }

            CellValue();

            tsslOperation.Text = "";
        }


		private void miDesignQuote_Click(object sender, EventArgs e)
		{
			int i;
			Detail det;

			if (miProject.Checked)
			{
				if (miDesignQuote.Checked)
				{
					// Visualizza le quote del intero progetto
					mDetails.MoveFirst();
					while (!mDetails.IsEOF())
					{
						mDetails.GetObject(out det);
						mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), false);
						mDetails.MoveNext();
					}

					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString(), false);
				}
				else
				{
					mDetails.MoveFirst();
					while (!mDetails.IsEOF())
					{
						mDetails.GetObject(out det);
						mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), true);
						mDetails.MoveNext();
					}

					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString(), true);
				}
			}
			else
			{
				// Visualizza le quote del singolo pezzo
				if (dataGridDetails.Bookmark >= 0)
					i = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
				else
					return;

				mDetails.GetObjectTable(i, out det);

				// Verifica se il pulsante � cliccato
				if (miDesignQuote.Checked)
					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), false);
				else
					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), true);
			}

			mDraw.ZoomAll();
		}

		#endregion
	}
}