using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.ImportOffer;
using Breton.TraceLoggers;
using Breton.Materials;
using Breton.KeyboardInterface;
using Breton.DesignUtil;
using Breton.Phases;

namespace Breton.OfferManager
{
    public partial class ReloadOrder : System.Windows.Forms.Form
    {
        #region Variables
        private DbInterface mDbInterface;
        private User mUser;
        private ParameterControlPositions mFormPosition;
        private string mLanguage;
        private string mNewXmlFile;
        private Offer mOff;
        private Order oldO;

        struct ProjectQty
        {
            public string codeOriginalProject;
            public int qty;
            public ArrayList detailsQty;
        }

        struct DetailQty
        {
            public string codeOriginalShape;
            public int qty;
        }

        private ArrayList mProjectsCode;
        private ArrayList mDetailsCode;
        private ArrayList mOldProjectsQty = new ArrayList();
        private ArrayList mNewProjectsQty = new ArrayList();
        //private DetailCollection mReplaceDetails;               // Contiene i dettagli sostituiti

        private MaterialCollection mMaterials;
		private OrderCollection Orders;
        private FieldCollection mImpost = null;
        private Field fGridThickness, fGridFinalThickness;
        private ParameterCollection mParam;
        private bool imperial = false;				// Sistema in pollici o mm
        private DirectoryInfo dirXml;
        private string tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
        private string mFileXml;
        private Painter mDraw;
        private WorkOrderCollection mWorkOrders;
        private WorkPhaseCollection mWorkPhases;
        private UsedObjectCollection mUsedObjects;

        private DialogResult response;
        private C1Utils.TDBGridSort mSortGridPrj;
        private C1Utils.TDBGridSort mSortGridDet;
		private C1Utils.TDBGridColumnView mColumnViewPrj;
		private C1Utils.TDBGridColumnView mColumnViewDet;
        #endregion

        public ReloadOrder(DbInterface db, User user, string language, Offer off, string newXmlFile)
        {
            InitializeComponent();

            if (db == null)
            {
                mDbInterface = new DbInterface();
                mDbInterface.Open("", "DbConnection.udl");
            }
            else
                mDbInterface = db;

            // Carica la posizione del form
            mFormPosition = new ParameterControlPositions(this.Name);
            mFormPosition.LoadFormPosition(this);

            mUser = user;
            
            mLanguage = language;
            ProjResource.gResource.ChangeLanguage(mLanguage);

            mNewXmlFile = newXmlFile;
            mOff = off;
        }

        #region Properties
        public DialogResult gShowDialog()
        {
            this.ShowDialog();
            return response;
        }
        #endregion

		#region Open Form & Load Parameters
		public new void ShowDialog()
		{
			if (LoadParameters())
				base.ShowDialog();
		}

		public new void Show()
		{
			if (LoadParameters())
				base.Show();
		}

		private bool LoadParameters()
		{
			Parameter par;
			string message, caption;

			ProjResource.gResource.LoadMessageBox(3, out caption, out message);

			mParam = new ParameterCollection(mDbInterface, true);
			while (!mParam.GetDataFromDb(null, null, "NUMBER", "IMPERIAL"))
			{
				if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
					return false;
			}

			if (mParam.GetObject(null, null, "NUMBER", "IMPERIAL", out par))
				imperial = par.gBoolValue;
			else
			{
				par = new Parameter(null, null, "NUMBER", "IMPERIAL", Parameter.ParTypes.BOOL, "0", "Sistema metrico in uso");
				mParam.AddObject(par);
				mParam.UpdateDataToDb();	// Aggiorna il DB con il parametro inserito
			}

			return true;
		}

		#endregion

        #region Form Functions
        private void ReloadOrder_Load(object sender, EventArgs e)
        {
            Orders = new OrderCollection(mDbInterface, mUser);
            oldO = new Order(mDbInterface);
            Order newO = new Order(mDbInterface);
            mMaterials = new MaterialCollection(mDbInterface);
			mSortGridPrj = new Breton.C1Utils.TDBGridSort(dataGridProjects, dataTableProjects);
			mColumnViewPrj = new Breton.C1Utils.TDBGridColumnView(dataGridProjects);
			mSortGridDet = new Breton.C1Utils.TDBGridSort(dataGridDetails, dataTableDetails);
			mColumnViewDet = new Breton.C1Utils.TDBGridColumnView(dataGridDetails);

            Project prj;
            Project.Message msg = Project.Message.NONE;
            string message, caption;
            Parameter par;

            try
            {
                // Carica le lingue del form
                ProjResource.gResource.LoadStringForm(this);

				mDraw = new Painter(VDPreview, imperial);

                SetKeyboardControl();

                dirXml = new DirectoryInfo(tmpXmlDir);
                if (!dirXml.Exists)
                {
                    dirXml.Create();
                    dirXml.Attributes = FileAttributes.Hidden;
                }

                this.Cursor = Cursors.WaitCursor;

                // Carica tutti i materiali dal database
                mMaterials.GetDataFromDb();

                #region Caricamento dati Vecchi
                // Carica l'ordine associato all'offerta
                if (!Orders.GetDataFromDb(Order.Keys.F_CODE, mOff.gCode) || !Orders.GetObject(out oldO))
                    throw new ApplicationException("Error read order from db");

                // Carica tutti i progetti dell'ordine
                if (!oldO.gProjects.GetDataFromDb(Project.Keys.F_CODE_ORIGINAL_PROJECT, oldO.gCode))
                    throw new ApplicationException("Error read projects from db");

                // Carica tutti i dettagli di ogni progetto
                oldO.gProjects.MoveFirst();
                while (!oldO.gProjects.IsEOF())
                {
                    oldO.gProjects.GetObject(out prj);

                    if (!prj.gDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
                        throw new ApplicationException("Error read details from db");

                    oldO.gProjects.MoveNext();
                }

                // Analisi degli stati in cui si trovano tutti i progetti e i relativi pezzi
                CheckProjectsState(ref oldO);
                #endregion

                #region Caricamento dati Nuovi
                // Carica ordine, progetti e dettagli della nuova offerta
                if (!newO.ReadFileXml(mNewXmlFile, out msg))
                    throw new ApplicationException("Error load new xml offer");
                #endregion

                SetProjectDetailsQty(oldO, ref mOldProjectsQty);
                SetProjectDetailsQty(newO, ref mNewProjectsQty);

                // Dopo avere analizzato l'ordine vecchio (verificando gli stati in cui si trova ogni progetto) e il nuovo, 
                // e aver caricato successivamente le rispettive quantit�, verifico le operazioni richieste
                CheckRequestOperation(oldO, newO);

                // Carica la griglia impostando le operazioni necessarie
                LoadGrid();
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ReloadOrder.ReloadOrder_Load ", ex);
                ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
				if (msg == Project.Message.NO_MATERIAL)
					message += ". " + ProjResource.gResource.LoadFixString("Offers", 5);
				else if (msg == Project.Message.NO_CUSTOMER)
					message += ". " + ProjResource.gResource.LoadFixString("Offers", 6);
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
				this.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void ReloadOrder_FormClosed(object sender, FormClosedEventArgs e)
        {
            mDraw = null;

            mFormPosition.SaveFormPosition(this);
            mSortGridPrj = null;
            mSortGridDet = null;
			mColumnViewPrj = null;
			mColumnViewDet = null;
        }
        #endregion
        
        #region Toolbar
        /// <summary>
        /// Esegue le operazioni indicate dalla griglia
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConferma_Click(object sender, EventArgs e)
        {
            Project prj;
            Detail det;
            ArrayList indexDelete = new ArrayList();
            string caption, message;
            bool transaction = !mDbInterface.TransactionActive();
			bool delete = false;

            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (transaction)
                    mDbInterface.BeginTransaction();

                // Scorre la collection verificando le operazioni che si possono fare e infine effettua la conferma
                oldO.gProjects.MoveFirst();
                while (!oldO.gProjects.IsEOF())
                {
                    oldO.gProjects.GetObject(out prj);

                    switch (prj.gImportOperation)
                    {
                        #region Eliminazione progetto
                        // Operazione da effettuare: Eliminazione
                        case Project.ImportOpertation.DELETE:
                            switch (prj.gImportState)
                            {
                                // E' possibile eliminarlo direttamente
                                case Project.ImportState.ELIMINABLE:
                                    // Inserisce il tableId del progetto da eliminare
                                    indexDelete.Add(prj.gTableId);
                                    break;

                                // Non � possibile eliminare direttamente il progetto
                                case Project.ImportState.NOT_ELIMINABLE_NO_PIECE:
                                    // Non si fa niente.
                                    break;

                                // Non � possibile eliminare direttamente il progetto in quanto esiste fisicamente
                                case Project.ImportState.NOT_ELIMINABLE_PIECE:
                                    // Non si fa niente.
                                    break;
                            }
                            break;
                        #endregion

                        #region Inserimento progetto
                        // Operazione da effettuare: Inserimento
                        case Project.ImportOpertation.INSERT:
                            // Non ci sono operazioni da fare. Si pu� aspettare che venga fatto UpdateDataToDb.
                            // I dati sono gi� corretti

							oldO.gStateOrd = Order.State.ORDER_LOADED.ToString();
                            break;
                        #endregion

                        #region Modifica progetto
                        // Operazione da effettuare: Modifica
                        case Project.ImportOpertation.UPDATE:
                            // Scorre tutti i dettagli e vede che operazioni sono necessarie
                            prj.gDetails.MoveFirst();
                            while (!prj.gDetails.IsEOF())
                            {
                                prj.gDetails.GetObject(out det);

                                switch (det.gImportOperation)
                                {
                                    #region Eliminazione dettaglio
                                    // Operazione da effettuare: Eliminazione
                                    case Detail.ImportOpertation.DELETE:
                                        switch (det.gImportState)
                                        {
                                            // E' possibile eliminarlo direttamente
                                            case Detail.ImportState.LOADED:
                                            case Detail.ImportState.ANALIZED:
                                            case Detail.ImportState.OPTIMIZING:
                                                prj.gDetails.DeleteObject(det);
												delete = true;
                                                break;

                                            // In qualsiasi altro stato non � possibile effettuare l'eliminazione
                                            default:
                                                // non si pu� fare niente
                                                break;
                                        }
                                        break;
                                    #endregion

                                    #region Inserimento dettaglio
                                    // Operazione da effettuare: Inserimento
                                    case Detail.ImportOpertation.INSERT:
                                        // Non ci sono operazioni da fare. Si pu� aspettare che venga fatto UpdateDataToDb.
                                        // I dati sono gi� corretti 
										prj.gStatePrj = Project.State.PROJ_LOADED.ToString();
										oldO.gStateOrd = Order.State.ORDER_LOADED.ToString();
                                        break;
                                    #endregion

                                    #region Modifica dettaglio
                                    // Operazione da effettuare: Modifica
                                    case Detail.ImportOpertation.UPDATE:
                                        switch (det.gImportState)
                                        {
                                            // E' possibile modificare direttamente
                                            case Detail.ImportState.LOADED:
                                                // I dati sono gia pronti
                                                break;

                                            case Detail.ImportState.ANALIZED:
                                            case Detail.ImportState.OPTIMIZING:
                                                if (!LoadTecnoPhases(det.gCode))
                                                    throw new ApplicationException("Error get tecno phases from db");
                                                if (!DeleteAll())
                                                    throw new ApplicationException("Error delete tecno phases from db");
                                                 // Eliminate le fasi tecno i dati sono pronti a essere salvati.
                                                break;

                                            // Se non si trovano in uno dei primi tre stati
                                            default:
                                                det.gState = DbObject.ObjectStates.Unchanged;
                                                break;

                                        }
                                        break;
                                    #endregion
                                }
								if (!delete)			
									prj.gDetails.MoveNext();

								delete = false;
                            }
                            if (!prj.gDetails.UpdateDataToDb())
                                throw new ApplicationException("Error update details");
                            break;
                        #endregion
                    }

                    oldO.gProjects.MoveNext();
                }

                // Elimina i progetti
                for (int i = 0; i < indexDelete.Count; i++)
                    oldO.gProjects.DeleteObjectTable((int)indexDelete[i]);

                if (!oldO.gProjects.UpdateDataToDb())
                    throw new ApplicationException("Error update projects");

				if (!Orders.UpdateDataToDb())
					throw new ApplicationException("Error update order");

                // Crea gli articoli per i nuovi dettagli inseriti
                oldO.gProjects.MoveFirst();
                while (!oldO.gProjects.IsEOF())
                {
                    oldO.gProjects.GetObject(out prj);

                    if (!prj.gDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
                        throw new ApplicationException("Error read details from db");
                    
                    prj.gDetails.CreateArticles();

                    oldO.gProjects.MoveNext();
                }

                if (transaction)
                    mDbInterface.EndTransaction(true);

                // Se l'operazione � andata a buon fine
                response = DialogResult.Yes;
            }
            catch(Exception ex)
            {
                if (transaction)
                    mDbInterface.EndTransaction(false);

                TraceLog.WriteLine("ReloadOrder.btnConferma_Click ", ex);
                ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Se l'operazione � andata a buon fine
                response = DialogResult.No;
            }
            finally
            {
                this.Cursor = Cursors.Default;
                Close();
            }
        }

        /// <summary>
        /// Chiude la finestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            response = DialogResult.No;
            Close();
        }
        #endregion

        #region Eventi dei controlli
        private void lblInfo_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.Location;
            p.X += this.Cursor.Size.Width;// btnQuestion.Width;
            p.Y += this.Cursor.Size.Height;// btnQuestion.Height;
            panelInfo.Visible = true;
            panelInfo.Location = p;
        }

        private void lblInfo_MouseLeave(object sender, EventArgs e)
        {
            panelInfo.Visible = false;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Metodo che analizza gli stati dei progetti e dei pezzi
        /// </summary>
        private void CheckProjectsState(ref Order ord)
        {
            Project prj;

            ord.gProjects.MoveFirst();
            // Scorre tutti i progetti dell'ordine
            while (!ord.gProjects.IsEOF())
            {
                ord.gProjects.GetObject(out prj);
                prj.gDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode.Trim());
                ord.gProjects.SetImportState(ref prj);
                ord.gProjects.MoveNext();
            }
        }

        /// <summary>
        /// Verifica le operazioni possibili su questa offerta
        /// </summary>
        /// <param name="oldO">ordine vecchio</param>
        /// <param name="newO">ordine nuovo</param>
		private void CheckRequestOperation(Order oldO, Order newO)
		{
			ProjectCollection tmpProjects = new ProjectCollection(mDbInterface);
			bool findProject = false;
			bool updateOrder = false;
			Project oldPrj, newPrj, tmpPrj, copyPrj;
			Detail tmpDet, copyDet;
			int oldPrjQty, newPrjQty;
			mProjectsCode = new ArrayList();

			// Scorre tutti i progetti dell'ordine vecchio
			oldO.gProjects.MoveFirst();
			while (!oldO.gProjects.IsEOF())
			{
				oldO.gProjects.GetObject(out oldPrj);

				// Verifica se il progetto in esame � gia stato analizzato
				if (mProjectsCode.IndexOf(oldPrj.gCodeOriginalProject.Trim()) < 0)
				{
					// Aggiungo il progetto alla lista dei progetti singoli
					mProjectsCode.Add(oldPrj.gCodeOriginalProject.Trim());

					// Scorre tutti i progetti del nuovo ordine
					newO.gProjects.MoveFirst();
					while (!newO.gProjects.IsEOF())
					{
						newO.gProjects.GetObject(out newPrj);

						// Inizia il controllo tra i 2 progetti
						if (oldPrj.gCodeOriginalProject.Trim() == newPrj.gCodeOriginalProject.Trim())
						{
							// Verifica delle quantit�:
							newPrjQty = GetProjectQty(mNewProjectsQty, newPrj.gCodeOriginalProject);
							oldPrjQty = GetProjectQty(mOldProjectsQty, oldPrj.gCodeOriginalProject);

							#region 1. Quantit� nuova > Quantit� vecchia, ci sono progetti da aggiungere
							if (newPrjQty > oldPrjQty)
							{
								// Prima di impostare i progetti nuovi da inserire (la quantit� in pi� trovata sulla nuova offerta), 
								// va a vedere se il progetto ha subito modifiche nei pezzi e in caso imposta la modifica
								for (int i = 0; i < oldPrjQty; i++)
								{
									oldO.gProjects.GetObject(out tmpPrj);
									if (tmpPrj.gImportOperation == Project.ImportOpertation.NONE)
									{
										CheckDetails(ref tmpPrj, newPrj);
									}
									oldO.gProjects.MoveNext();
								}
								// Riporta il cursore in posizione originale
								for (int i = 0; i < oldPrjQty; i++)
									oldO.gProjects.MovePrevious();

								// Inserisce i progetti mancanti
								for (int i = 0; i < newPrjQty - oldPrjQty; i++)
								{
									// Imposta l'operazione da effettuare
									copyPrj = new Project(mDbInterface, newPrj);
									copyPrj.gCustomerOrderCode = oldO.gCode;
									copyPrj.gImportOperation = Project.ImportOpertation.INSERT;

									// Scorre tutti i dettagli per impostare l'operazione di INSERT
									newPrj.gDetails.MoveFirst();
									while (!newPrj.gDetails.IsEOF())
									{
										newPrj.gDetails.GetObject(out tmpDet);
										//Copia il nuovo pezzo
										copyDet = new Detail(tmpDet);
										copyDet.gImportOperation = Detail.ImportOpertation.INSERT;
										copyDet.gShapeInfo = tmpDet.gShapeInfo;
										copyPrj.gDetails.AddObject(copyDet);

										newPrj.gDetails.MoveNext();
									}
									oldO.gProjects.AddObject(copyPrj);
								}
							}
							#endregion

							#region 2. Quantit� nuova = Quantit� vecchia
							else if (newPrjQty == oldPrjQty)
							{
								// Non dovendo inserire o eliminare progetti dato che la quantit� � uguale alla precedente
								// va a vedere se il progetto ha subito modifiche nei pezzi e in caso imposta la modifica
								for (int i = 0; i < oldPrjQty; i++)
								{
									oldO.gProjects.GetObject(out tmpPrj);
									if (tmpPrj.gImportOperation == Project.ImportOpertation.NONE)
									{
										CheckDetails(ref tmpPrj, newPrj);
									}
									oldO.gProjects.MoveNext();
								}
								// Riporta il cursore in posizione originale
								for (int i = 0; i < oldPrjQty; i++)
									oldO.gProjects.MovePrevious();
							}
							#endregion

							#region 3. Quantit� nuova < Quantit� vecchia, ci sono progetti da eliminare
							else if (newPrjQty < oldPrjQty)
							{
								// Aggiunge i progetti vecchi alla collection temporanea
								tmpProjects.AddObject(oldPrj);
								oldPrj.gState = DbObject.ObjectStates.Unchanged;
								for (int i = 1; i < oldPrjQty; i++)
								{
									oldO.gProjects.MoveNext();
									oldO.gProjects.GetObject(out tmpPrj);
									tmpProjects.AddObject(tmpPrj);
									tmpPrj.gState = DbObject.ObjectStates.Unchanged;
								}
								// Riporta il cursore in posizione originale
								for (int i = 1; i < oldPrjQty; i++)
									oldO.gProjects.MovePrevious();

								// Ordina i progetti caricati per stato per vedere quali sono i pi� prioritari per l'eliminazione
								tmpProjects.SortByImportState();

								// Imposta i progetti da eliminare
								tmpProjects.MoveFirst();
								for (int i = 0; i < oldPrjQty - newPrjQty; i++)
								{
									tmpProjects.GetObject(out tmpPrj);
									tmpPrj = oldO.gProjects.GetSameObject(tmpPrj);
									// Imposta l'operazione da effettuare
									tmpPrj.gImportOperation = Project.ImportOpertation.DELETE;
									// Scorre tutti i dettagli per impostare l'operazione di DELETE
									tmpPrj.gDetails.MoveFirst();
									while (!tmpPrj.gDetails.IsEOF())
									{
										tmpPrj.gDetails.GetObject(out tmpDet);
										tmpDet.gImportOperation = Detail.ImportOpertation.DELETE;
										tmpPrj.gDetails.MoveNext();
									}

									tmpProjects.MoveNext();
								}

								// Dopo aver impostato i progetti da eliminare (la quantit� in meno trovata sulla nuova offerta), 
								// va a vedere se il progetto ha subito modifiche nei pezzi e in caso modifica i progetti rimasti
								for (int i = 0; i < oldPrjQty; i++)
								{
									oldO.gProjects.GetObject(out tmpPrj);
									if (tmpPrj.gImportOperation == Project.ImportOpertation.NONE)
									{
										CheckDetails(ref tmpPrj, newPrj);
									}
									oldO.gProjects.MoveNext();
								}
								// Riporta il cursore in posizione originale
								for (int i = 0; i < oldPrjQty; i++)
									oldO.gProjects.MovePrevious();
							}
							#endregion

							findProject = true;
							break;
						}
						newO.gProjects.MoveNext();
					}
				}
				else
					findProject = true;

				#region 4. Elimina i progetti che non esistono pi� nella nuova offerta
				if (!findProject)
				{
					// Se il progetto vecchio non � stato trovato tra quelli nuovi deve essere eliminato

					// Imposta l'operazione da effettuare
					oldPrj.gImportOperation = Project.ImportOpertation.DELETE;
					// Scorre tutti i dettagli per impostare l'operazione di DELETE
					oldPrj.gDetails.MoveFirst();
					while (!oldPrj.gDetails.IsEOF())
					{
						oldPrj.gDetails.GetObject(out tmpDet);
						tmpDet.gImportOperation = Detail.ImportOpertation.DELETE;
						oldPrj.gDetails.MoveNext();
					}
				}
				#endregion

				findProject = false;
				oldO.gProjects.MoveNext();
			}

			#region 5. Inserisce gli eventuali progetti nuovi
			tmpProjects = oldO.gProjects;
			InsertNewProject(ref tmpProjects, newO.gProjects);
			oldO.gProjects = tmpProjects;
			#endregion


			// Scorre tutti i progetti dell'ordine vecchio
			oldO.gProjects.MoveFirst();
			while (!oldO.gProjects.IsEOF())
			{
				oldO.gProjects.GetObject(out oldPrj);

				if (oldPrj.gImportOperation == Project.ImportOpertation.UPDATE)
				{
					newO.gProjects.MoveFirst();
					while (!newO.gProjects.IsEOF())
					{
						newO.gProjects.GetObject(out newPrj);

						if (oldPrj.gCodeOriginalProject.Trim() == newPrj.gCodeOriginalProject.Trim())
						{
							oldPrj.Copy(newPrj);
							updateOrder = true;
						}

						newO.gProjects.MoveNext();
					}
				}
				oldO.gProjects.MoveNext();
			}

			if (updateOrder)
			{
				oldO.Copy(newO);

				oldO.gRevision = mOff.gRevision;
				oldO.gDate = mDbInterface.DateTimeNow;
			}
		}

        /// <summary>
        /// Verifica le operazioni richieste sui singoli dettagli
        /// </summary>
        /// <param name="oldPrj">Progetto vecchio</param>
        /// <param name="newPrj">Progetto nuovo</param>
        /// <returns></returns>
        private bool CheckDetails(ref Project oldPrj, Project newPrj)
        {
            DetailCollection tmpDetails = new DetailCollection(mDbInterface);
            bool findDetail = false;
            Detail oldDet, newDet, tmpDet;
            int oldDetQty, newDetQty;
            mDetailsCode = new ArrayList();
            int oldCursor = 0, newCursor = 0;

            try
            {
                // Scorre tutti i dettagli del vecchio progetto
                oldPrj.gDetails.MoveFirst();
                while (!oldPrj.gDetails.IsEOF())
                {
                    oldPrj.gDetails.GetObject(out oldDet);

                    // Verifica se il dettaglio in esame � gia stato analizzato
                    if (mDetailsCode.IndexOf(oldDet.gCodeOriginalShape.Trim()) < 0)
                    {
                        // Aggiungo il dettaglio alla lista dei dettagli singoli
                        mDetailsCode.Add(oldDet.gCodeOriginalShape.Trim());

                        // Scorre tutti i dettagli del nuovo progetto
                        newPrj.gDetails.MoveFirst();
                        while (!newPrj.gDetails.IsEOF())
                        {
                            newPrj.gDetails.GetObject(out newDet);

                            if (oldDet.gCodeOriginalShape.Trim() == newDet.gCodeOriginalShape.Trim())
                            {
                                // Verifica delle quantit�:
                                newDetQty = GetDetailQty(mNewProjectsQty, newDet.gCodeOriginalShape);
                                oldDetQty = GetDetailQty(mOldProjectsQty, oldDet.gCodeOriginalShape);

                                #region 1. Quantit� nuova > Quantit� vecchia, ci sono progetti da aggiungere
                                if (newDetQty > oldDetQty)
                                {
                                    // Se ci sono state delle modifiche su almeno un pezzo imposto il nuovo stato del progetto
                                    oldPrj.gImportOperation = Project.ImportOpertation.UPDATE;

                                    // Se ha avuto delle modifiche
                                    if (newDet.gRevision > oldDet.gRevision)
                                    {
                                        // Prima di impostare i dettagli nuovi da inserire (la quantit� in pi� trovata sulla nuova offerta), 
                                        // imposta i pezzi estenti come da modificare
                                        for (int i = 0; i < oldDetQty; i++)
                                        {
                                            oldPrj.gDetails.GetObject(out tmpDet);

                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                            {
                                                // Copia il nuovo pezzo sul vecchio
                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                tmpDet.Copy(newDet);
                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                            }

                                            oldPrj.gDetails.MoveNext();
                                        }

                                        // Riporta il cursore in posizione originale
                                        for (int i = 0; i < oldDetQty; i++)
                                            oldPrj.gDetails.MovePrevious();

                                        #region Verifica pezzi figli
                                        // Verifica se il pezzo modificato � un pezzo figlio. 
                                        // Nel caso lo sia � necessario modificare anche il pezzo padre
                                        Shape s = new Shape();
                                        XmlAnalizer.ReadShapeXml(newDet.gShapeInfo, ref s, false);

                                        #region Laminazione
                                        if (s.gType == Shape.PieceType.LAMINATION)
                                        {
                                            oldPrj.gDetails.MoveFirst();
                                            while (!oldPrj.gDetails.IsEOF())
                                            {
                                                oldPrj.gDetails.GetObject(out tmpDet);
                                                if (s.gShapeNumberJoin.ToString() == tmpDet.gCodeOriginalShape.Trim())
                                                {
                                                    newPrj.gDetails.MoveFirst();
                                                    while (!newPrj.gDetails.IsEOF())
                                                    {
                                                        if (oldDet.gCodeOriginalShape.Trim() == newDet.gCodeOriginalShape.Trim())
                                                        {
                                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                                            {
                                                                // Copia il nuovo pezzo sul vecchio
                                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                                tmpDet.Copy(newDet);
                                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                                            }
                                                        }

                                                        newPrj.gDetails.MoveNext();
                                                    }
                                                }
                                                oldPrj.gDetails.MoveNext();
                                            }

                                            // Riporta i cursori nella posizione iniziale
                                            oldPrj.gDetails.MoveTo(oldCursor);
                                            newPrj.gDetails.MoveTo(newCursor);
                                        }
                                        #endregion

                                        #region Veletta
                                        else if (s.gType == Shape.PieceType.VELETTA)
                                        {
                                            string tmpFile = System.IO.Path.GetTempPath() + "Temp_Detail_Xml\\oldXml.xml";
                                            string oldFile;
                                            // In questo caso per doversi adattare alla gestione dell'xml fatta in DesignMaster
                                            // � necessario estrarre dal database il vecchio file xml
                                            oldPrj.gDetails.MoveFirst();
                                            while (!oldPrj.gDetails.IsEOF())
                                            {
                                                oldPrj.gDetails.GetObject(out tmpDet);
                                                oldFile = tmpDet.gShapeInfo;
                                                oldPrj.gDetails.GetXmlParameterFromDb(tmpDet, tmpFile);
                                                tmpDet.gShapeInfo = oldFile;
                                                Shape sOld = new Shape();
                                                XmlAnalizer.ReadShapeXml(tmpFile, ref sOld, false);

                                                if (s.gShapeIdJoin.ToString() == sOld.gId.ToString())
                                                {
                                                    newPrj.gDetails.MoveFirst();
                                                    while (!newPrj.gDetails.IsEOF())
                                                    {
                                                        if (oldDet.gCodeOriginalShape.Trim() == newDet.gCodeOriginalShape.Trim())
                                                        {
                                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                                            {
                                                                // Copia il nuovo pezzo sul vecchio
                                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                                tmpDet.Copy(newDet);
                                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                                            }
                                                        }

                                                        newPrj.gDetails.MoveNext();
                                                    }
                                                }
                                                oldPrj.gDetails.MoveNext();
                                            }

                                            // Riporta i cursori nella posizione iniziale
                                            oldPrj.gDetails.MoveTo(oldCursor);
                                            newPrj.gDetails.MoveTo(newCursor);
                                        }
                                        #endregion
                                        #endregion
                                    }

                                    // Inserisce i dettagli mancanti
                                    for (int i = 0; i < newDetQty - oldDetQty; i++)
                                    {
                                        // Imposta l'operazione da effettuare
                                        newDet.gImportOperation = Detail.ImportOpertation.INSERT;
                                        newDet.gCustomerProjectCode = oldPrj.gCode;
                                        oldPrj.gDetails.AddObject(newDet);
                                    }
                                }
                                #endregion

                                #region 2. Quantit� nuova = Quantit� vecchia
                                else if (newDetQty == oldDetQty)
                                {
                                    // Se ha avuto delle modifiche
                                    if (newDet.gRevision > oldDet.gRevision)
                                    {
                                        // Non dovendo inserire o eliminare dettagli dato che la quantit� � uguale alla precedente
                                        // imposta i pezzi estenti come da modificare
                                        for (int i = 0; i < oldDetQty; i++)
                                        {
                                            oldPrj.gDetails.GetObject(out tmpDet);

                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                            {
                                                // Copia il nuovo pezzo sul vecchio
                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                tmpDet.Copy(newDet);
                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                            }

                                            oldPrj.gDetails.MoveNext();
                                        }

                                        // Riporta il cursore in posizione originale
                                        for (int i = 0; i < oldDetQty; i++)
                                            oldPrj.gDetails.MovePrevious();

                                        // Se ci sono state delle modifiche su almeno un pezzo imposto il nuovo stato del progetto
                                        oldPrj.gImportOperation = Project.ImportOpertation.UPDATE;
                                        
                                        #region Verifica pezzi figli
                                        // Verifica se il pezzo modificato � un pezzo figlio. 
                                        // Nel caso lo sia � necessario modificare anche il pezzo padre
                                        Shape s = new Shape();
                                        XmlAnalizer.ReadShapeXml(newDet.gShapeInfo, ref s, false);

                                        #region Laminazione
                                        if (s.gType == Shape.PieceType.LAMINATION)
                                        {
                                            oldPrj.gDetails.MoveFirst();
                                            while (!oldPrj.gDetails.IsEOF())
                                            {
                                                oldPrj.gDetails.GetObject(out tmpDet);
                                                if (s.gShapeNumberJoin.ToString() == tmpDet.gCodeOriginalShape.Trim())
                                                {
                                                    newPrj.gDetails.MoveFirst();
                                                    while (!newPrj.gDetails.IsEOF())
                                                    {
                                                        newPrj.gDetails.GetObject(out newDet);

                                                        if (tmpDet.gCodeOriginalShape.Trim() == newDet.gCodeOriginalShape.Trim())
                                                        {
                                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                                            {
                                                                // Copia il nuovo pezzo sul vecchio
                                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                                tmpDet.Copy(newDet);
                                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                                            }
                                                        }

                                                        newPrj.gDetails.MoveNext();
                                                    } 
                                                }
                                                oldPrj.gDetails.MoveNext();
                                            }

                                            // Riporta i cursori nella posizione iniziale
                                            oldPrj.gDetails.MoveTo(oldCursor);
                                            newPrj.gDetails.MoveTo(newCursor);
                                        }
                                        #endregion

                                        #region Veletta
                                        else if (s.gType == Shape.PieceType.VELETTA)
                                        {
                                            string tmpFile = System.IO.Path.GetTempPath() + "Temp_Detail_Xml\\oldXml.xml";
                                            string oldFile;
                                            // In questo caso per doversi adattare alla gestione dell'xml fatta in DesignMaster
                                            // � necessario estrarre dal database il vecchio file xml
                                            oldPrj.gDetails.MoveFirst();
                                            while (!oldPrj.gDetails.IsEOF())
                                            {
                                                oldPrj.gDetails.GetObject(out tmpDet);
                                                oldFile = tmpDet.gShapeInfo;
                                                oldPrj.gDetails.GetXmlParameterFromDb(tmpDet, tmpFile);
                                                tmpDet.gShapeInfo = oldFile;
                                                Shape sOld = new Shape();
                                                XmlAnalizer.ReadShapeXml(tmpFile, ref sOld, false);

                                                if (s.gShapeIdJoin.ToString() == sOld.gId.ToString())
                                                {
                                                    newPrj.gDetails.MoveFirst();
                                                    while (!newPrj.gDetails.IsEOF())
                                                    {
                                                        newPrj.gDetails.GetObject(out newDet);

                                                        if (tmpDet.gCodeOriginalShape.Trim() == newDet.gCodeOriginalShape.Trim())
                                                        {
                                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                                            {
                                                                // Copia il nuovo pezzo sul vecchio
                                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                                tmpDet.Copy(newDet);
                                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                                            }
                                                        }

                                                        newPrj.gDetails.MoveNext();
                                                    }
                                                }
                                                oldPrj.gDetails.MoveNext();
                                            }

                                            // Riporta i cursori nella posizione iniziale
                                            oldPrj.gDetails.MoveTo(oldCursor);
                                            newPrj.gDetails.MoveTo(newCursor);
                                        }
                                        #endregion
                                        #endregion
                                    }
                                }
                                #endregion

                                #region 3. Quantit� nuova < Quantit� vecchia, ci sono progetti da eliminare
                                else if (newDetQty < oldDetQty)
                                {
                                    // Se ci sono state delle modifiche su almeno un pezzo imposto il nuovo stato del progetto
                                    oldPrj.gImportOperation = Project.ImportOpertation.UPDATE;

                                    // Aggiunge i dettagli vecchi alla collection temporanea
                                    tmpDetails.AddObject(oldDet);
                                    oldDet.gState = DbObject.ObjectStates.Unchanged;
                                    for (int i = 1; i < oldDetQty; i++)
                                    {
                                        oldPrj.gDetails.MoveNext();
                                        oldPrj.gDetails.GetObject(out tmpDet);
                                        tmpDetails.AddObject(tmpDet);
                                        tmpDet.gState = DbObject.ObjectStates.Unchanged;
                                    }
                                    // Riporta il cursore in posizione originale
                                    for (int i = 1; i < oldDetQty; i++)
                                        oldPrj.gDetails.MovePrevious();

                                    // Ordina i dettagli caricati per stato per vedere quali sono i pi� prioritari per l'eliminazione
                                    tmpDetails.SortByImportState();

                                    // Imposta i dettagli da eliminare
                                    tmpDetails.MoveFirst();
                                    for (int i = 0; i < oldDetQty - newDetQty; i++)
                                    {
                                        tmpDetails.GetObject(out tmpDet);
                                        tmpDet = oldPrj.gDetails.GetSameObject(tmpDet);
                                        // Imposta l'operazione da effettuare
                                        tmpDet.gImportOperation = Detail.ImportOpertation.DELETE;
                                        tmpDetails.MoveNext();
                                    }

                                    // Se ha avuto delle modifiche
                                    if (newDet.gRevision > oldDet.gRevision)
                                    {
                                        // Dopo aver impostato i dettagli da eliminare (la quantit� in meno trovata sulla nuova offerta), 
                                        // imposta i pezzi estenti come da modificare
                                        for (int i = 0; i < oldDetQty; i++)
                                        {
                                            oldPrj.gDetails.GetObject(out tmpDet);

                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                            {
                                                // Copia il nuovo pezzo sul vecchio
                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                tmpDet.Copy(newDet);
                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                            }

                                            oldPrj.gDetails.MoveNext();
                                        }

                                        // Riporta il cursore in posizione originale
                                        for (int i = 0; i < oldDetQty; i++)
                                            oldPrj.gDetails.MovePrevious();

                                        #region Verifica pezzi figli
                                        // Verifica se il pezzo modificato � un pezzo figlio. 
                                        // Nel caso lo sia � necessario modificare anche il pezzo padre
                                        Shape s = new Shape();
                                        XmlAnalizer.ReadShapeXml(newDet.gShapeInfo, ref s, false);

                                        #region Laminazione
                                        if (s.gType == Shape.PieceType.LAMINATION)
                                        {
                                            oldPrj.gDetails.MoveFirst();
                                            while (!oldPrj.gDetails.IsEOF())
                                            {
                                                oldPrj.gDetails.GetObject(out tmpDet);
                                                if (s.gShapeNumberJoin.ToString() == tmpDet.gCodeOriginalShape.Trim())
                                                {
                                                    newPrj.gDetails.MoveFirst();
                                                    while (!newPrj.gDetails.IsEOF())
                                                    {
                                                        if (oldDet.gCodeOriginalShape.Trim() == newDet.gCodeOriginalShape.Trim())
                                                        {
                                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                                            {
                                                                // Copia il nuovo pezzo sul vecchio
                                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                                tmpDet.Copy(newDet);
                                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                                            }
                                                        }

                                                        newPrj.gDetails.MoveNext();
                                                    }
                                                }
                                                oldPrj.gDetails.MoveNext();
                                            }

                                            // Riporta i cursori nella posizione iniziale
                                            oldPrj.gDetails.MoveTo(oldCursor);
                                            newPrj.gDetails.MoveTo(newCursor);
                                        }
                                        #endregion

                                        #region Veletta
                                        else if (s.gType == Shape.PieceType.VELETTA)
                                        {
                                            string tmpFile = System.IO.Path.GetTempPath() + "Temp_Detail_Xml\\oldXml.xml";
                                            string oldFile;
                                            // In questo caso per doversi adattare alla gestione dell'xml fatta in DesignMaster
                                            // � necessario estrarre dal database il vecchio file xml
                                            oldPrj.gDetails.MoveFirst();
                                            while (!oldPrj.gDetails.IsEOF())
                                            {
                                                oldPrj.gDetails.GetObject(out tmpDet);
                                                oldFile = tmpDet.gShapeInfo;
                                                oldPrj.gDetails.GetXmlParameterFromDb(tmpDet, tmpFile);
                                                tmpDet.gShapeInfo = oldFile;
                                                Shape sOld = new Shape();
                                                XmlAnalizer.ReadShapeXml(tmpFile, ref sOld, false);

                                                if (s.gShapeIdJoin.ToString() == sOld.gId.ToString())
                                                {
                                                    newPrj.gDetails.MoveFirst();
                                                    while (!newPrj.gDetails.IsEOF())
                                                    {
                                                        if (oldDet.gCodeOriginalShape.Trim() == newDet.gCodeOriginalShape.Trim())
                                                        {
                                                            if (tmpDet.gImportOperation == Detail.ImportOpertation.NONE)
                                                            {
                                                                // Copia il nuovo pezzo sul vecchio
                                                                newDet.gCustomerProjectCode = tmpDet.gCustomerProjectCode;
                                                                tmpDet.Copy(newDet);
                                                                tmpDet.gImportOperation = Detail.ImportOpertation.UPDATE;
                                                            }
                                                        }

                                                        newPrj.gDetails.MoveNext();
                                                    }
                                                }
                                                oldPrj.gDetails.MoveNext();
                                            }

                                            // Riporta i cursori nella posizione iniziale
                                            oldPrj.gDetails.MoveTo(oldCursor);
                                            newPrj.gDetails.MoveTo(newCursor);
                                        }
                                        #endregion
                                        #endregion

                                        // Se ci sono state delle modifiche su almeno un pezzo imposto il nuovo stato del progetto
                                        oldPrj.gImportOperation = Project.ImportOpertation.UPDATE;
                                    }
                                }
                                #endregion

                                findDetail = true;
                                break;
                            }
                            newPrj.gDetails.MoveNext();
                            newCursor++;
                        }
                    }
                    else
                        findDetail = true;

                    #region 4. Elimina i dettagli che non esistono pi� nel nuovo progetto
                    if (!findDetail)
                    {
                        // Se il dettaglio vecchio non � stato trovato tra quelli nuovi deve essere eliminato
                        // Imposta l'operazione da effettuare
                        oldDet.gImportOperation = Detail.ImportOpertation.DELETE;

                        // Se ci sono state delle modifiche su almeno un pezzo imposto il nuovo stato del progetto
                        oldPrj.gImportOperation = Project.ImportOpertation.UPDATE;

                    }
                    #endregion

                    findDetail = false;
                    oldPrj.gDetails.MoveNext();
                    oldCursor++;
                }

                #region 5. Inserisce gli eventuali dettagli nuovi
                //tmpDetails = oldPrj.gDetails;
                InsertNewDetail(ref oldPrj, newPrj.gDetails);//, ref tmpDetails, newPrj.gDetails);
                //oldPrj.gDetails = tmpDetails;
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ReloadOrder.CheckDetails", ex);
                return false;
            }
        }

        /// <summary>
        /// Inserisce nella lista dei progetti tutti i progetti nuovi trovati nel nuovo file xml
        /// </summary>
        /// <param name="oldPrjs">progetti vecchi</param>
        /// <param name="newPrjs">progetti nuovi</param>
        private void InsertNewProject(ref ProjectCollection oldPrjs, ProjectCollection newPrjs)
        {
            Detail copyDet;
            Project nPr, oPr, copyPrj;
            Detail tmpDet;
            bool find = false;
            int prjQty;

            // Scorre tutti i progetti nuovi
            newPrjs.MoveFirst();
            while (!newPrjs.IsEOF())
            {
                newPrjs.GetObject(out nPr);
                // Scorre tutti i progetti vecchi e verifica quali sono i progetti mancanti
                oldPrjs.MoveFirst();
                while (!oldPrjs.IsEOF())
                {
                    oldPrjs.GetObject(out oPr);
                    if (nPr.gCodeOriginalProject.Trim() == oPr.gCodeOriginalProject.Trim())
                    {
                        find = true;
                        break;
                    }
                    oldPrjs.MoveNext();
                }

                if (!find)
                {
                    prjQty = GetProjectQty(mNewProjectsQty, nPr.gCodeOriginalProject);

                    // Imposta l'operazione da effettuare
                    copyPrj = new Project(mDbInterface, nPr);
                    copyPrj.gCustomerOrderCode = oldO.gCode;
                    copyPrj.gImportOperation = Project.ImportOpertation.INSERT;

                    // Scorre tutti i dettagli per impostare l'operazione di INSERT
                    nPr.gDetails.MoveFirst();
                    while (!nPr.gDetails.IsEOF())
                    {
                        nPr.gDetails.GetObject(out tmpDet);
                        //Copia il nuovo pezzo
                        copyDet = new Detail(tmpDet);
                        copyDet.gImportOperation = Detail.ImportOpertation.INSERT;
                        copyDet.gShapeInfo = tmpDet.gShapeInfo;
                        copyPrj.gDetails.AddObject(copyDet);

                        nPr.gDetails.MoveNext();
                    }
                    // Imposta l'operazione da effettuare
                    copyPrj = new Project(mDbInterface, nPr);
                    copyPrj.gCustomerOrderCode = oldO.gCode;
                    copyPrj.gImportOperation = Project.ImportOpertation.INSERT;

                    for (int i = 0; i < prjQty; i++)
                        oldPrjs.AddObject(copyPrj);
                }

                find = false;
                newPrjs.MoveNext();
            }
        }

        /// <summary>
        /// Inserisce nella lista dei dettagli tutti i dettagli nuovi trovati nel nuovo file xml
        /// </summary>
        /// <param name="oldDets">dettagli vecchi</param>
        /// <param name="newDets">dettagli nuovi</param>
        private void InsertNewDetail(ref Project oldPrj, DetailCollection newDets) //ref DetailCollection oldDets, DetailCollection newDets)
        {
            Detail nDt = null, oDt = null, copyDet = null;
            bool find = false;
            int detQty;

            // Scorre tutti i dettagli nuovi
            newDets.MoveFirst();
            while (!newDets.IsEOF())
            {
                nDt = new Detail();
                newDets.GetObject(out nDt);
                // Scorre tutti i dettagli vecchi e verifica quali sono i dettagli mancanti
                oldPrj.gDetails.MoveFirst();
                while (!oldPrj.gDetails.IsEOF())
                {
                    oldPrj.gDetails.GetObject(out oDt);
                    if (nDt.gCodeOriginalShape.Trim() == oDt.gCodeOriginalShape.Trim())
                    {
                        find = true;
                        break;
                    }
                    oldPrj.gDetails.MoveNext();
                }

                if (!find)
                {
                    detQty = GetDetailQty(mNewProjectsQty, nDt.gCodeOriginalShape);
                    // Imposta l'operazione da effettuare
                    
                    copyDet = new Detail(nDt);
                    copyDet.gCustomerProjectCode = oldPrj.gCode;
                    copyDet.gImportOperation = Detail.ImportOpertation.INSERT;
                    copyDet.gShapeInfo = nDt.gShapeInfo;

                    // Se ci sono state delle modifiche su almeno un pezzo imposto il nuovo stato del progetto
                    oldPrj.gImportOperation = Project.ImportOpertation.UPDATE;

                    for (int i = 0; i < detQty; i++)
                        oldPrj.gDetails.AddObject(copyDet);
                }

                find = false;
                newDets.MoveNext();
            }
        }

        /// <summary>
        /// Aggiorna la quantit� dei progetti e dei dettagli
        /// </summary>
        /// <param name="o">ordine da analizzare</param>
        /// <param name="collPrj">collection da caricare con le quantit� dei progetti</param>
        /// <returns></returns>
        private bool SetProjectDetailsQty(Order o, ref ArrayList collPrj)
        {
            ProjectQty pq = new ProjectQty();
            Project tmpPrj;
            DetailQty dq;
            Detail tmpDet;
            bool findPrj = false, findDet = false;

            // Scorre tutti i progetti dell'ordine
            o.gProjects.MoveFirst();
            while (!o.gProjects.IsEOF())
            {
                o.gProjects.GetObject(out tmpPrj);

                #region Imposta quantit� progetto
                for (int i = 0; i < collPrj.Count; i++)
                {
                    pq = (ProjectQty)collPrj[i];

                    if (pq.codeOriginalProject.Trim() == tmpPrj.gCodeOriginalProject.Trim())
                    {
                        pq.qty++;
                        collPrj[i] = pq;
                        findPrj = true;
                    }
                }

                if (!findPrj)
                {
                    pq.codeOriginalProject = tmpPrj.gCodeOriginalProject;
                    pq.qty = 1;
                    pq.detailsQty = new ArrayList();

                    #region Imposta quantit� dettagli
                    tmpPrj.gDetails.MoveFirst();
                    while (!tmpPrj.gDetails.IsEOF())
                    {
                        tmpPrj.gDetails.GetObject(out tmpDet);

                        for (int j = 0; j < pq.detailsQty.Count; j++)
                        {
                            dq = (DetailQty)pq.detailsQty[j];

                            if (dq.codeOriginalShape.Trim() == tmpDet.gCodeOriginalShape.Trim())
                            {
                                dq.qty++;
                                pq.detailsQty[j] = dq;
                                findDet = true;
                            }
                        }
                        if (!findDet)
                        {
                            dq.codeOriginalShape = tmpDet.gCodeOriginalShape;
                            dq.qty = 1;
                            pq.detailsQty.Add(dq);
                        }
                        findDet = false;

                        tmpPrj.gDetails.MoveNext();
                    }
                    #endregion

                    collPrj.Add(pq);    
                }
                findPrj = false;
                #endregion

                o.gProjects.MoveNext();
            }

            return true;
        }

        /// <summary>
        /// Restituisce la quantit� del progetto
        /// </summary>
        /// <param name="coll">collection in cui effettuare la ricerca</param>
        /// <param name="code">codice del progetto da ricercare</param>
        /// <returns></returns>
        private int GetProjectQty(ArrayList coll, string code)
        {
            for (int i = 0; i < coll.Count; i++)
            {
                ProjectQty pq = (ProjectQty)coll[i];

                if (pq.codeOriginalProject == code)
                    return pq.qty;
            }
            return 0;
        }

        /// <summary>
        /// Restituisce la quantit� del dettaglio
        /// </summary>
        /// <param name="coll">collection in cui effettuare la ricerca</param>
        /// <param name="code">codice del dettaglio da ricercare</param>
        /// <returns></returns>
        private int GetDetailQty(ArrayList coll, string code)
        {
            for (int i = 0; i < coll.Count; i++)
            {
                ProjectQty pq = (ProjectQty)coll[i];

                for (int j = 0; j < pq.detailsQty.Count; j++)
                {
                    DetailQty dq = (DetailQty)pq.detailsQty[j];

                    if (dq.codeOriginalShape == code)
                        return dq.qty;
                }
            }

            return 0;
        }

        /// <summary>
        /// Imposta le variabili da controllare
        /// </summary>
        private void SetKeyboardControl()
        {
            mImpost = new FieldCollection(imperial);

            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//DimZ
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//DimZ

            fGridThickness = mImpost.GetField(0);
            fGridFinalThickness = mImpost.GetField(1);
        }

        private void AutoSizeRow(C1.Win.C1TrueDBGrid.C1TrueDBGrid grid)
        {
            foreach (C1.Win.C1TrueDBGrid.BaseGrid.ViewRow r in grid.Splits[0].Rows)
            {
                r.AutoSize();
            }
        }

        /// ***********************************************************
        /// LoadTecnoPhases
        /// <summary>
        /// Carica le collection con gli ordini e le fasi di lavoro
        /// </summary>
        /// <param name="code">Codice del dettaglio</param>
        /// <returns></returns>
        /// ***********************************************************        
        private bool LoadTecnoPhases(string code)
        {
            int i = 0;
            string[] usObjs = new string[0];
            string[] wkOrds = new string[0];
            WorkPhase wk = new WorkPhase();
            mWorkPhases = new WorkPhaseCollection(mDbInterface);
            mWorkOrders = new WorkOrderCollection(mDbInterface);
            mUsedObjects = new UsedObjectCollection(mDbInterface);


            if (!mWorkPhases.GetDataFromDb(WorkPhase.Keys.F_SEQUENCE, null, code, null, null, null))
                return false;

            mWorkPhases.MoveFirst();
            while (!mWorkPhases.IsEOF())
            {
                mWorkPhases.GetObject(out wk);

                string[] tmp = new string[wkOrds.Length + 1];
                wkOrds.CopyTo(tmp, 0);
                wkOrds = tmp;
                wkOrds[i] = wk.gWorkOrderCode;

                string[] tmp2 = new string[usObjs.Length + 1];
                usObjs.CopyTo(tmp2, 0);
                usObjs = tmp2;
                usObjs[i] = wk.gUsedObjectCode;

                i++;
                mWorkPhases.MoveNext();
            }
            string[] tmp3 = new string[usObjs.Length + 1];
            usObjs.CopyTo(tmp3, 0);
            usObjs = tmp3;
            usObjs[i] = wk.gPhaseDUsedObject;

            if (!mWorkOrders.GetDataFromDb(WorkOrder.Keys.F_SEQUENCE, null, null, null, null, null, wkOrds))
                return false;

            if (!mUsedObjects.GetDataFromDb(usObjs))
                return false;

            return true;
        }

        ///**********************************************************
        /// Delete All
        /// <summary>
        /// Elimina il processo produttivo (fase tecno) per il pezzo
        /// </summary>
        /// <returns></returns>
        /// *********************************************************
        private bool DeleteAll()
        {
            try
            {
                // Cancellazione di tutti i record caricati
                mWorkPhases.MoveFirst();
                while (!mWorkPhases.IsEmpty())
                    mWorkPhases.DeleteObject();
                if (!mWorkPhases.UpdateDataToDb())
                    throw new ApplicationException("Error on work phases updated");

                mUsedObjects.MoveFirst();
                while (!mUsedObjects.IsEmpty())
                    mUsedObjects.DeleteObject();
                if (!mUsedObjects.UpdateDataToDb())
                    throw new ApplicationException("Error on used objects updated");

                mWorkOrders.MoveFirst();
                while (!mWorkOrders.IsEmpty())
                    mWorkOrders.DeleteObject();
                if (!mWorkOrders.UpdateDataToDb())
                    throw new ApplicationException("Error on work orders updated");

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Details.DeleteAll Error: " + ex.Message);
                return false;
            }
            finally
            {
                mWorkPhases = null;
                mUsedObjects = null;
                mWorkOrders = null;
            }
        }
        #endregion

        #region Gestione Tabelle
        /// <summary>
        /// Carica le tabelle 
        /// </summary>
        /// <param name="oldO"></param>
        private void LoadGrid()
        {
            // Carica la tabella dei progetti
            FillProjects();
            AutoSizeRow(dataGridProjects);

            CellValueProject();
        }

        /// <summary>
        /// Riempie la tabella dei progetti
        /// </summary>
        private void FillProjects()
        {
            Project prj;

            dataTableProjects.Clear();
            
            // Scorre tutti i progetti letti
            oldO.gProjects.MoveFirst();
            while (!oldO.gProjects.IsEOF())
            {
                // Legge il progetto attuale
                oldO.gProjects.GetObject(out prj);

                dataTableProjects.BeginInit();
                dataTableProjects.BeginLoadData();
                // Aggiunge una riga alla volta
                AddProjectRow(ref prj);

                dataTableProjects.EndInit();
                dataTableProjects.EndLoadData();
            }	
        }

        /// <summary>
        /// Aggiunge una riga alla tabella dei progetti
        /// </summary>
        /// <param name="prj">progetto da aggiungere</param>
        private void AddProjectRow(ref Project prj)
        {
            // Array con i dati della riga
            object[] myArray = new object[11];
            myArray[0] = dataTableProjects.Rows.Count;
            myArray[1] = prj.gId;
            myArray[2] = prj.gCode;
            myArray[3] = prj.gCustomerOrderCode;
            myArray[4] = StateEnumstrToStringPrj(prj.gStatePrj);
            myArray[5] = prj.gDate;
            if (prj.gDeliveryDate == DateTime.MaxValue)
                myArray[6] = null;
            else
                myArray[6] = prj.gDeliveryDate; 
            myArray[7] = prj.gDescription;
            myArray[8] = prj.gCodeOriginalProject;
            myArray[9] = prj.gImportOperation.ToString();
            SetProjectNote(ref myArray, prj);
            
            // Crea una nuova riga nella tabella e la riempie di dati
            DataRow r = dataTableProjects.NewRow();
            r.ItemArray = myArray;
            dataTableProjects.Rows.Add(r);

            // Assegna l'id della riga tabella al progetto
            prj.gTableId = int.Parse(r.ItemArray[0].ToString());

            // Passa all'elemento successivo
            oldO.gProjects.MoveNext();
        }

        /// <summary>
        /// Restituisce la descrizione dello stato
        /// </summary>
        /// <param name="st">stato</param>
        /// <returns></returns>
        private string StateEnumstrToStringPrj(string st)
        {
            if (st.Trim() == Project.State.PROJ_LOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 17);
            else if (st.Trim() == Project.State.PROJ_IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 18);
            else if (st.Trim() == Project.State.PROJ_COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 19);
            else if (st.Trim() == Project.State.PROJ_NDISP.ToString())
                return ProjResource.gResource.LoadFixString(this, 20);
            else if (st.Trim() == Project.State.PROJ_ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 21);
            else if (st.Trim() == Project.State.PROJ_RELOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 22);
            else
                return "";
        }

        /// <summary>
        /// Imposta la nota da scrivere per descrivere l'operazione che sar� eseguita e cosa fare in caso non potesse essere eseguita
        /// </summary>
        /// <param name="myArray"></param>
        /// <param name="prj"></param>
        private void SetProjectNote(ref object[] myArray, Project prj)
        {
            switch (prj.gImportOperation)
            {
                // Operazione da effettuare: Eliminazione
                case Project.ImportOpertation.DELETE:
                    switch (prj.gImportState)
                    {
                        // E' possibile eliminarlo direttamente
                        case Project.ImportState.ELIMINABLE:
                            myArray[10] = ProjResource.gResource.LoadFixString(this, 23);
                            break;

                        // Non � possibile eliminare direttamente il progetto
                        case Project.ImportState.NOT_ELIMINABLE_NO_PIECE:
                            myArray[10] = ProjResource.gResource.LoadFixString(this, 24);
                            break;

                        // Non � possibile eliminare direttamente il progetto in quanto esiste fisicamente
                        case Project.ImportState.NOT_ELIMINABLE_PIECE:
                            myArray[10] = ProjResource.gResource.LoadFixString(this, 25);
                            break;
                    }
                    break;

                // Operazione da effettuare: Inserimento
                case Project.ImportOpertation.INSERT:
                    myArray[10] = ProjResource.gResource.LoadFixString(this, 26);
                    break;

                // Operazione da effettuare: Modifica
                case Project.ImportOpertation.UPDATE:
                    myArray[10] = ProjResource.gResource.LoadFixString(this, 27);
                    break;

                // Nessuna operazione
                case Project.ImportOpertation.NONE:
                    myArray[10] = "";
                    break;
            }
        }

        /// <summary>
        /// Riempie la tabella dei dettagli
        /// </summary>
        private void FillDetails(ref Project prj)
        {
            Detail det;

            dataTableDetails.Clear();

            prj.gDetails.MoveFirst();
            while (!prj.gDetails.IsEOF())
            {
                prj.gDetails.GetObject(out det);

                dataTableDetails.BeginInit();
                dataTableDetails.BeginLoadData();
                // Aggiunge una riga alla volta
                AddDetailRow(ref det);

                dataTableDetails.EndInit();
                dataTableDetails.EndLoadData();

                prj.gDetails.MoveNext();
            }
        }

        /// <summary>
        /// Aggiunge una riga alla tabella dei dettagli
        /// </summary>
        /// <param name="det">dettaglio da aggiungere</param>
        private void AddDetailRow(ref Detail det)
        {
            // Array con i dati della riga
            object[] myArray = new object[14];
            myArray[0] = dataTableDetails.Rows.Count;
            myArray[1] = det.gId;
            myArray[2] = det.gCode;
            myArray[3] = det.gCustomerProjectCode;
            myArray[4] = det.gMaterialCode;
            myArray[5] = FindMaterialDescr(det.gMaterialCode);
            myArray[6] = StateEnumstrToStringDet(det.gImportState.ToString());
            fGridThickness.DoubleValue = double.Parse(det.gThickness.ToString());
            myArray[7] = double.Parse(fGridThickness.ToString());
            myArray[8] = det.gArticleCode;
            myArray[9] = det.gCodeOriginalShape;
            myArray[10] = det.gDescription;
            myArray[11] = det.gImportOperation.ToString();
            SetDetailNote(ref myArray, det);
            fGridFinalThickness.DoubleValue = double.Parse(det.gFinalThickness.ToString());
            myArray[13] = double.Parse(fGridFinalThickness.ToString());
            

            // Crea una nuova riga nella tabella e la riempie di dati
            DataRow r = dataTableDetails.NewRow();
            r.ItemArray = myArray;
            dataTableDetails.Rows.Add(r);

            // Assegna l'id della riga tabella al dettaglio
            det.gTableId = int.Parse(r.ItemArray[0].ToString());
        }

        /// <summary>
        /// Restituisce la descrizione dello stato
        /// </summary>
        /// <param name="st">stato</param>
        /// <returns></returns>
        private string StateEnumstrToStringDet(string st)
        {
            if (st.Trim() == Detail.ImportState.LOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 28);
            else if (st.Trim() == Detail.ImportState.ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 29);
            else if (st.Trim() == Detail.ImportState.OPTIMIZING.ToString())
                return ProjResource.gResource.LoadFixString(this, 30);
            else if (st.Trim() == Detail.ImportState.OPTIMIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 31);
            else if (st.Trim() == Detail.ImportState.USE_LIST.ToString())
                return ProjResource.gResource.LoadFixString(this, 32);
            else if (st.Trim() == Detail.ImportState.USE_LIST_PROCESSED.ToString())
                return ProjResource.gResource.LoadFixString(this, 33);
            else if (st.Trim() == Detail.ImportState.IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 34);
            else if (st.Trim() == Detail.ImportState.COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 35);
            else
                return "";
        }

        /// <summary>
        /// Restituisce la descrizione del materiale
        /// </summary>
        /// <param name="matCode">codice materiale</param>
        /// <returns></returns>
        private string FindMaterialDescr(string matCode)
        {
            Material mat;

            mMaterials.MoveFirst();
            while (!mMaterials.IsEOF())
            {
               
                mMaterials.GetObject(out mat);
                if (mat.gCode.Trim() == matCode.Trim())
                    return mat.gDescription;

                mMaterials.MoveNext();
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="myArray"></param>
        /// <param name="prj"></param>
        private void SetDetailNote(ref object[] myArray, Detail det)
        {
            switch (det.gImportOperation)
            {
                // Operazione da effettuare: Eliminazione
                case Detail.ImportOpertation.DELETE:
                    switch (det.gImportState)
                    {
                        // E' possibile eliminarlo direttamente
                        case Detail.ImportState.LOADED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 36);
                            break;

                        // E' possibile eliminarlo direttamente
                        case Detail.ImportState.ANALIZED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 37);
                            break;

                        // E' possibile eliminarlo direttamente
                        case Detail.ImportState.OPTIMIZING:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 38);
                            break;

                        // Non � possibile eliminare direttamente il dettaglio che � stato ottimizzato
                        case Detail.ImportState.OPTIMIZED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 39);
                            break;

                        // Non � possibile eliminare direttamente il dettaglio che si trova in lista di estrazione
                        case Detail.ImportState.USE_LIST:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 40);
                            break;

                        // Non � possibile eliminare direttamente il dettaglio
                        case Detail.ImportState.USE_LIST_PROCESSED:
                        case Detail.ImportState.IN_PROGRESS:
                        case Detail.ImportState.COMPLETED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 41);
                            break;
                    }
                    break;

                // Operazione da effettuare: Inserimento
                case Detail.ImportOpertation.INSERT:
                    myArray[12] = ProjResource.gResource.LoadFixString(this, 42);
                    break;

                // Operazione da effettuare: Modifica
                case Detail.ImportOpertation.UPDATE:
                    switch (det.gImportState)
                    {
                        // E' possibile modificare direttamente
                        case Detail.ImportState.LOADED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 43);
                            break;

                        // E' possibile modificare direttamente
                        case Detail.ImportState.ANALIZED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 44);
                            break;

                        // E' possibile modificare direttamente
                        case Detail.ImportState.OPTIMIZING:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 45);
                            break;

                        // Non � possibile modificare direttamente il dettaglio che � stato ottimizzato
                        case Detail.ImportState.OPTIMIZED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 46);
                            break;

                        // Non � possibile modificare direttamente il dettaglio che si trova in lista di estrazione
                        case Detail.ImportState.USE_LIST:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 47);
                            break;

                        // Non � possibile modificare direttamente il dettaglio
                        case Detail.ImportState.USE_LIST_PROCESSED:
                        case Detail.ImportState.IN_PROGRESS:
                        case Detail.ImportState.COMPLETED:
                            myArray[12] = ProjResource.gResource.LoadFixString(this, 48);
                            break;
                    }
                    break;

                // Nessuna operazione
                case Detail.ImportOpertation.NONE:
                    myArray[12] = "";
                    break;
            }
        }
        #endregion

        #region Control Events
        private void dataGridProjects_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
        {
            CellValueProject();
        }

        private void CellValueProject()
        {
            Project prj;
            int i;

            if (dataGridProjects.Bookmark >= 0)
                i = (int)dataGridProjects[dataGridProjects.Bookmark, 0];
            else
                return;

            if (oldO.gProjects.GetObjectTable(i, out prj))
            {
                FillDetails(ref prj);
                AutoSizeRow(dataGridDetails);
                CellValueDetail();
            }
        }

        private void dataGridDetails_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
        {
            CellValueDetail();
        }

        private void CellValueDetail()
        {
            Project prj;
            Detail det;
            int i;

            try
            {
                if (dataGridProjects.Bookmark >= 0)
                    i = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
                else
                    return;

                int tIDprj = (int)dataGridProjects[dataGridProjects.Bookmark, 0];
                oldO.gProjects.GetObjectTable(tIDprj, out prj);

                if (prj.gDetails.GetObjectTable(i, out det))
                {
                    // Carica una collection con l'elemento appena caricato
                    mFileXml = tmpXmlDir + "\\" + det.gCode.ToString().Trim() + ".xml";

                    if (det.gShapeInfo != null && det.gShapeInfo.Length > 0)
                        mFileXml = det.gShapeInfo;
                    else if (det.gCode != null && det.gCode.Length > 0 && !prj.gDetails.GetXmlParameterFromDb(det, mFileXml))
                        throw new ApplicationException("Error getting xml");

					//Shape mShape = new Shape();

					//if (!XmlAnalizer.ReadShapeXml(mFileXml, ref mShape, true))	// Lettura del file xml
					//    throw new ApplicationException("Error analyze xml");

					mDraw.DrawXml(mFileXml, true, false, false, true, true);
                    mDraw.ZoomAll();
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ReloadOrder.CellValueDetail ", ex);
            }
        }

        private void dataGridProjects_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Project prj;

            int i = (int)dataGridProjects[e.Row, "T_ID"];
            oldO.gProjects.GetObjectTable(i, out prj);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (prj.gImportOperation == Project.ImportOpertation.NONE)
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (prj.gImportOperation == Project.ImportOpertation.DELETE)
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.BackColor2 = Color.Coral;
            }
            else if (prj.gImportOperation == Project.ImportOpertation.INSERT)
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (prj.gImportOperation == Project.ImportOpertation.UPDATE)
            {
                e.CellStyle.BackColor = Color.Yellow;
                e.CellStyle.BackColor2 = Color.LightYellow;
            }
        }

        private void dataGridDetails_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Project prj;
            Detail det;

            int tIDprj = (int)dataGridProjects[dataGridProjects.Bookmark, 0];
            oldO.gProjects.GetObjectTable(tIDprj, out prj);

            int i = (int)dataGridDetails[e.Row, "T_ID"];
            prj.gDetails.GetObjectTable(i, out det);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (det.gImportOperation == Detail.ImportOpertation.NONE)
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (det.gImportOperation == Detail.ImportOpertation.DELETE)
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.BackColor2 = Color.Coral;
            }
            else if (det.gImportOperation == Detail.ImportOpertation.INSERT)
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (det.gImportOperation == Detail.ImportOpertation.UPDATE)
            {
                e.CellStyle.BackColor = Color.Yellow;
                e.CellStyle.BackColor2 = Color.LightYellow;
            }
        }

        private void VDPreview_Resize(object sender, EventArgs e)
        {
            if (mDraw != null)
                mDraw.ZoomAll();
        }

        private void VDPreview_MouseWheelEvent(object sender, AxVDProLib5._DVdrawEvents_MouseWheelEvent e)
        {
            e.cancel = 1;
        }

        #endregion
    }
     
}