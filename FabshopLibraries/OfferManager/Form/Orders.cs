using System;
using System.Data;
using System.Xml;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.Customers_Suppliers;
using Breton.TraceLoggers;
using Breton.ImportOffer;
using Breton.Articles;
using Breton.Slabs;
using Breton.SlabManager.Form;
using Breton.TechnoMaster;
using Breton.Phases;
using Breton.DesignUtil;

namespace Breton.OfferManager.Form
{
	public class Orders : System.Windows.Forms.Form
	{
		#region Variables
		private System.Windows.Forms.ToolBar toolBar;
		private System.Windows.Forms.ToolBarButton btnNuovo;
		private System.Windows.Forms.ToolBarButton btnImporta;
		private System.Windows.Forms.ToolBarButton btnModifica;
		private System.Windows.Forms.ToolBarButton btnSalva;
		private System.Windows.Forms.ToolBarButton btnElimina;
		private System.Windows.Forms.ToolBarButton btnReset;
        private System.Windows.Forms.ToolBarButton btnAnnulla;
		private System.Windows.Forms.ImageList imlToolIcon;
		private System.ComponentModel.IContainer components;
		private System.Data.DataSet dataSetOrders;
		private System.Data.DataTable dataTableOrders;
		private System.Data.DataColumn orderT_ID;
		private System.Data.DataColumn orderF_ID;
		private System.Data.DataColumn orderF_CODE;
		private System.Data.DataColumn orderF_DATE;
		private System.Data.DataColumn orderF_DELIVERY_DATE;
		private System.Data.DataColumn orderF_DESCRIPTION;
		private System.Data.DataColumn orderF_CUSTOMER_CODE;
		private System.Data.DataColumn orderF_OFFER_CODE;
		private System.Data.DataColumn orderF_STATE;
		private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridOrders;
		private System.Windows.Forms.Panel panelEdit;
		private System.Windows.Forms.Label lblCustomerName;
		private System.Windows.Forms.Label lblCustomerCode;
		private System.Windows.Forms.ComboBox cmbxCustomerCode;
		private System.Windows.Forms.Label lblOperation;
		private System.Windows.Forms.Label lblDeliveryDate;
		private System.Windows.Forms.DateTimePicker deliveryDate;
		private System.Windows.Forms.Label lblState;
		private System.Windows.Forms.ComboBox cmbxCustomerName;
		private System.Windows.Forms.ComboBox cmbxState;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.TextBox txtDescription;
		private System.Windows.Forms.Button btnNascondi;
		private System.Windows.Forms.Button btnConferma;
		private System.Data.DataColumn orderF_CUSTOMER_NAME;
		private System.Windows.Forms.TextBox txtDeliveryDate;
		private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.ToolBarButton btnProgetti;	
        private ToolBarButton btnListaEstraz;
        private ToolBarButton btnFilter;
        private ToolBarButton toolBarButton2;
        private DataSet DataSetProdProcess;
        private DataTable dataTableWorking;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn6;
        private DataColumn dataColumn15;
        private DataTable dataTableWorkGroup;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn13;
        private DataColumn dataColumn17;
        private DataTable dataTablePhase;
        private DataColumn dataColumn9;
        private DataColumn dataColumn10;
        private DataColumn dataColumn16;
        private DataTable dataTableMachine;
        private DataColumn dataColumn11;
        private DataColumn dataColumn12;
        private DataColumn dataColumn14;
        private DataColumn dataColumn18;
        private ToolBarButton btnTecno;
        private Panel panelProdProcess;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1WorkCenterGrid;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1WorkingGrid;
        private Button btnNascondiSimpleTecno;
        private Button btnOkSimpleTecno;
        private Label lblProdProc;
        private C1.Data.C1DataSet c1DataSetProdProcess;
        private C1.Win.C1List.C1Combo c1ProdProcess;
        private ToolBarButton btnRefresh;  
        private Panel panelFilter;
        private Button btnAdvanced;
        private Button btnChiudi;
        private DateTimePicker deliveryDateTo;
        private DateTimePicker insertDateTo;
        private TextBox txtCode;
        private Label lblInsertDate;
        private DateTimePicker insertDateFrom;
        private Label lblCode;
        private Button btnResetFilter;
        private Button btnFiltra;
        private TextBox txtFilterDescr;
        private Label lblFilterDescr;
        private ComboBox cmbxFilterState;
        private Label lblFilterState;
        private ComboBox cmbxFilterCustName;
        private Label lblFilterDeliv;
        private Label lblFilterCustName;
        private Label lblFilterCustCode;
        private ComboBox cmbxFilterCustCode;
        private DateTimePicker deliveryDateFrom;
        private Panel panelFilterAdv;
        private RadioButton rdbtnAllExtractionList;
        private RadioButton rdbtnTempExtractionList;
        private RadioButton rdbtnNoExtractionList;
		private RadioButton rdbtOptimized;
        private RadioButton rdbtnOptimizing;
        private Splitter splitter;
        private Panel panelData;
        private Panel panelOpti;
        private Panel panelUseList;
        private Panel panelQuantity;
        private Label lblQuantity;
        private Label lblQty;
        private ToolTip toolTip;
        private ToolBarButton btnEliminaOrdine;
        private Button btnCloseAdvFilter;
        private ProgressBar progressBar;
        private Timer timerTecno;


        private static bool formCreate, formDiscardCreate;
		private bool isModified = false;
		private bool isProjectsMod = false;
		private bool isDateChanged = false;
        private bool mDiscard = false;
        private OrderCollection mOrders;
        private ProjectCollection mProjectsDel;
		private ProjectCollection mProjects;
        private DetailCollection mDetails;
		private CustomerCollection mCustomers;
        private WorkOrderCollection mWorkOrders;
        private WorkPhaseCollection mWorkPhases;
        private UsedObjectCollection mUsedObjects;
		private DbInterface mDbInterface;
		private User mUser;
		private string mLanguage;		
		private string Edit;
		private ParameterControlPositions mFormPosition;
		private OrderOperation Operat;
        private ParameterCollection mParamApp = null;
        private bool isDateInsert = false;
        private bool isDateDeliv = false;
        private DirectoryInfo dirXml;
        private TecnoOperation mTecnoOp;
        private Shape mShapeTecno;
        private string tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
        private bool mAdvancedFilter;
        private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;		
        private ThreadTecno mTecno;
		private bool mSchedule;
		private ParameterCollection mParam;

        private EventHandler prjHandler, detHandler;
		private Panel panelSchedule;
		private RadioButton rdbtnAllSchedule;
		private RadioButton rdbtnNoSchedule;
		private RadioButton rdbtnTempSchedule;
		private RadioButton rdbtnNoOpti;
		private Label labelSeparator;
		private RadioButton rdbtnTempOpti;
		private DataColumn dataColumnF_REVISION;
	
		public delegate void SelectOrderEventHandler(object sender, string e);		// Gestore dell'evento
		public static event SelectOrderEventHandler SelectOrderEvent;				// Evento SelectOrderEvent
        public static event EventHandler UpdateOrders;

		public enum OrderOperation
		{
			None,
			New,
			Edit,
			Save,
			Delete
		}

		#endregion

        public Orders(DbInterface db, User user, string language)
            : this(db, user, language, false)
        {
        }

		public Orders(DbInterface db, User user, string language, bool discard)
		{
			InitializeComponent();

			if (db == null)
			{
				mDbInterface = new DbInterface();
				mDbInterface.Open("", "DbConnection.udl");
			}
			else
				mDbInterface = db;

			// Carica la posizione del form
			mFormPosition = new ParameterControlPositions(this.Name);
			mFormPosition.LoadFormPosition(this);

			mUser = user;

			mLanguage = language;
			ProjResource.gResource.ChangeLanguage(mLanguage);

            mDiscard = discard;
            
            // evento aggiornamento progetti
            prjHandler = new EventHandler(UpdateProjects);
            Breton.OfferManager.Form.Projects.UpdateProject += prjHandler;

            // evento aggiornamento dettagli
            detHandler = new EventHandler(UpdateDetails);
            Breton.OfferManager.Form.Details.UpdateDetail += detHandler;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Orders));
			this.toolBar = new System.Windows.Forms.ToolBar();
			this.btnNuovo = new System.Windows.Forms.ToolBarButton();
			this.btnImporta = new System.Windows.Forms.ToolBarButton();
			this.btnModifica = new System.Windows.Forms.ToolBarButton();
			this.btnSalva = new System.Windows.Forms.ToolBarButton();
			this.btnEliminaOrdine = new System.Windows.Forms.ToolBarButton();
			this.btnElimina = new System.Windows.Forms.ToolBarButton();
			this.btnReset = new System.Windows.Forms.ToolBarButton();
			this.btnAnnulla = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.btnProgetti = new System.Windows.Forms.ToolBarButton();
			this.btnListaEstraz = new System.Windows.Forms.ToolBarButton();
			this.btnFilter = new System.Windows.Forms.ToolBarButton();
			this.btnTecno = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
			this.btnRefresh = new System.Windows.Forms.ToolBarButton();
			this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
			this.dataTableOrders = new System.Data.DataTable();
			this.orderT_ID = new System.Data.DataColumn();
			this.orderF_ID = new System.Data.DataColumn();
			this.orderF_CODE = new System.Data.DataColumn();
			this.orderF_DATE = new System.Data.DataColumn();
			this.orderF_DELIVERY_DATE = new System.Data.DataColumn();
			this.orderF_DESCRIPTION = new System.Data.DataColumn();
			this.orderF_CUSTOMER_CODE = new System.Data.DataColumn();
			this.orderF_OFFER_CODE = new System.Data.DataColumn();
			this.orderF_STATE = new System.Data.DataColumn();
			this.orderF_CUSTOMER_NAME = new System.Data.DataColumn();
			this.dataColumnF_REVISION = new System.Data.DataColumn();
			this.dataSetOrders = new System.Data.DataSet();
			this.panelEdit = new System.Windows.Forms.Panel();
			this.btnNascondi = new System.Windows.Forms.Button();
			this.btnConferma = new System.Windows.Forms.Button();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.cmbxState = new System.Windows.Forms.ComboBox();
			this.lblState = new System.Windows.Forms.Label();
			this.cmbxCustomerName = new System.Windows.Forms.ComboBox();
			this.lblDeliveryDate = new System.Windows.Forms.Label();
			this.lblCustomerName = new System.Windows.Forms.Label();
			this.lblCustomerCode = new System.Windows.Forms.Label();
			this.cmbxCustomerCode = new System.Windows.Forms.ComboBox();
			this.lblOperation = new System.Windows.Forms.Label();
			this.txtDeliveryDate = new System.Windows.Forms.TextBox();
			this.deliveryDate = new System.Windows.Forms.DateTimePicker();
			this.DataSetProdProcess = new System.Data.DataSet();
			this.dataTableWorking = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn15 = new System.Data.DataColumn();
			this.dataTableWorkGroup = new System.Data.DataTable();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.dataColumn13 = new System.Data.DataColumn();
			this.dataColumn17 = new System.Data.DataColumn();
			this.dataTablePhase = new System.Data.DataTable();
			this.dataColumn9 = new System.Data.DataColumn();
			this.dataColumn10 = new System.Data.DataColumn();
			this.dataColumn16 = new System.Data.DataColumn();
			this.dataTableMachine = new System.Data.DataTable();
			this.dataColumn11 = new System.Data.DataColumn();
			this.dataColumn12 = new System.Data.DataColumn();
			this.dataColumn14 = new System.Data.DataColumn();
			this.dataColumn18 = new System.Data.DataColumn();
			this.panelProdProcess = new System.Windows.Forms.Panel();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.c1ProdProcess = new C1.Win.C1List.C1Combo();
			this.c1DataSetProdProcess = new C1.Data.C1DataSet();
			this.c1WorkCenterGrid = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.c1WorkingGrid = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.btnNascondiSimpleTecno = new System.Windows.Forms.Button();
			this.btnOkSimpleTecno = new System.Windows.Forms.Button();
			this.lblProdProc = new System.Windows.Forms.Label();
			this.panelFilter = new System.Windows.Forms.Panel();
			this.btnAdvanced = new System.Windows.Forms.Button();
			this.btnChiudi = new System.Windows.Forms.Button();
			this.deliveryDateTo = new System.Windows.Forms.DateTimePicker();
			this.insertDateTo = new System.Windows.Forms.DateTimePicker();
			this.txtCode = new System.Windows.Forms.TextBox();
			this.lblInsertDate = new System.Windows.Forms.Label();
			this.insertDateFrom = new System.Windows.Forms.DateTimePicker();
			this.lblCode = new System.Windows.Forms.Label();
			this.btnResetFilter = new System.Windows.Forms.Button();
			this.btnFiltra = new System.Windows.Forms.Button();
			this.txtFilterDescr = new System.Windows.Forms.TextBox();
			this.lblFilterDescr = new System.Windows.Forms.Label();
			this.cmbxFilterState = new System.Windows.Forms.ComboBox();
			this.lblFilterState = new System.Windows.Forms.Label();
			this.cmbxFilterCustName = new System.Windows.Forms.ComboBox();
			this.lblFilterDeliv = new System.Windows.Forms.Label();
			this.lblFilterCustName = new System.Windows.Forms.Label();
			this.lblFilterCustCode = new System.Windows.Forms.Label();
			this.cmbxFilterCustCode = new System.Windows.Forms.ComboBox();
			this.deliveryDateFrom = new System.Windows.Forms.DateTimePicker();
			this.panelFilterAdv = new System.Windows.Forms.Panel();
			this.panelOpti = new System.Windows.Forms.Panel();
			this.labelSeparator = new System.Windows.Forms.Label();
			this.rdbtnNoOpti = new System.Windows.Forms.RadioButton();
			this.rdbtnTempOpti = new System.Windows.Forms.RadioButton();
			this.rdbtnOptimizing = new System.Windows.Forms.RadioButton();
			this.rdbtOptimized = new System.Windows.Forms.RadioButton();
			this.btnCloseAdvFilter = new System.Windows.Forms.Button();
			this.panelSchedule = new System.Windows.Forms.Panel();
			this.rdbtnAllSchedule = new System.Windows.Forms.RadioButton();
			this.rdbtnNoSchedule = new System.Windows.Forms.RadioButton();
			this.rdbtnTempSchedule = new System.Windows.Forms.RadioButton();
			this.panelUseList = new System.Windows.Forms.Panel();
			this.rdbtnAllExtractionList = new System.Windows.Forms.RadioButton();
			this.rdbtnNoExtractionList = new System.Windows.Forms.RadioButton();
			this.rdbtnTempExtractionList = new System.Windows.Forms.RadioButton();
			this.splitter = new System.Windows.Forms.Splitter();
			this.panelData = new System.Windows.Forms.Panel();
			this.dataGridOrders = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.panelQuantity = new System.Windows.Forms.Panel();
			this.lblQty = new System.Windows.Forms.Label();
			this.lblQuantity = new System.Windows.Forms.Label();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.timerTecno = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.dataTableOrders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetOrders)).BeginInit();
			this.panelEdit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.DataSetProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorking)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorkGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTablePhase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableMachine)).BeginInit();
			this.panelProdProcess.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1ProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1DataSetProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkCenterGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkingGrid)).BeginInit();
			this.panelFilter.SuspendLayout();
			this.panelFilterAdv.SuspendLayout();
			this.panelOpti.SuspendLayout();
			this.panelSchedule.SuspendLayout();
			this.panelUseList.SuspendLayout();
			this.panelData.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridOrders)).BeginInit();
			this.panelQuantity.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolBar
			// 
			this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnNuovo,
            this.btnImporta,
            this.btnModifica,
            this.btnSalva,
            this.btnEliminaOrdine,
            this.btnElimina,
            this.btnReset,
            this.btnAnnulla,
            this.toolBarButton1,
            this.btnProgetti,
            this.btnListaEstraz,
            this.btnFilter,
            this.btnTecno,
            this.toolBarButton2,
            this.btnRefresh});
			this.toolBar.DropDownArrows = true;
			this.toolBar.ImageList = this.imlToolIcon;
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.ShowToolTips = true;
			this.toolBar.Size = new System.Drawing.Size(768, 44);
			this.toolBar.TabIndex = 2;
			this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
			// 
			// btnNuovo
			// 
			this.btnNuovo.ImageIndex = 0;
			this.btnNuovo.Name = "btnNuovo";
			this.btnNuovo.ToolTipText = "Nuova";
			this.btnNuovo.Visible = false;
			// 
			// btnImporta
			// 
			this.btnImporta.ImageIndex = 1;
			this.btnImporta.Name = "btnImporta";
			this.btnImporta.ToolTipText = "Importa XML";
			this.btnImporta.Visible = false;
			// 
			// btnModifica
			// 
			this.btnModifica.ImageIndex = 2;
			this.btnModifica.Name = "btnModifica";
			this.btnModifica.ToolTipText = "Modifica";
			// 
			// btnSalva
			// 
			this.btnSalva.ImageIndex = 3;
			this.btnSalva.Name = "btnSalva";
			this.btnSalva.ToolTipText = "Salva";
			// 
			// btnEliminaOrdine
			// 
			this.btnEliminaOrdine.ImageIndex = 12;
			this.btnEliminaOrdine.Name = "btnEliminaOrdine";
			// 
			// btnElimina
			// 
			this.btnElimina.ImageIndex = 4;
			this.btnElimina.Name = "btnElimina";
			this.btnElimina.ToolTipText = "Elimina Tecno";
			// 
			// btnReset
			// 
			this.btnReset.ImageIndex = 5;
			this.btnReset.Name = "btnReset";
			this.btnReset.ToolTipText = "Annulla operazioni effettuate";
			// 
			// btnAnnulla
			// 
			this.btnAnnulla.ImageIndex = 6;
			this.btnAnnulla.Name = "btnAnnulla";
			this.btnAnnulla.ToolTipText = "Esci";
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.Name = "toolBarButton1";
			this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnProgetti
			// 
			this.btnProgetti.ImageIndex = 7;
			this.btnProgetti.Name = "btnProgetti";
			// 
			// btnListaEstraz
			// 
			this.btnListaEstraz.ImageIndex = 8;
			this.btnListaEstraz.Name = "btnListaEstraz";
			this.btnListaEstraz.ToolTipText = "Lista di estrazione";
			// 
			// btnFilter
			// 
			this.btnFilter.ImageIndex = 9;
			this.btnFilter.Name = "btnFilter";
			// 
			// btnTecno
			// 
			this.btnTecno.ImageIndex = 11;
			this.btnTecno.Name = "btnTecno";
			// 
			// toolBarButton2
			// 
			this.toolBarButton2.Name = "toolBarButton2";
			this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnRefresh
			// 
			this.btnRefresh.ImageIndex = 10;
			this.btnRefresh.Name = "btnRefresh";
			// 
			// imlToolIcon
			// 
			this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
			this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon.Images.SetKeyName(0, "");
			this.imlToolIcon.Images.SetKeyName(1, "");
			this.imlToolIcon.Images.SetKeyName(2, "");
			this.imlToolIcon.Images.SetKeyName(3, "");
			this.imlToolIcon.Images.SetKeyName(4, "");
			this.imlToolIcon.Images.SetKeyName(5, "");
			this.imlToolIcon.Images.SetKeyName(6, "");
			this.imlToolIcon.Images.SetKeyName(7, "");
			this.imlToolIcon.Images.SetKeyName(8, "");
			this.imlToolIcon.Images.SetKeyName(9, "");
			this.imlToolIcon.Images.SetKeyName(10, "");
			this.imlToolIcon.Images.SetKeyName(11, "");
			this.imlToolIcon.Images.SetKeyName(12, "Delete.ico");
			// 
			// dataTableOrders
			// 
			this.dataTableOrders.Columns.AddRange(new System.Data.DataColumn[] {
            this.orderT_ID,
            this.orderF_ID,
            this.orderF_CODE,
            this.orderF_DATE,
            this.orderF_DELIVERY_DATE,
            this.orderF_DESCRIPTION,
            this.orderF_CUSTOMER_CODE,
            this.orderF_OFFER_CODE,
            this.orderF_STATE,
            this.orderF_CUSTOMER_NAME,
            this.dataColumnF_REVISION});
			this.dataTableOrders.TableName = "tableOrders";
			// 
			// orderT_ID
			// 
			this.orderT_ID.ColumnName = "T_ID";
			this.orderT_ID.DataType = typeof(int);
			// 
			// orderF_ID
			// 
			this.orderF_ID.Caption = "Id";
			this.orderF_ID.ColumnName = "F_ID";
			this.orderF_ID.DataType = typeof(int);
			// 
			// orderF_CODE
			// 
			this.orderF_CODE.Caption = "Codice";
			this.orderF_CODE.ColumnName = "F_CODE";
			// 
			// orderF_DATE
			// 
			this.orderF_DATE.Caption = "Data";
			this.orderF_DATE.ColumnName = "F_DATE";
			this.orderF_DATE.DataType = typeof(System.DateTime);
			// 
			// orderF_DELIVERY_DATE
			// 
			this.orderF_DELIVERY_DATE.Caption = "Data consegna";
			this.orderF_DELIVERY_DATE.ColumnName = "F_DELIVERY_DATE";
			this.orderF_DELIVERY_DATE.DataType = typeof(System.DateTime);
			// 
			// orderF_DESCRIPTION
			// 
			this.orderF_DESCRIPTION.Caption = "Descrizione";
			this.orderF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
			// 
			// orderF_CUSTOMER_CODE
			// 
			this.orderF_CUSTOMER_CODE.Caption = "Codice Cliente";
			this.orderF_CUSTOMER_CODE.ColumnName = "F_CUSTOMER_CODE";
			// 
			// orderF_OFFER_CODE
			// 
			this.orderF_OFFER_CODE.Caption = "Codice offerta";
			this.orderF_OFFER_CODE.ColumnName = "F_OFFER_CODE";
			// 
			// orderF_STATE
			// 
			this.orderF_STATE.Caption = "Stato";
			this.orderF_STATE.ColumnName = "F_STATE";
			// 
			// orderF_CUSTOMER_NAME
			// 
			this.orderF_CUSTOMER_NAME.Caption = "Nome cliente";
			this.orderF_CUSTOMER_NAME.ColumnName = "F_CUSTOMER_NAME";
			// 
			// dataColumnF_REVISION
			// 
			this.dataColumnF_REVISION.ColumnName = "F_REVISION";
			this.dataColumnF_REVISION.DataType = typeof(int);
			// 
			// dataSetOrders
			// 
			this.dataSetOrders.DataSetName = "NewDataSet";
			this.dataSetOrders.Locale = new System.Globalization.CultureInfo("en-US");
			this.dataSetOrders.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableOrders});
			// 
			// panelEdit
			// 
			this.panelEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelEdit.Controls.Add(this.btnNascondi);
			this.panelEdit.Controls.Add(this.btnConferma);
			this.panelEdit.Controls.Add(this.txtDescription);
			this.panelEdit.Controls.Add(this.lblDescription);
			this.panelEdit.Controls.Add(this.cmbxState);
			this.panelEdit.Controls.Add(this.lblState);
			this.panelEdit.Controls.Add(this.cmbxCustomerName);
			this.panelEdit.Controls.Add(this.lblDeliveryDate);
			this.panelEdit.Controls.Add(this.lblCustomerName);
			this.panelEdit.Controls.Add(this.lblCustomerCode);
			this.panelEdit.Controls.Add(this.cmbxCustomerCode);
			this.panelEdit.Controls.Add(this.lblOperation);
			this.panelEdit.Controls.Add(this.txtDeliveryDate);
			this.panelEdit.Controls.Add(this.deliveryDate);
			this.panelEdit.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelEdit.Location = new System.Drawing.Point(0, 44);
			this.panelEdit.Name = "panelEdit";
			this.panelEdit.Size = new System.Drawing.Size(768, 118);
			this.panelEdit.TabIndex = 4;
			this.panelEdit.Visible = false;
			// 
			// btnNascondi
			// 
			this.btnNascondi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnNascondi.Image = ((System.Drawing.Image)(resources.GetObject("btnNascondi.Image")));
			this.btnNascondi.Location = new System.Drawing.Point(595, 71);
			this.btnNascondi.Name = "btnNascondi";
			this.btnNascondi.Size = new System.Drawing.Size(40, 40);
			this.btnNascondi.TabIndex = 109;
			this.btnNascondi.Click += new System.EventHandler(this.btnNascondi_Click);
			// 
			// btnConferma
			// 
			this.btnConferma.Image = ((System.Drawing.Image)(resources.GetObject("btnConferma.Image")));
			this.btnConferma.Location = new System.Drawing.Point(638, 71);
			this.btnConferma.Name = "btnConferma";
			this.btnConferma.Size = new System.Drawing.Size(40, 40);
			this.btnConferma.TabIndex = 108;
			this.btnConferma.Click += new System.EventHandler(this.btnConferma_Click);
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(478, 49);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(200, 20);
			this.txtDescription.TabIndex = 107;
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDescription.Location = new System.Drawing.Point(331, 49);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 15);
			this.lblDescription.TabIndex = 106;
			this.lblDescription.Text = "Descrizione";
			// 
			// cmbxState
			// 
			this.cmbxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbxState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxState.Location = new System.Drawing.Point(128, 72);
			this.cmbxState.Name = "cmbxState";
			this.cmbxState.Size = new System.Drawing.Size(192, 21);
			this.cmbxState.TabIndex = 105;
			this.cmbxState.Visible = false;
			// 
			// lblState
			// 
			this.lblState.AutoSize = true;
			this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblState.Location = new System.Drawing.Point(8, 73);
			this.lblState.Name = "lblState";
			this.lblState.Size = new System.Drawing.Size(35, 15);
			this.lblState.TabIndex = 104;
			this.lblState.Text = "Stato";
			this.lblState.Visible = false;
			// 
			// cmbxCustomerName
			// 
			this.cmbxCustomerName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbxCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxCustomerName.Location = new System.Drawing.Point(128, 50);
			this.cmbxCustomerName.Name = "cmbxCustomerName";
			this.cmbxCustomerName.Size = new System.Drawing.Size(192, 21);
			this.cmbxCustomerName.TabIndex = 102;
			this.cmbxCustomerName.SelectedIndexChanged += new System.EventHandler(this.cmbxCustomerName_SelectedIndexChanged);
			// 
			// lblDeliveryDate
			// 
			this.lblDeliveryDate.AutoSize = true;
			this.lblDeliveryDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDeliveryDate.Location = new System.Drawing.Point(331, 28);
			this.lblDeliveryDate.Name = "lblDeliveryDate";
			this.lblDeliveryDate.Size = new System.Drawing.Size(103, 15);
			this.lblDeliveryDate.TabIndex = 101;
			this.lblDeliveryDate.Text = "Data di consegna";
			// 
			// lblCustomerName
			// 
			this.lblCustomerName.AutoSize = true;
			this.lblCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCustomerName.Location = new System.Drawing.Point(8, 50);
			this.lblCustomerName.Name = "lblCustomerName";
			this.lblCustomerName.Size = new System.Drawing.Size(80, 15);
			this.lblCustomerName.TabIndex = 100;
			this.lblCustomerName.Text = "Nome cliente";
			// 
			// lblCustomerCode
			// 
			this.lblCustomerCode.AutoSize = true;
			this.lblCustomerCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCustomerCode.Location = new System.Drawing.Point(8, 28);
			this.lblCustomerCode.Name = "lblCustomerCode";
			this.lblCustomerCode.Size = new System.Drawing.Size(84, 15);
			this.lblCustomerCode.TabIndex = 99;
			this.lblCustomerCode.Text = "Codice cliente";
			// 
			// cmbxCustomerCode
			// 
			this.cmbxCustomerCode.DisplayMember = "F_CODE";
			this.cmbxCustomerCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbxCustomerCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxCustomerCode.Location = new System.Drawing.Point(128, 28);
			this.cmbxCustomerCode.Name = "cmbxCustomerCode";
			this.cmbxCustomerCode.Size = new System.Drawing.Size(192, 21);
			this.cmbxCustomerCode.TabIndex = 96;
			this.cmbxCustomerCode.SelectedIndexChanged += new System.EventHandler(this.cmbxCustomerCode_SelectedIndexChanged);
			// 
			// lblOperation
			// 
			this.lblOperation.AutoSize = true;
			this.lblOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOperation.Location = new System.Drawing.Point(8, 8);
			this.lblOperation.Name = "lblOperation";
			this.lblOperation.Size = new System.Drawing.Size(35, 16);
			this.lblOperation.TabIndex = 98;
			this.lblOperation.Text = "Edit";
			// 
			// txtDeliveryDate
			// 
			this.txtDeliveryDate.Location = new System.Drawing.Point(478, 28);
			this.txtDeliveryDate.Name = "txtDeliveryDate";
			this.txtDeliveryDate.ReadOnly = true;
			this.txtDeliveryDate.Size = new System.Drawing.Size(200, 20);
			this.txtDeliveryDate.TabIndex = 110;
			// 
			// deliveryDate
			// 
			this.deliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.deliveryDate.Location = new System.Drawing.Point(478, 28);
			this.deliveryDate.Name = "deliveryDate";
			this.deliveryDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.deliveryDate.Size = new System.Drawing.Size(200, 20);
			this.deliveryDate.TabIndex = 103;
			this.deliveryDate.ValueChanged += new System.EventHandler(this.deliveryDate_ValueChanged);
			// 
			// DataSetProdProcess
			// 
			this.DataSetProdProcess.DataSetName = "NewDataSet";
			this.DataSetProdProcess.Locale = new System.Globalization.CultureInfo("en-US");
			this.DataSetProdProcess.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableWorking,
            this.dataTableWorkGroup,
            this.dataTablePhase,
            this.dataTableMachine});
			// 
			// dataTableWorking
			// 
			this.dataTableWorking.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn6,
            this.dataColumn15});
			this.dataTableWorking.TableName = "TableWorking";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "T_ID";
			this.dataColumn1.DataType = typeof(int);
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "F_TECNO_WORKING";
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "F_WORKING";
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "F_ID_T_LS_PHASE_WORKING";
			this.dataColumn6.DataType = typeof(int);
			// 
			// dataColumn15
			// 
			this.dataColumn15.ColumnName = "F_LAYER_CODE";
			// 
			// dataTableWorkGroup
			// 
			this.dataTableWorkGroup.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn13,
            this.dataColumn17});
			this.dataTableWorkGroup.TableName = "TableWorkGroup";
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "T_ID";
			this.dataColumn4.DataType = typeof(int);
			// 
			// dataColumn5
			// 
			this.dataColumn5.ColumnName = "F_TECNO_WORKING";
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "F_ID_T_AN_WORK_CENTER_GROUP";
			this.dataColumn7.DataType = typeof(int);
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "F_WORK_CENTER_GROUP";
			// 
			// dataColumn13
			// 
			this.dataColumn13.ColumnName = "F_TECNO_WORK_GROUP";
			// 
			// dataColumn17
			// 
			this.dataColumn17.ColumnName = "F_WORK_CENTER_GROUP_CODE";
			// 
			// dataTablePhase
			// 
			this.dataTablePhase.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn16});
			this.dataTablePhase.TableName = "TablePhase";
			// 
			// dataColumn9
			// 
			this.dataColumn9.ColumnName = "F_PHASE_ID";
			this.dataColumn9.DataType = typeof(int);
			// 
			// dataColumn10
			// 
			this.dataColumn10.ColumnName = "F_PHASE_DESCRIPTION";
			// 
			// dataColumn16
			// 
			this.dataColumn16.ColumnName = "F_LAYER_CODE";
			// 
			// dataTableMachine
			// 
			this.dataTableMachine.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn14,
            this.dataColumn18});
			this.dataTableMachine.TableName = "TableMachine";
			// 
			// dataColumn11
			// 
			this.dataColumn11.ColumnName = "F_WORK_CENTER_GROUP_ID";
			this.dataColumn11.DataType = typeof(int);
			// 
			// dataColumn12
			// 
			this.dataColumn12.ColumnName = "F_WORK_CENTER_GROUP_DESCRIPTION";
			// 
			// dataColumn14
			// 
			this.dataColumn14.ColumnName = "F_TECNO_WORK_CENTER_GROUP";
			// 
			// dataColumn18
			// 
			this.dataColumn18.ColumnName = "F_WORK_CENTER_GROUP_CODE";
			// 
			// panelProdProcess
			// 
			this.panelProdProcess.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelProdProcess.Controls.Add(this.progressBar);
			this.panelProdProcess.Controls.Add(this.c1ProdProcess);
			this.panelProdProcess.Controls.Add(this.c1WorkCenterGrid);
			this.panelProdProcess.Controls.Add(this.c1WorkingGrid);
			this.panelProdProcess.Controls.Add(this.btnNascondiSimpleTecno);
			this.panelProdProcess.Controls.Add(this.btnOkSimpleTecno);
			this.panelProdProcess.Controls.Add(this.lblProdProc);
			this.panelProdProcess.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelProdProcess.Location = new System.Drawing.Point(0, 374);
			this.panelProdProcess.Name = "panelProdProcess";
			this.panelProdProcess.Size = new System.Drawing.Size(768, 59);
			this.panelProdProcess.TabIndex = 13;
			this.panelProdProcess.Visible = false;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(496, 11);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(210, 22);
			this.progressBar.TabIndex = 125;
			this.progressBar.Visible = false;
			// 
			// c1ProdProcess
			// 
			this.c1ProdProcess.AddItemSeparator = ';';
			this.c1ProdProcess.Caption = "";
			this.c1ProdProcess.CaptionHeight = 17;
			this.c1ProdProcess.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.c1ProdProcess.ColumnCaptionHeight = 17;
			this.c1ProdProcess.ColumnFooterHeight = 17;
			this.c1ProdProcess.ColumnHeaders = false;
			this.c1ProdProcess.ColumnWidth = 100;
			this.c1ProdProcess.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList;
			this.c1ProdProcess.ContentHeight = 16;
			this.c1ProdProcess.DataMember = "T_LS_PRODUCTION_PROCESSES";
			this.c1ProdProcess.DataSource = this.c1DataSetProdProcess;
			this.c1ProdProcess.DeadAreaBackColor = System.Drawing.Color.Empty;
			this.c1ProdProcess.DisplayMember = "T_LS_PRODUCTION_PROCESSES.F_DESCRIPTION";
			this.c1ProdProcess.EditorBackColor = System.Drawing.SystemColors.Window;
			this.c1ProdProcess.EditorFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1ProdProcess.EditorForeColor = System.Drawing.SystemColors.WindowText;
			this.c1ProdProcess.EditorHeight = 16;
			this.c1ProdProcess.ExtendRightColumn = true;
			this.c1ProdProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1ProdProcess.Images.Add(((System.Drawing.Image)(resources.GetObject("c1ProdProcess.Images"))));
			this.c1ProdProcess.ItemHeight = 15;
			this.c1ProdProcess.Location = new System.Drawing.Point(142, 11);
			this.c1ProdProcess.MatchEntryTimeout = ((long)(2000));
			this.c1ProdProcess.MaxDropDownItems = ((short)(5));
			this.c1ProdProcess.MaxLength = 32767;
			this.c1ProdProcess.MouseCursor = System.Windows.Forms.Cursors.Default;
			this.c1ProdProcess.Name = "c1ProdProcess";
			this.c1ProdProcess.RowDivider.Color = System.Drawing.Color.DarkGray;
			this.c1ProdProcess.RowDivider.Style = C1.Win.C1List.LineStyleEnum.None;
			this.c1ProdProcess.RowSubDividerColor = System.Drawing.Color.DarkGray;
			this.c1ProdProcess.Size = new System.Drawing.Size(248, 22);
			this.c1ProdProcess.TabIndex = 123;
			this.c1ProdProcess.ValueMember = "T_LS_PRODUCTION_PROCESSES.F_ID";
			this.c1ProdProcess.SelectedValueChanged += new System.EventHandler(this.c1ProdProcess_SelectedValueChanged);
			this.c1ProdProcess.PropBag = resources.GetString("c1ProdProcess.PropBag");
			// 
			// c1DataSetProdProcess
			// 
			this.c1DataSetProdProcess.DataLibrary = "ShopMasterDataLibrary";
			this.c1DataSetProdProcess.DataLibraryUrl = "";
			this.c1DataSetProdProcess.DataSetDef = "DataSet_Production_Process";
			this.c1DataSetProdProcess.FillOnRequest = false;
			this.c1DataSetProdProcess.Locale = new System.Globalization.CultureInfo("en-US");
			this.c1DataSetProdProcess.Name = "c1DataSetProdProcess";
			this.c1DataSetProdProcess.SchemaClassName = "Breton.ShopMasterDataLibrary.DataClass";
			this.c1DataSetProdProcess.SchemaDef = null;
			// 
			// c1WorkCenterGrid
			// 
			this.c1WorkCenterGrid.AllowColMove = false;
			this.c1WorkCenterGrid.AllowSort = false;
			this.c1WorkCenterGrid.AllowUpdate = false;
			this.c1WorkCenterGrid.Caption = "Gruppi di lavoro";
			this.c1WorkCenterGrid.CaptionHeight = 17;
			this.c1WorkCenterGrid.ColumnHeaders = false;
			this.c1WorkCenterGrid.DataSource = this.dataTableWorkGroup;
			this.c1WorkCenterGrid.ExtendRightColumn = true;
			this.c1WorkCenterGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1WorkCenterGrid.GroupByCaption = "Drag a column header here to group by that column";
			this.c1WorkCenterGrid.Images.Add(((System.Drawing.Image)(resources.GetObject("c1WorkCenterGrid.Images"))));
			this.c1WorkCenterGrid.Location = new System.Drawing.Point(293, 57);
			this.c1WorkCenterGrid.Name = "c1WorkCenterGrid";
			this.c1WorkCenterGrid.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1WorkCenterGrid.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1WorkCenterGrid.PreviewInfo.ZoomFactor = 75D;
			this.c1WorkCenterGrid.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1WorkCenterGrid.PrintInfo.PageSettings")));
			this.c1WorkCenterGrid.RowHeight = 15;
			this.c1WorkCenterGrid.ScrollTrack = false;
			this.c1WorkCenterGrid.Size = new System.Drawing.Size(233, 84);
			this.c1WorkCenterGrid.TabIndex = 122;
			this.c1WorkCenterGrid.PropBag = resources.GetString("c1WorkCenterGrid.PropBag");
			// 
			// c1WorkingGrid
			// 
			this.c1WorkingGrid.AllowColMove = false;
			this.c1WorkingGrid.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None;
			this.c1WorkingGrid.AllowSort = false;
			this.c1WorkingGrid.AllowUpdate = false;
			this.c1WorkingGrid.Caption = "Fasi lavorazione";
			this.c1WorkingGrid.CaptionHeight = 17;
			this.c1WorkingGrid.ColumnHeaders = false;
			this.c1WorkingGrid.DataSource = this.dataTableWorking;
			this.c1WorkingGrid.ExtendRightColumn = true;
			this.c1WorkingGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1WorkingGrid.GroupByCaption = "Drag a column header here to group by that column";
			this.c1WorkingGrid.Images.Add(((System.Drawing.Image)(resources.GetObject("c1WorkingGrid.Images"))));
			this.c1WorkingGrid.Location = new System.Drawing.Point(35, 57);
			this.c1WorkingGrid.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.None;
			this.c1WorkingGrid.Name = "c1WorkingGrid";
			this.c1WorkingGrid.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1WorkingGrid.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1WorkingGrid.PreviewInfo.ZoomFactor = 75D;
			this.c1WorkingGrid.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1WorkingGrid.PrintInfo.PageSettings")));
			this.c1WorkingGrid.RowHeight = 15;
			this.c1WorkingGrid.ScrollTrack = false;
			this.c1WorkingGrid.Size = new System.Drawing.Size(233, 84);
			this.c1WorkingGrid.TabIndex = 121;
			this.c1WorkingGrid.PropBag = resources.GetString("c1WorkingGrid.PropBag");
			// 
			// btnNascondiSimpleTecno
			// 
			this.btnNascondiSimpleTecno.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnNascondiSimpleTecno.Image = ((System.Drawing.Image)(resources.GetObject("btnNascondiSimpleTecno.Image")));
			this.btnNascondiSimpleTecno.Location = new System.Drawing.Point(443, 5);
			this.btnNascondiSimpleTecno.Name = "btnNascondiSimpleTecno";
			this.btnNascondiSimpleTecno.Size = new System.Drawing.Size(36, 36);
			this.btnNascondiSimpleTecno.TabIndex = 119;
			this.btnNascondiSimpleTecno.Click += new System.EventHandler(this.btnNascondiSimpleTecno_Click);
			// 
			// btnOkSimpleTecno
			// 
			this.btnOkSimpleTecno.Enabled = false;
			this.btnOkSimpleTecno.Image = ((System.Drawing.Image)(resources.GetObject("btnOkSimpleTecno.Image")));
			this.btnOkSimpleTecno.Location = new System.Drawing.Point(407, 5);
			this.btnOkSimpleTecno.Name = "btnOkSimpleTecno";
			this.btnOkSimpleTecno.Size = new System.Drawing.Size(36, 36);
			this.btnOkSimpleTecno.TabIndex = 118;
			this.btnOkSimpleTecno.Click += new System.EventHandler(this.btnOkSimpleTecno_Click);
			// 
			// lblProdProc
			// 
			this.lblProdProc.AutoSize = true;
			this.lblProdProc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblProdProc.Location = new System.Drawing.Point(11, 15);
			this.lblProdProc.Name = "lblProdProc";
			this.lblProdProc.Size = new System.Drawing.Size(114, 15);
			this.lblProdProc.TabIndex = 39;
			this.lblProdProc.Text = "Processo produttivo";
			// 
			// panelFilter
			// 
			this.panelFilter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelFilter.Controls.Add(this.btnAdvanced);
			this.panelFilter.Controls.Add(this.btnChiudi);
			this.panelFilter.Controls.Add(this.deliveryDateTo);
			this.panelFilter.Controls.Add(this.insertDateTo);
			this.panelFilter.Controls.Add(this.txtCode);
			this.panelFilter.Controls.Add(this.lblInsertDate);
			this.panelFilter.Controls.Add(this.insertDateFrom);
			this.panelFilter.Controls.Add(this.lblCode);
			this.panelFilter.Controls.Add(this.btnResetFilter);
			this.panelFilter.Controls.Add(this.btnFiltra);
			this.panelFilter.Controls.Add(this.txtFilterDescr);
			this.panelFilter.Controls.Add(this.lblFilterDescr);
			this.panelFilter.Controls.Add(this.cmbxFilterState);
			this.panelFilter.Controls.Add(this.lblFilterState);
			this.panelFilter.Controls.Add(this.cmbxFilterCustName);
			this.panelFilter.Controls.Add(this.lblFilterDeliv);
			this.panelFilter.Controls.Add(this.lblFilterCustName);
			this.panelFilter.Controls.Add(this.lblFilterCustCode);
			this.panelFilter.Controls.Add(this.cmbxFilterCustCode);
			this.panelFilter.Controls.Add(this.deliveryDateFrom);
			this.panelFilter.Controls.Add(this.panelFilterAdv);
			this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelFilter.Location = new System.Drawing.Point(0, 162);
			this.panelFilter.Name = "panelFilter";
			this.panelFilter.Size = new System.Drawing.Size(768, 212);
			this.panelFilter.TabIndex = 124;
			this.panelFilter.Visible = false;
			// 
			// btnAdvanced
			// 
			this.btnAdvanced.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAdvanced.Location = new System.Drawing.Point(333, 73);
			this.btnAdvanced.Name = "btnAdvanced";
			this.btnAdvanced.Size = new System.Drawing.Size(103, 40);
			this.btnAdvanced.TabIndex = 121;
			this.btnAdvanced.Text = "Avanzato...";
			this.btnAdvanced.UseVisualStyleBackColor = true;
			this.btnAdvanced.Click += new System.EventHandler(this.btnAdvanced_Click);
			// 
			// btnChiudi
			// 
			this.btnChiudi.Image = ((System.Drawing.Image)(resources.GetObject("btnChiudi.Image")));
			this.btnChiudi.Location = new System.Drawing.Point(638, 73);
			this.btnChiudi.Name = "btnChiudi";
			this.btnChiudi.Size = new System.Drawing.Size(40, 40);
			this.btnChiudi.TabIndex = 120;
			this.btnChiudi.Click += new System.EventHandler(this.btnChiudi_Click);
			// 
			// deliveryDateTo
			// 
			this.deliveryDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.deliveryDateTo.Location = new System.Drawing.Point(578, 27);
			this.deliveryDateTo.Name = "deliveryDateTo";
			this.deliveryDateTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.deliveryDateTo.Size = new System.Drawing.Size(100, 20);
			this.deliveryDateTo.TabIndex = 119;
			this.deliveryDateTo.ValueChanged += new System.EventHandler(this.deliveryDateTo_ValueChanged);
			// 
			// insertDateTo
			// 
			this.insertDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.insertDateTo.Location = new System.Drawing.Point(578, 3);
			this.insertDateTo.Name = "insertDateTo";
			this.insertDateTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.insertDateTo.Size = new System.Drawing.Size(100, 20);
			this.insertDateTo.TabIndex = 118;
			this.insertDateTo.ValueChanged += new System.EventHandler(this.insertDateTo_ValueChanged);
			// 
			// txtCode
			// 
			this.txtCode.Location = new System.Drawing.Point(128, 3);
			this.txtCode.Name = "txtCode";
			this.txtCode.Size = new System.Drawing.Size(192, 20);
			this.txtCode.TabIndex = 117;
			// 
			// lblInsertDate
			// 
			this.lblInsertDate.AutoSize = true;
			this.lblInsertDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblInsertDate.Location = new System.Drawing.Point(330, 3);
			this.lblInsertDate.Name = "lblInsertDate";
			this.lblInsertDate.Size = new System.Drawing.Size(114, 15);
			this.lblInsertDate.TabIndex = 115;
			this.lblInsertDate.Text = "Data di inserimento";
			// 
			// insertDateFrom
			// 
			this.insertDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.insertDateFrom.Location = new System.Drawing.Point(471, 3);
			this.insertDateFrom.Name = "insertDateFrom";
			this.insertDateFrom.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.insertDateFrom.Size = new System.Drawing.Size(100, 20);
			this.insertDateFrom.TabIndex = 116;
			this.insertDateFrom.ValueChanged += new System.EventHandler(this.insertDateFrom_ValueChanged);
			// 
			// lblCode
			// 
			this.lblCode.AutoSize = true;
			this.lblCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCode.Location = new System.Drawing.Point(8, 3);
			this.lblCode.Name = "lblCode";
			this.lblCode.Size = new System.Drawing.Size(45, 15);
			this.lblCode.TabIndex = 114;
			this.lblCode.Text = "Codice";
			// 
			// btnResetFilter
			// 
			this.btnResetFilter.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnResetFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnResetFilter.Image")));
			this.btnResetFilter.Location = new System.Drawing.Point(550, 73);
			this.btnResetFilter.Name = "btnResetFilter";
			this.btnResetFilter.Size = new System.Drawing.Size(40, 40);
			this.btnResetFilter.TabIndex = 111;
			this.btnResetFilter.Click += new System.EventHandler(this.btnResetFilter_Click);
			// 
			// btnFiltra
			// 
			this.btnFiltra.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltra.Image")));
			this.btnFiltra.Location = new System.Drawing.Point(593, 73);
			this.btnFiltra.Name = "btnFiltra";
			this.btnFiltra.Size = new System.Drawing.Size(40, 40);
			this.btnFiltra.TabIndex = 112;
			this.btnFiltra.Click += new System.EventHandler(this.btnFiltra_Click);
			// 
			// txtFilterDescr
			// 
			this.txtFilterDescr.Location = new System.Drawing.Point(471, 51);
			this.txtFilterDescr.Name = "txtFilterDescr";
			this.txtFilterDescr.Size = new System.Drawing.Size(207, 20);
			this.txtFilterDescr.TabIndex = 107;
			// 
			// lblFilterDescr
			// 
			this.lblFilterDescr.AutoSize = true;
			this.lblFilterDescr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFilterDescr.Location = new System.Drawing.Point(330, 51);
			this.lblFilterDescr.Name = "lblFilterDescr";
			this.lblFilterDescr.Size = new System.Drawing.Size(72, 15);
			this.lblFilterDescr.TabIndex = 106;
			this.lblFilterDescr.Text = "Descrizione";
			// 
			// cmbxFilterState
			// 
			this.cmbxFilterState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbxFilterState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxFilterState.Location = new System.Drawing.Point(128, 83);
			this.cmbxFilterState.Name = "cmbxFilterState";
			this.cmbxFilterState.Size = new System.Drawing.Size(192, 21);
			this.cmbxFilterState.TabIndex = 105;
			// 
			// lblFilterState
			// 
			this.lblFilterState.AutoSize = true;
			this.lblFilterState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFilterState.Location = new System.Drawing.Point(8, 83);
			this.lblFilterState.Name = "lblFilterState";
			this.lblFilterState.Size = new System.Drawing.Size(35, 15);
			this.lblFilterState.TabIndex = 104;
			this.lblFilterState.Text = "Stato";
			// 
			// cmbxFilterCustName
			// 
			this.cmbxFilterCustName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxFilterCustName.Location = new System.Drawing.Point(128, 56);
			this.cmbxFilterCustName.Name = "cmbxFilterCustName";
			this.cmbxFilterCustName.Size = new System.Drawing.Size(192, 21);
			this.cmbxFilterCustName.TabIndex = 102;
			this.cmbxFilterCustName.SelectedIndexChanged += new System.EventHandler(this.cmbxFilterCustName_SelectedIndexChanged);
			// 
			// lblFilterDeliv
			// 
			this.lblFilterDeliv.AutoSize = true;
			this.lblFilterDeliv.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFilterDeliv.Location = new System.Drawing.Point(330, 28);
			this.lblFilterDeliv.Name = "lblFilterDeliv";
			this.lblFilterDeliv.Size = new System.Drawing.Size(103, 15);
			this.lblFilterDeliv.TabIndex = 101;
			this.lblFilterDeliv.Text = "Data di consegna";
			// 
			// lblFilterCustName
			// 
			this.lblFilterCustName.AutoSize = true;
			this.lblFilterCustName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFilterCustName.Location = new System.Drawing.Point(8, 56);
			this.lblFilterCustName.Name = "lblFilterCustName";
			this.lblFilterCustName.Size = new System.Drawing.Size(80, 15);
			this.lblFilterCustName.TabIndex = 100;
			this.lblFilterCustName.Text = "Nome cliente";
			// 
			// lblFilterCustCode
			// 
			this.lblFilterCustCode.AutoSize = true;
			this.lblFilterCustCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFilterCustCode.Location = new System.Drawing.Point(8, 29);
			this.lblFilterCustCode.Name = "lblFilterCustCode";
			this.lblFilterCustCode.Size = new System.Drawing.Size(84, 15);
			this.lblFilterCustCode.TabIndex = 99;
			this.lblFilterCustCode.Text = "Codice cliente";
			// 
			// cmbxFilterCustCode
			// 
			this.cmbxFilterCustCode.DisplayMember = "F_CODE";
			this.cmbxFilterCustCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxFilterCustCode.Location = new System.Drawing.Point(128, 29);
			this.cmbxFilterCustCode.Name = "cmbxFilterCustCode";
			this.cmbxFilterCustCode.Size = new System.Drawing.Size(192, 21);
			this.cmbxFilterCustCode.TabIndex = 96;
			this.cmbxFilterCustCode.SelectedIndexChanged += new System.EventHandler(this.cmbxFilterCustCode_SelectedIndexChanged);
			// 
			// deliveryDateFrom
			// 
			this.deliveryDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.deliveryDateFrom.Location = new System.Drawing.Point(471, 27);
			this.deliveryDateFrom.Name = "deliveryDateFrom";
			this.deliveryDateFrom.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.deliveryDateFrom.Size = new System.Drawing.Size(100, 20);
			this.deliveryDateFrom.TabIndex = 103;
			this.deliveryDateFrom.ValueChanged += new System.EventHandler(this.deliveryDateFrom_ValueChanged);
			// 
			// panelFilterAdv
			// 
			this.panelFilterAdv.Controls.Add(this.panelOpti);
			this.panelFilterAdv.Controls.Add(this.btnCloseAdvFilter);
			this.panelFilterAdv.Controls.Add(this.panelSchedule);
			this.panelFilterAdv.Controls.Add(this.panelUseList);
			this.panelFilterAdv.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelFilterAdv.Location = new System.Drawing.Point(0, 115);
			this.panelFilterAdv.Name = "panelFilterAdv";
			this.panelFilterAdv.Size = new System.Drawing.Size(764, 93);
			this.panelFilterAdv.TabIndex = 124;
			this.panelFilterAdv.Visible = false;
			// 
			// panelOpti
			// 
			this.panelOpti.Controls.Add(this.labelSeparator);
			this.panelOpti.Controls.Add(this.rdbtnNoOpti);
			this.panelOpti.Controls.Add(this.rdbtnTempOpti);
			this.panelOpti.Controls.Add(this.rdbtnOptimizing);
			this.panelOpti.Controls.Add(this.rdbtOptimized);
			this.panelOpti.Location = new System.Drawing.Point(0, 1);
			this.panelOpti.Name = "panelOpti";
			this.panelOpti.Size = new System.Drawing.Size(266, 89);
			this.panelOpti.TabIndex = 123;
			// 
			// labelSeparator
			// 
			this.labelSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.labelSeparator.Location = new System.Drawing.Point(3, 65);
			this.labelSeparator.Name = "labelSeparator";
			this.labelSeparator.Size = new System.Drawing.Size(260, 1);
			this.labelSeparator.TabIndex = 4;
			// 
			// rdbtnNoOpti
			// 
			this.rdbtnNoOpti.AutoSize = true;
			this.rdbtnNoOpti.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnNoOpti.Location = new System.Drawing.Point(15, 68);
			this.rdbtnNoOpti.Name = "rdbtnNoOpti";
			this.rdbtnNoOpti.Size = new System.Drawing.Size(143, 19);
			this.rdbtnNoOpti.TabIndex = 3;
			this.rdbtnNoOpti.TabStop = true;
			this.rdbtnNoOpti.Text = "Non in ottimizzazione";
			this.rdbtnNoOpti.UseVisualStyleBackColor = true;
			// 
			// rdbtnTempOpti
			// 
			this.rdbtnTempOpti.AutoSize = true;
			this.rdbtnTempOpti.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnTempOpti.Location = new System.Drawing.Point(15, 23);
			this.rdbtnTempOpti.Name = "rdbtnTempOpti";
			this.rdbtnTempOpti.Size = new System.Drawing.Size(160, 19);
			this.rdbtnTempOpti.TabIndex = 1;
			this.rdbtnTempOpti.TabStop = true;
			this.rdbtnTempOpti.Text = "Parzialmente ottimizzato";
			this.rdbtnTempOpti.UseVisualStyleBackColor = true;
			this.rdbtnTempOpti.CheckedChanged += new System.EventHandler(this.FirstPart_CheckedChanged);
			// 
			// rdbtnOptimizing
			// 
			this.rdbtnOptimizing.AutoSize = true;
			this.rdbtnOptimizing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnOptimizing.Location = new System.Drawing.Point(15, 3);
			this.rdbtnOptimizing.Name = "rdbtnOptimizing";
			this.rdbtnOptimizing.Size = new System.Drawing.Size(117, 19);
			this.rdbtnOptimizing.TabIndex = 0;
			this.rdbtnOptimizing.Text = "In ottimizzazione";
			this.rdbtnOptimizing.UseVisualStyleBackColor = true;
			this.rdbtnOptimizing.CheckedChanged += new System.EventHandler(this.FirstPart_CheckedChanged);
			// 
			// rdbtOptimized
			// 
			this.rdbtOptimized.AutoSize = true;
			this.rdbtOptimized.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtOptimized.Location = new System.Drawing.Point(15, 43);
			this.rdbtOptimized.Name = "rdbtOptimized";
			this.rdbtOptimized.Size = new System.Drawing.Size(86, 19);
			this.rdbtOptimized.TabIndex = 2;
			this.rdbtOptimized.TabStop = true;
			this.rdbtOptimized.Text = "Ottimizzato";
			this.rdbtOptimized.UseVisualStyleBackColor = true;
			this.rdbtOptimized.CheckedChanged += new System.EventHandler(this.FirstPart_CheckedChanged);
			// 
			// btnCloseAdvFilter
			// 
			this.btnCloseAdvFilter.Image = ((System.Drawing.Image)(resources.GetObject("btnCloseAdvFilter.Image")));
			this.btnCloseAdvFilter.Location = new System.Drawing.Point(638, 3);
			this.btnCloseAdvFilter.Name = "btnCloseAdvFilter";
			this.btnCloseAdvFilter.Size = new System.Drawing.Size(40, 40);
			this.btnCloseAdvFilter.TabIndex = 121;
			this.btnCloseAdvFilter.Click += new System.EventHandler(this.btnCloseAdvFilter_Click);
			// 
			// panelSchedule
			// 
			this.panelSchedule.Controls.Add(this.rdbtnAllSchedule);
			this.panelSchedule.Controls.Add(this.rdbtnNoSchedule);
			this.panelSchedule.Controls.Add(this.rdbtnTempSchedule);
			this.panelSchedule.Location = new System.Drawing.Point(274, 1);
			this.panelSchedule.Name = "panelSchedule";
			this.panelSchedule.Size = new System.Drawing.Size(299, 67);
			this.panelSchedule.TabIndex = 123;
			// 
			// rdbtnAllSchedule
			// 
			this.rdbtnAllSchedule.AutoSize = true;
			this.rdbtnAllSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnAllSchedule.Location = new System.Drawing.Point(12, 43);
			this.rdbtnAllSchedule.Name = "rdbtnAllSchedule";
			this.rdbtnAllSchedule.Size = new System.Drawing.Size(115, 19);
			this.rdbtnAllSchedule.TabIndex = 5;
			this.rdbtnAllSchedule.TabStop = true;
			this.rdbtnAllSchedule.Text = "Tutto schedulato";
			this.rdbtnAllSchedule.UseVisualStyleBackColor = true;
			this.rdbtnAllSchedule.CheckedChanged += new System.EventHandler(this.SchedulePart_CheckedChanged);
			// 
			// rdbtnNoSchedule
			// 
			this.rdbtnNoSchedule.AutoSize = true;
			this.rdbtnNoSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnNoSchedule.Location = new System.Drawing.Point(12, 3);
			this.rdbtnNoSchedule.Name = "rdbtnNoSchedule";
			this.rdbtnNoSchedule.Size = new System.Drawing.Size(111, 19);
			this.rdbtnNoSchedule.TabIndex = 3;
			this.rdbtnNoSchedule.Text = "Non schedulato";
			this.rdbtnNoSchedule.UseVisualStyleBackColor = true;
			this.rdbtnNoSchedule.CheckedChanged += new System.EventHandler(this.SchedulePart_CheckedChanged);
			// 
			// rdbtnTempSchedule
			// 
			this.rdbtnTempSchedule.AutoSize = true;
			this.rdbtnTempSchedule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnTempSchedule.Location = new System.Drawing.Point(12, 23);
			this.rdbtnTempSchedule.Name = "rdbtnTempSchedule";
			this.rdbtnTempSchedule.Size = new System.Drawing.Size(161, 19);
			this.rdbtnTempSchedule.TabIndex = 4;
			this.rdbtnTempSchedule.TabStop = true;
			this.rdbtnTempSchedule.Text = "Parzialmente schedulato";
			this.rdbtnTempSchedule.UseVisualStyleBackColor = true;
			this.rdbtnTempSchedule.CheckedChanged += new System.EventHandler(this.SchedulePart_CheckedChanged);
			// 
			// panelUseList
			// 
			this.panelUseList.Controls.Add(this.rdbtnAllExtractionList);
			this.panelUseList.Controls.Add(this.rdbtnNoExtractionList);
			this.panelUseList.Controls.Add(this.rdbtnTempExtractionList);
			this.panelUseList.Location = new System.Drawing.Point(274, 1);
			this.panelUseList.Name = "panelUseList";
			this.panelUseList.Size = new System.Drawing.Size(299, 67);
			this.panelUseList.TabIndex = 122;
			// 
			// rdbtnAllExtractionList
			// 
			this.rdbtnAllExtractionList.AutoSize = true;
			this.rdbtnAllExtractionList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnAllExtractionList.Location = new System.Drawing.Point(12, 43);
			this.rdbtnAllExtractionList.Name = "rdbtnAllExtractionList";
			this.rdbtnAllExtractionList.Size = new System.Drawing.Size(163, 19);
			this.rdbtnAllExtractionList.TabIndex = 5;
			this.rdbtnAllExtractionList.TabStop = true;
			this.rdbtnAllExtractionList.Text = "Tutto in lista di estrazione";
			this.rdbtnAllExtractionList.UseVisualStyleBackColor = true;
			// 
			// rdbtnNoExtractionList
			// 
			this.rdbtnNoExtractionList.AutoSize = true;
			this.rdbtnNoExtractionList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnNoExtractionList.Location = new System.Drawing.Point(12, 3);
			this.rdbtnNoExtractionList.Name = "rdbtnNoExtractionList";
			this.rdbtnNoExtractionList.Size = new System.Drawing.Size(159, 19);
			this.rdbtnNoExtractionList.TabIndex = 3;
			this.rdbtnNoExtractionList.Text = "Non in lista di estrazione";
			this.rdbtnNoExtractionList.UseVisualStyleBackColor = true;
			// 
			// rdbtnTempExtractionList
			// 
			this.rdbtnTempExtractionList.AutoSize = true;
			this.rdbtnTempExtractionList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rdbtnTempExtractionList.Location = new System.Drawing.Point(12, 23);
			this.rdbtnTempExtractionList.Name = "rdbtnTempExtractionList";
			this.rdbtnTempExtractionList.Size = new System.Drawing.Size(209, 19);
			this.rdbtnTempExtractionList.TabIndex = 4;
			this.rdbtnTempExtractionList.TabStop = true;
			this.rdbtnTempExtractionList.Text = "Parzialmente in lista di estrazione";
			this.rdbtnTempExtractionList.UseVisualStyleBackColor = true;
			// 
			// splitter
			// 
			this.splitter.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter.Location = new System.Drawing.Point(0, 433);
			this.splitter.Name = "splitter";
			this.splitter.Size = new System.Drawing.Size(768, 4);
			this.splitter.TabIndex = 125;
			this.splitter.TabStop = false;
			// 
			// panelData
			// 
			this.panelData.Controls.Add(this.dataGridOrders);
			this.panelData.Controls.Add(this.panelQuantity);
			this.panelData.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelData.Location = new System.Drawing.Point(0, 437);
			this.panelData.Name = "panelData";
			this.panelData.Size = new System.Drawing.Size(768, 127);
			this.panelData.TabIndex = 126;
			// 
			// dataGridOrders
			// 
			this.dataGridOrders.AllowFilter = false;
			this.dataGridOrders.AllowSort = false;
			this.dataGridOrders.AllowUpdate = false;
			this.dataGridOrders.AllowVerticalSplit = true;
			this.dataGridOrders.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dataGridOrders.CaptionHeight = 17;
			this.dataGridOrders.Cursor = System.Windows.Forms.Cursors.Default;
			this.dataGridOrders.DataSource = this.dataTableOrders;
			this.dataGridOrders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridOrders.ExtendRightColumn = true;
			this.dataGridOrders.FetchRowStyles = true;
			this.dataGridOrders.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridOrders.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridOrders.Images"))));
			this.dataGridOrders.Location = new System.Drawing.Point(0, 26);
			this.dataGridOrders.Name = "dataGridOrders";
			this.dataGridOrders.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridOrders.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridOrders.PreviewInfo.ZoomFactor = 75D;
			this.dataGridOrders.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridOrders.PrintInfo.PageSettings")));
			this.dataGridOrders.RowHeight = 15;
			this.dataGridOrders.Size = new System.Drawing.Size(768, 101);
			this.dataGridOrders.TabIndex = 3;
			this.dataGridOrders.TabStop = false;
			this.dataGridOrders.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.dataGridOrders_RowColChange);
			this.dataGridOrders.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridOrders_FetchRowStyle);
			this.dataGridOrders.PropBag = resources.GetString("dataGridOrders.PropBag");
			// 
			// panelQuantity
			// 
			this.panelQuantity.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelQuantity.Controls.Add(this.lblQty);
			this.panelQuantity.Controls.Add(this.lblQuantity);
			this.panelQuantity.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelQuantity.Location = new System.Drawing.Point(0, 0);
			this.panelQuantity.Name = "panelQuantity";
			this.panelQuantity.Size = new System.Drawing.Size(768, 26);
			this.panelQuantity.TabIndex = 4;
			// 
			// lblQty
			// 
			this.lblQty.AutoSize = true;
			this.lblQty.BackColor = System.Drawing.Color.White;
			this.lblQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblQty.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblQty.Location = new System.Drawing.Point(139, 3);
			this.lblQty.Name = "lblQty";
			this.lblQty.Size = new System.Drawing.Size(2, 17);
			this.lblQty.TabIndex = 42;
			// 
			// lblQuantity
			// 
			this.lblQuantity.AutoSize = true;
			this.lblQuantity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblQuantity.Location = new System.Drawing.Point(14, 3);
			this.lblQuantity.Name = "lblQuantity";
			this.lblQuantity.Size = new System.Drawing.Size(108, 15);
			this.lblQuantity.TabIndex = 40;
			this.lblQuantity.Text = "Ordini visualizzati: ";
			// 
			// toolTip
			// 
			this.toolTip.AutoPopDelay = 7000;
			this.toolTip.InitialDelay = 500;
			this.toolTip.IsBalloon = true;
			this.toolTip.ReshowDelay = 100;
			this.toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.toolTip.ToolTipTitle = "Info";
			// 
			// timerTecno
			// 
			this.timerTecno.Tick += new System.EventHandler(this.timerTecno_Tick);
			// 
			// Orders
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(768, 564);
			this.Controls.Add(this.panelData);
			this.Controls.Add(this.splitter);
			this.Controls.Add(this.panelProdProcess);
			this.Controls.Add(this.panelFilter);
			this.Controls.Add(this.panelEdit);
			this.Controls.Add(this.toolBar);
			this.Name = "Orders";
			this.Text = " ";
			this.Activated += new System.EventHandler(this.Orders_Activated);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Orders_Closing);
			this.Closed += new System.EventHandler(this.Orders_Closed);
			this.Load += new System.EventHandler(this.Orders_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataTableOrders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetOrders)).EndInit();
			this.panelEdit.ResumeLayout(false);
			this.panelEdit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.DataSetProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorking)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorkGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTablePhase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableMachine)).EndInit();
			this.panelProdProcess.ResumeLayout(false);
			this.panelProdProcess.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1ProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1DataSetProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkCenterGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkingGrid)).EndInit();
			this.panelFilter.ResumeLayout(false);
			this.panelFilter.PerformLayout();
			this.panelFilterAdv.ResumeLayout(false);
			this.panelOpti.ResumeLayout(false);
			this.panelOpti.PerformLayout();
			this.panelSchedule.ResumeLayout(false);
			this.panelSchedule.PerformLayout();
			this.panelUseList.ResumeLayout(false);
			this.panelUseList.PerformLayout();
			this.panelData.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridOrders)).EndInit();
			this.panelQuantity.ResumeLayout(false);
			this.panelQuantity.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		#region Properties

		public static bool gCreate
		{
			set	{ formCreate = value; }
			get { return formCreate; }
		}

        public static bool gDiscardCreate
        {
            set { formDiscardCreate = value; }
            get { return formDiscardCreate; }
        }

		#endregion

		#region Open Form & Load Parameters
		public new void ShowDialog()
		{
			if (LoadParameters())
				base.ShowDialog();
		}

		public new void Show()
		{
			if (LoadParameters())
				base.Show();
		}

		private bool LoadParameters()
		{
			Parameter par;
			string message, caption;

			ProjResource.gResource.LoadMessageBox(3, out caption, out message);

			mParam = new ParameterCollection(mDbInterface, true);

			while (!mParam.GetDataFromDb(null, null, "MODE", "SCHEDULE"))
			{
				if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
					return false;
			}
			//Se siamo in modalit� schedulazione non visualizzo il pulsante per la lista di estrazione 
			if (mParam.GetObject(null, null, "MODE", "SCHEDULE", out par))
				mSchedule = par.gBoolValue;

			while (!mParam.GetDataFromDb("", "", "TOOLBAR", "BUTTON_USELIST"))
			{
				if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
					return false;
			}
			if (mParam.GetObject("", "", "TOOLBAR", "BUTTON_USELIST", out par))
				btnListaEstraz.Visible = par.gBoolValue;
			else
			{
				par = new Parameter(Environment.MachineName, Application.ProductName, "TOOLBAR", "BUTTON_USELIST", Parameter.ParTypes.BOOL, "0", "Visualizzazione dei pulsanti per la lista di estrazione (1 = true, 0 = false)");
				mParam.AddObject(par);
				mParam.UpdateDataToDb();
				btnListaEstraz.Visible = false;
			}

			return true;
		}

		#endregion

		#region Form Function
		private void Orders_Load(object sender, System.EventArgs e)
		{
            Parameter par;

			// Carica le lingue del form
			ProjResource.gResource.LoadStringForm(this);
			Edit = ProjResource.gResource.LoadFixString(this,0);

            mOrders = new OrderCollection(mDbInterface, mUser);
			mProjects = new ProjectCollection(mDbInterface);
            mProjectsDel = new ProjectCollection(mDbInterface);
            mDetails = new DetailCollection(mDbInterface);
			mCustomers = new CustomerCollection(mDbInterface);
            mTecnoOp = new TecnoOperation(mDbInterface);
            mParamApp = new ParameterCollection(mDbInterface, false);
            mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridOrders, dataTableOrders);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridOrders);

            dirXml = new DirectoryInfo(tmpXmlDir);
            if (!dirXml.Exists)
            {
                dirXml.Create();
                dirXml.Attributes = FileAttributes.Hidden;
            }

			DisablePanelEdit();

			if (mCustomers.GetDataFromDb(Customer_Supplier.Keys.F_CODE) && mCustomers.Count > 0)	// Carica i clienti
            {
                FillComboCustomer(cmbxCustomerCode, cmbxCustomerName);		// e riempie le comboBox
                FillComboCustomer(cmbxFilterCustCode, cmbxFilterCustName);
				cmbxCustomerCode.SelectedValue = -1;
				cmbxCustomerName.SelectedValue = -1;
				cmbxFilterCustCode.SelectedValue = -1;
				cmbxFilterCustName.SelectedValue = -1;
            }

            // Carica tutti gli ordini
            if (mDiscard)
            {
                if (mOrders.GetDataFromDb(Order.Keys.F_ID, null, null, null, Order.State.ORDER_RELOADED.ToString()))
                {
                    FillTable();				// e riempie la Table
                }
            }
            else
            {
				if (mOrders.GetDataFromDb(Order.Keys.F_ID))
                {
                    FillTable();				// e riempie la Table
                }
            }

            for (int i = 0; i < (int)Order.State.MAX; i++)
            {
                cmbxState.Items.Add(StateEnumToString((Order.State)i));
                cmbxFilterState.Items.Add(StateEnumToString((Order.State)i));
            }

			btnListaEstraz.Visible = !mSchedule;

			if (mSchedule)
			{
				panelSchedule.Visible = true;
				panelUseList.Visible = false;
				panelOpti.Enabled = false;
				labelSeparator.Visible = true;
				rdbtnNoOpti.Visible = true;
				panelOpti.Height = rdbtnNoOpti.Location.Y + rdbtnNoOpti.Height;
				panelFilterAdv.Height = panelOpti.Height + 10;
				int left1 = panelOpti.Left;
				int left2 = panelSchedule.Left;
				panelSchedule.Left = left1;
				panelOpti.Left = left2;
			}
			else
			{
				panelSchedule.Visible = false;
				panelUseList.Visible = true;
				panelOpti.Enabled = true;
				panelUseList.Enabled = false;
				labelSeparator.Visible = false;
				rdbtnNoOpti.Visible = false;
				panelOpti.Height = rdbtOptimized.Location.Y + rdbtOptimized.Height;
				panelFilterAdv.Height = panelOpti.Height + 10;
			}
			
            SetToolTipText(); // Imposta i tooltip text

            c1DataSetProdProcess.Fill();	// Carica i processi produttivi

            // Imposta le dimensioni standard del panel
            panelFilter.Height = btnAdvanced.Location.Y + btnAdvanced.Height + 10;

            if (mDiscard)
                gDiscardCreate = true;
            else
			    gCreate = true;
		}

        private void Orders_Activated(object sender, EventArgs e)
        {
            if (mDiscard)
            {
                if (mOrders.RefreshData())
                    FillTable();
            }
        }

		private void Orders_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DialogResult dlgClose;
			string message ,caption;

			//Se ci sono state delle modifiche viene chiesto il salvataggio
			if (isModified)
			{				
				ProjResource.gResource.LoadMessageBox(0, out caption, out message);
				dlgClose = MessageBox.Show(message,caption,MessageBoxButtons.YesNoCancel,MessageBoxIcon.Question);
				if (dlgClose == DialogResult.Yes)
				{
                    if (!mOrders.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
                    {
                        ProjResource.gResource.LoadMessageBox(1, out caption, out message);
                        MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        OnUpdate();
                    }
					if (isProjectsMod && !mProjects.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
					{
						ProjResource.gResource.LoadMessageBox(1, out caption, out message);
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				else if (dlgClose == DialogResult.Cancel)
					e.Cancel=true;				//Non viene effettuato il salvataggio e la form rimane aperta
			}
		}

		private void Orders_Closed(object sender, System.EventArgs e)
		{
			mFormPosition.SaveFormPosition(this);
            mSortGrid = null;
			mColumnView = null;

            try
            {
                if (dirXml.Exists &&
                    !Breton.TechnoMaster.Form.Working.gCreate &&
                    !Breton.OfferManager.Form.Projects.gCreate &&
                    !Breton.OfferManager.Form.Details.gCreate &&
                    !Breton.OfferManager.Form.DetailsAdvance.gCreate &&
                    !Breton.OfferManager.Form.Offers.gCreate)
                    dirXml.Delete(true);
            }
            catch(Exception ex)
            {
                TraceLog.WriteLine("Orders.Orders_Closed", ex);
            }

            if (mDiscard)
                gDiscardCreate = false;
            else
                gCreate = false;

            Breton.OfferManager.Form.Projects.UpdateProject -= prjHandler;
            Breton.OfferManager.Form.Details.UpdateDetail -= detHandler;
		}

		#endregion

		#region  Gestione Tabelle

		private void FillComboCustomer(ComboBox custCode, ComboBox custName)
		{
			Customer cust;

			ArrayList codeCustomer = new ArrayList();
			ArrayList nameCustomer = new ArrayList();

			custCode.DataSource = null;
			custName.DataSource = null;
            
			custCode.Items.Clear();
            custName.Items.Clear();

			// Scorre tutti i clienti letti
			mCustomers.MoveFirst();
			while (!mCustomers.IsEOF())
			{
				// Legge il cliente attuale
				mCustomers.GetObject(out cust);
				cust.gTableId = codeCustomer.Count;

				codeCustomer.Add(cust);

				// Passa all'elemento successivo
				mCustomers.MoveNext();
			}
			custCode.DataSource = codeCustomer;
			custCode.DisplayMember = "gCode";
			custCode.ValueMember = "gTableId";

			// Riordina i clienti per descrizione
			mCustomers.SortByField(Customer.Keys.F_NAME.ToString());

			// Scorre tutti i clienti letti
			mCustomers.MoveFirst();
			while (!mCustomers.IsEOF())
			{
				// Legge il cliente attuale
				mCustomers.GetObject(out cust);
				nameCustomer.Add(cust);

				// Passa all'elemento successivo
				mCustomers.MoveNext();
			}

			custName.DataSource = nameCustomer;
			custName.DisplayMember = "gName";
			custName.ValueMember = "gTableId";
		}

		private void FillTable()
		{
			Order ord;
		
			dataTableOrders.Clear();

			// Scorre tutti gli ordini letti
			mOrders.MoveFirst();
			while (!mOrders.IsEOF())
			{
				// Legge l'ordine attuale
				mOrders.GetObject(out ord);
				
				dataTableOrders.BeginInit();
				dataTableOrders.BeginLoadData();
				// Aggiunge una riga alla volta
				AddRow(ref ord);
				
				dataTableOrders.EndInit();
				dataTableOrders.EndLoadData();
			}

            lblQty.Text = mOrders.Count.ToString();
		}

		private void AddRow(ref Order ord)
		{
			// Array con i dati della riga
			object [] myArray = new object[11];
			myArray[0] = dataTableOrders.Rows.Count;
			myArray[1] = ord.gId;
			myArray[2] = ord.gCode;
			myArray[3] = ord.gDate;
			if (ord.gDeliveryDate == DateTime.MaxValue)
				myArray[4] = null;
			else
				myArray[4] = ord.gDeliveryDate;
			myArray[5] = ord.gDescription;
			myArray[6] = ord.gCustomerCode;
			myArray[7] = ord.gOfferCode;
			myArray[8] = StateEnumstrToString(ord.gStateOrd);
			myArray[9] = FindCustomerName(ord.gCustomerCode);
			myArray[10] = (ord.gRevision >= 0 ? ord.gRevision : 0);

			// Crea una nuova riga nella tabella e la riempie di dati
			DataRow r = dataTableOrders.NewRow();
			r.ItemArray = myArray;
			dataTableOrders.Rows.Add(r);
			
			// Assegna l'id della riga tabella all'ordine
			ord.gTableId = int.Parse(r.ItemArray[0].ToString());

			// Passa all'elemento successivo
			mOrders.MoveNext();
		}

		private void Delete()		
		{	
			Order ord;
			int j=0;
			string message, caption;

			if (dataGridOrders.Bookmark >= 0)
			{
				// Indice della riga da eliminare
				int[] indexDelete = new int[0];

				// Carica le in un vettore gli indici degli ordini da eliminare
				while (j<dataGridOrders.SelectedRows.Count)
				{
					int[] tmp = new int[indexDelete.Length+1];
					indexDelete.CopyTo(tmp,0);
					indexDelete = tmp;						
					mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.SelectedRows[j],0].ToString()), out ord);
					indexDelete[j] = ord.gTableId;
					j++;
				}
				if (dataGridOrders.SelectedRows.Count == 0)
				{
					indexDelete = new int[1];
					mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.Bookmark,0].ToString()), out ord);
					indexDelete[j] = ord.gTableId;
				}				
			
				ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
				if (indexDelete.Length>1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					for (int i=0;i<indexDelete.Length;i++)
					{
						if(!mOrders.DeleteObjectTable(indexDelete[i]))
						{
							TraceLog.WriteLine("Orders: Delete() Error");
						}
					}
					isModified = true;
					ResetIndexTable();
				}
				else if (indexDelete.Length==1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					// Elimina la riga dalla collection e dalla tabella
					mOrders.DeleteObjectTable(indexDelete[0]);
					dataTableOrders.Rows.RemoveAt(indexDelete[0]);			
					isModified = true;
					ResetIndexTable();
				}		
				FillTable();
			}
		}
		
		private void ResetIndexTable()
		{
			Order ord;
			int i=0;

			mOrders.MoveFirst();
			while (!mOrders.IsEOF())
			{
				// Legge l'ordine
				mOrders.GetObject(out ord);
				
				ord.gTableId = i;
				i++;
				mOrders.MoveNext();
			}
			for (i=0; i<=dataTableOrders.Rows.Count-1; i++)
			{
				object [] myArray = new object[11];
				myArray = dataTableOrders.Rows[i].ItemArray;
				myArray[0] = i;
				dataTableOrders.Rows[i].ItemArray = myArray;
			}
		}

		#endregion

		#region Tool Bar
		private void toolBar_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			string message ,caption;
            Order ord;
            int i = 0;

			switch(toolBar.Buttons.IndexOf(e.Button))
			{
				// Modifica
				case 2:
					if (dataGridOrders.Bookmark >= 0)
					{						
						Operat = OrderOperation.Edit;					
						dataGridOrders.Enabled=false;
                        lblOperation.Text = Edit;
                        panelEdit.Visible = true;
                        panelFilter.Visible = false;
						EnablePanelEdit();
						AggiornaCampiNewMod();
						DisableToolBar_Edit();
						isDateChanged = false;
					}
					break;

				// Salva
				case 3:
					ProjResource.gResource.LoadMessageBox(0, out caption, out message);
					if (isModified && MessageBox.Show(message, caption ,MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
					{
						Operat = OrderOperation.Save;
                        this.Cursor = Cursors.WaitCursor;
                        if (!mOrders.UpdateDataToDb())
                        {
                            ProjResource.gResource.LoadMessageBox(1, out caption, out message);
                            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Cursor = Cursors.Default;
                            Operat = OrderOperation.None;
                        }
                        else
                        {
                            OnUpdate();
                        }
						if (isProjectsMod && !mProjects.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
						{
							ProjResource.gResource.LoadMessageBox(1, out caption, out message);
							MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Cursor = Cursors.Default;
						}
						else
						{
							isModified=false;
							if (mOrders.GetDataFromDb(Order.Keys.F_ID))
								FillTable();	//Riempe la Table

                            this.Cursor = Cursors.Default;
							Operat = OrderOperation.None;
						}
					}
					break;

                // Elimina ordine
                case 4:
                    this.Cursor = Cursors.WaitCursor;
                    Delete();
                    this.Cursor = Cursors.Default;
                    break;

                // Elimina tecno
                case 5:
                    Project prj;
                    Detail det;
                    bool transaction = !mDbInterface.TransactionActive();

                    try
                    {
                        if (dataGridOrders.Bookmark >= 0)
                        {
                            ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
                            if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                this.Cursor = Cursors.WaitCursor;

                                if (dataGridOrders.SelectedRows.Count == 0)
                                {
                                    if (mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.Bookmark, 0].ToString()), out ord))
                                    {
                                        //Apre la transazione
                                        if (transaction)
                                            mDbInterface.BeginTransaction();

                                        if (!mProjectsDel.GetDataFromDb(Project.Keys.F_CODE, ord.gCode))
                                            throw new ApplicationException("Error get projects from db");
                                        mProjectsDel.MoveFirst();
                                        while (!mProjectsDel.IsEOF())
                                        {
                                            if (mProjectsDel.GetObject(out prj))
                                            {
                                                if (!mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
                                                    throw new ApplicationException("Error get details from db");

                                                mDetails.MoveFirst();
                                                while (!mDetails.IsEOF())
                                                {
                                                    mWorkOrders = new WorkOrderCollection(mDbInterface);
                                                    mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                                    mUsedObjects = new UsedObjectCollection(mDbInterface);

                                                    mDetails.GetObject(out det);
                                                    if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                                                    {
                                                        if (!LoadTecnoPhases(det.gCode))
                                                            throw new ApplicationException("Error get tecno phases from db");
                                                        if (!DeleteAll())
                                                            throw new ApplicationException("Error delete tecno phases from db");
                                                    }
                                                    mDetails.MoveNext();
                                                }
                                            }
                                            mProjectsDel.MoveNext();
                                        }
                                        //Chiude la transazione
                                        if (transaction)
                                            mDbInterface.EndTransaction(true);
                                        
                                        UpdateProjects(null, null);
                                        OnSelectOrder(ord.gCode);
                                    }
                                }
                                else
                                {
                                    //Apre la transazione
                                    if (transaction)
                                        mDbInterface.BeginTransaction();

                                    for (i = 0; i < dataGridOrders.SelectedRows.Count; i++)
                                    {
                                        if (mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.SelectedRows[i], 0].ToString()), out ord))
                                        {
                                            if(!mProjectsDel.GetDataFromDb(Project.Keys.F_CODE, ord.gCode))
                                                throw new ApplicationException("Error get projects from db");

                                            mProjectsDel.MoveFirst();
                                            while (!mProjectsDel.IsEOF())
                                            {
                                                if (mProjectsDel.GetObject(out prj))
                                                {
                                                    if(!mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
                                                        throw new ApplicationException("Error get details from db");

                                                    mDetails.MoveFirst();
                                                    while (!mDetails.IsEOF())
                                                    {
                                                        mWorkOrders = new WorkOrderCollection(mDbInterface);
                                                        mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                                        mUsedObjects = new UsedObjectCollection(mDbInterface);

                                                        mDetails.GetObject(out det);
                                                        if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                                                        {
                                                            if(!LoadTecnoPhases(det.gCode))
                                                                throw new ApplicationException("Error get tecno phases from db");
                                                            if(!DeleteAll())
                                                                throw new ApplicationException("Error delete tecno phases from db");

                                                        }
                                                        mDetails.MoveNext();
                                                    }
                                                }
                                                mProjectsDel.MoveNext();
                                            }
                                        }
                                    }

                                    //Chiude la transazione
                                    if (transaction)
                                        mDbInterface.EndTransaction(true);

                                    for (i = 0; i < dataGridOrders.SelectedRows.Count; i++)
                                    {
                                        if (mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.SelectedRows[i], 0].ToString()), out ord))
                                            OnSelectOrder(ord.gCode);
                                    }

                                    UpdateProjects(null, null);
                                }
                                this.Cursor = Cursors.Default;
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        TraceLog.WriteLine("Orders.toolBar_ButtonClick DeleteTecno", ex);
                        //Chiude la transazione
                        if (transaction)
                            mDbInterface.EndTransaction(false);
                    }
                    break;

				// Reset
				case 6:
					ProjResource.gResource.LoadMessageBox(2, out caption, out message);
					if (isModified &&
						MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes &&
						mOrders.GetDataFromDb(Order.Keys.F_ID))
					{
						FillTable();
						Operat = OrderOperation.None;
						AggiornaCampiNewMod();
						DisablePanelEdit();
						isModified = false;
						isProjectsMod = false;
					}
					break;

				// Esci
				case 7:
					Close();
					break;

				// Progetti
				case 9:
					string orderCode;
                    ProjectCollection prjColl = new ProjectCollection(mDbInterface);

					if (dataGridOrders.Bookmark >= 0) 
						i = (int)dataGridOrders[dataGridOrders.Bookmark, 0];
					else
						return;
					// Carica il codice dell'ordine di cui si vogliono visualizzare i progetti
					mOrders.GetObjectTable(i,out ord);
					orderCode = ord.gCode.Trim();

                    prjColl.GetDataFromDb(Project.Keys.F_CODE, orderCode);
                    if (prjColl.Count > 1)
                    {
                        if (!Projects.gCreate)
                        {
                            Projects frmProjects = new Projects(mDbInterface, mUser, mLanguage, orderCode);
                            frmProjects.MdiParent = this.MdiParent;
                            frmProjects.Show();
                        }
                        else
                        {
                            OnSelectOrder(orderCode);
                            AttivaForm("Projects");
                        }
                    }
                    else if (prjColl.Count == 1)
                    {
                        if (!Details.gCreate)
                        {
                            prjColl.GetObject(out prj);
                            Details frmDetails = new Details(mDbInterface, mUser, mLanguage, prj.gCode);
                            frmDetails.MdiParent = this.MdiParent;
                            frmDetails.Show();
                        }
                        else
                        {
                            OnSelectOrder(orderCode);
                            AttivaForm("Details");
                        }
                    }
					break;

                // Lista di estrazione
                case 10:
                    Slab slb;
                    string[] slabCodes;
                    SlabCollection useListSlabs = new SlabCollection(mDbInterface);
                    SlabCollection tmpSlabs = new SlabCollection(mDbInterface);

                    if (dataGridOrders.Bookmark >= 0)
                        i = (int)dataGridOrders[dataGridOrders.Bookmark, 0];
                    else
                        return;

                    // Carica il codice dell'ordine di cui si vogliono visualizzare le lastre
                    mOrders.GetObjectTable(i, out ord);

                    if (!mOrders.GetOrderSlabs(ord.gCode.Trim(), out slabCodes, true))
                        return;

                    if (slabCodes != null && slabCodes.Length > 0)
                        tmpSlabs.GetDataFromDb_array(Article.Keys.F_CODE, slabCodes);
                    else
                        return;

                    // Carica una collection da passare alla lista di estrazione
                    tmpSlabs.MoveFirst();
                    while (!tmpSlabs.IsEOF())
                    {
                        if (tmpSlabs.GetObject(out slb) && slb.gStateArt.Trim() == Article.ArticleStates.START_PROGRAMMED.ToString()) 
                        {
                            slb.gImageLink = "";
                            useListSlabs.AddObject(slb);
                        }
                        tmpSlabs.MoveNext();
                    }

                    if (!UseLists.gCreate)
                    {
                        UseLists frmUslst = new UseLists(mDbInterface, mUser, mLanguage, useListSlabs);
                        frmUslst.MdiParent = this.MdiParent;
                        frmUslst.Show();
                    }
                    else
                    {
                        AttivaForm("UseLists");
                    }
                    break;

                // Filtro
                case 11:
                    panelFilter.Visible = true;
                    this.splitter.MinSize = (btnAdvanced.Location.Y + btnAdvanced.Height + 10);
                    break;

                // Processo produttivo
                case 12:
                    Parameter par;

                    // Verifica che l'indice sia valido
                    if (dataGridOrders.Bookmark >= 0)
                    {
                        if (mParamApp.GetDataFromDb("", "", "TECNO", "LAST_PROCESS") &&
                          mParamApp.GetObject("", "", "TECNO", "LAST_PROCESS", out par))
                            c1ProdProcess.SelectedValue = par.gIntValue;

                        panelProdProcess.Visible = true;
                    }
                    else
                        return;
                    break;

                // Refresh
                case 14:
                    this.Cursor = Cursors.WaitCursor;
                    if (mOrders.RefreshData())
                        FillTable();
                    this.Cursor = Cursors.Default;
                    break;
			}
		}
		#endregion

		#region Eventi dei controlli

		private void dataGridOrders_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
		{
			CellValue();
		}

		private void CellValue()
		{
			Order ord;
			Customer cust;
			string customerCode;
			int i;

			if (dataGridOrders.Bookmark >= 0) 
				i = (int)dataGridOrders[dataGridOrders.Bookmark, 0];
			else
				return;

			// Aggiorna i campi di Edit con i valori della riga selezionata se non sto facendo operazioni
			if (Operat == OrderOperation.None)
			{
				if 	(mOrders.GetObjectTable(i, out ord))
				{
					customerCode = ord.gCustomerCode;
					for (i=0;i<=mCustomers.Count-1;i++)
					{
						if (mCustomers.GetObjectTable(i, out cust))
						{
							if (cust.gCode == customerCode)
							{
								cmbxCustomerCode.SelectedValue = cust.gTableId;
								break;
							}
							else if (cust.gCode.Length == 0)
							{
								cmbxCustomerCode.SelectedValue = -1;
								break;
							}
						}
					}
					//cmbxState.SelectedIndex = StateEnumstrToInt(ord.gStateOrd);
					if (ord.gDeliveryDate == DateTime.MaxValue)
					{
						txtDeliveryDate.Text = "";
						deliveryDate.ResetText();
					}
					else
					{
						txtDeliveryDate.Text = ord.gDeliveryDate.ToShortDateString();
						deliveryDate.Value = ord.gDeliveryDate;
					}
					txtDescription.Text = ord.gDescription;
				}
			}
		}

        private void dataGridOrders_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Order ord;

            int i = (int)dataGridOrders[e.Row, "T_ID"];
            mOrders.GetObjectTable(i, out ord);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (ord.gStateOrd.Trim() == Order.State.ORDER_LOADED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_ANALIZED.ToString())
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_CONFIRMED.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_COMPLETED.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_UNLOADED.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_NDISP.ToString())
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.BackColor2 = Color.Coral;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_RELOADED.ToString())
            {
                e.CellStyle.BackColor = Color.Orange;
                e.CellStyle.BackColor2 = Color.Gold;
            }
        }

		private void cmbxCustomerCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbxCustomerCode.SelectedValue != null)
				cmbxCustomerName.SelectedValue = cmbxCustomerCode.SelectedValue;
		}

		private void cmbxCustomerName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbxCustomerName.SelectedValue != null)
				cmbxCustomerCode.SelectedValue = cmbxCustomerName.SelectedValue;
		}

		private void cmbxFilterCustCode_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbxFilterCustCode.SelectedValue != null)
				cmbxFilterCustName.SelectedValue = cmbxFilterCustCode.SelectedValue;
		}

		private void cmbxFilterCustName_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbxFilterCustName.SelectedValue != null)
				cmbxFilterCustCode.SelectedValue = cmbxFilterCustName.SelectedValue;
		}

		private void btnConferma_Click(object sender, System.EventArgs e)
		{
			Order ord;
			Customer cust;
			DateTime prevDate;
			
			// Operazione in corso: Modifica ordine
			if (Operat==OrderOperation.Edit)
			{
				// Estrae l'oggetto da modificare
				if (mOrders.GetObjectTable((int)dataGridOrders[dataGridOrders.Bookmark, 0], out ord))
				{
					// Aggiorna tutti i valori con i nuovi inseriti
					mCustomers.GetObjectTable((int)cmbxCustomerCode.SelectedValue, out cust);
					if (cust != null)
						ord.gCustomerCode = cust.gCode;

					//ord.gStateOrd = StateStringToEnum(cmbxState.SelectedItem.ToString()).ToString();
					if (isDateChanged)
					{
						prevDate = ord.gDeliveryDate;
						ord.gDeliveryDate = deliveryDate.Value;
						SetAllProjectsDate(ord.gCode, prevDate, deliveryDate.Value);
					}
					ord.gDescription = txtDescription.Text;					
				}

				// Aggiorna la Table
				int idTable = (int)dataGridOrders[dataGridOrders.Bookmark, 0];
                object[] rowChange =  {idTable, ord.gId, ord.gCode, ord.gDate, ord.gDeliveryDate, ord.gDescription, ord.gCustomerCode, 
										  ord.gOfferCode, StateEnumstrToString(ord.gStateOrd), FindCustomerName(ord.gCustomerCode)};
								
				if (ord.gDeliveryDate == DateTime.MaxValue)
					rowChange[4] = null;
				else
					rowChange[4] = ord.gDeliveryDate;
				
				dataTableOrders.Rows[idTable].ItemArray = rowChange;		
				isModified=true;
				dataGridOrders.Enabled=true;
			}
			else if (Operat==OrderOperation.None)
			{
				return;
			}
			// Abilita-Disabilita i vari controlli
            panelEdit.Visible = false;
			Operat = OrderOperation.None;
			DisablePanelEdit();
			AggiornaCampiNewMod();
			EnableToolBar_Edit();
			CellValue();
		}

		private void btnNascondi_Click(object sender, System.EventArgs e)
		{
			// Annnulla l'operazione in corso
			DisablePanelEdit();
            panelEdit.Visible = false;
			dataGridOrders.Enabled=true;
			Operat=OrderOperation.None;
			EnableToolBar_Edit();
			AggiornaCampiNewMod();
			CellValue();
		}

        private void btnResetFilter_Click(object sender, EventArgs e)
        {
            // Resetta i valori dei controlli per il filtro
            txtCode.Text = "";
			cmbxFilterCustCode.SelectedValue = -1;
			cmbxFilterCustName.SelectedValue = -1;
            cmbxFilterState.SelectedIndex = -1;
            txtFilterDescr.Text = "";

            insertDateFrom.ResetText();
            insertDateTo.ResetText();
            deliveryDateFrom.ResetText();
            deliveryDateTo.ResetText();

            Font regFont = new Font(insertDateFrom.Font, FontStyle.Regular);
            insertDateFrom.Font = regFont;
            insertDateTo.Font = regFont;
            deliveryDateFrom.Font = regFont;
            deliveryDateTo.Font = regFont;

            isDateDeliv = false;
            isDateInsert = false;
        }

        private void btnFiltra_Click(object sender, EventArgs e)
        {
            string fromIns, toIns, fromDel, toDel;
            string state = "";
			Order.AdvanceState scheduleState = Order.AdvanceState.NONE;
            Order.AdvanceState optiState = Order.AdvanceState.NONE;
            Order.AdvanceState useListState = Order.AdvanceState.NONE;

            if (txtCode.Text.Length > 0 || cmbxFilterCustCode.Text.Length > 0 || cmbxFilterCustName.Text.Length > 0 ||
                cmbxFilterState.SelectedIndex >= 0 || isDateInsert || isDateDeliv || txtFilterDescr.Text.Length > 0 || mAdvancedFilter)
            {
                DateTime fromInsDate = new DateTime(insertDateFrom.Value.Year, insertDateFrom.Value.Month, insertDateFrom.Value.Day, 0, 0, 0);
                fromIns = mDbInterface.OleDbToDate(fromInsDate);
                DateTime toInsDate = new DateTime(insertDateTo.Value.Year, insertDateTo.Value.Month, insertDateTo.Value.Day, 0, 0, 0);
                toIns = mDbInterface.OleDbToDate(toInsDate);
                DateTime fromDelDate = new DateTime(deliveryDateFrom.Value.Year, deliveryDateFrom.Value.Month, deliveryDateFrom.Value.Day, 0, 0, 0);
                fromDel = mDbInterface.OleDbToDate(fromDelDate);
                DateTime toDelDate = new DateTime(deliveryDateTo.Value.Year, deliveryDateTo.Value.Month, deliveryDateTo.Value.Day, 0, 0, 0);
                toDel = mDbInterface.OleDbToDate(toDelDate);

                if (mAdvancedFilter)
                {
					if (rdbtnNoSchedule.Checked)
						scheduleState = Order.AdvanceState.NO_SCHEDULED;
					else if (rdbtnTempSchedule.Checked)
						scheduleState = Order.AdvanceState.PARTIAL_SCHEDULED;
					else if (rdbtnAllSchedule.Checked)
						scheduleState = Order.AdvanceState.TOTAL_SCHEDULE;

                    if (rdbtnOptimizing.Checked)
                        optiState = Order.AdvanceState.OPTIMIZING;
                    else if (rdbtnTempOpti.Checked)
                        optiState = Order.AdvanceState.PARTIAL_OPTIMIZED;
                    else if (rdbtOptimized.Checked)
                        optiState = Order.AdvanceState.OPTIMIZED;
					else if (rdbtnNoOpti.Checked)
						optiState = Order.AdvanceState.NO_OPTI;

                    if (rdbtnNoExtractionList.Checked)
                        useListState = Order.AdvanceState.NO_USE_LIST;
                    else if (rdbtnTempExtractionList.Checked)
                        useListState = Order.AdvanceState.PARTIAL_USE_LIST;
                    else if (rdbtnAllExtractionList.Checked)
                        useListState = Order.AdvanceState.TOTAL_USE_LIST;
                }
                else
                {
                    if (cmbxFilterState.SelectedIndex >= 0)
                        state = StateStringToEnum(cmbxFilterState.Text).ToString();
                }

                mOrders.GetFilterDataFromDb(Order.Keys.F_ID, txtCode.Text, cmbxFilterCustCode.Text, cmbxFilterCustName.Text,
					state, fromIns, toIns, isDateInsert, fromDel, toDel, isDateDeliv, txtFilterDescr.Text, scheduleState, optiState, useListState, false);
            }
            else
            {
				mOrders.GetDataFromDb(Order.Keys.F_ID);
            }

            FillTable();
        }

        private void btnChiudi_Click(object sender, EventArgs e)
        {
            panelFilter.Visible = false;
        }

		private void deliveryDate_ValueChanged(object sender, System.EventArgs e)
		{
			isDateChanged = true;
		}

        private void insertDateFrom_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(insertDateFrom.Font, FontStyle.Bold);
            insertDateFrom.Font = boldFont;
            isDateInsert = true;
            if (insertDateFrom.Value > insertDateTo.Value)
                insertDateFrom.Value = insertDateTo.Value;
        }

        private void insertDateTo_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(insertDateTo.Font, FontStyle.Bold);
            insertDateTo.Font = boldFont;
            isDateInsert = true;
            if (insertDateTo.Value < insertDateFrom.Value)
                insertDateTo.Value = insertDateFrom.Value;
        }

        private void deliveryDateFrom_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(deliveryDateFrom.Font, FontStyle.Bold);
            deliveryDateFrom.Font = boldFont;
            isDateDeliv = true;
            if (deliveryDateFrom.Value > deliveryDateTo.Value)
                deliveryDateFrom.Value = deliveryDateTo.Value;
        }

        private void deliveryDateTo_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(deliveryDateTo.Font, FontStyle.Bold);
            deliveryDateTo.Font = boldFont;
            isDateDeliv = true;
            if (deliveryDateTo.Value < deliveryDateFrom.Value)
                deliveryDateTo.Value = deliveryDateFrom.Value;
        }

        private void c1ProdProcess_SelectedValueChanged(object sender, EventArgs e)
        {
            if (c1ProdProcess.SelectedIndex >= 0)
                btnOkSimpleTecno.Enabled = true;
            else
                btnOkSimpleTecno.Enabled = false;
        }

        /// <summary>
        /// Conferma il processo produttivo selezionato creando le fasi tecno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOkSimpleTecno_Click(object sender, EventArgs e)
        {
            Order ord;
            Parameter par;
            int j = 0;
            string caption, message;

            if (mTecno == null)
                mTecno = new ThreadTecno(mDbInterface, dataTableWorkGroup, dataTableWorking, c1WorkCenterGrid, c1WorkingGrid);

            if (dataGridOrders.Bookmark >= 0)
            {
                mTecno.gSelectedProcess = (int)c1ProdProcess.SelectedValue;

                if (!mTecno.gInProgress)
                {
                    while (j < dataGridOrders.SelectedRows.Count)
                    {
                        mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.SelectedRows[j], 0].ToString()), out ord);
                        mTecno.AddOrder(ord);
                        j++;
                    }

                    if (dataGridOrders.SelectedRows.Count == 0)
                    {
                        mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.Bookmark, 0].ToString()), out ord);
                        mTecno.AddOrder(ord);
                    }

                    ProjResource.gResource.LoadMessageBox(this, 6, out caption, out message);
                    if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        c1ProdProcess.Enabled = false;
                        mTecno.Start();
                        progressBar.Visible = true;
                        progressBar.Value = 0;
                        timerTecno.Enabled = true;
                        timerTecno.Start();
                    }
                }
                else
                {
                    ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);
                    if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        if (mTecno != null && mTecno.gInProgress)
                            mTecno.Stop();
                    }
                }

                if (mParamApp.GetDataFromDb("", "", "TECNO", "LAST_PROCESS") &&
                     mParamApp.GetObject("", "", "TECNO", "LAST_PROCESS", out par))
                {
                    par.gIntValue = (int)c1ProdProcess.SelectedValue;
                }
                else
                {
                    par = new Parameter(Environment.MachineName, Application.ProductName, "TECNO", "LAST_PROCESS", Parameter.ParTypes.INT, c1ProdProcess.SelectedValue.ToString(), "");
                    mParamApp.AddObject(par);
                }
                mParamApp.UpdateDataToDb();
            }
        }

        private void timerTecno_Tick(object sender, EventArgs e)
        {
            string message, caption;

            // Verifica se sta copiando
            if (mTecno != null && mTecno.gInProgress)
            {
                if (mTecno.gMaxProgress > 0)
                {
                    // Setta i valori della progressBar
                    progressBar.Maximum = mTecno.gMaxProgress;
                    progressBar.Value = mTecno.gProgress;
                }

                // Verifica se � terminata l'analisi dell'ordine
                if (mTecno.gAnalized)
                {
                    mTecno.Continue();
                }
            }
            else
            {
                // Se il processo � terminato
                timerTecno.Stop();
                timerTecno.Enabled = false;
                progressBar.Visible = false;
                Refresh();
                c1ProdProcess.Enabled = true;

                this.Cursor = Cursors.WaitCursor;
                OnUpdate();
                mOrders.RefreshData();
                FillTable();
                this.Cursor = Cursors.Default;

                // Verifica il risultato delle fasi tecno
                if (mTecno.gError)
                {
                    ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
                    MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mTecno.gError = false;
                }
                if (mTecno.gNotLoaded)
                {
                    ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
                    MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mTecno.gNotLoaded = false;
                }

                mTecno = null;
            }
        }

        private void btnNascondiSimpleTecno_Click(object sender, EventArgs e)
        {
            panelProdProcess.Visible = false;
        }

        private void btnAdvanced_Click(object sender, EventArgs e)
        {
            mAdvancedFilter = true;
            panelFilterAdv.Visible = true;
            cmbxFilterState.SelectedIndex = -1;
            cmbxFilterState.Enabled = false;

			while (btnAdvanced.Location.Y + btnAdvanced.Height > panelFilterAdv.Location.Y)
            {
                panelFilter.Height += 2;
                System.Threading.Thread.Sleep(1);
            }
			panelFilterAdv.BorderStyle = BorderStyle.Fixed3D;            

			splitter.MinSize = (panelFilterAdv.Location.Y + 10 + panelFilterAdv.Height);
        }

        private void btnCloseAdvFilter_Click(object sender, EventArgs e)
        {
            mAdvancedFilter = false;
            panelFilterAdv.Visible = false;
            cmbxFilterState.Enabled = true;
            // Riporto allo stato iniziale il pannello
            panelFilterAdv.BorderStyle = BorderStyle.None;
            while (panelFilter.Height > btnAdvanced.Location.Y + btnAdvanced.Height + 10)
            {
                panelFilter.Height -= 2;
                System.Threading.Thread.Sleep(1);
            }
            splitter.MinSize = (btnAdvanced.Location.Y + btnAdvanced.Height + 10);
        }

        private void FirstPart_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbtnOptimizing.Checked)
            {
                rdbtnNoExtractionList.Checked = false;
                rdbtnTempExtractionList.Checked = false;
                rdbtnAllExtractionList.Checked = false;
                panelUseList.Enabled = false;
            }
            else if (rdbtnTempOpti.Checked)
            {
                rdbtnNoExtractionList.Checked = true;
                rdbtnTempExtractionList.Checked = false;
                rdbtnAllExtractionList.Checked = false;
                panelUseList.Enabled = true;
            }
            else if (rdbtOptimized.Checked)
            {
                rdbtnNoExtractionList.Checked = true;
                rdbtnTempExtractionList.Checked = false;
                rdbtnAllExtractionList.Checked = false;
                panelUseList.Enabled = true;
            }
        }

		private void SchedulePart_CheckedChanged(object sender, EventArgs e)
		{
			if (rdbtnNoSchedule.Checked)
			{
				rdbtnOptimizing.Checked = false;
				rdbtnTempOpti.Checked = false;
				rdbtOptimized.Checked = false;
				rdbtnNoOpti.Checked = false;
				panelOpti.Enabled = false;
			}
			else if (rdbtnTempSchedule.Checked)
			{
				rdbtnOptimizing.Checked = false;
				rdbtnTempOpti.Checked = false;
				rdbtOptimized.Checked = false;
				rdbtnNoOpti.Checked = false;
				panelOpti.Enabled= true;
			}
			else if (rdbtnAllSchedule.Checked)
			{
				rdbtnOptimizing.Checked = false;
				rdbtnTempOpti.Checked = false;
				rdbtOptimized.Checked = false;
				rdbtnNoOpti.Checked = false;
				panelOpti.Enabled = true;
			}
		}
		#endregion

		#region Gestione Eventi
		//Genera l'evento selectOrder
		protected virtual void OnSelectOrder(string ev)
		{
			if (SelectOrderEvent != null)
				SelectOrderEvent(this, ev);
		}
        
        private void UpdateProjects(object sender, EventArgs ev)
        {
            if (mDiscard)
            {
                if (mOrders.GetDataFromDb(Order.Keys.F_ID, null, null, null, Order.State.ORDER_RELOADED.ToString()))
                    FillTable();				// e riempie la Table
            }
            else
            {
                if (mOrders.RefreshData()) //.GetDataFromDb())
                    FillTable();				// e riempie la Table
            }
        }

        private void UpdateDetails(object sender, EventArgs ev)
        {
            if (mDiscard)
            {
                if (mOrders.GetDataFromDb(Order.Keys.F_ID, null, null, null, Order.State.ORDER_RELOADED.ToString()))
                    FillTable();				// e riempie la Table
            }
            else
            {
                if (mOrders.RefreshData()) //.GetDataFromDb())
                    FillTable();				// e riempie la Table
            }
        }

        //Genera l'evento OnUpdate
        protected virtual void OnUpdate()
        {
            if (UpdateOrders != null)
                UpdateOrders(this, new EventArgs());
        }
		#endregion
        
		#region Abilita-Disabilita vari controlli
		
		private void AggiornaCampiNewMod()
		{
			// Aggiornamento dei campi di Edit a seconda dell'operazione in corso
			if (Operat==OrderOperation.Edit)
			{
				CellValue();
			}
			else if (Operat==OrderOperation.None)
			{
				lblOperation.Text="";
				cmbxCustomerCode.SelectedValue = -1;
				cmbxCustomerName.SelectedValue = -1;
				//cmbxState.SelectedIndex = -1;
				deliveryDate.ResetText();
				txtDescription.Text = "";
				dataGridOrders.Enabled=true;
			}			
		}

		// Abilita i pulsanti della ToolBar
		private void EnableToolBar_Edit()
		{
            btnModifica.Enabled = true;
            btnSalva.Enabled = true;
            btnElimina.Enabled = true;
            btnReset.Enabled = true;
            btnProgetti.Enabled = true;
            btnListaEstraz.Enabled = true;
            btnFilter.Enabled = true;
		}

		// Disabilita i pulsanti della ToolBar
		private void DisableToolBar_Edit()
		{
            btnModifica.Enabled = false;
            btnSalva.Enabled = false;
            btnElimina.Enabled = false;
            btnReset.Enabled = false;
            btnProgetti.Enabled = false;
            btnListaEstraz.Enabled = false;
            btnFilter.Enabled = false;
		}

		// Abilita i controlli presenti nel panelEdit
		private void EnablePanelEdit()
		{
			cmbxCustomerCode.Enabled = true;
			cmbxCustomerName.Enabled = true;
			//cmbxState.Enabled = true;
			deliveryDate.Visible = true;
			txtDeliveryDate.Visible = false;
			txtDescription.ReadOnly = false;
			btnConferma.Enabled=true;
			btnNascondi.Enabled=true;
		}

		// Disabilita i controlli presenti nel panelEdit
		private void DisablePanelEdit()
		{
			cmbxCustomerCode.Enabled = false;
			cmbxCustomerName.Enabled = false;
			//cmbxState.Enabled = false;
			deliveryDate.Visible = false;
			txtDeliveryDate.Visible = true;
			txtDescription.ReadOnly = true;
			btnConferma.Enabled=false;
			btnNascondi.Enabled=false;
		}

		private void AttivaForm(string formName)
		{
			System.Windows.Forms.Form[] frm = new System.Windows.Forms.Form[this.ParentForm.MdiChildren.Length];
			frm =this.ParentForm.MdiChildren;
			for (int i=0;i<frm.Length;i++)
			{
				if(frm[i].Name == formName)
					frm[i].Activate();						
			}
		}

		private string FindCustomerName(string custCode)
		{
			Customer cust;
			mCustomers.MoveFirst();

			while (!mCustomers.IsEOF())
			{
				if(mCustomers.GetObject(out cust) && cust.gCode==custCode)
					return cust.gName;

				mCustomers.MoveNext();
			}
			return "";
		}
		#endregion	

        #region Private Methods

        private void SetAllProjectsDate(string orderCode, DateTime oldDate, DateTime newDate)
        {
            Project prj;

            mProjects.GetDataFromDb(Project.Keys.F_CODE, orderCode);
            // Scorre tutti i progetti letti
            mProjects.MoveFirst();
            while (!mProjects.IsEOF())
            {
                // Legge il progetto attuale
                mProjects.GetObject(out prj);
                if (prj.gCustomerOrderCode == orderCode && prj.gDeliveryDate == oldDate)
                {
                    prj.gDeliveryDate = newDate;
                    isProjectsMod = true;
                }
                mProjects.MoveNext();
            }
        }

        /// <summary>
        /// Enum -> string
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        private string StateEnumToString(Order.State st)
        {
            switch (st)
            {
                case Order.State.ORDER_LOADED:
                    return ProjResource.gResource.LoadFixString(this, 1);
                case Order.State.ORDER_CONFIRMED:
                    return ProjResource.gResource.LoadFixString(this, 2);
                case Order.State.ORDER_IN_PROGRESS:
                    return ProjResource.gResource.LoadFixString(this, 3);
                case Order.State.ORDER_COMPLETED:
                    return ProjResource.gResource.LoadFixString(this, 4);
                case Order.State.ORDER_UNLOADED:
                    return ProjResource.gResource.LoadFixString(this, 5);
                case Order.State.ORDER_NDISP:
                    return ProjResource.gResource.LoadFixString(this, 6);
                case Order.State.ORDER_ANALIZED:
                    return ProjResource.gResource.LoadFixString(this, 7);
                case Order.State.ORDER_RELOADED:
                    return ProjResource.gResource.LoadFixString(this, 8);
                default:
                    return "";
            }
        }

        /// <summary>
        /// Da stringa enum -> stringa
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        private string StateEnumstrToString(string st)
        {
            if (st.Trim() == Order.State.ORDER_LOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 1);
            else if (st.Trim() == Order.State.ORDER_CONFIRMED.ToString())
                return ProjResource.gResource.LoadFixString(this, 2);
            else if (st.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 3);
            else if (st.Trim() == Order.State.ORDER_COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 4);
            else if (st.Trim() == Order.State.ORDER_UNLOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 5);
            else if (st.Trim() == Order.State.ORDER_NDISP.ToString())
                return ProjResource.gResource.LoadFixString(this, 6);
            else if (st.Trim() == Order.State.ORDER_ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 7);
            else if (st.Trim() == Order.State.ORDER_RELOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 8);
            else
                return "";
        }
        
        /// <summary>
        /// Da stringa enum -> int
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        private int StateEnumstrToInt(string st)
        {
            if (st.Trim() == Order.State.ORDER_LOADED.ToString())
                return 0;
            else if (st.Trim() == Order.State.ORDER_CONFIRMED.ToString())
                return 1;
            else if (st.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
                return 2;
            else if (st.Trim() == Order.State.ORDER_COMPLETED.ToString())
                return 3;
            else if (st.Trim() == Order.State.ORDER_UNLOADED.ToString())
                return 4;
            else if (st.Trim() == Order.State.ORDER_NDISP.ToString())
                return 5;
            else if (st.Trim() == Order.State.ORDER_ANALIZED.ToString())
                return 6;
            else if (st.Trim() == Order.State.ORDER_RELOADED.ToString())
                return 7;
            else
                return -1;
        }

        /// <summary>
        /// string -> enum
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        private Order.State StateStringToEnum(string state)
        {
            if (state == ProjResource.gResource.LoadFixString(this, 1))
                return Order.State.ORDER_LOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 2))
                return Order.State.ORDER_CONFIRMED;
            else if (state == ProjResource.gResource.LoadFixString(this, 3))
                return Order.State.ORDER_IN_PROGRESS;
            else if (state == ProjResource.gResource.LoadFixString(this, 4))
                return Order.State.ORDER_COMPLETED;
            else if (state == ProjResource.gResource.LoadFixString(this, 5))
                return Order.State.ORDER_UNLOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 6))
                return Order.State.ORDER_NDISP;
            else if (state == ProjResource.gResource.LoadFixString(this, 7))
                return Order.State.ORDER_ANALIZED;
            else if (state == ProjResource.gResource.LoadFixString(this, 8))
                return Order.State.ORDER_RELOADED;
            else
                return Order.State.NONE;
        }

        /// <summary>
        /// Imposta i ToolTip text nei controlli necessari
        /// </summary>
        private void SetToolTipText()
        {
            toolTip.SetToolTip(rdbtnOptimizing, ProjResource.gResource.LoadFixString(this, 10));
            toolTip.SetToolTip(rdbtnTempOpti, ProjResource.gResource.LoadFixString(this, 11));
            toolTip.SetToolTip(rdbtOptimized, ProjResource.gResource.LoadFixString(this, 12));
            toolTip.SetToolTip(rdbtnNoExtractionList, ProjResource.gResource.LoadFixString(this, 13));
            toolTip.SetToolTip(rdbtnTempExtractionList, ProjResource.gResource.LoadFixString(this, 14));
            toolTip.SetToolTip(rdbtnAllExtractionList, ProjResource.gResource.LoadFixString(this, 15));
			toolTip.SetToolTip(rdbtnNoSchedule, ProjResource.gResource.LoadFixString(this, 16));
			toolTip.SetToolTip(rdbtnTempSchedule, ProjResource.gResource.LoadFixString(this, 17));
			toolTip.SetToolTip(rdbtnAllSchedule, ProjResource.gResource.LoadFixString(this, 18));
			toolTip.SetToolTip(rdbtnNoOpti, ProjResource.gResource.LoadFixString(this, 19));
        }

        /// ***********************************************************
        /// LoadTecnoPhases
        /// <summary>
        /// Carica le collection con gli ordini e le fasi di lavoro
        /// </summary>
        /// <param name="code">Codice del dettaglio</param>
        /// <returns></returns>
        /// ***********************************************************        
        private bool LoadTecnoPhases(string code)
        {
            int i = 0;
            string[] usObjs = new string[0];
            string[] wkOrds = new string[0];
            WorkPhase wk = new WorkPhase();

            if (!mWorkPhases.GetDataFromDb(WorkPhase.Keys.F_SEQUENCE, null, code, null, null, null))
                return false;

            mWorkPhases.MoveFirst();
            while (!mWorkPhases.IsEOF())
            {
                mWorkPhases.GetObject(out wk);

                string[] tmp = new string[wkOrds.Length + 1];
                wkOrds.CopyTo(tmp, 0);
                wkOrds = tmp;
                wkOrds[i] = wk.gWorkOrderCode;

                string[] tmp2 = new string[usObjs.Length + 1];
                usObjs.CopyTo(tmp2, 0);
                usObjs = tmp2;
                usObjs[i] = wk.gUsedObjectCode;

                i++;
                mWorkPhases.MoveNext();
            }
            string[] tmp3 = new string[usObjs.Length + 1];
            usObjs.CopyTo(tmp3, 0);
            usObjs = tmp3;
            usObjs[i] = wk.gPhaseDUsedObject;

            if (!mWorkOrders.GetDataFromDb(WorkOrder.Keys.F_SEQUENCE, null, null, null, null, null, wkOrds))
                return false;

            if (!mUsedObjects.GetDataFromDb(usObjs))
                return false;

            return true;
        }

        ///**********************************************************
        /// Delete All
        /// <summary>
        /// Elimina il processo produttivo (fase tecno) per il pezzo
        /// </summary>
        /// <returns></returns>
        /// *********************************************************
        private bool DeleteAll()
        {
            try
            {
                // Cancellazione di tutti i record caricati
                mWorkPhases.MoveFirst();
                while (!mWorkPhases.IsEmpty())
                    mWorkPhases.DeleteObject();
                if (!mWorkPhases.UpdateDataToDb())
                    throw new ApplicationException("Error on work phases updated");

                mUsedObjects.MoveFirst();
                while (!mUsedObjects.IsEmpty())
                    mUsedObjects.DeleteObject();
                if (!mUsedObjects.UpdateDataToDb())
                    throw new ApplicationException("Error on used objects updated");

                mWorkOrders.MoveFirst();
                while (!mWorkOrders.IsEmpty())
                    mWorkOrders.DeleteObject();
                if (!mWorkOrders.UpdateDataToDb())
                    throw new ApplicationException("Error on work orders updated");

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Details.DeleteAll Error: " + ex.Message);
                return false;
            }
            finally
            {
                mWorkPhases = null;
                mUsedObjects = null;
                mWorkOrders = null;
            }
        }
        #endregion
    }
}
