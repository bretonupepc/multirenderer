namespace Breton.OfferManager.Form
{
    partial class ProjectsAdvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectsAdvance));
            this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
            this.dataSetProjects = new System.Data.DataSet();
            this.dataTableProjects = new System.Data.DataTable();
            this.dataColumnT_ID = new System.Data.DataColumn();
            this.dataColumnF_ID = new System.Data.DataColumn();
            this.dataColumnF_CODE = new System.Data.DataColumn();
            this.dataColumnF_CUSTOMER_ORDER_CODE = new System.Data.DataColumn();
            this.dataColumnF_STATE = new System.Data.DataColumn();
            this.dataColumnF_DATE = new System.Data.DataColumn();
            this.dataColumnF_DELIVERY_DATE = new System.Data.DataColumn();
            this.dataColumnF_DESCRIPTION = new System.Data.DataColumn();
            this.dataColumnF_CODE_ORIGINAL_PROJECT = new System.Data.DataColumn();
            this.toolBar = new System.Windows.Forms.ToolBar();
            this.btnDettagli = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.btnAnnulla = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.btnRefresh = new System.Windows.Forms.ToolBarButton();
            this.dataGridProjects = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProjects)).BeginInit();
            this.SuspendLayout();
            // 
            // imlToolIcon
            // 
            this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
            this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.imlToolIcon.Images.SetKeyName(0, "");
            this.imlToolIcon.Images.SetKeyName(1, "");
            this.imlToolIcon.Images.SetKeyName(2, "");
            this.imlToolIcon.Images.SetKeyName(3, "");
            this.imlToolIcon.Images.SetKeyName(4, "DeleteTecno.ico");
            this.imlToolIcon.Images.SetKeyName(5, "");
            this.imlToolIcon.Images.SetKeyName(6, "");
            this.imlToolIcon.Images.SetKeyName(7, "");
            this.imlToolIcon.Images.SetKeyName(8, "Refresh.ico");
            // 
            // dataSetProjects
            // 
            this.dataSetProjects.DataSetName = "NewDataSet";
            this.dataSetProjects.Locale = new System.Globalization.CultureInfo("en-US");
            this.dataSetProjects.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableProjects});
            // 
            // dataTableProjects
            // 
            this.dataTableProjects.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnT_ID,
            this.dataColumnF_ID,
            this.dataColumnF_CODE,
            this.dataColumnF_CUSTOMER_ORDER_CODE,
            this.dataColumnF_STATE,
            this.dataColumnF_DATE,
            this.dataColumnF_DELIVERY_DATE,
            this.dataColumnF_DESCRIPTION,
            this.dataColumnF_CODE_ORIGINAL_PROJECT});
            this.dataTableProjects.TableName = "TableProjects";
            // 
            // dataColumnT_ID
            // 
            this.dataColumnT_ID.ColumnName = "T_ID";
            this.dataColumnT_ID.DataType = typeof(int);
            // 
            // dataColumnF_ID
            // 
            this.dataColumnF_ID.Caption = "Id";
            this.dataColumnF_ID.ColumnName = "F_ID";
            this.dataColumnF_ID.DataType = typeof(int);
            // 
            // dataColumnF_CODE
            // 
            this.dataColumnF_CODE.Caption = "Codice";
            this.dataColumnF_CODE.ColumnName = "F_CODE";
            // 
            // dataColumnF_CUSTOMER_ORDER_CODE
            // 
            this.dataColumnF_CUSTOMER_ORDER_CODE.Caption = "Codice ordine";
            this.dataColumnF_CUSTOMER_ORDER_CODE.ColumnName = "F_CUSTOMER_ORDER_CODE";
            // 
            // dataColumnF_STATE
            // 
            this.dataColumnF_STATE.Caption = "Stato";
            this.dataColumnF_STATE.ColumnName = "F_STATE";
            // 
            // dataColumnF_DATE
            // 
            this.dataColumnF_DATE.Caption = "Data inserimento";
            this.dataColumnF_DATE.ColumnName = "F_DATE";
            this.dataColumnF_DATE.DataType = typeof(System.DateTime);
            // 
            // dataColumnF_DELIVERY_DATE
            // 
            this.dataColumnF_DELIVERY_DATE.Caption = "Data consegna";
            this.dataColumnF_DELIVERY_DATE.ColumnName = "F_DELIVERY_DATE";
            this.dataColumnF_DELIVERY_DATE.DataType = typeof(System.DateTime);
            // 
            // dataColumnF_DESCRIPTION
            // 
            this.dataColumnF_DESCRIPTION.Caption = "Descrizione";
            this.dataColumnF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
            // 
            // dataColumnF_CODE_ORIGINAL_PROJECT
            // 
            this.dataColumnF_CODE_ORIGINAL_PROJECT.ColumnName = "F_CODE_ORIGINAL_PROJECT";
            // 
            // toolBar
            // 
            this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnDettagli,
            this.toolBarButton1,
            this.btnAnnulla,
            this.toolBarButton2,
            this.btnRefresh});
            this.toolBar.DropDownArrows = true;
            this.toolBar.ImageList = this.imlToolIcon;
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.ShowToolTips = true;
            this.toolBar.Size = new System.Drawing.Size(787, 44);
            this.toolBar.TabIndex = 4;
            this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
            // 
            // btnDettagli
            // 
            this.btnDettagli.ImageIndex = 7;
            this.btnDettagli.Name = "btnDettagli";
            // 
            // toolBarButton1
            // 
            this.toolBarButton1.Name = "toolBarButton1";
            this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // btnAnnulla
            // 
            this.btnAnnulla.ImageIndex = 6;
            this.btnAnnulla.Name = "btnAnnulla";
            this.btnAnnulla.ToolTipText = "Esci";
            // 
            // toolBarButton2
            // 
            this.toolBarButton2.Name = "toolBarButton2";
            this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // btnRefresh
            // 
            this.btnRefresh.ImageIndex = 8;
            this.btnRefresh.Name = "btnRefresh";
            // 
            // dataGridProjects
            // 
            this.dataGridProjects.AllowSort = false;
            this.dataGridProjects.AllowUpdate = false;
            this.dataGridProjects.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridProjects.CaptionHeight = 17;
            this.dataGridProjects.DataSource = this.dataTableProjects;
            this.dataGridProjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridProjects.FetchRowStyles = true;
            this.dataGridProjects.GroupByCaption = "Drag a column header here to group by that column";
            this.dataGridProjects.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridProjects.Images"))));
            this.dataGridProjects.Location = new System.Drawing.Point(0, 44);
            this.dataGridProjects.MaintainRowCurrency = true;
            this.dataGridProjects.Name = "dataGridProjects";
            this.dataGridProjects.PreviewInfo.Location = new System.Drawing.Point(0, 0);
            this.dataGridProjects.PreviewInfo.Size = new System.Drawing.Size(0, 0);
            this.dataGridProjects.PreviewInfo.ZoomFactor = 75;
            this.dataGridProjects.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridProjects.PrintInfo.PageSettings")));
            this.dataGridProjects.RowHeight = 15;
            this.dataGridProjects.Size = new System.Drawing.Size(787, 199);
            this.dataGridProjects.TabIndex = 5;
            this.dataGridProjects.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridProjects_FetchRowStyle);
            this.dataGridProjects.PropBag = resources.GetString("dataGridProjects.PropBag");
            // 
            // ProjectsAdvance
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(787, 243);
            this.Controls.Add(this.dataGridProjects);
            this.Controls.Add(this.toolBar);
            this.Name = "ProjectsAdvance";
            this.Text = "Progetti - Avanzamento della produzione";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProjectsAdvance_FormClosed);
            this.Load += new System.EventHandler(this.ProjectsAdvance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSetProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProjects)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imlToolIcon;
        private System.Data.DataSet dataSetProjects;
        private System.Data.DataTable dataTableProjects;
        private System.Data.DataColumn dataColumnT_ID;
        private System.Data.DataColumn dataColumnF_ID;
        private System.Data.DataColumn dataColumnF_CODE;
        private System.Data.DataColumn dataColumnF_CUSTOMER_ORDER_CODE;
        private System.Data.DataColumn dataColumnF_STATE;
        private System.Data.DataColumn dataColumnF_DATE;
        private System.Data.DataColumn dataColumnF_DELIVERY_DATE;
        private System.Data.DataColumn dataColumnF_DESCRIPTION;
        private System.Data.DataColumn dataColumnF_CODE_ORIGINAL_PROJECT;
        private System.Windows.Forms.ToolBar toolBar;
        private System.Windows.Forms.ToolBarButton btnAnnulla;
        private System.Windows.Forms.ToolBarButton toolBarButton1;
        private System.Windows.Forms.ToolBarButton btnDettagli;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridProjects;
        private System.Windows.Forms.ToolBarButton toolBarButton2;
        private System.Windows.Forms.ToolBarButton btnRefresh;
    }
}