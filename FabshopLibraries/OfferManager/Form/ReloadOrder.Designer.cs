namespace Breton.OfferManager
{
    partial class ReloadOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReloadOrder));
			this.panelInfo = new System.Windows.Forms.Panel();
			this.lblNothing = new System.Windows.Forms.Label();
			this.lblUpdate = new System.Windows.Forms.Label();
			this.lblDelete = new System.Windows.Forms.Label();
			this.lblInsert = new System.Windows.Forms.Label();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.btnConferma = new System.Windows.Forms.ToolStripButton();
			this.btnExit = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.lblInfo = new System.Windows.Forms.ToolStripLabel();
			this.PanelProject = new System.Windows.Forms.Panel();
			this.dataGridProjects = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.dataTableProjects = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.dataColumn9 = new System.Data.DataColumn();
			this.dataColumn21 = new System.Data.DataColumn();
			this.dataColumn23 = new System.Data.DataColumn();
			this.dataSetReloadOrder = new System.Data.DataSet();
			this.dataTableDetails = new System.Data.DataTable();
			this.dataColumn10 = new System.Data.DataColumn();
			this.dataColumn11 = new System.Data.DataColumn();
			this.dataColumn12 = new System.Data.DataColumn();
			this.dataColumn13 = new System.Data.DataColumn();
			this.dataColumn14 = new System.Data.DataColumn();
			this.dataColumn15 = new System.Data.DataColumn();
			this.dataColumn16 = new System.Data.DataColumn();
			this.dataColumn17 = new System.Data.DataColumn();
			this.dataColumn18 = new System.Data.DataColumn();
			this.dataColumn19 = new System.Data.DataColumn();
			this.dataColumn20 = new System.Data.DataColumn();
			this.dataColumn22 = new System.Data.DataColumn();
			this.dataColumn24 = new System.Data.DataColumn();
			this.dataColumn25 = new System.Data.DataColumn();
			this.dataGridDetails = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.splitter2 = new System.Windows.Forms.Splitter();
			this.VDPreview = new AxVDProLib5.AxVDPro();
			this.panelDetails = new System.Windows.Forms.Panel();
			this.panelPreview = new System.Windows.Forms.Panel();
			this.panelInfo.SuspendLayout();
			this.toolStrip.SuspendLayout();
			this.PanelProject.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridProjects)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableProjects)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetReloadOrder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableDetails)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).BeginInit();
			this.panelDetails.SuspendLayout();
			this.panelPreview.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelInfo
			// 
			this.panelInfo.BackColor = System.Drawing.Color.Transparent;
			this.panelInfo.Controls.Add(this.lblNothing);
			this.panelInfo.Controls.Add(this.lblUpdate);
			this.panelInfo.Controls.Add(this.lblDelete);
			this.panelInfo.Controls.Add(this.lblInsert);
			this.panelInfo.Location = new System.Drawing.Point(154, 12);
			this.panelInfo.Name = "panelInfo";
			this.panelInfo.Size = new System.Drawing.Size(159, 83);
			this.panelInfo.TabIndex = 0;
			this.panelInfo.Visible = false;
			// 
			// lblNothing
			// 
			this.lblNothing.BackColor = System.Drawing.Color.White;
			this.lblNothing.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblNothing.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblNothing.Location = new System.Drawing.Point(1, 64);
			this.lblNothing.Name = "lblNothing";
			this.lblNothing.Size = new System.Drawing.Size(157, 19);
			this.lblNothing.TabIndex = 3;
			this.lblNothing.Text = "Nessuna operazione";
			// 
			// lblUpdate
			// 
			this.lblUpdate.BackColor = System.Drawing.Color.Yellow;
			this.lblUpdate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblUpdate.Location = new System.Drawing.Point(1, 43);
			this.lblUpdate.Name = "lblUpdate";
			this.lblUpdate.Size = new System.Drawing.Size(157, 19);
			this.lblUpdate.TabIndex = 2;
			this.lblUpdate.Text = "Modifica esistente";
			// 
			// lblDelete
			// 
			this.lblDelete.BackColor = System.Drawing.Color.Red;
			this.lblDelete.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDelete.Location = new System.Drawing.Point(1, 22);
			this.lblDelete.Name = "lblDelete";
			this.lblDelete.Size = new System.Drawing.Size(157, 19);
			this.lblDelete.TabIndex = 1;
			this.lblDelete.Text = "Eliminazione esistente";
			// 
			// lblInsert
			// 
			this.lblInsert.BackColor = System.Drawing.Color.Lime;
			this.lblInsert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblInsert.Location = new System.Drawing.Point(1, 1);
			this.lblInsert.Name = "lblInsert";
			this.lblInsert.Size = new System.Drawing.Size(157, 19);
			this.lblInsert.TabIndex = 0;
			this.lblInsert.Text = "Creazione nuovo";
			// 
			// toolStrip
			// 
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnConferma,
            this.btnExit,
            this.toolStripSeparator1,
            this.lblInfo});
			this.toolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
			this.toolStrip.Location = new System.Drawing.Point(0, 0);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(1082, 39);
			this.toolStrip.TabIndex = 1;
			// 
			// btnConferma
			// 
			this.btnConferma.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnConferma.Image = global::OfferManager.Properties.Resources.Confirm;
			this.btnConferma.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.btnConferma.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnConferma.Name = "btnConferma";
			this.btnConferma.Size = new System.Drawing.Size(36, 36);
			this.btnConferma.Click += new System.EventHandler(this.btnConferma_Click);
			// 
			// btnExit
			// 
			this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.btnExit.Image = global::OfferManager.Properties.Resources.exit;
			this.btnExit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(36, 36);
			this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.AutoSize = false;
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 36);
			// 
			// lblInfo
			// 
			this.lblInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.lblInfo.Image = global::OfferManager.Properties.Resources.Info;
			this.lblInfo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
			this.lblInfo.Name = "lblInfo";
			this.lblInfo.Size = new System.Drawing.Size(32, 32);
			this.lblInfo.MouseLeave += new System.EventHandler(this.lblInfo_MouseLeave);
			this.lblInfo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lblInfo_MouseMove);
			// 
			// PanelProject
			// 
			this.PanelProject.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.PanelProject.Controls.Add(this.dataGridProjects);
			this.PanelProject.Dock = System.Windows.Forms.DockStyle.Top;
			this.PanelProject.Location = new System.Drawing.Point(0, 39);
			this.PanelProject.Name = "PanelProject";
			this.PanelProject.Size = new System.Drawing.Size(1082, 109);
			this.PanelProject.TabIndex = 12;
			// 
			// dataGridProjects
			// 
			this.dataGridProjects.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.IndividualRows;
			this.dataGridProjects.AllowSort = false;
			this.dataGridProjects.AllowUpdate = false;
			this.dataGridProjects.Caption = "Progetti";
			this.dataGridProjects.CaptionHeight = 17;
			this.dataGridProjects.DataSource = this.dataTableProjects;
			this.dataGridProjects.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridProjects.ExtendRightColumn = true;
			this.dataGridProjects.FetchRowStyles = true;
			this.dataGridProjects.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridProjects.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridProjects.Images"))));
			this.dataGridProjects.Location = new System.Drawing.Point(0, 0);
			this.dataGridProjects.Name = "dataGridProjects";
			this.dataGridProjects.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridProjects.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridProjects.PreviewInfo.ZoomFactor = 75;
			this.dataGridProjects.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridProjects.PrintInfo.PageSettings")));
			this.dataGridProjects.RowHeight = 15;
			this.dataGridProjects.Size = new System.Drawing.Size(1078, 105);
			this.dataGridProjects.TabIndex = 0;
			this.dataGridProjects.Text = "c1TrueDBGrid1";
			this.dataGridProjects.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridProjects_FetchRowStyle);
			this.dataGridProjects.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.dataGridProjects_RowColChange);
			this.dataGridProjects.PropBag = resources.GetString("dataGridProjects.PropBag");
			// 
			// dataTableProjects
			// 
			this.dataTableProjects.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn21,
            this.dataColumn23});
			this.dataTableProjects.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "T_ID"}, false)});
			this.dataTableProjects.TableName = "TableProjects";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "T_ID";
			this.dataColumn1.DataType = typeof(int);
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "F_ID";
			this.dataColumn2.DataType = typeof(int);
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "F_CODE";
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "F_CUSTOMER_ORDER_CODE";
			// 
			// dataColumn5
			// 
			this.dataColumn5.ColumnName = "F_STATE";
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "F_DATE";
			this.dataColumn6.DataType = typeof(System.DateTime);
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "F_DELIVERY_DATE";
			this.dataColumn7.DataType = typeof(System.DateTime);
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "F_DESCRIPTION";
			// 
			// dataColumn9
			// 
			this.dataColumn9.ColumnName = "F_CODE_ORIGINAL_PROJECT";
			// 
			// dataColumn21
			// 
			this.dataColumn21.ColumnName = "F_IMPORT_OPERATION";
			// 
			// dataColumn23
			// 
			this.dataColumn23.ColumnName = "F_NOTE";
			// 
			// dataSetReloadOrder
			// 
			this.dataSetReloadOrder.DataSetName = "DataSetReloadOrder";
			this.dataSetReloadOrder.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableProjects,
            this.dataTableDetails});
			// 
			// dataTableDetails
			// 
			this.dataTableDetails.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn22,
            this.dataColumn24,
            this.dataColumn25});
			this.dataTableDetails.TableName = "TableDetails";
			// 
			// dataColumn10
			// 
			this.dataColumn10.ColumnName = "T_ID";
			this.dataColumn10.DataType = typeof(int);
			this.dataColumn10.ReadOnly = true;
			// 
			// dataColumn11
			// 
			this.dataColumn11.ColumnName = "F_ID";
			this.dataColumn11.DataType = typeof(int);
			this.dataColumn11.ReadOnly = true;
			// 
			// dataColumn12
			// 
			this.dataColumn12.ColumnName = "F_CODE";
			this.dataColumn12.ReadOnly = true;
			// 
			// dataColumn13
			// 
			this.dataColumn13.ColumnName = "F_CUSTOMER_PROJECT_CODE";
			this.dataColumn13.ReadOnly = true;
			// 
			// dataColumn14
			// 
			this.dataColumn14.ColumnName = "F_MATERIAL_CODE";
			this.dataColumn14.ReadOnly = true;
			// 
			// dataColumn15
			// 
			this.dataColumn15.ColumnName = "F_MATERIAL_DESCRIPTION";
			this.dataColumn15.ReadOnly = true;
			// 
			// dataColumn16
			// 
			this.dataColumn16.ColumnName = "F_STATE";
			this.dataColumn16.ReadOnly = true;
			// 
			// dataColumn17
			// 
			this.dataColumn17.ColumnName = "F_THICKNESS";
			this.dataColumn17.DataType = typeof(double);
			this.dataColumn17.ReadOnly = true;
			// 
			// dataColumn18
			// 
			this.dataColumn18.ColumnName = "F_ARTICLE_CODE";
			this.dataColumn18.ReadOnly = true;
			// 
			// dataColumn19
			// 
			this.dataColumn19.ColumnName = "F_CODE_ORIGINAL_SHAPE";
			this.dataColumn19.ReadOnly = true;
			// 
			// dataColumn20
			// 
			this.dataColumn20.ColumnName = "F_DESCRIPTION";
			this.dataColumn20.ReadOnly = true;
			// 
			// dataColumn22
			// 
			this.dataColumn22.ColumnName = "F_IMPORT_OPERATION";
			this.dataColumn22.ReadOnly = true;
			// 
			// dataColumn24
			// 
			this.dataColumn24.ColumnName = "F_NOTE";
			this.dataColumn24.ReadOnly = true;
			// 
			// dataColumn25
			// 
			this.dataColumn25.ColumnName = "F_FINAL_THICKNESS";
			this.dataColumn25.DataType = typeof(double);
			// 
			// dataGridDetails
			// 
			this.dataGridDetails.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.IndividualRows;
			this.dataGridDetails.AllowSort = false;
			this.dataGridDetails.AllowUpdate = false;
			this.dataGridDetails.Caption = "Dettagli";
			this.dataGridDetails.CaptionHeight = 17;
			this.dataGridDetails.DataSource = this.dataTableDetails;
			this.dataGridDetails.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridDetails.ExtendRightColumn = true;
			this.dataGridDetails.FetchRowStyles = true;
			this.dataGridDetails.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridDetails.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridDetails.Images"))));
			this.dataGridDetails.Location = new System.Drawing.Point(0, 0);
			this.dataGridDetails.Name = "dataGridDetails";
			this.dataGridDetails.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridDetails.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridDetails.PreviewInfo.ZoomFactor = 75;
			this.dataGridDetails.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridDetails.PrintInfo.PageSettings")));
			this.dataGridDetails.RowHeight = 15;
			this.dataGridDetails.Size = new System.Drawing.Size(829, 292);
			this.dataGridDetails.TabIndex = 2;
			this.dataGridDetails.Text = "c1TrueDBGrid2";
			this.dataGridDetails.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridDetails_FetchRowStyle);
			this.dataGridDetails.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.dataGridDetails_RowColChange);
			this.dataGridDetails.PropBag = resources.GetString("dataGridDetails.PropBag");
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
			this.splitter1.Location = new System.Drawing.Point(0, 148);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(1082, 4);
			this.splitter1.TabIndex = 13;
			this.splitter1.TabStop = false;
			// 
			// splitter2
			// 
			this.splitter2.Location = new System.Drawing.Point(833, 152);
			this.splitter2.Name = "splitter2";
			this.splitter2.Size = new System.Drawing.Size(4, 296);
			this.splitter2.TabIndex = 14;
			this.splitter2.TabStop = false;
			// 
			// VDPreview
			// 
			this.VDPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.VDPreview.Enabled = true;
			this.VDPreview.Location = new System.Drawing.Point(0, 0);
			this.VDPreview.Name = "VDPreview";
			this.VDPreview.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("VDPreview.OcxState")));
			this.VDPreview.Size = new System.Drawing.Size(241, 292);
			this.VDPreview.TabIndex = 15;
			this.VDPreview.MouseWheelEvent += new AxVDProLib5._DVdrawEvents_MouseWheelEventHandler(this.VDPreview_MouseWheelEvent);
			this.VDPreview.Resize += new System.EventHandler(this.VDPreview_Resize);
			// 
			// panelDetails
			// 
			this.panelDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelDetails.Controls.Add(this.dataGridDetails);
			this.panelDetails.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelDetails.Location = new System.Drawing.Point(0, 152);
			this.panelDetails.Name = "panelDetails";
			this.panelDetails.Size = new System.Drawing.Size(833, 296);
			this.panelDetails.TabIndex = 16;
			// 
			// panelPreview
			// 
			this.panelPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelPreview.Controls.Add(this.VDPreview);
			this.panelPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelPreview.Location = new System.Drawing.Point(837, 152);
			this.panelPreview.Name = "panelPreview";
			this.panelPreview.Size = new System.Drawing.Size(245, 296);
			this.panelPreview.TabIndex = 17;
			// 
			// ReloadOrder
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(1082, 448);
			this.Controls.Add(this.panelInfo);
			this.Controls.Add(this.panelPreview);
			this.Controls.Add(this.splitter2);
			this.Controls.Add(this.panelDetails);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.PanelProject);
			this.Controls.Add(this.toolStrip);
			this.Name = "ReloadOrder";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "ReloadOrder";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReloadOrder_FormClosed);
			this.Load += new System.EventHandler(this.ReloadOrder_Load);
			this.panelInfo.ResumeLayout(false);
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.PanelProject.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridProjects)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableProjects)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetReloadOrder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableDetails)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).EndInit();
			this.panelDetails.ResumeLayout(false);
			this.panelPreview.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Label lblInsert;
        private System.Windows.Forms.Label lblNothing;
        private System.Windows.Forms.Label lblUpdate;
        private System.Windows.Forms.Label lblDelete;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripLabel lblInfo;
        private System.Windows.Forms.ToolStripButton btnConferma;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel PanelProject;
        private System.Data.DataSet dataSetReloadOrder;
        private System.Data.DataTable dataTableProjects;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataTable dataTableDetails;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridProjects;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridDetails;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private AxVDProLib5.AxVDPro VDPreview;
        private System.Windows.Forms.Panel panelDetails;
        private System.Windows.Forms.Panel panelPreview;
        private System.Data.DataColumn dataColumn25;

    }
}