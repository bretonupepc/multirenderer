using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.Customers_Suppliers;
using Breton.TraceLoggers;
using Breton.ImportOffer;

namespace Breton.OfferManager.Form
{
    public partial class ProjectsAdvance : System.Windows.Forms.Form
    {
        #region Variables
        private static bool formCreate;
        private DbInterface mDbInterface;
        private User mUser;
        private string mLanguage;
        private string mOrderCode;
        private ParameterControlPositions mFormPosition;
        private ProjectCollection mProjects;
        private bool mDialog;
        private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;
        
        private Breton.OfferManager.Form.OrdersAdvance.SelectOrderAdvanceEventHandler ordHandler;

        public delegate void SelectProjectAdvanceEventHandler(object sender, string e);		// Gestore dell'evento
        public static event SelectProjectAdvanceEventHandler SelectProjectAdvanceEvent;		// Evento SelectOrderEvent

        #endregion

        public ProjectsAdvance(DbInterface db, User user, string language, string orderCode,bool dialog)
        {
            InitializeComponent();

            if (db == null)
            {
                mDbInterface = new DbInterface();
                mDbInterface.Open("", "DbConnection.udl");
            }
            else
                mDbInterface = db;

            // Carica la posizione del form
            mFormPosition = new ParameterControlPositions(this.Name);
            mFormPosition.LoadFormPosition(this);

            mUser = user;

            mLanguage = language;
            ProjResource.gResource.ChangeLanguage(mLanguage);

            mOrderCode = orderCode;

            mDialog = dialog;

            ordHandler = new OrdersAdvance.SelectOrderAdvanceEventHandler(OrdineSelezionato);
            Breton.OfferManager.Form.OrdersAdvance.SelectOrderAdvanceEvent += ordHandler;
        }

        #region Properties

        public static bool gCreate
        {
            set { formCreate = value; }
            get { return formCreate; }
        }

        #endregion

        #region Form Functions
        private void ProjectsAdvance_Load(object sender, EventArgs e)
        {
            // Carica le lingue del form
            ProjResource.gResource.LoadStringForm(this);

            mProjects = new ProjectCollection(mDbInterface);
            mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridProjects, dataTableProjects);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridProjects);

            if (mProjects.GetDataFromDb(Project.Keys.F_CODE, mOrderCode))
                FillTable();

            gCreate = true;
        }

        private void ProjectsAdvance_FormClosed(object sender, FormClosedEventArgs e)
        {
            mFormPosition.SaveFormPosition(this);
            mSortGrid = null;
			mColumnView = null;
            gCreate = false;

            Breton.OfferManager.Form.OrdersAdvance.SelectOrderAdvanceEvent -= ordHandler;
        }
        #endregion

        #region Gestione tabelle

        private void FillTable()
        {
            Project prj;

            dataTableProjects.Clear();

            // Scorre tutti i progetti letti
            mProjects.MoveFirst();
            while (!mProjects.IsEOF())
            {
                // Legge il progetto attuale
                mProjects.GetObject(out prj);

                dataTableProjects.BeginInit();
                dataTableProjects.BeginLoadData();
                // Aggiunge una riga alla volta
                AddRow(ref prj);

                dataTableProjects.EndInit();
                dataTableProjects.EndLoadData();
            }
        }

        private void AddRow(ref Project prj)
        {
            // Array con i dati della riga
            object[] myArray = new object[9];
            myArray[0] = dataTableProjects.Rows.Count;
            myArray[1] = prj.gId;
            myArray[2] = prj.gCode;
            myArray[3] = prj.gCustomerOrderCode;
            myArray[4] = StateEnumstrToString(prj.gStatePrj);
            myArray[5] = prj.gDate;
			if (prj.gDeliveryDate == DateTime.MaxValue)
				myArray[6] = null;
			else
				myArray[6] = prj.gDeliveryDate;
            myArray[7] = prj.gDescription;
            myArray[8] = prj.gCodeOriginalProject;

            // Crea una nuova riga nella tabella e la riempie di dati
            DataRow r = dataTableProjects.NewRow();
            r.ItemArray = myArray;
            dataTableProjects.Rows.Add(r);

            // Assegna l'id della riga tabella al progetto
            prj.gTableId = int.Parse(r.ItemArray[0].ToString());

            // Passa all'elemento successivo
            mProjects.MoveNext();
        }

        #endregion

        #region ToolBar
        private void toolBar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            Project prj;
            int i = 0;

            switch (toolBar.Buttons.IndexOf(e.Button))
            {
                // Vedi dettagli
                case 0:
                    string projectCode;

                    if (dataGridProjects.Bookmark >= 0)
                        i = (int)dataGridProjects[dataGridProjects.Bookmark, 0];
                    else
                        return;
                    // Carica il codice dell'ordine di cui si vogliono visualizzare i progetti
                    mProjects.GetObjectTable(i, out prj);
                    projectCode = prj.gCode.Trim();

                    if (!DetailsAdvance.gCreate)
                    {
                        DetailsAdvance frmDet = new DetailsAdvance(mDbInterface, mUser, mLanguage, projectCode, mDialog);
                        if (mDialog)
                        {
                            frmDet.ShowInTaskbar = false;
                            frmDet.ShowDialog();
							this.Tag = frmDet.Tag;
							Close();
                        }
                        else
                        {
                            frmDet.MdiParent = this.MdiParent;
                            frmDet.Show();
                        }
                    }
                    else
                    {
                        OnSelectProjectAdvance(projectCode);
                        AttivaForm("DetailsAdvance");
                    }
                    break;

                // Esci
                case 2:
                    Close();
                    break;

                // Refresh
                case 4:
                    this.Cursor = Cursors.WaitCursor;
                    if (mProjects.RefreshData())
                        FillTable();
                    this.Cursor = Cursors.Default;
                    break;
            }
        }
        #endregion

        #region Eventi dei controlli

        private void dataGridProjects_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Project prj;

            int i = (int)dataGridProjects[e.Row, "T_ID"];
            mProjects.GetObjectTable(i, out prj);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (prj.gStatePrj.Trim() == Project.State.PROJ_LOADED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_ANALIZED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_IN_PROGRESS.ToString())
            {
                e.CellStyle.BackColor = Color.Yellow;
                e.CellStyle.BackColor2 = Color.LightYellow;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_COMPLETED.ToString())
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_NDISP.ToString())
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.BackColor2 = Color.Coral;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_RELOADED.ToString())
            {
                e.CellStyle.BackColor = Color.Orange;
                e.CellStyle.BackColor2 = Color.Gold;
            }
        }

        #endregion

        #region Gestione eventi
        //Genera l'evento selectProject
        protected virtual void OnSelectProjectAdvance(string ev)
        {
            if (SelectProjectAdvanceEvent != null)
                SelectProjectAdvanceEvent(this, ev);
        }

        private void OrdineSelezionato(object sender, string ev)
        {
            mOrderCode = ev;

            if (mProjects.GetDataFromDb(Project.Keys.F_CODE, mOrderCode))
            {
                FillTable();
                OnSelectProjectAdvance(null);
            }
        }
        #endregion

        #region Private Methods

        private void AttivaForm(string formName)
        {
            System.Windows.Forms.Form[] frm = new System.Windows.Forms.Form[this.ParentForm.MdiChildren.Length];
            frm = this.ParentForm.MdiChildren;
            for (int i = 0; i < frm.Length; i++)
            {
                if (frm[i].Name == formName)
                    frm[i].Activate();
            }
        }

        private string StateEnumToString(Project.State st)
        {
            switch (st)
            {
                case Project.State.PROJ_LOADED:
                    return ProjResource.gResource.LoadFixString(this, 1);
                case Project.State.PROJ_IN_PROGRESS:
                    return ProjResource.gResource.LoadFixString(this, 2);
                case Project.State.PROJ_COMPLETED:
                    return ProjResource.gResource.LoadFixString(this, 3);
                case Project.State.PROJ_NDISP:
                    return ProjResource.gResource.LoadFixString(this, 4);
                case Project.State.PROJ_ANALIZED:
                    return ProjResource.gResource.LoadFixString(this, 5);
                case Project.State.PROJ_RELOADED:
                    return ProjResource.gResource.LoadFixString(this, 6);
                default:
                    return "";
            }
        }

        private string StateEnumstrToString(string st)
        {
            if (st.Trim() == Project.State.PROJ_LOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 1);
            else if (st.Trim() == Project.State.PROJ_IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 2);
            else if (st.Trim() == Project.State.PROJ_COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 3);
            else if (st.Trim() == Project.State.PROJ_NDISP.ToString())
                return ProjResource.gResource.LoadFixString(this, 4);
            else if (st.Trim() == Project.State.PROJ_ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 5);
            else if (st.Trim() == Project.State.PROJ_RELOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 6);
            else
                return "";
        }

        private int StateEnumstrToInt(string st)
        {
            if (st.Trim() == Project.State.PROJ_LOADED.ToString())
                return 0;
            else if (st.Trim() == Project.State.PROJ_IN_PROGRESS.ToString())
                return 1;
            else if (st.Trim() == Project.State.PROJ_COMPLETED.ToString())
                return 2;
            else if (st.Trim() == Project.State.PROJ_NDISP.ToString())
                return 3;
            else if (st.Trim() == Project.State.PROJ_ANALIZED.ToString())
                return 4;
            else if (st.Trim() == Project.State.PROJ_RELOADED.ToString())
                return 5;
            else
                return -1;
        }

        private Project.State StateStringToEnum(string state)
        {
            if (state == ProjResource.gResource.LoadFixString(this, 1))
                return Project.State.PROJ_LOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 2))
                return Project.State.PROJ_IN_PROGRESS;
            else if (state == ProjResource.gResource.LoadFixString(this, 3))
                return Project.State.PROJ_COMPLETED;
            else if (state == ProjResource.gResource.LoadFixString(this, 4))
                return Project.State.PROJ_NDISP;
            else if (state == ProjResource.gResource.LoadFixString(this, 5))
                return Project.State.PROJ_ANALIZED;
            else if (state == ProjResource.gResource.LoadFixString(this, 6))
                return Project.State.PROJ_RELOADED;
            else
                return Project.State.NONE;
        }
        #endregion   
    }
}