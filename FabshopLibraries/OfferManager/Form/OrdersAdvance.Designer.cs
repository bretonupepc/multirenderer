namespace Breton.OfferManager.Form
{
    partial class OrdersAdvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrdersAdvance));
			this.dataSetOrders = new System.Data.DataSet();
			this.dataTableOrders = new System.Data.DataTable();
			this.orderT_ID = new System.Data.DataColumn();
			this.orderF_ID = new System.Data.DataColumn();
			this.orderF_CODE = new System.Data.DataColumn();
			this.orderF_DATE = new System.Data.DataColumn();
			this.orderF_DELIVERY_DATE = new System.Data.DataColumn();
			this.orderF_DESCRIPTION = new System.Data.DataColumn();
			this.orderF_CUSTOMER_CODE = new System.Data.DataColumn();
			this.orderF_OFFER_CODE = new System.Data.DataColumn();
			this.orderF_STATE = new System.Data.DataColumn();
			this.orderF_CUSTOMER_NAME = new System.Data.DataColumn();
			this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
			this.toolBar = new System.Windows.Forms.ToolBar();
			this.btnFilter = new System.Windows.Forms.ToolBarButton();
			this.btnProgetti = new System.Windows.Forms.ToolBarButton();
			this.btnCloseOrder = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.btnAnnulla = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
			this.btnRefresh = new System.Windows.Forms.ToolBarButton();
			this.panelFilter = new System.Windows.Forms.Panel();
			this.btnChiudi = new System.Windows.Forms.Button();
			this.deliveryDateTo = new System.Windows.Forms.DateTimePicker();
			this.insertDateTo = new System.Windows.Forms.DateTimePicker();
			this.txtCode = new System.Windows.Forms.TextBox();
			this.lblInsertDate = new System.Windows.Forms.Label();
			this.insertDateFrom = new System.Windows.Forms.DateTimePicker();
			this.lblCode = new System.Windows.Forms.Label();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnFiltra = new System.Windows.Forms.Button();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.cmbxState = new System.Windows.Forms.ComboBox();
			this.lblState = new System.Windows.Forms.Label();
			this.cmbxCustomerName = new System.Windows.Forms.ComboBox();
			this.lblDeliveryDate = new System.Windows.Forms.Label();
			this.lblCustomerName = new System.Windows.Forms.Label();
			this.lblCustomerCode = new System.Windows.Forms.Label();
			this.cmbxCustomerCode = new System.Windows.Forms.ComboBox();
			this.deliveryDateFrom = new System.Windows.Forms.DateTimePicker();
			this.dataGridOrders = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.btnSelectOrder = new System.Windows.Forms.ToolBarButton();
			((System.ComponentModel.ISupportInitialize)(this.dataSetOrders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableOrders)).BeginInit();
			this.panelFilter.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridOrders)).BeginInit();
			this.SuspendLayout();
			// 
			// dataSetOrders
			// 
			this.dataSetOrders.DataSetName = "NewDataSet";
			this.dataSetOrders.Locale = new System.Globalization.CultureInfo("en-US");
			this.dataSetOrders.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableOrders});
			// 
			// dataTableOrders
			// 
			this.dataTableOrders.Columns.AddRange(new System.Data.DataColumn[] {
            this.orderT_ID,
            this.orderF_ID,
            this.orderF_CODE,
            this.orderF_DATE,
            this.orderF_DELIVERY_DATE,
            this.orderF_DESCRIPTION,
            this.orderF_CUSTOMER_CODE,
            this.orderF_OFFER_CODE,
            this.orderF_STATE,
            this.orderF_CUSTOMER_NAME});
			this.dataTableOrders.TableName = "tableOrders";
			// 
			// orderT_ID
			// 
			this.orderT_ID.ColumnName = "T_ID";
			this.orderT_ID.DataType = typeof(int);
			// 
			// orderF_ID
			// 
			this.orderF_ID.Caption = "Id";
			this.orderF_ID.ColumnName = "F_ID";
			this.orderF_ID.DataType = typeof(int);
			// 
			// orderF_CODE
			// 
			this.orderF_CODE.Caption = "Codice";
			this.orderF_CODE.ColumnName = "F_CODE";
			// 
			// orderF_DATE
			// 
			this.orderF_DATE.Caption = "Data";
			this.orderF_DATE.ColumnName = "F_DATE";
			this.orderF_DATE.DataType = typeof(System.DateTime);
			// 
			// orderF_DELIVERY_DATE
			// 
			this.orderF_DELIVERY_DATE.Caption = "Data consegna";
			this.orderF_DELIVERY_DATE.ColumnName = "F_DELIVERY_DATE";
			this.orderF_DELIVERY_DATE.DataType = typeof(System.DateTime);
			// 
			// orderF_DESCRIPTION
			// 
			this.orderF_DESCRIPTION.Caption = "Descrizione";
			this.orderF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
			// 
			// orderF_CUSTOMER_CODE
			// 
			this.orderF_CUSTOMER_CODE.Caption = "Codice Cliente";
			this.orderF_CUSTOMER_CODE.ColumnName = "F_CUSTOMER_CODE";
			// 
			// orderF_OFFER_CODE
			// 
			this.orderF_OFFER_CODE.Caption = "Codice offerta";
			this.orderF_OFFER_CODE.ColumnName = "F_OFFER_CODE";
			// 
			// orderF_STATE
			// 
			this.orderF_STATE.Caption = "Stato";
			this.orderF_STATE.ColumnName = "F_STATE";
			// 
			// orderF_CUSTOMER_NAME
			// 
			this.orderF_CUSTOMER_NAME.Caption = "Nome cliente";
			this.orderF_CUSTOMER_NAME.ColumnName = "F_CUSTOMER_NAME";
			// 
			// imlToolIcon
			// 
			this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
			this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon.Images.SetKeyName(0, "");
			this.imlToolIcon.Images.SetKeyName(1, "");
			this.imlToolIcon.Images.SetKeyName(2, "");
			this.imlToolIcon.Images.SetKeyName(3, "");
			this.imlToolIcon.Images.SetKeyName(4, "");
			this.imlToolIcon.Images.SetKeyName(5, "");
			this.imlToolIcon.Images.SetKeyName(6, "");
			this.imlToolIcon.Images.SetKeyName(7, "");
			this.imlToolIcon.Images.SetKeyName(8, "");
			this.imlToolIcon.Images.SetKeyName(9, "Filter.ico");
			this.imlToolIcon.Images.SetKeyName(10, "CloseOrder.ico");
			this.imlToolIcon.Images.SetKeyName(11, "Refresh.ico");
			this.imlToolIcon.Images.SetKeyName(12, "SelectOrder.ico");
			// 
			// toolBar
			// 
			this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnFilter,
            this.btnProgetti,
            this.btnCloseOrder,
            this.toolBarButton1,
            this.btnAnnulla,
            this.toolBarButton2,
            this.btnRefresh,
            this.btnSelectOrder});
			this.toolBar.DropDownArrows = true;
			this.toolBar.ImageList = this.imlToolIcon;
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.ShowToolTips = true;
			this.toolBar.Size = new System.Drawing.Size(686, 44);
			this.toolBar.TabIndex = 3;
			this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
			// 
			// btnFilter
			// 
			this.btnFilter.ImageIndex = 9;
			this.btnFilter.Name = "btnFilter";
			// 
			// btnProgetti
			// 
			this.btnProgetti.ImageIndex = 7;
			this.btnProgetti.Name = "btnProgetti";
			// 
			// btnCloseOrder
			// 
			this.btnCloseOrder.ImageIndex = 10;
			this.btnCloseOrder.Name = "btnCloseOrder";
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.Name = "toolBarButton1";
			this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnAnnulla
			// 
			this.btnAnnulla.ImageIndex = 6;
			this.btnAnnulla.Name = "btnAnnulla";
			this.btnAnnulla.ToolTipText = "Esci";
			// 
			// toolBarButton2
			// 
			this.toolBarButton2.Name = "toolBarButton2";
			this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnRefresh
			// 
			this.btnRefresh.ImageIndex = 11;
			this.btnRefresh.Name = "btnRefresh";
			// 
			// panelFilter
			// 
			this.panelFilter.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelFilter.Controls.Add(this.btnChiudi);
			this.panelFilter.Controls.Add(this.deliveryDateTo);
			this.panelFilter.Controls.Add(this.insertDateTo);
			this.panelFilter.Controls.Add(this.txtCode);
			this.panelFilter.Controls.Add(this.lblInsertDate);
			this.panelFilter.Controls.Add(this.insertDateFrom);
			this.panelFilter.Controls.Add(this.lblCode);
			this.panelFilter.Controls.Add(this.btnReset);
			this.panelFilter.Controls.Add(this.btnFiltra);
			this.panelFilter.Controls.Add(this.txtDescription);
			this.panelFilter.Controls.Add(this.lblDescription);
			this.panelFilter.Controls.Add(this.cmbxState);
			this.panelFilter.Controls.Add(this.lblState);
			this.panelFilter.Controls.Add(this.cmbxCustomerName);
			this.panelFilter.Controls.Add(this.lblDeliveryDate);
			this.panelFilter.Controls.Add(this.lblCustomerName);
			this.panelFilter.Controls.Add(this.lblCustomerCode);
			this.panelFilter.Controls.Add(this.cmbxCustomerCode);
			this.panelFilter.Controls.Add(this.deliveryDateFrom);
			this.panelFilter.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelFilter.Location = new System.Drawing.Point(0, 44);
			this.panelFilter.Name = "panelFilter";
			this.panelFilter.Size = new System.Drawing.Size(686, 116);
			this.panelFilter.TabIndex = 5;
			this.panelFilter.Visible = false;
			// 
			// btnChiudi
			// 
			this.btnChiudi.Image = ((System.Drawing.Image)(resources.GetObject("btnChiudi.Image")));
			this.btnChiudi.Location = new System.Drawing.Point(624, 70);
			this.btnChiudi.Name = "btnChiudi";
			this.btnChiudi.Size = new System.Drawing.Size(40, 40);
			this.btnChiudi.TabIndex = 120;
			this.btnChiudi.Click += new System.EventHandler(this.btnChiudi_Click);
			// 
			// deliveryDateTo
			// 
			this.deliveryDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.deliveryDateTo.Location = new System.Drawing.Point(562, 24);
			this.deliveryDateTo.Name = "deliveryDateTo";
			this.deliveryDateTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.deliveryDateTo.Size = new System.Drawing.Size(100, 20);
			this.deliveryDateTo.TabIndex = 119;
			this.deliveryDateTo.ValueChanged += new System.EventHandler(this.deliveryDateTo_ValueChanged);
			// 
			// insertDateTo
			// 
			this.insertDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.insertDateTo.Location = new System.Drawing.Point(562, 3);
			this.insertDateTo.Name = "insertDateTo";
			this.insertDateTo.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.insertDateTo.Size = new System.Drawing.Size(100, 20);
			this.insertDateTo.TabIndex = 118;
			this.insertDateTo.ValueChanged += new System.EventHandler(this.insertDateTo_ValueChanged);
			// 
			// txtCode
			// 
			this.txtCode.Location = new System.Drawing.Point(128, 3);
			this.txtCode.Name = "txtCode";
			this.txtCode.Size = new System.Drawing.Size(192, 20);
			this.txtCode.TabIndex = 117;
			// 
			// lblInsertDate
			// 
			this.lblInsertDate.AutoSize = true;
			this.lblInsertDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblInsertDate.Location = new System.Drawing.Point(330, 3);
			this.lblInsertDate.Name = "lblInsertDate";
			this.lblInsertDate.Size = new System.Drawing.Size(114, 15);
			this.lblInsertDate.TabIndex = 115;
			this.lblInsertDate.Text = "Data di inserimento";
			// 
			// insertDateFrom
			// 
			this.insertDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.insertDateFrom.Location = new System.Drawing.Point(455, 3);
			this.insertDateFrom.Name = "insertDateFrom";
			this.insertDateFrom.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.insertDateFrom.Size = new System.Drawing.Size(100, 20);
			this.insertDateFrom.TabIndex = 116;
			this.insertDateFrom.ValueChanged += new System.EventHandler(this.insertDateFrom_ValueChanged);
			// 
			// lblCode
			// 
			this.lblCode.AutoSize = true;
			this.lblCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCode.Location = new System.Drawing.Point(8, 3);
			this.lblCode.Name = "lblCode";
			this.lblCode.Size = new System.Drawing.Size(45, 15);
			this.lblCode.TabIndex = 114;
			this.lblCode.Text = "Codice";
			// 
			// btnReset
			// 
			this.btnReset.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnReset.Image = ((System.Drawing.Image)(resources.GetObject("btnReset.Image")));
			this.btnReset.Location = new System.Drawing.Point(538, 70);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(40, 40);
			this.btnReset.TabIndex = 111;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnFiltra
			// 
			this.btnFiltra.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltra.Image")));
			this.btnFiltra.Location = new System.Drawing.Point(581, 70);
			this.btnFiltra.Name = "btnFiltra";
			this.btnFiltra.Size = new System.Drawing.Size(40, 40);
			this.btnFiltra.TabIndex = 112;
			this.btnFiltra.Click += new System.EventHandler(this.btnFiltra_Click);
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(455, 47);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(207, 20);
			this.txtDescription.TabIndex = 107;
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDescription.Location = new System.Drawing.Point(330, 47);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 15);
			this.lblDescription.TabIndex = 106;
			this.lblDescription.Text = "Descrizione";
			// 
			// cmbxState
			// 
			this.cmbxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbxState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxState.Location = new System.Drawing.Point(128, 72);
			this.cmbxState.Name = "cmbxState";
			this.cmbxState.Size = new System.Drawing.Size(192, 21);
			this.cmbxState.TabIndex = 105;
			// 
			// lblState
			// 
			this.lblState.AutoSize = true;
			this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblState.Location = new System.Drawing.Point(8, 72);
			this.lblState.Name = "lblState";
			this.lblState.Size = new System.Drawing.Size(35, 15);
			this.lblState.TabIndex = 104;
			this.lblState.Text = "Stato";
			// 
			// cmbxCustomerName
			// 
			this.cmbxCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxCustomerName.Location = new System.Drawing.Point(128, 49);
			this.cmbxCustomerName.Name = "cmbxCustomerName";
			this.cmbxCustomerName.Size = new System.Drawing.Size(192, 21);
			this.cmbxCustomerName.TabIndex = 102;
			// 
			// lblDeliveryDate
			// 
			this.lblDeliveryDate.AutoSize = true;
			this.lblDeliveryDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDeliveryDate.Location = new System.Drawing.Point(330, 25);
			this.lblDeliveryDate.Name = "lblDeliveryDate";
			this.lblDeliveryDate.Size = new System.Drawing.Size(103, 15);
			this.lblDeliveryDate.TabIndex = 101;
			this.lblDeliveryDate.Text = "Data di consegna";
			// 
			// lblCustomerName
			// 
			this.lblCustomerName.AutoSize = true;
			this.lblCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCustomerName.Location = new System.Drawing.Point(8, 49);
			this.lblCustomerName.Name = "lblCustomerName";
			this.lblCustomerName.Size = new System.Drawing.Size(80, 15);
			this.lblCustomerName.TabIndex = 100;
			this.lblCustomerName.Text = "Nome cliente";
			// 
			// lblCustomerCode
			// 
			this.lblCustomerCode.AutoSize = true;
			this.lblCustomerCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCustomerCode.Location = new System.Drawing.Point(8, 26);
			this.lblCustomerCode.Name = "lblCustomerCode";
			this.lblCustomerCode.Size = new System.Drawing.Size(84, 15);
			this.lblCustomerCode.TabIndex = 99;
			this.lblCustomerCode.Text = "Codice cliente";
			// 
			// cmbxCustomerCode
			// 
			this.cmbxCustomerCode.DisplayMember = "F_CODE";
			this.cmbxCustomerCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxCustomerCode.Location = new System.Drawing.Point(128, 26);
			this.cmbxCustomerCode.Name = "cmbxCustomerCode";
			this.cmbxCustomerCode.Size = new System.Drawing.Size(192, 21);
			this.cmbxCustomerCode.TabIndex = 96;
			// 
			// deliveryDateFrom
			// 
			this.deliveryDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.deliveryDateFrom.Location = new System.Drawing.Point(455, 24);
			this.deliveryDateFrom.Name = "deliveryDateFrom";
			this.deliveryDateFrom.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.deliveryDateFrom.Size = new System.Drawing.Size(100, 20);
			this.deliveryDateFrom.TabIndex = 103;
			this.deliveryDateFrom.ValueChanged += new System.EventHandler(this.deliveryDateFrom_ValueChanged);
			// 
			// dataGridOrders
			// 
			this.dataGridOrders.AllowFilter = false;
			this.dataGridOrders.AllowHorizontalSplit = true;
			this.dataGridOrders.AllowSort = false;
			this.dataGridOrders.AllowUpdate = false;
			this.dataGridOrders.AllowVerticalSplit = true;
			this.dataGridOrders.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dataGridOrders.CaptionHeight = 18;
			this.dataGridOrders.Cursor = System.Windows.Forms.Cursors.Default;
			this.dataGridOrders.DataSource = this.dataTableOrders;
			this.dataGridOrders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridOrders.FetchRowStyles = true;
			this.dataGridOrders.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridOrders.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridOrders.Images"))));
			this.dataGridOrders.Location = new System.Drawing.Point(0, 160);
			this.dataGridOrders.MaintainRowCurrency = true;
			this.dataGridOrders.Name = "dataGridOrders";
			this.dataGridOrders.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridOrders.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridOrders.PreviewInfo.ZoomFactor = 75;
			this.dataGridOrders.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridOrders.PrintInfo.PageSettings")));
			this.dataGridOrders.RowHeight = 15;
			this.dataGridOrders.Size = new System.Drawing.Size(686, 279);
			this.dataGridOrders.TabIndex = 6;
			this.dataGridOrders.TabStop = false;
			this.dataGridOrders.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridOrders_FetchRowStyle);
			this.dataGridOrders.PropBag = resources.GetString("dataGridOrders.PropBag");
			// 
			// btnSelectOrder
			// 
			this.btnSelectOrder.ImageIndex = 12;
			this.btnSelectOrder.Name = "btnSelectOrder";
			this.btnSelectOrder.Visible = false;
			// 
			// OrdersAdvance
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(686, 439);
			this.Controls.Add(this.dataGridOrders);
			this.Controls.Add(this.panelFilter);
			this.Controls.Add(this.toolBar);
			this.Name = "OrdersAdvance";
			this.Text = "Ordini - Avanzamento della produzione";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrdersAdvance_FormClosed);
			this.Load += new System.EventHandler(this.OrdersAdvance_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataSetOrders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableOrders)).EndInit();
			this.panelFilter.ResumeLayout(false);
			this.panelFilter.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridOrders)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Data.DataSet dataSetOrders;
        private System.Data.DataTable dataTableOrders;
        private System.Data.DataColumn orderT_ID;
        private System.Data.DataColumn orderF_ID;
        private System.Data.DataColumn orderF_CODE;
        private System.Data.DataColumn orderF_DATE;
        private System.Data.DataColumn orderF_DELIVERY_DATE;
        private System.Data.DataColumn orderF_DESCRIPTION;
        private System.Data.DataColumn orderF_CUSTOMER_CODE;
        private System.Data.DataColumn orderF_OFFER_CODE;
        private System.Data.DataColumn orderF_STATE;
        private System.Data.DataColumn orderF_CUSTOMER_NAME;
        private System.Windows.Forms.ImageList imlToolIcon;
        private System.Windows.Forms.ToolBar toolBar;
        private System.Windows.Forms.ToolBarButton btnAnnulla;
        private System.Windows.Forms.ToolBarButton toolBarButton1;
        private System.Windows.Forms.ToolBarButton btnProgetti;
        private System.Windows.Forms.Panel panelFilter;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.ComboBox cmbxState;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.ComboBox cmbxCustomerName;
        private System.Windows.Forms.Label lblDeliveryDate;
        private System.Windows.Forms.Label lblCustomerName;
        private System.Windows.Forms.Label lblCustomerCode;
        private System.Windows.Forms.ComboBox cmbxCustomerCode;
        private System.Windows.Forms.DateTimePicker deliveryDateFrom;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridOrders;
        private System.Windows.Forms.ToolBarButton btnFilter;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnFiltra;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Label lblInsertDate;
        private System.Windows.Forms.DateTimePicker insertDateFrom;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.DateTimePicker deliveryDateTo;
        private System.Windows.Forms.DateTimePicker insertDateTo;
        private System.Windows.Forms.Button btnChiudi;
        private System.Windows.Forms.ToolBarButton btnCloseOrder;
        private System.Windows.Forms.ToolBarButton toolBarButton2;
        private System.Windows.Forms.ToolBarButton btnRefresh;
		private System.Windows.Forms.ToolBarButton btnSelectOrder;
    }
}