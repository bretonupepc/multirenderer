using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.TraceLoggers;
using Breton.Materials;
using Breton.Phases;
using Breton.TechnoMaster;
using Breton.TechnoMaster.Form;
using Breton.ImportOffer;
using Breton.DesignUtil;
using Breton.KeyboardInterface;
using VDProLib5;

namespace Breton.OfferManager.Form
{
	public class Details : System.Windows.Forms.Form
	{
		#region Variables
		private System.Windows.Forms.ImageList imlToolIcon;
		private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridDetails;
		private System.Windows.Forms.Panel panelEdit;
		private System.Windows.Forms.Label lblOperation;
		private System.Windows.Forms.Button btnNascondi;
		private System.Windows.Forms.Button btnConferma;
		private System.Windows.Forms.ComboBox cmbxState;
		private System.Windows.Forms.Label lblState;
		private System.Windows.Forms.ToolBar toolBar;
		private System.Windows.Forms.ToolBarButton btnNuovo;
		private System.Windows.Forms.ToolBarButton btnImporta;
		private System.Windows.Forms.ToolBarButton btnModifica;
		private System.Windows.Forms.ToolBarButton btnSalva;
		private System.Windows.Forms.ToolBarButton btnElimina;
		private System.Windows.Forms.ToolBarButton btnReset;
		private System.Windows.Forms.ToolBarButton btnAnnulla;
		private System.Data.DataSet dataSetDetails;
		private System.Data.DataTable dataTableDetails;
		private System.Data.DataColumn dataColumnT_ID;
		private System.Data.DataColumn dataColumnF_ID;
		private System.Data.DataColumn dataColumnF_CODE;
		private System.Data.DataColumn dataColumnF_CUSTOMER_PROJECT_CODE;
		private System.Data.DataColumn dataColumnF_MATERIAL_CODE;
		private System.Data.DataColumn dataColumnF_MATERIAL_DESCRIPTION;
		private System.Data.DataColumn dataColumnF_STATE;
		private System.Data.DataColumn dataColumnF_THICKNESS;
		private System.Data.DataColumn dataColumnF_ARTICLE_CODE;
		private System.Data.DataColumn dataColumnF_CODE_ORIGINALE_SHAPE;
        private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.ToolBarButton btnProcess; 
        private Panel panelData;
        private Panel panelPreview;
        private Splitter splitter;
        private Label lblShapeInfo;
        private DataColumn dataColumnF_DESCRIPTION; 
        private TextBox txtDescription;
        private Label lblDescription;
        private AxVDProLib5.AxVDPro VDPreview;
        private ToolBarButton toolBarButton2;
        private ToolBarButton btnRefresh;
        private Panel panelProdProcess;
        private Button btnNascondiSimpleTecno;
        private Button btnOkSimpleTecno;
        private Label lblProdProc;
        private C1.Data.C1DataSet c1DataSetProdProcess;
        private ToolBarButton btnSimpleTecno;
        private C1.Win.C1List.C1Combo c1ProdProcess;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1WorkCenterGrid;
        private DataTable dataTableWorkGroup;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn13;
        private DataColumn dataColumn17;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1WorkingGrid;
        private DataTable dataTableWorking;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn6;
        private DataColumn dataColumn15;
        private DataSet DataSetProdProcess;
        private DataTable dataTablePhase;
        private DataColumn dataColumn9;
        private DataColumn dataColumn10;
        private DataColumn dataColumn16;
        private DataTable dataTableMachine;
        private DataColumn dataColumn11;
        private DataColumn dataColumn12;
        private DataColumn dataColumn14;
        private DataColumn dataColumn18;
        private DataColumn dataColumnF_FINAL_THICKNESS;
        private ContextMenuStrip contextMenuDraw;
        private ToolStripMenuItem miProject;
        private ToolStripMenuItem miZoomAll;
        private ToolStripMenuItem miZoom;
        private ToolStripMenuItem mi10;
        private ToolStripMenuItem mi50;
        private ToolStripMenuItem mi100;
        private ToolStripMenuItem mi150;
        private ToolStripMenuItem mi200;
        private ToolStripMenuItem mi300;
        private ToolStripMenuItem miLock;
        private DataColumn dataColumnF_LENGTH;
        private DataColumn dataColumnF_WIDTH;
        private ToolStripMenuItem miQuote;
        private ToolStripMenuItem miLinearQuote;
        private ToolStripMenuItem miRadiusQuote;
        private ToolStripMenuItem miAngleQuote;
        private ToolStripMenuItem miAutoQuote;
        private StatusStrip statusStrip;
        private ToolStripStatusLabel tsslblOrtho;
        private ToolStripStatusLabel tsslOperation;
		private ToolStripMenuItem miReset;
		private ToolStripMenuItem miDesignQuote;
		private System.ComponentModel.IContainer components;

		private static bool formCreate;
		private bool isModified = false;
		private DetailCollection mDetails;
		private MaterialCollection mMaterials;
        private WorkPhaseCollection mWorkPhases;
        private WorkOrderCollection mWorkOrders;
        private UsedObjectCollection mUsedObjects;
		private DbInterface mDbInterface;
		private User mUser;
		private string mLanguage;
		private string Edit;
		private string mProjectCode;
		private ParameterControlPositions mFormPosition;
        private ParameterCollection mParam = null, mParamApp = null;
		private DetailOperation Operat;
		private DirectoryInfo dirXml;
		private DirectoryInfo dirXmlDet;
        private TecnoOperation mTecnoOp;
		private string tmpXmlDir;// = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
		private string tmpXmlDetDir; // = tmpXmlDir + "\\Detail";
        private string mFileXml;
        private Painter mDraw;
        private FieldCollection mImpost = null;
        private Field fGridThickness, fGridFinalThickness, fGridLength, fGridWidth;
        private bool imperial = false;				// Sistema in pollici o mm
        private string[] allPiece;
        private bool coord_assolute;
        private bool mProjectDraw;
        private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;
        private int clickNumber = 0;
		private ThreadTecno mTecno;
		private bool locked;

        private EventHandler tecnoHandler;
        private Breton.OfferManager.Form.Projects.SelectProjectEventHandler prjHandler;
		private ProgressBar progressBar;
		private Timer timerTecno;
        private Breton.OfferManager.Form.Orders.SelectOrderEventHandler ordHandler;
        public static event EventHandler UpdateDetail;

		public enum DetailOperation
		{
			None,
			New,
			Edit,
			Save,
			Delete
		}

		#endregion 

		public Details(DbInterface db, User user, string language, string projectCode)
		{
			InitializeComponent();

			if (db == null)
			{
				mDbInterface = new DbInterface();
				mDbInterface.Open("", "DbConnection.udl");
			}
			else
				mDbInterface = db;

			// Carica la posizione del form
			mFormPosition = new ParameterControlPositions(this.Name);
			mFormPosition.LoadFormPosition(this);

			mUser = user;

			mLanguage = language;
			ProjResource.gResource.ChangeLanguage(mLanguage);

			mProjectCode = projectCode;

            // evento esecuzione tecno
            tecnoHandler = new EventHandler(TecnoExec);
            Breton.TechnoMaster.Form.Working.TecnoExecute += tecnoHandler;
			// Crea il delegato per la gestione del'evento SelectProject
            prjHandler = new Breton.OfferManager.Form.Projects.SelectProjectEventHandler(ProgettoSelezionato);
            Breton.OfferManager.Form.Projects.SelectProjectEvent += prjHandler;

            // Crea il delegato per la gestione del'evento SelectOrder
            ordHandler = new Breton.OfferManager.Form.Orders.SelectOrderEventHandler(OrdineSelezionato);
            Breton.OfferManager.Form.Orders.SelectOrderEvent += ordHandler;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Details));
			this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
			this.dataGridDetails = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.dataTableDetails = new System.Data.DataTable();
			this.dataColumnT_ID = new System.Data.DataColumn();
			this.dataColumnF_ID = new System.Data.DataColumn();
			this.dataColumnF_CODE = new System.Data.DataColumn();
			this.dataColumnF_CUSTOMER_PROJECT_CODE = new System.Data.DataColumn();
			this.dataColumnF_MATERIAL_CODE = new System.Data.DataColumn();
			this.dataColumnF_MATERIAL_DESCRIPTION = new System.Data.DataColumn();
			this.dataColumnF_STATE = new System.Data.DataColumn();
			this.dataColumnF_THICKNESS = new System.Data.DataColumn();
			this.dataColumnF_ARTICLE_CODE = new System.Data.DataColumn();
			this.dataColumnF_CODE_ORIGINALE_SHAPE = new System.Data.DataColumn();
			this.dataColumnF_DESCRIPTION = new System.Data.DataColumn();
			this.dataColumnF_FINAL_THICKNESS = new System.Data.DataColumn();
			this.dataColumnF_LENGTH = new System.Data.DataColumn();
			this.dataColumnF_WIDTH = new System.Data.DataColumn();
			this.panelEdit = new System.Windows.Forms.Panel();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.lblOperation = new System.Windows.Forms.Label();
			this.btnNascondi = new System.Windows.Forms.Button();
			this.btnConferma = new System.Windows.Forms.Button();
			this.cmbxState = new System.Windows.Forms.ComboBox();
			this.lblState = new System.Windows.Forms.Label();
			this.toolBar = new System.Windows.Forms.ToolBar();
			this.btnNuovo = new System.Windows.Forms.ToolBarButton();
			this.btnImporta = new System.Windows.Forms.ToolBarButton();
			this.btnModifica = new System.Windows.Forms.ToolBarButton();
			this.btnSalva = new System.Windows.Forms.ToolBarButton();
			this.btnElimina = new System.Windows.Forms.ToolBarButton();
			this.btnReset = new System.Windows.Forms.ToolBarButton();
			this.btnAnnulla = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.btnProcess = new System.Windows.Forms.ToolBarButton();
			this.btnSimpleTecno = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
			this.btnRefresh = new System.Windows.Forms.ToolBarButton();
			this.dataSetDetails = new System.Data.DataSet();
			this.panelData = new System.Windows.Forms.Panel();
			this.panelPreview = new System.Windows.Forms.Panel();
			this.contextMenuDraw = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.miProject = new System.Windows.Forms.ToolStripMenuItem();
			this.miZoomAll = new System.Windows.Forms.ToolStripMenuItem();
			this.miLock = new System.Windows.Forms.ToolStripMenuItem();
			this.miZoom = new System.Windows.Forms.ToolStripMenuItem();
			this.mi10 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi50 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi100 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi150 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi200 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi300 = new System.Windows.Forms.ToolStripMenuItem();
			this.miQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miLinearQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miRadiusQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miAngleQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miAutoQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miDesignQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miReset = new System.Windows.Forms.ToolStripMenuItem();
			this.VDPreview = new AxVDProLib5.AxVDPro();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.tsslblOrtho = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslOperation = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblShapeInfo = new System.Windows.Forms.Label();
			this.splitter = new System.Windows.Forms.Splitter();
			this.panelProdProcess = new System.Windows.Forms.Panel();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.c1WorkCenterGrid = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.dataTableWorkGroup = new System.Data.DataTable();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.dataColumn13 = new System.Data.DataColumn();
			this.dataColumn17 = new System.Data.DataColumn();
			this.c1WorkingGrid = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.dataTableWorking = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn15 = new System.Data.DataColumn();
			this.c1ProdProcess = new C1.Win.C1List.C1Combo();
			this.c1DataSetProdProcess = new C1.Data.C1DataSet();
			this.btnNascondiSimpleTecno = new System.Windows.Forms.Button();
			this.btnOkSimpleTecno = new System.Windows.Forms.Button();
			this.lblProdProc = new System.Windows.Forms.Label();
			this.DataSetProdProcess = new System.Data.DataSet();
			this.dataTablePhase = new System.Data.DataTable();
			this.dataColumn9 = new System.Data.DataColumn();
			this.dataColumn10 = new System.Data.DataColumn();
			this.dataColumn16 = new System.Data.DataColumn();
			this.dataTableMachine = new System.Data.DataTable();
			this.dataColumn11 = new System.Data.DataColumn();
			this.dataColumn12 = new System.Data.DataColumn();
			this.dataColumn14 = new System.Data.DataColumn();
			this.dataColumn18 = new System.Data.DataColumn();
			this.timerTecno = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableDetails)).BeginInit();
			this.panelEdit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataSetDetails)).BeginInit();
			this.panelData.SuspendLayout();
			this.panelPreview.SuspendLayout();
			this.contextMenuDraw.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).BeginInit();
			this.statusStrip.SuspendLayout();
			this.panelProdProcess.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkCenterGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorkGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkingGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorking)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1ProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1DataSetProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DataSetProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTablePhase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableMachine)).BeginInit();
			this.SuspendLayout();
			// 
			// imlToolIcon
			// 
			this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
			this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon.Images.SetKeyName(0, "");
			this.imlToolIcon.Images.SetKeyName(1, "");
			this.imlToolIcon.Images.SetKeyName(2, "");
			this.imlToolIcon.Images.SetKeyName(3, "");
			this.imlToolIcon.Images.SetKeyName(4, "DeleteTecno.ico");
			this.imlToolIcon.Images.SetKeyName(5, "");
			this.imlToolIcon.Images.SetKeyName(6, "");
			this.imlToolIcon.Images.SetKeyName(7, "TecnoProcess.ico");
			this.imlToolIcon.Images.SetKeyName(8, "Process.ico");
			this.imlToolIcon.Images.SetKeyName(9, "Refresh.ico");
			// 
			// dataGridDetails
			// 
			this.dataGridDetails.AllowSort = false;
			this.dataGridDetails.AllowUpdate = false;
			this.dataGridDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridDetails.CaptionHeight = 17;
			this.dataGridDetails.DataSource = this.dataTableDetails;
			this.dataGridDetails.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridDetails.FetchRowStyles = true;
			this.dataGridDetails.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridDetails.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridDetails.Images"))));
			this.dataGridDetails.Location = new System.Drawing.Point(0, 0);
			this.dataGridDetails.MaintainRowCurrency = true;
			this.dataGridDetails.Name = "dataGridDetails";
			this.dataGridDetails.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridDetails.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridDetails.PreviewInfo.ZoomFactor = 75;
			this.dataGridDetails.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridDetails.PrintInfo.PageSettings")));
			this.dataGridDetails.RowHeight = 15;
			this.dataGridDetails.Size = new System.Drawing.Size(372, 279);
			this.dataGridDetails.TabIndex = 7;
			this.dataGridDetails.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridDetails_FetchRowStyle);
			this.dataGridDetails.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.dataGridDetails_RowColChange);
			this.dataGridDetails.PropBag = resources.GetString("dataGridDetails.PropBag");
			// 
			// dataTableDetails
			// 
			this.dataTableDetails.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnT_ID,
            this.dataColumnF_ID,
            this.dataColumnF_CODE,
            this.dataColumnF_CUSTOMER_PROJECT_CODE,
            this.dataColumnF_MATERIAL_CODE,
            this.dataColumnF_MATERIAL_DESCRIPTION,
            this.dataColumnF_STATE,
            this.dataColumnF_THICKNESS,
            this.dataColumnF_ARTICLE_CODE,
            this.dataColumnF_CODE_ORIGINALE_SHAPE,
            this.dataColumnF_DESCRIPTION,
            this.dataColumnF_FINAL_THICKNESS,
            this.dataColumnF_LENGTH,
            this.dataColumnF_WIDTH});
			this.dataTableDetails.TableName = "TableDetails";
			// 
			// dataColumnT_ID
			// 
			this.dataColumnT_ID.ColumnName = "T_ID";
			this.dataColumnT_ID.DataType = typeof(int);
			// 
			// dataColumnF_ID
			// 
			this.dataColumnF_ID.Caption = "Id";
			this.dataColumnF_ID.ColumnName = "F_ID";
			this.dataColumnF_ID.DataType = typeof(int);
			// 
			// dataColumnF_CODE
			// 
			this.dataColumnF_CODE.Caption = "Codice";
			this.dataColumnF_CODE.ColumnName = "F_CODE";
			// 
			// dataColumnF_CUSTOMER_PROJECT_CODE
			// 
			this.dataColumnF_CUSTOMER_PROJECT_CODE.Caption = "Codice progetto";
			this.dataColumnF_CUSTOMER_PROJECT_CODE.ColumnName = "F_CUSTOMER_PROJECT_CODE";
			// 
			// dataColumnF_MATERIAL_CODE
			// 
			this.dataColumnF_MATERIAL_CODE.Caption = "Codice materiale";
			this.dataColumnF_MATERIAL_CODE.ColumnName = "F_MATERIAL_CODE";
			// 
			// dataColumnF_MATERIAL_DESCRIPTION
			// 
			this.dataColumnF_MATERIAL_DESCRIPTION.Caption = "Descrizione materiale";
			this.dataColumnF_MATERIAL_DESCRIPTION.ColumnName = "F_MATERIAL_DESCRIPTION";
			// 
			// dataColumnF_STATE
			// 
			this.dataColumnF_STATE.Caption = "Stato";
			this.dataColumnF_STATE.ColumnName = "F_STATE";
			// 
			// dataColumnF_THICKNESS
			// 
			this.dataColumnF_THICKNESS.Caption = "Spessore";
			this.dataColumnF_THICKNESS.ColumnName = "F_THICKNESS";
			this.dataColumnF_THICKNESS.DataType = typeof(double);
			// 
			// dataColumnF_ARTICLE_CODE
			// 
			this.dataColumnF_ARTICLE_CODE.Caption = "Codice articolo";
			this.dataColumnF_ARTICLE_CODE.ColumnName = "F_ARTICLE_CODE";
			// 
			// dataColumnF_CODE_ORIGINALE_SHAPE
			// 
			this.dataColumnF_CODE_ORIGINALE_SHAPE.ColumnName = "F_CODE_ORIGINALE_SHAPE";
			// 
			// dataColumnF_DESCRIPTION
			// 
			this.dataColumnF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
			// 
			// dataColumnF_FINAL_THICKNESS
			// 
			this.dataColumnF_FINAL_THICKNESS.Caption = "Spessore finale";
			this.dataColumnF_FINAL_THICKNESS.ColumnName = "F_FINAL_THICKNESS";
			this.dataColumnF_FINAL_THICKNESS.DataType = typeof(double);
			// 
			// dataColumnF_LENGTH
			// 
			this.dataColumnF_LENGTH.Caption = "Lunghezza";
			this.dataColumnF_LENGTH.ColumnName = "F_LENGTH";
			this.dataColumnF_LENGTH.DataType = typeof(double);
			// 
			// dataColumnF_WIDTH
			// 
			this.dataColumnF_WIDTH.Caption = "Larghezza";
			this.dataColumnF_WIDTH.ColumnName = "F_WIDTH";
			this.dataColumnF_WIDTH.DataType = typeof(double);
			// 
			// panelEdit
			// 
			this.panelEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelEdit.Controls.Add(this.txtDescription);
			this.panelEdit.Controls.Add(this.lblDescription);
			this.panelEdit.Controls.Add(this.lblOperation);
			this.panelEdit.Controls.Add(this.btnNascondi);
			this.panelEdit.Controls.Add(this.btnConferma);
			this.panelEdit.Controls.Add(this.cmbxState);
			this.panelEdit.Controls.Add(this.lblState);
			this.panelEdit.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelEdit.Location = new System.Drawing.Point(0, 44);
			this.panelEdit.Name = "panelEdit";
			this.panelEdit.Size = new System.Drawing.Size(829, 64);
			this.panelEdit.TabIndex = 8;
			this.panelEdit.Visible = false;
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(131, 28);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(200, 22);
			this.txtDescription.TabIndex = 120;
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDescription.Location = new System.Drawing.Point(10, 33);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(87, 18);
			this.lblDescription.TabIndex = 119;
			this.lblDescription.Text = "Descrizione";
			// 
			// lblOperation
			// 
			this.lblOperation.AutoSize = true;
			this.lblOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOperation.Location = new System.Drawing.Point(10, 6);
			this.lblOperation.Name = "lblOperation";
			this.lblOperation.Size = new System.Drawing.Size(0, 20);
			this.lblOperation.TabIndex = 118;
			// 
			// btnNascondi
			// 
			this.btnNascondi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnNascondi.Image = ((System.Drawing.Image)(resources.GetObject("btnNascondi.Image")));
			this.btnNascondi.Location = new System.Drawing.Point(396, 19);
			this.btnNascondi.Name = "btnNascondi";
			this.btnNascondi.Size = new System.Drawing.Size(36, 36);
			this.btnNascondi.TabIndex = 117;
			this.btnNascondi.Click += new System.EventHandler(this.btnNascondi_Click);
			// 
			// btnConferma
			// 
			this.btnConferma.Image = ((System.Drawing.Image)(resources.GetObject("btnConferma.Image")));
			this.btnConferma.Location = new System.Drawing.Point(360, 19);
			this.btnConferma.Name = "btnConferma";
			this.btnConferma.Size = new System.Drawing.Size(36, 36);
			this.btnConferma.TabIndex = 116;
			this.btnConferma.Click += new System.EventHandler(this.btnConferma_Click);
			// 
			// cmbxState
			// 
			this.cmbxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbxState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxState.Location = new System.Drawing.Point(584, 22);
			this.cmbxState.Name = "cmbxState";
			this.cmbxState.Size = new System.Drawing.Size(200, 25);
			this.cmbxState.TabIndex = 107;
			this.cmbxState.Visible = false;
			// 
			// lblState
			// 
			this.lblState.AutoSize = true;
			this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblState.Location = new System.Drawing.Point(478, 23);
			this.lblState.Name = "lblState";
			this.lblState.Size = new System.Drawing.Size(43, 18);
			this.lblState.TabIndex = 106;
			this.lblState.Text = "Stato";
			this.lblState.Visible = false;
			// 
			// toolBar
			// 
			this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnNuovo,
            this.btnImporta,
            this.btnModifica,
            this.btnSalva,
            this.btnElimina,
            this.btnReset,
            this.btnAnnulla,
            this.toolBarButton1,
            this.btnProcess,
            this.btnSimpleTecno,
            this.toolBarButton2,
            this.btnRefresh});
			this.toolBar.DropDownArrows = true;
			this.toolBar.ImageList = this.imlToolIcon;
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.ShowToolTips = true;
			this.toolBar.Size = new System.Drawing.Size(829, 44);
			this.toolBar.TabIndex = 6;
			this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
			// 
			// btnNuovo
			// 
			this.btnNuovo.ImageIndex = 0;
			this.btnNuovo.Name = "btnNuovo";
			this.btnNuovo.ToolTipText = "Nuova";
			this.btnNuovo.Visible = false;
			// 
			// btnImporta
			// 
			this.btnImporta.ImageIndex = 1;
			this.btnImporta.Name = "btnImporta";
			this.btnImporta.ToolTipText = "Importa XML";
			this.btnImporta.Visible = false;
			// 
			// btnModifica
			// 
			this.btnModifica.ImageIndex = 2;
			this.btnModifica.Name = "btnModifica";
			this.btnModifica.ToolTipText = "Modifica";
			// 
			// btnSalva
			// 
			this.btnSalva.ImageIndex = 3;
			this.btnSalva.Name = "btnSalva";
			this.btnSalva.ToolTipText = "Salva";
			// 
			// btnElimina
			// 
			this.btnElimina.ImageIndex = 4;
			this.btnElimina.Name = "btnElimina";
			this.btnElimina.ToolTipText = "Elimina";
			// 
			// btnReset
			// 
			this.btnReset.ImageIndex = 5;
			this.btnReset.Name = "btnReset";
			this.btnReset.ToolTipText = "Annulla operazioni effettuate";
			// 
			// btnAnnulla
			// 
			this.btnAnnulla.ImageIndex = 6;
			this.btnAnnulla.Name = "btnAnnulla";
			this.btnAnnulla.ToolTipText = "Esci";
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.Name = "toolBarButton1";
			this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnProcess
			// 
			this.btnProcess.ImageIndex = 7;
			this.btnProcess.Name = "btnProcess";
			// 
			// btnSimpleTecno
			// 
			this.btnSimpleTecno.ImageIndex = 8;
			this.btnSimpleTecno.Name = "btnSimpleTecno";
			// 
			// toolBarButton2
			// 
			this.toolBarButton2.Name = "toolBarButton2";
			this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnRefresh
			// 
			this.btnRefresh.ImageIndex = 9;
			this.btnRefresh.Name = "btnRefresh";
			// 
			// dataSetDetails
			// 
			this.dataSetDetails.DataSetName = "NewDataSet";
			this.dataSetDetails.Locale = new System.Globalization.CultureInfo("en-US");
			this.dataSetDetails.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableDetails});
			// 
			// panelData
			// 
			this.panelData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelData.Controls.Add(this.dataGridDetails);
			this.panelData.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelData.Location = new System.Drawing.Point(0, 169);
			this.panelData.Name = "panelData";
			this.panelData.Size = new System.Drawing.Size(376, 283);
			this.panelData.TabIndex = 9;
			// 
			// panelPreview
			// 
			this.panelPreview.BackColor = System.Drawing.Color.Black;
			this.panelPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelPreview.ContextMenuStrip = this.contextMenuDraw;
			this.panelPreview.Controls.Add(this.VDPreview);
			this.panelPreview.Controls.Add(this.statusStrip);
			this.panelPreview.Controls.Add(this.lblShapeInfo);
			this.panelPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelPreview.Location = new System.Drawing.Point(381, 169);
			this.panelPreview.Name = "panelPreview";
			this.panelPreview.Size = new System.Drawing.Size(448, 283);
			this.panelPreview.TabIndex = 10;
			// 
			// contextMenuDraw
			// 
			this.contextMenuDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.contextMenuDraw.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miProject,
            this.miZoomAll,
            this.miLock,
            this.miZoom,
            this.miQuote,
            this.miReset});
			this.contextMenuDraw.Name = "contextMenuStrip1";
			this.contextMenuDraw.Size = new System.Drawing.Size(183, 136);
			// 
			// miProject
			// 
			this.miProject.CheckOnClick = true;
			this.miProject.Name = "miProject";
			this.miProject.Size = new System.Drawing.Size(182, 22);
			this.miProject.Text = "Vedi progetto";
			this.miProject.CheckedChanged += new System.EventHandler(this.miProject_CheckedChanged);
			// 
			// miZoomAll
			// 
			this.miZoomAll.Name = "miZoomAll";
			this.miZoomAll.Size = new System.Drawing.Size(182, 22);
			this.miZoomAll.Text = "Adatta disegno";
			this.miZoomAll.Click += new System.EventHandler(this.miZoomAll_Click);
			// 
			// miLock
			// 
			this.miLock.CheckOnClick = true;
			this.miLock.Name = "miLock";
			this.miLock.Size = new System.Drawing.Size(182, 22);
			this.miLock.Text = "Blocca disegno";
			this.miLock.Click += new System.EventHandler(this.miLock_Click);
			// 
			// miZoom
			// 
			this.miZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi10,
            this.mi50,
            this.mi100,
            this.mi150,
            this.mi200,
            this.mi300});
			this.miZoom.Name = "miZoom";
			this.miZoom.Size = new System.Drawing.Size(182, 22);
			this.miZoom.Text = "Zoom";
			// 
			// mi10
			// 
			this.mi10.Name = "mi10";
			this.mi10.Size = new System.Drawing.Size(121, 22);
			this.mi10.Text = "10 %";
			this.mi10.Click += new System.EventHandler(this.mi10_Click);
			// 
			// mi50
			// 
			this.mi50.Name = "mi50";
			this.mi50.Size = new System.Drawing.Size(121, 22);
			this.mi50.Text = "50 %";
			this.mi50.Click += new System.EventHandler(this.mi50_Click);
			// 
			// mi100
			// 
			this.mi100.Name = "mi100";
			this.mi100.Size = new System.Drawing.Size(121, 22);
			this.mi100.Text = "100 %";
			this.mi100.Click += new System.EventHandler(this.mi100_Click);
			// 
			// mi150
			// 
			this.mi150.Name = "mi150";
			this.mi150.Size = new System.Drawing.Size(121, 22);
			this.mi150.Text = "150 %";
			this.mi150.Click += new System.EventHandler(this.mi150_Click);
			// 
			// mi200
			// 
			this.mi200.Name = "mi200";
			this.mi200.Size = new System.Drawing.Size(121, 22);
			this.mi200.Text = "200 %";
			this.mi200.Click += new System.EventHandler(this.mi200_Click);
			// 
			// mi300
			// 
			this.mi300.Name = "mi300";
			this.mi300.Size = new System.Drawing.Size(121, 22);
			this.mi300.Text = "300 %";
			this.mi300.Click += new System.EventHandler(this.mi300_Click);
			// 
			// miQuote
			// 
			this.miQuote.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miLinearQuote,
            this.miRadiusQuote,
            this.miAngleQuote,
            this.miAutoQuote,
            this.miDesignQuote});
			this.miQuote.Name = "miQuote";
			this.miQuote.Size = new System.Drawing.Size(182, 22);
			this.miQuote.Text = "Quote";
			// 
			// miLinearQuote
			// 
			this.miLinearQuote.CheckOnClick = true;
			this.miLinearQuote.Name = "miLinearQuote";
			this.miLinearQuote.Size = new System.Drawing.Size(206, 22);
			this.miLinearQuote.Text = "Quote lineari";
			this.miLinearQuote.CheckedChanged += new System.EventHandler(this.miLinearQuote_CheckedChanged);
			this.miLinearQuote.Click += new System.EventHandler(this.miLinearQuote_Click);
			// 
			// miRadiusQuote
			// 
			this.miRadiusQuote.CheckOnClick = true;
			this.miRadiusQuote.Name = "miRadiusQuote";
			this.miRadiusQuote.Size = new System.Drawing.Size(206, 22);
			this.miRadiusQuote.Text = "Quote raggio";
			this.miRadiusQuote.CheckedChanged += new System.EventHandler(this.miRadiusQuote_CheckedChanged);
			this.miRadiusQuote.Click += new System.EventHandler(this.miRadiusQuote_Click);
			// 
			// miAngleQuote
			// 
			this.miAngleQuote.CheckOnClick = true;
			this.miAngleQuote.Name = "miAngleQuote";
			this.miAngleQuote.Size = new System.Drawing.Size(206, 22);
			this.miAngleQuote.Text = "Quote angolari";
			this.miAngleQuote.CheckedChanged += new System.EventHandler(this.miAngleQuote_CheckedChanged);
			this.miAngleQuote.Click += new System.EventHandler(this.miAngleQuote_Click);
			// 
			// miAutoQuote
			// 
			this.miAutoQuote.CheckOnClick = true;
			this.miAutoQuote.Name = "miAutoQuote";
			this.miAutoQuote.Size = new System.Drawing.Size(206, 22);
			this.miAutoQuote.Text = "Quote automatiche";
			this.miAutoQuote.Click += new System.EventHandler(this.miAutoQuote_Click);
			// 
			// miDesignQuote
			// 
			this.miDesignQuote.CheckOnClick = true;
			this.miDesignQuote.Name = "miDesignQuote";
			this.miDesignQuote.Size = new System.Drawing.Size(206, 22);
			this.miDesignQuote.Text = "Quote Design";
			this.miDesignQuote.Click += new System.EventHandler(this.miDesignQuote_Click);
			// 
			// miReset
			// 
			this.miReset.Name = "miReset";
			this.miReset.Size = new System.Drawing.Size(182, 22);
			this.miReset.Text = "Reset";
			this.miReset.Click += new System.EventHandler(this.miReset_Click);
			// 
			// VDPreview
			// 
			this.VDPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.VDPreview.Enabled = true;
			this.VDPreview.Location = new System.Drawing.Point(0, 18);
			this.VDPreview.Name = "VDPreview";
			this.VDPreview.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("VDPreview.OcxState")));
			this.VDPreview.Size = new System.Drawing.Size(444, 235);
			this.VDPreview.TabIndex = 2;
			this.VDPreview.MouseDownEvent += new AxVDProLib5._DVdrawEvents_MouseDownEventHandler(this.VDPreview_MouseDownEvent);
			this.VDPreview.MouseWheelEvent += new AxVDProLib5._DVdrawEvents_MouseWheelEventHandler(this.VDPreview_MouseWheelEvent);
			this.VDPreview.Resize += new System.EventHandler(this.VDPreview_Resize);
			// 
			// statusStrip
			// 
			this.statusStrip.BackColor = System.Drawing.SystemColors.Control;
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslblOrtho,
            this.tsslOperation});
			this.statusStrip.Location = new System.Drawing.Point(0, 253);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(444, 26);
			this.statusStrip.TabIndex = 3;
			// 
			// tsslblOrtho
			// 
			this.tsslblOrtho.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.tsslblOrtho.Name = "tsslblOrtho";
			this.tsslblOrtho.Size = new System.Drawing.Size(85, 21);
			this.tsslblOrtho.Text = "ORTHO: Off";
			this.tsslblOrtho.Click += new System.EventHandler(this.tsslblOrtho_Click);
			// 
			// tsslOperation
			// 
			this.tsslOperation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tsslOperation.Name = "tsslOperation";
			this.tsslOperation.Size = new System.Drawing.Size(0, 21);
			// 
			// lblShapeInfo
			// 
			this.lblShapeInfo.AutoSize = true;
			this.lblShapeInfo.BackColor = System.Drawing.Color.Black;
			this.lblShapeInfo.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblShapeInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblShapeInfo.ForeColor = System.Drawing.Color.White;
			this.lblShapeInfo.Location = new System.Drawing.Point(0, 0);
			this.lblShapeInfo.Name = "lblShapeInfo";
			this.lblShapeInfo.Size = new System.Drawing.Size(0, 18);
			this.lblShapeInfo.TabIndex = 1;
			// 
			// splitter
			// 
			this.splitter.Location = new System.Drawing.Point(376, 169);
			this.splitter.Name = "splitter";
			this.splitter.Size = new System.Drawing.Size(5, 283);
			this.splitter.TabIndex = 11;
			this.splitter.TabStop = false;
			// 
			// panelProdProcess
			// 
			this.panelProdProcess.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelProdProcess.Controls.Add(this.progressBar);
			this.panelProdProcess.Controls.Add(this.c1WorkCenterGrid);
			this.panelProdProcess.Controls.Add(this.c1WorkingGrid);
			this.panelProdProcess.Controls.Add(this.c1ProdProcess);
			this.panelProdProcess.Controls.Add(this.btnNascondiSimpleTecno);
			this.panelProdProcess.Controls.Add(this.btnOkSimpleTecno);
			this.panelProdProcess.Controls.Add(this.lblProdProc);
			this.panelProdProcess.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelProdProcess.Location = new System.Drawing.Point(0, 108);
			this.panelProdProcess.Name = "panelProdProcess";
			this.panelProdProcess.Size = new System.Drawing.Size(829, 61);
			this.panelProdProcess.TabIndex = 12;
			this.panelProdProcess.Visible = false;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(495, 12);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(210, 22);
			this.progressBar.TabIndex = 125;
			this.progressBar.Visible = false;
			// 
			// c1WorkCenterGrid
			// 
			this.c1WorkCenterGrid.AllowColMove = false;
			this.c1WorkCenterGrid.AllowSort = false;
			this.c1WorkCenterGrid.AllowUpdate = false;
			this.c1WorkCenterGrid.Caption = "Gruppi di lavoro";
			this.c1WorkCenterGrid.CaptionHeight = 17;
			this.c1WorkCenterGrid.ColumnHeaders = false;
			this.c1WorkCenterGrid.DataSource = this.dataTableWorkGroup;
			this.c1WorkCenterGrid.ExtendRightColumn = true;
			this.c1WorkCenterGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1WorkCenterGrid.GroupByCaption = "Drag a column header here to group by that column";
			this.c1WorkCenterGrid.Images.Add(((System.Drawing.Image)(resources.GetObject("c1WorkCenterGrid.Images"))));
			this.c1WorkCenterGrid.Location = new System.Drawing.Point(293, 57);
			this.c1WorkCenterGrid.Name = "c1WorkCenterGrid";
			this.c1WorkCenterGrid.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1WorkCenterGrid.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1WorkCenterGrid.PreviewInfo.ZoomFactor = 75;
			this.c1WorkCenterGrid.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1WorkCenterGrid.PrintInfo.PageSettings")));
			this.c1WorkCenterGrid.RowHeight = 15;
			this.c1WorkCenterGrid.ScrollTrack = false;
			this.c1WorkCenterGrid.Size = new System.Drawing.Size(233, 84);
			this.c1WorkCenterGrid.TabIndex = 122;
			this.c1WorkCenterGrid.PropBag = resources.GetString("c1WorkCenterGrid.PropBag");
			// 
			// dataTableWorkGroup
			// 
			this.dataTableWorkGroup.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn13,
            this.dataColumn17});
			this.dataTableWorkGroup.TableName = "TableWorkGroup";
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "T_ID";
			this.dataColumn4.DataType = typeof(int);
			// 
			// dataColumn5
			// 
			this.dataColumn5.ColumnName = "F_TECNO_WORKING";
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "F_ID_T_AN_WORK_CENTER_GROUP";
			this.dataColumn7.DataType = typeof(int);
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "F_WORK_CENTER_GROUP";
			// 
			// dataColumn13
			// 
			this.dataColumn13.ColumnName = "F_TECNO_WORK_GROUP";
			// 
			// dataColumn17
			// 
			this.dataColumn17.ColumnName = "F_WORK_CENTER_GROUP_CODE";
			// 
			// c1WorkingGrid
			// 
			this.c1WorkingGrid.AllowColMove = false;
			this.c1WorkingGrid.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None;
			this.c1WorkingGrid.AllowSort = false;
			this.c1WorkingGrid.AllowUpdate = false;
			this.c1WorkingGrid.Caption = "Fasi lavorazione";
			this.c1WorkingGrid.CaptionHeight = 17;
			this.c1WorkingGrid.ColumnHeaders = false;
			this.c1WorkingGrid.DataSource = this.dataTableWorking;
			this.c1WorkingGrid.ExtendRightColumn = true;
			this.c1WorkingGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1WorkingGrid.GroupByCaption = "Drag a column header here to group by that column";
			this.c1WorkingGrid.Images.Add(((System.Drawing.Image)(resources.GetObject("c1WorkingGrid.Images"))));
			this.c1WorkingGrid.Location = new System.Drawing.Point(35, 57);
			this.c1WorkingGrid.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.None;
			this.c1WorkingGrid.Name = "c1WorkingGrid";
			this.c1WorkingGrid.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1WorkingGrid.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1WorkingGrid.PreviewInfo.ZoomFactor = 75;
			this.c1WorkingGrid.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1WorkingGrid.PrintInfo.PageSettings")));
			this.c1WorkingGrid.RowHeight = 15;
			this.c1WorkingGrid.ScrollTrack = false;
			this.c1WorkingGrid.Size = new System.Drawing.Size(233, 84);
			this.c1WorkingGrid.TabIndex = 121;
			this.c1WorkingGrid.PropBag = resources.GetString("c1WorkingGrid.PropBag");
			// 
			// dataTableWorking
			// 
			this.dataTableWorking.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn6,
            this.dataColumn15});
			this.dataTableWorking.TableName = "TableWorking";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "T_ID";
			this.dataColumn1.DataType = typeof(int);
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "F_TECNO_WORKING";
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "F_WORKING";
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "F_ID_T_LS_PHASE_WORKING";
			this.dataColumn6.DataType = typeof(int);
			// 
			// dataColumn15
			// 
			this.dataColumn15.ColumnName = "F_LAYER_CODE";
			// 
			// c1ProdProcess
			// 
			this.c1ProdProcess.AddItemSeparator = ';';
			this.c1ProdProcess.Caption = "";
			this.c1ProdProcess.CaptionHeight = 17;
			this.c1ProdProcess.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.c1ProdProcess.ColumnCaptionHeight = 17;
			this.c1ProdProcess.ColumnFooterHeight = 17;
			this.c1ProdProcess.ColumnHeaders = false;
			this.c1ProdProcess.ColumnWidth = 100;
			this.c1ProdProcess.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList;
			this.c1ProdProcess.ContentHeight = 19;
			this.c1ProdProcess.DataMember = "T_LS_PRODUCTION_PROCESSES";
			this.c1ProdProcess.DataSource = this.c1DataSetProdProcess;
			this.c1ProdProcess.DeadAreaBackColor = System.Drawing.Color.Empty;
			this.c1ProdProcess.DisplayMember = "T_LS_PRODUCTION_PROCESSES.F_DESCRIPTION";
			this.c1ProdProcess.EditorBackColor = System.Drawing.SystemColors.Window;
			this.c1ProdProcess.EditorFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1ProdProcess.EditorForeColor = System.Drawing.SystemColors.WindowText;
			this.c1ProdProcess.EditorHeight = 19;
			this.c1ProdProcess.ExtendRightColumn = true;
			this.c1ProdProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1ProdProcess.Images.Add(((System.Drawing.Image)(resources.GetObject("c1ProdProcess.Images"))));
			this.c1ProdProcess.ItemHeight = 15;
			this.c1ProdProcess.Location = new System.Drawing.Point(131, 12);
			this.c1ProdProcess.MatchEntryTimeout = ((long)(2000));
			this.c1ProdProcess.MaxDropDownItems = ((short)(5));
			this.c1ProdProcess.MaxLength = 32767;
			this.c1ProdProcess.MouseCursor = System.Windows.Forms.Cursors.Default;
			this.c1ProdProcess.Name = "c1ProdProcess";
			this.c1ProdProcess.RowDivider.Color = System.Drawing.Color.DarkGray;
			this.c1ProdProcess.RowDivider.Style = C1.Win.C1List.LineStyleEnum.None;
			this.c1ProdProcess.RowSubDividerColor = System.Drawing.Color.DarkGray;
			this.c1ProdProcess.Size = new System.Drawing.Size(248, 25);
			this.c1ProdProcess.TabIndex = 120;
			this.c1ProdProcess.ValueMember = "T_LS_PRODUCTION_PROCESSES.F_ID";
			this.c1ProdProcess.SelectedValueChanged += new System.EventHandler(this.c1ProdProcess_SelectedValueChanged);
			this.c1ProdProcess.PropBag = resources.GetString("c1ProdProcess.PropBag");
			// 
			// c1DataSetProdProcess
			// 
			this.c1DataSetProdProcess.DataLibrary = "ShopMasterDataLibrary";
			this.c1DataSetProdProcess.DataLibraryUrl = "";
			this.c1DataSetProdProcess.DataSetDef = "DataSet_Production_Process";
			this.c1DataSetProdProcess.FillOnRequest = false;
			this.c1DataSetProdProcess.Locale = new System.Globalization.CultureInfo("en-US");
			this.c1DataSetProdProcess.Name = "c1DataSetProdProcess";
			this.c1DataSetProdProcess.SchemaClassName = "Breton.ShopMasterDataLibrary.DataClass";
			this.c1DataSetProdProcess.SchemaDef = null;
			// 
			// btnNascondiSimpleTecno
			// 
			this.btnNascondiSimpleTecno.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnNascondiSimpleTecno.Image = ((System.Drawing.Image)(resources.GetObject("btnNascondiSimpleTecno.Image")));
			this.btnNascondiSimpleTecno.Location = new System.Drawing.Point(443, 5);
			this.btnNascondiSimpleTecno.Name = "btnNascondiSimpleTecno";
			this.btnNascondiSimpleTecno.Size = new System.Drawing.Size(36, 36);
			this.btnNascondiSimpleTecno.TabIndex = 119;
			this.btnNascondiSimpleTecno.Click += new System.EventHandler(this.btnNascondiSimpleTecno_Click);
			// 
			// btnOkSimpleTecno
			// 
			this.btnOkSimpleTecno.Enabled = false;
			this.btnOkSimpleTecno.Image = ((System.Drawing.Image)(resources.GetObject("btnOkSimpleTecno.Image")));
			this.btnOkSimpleTecno.Location = new System.Drawing.Point(407, 5);
			this.btnOkSimpleTecno.Name = "btnOkSimpleTecno";
			this.btnOkSimpleTecno.Size = new System.Drawing.Size(36, 36);
			this.btnOkSimpleTecno.TabIndex = 118;
			this.btnOkSimpleTecno.Click += new System.EventHandler(this.btnOkSimpleTecno_Click);
			// 
			// lblProdProc
			// 
			this.lblProdProc.AutoSize = true;
			this.lblProdProc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblProdProc.Location = new System.Drawing.Point(11, 15);
			this.lblProdProc.Name = "lblProdProc";
			this.lblProdProc.Size = new System.Drawing.Size(142, 18);
			this.lblProdProc.TabIndex = 39;
			this.lblProdProc.Text = "Processo produttivo";
			// 
			// DataSetProdProcess
			// 
			this.DataSetProdProcess.DataSetName = "NewDataSet";
			this.DataSetProdProcess.Locale = new System.Globalization.CultureInfo("en-US");
			this.DataSetProdProcess.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableWorking,
            this.dataTableWorkGroup,
            this.dataTablePhase,
            this.dataTableMachine});
			// 
			// dataTablePhase
			// 
			this.dataTablePhase.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn16});
			this.dataTablePhase.TableName = "TablePhase";
			// 
			// dataColumn9
			// 
			this.dataColumn9.ColumnName = "F_PHASE_ID";
			this.dataColumn9.DataType = typeof(int);
			// 
			// dataColumn10
			// 
			this.dataColumn10.ColumnName = "F_PHASE_DESCRIPTION";
			// 
			// dataColumn16
			// 
			this.dataColumn16.ColumnName = "F_LAYER_CODE";
			// 
			// dataTableMachine
			// 
			this.dataTableMachine.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn14,
            this.dataColumn18});
			this.dataTableMachine.TableName = "TableMachine";
			// 
			// dataColumn11
			// 
			this.dataColumn11.ColumnName = "F_WORK_CENTER_GROUP_ID";
			this.dataColumn11.DataType = typeof(int);
			// 
			// dataColumn12
			// 
			this.dataColumn12.ColumnName = "F_WORK_CENTER_GROUP_DESCRIPTION";
			// 
			// dataColumn14
			// 
			this.dataColumn14.ColumnName = "F_TECNO_WORK_CENTER_GROUP";
			// 
			// dataColumn18
			// 
			this.dataColumn18.ColumnName = "F_WORK_CENTER_GROUP_CODE";
			// 
			// timerTecno
			// 
			this.timerTecno.Tick += new System.EventHandler(this.timerTecno_Tick);
			// 
			// Details
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(829, 452);
			this.Controls.Add(this.panelPreview);
			this.Controls.Add(this.splitter);
			this.Controls.Add(this.panelData);
			this.Controls.Add(this.panelProdProcess);
			this.Controls.Add(this.panelEdit);
			this.Controls.Add(this.toolBar);
			this.Name = "Details";
			this.Text = "Dettagli - Programmazione della produzione";
			this.Load += new System.EventHandler(this.Details_Load);
			this.Closed += new System.EventHandler(this.Details_Closed);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Details_Closing);
			((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableDetails)).EndInit();
			this.panelEdit.ResumeLayout(false);
			this.panelEdit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataSetDetails)).EndInit();
			this.panelData.ResumeLayout(false);
			this.panelPreview.ResumeLayout(false);
			this.panelPreview.PerformLayout();
			this.contextMenuDraw.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).EndInit();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.panelProdProcess.ResumeLayout(false);
			this.panelProdProcess.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkCenterGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorkGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkingGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorking)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1ProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1DataSetProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DataSetProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTablePhase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableMachine)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion	

		#region Properties

		public static bool gCreate
		{
			set	{ formCreate = value; }
			get { return formCreate; }
		}

		#endregion

		#region Open Form & Load Parameters
		public new void ShowDialog()
		{
			if (LoadParameters())
				base.ShowDialog();
		}

		public new void Show()
		{
			if (LoadParameters())
				base.Show();
		}

		private bool LoadParameters()
		{
			Parameter par;
			string message, caption;

			ProjResource.gResource.LoadMessageBox(3, out caption, out message);

			mParam = new ParameterCollection(mDbInterface, true);
			while (!mParam.GetDataFromDb(null, null, "NUMBER", "IMPERIAL"))
			{
				if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
					return false;
			}
			if (mParam.GetObject(null, null, "NUMBER", "IMPERIAL", out par))
				imperial = par.gBoolValue;
			else
			{
				par = new Parameter(null, null, "NUMBER", "IMPERIAL", Parameter.ParTypes.BOOL, "0", "Sistema metrico in uso");
				mParam.AddObject(par);
				mParam.UpdateDataToDb();	// Aggiorna il DB con il parametro inserito
			}

			return true;
		}

		#endregion

		#region Form Functions
		private void Details_Load(object sender, System.EventArgs e)
        {
            Parameter par;
            // Carica le lingue del form
            ProjResource.gResource.LoadStringForm(this);
            LoadContextMenuString();
            Edit = ProjResource.gResource.LoadFixString(this, 0);

            mDetails = new DetailCollection(mDbInterface);
            mMaterials = new MaterialCollection(mDbInterface);
            mParamApp = new ParameterCollection(mDbInterface, false);
            mTecnoOp = new TecnoOperation(mDbInterface);

			tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
			tmpXmlDetDir = tmpXmlDir + "\\Detail";

            dirXml = new DirectoryInfo(tmpXmlDir);
			dirXmlDet = new DirectoryInfo(tmpXmlDetDir);

            mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridDetails, dataTableDetails);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridDetails);

			mDraw = new Painter(VDPreview, imperial);

            DisablePanelEdit();
            SetKeyboardControl();

            // Carica tutti i materiali dal database
            mMaterials.GetDataFromDb();

            if (mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode))
                FillTable();

            // Riempie comboBox stati
            for (int i = 0; i < (int)Detail.State.MAX; i++)
                cmbxState.Items.Add(StateEnumToString((Detail.State)i));

            if (!dirXml.Exists)
            {
                dirXml.Create();
                dirXml.Attributes = FileAttributes.Hidden;
            }

			if (!dirXmlDet.Exists)
            {
				dirXmlDet.Create();
				dirXmlDet.Attributes = FileAttributes.Hidden;
            }

            // Carica l'ultima scelta per quanto riguarda la visualizzazione o meno del progetto
            if (mParamApp.GetDataFromDb("", "", "GRAPHICS", "SHOW_PROJECT") &&
                    mParamApp.GetObject("", "", "GRAPHICS", "SHOW_PROJECT", out par))
                miProject.Checked = par.gBoolValue;

            c1DataSetProdProcess.Fill();	// Carica i processi produttivi

            gCreate = true;
        }
		
		private void Details_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DialogResult dlgClose;
			string message ,caption;

			//Se ci sono state delle modifiche viene chiesto il salvataggio
			if (isModified)
			{				
				ProjResource.gResource.LoadMessageBox(0, out caption, out message);
				dlgClose = MessageBox.Show(message,caption,MessageBoxButtons.YesNoCancel,MessageBoxIcon.Question);
				if (dlgClose == DialogResult.Yes)
				{
					if (!mDetails.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
					{
						ProjResource.gResource.LoadMessageBox(1, out caption, out message);
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				else if (dlgClose == DialogResult.Cancel)
					e.Cancel=true;				//Non viene effettuato il salvataggio e la form rimane aperta
			}
		}

		private void Details_Closed(object sender, System.EventArgs e)
		{
            miLinearQuote.Checked = false;
            miRadiusQuote.Checked = false;
            miAngleQuote.Checked = false;

			mFormPosition.SaveFormPosition(this);

			try
			{
				//if (dirXmlDet.Exists)
				//    dirXmlDet.Delete(true);
				
				//if (dirXml.Exists &&
				//    !Breton.TechnoMaster.Form.Working.gCreate &&
				//    !Breton.OfferManager.Form.Orders.gCreate &&
				//    !Breton.OfferManager.Form.Projects.gCreate &&
				//    !Breton.OfferManager.Form.DetailsAdvance.gCreate &&
				//    !Breton.OfferManager.Form.Offers.gCreate)
				//    dirXml.Delete(true);
			}
			catch
			{ 
			}
			

            mDraw = null;

			gCreate = false;
			mSortGrid = null;
			mColumnView = null;

            Breton.TechnoMaster.Form.Working.TecnoExecute -= tecnoHandler;
            Breton.OfferManager.Form.Projects.SelectProjectEvent -= prjHandler;
            Breton.OfferManager.Form.Orders.SelectOrderEvent -= ordHandler;
		}

		#endregion

		#region Gestione tabelle

		private void FillTable()
		{
			Detail det;
			dataTableDetails.Clear();

			// Scorre tutti i dettagli letti
			mDetails.MoveFirst();
			while (!mDetails.IsEOF())
			{
				// Legge il progetto attuale
				mDetails.GetObject(out det);
				
				dataTableDetails.BeginInit();
				dataTableDetails.BeginLoadData();
				// Aggiunge una riga alla volta
				AddRow(ref det);

				dataTableDetails.EndInit();
				dataTableDetails.EndLoadData();
			}	
		}

		private void AddRow(ref Detail det)
		{
			// Array con i dati della riga
			object [] myArray = new object[14];
			myArray[0] = dataTableDetails.Rows.Count;
			myArray[1] = det.gId;
			myArray[2] = det.gCode;
			myArray[3] = det.gCustomerProjectCode;
			myArray[4] = det.gMaterialCode;
			myArray[5] = FindMaterialDescr(det.gMaterialCode);
			myArray[6] = StateEnumstrToString(det.gStateDet);
            fGridThickness.DoubleValue = det.gThickness;
            myArray[7] = double.Parse(fGridThickness.ToString());
			myArray[8] = det.gArticleCode;
			myArray[9] = det.gCodeOriginalShape;
            myArray[10] = det.gDescription;
            fGridFinalThickness.DoubleValue = det.gFinalThickness;
            myArray[11] = double.Parse(fGridFinalThickness.ToString());
            fGridLength.DoubleValue = det.gLength;
            myArray[12] = double.Parse(fGridLength.ToString());
            fGridWidth.DoubleValue = det.gWidth;
            myArray[13] = double.Parse(fGridWidth.ToString());

			// Crea una nuova riga nella tabella e la riempie di dati
			DataRow r = dataTableDetails.NewRow();
			r.ItemArray = myArray;
			dataTableDetails.Rows.Add(r);			

			// Assegna l'id della riga tabella al dettaglio
			det.gTableId = int.Parse(r.ItemArray[0].ToString());

			// Passa all'elemento successivo
			mDetails.MoveNext();
		}

		#endregion

		#region Tool Bar
		private void toolBar_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
            string[] fileXml;
            string[] detCode;
            bool[] tecnoAn;
			string message ,caption;
            Detail det;
            int i = 0;

			switch(toolBar.Buttons.IndexOf(e.Button))
			{
				// Modifica
				case 2:
					if (dataGridDetails.Bookmark >= 0)
					{
                        Operat = DetailOperation.Edit;
                        dataGridDetails.Enabled = false;
                        lblOperation.Text = Edit;
                        panelEdit.Visible = true;
                        panelProdProcess.Visible = false;
						EnablePanelEdit();
						AggiornaCampiNewMod();
						DisableToolBar_Edit();
					}
					break;
					
				// Salva
				case 3:
					ProjResource.gResource.LoadMessageBox(0, out caption, out message);
					if (isModified && MessageBox.Show(message, caption ,MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
					{
						Operat = DetailOperation.Save;
						if (!mDetails.UpdateDataToDb())
						{
							ProjResource.gResource.LoadMessageBox(1, out caption, out message);
							MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
							Operat = DetailOperation.None;
						}
						else
						{
							isModified=false;
							if (mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode))
								FillTable();	//Riempe la Table

							Operat = DetailOperation.None;

                            OnUpdate();
						}
					}
					break;

                // Elimina tecno
                case 4:
                    bool transaction = !mDbInterface.TransactionActive();
                    try
                    {
                        if (dataGridDetails.Bookmark >= 0)
                        {
                            ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
                            if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (dataGridDetails.SelectedRows.Count == 0)
                                {
                                    if (mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.Bookmark, 0].ToString()), out det))
                                    {
                                        //Apre la transazione
                                        if (transaction)
                                            mDbInterface.BeginTransaction();

                                        this.Cursor = Cursors.WaitCursor;
                                        mWorkOrders = new WorkOrderCollection(mDbInterface);
                                        mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                        mUsedObjects = new UsedObjectCollection(mDbInterface);

                                        if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                                        {
                                            if(!LoadTecnoPhases(det.gCode))
                                                throw new ApplicationException("Error get tecno phases from db");
                                            if(!DeleteAll())
                                                throw new ApplicationException("Error delete tecno phases from db");
                                        }
                                        //Chiude la transazione
                                        if (transaction)
                                            mDbInterface.EndTransaction(true);

                                        TecnoExec(null, null);
                                        this.Cursor = Cursors.Default;
                                    }
                                }
                                else
                                {
                                    //Apre la transazione
                                    if (transaction)
                                        mDbInterface.BeginTransaction();

                                    this.Cursor = Cursors.WaitCursor;
                                    for (i = 0; i < dataGridDetails.SelectedRows.Count; i++)
                                    {
                                        if (mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.SelectedRows[i], 0].ToString()), out det))
                                        {
                                            mWorkOrders = new WorkOrderCollection(mDbInterface);
                                            mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                            mUsedObjects = new UsedObjectCollection(mDbInterface);

                                            if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                                            {
                                                if (!LoadTecnoPhases(det.gCode))
                                                    throw new ApplicationException("Error get tecno phases from db");
                                                if (!DeleteAll())
                                                    throw new ApplicationException("Error delete tecno phases from db");
                                            }
                                        }
                                    }
                                    //Chiude la transazione
                                    if (transaction)
                                        mDbInterface.EndTransaction(true);

                                    TecnoExec(null, null);
                                    this.Cursor = Cursors.Default;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        TraceLog.WriteLine("Projects.toolBar_ButtonClick DeleteTecno", ex);
                        //Chiude la transazione
                        if (transaction)
                            mDbInterface.EndTransaction(false);
                    }
                    break;

				// Reset
				case 5:
					ProjResource.gResource.LoadMessageBox(2, out caption, out message);
					if (isModified &&
						MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes &&
						mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode))
					{
						FillTable();
						Operat = DetailOperation.None;
						AggiornaCampiNewMod();
						DisablePanelEdit();
						isModified=false;
					}
					break;

				// Esci
				case 6:
					Close();
					break;

				// Processo produttivo
				case 8:					
					// Verifica che l'indice sia valido
                    fileXml = new string[0];
                    detCode = new string[0];
                    tecnoAn = new bool[0];

                    int j = 0;
                    if (dataGridDetails.Bookmark >= 0)
                    {
                        while (j < dataGridDetails.SelectedRows.Count)
                        {
                            string[] tmp1 = new string[detCode.Length + 1];
                            string[] tmp2 = new string[fileXml.Length + 1];
                            bool[] tmp3 = new bool[tecnoAn.Length + 1];

                            detCode.CopyTo(tmp1, 0);
                            detCode = tmp1;
                            fileXml.CopyTo(tmp2, 0);
                            fileXml = tmp2;
                            tecnoAn.CopyTo(tmp3, 0);
                            tecnoAn = tmp3;

                            mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.SelectedRows[j], 0].ToString()), out det);
                            detCode[j] = det.gCode;
							fileXml[j] = tmpXmlDetDir + "\\" + det.gCode.Trim() + ".xml";
                            if (!mDetails.GetXmlParameterFromDb(det, fileXml[j]))
                                throw new ApplicationException("Error getting xml");

                            if (det.gStateDet.Trim() == Detail.State.SHAPE_LOADED.ToString() ||
                                det.gStateDet.Trim() == Detail.State.SHAPE_RELOADED.ToString())
                                tecnoAn[j] = false;
                            else
                                tecnoAn[j] = true;
                            j++;
                        }
                        if (dataGridDetails.SelectedRows.Count == 0)
                        {
                            detCode = new string[1];
                            fileXml = new string[1];
                            tecnoAn = new bool[1];
                            mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.Bookmark, 0].ToString()), out det);
                            detCode[j] = det.gCode;
							fileXml[j] = tmpXmlDetDir + "\\" + det.gCode.Trim() + ".xml";
                            if (!mDetails.GetXmlParameterFromDb(det, fileXml[j]))
                                throw new ApplicationException("Error getting xml");

                            if (det.gStateDet.Trim() == Detail.State.SHAPE_LOADED.ToString() ||
                                det.gStateDet.Trim() == Detail.State.SHAPE_RELOADED.ToString())
                                tecnoAn[j] = false;
                            else
                                tecnoAn[j] = true;
                        }				
                    }
                    else
                        return;

                    if (!Working.gCreate)
                    {
                        Working frmWorking = new Working(mDbInterface, mUser, mLanguage, fileXml, mProjectCode, detCode, tecnoAn);
                        frmWorking.MdiParent = this.MdiParent;
                        frmWorking.Show();
                    }
                    else
                    {
                        AttivaForm("Working");
                    }
					break;

                // Analisi tecno semplificata
                case 9:
                    Parameter par;

                    // Verifica che l'indice sia valido
                    if (dataGridDetails.Bookmark >= 0)
                    {
                        if (mParamApp.GetDataFromDb("", "", "TECNO", "LAST_PROCESS") &&
                           mParamApp.GetObject("", "", "TECNO", "LAST_PROCESS", out par))
                            c1ProdProcess.SelectedValue = par.gIntValue;

                        panelProdProcess.Visible = true;
                    }
                    else
                        return;
                    
                    break;

                // Refresh
                case 11:
                    this.Cursor = Cursors.WaitCursor;
					locked = true;
					if (dirXmlDet.Exists)
					{
						while (dirXmlDet.GetFiles().Length > 0)
							dirXmlDet.GetFiles()[0].Delete();
					}
					
                    if (mDetails.RefreshData())
                        FillTable();

					mProjectDraw = false;
					CellValue();
					locked = false;
                    this.Cursor = Cursors.Default;
                    break;
			}
		}
		#endregion
		
		#region Eventi dei controlli
		private void dataGridDetails_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
		{
			if(!locked)
				CellValue();
		}

		private void CellValue()
		{
            VDProLib5.vdLayer layer;
			Detail det;
			int i;

			if (dataGridDetails.Bookmark >= 0) 
				i = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
			else
				return;

            try
            {
                // Aggiorna i campi di Edit con i valori della riga selezionata se non sto facendo operazioni
                if (Operat == DetailOperation.None)
                {
                    if (mDetails.GetObjectTable(i, out det))
                    {
                        //cmbxState.SelectedIndex = StateEnumstrToInt(det.gStateDet);
                        txtDescription.Text = det.gDescription;
                        lblShapeInfo.Text = det.gDescription;

                        if (miProject.Checked)
                        {
                            if (coord_assolute)
                            {
                                if (!mProjectDraw)
                                    DrawProject();

								if (mDraw.gVDCom.gCurrentLayer != null)
									mDraw.gVDCom.DeselectLayer(mDraw.gVDCom.gCurrentLayer);
								mDraw.gVDCom.SelectLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), 4, 5);

								mDraw.QuotasOff();
								mDraw.LayersProcessing(true);
                            }
                            else
                            {
                                // Spegne tutti i layer
								for (int j = 0; j < mDraw.gVDCom.LayersCount; j++)
                                {
									layer = mDraw.gVDCom.Layer(j);
                                    mDraw.OnOffLayer(layer.LayerName, true);
                                }
								mDraw.gVDCom.ActiveLayer = VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim();
                                // Accende il layer interessato
                                mDraw.OnOffLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), false);
                                mDraw.OnOffLayer(VDInterface.Layers.TEXT.ToString() + det.gCodeOriginalShape.Trim(), false);
                            }
                        }
                        else
                        {
                            // Estrae l'xml del pezzo interessato
							mFileXml = tmpXmlDetDir + "\\" + det.gCode.Trim() + ".xml";
                            if (!File.Exists(mFileXml) && !mDetails.GetXmlParameterFromDb(det, mFileXml))
                                throw new ApplicationException("Error getting xml");

							mDraw.DrawXml(mFileXml, true, false, false, true, true); // Disegna il pezzo
							mDraw.ZoomAll();
                        }

						miLinearQuote_Click(null, null);
						miRadiusQuote_Click(null, null);
						miAngleQuote_Click(null, null);
                    }

                    miAutoQuote.Checked = false;
					miDesignQuote.Checked = false;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Details.CellValue() :" + ex.Message);
            }
		}

        private void dataGridDetails_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Detail det;

            int i = (int)dataGridDetails[e.Row, "T_ID"];
            mDetails.GetObjectTable(i, out det);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (det.gStateDet.Trim() == Detail.State.SHAPE_LOADED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_COMPLETED.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_IN_PROGRESS.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_STANDBY.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_RELOADED.ToString())
            {
                e.CellStyle.BackColor = Color.Orange;
                e.CellStyle.BackColor2 = Color.Gold;
            }
        }

		private string StateEnumToString(Detail.State st)
		{
			switch (st)
			{
                case Detail.State.SHAPE_LOADED:
					return ProjResource.gResource.LoadFixString(this, 1);
                case Detail.State.SHAPE_IN_PROGRESS:
                    return ProjResource.gResource.LoadFixString(this, 2);
                case Detail.State.SHAPE_COMPLETED:
                    return ProjResource.gResource.LoadFixString(this, 3);
                case Detail.State.SHAPE_STANDBY:
                    return ProjResource.gResource.LoadFixString(this, 4);
                case Detail.State.SHAPE_ANALIZED:
                    return ProjResource.gResource.LoadFixString(this, 5);
                case Detail.State.SHAPE_RELOADED:
                    return ProjResource.gResource.LoadFixString(this, 6);
				default:
					return "";
			}
		}

		private string StateEnumstrToString(string st)
		{
            if (st.Trim() == Detail.State.SHAPE_LOADED.ToString())	
				return ProjResource.gResource.LoadFixString(this, 1);
            else if (st.Trim() == Detail.State.SHAPE_IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 2);
            else if (st.Trim() == Detail.State.SHAPE_COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 3);
            else if (st.Trim() == Detail.State.SHAPE_STANDBY.ToString())
                return ProjResource.gResource.LoadFixString(this, 4);
            else if (st.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 5);
            else if (st.Trim() == Detail.State.SHAPE_RELOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 6);
			else
				return "";
		}

		private int StateEnumstrToInt(string st)
		{
            if (st.Trim() == Detail.State.SHAPE_LOADED.ToString())	
				return 0;
            else if (st.Trim() == Detail.State.SHAPE_IN_PROGRESS.ToString())
                return 1;
            else if (st.Trim() == Detail.State.SHAPE_COMPLETED.ToString())
                return 2;
            else if (st.Trim() == Detail.State.SHAPE_STANDBY.ToString())
                return 3;
            else if (st.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                return 4;
            else if (st.Trim() == Detail.State.SHAPE_RELOADED.ToString())
                return 5;
			else
				return -1;
		}

		private Detail.State StateStringToEnum(string state)
		{
            if (state == ProjResource.gResource.LoadFixString(this, 1))
                return Detail.State.SHAPE_LOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 2))
                return Detail.State.SHAPE_IN_PROGRESS;
            else if (state == ProjResource.gResource.LoadFixString(this, 3))
                return Detail.State.SHAPE_COMPLETED;
            else if (state == ProjResource.gResource.LoadFixString(this, 4))
                return Detail.State.SHAPE_STANDBY;
            else if (state == ProjResource.gResource.LoadFixString(this, 5))
                return Detail.State.SHAPE_ANALIZED;
            else if (state == ProjResource.gResource.LoadFixString(this, 6))
                return Detail.State.SHAPE_RELOADED;
            else
                return Detail.State.NONE;
		}

		private void btnConferma_Click(object sender, System.EventArgs e)
		{
			Detail det;
			
			// Operazione in corso: Modifica dettaglio
			if (Operat==DetailOperation.Edit)
			{
				// Estrae l'oggetto da modificare
				if (mDetails.GetObjectTable((int)dataGridDetails[dataGridDetails.Bookmark, 0], out det))
				{
					// Aggiorna tutti i valori con i nuovi inseriti
					//det.gStateDet = StateStringToEnum(cmbxState.SelectedItem.ToString()).ToString();
                    det.gDescription = txtDescription.Text;	
				}


                // Aggiorna la Table
				int idTable = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
				object[] rowChange =  {idTable, det.gId, det.gCode, det.gCustomerProjectCode, det.gMaterialCode, FindMaterialDescr(det.gMaterialCode),
                                        StateEnumstrToString(det.gStateDet), double.Parse(fGridThickness.ToString()), det.gArticleCode, det.gCodeOriginalShape, det.gDescription, 
                                        double.Parse(fGridFinalThickness.ToString()), double.Parse(fGridLength.ToString()),double.Parse(fGridWidth.ToString())};
				
				dataTableDetails.Rows[idTable].ItemArray = rowChange;		
				isModified=true;
				dataGridDetails.Enabled=true;
			}
			else if (Operat==DetailOperation.None)
			{
				return;
			}
			// Abilita-Disabilita i vari controlli
			Operat = DetailOperation.None;
			DisablePanelEdit();
            panelEdit.Visible = false;
			AggiornaCampiNewMod();
			EnableToolBar_Edit();
			CellValue();
		}

		private void btnNascondi_Click(object sender, System.EventArgs e)
		{
			// Annnulla l'operazione in corso
			DisablePanelEdit();
            panelEdit.Visible = false;
            dataGridDetails.Enabled = true;
            Operat = DetailOperation.None;
            EnableToolBar_Edit();
			AggiornaCampiNewMod();
			CellValue();
		}

        private void VDPreview_MouseWheelEvent(object sender, AxVDProLib5._DVdrawEvents_MouseWheelEvent e)
        {
            if (miLock.Checked)
                e.cancel = 1;
        }

        private void VDPreview_Resize(object sender, EventArgs e)
        {
            if (mDraw != null)
                mDraw.ZoomAll();
        }

        private void VDPreview_MouseDownEvent(object sender, AxVDProLib5._DVdrawEvents_MouseDownEvent e)
        {
            Detail det;
            string codeOriginalDet;

            // Se il click non avviene per posizionare una quota
            if (!miLinearQuote.Checked && !miRadiusQuote.Checked && !miAngleQuote.Checked)
            {
				codeOriginalDet = mDraw.gVDCom.SelectLayerClick();

				if (codeOriginalDet == null)
					return;

				mDetails.MoveFirst();
				while (!mDetails.IsEOF())
				{
					mDetails.GetObject(out det);
					if (det.gCodeOriginalShape.Trim() == codeOriginalDet)
					{
						dataGridDetails.Bookmark = SelectBookmark(det);
						break;
					}
					mDetails.MoveNext();
				}                
            }
            // Se � stato cliccato il tasto destro azzero il conteggio
            if (e.button == 2)
            {
                clickNumber = 0;
                return;
            }

            clickNumber++;
            // Se deve inserire una quota lineare si aspetta 3 click
            if (miLinearQuote.Checked)
            {
                if (clickNumber < 3)
                {
					if (!mDraw.gVDCom.GetSnapPoint())
						clickNumber = 0;
                }
                else
                    clickNumber = 0;
            }
            // Se deve inserire una quota angolare si aspetta 4 click
            else if (miAngleQuote.Checked)
            {
                if (clickNumber < 4)
                {
					if (!mDraw.gVDCom.GetSnapPoint())
						clickNumber = 0;
                }
                else
                    clickNumber = 0;
            }
        }

        private void tsslblOrtho_Click(object sender, EventArgs e)
        {
            // Imposta l'ortogonalit� al layer attivo
			if (mDraw.gVDCom.OrthoMode)
            {
                tsslblOrtho.Text = "ORTHO: Off";
				mDraw.gVDCom.OrthoMode = false;
            }
            else
            {
                tsslblOrtho.Text = "ORTHO: On";
				mDraw.gVDCom.OrthoMode = true;
            }
        }

        private void c1ProdProcess_SelectedValueChanged(object sender, EventArgs e)
        {
            if (c1ProdProcess.SelectedIndex >= 0)
                btnOkSimpleTecno.Enabled = true;
            else
                btnOkSimpleTecno.Enabled = false;
        }

        /// <summary>
        /// Conferma il processo produttivo selezionato creando le fasi tecno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOkSimpleTecno_Click(object sender, EventArgs e)
        {
			//Detail det, detTecno;
			//XmlDocument doc = new XmlDocument();
			//Parameter par;
			//string message, caption;
			//int processId, j = 0;
			//string tecnoXmlFile;
			//bool editShape = false;
			//bool error = false, notLoaded = false;
			//DetailCollection mDetailsTecno = new DetailCollection(mDbInterface);
			//ShapeGeometry spGeo; // = new ShapeGeometry();
            
			//if (dataGridDetails.Bookmark >= 0)
			//{
			//    this.Cursor = Cursors.WaitCursor;
			//    while (j < dataGridDetails.SelectedRows.Count)
			//    {
			//        mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.SelectedRows[j], 0].ToString()), out det);
			//        tecnoXmlFile = tmpXmlDir + "\\" + det.gCode.Trim() + ".xml";
			//        if (!mDetails.GetXmlParameterFromDb(det, tecnoXmlFile))
			//            throw new ApplicationException("Error getting xml");

			//        mShapeTecno = new Shape();
			//        XmlAnalizer.ReadShapeXml(tecnoXmlFile, ref mShapeTecno, false);	// Lettura del file xml
			//        spGeo = new ShapeGeometry();
			//        spGeo.LoadShapePath(mShape);
			//        spGeo.LoadConvexPath(mShape);
			//        spGeo.SetStraightLine(ref mShape);
			//        doc.Load(tecnoXmlFile);

			//        if (det.gStateDet.Trim() == Detail.State.SHAPE_LOADED.ToString() ||
			//            det.gStateDet.Trim() == Detail.State.SHAPE_RELOADED.ToString())
			//        {
			//            processId = (int)c1ProdProcess.SelectedValue;
			//            if (mTecnoOp.LoadWorkingPhase(det, processId, ref dataTableWorking, ref dataTableWorkGroup, mShapeTecno))
			//            {
			//                mDetailsTecno.GetSingleElementFromDb(det.gCode);
			//                mDetailsTecno.GetObject(out detTecno);
			//                mWorkOrders = new WorkOrderCollection(mDbInterface);
			//                mWorkPhases = new WorkPhaseCollection(mDbInterface);
			//                mUsedObjects = new UsedObjectCollection(mDbInterface);

			//                if (!mTecnoOp.ConfirmTecno(ref detTecno, ref mDetailsTecno, doc, ref editShape, dataTableWorkGroup, c1WorkCenterGrid, dataTableWorking,
			//                    c1WorkingGrid, mWorkOrders, mWorkPhases, mUsedObjects, tecnoXmlFile))
			//                    error = true;
			//            }
			//            else
			//                error = true;
			//        }
			//        else
			//        {
			//            // Non � possibile analizzare questo pezzo da qui dato che non ha lo stato "LOADED" o "RELOADED"
			//            notLoaded = true;
			//        }
			//        j++;
			//    }
			//    if (dataGridDetails.SelectedRows.Count == 0)
			//    {
			//        mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.Bookmark, 0].ToString()), out det);
			//        tecnoXmlFile = tmpXmlDir + "\\" + det.gCode.Trim() + ".xml";
			//        if (!mDetails.GetXmlParameterFromDb(det, tecnoXmlFile))
			//            throw new ApplicationException("Error getting xml");

			//        mShapeTecno = new Shape();
			//        XmlAnalizer.ReadShapeXml(tecnoXmlFile, ref mShapeTecno, false);	// Lettura del file xml
			//        spGeo = new ShapeGeometry();
			//        spGeo.LoadShapePath(mShape);
			//        spGeo.LoadConvexPath(mShape);
			//        spGeo.SetStraightLine(ref mShape);
			//        doc.Load(tecnoXmlFile);

			//        if (det.gStateDet.Trim() == Detail.State.SHAPE_LOADED.ToString() ||
			//            det.gStateDet.Trim() == Detail.State.SHAPE_RELOADED.ToString())
			//        {
			//            processId = (int)c1ProdProcess.SelectedValue;
			//            if (mTecnoOp.LoadWorkingPhase(det, processId, ref dataTableWorking, ref dataTableWorkGroup, mShapeTecno))
			//            {
			//                mDetailsTecno.GetSingleElementFromDb(det.gCode);
			//                mDetailsTecno.GetObject(out detTecno);
			//                mWorkOrders = new WorkOrderCollection(mDbInterface);
			//                mWorkPhases = new WorkPhaseCollection(mDbInterface);
			//                mUsedObjects = new UsedObjectCollection(mDbInterface);

			//                if (!mTecnoOp.ConfirmTecno(ref detTecno, ref mDetailsTecno, doc, ref editShape, dataTableWorkGroup,
			//                    c1WorkCenterGrid, dataTableWorking, c1WorkingGrid, mWorkOrders, mWorkPhases, mUsedObjects, tecnoXmlFile))
			//                {
			//                    // Errore
			//                    ProjResource.gResource.LoadMessageBox(this, 2, out caption, out message);
			//                    MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
			//                }
			//            }
			//            else
			//            {
			//                // Errore
			//                ProjResource.gResource.LoadMessageBox(this, 2, out caption, out message);
			//                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
			//            }
			//        }
			//        else
			//        {
			//            // Non � possibile analizzare questo pezzo da qui dato che non ha lo stato "LOADED"
			//            ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
			//            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
			//        }
			//    }
			//    this.Cursor = Cursors.Default;
			//    if (error)
			//    {
			//        ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
			//        MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
			//    }
			//    if (notLoaded)
			//    {
			//        ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
			//        MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
			//    }

			//    // Aggiorna la griglia e lancia evento di update
			//    mDetails.RefreshData();
			//    FillTable();
			//    OnUpdate();

			//    if (mParamApp.GetDataFromDb("", "", "TECNO", "LAST_PROCESS") &&
			//     mParamApp.GetObject("", "", "TECNO", "LAST_PROCESS", out par))
			//    {
			//        par.gIntValue = (int)c1ProdProcess.SelectedValue;
			//    }
			//    else
			//    {
			//        par = new Parameter(Environment.MachineName, Application.ProductName, "TECNO", "LAST_PROCESS", Parameter.ParTypes.INT, c1ProdProcess.SelectedValue.ToString(), "");
			//        mParamApp.AddObject(par);
			//    }
			//    mParamApp.UpdateDataToDb();
			//}

			Detail det;
			Parameter par;
			int j = 0;
			string caption, message;

			if (mTecno == null)
				mTecno = new ThreadTecno(mDbInterface, dataTableWorkGroup, dataTableWorking, c1WorkCenterGrid, c1WorkingGrid);

			if (dataGridDetails.Bookmark >= 0)
			{
				mTecno.gSelectedProcess = (int)c1ProdProcess.SelectedValue;

				if (!mTecno.gInProgress)
				{
					while (j < dataGridDetails.SelectedRows.Count)
					{
						mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.SelectedRows[j], 0].ToString()), out det);				
						mTecno.AddDetail(det);
						j++;
					}

					if (dataGridDetails.SelectedRows.Count == 0)
					{
						mDetails.GetObjectTable(int.Parse(dataGridDetails[dataGridDetails.Bookmark, 0].ToString()), out det);
						mTecno.AddDetail(det);
					}

					ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
					if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
					{
						c1ProdProcess.Enabled = false;
						mTecno.Start();
						progressBar.Visible = true;
						progressBar.Value = 0;
						timerTecno.Enabled = true;
						timerTecno.Start();
					}
				}
				else
				{
					ProjResource.gResource.LoadMessageBox(this, 6, out caption, out message);
					if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
					{
						if (mTecno != null && mTecno.gInProgress)
							mTecno.Stop();
					}
				}

				if (mParamApp.GetDataFromDb("", "", "TECNO", "LAST_PROCESS") &&
					 mParamApp.GetObject("", "", "TECNO", "LAST_PROCESS", out par))
				{
					par.gIntValue = (int)c1ProdProcess.SelectedValue;
				}
				else
				{
					par = new Parameter(Environment.MachineName, Application.ProductName, "TECNO", "LAST_PROCESS", Parameter.ParTypes.INT, c1ProdProcess.SelectedValue.ToString(), "");
					mParamApp.AddObject(par);
				}
				mParamApp.UpdateDataToDb();
			}
        }
        
        private void btnNascondiSimpleTecno_Click(object sender, EventArgs e)
        {
            panelProdProcess.Visible = false;
        }

		private void timerTecno_Tick(object sender, EventArgs e)
		{
			string message, caption;

			// Verifica se sta copiando
			if (mTecno != null && mTecno.gInProgress)
			{
				if (mTecno.gMaxProgress > 0)
				{
					// Setta i valori della progressBar
					progressBar.Maximum = mTecno.gMaxProgress;
					progressBar.Value = mTecno.gProgress;
				}

				// Verifica se � terminata l'analisi dell'ordine
				if (mTecno.gAnalized)// && !mFirstThick)
				{
					mTecno.Continue();
				}
			}
			else
			{
				// Se il processo � terminato
				timerTecno.Stop();
				timerTecno.Enabled = false;
				progressBar.Visible = false;
				Refresh();
				c1ProdProcess.Enabled = true;

				this.Cursor = Cursors.WaitCursor;
				OnUpdate();
				mDetails.RefreshData();
				FillTable();
				this.Cursor = Cursors.Default;

				// Verifica il risultato delle fasi tecno
				if (mTecno.gError)
				{
					ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					mTecno.gError = false;
				}
				if (mTecno.gNotLoaded)
				{
					ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					mTecno.gNotLoaded = false;
				}

				mTecno = null;
			}
		}


		#endregion

		#region Gestione eventi
		private void ProgettoSelezionato(object sender, string ev)
		{
			mProjectCode = ev;
			if (mProjectCode == null)
			{
				dataTableDetails.Clear();
				return;
			}

            if (mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode))
            {
                FillTable();

				if (dirXmlDet.Exists)
				{
					while (dirXmlDet.GetFiles().Length > 0)
						dirXmlDet.GetFiles()[0].Delete();
				}

                mProjectDraw = false;
                CellValue();
            }
		}

        private void TecnoExec(object sender, EventArgs ev)
        {
            if (mDetails.GetDataFromDb(Detail.Keys.F_CODE, mProjectCode))
            {
                FillTable();
                OnUpdate();
            }
        }

        //Genera l'evento UpdateDetail
        protected virtual void OnUpdate()
        {
            if (UpdateDetail != null)
                UpdateDetail(this, new EventArgs());
        }

        private void OrdineSelezionato(object sender, string ev)
		{
            Project prj;
            ProjectCollection mProjects = new ProjectCollection(mDbInterface);

			if (mProjects.GetDataFromDb(Project.Keys.F_CODE, ev) && 
                mProjects.GetObject(out prj) && 
                mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
			{
                mProjectCode = prj.gCode;
				
				if (dirXmlDet.Exists)
				{
					while (dirXmlDet.GetFiles().Length > 0)
						dirXmlDet.GetFiles()[0].Delete();
				}

				FillTable();
                mProjectDraw = false;
                CellValue();
			}
		}
        #endregion

		#region Abilita-Disabilita vari controlli

		private void AggiornaCampiNewMod()
		{
			// Aggiornamento dei campi di Edit a seconda dell'operazione in corso
			if (Operat==DetailOperation.Edit)
			{
				CellValue();
			}
			else if (Operat==DetailOperation.None)
			{
				lblOperation.Text="";
				//cmbxState.SelectedIndex = -1;
                txtDescription.Text = "";
				dataGridDetails.Enabled=true;
			}			
		}

		// Abilita i pulsanti della ToolBar
		private void EnableToolBar_Edit()
		{
            btnModifica.Enabled = true;
            btnSalva.Enabled = true;
            btnElimina.Enabled = true;
            btnReset.Enabled = true;
            btnProcess.Enabled = true;
            btnSimpleTecno.Enabled = true;
            btnRefresh.Enabled = true;
		}

		// Disabilita i pulsanti della ToolBar
		private void DisableToolBar_Edit()
		{
            btnModifica.Enabled = false;
            btnSalva.Enabled = false;
            btnElimina.Enabled = false;
            btnReset.Enabled = false;
            btnProcess.Enabled = false;
            btnSimpleTecno.Enabled = false;
            btnRefresh.Enabled = false;
		}

		// Abilita i controlli presenti nel panelEdit
		private void EnablePanelEdit()
		{
			//cmbxState.Enabled = true;
			btnConferma.Enabled=true;
			btnNascondi.Enabled=true;
		}

		// Disabilita i controlli presenti nel panelEdit
		private void DisablePanelEdit()
		{
			//cmbxState.Enabled = false;
			btnConferma.Enabled=false;
			btnNascondi.Enabled=false;
		}

		private string FindMaterialDescr(string matCode)
		{
			Material mat;
			mMaterials.MoveFirst();

			while (!mMaterials.IsEOF())
			{
				if(mMaterials.GetObject(out mat) && mat.gCode==matCode)
					return mat.gDescription;

				mMaterials.MoveNext();
			}
			return "";
		}

        private void AttivaForm(string formName)
        {
            System.Windows.Forms.Form[] frm = new System.Windows.Forms.Form[this.ParentForm.MdiChildren.Length];
            frm = this.ParentForm.MdiChildren;
            for (int i = 0; i < frm.Length; i++)
            {
                if (frm[i].Name == formName)
                    frm[i].Activate();
            }
        }

		#endregion

        #region Private Methods

        /// ***********************************************************
        /// LoadTecnoPhases
        /// <summary>
        /// Carica le collection con gli ordini e le fasi di lavoro
        /// </summary>
        /// <param name="code">Codice del dettaglio</param>
        /// <returns></returns>
        /// ***********************************************************        
        private bool LoadTecnoPhases(string code)
        {
            int i = 0;
            string[] usObjs = new string[0];
            string[] wkOrds = new string[0];
            WorkPhase wk = new WorkPhase();

            if (!mWorkPhases.GetDataFromDb(WorkPhase.Keys.F_SEQUENCE, null, code, null, null, null))
                return false;

            mWorkPhases.MoveFirst();
            while (!mWorkPhases.IsEOF())
            {
                mWorkPhases.GetObject(out wk);

                string[] tmp = new string[wkOrds.Length + 1];
                wkOrds.CopyTo(tmp, 0);
                wkOrds = tmp;
                wkOrds[i] = wk.gWorkOrderCode;

                string[] tmp2 = new string[usObjs.Length + 1];
                usObjs.CopyTo(tmp2, 0);
                usObjs = tmp2;
                usObjs[i] = wk.gUsedObjectCode;

                i++;
                mWorkPhases.MoveNext();
            }
            string[] tmp3 = new string[usObjs.Length + 1];
            usObjs.CopyTo(tmp3, 0);
            usObjs = tmp3;
            usObjs[i] = wk.gPhaseDUsedObject;

            if (!mWorkOrders.GetDataFromDb(WorkOrder.Keys.F_SEQUENCE, null, null, null, null, null, wkOrds))
                return false;

            if (!mUsedObjects.GetDataFromDb(usObjs))
                return false;

            return true;
        }

        ///**********************************************************
        /// Delete All
        /// <summary>
        /// Elimina il processo produttivo (fase tecno) per il pezzo
        /// </summary>
        /// <returns></returns>
        /// *********************************************************
        private bool DeleteAll()
        {
            try
            {
                // Cancellazione di tutti i record caricati
                mWorkPhases.MoveFirst();
                while (!mWorkPhases.IsEmpty())
                    mWorkPhases.DeleteObject();
                if (!mWorkPhases.UpdateDataToDb())
                    throw new ApplicationException("Error on work phases updated");

                mUsedObjects.MoveFirst();
                while (!mUsedObjects.IsEmpty())
                    mUsedObjects.DeleteObject();
                if (!mUsedObjects.UpdateDataToDb())
                    throw new ApplicationException("Error on used objects updated");

                mWorkOrders.MoveFirst();
                while (!mWorkOrders.IsEmpty())
                    mWorkOrders.DeleteObject();
                if (!mWorkOrders.UpdateDataToDb())
                    throw new ApplicationException("Error on work orders updated");

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Details.DeleteAll Error: " + ex.Message);
                return false;
            }
            finally
            {
                mWorkPhases = null;
                mUsedObjects = null;
                mWorkOrders = null;
            }
        }

        private void SetKeyboardControl()
        {
            mImpost = new FieldCollection(imperial);
            
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//DimZ
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//FinalThickenss
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//Length
            mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//Width
            
            fGridThickness = mImpost.GetField(0);
            fGridFinalThickness = mImpost.GetField(1);
            fGridLength = mImpost.GetField(2);
            fGridWidth = mImpost.GetField(3);


            dataGridDetails.Columns[7].Caption += " (" + fGridThickness.GetMeasureUnit() + ")";
            dataGridDetails.Columns[11].Caption += " (" + fGridFinalThickness.GetMeasureUnit() + ")";
            dataGridDetails.Columns[12].Caption += " (" + fGridLength.GetMeasureUnit() + ")";
            dataGridDetails.Columns[13].Caption += " (" + fGridWidth.GetMeasureUnit() + ")";          
        }

        private void ResetVal()
        {
            fGridThickness.EnterValue("");
            fGridFinalThickness.EnterValue("");
            fGridLength.EnterValue("");
            fGridWidth.EnterValue("");
        }

        /// <summary>
        /// Ricerca la riga nella griglia che identifica l'oggetto
        /// </summary>
        /// <param name="det">oggetto da ricercare nella griglia</param>
        /// <returns></returns>
        private int SelectBookmark(Detail det)
        {
            for (int i = 0; i < dataTableDetails.Rows.Count; i++)
            {
                if ((int)dataGridDetails[i, 0] == det.gTableId)
                    return i;
            }

            return -1;
        }

        private void LoadContextMenuString()
        {
            miProject.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 0);
            miZoomAll.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 1);
            miLock.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 2);
            miZoom.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 3);
            mi10.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 4);
            mi50.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 5);
            mi100.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 6);
            mi150.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 7);
            mi200.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 8);
            mi300.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 9);
            miQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 10);
            miLinearQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 11);
            miRadiusQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 12);
            miAngleQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 13);
            miAutoQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 14);
            miReset.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 15);
		    miDesignQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 16);
        }
        #endregion      

        #region Context Menu
        private void mi10_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(0.10);
        }

        private void mi50_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(0.50);
        }

        private void mi100_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(1.00);
        }

        private void mi150_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(1.50);
        }

        private void mi200_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(2.00);
        }

        private void mi300_Click(object sender, EventArgs e)
        {
            mDraw.ZoomTo(3.00);
        }
        
        private void miZoomAll_Click(object sender, EventArgs e)
        {
            mDraw.ZoomAll();
        }

        private void miLock_Click(object sender, EventArgs e)
        {
            // Verifica se � stato richiesto di bloccare il pannello di disegno
            if (miLock.Checked)
                mDraw.gVDCom.DisableMouseWheelPan = true; // Disabilita l'uso della ruota del mouse
            else
				mDraw.gVDCom.DisableMouseWheelPan = false; // Abilita l'uso della ruota del mouse
        }

        private void miProject_CheckedChanged(object sender, EventArgs e)
        {
            Parameter par;

            miLinearQuote.Checked = false;
            miRadiusQuote.Checked = false;
            miAngleQuote.Checked = false;
            tsslOperation.Text = "";

            // Disegna tutto il progetto
            if (miProject.Checked)
            {
                DrawProject();
            }
            
            CellValue();

            if (mParamApp.GetDataFromDb("", "", "GRAPHICS", "SHOW_PROJECT") &&
                 mParamApp.GetObject("", "", "GRAPHICS", "SHOW_PROJECT", out par))
            {
                par.gBoolValue = miProject.Checked;
            }
            else
            {
                par = new Parameter(Environment.MachineName, Application.ProductName, "GRAPHICS", "SHOW_PROJECT", Parameter.ParTypes.INT, miProject.Checked.ToString(), "");
                mParamApp.AddObject(par);
            }
            mParamApp.UpdateDataToDb();
        }

        private void DrawProject()
        {
			string fileOffer;
			OfferCollection offers = new OfferCollection(mDbInterface, mUser);
			Offer off;
			OrderCollection orders = new OrderCollection(mDbInterface, mUser);
			Order ord;
			ProjectCollection projects = new ProjectCollection(mDbInterface);
			Project prj;
            Detail det;
            string mFileXml;
            int i = 0;

            allPiece = new string[0];
            mDetails.MoveFirst();
            while (!mDetails.IsEOF())
            {
                mDetails.GetObject(out det);

				mFileXml = tmpXmlDetDir + "\\" + det.gCode.Trim() + ".xml";

                if (!File.Exists(mFileXml) && !mDetails.GetXmlParameterFromDb(det, mFileXml))
                    throw new ApplicationException("Error getting xml");

                string[] tmp = new string[allPiece.Length + 1];
                allPiece.CopyTo(tmp, 0);
                allPiece = tmp;
                allPiece[i] = mFileXml;
                mDetails.MoveNext();
                i++;
            }

            Shape shp = new Shape();

            // Verifica che il pezzo che si sta disegnando abbia le coordinate assolute
            if (allPiece != null && allPiece.Length > 0)
            {
                XmlAnalizer.ReadShapeXml(allPiece[0], ref shp, true);

                if (shp.gContours.Count > 0)
                {
                    // Disegna tutto il progetto con le coordinate assolute
                    mDraw.DrawXml(allPiece, true, false, false, true, true);
                    mDraw.ZoomAll();
                    coord_assolute = true;
                }
                else
                {
                    coord_assolute = false;
                }
            }

			// Cerca l'xml dell'intera offerta
			mDetails.MoveFirst();
			mDetails.GetObject(out det);
			projects.GetSingleElementFromDb(det.gCustomerProjectCode);
			projects.GetObject(out prj);
			orders.GetSingleElementFromDb(prj.gCustomerOrderCode);
			orders.GetObject(out ord);
			offers.GetSingleElementFromDb(ord.gOfferCode);
			offers.GetObject(out off);
			fileOffer = tmpXmlDetDir + "\\" + off.gCode.Trim() + ".xml";

			if (!File.Exists(fileOffer))
				offers.GetXmlParameterFromDb(off, fileOffer);

			mDraw.DrawTopXml(fileOffer, prj.gCodeOriginalProject.Trim());

            mProjectDraw = true;
        }

        private void miLinearQuote_Click(object sender, EventArgs e)
        {
            if (miLinearQuote.Checked)
            {
                tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 7);

                // Disattiva pulsanti eventualmente attive
                miRadiusQuote.Checked = false;
                miAngleQuote.Checked = false;

                clickNumber = 0;

				mDraw.gVDCom.LinearQuote();
            }
            else
            {
                // Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
                tsslOperation.Text = "";
            }
        }

        private void miRadiusQuote_Click(object sender, EventArgs e)
        {
			if (miRadiusQuote.Checked)
			{
				tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 8);

				// Disattiva pulsanti eventualmente attive
				miLinearQuote.Checked = false;
				miAngleQuote.Checked = false;

				clickNumber = 0;

				mDraw.gVDCom.RadiusQuote();
			}
			else
			{
				// Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
				tsslOperation.Text = "";
			}
        }

        private void miAngleQuote_Click(object sender, EventArgs e)
        {
            if (miAngleQuote.Checked)
            {
                tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 9);

                // Disattiva pulsanti eventualmente attive
                miLinearQuote.Checked = false;
                miRadiusQuote.Checked = false;

                clickNumber = 0;

				mDraw.gVDCom.AngleQuote();
            }
            else
            {
                // Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
                tsslOperation.Text = "";
            }
        }

		private void miLinearQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.LinearQuoting = miLinearQuote.Checked;
		}

		private void miRadiusQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.RadiusQuoting = miRadiusQuote.Checked;
		}

		private void miAngleQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.AngleQuoting = miAngleQuote.Checked;
		}

        private void miAutoQuote_Click(object sender, EventArgs e)
        {
            Detail det;
            int i;

            if (dataGridDetails.Bookmark >= 0) 
				i = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
			else
				return;

            mDetails.GetObjectTable(i, out det);

            // Verifica se il pulsante � cliccato
            if (miAutoQuote.Checked)
            {
                mDraw.OnOffLayer(VDInterface.Layers.PROCESSING.ToString() + det.gCodeOriginalShape.Trim(), true);
                mDraw.OnOffLayer(VDInterface.Layers.QUOTAS.ToString() + det.gCodeOriginalShape.Trim(), false);
            }
            else
            {
                mDraw.OnOffLayer(VDInterface.Layers.PROCESSING.ToString() + det.gCodeOriginalShape.Trim(), false);
                mDraw.OnOffLayer(VDInterface.Layers.QUOTAS.ToString() + det.gCodeOriginalShape.Trim(), true);
            }
        }
        
        private void miReset_Click(object sender, EventArgs e)
        {
            miLinearQuote.Checked = false;
            miRadiusQuote.Checked = false;
            miAngleQuote.Checked = false;

            // Ridisegna tutto
            if (miProject.Checked)
            {
                DrawProject();
            }

            CellValue();

            tsslOperation.Text = "";
        }
		
		/// <summary>
		/// Visualizza le quote impostate in DesignMaster
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void miDesignQuote_Click(object sender, EventArgs e)
		{
			int i;
			Detail det;

			if (miProject.Checked)
			{
				if (miDesignQuote.Checked)
				{
					// Visualizza le quote del intero progetto
					mDetails.MoveFirst();
					while (!mDetails.IsEOF())
					{
						mDetails.GetObject(out det);
						mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), false);
						mDetails.MoveNext();
					}

					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString(), false);
				}
				else
				{
					mDetails.MoveFirst();
					while (!mDetails.IsEOF())
					{
						mDetails.GetObject(out det);
						mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), true);
						mDetails.MoveNext();
					}

					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString(), true);
				}
			}
			else
			{
				// Visualizza le quote del singolo pezzo
				if (dataGridDetails.Bookmark >= 0)
					i = (int)dataGridDetails[dataGridDetails.Bookmark, 0];
				else
					return;

				mDetails.GetObjectTable(i, out det);

				// Verifica se il pulsante � cliccato
				if (miDesignQuote.Checked)
					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), false);
				else
					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), true);
			}

			mDraw.ZoomAll();
		}
		#endregion
	}
}
