using System;
using System.IO;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Xml;
using Breton.DbAccess;
using Breton.ImportOffer;
using Breton.Parameters;
using Breton.Users;
using Breton.TraceLoggers;
using Breton.MathUtils;

namespace Breton.OfferManager.Form
{
	public class Offers : System.Windows.Forms.Form
	{
		#region Variables
		private System.Windows.Forms.ToolBar toolBar;
		private System.Windows.Forms.ToolBarButton btnNuovo;
		private System.Windows.Forms.ToolBarButton btnModifica;
		private System.Windows.Forms.ToolBarButton btnSalva;
		private System.Windows.Forms.ToolBarButton btnElimina;
		private System.Windows.Forms.ToolBarButton btnReset;
		private System.Windows.Forms.ToolBarButton btnAnnulla;
		private System.Windows.Forms.ImageList imlToolIcon16;
		private System.Windows.Forms.ImageList imlToolIcon;
		private System.Windows.Forms.ToolBarButton btnImporta;
		private System.Windows.Forms.Panel panelData;
		private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.ToolBarButton btnCreaOrdine;
		private System.Windows.Forms.OpenFileDialog openXml;		
		private System.Data.DataSet dataSetOffers;
		private System.Data.DataTable dataTableOffers;
		private System.Data.DataColumn T_ID;
		private System.Data.DataColumn F_ID;
		private System.Data.DataColumn F_CODE;
		private System.Data.DataColumn F_DATE;
		private System.Data.DataColumn F_STATE;
		private System.Data.DataColumn F_NOTE;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridOffers;
		private System.Windows.Forms.ToolBarButton btnMostraOrdine;
		private System.ComponentModel.IContainer components;
		private DataColumn dataColumnF_REVISION;

		private DbInterface mDbInterface = null;
		private User mUser = null;
		private ParameterControlPositions mFormPosition;
		private OfferCollection mOffers;
		private string mLanguage;
		private static bool formCreate = false;
		private bool isModified = false;
        private DataColumn dataColumnF_DESCRIPTION;		
		private const string xmlFileName = "OFFER.xml";
        private const string xmlFileNameMod = "OFFER_Mod.xml";
        private DirectoryInfo dirXml;
        private string tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
        private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;		

        private EventHandler ordHandler;
		#endregion

		public Offers(DbInterface db, User user, string language)
		{
			InitializeComponent();

			if (db == null)
			{
				mDbInterface = new DbInterface();
				mDbInterface.Open("", "DbConnection.udl");
			}
			else
				mDbInterface = db;

			// Carica la posizione del form
			mFormPosition = new ParameterControlPositions(this.Name);
			mFormPosition.LoadFormPosition(this);

			mUser = user;

			mLanguage = language;
			ProjResource.gResource.ChangeLanguage(mLanguage);

            ordHandler = new EventHandler(UpdateOrders);
            Breton.OfferManager.Form.Orders.UpdateOrders += ordHandler;
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Offers));
			this.toolBar = new System.Windows.Forms.ToolBar();
			this.btnNuovo = new System.Windows.Forms.ToolBarButton();
			this.btnImporta = new System.Windows.Forms.ToolBarButton();
			this.btnModifica = new System.Windows.Forms.ToolBarButton();
			this.btnSalva = new System.Windows.Forms.ToolBarButton();
			this.btnElimina = new System.Windows.Forms.ToolBarButton();
			this.btnReset = new System.Windows.Forms.ToolBarButton();
			this.btnAnnulla = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.btnCreaOrdine = new System.Windows.Forms.ToolBarButton();
			this.btnMostraOrdine = new System.Windows.Forms.ToolBarButton();
			this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
			this.imlToolIcon16 = new System.Windows.Forms.ImageList(this.components);
			this.panelData = new System.Windows.Forms.Panel();
			this.dataGridOffers = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.dataTableOffers = new System.Data.DataTable();
			this.T_ID = new System.Data.DataColumn();
			this.F_ID = new System.Data.DataColumn();
			this.F_CODE = new System.Data.DataColumn();
			this.F_DATE = new System.Data.DataColumn();
			this.F_STATE = new System.Data.DataColumn();
			this.F_NOTE = new System.Data.DataColumn();
			this.dataColumnF_DESCRIPTION = new System.Data.DataColumn();
			this.dataColumnF_REVISION = new System.Data.DataColumn();
			this.openXml = new System.Windows.Forms.OpenFileDialog();
			this.dataSetOffers = new System.Data.DataSet();
			this.panelData.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridOffers)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableOffers)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetOffers)).BeginInit();
			this.SuspendLayout();
			// 
			// toolBar
			// 
			this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnNuovo,
            this.btnImporta,
            this.btnModifica,
            this.btnSalva,
            this.btnElimina,
            this.btnReset,
            this.btnAnnulla,
            this.toolBarButton1,
            this.btnCreaOrdine,
            this.btnMostraOrdine});
			this.toolBar.DropDownArrows = true;
			this.toolBar.ImageList = this.imlToolIcon;
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.ShowToolTips = true;
			this.toolBar.Size = new System.Drawing.Size(640, 44);
			this.toolBar.TabIndex = 1;
			this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
			// 
			// btnNuovo
			// 
			this.btnNuovo.ImageIndex = 0;
			this.btnNuovo.Name = "btnNuovo";
			this.btnNuovo.ToolTipText = "Nuova";
			this.btnNuovo.Visible = false;
			// 
			// btnImporta
			// 
			this.btnImporta.ImageIndex = 1;
			this.btnImporta.Name = "btnImporta";
			this.btnImporta.ToolTipText = "Importa XML";
			// 
			// btnModifica
			// 
			this.btnModifica.ImageIndex = 2;
			this.btnModifica.Name = "btnModifica";
			this.btnModifica.ToolTipText = "Modifica";
			this.btnModifica.Visible = false;
			// 
			// btnSalva
			// 
			this.btnSalva.ImageIndex = 3;
			this.btnSalva.Name = "btnSalva";
			this.btnSalva.ToolTipText = "Salva";
			// 
			// btnElimina
			// 
			this.btnElimina.ImageIndex = 4;
			this.btnElimina.Name = "btnElimina";
			this.btnElimina.ToolTipText = "Elimina";
			// 
			// btnReset
			// 
			this.btnReset.ImageIndex = 5;
			this.btnReset.Name = "btnReset";
			this.btnReset.ToolTipText = "Annulla operazioni effettuate";
			// 
			// btnAnnulla
			// 
			this.btnAnnulla.ImageIndex = 6;
			this.btnAnnulla.Name = "btnAnnulla";
			this.btnAnnulla.ToolTipText = "Esci";
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.Name = "toolBarButton1";
			this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnCreaOrdine
			// 
			this.btnCreaOrdine.ImageIndex = 7;
			this.btnCreaOrdine.Name = "btnCreaOrdine";
			this.btnCreaOrdine.ToolTipText = "Crea ordine di lavoro";
			// 
			// btnMostraOrdine
			// 
			this.btnMostraOrdine.ImageIndex = 8;
			this.btnMostraOrdine.Name = "btnMostraOrdine";
			this.btnMostraOrdine.ToolTipText = "Mostra ordine di lavoro";
			// 
			// imlToolIcon
			// 
			this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
			this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon.Images.SetKeyName(0, "");
			this.imlToolIcon.Images.SetKeyName(1, "");
			this.imlToolIcon.Images.SetKeyName(2, "");
			this.imlToolIcon.Images.SetKeyName(3, "");
			this.imlToolIcon.Images.SetKeyName(4, "");
			this.imlToolIcon.Images.SetKeyName(5, "");
			this.imlToolIcon.Images.SetKeyName(6, "");
			this.imlToolIcon.Images.SetKeyName(7, "");
			this.imlToolIcon.Images.SetKeyName(8, "");
			// 
			// imlToolIcon16
			// 
			this.imlToolIcon16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon16.ImageStream")));
			this.imlToolIcon16.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon16.Images.SetKeyName(0, "");
			this.imlToolIcon16.Images.SetKeyName(1, "");
			this.imlToolIcon16.Images.SetKeyName(2, "");
			this.imlToolIcon16.Images.SetKeyName(3, "");
			this.imlToolIcon16.Images.SetKeyName(4, "");
			this.imlToolIcon16.Images.SetKeyName(5, "");
			this.imlToolIcon16.Images.SetKeyName(6, "");
			this.imlToolIcon16.Images.SetKeyName(7, "");
			this.imlToolIcon16.Images.SetKeyName(8, "");
			// 
			// panelData
			// 
			this.panelData.Controls.Add(this.dataGridOffers);
			this.panelData.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelData.Location = new System.Drawing.Point(0, 44);
			this.panelData.Name = "panelData";
			this.panelData.Size = new System.Drawing.Size(640, 274);
			this.panelData.TabIndex = 2;
			// 
			// dataGridOffers
			// 
			this.dataGridOffers.AllowSort = false;
			this.dataGridOffers.AllowUpdate = false;
			this.dataGridOffers.CaptionHeight = 17;
			this.dataGridOffers.DataSource = this.dataTableOffers;
			this.dataGridOffers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridOffers.FetchRowStyles = true;
			this.dataGridOffers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dataGridOffers.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridOffers.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridOffers.Images"))));
			this.dataGridOffers.Location = new System.Drawing.Point(0, 0);
			this.dataGridOffers.Name = "dataGridOffers";
			this.dataGridOffers.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridOffers.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridOffers.PreviewInfo.ZoomFactor = 75;
			this.dataGridOffers.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridOffers.PrintInfo.PageSettings")));
			this.dataGridOffers.RowHeight = 15;
			this.dataGridOffers.Size = new System.Drawing.Size(640, 274);
			this.dataGridOffers.TabIndex = 0;
			this.dataGridOffers.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridOffers_FetchRowStyle);
			this.dataGridOffers.PropBag = resources.GetString("dataGridOffers.PropBag");
			// 
			// dataTableOffers
			// 
			this.dataTableOffers.Columns.AddRange(new System.Data.DataColumn[] {
            this.T_ID,
            this.F_ID,
            this.F_CODE,
            this.F_DATE,
            this.F_STATE,
            this.F_NOTE,
            this.dataColumnF_DESCRIPTION,
            this.dataColumnF_REVISION});
			this.dataTableOffers.TableName = "TableOffers";
			// 
			// T_ID
			// 
			this.T_ID.ColumnName = "T_ID";
			// 
			// F_ID
			// 
			this.F_ID.Caption = "Id";
			this.F_ID.ColumnName = "F_ID";
			this.F_ID.DataType = typeof(int);
			// 
			// F_CODE
			// 
			this.F_CODE.Caption = "Code";
			this.F_CODE.ColumnName = "F_CODE";
			// 
			// F_DATE
			// 
			this.F_DATE.Caption = "Data";
			this.F_DATE.ColumnName = "F_DATE";
			this.F_DATE.DataType = typeof(System.DateTime);
			// 
			// F_STATE
			// 
			this.F_STATE.Caption = "Stato";
			this.F_STATE.ColumnName = "F_STATE";
			// 
			// F_NOTE
			// 
			this.F_NOTE.Caption = "Note";
			this.F_NOTE.ColumnName = "F_NOTE";
			// 
			// dataColumnF_DESCRIPTION
			// 
			this.dataColumnF_DESCRIPTION.Caption = "Descrizione";
			this.dataColumnF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
			// 
			// dataColumnF_REVISION
			// 
			this.dataColumnF_REVISION.ColumnName = "F_REVISION";
			this.dataColumnF_REVISION.DataType = typeof(int);
			// 
			// openXml
			// 
			this.openXml.Filter = "XML files (*.xml) |*.xml";
			this.openXml.Multiselect = true;
			this.openXml.RestoreDirectory = true;
			this.openXml.Title = "Seleziona il file Xml";
			// 
			// dataSetOffers
			// 
			this.dataSetOffers.DataSetName = "NewDataSet";
			this.dataSetOffers.Locale = new System.Globalization.CultureInfo("en-US");
			this.dataSetOffers.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableOffers});
			// 
			// Offers
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(640, 318);
			this.Controls.Add(this.panelData);
			this.Controls.Add(this.toolBar);
			this.HelpButton = true;
			this.Name = "Offers";
			this.Text = "Offerte";
			this.Load += new System.EventHandler(this.Offers_Load);
			this.Closed += new System.EventHandler(this.Offers_Closed);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Offers_Closing);
			this.panelData.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridOffers)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableOffers)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetOffers)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion

		#region Properties

		public static bool gCreate
		{
			set	{ formCreate = value; }
			get { return formCreate; }
		}

		#endregion

		#region Form Function

		private void Offers_Load(object sender, System.EventArgs e)
		{
			// Carica le lingue del form
			ProjResource.gResource.LoadStringForm(this);
			openXml.Title = ProjResource.gResource.LoadFixString(this,3);
            mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridOffers, dataTableOffers);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridOffers);

            dirXml = new DirectoryInfo(tmpXmlDir);
            if (!dirXml.Exists)
            {
                dirXml.Create();
                dirXml.Attributes = FileAttributes.Hidden;
            }

			//Legge le offerte dal db
			mOffers = new OfferCollection(mDbInterface, mUser);
			if (mOffers.GetDataFromDb(Offer.Keys.F_ID))
				FillTable();	//Riempe la Table

			gCreate = true;
		}

		private void Offers_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DialogResult dlgClose;
			string message ,caption;

			//Se ci sono state delle modifiche viene chiesto il salvataggio
			if (isModified)
			{				
				ProjResource.gResource.LoadMessageBox(0, out caption, out message);
				dlgClose = MessageBox.Show(message,caption,MessageBoxButtons.YesNoCancel,MessageBoxIcon.Question);
				if (dlgClose == DialogResult.Yes)
				{
					if (!mOffers.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
					{
						ProjResource.gResource.LoadMessageBox(1, out caption, out message);
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				else if (dlgClose == DialogResult.Cancel)
					e.Cancel=true;				//Non viene effettuato il salvataggio e la form rimane aperta
			}

		}

		private void Offers_Closed(object sender, System.EventArgs e)
		{
			mFormPosition.SaveFormPosition(this);
            mSortGrid = null;
			mColumnView = null;

            if (dirXml.Exists &&
                   !Breton.TechnoMaster.Form.Working.gCreate &&
                   !Breton.OfferManager.Form.Projects.gCreate &&
                   !Breton.OfferManager.Form.Details.gCreate &&
                   !Breton.OfferManager.Form.DetailsAdvance.gCreate &&
                   !Breton.OfferManager.Form.Orders.gCreate)
                dirXml.Delete(true);

			gCreate = false;
		}

		#endregion

		#region Gestione Tabelle

		private void FillTable()
		{
			Offer off;
		
			dataTableOffers.Clear();

			// Scorre tutte le offerte lette
			mOffers.MoveFirst();
			while (!mOffers.IsEOF())
			{
				// Legge l'offerta attuale
				mOffers.GetObject(out off);
				
				dataTableOffers.BeginInit();
				dataTableOffers.BeginLoadData();
				// Aggiunge una riga alla volta
				AddRow(ref off);
				
				dataTableOffers.EndInit();
				dataTableOffers.EndLoadData();
			}

			dataGridOffers.Bookmark = 0;
		}

		private void AddRow(ref Offer off)
		{
			// Array con i dati della riga
			object [] myArray = new object[8];
			myArray[0] = dataTableOffers.Rows.Count;
			myArray[1] = off.gId;
			myArray[2] = off.gCode;
			myArray[3] = off.gInsertDate;
			myArray[4] = StateEnumstrToString(off.gStateOff);
			myArray[5] = off.gNote;
            myArray[6] = off.gDescription;
			myArray[7] = (off.gRevision >= 0 ? off.gRevision : 0);

			// Crea una nuova riga nella tabella e la riempie di dati
			DataRow r = dataTableOffers.NewRow();
			r.ItemArray = myArray;
			dataTableOffers.Rows.Add(r);
			

			// Assegna l'id della riga tabella alla classe materiale
			off.gTableId = int.Parse(r.ItemArray[0].ToString());

			// Passa all'elemento successivo
			mOffers.MoveNext();
		}

		private void Delete()		
		{
			Offer off;
            Order ord;
            OrderCollection mOrderDelete = new OrderCollection(mDbInterface, mUser);
			int j=0,l=0;
			string message, caption;

			if (dataGridOffers.Bookmark >= 0)
			{
				// Indice della riga da eliminare
				int[] indexDelete = new int[0];

				while (j<dataGridOffers.SelectedRows.Count)
				{
                    mOffers.GetObjectTable(int.Parse(dataGridOffers[dataGridOffers.SelectedRows[j], 0].ToString()), out off);
                    mOrderDelete.GetSingleElementFromDb(Order.CODE_PREFIX + off.gCode.Replace(Offer.CODE_PREFIX, ""));
                    mOrderDelete.GetObject(out ord);
                    if (ord == null || ord.gStateOrd.Trim() != Order.State.ORDER_IN_PROGRESS.ToString())
                    {
                        int[] tmp = new int[indexDelete.Length + 1];
                        indexDelete.CopyTo(tmp, 0);
                        indexDelete = tmp;
                        indexDelete[l] = off.gTableId;
                        l++;
                    }
					j++;
				}
				if (dataGridOffers.SelectedRows.Count == 0)
				{
                    mOffers.GetObjectTable(int.Parse(dataGridOffers[dataGridOffers.Bookmark, 0].ToString()), out off);
                    mOrderDelete.GetSingleElementFromDb(Order.CODE_PREFIX + off.gCode.Replace(Offer.CODE_PREFIX, ""));
                    mOrderDelete.GetObject(out ord);
                    if (ord == null || ord.gStateOrd.Trim() != Order.State.ORDER_IN_PROGRESS.ToString())
                    {
                        indexDelete = new int[1];
                        indexDelete[j] = off.gTableId;
                    }
				}

				ProjResource.gResource.LoadMessageBox(this, 2, out caption, out message);
				if (indexDelete.Length>1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					for (int i=0;i<indexDelete.Length;i++)
					{
						if(!mOffers.DeleteObjectTable(indexDelete[i]))
						{
							TraceLog.WriteLine("Offers: Delete() Error");
						}
					}
					isModified = true;
					ResetIndexTable();
				}
				else if (indexDelete.Length==1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo,MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					// Elimina la riga dalla collection e dalla tabella
					mOffers.DeleteObjectTable(indexDelete[0]);
					dataTableOffers.Rows.RemoveAt(indexDelete[0]);			
					isModified = true;
					ResetIndexTable();
				}		
				FillTable();
			}
		}
		
		private void ResetIndexTable()
		{
			Offer off;
			int i=0;

			mOffers.MoveFirst();
			while (!mOffers.IsEOF())
			{
				// Legge l'offerta attuale
				mOffers.GetObject(out off);
				
				off.gTableId = i;
				i++;
				mOffers.MoveNext();
			}
			for (i=0; i<=dataTableOffers.Rows.Count-1; i++)
			{
				object [] myArray = new object[8];
				myArray = dataTableOffers.Rows[i].ItemArray;
				myArray[0] = i;
				dataTableOffers.Rows[i].ItemArray = myArray;
			}
		}

		#endregion

		#region ToolBar
		private void toolBar_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			string message, caption;
			switch(toolBar.Buttons.IndexOf(e.Button))
			{
				//Importa offerta
				case 1:
					XmlDocument originalDoc = new XmlDocument();
					string[] xmlFiles;
					Offer off = new Offer();
                    Offer tmpO = new Offer();
                    //OfferCollection tmpOffers = new OfferCollection(mDbInterface, mUser);
					Offer.errorType error;
                    ArrayList offCode = new ArrayList();
                    bool reload = true;
					string user;
		
					if (openXml.ShowDialog() == DialogResult.OK)
					{
						xmlFiles = openXml.FileNames;
					}
					else
						return;

                    // Verifica se � necessario fare un aggiornamento del database prima di proseguire
                    if (isModified)
                    {
                        ProjResource.gResource.LoadMessageBox(this, 6, out caption, out message);
                        if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (!mOffers.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
                            {
                                ProjResource.gResource.LoadMessageBox(1, out caption, out message);
                                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            else
                                isModified = false;
                        }
                        else
                            return;
                    }

					//Importazione dei files Xml
					for (int i = 0; i < xmlFiles.Length; i++)
					{
						off = new Offer();
						off.ReadFileXml(xmlFiles[i], mDbInterface, out error);

						if (error == Offer.errorType.DUPLICATE_CODE)
						{
							if (!mOffers.IsCorrectUser(off, out user) && !mUser.gIsAdministrator)
							{
								ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);
								MessageBox.Show(message + " " + user.ToUpper(), caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}

							ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
							if (MessageBox.Show(message + " (" + off.gCode + ")", caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
							{
								// Trova l'oggetto gi� esistente
								mOffers.MoveFirst();
								while (!mOffers.IsEOF())
								{
									mOffers.GetObject(out tmpO);
									if (tmpO.gCode.Trim() == off.gCode.Trim())
										break;
									mOffers.MoveNext();
								}

								string reloadingFile = tmpXmlDir + "\\" + off.gCode.Trim() + "_ReloadingOffer.xml";
								mOffers.GetXmlParameterFromDb(tmpO, reloadingFile);

								originalDoc.Load(xmlFiles[i]);
								if (DeleteNodes(ref originalDoc))
									originalDoc.Save(tmpXmlDir + "\\" + xmlFileNameMod);

								ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
								// Verifica se il nuovo file che si sta importando � identico a quello gia esistente
								if (GenUtils.FileCompare(reloadingFile, tmpXmlDir + "\\" + xmlFileNameMod) && MessageBox.Show(message + " (" + off.gCode + ")", caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
									return;

								// L'utente ha deciso di sovrascrivere comunque l'offerta
								tmpO.gInsertDate = mDbInterface.DateTimeNow;

								if (tmpO.gStateOff.Trim() == Offer.State.OFFER_TO_ANALYZE.ToString() ||
									tmpO.gStateOff.Trim() == Offer.State.OFFER_ERROR.ToString() ||
									tmpO.gStateOff.Trim() == Offer.State.OFFER_RELOADED.ToString())
								{
									tmpO.gCode = off.gCode;
									tmpO.gDescription = off.gDescription;
									//tmpO.gRevision = off.gRevision;

									//originalDoc.Load(xmlFiles[i]);
									//if (DeleteNodes(ref originalDoc))
									//    originalDoc.Save(tmpXmlDir + "\\" + xmlFileNameMod);

									tmpO.gXmlParameterLink = tmpXmlDir + "\\" + xmlFileNameMod;
									if (tmpO.gStateOff.Trim() == Offer.State.OFFER_ERROR.ToString())
									{
										tmpO.gStateOff = Offer.State.OFFER_TO_ANALYZE.ToString();
										tmpO.gNote = "";
									}
									if (tmpO.gStateOff.Trim() != Offer.State.OFFER_RELOADED.ToString())
										offCode.Add(tmpO.gCode);

									reload = false;
								}
								else if (tmpO.gStateOff.Trim() == Offer.State.OFFER_ANALYZED.ToString())
								{
									//string reloadingFile = tmpXmlDir + "\\" + off.gCode.Trim() + "_ReloadingOffer.xml";
									//mOffers.GetXmlParameterFromDb(tmpO, reloadingFile);
									// Commentata la parte per il controllo dei file perch� se a volte viene reimportato un file non tutti i pezzi 
									// potrebbero essere pronti alla modifica e quindi dovr� essere possibile importarlo nuovamente

									// Verifica se il nuovo file che si sta importando � identico a quello gia esistente
									//if (!GenUtils.FileCompare(reloadingFile, xmlFiles[i]))
									//{
									// Analisi nuovo file xml
									//originalDoc.Load(xmlFiles[i]);
									//if (DeleteNodes(ref originalDoc))
									//    originalDoc.Save(tmpXmlDir + "\\" + xmlFileNameMod);

									if (off.gRevision >= 0)
									{
										if (off.gRevision > tmpO.gRevision)
											tmpO.gRevision = off.gRevision;
										else
										{
											ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
											MessageBox.Show(message + " (" + off.gCode + ")", caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
											break;
										}
									}
									else
										tmpO.gRevision++;

									// Dovr� essere visualizzata una finestra dialog e quindi attendere la chiusura del form
									ReloadOrder frmReload = new ReloadOrder(mDbInterface, mUser, mLanguage, tmpO, tmpXmlDir + "\\" + xmlFileNameMod);
									if (frmReload.gShowDialog() == DialogResult.Yes)
									{
										tmpO.gCode = off.gCode;

										tmpO.gXmlParameterLink = tmpXmlDir + "\\" + xmlFileNameMod;
										mOffers.UpdateDataToDb();
										
										FillTable();
									}
									else
									{

										tmpO.gCode = off.gCode;
										tmpO.gDescription = off.gDescription;
										//tmpO.gRevision = off.gRevision;

										//originalDoc.Load(xmlFiles[i]);
										//if (DeleteNodes(ref originalDoc))
										//    originalDoc.Save(tmpXmlDir + "\\" + xmlFileNameMod);

										tmpO.gXmlParameterLink = tmpXmlDir + "\\" + xmlFileNameMod;
										tmpO.gStateOff = Offer.State.OFFER_RELOADED.ToString();
										reload = false;
									}
								}
							}
							else
							{
								reload = true;
							}
						}
						else
						{
							originalDoc.Load(xmlFiles[i]);
							if (DeleteNodes(ref originalDoc))
								originalDoc.Save(tmpXmlDir + "\\" + xmlFileNameMod);

							off.gStateOff = Offer.State.OFFER_TO_ANALYZE.ToString();
							off.gXmlParameterLink = xmlFiles[i];
							off.gRevision = 0;
							offCode.Add(off.gCode);
							mOffers.AddObject(off);
							reload = false;
						}
					}

                    if (reload)
                        break;

					if (!mOffers.UpdateDataToDb())
					{
						ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
						break;
					}
					else
					{
						FillTable();

						ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
						if (offCode.Count > 0 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							for (int k = 0; k < offCode.Count; k++)
								MakeOrder(offCode[k].ToString());
						}
					}
					break;

				//Salva
				case 3:
					ProjResource.gResource.LoadMessageBox(0, out caption, out message);
					if (isModified && MessageBox.Show(message, caption, MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
					{
                        this.Cursor = Cursors.WaitCursor;
						if(!mOffers.UpdateDataToDb())
						{
							ProjResource.gResource.LoadMessageBox(1, out caption, out message);
							MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Cursor = Cursors.Default;
							return;
						}
						isModified=false;
						mOffers.MoveFirst();
						FillTable();
                        this.Cursor = Cursors.Default;
					}
					
					break;

				//Elimina offerta
				case 4:
                    this.Cursor = Cursors.WaitCursor;
					Delete();
                    this.Cursor = Cursors.Default;
					break;

				//Reset
				case 5:
					ProjResource.gResource.LoadMessageBox(2, out caption, out message);
					if (isModified &&
						MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes
						&& mOffers.GetDataFromDb(Offer.Keys.F_ID))
					{						
						FillTable();
						isModified=false;			
					}
					break;
				
				// Esci
				case 6:
					Close();
					break;

				// Crea Ordine
				case 8:
					int j=0;

					if (dataGridOffers.Bookmark >= 0)
					{
						// Indice della riga da eliminare
						int[] indexAnalyze = new int[0];

						while (j<dataGridOffers.SelectedRows.Count)
						{
							int[] tmp = new int[indexAnalyze.Length+1];
							indexAnalyze.CopyTo(tmp,0);
							indexAnalyze = tmp;						
							mOffers.GetObjectTable(int.Parse(dataGridOffers[dataGridOffers.SelectedRows[j],0].ToString()), out off);
							indexAnalyze[j] = off.gTableId;
							j++;
						}
						if (dataGridOffers.SelectedRows.Count == 0)
						{
							indexAnalyze = new int[1];
							mOffers.GetObjectTable(int.Parse(dataGridOffers[dataGridOffers.Bookmark,0].ToString()), out off);
							indexAnalyze[j] = off.gTableId;
						}				
			
						for (int i=0;i<indexAnalyze.Length;i++)
						{
							MakeOrder(indexAnalyze[i]);
						}
					}
					
					break;

				//Mostra Ordine
				case 9:
					if (!Orders.gCreate)
					{
						Orders frmOrders = new Orders(mDbInterface, mUser, mLanguage);
						frmOrders.MdiParent = this.MdiParent;
						frmOrders.Show();
					}
					else
					{
						AttivaForm("Orders");
					}
					break;
			}

		}
		#endregion

		#region Eventi dei controlli

		private void dataGridOffers_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
		{
			// Valore dello stato alla riga e.Row (stato � il valore alla colonna 4 del dataGridOffers)
			string strState = (dataGridOffers[e.Row,"F_STATE"].ToString());
			Offer.State state = StateStringToEnum(strState);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
			switch(state)
			{
				//Da analizzare
				case Offer.State.OFFER_TO_ANALYZE:
					e.CellStyle.BackColor = Color.Yellow;
                    e.CellStyle.BackColor2 = Color.LightYellow;
					break;
				//Analizzato
				case Offer.State.OFFER_ANALYZED:
					e.CellStyle.BackColor = Color.LightGreen;
                    e.CellStyle.BackColor2 = Color.LimeGreen;
					break;
				//Analizzato ma con problemi
				case Offer.State.OFFER_ERROR:
					e.CellStyle.BackColor = Color.Red;
                    e.CellStyle.BackColor2 = Color.Coral;
					break;
                // Ricaricato dopo aver fatto modifiche
                case Offer.State.OFFER_RELOADED:
                    e.CellStyle.BackColor = Color.Orange; 
                    e.CellStyle.BackColor2 = Color.Gold;
                    break;
				default:
					e.CellStyle.BackColor = Color.White;
                    e.CellStyle.BackColor2 = Color.WhiteSmoke;
					break;
			}
		}

		private string StateEnumToString(Offer.State st)
		{
			switch (st)
			{
				// Da analizzare
				case Offer.State.OFFER_TO_ANALYZE:
					return ProjResource.gResource.LoadFixString(this,0);
				// Analizzato
				case Offer.State.OFFER_ANALYZED:
					return ProjResource.gResource.LoadFixString(this,1);
				//Analizzato ma con problemi
				case Offer.State.OFFER_ERROR:
					return ProjResource.gResource.LoadFixString(this,2);
                //Ricaricato dopo aver fatto modifiche
                case Offer.State.OFFER_RELOADED:
                    return ProjResource.gResource.LoadFixString(this, 7);
				default:
					return "";
			}
		}

		private string StateEnumstrToString(string st)
		{
			if (st.Trim() == Offer.State.OFFER_TO_ANALYZE.ToString())		// Da analizzare
				return ProjResource.gResource.LoadFixString(this,0);
			else if (st.Trim() == Offer.State.OFFER_ANALYZED.ToString())	// Analizzato
				return ProjResource.gResource.LoadFixString(this,1);
			else if (st.Trim() == Offer.State.OFFER_ERROR.ToString())		// Analizzato ma con problemi
				return ProjResource.gResource.LoadFixString(this,2);
            else if (st.Trim() == Offer.State.OFFER_RELOADED.ToString())	// Ricaricato dopo aver fatto modifiche
                return ProjResource.gResource.LoadFixString(this, 7);
			else
				return "";
		}

		private Offer.State StateStringToEnum(string state)
		{
			if(state == ProjResource.gResource.LoadFixString(this,0))		// Da analizzare
				return Offer.State.OFFER_TO_ANALYZE;
			else if (state == ProjResource.gResource.LoadFixString(this,1))	// Analizzato
				return Offer.State.OFFER_ANALYZED;
			else if(state == ProjResource.gResource.LoadFixString(this,2))	// Analizzato ma con problemi
				return Offer.State.OFFER_ERROR;
            else if (state == ProjResource.gResource.LoadFixString(this, 7))// Ricaricato dopo aver fatto modifiche
                return Offer.State.OFFER_RELOADED;
			else
				return Offer.State.NONE;
		}

		#endregion

		#region Creazione dell'ordine

        private void MakeOrder(string offCode)
        {
            Offer off;
            int offTableId = -1;

            mOffers.MoveFirst();
            while (!mOffers.IsEOF())
            {
                mOffers.GetObject(out off);
                if (off.gCode.Trim() == offCode.Trim())
                {
                    offTableId = off.gTableId;
                    break;
                }

                mOffers.MoveNext();
            }

            if (offTableId >= 0)
                MakeOrder(offTableId);
        }

		private void MakeOrder(int offId)
		{
			string message, caption;
			Project.Message msg = Project.Message.NONE;
			Offer off;
			Order ord = new Order(mDbInterface);
            OrderCollection mOrders = new OrderCollection(mDbInterface, mUser);
			XmlDocument originalDoc = new XmlDocument();

			mOffers.GetObjectTable(offId, out off);
			if(off.gStateOff.Trim() == Offer.State.OFFER_ANALYZED.ToString())
			{
				ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
				MessageBox.Show(message + " " + off.gCode, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

            this.Cursor = Cursors.WaitCursor;
			try
			{
                // Se l'offerta � da analizzare procedo con la semplice creazione dell'ordine da xml
                if (off.gStateOff.Trim() == Offer.State.OFFER_TO_ANALYZE.ToString() || off.gStateOff.Trim() == Offer.State.OFFER_ERROR.ToString())
                {

					mOffers.GetXmlParameterFromDb(off, tmpXmlDir + "\\" + xmlFileName);
                    originalDoc.Load(tmpXmlDir + "\\" + xmlFileName);
                    if (DeleteNodes(ref originalDoc))
                        originalDoc.Save(tmpXmlDir +"\\"+ xmlFileNameMod);

                    //Legge il file xml appena caricato dal db e imposta i valori interessati nell'oggetto off
					if (ord.ReadFileXml(tmpXmlDir + "\\" + xmlFileNameMod, out msg))
                    {	// Creazione ordine eseguita con successo
                        // Genero il codice ordine modificando il prefisso del codice offerta                   
                        ord.gCode = Order.CODE_PREFIX + off.gCode.Replace(Offer.CODE_PREFIX, "");
                        ord.gDescription = off.gDescription;
                        ord.gStateOrd = Order.State.ORDER_LOADED.ToString();	// Stato ordine di default
                        ord.gOfferCode = off.gCode;							    // Offerta a cui � relativo l'ordine
						
                        mOrders.AddObject(ord);
						if (!mOrders.UpdateDataToDb())
							throw new ApplicationException("Error on order save");

						//if (!mOrders.GetSingleElementFromDb(ord.gCode))
						//    throw new ApplicationException("Error on order load");

						//if (!CreateArticleforDetail(mOrders))
						//    throw new ApplicationException("Error create articles for details");

                        if (msg == Project.Message.DA_CONFERMARE)
                            off.gNote = ProjResource.gResource.LoadFixString(this, 4);
                        off.gStateOff = Offer.State.OFFER_ANALYZED.ToString();
                        if (mOffers.UpdateDataToDb())	//Aggiorno nel database l'offerta con lo stato modificato
                            FillTable();
                    }
                    else
                    {
                        throw new ApplicationException("Error on Orders Creation");
                    }
					//if (transaction)
					//    mDbInterface.EndTransaction(true);
                }
                else if (off.gStateOff.Trim() == Offer.State.OFFER_RELOADED.ToString())
                {
                    string reloadFile = tmpXmlDir + "\\" + off.gCode.Trim() + "_ReloadOffer.xml";
                    
                    mOffers.GetXmlParameterFromDb(off, reloadFile);

                    ReloadOrder frmReload = new ReloadOrder(mDbInterface, mUser, mLanguage, off, reloadFile);
                    if (frmReload.gShowDialog() == DialogResult.Yes)
                    {
                        off.gStateOff = Offer.State.OFFER_ANALYZED.ToString();
                        if (mOffers.UpdateDataToDb())
                            FillTable();
                    }
                }
			}
			catch(Exception ex)
			{
				TraceLog.WriteLine("Offers.MakeOrder: ", ex);
                
				//if (transaction)
				//    mDbInterface.EndTransaction(false);
				
                //Creazione ordine fallita
				off.gStateOff = Offer.State.OFFER_ERROR.ToString();
                if (msg == Project.Message.NO_MATERIAL)
                    off.gNote += ProjResource.gResource.LoadFixString(this, 5);
                if (msg == Project.Message.NO_CUSTOMER)
                    off.gNote += ProjResource.gResource.LoadFixString(this, 6);
				if (msg == Project.Message.NO_CUT_TO_SIZE)
					off.gNote += ProjResource.gResource.LoadFixString(this, 8);

				if (mOffers.UpdateDataToDb())	//Aggiorno nel database l'offerta con lo stato modificato
					FillTable();
                
			}
			finally
			{
				originalDoc = null;
                this.Cursor = Cursors.Default;
			}
		}

		#endregion

        #region Gestione Eventi
        private void UpdateOrders(object sender, EventArgs ev)
        {
            if (mOffers.GetDataFromDb(Offer.Keys.F_ID))
                FillTable();	//Riempe la Table
        }
        #endregion

        #region Abilita-Disabilita vari controlli

        private void AttivaForm(string formName)
		{
			System.Windows.Forms.Form[] frm = new System.Windows.Forms.Form[this.ParentForm.MdiChildren.Length];
			frm =this.ParentForm.MdiChildren;
			for (int i=0;i<frm.Length;i++)
			{
				if(frm[i].Name == formName)
					frm[i].Activate();						
			}
		}

		#endregion

		#region Private Methods

		//**********************************************************************
		// DeleteNodes
		// Cancella tutti i nodi che non interessano a questo livello
		// Parametri:
		//			doc		: xml document da modificare
		//**********************************************************************
		private bool DeleteNodes(ref XmlDocument doc)
		{
			ArrayList nodes = new ArrayList();
			XmlNode node = doc.SelectSingleNode("/EXPORT");

			try
			{
				foreach(XmlNode nd in node.ChildNodes)
				{
					if(nd.Name.ToUpperInvariant() != "FABMASTER")
						nodes.Add(nd);
				}

				node.RemoveAll();
				for(int i =0;i<nodes.Count;i++)
					node.AppendChild((XmlNode)nodes[i]);
				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("Offers.DeleteNodes Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Offers.DeleteNodes Error. ", ex);
				return false;
			}
		}

        /// <summary>
        /// Crea tutti gli articoli per i dettagli presenti negli ordini passati
        /// </summary>
        /// <param name="orders">Collection da cui estrarre i dettagli</param>
        /// <returns></returns>
        private bool CreateArticleforDetail(OrderCollection orders)
        {
            Order ord;
            DetailCollection details = new DetailCollection(mDbInterface);

            try
            {
                orders.MoveFirst();
                while (!orders.IsEOF())
                {
                    orders.GetObject(out ord);
                    if (!details.GetDataFromDbFromOrderCode(ord.gCode))
                        throw new ApplicationException("Error get details from order code");

                    if (!details.CreateArticles())
                        throw new ApplicationException("Error create articles");

                    orders.MoveNext();
                }
                return true;
            }
            catch(Exception ex)
            {
                TraceLog.WriteLine("Offers.CreateArticleforDetail ", ex);
                return false;
            }
        }

		#endregion
	}
}
