using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.TraceLoggers;
using Breton.ImportOffer;
using Breton.Customers_Suppliers;
using Breton.DesignUtil;
using Breton.MgzLevel;
using Breton.Articles;
using Breton.KeyboardInterface;
using VDProLib5;

namespace Breton.OfferManager.Form
{
    public partial class OrdersSlabs : System.Windows.Forms.Form
    {
        #region Variables
        private static bool formCreate;
        private DbInterface mDbInterface;
        private User mUser;
        private string mLanguage;
        private ParameterControlPositions mFormPosition;
        private ParameterCollection mParam, mParamApp;
        private DetailCollection mDetails;
        private ProjectCollection mProjects;
        private OrderCollection mOrders;
        private CustomerCollection mCustomers;
        private string tmpDetDir = System.IO.Path.GetTempPath() + "OrdersSlabs";
        private DirectoryInfo dirDet;
        private Painter mDraw;
        private Shape mShape;
		private FieldCollection mImpost = null;
		private Field fGridThickness, fGridFinalThickness, fGridLength, fGridWidth;
		private bool imperial = false;	
        private C1Utils.TDBGridSort mSortGridOrd;
        private C1Utils.TDBGridSort mSortGridArt;
		private C1Utils.TDBGridColumnView mColumnViewOrd;
		private C1Utils.TDBGridColumnView mColumnViewArt;
		private string[] allPiece;
		private bool coord_assolute;
		private bool mProjectDraw;
		private int clickNumber = 0;
		private string mFileXml;
		private string mCurrentPrj;
        #endregion

        public OrdersSlabs(DbInterface db, User user, string language)
        {
            InitializeComponent();

            if (db == null)
            {
                mDbInterface = new DbInterface();
                mDbInterface.Open("", "DbConnection.udl");
            }
            else
                mDbInterface = db;

            // Carica la posizione del form
            mFormPosition = new ParameterControlPositions(this.Name);
            mFormPosition.LoadFormPosition(this);

            mUser = user;

            mLanguage = language;
            ProjResource.gResource.ChangeLanguage(mLanguage);
        }

        #region Properties
        public static bool gCreate
        {
            set { formCreate = value; }
            get { return formCreate; }
        }
        #endregion

		#region Open Form & Load Parameters
		public new void ShowDialog()
		{
			if (LoadParameters())
				base.ShowDialog();
		}

		public new void Show()
		{
			if (LoadParameters())
				base.Show();
		}

		private bool LoadParameters()
		{
			Parameter par;
			string message, caption;

			ProjResource.gResource.LoadMessageBox(3, out caption, out message);

			mParam = new ParameterCollection(mDbInterface, true);
			while (!mParam.GetDataFromDb(null, null, "NUMBER", "IMPERIAL"))
			{
				if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
					return false;
			}

			if (mParam.GetObject(null, null, "NUMBER", "IMPERIAL", out par))
				imperial = par.gBoolValue;
			else
			{
				par = new Parameter(null, null, "NUMBER", "IMPERIAL", Parameter.ParTypes.BOOL, "0", "Sistema metrico in uso");
				mParam.AddObject(par);
				mParam.UpdateDataToDb();	// Aggiorna il DB con il parametro inserito
			}


			return true;
		}

		#endregion

        #region Form Function
        private void OrdersSlabs_Load(object sender, EventArgs e)
        {
            Parameter par;
			LoadContextMenuString();
            // Carica le lingue del form
            ProjResource.gResource.LoadStringForm(this);

            mOrders = new OrderCollection(mDbInterface, null);  // Utente nullo perch� non � necessario nascondere a un dato utente altri ordini quando si va a vederli sulle lastre
            mProjects = new ProjectCollection(mDbInterface);
            mDetails = new DetailCollection(mDbInterface);
            mCustomers = new CustomerCollection(mDbInterface);
            mParamApp = new ParameterCollection(mDbInterface, false);
            mSortGridOrd = new Breton.C1Utils.TDBGridSort(c1DataGridOrders, dataTableOrders);
			mColumnViewOrd = new Breton.C1Utils.TDBGridColumnView(c1DataGridOrders);
			mSortGridArt = new Breton.C1Utils.TDBGridSort(c1DataGridSlabs, dataTableArticles);
			mColumnViewArt = new Breton.C1Utils.TDBGridColumnView(c1DataGridSlabs);

            if (mParamApp.GetDataFromDb("", "", "LAST_CHOICE", "ORDER_SLAB") && mParamApp.GetObject(out par) && par != null)
            {
                // Cerca ordini
                if (par.gIntValue == 0)
                {
                    panelSearchOrders.Visible = true;
                    panelDataOrders.Visible = true;
                }
                // Cerca lastre
                else if (par.gIntValue == 1)
                {
                    panelSearchSlabs.Visible = true;
                    panelDataSlabs.Visible = true;
                }
            }
            else
            {
                panelSearchSlabs.Visible = true;
                panelDataSlabs.Visible = true;
            }

			mDraw = new Painter(VDPreview, imperial);

			// Carica l'ultima scelta per quanto riguarda la visualizzazione o meno del progetto
			if (mParamApp.GetDataFromDb("", "", "GRAPHICS", "SHOW_PROJECT") &&
					mParamApp.GetObject("", "", "GRAPHICS", "SHOW_PROJECT", out par))
				miProject.Checked = par.gBoolValue;

			SetKeyboardControl();

            dirDet = new DirectoryInfo(tmpDetDir);
            dirDet.Create();
            dirDet.Attributes = FileAttributes.Hidden;

            gCreate = true;
        }

        private void OrdersSlabs_FormClosed(object sender, FormClosedEventArgs e)
        {
            Parameter par;

			miLinearQuote.Checked = false;
			miRadiusQuote.Checked = false;
			miAngleQuote.Checked = false;

            mFormPosition.SaveFormPosition(this);
            mSortGridOrd = null;
            mSortGridArt = null;
			mColumnViewOrd = null;
			mColumnViewArt = null;
            gCreate = false;

            if (mParamApp.GetDataFromDb("", "", "LAST_CHOICE", "ORDER_SLAB") &&
				mParamApp.GetObject(out par))
            {
                if (panelSearchOrders.Visible)
                    par.gIntValue = 0;
                else if (panelSearchSlabs.Visible)
                    par.gIntValue = 1;
            }
            else
            {
                par = new Parameter(Environment.MachineName, Application.ProductName, "LAST_CHOICE", "ORDER_SLAB", Parameter.ParTypes.STRING, (panelSearchOrders.Visible ? "0" : "1"), "");
                mParamApp.AddObject(par);
            }
            mParamApp.UpdateDataToDb();

            mDraw = null;

            if (dirDet.Exists)
                dirDet.Delete(true);
        }
        #endregion

        #region Gestione Tabelle
        private void FillTableOrders()
        {
            Detail det;

            dataTableOrders.Clear();

            // Scorre tutti i dettagli letti
            mDetails.MoveFirst();
            while (!mDetails.IsEOF())
            {
                // Legge il progetto attuale
                mDetails.GetObject(out det);

                dataTableOrders.BeginInit();
                dataTableOrders.BeginLoadData();
                // Aggiunge una riga alla volta
                AddOrderRow(ref det);

                dataTableOrders.EndInit();
                dataTableOrders.EndLoadData();

                // Passa all'elemento successivo
                mDetails.MoveNext();
            }
        }

        private void AddOrderRow(ref Detail det)
        {
            Order ord;
            Customer cst;
            // Array con i dati della riga
            object[] myArray = new object[15];
            myArray[0] = dataTableOrders.Rows.Count;
            myArray[1] = det.gId;
            ord = GetOrderFromDetail(det);
            if (ord == null) return;
            myArray[2] = ord.gCode;
            myArray[3] = ord.gDescription;
            cst = GetCutomerFromOrder(ord);
            if (cst == null) return;
            myArray[4] = cst.gCode;
            myArray[5] = cst.gName;
            myArray[6] = det.gCode;
            myArray[7] = det.gDescription;
            myArray[8] = det.gArticleCode;
            myArray[9] = MagDescription(det.gArticleCode);
			fGridThickness.DoubleValue = det.gThickness;
			myArray[10] = double.Parse(fGridThickness.ToString());
			fGridFinalThickness.DoubleValue = det.gFinalThickness;
			myArray[11] = double.Parse(fGridFinalThickness.ToString());
			fGridLength.DoubleValue = det.gLength;
			myArray[12] = double.Parse(fGridLength.ToString());
			fGridWidth.DoubleValue = det.gWidth;
			myArray[13] = double.Parse(fGridWidth.ToString());
			myArray[14] = det.gCodeOriginalShape;

            // Crea una nuova riga nella tabella e la riempie di dati
            DataRow r = dataTableOrders.NewRow();
            r.ItemArray = myArray;
            dataTableOrders.Rows.Add(r);

            // Assegna l'id della riga tabella al dettaglio
            det.gTableId = int.Parse(r.ItemArray[0].ToString());
        }

        private void FillTableSlabs()
        {
            Detail det;

            dataTableArticles.Clear();

            // Scorre tutti i dettagli trovati letti
            mDetails.MoveFirst();
            while (!mDetails.IsEOF())
            {
                // Legge il progetto attuale
                mDetails.GetObject(out det);

                dataTableArticles.BeginInit();
                dataTableArticles.BeginLoadData();
                // Aggiunge una riga alla volta
                AddSlabRow(ref det);

                dataTableArticles.EndInit();
                dataTableArticles.EndLoadData();
            }
        }

        private void AddSlabRow(ref Detail det)
        {
            // Array con i dati della riga
            object[] myArray = new object[12];
            myArray[0] = dataTableArticles.Rows.Count;
            myArray[1] = det.gId;
            myArray[2] = mDetails.GetSourceArticleFromDetail(det.gCode);
            myArray[3] = det.gCode;
            myArray[4] = det.gDescription;
            myArray[5] = det.gArticleCode;
            myArray[6] = MagDescription(det.gArticleCode);
			fGridThickness.DoubleValue = det.gThickness;
			myArray[7] = double.Parse(fGridThickness.ToString());
			fGridFinalThickness.DoubleValue = det.gFinalThickness;
			myArray[8] = double.Parse(fGridFinalThickness.ToString());
			fGridLength.DoubleValue = det.gLength;
			myArray[9] = double.Parse(fGridLength.ToString());
			fGridWidth.DoubleValue = det.gWidth;
			myArray[10] = double.Parse(fGridWidth.ToString());
			myArray[11] = det.gCodeOriginalShape;

            // Crea una nuova riga nella tabella e la riempie di dati
            DataRow r = dataTableArticles.NewRow();
            r.ItemArray = myArray;
            dataTableArticles.Rows.Add(r);

            // Assegna l'id della riga tabella al dettaglio
            det.gTableId = int.Parse(r.ItemArray[0].ToString());

            // Passa all'elemento successivo
            mDetails.MoveNext();
        }

        #endregion

        #region Toolbar
        private void toolBar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            switch (toolBar.Buttons.IndexOf(e.Button))
            {
                // Cerca lastre
                case 0:
                    this.AcceptButton = btnConfermaOrdine;
                    panelSearchSlabs.Visible = true;
                    panelDataSlabs.Visible = true;
                    if (panelSearchOrders.Visible)
                    {
                        panelSearchOrders.Visible = false;
                        panelDataOrders.Visible = false;
                    }
                    break;

                // Cerca ordini
                case 1:
                    this.AcceptButton = btnConfermaArticolo;
                    panelSearchOrders.Visible = true;
                    panelDataOrders.Visible = true;
                    if (panelSearchSlabs.Visible)
                    {
                        panelSearchSlabs.Visible = false;
                        panelDataSlabs.Visible = false;
                    }
                    break;

                // Esci
                case 3:
                    Close();
                    break;
            }
        }
        #endregion

        #region Eventi dei controlli
        private void btnConfermaOrdine_Click(object sender, EventArgs e)
        {
            string orderCode = txtOrderCode.Text;
            if (!mDetails.GetDataFromDbFromOrderCode(orderCode))
                TraceLog.WriteLine("Error get details from order code");
            else
            {
                FillTableSlabs();
				miProject_CheckedChanged(null, null);
                CellSlabsValue();
            }
        }

        private void btnConfermaArticolo_Click(object sender, EventArgs e)
        {
            string[] details;
            string articleCode = txtArticleCode.Text;
            if (!mDetails.GetArticleDetails(articleCode, out details))
                TraceLog.WriteLine("Error get details from source article");

            if (details != null && !mDetails.GetDataFromDb(details))
                TraceLog.WriteLine("Error get details from DB");
			else
            {
				if (details == null)
				{
					mDetails.Clear();
					// Cancella il disegno
					mDraw.gVDCom.EraseAll();
					mDraw.gVDCom.Redraw();
				}
                FillTableOrders();
				miProject_CheckedChanged(null, null);
                CellOrdersValue();
            }
        }

        private void c1DataGridSlabs_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
        {
            CellSlabsValue();
        }

        private void CellSlabsValue()
        {
			VDProLib5.vdLayer layer;
            Detail det;
            int i;

            if (c1DataGridSlabs.Bookmark >= 0)
                i = (int)c1DataGridSlabs[c1DataGridSlabs.Bookmark, 0];
            else
                return;

            try
            {
                if (mDetails.GetObjectTable(i, out det))
                {
					if (miProject.Checked)
					{
						if (coord_assolute)
						{
							if (det.gCustomerProjectCode.Trim() != mCurrentPrj)
							{
								mCurrentPrj = det.gCustomerProjectCode.Trim();
								mProjectDraw = false;
							}

							if (!mProjectDraw)
								DrawProject();

							if (mDraw.gVDCom.gCurrentLayer != null)
								mDraw.gVDCom.DeselectLayer(mDraw.gVDCom.gCurrentLayer);
							mDraw.gVDCom.SelectLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), 4, 5);

							mDraw.QuotasOff();
							mDraw.LayersProcessing(true);
						}
						else
						{
							// Spegne tutti i layer
							for (int j = 0; j < mDraw.gVDCom.LayersCount; j++)
							{
								layer = mDraw.gVDCom.Layer(j);
								mDraw.OnOffLayer(layer.LayerName, true);
							}
							mDraw.gVDCom.ActiveLayer = VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim();
							// Accende il layer interessato
							mDraw.OnOffLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), false);
							mDraw.OnOffLayer(VDInterface.Layers.TEXT.ToString() + det.gCodeOriginalShape.Trim(), false);
						}
					}
					else
					{
						// Estrae l'xml del pezzo interessato
						mFileXml = tmpDetDir + "\\" + det.gCode.Trim() + ".xml";
						if (!File.Exists(mFileXml) && !mDetails.GetXmlParameterFromDb(det, mFileXml))
							throw new ApplicationException("Error getting xml");

						mDraw.DrawXml(mFileXml, true, false, false, true, true); // Disegna il pezzo
						mDraw.ZoomAll();
					}

					miLinearQuote_Click(null, null);
					miRadiusQuote_Click(null, null);
					miAngleQuote_Click(null, null);

					miAutoQuote.Checked = false;
					miDesignQuote.Checked = false;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("OrdersSlabs.CellSlabsValue() :" + ex.Message);
            }
        }

        private void c1DataGridOrders_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
        {
            CellOrdersValue();
        }

		private void CellOrdersValue()
		{
			VDProLib5.vdLayer layer;
			Detail det;
			int i;

			if (c1DataGridOrders.Bookmark >= 0)
				i = (int)c1DataGridOrders[c1DataGridOrders.Bookmark, 0];
			else
				return;

			try
			{

				if (mDetails.GetObjectTable(i, out det))
				{
					if (miProject.Checked)
					{
						if (coord_assolute)
						{
							if (det.gCustomerProjectCode.Trim() != mCurrentPrj)
							{
								mCurrentPrj = det.gCustomerProjectCode.Trim();
								mProjectDraw = false;
							}

							if (!mProjectDraw)
								DrawProject();

							if (mDraw.gVDCom.gCurrentLayer != null)
								mDraw.gVDCom.DeselectLayer(mDraw.gVDCom.gCurrentLayer);
							mDraw.gVDCom.SelectLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), 4, 5);

							mDraw.QuotasOff();
							mDraw.LayersProcessing(true);
						}
						else
						{
							// Spegne tutti i layer
							for (int j = 0; j < mDraw.gVDCom.LayersCount; j++)
							{
								layer = mDraw.gVDCom.Layer(j);
								mDraw.OnOffLayer(layer.LayerName, true);
							}
							mDraw.gVDCom.ActiveLayer = VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim();
							// Accende il layer interessato
							mDraw.OnOffLayer(VDInterface.Layers.SHAPE.ToString() + det.gCodeOriginalShape.Trim(), false);
							mDraw.OnOffLayer(VDInterface.Layers.TEXT.ToString() + det.gCodeOriginalShape.Trim(), false);
						}
					}
					else
					{
						// Estrae l'xml del pezzo interessato
						mFileXml = tmpDetDir + "\\" + det.gCode.Trim() + ".xml";
						if (!File.Exists(mFileXml) && !mDetails.GetXmlParameterFromDb(det, mFileXml))
							throw new ApplicationException("Error getting xml");

						mDraw.DrawXml(mFileXml, true, false, false, true, true); // Disegna il pezzo
						mDraw.ZoomAll();
					}
					miLinearQuote_Click(null, null);
					miRadiusQuote_Click(null, null);
					miAngleQuote_Click(null, null);

					miAutoQuote.Checked = false;
					miDesignQuote.Checked = false;
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("OrdersSlabs.CellOrdersValue() :" + ex.Message);
			}
		}

        private void VDPreview_Resize(object sender, EventArgs e)
        {
            if (mDraw != null)
                mDraw.ZoomAll();
        }

		private void tsslblOrtho_Click(object sender, EventArgs e)
		{
			// Imposta l'ortogonalit� al layer attivo
			if (mDraw.gVDCom.OrthoMode)
			{
				tsslblOrtho.Text = "ORTHO: Off";
				mDraw.gVDCom.OrthoMode = false;
			}
			else
			{
				tsslblOrtho.Text = "ORTHO: On";
				mDraw.gVDCom.OrthoMode = true;
			}
		}

		private void VDPreview_MouseWheelEvent(object sender, AxVDProLib5._DVdrawEvents_MouseWheelEvent e)
		{
			if (miLock.Checked)
				e.cancel = 1;
		}

		private void VDPreview_MouseDownEvent(object sender, AxVDProLib5._DVdrawEvents_MouseDownEvent e)
		{
			Detail det;
			string codeOriginalDet;
			
			// Se il click non avviene per posizionare una quota
			if (!miLinearQuote.Checked && !miRadiusQuote.Checked && !miAngleQuote.Checked)
			{
				codeOriginalDet = mDraw.gVDCom.SelectLayerClick();

				if (codeOriginalDet == null)
					return;


				mDetails.MoveFirst();
				while (!mDetails.IsEOF())
				{
					mDetails.GetObject(out det);
					if (det.gCodeOriginalShape.Trim() == codeOriginalDet && det.gCustomerProjectCode.Trim() == mCurrentPrj.Trim())
					{
						if (panelSearchOrders.Visible)
							c1DataGridOrders.Bookmark = SelectBookmark(det);
						else
							c1DataGridSlabs.Bookmark = SelectBookmark(det);

						break;
					}
					mDetails.MoveNext();
				}
			}
			// Se � stato cliccato il tasto destro azzero il conteggio
			if (e.button == 2)
			{
				clickNumber = 0;
				return;
			}

			clickNumber++;
			// Se deve inserire una quota lineare si aspetta 3 click
			if (miLinearQuote.Checked)
			{
				if (clickNumber < 3)
				{
					if (!mDraw.gVDCom.GetSnapPoint())
						clickNumber = 0;
				}
				else
					clickNumber = 0;
			}
			// Se deve inserire una quota angolare si aspetta 4 click
			else if (miAngleQuote.Checked)
			{
				if (clickNumber < 4)
				{
					if (!mDraw.gVDCom.GetSnapPoint())
						clickNumber = 0;
				}
				else
					clickNumber = 0;
			}
		}
        #endregion

        #region Private Methods
        /// <summary>
        /// Restituisce l'ordine a cui si riferisce il dettaglio interessato
        /// </summary>
        /// <param name="det">Dettaglio da ricercare</param>
        /// <returns></returns>
        private Order GetOrderFromDetail(Detail det)
        {
            Order ord;
            Project prj;

            try
            {
                if (!mProjects.GetSingleElementFromDb(det.gCustomerProjectCode))
                    throw new ApplicationException("Error get project from db");

                mProjects.GetObject(out prj);

                if (prj != null)
                {
                    if(!mOrders.GetSingleElementFromDb(prj.gCustomerOrderCode))
                        throw new ApplicationException("Error get order from db");

                    mOrders.GetObject(out ord);
                    return ord;
                }

                return null;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("OrdersSlabs.GetOrderFromDetail", ex);
                return null;
            }
        }

        private Customer GetCutomerFromOrder(Order ord)
        {
            Customer cst;

            try
            {
                if (!mCustomers.GetSingleElementFromDb(ord.gCustomerCode))
                    throw new ApplicationException("Error get customer from db");

                mCustomers.GetObject(out cst);

                return cst;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("OrdersSlabs.GetCutomerFromOrder", ex);
                return null;
            }
        }

		private void SetKeyboardControl()
		{
			mImpost = new FieldCollection(imperial);

			mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//DimZ
			mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//FinalThickenss
			mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//Length
			mImpost.AddNumericField(null, 0, 0, 0, 0, KeyboardInterface.MeasureUnit.MU.MM, "0.", false);	//Width

			fGridThickness = mImpost.GetField(0);
			fGridFinalThickness = mImpost.GetField(1);
			fGridLength = mImpost.GetField(2);
			fGridWidth = mImpost.GetField(3);


			c1DataGridSlabs.Columns[7].Caption += " (" + fGridThickness.GetMeasureUnit() + ")";
			c1DataGridSlabs.Columns[8].Caption += " (" + fGridFinalThickness.GetMeasureUnit() + ")";
			c1DataGridSlabs.Columns[9].Caption += " (" + fGridLength.GetMeasureUnit() + ")";
			c1DataGridSlabs.Columns[10].Caption += " (" + fGridWidth.GetMeasureUnit() + ")";

			c1DataGridOrders.Columns[10].Caption += " (" + fGridThickness.GetMeasureUnit() + ")";
			c1DataGridOrders.Columns[11].Caption += " (" + fGridFinalThickness.GetMeasureUnit() + ")";
			c1DataGridOrders.Columns[12].Caption += " (" + fGridLength.GetMeasureUnit() + ")";
			c1DataGridOrders.Columns[13].Caption += " (" + fGridWidth.GetMeasureUnit() + ")";
		}

		private void ResetVal()
		{
			fGridThickness.EnterValue("");
			fGridFinalThickness.EnterValue("");
			fGridLength.EnterValue("");
			fGridWidth.EnterValue("");
		}

		/// <summary>
		/// Ricerca la riga nella griglia che identifica l'oggetto
		/// </summary>
		/// <param name="det">oggetto da ricercare nella griglia</param>
		/// <returns></returns>
		private int SelectBookmark(Detail det)
		{
			if (panelSearchOrders.Visible)
			{
				for (int i = 0; i <dataTableOrders.Rows.Count; i++)
				{
					if ((int)c1DataGridOrders[i, 0] == det.gTableId)
						return i;
				}
			}
			else
			{
				for (int i = 0; i < dataTableArticles.Rows.Count; i++)
				{
					if ((int)c1DataGridSlabs[i, 0] == det.gTableId)
						return i;
				}
			}

			return -1;
		}
        #endregion

        #region Gestione magazzino

        private string MagDescription(string articleCode)
        {
            Breton.Mgz.MgzUtility magUt = new Breton.Mgz.MgzUtility(mDbInterface);
            return magUt.MagDescription(articleCode);
        }

        #endregion

		#region Context Menu
		private void LoadContextMenuString()
		{
			miProject.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 0);
			miZoomAll.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 1);
			miLock.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 2);
			miZoom.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 3);
			mi10.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 4);
			mi50.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 5);
			mi100.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 6);
			mi150.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 7);
			mi200.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 8);
			mi300.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 9);
			miQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 10);
			miLinearQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 11);
			miRadiusQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 12);
			miAngleQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 13);
			miAutoQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 14);
			miReset.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 15);
			miDesignQuote.Text = ProjResource.gResource.LoadFixString("contextMenuDraw", 16);
		}


		private void miProject_CheckedChanged(object sender, EventArgs e)
		{
			Detail det;
			int i = -1;
			Parameter par;

			miLinearQuote.Checked = false;
			miRadiusQuote.Checked = false;
			miAngleQuote.Checked = false;
			tsslOperation.Text = "";

			// Disegna tutto il progetto
			if (miProject.Checked)
			{
			    if (panelSearchOrders.Visible)
			    {
			        if (c1DataGridOrders.Bookmark >= 0)
			            i = (int)c1DataGridOrders[c1DataGridOrders.Bookmark, 0];
			    }
			    else
			    {
			        if (c1DataGridSlabs.Bookmark >= 0)
			            i = (int)c1DataGridSlabs[c1DataGridSlabs.Bookmark, 0];
			    }
				if (mDetails.GetObjectTable(i, out det))
				{
					mCurrentPrj = det.gCustomerProjectCode.Trim();
					DrawProject();
				}
			}

			if (panelSearchOrders.Visible)
				CellOrdersValue();
			else
				CellSlabsValue();

			if (mParamApp.GetDataFromDb("", "", "GRAPHICS", "SHOW_PROJECT") &&
				 mParamApp.GetObject("", "", "GRAPHICS", "SHOW_PROJECT", out par))
			{
				par.gBoolValue = miProject.Checked;
			}
			else
			{
				par = new Parameter(Environment.MachineName, Application.ProductName, "GRAPHICS", "SHOW_PROJECT", Parameter.ParTypes.INT, miProject.Checked.ToString(), "");
				mParamApp.AddObject(par);
			}
			mParamApp.UpdateDataToDb();
		}

		private void DrawProject()
		{
			string fileOffer;
			OfferCollection offers = new OfferCollection(mDbInterface, mUser);
			Offer off;
			OrderCollection orders = new OrderCollection(mDbInterface, mUser);
			Order ord;
			ProjectCollection projects = new ProjectCollection(mDbInterface);
			Project prj;
			DetailCollection tmpDetails = new DetailCollection(mDbInterface);
			Detail det = null;
			int i = 0;
			string[] tmp;
			ArrayList detailUsed = new ArrayList();


			allPiece = new string[0];
			mDetails.MoveFirst();
			while (!mDetails.IsEOF())
			{
				mDetails.GetObject(out det);

				mFileXml = tmpDetDir + "\\" + det.gCode.Trim() + ".xml";

				if (!File.Exists(mFileXml) && !mDetails.GetXmlParameterFromDb(det, mFileXml))
					throw new ApplicationException("Error getting xml");

				if (det.gCustomerProjectCode.Trim() == mCurrentPrj.Trim())
				{
					tmp = new string[allPiece.Length + 1];
					allPiece.CopyTo(tmp, 0);
					allPiece = tmp;
					allPiece[i] = mFileXml;
					detailUsed.Add(det.gCodeOriginalShape.Trim());
					i++;
				}

				mDetails.MoveNext();
			}

			Shape shp = new Shape();

			// Verifica che il pezzo che si sta disegnando abbia le coordinate assolute
			if (allPiece != null && allPiece.Length > 0)
			{
				XmlAnalizer.ReadShapeXml(allPiece[0], ref shp, true);

				if (shp.gContours.Count > 0)
				{
					// Disegna tutto il progetto con le coordinate assolute
					mDraw.ShapeColor = 7;
					mDraw.DrawXml(allPiece, true, false, false, true, true);
					mDraw.ZoomAll();
					coord_assolute = true;
				}
				else
				{
					coord_assolute = false;
				}
			}

			// Cerca i pezzi del progetto che non appartengono alla visualizzazione 
			i = 0;
			allPiece = new string[0];
			tmpDetails.GetDataFromDb(Detail.Keys.F_CODE, mCurrentPrj);
			tmpDetails.MoveFirst();
			while (!tmpDetails.IsEOF())
			{
				tmpDetails.GetObject(out det);

					if (detailUsed.IndexOf(det.gCodeOriginalShape.Trim()) < 0)
					{
						mFileXml = tmpDetDir + "\\" + det.gCode.Trim() + ".xml";

						if (!File.Exists(mFileXml) && !tmpDetails.GetXmlParameterFromDb(det, mFileXml))
							throw new ApplicationException("Error getting xml");

						if (det.gCustomerProjectCode.Trim() == mCurrentPrj.Trim())
						{
							tmp = new string[allPiece.Length + 1];
							allPiece.CopyTo(tmp, 0);
							allPiece = tmp;
							allPiece[i] = mFileXml;
							i++;
						}
					}

				tmpDetails.MoveNext();
			}

			// Disegna i pezzi del progetto non evidenziandoli
			if (allPiece != null && allPiece.Length > 0)
			{
				XmlAnalizer.ReadShapeXml(allPiece[0], ref shp, true);

				if (shp.gContours.Count > 0)
				{
					// Disegna tutto il progetto con le coordinate assolute
					mDraw.ShapeColor = 9;
					mDraw.DrawXml(allPiece, true, false, false, true, true);
					mDraw.ZoomAll();
					coord_assolute = true;
				}
				else
				{
					coord_assolute = false;
				}
			}


			if (mCurrentPrj != null)
			{
				// Cerca l'xml dell'intera offerta
				projects.GetSingleElementFromDb(mCurrentPrj);
				projects.GetObject(out prj);
				orders.GetSingleElementFromDb(prj.gCustomerOrderCode);
				orders.GetObject(out ord);
				if (ord != null)
				{
					offers.GetSingleElementFromDb(ord.gOfferCode);
					offers.GetObject(out off);
					fileOffer = tmpDetDir + "\\" + off.gCode.Trim() + ".xml";

					if (!File.Exists(fileOffer))
						offers.GetXmlParameterFromDb(off, tmpDetDir + "\\" + off.gCode.Trim() + ".xml");

					mDraw.DrawTopXml(fileOffer, prj.gCodeOriginalProject.Trim());
				}
			}
			mProjectDraw = true;
		}

		private void miZoomAll_Click(object sender, EventArgs e)
		{
			if (mDraw != null)
				mDraw.ZoomAll();
		}

		private void miLock_Click(object sender, EventArgs e)
		{
			// Verifica se � stato richiesto di bloccare il pannello di disegno
			if (miLock.Checked)
				mDraw.gVDCom.DisableMouseWheelPan = true; // Disabilita l'uso della ruota del mouse
			else
				mDraw.gVDCom.DisableMouseWheelPan = false; // Abilita l'uso della ruota del mouse
		}

		private void mi10_Click(object sender, EventArgs e)
		{
			if (mDraw != null)
				mDraw.ZoomTo(0.10);
		}

		private void mi50_Click(object sender, EventArgs e)
		{
			if (mDraw != null)
				mDraw.ZoomTo(0.50);
		}

		private void mi100_Click(object sender, EventArgs e)
		{
			if (mDraw != null)
				mDraw.ZoomTo(1.00);
		}

		private void mi150_Click(object sender, EventArgs e)
		{
			if (mDraw != null)
				mDraw.ZoomTo(1.50);
		}

		private void mi200_Click(object sender, EventArgs e)
		{
			if (mDraw != null)
				mDraw.ZoomTo(2.00);
		}

		private void mi300_Click(object sender, EventArgs e)
		{
			if (mDraw != null)
				mDraw.ZoomTo(3.00);
		}

		private void miLinearQuote_Click(object sender, EventArgs e)
		{
			if (miLinearQuote.Checked)
			{
				tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 1);

				// Disattiva pulsanti eventualmente attive
				miRadiusQuote.Checked = false;
				miAngleQuote.Checked = false;

				clickNumber = 0;

				mDraw.gVDCom.LinearQuote();
			}
			else
			{
				// Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
				tsslOperation.Text = "";
			}
		}

		private void miRadiusQuote_Click(object sender, EventArgs e)
		{
			if (miRadiusQuote.Checked)
			{
				tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 2);

				// Disattiva pulsanti eventualmente attive
				miLinearQuote.Checked = false;
				miAngleQuote.Checked = false;

				clickNumber = 0;

				mDraw.gVDCom.RadiusQuote();
			}
			else
			{
				// Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
				tsslOperation.Text = "";
			}
		}

		private void miAngleQuote_Click(object sender, EventArgs e)
		{
			if (miAngleQuote.Checked)
			{
				tsslOperation.Text = ProjResource.gResource.LoadFixString(this, 3);

				// Disattiva pulsanti eventualmente attive
				miLinearQuote.Checked = false;
				miRadiusQuote.Checked = false;

				clickNumber = 0;

				mDraw.gVDCom.AngleQuote();
			}
			else
			{
				// Cancella l'operazione in corso
				mDraw.gVDCom.Cancel();
				tsslOperation.Text = "";
			}
		}

		private void miLinearQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.LinearQuoting = miLinearQuote.Checked;
		}

		private void miRadiusQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.RadiusQuoting = miRadiusQuote.Checked;
		}

		private void miAngleQuote_CheckedChanged(object sender, EventArgs e)
		{
			mDraw.gVDCom.AngleQuoting = miAngleQuote.Checked;
		}

		private void miAutoQuote_Click(object sender, EventArgs e)
		{
			Detail det;
			int i;

			if (panelSearchOrders.Visible)
			{
				if (c1DataGridOrders.Bookmark >= 0)
					i = (int)c1DataGridOrders[c1DataGridOrders.Bookmark, 0];
				else
					return;
			}
			else
			{
				if (c1DataGridSlabs.Bookmark >= 0)
					i = (int)c1DataGridSlabs[c1DataGridSlabs.Bookmark, 0];
				else
					return;
			}

			mDetails.GetObjectTable(i, out det);

			// Verifica se il pulsante � cliccato
			if (miAutoQuote.Checked)
			{
				mDraw.OnOffLayer(VDInterface.Layers.PROCESSING.ToString() + det.gCodeOriginalShape.Trim(), true);
				mDraw.OnOffLayer(VDInterface.Layers.QUOTAS.ToString() + det.gCodeOriginalShape.Trim(), false);
			}
			else
			{
				mDraw.OnOffLayer(VDInterface.Layers.PROCESSING.ToString() + det.gCodeOriginalShape.Trim(), false);
				mDraw.OnOffLayer(VDInterface.Layers.QUOTAS.ToString() + det.gCodeOriginalShape.Trim(), true);
			}
		}

		private void miDesignQuote_Click(object sender, EventArgs e)
		{
			int i;
			Detail det;

			if (miProject.Checked)
			{
				if (miDesignQuote.Checked)
				{
					// Visualizza le quote del intero progetto
					mDetails.MoveFirst();
					while (!mDetails.IsEOF())
					{
						mDetails.GetObject(out det);
						mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), false);
						mDetails.MoveNext();
					}

					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString(), false);
				}
				else
				{
					mDetails.MoveFirst();
					while (!mDetails.IsEOF())
					{
						mDetails.GetObject(out det);
						mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), true);
						mDetails.MoveNext();
					}

					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString(), true);
				}
			}
			else
			{
				if (panelSearchOrders.Visible)
				{
					if (c1DataGridOrders.Bookmark >= 0)
						i = (int)c1DataGridOrders[c1DataGridOrders.Bookmark, 0];
					else
						return;
				}
				else
				{
					if (c1DataGridSlabs.Bookmark >= 0)
						i = (int)c1DataGridSlabs[c1DataGridSlabs.Bookmark, 0];
					else
						return;
				}

				mDetails.GetObjectTable(i, out det);

				// Verifica se il pulsante � cliccato
				if (miDesignQuote.Checked)
					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), false);
				else
					mDraw.OnOffLayer(VDInterface.Layers.QUOTASDESIGN.ToString() + det.gCodeOriginalShape.Trim(), true);
			}

			if (mDraw != null)
				mDraw.ZoomAll();
		}

		private void miReset_Click(object sender, EventArgs e)
		{
			Detail det;
			int i = -1;
			miLinearQuote.Checked = false;
			miRadiusQuote.Checked = false;
			miAngleQuote.Checked = false;

			// Ridisegna tutto
			if (miProject.Checked)
			{
				if (panelSearchOrders.Visible)
				{
					if (c1DataGridOrders.Bookmark >= 0)
						i = (int)c1DataGridOrders[c1DataGridOrders.Bookmark, 0];
				}
				else
				{
					if (c1DataGridSlabs.Bookmark >= 0)
						i = (int)c1DataGridSlabs[c1DataGridSlabs.Bookmark, 0];
				}
				if (mDetails.GetObjectTable(i, out det))
				{
					mCurrentPrj = det.gCustomerProjectCode.Trim();
					DrawProject();
				}
			}

			if (panelSearchOrders.Visible)
				CellOrdersValue();
			else
				CellSlabsValue();

			tsslOperation.Text = "";
		}
		#endregion
	}
}