using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.Customers_Suppliers;
using Breton.TraceLoggers;
using Breton.ImportOffer;
using Breton.Articles;
using Breton.Phases;
using Breton.Slabs;
using Breton.Magazzino;
using Breton.Stocks;

namespace Breton.OfferManager.Form
{
    public partial class OrdersAdvance : System.Windows.Forms.Form
    {
        #region Variables
        private static bool formCreate;
        private OrderCollection mOrders;
        private ProjectCollection mProjects;
        private DetailCollection mDetails;
        private CustomerCollection mCustomers;
        private Stock mStock;
        private Coordinate coord;
        private DbInterface mDbInterface;
        private User mUser;
        private string mLanguage;
        private ParameterControlPositions mFormPosition;
        private ParameterCollection mParam = null;
        private ParameterCollection mParamApp = null;
        private bool isDateInsert = false;
        private bool isDateDeliv = false;
        private bool getNodeExpert;
        private bool mDialog;
        private string mLastNode;
        private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;		

        public delegate void SelectOrderAdvanceEventHandler(object sender, string e);		// Gestore dell'evento
        public static event SelectOrderAdvanceEventHandler SelectOrderAdvanceEvent;			// Evento SelectOrderAdvanceEvent
        private Breton.Magazzino.GetNode.SelectNodeEventHandler setNodeHandler;
        private Breton.Magazzino.GetNodeExpert.SelectNodeEventHandler setNodeExpertHandler;
        #endregion

        public OrdersAdvance(DbInterface db, User user, string language, bool dialog)
        {
            InitializeComponent();

            if (db == null)
            {
                mDbInterface = new DbInterface();
                mDbInterface.Open("", "DbConnection.udl");
            }
            else
                mDbInterface = db;

            // Carica la posizione del form
            mFormPosition = new ParameterControlPositions(this.Name);
            mFormPosition.LoadFormPosition(this);

            mUser = user;

            mLanguage = language;
            ProjResource.gResource.ChangeLanguage(mLanguage);

            mDialog = dialog;

            // Delegato per l'evento selectNode
            setNodeHandler = new GetNode.SelectNodeEventHandler(NodeSelected);
            Breton.Magazzino.GetNode.SelectNodeEvent += setNodeHandler;

            // Delegato per l'evento selectNode
            setNodeExpertHandler = new GetNodeExpert.SelectNodeEventHandler(NodeSelected);
            Breton.Magazzino.GetNodeExpert.SelectNodeEvent += setNodeExpertHandler;
        }

        #region Properties

        public static bool gCreate
        {
            set { formCreate = value; }
            get { return formCreate; }
        }

        #endregion

        #region Form function
        private void OrdersAdvance_Load(object sender, EventArgs e)
        {
            // Carica le lingue del form
            ProjResource.gResource.LoadStringForm(this);
            Parameter par;

            mOrders = new OrderCollection(mDbInterface, mUser);
            mProjects = new ProjectCollection(mDbInterface);
            mDetails = new DetailCollection(mDbInterface);
            mCustomers = new CustomerCollection(mDbInterface);
            mStock = new Stock(mDbInterface);
            mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridOrders, dataTableOrders);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridOrders);

			if (mCustomers.GetDataFromDb(Customer_Supplier.Keys.F_CODE) && mCustomers.Count > 0)	// Carica i clienti
			{
				FillComboCustomer();		// e riempie la comboBox
				cmbxCustomerCode.SelectedValue = -1;
				cmbxCustomerName.SelectedValue = -1;
			}

			if (mDialog)
			{
				btnSelectOrder.Visible = true;
				btnCloseOrder.Visible = false;
			}
			else
			{
				btnCloseOrder.Visible = true;
				btnSelectOrder.Visible = false;
			}

            // Carica gli ordini in lavorazione
			if (mOrders.GetDataFromDb(Order.Keys.F_ID)) //Order.Keys.F_CODE, null, null,null, Order.State.ORDER_IN_PROGRESS.ToString()))
                FillTable();				// e riempie la Table

            for (int i = 0; i < (int)Order.State.MAX; i++)
                cmbxState.Items.Add(StateEnumToString((Order.State)i));
            
            //cmbxState.SelectedIndex = StateEnumstrToInt(Order.State.ORDER_IN_PROGRESS.ToString());
          
            mParam = new ParameterCollection(mDbInterface, true); 

            // Carica per l'acquisizione con o senza bolla
            if (mParam.GetDataFromDb("", "", "STOCK", "GET_NODE_EXPERT") &&
                mParam.GetObject("", "", "STOCK", "GET_NODE_EXPERT", out par))
                getNodeExpert = par.gBoolValue;
            else
            {
                par = new Parameter(Environment.MachineName, Application.ProductName, "STOCK", "GET_NODE_EXPERT", Parameter.ParTypes.BOOL, "1", "Visualizza la finestra avanzata per la selezione dei nodi di magazzino (1=true 0=false)");
                mParam.AddObject(par);
                mParam.UpdateDataToDb();	// Aggiorna il DB con il parametro inserito
                getNodeExpert = true;
            }
            
            mParamApp = new ParameterCollection(mDbInterface, false);

            if (mParamApp.GetDataFromDb("", "", "STOCK", "LAST_NODE_SELECT") &&
                mParamApp.GetObject("", "", "STOCK", "LAST_NODE_SELECT", out par))
                mLastNode = par.gStringValue;
            else
            {
                par = new Parameter(Environment.MachineName, Application.ProductName, "STOCK", "LAST_NODE_SELECT", Parameter.ParTypes.STRING, "", "");
                mParamApp.AddObject(par);
                mParamApp.UpdateDataToDb();	// Aggiorna il DB con il parametro inserito
                mLastNode = "";
            }
            gCreate = true;
        }

        private void OrdersAdvance_FormClosed(object sender, FormClosedEventArgs e)
        {
            Parameter par;

            mFormPosition.SaveFormPosition(this);
            mSortGrid = null;
			mColumnView = null;

            if (mParamApp.GetDataFromDb("", "", "STOCK", "LAST_NODE_SELECT") &&
                mParamApp.GetObject("", "", "STOCK", "LAST_NODE_SELECT", out par))
                par.gStringValue = mLastNode;
            mParamApp.UpdateDataToDb();


            Breton.Magazzino.GetNode.SelectNodeEvent -= setNodeHandler;
            Breton.Magazzino.GetNodeExpert.SelectNodeEvent -= setNodeExpertHandler;

            gCreate = false;
        }
        #endregion

        #region  Gestione Tabelle

		private void FillComboCustomer()
		{
			Customer cust;

			ArrayList codeCustomer = new ArrayList();
			ArrayList nameCustomer = new ArrayList();

			cmbxCustomerCode.DataSource = null;
			cmbxCustomerName.DataSource = null;

			cmbxCustomerCode.Items.Clear();
			cmbxCustomerName.Items.Clear();

			// Scorre tutti i clienti letti
			mCustomers.MoveFirst();
			while (!mCustomers.IsEOF())
			{
				// Legge il cliente attuale
				mCustomers.GetObject(out cust);
				cust.gTableId = codeCustomer.Count;

				codeCustomer.Add(cust);

				// Passa all'elemento successivo
				mCustomers.MoveNext();
			}
			cmbxCustomerCode.DataSource = codeCustomer;
			cmbxCustomerCode.DisplayMember = "gCode";
			cmbxCustomerCode.ValueMember = "gTableId";

			// Riordina i clienti per descrizione
			mCustomers.SortByField(Customer.Keys.F_NAME.ToString());

			// Scorre tutti i clienti letti
			mCustomers.MoveFirst();
			while (!mCustomers.IsEOF())
			{
				// Legge il cliente attuale
				mCustomers.GetObject(out cust);
				nameCustomer.Add(cust);

				// Passa all'elemento successivo
				mCustomers.MoveNext();
			}

			cmbxCustomerName.DataSource = nameCustomer;
			cmbxCustomerName.DisplayMember = "gName";
			cmbxCustomerName.ValueMember = "gTableId";
		}


        private void FillTable()
        {
            Order ord;

            dataTableOrders.Clear();

            // Scorre tutti gli ordini letti
            mOrders.MoveFirst();
            while (!mOrders.IsEOF())
            {
                // Legge l'ordine attuale
                mOrders.GetObject(out ord);

                dataTableOrders.BeginInit();
                dataTableOrders.BeginLoadData();
                // Aggiunge una riga alla volta
                AddRow(ref ord);

                dataTableOrders.EndInit();
                dataTableOrders.EndLoadData();
            }
        }

        private void AddRow(ref Order ord)
        {
            // Array con i dati della riga
            object[] myArray = new object[10];
            myArray[0] = dataTableOrders.Rows.Count;
            myArray[1] = ord.gId;
            myArray[2] = ord.gCode;
            myArray[3] = ord.gDate;
            if (ord.gDeliveryDate == DateTime.MaxValue)
                myArray[4] = null;
            else
                myArray[4] = ord.gDeliveryDate;
            myArray[5] = ord.gDescription;
            myArray[6] = ord.gCustomerCode;
            myArray[7] = ord.gOfferCode;
            myArray[8] = StateEnumstrToString(ord.gStateOrd);
            myArray[9] = FindCustomerName(ord.gCustomerCode);

            // Crea una nuova riga nella tabella e la riempie di dati
            DataRow r = dataTableOrders.NewRow();
            r.ItemArray = myArray;
            dataTableOrders.Rows.Add(r);

            // Assegna l'id della riga tabella all'ordine
            ord.gTableId = int.Parse(r.ItemArray[0].ToString());

            // Passa all'elemento successivo
            mOrders.MoveNext();
        }
        #endregion

        #region Toolbar
        private void toolBar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            string caption, message;

            switch (toolBar.Buttons.IndexOf(e.Button))
            {
                // Filtro
                case 0:
                    panelFilter.Visible = true;
                    break;

                // Vedi progetti
                case 1:
                    string orderCode;
                    Order ord;
                    Project prj;
                    int i;

                    ProjectCollection prjColl = new ProjectCollection(mDbInterface);

                    if (dataGridOrders.Bookmark >= 0)
                        i = (int)dataGridOrders[dataGridOrders.Bookmark, 0];
                    else
                        return;

                    // Carica il codice dell'ordine di cui si vogliono visualizzare i progetti
                    mOrders.GetObjectTable(i, out ord);
                    orderCode = ord.gCode.Trim();

                    prjColl.GetDataFromDb(Project.Keys.F_CODE, orderCode);
                    if (prjColl.Count > 1)
                    {
                        if (!ProjectsAdvance.gCreate)
                        {
                            ProjectsAdvance frmProjects = new ProjectsAdvance(mDbInterface, mUser, mLanguage, orderCode, mDialog);
                            if (mDialog)
                            {
								frmProjects.ShowInTaskbar = false;
                                frmProjects.ShowDialog();
								this.Tag = frmProjects.Tag;
								Close();
                            }
                            else
                            {
                                frmProjects.MdiParent = this.MdiParent;
                                frmProjects.Show();
                            }
                        }
                        else
                        {
                            OnSelectOrderAdvance(orderCode);
                            AttivaForm("ProjectsAdvance");
                        }
                    }
                    else if (prjColl.Count == 1)
                    {
                        if (!DetailsAdvance.gCreate)
                        {
                            prjColl.GetObject(out prj);
                            DetailsAdvance frmDetails = new DetailsAdvance(mDbInterface, mUser, mLanguage, prj.gCode, mDialog);
                            if (mDialog)
                            {
                                frmDetails.ShowInTaskbar = false;
                                frmDetails.ShowDialog();
								this.Tag = frmDetails.Tag;
								Close();
                            }
                            else
                            {
                                frmDetails.MdiParent = this.MdiParent;
                                frmDetails.Show();
                            }
                        }
                        else
                        {
                            OnSelectOrderAdvance(orderCode);
                            AttivaForm("DetailsAdvance");
                        }
                    }
                    break;

                // Chiudi ordine
                case 2:
                    WorkPhase wk;
                    Detail det;
                    Article art;
                    string mgz_code;
                    bool transaction = !mDbInterface.TransactionActive();
                    try
                    {
                        if (dataGridOrders.Bookmark >= 0)
                        {
                            if (dataGridOrders.SelectedRows.Count == 0)
                            {
                                if (mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.Bookmark, 0].ToString()), out ord))
                                {
                                    if (ord.gStateOrd.Trim() == Order.State.ORDER_COMPLETED.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_LOADED.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_RELOADED.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_NDISP.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_UNLOADED.ToString())
                                    {
                                        ProjResource.gResource.LoadMessageBox(this, 2, out caption, out message);
                                        MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                for (i = 0; i < dataGridOrders.SelectedRows.Count; i++)
                                {
                                    if (mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.SelectedRows[i], 0].ToString()), out ord))
                                    {
                                        if (ord.gStateOrd.Trim() == Order.State.ORDER_COMPLETED.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_LOADED.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_RELOADED.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_NDISP.ToString() ||
                                        ord.gStateOrd.Trim() == Order.State.ORDER_UNLOADED.ToString())
                                        {
                                            ProjResource.gResource.LoadMessageBox(this, 2, out caption, out message);
                                            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                    }
                                }
                            }

                            ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
                            if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (getNodeExpert)
                                {
                                    GetNodeExpert stkSet = new GetNodeExpert(mDbInterface, mLanguage, mLastNode);
                                    stkSet.ShowInTaskbar = false;
                                    stkSet.ShowDialog();
                                }
                                else
                                {
                                    GetNode stkSet = new GetNode(mDbInterface, mLanguage, mLastNode);
                                    stkSet.ShowInTaskbar = false;
                                    stkSet.ShowDialog();
                                }

                                // verifica che sia stato scelto un nodo del magazzino
                                if (coord == null)
                                    return;

                                if (dataGridOrders.SelectedRows.Count == 0)
                                {
                                    if (mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.Bookmark, 0].ToString()), out ord))
                                    {
                                        this.Cursor = Cursors.WaitCursor;

                                        if (!mProjects.GetDataFromDb(Project.Keys.F_CODE, ord.gCode))
                                            throw new ApplicationException("Error get projects from db");

                                        mProjects.MoveFirst();
										while (!mProjects.IsEOF())
										{
											//Apre la transazione
											if (transaction)
												mDbInterface.BeginTransaction();

											if (mProjects.GetObject(out prj))
											{
												if (!mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
													throw new ApplicationException("Error get details from db");

												mDetails.MoveFirst();
												while (!mDetails.IsEOF())
												{
													WorkPhaseCollection mWorkPhases = new WorkPhaseCollection(mDbInterface);
													ArticleCollection mArticle = new ArticleCollection(mDbInterface);

													mDetails.GetObject(out det);

													if (!mWorkPhases.GetDataFromDb(WorkPhase.Keys.F_SEQUENCE, null, det.gCode, null, null, null))
														throw new ApplicationException("Error get work phases from db");

													mWorkPhases.MoveFirst();
													while (!mWorkPhases.IsEOF())
													{
														mWorkPhases.GetObject(out wk);
														if (!mWorkPhases.SetWorkPhaseHState(wk, WorkPhase.State.PHASE_COMPLETED))
															throw new ApplicationException("Error set work phases completed");
														//wk.gStateWorkPhase = WorkPhase.State.PHASE_COMPLETED.ToString();
														mWorkPhases.MoveNext();
													}

													if (!mWorkPhases.UpdateDataToDb())
														throw new ApplicationException("Error on work phases updated");

													if (!mArticle.GetSingleElementFromDb(det.gArticleCode))
														throw new ApplicationException("Error get single article from db");

													mArticle.GetObject(out art);
													// controlla se l'articolo � presente in magazzino
													mgz_code = mStock.GetStockCode(art.gCode);
													// se si lo sposta nella nuova posizione, altrimenti lo inserisce
													if (mgz_code != null)
													{
														if (!mStock.MoveArticle(art, coord))
															throw new ApplicationException("Error on article move");
													}
													else
													{
														if (!mStock.InsertArticle(art, coord))
															throw new ApplicationException("Error on article insert");
													}

													mDetails.MoveNext();
												}
											}
											mProjects.MoveNext();

											// Chiude la transazione
											if (transaction)
												mDbInterface.EndTransaction(true);
										}

                                        mOrders.RefreshData();
                                        FillTable();
                                        this.Cursor = Cursors.Default;
                                    }
                                }
                                else
                                {
                                    // Apre la transazione
                                    if (transaction)
                                        mDbInterface.BeginTransaction();

                                    this.Cursor = Cursors.WaitCursor;
                                    for (i = 0; i < dataGridOrders.SelectedRows.Count; i++)
                                    {
                                        if (mOrders.GetObjectTable(int.Parse(dataGridOrders[dataGridOrders.SelectedRows[i], 0].ToString()), out ord))
                                        {
                                            if (!mProjects.GetDataFromDb(Project.Keys.F_CODE, ord.gCode))
                                                throw new ApplicationException("Error get projects from db");

                                            mProjects.MoveFirst();
                                            while (!mProjects.IsEOF())
                                            {
                                                if (mProjects.GetObject(out prj))
                                                {
                                                    if (!mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
                                                        throw new ApplicationException("Error get details from db");

                                                    mDetails.MoveFirst();
                                                    while (!mDetails.IsEOF())
                                                    {
                                                        WorkPhaseCollection mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                                        ArticleCollection mArticle = new ArticleCollection(mDbInterface);

                                                        mDetails.GetObject(out det);

                                                        if (!mWorkPhases.GetDataFromDb(WorkPhase.Keys.F_SEQUENCE, null, det.gCode, null, null, null))
                                                            throw new ApplicationException("Error get work phases from db");

                                                        mWorkPhases.MoveFirst();
                                                        while (!mWorkPhases.IsEOF())
                                                        {
                                                            mWorkPhases.GetObject(out wk);
                                                            if (!mWorkPhases.SetWorkPhaseHState(wk, WorkPhase.State.PHASE_COMPLETED))
                                                                throw new ApplicationException("Error set work phases completed");
                                                            //wk.gStateWorkPhase = WorkPhase.State.PHASE_COMPLETED.ToString();
                                                            mWorkPhases.MoveNext();
                                                        }
                                                        if (!mWorkPhases.UpdateDataToDb())
                                                            throw new ApplicationException("Error on work phases updated");

                                                        mArticle.GetSingleElementFromDb(det.gArticleCode);
                                                        mArticle.GetObject(out art);
                                                        // controlla se l'articolo � presente in magazzino
                                                        mgz_code = mStock.GetStockCode(art.gCode);
                                                        // se si lo sposta nella nuova posizione, altrimenti lo inserisce
                                                        if (mgz_code != null)
                                                        {
                                                            if (!mStock.MoveArticle(art, coord))
                                                                throw new ApplicationException("Error on article move");
                                                        }
                                                        else
                                                        {
                                                            if (!mStock.InsertArticle(art, coord))
                                                                throw new ApplicationException("Error on article insert");
                                                        }

                                                        mDetails.MoveNext();
                                                    }
                                                }
                                                mProjects.MoveNext();
                                            }
                                        }
                                    }
                                    // Chiude la transazione
                                    if (transaction)
                                        mDbInterface.EndTransaction(true);

                                    mOrders.RefreshData();
                                    FillTable();
                                    this.Cursor = Cursors.Default;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // Messaggio di errore
                        TraceLog.WriteLine("OrdersAdvance.toolBar_ButtonClick CloseOrder Error:", ex);
                        if (transaction)
                            // Chiude la transazione
                            mDbInterface.EndTransaction(false);

                        ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
                        MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        this.Cursor = Cursors.Default;
                        coord = null;
                    }

                    break;

                // Esci
                case 4:
                    Close();
                    break;

                // Refresh
                case 6:
                    this.Cursor = Cursors.WaitCursor;
                    if (mOrders.RefreshData())
                        FillTable();
                    this.Cursor = Cursors.Default;
                    break;

				// Seleziona ordine
				case 7:
					if (dataGridOrders.Bookmark >= 0)
					{
						mOrders.GetObjectTable((int)dataGridOrders[dataGridOrders.Bookmark, 0], out ord);
						this.Tag = ord.gCode;
					}
					Close(); 
					break;
            }
        }
        #endregion

        #region Eventi dei controlli
        private void btnFiltra_Click(object sender, EventArgs e)
        {
            string fromIns, toIns, fromDel, toDel;
            string state ="";

			Order.AdvanceState scheduleState = Order.AdvanceState.NONE;
            Order.AdvanceState optiState = Order.AdvanceState.NONE;
            Order.AdvanceState useListState = Order.AdvanceState.NONE;

            if (txtCode.Text.Length > 0 || cmbxCustomerCode.Text.Length > 0 || cmbxCustomerName.Text.Length > 0 ||
                cmbxState.SelectedIndex >= 0 || isDateInsert || isDateDeliv || txtDescription.Text.Length > 0)
            {
                DateTime fromInsDate = new DateTime(insertDateFrom.Value.Year, insertDateFrom.Value.Month, insertDateFrom.Value.Day, 0, 0, 0);
                fromIns = mDbInterface.OleDbToDate(fromInsDate);
                DateTime toInsDate = new DateTime(insertDateTo.Value.Year, insertDateTo.Value.Month, insertDateTo.Value.Day, 0, 0, 0);
                toIns = mDbInterface.OleDbToDate(toInsDate);
                DateTime fromDelDate = new DateTime(deliveryDateFrom.Value.Year, deliveryDateFrom.Value.Month, deliveryDateFrom.Value.Day, 0, 0, 0);
                fromDel = mDbInterface.OleDbToDate(fromDelDate);
                DateTime toDelDate = new DateTime(deliveryDateTo.Value.Year, deliveryDateTo.Value.Month, deliveryDateTo.Value.Day, 0, 0, 0);
                toDel = mDbInterface.OleDbToDate(toDelDate);

                if (cmbxState.SelectedIndex >= 0)
                    state = StateStringToEnum(cmbxState.Text).ToString();

                mOrders.GetFilterDataFromDb(Order.Keys.F_ID, txtCode.Text, cmbxCustomerCode.Text, cmbxCustomerName.Text,
					state, fromIns, toIns, isDateInsert, fromDel, toDel, isDateDeliv, txtDescription.Text, scheduleState, optiState, useListState, false);
            }
            else
            {
				mOrders.GetDataFromDb(Order.Keys.F_ID);
            }

            FillTable();
        }
        
        private void btnReset_Click(object sender, EventArgs e)
        {
            // Resetta i valori dei controlli per il filtro
            txtCode.Text = "";
			cmbxCustomerCode.SelectedValue = -1;
			cmbxCustomerName.SelectedValue = -1;
            cmbxState.SelectedIndex = -1;
            txtDescription.Text = "";

            insertDateFrom.ResetText();
            insertDateTo.ResetText();
            deliveryDateFrom.ResetText();
            deliveryDateTo.ResetText();

            Font regFont = new Font(insertDateFrom.Font, FontStyle.Regular);
            insertDateFrom.Font = regFont;
            insertDateTo.Font = regFont;
            deliveryDateFrom.Font = regFont;
            deliveryDateTo.Font = regFont;

            isDateDeliv = false;
            isDateInsert = false;
        }

        private void btnChiudi_Click(object sender, EventArgs e)
        {
            panelFilter.Visible = false;
        }

        private void dataGridOrders_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Order ord;

            int i = (int)dataGridOrders[e.Row, "T_ID"];
            mOrders.GetObjectTable(i, out ord);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (ord.gStateOrd.Trim() == Order.State.ORDER_LOADED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_ANALIZED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_CONFIRMED.ToString())
            {
                e.CellStyle.BackColor = Color.Yellow;
                e.CellStyle.BackColor2 = Color.LightYellow;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
            {
                e.CellStyle.BackColor = Color.Yellow;
                e.CellStyle.BackColor2 = Color.LightYellow;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_COMPLETED.ToString())
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_UNLOADED.ToString())
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_NDISP.ToString())
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.BackColor2 = Color.Coral;
            }
            else if (ord.gStateOrd.Trim() == Order.State.ORDER_RELOADED.ToString())
            {
                e.CellStyle.BackColor = Color.Orange;
                e.CellStyle.BackColor2 = Color.Gold;
            }
        }

        private void insertDateFrom_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(insertDateFrom.Font, FontStyle.Bold);
            insertDateFrom.Font = boldFont;
            isDateInsert = true;
            if (insertDateFrom.Value > insertDateTo.Value)
                insertDateFrom.Value = insertDateTo.Value;
        }

        private void insertDateTo_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(insertDateTo.Font, FontStyle.Bold);
            insertDateTo.Font = boldFont;
            isDateInsert = true;
            if (insertDateTo.Value < insertDateFrom.Value)
                insertDateTo.Value = insertDateFrom.Value;
        }

        private void deliveryDateFrom_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(deliveryDateFrom.Font, FontStyle.Bold);
            deliveryDateFrom.Font = boldFont;
            isDateDeliv = true;
            if (deliveryDateFrom.Value > deliveryDateTo.Value)
                deliveryDateFrom.Value = deliveryDateTo.Value;
        }

        private void deliveryDateTo_ValueChanged(object sender, EventArgs e)
        {
            Font boldFont = new Font(deliveryDateTo.Font, FontStyle.Bold);
            deliveryDateTo.Font = boldFont;
            isDateDeliv = true;
            if (deliveryDateTo.Value < deliveryDateFrom.Value)
                deliveryDateTo.Value = deliveryDateFrom.Value;
        }
        #endregion

        #region Gestione eventi
        //Genera l'evento selectOrder
        protected virtual void OnSelectOrderAdvance(string ev)
        {
            if (SelectOrderAdvanceEvent != null)
                SelectOrderAdvanceEvent(this, ev);
        }

        private void NodeSelected(object sender, TreeNode nd)
        {
            if (nd != null)
                SetCoordinate(nd.Level, nd.Tag.ToString(), nd.Text);
        }

        #endregion

        #region Private methods

        private void AttivaForm(string formName)
        {
            if (this.ParentForm == null)
                return;

            System.Windows.Forms.Form[] frm = new System.Windows.Forms.Form[this.ParentForm.MdiChildren.Length];
            frm = this.ParentForm.MdiChildren;
            for (int i = 0; i < frm.Length; i++)
            {
                if (frm[i].Name == formName)
                    frm[i].Activate();
            }
        }

        private string FindCustomerName(string custCode)
        {
            Customer cust;
            mCustomers.MoveFirst();

            while (!mCustomers.IsEOF())
            {
                if (mCustomers.GetObject(out cust) && cust.gCode == custCode)
                    return cust.gName;

                mCustomers.MoveNext();
            }
            return "";
        }

        private string StateEnumToString(Order.State st)
        {
            switch (st)
            {
                case Order.State.ORDER_LOADED:
                    return ProjResource.gResource.LoadFixString(this, 1);
                case Order.State.ORDER_CONFIRMED:
                    return ProjResource.gResource.LoadFixString(this, 2);
                case Order.State.ORDER_IN_PROGRESS:
                    return ProjResource.gResource.LoadFixString(this, 3);
                case Order.State.ORDER_COMPLETED:
                    return ProjResource.gResource.LoadFixString(this, 4);
                case Order.State.ORDER_UNLOADED:
                    return ProjResource.gResource.LoadFixString(this, 5);
                case Order.State.ORDER_NDISP:
                    return ProjResource.gResource.LoadFixString(this, 6);
                case Order.State.ORDER_ANALIZED:
                    return ProjResource.gResource.LoadFixString(this, 7);
                case Order.State.ORDER_RELOADED:
                    return ProjResource.gResource.LoadFixString(this, 8);
                default:
                    return "";
            }
        }

        private string StateEnumstrToString(string st)
        {
            if (st.Trim() == Order.State.ORDER_LOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 1);
            else if (st.Trim() == Order.State.ORDER_CONFIRMED.ToString())
                return ProjResource.gResource.LoadFixString(this, 2);
            else if (st.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 3);
            else if (st.Trim() == Order.State.ORDER_COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 4);
            else if (st.Trim() == Order.State.ORDER_UNLOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 5);
            else if (st.Trim() == Order.State.ORDER_NDISP.ToString())
                return ProjResource.gResource.LoadFixString(this, 6);
            else if (st.Trim() == Order.State.ORDER_ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 7);
            else if (st.Trim() == Order.State.ORDER_RELOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 8);
            else
                return "";
        }

        private int StateEnumstrToInt(string st)
        {
            if (st.Trim() == Order.State.ORDER_LOADED.ToString())
                return 0;
            else if (st.Trim() == Order.State.ORDER_CONFIRMED.ToString())
                return 1;
            else if (st.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
                return 2;
            else if (st.Trim() == Order.State.ORDER_COMPLETED.ToString())
                return 3;
            else if (st.Trim() == Order.State.ORDER_UNLOADED.ToString())
                return 4;
            else if (st.Trim() == Order.State.ORDER_NDISP.ToString())
                return 5;
            else if (st.Trim() == Order.State.ORDER_ANALIZED.ToString())
                return 6;
            else if (st.Trim() == Order.State.ORDER_RELOADED.ToString())
                return 7;
            else
                return -1;
        }

        private Order.State StateStringToEnum(string state)
        {
            if (state == ProjResource.gResource.LoadFixString(this, 1))
                return Order.State.ORDER_LOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 2))
                return Order.State.ORDER_CONFIRMED;
            else if (state == ProjResource.gResource.LoadFixString(this, 3))
                return Order.State.ORDER_IN_PROGRESS;
            else if (state == ProjResource.gResource.LoadFixString(this, 4))
                return Order.State.ORDER_COMPLETED;
            else if (state == ProjResource.gResource.LoadFixString(this, 5))
                return Order.State.ORDER_UNLOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 6))
                return Order.State.ORDER_NDISP;
            else if (state == ProjResource.gResource.LoadFixString(this, 7))
                return Order.State.ORDER_ANALIZED;
            else if (state == ProjResource.gResource.LoadFixString(this, 8))
                return Order.State.ORDER_RELOADED;
            else
                return Order.State.NONE;
        }

        private void SetCoordinate(int level, string code, string descr)
        {
            coord = new Coordinate("", "", "", "", "", "");

            if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Site)
                coord.gSite = code;
            else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Area)
                coord.gArea = code;
            else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Shelf)
                coord.gShelf = code;
            else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Span)
                coord.gSpan = code;
            else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Level)
                coord.gLevel = code;
            else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Position)
                coord.gPosition = code;
            else
                TraceLog.WriteLine("OrdersAdvance.SetCoordinate: Error selection");

            mLastNode = code;
        }
        #endregion
    }
}