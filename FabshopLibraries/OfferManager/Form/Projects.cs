using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Breton.DbAccess;
using Breton.Users;
using Breton.Parameters;
using Breton.Customers_Suppliers;
using Breton.TraceLoggers;
using Breton.ImportOffer;
using Breton.Phases;
using Breton.DesignUtil;
using Breton.TechnoMaster;

namespace Breton.OfferManager.Form
{
	public class Projects : System.Windows.Forms.Form
	{
		#region Variables
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.ToolBar toolBar;
		private System.Windows.Forms.ToolBarButton btnNuovo;
		private System.Windows.Forms.ToolBarButton btnImporta;
		private System.Windows.Forms.ToolBarButton btnModifica;
		private System.Windows.Forms.ToolBarButton btnSalva;
		private System.Windows.Forms.ToolBarButton btnElimina;
		private System.Windows.Forms.ToolBarButton btnReset;
		private System.Windows.Forms.ToolBarButton btnAnnulla;
		private System.Windows.Forms.ToolBarButton toolBarButton1;
		private System.Windows.Forms.ToolBarButton btnDettagli;
        private System.Windows.Forms.ImageList imlToolIcon;
		private System.Data.DataSet dataSetProjects;
		private System.Data.DataTable dataTableProjects;
		private System.Data.DataColumn dataColumnT_ID;
		private System.Data.DataColumn dataColumnF_ID;
		private System.Data.DataColumn dataColumnF_CODE;
		private System.Data.DataColumn dataColumnF_CUSTOMER_ORDER_CODE;
		private System.Data.DataColumn dataColumnF_STATE;
		private System.Data.DataColumn dataColumnF_DATE;
		private System.Data.DataColumn dataColumnF_DELIVERY_DATE;
		private System.Data.DataColumn dataColumnF_DESCRIPTION;
		private System.Data.DataColumn dataColumnF_CODE_ORIGINAL_PROJECT;
		private System.Windows.Forms.ComboBox cmbxState;
		private System.Windows.Forms.Label lblState;		
		private System.Windows.Forms.TextBox txtDescription;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.Label lblDeliveryDate;
		private System.Windows.Forms.TextBox txtDeliveryDate;
		private System.Windows.Forms.DateTimePicker deliveryDate;
		private System.Windows.Forms.Button btnNascondi;
		private System.Windows.Forms.Button btnConferma;
		private System.Windows.Forms.Label lblOperation;
		private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridProjects;
		private System.Windows.Forms.Panel panelEdit;
        private ToolBarButton toolBarButton2;
        private ToolBarButton btnRefresh;
        private ToolBarButton btnTecno;
        private DataSet DataSetProdProcess;
        private DataTable dataTableWorking;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn6;
        private DataColumn dataColumn15;
        private DataTable dataTableWorkGroup;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn13;
        private DataColumn dataColumn17;
        private DataTable dataTablePhase;
        private DataColumn dataColumn9;
        private DataColumn dataColumn10;
        private DataColumn dataColumn16;
        private DataTable dataTableMachine;
        private DataColumn dataColumn11;
        private DataColumn dataColumn12;
        private DataColumn dataColumn14;
        private DataColumn dataColumn18;
        private Panel panelProdProcess;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1WorkCenterGrid;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1WorkingGrid;
        private Button btnNascondiSimpleTecno;
        private Button btnOkSimpleTecno;
        private Label lblProdProc;
        private C1.Data.C1DataSet c1DataSetProdProcess;
        private C1.Win.C1List.C1Combo c1ProdProcess;
        private ProgressBar progressBar;
        private Timer timerTecno;

		private static bool formCreate;
		private bool isModified = false;
		private bool isDateChanged = false;
        private ParameterCollection mParamApp = null;
		private ProjectCollection mProjects;
        private DetailCollection mDetails;
        private WorkOrderCollection mWorkOrders;
        private WorkPhaseCollection mWorkPhases;
        private UsedObjectCollection mUsedObjects;
		private DbInterface mDbInterface;
		private User mUser;
		private string mLanguage;
		private string Edit;
		private string mOrderCode;
		private ParameterControlPositions mFormPosition;
		private ProjectOperation Operat;
        private DirectoryInfo dirXml;
        private TecnoOperation mTecnoOp;
        private string tmpXmlDir = System.IO.Path.GetTempPath() + "Temp_Detail_Xml";
        private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;
        private ThreadTecno mTecno;

        private EventHandler detHandler;
        public static event EventHandler UpdateProject;
		public delegate void SelectProjectEventHandler(object sender, string e);		// Gestore dell'evento
		public static event SelectProjectEventHandler SelectProjectEvent;				// Evento SelectOrderEvent

		public enum ProjectOperation
		{
			None,
			New,
			Edit,
			Save,
			Delete
		}
		#endregion

		public Projects(DbInterface db, User user, string language, string orderCode)
		{
			InitializeComponent();

			if (db == null)
			{
				mDbInterface = new DbInterface();
				mDbInterface.Open("", "DbConnection.udl");
			}
			else
				mDbInterface = db;

			// Carica la posizione del form
			mFormPosition = new ParameterControlPositions(this.Name);
			mFormPosition.LoadFormPosition(this);

			mUser = user;

			mLanguage = language;
			ProjResource.gResource.ChangeLanguage(mLanguage);

			mOrderCode = orderCode;

            // evento aggiornamento dettagli
            detHandler = new EventHandler(UpdateDetails);
            Breton.OfferManager.Form.Details.UpdateDetail += detHandler;
			// Crea il delegato per la gestione del'evento SelectOrder
			Breton.OfferManager.Form.Orders.SelectOrderEvent += new Breton.OfferManager.Form.Orders.SelectOrderEventHandler(OrdineSelezionato);
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Projects));
			C1.Win.C1List.Style style1 = new C1.Win.C1List.Style();
			C1.Win.C1List.Style style2 = new C1.Win.C1List.Style();
			C1.Win.C1List.Style style3 = new C1.Win.C1List.Style();
			C1.Win.C1List.Style style4 = new C1.Win.C1List.Style();
			C1.Win.C1List.Style style5 = new C1.Win.C1List.Style();
			C1.Win.C1List.Style style6 = new C1.Win.C1List.Style();
			C1.Win.C1List.Style style7 = new C1.Win.C1List.Style();
			C1.Win.C1List.Style style8 = new C1.Win.C1List.Style();
			this.toolBar = new System.Windows.Forms.ToolBar();
			this.btnNuovo = new System.Windows.Forms.ToolBarButton();
			this.btnImporta = new System.Windows.Forms.ToolBarButton();
			this.btnModifica = new System.Windows.Forms.ToolBarButton();
			this.btnSalva = new System.Windows.Forms.ToolBarButton();
			this.btnElimina = new System.Windows.Forms.ToolBarButton();
			this.btnReset = new System.Windows.Forms.ToolBarButton();
			this.btnAnnulla = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.btnDettagli = new System.Windows.Forms.ToolBarButton();
			this.btnTecno = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
			this.btnRefresh = new System.Windows.Forms.ToolBarButton();
			this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
			this.dataSetProjects = new System.Data.DataSet();
			this.dataTableProjects = new System.Data.DataTable();
			this.dataColumnT_ID = new System.Data.DataColumn();
			this.dataColumnF_ID = new System.Data.DataColumn();
			this.dataColumnF_CODE = new System.Data.DataColumn();
			this.dataColumnF_CUSTOMER_ORDER_CODE = new System.Data.DataColumn();
			this.dataColumnF_STATE = new System.Data.DataColumn();
			this.dataColumnF_DATE = new System.Data.DataColumn();
			this.dataColumnF_DELIVERY_DATE = new System.Data.DataColumn();
			this.dataColumnF_DESCRIPTION = new System.Data.DataColumn();
			this.dataColumnF_CODE_ORIGINAL_PROJECT = new System.Data.DataColumn();
			this.dataGridProjects = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.panelEdit = new System.Windows.Forms.Panel();
			this.lblOperation = new System.Windows.Forms.Label();
			this.btnNascondi = new System.Windows.Forms.Button();
			this.btnConferma = new System.Windows.Forms.Button();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.lblDescription = new System.Windows.Forms.Label();
			this.lblDeliveryDate = new System.Windows.Forms.Label();
			this.txtDeliveryDate = new System.Windows.Forms.TextBox();
			this.deliveryDate = new System.Windows.Forms.DateTimePicker();
			this.cmbxState = new System.Windows.Forms.ComboBox();
			this.lblState = new System.Windows.Forms.Label();
			this.DataSetProdProcess = new System.Data.DataSet();
			this.dataTableWorking = new System.Data.DataTable();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn15 = new System.Data.DataColumn();
			this.dataTableWorkGroup = new System.Data.DataTable();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.dataColumn13 = new System.Data.DataColumn();
			this.dataColumn17 = new System.Data.DataColumn();
			this.dataTablePhase = new System.Data.DataTable();
			this.dataColumn9 = new System.Data.DataColumn();
			this.dataColumn10 = new System.Data.DataColumn();
			this.dataColumn16 = new System.Data.DataColumn();
			this.dataTableMachine = new System.Data.DataTable();
			this.dataColumn11 = new System.Data.DataColumn();
			this.dataColumn12 = new System.Data.DataColumn();
			this.dataColumn14 = new System.Data.DataColumn();
			this.dataColumn18 = new System.Data.DataColumn();
			this.panelProdProcess = new System.Windows.Forms.Panel();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.c1ProdProcess = new C1.Win.C1List.C1Combo();
			this.c1DataSetProdProcess = new C1.Data.C1DataSet();
			this.c1WorkCenterGrid = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.c1WorkingGrid = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.btnNascondiSimpleTecno = new System.Windows.Forms.Button();
			this.btnOkSimpleTecno = new System.Windows.Forms.Button();
			this.lblProdProc = new System.Windows.Forms.Label();
			this.timerTecno = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.dataSetProjects)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableProjects)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridProjects)).BeginInit();
			this.panelEdit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.DataSetProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorking)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorkGroup)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTablePhase)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableMachine)).BeginInit();
			this.panelProdProcess.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1ProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1DataSetProdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkCenterGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkingGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// toolBar
			// 
			this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnNuovo,
            this.btnImporta,
            this.btnModifica,
            this.btnSalva,
            this.btnElimina,
            this.btnReset,
            this.btnAnnulla,
            this.toolBarButton1,
            this.btnDettagli,
            this.btnTecno,
            this.toolBarButton2,
            this.btnRefresh});
			this.toolBar.DropDownArrows = true;
			this.toolBar.ImageList = this.imlToolIcon;
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.ShowToolTips = true;
			this.toolBar.Size = new System.Drawing.Size(732, 44);
			this.toolBar.TabIndex = 3;
			this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
			// 
			// btnNuovo
			// 
			this.btnNuovo.ImageIndex = 0;
			this.btnNuovo.Name = "btnNuovo";
			this.btnNuovo.ToolTipText = "Nuova";
			this.btnNuovo.Visible = false;
			// 
			// btnImporta
			// 
			this.btnImporta.ImageIndex = 1;
			this.btnImporta.Name = "btnImporta";
			this.btnImporta.ToolTipText = "Importa XML";
			this.btnImporta.Visible = false;
			// 
			// btnModifica
			// 
			this.btnModifica.ImageIndex = 2;
			this.btnModifica.Name = "btnModifica";
			this.btnModifica.ToolTipText = "Modifica";
			// 
			// btnSalva
			// 
			this.btnSalva.ImageIndex = 3;
			this.btnSalva.Name = "btnSalva";
			this.btnSalva.ToolTipText = "Salva";
			// 
			// btnElimina
			// 
			this.btnElimina.ImageIndex = 4;
			this.btnElimina.Name = "btnElimina";
			this.btnElimina.ToolTipText = "Elimina Tecno";
			// 
			// btnReset
			// 
			this.btnReset.ImageIndex = 5;
			this.btnReset.Name = "btnReset";
			this.btnReset.ToolTipText = "Annulla operazioni effettuate";
			// 
			// btnAnnulla
			// 
			this.btnAnnulla.ImageIndex = 6;
			this.btnAnnulla.Name = "btnAnnulla";
			this.btnAnnulla.ToolTipText = "Esci";
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.Name = "toolBarButton1";
			this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnDettagli
			// 
			this.btnDettagli.ImageIndex = 7;
			this.btnDettagli.Name = "btnDettagli";
			// 
			// btnTecno
			// 
			this.btnTecno.ImageIndex = 9;
			this.btnTecno.Name = "btnTecno";
			// 
			// toolBarButton2
			// 
			this.toolBarButton2.Name = "toolBarButton2";
			this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnRefresh
			// 
			this.btnRefresh.ImageIndex = 8;
			this.btnRefresh.Name = "btnRefresh";
			// 
			// imlToolIcon
			// 
			this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
			this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon.Images.SetKeyName(0, "");
			this.imlToolIcon.Images.SetKeyName(1, "");
			this.imlToolIcon.Images.SetKeyName(2, "");
			this.imlToolIcon.Images.SetKeyName(3, "");
			this.imlToolIcon.Images.SetKeyName(4, "DeleteTecno.ico");
			this.imlToolIcon.Images.SetKeyName(5, "");
			this.imlToolIcon.Images.SetKeyName(6, "");
			this.imlToolIcon.Images.SetKeyName(7, "");
			this.imlToolIcon.Images.SetKeyName(8, "Refresh.ico");
			this.imlToolIcon.Images.SetKeyName(9, "Process.ico");
			// 
			// dataSetProjects
			// 
			this.dataSetProjects.DataSetName = "NewDataSet";
			this.dataSetProjects.Locale = new System.Globalization.CultureInfo("en-US");
			this.dataSetProjects.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableProjects});
			// 
			// dataTableProjects
			// 
			this.dataTableProjects.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnT_ID,
            this.dataColumnF_ID,
            this.dataColumnF_CODE,
            this.dataColumnF_CUSTOMER_ORDER_CODE,
            this.dataColumnF_STATE,
            this.dataColumnF_DATE,
            this.dataColumnF_DELIVERY_DATE,
            this.dataColumnF_DESCRIPTION,
            this.dataColumnF_CODE_ORIGINAL_PROJECT});
			this.dataTableProjects.TableName = "TableProjects";
			// 
			// dataColumnT_ID
			// 
			this.dataColumnT_ID.ColumnName = "T_ID";
			this.dataColumnT_ID.DataType = typeof(int);
			// 
			// dataColumnF_ID
			// 
			this.dataColumnF_ID.Caption = "Id";
			this.dataColumnF_ID.ColumnName = "F_ID";
			this.dataColumnF_ID.DataType = typeof(int);
			// 
			// dataColumnF_CODE
			// 
			this.dataColumnF_CODE.Caption = "Codice";
			this.dataColumnF_CODE.ColumnName = "F_CODE";
			// 
			// dataColumnF_CUSTOMER_ORDER_CODE
			// 
			this.dataColumnF_CUSTOMER_ORDER_CODE.Caption = "Codice ordine";
			this.dataColumnF_CUSTOMER_ORDER_CODE.ColumnName = "F_CUSTOMER_ORDER_CODE";
			// 
			// dataColumnF_STATE
			// 
			this.dataColumnF_STATE.Caption = "Stato";
			this.dataColumnF_STATE.ColumnName = "F_STATE";
			// 
			// dataColumnF_DATE
			// 
			this.dataColumnF_DATE.Caption = "Data inserimento";
			this.dataColumnF_DATE.ColumnName = "F_DATE";
			this.dataColumnF_DATE.DataType = typeof(System.DateTime);
			// 
			// dataColumnF_DELIVERY_DATE
			// 
			this.dataColumnF_DELIVERY_DATE.Caption = "Data consegna";
			this.dataColumnF_DELIVERY_DATE.ColumnName = "F_DELIVERY_DATE";
			this.dataColumnF_DELIVERY_DATE.DataType = typeof(System.DateTime);
			// 
			// dataColumnF_DESCRIPTION
			// 
			this.dataColumnF_DESCRIPTION.Caption = "Descrizione";
			this.dataColumnF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
			// 
			// dataColumnF_CODE_ORIGINAL_PROJECT
			// 
			this.dataColumnF_CODE_ORIGINAL_PROJECT.ColumnName = "F_CODE_ORIGINAL_PROJECT";
			// 
			// dataGridProjects
			// 
			this.dataGridProjects.AllowSort = false;
			this.dataGridProjects.AllowUpdate = false;
			this.dataGridProjects.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dataGridProjects.CaptionHeight = 17;
			this.dataGridProjects.DataSource = this.dataTableProjects;
			this.dataGridProjects.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridProjects.FetchRowStyles = true;
			this.dataGridProjects.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridProjects.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridProjects.Images"))));
			this.dataGridProjects.Location = new System.Drawing.Point(0, 197);
			this.dataGridProjects.MaintainRowCurrency = true;
			this.dataGridProjects.Name = "dataGridProjects";
			this.dataGridProjects.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridProjects.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridProjects.PreviewInfo.ZoomFactor = 75;
			this.dataGridProjects.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridProjects.PrintInfo.PageSettings")));
			this.dataGridProjects.RowHeight = 15;
			this.dataGridProjects.Size = new System.Drawing.Size(732, 321);
			this.dataGridProjects.TabIndex = 4;
			this.dataGridProjects.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridProjects_FetchRowStyle);
			this.dataGridProjects.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.dataGridProjects_RowColChange);
			this.dataGridProjects.PropBag = resources.GetString("dataGridProjects.PropBag");
			// 
			// panelEdit
			// 
			this.panelEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelEdit.Controls.Add(this.lblOperation);
			this.panelEdit.Controls.Add(this.btnNascondi);
			this.panelEdit.Controls.Add(this.btnConferma);
			this.panelEdit.Controls.Add(this.txtDescription);
			this.panelEdit.Controls.Add(this.lblDescription);
			this.panelEdit.Controls.Add(this.lblDeliveryDate);
			this.panelEdit.Controls.Add(this.txtDeliveryDate);
			this.panelEdit.Controls.Add(this.deliveryDate);
			this.panelEdit.Controls.Add(this.cmbxState);
			this.panelEdit.Controls.Add(this.lblState);
			this.panelEdit.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelEdit.Location = new System.Drawing.Point(0, 44);
			this.panelEdit.Name = "panelEdit";
			this.panelEdit.Size = new System.Drawing.Size(732, 98);
			this.panelEdit.TabIndex = 5;
			this.panelEdit.Visible = false;
			// 
			// lblOperation
			// 
			this.lblOperation.AutoSize = true;
			this.lblOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOperation.Location = new System.Drawing.Point(11, 6);
			this.lblOperation.Name = "lblOperation";
			this.lblOperation.Size = new System.Drawing.Size(0, 16);
			this.lblOperation.TabIndex = 118;
			// 
			// btnNascondi
			// 
			this.btnNascondi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnNascondi.Image = ((System.Drawing.Image)(resources.GetObject("btnNascondi.Image")));
			this.btnNascondi.Location = new System.Drawing.Point(620, 58);
			this.btnNascondi.Name = "btnNascondi";
			this.btnNascondi.Size = new System.Drawing.Size(36, 36);
			this.btnNascondi.TabIndex = 117;
			this.btnNascondi.Click += new System.EventHandler(this.btnNascondi_Click);
			// 
			// btnConferma
			// 
			this.btnConferma.Image = ((System.Drawing.Image)(resources.GetObject("btnConferma.Image")));
			this.btnConferma.Location = new System.Drawing.Point(583, 58);
			this.btnConferma.Name = "btnConferma";
			this.btnConferma.Size = new System.Drawing.Size(36, 36);
			this.btnConferma.TabIndex = 116;
			this.btnConferma.Click += new System.EventHandler(this.btnConferma_Click);
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(456, 34);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(200, 20);
			this.txtDescription.TabIndex = 114;
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDescription.Location = new System.Drawing.Point(344, 34);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(72, 15);
			this.lblDescription.TabIndex = 113;
			this.lblDescription.Text = "Descrizione";
			// 
			// lblDeliveryDate
			// 
			this.lblDeliveryDate.AutoSize = true;
			this.lblDeliveryDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDeliveryDate.Location = new System.Drawing.Point(16, 34);
			this.lblDeliveryDate.Name = "lblDeliveryDate";
			this.lblDeliveryDate.Size = new System.Drawing.Size(103, 15);
			this.lblDeliveryDate.TabIndex = 111;
			this.lblDeliveryDate.Text = "Data di consegna";
			// 
			// txtDeliveryDate
			// 
			this.txtDeliveryDate.Location = new System.Drawing.Point(128, 34);
			this.txtDeliveryDate.Name = "txtDeliveryDate";
			this.txtDeliveryDate.ReadOnly = true;
			this.txtDeliveryDate.Size = new System.Drawing.Size(200, 20);
			this.txtDeliveryDate.TabIndex = 115;
			// 
			// deliveryDate
			// 
			this.deliveryDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.deliveryDate.Location = new System.Drawing.Point(128, 34);
			this.deliveryDate.Name = "deliveryDate";
			this.deliveryDate.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.deliveryDate.Size = new System.Drawing.Size(200, 20);
			this.deliveryDate.TabIndex = 112;
			this.deliveryDate.ValueChanged += new System.EventHandler(this.deliveryDate_ValueChanged);
			// 
			// cmbxState
			// 
			this.cmbxState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbxState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbxState.Location = new System.Drawing.Point(128, 55);
			this.cmbxState.Name = "cmbxState";
			this.cmbxState.Size = new System.Drawing.Size(200, 21);
			this.cmbxState.TabIndex = 107;
			this.cmbxState.Visible = false;
			// 
			// lblState
			// 
			this.lblState.AutoSize = true;
			this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblState.Location = new System.Drawing.Point(16, 55);
			this.lblState.Name = "lblState";
			this.lblState.Size = new System.Drawing.Size(35, 15);
			this.lblState.TabIndex = 106;
			this.lblState.Text = "Stato";
			this.lblState.Visible = false;
			// 
			// DataSetProdProcess
			// 
			this.DataSetProdProcess.DataSetName = "NewDataSet";
			this.DataSetProdProcess.Locale = new System.Globalization.CultureInfo("en-US");
			this.DataSetProdProcess.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableWorking,
            this.dataTableWorkGroup,
            this.dataTablePhase,
            this.dataTableMachine});
			// 
			// dataTableWorking
			// 
			this.dataTableWorking.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn6,
            this.dataColumn15});
			this.dataTableWorking.TableName = "TableWorking";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "T_ID";
			this.dataColumn1.DataType = typeof(int);
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "F_TECNO_WORKING";
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "F_WORKING";
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "F_ID_T_LS_PHASE_WORKING";
			this.dataColumn6.DataType = typeof(int);
			// 
			// dataColumn15
			// 
			this.dataColumn15.ColumnName = "F_LAYER_CODE";
			// 
			// dataTableWorkGroup
			// 
			this.dataTableWorkGroup.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn13,
            this.dataColumn17});
			this.dataTableWorkGroup.TableName = "TableWorkGroup";
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "T_ID";
			this.dataColumn4.DataType = typeof(int);
			// 
			// dataColumn5
			// 
			this.dataColumn5.ColumnName = "F_TECNO_WORKING";
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "F_ID_T_AN_WORK_CENTER_GROUP";
			this.dataColumn7.DataType = typeof(int);
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "F_WORK_CENTER_GROUP";
			// 
			// dataColumn13
			// 
			this.dataColumn13.ColumnName = "F_TECNO_WORK_GROUP";
			// 
			// dataColumn17
			// 
			this.dataColumn17.ColumnName = "F_WORK_CENTER_GROUP_CODE";
			// 
			// dataTablePhase
			// 
			this.dataTablePhase.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn16});
			this.dataTablePhase.TableName = "TablePhase";
			// 
			// dataColumn9
			// 
			this.dataColumn9.ColumnName = "F_PHASE_ID";
			this.dataColumn9.DataType = typeof(int);
			// 
			// dataColumn10
			// 
			this.dataColumn10.ColumnName = "F_PHASE_DESCRIPTION";
			// 
			// dataColumn16
			// 
			this.dataColumn16.ColumnName = "F_LAYER_CODE";
			// 
			// dataTableMachine
			// 
			this.dataTableMachine.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn14,
            this.dataColumn18});
			this.dataTableMachine.TableName = "TableMachine";
			// 
			// dataColumn11
			// 
			this.dataColumn11.ColumnName = "F_WORK_CENTER_GROUP_ID";
			this.dataColumn11.DataType = typeof(int);
			// 
			// dataColumn12
			// 
			this.dataColumn12.ColumnName = "F_WORK_CENTER_GROUP_DESCRIPTION";
			// 
			// dataColumn14
			// 
			this.dataColumn14.ColumnName = "F_TECNO_WORK_CENTER_GROUP";
			// 
			// dataColumn18
			// 
			this.dataColumn18.ColumnName = "F_WORK_CENTER_GROUP_CODE";
			// 
			// panelProdProcess
			// 
			this.panelProdProcess.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelProdProcess.Controls.Add(this.progressBar);
			this.panelProdProcess.Controls.Add(this.c1ProdProcess);
			this.panelProdProcess.Controls.Add(this.c1WorkCenterGrid);
			this.panelProdProcess.Controls.Add(this.c1WorkingGrid);
			this.panelProdProcess.Controls.Add(this.btnNascondiSimpleTecno);
			this.panelProdProcess.Controls.Add(this.btnOkSimpleTecno);
			this.panelProdProcess.Controls.Add(this.lblProdProc);
			this.panelProdProcess.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelProdProcess.Location = new System.Drawing.Point(0, 142);
			this.panelProdProcess.Name = "panelProdProcess";
			this.panelProdProcess.Size = new System.Drawing.Size(732, 55);
			this.panelProdProcess.TabIndex = 13;
			this.panelProdProcess.Visible = false;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(494, 12);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(210, 22);
			this.progressBar.TabIndex = 124;
			this.progressBar.Visible = false;
			// 
			// c1ProdProcess
			// 
			this.c1ProdProcess.AddItemSeparator = ';';
			this.c1ProdProcess.Caption = "";
			this.c1ProdProcess.CaptionHeight = 17;
			this.c1ProdProcess.CaptionStyle = style1;
			this.c1ProdProcess.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
			this.c1ProdProcess.ColumnCaptionHeight = 17;
			this.c1ProdProcess.ColumnFooterHeight = 17;
			this.c1ProdProcess.ColumnHeaders = false;
			this.c1ProdProcess.ColumnWidth = 100;
			this.c1ProdProcess.ComboStyle = C1.Win.C1List.ComboStyleEnum.DropdownList;
			this.c1ProdProcess.ContentHeight = 16;
			this.c1ProdProcess.DataMember = "T_LS_PRODUCTION_PROCESSES";
			this.c1ProdProcess.DataSource = this.c1DataSetProdProcess;
			this.c1ProdProcess.DeadAreaBackColor = System.Drawing.Color.Empty;
			this.c1ProdProcess.DisplayMember = "T_LS_PRODUCTION_PROCESSES.F_DESCRIPTION";
			this.c1ProdProcess.EditorBackColor = System.Drawing.SystemColors.Window;
			this.c1ProdProcess.EditorFont = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1ProdProcess.EditorForeColor = System.Drawing.SystemColors.WindowText;
			this.c1ProdProcess.EditorHeight = 16;
			this.c1ProdProcess.EvenRowStyle = style2;
			this.c1ProdProcess.ExtendRightColumn = true;
			this.c1ProdProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1ProdProcess.FooterStyle = style3;
			this.c1ProdProcess.HeadingStyle = style4;
			this.c1ProdProcess.HighLightRowStyle = style5;
			this.c1ProdProcess.Images.Add(((System.Drawing.Image)(resources.GetObject("c1ProdProcess.Images"))));
			this.c1ProdProcess.ItemHeight = 15;
			this.c1ProdProcess.Location = new System.Drawing.Point(140, 12);
			this.c1ProdProcess.MatchEntryTimeout = ((long)(2000));
			this.c1ProdProcess.MaxDropDownItems = ((short)(5));
			this.c1ProdProcess.MaxLength = 32767;
			this.c1ProdProcess.MouseCursor = System.Windows.Forms.Cursors.Default;
			this.c1ProdProcess.Name = "c1ProdProcess";
			this.c1ProdProcess.OddRowStyle = style6;
			this.c1ProdProcess.RowDivider.Color = System.Drawing.Color.DarkGray;
			this.c1ProdProcess.RowDivider.Style = C1.Win.C1List.LineStyleEnum.None;
			this.c1ProdProcess.RowSubDividerColor = System.Drawing.Color.DarkGray;
			this.c1ProdProcess.SelectedStyle = style7;
			this.c1ProdProcess.Size = new System.Drawing.Size(248, 22);
			this.c1ProdProcess.Style = style8;
			this.c1ProdProcess.TabIndex = 123;
			this.c1ProdProcess.ValueMember = "T_LS_PRODUCTION_PROCESSES.F_ID";
			this.c1ProdProcess.SelectedValueChanged += new System.EventHandler(this.c1ProdProcess_SelectedValueChanged);
			this.c1ProdProcess.PropBag = resources.GetString("c1ProdProcess.PropBag");
			// 
			// c1DataSetProdProcess
			// 
			this.c1DataSetProdProcess.DataLibrary = "ShopMasterDataLibrary";
			this.c1DataSetProdProcess.DataLibraryUrl = "";
			this.c1DataSetProdProcess.DataSetDef = "DataSet_Production_Process";
			this.c1DataSetProdProcess.FillOnRequest = false;
			this.c1DataSetProdProcess.Locale = new System.Globalization.CultureInfo("en-US");
			this.c1DataSetProdProcess.Name = "c1DataSetProdProcess";
			this.c1DataSetProdProcess.SchemaClassName = "Breton.ShopMasterDataLibrary.DataClass";
			this.c1DataSetProdProcess.SchemaDef = null;
			// 
			// c1WorkCenterGrid
			// 
			this.c1WorkCenterGrid.AllowColMove = false;
			this.c1WorkCenterGrid.AllowSort = false;
			this.c1WorkCenterGrid.AllowUpdate = false;
			this.c1WorkCenterGrid.Caption = "Gruppi di lavoro";
			this.c1WorkCenterGrid.CaptionHeight = 17;
			this.c1WorkCenterGrid.ColumnHeaders = false;
			this.c1WorkCenterGrid.DataSource = this.dataTableWorkGroup;
			this.c1WorkCenterGrid.ExtendRightColumn = true;
			this.c1WorkCenterGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1WorkCenterGrid.GroupByCaption = "Drag a column header here to group by that column";
			this.c1WorkCenterGrid.Images.Add(((System.Drawing.Image)(resources.GetObject("c1WorkCenterGrid.Images"))));
			this.c1WorkCenterGrid.Location = new System.Drawing.Point(293, 57);
			this.c1WorkCenterGrid.Name = "c1WorkCenterGrid";
			this.c1WorkCenterGrid.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1WorkCenterGrid.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1WorkCenterGrid.PreviewInfo.ZoomFactor = 75;
			this.c1WorkCenterGrid.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1WorkCenterGrid.PrintInfo.PageSettings")));
			this.c1WorkCenterGrid.RowHeight = 15;
			this.c1WorkCenterGrid.ScrollTrack = false;
			this.c1WorkCenterGrid.Size = new System.Drawing.Size(233, 84);
			this.c1WorkCenterGrid.TabIndex = 122;
			this.c1WorkCenterGrid.PropBag = resources.GetString("c1WorkCenterGrid.PropBag");
			// 
			// c1WorkingGrid
			// 
			this.c1WorkingGrid.AllowColMove = false;
			this.c1WorkingGrid.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.None;
			this.c1WorkingGrid.AllowSort = false;
			this.c1WorkingGrid.AllowUpdate = false;
			this.c1WorkingGrid.Caption = "Fasi lavorazione";
			this.c1WorkingGrid.CaptionHeight = 17;
			this.c1WorkingGrid.ColumnHeaders = false;
			this.c1WorkingGrid.DataSource = this.dataTableWorking;
			this.c1WorkingGrid.ExtendRightColumn = true;
			this.c1WorkingGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.c1WorkingGrid.GroupByCaption = "Drag a column header here to group by that column";
			this.c1WorkingGrid.Images.Add(((System.Drawing.Image)(resources.GetObject("c1WorkingGrid.Images"))));
			this.c1WorkingGrid.Location = new System.Drawing.Point(35, 57);
			this.c1WorkingGrid.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.None;
			this.c1WorkingGrid.Name = "c1WorkingGrid";
			this.c1WorkingGrid.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1WorkingGrid.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1WorkingGrid.PreviewInfo.ZoomFactor = 75;
			this.c1WorkingGrid.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1WorkingGrid.PrintInfo.PageSettings")));
			this.c1WorkingGrid.RowHeight = 15;
			this.c1WorkingGrid.ScrollTrack = false;
			this.c1WorkingGrid.Size = new System.Drawing.Size(233, 84);
			this.c1WorkingGrid.TabIndex = 121;
			this.c1WorkingGrid.PropBag = resources.GetString("c1WorkingGrid.PropBag");
			// 
			// btnNascondiSimpleTecno
			// 
			this.btnNascondiSimpleTecno.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnNascondiSimpleTecno.Image = ((System.Drawing.Image)(resources.GetObject("btnNascondiSimpleTecno.Image")));
			this.btnNascondiSimpleTecno.Location = new System.Drawing.Point(443, 5);
			this.btnNascondiSimpleTecno.Name = "btnNascondiSimpleTecno";
			this.btnNascondiSimpleTecno.Size = new System.Drawing.Size(36, 36);
			this.btnNascondiSimpleTecno.TabIndex = 119;
			this.btnNascondiSimpleTecno.Click += new System.EventHandler(this.btnNascondiSimpleTecno_Click);
			// 
			// btnOkSimpleTecno
			// 
			this.btnOkSimpleTecno.Enabled = false;
			this.btnOkSimpleTecno.Image = ((System.Drawing.Image)(resources.GetObject("btnOkSimpleTecno.Image")));
			this.btnOkSimpleTecno.Location = new System.Drawing.Point(407, 5);
			this.btnOkSimpleTecno.Name = "btnOkSimpleTecno";
			this.btnOkSimpleTecno.Size = new System.Drawing.Size(36, 36);
			this.btnOkSimpleTecno.TabIndex = 118;
			this.btnOkSimpleTecno.Click += new System.EventHandler(this.btnOkSimpleTecno_Click);
			// 
			// lblProdProc
			// 
			this.lblProdProc.AutoSize = true;
			this.lblProdProc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblProdProc.Location = new System.Drawing.Point(11, 15);
			this.lblProdProc.Name = "lblProdProc";
			this.lblProdProc.Size = new System.Drawing.Size(114, 15);
			this.lblProdProc.TabIndex = 39;
			this.lblProdProc.Text = "Processo produttivo";
			// 
			// timerTecno
			// 
			this.timerTecno.Tick += new System.EventHandler(this.timerTecno_Tick);
			// 
			// Projects
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(732, 518);
			this.Controls.Add(this.dataGridProjects);
			this.Controls.Add(this.panelProdProcess);
			this.Controls.Add(this.panelEdit);
			this.Controls.Add(this.toolBar);
			this.Name = "Projects";
			this.Text = "Progetti - Programmazione della produzione";
			this.Load += new System.EventHandler(this.Projects_Load);
			this.Closed += new System.EventHandler(this.Projects_Closed);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Projects_Closing);
			((System.ComponentModel.ISupportInitialize)(this.dataSetProjects)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableProjects)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridProjects)).EndInit();
			this.panelEdit.ResumeLayout(false);
			this.panelEdit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.DataSetProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorking)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableWorkGroup)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTablePhase)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableMachine)).EndInit();
			this.panelProdProcess.ResumeLayout(false);
			this.panelProdProcess.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1ProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1DataSetProdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkCenterGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.c1WorkingGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		#endregion		

		#region Properties

		public static bool gCreate
		{
			set	{ formCreate = value; }
			get { return formCreate; }
		}

		#endregion

		#region Form Function
		private void Projects_Load(object sender, System.EventArgs e)
		{
			// Carica le lingue del form
			ProjResource.gResource.LoadStringForm(this);
			Edit = ProjResource.gResource.LoadFixString(this,0);

			mProjects = new ProjectCollection(mDbInterface);
            mDetails = new DetailCollection(mDbInterface);
            mTecnoOp = new TecnoOperation(mDbInterface);
            mParamApp = new ParameterCollection(mDbInterface, false);
            mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridProjects, dataTableProjects);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridProjects);

			DisablePanelEdit();
			
			if (mProjects.GetDataFromDb(Project.Keys.F_CODE, mOrderCode))
				FillTable();

			for (int i=0; i<(int)Project.State.MAX;i++)
				cmbxState.Items.Add(StateEnumToString((Project.State)i));

            c1DataSetProdProcess.Fill();	// Carica i processi produttivi

            dirXml = new DirectoryInfo(tmpXmlDir);
            if (!dirXml.Exists)
            {
                dirXml.Create();
                dirXml.Attributes = FileAttributes.Hidden;
            }

            gCreate = true;
		}

		private void Projects_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			DialogResult dlgClose;
			string message ,caption;

			//Se ci sono state delle modifiche viene chiesto il salvataggio
			if (isModified)
			{				
				ProjResource.gResource.LoadMessageBox(0, out caption, out message);
				dlgClose = MessageBox.Show(message,caption,MessageBoxButtons.YesNoCancel,MessageBoxIcon.Question);
				if (dlgClose == DialogResult.Yes)
				{
					if (!mProjects.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
					{
						ProjResource.gResource.LoadMessageBox(1, out caption, out message);
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
				else if (dlgClose == DialogResult.Cancel)
					e.Cancel=true;				//Non viene effettuato il salvataggio e la form rimane aperta
			}
		}

		private void Projects_Closed(object sender, System.EventArgs e)
		{
			mFormPosition.SaveFormPosition(this);
            mSortGrid = null;
			mColumnView = null;

            if (dirXml.Exists &&
                !Breton.TechnoMaster.Form.Working.gCreate &&
                !Breton.OfferManager.Form.Orders.gCreate &&
                !Breton.OfferManager.Form.Details.gCreate &&
                !Breton.OfferManager.Form.DetailsAdvance.gCreate &&
                !Breton.OfferManager.Form.Offers.gCreate)
                dirXml.Delete(true);

			gCreate = false;
            Breton.OfferManager.Form.Details.UpdateDetail -= detHandler;
		}
		#endregion

		#region Gestione tabelle

		private void FillTable()
		{
			Project prj;
		
			dataTableProjects.Clear();

			// Scorre tutti i progetti letti
			mProjects.MoveFirst();
			while (!mProjects.IsEOF())
			{
				// Legge il progetto attuale
				mProjects.GetObject(out prj);
				
				dataTableProjects.BeginInit();
				dataTableProjects.BeginLoadData();
				// Aggiunge una riga alla volta
				AddRow(ref prj);
				
				dataTableProjects.EndInit();
				dataTableProjects.EndLoadData();
			}	
		}

		private void AddRow(ref Project prj)
		{
			// Array con i dati della riga
			object [] myArray = new object[9];
			myArray[0] = dataTableProjects.Rows.Count;
			myArray[1] = prj.gId;
			myArray[2] = prj.gCode;
			myArray[3] = prj.gCustomerOrderCode;
			myArray[4] = StateEnumstrToString(prj.gStatePrj);
			myArray[5] = prj.gDate;
            if (prj.gDeliveryDate == DateTime.MaxValue)
                myArray[6] = null;
            else
                myArray[6] = prj.gDeliveryDate;
			myArray[7] = prj.gDescription;
			myArray[8] = prj.gCodeOriginalProject;

			// Crea una nuova riga nella tabella e la riempie di dati
			DataRow r = dataTableProjects.NewRow();
			r.ItemArray = myArray;
			dataTableProjects.Rows.Add(r);			

			// Assegna l'id della riga tabella al progetto
			prj.gTableId = int.Parse(r.ItemArray[0].ToString());

			// Passa all'elemento successivo
			mProjects.MoveNext();
		}

		#endregion

		#region Tool Bar

		private void toolBar_ButtonClick(object sender, System.Windows.Forms.ToolBarButtonClickEventArgs e)
		{
			string message ,caption;
            Project prj;
            int i = 0;

			switch(toolBar.Buttons.IndexOf(e.Button))
			{
				// Modifica
				case 2:
					if (dataGridProjects.Bookmark >= 0)
					{						
						Operat = ProjectOperation.Edit;					
						dataGridProjects.Enabled=false;
						lblOperation.Text=Edit;
                        panelEdit.Visible = true;
						EnablePanelEdit();
						AggiornaCampiNewMod();
						DisableToolBar_Edit();
						isDateChanged = false;
					}
					break;
					
				// Salva
				case 3:
					ProjResource.gResource.LoadMessageBox(0, out caption, out message);
					if (isModified && MessageBox.Show(message, caption ,MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.Yes)
					{
						Operat = ProjectOperation.Save;
						if (!mProjects.UpdateDataToDb())
						{
							ProjResource.gResource.LoadMessageBox(1, out caption, out message);
							MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
							Operat = ProjectOperation.None;
						}
						else
						{
							isModified=false;
							if (mProjects.GetDataFromDb(Project.Keys.F_CODE, mOrderCode))
								FillTable();	//Riempe la Table

							Operat = ProjectOperation.None;

                            OnUpdate();
						}
					}
					break;

                // Elimina tecno
                case 4:
                    Detail det;
                    bool transaction = !mDbInterface.TransactionActive();

                    try
                    {
                        if (dataGridProjects.Bookmark >= 0)
                        {
                            ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
                            if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (dataGridProjects.SelectedRows.Count == 0)
                                {
                                    if (mProjects.GetObjectTable(int.Parse(dataGridProjects[dataGridProjects.Bookmark, 0].ToString()), out prj))
                                    {
                                        //Apre la transazione
                                        if (transaction)
                                            mDbInterface.BeginTransaction();

                                        this.Cursor = Cursors.WaitCursor;
                                        if (!mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
                                            throw new ApplicationException("Error get details from db");

                                        mDetails.MoveFirst();
                                        while (!mDetails.IsEOF())
                                        {
                                            mWorkOrders = new WorkOrderCollection(mDbInterface);
                                            mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                            mUsedObjects = new UsedObjectCollection(mDbInterface);

                                            mDetails.GetObject(out det);
                                            if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                                            {
                                                if(!LoadTecnoPhases(det.gCode))
                                                    throw new ApplicationException("Error get tecno phases from db");
                                                if(!DeleteAll())
                                                    throw new ApplicationException("Error delete tecno phases from db");
                                            }
                                            mDetails.MoveNext();
                                        }

                                        //Chiude la transazione
                                        if (transaction)
                                            mDbInterface.EndTransaction(true);

                                        UpdateDetails(null, null);
                                        OnSelectProject(prj.gCode);
                                        this.Cursor = Cursors.Default;
                                    }
                                }
                                else
                                {
                                    //Apre la transazione
                                    if (transaction)
                                        mDbInterface.BeginTransaction();

                                    this.Cursor = Cursors.WaitCursor;
                                    for (i = 0; i < dataGridProjects.SelectedRows.Count; i++)
                                    {
                                        if (mProjects.GetObjectTable(int.Parse(dataGridProjects[dataGridProjects.SelectedRows[i], 0].ToString()), out prj))
                                        {
                                            if(!mDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode))
                                                throw new ApplicationException("Error get details from db");

                                            mDetails.MoveFirst();
                                            while (!mDetails.IsEOF())
                                            {
                                                mWorkOrders = new WorkOrderCollection(mDbInterface);
                                                mWorkPhases = new WorkPhaseCollection(mDbInterface);
                                                mUsedObjects = new UsedObjectCollection(mDbInterface);

                                                mDetails.GetObject(out det);
                                                if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
                                                {
                                                    if (!LoadTecnoPhases(det.gCode))
                                                        throw new ApplicationException("Error get tecno phases from db");
                                                    if (!DeleteAll())
                                                        throw new ApplicationException("Error delete tecno phases from db");
                                                }
                                                mDetails.MoveNext();
                                            }
                                        }
                                    }
                                    //Chiude la transazione
                                    if (transaction)
                                        mDbInterface.EndTransaction(true);

                                    for (i = 0; i < dataGridProjects.SelectedRows.Count; i++)
                                    {
                                        if (mProjects.GetObjectTable(int.Parse(dataGridProjects[dataGridProjects.SelectedRows[i], 0].ToString()), out prj))
                                            OnSelectProject(prj.gCode);
                                    }

                                    UpdateDetails(null, null);

                                    this.Cursor = Cursors.Default;
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        TraceLog.WriteLine("Projects.toolBar_ButtonClick DeleteTecno", ex);
                        //Chiude la transazione
                        if (transaction)
                            mDbInterface.EndTransaction(false);


                    }                   
                        
                    break;

				// Reset
				case 5:
					ProjResource.gResource.LoadMessageBox(2, out caption, out message);
					if (isModified &&
						MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes &&
						mProjects.GetDataFromDb(Project.Keys.F_CODE, mOrderCode))
					{
						FillTable();
						Operat = ProjectOperation.None;
						AggiornaCampiNewMod();
						DisablePanelEdit();
						isModified=false;
					}
					break;

				// Esci
				case 6:
					Close();
					break;

				// Dettagli
				case 8:
					string projectCode;
					
					if (dataGridProjects.Bookmark >= 0) 
						i = (int)dataGridProjects[dataGridProjects.Bookmark, 0];
					else
						return;
					// Carica il codice dell'ordine di cui si vogliono visualizzare i progetti
					mProjects.GetObjectTable(i,out prj);
					projectCode = prj.gCode.Trim();

					if (!Details.gCreate)
					{
						Details frmDet = new Details(mDbInterface, mUser, mLanguage, projectCode);
						frmDet.MdiParent = this.MdiParent;
						frmDet.Show();
					}
					else
					{
						OnSelectProject(projectCode);
						AttivaForm("Details");
					}
					break;

                // Processo produttivo
                case 9:
                    Parameter par;

                    // Verifica che l'indice sia valido
                    if (dataGridProjects.Bookmark >= 0)
                    {
                        if (mParamApp.GetDataFromDb("", "", "TECNO", "LAST_PROCESS") &&
                          mParamApp.GetObject("", "", "TECNO", "LAST_PROCESS", out par))
                            c1ProdProcess.SelectedValue = par.gIntValue;

                        panelProdProcess.Visible = true;
                    }
                    else
                        return;
                    break;

                // Refresh
                case 11:
                    this.Cursor = Cursors.WaitCursor;
                    if (mProjects.RefreshData())
                        FillTable();
                    this.Cursor = Cursors.Default;
                    break;
			}
		}

		#endregion

		#region Eventi dei controlli

		private void dataGridProjects_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
		{
			CellValue();
		}

		private void CellValue()
		{
			Project prj;
			int i;

			if (dataGridProjects.Bookmark >= 0) 
				i = (int)dataGridProjects[dataGridProjects.Bookmark, 0];
			else
				return;

			// Aggiorna i campi di Edit con i valori della riga selezionata se non sto facendo operazioni
			if (Operat == ProjectOperation.None)
			{
				if 	(mProjects.GetObjectTable(i, out prj))
				{
					//cmbxState.SelectedIndex = StateEnumstrToInt(prj.gStatePrj);
					if (prj.gDeliveryDate == DateTime.MaxValue)
					{
						txtDeliveryDate.Text = "";
						deliveryDate.ResetText();
					}
					else
					{
						txtDeliveryDate.Text = prj.gDeliveryDate.ToShortDateString();
						deliveryDate.Value = prj.gDeliveryDate;
					}
					txtDescription.Text = prj.gDescription;
				}
			}
		}

        private void dataGridProjects_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
        {
            Project prj;

            int i = (int)dataGridProjects[e.Row, "T_ID"];
            mProjects.GetObjectTable(i, out prj);

            e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
            if (prj.gStatePrj.Trim() == Project.State.PROJ_LOADED.ToString())
            {
                e.CellStyle.BackColor = Color.White;
                e.CellStyle.BackColor2 = Color.WhiteSmoke;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_ANALIZED.ToString())
            {
                e.CellStyle.BackColor = Color.LightGreen;
                e.CellStyle.BackColor2 = Color.LimeGreen;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_COMPLETED.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_IN_PROGRESS.ToString())
            {
                e.CellStyle.BackColor = Color.Green;
                e.CellStyle.BackColor2 = Color.Green;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_NDISP.ToString())
            {
                e.CellStyle.BackColor = Color.Red;
                e.CellStyle.BackColor2 = Color.Coral;
            }
            else if (prj.gStatePrj.Trim() == Project.State.PROJ_RELOADED.ToString())
            {
                e.CellStyle.BackColor = Color.Orange;
                e.CellStyle.BackColor2 = Color.Gold;
            }
        }

		private string StateEnumToString(Project.State st)
		{
			switch (st)
			{
				case Project.State.PROJ_LOADED:
					return ProjResource.gResource.LoadFixString(this, 1);
                case Project.State.PROJ_IN_PROGRESS:
                    return ProjResource.gResource.LoadFixString(this, 2);
                case Project.State.PROJ_COMPLETED:
                    return ProjResource.gResource.LoadFixString(this, 3);
                case Project.State.PROJ_NDISP:
                    return ProjResource.gResource.LoadFixString(this, 4);
                case Project.State.PROJ_ANALIZED:
                    return ProjResource.gResource.LoadFixString(this, 5);
                case Project.State.PROJ_RELOADED:
                    return ProjResource.gResource.LoadFixString(this, 6);
				default:
					return "";
			}
		}

		private string StateEnumstrToString(string st)
		{
            if (st.Trim() == Project.State.PROJ_LOADED.ToString())	
				return ProjResource.gResource.LoadFixString(this, 1);
            else if (st.Trim() == Project.State.PROJ_IN_PROGRESS.ToString())
                return ProjResource.gResource.LoadFixString(this, 2);
            else if (st.Trim() == Project.State.PROJ_COMPLETED.ToString())
                return ProjResource.gResource.LoadFixString(this, 3);
            else if (st.Trim() == Project.State.PROJ_NDISP.ToString())
                return ProjResource.gResource.LoadFixString(this, 4);
            else if (st.Trim() == Project.State.PROJ_ANALIZED.ToString())
                return ProjResource.gResource.LoadFixString(this, 5);
            else if (st.Trim() == Project.State.PROJ_RELOADED.ToString())
                return ProjResource.gResource.LoadFixString(this, 6);
			else
				return "";
		}

		private int StateEnumstrToInt(string st)
		{
            if (st.Trim() == Project.State.PROJ_LOADED.ToString())	
				return 0;
            else if (st.Trim() == Project.State.PROJ_IN_PROGRESS.ToString())
                return 1;
            else if (st.Trim() == Project.State.PROJ_COMPLETED.ToString())
                return 2;
            else if (st.Trim() == Project.State.PROJ_NDISP.ToString())
                return 3;
            else if (st.Trim() == Project.State.PROJ_ANALIZED.ToString())
                return 4;
            else if (st.Trim() == Project.State.PROJ_RELOADED.ToString())
                return 5;
			else
				return -1;
		}

		private Project.State StateStringToEnum(string state)
		{
            if (state == ProjResource.gResource.LoadFixString(this, 1))
                return Project.State.PROJ_LOADED;
            else if (state == ProjResource.gResource.LoadFixString(this, 2))
                return Project.State.PROJ_IN_PROGRESS;
            else if (state == ProjResource.gResource.LoadFixString(this, 3))
                return Project.State.PROJ_COMPLETED;
            else if (state == ProjResource.gResource.LoadFixString(this, 4))
                return Project.State.PROJ_NDISP;
            else if (state == ProjResource.gResource.LoadFixString(this, 5))
                return Project.State.PROJ_ANALIZED;
            else if (state == ProjResource.gResource.LoadFixString(this, 6))
                return Project.State.PROJ_RELOADED;
            else
                return Project.State.NONE;
		}

		private void btnConferma_Click(object sender, System.EventArgs e)
		{
			Project prj;
			DateTime prevDate;
			
			// Operazione in corso: Modifica progetto
			if (Operat==ProjectOperation.Edit)
			{
				// Estrae l'oggetto da modificare
				if (mProjects.GetObjectTable((int)dataGridProjects[dataGridProjects.Bookmark, 0], out prj))
				{
					// Aggiorna tutti i valori con i nuovi inseriti
					//prj.gStatePrj = StateStringToEnum(cmbxState.SelectedItem.ToString()).ToString();
					if (isDateChanged)
					{
						prevDate = prj.gDeliveryDate;
						prj.gDeliveryDate = deliveryDate.Value;
					}
					prj.gDescription = txtDescription.Text;					
				}

				// Aggiorna la Table
				int idTable = (int)dataGridProjects[dataGridProjects.Bookmark, 0];
				object[] rowChange =  {idTable, prj.gId, prj.gCode, prj.gCustomerOrderCode, StateEnumstrToString(prj.gStatePrj),
										  prj.gDate, (prj.gDeliveryDate == DateTime.MaxValue ? null : prj.gDeliveryDate.ToShortDateString()), 
										  prj.gDescription, prj.gCodeOriginalProject};
				dataTableProjects.Rows[idTable].ItemArray = rowChange;		
				isModified=true;
				dataGridProjects.Enabled=true;
			}
			else if (Operat==ProjectOperation.None)
			{
				return;
			}
			// Abilita-Disabilita i vari controlli
			Operat = ProjectOperation.None;
			DisablePanelEdit();
            panelEdit.Visible = false;
			AggiornaCampiNewMod();
			EnableToolBar_Edit();
			CellValue();
		}

		private void btnNascondi_Click(object sender, System.EventArgs e)
		{
			// Annnulla l'operazione in corso
			DisablePanelEdit();
            panelEdit.Visible = false;
			dataGridProjects.Enabled=true;
			Operat=ProjectOperation.None;
			EnableToolBar_Edit();
			AggiornaCampiNewMod();
			CellValue();
		}

		private void deliveryDate_ValueChanged(object sender, System.EventArgs e)
		{
			isDateChanged = true;
		}

        private void c1ProdProcess_SelectedValueChanged(object sender, EventArgs e)
        {
            if (c1ProdProcess.SelectedIndex >= 0)
                btnOkSimpleTecno.Enabled = true;
            else
                btnOkSimpleTecno.Enabled = false;
        }

        /// <summary>
        /// Conferma il processo produttivo selezionato creando le fasi tecno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOkSimpleTecno_Click(object sender, EventArgs e)
        {
            Project prj;
            Parameter par;
            int j = 0;
            string caption, message;

            if (mTecno == null)
                mTecno = new ThreadTecno(mDbInterface, dataTableWorkGroup, dataTableWorking, c1WorkCenterGrid, c1WorkingGrid);

            if (dataGridProjects.Bookmark >= 0)
            {
                mTecno.gSelectedProcess = (int)c1ProdProcess.SelectedValue;

                if (!mTecno.gInProgress)
                {
                    while (j < dataGridProjects.SelectedRows.Count)
                    {
                        mProjects.GetObjectTable(int.Parse(dataGridProjects[dataGridProjects.SelectedRows[j], 0].ToString()), out prj);
                        mTecno.AddProject(prj);
                        j++;
                    }

                    if (dataGridProjects.SelectedRows.Count == 0)
                    {
                        mProjects.GetObjectTable(int.Parse(dataGridProjects[dataGridProjects.Bookmark, 0].ToString()), out prj);
                        mTecno.AddProject(prj);
                    }

                    ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
                    if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        c1ProdProcess.Enabled = false;
                        mTecno.Start();
                        progressBar.Visible = true;
                        progressBar.Value = 0;
                        timerTecno.Enabled = true;
                        timerTecno.Start();
                    }
                }
                else
                {
                    ProjResource.gResource.LoadMessageBox(this, 6, out caption, out message);
                    if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        if (mTecno != null && mTecno.gInProgress)
                            mTecno.Stop();
                    }
                }

                if (mParamApp.GetDataFromDb("", "", "TECNO", "LAST_PROCESS") &&
                     mParamApp.GetObject("", "", "TECNO", "LAST_PROCESS", out par))
                {
                    par.gIntValue = (int)c1ProdProcess.SelectedValue;
                }
                else
                {
                    par = new Parameter(Environment.MachineName, Application.ProductName, "TECNO", "LAST_PROCESS", Parameter.ParTypes.INT, c1ProdProcess.SelectedValue.ToString(), "");
                    mParamApp.AddObject(par);
                }
                mParamApp.UpdateDataToDb();
            }
        }

        private void btnNascondiSimpleTecno_Click(object sender, EventArgs e)
        {
            panelProdProcess.Visible = false;
        }

        private void timerTecno_Tick(object sender, EventArgs e)
        {
            string message, caption;

            // Verifica se sta copiando
            if (mTecno != null && mTecno.gInProgress)
            {
                if (mTecno.gMaxProgress > 0)
                {
                    // Setta i valori della progressBar
                    progressBar.Maximum = mTecno.gMaxProgress;
                    progressBar.Value = mTecno.gProgress;
                }

                // Verifica se � terminata l'analisi dell'ordine
                if (mTecno.gAnalized)// && !mFirstThick)
                {
                    mTecno.Continue();
                }
            }
            else
            {
                // Se il processo � terminato
                timerTecno.Stop();
                timerTecno.Enabled = false;
                progressBar.Visible = false;
                Refresh();
                c1ProdProcess.Enabled = true;

                this.Cursor = Cursors.WaitCursor;
                OnUpdate();
                mProjects.RefreshData();
                FillTable();
                this.Cursor = Cursors.Default;

                // Verifica il risultato delle fasi tecno
                if (mTecno.gError)
                {
                    ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
                    MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mTecno.gError = false;
                }
                if (mTecno.gNotLoaded)
                {
                    ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
                    MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    mTecno.gNotLoaded = false;
                }

                mTecno = null;
            }
        }

		#endregion

		#region Gestione eventi
		//Genera l'evento SelectProject
		protected virtual void OnSelectProject(string ev)
		{
			if(SelectProjectEvent != null)
				SelectProjectEvent(this, ev);
		}

		private void OrdineSelezionato(object sender, string ev)
		{
			mOrderCode = ev;

			if (mProjects.GetDataFromDb(Project.Keys.F_CODE, mOrderCode))
			{
				FillTable();
				OnSelectProject(null);
			}
		}

        private void UpdateDetails(object sender, EventArgs ev)
        {
            if (mProjects.GetDataFromDb(Project.Keys.F_CODE, mOrderCode))
            {
                FillTable();
                OnUpdate();
            }
        }


        //Genera l'evento UpdateProject
        protected virtual void OnUpdate()
        {
            if (UpdateProject != null)
                UpdateProject(this, new EventArgs());
        }
        

		#endregion

		#region Abilita-Disabilita vari controlli

		private void AggiornaCampiNewMod()
		{
			// Aggiornamento dei campi di Edit a seconda dell'operazione in corso
			if (Operat==ProjectOperation.Edit)
			{
				CellValue();
			}
			else if (Operat==ProjectOperation.None)
			{
				lblOperation.Text="";
				//cmbxState.SelectedIndex = -1;
				deliveryDate.ResetText();
				txtDescription.Text = "";
				dataGridProjects.Enabled=true;
			}			
		}

		// Abilita i pulsanti della ToolBar
		private void EnableToolBar_Edit()
		{
			btnModifica.Enabled=true;
			btnSalva.Enabled=true;
			btnElimina.Enabled=true;
			btnReset.Enabled=true;
		}

		// Disabilita i pulsanti della ToolBar
		private void DisableToolBar_Edit()
		{
			btnModifica.Enabled=false;
			btnSalva.Enabled=false;
			btnElimina.Enabled=false;
			btnReset.Enabled=false;
		}

		// Abilita i controlli presenti nel panelEdit
		private void EnablePanelEdit()
		{
			//cmbxState.Enabled = true;
			deliveryDate.Visible = true;
			txtDeliveryDate.Visible = false;
			txtDescription.ReadOnly = false;
			btnConferma.Enabled=true;
			btnNascondi.Enabled=true;
		}

		// Disabilita i controlli presenti nel panelEdit
		private void DisablePanelEdit()
		{
			//cmbxState.Enabled = false;
			deliveryDate.Visible = false;
			txtDeliveryDate.Visible = true;
			txtDescription.ReadOnly = true;
			btnConferma.Enabled=false;
			btnNascondi.Enabled=false;
		}

		private void AttivaForm(string formName)
		{
			System.Windows.Forms.Form[] frm = new System.Windows.Forms.Form[this.ParentForm.MdiChildren.Length];
			frm =this.ParentForm.MdiChildren;
			for (int i=0;i<frm.Length;i++)
			{
				if(frm[i].Name == formName)
					frm[i].Activate();						
			}
		}

		#endregion

        #region Private Methods

        /// ***********************************************************
        /// LoadTecnoPhases
        /// <summary>
        /// Carica le collection con gli ordini e le fasi di lavoro
        /// </summary>
        /// <param name="code">Codice del dettaglio</param>
        /// <returns></returns>
        /// ***********************************************************        
        private bool LoadTecnoPhases(string code)
        {
            int i = 0;
            string[] usObjs = new string[0];
            string[] wkOrds = new string[0];
            WorkPhase wk = new WorkPhase();

            if (!mWorkPhases.GetDataFromDb(WorkPhase.Keys.F_SEQUENCE, null, code, null, null, null))
                return false;

            mWorkPhases.MoveFirst();
            while (!mWorkPhases.IsEOF())
            {
                mWorkPhases.GetObject(out wk);

                string[] tmp = new string[wkOrds.Length + 1];
                wkOrds.CopyTo(tmp, 0);
                wkOrds = tmp;
                wkOrds[i] = wk.gWorkOrderCode;

                string[] tmp2 = new string[usObjs.Length + 1];
                usObjs.CopyTo(tmp2, 0);
                usObjs = tmp2;
                usObjs[i] = wk.gUsedObjectCode;

                i++;
                mWorkPhases.MoveNext();
            }
            string[] tmp3 = new string[usObjs.Length + 1];
            usObjs.CopyTo(tmp3, 0);
            usObjs = tmp3;
            usObjs[i] = wk.gPhaseDUsedObject;

            if (!mWorkOrders.GetDataFromDb(WorkOrder.Keys.F_SEQUENCE, null, null, null, null, null, wkOrds))
                return false;

            if (!mUsedObjects.GetDataFromDb(usObjs))
                return false;

            return true;
        }

        ///**********************************************************
        /// Delete All
        /// <summary>
        /// Elimina il processo produttivo (fase tecno) per il pezzo
        /// </summary>
        /// <returns></returns>
        /// *********************************************************
        private bool DeleteAll()
        {
            try
            {
                // Cancellazione di tutti i record caricati
                mWorkPhases.MoveFirst();
                while (!mWorkPhases.IsEmpty())
                    mWorkPhases.DeleteObject();
                if (!mWorkPhases.UpdateDataToDb())
                    throw new ApplicationException("Error on work phases updated");

                mUsedObjects.MoveFirst();
                while (!mUsedObjects.IsEmpty())
                    mUsedObjects.DeleteObject();
                if (!mUsedObjects.UpdateDataToDb())
                    throw new ApplicationException("Error on used objects updated");

                mWorkOrders.MoveFirst();
                while (!mWorkOrders.IsEmpty())
                    mWorkOrders.DeleteObject();
                if (!mWorkOrders.UpdateDataToDb())
                    throw new ApplicationException("Error on work orders updated");

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Details.DeleteAll Error: " + ex.Message);
                return false;
            }
            finally
            {
                mWorkPhases = null;
                mUsedObjects = null;
                mWorkOrders = null;
            }
        }
        #endregion
	}
}
