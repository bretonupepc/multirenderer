namespace Breton.OfferManager.Form
{
    partial class OrdersSlabs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrdersSlabs));
			this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
			this.toolBar = new System.Windows.Forms.ToolBar();
			this.btnSearchSlabs = new System.Windows.Forms.ToolBarButton();
			this.btnSearchOrders = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton3 = new System.Windows.Forms.ToolBarButton();
			this.btnExit = new System.Windows.Forms.ToolBarButton();
			this.panelSearchSlabs = new System.Windows.Forms.Panel();
			this.btnConfermaOrdine = new System.Windows.Forms.Button();
			this.txtOrderCode = new System.Windows.Forms.TextBox();
			this.lblOrder = new System.Windows.Forms.Label();
			this.panelSearchOrders = new System.Windows.Forms.Panel();
			this.btnConfermaArticolo = new System.Windows.Forms.Button();
			this.txtArticleCode = new System.Windows.Forms.TextBox();
			this.lblArticle = new System.Windows.Forms.Label();
			this.panelPreview = new System.Windows.Forms.Panel();
			this.contextMenuDraw = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.miProject = new System.Windows.Forms.ToolStripMenuItem();
			this.miZoomAll = new System.Windows.Forms.ToolStripMenuItem();
			this.miLock = new System.Windows.Forms.ToolStripMenuItem();
			this.miZoom = new System.Windows.Forms.ToolStripMenuItem();
			this.mi10 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi50 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi100 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi150 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi200 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi300 = new System.Windows.Forms.ToolStripMenuItem();
			this.miQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miLinearQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miRadiusQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miAngleQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miAutoQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miDesignQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miReset = new System.Windows.Forms.ToolStripMenuItem();
			this.VDPreview = new AxVDProLib5.AxVDPro();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.tsslblOrtho = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslOperation = new System.Windows.Forms.ToolStripStatusLabel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panelDataOrders = new System.Windows.Forms.Panel();
			this.c1DataGridOrders = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.dataTableOrders = new System.Data.DataTable();
			this.dataColumn16 = new System.Data.DataColumn();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.dataColumn15 = new System.Data.DataColumn();
			this.dataColumn18 = new System.Data.DataColumn();
			this.dataColumn19 = new System.Data.DataColumn();
			this.dataColumn20 = new System.Data.DataColumn();
			this.dataColumn21 = new System.Data.DataColumn();
			this.dataColumn22 = new System.Data.DataColumn();
			this.dataSetOrdersSlabs = new System.Data.DataSet();
			this.dataTableArticles = new System.Data.DataTable();
			this.dataColumn17 = new System.Data.DataColumn();
			this.dataColumn9 = new System.Data.DataColumn();
			this.dataColumn10 = new System.Data.DataColumn();
			this.dataColumn11 = new System.Data.DataColumn();
			this.dataColumn12 = new System.Data.DataColumn();
			this.dataColumn13 = new System.Data.DataColumn();
			this.dataColumn14 = new System.Data.DataColumn();
			this.dataColumn23 = new System.Data.DataColumn();
			this.dataColumn24 = new System.Data.DataColumn();
			this.dataColumn25 = new System.Data.DataColumn();
			this.dataColumn26 = new System.Data.DataColumn();
			this.dataColumn27 = new System.Data.DataColumn();
			this.panelDataSlabs = new System.Windows.Forms.Panel();
			this.c1DataGridSlabs = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.panelSearchSlabs.SuspendLayout();
			this.panelSearchOrders.SuspendLayout();
			this.panelPreview.SuspendLayout();
			this.contextMenuDraw.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).BeginInit();
			this.statusStrip.SuspendLayout();
			this.panelDataOrders.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1DataGridOrders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableOrders)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetOrdersSlabs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableArticles)).BeginInit();
			this.panelDataSlabs.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.c1DataGridSlabs)).BeginInit();
			this.SuspendLayout();
			// 
			// imlToolIcon
			// 
			this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
			this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon.Images.SetKeyName(0, "Order.ico");
			this.imlToolIcon.Images.SetKeyName(1, "Slab.ico");
			this.imlToolIcon.Images.SetKeyName(2, "exit.ico");
			// 
			// toolBar
			// 
			this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnSearchSlabs,
            this.btnSearchOrders,
            this.toolBarButton3,
            this.btnExit});
			this.toolBar.DropDownArrows = true;
			this.toolBar.ImageList = this.imlToolIcon;
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.ShowToolTips = true;
			this.toolBar.Size = new System.Drawing.Size(818, 44);
			this.toolBar.TabIndex = 2;
			this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
			// 
			// btnSearchSlabs
			// 
			this.btnSearchSlabs.ImageIndex = 1;
			this.btnSearchSlabs.Name = "btnSearchSlabs";
			// 
			// btnSearchOrders
			// 
			this.btnSearchOrders.ImageIndex = 0;
			this.btnSearchOrders.Name = "btnSearchOrders";
			// 
			// toolBarButton3
			// 
			this.toolBarButton3.Name = "toolBarButton3";
			this.toolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnExit
			// 
			this.btnExit.ImageIndex = 2;
			this.btnExit.Name = "btnExit";
			// 
			// panelSearchSlabs
			// 
			this.panelSearchSlabs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelSearchSlabs.Controls.Add(this.btnConfermaOrdine);
			this.panelSearchSlabs.Controls.Add(this.txtOrderCode);
			this.panelSearchSlabs.Controls.Add(this.lblOrder);
			this.panelSearchSlabs.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelSearchSlabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.panelSearchSlabs.Location = new System.Drawing.Point(0, 44);
			this.panelSearchSlabs.Name = "panelSearchSlabs";
			this.panelSearchSlabs.Size = new System.Drawing.Size(818, 56);
			this.panelSearchSlabs.TabIndex = 3;
			this.panelSearchSlabs.Visible = false;
			// 
			// btnConfermaOrdine
			// 
			this.btnConfermaOrdine.Image = ((System.Drawing.Image)(resources.GetObject("btnConfermaOrdine.Image")));
			this.btnConfermaOrdine.Location = new System.Drawing.Point(399, 8);
			this.btnConfermaOrdine.Name = "btnConfermaOrdine";
			this.btnConfermaOrdine.Size = new System.Drawing.Size(36, 36);
			this.btnConfermaOrdine.TabIndex = 120;
			this.btnConfermaOrdine.Click += new System.EventHandler(this.btnConfermaOrdine_Click);
			// 
			// txtOrderCode
			// 
			this.txtOrderCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtOrderCode.Location = new System.Drawing.Point(85, 17);
			this.txtOrderCode.Name = "txtOrderCode";
			this.txtOrderCode.Size = new System.Drawing.Size(200, 24);
			this.txtOrderCode.TabIndex = 119;
			// 
			// lblOrder
			// 
			this.lblOrder.AutoSize = true;
			this.lblOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrder.Location = new System.Drawing.Point(14, 18);
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Size = new System.Drawing.Size(52, 18);
			this.lblOrder.TabIndex = 118;
			this.lblOrder.Text = "Ordine";
			// 
			// panelSearchOrders
			// 
			this.panelSearchOrders.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelSearchOrders.Controls.Add(this.btnConfermaArticolo);
			this.panelSearchOrders.Controls.Add(this.txtArticleCode);
			this.panelSearchOrders.Controls.Add(this.lblArticle);
			this.panelSearchOrders.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelSearchOrders.Location = new System.Drawing.Point(0, 100);
			this.panelSearchOrders.Name = "panelSearchOrders";
			this.panelSearchOrders.Size = new System.Drawing.Size(818, 56);
			this.panelSearchOrders.TabIndex = 4;
			this.panelSearchOrders.Visible = false;
			// 
			// btnConfermaArticolo
			// 
			this.btnConfermaArticolo.Image = ((System.Drawing.Image)(resources.GetObject("btnConfermaArticolo.Image")));
			this.btnConfermaArticolo.Location = new System.Drawing.Point(399, 8);
			this.btnConfermaArticolo.Name = "btnConfermaArticolo";
			this.btnConfermaArticolo.Size = new System.Drawing.Size(36, 36);
			this.btnConfermaArticolo.TabIndex = 120;
			this.btnConfermaArticolo.Click += new System.EventHandler(this.btnConfermaArticolo_Click);
			// 
			// txtArticleCode
			// 
			this.txtArticleCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtArticleCode.Location = new System.Drawing.Point(85, 17);
			this.txtArticleCode.Name = "txtArticleCode";
			this.txtArticleCode.Size = new System.Drawing.Size(200, 24);
			this.txtArticleCode.TabIndex = 119;
			// 
			// lblArticle
			// 
			this.lblArticle.AutoSize = true;
			this.lblArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblArticle.Location = new System.Drawing.Point(14, 18);
			this.lblArticle.Name = "lblArticle";
			this.lblArticle.Size = new System.Drawing.Size(58, 18);
			this.lblArticle.TabIndex = 118;
			this.lblArticle.Text = "Articolo";
			// 
			// panelPreview
			// 
			this.panelPreview.BackColor = System.Drawing.Color.Black;
			this.panelPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelPreview.ContextMenuStrip = this.contextMenuDraw;
			this.panelPreview.Controls.Add(this.VDPreview);
			this.panelPreview.Controls.Add(this.statusStrip);
			this.panelPreview.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelPreview.Location = new System.Drawing.Point(624, 156);
			this.panelPreview.Name = "panelPreview";
			this.panelPreview.Size = new System.Drawing.Size(194, 215);
			this.panelPreview.TabIndex = 11;
			// 
			// contextMenuDraw
			// 
			this.contextMenuDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.contextMenuDraw.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miProject,
            this.miZoomAll,
            this.miLock,
            this.miZoom,
            this.miQuote,
            this.miReset});
			this.contextMenuDraw.Name = "contextMenuStrip1";
			this.contextMenuDraw.Size = new System.Drawing.Size(179, 136);
			// 
			// miProject
			// 
			this.miProject.CheckOnClick = true;
			this.miProject.Name = "miProject";
			this.miProject.Size = new System.Drawing.Size(178, 22);
			this.miProject.Text = "Vedi progetto";
			this.miProject.CheckedChanged += new System.EventHandler(this.miProject_CheckedChanged);
			// 
			// miZoomAll
			// 
			this.miZoomAll.Name = "miZoomAll";
			this.miZoomAll.Size = new System.Drawing.Size(178, 22);
			this.miZoomAll.Text = "Adatta disegno";
			this.miZoomAll.Click += new System.EventHandler(this.miZoomAll_Click);
			// 
			// miLock
			// 
			this.miLock.CheckOnClick = true;
			this.miLock.Name = "miLock";
			this.miLock.Size = new System.Drawing.Size(178, 22);
			this.miLock.Text = "Blocca disegno";
			this.miLock.Click += new System.EventHandler(this.miLock_Click);
			// 
			// miZoom
			// 
			this.miZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi10,
            this.mi50,
            this.mi100,
            this.mi150,
            this.mi200,
            this.mi300});
			this.miZoom.Name = "miZoom";
			this.miZoom.Size = new System.Drawing.Size(178, 22);
			this.miZoom.Text = "Zoom";
			// 
			// mi10
			// 
			this.mi10.Name = "mi10";
			this.mi10.Size = new System.Drawing.Size(117, 22);
			this.mi10.Text = "10 %";
			this.mi10.Click += new System.EventHandler(this.mi10_Click);
			// 
			// mi50
			// 
			this.mi50.Name = "mi50";
			this.mi50.Size = new System.Drawing.Size(117, 22);
			this.mi50.Text = "50 %";
			this.mi50.Click += new System.EventHandler(this.mi50_Click);
			// 
			// mi100
			// 
			this.mi100.Name = "mi100";
			this.mi100.Size = new System.Drawing.Size(117, 22);
			this.mi100.Text = "100 %";
			this.mi100.Click += new System.EventHandler(this.mi100_Click);
			// 
			// mi150
			// 
			this.mi150.Name = "mi150";
			this.mi150.Size = new System.Drawing.Size(117, 22);
			this.mi150.Text = "150 %";
			this.mi150.Click += new System.EventHandler(this.mi150_Click);
			// 
			// mi200
			// 
			this.mi200.Name = "mi200";
			this.mi200.Size = new System.Drawing.Size(117, 22);
			this.mi200.Text = "200 %";
			this.mi200.Click += new System.EventHandler(this.mi200_Click);
			// 
			// mi300
			// 
			this.mi300.Name = "mi300";
			this.mi300.Size = new System.Drawing.Size(117, 22);
			this.mi300.Text = "300 %";
			this.mi300.Click += new System.EventHandler(this.mi300_Click);
			// 
			// miQuote
			// 
			this.miQuote.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miLinearQuote,
            this.miRadiusQuote,
            this.miAngleQuote,
            this.miAutoQuote,
            this.miDesignQuote});
			this.miQuote.Name = "miQuote";
			this.miQuote.Size = new System.Drawing.Size(178, 22);
			this.miQuote.Text = "Quote";
			// 
			// miLinearQuote
			// 
			this.miLinearQuote.CheckOnClick = true;
			this.miLinearQuote.Name = "miLinearQuote";
			this.miLinearQuote.Size = new System.Drawing.Size(202, 22);
			this.miLinearQuote.Text = "Quote lineari";
			this.miLinearQuote.CheckedChanged += new System.EventHandler(this.miLinearQuote_CheckedChanged);
			this.miLinearQuote.Click += new System.EventHandler(this.miLinearQuote_Click);
			// 
			// miRadiusQuote
			// 
			this.miRadiusQuote.CheckOnClick = true;
			this.miRadiusQuote.Name = "miRadiusQuote";
			this.miRadiusQuote.Size = new System.Drawing.Size(202, 22);
			this.miRadiusQuote.Text = "Quote raggio";
			this.miRadiusQuote.CheckedChanged += new System.EventHandler(this.miRadiusQuote_CheckedChanged);
			this.miRadiusQuote.Click += new System.EventHandler(this.miRadiusQuote_Click);
			// 
			// miAngleQuote
			// 
			this.miAngleQuote.CheckOnClick = true;
			this.miAngleQuote.Name = "miAngleQuote";
			this.miAngleQuote.Size = new System.Drawing.Size(202, 22);
			this.miAngleQuote.Text = "Quote angolari";
			this.miAngleQuote.CheckedChanged += new System.EventHandler(this.miAngleQuote_CheckedChanged);
			this.miAngleQuote.Click += new System.EventHandler(this.miAngleQuote_Click);
			// 
			// miAutoQuote
			// 
			this.miAutoQuote.CheckOnClick = true;
			this.miAutoQuote.Name = "miAutoQuote";
			this.miAutoQuote.Size = new System.Drawing.Size(202, 22);
			this.miAutoQuote.Text = "Quote automatiche";
			this.miAutoQuote.Click += new System.EventHandler(this.miAutoQuote_Click);
			// 
			// miDesignQuote
			// 
			this.miDesignQuote.CheckOnClick = true;
			this.miDesignQuote.Name = "miDesignQuote";
			this.miDesignQuote.Size = new System.Drawing.Size(202, 22);
			this.miDesignQuote.Text = "Quote Design";
			this.miDesignQuote.Click += new System.EventHandler(this.miDesignQuote_Click);
			// 
			// miReset
			// 
			this.miReset.Name = "miReset";
			this.miReset.Size = new System.Drawing.Size(178, 22);
			this.miReset.Text = "Reset";
			this.miReset.Click += new System.EventHandler(this.miReset_Click);
			// 
			// VDPreview
			// 
			this.VDPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.VDPreview.Enabled = true;
			this.VDPreview.Location = new System.Drawing.Point(0, 0);
			this.VDPreview.Name = "VDPreview";
			this.VDPreview.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("VDPreview.OcxState")));
			this.VDPreview.Size = new System.Drawing.Size(190, 182);
			this.VDPreview.TabIndex = 2;
			this.VDPreview.MouseDownEvent += new AxVDProLib5._DVdrawEvents_MouseDownEventHandler(this.VDPreview_MouseDownEvent);
			this.VDPreview.MouseWheelEvent += new AxVDProLib5._DVdrawEvents_MouseWheelEventHandler(this.VDPreview_MouseWheelEvent);
			this.VDPreview.Resize += new System.EventHandler(this.VDPreview_Resize);
			// 
			// statusStrip
			// 
			this.statusStrip.BackColor = System.Drawing.SystemColors.Control;
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslblOrtho,
            this.tsslOperation});
			this.statusStrip.Location = new System.Drawing.Point(0, 182);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(190, 29);
			this.statusStrip.TabIndex = 4;
			// 
			// tsslblOrtho
			// 
			this.tsslblOrtho.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.tsslblOrtho.Name = "tsslblOrtho";
			this.tsslblOrtho.Size = new System.Drawing.Size(91, 24);
			this.tsslblOrtho.Text = "ORTHO: Off";
			this.tsslblOrtho.Click += new System.EventHandler(this.tsslblOrtho_Click);
			// 
			// tsslOperation
			// 
			this.tsslOperation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tsslOperation.Name = "tsslOperation";
			this.tsslOperation.Size = new System.Drawing.Size(0, 24);
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
			this.splitter1.Location = new System.Drawing.Point(620, 156);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(4, 215);
			this.splitter1.TabIndex = 12;
			this.splitter1.TabStop = false;
			// 
			// panelDataOrders
			// 
			this.panelDataOrders.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelDataOrders.Controls.Add(this.c1DataGridOrders);
			this.panelDataOrders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelDataOrders.Location = new System.Drawing.Point(0, 156);
			this.panelDataOrders.Name = "panelDataOrders";
			this.panelDataOrders.Size = new System.Drawing.Size(620, 215);
			this.panelDataOrders.TabIndex = 13;
			this.panelDataOrders.Visible = false;
			// 
			// c1DataGridOrders
			// 
			this.c1DataGridOrders.AllowSort = false;
			this.c1DataGridOrders.AllowUpdate = false;
			this.c1DataGridOrders.Caption = "Ordini - Dettagli";
			this.c1DataGridOrders.CaptionHeight = 17;
			this.c1DataGridOrders.DataSource = this.dataTableOrders;
			this.c1DataGridOrders.Dock = System.Windows.Forms.DockStyle.Fill;
			this.c1DataGridOrders.GroupByCaption = "Drag a column header here to group by that column";
			this.c1DataGridOrders.Images.Add(((System.Drawing.Image)(resources.GetObject("c1DataGridOrders.Images"))));
			this.c1DataGridOrders.Location = new System.Drawing.Point(0, 0);
			this.c1DataGridOrders.Name = "c1DataGridOrders";
			this.c1DataGridOrders.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1DataGridOrders.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1DataGridOrders.PreviewInfo.ZoomFactor = 75D;
			this.c1DataGridOrders.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1DataGridOrders.PrintInfo.PageSettings")));
			this.c1DataGridOrders.RowHeight = 15;
			this.c1DataGridOrders.Size = new System.Drawing.Size(616, 211);
			this.c1DataGridOrders.TabIndex = 0;
			this.c1DataGridOrders.Text = "c1TrueDBGrid2";
			this.c1DataGridOrders.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.c1DataGridOrders_RowColChange);
			this.c1DataGridOrders.PropBag = resources.GetString("c1DataGridOrders.PropBag");
			// 
			// dataTableOrders
			// 
			this.dataTableOrders.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn16,
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn15,
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22});
			this.dataTableOrders.TableName = "TableOrders";
			// 
			// dataColumn16
			// 
			this.dataColumn16.ColumnName = "T_ID";
			this.dataColumn16.DataType = typeof(int);
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "F_ID";
			this.dataColumn1.DataType = typeof(int);
			// 
			// dataColumn2
			// 
			this.dataColumn2.Caption = "Codice ordine";
			this.dataColumn2.ColumnName = "F_ORDER_CODE";
			// 
			// dataColumn3
			// 
			this.dataColumn3.Caption = "Descrizione ordine";
			this.dataColumn3.ColumnName = "F_ORDER_DESCR";
			// 
			// dataColumn4
			// 
			this.dataColumn4.Caption = "Codice cliente";
			this.dataColumn4.ColumnName = "F_CUSTOMER_CODE";
			// 
			// dataColumn5
			// 
			this.dataColumn5.Caption = "Nome cliente";
			this.dataColumn5.ColumnName = "F_CUSTOMER_NAME";
			// 
			// dataColumn6
			// 
			this.dataColumn6.Caption = "Codice dettaglio";
			this.dataColumn6.ColumnName = "F_CUSTOMER_ORDER_DETAIL_CODE";
			// 
			// dataColumn7
			// 
			this.dataColumn7.Caption = "Descrizione dettaglio";
			this.dataColumn7.ColumnName = "F_CUSTOMER_ORDER_DETAIL_DESCR";
			// 
			// dataColumn8
			// 
			this.dataColumn8.Caption = "Codice pezzo";
			this.dataColumn8.ColumnName = "F_ARTICLE_CODE";
			// 
			// dataColumn15
			// 
			this.dataColumn15.Caption = "Posizione magazzino";
			this.dataColumn15.ColumnName = "F_POSITION";
			// 
			// dataColumn18
			// 
			this.dataColumn18.ColumnName = "F_TICKNESS";
			this.dataColumn18.DataType = typeof(double);
			// 
			// dataColumn19
			// 
			this.dataColumn19.ColumnName = "F_FINAL_THICKNESS";
			this.dataColumn19.DataType = typeof(double);
			// 
			// dataColumn20
			// 
			this.dataColumn20.ColumnName = "F_LENGTH";
			this.dataColumn20.DataType = typeof(double);
			// 
			// dataColumn21
			// 
			this.dataColumn21.ColumnName = "F_WIDTH";
			this.dataColumn21.DataType = typeof(double);
			// 
			// dataColumn22
			// 
			this.dataColumn22.ColumnName = "F_CODE_ORIGINAL_SHAPE";
			// 
			// dataSetOrdersSlabs
			// 
			this.dataSetOrdersSlabs.DataSetName = "NewDataSet";
			this.dataSetOrdersSlabs.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableOrders,
            this.dataTableArticles});
			// 
			// dataTableArticles
			// 
			this.dataTableArticles.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25,
            this.dataColumn26,
            this.dataColumn27});
			this.dataTableArticles.TableName = "TableArticles";
			// 
			// dataColumn17
			// 
			this.dataColumn17.ColumnName = "T_ID";
			this.dataColumn17.DataType = typeof(int);
			// 
			// dataColumn9
			// 
			this.dataColumn9.ColumnName = "F_ID";
			this.dataColumn9.DataType = typeof(int);
			// 
			// dataColumn10
			// 
			this.dataColumn10.Caption = "Codice lastra origine";
			this.dataColumn10.ColumnName = "F_ARTICLE_CODE_SOURCE";
			// 
			// dataColumn11
			// 
			this.dataColumn11.Caption = "Codice dettaglio";
			this.dataColumn11.ColumnName = "F_CUSTOMER_ORDER_DETAIL_CODE";
			// 
			// dataColumn12
			// 
			this.dataColumn12.Caption = "Descrizione dettaglio";
			this.dataColumn12.ColumnName = "F_CUSTOMER_ORDER_DETAIL_DESCR";
			// 
			// dataColumn13
			// 
			this.dataColumn13.Caption = "Codice pezzo finale";
			this.dataColumn13.ColumnName = "F_ARTICLE_CODE_DEST";
			// 
			// dataColumn14
			// 
			this.dataColumn14.Caption = "Posizione magazzino";
			this.dataColumn14.ColumnName = "F_POSITION";
			// 
			// dataColumn23
			// 
			this.dataColumn23.ColumnName = "F_THICKNESS";
			this.dataColumn23.DataType = typeof(double);
			// 
			// dataColumn24
			// 
			this.dataColumn24.ColumnName = "F_FINAL_THICKNESS";
			this.dataColumn24.DataType = typeof(double);
			// 
			// dataColumn25
			// 
			this.dataColumn25.ColumnName = "F_LENGTH";
			this.dataColumn25.DataType = typeof(double);
			// 
			// dataColumn26
			// 
			this.dataColumn26.ColumnName = "F_WIDTH";
			this.dataColumn26.DataType = typeof(double);
			// 
			// dataColumn27
			// 
			this.dataColumn27.ColumnName = "F_CODE_ORIGINAL_SHAPE";
			// 
			// panelDataSlabs
			// 
			this.panelDataSlabs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelDataSlabs.Controls.Add(this.c1DataGridSlabs);
			this.panelDataSlabs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelDataSlabs.Location = new System.Drawing.Point(0, 156);
			this.panelDataSlabs.Name = "panelDataSlabs";
			this.panelDataSlabs.Size = new System.Drawing.Size(620, 215);
			this.panelDataSlabs.TabIndex = 14;
			this.panelDataSlabs.Visible = false;
			// 
			// c1DataGridSlabs
			// 
			this.c1DataGridSlabs.AllowSort = false;
			this.c1DataGridSlabs.AllowUpdate = false;
			this.c1DataGridSlabs.Caption = "Articoli (lastre, recuperi)";
			this.c1DataGridSlabs.CaptionHeight = 17;
			this.c1DataGridSlabs.DataSource = this.dataTableArticles;
			this.c1DataGridSlabs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.c1DataGridSlabs.GroupByCaption = "Drag a column header here to group by that column";
			this.c1DataGridSlabs.Images.Add(((System.Drawing.Image)(resources.GetObject("c1DataGridSlabs.Images"))));
			this.c1DataGridSlabs.Location = new System.Drawing.Point(0, 0);
			this.c1DataGridSlabs.Name = "c1DataGridSlabs";
			this.c1DataGridSlabs.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.c1DataGridSlabs.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.c1DataGridSlabs.PreviewInfo.ZoomFactor = 75D;
			this.c1DataGridSlabs.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("c1DataGridSlabs.PrintInfo.PageSettings")));
			this.c1DataGridSlabs.RowHeight = 15;
			this.c1DataGridSlabs.Size = new System.Drawing.Size(616, 211);
			this.c1DataGridSlabs.TabIndex = 0;
			this.c1DataGridSlabs.Text = "c1TrueDBGrid1";
			this.c1DataGridSlabs.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.c1DataGridSlabs_RowColChange);
			this.c1DataGridSlabs.PropBag = resources.GetString("c1DataGridSlabs.PropBag");
			// 
			// OrdersSlabs
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(818, 371);
			this.Controls.Add(this.panelDataSlabs);
			this.Controls.Add(this.panelDataOrders);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.panelPreview);
			this.Controls.Add(this.panelSearchOrders);
			this.Controls.Add(this.panelSearchSlabs);
			this.Controls.Add(this.toolBar);
			this.Name = "OrdersSlabs";
			this.Text = "OrdersSlabs";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrdersSlabs_FormClosed);
			this.Load += new System.EventHandler(this.OrdersSlabs_Load);
			this.panelSearchSlabs.ResumeLayout(false);
			this.panelSearchSlabs.PerformLayout();
			this.panelSearchOrders.ResumeLayout(false);
			this.panelSearchOrders.PerformLayout();
			this.panelPreview.ResumeLayout(false);
			this.panelPreview.PerformLayout();
			this.contextMenuDraw.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).EndInit();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.panelDataOrders.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.c1DataGridOrders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableOrders)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataSetOrdersSlabs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableArticles)).EndInit();
			this.panelDataSlabs.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.c1DataGridSlabs)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imlToolIcon;
        private System.Windows.Forms.ToolBar toolBar;
        private System.Windows.Forms.ToolBarButton btnSearchSlabs;
        private System.Windows.Forms.ToolBarButton btnSearchOrders;
        private System.Windows.Forms.ToolBarButton toolBarButton3;
        private System.Windows.Forms.ToolBarButton btnExit;
        private System.Windows.Forms.Panel panelSearchSlabs;
        private System.Windows.Forms.Panel panelSearchOrders;
        private System.Windows.Forms.Button btnConfermaOrdine;
        private System.Windows.Forms.TextBox txtOrderCode;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.Button btnConfermaArticolo;
        private System.Windows.Forms.TextBox txtArticleCode;
        private System.Windows.Forms.Label lblArticle;
        private System.Windows.Forms.Panel panelPreview;
        private AxVDProLib5.AxVDPro VDPreview;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panelDataOrders;
        private System.Windows.Forms.Panel panelDataSlabs;
        private System.Data.DataSet dataSetOrdersSlabs;
        private System.Data.DataTable dataTableOrders;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataTable dataTableArticles;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1DataGridOrders;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid c1DataGridSlabs;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
		private System.Data.DataColumn dataColumn18;
		private System.Data.DataColumn dataColumn19;
		private System.Data.DataColumn dataColumn20;
		private System.Data.DataColumn dataColumn21;
		private System.Data.DataColumn dataColumn22;
		private System.Data.DataColumn dataColumn23;
		private System.Data.DataColumn dataColumn24;
		private System.Data.DataColumn dataColumn25;
		private System.Data.DataColumn dataColumn26;
		private System.Data.DataColumn dataColumn27;
		private System.Windows.Forms.ContextMenuStrip contextMenuDraw;
		private System.Windows.Forms.ToolStripMenuItem miProject;
		private System.Windows.Forms.ToolStripMenuItem miZoomAll;
		private System.Windows.Forms.ToolStripMenuItem miLock;
		private System.Windows.Forms.ToolStripMenuItem miZoom;
		private System.Windows.Forms.ToolStripMenuItem mi10;
		private System.Windows.Forms.ToolStripMenuItem mi50;
		private System.Windows.Forms.ToolStripMenuItem mi100;
		private System.Windows.Forms.ToolStripMenuItem mi150;
		private System.Windows.Forms.ToolStripMenuItem mi200;
		private System.Windows.Forms.ToolStripMenuItem mi300;
		private System.Windows.Forms.ToolStripMenuItem miQuote;
		private System.Windows.Forms.ToolStripMenuItem miLinearQuote;
		private System.Windows.Forms.ToolStripMenuItem miRadiusQuote;
		private System.Windows.Forms.ToolStripMenuItem miAngleQuote;
		private System.Windows.Forms.ToolStripMenuItem miAutoQuote;
		private System.Windows.Forms.ToolStripMenuItem miDesignQuote;
		private System.Windows.Forms.ToolStripMenuItem miReset;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel tsslblOrtho;
		private System.Windows.Forms.ToolStripStatusLabel tsslOperation;
    }
}