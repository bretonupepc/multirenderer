namespace Breton.OfferManager.Form
{
    partial class DetailsAdvance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DetailsAdvance));
			this.imlToolIcon = new System.Windows.Forms.ImageList(this.components);
			this.dataSetDetails = new System.Data.DataSet();
			this.dataTableDetails = new System.Data.DataTable();
			this.dataColumnT_ID = new System.Data.DataColumn();
			this.dataColumnF_ID = new System.Data.DataColumn();
			this.dataColumnF_CODE = new System.Data.DataColumn();
			this.dataColumnF_CUSTOMER_PROJECT_CODE = new System.Data.DataColumn();
			this.dataColumnF_MATERIAL_CODE = new System.Data.DataColumn();
			this.dataColumnF_MATERIAL_DESCRIPTION = new System.Data.DataColumn();
			this.dataColumnF_STATE = new System.Data.DataColumn();
			this.dataColumnF_THICKNESS = new System.Data.DataColumn();
			this.dataColumnF_ARTICLE_CODE = new System.Data.DataColumn();
			this.dataColumnF_CODE_ORIGINALE_SHAPE = new System.Data.DataColumn();
			this.dataColumnF_DESCRIPTION = new System.Data.DataColumn();
			this.dataColumnF_MAG_POSITION = new System.Data.DataColumn();
			this.dataColumnF_FINAL_THICKNESS = new System.Data.DataColumn();
			this.dataColumnF_LENGTH = new System.Data.DataColumn();
			this.dataColumnF_WIDTH = new System.Data.DataColumn();
			this.panelData = new System.Windows.Forms.Panel();
			this.dataGridDetails = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.splitter = new System.Windows.Forms.Splitter();
			this.panelPreview = new System.Windows.Forms.Panel();
			this.contextMenuDraw = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.miProject = new System.Windows.Forms.ToolStripMenuItem();
			this.miZoomAll = new System.Windows.Forms.ToolStripMenuItem();
			this.miLock = new System.Windows.Forms.ToolStripMenuItem();
			this.miZoom = new System.Windows.Forms.ToolStripMenuItem();
			this.mi10 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi50 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi100 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi150 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi200 = new System.Windows.Forms.ToolStripMenuItem();
			this.mi300 = new System.Windows.Forms.ToolStripMenuItem();
			this.miQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miLinearQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miRadiusQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miAngleQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miAutoQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miDesignQuote = new System.Windows.Forms.ToolStripMenuItem();
			this.miReset = new System.Windows.Forms.ToolStripMenuItem();
			this.VDPreview = new AxVDProLib5.AxVDPro();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.tsslblOrtho = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslOperation = new System.Windows.Forms.ToolStripStatusLabel();
			this.lblShapeInfo = new System.Windows.Forms.Label();
			this.toolBar = new System.Windows.Forms.ToolBar();
			this.btnReopenDetail = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
			this.btnAnnulla = new System.Windows.Forms.ToolBarButton();
			this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
			this.btnRefresh = new System.Windows.Forms.ToolBarButton();
			this.btnSelectDetail = new System.Windows.Forms.ToolBarButton();
			((System.ComponentModel.ISupportInitialize)(this.dataSetDetails)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableDetails)).BeginInit();
			this.panelData.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).BeginInit();
			this.panelPreview.SuspendLayout();
			this.contextMenuDraw.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).BeginInit();
			this.statusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// imlToolIcon
			// 
			this.imlToolIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlToolIcon.ImageStream")));
			this.imlToolIcon.TransparentColor = System.Drawing.Color.Transparent;
			this.imlToolIcon.Images.SetKeyName(0, "ReopenDetail.ico");
			this.imlToolIcon.Images.SetKeyName(1, "");
			this.imlToolIcon.Images.SetKeyName(2, "Refresh.ico");
			this.imlToolIcon.Images.SetKeyName(3, "SelectDetail.ico");
			// 
			// dataSetDetails
			// 
			this.dataSetDetails.DataSetName = "NewDataSet";
			this.dataSetDetails.Locale = new System.Globalization.CultureInfo("en-US");
			this.dataSetDetails.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableDetails});
			// 
			// dataTableDetails
			// 
			this.dataTableDetails.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnT_ID,
            this.dataColumnF_ID,
            this.dataColumnF_CODE,
            this.dataColumnF_CUSTOMER_PROJECT_CODE,
            this.dataColumnF_MATERIAL_CODE,
            this.dataColumnF_MATERIAL_DESCRIPTION,
            this.dataColumnF_STATE,
            this.dataColumnF_THICKNESS,
            this.dataColumnF_ARTICLE_CODE,
            this.dataColumnF_CODE_ORIGINALE_SHAPE,
            this.dataColumnF_DESCRIPTION,
            this.dataColumnF_MAG_POSITION,
            this.dataColumnF_FINAL_THICKNESS,
            this.dataColumnF_LENGTH,
            this.dataColumnF_WIDTH});
			this.dataTableDetails.TableName = "TableDetails";
			// 
			// dataColumnT_ID
			// 
			this.dataColumnT_ID.ColumnName = "T_ID";
			this.dataColumnT_ID.DataType = typeof(int);
			// 
			// dataColumnF_ID
			// 
			this.dataColumnF_ID.Caption = "Id";
			this.dataColumnF_ID.ColumnName = "F_ID";
			this.dataColumnF_ID.DataType = typeof(int);
			// 
			// dataColumnF_CODE
			// 
			this.dataColumnF_CODE.Caption = "Codice";
			this.dataColumnF_CODE.ColumnName = "F_CODE";
			// 
			// dataColumnF_CUSTOMER_PROJECT_CODE
			// 
			this.dataColumnF_CUSTOMER_PROJECT_CODE.Caption = "Codice progetto";
			this.dataColumnF_CUSTOMER_PROJECT_CODE.ColumnName = "F_CUSTOMER_PROJECT_CODE";
			// 
			// dataColumnF_MATERIAL_CODE
			// 
			this.dataColumnF_MATERIAL_CODE.Caption = "Codice materiale";
			this.dataColumnF_MATERIAL_CODE.ColumnName = "F_MATERIAL_CODE";
			// 
			// dataColumnF_MATERIAL_DESCRIPTION
			// 
			this.dataColumnF_MATERIAL_DESCRIPTION.Caption = "Descrizione materiale";
			this.dataColumnF_MATERIAL_DESCRIPTION.ColumnName = "F_MATERIAL_DESCRIPTION";
			// 
			// dataColumnF_STATE
			// 
			this.dataColumnF_STATE.Caption = "Stato";
			this.dataColumnF_STATE.ColumnName = "F_STATE";
			// 
			// dataColumnF_THICKNESS
			// 
			this.dataColumnF_THICKNESS.Caption = "Spessore";
			this.dataColumnF_THICKNESS.ColumnName = "F_THICKNESS";
			this.dataColumnF_THICKNESS.DataType = typeof(double);
			// 
			// dataColumnF_ARTICLE_CODE
			// 
			this.dataColumnF_ARTICLE_CODE.Caption = "Codice articolo";
			this.dataColumnF_ARTICLE_CODE.ColumnName = "F_ARTICLE_CODE";
			// 
			// dataColumnF_CODE_ORIGINALE_SHAPE
			// 
			this.dataColumnF_CODE_ORIGINALE_SHAPE.ColumnName = "F_CODE_ORIGINALE_SHAPE";
			// 
			// dataColumnF_DESCRIPTION
			// 
			this.dataColumnF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
			// 
			// dataColumnF_MAG_POSITION
			// 
			this.dataColumnF_MAG_POSITION.ColumnName = "F_MAG_POSITION";
			// 
			// dataColumnF_FINAL_THICKNESS
			// 
			this.dataColumnF_FINAL_THICKNESS.ColumnName = "F_FINAL_THICKNESS";
			this.dataColumnF_FINAL_THICKNESS.DataType = typeof(double);
			// 
			// dataColumnF_LENGTH
			// 
			this.dataColumnF_LENGTH.ColumnName = "F_LENGTH";
			this.dataColumnF_LENGTH.DataType = typeof(double);
			// 
			// dataColumnF_WIDTH
			// 
			this.dataColumnF_WIDTH.ColumnName = "F_WIDTH";
			this.dataColumnF_WIDTH.DataType = typeof(double);
			// 
			// panelData
			// 
			this.panelData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelData.Controls.Add(this.dataGridDetails);
			this.panelData.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelData.Location = new System.Drawing.Point(0, 44);
			this.panelData.Name = "panelData";
			this.panelData.Size = new System.Drawing.Size(603, 339);
			this.panelData.TabIndex = 10;
			// 
			// dataGridDetails
			// 
			this.dataGridDetails.AllowSort = false;
			this.dataGridDetails.AllowUpdate = false;
			this.dataGridDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridDetails.CaptionHeight = 17;
			this.dataGridDetails.DataSource = this.dataTableDetails;
			this.dataGridDetails.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridDetails.FetchRowStyles = true;
			this.dataGridDetails.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridDetails.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridDetails.Images"))));
			this.dataGridDetails.Location = new System.Drawing.Point(0, 0);
			this.dataGridDetails.MaintainRowCurrency = true;
			this.dataGridDetails.Name = "dataGridDetails";
			this.dataGridDetails.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridDetails.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridDetails.PreviewInfo.ZoomFactor = 75;
			this.dataGridDetails.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridDetails.PrintInfo.PageSettings")));
			this.dataGridDetails.RowHeight = 15;
			this.dataGridDetails.Size = new System.Drawing.Size(599, 335);
			this.dataGridDetails.TabIndex = 7;
			this.dataGridDetails.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridDetails_FetchRowStyle);
			this.dataGridDetails.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.dataGridDetails_RowColChange);
			this.dataGridDetails.PropBag = resources.GetString("dataGridDetails.PropBag");
			// 
			// splitter
			// 
			this.splitter.Location = new System.Drawing.Point(603, 44);
			this.splitter.Name = "splitter";
			this.splitter.Size = new System.Drawing.Size(5, 339);
			this.splitter.TabIndex = 12;
			this.splitter.TabStop = false;
			// 
			// panelPreview
			// 
			this.panelPreview.BackColor = System.Drawing.Color.Black;
			this.panelPreview.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelPreview.ContextMenuStrip = this.contextMenuDraw;
			this.panelPreview.Controls.Add(this.VDPreview);
			this.panelPreview.Controls.Add(this.statusStrip);
			this.panelPreview.Controls.Add(this.lblShapeInfo);
			this.panelPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelPreview.Location = new System.Drawing.Point(608, 44);
			this.panelPreview.Name = "panelPreview";
			this.panelPreview.Size = new System.Drawing.Size(308, 339);
			this.panelPreview.TabIndex = 13;
			// 
			// contextMenuDraw
			// 
			this.contextMenuDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.contextMenuDraw.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miProject,
            this.miZoomAll,
            this.miLock,
            this.miZoom,
            this.miQuote,
            this.miReset});
			this.contextMenuDraw.Name = "contextMenuStrip1";
			this.contextMenuDraw.Size = new System.Drawing.Size(183, 136);
			// 
			// miProject
			// 
			this.miProject.CheckOnClick = true;
			this.miProject.Name = "miProject";
			this.miProject.Size = new System.Drawing.Size(182, 22);
			this.miProject.Text = "Vedi progetto";
			this.miProject.CheckedChanged += new System.EventHandler(this.miProject_CheckedChanged);
			// 
			// miZoomAll
			// 
			this.miZoomAll.Name = "miZoomAll";
			this.miZoomAll.Size = new System.Drawing.Size(182, 22);
			this.miZoomAll.Text = "Adatta disegno";
			this.miZoomAll.Click += new System.EventHandler(this.miZoomAll_Click);
			// 
			// miLock
			// 
			this.miLock.CheckOnClick = true;
			this.miLock.Name = "miLock";
			this.miLock.Size = new System.Drawing.Size(182, 22);
			this.miLock.Text = "Blocca disegno";
			this.miLock.Click += new System.EventHandler(this.miLock_Click);
			// 
			// miZoom
			// 
			this.miZoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mi10,
            this.mi50,
            this.mi100,
            this.mi150,
            this.mi200,
            this.mi300});
			this.miZoom.Name = "miZoom";
			this.miZoom.Size = new System.Drawing.Size(182, 22);
			this.miZoom.Text = "Zoom";
			// 
			// mi10
			// 
			this.mi10.Name = "mi10";
			this.mi10.Size = new System.Drawing.Size(121, 22);
			this.mi10.Text = "10 %";
			this.mi10.Click += new System.EventHandler(this.mi10_Click);
			// 
			// mi50
			// 
			this.mi50.Name = "mi50";
			this.mi50.Size = new System.Drawing.Size(121, 22);
			this.mi50.Text = "50 %";
			this.mi50.Click += new System.EventHandler(this.mi50_Click);
			// 
			// mi100
			// 
			this.mi100.Name = "mi100";
			this.mi100.Size = new System.Drawing.Size(121, 22);
			this.mi100.Text = "100 %";
			this.mi100.Click += new System.EventHandler(this.mi100_Click);
			// 
			// mi150
			// 
			this.mi150.Name = "mi150";
			this.mi150.Size = new System.Drawing.Size(121, 22);
			this.mi150.Text = "150 %";
			this.mi150.Click += new System.EventHandler(this.mi150_Click);
			// 
			// mi200
			// 
			this.mi200.Name = "mi200";
			this.mi200.Size = new System.Drawing.Size(121, 22);
			this.mi200.Text = "200 %";
			this.mi200.Click += new System.EventHandler(this.mi200_Click);
			// 
			// mi300
			// 
			this.mi300.Name = "mi300";
			this.mi300.Size = new System.Drawing.Size(121, 22);
			this.mi300.Text = "300 %";
			this.mi300.Click += new System.EventHandler(this.mi300_Click);
			// 
			// miQuote
			// 
			this.miQuote.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miLinearQuote,
            this.miRadiusQuote,
            this.miAngleQuote,
            this.miAutoQuote,
            this.miDesignQuote});
			this.miQuote.Name = "miQuote";
			this.miQuote.Size = new System.Drawing.Size(182, 22);
			this.miQuote.Text = "Quote";
			// 
			// miLinearQuote
			// 
			this.miLinearQuote.CheckOnClick = true;
			this.miLinearQuote.Name = "miLinearQuote";
			this.miLinearQuote.Size = new System.Drawing.Size(206, 22);
			this.miLinearQuote.Text = "Quote lineari";
			this.miLinearQuote.CheckedChanged += new System.EventHandler(this.miLinearQuote_CheckedChanged);
			this.miLinearQuote.Click += new System.EventHandler(this.miLinearQuote_Click);
			// 
			// miRadiusQuote
			// 
			this.miRadiusQuote.CheckOnClick = true;
			this.miRadiusQuote.Name = "miRadiusQuote";
			this.miRadiusQuote.Size = new System.Drawing.Size(206, 22);
			this.miRadiusQuote.Text = "Quote raggio";
			this.miRadiusQuote.CheckedChanged += new System.EventHandler(this.miRadiusQuote_CheckedChanged);
			this.miRadiusQuote.Click += new System.EventHandler(this.miRadiusQuote_Click);
			// 
			// miAngleQuote
			// 
			this.miAngleQuote.CheckOnClick = true;
			this.miAngleQuote.Name = "miAngleQuote";
			this.miAngleQuote.Size = new System.Drawing.Size(206, 22);
			this.miAngleQuote.Text = "Quote angolari";
			this.miAngleQuote.CheckedChanged += new System.EventHandler(this.miAngleQuote_CheckedChanged);
			this.miAngleQuote.Click += new System.EventHandler(this.miAngleQuote_Click);
			// 
			// miAutoQuote
			// 
			this.miAutoQuote.CheckOnClick = true;
			this.miAutoQuote.Name = "miAutoQuote";
			this.miAutoQuote.Size = new System.Drawing.Size(206, 22);
			this.miAutoQuote.Text = "Quote automatiche";
			this.miAutoQuote.Click += new System.EventHandler(this.miAutoQuote_Click);
			// 
			// miDesignQuote
			// 
			this.miDesignQuote.Name = "miDesignQuote";
			this.miDesignQuote.Size = new System.Drawing.Size(206, 22);
			this.miDesignQuote.Text = "Quote Design";
			this.miDesignQuote.Click += new System.EventHandler(this.miDesignQuote_Click);
			// 
			// miReset
			// 
			this.miReset.Name = "miReset";
			this.miReset.Size = new System.Drawing.Size(182, 22);
			this.miReset.Text = "Reset";
			this.miReset.Click += new System.EventHandler(this.miReset_Click);
			// 
			// VDPreview
			// 
			this.VDPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.VDPreview.Enabled = true;
			this.VDPreview.Location = new System.Drawing.Point(0, 18);
			this.VDPreview.Name = "VDPreview";
			this.VDPreview.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("VDPreview.OcxState")));
			this.VDPreview.Size = new System.Drawing.Size(304, 291);
			this.VDPreview.TabIndex = 0;
			this.VDPreview.MouseDownEvent += new AxVDProLib5._DVdrawEvents_MouseDownEventHandler(this.VDPreview_MouseDownEvent);
			this.VDPreview.MouseWheelEvent += new AxVDProLib5._DVdrawEvents_MouseWheelEventHandler(this.VDPreview_MouseWheelEvent);
			this.VDPreview.Resize += new System.EventHandler(this.VDPreview_Resize);
			// 
			// statusStrip
			// 
			this.statusStrip.BackColor = System.Drawing.SystemColors.Control;
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslblOrtho,
            this.tsslOperation});
			this.statusStrip.Location = new System.Drawing.Point(0, 309);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(304, 26);
			this.statusStrip.TabIndex = 4;
			// 
			// tsslblOrtho
			// 
			this.tsslblOrtho.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.tsslblOrtho.Name = "tsslblOrtho";
			this.tsslblOrtho.Size = new System.Drawing.Size(85, 21);
			this.tsslblOrtho.Text = "ORTHO: Off";
			this.tsslblOrtho.Click += new System.EventHandler(this.tsslblOrtho_Click);
			// 
			// tsslOperation
			// 
			this.tsslOperation.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tsslOperation.Name = "tsslOperation";
			this.tsslOperation.Size = new System.Drawing.Size(0, 21);
			// 
			// lblShapeInfo
			// 
			this.lblShapeInfo.AutoSize = true;
			this.lblShapeInfo.BackColor = System.Drawing.Color.Black;
			this.lblShapeInfo.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblShapeInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblShapeInfo.ForeColor = System.Drawing.Color.White;
			this.lblShapeInfo.Location = new System.Drawing.Point(0, 0);
			this.lblShapeInfo.Name = "lblShapeInfo";
			this.lblShapeInfo.Size = new System.Drawing.Size(0, 18);
			this.lblShapeInfo.TabIndex = 1;
			// 
			// toolBar
			// 
			this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
			this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.btnReopenDetail,
            this.toolBarButton1,
            this.btnAnnulla,
            this.toolBarButton2,
            this.btnRefresh,
            this.btnSelectDetail});
			this.toolBar.DropDownArrows = true;
			this.toolBar.ImageList = this.imlToolIcon;
			this.toolBar.Location = new System.Drawing.Point(0, 0);
			this.toolBar.Name = "toolBar";
			this.toolBar.ShowToolTips = true;
			this.toolBar.Size = new System.Drawing.Size(916, 44);
			this.toolBar.TabIndex = 14;
			this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
			// 
			// btnReopenDetail
			// 
			this.btnReopenDetail.ImageIndex = 0;
			this.btnReopenDetail.Name = "btnReopenDetail";
			this.btnReopenDetail.ToolTipText = "Riprogramma il pezzo";
			this.btnReopenDetail.Visible = false;
			// 
			// toolBarButton1
			// 
			this.toolBarButton1.Name = "toolBarButton1";
			this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			this.toolBarButton1.Visible = false;
			// 
			// btnAnnulla
			// 
			this.btnAnnulla.ImageIndex = 1;
			this.btnAnnulla.Name = "btnAnnulla";
			this.btnAnnulla.ToolTipText = "Esci";
			// 
			// toolBarButton2
			// 
			this.toolBarButton2.Name = "toolBarButton2";
			this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
			// 
			// btnRefresh
			// 
			this.btnRefresh.ImageIndex = 2;
			this.btnRefresh.Name = "btnRefresh";
			// 
			// btnSelectDetail
			// 
			this.btnSelectDetail.ImageIndex = 3;
			this.btnSelectDetail.Name = "btnSelectDetail";
			// 
			// DetailsAdvance
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.ClientSize = new System.Drawing.Size(916, 383);
			this.Controls.Add(this.panelPreview);
			this.Controls.Add(this.splitter);
			this.Controls.Add(this.panelData);
			this.Controls.Add(this.toolBar);
			this.Name = "DetailsAdvance";
			this.Text = "Dettagli - Avanzamento della produzione";
			this.Load += new System.EventHandler(this.DetailsAdvance_Load);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DetailsAdvance_FormClosed);
			this.Resize += new System.EventHandler(this.DetailsAdvance_Resize);
			((System.ComponentModel.ISupportInitialize)(this.dataSetDetails)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableDetails)).EndInit();
			this.panelData.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridDetails)).EndInit();
			this.panelPreview.ResumeLayout(false);
			this.panelPreview.PerformLayout();
			this.contextMenuDraw.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.VDPreview)).EndInit();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imlToolIcon;
        private System.Data.DataSet dataSetDetails;
        private System.Data.DataTable dataTableDetails;
        private System.Data.DataColumn dataColumnT_ID;
        private System.Data.DataColumn dataColumnF_ID;
        private System.Data.DataColumn dataColumnF_CODE;
        private System.Data.DataColumn dataColumnF_CUSTOMER_PROJECT_CODE;
        private System.Data.DataColumn dataColumnF_MATERIAL_CODE;
        private System.Data.DataColumn dataColumnF_MATERIAL_DESCRIPTION;
        private System.Data.DataColumn dataColumnF_STATE;
        private System.Data.DataColumn dataColumnF_THICKNESS;
        private System.Data.DataColumn dataColumnF_ARTICLE_CODE;
        private System.Data.DataColumn dataColumnF_CODE_ORIGINALE_SHAPE;
        private System.Data.DataColumn dataColumnF_DESCRIPTION;
        private System.Windows.Forms.Panel panelData;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridDetails;
        private System.Windows.Forms.Splitter splitter;
        private System.Windows.Forms.Panel panelPreview;
        private AxVDProLib5.AxVDPro VDPreview;
        private System.Windows.Forms.Label lblShapeInfo;
        private System.Data.DataColumn dataColumnF_MAG_POSITION;
        private System.Windows.Forms.ToolBar toolBar;
        private System.Windows.Forms.ToolBarButton toolBarButton1;
        private System.Windows.Forms.ToolBarButton btnAnnulla;
        private System.Windows.Forms.ToolBarButton btnReopenDetail;
        private System.Windows.Forms.ToolBarButton toolBarButton2;
        private System.Windows.Forms.ToolBarButton btnRefresh;
        private System.Windows.Forms.ContextMenuStrip contextMenuDraw;
        private System.Windows.Forms.ToolStripMenuItem miProject;
        private System.Windows.Forms.ToolStripMenuItem miZoomAll;
        private System.Windows.Forms.ToolStripMenuItem miLock;
        private System.Windows.Forms.ToolStripMenuItem miZoom;
        private System.Windows.Forms.ToolStripMenuItem mi10;
        private System.Windows.Forms.ToolStripMenuItem mi50;
        private System.Windows.Forms.ToolStripMenuItem mi100;
        private System.Windows.Forms.ToolStripMenuItem mi150;
        private System.Windows.Forms.ToolStripMenuItem mi200;
        private System.Windows.Forms.ToolStripMenuItem mi300;
        private System.Data.DataColumn dataColumnF_FINAL_THICKNESS;
        private System.Data.DataColumn dataColumnF_LENGTH;
        private System.Data.DataColumn dataColumnF_WIDTH;
        private System.Windows.Forms.ToolStripMenuItem miQuote;
        private System.Windows.Forms.ToolStripMenuItem miLinearQuote;
        private System.Windows.Forms.ToolStripMenuItem miRadiusQuote;
        private System.Windows.Forms.ToolStripMenuItem miAngleQuote;
        private System.Windows.Forms.ToolStripMenuItem miAutoQuote;
        private System.Windows.Forms.ToolStripMenuItem miReset;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel tsslblOrtho;
        private System.Windows.Forms.ToolStripStatusLabel tsslOperation;
		private System.Windows.Forms.ToolStripMenuItem miDesignQuote;
		private System.Windows.Forms.ToolBarButton btnSelectDetail;
    }
}