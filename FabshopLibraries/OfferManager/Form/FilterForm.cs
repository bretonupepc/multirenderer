using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Breton.ImportOffer;

namespace Breton.OfferManager.Form
{
	public class FilterForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnNascondi;
		private System.Windows.Forms.Button btnConferma;
		private System.Windows.Forms.TextBox txtField;
		private System.Windows.Forms.ComboBox cmbOperField;
		private System.Windows.Forms.TextBox txtToField;
		private System.Windows.Forms.ComboBox cmbToField;

		private System.ComponentModel.Container components = null;

		public FilterForm()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilterForm));
            this.txtField = new System.Windows.Forms.TextBox();
            this.cmbOperField = new System.Windows.Forms.ComboBox();
            this.txtToField = new System.Windows.Forms.TextBox();
            this.cmbToField = new System.Windows.Forms.ComboBox();
            this.btnNascondi = new System.Windows.Forms.Button();
            this.btnConferma = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtField
            // 
            this.txtField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtField.Location = new System.Drawing.Point(48, 16);
            this.txtField.Name = "txtField";
            this.txtField.Size = new System.Drawing.Size(224, 20);
            this.txtField.TabIndex = 11;
            // 
            // cmbOperField
            // 
            this.cmbOperField.DisplayMember = "=";
            this.cmbOperField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOperField.Items.AddRange(new object[] {
            "=",
            ">",
            "<",
            "<>"});
            this.cmbOperField.Location = new System.Drawing.Point(8, 16);
            this.cmbOperField.Name = "cmbOperField";
            this.cmbOperField.Size = new System.Drawing.Size(41, 21);
            this.cmbOperField.TabIndex = 10;
            this.cmbOperField.SelectedIndexChanged += new System.EventHandler(this.cmbOperField_SelectedIndexChanged);
            // 
            // txtToField
            // 
            this.txtToField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToField.Location = new System.Drawing.Point(48, 40);
            this.txtToField.Name = "txtToField";
            this.txtToField.Size = new System.Drawing.Size(224, 20);
            this.txtToField.TabIndex = 13;
            this.txtToField.Visible = false;
            // 
            // cmbToField
            // 
            this.cmbToField.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbToField.Items.AddRange(new object[] {
            "",
            "<"});
            this.cmbToField.Location = new System.Drawing.Point(8, 40);
            this.cmbToField.Name = "cmbToField";
            this.cmbToField.Size = new System.Drawing.Size(41, 21);
            this.cmbToField.TabIndex = 12;
            this.cmbToField.Visible = false;
            // 
            // btnNascondi
            // 
            this.btnNascondi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnNascondi.Image = ((System.Drawing.Image)(resources.GetObject("btnNascondi.Image")));
            this.btnNascondi.Location = new System.Drawing.Point(232, 62);
            this.btnNascondi.Name = "btnNascondi";
            this.btnNascondi.Size = new System.Drawing.Size(40, 40);
            this.btnNascondi.TabIndex = 101;
            // 
            // btnConferma
            // 
            this.btnConferma.Image = ((System.Drawing.Image)(resources.GetObject("btnConferma.Image")));
            this.btnConferma.Location = new System.Drawing.Point(192, 62);
            this.btnConferma.Name = "btnConferma";
            this.btnConferma.Size = new System.Drawing.Size(40, 40);
            this.btnConferma.TabIndex = 100;
            // 
            // FilterForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(280, 104);
            this.Controls.Add(this.btnNascondi);
            this.Controls.Add(this.btnConferma);
            this.Controls.Add(this.txtField);
            this.Controls.Add(this.cmbOperField);
            this.Controls.Add(this.txtToField);
            this.Controls.Add(this.cmbToField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FilterForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Filtro";
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
		
		#region Eventi dei controlli
		
		private void cmbOperField_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(cmbOperField.SelectedIndex == 1)
			{
				cmbToField.Visible = true;
				txtToField.Visible = true;
			}
			else
			{
				cmbToField.SelectedIndex = 0;
				txtToField.Text = "";
				cmbToField.Visible = false;
				txtToField.Visible = false;
			}
		}

		#endregion
	}
}
