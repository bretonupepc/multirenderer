﻿namespace Breton.LineBlocksSlabs.Form
{
	partial class LineSlabs
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LineSlabs));
			this.panelAll = new System.Windows.Forms.Panel();
			this.panelGrid = new System.Windows.Forms.Panel();
			this.dataTableLineSlabs = new System.Data.DataTable();
			this.dataColumnT_ID = new System.Data.DataColumn();
			this.dataColumnF_ID = new System.Data.DataColumn();
			this.dataColumnF_CODE = new System.Data.DataColumn();
			this.dataColumn1 = new System.Data.DataColumn();
			this.dataColumn2 = new System.Data.DataColumn();
			this.dataColumn3 = new System.Data.DataColumn();
			this.dataColumn4 = new System.Data.DataColumn();
			this.dataColumn5 = new System.Data.DataColumn();
			this.dataColumn6 = new System.Data.DataColumn();
			this.dataColumn7 = new System.Data.DataColumn();
			this.dataColumn8 = new System.Data.DataColumn();
			this.dataColumn9 = new System.Data.DataColumn();
			this.dataColumn10 = new System.Data.DataColumn();
			this.dataColumn11 = new System.Data.DataColumn();
			this.dataColumn12 = new System.Data.DataColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.chbxShowProcessed = new System.Windows.Forms.CheckBox();
			this.lblBlockDescription = new System.Windows.Forms.Label();
			this.lblBlock = new System.Windows.Forms.Label();
			this.panelUpDown = new System.Windows.Forms.Panel();
			this.txtPriority = new System.Windows.Forms.TextBox();
			this.panelNewSlab = new System.Windows.Forms.Panel();
			this.txtSlabCode = new System.Windows.Forms.TextBox();
			this.panelCommand = new System.Windows.Forms.Panel();
			this.lblTitle = new System.Windows.Forms.Label();
			this.dataSetLineSlabs = new System.Data.DataSet();
			this.btnSetAllProcessed = new System.Windows.Forms.Button();
			this.dataGridLineSlabs = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.btnDown = new System.Windows.Forms.Button();
			this.btnUp = new System.Windows.Forms.Button();
			this.btnConfirmSlab = new System.Windows.Forms.Button();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.btnChangeStatus = new System.Windows.Forms.Button();
			this.btnDeleteSlab = new System.Windows.Forms.Button();
			this.btnEditSequence = new System.Windows.Forms.Button();
			this.btnAddCode = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.panelAll.SuspendLayout();
			this.panelGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataTableLineSlabs)).BeginInit();
			this.panel1.SuspendLayout();
			this.panelUpDown.SuspendLayout();
			this.panelNewSlab.SuspendLayout();
			this.panelCommand.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataSetLineSlabs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridLineSlabs)).BeginInit();
			this.SuspendLayout();
			// 
			// panelAll
			// 
			this.panelAll.BackColor = System.Drawing.Color.LightSkyBlue;
			this.panelAll.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelAll.Controls.Add(this.panelGrid);
			this.panelAll.Controls.Add(this.panelUpDown);
			this.panelAll.Controls.Add(this.panelNewSlab);
			this.panelAll.Controls.Add(this.panelCommand);
			this.panelAll.Controls.Add(this.lblTitle);
			this.panelAll.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelAll.Location = new System.Drawing.Point(20, 20);
			this.panelAll.Name = "panelAll";
			this.panelAll.Size = new System.Drawing.Size(828, 593);
			this.panelAll.TabIndex = 0;
			// 
			// panelGrid
			// 
			this.panelGrid.Controls.Add(this.dataGridLineSlabs);
			this.panelGrid.Controls.Add(this.panel1);
			this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelGrid.Location = new System.Drawing.Point(0, 50);
			this.panelGrid.Name = "panelGrid";
			this.panelGrid.Size = new System.Drawing.Size(642, 476);
			this.panelGrid.TabIndex = 15;
			// 
			// dataTableLineSlabs
			// 
			this.dataTableLineSlabs.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnT_ID,
            this.dataColumnF_ID,
            this.dataColumnF_CODE,
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12});
			this.dataTableLineSlabs.TableName = "tableLineSlabs";
			// 
			// dataColumnT_ID
			// 
			this.dataColumnT_ID.ColumnName = "T_ID";
			this.dataColumnT_ID.DataType = typeof(int);
			// 
			// dataColumnF_ID
			// 
			this.dataColumnF_ID.ColumnName = "F_ID";
			this.dataColumnF_ID.DataType = typeof(int);
			// 
			// dataColumnF_CODE
			// 
			this.dataColumnF_CODE.ColumnName = "F_CODE";
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "F_BLOCK_CODE";
			// 
			// dataColumn2
			// 
			this.dataColumn2.ColumnName = "F_PRIORITY";
			this.dataColumn2.DataType = typeof(int);
			// 
			// dataColumn3
			// 
			this.dataColumn3.ColumnName = "F_STATE";
			// 
			// dataColumn4
			// 
			this.dataColumn4.ColumnName = "F_EXTERNAL_CODE";
			// 
			// dataColumn5
			// 
			this.dataColumn5.ColumnName = "F_BATCH_CODE";
			// 
			// dataColumn6
			// 
			this.dataColumn6.ColumnName = "F_LENGTH";
			this.dataColumn6.DataType = typeof(double);
			// 
			// dataColumn7
			// 
			this.dataColumn7.ColumnName = "F_WIDTH";
			this.dataColumn7.DataType = typeof(double);
			// 
			// dataColumn8
			// 
			this.dataColumn8.ColumnName = "F_THICKNESS";
			this.dataColumn8.DataType = typeof(double);
			// 
			// dataColumn9
			// 
			this.dataColumn9.ColumnName = "F_WEIGHT";
			this.dataColumn9.DataType = typeof(double);
			// 
			// dataColumn10
			// 
			this.dataColumn10.ColumnName = "F_QUALITY";
			// 
			// dataColumn11
			// 
			this.dataColumn11.ColumnName = "F_MAT_CODE";
			// 
			// dataColumn12
			// 
			this.dataColumn12.ColumnName = "F_MAT_DESCRIPTION";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.chbxShowProcessed);
			this.panel1.Controls.Add(this.lblBlockDescription);
			this.panel1.Controls.Add(this.lblBlock);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(642, 51);
			this.panel1.TabIndex = 1;
			// 
			// chbxShowProcessed
			// 
			this.chbxShowProcessed.AutoSize = true;
			this.chbxShowProcessed.Dock = System.Windows.Forms.DockStyle.Right;
			this.chbxShowProcessed.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.chbxShowProcessed.ForeColor = System.Drawing.Color.DimGray;
			this.chbxShowProcessed.Location = new System.Drawing.Point(529, 0);
			this.chbxShowProcessed.Name = "chbxShowProcessed";
			this.chbxShowProcessed.Size = new System.Drawing.Size(113, 51);
			this.chbxShowProcessed.TabIndex = 1;
			this.chbxShowProcessed.Text = "Processate";
			this.chbxShowProcessed.UseVisualStyleBackColor = true;
			this.chbxShowProcessed.CheckedChanged += new System.EventHandler(this.chbxShowProcessed_CheckedChanged);
			// 
			// lblBlockDescription
			// 
			this.lblBlockDescription.AutoSize = true;
			this.lblBlockDescription.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblBlockDescription.Location = new System.Drawing.Point(3, 17);
			this.lblBlockDescription.Name = "lblBlockDescription";
			this.lblBlockDescription.Size = new System.Drawing.Size(73, 21);
			this.lblBlockDescription.TabIndex = 0;
			this.lblBlockDescription.Text = "Blocco:";
			// 
			// lblBlock
			// 
			this.lblBlock.AutoSize = true;
			this.lblBlock.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblBlock.Location = new System.Drawing.Point(82, 9);
			this.lblBlock.Name = "lblBlock";
			this.lblBlock.Size = new System.Drawing.Size(0, 29);
			this.lblBlock.TabIndex = 0;
			// 
			// panelUpDown
			// 
			this.panelUpDown.Controls.Add(this.txtPriority);
			this.panelUpDown.Controls.Add(this.btnDown);
			this.panelUpDown.Controls.Add(this.btnUp);
			this.panelUpDown.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelUpDown.Location = new System.Drawing.Point(642, 50);
			this.panelUpDown.Name = "panelUpDown";
			this.panelUpDown.Size = new System.Drawing.Size(77, 476);
			this.panelUpDown.TabIndex = 17;
			this.panelUpDown.Visible = false;
			// 
			// txtPriority
			// 
			this.txtPriority.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.txtPriority.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtPriority.Location = new System.Drawing.Point(7, 137);
			this.txtPriority.MaxLength = 18;
			this.txtPriority.Name = "txtPriority";
			this.txtPriority.Size = new System.Drawing.Size(60, 40);
			this.txtPriority.TabIndex = 10;
			this.txtPriority.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.txtPriority.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPriority_KeyPress);
			this.txtPriority.Validated += new System.EventHandler(this.txtPriority_Validated);
			// 
			// panelNewSlab
			// 
			this.panelNewSlab.Controls.Add(this.btnConfirmSlab);
			this.panelNewSlab.Controls.Add(this.txtSlabCode);
			this.panelNewSlab.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelNewSlab.Location = new System.Drawing.Point(0, 526);
			this.panelNewSlab.Name = "panelNewSlab";
			this.panelNewSlab.Size = new System.Drawing.Size(719, 63);
			this.panelNewSlab.TabIndex = 17;
			this.panelNewSlab.Visible = false;
			// 
			// txtSlabCode
			// 
			this.txtSlabCode.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.txtSlabCode.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSlabCode.Location = new System.Drawing.Point(213, 12);
			this.txtSlabCode.MaxLength = 18;
			this.txtSlabCode.Name = "txtSlabCode";
			this.txtSlabCode.Size = new System.Drawing.Size(278, 40);
			this.txtSlabCode.TabIndex = 0;
			// 
			// panelCommand
			// 
			this.panelCommand.Controls.Add(this.btnSetAllProcessed);
			this.panelCommand.Controls.Add(this.btnRefresh);
			this.panelCommand.Controls.Add(this.btnChangeStatus);
			this.panelCommand.Controls.Add(this.btnDeleteSlab);
			this.panelCommand.Controls.Add(this.btnEditSequence);
			this.panelCommand.Controls.Add(this.btnAddCode);
			this.panelCommand.Controls.Add(this.btnClose);
			this.panelCommand.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelCommand.Location = new System.Drawing.Point(719, 50);
			this.panelCommand.Name = "panelCommand";
			this.panelCommand.Size = new System.Drawing.Size(105, 539);
			this.panelCommand.TabIndex = 16;
			// 
			// lblTitle
			// 
			this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
			this.lblTitle.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitle.Location = new System.Drawing.Point(0, 0);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(824, 50);
			this.lblTitle.TabIndex = 14;
			this.lblTitle.Text = "Slab list";
			this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dataSetLineSlabs
			// 
			this.dataSetLineSlabs.DataSetName = "dataSetLineSlabs";
			this.dataSetLineSlabs.Locale = new System.Globalization.CultureInfo("it-IT");
			this.dataSetLineSlabs.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableLineSlabs});
			// 
			// btnSetAllProcessed
			// 
			this.btnSetAllProcessed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSetAllProcessed.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSetAllProcessed.Image = global::LineBlocksSlabs.Properties.Resources.ResetSlabsList;
			this.btnSetAllProcessed.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnSetAllProcessed.Location = new System.Drawing.Point(23, 289);
			this.btnSetAllProcessed.Name = "btnSetAllProcessed";
			this.btnSetAllProcessed.Size = new System.Drawing.Size(80, 70);
			this.btnSetAllProcessed.TabIndex = 9;
			this.btnSetAllProcessed.Text = "Reset List";
			this.btnSetAllProcessed.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnSetAllProcessed.UseVisualStyleBackColor = true;
			this.btnSetAllProcessed.Click += new System.EventHandler(this.btnSetAllProcessed_Click);
			// 
			// dataGridLineSlabs
			// 
			this.dataGridLineSlabs.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.IndividualRows;
			this.dataGridLineSlabs.AllowSort = false;
			this.dataGridLineSlabs.AllowUpdate = false;
			this.dataGridLineSlabs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dataGridLineSlabs.CaptionHeight = 21;
			this.dataGridLineSlabs.DataSource = this.dataTableLineSlabs;
			this.dataGridLineSlabs.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridLineSlabs.ExtendRightColumn = true;
			this.dataGridLineSlabs.FetchRowStyles = true;
			this.dataGridLineSlabs.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dataGridLineSlabs.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridLineSlabs.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridLineSlabs.Images"))));
			this.dataGridLineSlabs.LinesPerRow = 1;
			this.dataGridLineSlabs.Location = new System.Drawing.Point(0, 51);
			this.dataGridLineSlabs.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRowRaiseCell;
			this.dataGridLineSlabs.MultiSelect = C1.Win.C1TrueDBGrid.MultiSelectEnum.None;
			this.dataGridLineSlabs.Name = "dataGridLineSlabs";
			this.dataGridLineSlabs.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridLineSlabs.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridLineSlabs.PreviewInfo.ZoomFactor = 75D;
			this.dataGridLineSlabs.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridLineSlabs.PrintInfo.PageSettings")));
			this.dataGridLineSlabs.RecordSelectorWidth = 30;
			this.dataGridLineSlabs.RowHeight = 45;
			this.dataGridLineSlabs.Size = new System.Drawing.Size(642, 425);
			this.dataGridLineSlabs.TabIndex = 88;
			this.dataGridLineSlabs.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Black;
			this.dataGridLineSlabs.RowColChange += new C1.Win.C1TrueDBGrid.RowColChangeEventHandler(this.dataGridLineSlabs_RowColChange);
			this.dataGridLineSlabs.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridLineSlabs_FetchRowStyle);
			this.dataGridLineSlabs.PropBag = resources.GetString("dataGridLineSlabs.PropBag");
			// 
			// btnDown
			// 
			this.btnDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDown.Image = global::LineBlocksSlabs.Properties.Resources.pDw;
			this.btnDown.Location = new System.Drawing.Point(7, 249);
			this.btnDown.Name = "btnDown";
			this.btnDown.Size = new System.Drawing.Size(60, 60);
			this.btnDown.TabIndex = 9;
			this.btnDown.UseVisualStyleBackColor = true;
			this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
			// 
			// btnUp
			// 
			this.btnUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.btnUp.Image = global::LineBlocksSlabs.Properties.Resources.pUp;
			this.btnUp.Location = new System.Drawing.Point(7, 183);
			this.btnUp.Name = "btnUp";
			this.btnUp.Size = new System.Drawing.Size(60, 60);
			this.btnUp.TabIndex = 8;
			this.btnUp.UseVisualStyleBackColor = true;
			this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
			// 
			// btnConfirmSlab
			// 
			this.btnConfirmSlab.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.btnConfirmSlab.Image = global::LineBlocksSlabs.Properties.Resources.ConfermaB;
			this.btnConfirmSlab.Location = new System.Drawing.Point(497, 7);
			this.btnConfirmSlab.Name = "btnConfirmSlab";
			this.btnConfirmSlab.Size = new System.Drawing.Size(45, 45);
			this.btnConfirmSlab.TabIndex = 7;
			this.btnConfirmSlab.UseVisualStyleBackColor = true;
			this.btnConfirmSlab.Click += new System.EventHandler(this.btnConfirmSlab_Click);
			// 
			// btnRefresh
			// 
			this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRefresh.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRefresh.Image = global::LineBlocksSlabs.Properties.Resources.refresh;
			this.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnRefresh.Location = new System.Drawing.Point(23, 371);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(80, 70);
			this.btnRefresh.TabIndex = 8;
			this.btnRefresh.Text = "Aggiorna";
			this.btnRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnRefresh.UseVisualStyleBackColor = true;
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// btnChangeStatus
			// 
			this.btnChangeStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnChangeStatus.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnChangeStatus.Image = global::LineBlocksSlabs.Properties.Resources.black_ball32;
			this.btnChangeStatus.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnChangeStatus.Location = new System.Drawing.Point(23, 219);
			this.btnChangeStatus.Name = "btnChangeStatus";
			this.btnChangeStatus.Size = new System.Drawing.Size(80, 70);
			this.btnChangeStatus.TabIndex = 7;
			this.btnChangeStatus.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnChangeStatus.UseVisualStyleBackColor = true;
			this.btnChangeStatus.Click += new System.EventHandler(this.btnChangeStatus_Click);
			// 
			// btnDeleteSlab
			// 
			this.btnDeleteSlab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDeleteSlab.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDeleteSlab.Image = global::LineBlocksSlabs.Properties.Resources.Delete;
			this.btnDeleteSlab.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnDeleteSlab.Location = new System.Drawing.Point(23, 149);
			this.btnDeleteSlab.Name = "btnDeleteSlab";
			this.btnDeleteSlab.Size = new System.Drawing.Size(80, 70);
			this.btnDeleteSlab.TabIndex = 7;
			this.btnDeleteSlab.Text = "Elimina";
			this.btnDeleteSlab.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnDeleteSlab.UseVisualStyleBackColor = true;
			this.btnDeleteSlab.Click += new System.EventHandler(this.btnDeleteSlab_Click);
			// 
			// btnEditSequence
			// 
			this.btnEditSequence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEditSequence.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnEditSequence.Image = global::LineBlocksSlabs.Properties.Resources.up_down;
			this.btnEditSequence.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnEditSequence.Location = new System.Drawing.Point(23, 79);
			this.btnEditSequence.Name = "btnEditSequence";
			this.btnEditSequence.Size = new System.Drawing.Size(80, 70);
			this.btnEditSequence.TabIndex = 7;
			this.btnEditSequence.Text = "Sequenza";
			this.btnEditSequence.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnEditSequence.UseVisualStyleBackColor = true;
			this.btnEditSequence.Click += new System.EventHandler(this.btnEditSequence_Click);
			// 
			// btnAddCode
			// 
			this.btnAddCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAddCode.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAddCode.Image = global::LineBlocksSlabs.Properties.Resources.AddSlab;
			this.btnAddCode.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnAddCode.Location = new System.Drawing.Point(23, 9);
			this.btnAddCode.Name = "btnAddCode";
			this.btnAddCode.Size = new System.Drawing.Size(80, 70);
			this.btnAddCode.TabIndex = 7;
			this.btnAddCode.Text = "Aggiungi";
			this.btnAddCode.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnAddCode.UseVisualStyleBackColor = true;
			this.btnAddCode.Click += new System.EventHandler(this.btnAddCode_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnClose.Image = global::LineBlocksSlabs.Properties.Resources.Exit_1;
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnClose.Location = new System.Drawing.Point(23, 464);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(80, 70);
			this.btnClose.TabIndex = 6;
			this.btnClose.Text = "Esci";
			this.btnClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// LineSlabs
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.CornflowerBlue;
			this.ClientSize = new System.Drawing.Size(868, 633);
			this.Controls.Add(this.panelAll);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "LineSlabs";
			this.Padding = new System.Windows.Forms.Padding(20);
			this.Text = "LineSlabs";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LineSlabs_FormClosed);
			this.Load += new System.EventHandler(this.LineSlabs_Load);
			this.panelAll.ResumeLayout(false);
			this.panelGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataTableLineSlabs)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panelUpDown.ResumeLayout(false);
			this.panelUpDown.PerformLayout();
			this.panelNewSlab.ResumeLayout(false);
			this.panelNewSlab.PerformLayout();
			this.panelCommand.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataSetLineSlabs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridLineSlabs)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panelAll;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Panel panelGrid;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.Label lblBlockDescription;
		private System.Windows.Forms.Panel panelCommand;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lblBlock;
		private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridLineSlabs;
		private System.Data.DataTable dataTableLineSlabs;
		private System.Data.DataColumn dataColumnT_ID;
		private System.Data.DataColumn dataColumnF_ID;
		private System.Data.DataColumn dataColumnF_CODE;
		private System.Data.DataColumn dataColumn1;
		private System.Data.DataColumn dataColumn2;
		private System.Data.DataSet dataSetLineSlabs;
		private System.Windows.Forms.Button btnEditSequence;
		private System.Windows.Forms.Button btnAddCode;
		private System.Windows.Forms.Button btnDeleteSlab;
		private System.Data.DataColumn dataColumn3;
		private System.Windows.Forms.Panel panelUpDown;
		private System.Windows.Forms.Panel panelNewSlab;
		private System.Windows.Forms.Button btnConfirmSlab;
		private System.Windows.Forms.TextBox txtSlabCode;
		private System.Windows.Forms.Button btnDown;
		private System.Windows.Forms.Button btnUp;
		private System.Windows.Forms.TextBox txtPriority;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.Button btnChangeStatus;
		private System.Data.DataColumn dataColumn4;
		private System.Windows.Forms.CheckBox chbxShowProcessed;
		private System.Data.DataColumn dataColumn5;
		private System.Data.DataColumn dataColumn6;
		private System.Data.DataColumn dataColumn7;
		private System.Data.DataColumn dataColumn8;
		private System.Data.DataColumn dataColumn9;
		private System.Data.DataColumn dataColumn10;
		private System.Data.DataColumn dataColumn11;
		private System.Data.DataColumn dataColumn12;
		private System.Windows.Forms.Button btnSetAllProcessed;
	}
}