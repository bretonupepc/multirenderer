﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Breton.Parameters;
using Breton.Users;
using Breton.DbAccess;
using Breton.TraceLoggers;
using Breton.Materials;

namespace Breton.LineBlocksSlabs.Form
{
	public partial class LineSlabs : System.Windows.Forms.Form
	{
		#region Variables
		private DbInterface mDbInterface = null;
		private DbInterface mDbInterfaceLineBS = null;

		private User mUser = null;
		private string mLanguage;
		private ParameterControlPositions mFormPosition;

		private static bool formCreate = false;
		//private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;

		private LineBlock mBlock;
		private LineSlabCollection mLineSlabs;
		private bool mBlockMode;

		public static event Action<string> UpdateSlabsBlockEvent;
		#endregion

		#region Constructor
		public LineSlabs(DbInterface db, DbInterface dbLSB, User user, string language, LineBlock block): this(db, dbLSB, user, language, block, true)
		{
			
		}

		public LineSlabs(DbInterface db, DbInterface dbLSB, User user, string language, LineBlock block, bool blockMode)
		{
			InitializeComponent();

			mUser = user;

			if (db == null)
			{
				mDbInterface = new DbInterface();
				mDbInterface.Open("", "DbConnection.udl");
			}
			else
				mDbInterface = db;

			if (dbLSB == null)
			{
				mDbInterfaceLineBS = new DbInterface();
				mDbInterfaceLineBS.Open("", "DbLineBlocksSlabs.udl");
			}
			else
				mDbInterfaceLineBS = dbLSB;

			// Carica la posizione del form
			mFormPosition = new ParameterControlPositions(this.Name);
			mFormPosition.LoadFormPosition(this);

			//istanzio la collection delle lastre
			mLineSlabs = new LineSlabCollection(mDbInterfaceLineBS);

			mLanguage = language;
			ProjResource.gResource.ChangeLanguage(mLanguage);

			mBlock = block;
			mBlockMode = blockMode;
		}
		#endregion

		#region Properties

		public static bool gCreate
		{
			set { formCreate = value; }
			get { return formCreate; }
		}

		#endregion

		#region Form Function
		private void LineSlabs_Load(object sender, EventArgs e)
		{
			this.Dock = DockStyle.Fill;

			gCreate = true;

			// Carica le lingue del form
			ProjResource.gResource.LoadStringForm(this);
			//mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridLineSlabs, dataTableLineSlabs);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridLineSlabs);

			if (mBlockMode && mBlock != null)
			{
				btnAddCode.Enabled = true;
				lblBlock.Visible = true;
				lblBlockDescription.Visible = true;
				lblBlock.Text = mBlock.gCode.Trim();

				// La lista lastre non viene più caricata qui perchè viene lasciata al cambio del checkbox Processata 

				////Legge la lista delle lastre associate al blocco
				//if (mLineSlabs.GetDataFromDb(LineSlab.Keys.F_PRIORITY, mBlock.gCode))
				//    FillTable();	//Riempe la Table
			}
			else
			{
				btnAddCode.Enabled = false;
				lblBlock.Visible = false;
				lblBlockDescription.Visible = false;

				// La lista lastre non viene più caricata qui perchè viene lasciata al cambio del checkbox Processata

				////Legge la lista delle lastre associate al blocco
				//if (mLineSlabs.GetDataFromDb(LineSlab.Keys.F_PRIORITY))
				//    FillTable();	//Riempe la Table
			}

			// Abilita o meno la visualizzazione delle lastre processate o meno in base alla modalità di lavoro in cui siamo
			if (mBlockMode)
				chbxShowProcessed.Checked = true;
			else
			{
				chbxShowProcessed.Checked = false;
				chbxShowProcessed_CheckedChanged(null, null);
			}
		}

		private void LineSlabs_FormClosed(object sender, FormClosedEventArgs e)
		{
			mFormPosition.SaveFormPosition(this);
			gCreate = false;
		}
		#endregion

		#region Gestione Tabelle

		/// <summary>
		/// Scorre la collection per riempire la table
		/// </summary>
		private void FillTable()
		{
			LineSlab slb;

			dataTableLineSlabs.Clear();			

			// Scorre tutte le lastre lette
			mLineSlabs.MoveFirst();
			while (!mLineSlabs.IsEOF())
			{
				// Legge la classe di materiale attuale
				mLineSlabs.GetObject(out slb);

				dataTableLineSlabs.BeginInit();
				dataTableLineSlabs.BeginLoadData();

				AddRow(ref slb);

				dataTableLineSlabs.EndInit();
				dataTableLineSlabs.EndLoadData();

				mLineSlabs.MoveNext();
			}

			
		}

		/// <summary>
		/// Aggiunge una riga al dataTable
		/// </summary>
		/// <param name="blc"></param>
		private void AddRow(ref LineSlab slb)
		{
			object[] myArray = new object[15];
			myArray[0] = dataTableLineSlabs.Rows.Count;
			myArray[1] = slb.gId;
			myArray[2] = slb.gCode;
			myArray[3] = slb.gLineBlockCode;
			myArray[4] = slb.gPriority;
			myArray[5] = slb.gLineSlabState;
			myArray[6] = slb.gExternalCode;
			myArray[7] = slb.gBatchCode; 
			myArray[8] = slb.gLength;
			myArray[9] = slb.gWidth;
			myArray[10] = slb.gThickness;
			myArray[11] = slb.gWeight;
			myArray[12] = slb.gQualityCode;
			Material mat;
			GetBlockMaterial(slb.gLineBlockCode, out mat);
			if (mat != null)
			{
				myArray[13] = mat.gCode;
				myArray[14] = mat.gDescription;
			}

			// Crea una nuova riga nella tabella e la riempie di dati
			DataRow r = dataTableLineSlabs.NewRow();
			r.ItemArray = myArray;
			dataTableLineSlabs.Rows.Add(r);

			// Assegna l'id della riga tabella alla qualità
			slb.gTableId = (int)r.ItemArray[0];
		}

		/// <summary>
		/// Cancella una riga dalla griglia
		/// </summary>
		private void Delete()
		{
			LineSlab slb = null;
			int j = 0;
			string message, caption;

			if (dataGridLineSlabs.Bookmark >= 0)
			{
				// Per il momento da la possibilità di eliminare una sola riga per volta

				// Indice della riga da eliminare
				int[] indexDelete = new int[0];
				//while (j < dataGridLineSlabs.SelectedRows.Count)
				//{
				//    int[] tmp = new int[indexDelete.Length + 1];
				//    indexDelete.CopyTo(tmp, 0);
				//    indexDelete = tmp;
				//    mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.SelectedRows[j], 0].ToString()), out slb);
				//    indexDelete[j] = slb.gTableId;
				//    j++;
				//}
				if (dataGridLineSlabs.SelectedRows.Count == 0)
				{
					indexDelete = new int[1];
					mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()), out slb);
					indexDelete[j] = slb.gTableId;
				}
				if (slb == null)
					return;

				if (slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
				{
					ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}

				ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
				if (indexDelete.Length > 1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					for (int i = 0; i < indexDelete.Length; i++)
					{
						if (!mLineSlabs.DeleteObjectTable(indexDelete[i]))
							TraceLog.WriteLine("LineBlocks: Delete() Error");
					}
					ResetIndexTable();
				}
				else if (indexDelete.Length == 1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " eliminata manualmente dalla lista");
					// Elimina la riga dalla collection
					mLineSlabs.DeleteObjectTable(indexDelete[0]);
					ResetIndexTable();
				}

				if (mLineSlabs.UpdateDataToDb())
					FillTable();
			}
		}

		/// <summary>
		/// reset dell'indice della tabella e le priorità
		/// </summary>
		private void ResetIndexTable()
		{
			LineSlab slb;
			int i = 0;

			mLineSlabs.MoveFirst();
			while (!mLineSlabs.IsEOF())
			{
				// Legge la qualità attuale
				mLineSlabs.GetObject(out slb);

				slb.gTableId = i;
				
				// La priorità non viene più aggiornata in fase di eliminazione di una lastra perchè nella modalità "lastre" aggiornava la priorità in modo errato
				// Aggiorno la priorità (indice di priorità parte da 1) (Solo se sono in modalità blocchi)
				//slb.gPriority = i + 1;

				i++;
				mLineSlabs.MoveNext();
			}
			for (i = 0; i <= dataTableLineSlabs.Rows.Count - 1; i++)
			{
				object[] myArray = new object[15];
				myArray = dataTableLineSlabs.Rows[i].ItemArray;
				myArray[0] = i;
				dataTableLineSlabs.Rows[i].ItemArray = myArray;
			}
		}

		private void GetBlockMaterial(string blockCode, out Material mat)
		{
			mat = null;

			LineBlock blk;
			LineBlockCollection blocks = new LineBlockCollection(mDbInterfaceLineBS);
			blocks.GetSingleElementFromDb(blockCode);
			blocks.GetObject(out blk);

			if (blk != null)
			{
				MaterialCollection materials = new MaterialCollection(mDbInterface);
				materials.GetSingleElementFromDb(blk.gMaterialCode);
				materials.GetObject(out mat);
			}
		}

		#endregion

		#region Control Events

		private void btnAddCode_Click(object sender, EventArgs e)
		{
			panelNewSlab.Visible = true;
			panelUpDown.Visible = false;
			txtSlabCode.Text = "";
			txtSlabCode.Focus();
			this.AcceptButton = btnConfirmSlab;
		}

		private void btnEditSequence_Click(object sender, EventArgs e)
		{
			panelNewSlab.Visible = false;
			panelUpDown.Visible = true;
			txtPriority.Text = "";
			txtPriority.Focus();
			this.AcceptButton = null;
		}

		private void btnDeleteSlab_Click(object sender, EventArgs e)
		{
			Delete();
		}
		
		private void btnConfirmSlab_Click(object sender, EventArgs e)
		{
			LineSlab slb;
			string message,caption;

			// Verifica che il codice non sia già presente in questa lista 
			mLineSlabs.MoveFirst();
			while (!mLineSlabs.IsEOF())
			{
				mLineSlabs.GetObject(out slb);
				if (slb != null && slb.gCode.Trim() == txtSlabCode.Text)
				{
					ProjResource.gResource.LoadMessageBox(this, 2, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				mLineSlabs.MoveNext();
			}

			// Verifica che non sia già stato raggiunto il limite delle lastre totali
			if (mBlockMode && mBlock != null && mBlock.gTotalSlabs == mLineSlabs.Count)
			{
				ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
				MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			mLineSlabs.MoveLast();
			mLineSlabs.GetObject(out slb);

			if (txtSlabCode.Text.Length > 0)
			{
				int maxpriority;
				if (slb == null)
					maxpriority = 0;
				else
					maxpriority = slb.gPriority;

				// Aggiungo l'elemento alla lista e confermo
				mLineSlabs.AddObject(new LineSlab(-1, txtSlabCode.Text.Trim(), maxpriority + 1, mBlock.gCode, LineSlab.SlabStates.SLAB_INSERTED.ToString(), "", "", 0, 0, 0, 0, ""));
				if (mLineSlabs.UpdateDataToDb())
				{
					FillTable();
					dataGridLineSlabs.Bookmark = dataGridLineSlabs.RowCount - 1;

					TraceLog.WriteLine("Lastra: " + txtSlabCode.Text.Trim() + " aggiunta manualmente al blocco: " + mBlock.gCode.Trim());
				}
			}
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}
		
		private void dataGridLineSlabs_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
		{
			LineSlab slb;

			int i = (int)dataGridLineSlabs[e.Row, "T_ID"];
			mLineSlabs.GetObjectTable(i, out slb);

			e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
			string state = dataGridLineSlabs[e.Row, "F_STATE"].ToString();

			if (state.Trim() == LineSlab.SlabStates.SLAB_INSERTED.ToString())
			{
				e.CellStyle.BackColor = Color.LightGreen;
				e.CellStyle.BackColor2 = Color.LimeGreen;
				e.CellStyle.ForeColor = Color.Black;
			}
			else if (state.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
			{
				e.CellStyle.BackColor = Color.DimGray;
				e.CellStyle.BackColor2 = Color.Gray;
				e.CellStyle.ForeColor = Color.White;
			}
		}

		private void btnUp_Click(object sender, EventArgs e)
		{
			string message, caption;
			LineSlab slb;
			int bookmark = dataGridLineSlabs.Bookmark;
			
			if (bookmark > 0)
			{
				mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()), out slb);
				// se si sta cercando di modificare la priorità di una lastra già processata blocca l'esecuzione
				if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
				{
					ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}

				TraceLog.WriteLine("UP");
				TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " Priorità: " + slb.gPriority.ToString());
				slb.gPriority--;
				mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()) - 1, out slb);
				// se la lastra che dovrebbe essere sostituita è già processata termina l'esecuzione
				if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
				{
					ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					if (mLineSlabs.RefreshData())
					{
						FillTable();
						dataGridLineSlabs.Bookmark = bookmark;
					}
					return;
				}
				slb.gPriority++;
				TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " Priorità: " + slb.gPriority.ToString());

				// Non serve ordinare perchè dopo l'update rilegge i dati ordinati per priority
				//mLineSlabs.SortByField(LineSlab.Keys.F_PRIORITY.ToString());

				if (mLineSlabs.UpdateDataToDb())
				{
					FillTable();
					dataGridLineSlabs.Bookmark = bookmark - 1;
				}
			}
		}

		private void btnDown_Click(object sender, EventArgs e)
		{
			string message, caption;
			LineSlab slb;
			int bookmark = dataGridLineSlabs.Bookmark;

			if (bookmark < dataGridLineSlabs.RowCount - 1)
			{
				mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()), out slb);
				// se si sta cercando di modificare la priorità di una lastra già processata blocca l'esecuzione
				if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
				{
					ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
				TraceLog.WriteLine("DOWN");
				TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " Priorità: " + slb.gPriority.ToString());
				slb.gPriority++;
				mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()) + 1, out slb);

				// Qui non viene effettuato il controllo sulla lastra successiva in quanto essendo successiva non può essere già processata

				slb.gPriority--;
				TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " Priorità: " + slb.gPriority.ToString());
				// Non serve ordinare perchè dopo l'update rilegge i dati ordinati per priority
				//mLineSlabs.SortByField(LineSlab.Keys.F_PRIORITY.ToString());

				if (mLineSlabs.UpdateDataToDb())
				{
					FillTable();
					dataGridLineSlabs.Bookmark = bookmark + 1;
				}
			}
		}

		private void txtPriority_Validated(object sender, EventArgs e)
		{
			string message, caption;
			LineSlab slb;
			int bookmark = dataGridLineSlabs.Bookmark;
			int val, maxPriority = -1;

			mLineSlabs.MoveLast();
			mLineSlabs.GetObject(out slb);
			if (slb != null)
				maxPriority = slb.gPriority;

			if (bookmark >= 0 &&bookmark < dataGridLineSlabs.RowCount)
			{
				mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()), out slb);
				// se si sta cercando di modificare la priorità di una lastra già processata blocca l'esecuzione
				if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
				{
					ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}

				TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " spostata dalla posizione: " + slb.gPriority.ToString());
				if (int.TryParse(txtPriority.Text, out val) && val > 0 && val <= maxPriority)
				{
					int i = int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString());
					
					// aggiorna tutte le priorità consecutive
					if (slb.gPriority < val)
					{						
						while (true)
						{
							i++;
							if (i >= mLineSlabs.Count)
								break;

							mLineSlabs.GetObjectTable(i, out slb);

							// Qui non viene effettuato il controllo sulla lastra successiva in quanto essendo successiva non può essere già processata

							if (slb.gPriority <= val)
								slb.gPriority--;
							else
								break;
						}
					}
					else
					{
						while (true)
						{
							i--;
							if (i < 0)
								break;
							mLineSlabs.GetObjectTable(i, out slb);

							if (slb.gPriority >= val)
								slb.gPriority++;
							else
								break;

							// se la lastra che dovrebbe essere sostituita è già processata termina l'esecuzione
							if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
							{
								ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
								MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
								if (mLineSlabs.RefreshData())
								{
									FillTable();
									dataGridLineSlabs.Bookmark = bookmark;
								}
								return;
							}
						}
					}
					// Legge la lastra da modificare
					mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()), out slb);
					// assegna la priorità richiesta alla lastra in questione
					slb.gPriority = val;
					TraceLog.WriteLine("alla posizione: " + val.ToString());
				}
				else
					return;

				if (mLineSlabs.UpdateDataToDb())
				{
					FillTable();
					dataGridLineSlabs.Bookmark = val - 1;
				}
			}
		}

		private void txtPriority_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtPriority_Validated(sender, null);
		}
		
		private void btnRefresh_Click(object sender, EventArgs e)
		{
			if (mLineSlabs.RefreshData())
				FillTable();
		}
		
		private void dataGridLineSlabs_RowColChange(object sender, C1.Win.C1TrueDBGrid.RowColChangeEventArgs e)
		{
			LineSlab slb;

			int bookmark = dataGridLineSlabs.Bookmark;

			if (bookmark < 0)
				return;

			mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()), out slb);

			if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
			{
				btnChangeStatus.Image = global::LineBlocksSlabs.Properties.Resources.green_ball32;
				btnChangeStatus.Text = ProjResource.gResource.LoadFixString(this, 1);
			}
			else if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_INSERTED.ToString())
			{
				btnChangeStatus.Image = global::LineBlocksSlabs.Properties.Resources.black_ball32;
				btnChangeStatus.Text = ProjResource.gResource.LoadFixString(this, 0);
			}
		}

		private void btnChangeStatus_Click(object sender, EventArgs e)
		{
			LineSlab slb;

			int bookmark = dataGridLineSlabs.Bookmark;
			
			if (bookmark < 0)
				return;

			mLineSlabs.GetObjectTable(int.Parse(dataGridLineSlabs[dataGridLineSlabs.Bookmark, 0].ToString()), out slb);

			if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
			{
				slb.gLineSlabState = LineSlab.SlabStates.SLAB_INSERTED.ToString();
				TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " manually set INSERTED");
			}
			else if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_INSERTED.ToString())
			{
				slb.gLineSlabState = LineSlab.SlabStates.SLAB_PROCESSED.ToString();
				TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " manually set PROCESSED");
			}

			if (mLineSlabs.UpdateDataToDb("Modifica stato lastra manuale"))
			{
				FillTable();
				dataGridLineSlabs.Bookmark = bookmark;
				if (mBlockMode && mBlock != null)
					UpdateBlockQuantities(mBlock.gCode.Trim());

				OnUpdate(slb.gLineBlockCode.Trim());
			}
		}

		private void btnSetAllProcessed_Click(object sender, EventArgs e)
		{
			LineSlab slb;

			mLineSlabs.MoveFirst();
			while (!mLineSlabs.IsEOF())
			{
				mLineSlabs.GetObject(out slb);
				if (slb != null && slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_INSERTED.ToString())
				{
					slb.gLineSlabState = LineSlab.SlabStates.SLAB_PROCESSED.ToString();
					TraceLog.WriteLine("Lastra: " + slb.gCode.Trim() + " manually set PROCESSED");
				}
				mLineSlabs.MoveNext();
			}

			if (mLineSlabs.UpdateDataToDb("Resettate tutte le lastre in manuale"))
			{
				FillTable();
				
				if (mBlockMode && mBlock != null)
					UpdateBlockQuantities(mBlock.gCode.Trim());

				OnUpdate(null);
			}

		}

		private void chbxShowProcessed_CheckedChanged(object sender, EventArgs e)
		{
			if (mBlockMode && mBlock != null)
			{
				if (chbxShowProcessed.Checked)
				{
					//Legge la lista delle lastre associate al blocco
					if (mLineSlabs.GetDataFromDb(LineSlab.Keys.F_PRIORITY, mBlock.gCode))
						FillTable();	//Riempe la Table
				}
				else
				{
					//Legge la lista delle lastre associate al blocco
					if (mLineSlabs.GetDataFromDb(LineSlab.Keys.F_PRIORITY, -1, null, LineSlab.SlabStates.SLAB_INSERTED.ToString(), mBlock.gCode, null, null, null, 0, 0))
						FillTable();	//Riempe la Table
				}
			}
			else
			{
				if (chbxShowProcessed.Checked)
				{
					//Legge la lista delle lastre associate al blocco
					if (mLineSlabs.GetDataFromDb(LineSlab.Keys.F_PRIORITY))
						FillTable();	//Riempe la Table
				}
				else
				{
					//Legge la lista delle lastre associate al blocco
					if (mLineSlabs.GetDataFromDb(LineSlab.Keys.F_PRIORITY, -1, null, LineSlab.SlabStates.SLAB_INSERTED.ToString(), null, null, null, null, 0, 0))
						FillTable();	//Riempe la Table

				}
			}
		}

		/// <summary>
		/// Evento di update delle lastre dei blocchi
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void UpdateBlockQuantities(string blockCode)
		{
			LineBlock blk;
			LineBlockCollection blocks = new LineBlockCollection(mDbInterfaceLineBS);
			blocks.GetSingleElementFromDb(blockCode);
			blocks.GetObject(out blk);

			// Verifica quante sono le lastre processate e quante sono quelle da processare
			// Questo controllo è valido solo se le lastre associate al blocco sono in numero uguale alla quantità impostata nel blocco.
			int procSlab = 0, insertSlab = 0;
			LineSlab slb;
			LineSlabCollection slabs = new LineSlabCollection(mDbInterfaceLineBS);
			slabs.GetDataFromDb(LineSlab.Keys.F_PRIORITY, blockCode);

			// Commentato questo controllo in quanto potevano esserci lastre con quantità sbagliate
			//if (blk.gTotalSlabs != slabs.Count)
			//    return;

			// Scorre tutta la collection per fare il conteggio
			slabs.MoveFirst();
			while (!slabs.IsEOF())
			{
				slabs.GetObject(out slb);
				// Se lo stato è processed incrementa la variabile che conta le lastre processate
				if (slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_PROCESSED.ToString())
					procSlab++;
				else if (slb.gLineSlabState.Trim() == LineSlab.SlabStates.SLAB_INSERTED.ToString())
					insertSlab++;

				slabs.MoveNext();
			}

			blk.gProcSlabs = procSlab;
			blk.gHangSlabs = blk.gTotalSlabs - blk.gProcSlabs;
			if (blk.gHangSlabs == 0)
				blk.gBlockState = LineBlock.BloccoStates.BLKST_PROCESSED;
			else if (blk.gHangSlabs > 0 && blk.gBlockState == LineBlock.BloccoStates.BLKST_PROCESSED)
				blk.gBlockState = LineBlock.BloccoStates.BLKST_SUSPENDED;

			TraceLog.WriteLine("Blocco: " + blk.gCode.Trim());
			TraceLog.WriteLine("Stato del blocco: " + blk.gBlockState.ToString());
			TraceLog.WriteLine("Lastre processate: " + blk.gProcSlabs.ToString());
			TraceLog.WriteLine("Lastre mancanti: " + blk.gHangSlabs.ToString());			

			blocks.UpdateDataToDb();
		}
		#endregion	

		#region Gestione Eventi

		protected virtual void OnUpdate(string blockCode)
		{
			if (UpdateSlabsBlockEvent != null)
				UpdateSlabsBlockEvent(blockCode);
		}
		#endregion
	}
}
