﻿namespace Breton.LineBlocksSlabs.Form
{
	partial class LineBlocks
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LineBlocks));
			this.panelDati = new System.Windows.Forms.Panel();
			this.dataGridLineBlocks = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
			this.dataTableLineBlocks = new System.Data.DataTable();
			this.dataColumnT_ID = new System.Data.DataColumn();
			this.dataColumnF_ID = new System.Data.DataColumn();
			this.dataColumnF_CODE = new System.Data.DataColumn();
			this.dataColumnF_DESCRIPTION = new System.Data.DataColumn();
			this.dataColumnF_MATERIAL_CODE = new System.Data.DataColumn();
			this.dataColumnF_SUPPLIER_CODE = new System.Data.DataColumn();
			this.dataColumnF_STOCK_CODE = new System.Data.DataColumn();
			this.dataColumnF_STOCK_LEVEL = new System.Data.DataColumn();
			this.dataColumnF_QUALITY_CODE = new System.Data.DataColumn();
			this.dataColumnF_SLAB_THICKNESS = new System.Data.DataColumn();
			this.dataColumnF_TOTAL_SLABS = new System.Data.DataColumn();
			this.dataColumnF_PROC_SLABS = new System.Data.DataColumn();
			this.dataColumnF_HANG_SLABS = new System.Data.DataColumn();
			this.dataColumnF_DLETED_SLABS = new System.Data.DataColumn();
			this.dataColumnF_LENGTH = new System.Data.DataColumn();
			this.dataColumnF_WIDTH = new System.Data.DataColumn();
			this.dataColumnF_THICKNESS = new System.Data.DataColumn();
			this.dataColumnF_SURFACE = new System.Data.DataColumn();
			this.dataColumnF_DATE_PROD = new System.Data.DataColumn();
			this.dataColumnF_CODE_OP = new System.Data.DataColumn();
			this.dataColumnF_BL_SURFACE = new System.Data.DataColumn();
			this.dataColumnF_BL_ORIGIN = new System.Data.DataColumn();
			this.dataColumnF_UPD_DATE = new System.Data.DataColumn();
			this.dataColumn1 = new System.Data.DataColumn();
			this.panel2 = new System.Windows.Forms.Panel();
			this.btnProcessed = new System.Windows.Forms.Button();
			this.btnSusp = new System.Windows.Forms.Button();
			this.btnDW = new System.Windows.Forms.Button();
			this.btnActiv = new System.Windows.Forms.Button();
			this.btnUP = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.grpBlockStates = new System.Windows.Forms.GroupBox();
			this.check_ABORTED = new System.Windows.Forms.CheckBox();
			this.check_PROCESSED = new System.Windows.Forms.CheckBox();
			this.check_SUSPENDED = new System.Windows.Forms.CheckBox();
			this.check_ACTIVATED = new System.Windows.Forms.CheckBox();
			this.check_IN_PROGRESS = new System.Windows.Forms.CheckBox();
			this.check_DEFINED = new System.Windows.Forms.CheckBox();
			this.check_UNDEF = new System.Windows.Forms.CheckBox();
			this.lblBlockList = new System.Windows.Forms.Label();
			this.dataSetLineBlocks = new System.Data.DataSet();
			this.panelEditNew = new System.Windows.Forms.Panel();
			this.lblMillimetriMU = new System.Windows.Forms.Label();
			this.lblSuperficieMU = new System.Windows.Forms.Label();
			this.btnGetQuality = new System.Windows.Forms.Button();
			this.btnGetMaterial = new System.Windows.Forms.Button();
			this.btnGetSupplier = new System.Windows.Forms.Button();
			this.txtStockPosition = new System.Windows.Forms.TextBox();
			this.btnGetNode = new System.Windows.Forms.Button();
			this.cmbMaterialCode = new System.Windows.Forms.ComboBox();
			this.cmbSupplierCode = new System.Windows.Forms.ComboBox();
			this.cmbQuality = new System.Windows.Forms.ComboBox();
			this.dtpUpdDate = new System.Windows.Forms.DateTimePicker();
			this.dtpDateProd = new System.Windows.Forms.DateTimePicker();
			this.lblBlockState = new System.Windows.Forms.Label();
			this.cboBlockState = new System.Windows.Forms.ComboBox();
			this.lblUpdDate = new System.Windows.Forms.Label();
			this.txtBlOrigin = new System.Windows.Forms.TextBox();
			this.lblBlOrigin = new System.Windows.Forms.Label();
			this.txtBlSurface = new System.Windows.Forms.TextBox();
			this.lblBlSurface = new System.Windows.Forms.Label();
			this.lblDateProd = new System.Windows.Forms.Label();
			this.txtSurface = new System.Windows.Forms.TextBox();
			this.lblSurface = new System.Windows.Forms.Label();
			this.txtThickness = new System.Windows.Forms.TextBox();
			this.lblThickness = new System.Windows.Forms.Label();
			this.txtWidth = new System.Windows.Forms.TextBox();
			this.lblWidth = new System.Windows.Forms.Label();
			this.txtLength = new System.Windows.Forms.TextBox();
			this.lblLength = new System.Windows.Forms.Label();
			this.txtCodeOp = new System.Windows.Forms.TextBox();
			this.lblCodeOp = new System.Windows.Forms.Label();
			this.txtDeletedSlabs = new System.Windows.Forms.TextBox();
			this.lblDeletedSlabs = new System.Windows.Forms.Label();
			this.txtHangSlabs = new System.Windows.Forms.TextBox();
			this.lblHangSlabs = new System.Windows.Forms.Label();
			this.txtProcSlabs = new System.Windows.Forms.TextBox();
			this.lblProcSlabs = new System.Windows.Forms.Label();
			this.txtTotalSlabs = new System.Windows.Forms.TextBox();
			this.lblTotalSlabs = new System.Windows.Forms.Label();
			this.lblQuality = new System.Windows.Forms.Label();
			this.lblStockCode = new System.Windows.Forms.Label();
			this.lblSupplierCode = new System.Windows.Forms.Label();
			this.lblMaterialCode = new System.Windows.Forms.Label();
			this.lblDesc = new System.Windows.Forms.Label();
			this.txtDesc = new System.Windows.Forms.TextBox();
			this.txtBlockCode = new System.Windows.Forms.TextBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.btnReset = new System.Windows.Forms.Button();
			this.btnConfirm = new System.Windows.Forms.Button();
			this.lblObbl = new System.Windows.Forms.Label();
			this.lblOperation = new System.Windows.Forms.Label();
			this.lblBlockCode = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSlabsList = new System.Windows.Forms.Button();
			this.btnNew = new System.Windows.Forms.Button();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnSave = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.panelDati.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridLineBlocks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableLineBlocks)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.grpBlockStates.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataSetLineBlocks)).BeginInit();
			this.panelEditNew.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelDati
			// 
			this.panelDati.BackColor = System.Drawing.Color.LightSkyBlue;
			this.panelDati.Controls.Add(this.dataGridLineBlocks);
			this.panelDati.Controls.Add(this.panel2);
			this.panelDati.Controls.Add(this.panel3);
			this.panelDati.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelDati.Location = new System.Drawing.Point(20, 389);
			this.panelDati.Margin = new System.Windows.Forms.Padding(4);
			this.panelDati.Name = "panelDati";
			this.panelDati.Size = new System.Drawing.Size(859, 551);
			this.panelDati.TabIndex = 40;
			// 
			// dataGridLineBlocks
			// 
			this.dataGridLineBlocks.AllowColMove = false;
			this.dataGridLineBlocks.AllowRowSizing = C1.Win.C1TrueDBGrid.RowSizingEnum.IndividualRows;
			this.dataGridLineBlocks.AllowUpdate = false;
			this.dataGridLineBlocks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dataGridLineBlocks.CaptionHeight = 21;
			this.dataGridLineBlocks.DataSource = this.dataTableLineBlocks;
			this.dataGridLineBlocks.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridLineBlocks.ExtendRightColumn = true;
			this.dataGridLineBlocks.FetchRowStyles = true;
			this.dataGridLineBlocks.Font = new System.Drawing.Font("Tahoma", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dataGridLineBlocks.GroupByCaption = "Drag a column header here to group by that column";
			this.dataGridLineBlocks.Images.Add(((System.Drawing.Image)(resources.GetObject("dataGridLineBlocks.Images"))));
			this.dataGridLineBlocks.LinesPerRow = 1;
			this.dataGridLineBlocks.Location = new System.Drawing.Point(0, 111);
			this.dataGridLineBlocks.Name = "dataGridLineBlocks";
			this.dataGridLineBlocks.PreviewInfo.Location = new System.Drawing.Point(0, 0);
			this.dataGridLineBlocks.PreviewInfo.Size = new System.Drawing.Size(0, 0);
			this.dataGridLineBlocks.PreviewInfo.ZoomFactor = 75;
			this.dataGridLineBlocks.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("dataGridLineBlocks.PrintInfo.PageSettings")));
			this.dataGridLineBlocks.RecordSelectorWidth = 30;
			this.dataGridLineBlocks.RowHeight = 45;
			this.dataGridLineBlocks.Size = new System.Drawing.Size(755, 440);
			this.dataGridLineBlocks.TabIndex = 87;
			this.dataGridLineBlocks.VisualStyle = C1.Win.C1TrueDBGrid.VisualStyle.Office2007Black;
			this.dataGridLineBlocks.FetchRowStyle += new C1.Win.C1TrueDBGrid.FetchRowStyleEventHandler(this.dataGridLineBlocks_FetchRowStyle);
			this.dataGridLineBlocks.PropBag = resources.GetString("dataGridLineBlocks.PropBag");
			// 
			// dataTableLineBlocks
			// 
			this.dataTableLineBlocks.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnT_ID,
            this.dataColumnF_ID,
            this.dataColumnF_CODE,
            this.dataColumnF_DESCRIPTION,
            this.dataColumnF_MATERIAL_CODE,
            this.dataColumnF_SUPPLIER_CODE,
            this.dataColumnF_STOCK_CODE,
            this.dataColumnF_STOCK_LEVEL,
            this.dataColumnF_QUALITY_CODE,
            this.dataColumnF_SLAB_THICKNESS,
            this.dataColumnF_TOTAL_SLABS,
            this.dataColumnF_PROC_SLABS,
            this.dataColumnF_HANG_SLABS,
            this.dataColumnF_DLETED_SLABS,
            this.dataColumnF_LENGTH,
            this.dataColumnF_WIDTH,
            this.dataColumnF_THICKNESS,
            this.dataColumnF_SURFACE,
            this.dataColumnF_DATE_PROD,
            this.dataColumnF_CODE_OP,
            this.dataColumnF_BL_SURFACE,
            this.dataColumnF_BL_ORIGIN,
            this.dataColumnF_UPD_DATE,
            this.dataColumn1});
			this.dataTableLineBlocks.TableName = "tableLineBlocks";
			// 
			// dataColumnT_ID
			// 
			this.dataColumnT_ID.ColumnName = "T_ID";
			this.dataColumnT_ID.DataType = typeof(int);
			// 
			// dataColumnF_ID
			// 
			this.dataColumnF_ID.ColumnName = "F_ID";
			this.dataColumnF_ID.DataType = typeof(int);
			// 
			// dataColumnF_CODE
			// 
			this.dataColumnF_CODE.ColumnName = "F_CODE";
			// 
			// dataColumnF_DESCRIPTION
			// 
			this.dataColumnF_DESCRIPTION.ColumnName = "F_DESCRIPTION";
			// 
			// dataColumnF_MATERIAL_CODE
			// 
			this.dataColumnF_MATERIAL_CODE.ColumnName = "F_MATERIAL_CODE";
			// 
			// dataColumnF_SUPPLIER_CODE
			// 
			this.dataColumnF_SUPPLIER_CODE.ColumnName = "F_SUPPLIER_CODE";
			// 
			// dataColumnF_STOCK_CODE
			// 
			this.dataColumnF_STOCK_CODE.ColumnName = "F_STOCK_CODE";
			// 
			// dataColumnF_STOCK_LEVEL
			// 
			this.dataColumnF_STOCK_LEVEL.ColumnName = "F_STOCK_LEVEL";
			this.dataColumnF_STOCK_LEVEL.DataType = typeof(int);
			// 
			// dataColumnF_QUALITY_CODE
			// 
			this.dataColumnF_QUALITY_CODE.ColumnName = "F_QUALITY_CODE";
			// 
			// dataColumnF_SLAB_THICKNESS
			// 
			this.dataColumnF_SLAB_THICKNESS.ColumnName = "F_SLAB_THICKNESS";
			this.dataColumnF_SLAB_THICKNESS.DataType = typeof(double);
			// 
			// dataColumnF_TOTAL_SLABS
			// 
			this.dataColumnF_TOTAL_SLABS.ColumnName = "F_TOTAL_SLABS";
			this.dataColumnF_TOTAL_SLABS.DataType = typeof(int);
			// 
			// dataColumnF_PROC_SLABS
			// 
			this.dataColumnF_PROC_SLABS.ColumnName = "F_PROC_SLABS";
			this.dataColumnF_PROC_SLABS.DataType = typeof(int);
			// 
			// dataColumnF_HANG_SLABS
			// 
			this.dataColumnF_HANG_SLABS.ColumnName = "F_HANG_SLABS";
			this.dataColumnF_HANG_SLABS.DataType = typeof(int);
			// 
			// dataColumnF_DLETED_SLABS
			// 
			this.dataColumnF_DLETED_SLABS.ColumnName = "F_DLETED_SLABS";
			this.dataColumnF_DLETED_SLABS.DataType = typeof(int);
			// 
			// dataColumnF_LENGTH
			// 
			this.dataColumnF_LENGTH.ColumnName = "F_LENGTH";
			this.dataColumnF_LENGTH.DataType = typeof(double);
			// 
			// dataColumnF_WIDTH
			// 
			this.dataColumnF_WIDTH.ColumnName = "F_WIDTH";
			this.dataColumnF_WIDTH.DataType = typeof(double);
			// 
			// dataColumnF_THICKNESS
			// 
			this.dataColumnF_THICKNESS.ColumnName = "F_THICKNESS";
			this.dataColumnF_THICKNESS.DataType = typeof(double);
			// 
			// dataColumnF_SURFACE
			// 
			this.dataColumnF_SURFACE.ColumnName = "F_SURFACE";
			this.dataColumnF_SURFACE.DataType = typeof(double);
			// 
			// dataColumnF_DATE_PROD
			// 
			this.dataColumnF_DATE_PROD.ColumnName = "F_DATE_PROD";
			// 
			// dataColumnF_CODE_OP
			// 
			this.dataColumnF_CODE_OP.ColumnName = "F_CODE_OP";
			// 
			// dataColumnF_BL_SURFACE
			// 
			this.dataColumnF_BL_SURFACE.ColumnName = "F_BL_SURFACE";
			// 
			// dataColumnF_BL_ORIGIN
			// 
			this.dataColumnF_BL_ORIGIN.ColumnName = "F_BL_ORIGIN";
			// 
			// dataColumnF_UPD_DATE
			// 
			this.dataColumnF_UPD_DATE.ColumnName = "F_UPD_DATE";
			this.dataColumnF_UPD_DATE.DataType = typeof(System.DateTime);
			// 
			// dataColumn1
			// 
			this.dataColumn1.ColumnName = "F_BLOCK_STATES";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.LightSkyBlue;
			this.panel2.Controls.Add(this.btnProcessed);
			this.panel2.Controls.Add(this.btnSusp);
			this.panel2.Controls.Add(this.btnDW);
			this.panel2.Controls.Add(this.btnActiv);
			this.panel2.Controls.Add(this.btnUP);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel2.Location = new System.Drawing.Point(755, 111);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(104, 440);
			this.panel2.TabIndex = 85;
			// 
			// btnProcessed
			// 
			this.btnProcessed.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnProcessed.Image = global::LineBlocksSlabs.Properties.Resources.processed;
			this.btnProcessed.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnProcessed.Location = new System.Drawing.Point(12, 367);
			this.btnProcessed.Name = "btnProcessed";
			this.btnProcessed.Size = new System.Drawing.Size(80, 70);
			this.btnProcessed.TabIndex = 85;
			this.btnProcessed.Text = "Finito";
			this.btnProcessed.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnProcessed.UseVisualStyleBackColor = true;
			this.btnProcessed.Click += new System.EventHandler(this.btnProcessed_Click);
			// 
			// btnSusp
			// 
			this.btnSusp.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSusp.Image = global::LineBlocksSlabs.Properties.Resources.SemaforoStop;
			this.btnSusp.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnSusp.Location = new System.Drawing.Point(12, 271);
			this.btnSusp.Name = "btnSusp";
			this.btnSusp.Size = new System.Drawing.Size(80, 90);
			this.btnSusp.TabIndex = 82;
			this.btnSusp.Text = "Disattiva";
			this.btnSusp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnSusp.UseVisualStyleBackColor = true;
			this.btnSusp.Click += new System.EventHandler(this.btnSusp_Click);
			// 
			// btnDW
			// 
			this.btnDW.Image = ((System.Drawing.Image)(resources.GetObject("btnDW.Image")));
			this.btnDW.Location = new System.Drawing.Point(12, 89);
			this.btnDW.Name = "btnDW";
			this.btnDW.Size = new System.Drawing.Size(80, 70);
			this.btnDW.TabIndex = 84;
			this.btnDW.UseVisualStyleBackColor = true;
			this.btnDW.Click += new System.EventHandler(this.btnDW_Click);
			// 
			// btnActiv
			// 
			this.btnActiv.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnActiv.Image = global::LineBlocksSlabs.Properties.Resources.SemaforoStart;
			this.btnActiv.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnActiv.Location = new System.Drawing.Point(12, 181);
			this.btnActiv.Name = "btnActiv";
			this.btnActiv.Size = new System.Drawing.Size(80, 90);
			this.btnActiv.TabIndex = 81;
			this.btnActiv.Text = "Attiva";
			this.btnActiv.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnActiv.UseVisualStyleBackColor = true;
			this.btnActiv.Click += new System.EventHandler(this.btnActiv_Click);
			// 
			// btnUP
			// 
			this.btnUP.Image = ((System.Drawing.Image)(resources.GetObject("btnUP.Image")));
			this.btnUP.Location = new System.Drawing.Point(12, 9);
			this.btnUP.Name = "btnUP";
			this.btnUP.Size = new System.Drawing.Size(80, 70);
			this.btnUP.TabIndex = 83;
			this.btnUP.UseVisualStyleBackColor = true;
			this.btnUP.Click += new System.EventHandler(this.btnUP_Click);
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.LightSkyBlue;
			this.panel3.Controls.Add(this.grpBlockStates);
			this.panel3.Controls.Add(this.lblBlockList);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(859, 111);
			this.panel3.TabIndex = 86;
			// 
			// grpBlockStates
			// 
			this.grpBlockStates.Controls.Add(this.check_ABORTED);
			this.grpBlockStates.Controls.Add(this.check_PROCESSED);
			this.grpBlockStates.Controls.Add(this.check_SUSPENDED);
			this.grpBlockStates.Controls.Add(this.check_ACTIVATED);
			this.grpBlockStates.Controls.Add(this.check_IN_PROGRESS);
			this.grpBlockStates.Controls.Add(this.check_DEFINED);
			this.grpBlockStates.Controls.Add(this.check_UNDEF);
			this.grpBlockStates.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.grpBlockStates.Location = new System.Drawing.Point(233, 0);
			this.grpBlockStates.Name = "grpBlockStates";
			this.grpBlockStates.Size = new System.Drawing.Size(636, 104);
			this.grpBlockStates.TabIndex = 184;
			this.grpBlockStates.TabStop = false;
			this.grpBlockStates.Text = "Visualizzati:";
			// 
			// check_ABORTED
			// 
			this.check_ABORTED.AutoSize = true;
			this.check_ABORTED.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.check_ABORTED.ForeColor = System.Drawing.Color.Red;
			this.check_ABORTED.Location = new System.Drawing.Point(497, 70);
			this.check_ABORTED.Name = "check_ABORTED";
			this.check_ABORTED.Size = new System.Drawing.Size(119, 28);
			this.check_ABORTED.TabIndex = 92;
			this.check_ABORTED.Text = "ABORTED";
			this.check_ABORTED.UseVisualStyleBackColor = true;
			this.check_ABORTED.Visible = false;
			this.check_ABORTED.CheckedChanged += new System.EventHandler(this.checkStates_CheckedChanged);
			// 
			// check_PROCESSED
			// 
			this.check_PROCESSED.AutoSize = true;
			this.check_PROCESSED.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.check_PROCESSED.ForeColor = System.Drawing.Color.DimGray;
			this.check_PROCESSED.Location = new System.Drawing.Point(340, 27);
			this.check_PROCESSED.Name = "check_PROCESSED";
			this.check_PROCESSED.Size = new System.Drawing.Size(139, 28);
			this.check_PROCESSED.TabIndex = 91;
			this.check_PROCESSED.Text = "PROCESSED";
			this.check_PROCESSED.UseVisualStyleBackColor = true;
			this.check_PROCESSED.CheckedChanged += new System.EventHandler(this.checkStates_CheckedChanged);
			// 
			// check_SUSPENDED
			// 
			this.check_SUSPENDED.AutoSize = true;
			this.check_SUSPENDED.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.check_SUSPENDED.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.check_SUSPENDED.Location = new System.Drawing.Point(177, 70);
			this.check_SUSPENDED.Name = "check_SUSPENDED";
			this.check_SUSPENDED.Size = new System.Drawing.Size(141, 28);
			this.check_SUSPENDED.TabIndex = 90;
			this.check_SUSPENDED.Text = "SUSPENDED";
			this.check_SUSPENDED.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.check_SUSPENDED.UseVisualStyleBackColor = true;
			this.check_SUSPENDED.CheckedChanged += new System.EventHandler(this.checkStates_CheckedChanged);
			// 
			// check_ACTIVATED
			// 
			this.check_ACTIVATED.AutoSize = true;
			this.check_ACTIVATED.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.check_ACTIVATED.ForeColor = System.Drawing.Color.Yellow;
			this.check_ACTIVATED.Location = new System.Drawing.Point(17, 70);
			this.check_ACTIVATED.Name = "check_ACTIVATED";
			this.check_ACTIVATED.Size = new System.Drawing.Size(136, 28);
			this.check_ACTIVATED.TabIndex = 89;
			this.check_ACTIVATED.Text = "ACTIVATED";
			this.check_ACTIVATED.UseVisualStyleBackColor = true;
			this.check_ACTIVATED.CheckedChanged += new System.EventHandler(this.checkStates_CheckedChanged);
			// 
			// check_IN_PROGRESS
			// 
			this.check_IN_PROGRESS.AutoSize = true;
			this.check_IN_PROGRESS.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.check_IN_PROGRESS.ForeColor = System.Drawing.Color.Lime;
			this.check_IN_PROGRESS.Location = new System.Drawing.Point(177, 27);
			this.check_IN_PROGRESS.Name = "check_IN_PROGRESS";
			this.check_IN_PROGRESS.Size = new System.Drawing.Size(158, 28);
			this.check_IN_PROGRESS.TabIndex = 89;
			this.check_IN_PROGRESS.Text = "IN_PROGRESS";
			this.check_IN_PROGRESS.UseVisualStyleBackColor = true;
			this.check_IN_PROGRESS.CheckedChanged += new System.EventHandler(this.checkStates_CheckedChanged);
			// 
			// check_DEFINED
			// 
			this.check_DEFINED.AutoSize = true;
			this.check_DEFINED.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.check_DEFINED.ForeColor = System.Drawing.Color.DarkGray;
			this.check_DEFINED.Location = new System.Drawing.Point(17, 27);
			this.check_DEFINED.Name = "check_DEFINED";
			this.check_DEFINED.Size = new System.Drawing.Size(112, 28);
			this.check_DEFINED.TabIndex = 88;
			this.check_DEFINED.Text = "DEFINED";
			this.check_DEFINED.UseVisualStyleBackColor = true;
			this.check_DEFINED.CheckedChanged += new System.EventHandler(this.checkStates_CheckedChanged);
			// 
			// check_UNDEF
			// 
			this.check_UNDEF.AutoSize = true;
			this.check_UNDEF.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.check_UNDEF.ForeColor = System.Drawing.Color.White;
			this.check_UNDEF.Location = new System.Drawing.Point(497, 27);
			this.check_UNDEF.Name = "check_UNDEF";
			this.check_UNDEF.Size = new System.Drawing.Size(93, 28);
			this.check_UNDEF.TabIndex = 87;
			this.check_UNDEF.Text = "UNDEF";
			this.check_UNDEF.UseVisualStyleBackColor = true;
			this.check_UNDEF.Visible = false;
			this.check_UNDEF.CheckedChanged += new System.EventHandler(this.checkStates_CheckedChanged);
			// 
			// lblBlockList
			// 
			this.lblBlockList.AutoSize = true;
			this.lblBlockList.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblBlockList.Location = new System.Drawing.Point(4, 41);
			this.lblBlockList.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblBlockList.Name = "lblBlockList";
			this.lblBlockList.Size = new System.Drawing.Size(214, 34);
			this.lblBlockList.TabIndex = 183;
			this.lblBlockList.Text = "Elenco blocchi";
			// 
			// dataSetLineBlocks
			// 
			this.dataSetLineBlocks.DataSetName = "dataSetLineBlocks";
			this.dataSetLineBlocks.Locale = new System.Globalization.CultureInfo("it-IT");
			this.dataSetLineBlocks.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableLineBlocks});
			// 
			// panelEditNew
			// 
			this.panelEditNew.AutoScroll = true;
			this.panelEditNew.BackColor = System.Drawing.Color.LightSkyBlue;
			this.panelEditNew.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelEditNew.Controls.Add(this.lblMillimetriMU);
			this.panelEditNew.Controls.Add(this.lblSuperficieMU);
			this.panelEditNew.Controls.Add(this.btnGetQuality);
			this.panelEditNew.Controls.Add(this.btnGetMaterial);
			this.panelEditNew.Controls.Add(this.btnGetSupplier);
			this.panelEditNew.Controls.Add(this.txtStockPosition);
			this.panelEditNew.Controls.Add(this.btnGetNode);
			this.panelEditNew.Controls.Add(this.cmbMaterialCode);
			this.panelEditNew.Controls.Add(this.cmbSupplierCode);
			this.panelEditNew.Controls.Add(this.cmbQuality);
			this.panelEditNew.Controls.Add(this.dtpUpdDate);
			this.panelEditNew.Controls.Add(this.dtpDateProd);
			this.panelEditNew.Controls.Add(this.lblBlockState);
			this.panelEditNew.Controls.Add(this.cboBlockState);
			this.panelEditNew.Controls.Add(this.lblUpdDate);
			this.panelEditNew.Controls.Add(this.txtBlOrigin);
			this.panelEditNew.Controls.Add(this.lblBlOrigin);
			this.panelEditNew.Controls.Add(this.txtBlSurface);
			this.panelEditNew.Controls.Add(this.lblBlSurface);
			this.panelEditNew.Controls.Add(this.lblDateProd);
			this.panelEditNew.Controls.Add(this.txtSurface);
			this.panelEditNew.Controls.Add(this.lblSurface);
			this.panelEditNew.Controls.Add(this.txtThickness);
			this.panelEditNew.Controls.Add(this.lblThickness);
			this.panelEditNew.Controls.Add(this.txtWidth);
			this.panelEditNew.Controls.Add(this.lblWidth);
			this.panelEditNew.Controls.Add(this.txtLength);
			this.panelEditNew.Controls.Add(this.lblLength);
			this.panelEditNew.Controls.Add(this.txtCodeOp);
			this.panelEditNew.Controls.Add(this.lblCodeOp);
			this.panelEditNew.Controls.Add(this.txtDeletedSlabs);
			this.panelEditNew.Controls.Add(this.lblDeletedSlabs);
			this.panelEditNew.Controls.Add(this.txtHangSlabs);
			this.panelEditNew.Controls.Add(this.lblHangSlabs);
			this.panelEditNew.Controls.Add(this.txtProcSlabs);
			this.panelEditNew.Controls.Add(this.lblProcSlabs);
			this.panelEditNew.Controls.Add(this.txtTotalSlabs);
			this.panelEditNew.Controls.Add(this.lblTotalSlabs);
			this.panelEditNew.Controls.Add(this.lblQuality);
			this.panelEditNew.Controls.Add(this.lblStockCode);
			this.panelEditNew.Controls.Add(this.lblSupplierCode);
			this.panelEditNew.Controls.Add(this.lblMaterialCode);
			this.panelEditNew.Controls.Add(this.lblDesc);
			this.panelEditNew.Controls.Add(this.txtDesc);
			this.panelEditNew.Controls.Add(this.txtBlockCode);
			this.panelEditNew.Controls.Add(this.lblTitle);
			this.panelEditNew.Controls.Add(this.btnReset);
			this.panelEditNew.Controls.Add(this.btnConfirm);
			this.panelEditNew.Controls.Add(this.lblObbl);
			this.panelEditNew.Controls.Add(this.lblOperation);
			this.panelEditNew.Controls.Add(this.lblBlockCode);
			this.panelEditNew.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelEditNew.Location = new System.Drawing.Point(20, 20);
			this.panelEditNew.Margin = new System.Windows.Forms.Padding(4);
			this.panelEditNew.Name = "panelEditNew";
			this.panelEditNew.Size = new System.Drawing.Size(859, 369);
			this.panelEditNew.TabIndex = 41;
			this.panelEditNew.Visible = false;
			// 
			// lblMillimetriMU
			// 
			this.lblMillimetriMU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMillimetriMU.Location = new System.Drawing.Point(796, 127);
			this.lblMillimetriMU.Name = "lblMillimetriMU";
			this.lblMillimetriMU.Size = new System.Drawing.Size(37, 20);
			this.lblMillimetriMU.TabIndex = 237;
			this.lblMillimetriMU.Text = "mm";
			this.lblMillimetriMU.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSuperficieMU
			// 
			this.lblSuperficieMU.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSuperficieMU.Location = new System.Drawing.Point(796, 162);
			this.lblSuperficieMU.Name = "lblSuperficieMU";
			this.lblSuperficieMU.Size = new System.Drawing.Size(37, 20);
			this.lblSuperficieMU.TabIndex = 236;
			this.lblSuperficieMU.Text = "mq";
			this.lblSuperficieMU.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// btnGetQuality
			// 
			this.btnGetQuality.Image = ((System.Drawing.Image)(resources.GetObject("btnGetQuality.Image")));
			this.btnGetQuality.Location = new System.Drawing.Point(376, 152);
			this.btnGetQuality.Name = "btnGetQuality";
			this.btnGetQuality.Size = new System.Drawing.Size(39, 34);
			this.btnGetQuality.TabIndex = 233;
			this.btnGetQuality.Click += new System.EventHandler(this.btnGetQuality_Click);
			// 
			// btnGetMaterial
			// 
			this.btnGetMaterial.Image = ((System.Drawing.Image)(resources.GetObject("btnGetMaterial.Image")));
			this.btnGetMaterial.Location = new System.Drawing.Point(376, 82);
			this.btnGetMaterial.Name = "btnGetMaterial";
			this.btnGetMaterial.Size = new System.Drawing.Size(39, 34);
			this.btnGetMaterial.TabIndex = 232;
			this.btnGetMaterial.Click += new System.EventHandler(this.btnGetMaterial_Click);
			// 
			// btnGetSupplier
			// 
			this.btnGetSupplier.Image = ((System.Drawing.Image)(resources.GetObject("btnGetSupplier.Image")));
			this.btnGetSupplier.Location = new System.Drawing.Point(376, 117);
			this.btnGetSupplier.Name = "btnGetSupplier";
			this.btnGetSupplier.Size = new System.Drawing.Size(39, 34);
			this.btnGetSupplier.TabIndex = 231;
			this.btnGetSupplier.Click += new System.EventHandler(this.btnGetSupplier_Click);
			// 
			// txtStockPosition
			// 
			this.txtStockPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtStockPosition.Location = new System.Drawing.Point(149, 191);
			this.txtStockPosition.Multiline = true;
			this.txtStockPosition.Name = "txtStockPosition";
			this.txtStockPosition.ReadOnly = true;
			this.txtStockPosition.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtStockPosition.Size = new System.Drawing.Size(470, 32);
			this.txtStockPosition.TabIndex = 230;
			// 
			// btnGetNode
			// 
			this.btnGetNode.Image = ((System.Drawing.Image)(resources.GetObject("btnGetNode.Image")));
			this.btnGetNode.Location = new System.Drawing.Point(625, 191);
			this.btnGetNode.Name = "btnGetNode";
			this.btnGetNode.Size = new System.Drawing.Size(39, 34);
			this.btnGetNode.TabIndex = 229;
			this.btnGetNode.Click += new System.EventHandler(this.btnGetNode_Click);
			// 
			// cmbMaterialCode
			// 
			this.cmbMaterialCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbMaterialCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbMaterialCode.Location = new System.Drawing.Point(149, 81);
			this.cmbMaterialCode.Name = "cmbMaterialCode";
			this.cmbMaterialCode.Size = new System.Drawing.Size(221, 33);
			this.cmbMaterialCode.TabIndex = 227;
			// 
			// cmbSupplierCode
			// 
			this.cmbSupplierCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbSupplierCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbSupplierCode.Location = new System.Drawing.Point(149, 116);
			this.cmbSupplierCode.Name = "cmbSupplierCode";
			this.cmbSupplierCode.Size = new System.Drawing.Size(221, 33);
			this.cmbSupplierCode.TabIndex = 226;
			// 
			// cmbQuality
			// 
			this.cmbQuality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbQuality.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cmbQuality.Location = new System.Drawing.Point(149, 151);
			this.cmbQuality.Name = "cmbQuality";
			this.cmbQuality.Size = new System.Drawing.Size(221, 33);
			this.cmbQuality.TabIndex = 225;
			// 
			// dtpUpdDate
			// 
			this.dtpUpdDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpUpdDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpUpdDate.Location = new System.Drawing.Point(490, 267);
			this.dtpUpdDate.Name = "dtpUpdDate";
			this.dtpUpdDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.dtpUpdDate.Size = new System.Drawing.Size(197, 28);
			this.dtpUpdDate.TabIndex = 224;
			this.dtpUpdDate.ValueChanged += new System.EventHandler(this.dtpUpdDate_ValueChanged);
			// 
			// dtpDateProd
			// 
			this.dtpDateProd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dtpDateProd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this.dtpDateProd.Location = new System.Drawing.Point(490, 299);
			this.dtpDateProd.Name = "dtpDateProd";
			this.dtpDateProd.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.dtpDateProd.Size = new System.Drawing.Size(196, 28);
			this.dtpDateProd.TabIndex = 223;
			this.dtpDateProd.ValueChanged += new System.EventHandler(this.dtpDateProd_ValueChanged);
			// 
			// lblBlockState
			// 
			this.lblBlockState.AutoSize = true;
			this.lblBlockState.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblBlockState.ForeColor = System.Drawing.Color.Black;
			this.lblBlockState.Location = new System.Drawing.Point(328, 237);
			this.lblBlockState.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBlockState.Name = "lblBlockState";
			this.lblBlockState.Size = new System.Drawing.Size(101, 21);
			this.lblBlockState.TabIndex = 221;
			this.lblBlockState.Text = "Stato blocco";
			// 
			// cboBlockState
			// 
			this.cboBlockState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboBlockState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cboBlockState.FormattingEnabled = true;
			this.cboBlockState.Location = new System.Drawing.Point(490, 230);
			this.cboBlockState.Margin = new System.Windows.Forms.Padding(2);
			this.cboBlockState.Name = "cboBlockState";
			this.cboBlockState.Size = new System.Drawing.Size(197, 33);
			this.cboBlockState.TabIndex = 220;
			// 
			// lblUpdDate
			// 
			this.lblUpdDate.AutoSize = true;
			this.lblUpdDate.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblUpdDate.ForeColor = System.Drawing.Color.Black;
			this.lblUpdDate.Location = new System.Drawing.Point(328, 269);
			this.lblUpdDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblUpdDate.Name = "lblUpdDate";
			this.lblUpdDate.Size = new System.Drawing.Size(123, 21);
			this.lblUpdDate.TabIndex = 218;
			this.lblUpdDate.Text = "Aggiornamento";
			// 
			// txtBlOrigin
			// 
			this.txtBlOrigin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtBlOrigin.Location = new System.Drawing.Point(149, 294);
			this.txtBlOrigin.Margin = new System.Windows.Forms.Padding(2);
			this.txtBlOrigin.Name = "txtBlOrigin";
			this.txtBlOrigin.Size = new System.Drawing.Size(170, 30);
			this.txtBlOrigin.TabIndex = 217;
			this.txtBlOrigin.Validated += new System.EventHandler(this.txtBlOrigin_Validated);
			this.txtBlOrigin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBlOrigin_KeyPress);
			this.txtBlOrigin.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblBlOrigin
			// 
			this.lblBlOrigin.AutoSize = true;
			this.lblBlOrigin.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblBlOrigin.ForeColor = System.Drawing.Color.Black;
			this.lblBlOrigin.Location = new System.Drawing.Point(3, 303);
			this.lblBlOrigin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBlOrigin.Name = "lblBlOrigin";
			this.lblBlOrigin.Size = new System.Drawing.Size(115, 21);
			this.lblBlOrigin.TabIndex = 216;
			this.lblBlOrigin.Text = "Origine blocco";
			// 
			// txtBlSurface
			// 
			this.txtBlSurface.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtBlSurface.Location = new System.Drawing.Point(149, 262);
			this.txtBlSurface.Margin = new System.Windows.Forms.Padding(2);
			this.txtBlSurface.Name = "txtBlSurface";
			this.txtBlSurface.Size = new System.Drawing.Size(170, 30);
			this.txtBlSurface.TabIndex = 215;
			this.txtBlSurface.Validated += new System.EventHandler(this.txtBlSurface_Validated);
			this.txtBlSurface.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBlSurface_KeyPress);
			this.txtBlSurface.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblBlSurface
			// 
			this.lblBlSurface.AutoSize = true;
			this.lblBlSurface.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblBlSurface.ForeColor = System.Drawing.Color.Black;
			this.lblBlSurface.Location = new System.Drawing.Point(3, 271);
			this.lblBlSurface.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBlSurface.Name = "lblBlSurface";
			this.lblBlSurface.Size = new System.Drawing.Size(134, 21);
			this.lblBlSurface.TabIndex = 214;
			this.lblBlSurface.Text = "Superficie blocco";
			// 
			// lblDateProd
			// 
			this.lblDateProd.AutoSize = true;
			this.lblDateProd.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDateProd.ForeColor = System.Drawing.Color.Black;
			this.lblDateProd.Location = new System.Drawing.Point(328, 301);
			this.lblDateProd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDateProd.Name = "lblDateProd";
			this.lblDateProd.Size = new System.Drawing.Size(132, 21);
			this.lblDateProd.TabIndex = 212;
			this.lblDateProd.Text = "Data produzione";
			// 
			// txtSurface
			// 
			this.txtSurface.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSurface.Location = new System.Drawing.Point(731, 153);
			this.txtSurface.Margin = new System.Windows.Forms.Padding(2);
			this.txtSurface.Name = "txtSurface";
			this.txtSurface.Size = new System.Drawing.Size(60, 30);
			this.txtSurface.TabIndex = 211;
			this.txtSurface.Validated += new System.EventHandler(this.txtSurface_Validated);
			this.txtSurface.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSurface_KeyPress);
			this.txtSurface.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblSurface
			// 
			this.lblSurface.AutoSize = true;
			this.lblSurface.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSurface.ForeColor = System.Drawing.Color.Black;
			this.lblSurface.Location = new System.Drawing.Point(633, 162);
			this.lblSurface.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSurface.Name = "lblSurface";
			this.lblSurface.Size = new System.Drawing.Size(82, 21);
			this.lblSurface.TabIndex = 210;
			this.lblSurface.Text = "Superficie";
			// 
			// txtThickness
			// 
			this.txtThickness.AcceptsTab = true;
			this.txtThickness.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtThickness.Location = new System.Drawing.Point(731, 118);
			this.txtThickness.Margin = new System.Windows.Forms.Padding(2);
			this.txtThickness.Name = "txtThickness";
			this.txtThickness.Size = new System.Drawing.Size(60, 30);
			this.txtThickness.TabIndex = 209;
			this.txtThickness.Validated += new System.EventHandler(this.txtThickness_Validated);
			this.txtThickness.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtThickness_KeyPress);
			this.txtThickness.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblThickness
			// 
			this.lblThickness.AutoSize = true;
			this.lblThickness.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblThickness.ForeColor = System.Drawing.Color.Black;
			this.lblThickness.Location = new System.Drawing.Point(633, 127);
			this.lblThickness.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblThickness.Name = "lblThickness";
			this.lblThickness.Size = new System.Drawing.Size(77, 21);
			this.lblThickness.TabIndex = 208;
			this.lblThickness.Text = "Spessore";
			// 
			// txtWidth
			// 
			this.txtWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtWidth.Location = new System.Drawing.Point(731, 83);
			this.txtWidth.Margin = new System.Windows.Forms.Padding(2);
			this.txtWidth.Name = "txtWidth";
			this.txtWidth.Size = new System.Drawing.Size(60, 30);
			this.txtWidth.TabIndex = 207;
			this.txtWidth.Validated += new System.EventHandler(this.txtWidth_Validated);
			this.txtWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWidth_KeyPress);
			this.txtWidth.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblWidth
			// 
			this.lblWidth.AutoSize = true;
			this.lblWidth.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblWidth.ForeColor = System.Drawing.Color.Black;
			this.lblWidth.Location = new System.Drawing.Point(633, 92);
			this.lblWidth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblWidth.Name = "lblWidth";
			this.lblWidth.Size = new System.Drawing.Size(85, 21);
			this.lblWidth.TabIndex = 206;
			this.lblWidth.Text = "Larghezza";
			// 
			// txtLength
			// 
			this.txtLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtLength.Location = new System.Drawing.Point(731, 48);
			this.txtLength.Margin = new System.Windows.Forms.Padding(2);
			this.txtLength.Name = "txtLength";
			this.txtLength.Size = new System.Drawing.Size(60, 30);
			this.txtLength.TabIndex = 205;
			this.txtLength.Validated += new System.EventHandler(this.txtLength_Validated);
			this.txtLength.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLength_KeyPress);
			this.txtLength.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblLength
			// 
			this.lblLength.AutoSize = true;
			this.lblLength.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLength.ForeColor = System.Drawing.Color.Black;
			this.lblLength.Location = new System.Drawing.Point(633, 57);
			this.lblLength.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblLength.Name = "lblLength";
			this.lblLength.Size = new System.Drawing.Size(88, 21);
			this.lblLength.TabIndex = 204;
			this.lblLength.Text = "Lunghezza";
			// 
			// txtCodeOp
			// 
			this.txtCodeOp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtCodeOp.Location = new System.Drawing.Point(149, 230);
			this.txtCodeOp.Margin = new System.Windows.Forms.Padding(2);
			this.txtCodeOp.Name = "txtCodeOp";
			this.txtCodeOp.Size = new System.Drawing.Size(170, 30);
			this.txtCodeOp.TabIndex = 203;
			this.txtCodeOp.Validated += new System.EventHandler(this.txtCodeOp_Validated);
			this.txtCodeOp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeOp_KeyPress);
			this.txtCodeOp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblCodeOp
			// 
			this.lblCodeOp.AutoSize = true;
			this.lblCodeOp.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCodeOp.ForeColor = System.Drawing.Color.Black;
			this.lblCodeOp.Location = new System.Drawing.Point(3, 239);
			this.lblCodeOp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblCodeOp.Name = "lblCodeOp";
			this.lblCodeOp.Size = new System.Drawing.Size(136, 21);
			this.lblCodeOp.TabIndex = 202;
			this.lblCodeOp.Text = "Codice operatore";
			// 
			// txtDeletedSlabs
			// 
			this.txtDeletedSlabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDeletedSlabs.Location = new System.Drawing.Point(567, 154);
			this.txtDeletedSlabs.Margin = new System.Windows.Forms.Padding(2);
			this.txtDeletedSlabs.Name = "txtDeletedSlabs";
			this.txtDeletedSlabs.Size = new System.Drawing.Size(52, 30);
			this.txtDeletedSlabs.TabIndex = 201;
			this.txtDeletedSlabs.Visible = false;
			this.txtDeletedSlabs.Validated += new System.EventHandler(this.txtDeletedSlabs_Validated);
			this.txtDeletedSlabs.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDeletedSlabs_KeyPress);
			this.txtDeletedSlabs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblDeletedSlabs
			// 
			this.lblDeletedSlabs.AutoSize = true;
			this.lblDeletedSlabs.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDeletedSlabs.ForeColor = System.Drawing.Color.Black;
			this.lblDeletedSlabs.Location = new System.Drawing.Point(428, 163);
			this.lblDeletedSlabs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblDeletedSlabs.Name = "lblDeletedSlabs";
			this.lblDeletedSlabs.Size = new System.Drawing.Size(136, 21);
			this.lblDeletedSlabs.TabIndex = 200;
			this.lblDeletedSlabs.Text = "Lastre cancellate";
			this.lblDeletedSlabs.Visible = false;
			// 
			// txtHangSlabs
			// 
			this.txtHangSlabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtHangSlabs.Location = new System.Drawing.Point(567, 119);
			this.txtHangSlabs.Margin = new System.Windows.Forms.Padding(2);
			this.txtHangSlabs.Name = "txtHangSlabs";
			this.txtHangSlabs.Size = new System.Drawing.Size(52, 30);
			this.txtHangSlabs.TabIndex = 199;
			this.txtHangSlabs.Validated += new System.EventHandler(this.txtHangSlabs_Validated);
			this.txtHangSlabs.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHangSlabs_KeyPress);
			this.txtHangSlabs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblHangSlabs
			// 
			this.lblHangSlabs.AutoSize = true;
			this.lblHangSlabs.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHangSlabs.ForeColor = System.Drawing.Color.Black;
			this.lblHangSlabs.Location = new System.Drawing.Point(428, 128);
			this.lblHangSlabs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblHangSlabs.Name = "lblHangSlabs";
			this.lblHangSlabs.Size = new System.Drawing.Size(125, 21);
			this.lblHangSlabs.TabIndex = 198;
			this.lblHangSlabs.Text = "Lastre pendenti";
			// 
			// txtProcSlabs
			// 
			this.txtProcSlabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtProcSlabs.Location = new System.Drawing.Point(567, 84);
			this.txtProcSlabs.Margin = new System.Windows.Forms.Padding(2);
			this.txtProcSlabs.Name = "txtProcSlabs";
			this.txtProcSlabs.Size = new System.Drawing.Size(52, 30);
			this.txtProcSlabs.TabIndex = 197;
			this.txtProcSlabs.Validated += new System.EventHandler(this.txtProcSlabs_Validated);
			this.txtProcSlabs.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProcSlabs_KeyPress);
			this.txtProcSlabs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblProcSlabs
			// 
			this.lblProcSlabs.AutoSize = true;
			this.lblProcSlabs.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblProcSlabs.ForeColor = System.Drawing.Color.Black;
			this.lblProcSlabs.Location = new System.Drawing.Point(428, 93);
			this.lblProcSlabs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblProcSlabs.Name = "lblProcSlabs";
			this.lblProcSlabs.Size = new System.Drawing.Size(121, 21);
			this.lblProcSlabs.TabIndex = 196;
			this.lblProcSlabs.Text = "Lastre lavorate";
			// 
			// txtTotalSlabs
			// 
			this.txtTotalSlabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtTotalSlabs.Location = new System.Drawing.Point(567, 49);
			this.txtTotalSlabs.Margin = new System.Windows.Forms.Padding(2);
			this.txtTotalSlabs.Name = "txtTotalSlabs";
			this.txtTotalSlabs.Size = new System.Drawing.Size(52, 30);
			this.txtTotalSlabs.TabIndex = 195;
			this.txtTotalSlabs.Validated += new System.EventHandler(this.txtTotalSlabs_Validated);
			this.txtTotalSlabs.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalSlabs_KeyPress);
			this.txtTotalSlabs.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblTotalSlabs
			// 
			this.lblTotalSlabs.AutoSize = true;
			this.lblTotalSlabs.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTotalSlabs.ForeColor = System.Drawing.Color.Red;
			this.lblTotalSlabs.Location = new System.Drawing.Point(428, 58);
			this.lblTotalSlabs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblTotalSlabs.Name = "lblTotalSlabs";
			this.lblTotalSlabs.Size = new System.Drawing.Size(99, 21);
			this.lblTotalSlabs.TabIndex = 194;
			this.lblTotalSlabs.Text = "Lastre totali";
			// 
			// lblQuality
			// 
			this.lblQuality.AutoSize = true;
			this.lblQuality.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblQuality.ForeColor = System.Drawing.Color.Black;
			this.lblQuality.Location = new System.Drawing.Point(3, 163);
			this.lblQuality.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblQuality.Name = "lblQuality";
			this.lblQuality.Size = new System.Drawing.Size(114, 21);
			this.lblQuality.TabIndex = 192;
			this.lblQuality.Text = "Codice qualità";
			// 
			// lblStockCode
			// 
			this.lblStockCode.AutoSize = true;
			this.lblStockCode.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStockCode.ForeColor = System.Drawing.Color.Red;
			this.lblStockCode.Location = new System.Drawing.Point(2, 197);
			this.lblStockCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblStockCode.Name = "lblStockCode";
			this.lblStockCode.Size = new System.Drawing.Size(143, 21);
			this.lblStockCode.TabIndex = 190;
			this.lblStockCode.Text = "Codice magazzino";
			// 
			// lblSupplierCode
			// 
			this.lblSupplierCode.AutoSize = true;
			this.lblSupplierCode.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSupplierCode.ForeColor = System.Drawing.Color.Black;
			this.lblSupplierCode.Location = new System.Drawing.Point(3, 128);
			this.lblSupplierCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblSupplierCode.Name = "lblSupplierCode";
			this.lblSupplierCode.Size = new System.Drawing.Size(127, 21);
			this.lblSupplierCode.TabIndex = 188;
			this.lblSupplierCode.Text = "Codice fornitore";
			// 
			// lblMaterialCode
			// 
			this.lblMaterialCode.AutoSize = true;
			this.lblMaterialCode.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMaterialCode.ForeColor = System.Drawing.Color.Red;
			this.lblMaterialCode.Location = new System.Drawing.Point(3, 93);
			this.lblMaterialCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblMaterialCode.Name = "lblMaterialCode";
			this.lblMaterialCode.Size = new System.Drawing.Size(134, 21);
			this.lblMaterialCode.TabIndex = 186;
			this.lblMaterialCode.Text = "Codice materiale";
			// 
			// lblDesc
			// 
			this.lblDesc.AutoSize = true;
			this.lblDesc.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDesc.Location = new System.Drawing.Point(3, 332);
			this.lblDesc.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblDesc.Name = "lblDesc";
			this.lblDesc.Size = new System.Drawing.Size(102, 21);
			this.lblDesc.TabIndex = 185;
			this.lblDesc.Text = "Descrizione:";
			// 
			// txtDesc
			// 
			this.txtDesc.AcceptsTab = true;
			this.txtDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtDesc.Location = new System.Drawing.Point(149, 327);
			this.txtDesc.Margin = new System.Windows.Forms.Padding(2);
			this.txtDesc.Name = "txtDesc";
			this.txtDesc.Size = new System.Drawing.Size(537, 26);
			this.txtDesc.TabIndex = 184;
			this.txtDesc.Validated += new System.EventHandler(this.txtDesc_Validated);
			this.txtDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDesc_KeyPress);
			this.txtDesc.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// txtBlockCode
			// 
			this.txtBlockCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtBlockCode.Location = new System.Drawing.Point(149, 49);
			this.txtBlockCode.Margin = new System.Windows.Forms.Padding(2);
			this.txtBlockCode.Name = "txtBlockCode";
			this.txtBlockCode.Size = new System.Drawing.Size(203, 30);
			this.txtBlockCode.TabIndex = 183;
			this.txtBlockCode.Validated += new System.EventHandler(this.txtBlockCode_Validated);
			this.txtBlockCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBlockCode_KeyPress);
			this.txtBlockCode.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Attiva_Pad);
			// 
			// lblTitle
			// 
			this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.lblTitle.AutoSize = true;
			this.lblTitle.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitle.Location = new System.Drawing.Point(2, 7);
			this.lblTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(230, 34);
			this.lblTitle.TabIndex = 182;
			this.lblTitle.Text = "Parametri blocco:";
			// 
			// btnReset
			// 
			this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnReset.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnReset.Image = global::LineBlocksSlabs.Properties.Resources.ResetB;
			this.btnReset.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnReset.Location = new System.Drawing.Point(772, 285);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(80, 70);
			this.btnReset.TabIndex = 180;
			this.btnReset.Text = "Annulla";
			this.btnReset.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnReset.UseVisualStyleBackColor = true;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// btnConfirm
			// 
			this.btnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnConfirm.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnConfirm.Image = global::LineBlocksSlabs.Properties.Resources.ConfermaB;
			this.btnConfirm.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnConfirm.Location = new System.Drawing.Point(691, 285);
			this.btnConfirm.Name = "btnConfirm";
			this.btnConfirm.Size = new System.Drawing.Size(80, 70);
			this.btnConfirm.TabIndex = 181;
			this.btnConfirm.Text = "Conferma";
			this.btnConfirm.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnConfirm.UseVisualStyleBackColor = true;
			this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
			// 
			// lblObbl
			// 
			this.lblObbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblObbl.AutoSize = true;
			this.lblObbl.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblObbl.ForeColor = System.Drawing.Color.Red;
			this.lblObbl.Location = new System.Drawing.Point(710, 7);
			this.lblObbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblObbl.Name = "lblObbl";
			this.lblObbl.Size = new System.Drawing.Size(140, 18);
			this.lblObbl.TabIndex = 179;
			this.lblObbl.Text = "Campi obbligatori";
			// 
			// lblOperation
			// 
			this.lblOperation.AutoSize = true;
			this.lblOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOperation.Location = new System.Drawing.Point(19, 7);
			this.lblOperation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblOperation.Name = "lblOperation";
			this.lblOperation.Size = new System.Drawing.Size(0, 20);
			this.lblOperation.TabIndex = 7;
			// 
			// lblBlockCode
			// 
			this.lblBlockCode.AutoSize = true;
			this.lblBlockCode.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblBlockCode.ForeColor = System.Drawing.Color.Black;
			this.lblBlockCode.Location = new System.Drawing.Point(3, 58);
			this.lblBlockCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.lblBlockCode.Name = "lblBlockCode";
			this.lblBlockCode.Size = new System.Drawing.Size(111, 21);
			this.lblBlockCode.TabIndex = 0;
			this.lblBlockCode.Text = "Codice blocco";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.DeepSkyBlue;
			this.panel1.Controls.Add(this.btnSlabsList);
			this.panel1.Controls.Add(this.btnNew);
			this.panel1.Controls.Add(this.btnEdit);
			this.panel1.Controls.Add(this.btnDelete);
			this.panel1.Controls.Add(this.btnSave);
			this.panel1.Controls.Add(this.btnClose);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(879, 20);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(84, 920);
			this.panel1.TabIndex = 42;
			// 
			// btnSlabsList
			// 
			this.btnSlabsList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSlabsList.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSlabsList.Image = global::LineBlocksSlabs.Properties.Resources.SlabsList;
			this.btnSlabsList.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnSlabsList.Location = new System.Drawing.Point(2, 309);
			this.btnSlabsList.Name = "btnSlabsList";
			this.btnSlabsList.Size = new System.Drawing.Size(80, 70);
			this.btnSlabsList.TabIndex = 81;
			this.btnSlabsList.Text = "Lastre";
			this.btnSlabsList.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnSlabsList.UseVisualStyleBackColor = true;
			this.btnSlabsList.Click += new System.EventHandler(this.btnSlabsList_Click);
			// 
			// btnNew
			// 
			this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNew.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnNew.Image = global::LineBlocksSlabs.Properties.Resources.Nuovo;
			this.btnNew.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnNew.Location = new System.Drawing.Point(2, 4);
			this.btnNew.Name = "btnNew";
			this.btnNew.Size = new System.Drawing.Size(80, 70);
			this.btnNew.TabIndex = 80;
			this.btnNew.Text = "Nuovo";
			this.btnNew.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnNew.UseVisualStyleBackColor = true;
			this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
			// 
			// btnEdit
			// 
			this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEdit.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnEdit.Image = global::LineBlocksSlabs.Properties.Resources.Modifica;
			this.btnEdit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnEdit.Location = new System.Drawing.Point(2, 74);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(80, 70);
			this.btnEdit.TabIndex = 80;
			this.btnEdit.Text = "Modifica";
			this.btnEdit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnEdit.UseVisualStyleBackColor = true;
			this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnDelete.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDelete.Image = global::LineBlocksSlabs.Properties.Resources.Delete;
			this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnDelete.Location = new System.Drawing.Point(2, 144);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(80, 70);
			this.btnDelete.TabIndex = 80;
			this.btnDelete.Text = "Elimina";
			this.btnDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnSave.Image = global::LineBlocksSlabs.Properties.Resources.salva;
			this.btnSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnSave.Location = new System.Drawing.Point(2, 214);
			this.btnSave.Name = "btnSave";
			this.btnSave.Size = new System.Drawing.Size(80, 70);
			this.btnSave.TabIndex = 80;
			this.btnSave.Text = "Salva";
			this.btnSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnSave.UseVisualStyleBackColor = true;
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.Font = new System.Drawing.Font("Tahoma", 7.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnClose.Image = global::LineBlocksSlabs.Properties.Resources.Exit_1;
			this.btnClose.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btnClose.Location = new System.Drawing.Point(2, 847);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(80, 70);
			this.btnClose.TabIndex = 80;
			this.btnClose.Text = "Esci";
			this.btnClose.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// LineBlocks
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackColor = System.Drawing.Color.CornflowerBlue;
			this.ClientSize = new System.Drawing.Size(983, 960);
			this.Controls.Add(this.panelDati);
			this.Controls.Add(this.panelEditNew);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "LineBlocks";
			this.Padding = new System.Windows.Forms.Padding(20);
			this.Text = " ";
			this.Load += new System.EventHandler(this.LineBlocks_Load);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LineBlocks_FormClosed);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LineBlocks_FormClosing);
			this.panelDati.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridLineBlocks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataTableLineBlocks)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.grpBlockStates.ResumeLayout(false);
			this.grpBlockStates.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataSetLineBlocks)).EndInit();
			this.panelEditNew.ResumeLayout(false);
			this.panelEditNew.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panelDati;
		private System.Windows.Forms.Panel panelEditNew;
		private System.Windows.Forms.Label lblOperation;
		private System.Windows.Forms.Label lblBlockCode;
		private System.Data.DataSet dataSetLineBlocks;
		private System.Data.DataTable dataTableLineBlocks;
		private System.Data.DataColumn dataColumnT_ID;
		private System.Data.DataColumn dataColumnF_ID;
		private System.Data.DataColumn dataColumnF_CODE;
		private System.Data.DataColumn dataColumnF_DESCRIPTION;
		private System.Data.DataColumn dataColumnF_MATERIAL_CODE;
		private System.Data.DataColumn dataColumnF_SUPPLIER_CODE;
		private System.Data.DataColumn dataColumnF_STOCK_CODE;
		private System.Data.DataColumn dataColumnF_STOCK_LEVEL;
		private System.Data.DataColumn dataColumnF_QUALITY_CODE;
		private System.Data.DataColumn dataColumnF_SLAB_THICKNESS;
		private System.Data.DataColumn dataColumnF_TOTAL_SLABS;
		private System.Data.DataColumn dataColumnF_PROC_SLABS;
		private System.Data.DataColumn dataColumnF_HANG_SLABS;
		private System.Data.DataColumn dataColumnF_DLETED_SLABS;
		private System.Data.DataColumn dataColumnF_LENGTH;
		private System.Data.DataColumn dataColumnF_WIDTH;
		private System.Data.DataColumn dataColumnF_THICKNESS;
		private System.Data.DataColumn dataColumnF_SURFACE;
		private System.Data.DataColumn dataColumnF_DATE_PROD;
		private System.Data.DataColumn dataColumnF_CODE_OP;
		private System.Data.DataColumn dataColumnF_BL_SURFACE;
		private System.Data.DataColumn dataColumnF_BL_ORIGIN;
		private System.Data.DataColumn dataColumnF_UPD_DATE;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button btnNew;
		private System.Windows.Forms.Button btnEdit;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnReset;
		private System.Windows.Forms.Button btnConfirm;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.TextBox txtBlockCode;
		private System.Windows.Forms.TextBox txtDesc;
		private System.Windows.Forms.Label lblDesc;
		private System.Windows.Forms.Label lblObbl;
		private System.Windows.Forms.TextBox txtCodeOp;
		private System.Windows.Forms.Label lblCodeOp;
		private System.Windows.Forms.TextBox txtDeletedSlabs;
		private System.Windows.Forms.Label lblDeletedSlabs;
		private System.Windows.Forms.TextBox txtHangSlabs;
		private System.Windows.Forms.Label lblHangSlabs;
		private System.Windows.Forms.TextBox txtProcSlabs;
		private System.Windows.Forms.Label lblProcSlabs;
		private System.Windows.Forms.TextBox txtTotalSlabs;
		private System.Windows.Forms.Label lblTotalSlabs;
		private System.Windows.Forms.Label lblQuality;
		private System.Windows.Forms.Label lblStockCode;
		private System.Windows.Forms.Label lblSupplierCode;
		private System.Windows.Forms.Label lblMaterialCode;
		private System.Windows.Forms.TextBox txtLength;
		private System.Windows.Forms.Label lblLength;
		private System.Windows.Forms.TextBox txtSurface;
		private System.Windows.Forms.Label lblSurface;
		private System.Windows.Forms.TextBox txtThickness;
		private System.Windows.Forms.Label lblThickness;
		private System.Windows.Forms.TextBox txtWidth;
		private System.Windows.Forms.Label lblWidth;
		private System.Windows.Forms.Label lblDateProd;
		private System.Windows.Forms.TextBox txtBlSurface;
		private System.Windows.Forms.Label lblBlSurface;
		private System.Windows.Forms.TextBox txtBlOrigin;
		private System.Windows.Forms.Label lblBlOrigin;
		private System.Windows.Forms.Label lblUpdDate;
		private System.Windows.Forms.Label lblBlockState;
		private System.Windows.Forms.ComboBox cboBlockState;
		private System.Data.DataColumn dataColumn1;
		private System.Windows.Forms.DateTimePicker dtpUpdDate;
		private System.Windows.Forms.DateTimePicker dtpDateProd;
		private System.Windows.Forms.ComboBox cmbQuality;
		private System.Windows.Forms.ComboBox cmbSupplierCode;
		private System.Windows.Forms.ComboBox cmbMaterialCode;
		private System.Windows.Forms.Button btnGetNode;
		private System.Windows.Forms.TextBox txtStockPosition;
		private System.Windows.Forms.Button btnGetSupplier;
		private System.Windows.Forms.Button btnGetQuality;
		private System.Windows.Forms.Button btnGetMaterial;
		private System.Windows.Forms.Button btnSusp;
		private System.Windows.Forms.Button btnActiv;
		private System.Windows.Forms.Button btnDW;
		private System.Windows.Forms.Button btnUP;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Label lblBlockList;
		private System.Windows.Forms.CheckBox check_IN_PROGRESS;
		private System.Windows.Forms.CheckBox check_DEFINED;
		private System.Windows.Forms.CheckBox check_UNDEF;
		private System.Windows.Forms.CheckBox check_PROCESSED;
		private System.Windows.Forms.CheckBox check_SUSPENDED;
		private System.Windows.Forms.CheckBox check_ACTIVATED;
		private System.Windows.Forms.CheckBox check_ABORTED;
		private System.Windows.Forms.Label lblMillimetriMU;
		private System.Windows.Forms.Label lblSuperficieMU;
		private C1.Win.C1TrueDBGrid.C1TrueDBGrid dataGridLineBlocks;
		private System.Windows.Forms.Button btnSlabsList;
		private System.Windows.Forms.GroupBox grpBlockStates;
		private System.Windows.Forms.Button btnProcessed;
	}
}