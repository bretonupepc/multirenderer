﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Breton.Parameters;
using Breton.DbAccess;
using Breton.TraceLoggers;
using Breton.Users;
using System.Collections;
using Breton.SmartResources;
using Breton.LineBlocksSlabs;
using Breton.Qualities;
using Breton.Materials;
using Breton.Materials.Form;
using Breton.Customers_Suppliers;
using Breton.Customers_Suppliers.Form;
using Breton.Stocks;
using Breton.Magazzino;
using Breton.KeyboardInterface;
using Breton.KeyboardInterface.Form;

namespace Breton.LineBlocksSlabs.Form
{
	public partial class LineBlocks : System.Windows.Forms.Form
	{
		#region Variables

		private DbInterface mDbInterface = null;
		private DbInterface mDbInterfaceLineBS = null;
		private FieldCollection mImpost = null;
		private LineBlockCollection mLineBlocks = null;	//Collection
		private QualityCollection mQualities = null;
		private SupplierCollection mSuppliers = null;
		private MaterialCollection mMaterials = null;
		private ArrayList mBlockStatesView =null;

		private Stock mStock;
		private string mStockCode, mStockDescr;
		private int mStockLevel;
		
		private EventHandler qualityHandler;
		private EventHandler materialHandler;
		private EventHandler supplierHandler;
		private Breton.Magazzino.GetNode.SelectNodeEventHandler setNodeHandler;
		private Coordinate mMgzCoord;
		private Breton.Customers_Suppliers.Form.Suppliers.SelectSupplierEventHandler setSupHandler;
		private Breton.Materials.Form.frmMaterial.SetMaterialEventHandler setMatHandler;
		private Breton.Qualities.Form.Qualities.SelectQualityEventHandler setQlyHandler;
		private Action<string> updSlabs;

		private Field	fBlockCode, fCodeOp, fBlSurface, fBlOrigin, fSurface ,	fDesc;
		private Field	fTotalSlabs, fProcSlabs, fHangSlabs, fDeletedSlabs,
						fLength, fWidth, fThickness;

		private User mUser = null;
		private string mLanguage;
		private ParameterControlPositions mFormPosition;

		private static bool formCreate = false;
		private bool isModified = false;				//Modifica effettuata
		private C1Utils.TDBGridSort mSortGrid;
		private C1Utils.TDBGridColumnView mColumnView;

		private bool isDate = false;
		private bool imperial = false;

		private enum EditOperations
		{
			None,
			New,
			Edit,
			Save,
			Delete
		}
		private EditOperations mOperat;					//Operazione in corso

		public static event EventHandler UpdateBlocksEvent;

		#endregion

		#region Constructors

		/// <summary>
		/// LineBlocks
		/// </summary>
		/// <param name="db"></param>
		/// <param name="user"></param>
		/// <param name="language"></param>
		public LineBlocks(DbInterface db, DbInterface dbLineBlocksSlabs , User user, string language)
		{
			InitializeComponent();

			mUser = user;

			if (db == null)
			{
				mDbInterface = new DbInterface();
				mDbInterface.Open("", "DbConnection.udl");
			}
			else
				mDbInterface = db;

			if (dbLineBlocksSlabs == null)
			{
				mDbInterfaceLineBS = new DbInterface();
				mDbInterfaceLineBS.Open("", "DbLineBlocksSlabs.udl");
			}
			else
				mDbInterfaceLineBS = dbLineBlocksSlabs;


			// Carica la posizione del form
			mFormPosition = new ParameterControlPositions(this.Name);
			mFormPosition.LoadFormPosition(this);

			//istanzio la collectio blocchi
			mLineBlocks = new LineBlockCollection(mDbInterfaceLineBS);

			mLanguage = language;
			ProjResource.gResource.ChangeLanguage(mLanguage);
		}
		
		#endregion

		#region Properties

		public static bool gCreate
		{
			set { formCreate = value; }
			get { return formCreate; }
		}

		#endregion

		#region Form Function

		/// <summary>
		/// LineBlocks_Load
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LineBlocks_Load(object sender, EventArgs e)
		{

			this.Dock = DockStyle.Fill;

			// Carica le lingue del form
			ProjResource.gResource.LoadStringForm(this);

			// Handler per gli eventi di Aggiornamento liste da altri programmi
			qualityHandler = new EventHandler(UpdateQuality);
			Breton.Qualities.Form.Qualities.UpdateQuality += qualityHandler;
			
			materialHandler = new EventHandler(UpdateMaterial);
			Breton.Materials.Form.frmMaterial.UpdateFormMaterial += materialHandler;
			
			supplierHandler = new EventHandler(UpdateSupplier);
			Breton.Customers_Suppliers.Form.Suppliers.UpdateSuppliers += supplierHandler;

			setNodeHandler = new GetNode.SelectNodeEventHandler(NodeSelected);
			Breton.Magazzino.GetNode.SelectNodeEvent += setNodeHandler;

			// Handler per gli eventi di selezione da form esterni 
			setSupHandler = new Breton.Customers_Suppliers.Form.Suppliers.SelectSupplierEventHandler(FornitoreImpostato);
			Breton.Customers_Suppliers.Form.Suppliers.SelectSupplierEvent += setSupHandler;

			setMatHandler = new Breton.Materials.Form.frmMaterial.SetMaterialEventHandler(MaterialeImpostato);
			Breton.Materials.Form.frmMaterial.SetMaterialEvent += setMatHandler;

			setQlyHandler = new Breton.Qualities.Form.Qualities.SelectQualityEventHandler(QualitaImpostata);
			Breton.Qualities.Form.Qualities.SelectQualityEvent += setQlyHandler;

			updSlabs = new Action<string>(LineSlabs_UpdateSlabsBlockEvent);
			LineSlabs.UpdateSlabsBlockEvent += updSlabs;

			// Inizializzo la funzione di mImpost
			SetKeyboardControl();

			gCreate = true;

			// Carica le lingue del form
			ProjResource.gResource.LoadStringForm(this);
			mSortGrid = new Breton.C1Utils.TDBGridSort(dataGridLineBlocks, dataTableLineBlocks);
			mColumnView = new Breton.C1Utils.TDBGridColumnView(dataGridLineBlocks);

			//Legge le qualità dal db
			mQualities = new QualityCollection(mDbInterface);
			if (mQualities.GetDataFromDb())
				FillComboQualities();
			//Legge le qualità dal db
			mMaterials = new MaterialCollection(mDbInterface);
			if (mMaterials.GetDataFromDb(Material.Keys.F_DESCRIPTION))
				FillComboMaterials();
			//Legge le qualità dal db
			mSuppliers = new SupplierCollection(mDbInterface);
			if (mSuppliers.GetDataFromDb(Customer_Supplier.Keys.F_NAME))
				FillComboSuppliers();

			//Quale tipologia di blocchi devo leggere
			mBlockStatesView = new ArrayList();
			InitStates();
			//Legge la lista blocchi dal db
			if (mLineBlocks.GetDataFromDb(LineBlock.Keys.F_NONE,-1,null,mBlockStatesView,null))
			{ 
				FillTable();	//Riempe la Table
			}

			//Carico il combobox degli stati
			FillComboStates();

			//Imposta le coordinate magazzino
			mMgzCoord = new Coordinate();

			mOperat = EditOperations.None;
			DisablePanelEdit_New();
			EnableButtons();

		}

		private void LineBlocks_FormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult dlgClose;
			string message, caption;

			//Se ci sono state delle modifiche viene chiesto il salvataggio
			if (isModified)
			{

				ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
				dlgClose = MessageBox.Show(message, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (dlgClose == DialogResult.Yes)
				{
					if (!mLineBlocks.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
					{
						ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					else
						OnUpdate();
				}
				else if (dlgClose == DialogResult.Cancel)
					e.Cancel = true;				//Non viene effettuato il salvataggio e la form rimane aperta
			}

		}

		private void LineBlocks_FormClosed(object sender, FormClosedEventArgs e)
		{
			//Elimino Handler 
			Breton.Qualities.Form.Qualities.UpdateQuality -= qualityHandler;
			Breton.Materials.Form.frmMaterial.UpdateFormMaterial -= materialHandler;
			Breton.Customers_Suppliers.Form.Suppliers.UpdateSuppliers -= supplierHandler;
            Breton.Magazzino.GetNode.SelectNodeEvent -= setNodeHandler;
			
			Breton.Customers_Suppliers.Form.Suppliers.SelectSupplierEvent -= setSupHandler;
			Breton.Materials.Form.frmMaterial.SetMaterialEvent -= setMatHandler;
			Breton.Qualities.Form.Qualities.SelectQualityEvent -= setQlyHandler;

			LineSlabs.UpdateSlabsBlockEvent -= updSlabs;

			gCreate = false;
			// salva le posizioni del form
			mFormPosition.SaveFormPosition(this);

		}
		#endregion

		#region Gestione Tabelle

		/// <summary>
		/// Scorre la collection per riempire la table
		/// </summary>
		private void FillTable()
		{
			LineBlock blc;

			dataTableLineBlocks.Clear();

			// Scorre tutte le qualità lette
			mLineBlocks.MoveFirst();
			while (!mLineBlocks.IsEOF())
			{
				// Legge la classe di materiale attuale
				mLineBlocks.GetObject(out blc);

				dataTableLineBlocks.BeginInit();
				dataTableLineBlocks.BeginLoadData();

				AddRow(ref blc);

				dataTableLineBlocks.EndInit();
				dataTableLineBlocks.EndLoadData();
			}
		}

		/// <summary>
		/// Aggiunge una riga al dataTable
		/// </summary>
		/// <param name="blc"></param>
		private void AddRow(ref LineBlock blc)
		{
			object[] myArray = new object[24];
			myArray[0] = dataTableLineBlocks.Rows.Count;
			myArray[1] = blc.gId;
			myArray[2] = blc.gCode;
			myArray[3] = blc.gDescription;
			myArray[4] = blc.gMaterialCode;		
			myArray[5] = blc.gSupplierCode;		
			myArray[6] = blc.gStockCode;			
			myArray[7] = blc.gStockLevel;			
			myArray[8] = blc.gQualityCode;		
			myArray[9] = blc.gSlabThickness;		
			myArray[10] = blc.gTotalSlabs;			
			myArray[11] = blc.gProcSlabs;			
			myArray[12] = blc.gHangSlabs;			
			myArray[13] = blc.gDeletedSlabs;		
			myArray[14] = blc.gLength;				
			myArray[15] = blc.gWidth;				
			myArray[16] = blc.gThickness;			
			myArray[17] = blc.gSurface;
			myArray[18] = (blc.gDateProd == DateTime.MaxValue ? "" : blc.gDateProd.ToString());			
			myArray[19] = blc.gCodeOp;			    
			myArray[20] = blc.gBlSurface;			
			myArray[21] = blc.gBlOrigin;			
			myArray[22] = blc.gUpdDate;			
			myArray[23] = (LineBlock.BloccoStates) blc.gBlockState;			//Stato processo lavorazione blocco		F_BLOCK_STATES

			// Crea una nuova riga nella tabella e la riempie di dati
			DataRow r = dataTableLineBlocks.NewRow();
			r.ItemArray = myArray;
			dataTableLineBlocks.Rows.Add(r);

			// Assegna l'id della riga tabella alla qualità
			blc.gTableId = (int)r.ItemArray[0];

			// Passa all'elemento successivo
			mLineBlocks.MoveNext();
		}

		/// <summary>
		/// Cancella una riga dalla griglia parameters
		/// </summary>
		private void Delete()
		{
			LineBlock blc;
			int j = 0;
			string message, caption;

			if (dataGridLineBlocks.Bookmark >= 0)
			{
				// Indice della riga da eliminare
				int[] indexDelete = new int[0];
				while (j < dataGridLineBlocks.SelectedRows.Count)
				{
					int[] tmp = new int[indexDelete.Length + 1];
					indexDelete.CopyTo(tmp, 0);
					indexDelete = tmp;
					mLineBlocks.GetObjectTable(int.Parse(dataGridLineBlocks[dataGridLineBlocks.SelectedRows[j], 0].ToString()), out blc);
					indexDelete[j] = blc.gTableId;
					j++;
				}
				if (dataGridLineBlocks.SelectedRows.Count == 0)
				{
					indexDelete = new int[1];
					mLineBlocks.GetObjectTable(int.Parse(dataGridLineBlocks[dataGridLineBlocks.Bookmark, 0].ToString()), out blc);
					indexDelete[j] = blc.gTableId;
				}

				// Verifica se uno o più blocchi che si sta cercando di eliminare hanno lastre associate
				for (int k = 0; k < indexDelete.Length; k++)
				{
					LineBlock blk;
					LineSlabCollection tmpSlb = new LineSlabCollection(mDbInterfaceLineBS);

					mLineBlocks.GetObjectTable(indexDelete[k], out blk);
					tmpSlb.GetDataFromDb(LineSlab.Keys.F_NONE, blk.gCode);
					if (tmpSlb.Count > 0)
					{
						ProjResource.gResource.LoadMessageBox(this, 12, out caption, out message);
						MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
						return;
					}
				}

				ProjResource.gResource.LoadMessageBox(this, 5, out caption, out message);
				if (indexDelete.Length > 1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					for (int i = 0; i < indexDelete.Length; i++)
					{
						if (!mLineBlocks.DeleteObjectTable(indexDelete[i]))
						{
							TraceLog.WriteLine("LineBlocks: Delete() Error");
						}
					}
					isModified = true;
					ResetIndexTable();
				}
				else if (indexDelete.Length == 1 && MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
				{
					// Elimina la riga dalla collection e dalla tabella
					mLineBlocks.DeleteObjectTable(indexDelete[0]);
					dataTableLineBlocks.Rows.RemoveAt(indexDelete[0]);
					isModified = true;
					ResetIndexTable();
				}
				mOperat = EditOperations.None;
				FillTable();
			}
		}

		/// <summary>
		/// reset dell'indice della tabella
		/// </summary>
		private void ResetIndexTable()
		{
			LineBlock blc;
			int i = 0;

			mLineBlocks.MoveFirst();
			while (!mLineBlocks.IsEOF())
			{
				// Legge la qualità attuale
				mLineBlocks.GetObject(out blc);

				blc.gTableId = i;
				i++;
				mLineBlocks.MoveNext();
			}
			for (i = 0; i <= dataTableLineBlocks.Rows.Count - 1; i++)
			{
				object[] myArray = new object[24];
				myArray = dataTableLineBlocks.Rows[i].ItemArray;
				myArray[0] = i;
				dataTableLineBlocks.Rows[i].ItemArray = myArray;
			}
		}

		/// <summary>
		/// Imposta il colore in base allo stato del blocco
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dataGridLineBlocks_FetchRowStyle(object sender, C1.Win.C1TrueDBGrid.FetchRowStyleEventArgs e)
		{
			LineBlock blk;

			int i = (int)dataGridLineBlocks[e.Row, "T_ID"];
			mLineBlocks.GetObjectTable(i, out blk);

			e.CellStyle.GradientMode = C1.Win.C1TrueDBGrid.GradientModeEnum.Vertical;
			string state = dataGridLineBlocks[e.Row, "F_BLOCK_STATES"].ToString();

			if (state.Trim() == LineBlock.BloccoStates.UNDEF.ToString())
			{
				e.CellStyle.BackColor = Color.White;
				e.CellStyle.BackColor2 = Color.WhiteSmoke;
				e.CellStyle.ForeColor = Color.Black;
			}
			else if (state.Trim() == LineBlock.BloccoStates.BLKST_UNDEF.ToString())
			{
				e.CellStyle.BackColor = Color.White;
				e.CellStyle.BackColor2 = Color.WhiteSmoke;
				e.CellStyle.ForeColor = Color.Black;
			}
			else if (state.Trim() == LineBlock.BloccoStates.BLKST_ABORTED.ToString())
			{
				e.CellStyle.BackColor = Color.Red;
				e.CellStyle.BackColor2 = Color.WhiteSmoke;
				e.CellStyle.ForeColor = Color.Black;
			}
			else if (state.Trim() == LineBlock.BloccoStates.BLKST_ACTIVATED.ToString())
			{
				e.CellStyle.BackColor = Color.Gold;
				e.CellStyle.BackColor2 = Color.Yellow;
				e.CellStyle.ForeColor = Color.Black;
			}
			else if (state.Trim() == LineBlock.BloccoStates.BLKST_DEFINED.ToString())
			{
				e.CellStyle.BackColor = Color.LightGray;
				e.CellStyle.BackColor2 = Color.Silver;
				e.CellStyle.ForeColor = Color.Black;
			}
			else if (state.Trim() == LineBlock.BloccoStates.BLKST_IN_PROGRESS.ToString())
			{
				e.CellStyle.BackColor = Color.LightGreen;
				e.CellStyle.BackColor2 = Color.LimeGreen;
				e.CellStyle.ForeColor = Color.Black;
			}
			else if (state.Trim() == LineBlock.BloccoStates.BLKST_PROCESSED.ToString())
			{
				e.CellStyle.BackColor = Color.DimGray;
				e.CellStyle.BackColor2 = Color.Gray;
				e.CellStyle.ForeColor = Color.White;
			}
			else if (state.Trim() == LineBlock.BloccoStates.BLKST_SUSPENDED.ToString())
			{
				e.CellStyle.BackColor = Color.DarkOrange;
				e.CellStyle.BackColor2 = Color.Orange;
				e.CellStyle.ForeColor = Color.Black;
			}

		}

		#endregion

		#region Control Events

		/// <summary>
		/// bottone "btnNew" 
		/// aggiunge un nuovo parametro
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnNew_Click(object sender, EventArgs e)
		{

			mOperat = EditOperations.New;

			// Abilita i controlli nel pannello
			EnablePanelEdit_New();
			DisableButtons();
			mOperat = EditOperations.New;

			AggiornaCampiNewMod();
		}

		/// <summary>
		/// bottone "btnEdit"
		/// modifica un parametro
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnEdit_Click(object sender, EventArgs e)
		{
			int i;
			LineBlock blc;
			string message, caption;

			if (dataGridLineBlocks.Bookmark >= 0)
				i = (int)dataGridLineBlocks[dataGridLineBlocks.Bookmark, 0];
			else
				return;

			// Verifica che il blocco non sia in esecuzione.
			if (mLineBlocks.GetObjectTable(i, out blc) & blc.gBlockState == LineBlock.BloccoStates.BLKST_IN_PROGRESS)
			{
				ProjResource.gResource.LoadMessageBox(this, 10, out caption, out message);
				MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			mOperat = EditOperations.Edit;

			dataGridLineBlocks.Enabled = false;
			DisableButtons();
			EnablePanelEdit_New();

			AggiornaCampiNewMod();
		}

		/// <summary>
		/// bottone"btnDelete"
		/// elimina dalla griglia un parametro di configurazione
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnDelete_Click(object sender, EventArgs e)
		{
			mOperat = EditOperations.Delete;
			Delete();
			DisablePanelEdit_New();
			EnableButtons();

			AggiornaCampiNewMod();
			CellValue();
		}

		/// <summary>
		/// bottone "btnSave" 
		/// Salva nel DB i parametri o le modifiche
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSave_Click(object sender, EventArgs e)
		{
			string message, caption;

			// si vuole salvare?
			ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
			if (isModified && MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				// Salvo le informazioni su DB
				mOperat = EditOperations.Save;
				if (!mLineBlocks.UpdateDataToDb())
				{
					// errore nell'aggiornamento
					ProjResource.gResource.LoadMessageBox(this, 1, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
				{
					OnUpdate();
					TraceLog.WriteLine("Salvate le modifiche effettuate sui blocchi");
				}

				isModified = false;
				mOperat = EditOperations.None;

				//mParameters.MoveFirst();

				if (mLineBlocks.GetDataFromDb(LineBlock.Keys.F_NONE, -1, null, mBlockStatesView, null))
					FillTable();

				dataGridLineBlocks.CollapseGroupRow(0);
			}
		}

		/// <summary>
		/// chiude il form
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Attiva un nuovo blocco
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnActiv_Click(object sender, EventArgs e)
		{
			LineBlockCollection LineBlocksTmp;	//Collection
			DialogResult dlgClose;
			string message, caption, states;

			//Verifico se c'è già un blocco attivo
			//Legge la lista blocchi dal db
			LineBlocksTmp = new LineBlockCollection(mDbInterfaceLineBS);
			states = LineBlock.BloccoStates.BLKST_IN_PROGRESS.ToString();
			LineBlocksTmp.GetDataFromDb(LineBlock.Keys.F_NONE, null, states, null);

			if (LineBlocksTmp.Count > 0)
			{
				// esiste già un blocco in processo
				ProjResource.gResource.LoadMessageBox(this, 9, out caption, out message);
				MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

				return;
			}

			//Attivo il blocco
			ProjResource.gResource.LoadMessageBox(this, 6, out caption, out message);
			dlgClose = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dlgClose == DialogResult.Yes)
				ActivDeactivBlock(true);
		}

		/// <summary>
		/// Sospende il blocco attivo
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSusp_Click(object sender, EventArgs e)
		{
			DialogResult dlgClose;
			string message, caption;

			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);
			dlgClose = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (dlgClose == DialogResult.Yes)
				ActivDeactivBlock(false);

		}

		/// <summary>
		/// Imposta il blocco come processato
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnProcessed_Click(object sender, EventArgs e)
		{
			DialogResult dlgClose;
			string message, caption;
			LineBlock blc;
			int idTable;

			try
			{
				ProjResource.gResource.LoadMessageBox(this, 11, out caption, out message);
				dlgClose = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (dlgClose == DialogResult.Yes)
				{
					if (dataGridLineBlocks.Bookmark >= 0)
						idTable = (int)dataGridLineBlocks[dataGridLineBlocks.Bookmark, 0];
					else
						return;

					// Legge il blocco e lo setta come processato
					if (mLineBlocks.GetObjectTable(idTable, out blc))
					{
						blc.gBlockState = LineBlock.BloccoStates.BLKST_PROCESSED;

						if (mLineBlocks.UpdateDataToDb())
						{
							FillTable();
							OnUpdate();
						}
					}
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".ActivDeactivBlock: ", ex);
			}
		}

		/// <summary>
		/// Muovo il cursore del DataGrid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnUP_Click(object sender, EventArgs e)
		{
			dataGridLineBlocks.Row -= 1;  
		}

		/// <summary>
		/// Muovo il cursore del DataGrid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnDW_Click(object sender, EventArgs e)
		{
			dataGridLineBlocks.Row += 1;  
		}

		/// <summary>
		///  Annulla l'operazione in corso
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnReset_Click(object sender, System.EventArgs e)
		{
			// Annnulla l'operazione in corso
			DisablePanelEdit_New();
			EnableButtons();
			dataGridLineBlocks.Enabled = true;
			mOperat = EditOperations.None;
			AggiornaCampiNewMod();

			panelEditNew.Visible = false;

		}

		/// <summary>
		/// conferma e gestisce l'operazione precedentemente richiesta
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnConfirm_Click(object sender, EventArgs e)
		{
			LineBlock blc;
			string message, caption;

			if (cmbMaterialCode.SelectedValue.ToString() == "" || txtTotalSlabs.Text == "" || txtTotalSlabs.Text == "0" || mStockCode == null || mStockCode == "" || mStockLevel == 0)
			{
				// Alcuni parametri obbligatori non sono stati inseriti
				ProjResource.gResource.LoadMessageBox(this, 13, out caption, out message);
				MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			// Operazione in corso: Aggiunta nuovo parametro
			if (mOperat == EditOperations.New)
			{
				// Crea un nuovo oggetto passando i parametri al costruttore
				blc = new LineBlock();

				blc.gCode = (txtBlockCode.Text == "" ? null : txtBlockCode.Text);
				blc.gDescription = txtDesc.Text;
				blc.gMaterialCode = cmbMaterialCode.SelectedValue.ToString();
				blc.gSupplierCode = cmbSupplierCode.SelectedValue.ToString();
				blc.gStockCode = mStockCode;
				blc.gStockLevel = mStockLevel;
				blc.gQualityCode = cmbQuality.SelectedValue.ToString();
				blc.gSlabThickness = double.Parse(txtThickness.Text == "" ? "0" : txtThickness.Text);
				blc.gTotalSlabs = int.Parse(txtTotalSlabs.Text == "" ? "0" : txtTotalSlabs.Text);
				blc.gProcSlabs = int.Parse(txtProcSlabs.Text == "" ? "0" : txtProcSlabs.Text);
				blc.gHangSlabs = blc.gTotalSlabs - blc.gProcSlabs;
				blc.gDeletedSlabs = int.Parse(txtDeletedSlabs.Text == "" ? "0" : txtDeletedSlabs.Text);
				blc.gLength = double.Parse(txtLength.Text == "" ? "0" : txtLength.Text);
				blc.gWidth = double.Parse(txtWidth.Text == "" ? "0" : txtWidth.Text);
				blc.gThickness = double.Parse(txtThickness.Text == "" ? "0" :txtThickness.Text);
				blc.gSurface = double.Parse(txtSurface.Text == "" ? "0" : txtSurface.Text);
				blc.gDateProd = DateTime.Parse(dtpDateProd.Value.ToString() == "" ? DateTime.Now.ToString() : dtpDateProd.Value.ToString());
				blc.gCodeOp = txtCodeOp.Text;
				blc.gBlSurface = txtBlSurface.Text;
				blc.gBlOrigin = txtBlOrigin.Text;
				blc.gUpdDate = DateTime.Parse(dtpUpdDate.Value.ToString() == "" ? DateTime.Now.ToString() : dtpUpdDate.Value.ToString());
				blc.gBlockState = (LineBlock.BloccoStates)cboBlockState.SelectedValue;

				// Aggiunge l'oggetto alla collection
				if (mLineBlocks.AddObject(blc))
				{
					isModified = true;
					// Aggiunge l'oggetto alla Table
					mLineBlocks.MoveLast();
					mLineBlocks.GetObject(out blc);
					AddRow(ref blc);
				}
				else
				{
					// errore nell'inserimento
					ProjResource.gResource.LoadMessageBox(this, 2, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				FillTable();

				TraceLog.WriteLine("Aggiunto manualmente un nuovo blocco con i seguenti dati." +
					(blc.gCode != null && blc.gCode.Length > 0 ? "Codice: " + blc.gCode.Trim() : "") +
					(blc.gDescription != null && blc.gDescription.Length > 0 ? " Descrizione:" + blc.gDescription : "") +
					(blc.gMaterialCode != null && blc.gMaterialCode.Length > 0 ? " Materiale: " + blc.gMaterialCode.Trim() : "") +
					(blc.gSupplierCode != null && blc.gSupplierCode.Length > 0 ? " Fornitore: " + blc.gSupplierCode.Trim() : "") +
					(blc.gStockCode != null && blc.gStockCode.Length > 0 ? " Codice magazzino: " + blc.gStockCode.Trim() : "") +
					(blc.gStockLevel >= 0 ? " Livello magazzino: " + blc.gStockLevel.ToString() : "") +
					(blc.gQualityCode != null && blc.gQualityCode.Length > 0 ? " Qualità: " + blc.gQualityCode : "") +
					(blc.gSlabThickness >= 0 ? " Spessore: " + blc.gSlabThickness.ToString() : "") +
					" Totali: " + blc.gTotalSlabs.ToString() + " Processate: " + blc.gProcSlabs.ToString() + " Mancanti: " + blc.gHangSlabs.ToString() +
					" Lunghezza: " + blc.gLength.ToString() + " Larghezza: " + blc.gWidth.ToString() + " Superficie: " + blc.gSurface.ToString() +
					" Data produzione: " + blc.gDateProd.ToString() + " Operatore: " + blc.gCodeOp + " Superficie blocco: " + blc.gBlSurface +
					" Origine blocco: " + blc.gBlOrigin + " Update: " + blc.gUpdDate + " Stato: " + blc.gBlockState);
			}
			// Operazione in corso: Modifica di un design working types
			else if (mOperat == EditOperations.Edit)
			{
				// Aggiorna la Table
				int idTable = (int)dataGridLineBlocks[dataGridLineBlocks.Bookmark, 0];
				if (mLineBlocks.GetObjectTable(idTable, out blc))
				{
					// Aggiorna tutti i valori con i nuovi inseriti
					// blc.gCode = (txtBlockCode.Text == "" ? null : txtBlockCode.Text);
					blc.gDescription = txtDesc.Text;
					blc.gMaterialCode = cmbMaterialCode.SelectedValue.ToString();
					blc.gSupplierCode = (cmbSupplierCode.SelectedValue == null ? "" : cmbSupplierCode.SelectedValue.ToString() );
					blc.gStockCode = mStockCode;
					blc.gStockLevel = mStockLevel;
					blc.gQualityCode = cmbQuality.SelectedValue.ToString();
					blc.gSlabThickness = int.Parse(txtThickness.Text);
					blc.gTotalSlabs = int.Parse(txtTotalSlabs.Text);
					blc.gProcSlabs = int.Parse(txtProcSlabs.Text);
					blc.gHangSlabs = int.Parse(txtHangSlabs.Text);
					blc.gDeletedSlabs = int.Parse(txtDeletedSlabs.Text);
					blc.gLength = double.Parse(txtLength.Text);
					blc.gWidth = double.Parse(txtWidth.Text);
					blc.gThickness = double.Parse(txtThickness.Text);
					blc.gSurface = double.Parse(txtSurface.Text);
					blc.gDateProd = dtpDateProd.Value;
					blc.gCodeOp = txtCodeOp.Text;
					blc.gBlSurface = txtBlSurface.Text;
					blc.gBlOrigin = txtBlOrigin.Text;
					blc.gUpdDate = dtpUpdDate.Value;
					blc.gBlockState = (LineBlock.BloccoStates)cboBlockState.SelectedValue;
				}
				else
					throw new Exception("LineBlocks.btnConferma Button Error");

				object[] rowChange =  {idTable, blc.gId , blc.gCode , blc.gDescription, blc.gMaterialCode, 
				                       blc.gSupplierCode, blc.gStockCode, blc.gStockLevel,blc.gQualityCode, blc.gSlabThickness,
									   blc.gTotalSlabs, blc.gProcSlabs, blc.gHangSlabs, blc.gDeletedSlabs,
									   blc.gLength, blc.gWidth, blc.gThickness, blc.gSurface, blc.gDateProd, 
									   blc.gCodeOp, blc.gBlSurface, blc.gBlOrigin, blc.gUpdDate, blc.gBlockState};

				dataTableLineBlocks.Rows[idTable].ItemArray = rowChange;
				isModified = true;
				dataGridLineBlocks.Enabled = true;

				TraceLog.WriteLine("Modificato manualmente un blocco con i seguenti dati." +
					(blc.gCode != null && blc.gCode.Length > 0 ? "Codice: " + blc.gCode.Trim() : "") +
					(blc.gDescription != null && blc.gDescription.Length > 0 ? " Descrizione:" + blc.gDescription : "") +
					(blc.gMaterialCode != null && blc.gMaterialCode.Length > 0 ? " Materiale: " + blc.gMaterialCode.Trim() : "") +
					(blc.gSupplierCode != null && blc.gSupplierCode.Length > 0 ? " Fornitore: " + blc.gSupplierCode.Trim() : "") +
					(blc.gStockCode != null && blc.gStockCode.Length > 0 ? " Codice magazzino: " + blc.gStockCode.Trim() : "") +
					(blc.gStockLevel >= 0 ? " Livello magazzino: " + blc.gStockLevel.ToString() : "") +
					(blc.gQualityCode != null && blc.gQualityCode.Length > 0 ? " Qualità: " + blc.gQualityCode : "") +
					(blc.gSlabThickness >= 0 ? " Spessore: " + blc.gSlabThickness.ToString() : "") +
					" Totali: " + blc.gTotalSlabs.ToString() + " Processate: " + blc.gProcSlabs.ToString() + " Mancanti: " + blc.gHangSlabs.ToString() +
					" Lunghezza: " + blc.gLength.ToString() + " Larghezza: " + blc.gWidth.ToString() + " Superficie: " + blc.gSurface.ToString() +
					" Data produzione: " + blc.gDateProd.ToString() + " Operatore: " + blc.gCodeOp + " Superficie blocco: " + blc.gBlSurface +
					" Origine blocco: " + blc.gBlOrigin + " Update: " + blc.gUpdDate + " Stato: " + blc.gBlockState);
			}
			else if (mOperat == EditOperations.None)
			{
				return;
			}

			// Abilita-Disabilita i vari controlli
			mOperat = EditOperations.None;
			DisablePanelEdit_New();
			EnableButtons();
			AggiornaCampiNewMod();

			CellValue();
			panelEditNew.Visible =false;

		}

		/// <summary>
		/// Selezione magazzino
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnGetNode_Click(object sender, EventArgs e)
		{
			if (!GetNode.gCreate)
			{
				GetNode stkSet = new GetNode(mDbInterface, mLanguage, null);
				stkSet.MdiParent = this.MdiParent;
				stkSet.Show();
			}
		}

		/// <summary>
		/// Selezione fornitore
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnGetSupplier_Click(object sender, System.EventArgs e)
		{
			if (!Suppliers.gCreate)
			{
				Suppliers frmSupp = new Suppliers(mDbInterface, mUser, mLanguage);
				frmSupp.MdiParent = this.MdiParent;
				frmSupp.Show();
			}
		}

		/// <summary>
		/// Selezione Qiualità
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnGetQuality_Click(object sender, System.EventArgs e)
		{
		    if (!Breton.Qualities.Form.Qualities.gCreate)
			{
		        Breton.Qualities.Form.Qualities frmQual = new Breton.Qualities.Form.Qualities(mDbInterface, mUser, mLanguage);
		        frmQual.MdiParent = this.MdiParent;
		        frmQual.Show();
		    }
		}

		/// <summary>
		/// Gestore dell'evento di aggiornamento Materiali
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnGetMaterial_Click(object sender, System.EventArgs e)
		{
			if (!frmMaterial.gCreate)
			{
				frmMaterial frmMat = new frmMaterial(mDbInterface, mUser, "", mLanguage);
				frmMat.MdiParent = this.MdiParent;
				frmMat.Show();
			}
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSuperficie_Click(object sender, EventArgs e)
		{
			//if (!LineList.gCreate)
			//{
			//    LineList frmMat = new LineList(mDbInterface, mUser, "", mLanguage);
			//    frmMat.MdiParent = this.MdiParent;
			//    frmMat.Show();
			//}

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnOrigine_Click(object sender, EventArgs e)
		{
			//if (!Breton.LineBlocksSlabs.Form.LineLists.gCreate)
			//{
			//    Breton.LineBlocksSlabs.Form.LineLists frmLineList = new Breton.LineBlocksSlabs.Form.LineLists(mDbInterface, mUser, "", mLanguage);
			//    frmLineList.MdiParent = this.MdiParent;
			//    frmLineList.Show();
			//}

		}


		/// <summary>
		/// Gestore dell'evento di aggiornamento Qualità
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ev"></param>
		private void UpdateQuality(object sender, EventArgs ev)
		{
			if (mQualities.GetDataFromDb())
			{
				FillComboQualities();
			}
		}

		/// <summary>
		/// Gestore dell'evento di aggiornamento Materiale
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ev"></param>
		private void UpdateMaterial(object sender, EventArgs ev)
		{
			if (mMaterials.GetDataFromDb())
			{
				FillComboMaterials();
			}
		}

		/// <summary>
		/// Gestore dell'evento di aggiornamento Fornitore
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ev"></param>
		private void UpdateSupplier(object sender, EventArgs ev)
		{
			if (mSuppliers.GetDataFromDb())
			{
				FillComboSuppliers();
			}
		}

		/// <summary>
		/// Gestore evento selezione fornitore
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ev"></param>
		private void FornitoreImpostato(object sender, Supplier ev)
		{
			//if (mSuppliers.GetDataFromDb())
			//    FillComboSuppliers();

			SelectSupplierCombo(ev.gCode);
			//cmbSupplierCode.SelectedValue = ev.gCode.Trim();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ev"></param>
		private void QualitaImpostata(object sender, Quality ev)
		{
			//if (mQuality.GetDataFromDb())
			//    FillComboQualities();

			SelectQualityCombo(ev.gCode);
			//cmbQuality.SelectedValue = ev.gCode.Trim();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ev"></param>
		private void MaterialeImpostato(object sender, SetMaterial ev)
		{
			//if (mMaterial.GetDataFromDb())
			//    FillComboMaterials();

			SelectMaterialCombo(ev.setMat.gCode);
			//cmbMaterialCode.SelectedValue = ev.setMat.gCode.Trim();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="supCode"></param>
		private void SelectSupplierCombo(string supCode)
		{
			Supplier sup;

			for (int i = 1; i <= mSuppliers.Count; i++)
			{
				if (mSuppliers.GetObjectTable(i, out sup))
				{
					if (sup.gCode.Trim() == supCode.Trim())
					{
						cmbSupplierCode.SelectedIndex = i;
						break;
					}
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="supCode"></param>
		private void SelectQualityCombo(string quaCode)
		{
			Quality qua;

			for (int i = 1; i <= mQualities.Count; i++)
			{
				if (mQualities.GetObjectTable(i, out qua))
				{
					if (qua.gCode.Trim() == quaCode.Trim())
					{
						cmbQuality.SelectedIndex = i;
						break;
					}
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="supCode"></param>
		private void SelectMaterialCombo(string matCode)
		{
			Material mat;

			for (int i = 1; i <= mMaterials.Count; i++)
			{
				if (mMaterials.GetObjectTable(i, out mat))
				{
					if (mat.gCode.Trim() == matCode.Trim())
					{
						cmbMaterialCode.SelectedIndex = i;
						break;
					}
				}
			}
		}

		/// <summary>
		/// Aggiorna la griglia dei blocchi
		/// </summary>
		/// <param name="blockCode"></param>
		private void LineSlabs_UpdateSlabsBlockEvent(string blockCode)
		{
			// Aggiorna la griglia dei blocchi
			if (mLineBlocks.GetDataFromDb(LineBlock.Keys.F_NONE, -1, null, mBlockStatesView, null))
				FillTable();
		}

		/// <summary>
		/// Quale checkBox è stato modificato
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void checkStates_CheckedChanged(object sender, EventArgs e)
		{
			string message, caption;

			CheckBox chb = (CheckBox)sender;


			chb.Text += " (loading...)";
			Refresh();

			mBlockStatesView.Clear();

			if (check_UNDEF.Checked)
				mBlockStatesView.Add(LineBlock.BloccoStates.BLKST_UNDEF);
			if (check_DEFINED.Checked)
				mBlockStatesView.Add(LineBlock.BloccoStates.BLKST_DEFINED);
			if (check_ACTIVATED.Checked)
				mBlockStatesView.Add(LineBlock.BloccoStates.BLKST_ACTIVATED);
			if (check_IN_PROGRESS.Checked)
				mBlockStatesView.Add(LineBlock.BloccoStates.BLKST_IN_PROGRESS);
			if (check_SUSPENDED.Checked)
				mBlockStatesView.Add(LineBlock.BloccoStates.BLKST_SUSPENDED);
			if (check_PROCESSED.Checked)
				mBlockStatesView.Add(LineBlock.BloccoStates.BLKST_PROCESSED);
			if (check_ABORTED.Checked)
				mBlockStatesView.Add(LineBlock.BloccoStates.BLKST_ABORTED);


			// chiedo se si vuole salvare?
			ProjResource.gResource.LoadMessageBox(this, 0, out caption, out message);
			if (isModified && MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				if (!mLineBlocks.UpdateDataToDb())
				{
					ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
					MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				else
					OnUpdate();
			}
			isModified = false;

			//Legge la lista blocchi dal db con i nuovi filtri
			if (mLineBlocks.GetDataFromDb(LineBlock.Keys.F_NONE, -1, null, mBlockStatesView, null))
			{
				FillTable();	//Riempe la Table
			}

			chb.Text = chb.Text.Replace(" (loading...)", "");
		}

		/// <summary>
		/// Visualizza la finestra per la visualizzazione delle lastre del blocco
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnSlabsList_Click(object sender, EventArgs e)
		{
			LineBlock blc;
			int i;
			string caption, message;

			if (!LineSlabs.gCreate)
			{
				int bookmark = dataGridLineBlocks.Bookmark;

				if (isModified)
				{
					ProjResource.gResource.LoadMessageBox(this, 3, out caption, out message);
					if (MessageBox.Show(message, caption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (!mLineBlocks.UpdateDataToDb())	//Viene effettuato il salvataggio e chiusa la form		
						{
							ProjResource.gResource.LoadMessageBox(this, 4, out caption, out message);
							MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
						}
						else
						{
							isModified = false;
							OnUpdate();
							FillTable();
							dataGridLineBlocks.Bookmark = bookmark;
						}
					}
					else
						return; //Non viene effettuato il salvataggio e quindi non si apre la finestra della lista lastre
				}

				if (dataGridLineBlocks.Bookmark >= 0)
					i = (int)dataGridLineBlocks[dataGridLineBlocks.Bookmark, 0];
				else
					return;

				mLineBlocks.GetObjectTable(i, out blc);

				if (blc != null)
				{
					LineSlabs lineSlb = new LineSlabs(mDbInterface, mDbInterfaceLineBS, mUser, mLanguage, blc);
					lineSlb.MdiParent = this.MdiParent;
					lineSlb.Show();
				}
			}
		}

		#endregion

		#region Private Methods
		/// <summary>
		/// 
		/// </summary>
		private void ActivDeactivBlock(bool Attiva)
		{
			string message, caption;

			try
			{
				LineBlock blc, blcOld;
				int idTable;

				if (dataGridLineBlocks.Bookmark >= 0)
					idTable = (int)dataGridLineBlocks[dataGridLineBlocks.Bookmark, 0];
				else
					return;

				// Aggiorna i campi di Edit con i valori della riga selezionata se non sto facendo operazioni
				if (mLineBlocks.GetObjectTable(idTable, out blc))
				{
					if (Attiva)
					{
						blc.gBlockState = LineBlock.BloccoStates.BLKST_IN_PROGRESS;
						TraceLog.WriteLine("Attivato il blocco: " + blc.gCode.Trim());
					}
					else
					{
						if (blc.gBlockState == LineBlock.BloccoStates.BLKST_IN_PROGRESS)
						{
							blc.gBlockState = LineBlock.BloccoStates.BLKST_SUSPENDED;
							TraceLog.WriteLine("Disattivato il blocco: " + blc.gCode.Trim());
						}
						else
						{
							ProjResource.gResource.LoadMessageBox(this, 8, out caption, out message);
							MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
							TraceLog.WriteLine("Non è stato possibile disattivare il blocco: " + blc.gCode.Trim() + "perchè non era in lavoro");
						}
					}
					if (mLineBlocks.UpdateDataToDb())
					{
						FillTable();
						OnUpdate();
					}
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".ActivDeactivBlock: ", ex);
			}

		}
		
		/// <summary>
		/// inizializza il valore dei textBox relativi al record selezionato nella griglia
		/// </summary>
		private void CellValue()
		{
			try
			{
				LineBlock blc;
				int i;


				if (dataGridLineBlocks.Bookmark >= 0)
					i = (int)dataGridLineBlocks[dataGridLineBlocks.Bookmark, 0];
				else
					return;

				// Aggiorna i campi di Edit con i valori della riga selezionata se non sto facendo operazioni
				if (mOperat == EditOperations.Edit)
				{
					if (mLineBlocks.GetObjectTable(i, out blc))
					{
						txtBlockCode.Text = blc.gCode;
						cmbMaterialCode.SelectedValue = blc.gMaterialCode.Trim();
						cmbSupplierCode.SelectedValue = blc.gSupplierCode.Trim();
						cmbQuality.SelectedValue = blc.gQualityCode.Trim();
						
						mStockCode = blc.gStockCode;
						mStockLevel = blc.gStockLevel;
						mStockDescr = GetStockDescription(blc.gStockLevel, blc.gStockCode);
						txtStockPosition.Text = mStockCode.Trim() + " - " + mStockDescr.Trim();

						dtpDateProd.Value = blc.gDateProd;
						txtCodeOp.Text = blc.gCodeOp;
						txtTotalSlabs.Text = blc.gTotalSlabs.ToString();
						txtProcSlabs.Text = blc.gProcSlabs.ToString();
						txtHangSlabs.Text = blc.gHangSlabs.ToString();
						txtDeletedSlabs.Text = blc.gDeletedSlabs.ToString();

						txtLength.Text = blc.gLength.ToString();
						txtWidth.Text = blc.gWidth.ToString();
						txtThickness.Text = blc.gThickness.ToString();
						txtSurface.Text = blc.gSurface.ToString();
						txtBlSurface.Text = blc.gBlSurface.ToString();
						txtBlOrigin.Text = blc.gBlOrigin;
						dtpUpdDate.Value = blc.gUpdDate;

						txtDesc.Text = blc.gDescription ;
						cboBlockState.SelectedValue = blc.gBlockState;

					}
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("EditLineBlocks.CellValue: ", ex);
			}
		}

		/// <summary>
		/// Abilita i controlli presenti nel panel Edit_New
		/// </summary>
		private void EnablePanelEdit_New()
		{
			TextBox txt;
			ComboBox cbo;
			Button btn;
			DateTimePicker dtmp;

			foreach (Control ctr in panelEditNew.Controls)
			{
				if (ctr is TextBox)
				{
					txt = (TextBox)ctr;
					txt.Enabled = true;
					if (mOperat == EditOperations.Edit || mOperat == EditOperations.New )
						if (txt.Name == "txtBlockCode" || txt.Name == "txtHangSlabs")
						{
							txt.ReadOnly = true;
							txt.Enabled = false;
						}else
							txt.ReadOnly = false;
					else
						txt.ReadOnly = true;
				}
				else if (ctr is ComboBox)
				{
					cbo = (ComboBox)ctr;
					cbo.Enabled = true;
				}
				else if (ctr is DateTimePicker)
				{
					dtmp = (DateTimePicker)ctr;
					dtmp.Enabled = true;
				}
				else if (ctr is Button)
				{
					btn = (Button)ctr;
					btn.Enabled = true;
				}

			}
			// Abilita i pulsanti del "Pannello Edit"
			btnConfirm.Enabled = true;
			btnReset.Enabled = true;
		}

		/// <summary>
		/// Disabilita i controlli presenti nel panel Edit_New
		/// </summary>
		private void DisablePanelEdit_New()
		{
			TextBox txt;
			Button btn;
			ComboBox cbo;
			DateTimePicker dtmp;

			foreach (Control ctr in panelEditNew.Controls)
			{
				if (ctr is TextBox)
				{
					txt = (TextBox)ctr;
					txt.ReadOnly = true;
				}
			}
			foreach (Control ctr in panelEditNew.Controls)
			{
				if (ctr is Button)
				{
					btn = (Button)ctr;
					btn.Enabled = false;
				}
				else if (ctr is ComboBox)
				{
					cbo = (ComboBox)ctr;
					cbo.Enabled = false;
				}
				else if (ctr is DateTimePicker )
				{
					dtmp = (DateTimePicker)ctr;
					dtmp.Enabled = false;
				}
			}
		}

		/// <summary>
		/// Aggiorna i campi del pannello dati a seconda adell'operazione scelta
		/// </summary>
		private void AggiornaCampiNewMod()
		{
			Parameter par;
			// Aggiornamento dei campi di Edit a seconda dell'operazione in corso
			if (mOperat == EditOperations.New)
			{
				panelEditNew.Visible = true;

				txtBlockCode.Text = "";
				cmbMaterialCode.SelectedValue = "";
				cmbSupplierCode.SelectedValue = "";
				cmbQuality.SelectedValue = "";
				txtStockPosition.Text = "";
				dtpDateProd.Value = DateTime.Now;
				txtCodeOp.Text = "";
				txtTotalSlabs.Text = "";
				txtProcSlabs.Text = "";
				txtHangSlabs.Text = "";
				txtDeletedSlabs.Text = "";

				txtLength.Text = "";
				txtWidth.Text = "";
				txtThickness.Text = "";
				txtSurface.Text = "";
				txtBlSurface.Text = "";
				txtBlOrigin.Text = "";
				dtpUpdDate.Value = DateTime.Now;

				txtDesc.Text = "";
				cboBlockState.SelectedIndex  = 1;

				dataGridLineBlocks.Enabled = true;
			}
			else if (mOperat == EditOperations.Edit)
			{
				CellValue();

				SetFieldValue();

				panelEditNew.Visible = true;
			}
			else if (mOperat == EditOperations.None)
			{
				txtBlockCode.Text = "";
				cmbMaterialCode.SelectedValue = "";
				cmbSupplierCode.SelectedValue = "";
				cmbQuality.SelectedValue = "";
				txtStockPosition.Text = "";
				dtpDateProd.Value = DateTime.Now;
				txtCodeOp.Text = "";
				txtTotalSlabs.Text = "";
				txtProcSlabs.Text = "";
				txtHangSlabs.Text = "";
				txtDeletedSlabs.Text = "";

				txtLength.Text = "";
				txtWidth.Text = "";
				txtThickness.Text = "";
				txtSurface.Text = "";
				txtBlSurface.Text = "";
				txtBlOrigin.Text = "";
				dtpUpdDate.Value = DateTime.Now;

				txtDesc.Text = "";
				cboBlockState.SelectedIndex = -1;

				dataGridLineBlocks.Enabled = true;
			}
		}

		/// <summary>
		/// Abilita i pulsanti
		/// </summary>
		private void EnableButtons()
		{
			btnNew.Enabled = true;
			btnEdit.Enabled = true;
			btnDelete.Enabled = true;
			btnSave.Enabled = true;
			btnUP.Enabled = true;
			btnDW.Enabled = true;
			btnActiv.Enabled = true;
			btnSusp.Enabled = true;
		}

		/// <summary>
		/// Disabilita i pulsanti
		/// </summary>
		private void DisableButtons()
		{
			btnNew.Enabled = false;
			btnEdit.Enabled = false;
			btnDelete.Enabled = false;
			btnSave.Enabled = false;
			btnUP.Enabled = false;
			btnDW.Enabled = false;
			btnActiv.Enabled = false;
			btnSusp.Enabled = false;
		}

		/// <summary>
		/// Attiva il pulsante di Conferma quando il nome parametro non è nullo
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void AttivaOkNewUpdate(object sender, System.EventArgs e)
		{
			// Attiva il pulsante di Conferma quando il nome parametro non è nullo
		//    if (txtName.TextLength > 0 && txtValue.TextLength > 0 && cboType.SelectedIndex >= 0 && Operat != EditOperations.None)
		//        btnConfirm.Enabled = true;
		//    else
		//        btnConfirm.Enabled = false;
		}

		/// <summary>
		/// default per checkBox stato blocchi
		/// </summary>
		private void InitStates()
		{
			// attivo direttamente i checkBox grafici
			check_UNDEF.Checked = true;
			check_DEFINED.Checked = true;
			check_ACTIVATED.Checked = true;
			check_IN_PROGRESS.Checked = true;
			check_SUSPENDED.Checked = false;
			check_PROCESSED.Checked = false;
		}

		#endregion

		#region Gestione Filtro

		private void dtpDateProd_ValueChanged(object sender, EventArgs e)
		{
			Font boldFont = new Font(dtpDateProd.Font, FontStyle.Bold);
			dtpDateProd.Font = boldFont;
			isDate = true;

		}

		private void dtpUpdDate_ValueChanged(object sender, EventArgs e)
		{
			Font boldFont = new Font(dtpUpdDate.Font, FontStyle.Bold);
			dtpUpdDate.Font = boldFont;
			isDate = true;

		}

		#endregion

		#region Gestione Combo

		/// <summary>
		/// Carica il combobox qualità
		/// </summary>
		private void FillComboQualities()
		{
			Quality qly = new Quality();
			ArrayList qualities = new ArrayList();

			qualities.Add(qly);

			// Scorre tutte le qualità lette
			mQualities.MoveFirst();
			while (!mQualities.IsEOF())
			{
				// Legge la qualità attuale
				mQualities.GetObject(out qly);
				qly.gTableId = qualities.Count;  //cmbQuality.Items.Count;
				qualities.Add(qly);

				// Passa all'elemento successivo
				mQualities.MoveNext();
			}

			cmbQuality.DataSource = qualities;
			cmbQuality.DisplayMember = "gDescription";
			cmbQuality.ValueMember = "gCode";
		}

		/// <summary>
		/// Carica il combobox materiali
		/// </summary>
		private void FillComboMaterials()
		{
			Material mat = new Material();
			ArrayList materials = new ArrayList();

			materials.Add(mat);

			// Scorre tutti i materiali lette
			mMaterials.MoveFirst();
			while (!mMaterials.IsEOF())
			{
				// Legge la qualità attuale
				mMaterials.GetObject(out mat);
				mat.gTableId = materials.Count;// cmbMaterialCode.Items.Count;
				mat.gCode = mat.gCode.Trim();
				materials.Add(mat);

				// Passa all'elemento successivo
				mMaterials.MoveNext();
			}

			cmbMaterialCode.DataSource = materials;
			cmbMaterialCode.DisplayMember = "gDescription";
			cmbMaterialCode.ValueMember = "gCode";
		}

		/// <summary>
		/// Carica il combobox fornitori
		/// </summary>
		private void FillComboSuppliers()
		{
			Supplier sup = new Supplier();
			ArrayList suppliers = new ArrayList();

			suppliers.Add(sup);

			// Scorre tutte le qualità lette
			mSuppliers.MoveFirst();
			while (!mSuppliers.IsEOF())
			{
				// Legge la qualità attuale
				mSuppliers.GetObject(out sup);
				sup.gTableId = suppliers.Count; // cmbSupplierCode.Items.Count;
				sup.gCode = sup.gCode.Trim();
				suppliers.Add(sup);

				// Passa all'elemento successivo
				mSuppliers.MoveNext();
			}

			cmbSupplierCode.DataSource = suppliers;
			cmbSupplierCode.DisplayMember = "gName";
			cmbSupplierCode.ValueMember = "gCode";
		}

		/// <summary>
		/// Carica i testi della ComboBox BlockState (Stato del blocco)
		/// </summary>
		private void FillComboStates()
		{
			ArrayList states = new ArrayList();

			states.Add(new ComboDescription(LineBlock.BloccoStates.BLKST_UNDEF, ProjResource.gResource.LoadString(LineBlock.BloccoStates.BLKST_UNDEF.ToString())));
			states.Add(new ComboDescription(LineBlock.BloccoStates.BLKST_DEFINED, ProjResource.gResource.LoadString(LineBlock.BloccoStates.BLKST_DEFINED.ToString())));
			states.Add(new ComboDescription(LineBlock.BloccoStates.BLKST_ACTIVATED, ProjResource.gResource.LoadString(LineBlock.BloccoStates.BLKST_ACTIVATED.ToString())));
			//states.Add(new ComboDescription(LineBlock.BloccoStates.BLKST_IN_PROGRESS, ProjResource.gResource.LoadString(LineBlock.BloccoStates.BLKST_IN_PROGRESS.ToString())));
			states.Add(new ComboDescription(LineBlock.BloccoStates.BLKST_SUSPENDED, ProjResource.gResource.LoadString(LineBlock.BloccoStates.BLKST_SUSPENDED.ToString())));
			states.Add(new ComboDescription(LineBlock.BloccoStates.BLKST_PROCESSED, ProjResource.gResource.LoadString(LineBlock.BloccoStates.BLKST_PROCESSED.ToString())));
			states.Add(new ComboDescription(LineBlock.BloccoStates.BLKST_ABORTED, ProjResource.gResource.LoadString(LineBlock.BloccoStates.BLKST_ABORTED.ToString())));
			
			cboBlockState.DataSource = states;
			cboBlockState.DisplayMember = "ValueDescription";
			cboBlockState.ValueMember = "Value";

		}

		#endregion

		#region Keyboard control

		/// <summary>
		/// Setta i campi Field che saranno utilizzati nella gestione del form
		/// </summary>
		private void SetKeyboardControl()
		{
			mImpost = new FieldCollection(imperial);
			
			// Campi Filtro testo
			mImpost.AddStringField(txtBlockCode, null, null, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "", false);
			mImpost.AddStringField(txtCodeOp, null, null, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "", false);
			mImpost.AddStringField(txtBlSurface, null, null, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "", false);
			mImpost.AddStringField(txtBlOrigin, null, null, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "", false);
			mImpost.AddStringField(txtDesc, null, null, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "", false);
			// Campi Filtro numerici
			mImpost.AddNumericField(txtTotalSlabs, 0, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "0", false, true);
			mImpost.AddNumericField(txtProcSlabs, 0, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "0", false, true);
			mImpost.AddNumericField(txtHangSlabs, 0, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "0", false);
			mImpost.AddNumericField(txtDeletedSlabs, 0, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.NONE, "0", false);
			mImpost.AddNumericField(txtLength, 0, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.MM, "", false);
			mImpost.AddNumericField(txtWidth, 0, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.MM, "", false);
			mImpost.AddNumericField(txtThickness, 0, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.MM, "", false);
			mImpost.AddNumericField(txtSurface, 1000000, 0, 0, 0, Breton.KeyboardInterface.MeasureUnit.MU.SQM, "0.00", false);

			// creo delle variabili Fiel per non dover utilizzare mImpost+(indice)
			fBlockCode = mImpost.GetField(0);
			fCodeOp = mImpost.GetField(1);
			fBlSurface = mImpost.GetField(2);
			fBlOrigin = mImpost.GetField(3);
			fDesc = mImpost.GetField(4);
			
			fTotalSlabs = mImpost.GetField(5);
			fProcSlabs = mImpost.GetField(6);
			fHangSlabs = mImpost.GetField(7);
			fDeletedSlabs = mImpost.GetField(8);
			fLength = mImpost.GetField(9);
			fWidth = mImpost.GetField(10);
			fThickness = mImpost.GetField(11);
			fSurface = mImpost.GetField(12);

			lblMillimetriMU.Text = fWidth.GetMeasureUnit();
			lblSuperficieMU.Text = fSurface.GetMeasureUnit();
		}

		/// <summary>
		/// Resetta tutti i valori del campi Field
		/// </summary>
		private void ResetVal()
		{
			fBlockCode.EnterValue("");
			fCodeOp.EnterValue("");
			fBlSurface.EnterValue("");
			fBlOrigin.EnterValue("");
			fDesc.EnterValue("");

			fTotalSlabs.EnterValue("0");
			fProcSlabs.EnterValue("0");
			fHangSlabs.EnterValue("0");
			fDeletedSlabs.EnterValue("0");
			fLength.EnterValue("0");
			fWidth.EnterValue("0");
			fThickness.EnterValue("0");
			fSurface.EnterValue("0");

		}

		private void SetFieldValue()
		{
			fBlockCode.EnterValue(txtBlockCode.Text);
			fCodeOp.EnterValue(txtCodeOp.Text);
			fBlSurface.EnterValue(txtBlSurface.Text);
			fBlOrigin.EnterValue(txtBlOrigin.Text);
			fDesc.EnterValue(txtDesc.Text);

			fTotalSlabs.EnterValue(txtTotalSlabs.Text);
			fProcSlabs.EnterValue(txtProcSlabs.Text);
			fHangSlabs.EnterValue(txtHangSlabs.Text);
			fDeletedSlabs.EnterValue(txtDeletedSlabs.Text);
			fLength.EnterValue(txtLength.Text);
			fWidth.EnterValue(txtWidth.Text);
			fThickness.EnterValue(txtThickness.Text);
			fSurface.EnterValue(txtSurface.Text);
		}

		#endregion

		#region Attivazione Pad

		private void Attiva_Pad(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			PadT frmPadT;
			PadN frmPadN;
			TextBox txt = (TextBox)sender;
			switch (txt.Name)
			{

				case "txtBlockCode":
					frmPadT = new PadT(this, fBlockCode);
					break;
				case "txtCodeOp":
					frmPadT = new PadT(this, fCodeOp);
					break;
				case "txtBlSurface":
					frmPadT = new PadT(this, fBlSurface);
					break;
				case "txtBlOrigin":
					frmPadT = new PadT(this, fBlOrigin);
					break;
				case "txtDesc":
					frmPadT = new PadT(this, fDesc);
					break;

				case "txtTotalSlabs":
					frmPadN = new PadN(this, fTotalSlabs);
					fHangSlabs.DoubleValue = fTotalSlabs.DoubleValue  - fProcSlabs.DoubleValue ;
					//fHangSlabs.EnterValue(txtHangSlabs.Text)
					txtHangSlabs.Text = fHangSlabs.ToString();
					break;
				case "txtProcSlabs":
					frmPadN = new PadN(this, fProcSlabs);
					fHangSlabs.DoubleValue = fTotalSlabs.DoubleValue - fProcSlabs.DoubleValue;
					txtHangSlabs.Text = fHangSlabs.ToString();
					break;
				case "txtHangSlabs":
					frmPadN = new PadN(this, fHangSlabs);
					break;
				case "txtDeletedSlabs":
					frmPadN = new PadN(this, fDeletedSlabs);
					break;
				case "txtLength":
					frmPadN = new PadN(this, fLength);
					break;
				case "txtWidth":
					frmPadN = new PadN(this, fWidth);
					break;
				case "txtThickness":
					frmPadN = new PadN(this, fThickness);
					break;
				case "txtSurface":
					frmPadN = new PadN(this, fSurface);
					break;
			}
		}
		#endregion		

		#region InitPadValue

		private void GetAllFieldValue()
		{
			txtBlockCode_Validated(null, null);
			txtCodeOp_Validated(null, null);
			txtBlSurface_Validated(null, null);
			txtBlOrigin_Validated(null, null);
			txtDesc_Validated(null, null);
			txtTotalSlabs_Validated(null, null);
			txtProcSlabs_Validated(null, null);
			txtHangSlabs_Validated(null, null);
			txtDeletedSlabs_Validated(null, null);
			txtLength_Validated(null, null);
			txtWidth_Validated(null, null);
			txtThickness_Validated(null, null);
			txtSurface_Validated(null, null);
		}

		// Cattura tutti i testi e li inserisce nei rispettivi campi Field

		private void txtBlockCode_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fBlockCode.EnterValue(txtBlockCode.Text))
				txtBlockCode.Text = fBlockCode.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtBlockCode.Text = "";
			else
			{
				txtBlockCode.Focus();
				txtBlockCode.SelectAll();
			}
		}

		private void txtBlockCode_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtBlockCode_Validated(sender, null);
		}

		private void txtCodeOp_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fCodeOp.EnterValue(txtCodeOp.Text))
				txtCodeOp.Text = fCodeOp.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtCodeOp.Text = "";
			else
			{
				txtCodeOp.Focus();
				txtCodeOp.SelectAll();
			}
		}

		private void txtCodeOp_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtCodeOp_Validated(sender, null);
		}
	
		private void txtBlSurface_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fBlSurface.EnterValue(txtBlSurface.Text))
				txtBlSurface.Text = fBlSurface.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtBlSurface.Text = "";
			else
			{
				txtBlSurface.Focus();
				txtBlSurface.SelectAll();
			}
		}

		private void txtBlSurface_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtBlSurface_Validated(sender, null);
		}

		private void txtBlOrigin_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fBlOrigin.EnterValue(txtBlOrigin.Text))
				txtBlOrigin.Text = fBlOrigin.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtBlOrigin.Text = "";
			else
			{
				txtBlOrigin.Focus();
				txtBlOrigin.SelectAll();
			}
		}

		private void txtBlOrigin_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtBlOrigin_Validated(sender, null);
		}

		private void txtDesc_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fDesc.EnterValue(txtDesc.Text))
				txtDesc.Text = fDesc.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtDesc.Text = "";
			else
			{
				txtDesc.Focus();
				txtDesc.SelectAll();
			}
		}

		private void txtDesc_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtDesc_Validated(sender, null);
		}


		private void txtTotalSlabs_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fTotalSlabs.EnterValue(txtTotalSlabs.Text))
			{
				txtTotalSlabs.Text = fTotalSlabs.ToString();

				fHangSlabs.DoubleValue = fTotalSlabs.DoubleValue - fProcSlabs.DoubleValue;
				txtHangSlabs.Text = fHangSlabs.ToString();
			}
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtTotalSlabs.Text = "";
			else
			{
				txtTotalSlabs.Focus();
				txtTotalSlabs.SelectAll();
			}
		}

		private void txtTotalSlabs_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtTotalSlabs_Validated(sender, null);
		}

		private void txtProcSlabs_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fProcSlabs.EnterValue(txtProcSlabs.Text))
			{
				txtProcSlabs.Text = fProcSlabs.ToString();
				fHangSlabs.DoubleValue = fTotalSlabs.DoubleValue  - fProcSlabs.DoubleValue;
				txtHangSlabs.Text = fHangSlabs.ToString();
			}
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtProcSlabs.Text = "";
			else
			{
				txtProcSlabs.Focus();
				txtProcSlabs.SelectAll();
			}
		}

		private void txtProcSlabs_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtProcSlabs_Validated(sender, null);
		}

		private void txtHangSlabs_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fHangSlabs.EnterValue(txtHangSlabs.Text))
				txtHangSlabs.Text = fHangSlabs.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtHangSlabs.Text = "";
			else
			{
				txtHangSlabs.Focus();
				txtHangSlabs.SelectAll();
			}
		}

		private void txtHangSlabs_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtHangSlabs_Validated(sender, null);
		}

		private void txtDeletedSlabs_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fDeletedSlabs.EnterValue(txtDeletedSlabs.Text))
				txtDeletedSlabs.Text = fDeletedSlabs.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtDeletedSlabs.Text = "";
			else
			{
				txtDeletedSlabs.Focus();
				txtDeletedSlabs.SelectAll();
			}
		}

		private void txtDeletedSlabs_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtDeletedSlabs_Validated(sender, null);
		}

		private void txtLength_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fLength.EnterValue(txtLength.Text))
				txtLength.Text = fLength.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtLength.Text = "";
			else
			{
				txtLength.Focus();
				txtLength.SelectAll();
			}
		}

		private void txtLength_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtLength_Validated(sender, null);
		}
	
		private void txtWidth_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fWidth.EnterValue(txtWidth.Text))
				txtWidth.Text = fWidth.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtWidth.Text = "";
			else
			{
				txtWidth.Focus();
				txtWidth.SelectAll();
			}
		}

		private void txtWidth_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtWidth_Validated(sender, null);
		}

		private void txtThickness_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fThickness.EnterValue(txtThickness.Text))
				txtThickness.Text = fThickness.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtThickness.Text = "";
			else
			{
				txtThickness.Focus();
				txtThickness.SelectAll();
			}
		}

		private void txtThickness_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtThickness_Validated(sender, null);
		}

		private void txtSurface_Validated(object sender, System.EventArgs e)
		{
			string message, caption;
			ProjResource.gResource.LoadMessageBox(this, 7, out caption, out message);

			if (fSurface.EnterValue(txtSurface.Text))
				txtSurface.Text = fSurface.ToString();
			else if (MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Error) == DialogResult.OK)
				txtSurface.Text = "";
			else
			{
				txtSurface.Focus();
				txtSurface.SelectAll();
			}
		}

		private void txtSurface_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
		{
			if (e.KeyChar == (char)13)
				txtSurface_Validated(sender, null);
		}

		#endregion

		#region Gestione Magazzino
		private void GetStock(string huCode)
		{
			int level;
			string code, descr;

			Breton.Mgz.MgzUtility magUt = new Breton.Mgz.MgzUtility(mDbInterface);
			magUt.StockLocation(huCode, out level, out code, out descr);

			mMgzCoord = new Coordinate();
			SetCoordinate(level, code, descr);
		}

		private void SetCoordinate(int level, string code, string descr)
		{
			if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Site)
				mMgzCoord.gSite = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Area)
				mMgzCoord.gArea = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Shelf)
				mMgzCoord.gShelf = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Span)
				mMgzCoord.gSpan = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Level)
				mMgzCoord.gLevel = code;
			else
				TraceLog.WriteLine("LineBlocks.SetCoordinate: Position selected");

			mStockCode = code;
			mStockLevel = level;
			mStockDescr = descr;

			if ((code != null || descr != null) && (code.Length > 0 || descr.Length > 0))
				txtStockPosition.Text = code.Trim() + " - " + descr.Trim();

		}

		private string GetStockDescription(int level, string code)
		{
			Coordinate coord = new Coordinate();
			if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Site)
				coord.gSite = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Area)
				coord.gArea = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Shelf)
				coord.gShelf = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Span)
				coord.gSpan = code;
			else if (level == (int)Breton.MgzLevel.MgzLevelEnum.L_Level)
				coord.gLevel = code;
			else
				return "";

			Stock st = new Stock(mDbInterface);
			return st.GetCoordinateDescription(coord);
		}

		private void NodeSelected(object sender, TreeNode nd)
		{
			if (nd != null)
				SetCoordinate(nd.Level, nd.Tag.ToString(), nd.Text);
		}

		#endregion

		#region Gestione Eventi
		protected virtual void OnUpdate()
		{
			if (UpdateBlocksEvent != null)
				UpdateBlocksEvent(this, new EventArgs());
		}
		#endregion

		
	}

}