﻿using System;
using System.Collections.Generic;
using System.Text;
using Breton.DbAccess;

namespace Breton.LineBlocksSlabs
{
	public class LineBlock : DbObject
	{
		#region Variables

		//campi privati
		
		private int mId;					// Id del blocco						F_ID
		private string mCode;				// Codice blocco						F_CODE
		private string mDescription;		// Descrizione blocco					F_DESCRIPTION

		private string mMaterialCode;		//Codice materiale blocco				F_MATERIAL_CODE
		private string mSupplierCode;		//Codice fornitore da ImageMaster		F_SUPPLIER_CODE
		private string mStockCode;			//Codice magazzino da ImageMaster		F_STOCK_CODE
		private int mStockLevel;			//Livello del magazzino					F_STOCK_LEVEL
		private string mQualityCode;		//codice qualità blocco					F_QUALITY_CODE
		private double mSlabThickness;		//Spessore lastre blocco				F_SLAB_THICKNESS
		private int mTotalSlabs;			//Numero totale lastre del blocco		F_TOTAL_SLABS
		private int mProcSlabs;				//Numero lastre processate del bloccco	F_PROC_SLABS
		private int mHangSlabs;				//Numero lastre pendenti del blocco		F_HANG_SLABS
		private int mDeletedSlabs;			//Numero lastre Cancellate del blocco	F_DELETED_SLABS
		private double mLength;				//Lunghezza lastra						F_LENGTH
		private double mWidth;				//Larghezza lastra						F_WIDTH
		private double mThickness;			//Spessore blocco						F_THICKNESS
		private double mSurface;			//Superfice lastra						F_SURFACE
		private double mWeight;				//Peso lastra							F_WEIGHT
		private DateTime mDateProd;			//Data di produzione					F_DATE_PROD
		private String mCodeOp;			    //Codice operatore						F_CODE_OP
		private String mBlSurface;			//Superficie blocco						F_BL_SURFACE
		private String mBlOrigin;			//Origine blocco						F_BL_ORIGIN
		private DateTime mUpdDate;			//data aggiornamento record				F_UPD_DATE

		public BloccoStates mBlockState;			//Stato processo lavorazione blocco		F_BLOCK_STATES
		private bool mBlockStateChanged = false;

		//campi provenienti da altra tabella (doppia dichiarazione)


		//Chiavi dei campi
		public enum Keys
		{ F_NONE = -1, F_ID, F_CODE, F_DESCRIPTION, 
		  F_BLOCK_STATES, F_MATERIAL_CODE, F_SUPPLIER_CODE,  
		  F_STOCK_CODE, F_STOCK_LEVEL,  
		  F_QUALITY_CODE, F_SLAB_THICKNESS,
		  F_TOTAL_SLABS, F_PROC_SLABS, F_HANG_SLABS, F_DELETED_SLABS,
		  F_LENGTH, F_WIDTH, F_THICKNESS, F_SURFACE, 
		  F_DATE_PROD, F_CODE_OP, F_BL_SURFACE, F_BL_ORIGIN, F_UPD_DATE, F_WEIGHT,
		  F_MAX
		};

		public enum BloccoStates
		{
			UNDEF = -1,

			BLKST_UNDEF,		// block undefined
			BLKST_DEFINED,		// block defined
			BLKST_ACTIVATED,	// block activated
			BLKST_IN_PROGRESS,	// block in progress
			BLKST_SUSPENDED,	// block suspended
			BLKST_PROCESSED,	// block processed
			BLKST_ABORTED,		// block aborted
			MAX
		}

		#endregion

		#region Constructors

		public LineBlock(int id, string code, string description, BloccoStates BlockState, string MaterialCode, string SupplierCode,
					  string StockCode, int StockLevel, string QualityCode, double SlabThickness, int TotalSlabs,
					  int ProcSlabs, int HangSlabs, int DeletedSlabs, double Length, double Width, double Thickness,
					  double Surface, double Weight, DateTime DateProd, String CodeOp, String BlSurface, String BlOrigin, DateTime UpdDate)
		{
			mId = id;
			mCode = code;
            mDescription = description;

			mBlockState = BlockState;
			mMaterialCode = MaterialCode;
			mSupplierCode = SupplierCode;
			mStockCode = StockCode;
			mStockLevel = StockLevel;
			mQualityCode = QualityCode;
			mSlabThickness = SlabThickness;
			mTotalSlabs = TotalSlabs;
			mProcSlabs = ProcSlabs;
			mHangSlabs = HangSlabs;
			mDeletedSlabs = DeletedSlabs;
			mLength = Length;
			mWidth = Width;
			mThickness = Thickness;
			mSurface = Surface;
			mWeight = Weight;
			mDateProd = DateProd;
			mCodeOp = CodeOp;
			mBlSurface = BlSurface;
			mBlOrigin = BlOrigin;
			mUpdDate = UpdDate;
		}

		public LineBlock(): this(0, "", "", BloccoStates.UNDEF , "", "", "", -1, "", 0D, 0, 0, 0, 0, 0D, 0D, 0D, 0D, 0D, DateTime.Now, "", "", "", DateTime.Now)
		{
			//mId = 0;
			//mCode = "";
			//mDescription = "";
			//......
		}

		#endregion 

		#region Properties

		public int gId
		{
			set { mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set
			{
				mCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public string gDescription
		{
			set
			{
				mDescription = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}
		
		public BloccoStates gBlockState
		{
			set
			{
				mBlockState = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;

				mBlockStateChanged = true;
			}
			get { return mBlockState; }
		}

		public bool gBlockStateChanged
		{
			set { mBlockStateChanged = value; }
			get { return mBlockStateChanged; }
		}
		
		public string gMaterialCode
		{
			set
			{
				mMaterialCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mMaterialCode; }
		}

		public string gSupplierCode
		{
			set
			{
				mSupplierCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mSupplierCode; }
		}
		
		public string gStockCode
		{
			set
			{
				mStockCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mStockCode; }
		}

		public int gStockLevel
		{
			set
			{
				mStockLevel = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mStockLevel; }
		}

		public string gQualityCode
		{
			set
			{
				mQualityCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mQualityCode; }
		}

		public double gSlabThickness
		{
			set
			{
				mSlabThickness = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mSlabThickness; }
		}

		public int gTotalSlabs
		{
			set
			{
				mTotalSlabs = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mTotalSlabs; }
		}

		public int gProcSlabs
		{
			set
			{
				mProcSlabs = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mProcSlabs; }
		}

		public int gHangSlabs
		{
			set
			{
				mHangSlabs = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mHangSlabs; }
		}

		public int gDeletedSlabs
		{
			set
			{
				mDeletedSlabs = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDeletedSlabs; }
		}

		public double gLength
		{
			set
			{
				mLength = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mLength; }
		}
		
		public double gWidth
		{
			set
			{
				mWidth = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mWidth; }
		}

		public double gThickness
		{
			set
			{
				mThickness = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mThickness; }
		}
		
		public double gSurface
		{
			set
			{
				mSurface = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mSurface; }
		}

		public double gWeight
		{
			set
			{
				mWeight = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mWeight; }
		}

		public DateTime gDateProd
		{
			set
			{
				mDateProd = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDateProd; }
		}

		public String gCodeOp
		{
			set
			{
				mCodeOp = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mCodeOp; }
		}

		public String gBlSurface
		{
			set
			{
				mBlSurface = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mBlSurface; }
		}

		public String gBlOrigin
		{
			set
			{
				mBlOrigin = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mBlOrigin; }
		}

		public DateTime gUpdDate
		{
			set
			{
				mUpdDate = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mUpdDate; }
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is LineBlock)
			{
				LineBlock blc = (LineBlock)obj;

				return mCode.CompareTo(blc.gCode);
			}

			throw new ArgumentException("object is not Blocco");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == LineBlock.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_CODE.ToString() )
				return mCode.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_DESCRIPTION.ToString() )
				return mDescription.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_BLOCK_STATES.ToString() )
				return mBlockState.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_MATERIAL_CODE.ToString() )
				return mMaterialCode.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_SUPPLIER_CODE.ToString() )
				return mSupplierCode.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_STOCK_CODE.ToString() )
				return mStockCode.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_STOCK_LEVEL.ToString() )
				return mStockLevel.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_QUALITY_CODE.ToString() )
				return mQualityCode.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_SLAB_THICKNESS.ToString() )
				return mSlabThickness.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_TOTAL_SLABS.ToString() )
				return mTotalSlabs.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_PROC_SLABS.ToString() )
				return mProcSlabs.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_HANG_SLABS.ToString() )
				return mHangSlabs.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_DELETED_SLABS.ToString() )
				return mDeletedSlabs.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_LENGTH.ToString() )
				return mLength.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_WIDTH.ToString() )
				return mWidth.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_THICKNESS.ToString() )
				return mThickness.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_SURFACE.ToString() )
				return mSurface.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_WEIGHT.ToString() )
				return mWeight.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_DATE_PROD.ToString() )
				return mDateProd.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_CODE_OP.ToString() )
				return mCodeOp.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_BL_SURFACE.ToString() )
				return mBlSurface.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_BL_ORIGIN.ToString() )
				return mBlOrigin.GetTypeCode();
			if (key.ToUpper() == LineBlock.Keys.F_UPD_DATE.ToString() )
				return mUpdDate.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out DateTime retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}


		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == LineBlock.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_STOCK_LEVEL.ToString())
			{
				retValue = mStockLevel;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_TOTAL_SLABS.ToString())
			{
				retValue = mTotalSlabs;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_PROC_SLABS.ToString())
			{
				retValue = mProcSlabs;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_HANG_SLABS.ToString())
			{
				retValue = mHangSlabs;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_DELETED_SLABS.ToString())
			{
				retValue = mDeletedSlabs;
				return true;
			}
			
			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out double retValue)
		{
			if (key.ToUpper() == LineBlock.Keys.F_SLAB_THICKNESS.ToString())
			{
				retValue = mSlabThickness ;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_LENGTH.ToString())
			{
				retValue = mLength;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_WIDTH.ToString())
			{
				retValue = mWidth;
				return true;
			}
			if (key.ToUpper() == LineBlock.Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_SURFACE.ToString())
			{
				retValue = mSurface;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_WEIGHT.ToString())
			{
				retValue = mWeight;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == LineBlock.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_BLOCK_STATES.ToString())
			{
				retValue = mBlockState.ToString() ;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_MATERIAL_CODE.ToString())
			{
				retValue = mMaterialCode;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_SUPPLIER_CODE.ToString())
			{
				retValue = mSupplierCode;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_STOCK_CODE.ToString())
			{
				retValue = mStockCode;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_STOCK_LEVEL.ToString())
			{
				retValue = mStockLevel.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_QUALITY_CODE.ToString())
			{
				retValue = mQualityCode;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_SLAB_THICKNESS.ToString())
			{
				retValue = mSlabThickness.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_TOTAL_SLABS.ToString())
			{
				retValue = mTotalSlabs.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_PROC_SLABS.ToString())
			{
				retValue = mProcSlabs.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_HANG_SLABS.ToString())
			{
				retValue = mHangSlabs.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_DELETED_SLABS.ToString())
			{
				retValue = mDeletedSlabs.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_LENGTH.ToString())
			{
				retValue = mLength.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_WIDTH.ToString())
			{
				retValue = mWidth.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_SURFACE.ToString())
			{
				retValue = mSurface.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_DATE_PROD.ToString())
			{
				retValue = mDateProd.ToString();
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_CODE_OP.ToString())
			{
				retValue = mCodeOp;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_BL_SURFACE.ToString())
			{
				retValue = mBlSurface;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_BL_ORIGIN.ToString())
			{
				retValue = mBlOrigin;
				return true;
			}

			if (key.ToUpper() == LineBlock.Keys.F_UPD_DATE.ToString())
			{
				retValue = mUpdDate.ToString();
				return true;
			}


			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{

			return base.GetField(key, out retValue);
		}
		/// <summary>
		/// ottengo valore enum dalla stringa
		/// </summary>
		/// <param name="state"></param>
		/// <returns></returns>
		public static BloccoStates GetBlockStateEnum(string state)
		{
			if (state.Trim() == BloccoStates.BLKST_ABORTED.ToString())
				return BloccoStates.BLKST_ABORTED;
			if (state.Trim() == BloccoStates.BLKST_ACTIVATED.ToString())
				return BloccoStates.BLKST_ACTIVATED ;
			if (state.Trim() == BloccoStates.BLKST_DEFINED.ToString())
				return BloccoStates.BLKST_DEFINED;
			if (state.Trim() == BloccoStates.BLKST_IN_PROGRESS.ToString())
				return BloccoStates.BLKST_IN_PROGRESS;
			if (state.Trim() == BloccoStates.BLKST_PROCESSED.ToString())
				return BloccoStates.BLKST_PROCESSED;
			if (state.Trim() == BloccoStates.BLKST_SUSPENDED.ToString())
				return BloccoStates.BLKST_SUSPENDED;

			return BloccoStates.BLKST_UNDEF;

		}
		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo è valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}
		#endregion
	}

}


