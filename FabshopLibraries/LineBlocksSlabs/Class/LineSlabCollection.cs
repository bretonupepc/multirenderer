﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.OleDb;
using System.Text;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.LineBlocksSlabs
{
	public class LineSlabCollection : DbCollection
	{
		#region Constructors 
        public LineSlabCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out LineSlab obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);

			obj = (LineSlab) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public virtual bool GetObjectTable(int tableId, out LineSlab obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (LineSlab)mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public virtual bool AddObject(LineSlab obj)
		{
			return (base.AddObject((DbObject)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(LineSlab.Keys.F_NONE, -1, code, null, null, null, null, null, 0, 0);
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(LineSlab.Keys.F_NONE, id, null, null, null, null, null, null, 0, 0);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(LineSlab.Keys.F_NONE, -1, null, null, null, null, null, null, 0, 0);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			ids	: id dei materiali che si vuole estrarre
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineSlab.Keys orderKey, string blockCode)
		{
			return GetDataFromDb(orderKey, -1, null, null, blockCode, null, null, null, 0, 0);
		}

		public bool GetDataFromDb(LineSlab.Keys orderKey, string externalcode, string state)
		{
			return GetDataFromDb(orderKey, -1, null, state, null, null, null, null, 0, 0, externalcode);
		}

		public bool GetDataFromDb(LineSlab.Keys orderKey, int id, string code, string state, string blockCode, ArrayList codes, string qualityCode, string batchCode, double thick, double weight)
		{
			return GetDataFromDb(orderKey, id, code, state, blockCode, codes, qualityCode, batchCode, thick, weight, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		//			code		: estrae solo l'elemento con il codice specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineSlab.Keys orderKey, int id, string code, string state, string blockCode, ArrayList codes, string qualityCode, string batchCode, double thick, double weight, string externalcode)
		{
			return GetDataFromDb(orderKey, LineSlab.OrderMode.ASC, id, code, state, blockCode, codes, qualityCode, batchCode, thick, weight, externalcode);
		}


		public bool GetDataFromDb(LineSlab.Keys orderKey, LineSlab.OrderMode orderMode, int id, string code, string state, string blockCode, ArrayList codes, string qualityCode, string batchCode, double thick, double weight, string externalcode)
		{
			return GetDataFromDb(orderKey, orderMode, id, code, state, blockCode, codes, qualityCode, batchCode, thick, weight, externalcode, DateTime.MaxValue);
		}

		public bool GetDataFromDb(LineSlab.Keys orderKey, LineSlab.OrderMode orderMode, int id, string code, string state, string blockCode, ArrayList codes, string qualityCode, string batchCode, double thick, double weight, string externalcode, DateTime processedDate)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo l'oggetto con l'id richiesto
			if (id >= 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_ID = " + id.ToString();
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il codice richiesto
			if (code != null)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_CODE = '" + DbInterface.QueryString(code) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo le lastre con lo stato richiesto
			if (state != null)
			{
				sqlWhere += sqlAnd + "T_CO_SLAB_STATES.F_CODE= '" + DbInterface.QueryString(state) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo le lastre del blocco richiesto
			if (blockCode != null)
			{
				sqlWhere += sqlAnd + "T_WK_LINE_BLOCKS.F_CODE= '" + DbInterface.QueryString(blockCode) + "'";
				sqlAnd = " AND ";
			}

			// Estrare solo le lastre con i codici richiesti
			if (codes != null && codes.Count > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_CODE in ('" + codes[0].ToString() + "'";
				for (int i = 1; i < codes.Count; i++)
					sqlWhere += ", '" + codes[i].ToString() + "'";

				sqlWhere += ")";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con la qualità richiesta
			if (qualityCode != null && qualityCode.Length > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_QUALITY_CODE = '" + DbInterface.QueryString(qualityCode) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il batch code richiesto
			if (batchCode != null && batchCode.Length > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_BATCH_CODE = '" + DbInterface.QueryString(batchCode) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con lo spessore richiesto
			if (thick > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_THICKNESS = '" + thick.ToString() + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il peso richiesto
			if (weight > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_WEIGHT = '" + weight.ToString() + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il codice richiesto
			if (externalcode != null && externalcode.Length > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_EXTERNAL_CODE = '" + DbInterface.QueryString(externalcode) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il codice richiesto
			if (processedDate != DateTime.MaxValue)
			{
				sqlWhere += sqlAnd + "YEAR(T_WK_SLABS.F_PROCESSED_DATE) = " + processedDate.Year.ToString() + 
								" AND MONTH(T_WK_SLABS.F_PROCESSED_DATE) = " + processedDate.Month.ToString() + 
								" AND DAY(T_WK_SLABS.F_PROCESSED_DATE) =  " + processedDate.Day.ToString();
				
				sqlAnd = " and ";
			}

			switch (orderKey)
			{
				case LineSlab.Keys.F_NONE:
					break;
				case LineSlab.Keys.F_ID:
					sqlOrderBy = " order by T_WK_SLABS.F_ID ";
					break;
				case LineSlab.Keys.F_CODE:
					sqlOrderBy = " order by T_WK_SLABS.F_CODE ";
					break;
				case LineSlab.Keys.F_PRIORITY:
					sqlOrderBy = " order by T_WK_SLABS.F_PRIORITY ";
					break;
				case LineSlab.Keys.F_SLAB_STATE:
					sqlOrderBy = " order by T_CO_SLAB_STATES.F_CODE ";
					break;
				case LineSlab.Keys.F_BLOCK_CODE:
					sqlOrderBy = " order by T_WK_LINE_BLOCKS.F_CODE ";
					break;
			}

			switch (orderMode)
			{
				// Non fa niente
				case LineSlab.OrderMode.ASC:
				case LineSlab.OrderMode.NONE:
					sqlOrderBy += "";
					break;
				// Inverte l'ordinamento
				case LineSlab.OrderMode.DESC:
					sqlOrderBy += " desc";
					break;
			}

			return GetDataFromDb(" select T_WK_SLABS.*, T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE, T_CO_SLAB_STATES.F_CODE as F_SLAB_STATE" +
								 " from T_WK_SLABS " +
								 " inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
								 " inner join T_CO_SLAB_STATES on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = T_CO_SLAB_STATES.F_ID " +
								 sqlWhere + sqlOrderBy);

		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineSlab.Keys orderKey)
		{
			return GetDataFromDb(orderKey, -1, null, null, null, null, null, null, 0, 0);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			return UpdateDataToDb("");
		}
		public bool UpdateDataToDb(string message)
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova più volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					//Scorre tutti gli oggetti caricati nella collection
					foreach (LineSlab lslb in mRecordset)
					{
						switch (lslb.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_WK_SLABS (" +
													(lslb.gCode.Trim() == "" ? "" : " F_CODE, ") +
													" F_PRIORITY, " +
													" F_ID_T_WK_LINE_BLOCKS, " +
													" F_ID_T_CO_SLAB_STATES, " +
													" F_QUALITY_CODE, " +
													" F_BATCH_CODE, " +
													" F_WIDTH, " +
													" F_LENGTH, " +
													" F_THICKNESS, " +
													" F_WEIGHT, " +
													" F_EXTERNAL_CODE)" +
											" (select " + (lslb.gCode.Trim() == "" ? "" : "'" + DbInterface.QueryString(lslb.gCode) + "', ")
														 + lslb.gPriority.ToString() + ", " +
														 "T_WK_LINE_BLOCKS.F_ID, " +
														 "T_CO_SLAB_STATES.F_ID, " +
														 "'" + DbInterface.QueryString(lslb.gQualityCode.Trim()) + "', " +
														 "'" + DbInterface.QueryString(lslb.gBatchCode.Trim()) + "', " +
														 lslb.gWidth.ToString() + ", " +
														 lslb.gLength.ToString() + ", " +
														 lslb.gThickness.ToString() + ", " +
														 lslb.gWeight.ToString() + ", " +
														 "'" + DbInterface.QueryString(lslb.gExternalCode.Trim()) + "' " + 
										    " from T_WK_LINE_BLOCKS, T_CO_SLAB_STATES " +
											" where T_WK_LINE_BLOCKS.F_CODE = '" + lslb.gLineBlockCode.Trim() + "' and" +
													" T_CO_SLAB_STATES.F_CODE = '" + lslb.gLineSlabState.Trim() + "'); " +
											" SELECT SCOPE_IDENTITY()";

								if (message != null && message != "")
									sqlInsert += "\n--" + message;
								
								if (!mDbInterface.Execute(sqlInsert, out id))
									throw new ApplicationException(this.ToString() + ".UpdateDataToDb Error.");

								lslb.gId = id;

								break;

							//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_WK_SLABS set F_CODE = '" + DbInterface.QueryString(lslb.gCode) + "', " +
																" F_PRIORITY = " + lslb.gPriority.ToString() +
																", F_QUALITY_CODE = '" + DbInterface.QueryString(lslb.gQualityCode.Trim()) + "'" +
																", F_BATCH_CODE = '" + DbInterface.QueryString(lslb.gBatchCode.Trim()) + "'" +
																", F_WIDTH = " + lslb.gWidth.ToString() +
																", F_LENGTH = " + lslb.gLength.ToString() +
																", F_THICKNESS = " + lslb.gThickness.ToString() +
																", F_WEIGHT = " + lslb.gWeight.ToString() +
																", F_EXTERNAL_CODE = '" + DbInterface.QueryString(lslb.gExternalCode) + "'" +
																", F_ID_T_WK_LINE_BLOCKS = " +
																		" (select F_ID from T_WK_LINE_BLOCKS" +
																		" where F_CODE = '" + DbInterface.QueryString(lslb.gLineBlockCode) + "')" +
																", F_ID_T_CO_SLAB_STATES = " +
																		" (select F_ID from T_CO_SLAB_STATES" +
																		" where F_CODE = '" + DbInterface.QueryString(lslb.gLineSlabState) + "')";

								sqlUpdate += " where F_ID = " + lslb.gId.ToString();
								if (message != null && message != "")
									sqlUpdate += "\n--" + message;


								if (!mDbInterface.Execute(sqlUpdate))
									throw new ApplicationException(this.ToString() +  "UpdateDataToDb Error.");

								break;
						}
					}

					//Scorre tutti gli oggetti cancellati
					foreach (LineSlab lslb in mDeleted)
					{
						switch (lslb.gState)
						{
							//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_WK_SLABS where F_ID = " + lslb.gId.ToString();

								if (!mDbInterface.Execute(sqlDelete))
									throw new ApplicationException(this.ToString() + ".UpdateDataToDb Error.");
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}
				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine(this.ToString() +": Deadlock Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException(this.ToString() + ".UpdateDataToDb: Exceed Deadlock retrying", ex);
				}
				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine(this.ToString() + ": Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

		public override void SortByField(string key)
		{
			if (key == LineSlab.Keys.F_PRIORITY.ToString())
				SortByPriority();
		}

		/// <summary>
		/// Ricarica i dati con l'ultima query eseguita
		/// </summary>
		/// <returns>
		///		true	lettura eseguita
		///		false	lettura non eseguita
		/// </returns>
		public bool RefreshData()
		{
			return GetDataFromDb(mSqlGetData);
		}

		/// <summary>
		/// Verifica se esiste una lastra associata al blocco e in caso ne restituisce il codice 
		/// </summary>
		/// <param name="blockCode">codice del blocco interessato</param>
		/// <returns></returns>
		public static string GetFirstAvailableSlab(DbInterface db, string blockCode)
		{
			OleDbDataReader dr = null;
			string sqlQuery;
			string retVal = null;

			try
			{
				sqlQuery = "select top 1 T_WK_SLABS.F_CODE " +
							"from T_WK_SLABS " +
							"inner join T_CO_SLAB_STATES on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = T_CO_SLAB_STATES.F_ID  " +
							"inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
							"where T_WK_LINE_BLOCKS.F_CODE = '" + blockCode.Trim() + "' and " +
									" T_CO_SLAB_STATES.F_CODE = '" + LineSlab.SlabStates.SLAB_INSERTED + "' " +
							"order by  F_PRIORITY";

				if (!db.Requery(sqlQuery, out dr))
					throw new ApplicationException("Error in query");

				while (dr.Read())
				{
					retVal = dr["F_CODE"].ToString();
				}

				return retVal;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("LineSlabCollection.GetFirstAvailableSlab: Error", ex);

				return null;
			}

			finally
			{
				db.EndRequery(dr);
			}
		}

		public virtual bool GetFirstAvailableSlab()
		{
			return GetFirstAvailableSlab(1);
		}

		public virtual bool GetFirstAvailableSlab(int maxSlabs)
		{
			string top = (maxSlabs > 0 ? "top " + maxSlabs.ToString() : "");

			return GetDataFromDb("select " + top + " T_WK_SLABS.*, T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE, T_CO_SLAB_STATES.F_CODE as F_SLAB_STATE " +
						"from T_WK_SLABS " +
						"inner join T_CO_SLAB_STATES on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = T_CO_SLAB_STATES.F_ID  " +
						"inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
						"where T_CO_SLAB_STATES.F_CODE = '" + LineSlab.SlabStates.SLAB_INSERTED + "' " +
						"order by F_PRIORITY");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="blockCode"></param>
		/// <returns></returns>
		public string[] GetFirst3AvailableSlab(string blockCode)
		{
			OleDbDataReader dr = null;
			string sqlQuery;
			string[] retVal = new string[3];
			int i = 0;

			try
			{
				sqlQuery = "select top 3 T_WK_SLABS.F_CODE " +
							"from T_WK_SLABS " +
							"inner join T_CO_SLAB_STATES on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = T_CO_SLAB_STATES.F_ID  " +
							"inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
							"where T_WK_LINE_BLOCKS.F_CODE = '" + blockCode.Trim() + "' and " +
									" T_CO_SLAB_STATES.F_CODE = '" + LineSlab.SlabStates.SLAB_INSERTED + "' " +
							"order by  F_PRIORITY";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					throw new ApplicationException("Error in query");

				while (dr.Read())
				{
					retVal[i] = dr["F_CODE"].ToString();
					i++;
				}

				return retVal;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".GetFirst3AvailableSlab: Error", ex);

				return null;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		public string[] GetFirst3AvailableSlab()
		{
			OleDbDataReader dr = null;
			string sqlQuery;
			string[] retVal = new string[3];
			int i = 0;

			try
			{
				sqlQuery = "select top 3 T_WK_SLABS.F_CODE " +
							"from T_WK_SLABS " +
							"inner join T_CO_SLAB_STATES on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = T_CO_SLAB_STATES.F_ID  " +
							"where T_CO_SLAB_STATES.F_CODE = '" + LineSlab.SlabStates.SLAB_INSERTED + "' " +
							"order by F_PRIORITY";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					throw new ApplicationException("Error in query");

				while (dr.Read())
				{
					retVal[i] = dr["F_CODE"].ToString();
					i++;
				}

				return retVal;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".GetFirst3AvailableSlab: Error", ex);

				return null;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/// <summary>
		/// Restituisce l'indice di priorità massimo del blocco specifico
		/// </summary>
		/// <param name="blockCode"></param>
		/// <returns></returns>
		public int MaxPriority(string blockCode)
		{
			OleDbDataReader dr = null;
			string sqlQuery;
			int retVal = -1;

			try
			{
				sqlQuery = "select MAX(F_PRIORITY) " +
							"from T_WK_SLABS " +
							"inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
							"where T_WK_LINE_BLOCKS.F_CODE = '" + blockCode.Trim();

				if (!mDbInterface.Requery(sqlQuery, out dr))
					throw new ApplicationException("Error in query");

				while (dr.Read())
				{
					retVal = (int)dr[0];
				}

				return retVal;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".MaxPriority: Error", ex);

				return -1;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/// <summary>
		/// Restituisce l'indice di priorità massimo
		/// </summary>
		/// <returns></returns>
		public int MaxPriority()
		{
			OleDbDataReader dr = null;
			string sqlQuery;
			int retVal = -1;

			try
			{
				sqlQuery = "select MAX(F_PRIORITY) " +
							"from T_WK_SLABS";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					throw new ApplicationException("Error in query");

				while (dr.Read())
				{
					retVal = (dr[0] == DBNull.Value ? 0 : (int)dr[0]);
				}

				return retVal;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".MaxPriority: Error", ex);

				return -1;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}
		#endregion

		#region Private Methods

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			LineSlab las;

			Clear();

			// Verifica se la query è valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					las = new LineSlab((int)dr["F_ID"], dr["F_CODE"].ToString(), (int)dr["F_PRIORITY"],
						dr["F_BLOCK_CODE"].ToString(), dr["F_SLAB_STATE"].ToString(), dr["F_QUALITY_CODE"].ToString(),
						dr["F_BATCH_CODE"].ToString(), (double)dr["F_WIDTH"], (double)dr["F_LENGTH"], (double)dr["F_THICKNESS"],
						(double)dr["F_WEIGHT"], dr["F_EXTERNAL_CODE"].ToString(), (DateTime)dr["F_INSERT_DATE"], 
						(dr["F_PROCESSED_DATE"] != DBNull.Value ? (DateTime)dr["F_PROCESSED_DATE"] : DateTime.MaxValue));

					AddObject(las);
					las.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".GetDataFromDb: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		/// <summary>
		/// Effettua l'ordinamento per priorità
		/// </summary>
		private void SortByPriority()
		{
			LineSlab tmp;

			// Bubble Sort
			for (int i = 0; i < mRecordset.Count - 1; i++)
			{
				for (int j = i + 1; j < mRecordset.Count; j++)
				{
					if (((LineSlab)mRecordset[i]).gPriority > ((LineSlab)mRecordset[j]).gPriority)
					{
						tmp = (LineSlab)mRecordset[i];
						mRecordset[i] = mRecordset[j];
						mRecordset[j] = tmp;
					}
				}
			}
		}

		#endregion
	}
}
