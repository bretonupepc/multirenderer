﻿using System;
using System.Collections.Generic;
using System.Text;
using Breton.DbAccess;

namespace Breton.LineBlocksSlabs
{
	public class LineList : DbObject
	{
		#region Variables

		//campi privati
		
		private int mId;					// Id del blocco						F_ID
		private string mCode;				// Codice blocco						F_CODE
		private string mDescription;		// Descrizione blocco					F_DESCRIPTION

		//campi provenienti da altra tabella (doppia dichiarazione)


		//Chiavi dei campi
		public enum Keys
		{ F_NONE = -1, F_ID, F_CODE, F_DESCRIPTION, 
		  F_MAX
		};

		#endregion

		#region Constructors
		public LineList(int id, string code, string description)
		{
			mId = id;
			mCode = code;
            mDescription = description;
		}


		public LineList(): this(0, "", "")
		{
			//mId = 0;
			//mCode = "";
			//mDescription = "";
			//......
		}
		#endregion 

		#region Properties

		public int gId
		{
			set { mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set
			{
				mCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public string gDescription
		{
			set
			{
				mDescription = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is LineList)
			{
				LineList blc = (LineList)obj;

				return mCode.CompareTo(blc.gCode);
			}

			throw new ArgumentException("object is not Blocco");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == LineList.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == LineList.Keys.F_CODE.ToString() )
				return mCode.GetTypeCode();
			if (key.ToUpper() == LineList.Keys.F_DESCRIPTION.ToString() )
				return mDescription.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out DateTime retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}


		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == LineList.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out double retValue)
		{

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == LineList.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == LineList.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == LineList.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{

			return base.GetField(key, out retValue);
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo è valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}
		#endregion
	}

}


