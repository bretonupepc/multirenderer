﻿using System;
using System.Collections.Generic;
using System.Text;
using Breton.DbAccess;

namespace Breton.LineBlocksSlabs
{
	public class LineSlab : DbObject
	{
		#region Variables

		protected int mId;						// Id della lastra						
		protected string mCode;					// Codice lastra						
		protected int mPriority;				// Priorità o ordine della lastra
		protected string mQualityCode;			// Qualità della lastra
		protected string mBatchCode;			// Batch code della lastra
		protected double mWidth;				// Spessore della lastra
		protected double mLength;				// Spessore della lastra
		protected double mThickness;			// Spessore della lastra
		protected double mWeight;				// Peso della lastra
		protected string mExternalCode;			// Codice esterno
		protected DateTime mInsertDate;			// Data di inserimento
		protected DateTime mProcessedDate;		// Data di elaborazione

		protected string mLineBlockCode;		// Blocco a cui è associata
		protected string mLineSlabState;		// Stato della lastra



		//Chiavi dei campi
		public enum Keys
		{	F_NONE = -1, F_ID, F_CODE, F_PRIORITY, F_BLOCK_CODE, F_SLAB_STATE, F_QUALITY_CODE, F_BATCH_CODE, F_WIDTH, F_LENGTH, F_THICKNESS, F_WEIGHT, F_EXTERNAL_CODE, F_INSERT_DATE, F_PROCESSED_DATE, F_MAX }

		public enum SlabStates
		{
			SLAB_INSERTED,		
			SLAB_PROCESSED	
		}

		// Tipologia di ordinamento
		public enum OrderMode
		{
			NONE,
			ASC,
			DESC
		}
		#endregion

		#region Constructors
		public LineSlab()
			: this(0, "", -1, "", "", "", "", 0, 0, 0, 0, "")
		{
		}

		public LineSlab(int id, string code, int priority, string lineblockCode, string lineslabState, 
			string qualityCode, string batchCode, double width, double length, double thickness, double weight, 
			string externalcode) : this(id, code, priority, lineblockCode, lineslabState,qualityCode, batchCode, width, length, thickness, weight, externalcode, DateTime.MaxValue, DateTime.MaxValue)
		{ 
			
		}

		public LineSlab(int id, string code, int priority, string lineblockCode, string lineslabState, 
			string qualityCode, string batchCode, double width, double length, double thickness, double weight, 
			string externalcode, DateTime insertDate, DateTime processedDate)
		{
			mId = id;
			mCode = code;
			mPriority = priority;
			mLineBlockCode = lineblockCode;
			mLineSlabState = lineslabState;
			mQualityCode = qualityCode;
			mBatchCode = batchCode;
			mWidth = width;
			mLength = length;
			mThickness = thickness;
			mWeight = weight;
			mExternalCode = externalcode;
			mInsertDate = insertDate;
			mProcessedDate = processedDate;
		}

		
		#endregion 

		#region Properties
		public int gId
		{
			set { mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set
			{
				mCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public int gPriority
		{
			set
			{
				mPriority = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mPriority; }
		}

		public string gLineBlockCode
		{
			set
			{
				mLineBlockCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mLineBlockCode; }
		}

		public string gLineSlabState
		{
			set
			{
				mLineSlabState = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mLineSlabState; }
		}

		public string gQualityCode
		{
			get { return mQualityCode; }
			set
			{ 
				mQualityCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}

		public string gBatchCode
		{
			get { return mBatchCode; }
			set 
			{
				mBatchCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}

		public string gExternalCode
		{
			get { return mExternalCode; }
			set
			{ 
				mExternalCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}

		public double gWidth
		{
			get { return mWidth; }
			set
			{
				mWidth = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}
		
		public double gLength
		{
			get { return mLength; }
			set 
			{ 
				mLength = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}

		public double gThickness
		{
			get { return mThickness; }
			set 
			{ 
				mThickness = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}

		public double gWeight
		{
			get { return mWeight; }
			set 
			{ 
				mWeight = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}

		public DateTime gInsertDate
		{
			set { mInsertDate = value; }
			get { return mInsertDate; }
		}

		public DateTime gProcessedDate
		{
			set { mProcessedDate = value; }
			get { return mProcessedDate; }
		}
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is LineSlab)
			{
				LineSlab  lst = (LineSlab)obj;

				return mCode.CompareTo(lst.gCode);
			}

			throw new ArgumentException("object is not LineSlab");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == LineSlab.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_PRIORITY.ToString())
				return mPriority.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_SLAB_STATE.ToString())
				return mLineSlabState.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_BLOCK_CODE.ToString())
				return mLineBlockCode.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_QUALITY_CODE.ToString())
				return mQualityCode.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_BATCH_CODE.ToString())
				return mBatchCode.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_WIDTH.ToString())
				return mWidth.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_LENGTH.ToString())
				return mLength.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_THICKNESS.ToString())
				return mThickness.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_WEIGHT.ToString())
				return mWeight.GetTypeCode();
			if (key.ToUpper() == LineSlab.Keys.F_EXTERNAL_CODE.ToString())
				return mExternalCode.GetTypeCode();
			
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out DateTime retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}


		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}
			
			if (key.ToUpper() == Keys.F_PRIORITY.ToString())
			{
				retValue = mPriority;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out double retValue)
		{
			if (key.ToUpper() == Keys.F_WIDTH.ToString())
			{
				retValue = mWidth;
				return true;
			}

			if (key.ToUpper() == Keys.F_LENGTH.ToString())
			{
				retValue = mLength;
				return true;
			}

			if (key.ToUpper() == Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness;
				return true;
			}

			if (key.ToUpper() == Keys.F_WEIGHT.ToString())
			{
				retValue = mWeight;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == Keys.F_PRIORITY.ToString())
			{
				retValue = mPriority.ToString();
				return true;
			}

			if (key.ToUpper() == Keys.F_BLOCK_CODE.ToString())
			{
				retValue = mLineBlockCode;
				return true;
			}
			
			if (key.ToUpper() == Keys.F_SLAB_STATE.ToString())
			{
				retValue = mLineSlabState;
				return true;
			}

			if (key.ToUpper() == Keys.F_QUALITY_CODE.ToString())
			{
				retValue = mQualityCode;
				return true;
			}

			if (key.ToUpper() == Keys.F_BATCH_CODE.ToString())
			{
				retValue = mBatchCode;
				return true;
			}

			if (key.ToUpper() == Keys.F_EXTERNAL_CODE.ToString())
			{
				retValue = mExternalCode;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo è valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index >= (int)Keys.F_NONE && index <= (int)Keys.F_MAX);
		}
		#endregion
	}
}
