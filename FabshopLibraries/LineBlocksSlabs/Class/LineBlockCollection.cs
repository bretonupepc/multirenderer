﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.OleDb;
using System.Text;
using Breton.DbAccess;
using Breton.TraceLoggers;
using System.Globalization;

namespace Breton.LineBlocksSlabs
{
	public class LineBlockCollection : DbCollection
	{
		#region Constructors
		public LineBlockCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods

		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out LineBlock obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);

			obj = (LineBlock)dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out LineBlock obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (LineBlock)mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(LineBlock obj)
		{
			return (base.AddObject((DbObject)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(LineBlock.Keys.F_NONE, -1, code, null, null);
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(LineBlock.Keys.F_NONE, id, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(LineBlock.Keys.F_NONE, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineBlock.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			ids	: id dei materiali che si vuole estrarre
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineBlock.Keys orderKey, ArrayList codes)
		{
			return GetDataFromDb(orderKey, null, null, codes);
		}

		public bool GetDataFromDb(LineBlock.Keys orderKey, string code, string state)
		{
			return GetDataFromDb(orderKey, code, state, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		//			code		: estrae solo l'elemento con il codice specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineBlock.Keys orderKey, string code, string state, ArrayList codes)
		{
			ArrayList states = new ArrayList();
			states.Add(state);

			return GetDataFromDb(orderKey, -1, code, states, codes);
		}

		public bool GetDataFromDb(LineBlock.Keys orderKey, int id, string code, ArrayList states, ArrayList codes)
		{
			return GetDataFromDb(orderKey, id, code, states, codes, null, -1);
		}

		public bool GetDataFromDb(LineBlock.Keys orderKey, int id, string code, ArrayList states, ArrayList codes, string material, double thickness)
		{
			// VEDI_AUTOSIZE 
			// da definire

			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo l'oggetto con l'id richiesto
			if (id >= 0)
			{
				sqlWhere += sqlAnd + "T_WK_LINE_BLOCKS.F_ID = " + id.ToString();
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il codice richiesto
			if (code != null)
			{
				sqlWhere += sqlAnd + "T_WK_LINE_BLOCKS.F_CODE = '" + DbInterface.QueryString(code) + "'";
				sqlAnd = " and ";
			}


			// Estrare solo i Blocchi con i codici richiesti
			if (states != null)
			{
				if (states.Count > 0)
				{
					sqlWhere += sqlAnd + "T_CO_BLOCK_STATES.F_CODE in ('" + states[0];
					for (int i = 1; i < states.Count; i++)
						sqlWhere += "', '" + states[i];
					sqlWhere += "')";
				}
			}

			//Estrae solo l'oggetto del materiale richiesto
			if (material != null)
			{
				sqlWhere += sqlAnd + "T_WK_LINE_BLOCKS.F_MATERIAL_CODE = '" + DbInterface.QueryString(material) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto del materiale richiesto
			if (thickness > 0)
			{
				sqlWhere += sqlAnd + "T_WK_LINE_BLOCKS.F_THICKNESS = " + thickness.ToString(CultureInfo.InvariantCulture);
				sqlAnd = " and ";
			}


			switch (orderKey)
			{
				case LineBlock.Keys.F_NONE:
					break;
				case LineBlock.Keys.F_ID:
					sqlOrderBy = " order by F_ID ";
					break;
				case LineBlock.Keys.F_CODE:
					sqlOrderBy = " order by F_CODE ";
					break;
				case LineBlock.Keys.F_DESCRIPTION:
					sqlOrderBy = " order by F_DESCRIPTION ";
					break;
			}
			return GetDataFromDb("select T_WK_LINE_BLOCKS.*, T_CO_BLOCK_STATES.F_CODE as F_BLOCK_STATE " +
								 "from T_WK_LINE_BLOCKS " + 
								 "inner join T_CO_BLOCK_STATES ON T_WK_LINE_BLOCKS.F_ID_T_CO_BLOCK_STATES = T_CO_BLOCK_STATES.F_ID "+
								 sqlWhere + sqlOrderBy);

		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			// VEDI_AUTOSIZE 
			// da definire 

			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova più volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();
					
					//Scorre tutti gli oggetti caricati nella collection
					foreach (LineBlock blc in mRecordset)
					{
						switch (blc.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:

								sqlInsert = "INSERT INTO T_WK_LINE_BLOCKS (" ;
								sqlInsert += " F_MATERIAL_CODE, F_SUPPLIER_CODE, F_STOCK_CODE, F_STOCK_LEVEL,";
								sqlInsert += " F_QUALITY_CODE, F_SLAB_THICKNESS, F_TOTAL_SLABS, F_PROC_SLABS, F_HANG_SLABS, F_DELETED_SLABS,";
								sqlInsert += " F_LENGTH, F_WIDTH, F_THICKNESS, F_SURFACE, F_DATE_PROD, F_CODE_OP, F_BL_SURFACE,";
								sqlInsert += " F_BL_ORIGIN, F_UPD_DATE, F_DESCRIPTION, F_WEIGHT, ";
								sqlInsert += " F_ID_T_CO_BLOCK_STATES ";
								sqlInsert += " )";

								string Code = (blc.gCode == null ? "NULL" : "'" + DbInterface.QueryString(blc.gCode) + "'");
								string MaterialCode = (blc.gMaterialCode == null ? "NULL" : "'" + DbInterface.QueryString(blc.gMaterialCode) + "'");
								string SupplierCode = (blc.gSupplierCode == null ? "NULL" : "'" + DbInterface.QueryString(blc.gSupplierCode) + "'");
								string StockCode = (blc.gStockCode == null ? "NULL" : "'" + DbInterface.QueryString(blc.gStockCode) + "'");
								string StockLevel = (blc.gStockLevel == null ? "NULL" : blc.gStockLevel.ToString());
								string QualityCode = (blc.gQualityCode == null ? "NULL" : "'" + DbInterface.QueryString(blc.gQualityCode) + "'");
								string SlabThickness = (blc.gSlabThickness == null ? "NULL" : blc.gSlabThickness.ToString(CultureInfo.InvariantCulture));
								string TotalSlabs = (blc.gTotalSlabs == null ? "NULL" : blc.gTotalSlabs.ToString());
								string ProcSlabs = (blc.gProcSlabs == null ? "NULL" : blc.gProcSlabs.ToString());
								string HangSlabs = (blc.gHangSlabs == null ? "NULL" : blc.gHangSlabs.ToString());
								string DeletedSlabs = (blc.gDeletedSlabs == null ? "NULL" : blc.gDeletedSlabs.ToString());
								string Length = (blc.gLength == null ? "NULL" : blc.gLength.ToString(CultureInfo.InvariantCulture));
								string Width = (blc.gWidth == null ? "NULL" : blc.gWidth.ToString(CultureInfo.InvariantCulture));
								string Thickness = (blc.gThickness == null ? "NULL" : blc.gThickness.ToString(CultureInfo.InvariantCulture));
								string Surface = (blc.gSurface == null ? "NULL" : blc.gSurface.ToString(CultureInfo.InvariantCulture));
								string DateProd = mDbInterface.OleDbToDate(blc.gDateProd);
								string CodeOp = (blc.gCodeOp == null ? "NULL" : "'" + DbInterface.QueryString(blc.gCodeOp) + "'");
								string BlSurface = (blc.gBlSurface == null ? "NULL" : "'" + DbInterface.QueryString(blc.gBlSurface) + "'");
								string BlOrigin = (blc.gBlOrigin == null ? "NULL" : "'" + DbInterface.QueryString(blc.gBlOrigin) + "'");
								string UpdDate = mDbInterface.OleDbToDate(blc.gUpdDate);
								string Description = (blc.gDescription == null ? "NULL" : "'" + DbInterface.QueryString(blc.gDescription) + "'");
								string BlockState = (blc.gBlockState == LineBlock.BloccoStates.UNDEF ? "'BLKST_UNDEF'" : "'" + DbInterface.QueryString(blc.gBlockState.ToString()) + "'");
								string weight = blc.gSurface.ToString(CultureInfo.InvariantCulture);

								sqlInsert += "( SELECT ";
								//sqlInsert += Code + ",";
								sqlInsert += MaterialCode + "," + SupplierCode + "," + StockCode + "," + StockLevel + ",";
								sqlInsert += QualityCode + "," + SlabThickness + "," + TotalSlabs + "," + ProcSlabs + "," + HangSlabs + "," + DeletedSlabs + ",";
								sqlInsert += Length + "," + Width + "," + Thickness + "," + Surface + "," + DateProd + "," + CodeOp + "," + BlSurface + ",";
								sqlInsert += BlOrigin + "," + UpdDate + "," + Description + "," + weight + ",";

								sqlInsert += " F_ID FROM T_CO_BLOCK_STATES WHERE F_CODE = " + BlockState ;

								sqlInsert += " );select scope_identity()";

								if (!mDbInterface.Execute(sqlInsert, out id))
									throw new ApplicationException("LineBlockCollection.UpdateDataToDb Error.");

								blc.gId = id;

								break;

							//Aggiornamento
							case DbObject.ObjectStates.Updated:

								sqlUpdate += "UPDATE T_WK_LINE_BLOCKS SET ";
								sqlUpdate += " F_CODE = '" + DbInterface.QueryString(blc.gCode) + "'";
								sqlUpdate += " ,F_MATERIAL_CODE = '" + DbInterface.QueryString(blc.gMaterialCode) + "'";
								sqlUpdate += " ,F_SUPPLIER_CODE = '" + DbInterface.QueryString(blc.gSupplierCode) + "'";
								sqlUpdate += " ,F_STOCK_CODE  = '" + DbInterface.QueryString(blc.gStockCode) + "'";
								sqlUpdate += " ,F_STOCK_LEVEL  = " + blc.gStockLevel.ToString();
								sqlUpdate += " ,F_QUALITY_CODE = '" + DbInterface.QueryString(blc.gQualityCode) + "'";
								sqlUpdate += " ,F_SLAB_THICKNESS = " + blc.gSlabThickness.ToString(CultureInfo.InvariantCulture);
								sqlUpdate += " ,F_TOTAL_SLABS = " + blc.gTotalSlabs.ToString();
								sqlUpdate += " ,F_PROC_SLABS = " + blc.gProcSlabs.ToString();
								sqlUpdate += " ,F_HANG_SLABS = " + blc.gHangSlabs.ToString();
								sqlUpdate += " ,F_DELETED_SLABS = " + blc.gDeletedSlabs.ToString();
								sqlUpdate += " ,F_LENGTH = " + blc.gLength.ToString(CultureInfo.InvariantCulture);
								sqlUpdate += " ,F_WIDTH = " + blc.gWidth.ToString(CultureInfo.InvariantCulture);
								sqlUpdate += " ,F_THICKNESS = " + blc.gThickness.ToString(CultureInfo.InvariantCulture);
								sqlUpdate += " ,F_SURFACE = " + blc.gSurface.ToString(CultureInfo.InvariantCulture);
								sqlUpdate += " ,F_DATE_PROD = " + mDbInterface.OleDbToDate(blc.gDateProd);
								sqlUpdate += " ,F_CODE_OP = '" + DbInterface.QueryString(blc.gCodeOp) + "'";
								sqlUpdate += " ,F_BL_SURFACE ='" + DbInterface.QueryString(blc.gBlSurface) + "'";
								sqlUpdate += " ,F_BL_ORIGIN = '" + DbInterface.QueryString(blc.gBlOrigin) + "'";
								sqlUpdate += " ,F_UPD_DATE = " + mDbInterface.OleDbToDate(blc.gUpdDate);
								sqlUpdate += " ,F_DESCRIPTION ='" + DbInterface.QueryString(blc.gDescription) + "'";
								sqlUpdate += " ,F_WEIGHT = " + blc.gWidth.ToString(CultureInfo.InvariantCulture);								

								//Verifica se è stato modificato anche il codice della classe
								if (blc.gBlockStateChanged)
									sqlUpdate += ", F_ID_T_CO_BLOCK_STATES = "
										+ " (SELECT F_ID FROM T_CO_BLOCK_STATES"
										+ " WHERE F_CODE = '" + DbInterface.QueryString(blc.gBlockState.ToString()) + "')";

								sqlUpdate += " WHERE F_ID = " + DbInterface.QueryString(blc.gId.ToString());

								if (!mDbInterface.Execute(sqlUpdate))
									throw new ApplicationException("LineBlockCollection.UpdateDataToDb Error.");

								break;
						}
					}

					//Scorre tutti gli oggetti cancellati
					foreach (LineBlock blc in mDeleted)
					{
						switch (blc.gState)
						{
							//Cancellazione
							case DbObject.ObjectStates.Deleted:

								sqlDelete = "DELETE FROM T_WK_LINE_BLOCKS WHERE F_ID = " + blc.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									throw new ApplicationException("LineBlockCollection.UpdateDataToDb Error.");
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("LineBlockCollection: Deadlock Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("LineBlockCollection.UpdateDataToDb: Exceed Deadlock retrying", ex);
				}

				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("LineBlockCollection: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		/// <summary>
		/// Indica se per il materiale esiste già la configurazione
		/// </summary>
		/// <param name="MatCode"></param>
		/// <returns></returns>
		public bool HasConfiguration(string MatCode)
		{
			OleDbDataReader dr = null;
			bool exist = false;
			try
			{
				string sqlQuery = "select count(*) " +
								"from T_AN_MATERIAL_COMPONENTS " +
								"where F_MAT_CODE = '" + MatCode.Trim() + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					if (int.Parse(dr[0].ToString()) > 0)
						exist = true;
					else
						exist = false;
				}

				return exist;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + "GetMaterialComponent: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/// *******************************************************************
		/// <summary>
		/// Restituisce i valori del componente relativamente al materiale interessato
		/// </summary>
		/// <param name="MatCode">materiale</param>
		/// <param name="ComponentCode">componente</param>
		/// <param name="RGB">colore di sfondo</param>
		/// <returns></returns>
		/// ******************************************************************
		public bool GetMaterialComponent(string MatCode, out string ComponentCode, out string marker, out int RGB, out int startLeveling, out int endLeveling, 
			out double red, out double green, out double blu, out double hue, out double saturation, out double brightness, out double contrast, out double gamma)
		{
			OleDbDataReader dr = null;
			ComponentCode = null;
			RGB = 0;
			red = green = blu = hue = brightness = 0;
			contrast = gamma = saturation = 1;
			startLeveling = endLeveling = -1;
			marker = "";

			try
			{
				string sqlQuery = "select F_MAT_CODE, F_COMPONENT_CODE, F_RGB, F_START_LEVELING, F_END_LEVELING, F_RED, F_GREEN, F_BLU, F_HUE, F_SATURATION, F_BRIGHTNESS, F_CONTRAST, F_GAMMA, F_MARKER " +
								"from T_AN_MATERIAL_COMPONENTS " +
								"where F_MAT_CODE = '" + MatCode.Trim() + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					ComponentCode = dr["F_COMPONENT_CODE"].ToString();
					RGB = int.Parse(dr["F_RGB"].ToString());
					startLeveling = (dr["F_START_LEVELING"] != DBNull.Value ? int.Parse(dr["F_START_LEVELING"].ToString()) : -1);
					endLeveling = (dr["F_END_LEVELING"] != DBNull.Value ? int.Parse(dr["F_END_LEVELING"].ToString()) : -1);
					red = double.Parse(dr["F_RED"].ToString());
					green = double.Parse(dr["F_GREEN"].ToString());
					blu = double.Parse(dr["F_BLU"].ToString());
					hue = double.Parse(dr["F_HUE"].ToString());
					saturation = double.Parse(dr["F_SATURATION"].ToString());
					brightness = double.Parse(dr["F_BRIGHTNESS"].ToString());
					contrast = double.Parse(dr["F_CONTRAST"].ToString());
					gamma = double.Parse(dr["F_GAMMA"].ToString());
					marker = (dr["F_MARKER"] != DBNull.Value ? dr["F_MARKER"].ToString() : "");
				}

				if (ComponentCode == null || ComponentCode.Trim().Length == 0)
					return false;

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + "GetMaterialComponent: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/// *******************************************************************
		/// <summary>
		/// Restituisce l'immagine di background associata al materiale
		/// </summary>
		/// <param name="matCode">materiale</param>
		/// <param name="background">path dell'immagine di sfondo</param>
		/// <returns></returns>
		public bool GetMaterialBackground(string matCode, out string background, out int posX, out int posY, out int zoomFact)
		{
			OleDbDataReader dr = null;
			background = "";
			posX = -1;
			posY = -1;
			zoomFact = 100;

			try
			{
				string sqlQuery = "select F_MAT_CODE, F_BACKGROUND, F_BACKGROUND_ORIGIN_X, F_BACKGROUND_ORIGIN_Y, F_BACKGROUND_ZOOM " +
								"from T_AN_MATERIAL_COMPONENTS " +
								"where F_MAT_CODE = '" + matCode.Trim() + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					background = (dr["F_BACKGROUND"] != DBNull.Value ? dr["F_BACKGROUND"].ToString() : "");
					posX = int.Parse(dr["F_BACKGROUND_ORIGIN_X"].ToString());
					posY = int.Parse(dr["F_BACKGROUND_ORIGIN_Y"].ToString());
					zoomFact = int.Parse(dr["F_BACKGROUND_ZOOM"].ToString());
				}

				if (background == null || background.Trim().Length == 0)
					return false;

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + "GetMaterialBackground: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/// *******************************************************************
		/// <summary>
		/// Restituisce l'immagine di riflesso associata al materiale
		/// </summary>
		/// <param name="matCode">materiale</param>
		/// <param name="reflection">path dell'immagine di riflesso</param>
		/// <returns></returns>
		public bool GetMaterialReflection(string matCode, out string reflection, out int posX, out int posY, out int zoomFact, out int opacity)
		{
			OleDbDataReader dr = null;
			reflection = "";
			posX = -1;
			posY = -1;
			zoomFact = 100;
			opacity = 100;

			try
			{
				string sqlQuery = "select F_MAT_CODE, F_REFLECTION, F_REFLECTION_ORIGIN_X, F_REFLECTION_ORIGIN_Y, F_REFLECTION_ZOOM, F_REFLECTION_OPACITY " +
								"from T_AN_MATERIAL_COMPONENTS " +
								"where F_MAT_CODE = '" + matCode.Trim() + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					reflection = (dr["F_REFLECTION"] != DBNull.Value ? dr["F_REFLECTION"].ToString() : "");
					posX = int.Parse(dr["F_REFLECTION_ORIGIN_X"].ToString());
					posY = int.Parse(dr["F_REFLECTION_ORIGIN_Y"].ToString());
					zoomFact = int.Parse(dr["F_REFLECTION_ZOOM"].ToString());
					opacity = int.Parse(dr["F_REFLECTION_OPACITY"].ToString());
				}

				if (reflection == null || reflection.Trim().Length == 0)
					return false;

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + "GetMaterialReflection: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/// *******************************************************************
		/// <summary>
		/// Inserisce o aggiorna il componente al materiale interessato
		/// </summary>
		/// <param name="MatCode"></param>
		/// <param name="ComponentCode"></param>
		/// <param name="RGB"></param>
		/// <returns></returns>
		/// ******************************************************************
		public bool SetMaterialComponent(string MatCode, string ComponentCode, int RGB, string marker)
		{
			try
			{
				string sqlQuery = "declare @rc int " +
								"select @rc = count(*) " +
								"from T_AN_MATERIAL_COMPONENTS " +
								"where F_MAT_CODE = '" + MatCode.Trim() + "' " +
								"if @rc > 0 " +
									"begin " +
									"update T_AN_MATERIAL_COMPONENTS " +
									"set F_COMPONENT_CODE = '" + ComponentCode + "', F_RGB =" + RGB.ToString() + " , F_MARKER = '" + marker + "' "+
									"where F_MAT_CODE = '" + MatCode + "' " +
									"end " +
								"else " +
									"begin " +
									"insert into T_AN_MATERIAL_COMPONENTS (F_MAT_CODE, F_COMPONENT_CODE, F_RGB, F_MARKER) " +
									"values ('" + MatCode + "','" + ComponentCode + "'," + RGB.ToString() + ", '" + marker + "') " +
									"end";

				if (!mDbInterface.Execute(sqlQuery))
					return false;

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + "SetMaterialComponent: Error", ex);

				return false;
			}
		}

		/// *******************************************************************
		/// <summary>
		/// Aggiorna i valori di post elaborazione dell'immagine
		/// </summary>
		/// <param name="MatCode"></param>
		/// <param name="ComponentCode"></param>
		/// <param name="RGB"></param>
		/// <returns></returns>
		/// ******************************************************************
		public bool SetPostElaborationParameters(string matCode, int startLeveling, int endLeveling, double red, double green, double blu, double hue, double saturation,
			double brightness, double contrast, double gamma, string background, string reflection)
		{
			return SetPostElaborationParameters(matCode, startLeveling, endLeveling, red, green, blu, hue, saturation, brightness, contrast, gamma, background, -1, -1, 100, reflection, -1, -1, 100, 100);
		}
		public bool SetPostElaborationParameters(string matCode, int startLeveling, int endLeveling, double red, double green, double blu, double hue, double saturation,
			double brightness, double contrast, double gamma, string background, int posBackX, int posBackY, int zoomBack, string reflection, int posReflX, int posReflY, int zoomReflection, int reflOpacity)
		{
			try
			{
				string sqlQuery = "declare @rc int " +
								"select @rc = count(*) " +
								"from T_AN_MATERIAL_COMPONENTS " +
								"where F_MAT_CODE = '" + matCode.Trim() + "' " +
								"if @rc > 0 " +
									"begin " +
									"update T_AN_MATERIAL_COMPONENTS " +
									"set F_START_LEVELING = " + startLeveling.ToString() + ", " +
										"F_END_LEVELING = " + endLeveling.ToString() + ", " +
										"F_RED = " + red.ToString() + ", " +
										"F_GREEN = " + green.ToString() + ", " +
										"F_BLU = " + blu.ToString() + ", " +
										"F_HUE = " + hue.ToString() + ", " +
										"F_SATURATION = " + saturation.ToString() + ", " +
										"F_BRIGHTNESS = " + brightness.ToString() + ", " +
										"F_CONTRAST = " + contrast.ToString() + ", " +
										"F_GAMMA = " + gamma.ToString() + ", " +
										"F_BACKGROUND = '" + background.ToString() + "', " +
										"F_BACKGROUND_ORIGIN_X = " + posBackX.ToString() + ", " +
										"F_BACKGROUND_ORIGIN_Y = " + posBackY.ToString() + ", " +
										"F_BACKGROUND_ZOOM = " + zoomBack.ToString() + ", " +
										"F_REFLECTION = '" + reflection.ToString() + "' ," +
										"F_REFLECTION_ORIGIN_X = " + posReflX.ToString() + ", " +
										"F_REFLECTION_ORIGIN_Y = " + posReflY.ToString() + ", " +
										"F_REFLECTION_ZOOM = " + zoomReflection.ToString() + ", " +
										"F_REFLECTION_OPACITY = " + reflOpacity.ToString() + " " +
									"where F_MAT_CODE = '" + matCode + "' " +
									"end " +
								"else " +
									"begin " +
									"insert into T_AN_MATERIAL_COMPONENTS (F_MAT_CODE, " +
																		"F_START_LEVELING, " +
																		"F_END_LEVELING, " +
																		"F_RED, " +
																		"F_GREEN, " +
																		"F_BLU, " +
																		"F_HUE, " +
																		"F_SATURATION, " +
																		"F_BRIGHTNESS, " +
																		"F_CONTRAST, " +
																		"F_GAMMA, " +
																		"F_BACKGROUND, " +
																		"F_BACKGROUND_ORIGIN_X, " +
																		"F_BACKGROUND_ORIGIN_Y, " +
																		"F_BACKGROUND_ZOOM, " +
																		"F_REFLECTION, " +
																		"F_REFLECTION_ORIGIN_X, " +
																		"F_REFLECTION_ORIGIN_Y, " +
																		"F_REFLECTION_ZOOM, " +
																		"F_REFLECTION_OPACITY) " +
									"values ('" + matCode + "'," +
												startLeveling.ToString() + "," +
												endLeveling.ToString() + "," +
												red.ToString() + "," +
												green.ToString() + "," +
												blu.ToString() + "," +
												hue.ToString() + "," +
												saturation.ToString() + "," +
												brightness.ToString() + "," +
												contrast.ToString() + "," +
												gamma.ToString() + ", '" +
												background.ToString() + "', " +
												posBackX.ToString() + ", " +
												posBackY.ToString() + ", " +
												zoomBack.ToString() + ", '" +
												reflection.ToString() + "', " +
												posReflX.ToString() + ", " +
												posReflY.ToString() + ", " +
												zoomReflection.ToString() + ", " +
												reflOpacity.ToString() + ") " +
									 "end";

				if (!mDbInterface.Execute(sqlQuery))
					return false;

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + "SetPostElaborationParameters: Error", ex);

				return false;
			}
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion

		#region Private Methods

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			LineBlock blc;

			Clear();

			// Verifica se la query è valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{


					blc = new LineBlock(int.Parse(dr["F_ID"].ToString()), dr["F_CODE"].ToString(), dr["F_DESCRIPTION"].ToString(),
							LineBlock.GetBlockStateEnum(dr["F_BLOCK_STATE"].ToString()), dr["F_MATERIAL_CODE"].ToString(), dr["F_SUPPLIER_CODE"].ToString(),
							dr["F_STOCK_CODE"].ToString(), 
							(dr["F_STOCK_LEVEL"] == DBNull.Value ? -1 : (int)dr["F_STOCK_LEVEL"]),
							dr["F_QUALITY_CODE"].ToString(), double.Parse((dr["F_SLAB_THICKNESS"].ToString())),
							(dr["F_TOTAL_SLABS"] == DBNull.Value ? -1 : (int)dr["F_TOTAL_SLABS"]),
							(dr["F_PROC_SLABS"] == DBNull.Value ? 0 : (int)dr["F_PROC_SLABS"]),
							(dr["F_HANG_SLABS"] == DBNull.Value ? 0 : (int)dr["F_HANG_SLABS"]),
							(dr["F_DELETED_SLABS"] == DBNull.Value ? 0 : (int)dr["F_DELETED_SLABS"]),
							double.Parse((dr["F_LENGTH"].ToString())), double.Parse((dr["F_WIDTH"].ToString())),
							double.Parse((dr["F_THICKNESS"].ToString())), double.Parse((dr["F_SURFACE"].ToString())), double.Parse((dr["F_WEIGHT"].ToString())),
							(dr["F_DATE_PROD"] == DBNull.Value ? DateTime.MaxValue :  (DateTime)dr["F_DATE_PROD"]),
							dr["F_CODE_OP"].ToString(), dr["F_BL_SURFACE"].ToString(), dr["F_BL_ORIGIN"].ToString(),
							(DateTime)dr["F_UPD_DATE"]);

					AddObject(blc);
					blc.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ".GetDataFromDb: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		/// <summary>
		/// Effettua l'ordinamento per la descrizione
		/// </summary>
		private void SortByDescr()
		{
			LineBlock tmp;

			// Bubble Sort
			for (int i = 0; i < mRecordset.Count - 1; i++)
			{
				for (int j = i + 1; j < mRecordset.Count; j++)
				{
					if (((LineBlock)mRecordset[i]).gDescription.CompareTo(((LineBlock)mRecordset[j]).gDescription) > 0)
					{
						tmp = (LineBlock)mRecordset[i];
						mRecordset[i] = mRecordset[j];
						mRecordset[j] = tmp;
					}
				}
			}
		}

		/// <summary>
		/// Effettua l'ordinamento per codice
		/// </summary>
		private void SortByCode()
		{
			LineBlock tmp;

			// Bubble Sort
			for (int i = 0; i < mRecordset.Count - 1; i++)
			{
				for (int j = i + 1; j < mRecordset.Count; j++)
				{
					if (((LineBlock)mRecordset[i]).gCode.CompareTo(((LineBlock)mRecordset[j]).gCode) > 0)
					{
						tmp = (LineBlock)mRecordset[i];
						mRecordset[i] = mRecordset[j];
						mRecordset[j] = tmp;
					}
				}
			}
		}

		#endregion

	}
}
