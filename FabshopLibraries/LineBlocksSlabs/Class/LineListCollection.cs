﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.OleDb;
using System.Text;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.LineBlocksSlabs
{
	public class LineListCollection : DbCollection
	{
		#region Constructors
        public LineListCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods

		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out LineList obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);

			obj = (LineList)dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out LineList obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (LineList)mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(LineList obj)
		{
			return (base.AddObject((DbObject)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(LineList.Keys.F_NONE, -1, code, null, null);
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(LineList.Keys.F_NONE, id, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(LineList.Keys.F_NONE, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineList.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			ids	: id dei materiali che si vuole estrarre
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineList.Keys orderKey, ArrayList codes)
		{
			return GetDataFromDb(orderKey, null, null, codes);
		}

		public bool GetDataFromDb(LineList.Keys orderKey, string code, string state)
		{
			return GetDataFromDb(orderKey, code, state, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		//			code		: estrae solo l'elemento con il codice specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(LineList.Keys orderKey, string code, string state, ArrayList codes)
		{
			return GetDataFromDb(orderKey, -1, code, state, codes);
		}

		public bool GetDataFromDb(LineList.Keys orderKey, int id, string code, string state, ArrayList codes)
		{

			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo l'oggetto con l'id richiesto
			if (id >= 0)
			{
				sqlWhere += sqlAnd + "T_WK_SCHEDULE_LIST.F_ID = " + id.ToString();
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il codice richiesto
			if (code != null)
			{
				sqlWhere += sqlAnd + "T_WK_SCHEDULE_LIST.F_CODE = '" + DbInterface.QueryString(code) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo i materiali con lo stato richiesto
			if (state != null)
			{
				sqlWhere += sqlAnd + "T_CO_BLOCK_STATES.F_CODE= '" + DbInterface.QueryString(state) + "'";
				sqlAnd = " AND ";
			}

			// Estrare solo i Blocchi con i codici richiesti
			if (codes != null && codes.Count > 0)
			{
				//tabella blocco origine
				sqlWhere += sqlAnd + "T_WK_BLOCK_ORIGIN.F_CODE in ('" + codes[0].ToString() + "'";
				//tabella tipo lavorazione superficie
				sqlWhere += sqlAnd + "T_WK_BLOCK_SURFACE.F_CODE in ('" + codes[0].ToString() + "'";
				//tabella codice operatore
				sqlWhere += sqlAnd + "T_WK_BLOCK_USER.F_CODE in ('" + codes[0].ToString() + "'";
				for (int i = 1; i < codes.Count; i++)
					sqlWhere += ", '" + codes[i].ToString() + "'";

				sqlWhere += ")";
				sqlAnd = " and ";
			}

			switch (orderKey)
			{
				case LineList.Keys.F_NONE:
					break;
				case LineList.Keys.F_ID:
					sqlOrderBy = " order by F_ID ";
					break;
				case LineList.Keys.F_CODE:
					sqlOrderBy = " order by F_CODE ";
					break;
				case LineList.Keys.F_DESCRIPTION:
					sqlOrderBy = " order by F_DESCRIPTION ";
					break;
			}
			return GetDataFromDb("select T_WK_SCHEDULE_LIST.*, T_CO_BLOCK_STATES.F_CODE as F_BLOCK_STATE " +
								 "from T_WK_SCHEDULE_LIST " + 
								 "inner join T_CO_BLOCK_STATES ON T_WK_SCHEDULE_LIST.F_ID_T_CO_BLOCK_STATES = T_CO_BLOCK_STATES.F_ID "+
								 sqlWhere + sqlOrderBy);

		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			// VEDI_AUTOSIZE 
			// da definire 

			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova più volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					//Scorre tutti gli oggetti caricati nella collection
					foreach (LineList blc in mRecordset)
					{
						switch (blc.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:

								//tabella blocco origine
								sqlInsert = "INSERT INTO T_WK_BLOCK_ORIGIN (";
								//tabella tipo lavorazione superficie
								sqlInsert = "INSERT INTO T_WK_BLOCK_SURFACE (";
								//tabella codice operatore
								sqlInsert = "INSERT INTO T_WK_BLOCK_USER (";
								
								sqlInsert += " F_CODE,";
								sqlInsert += " F_DESCRIPTION,";
								sqlInsert += " )";

								string Code = (blc.gCode == null ? "NULL" : "'" + DbInterface.QueryString(blc.gCode) + "'");
								string Description = (blc.gDescription == null ? "NULL" : "'" + DbInterface.QueryString(blc.gDescription) + "'");

								sqlInsert += "( SELECT ";
								sqlInsert += Code + ",";
								sqlInsert += Description + ",";
								sqlInsert += " )";

								if (!mDbInterface.Execute(sqlInsert, out id))
									throw new ApplicationException("LineListCollection.UpdateDataToDb Error.");

								blc.gId = id;

								break;

							//Aggiornamento
							case DbObject.ObjectStates.Updated:
								//tabella blocco origine
								sqlUpdate += "UPDATE T_WK_BLOCK_ORIGIN SET ";
								//tabella tipo lavorazione superficie
								sqlUpdate += "UPDATE T_WK_BLOCK_SURFACE SET ";
								//tabella codice operatore
								sqlUpdate += "UPDATE T_WK_BLOCK_USER SET ";
								
								sqlUpdate += "F_CODE " + (blc.gCode == null ? "IS NULL" : "= '" + DbInterface.QueryString(blc.gCode) + "'");
								sqlUpdate += " ,F_DESCRIPTION " + (blc.gDescription == null ? "IS NULL" : "= '" + DbInterface.QueryString(blc.gDescription) + "'");
								sqlUpdate += " WHERE F_ID " + (blc.gId == null ? "IS NULL" : "= '" + DbInterface.QueryString(blc.gId.ToString()) + "'");

								if (!mDbInterface.Execute(sqlUpdate))
									throw new ApplicationException("LineListCollection.UpdateDataToDb Error.");

								break;
						}
					}

					//Scorre tutti gli oggetti cancellati
					foreach (LineList blc in mDeleted)
					{
						switch (blc.gState)
						{
							//Cancellazione
							case DbObject.ObjectStates.Deleted:
								//tabella blocco origine
								sqlDelete = "DELETE FROM T_WK_BLOCK_ORIGIN WHERE F_ID = " + blc.gId.ToString();
								//tabella tipo lavorazione superficie
								sqlDelete = "DELETE FROM T_WK_BLOCK_SURFACE WHERE F_ID = " + blc.gId.ToString();
								//tabella codice operatore
								sqlDelete = "DELETE FROM INTO T_WK_BLOCK_USER WHERE F_ID = " + blc.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									throw new ApplicationException("LineListCollection.UpdateDataToDb Error.");
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("LineListCollection: Deadlock Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("LineListCollection.UpdateDataToDb: Exceed Deadlock retrying", ex);
				}

				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("LineListCollection: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}


		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion

		#region Private Methods

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			LineList blc;

			Clear();

			// Verifica se la query è valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					blc = new LineList(int.Parse(dr["F_ID"].ToString()), dr["F_CODE"].ToString(), dr["F_DESCRIPTION"].ToString() );

					AddObject(blc);
					blc.gState = DbObject.ObjectStates.Unchanged; 
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("LineListCollection: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		/// <summary>
		/// Effettua l'ordinamento per la descrizione
		/// </summary>
		private void SortByDescr()
		{
			LineList tmp;

			// Bubble Sort
			for (int i = 0; i < mRecordset.Count - 1; i++)
			{
				for (int j = i + 1; j < mRecordset.Count; j++)
				{
					if (((LineList)mRecordset[i]).gDescription.CompareTo(((LineList)mRecordset[j]).gDescription) > 0)
					{
						tmp = (LineList)mRecordset[i];
						mRecordset[i] = mRecordset[j];
						mRecordset[j] = tmp;
					}
				}
			}
		}

		/// <summary>
		/// Effettua l'ordinamento per codice
		/// </summary>
		private void SortByCode()
		{
			LineList tmp;

			// Bubble Sort
			for (int i = 0; i < mRecordset.Count - 1; i++)
			{
				for (int j = i + 1; j < mRecordset.Count; j++)
				{
					if (((LineList)mRecordset[i]).gCode.CompareTo(((LineList)mRecordset[j]).gCode) > 0)
					{
						tmp = (LineList)mRecordset[i];
						mRecordset[i] = mRecordset[j];
						mRecordset[j] = tmp;
					}
				}
			}
		}

		#endregion

	}
}
