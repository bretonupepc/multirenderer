﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data.OleDb;
using System.Text;
using Breton.DbAccess;
using Breton.TraceLoggers;
using System.Threading;

namespace Breton.LineBlocksSlabs
{
	public abstract class LineSlabValueCollection: LineSlabCollection
	{
		#region Variables
		protected LineSlabValue.ValueTypes mValType;
		protected List<LineSlabValue.ValueTypes> mActiveValues;
		#endregion
		
		#region Constructors
		public LineSlabValueCollection(DbInterface db, List<LineSlabValue.ValueTypes> activeValues)
			: base(db)
		{
			mActiveValues = activeValues;
		}
		#endregion

		#region Public Methods

		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out LineSlabValue obj)
		{
			bool ret;
			LineSlab dbObj;

			ret = base.GetObject(out dbObj);

			obj = (LineSlabValue)dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out LineSlabValue obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (LineSlabValue)mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(LineSlabValue obj)
		{
			return (base.AddObject((LineSlabValue)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(LineSlab.Keys.F_NONE, LineSlab.OrderMode.NONE, id, null, null, null, null, null, null, -1, -1, null, null, DateTime.MaxValue);
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(LineSlab.Keys.F_NONE, LineSlab.OrderMode.NONE, -1, code, null, null, null, null, null, -1, -1, null, null, DateTime.MaxValue);
		}

		public bool GetDataFromDb(LineSlab.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null);
		}

		public bool GetDataFromDb(LineSlab.Keys orderKey, string state)
		{
			return GetDataFromDb(orderKey, LineSlab.OrderMode.NONE, -1, null, state, null, null, null, null, -1, -1, null, null, DateTime.MaxValue);
		}

		public bool GetDataFromDb(LineSlab.Keys orderKey, string state, List<LineSlabValue.LineSlabStates> slabValueStates)
		{
			return GetDataFromDb(orderKey, LineSlab.OrderMode.NONE, state, slabValueStates);
		}

		public bool GetDataFromDb(LineSlab.Keys orderKey, LineSlab.OrderMode orderMode, string state, List<LineSlabValue.LineSlabStates> slabValueStates)
		{
			return GetDataFromDb(orderKey, orderMode, -1, null, state, null, null, null, null, -1, -1, null, slabValueStates, DateTime.MaxValue);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		//			code		: estrae solo l'elemento con il codice specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		//public bool GetDataFromDb(LineSlab.Keys orderKey, int id, string code, string state, string blockCode, ArrayList codes, string qualityCode, string batchCode, double thick, double weight, string externalcode, LineSlabValue.LineSlabStates slabValueState)
		//{
		//    return 
		//}

		public bool GetDataFromDb(LineSlab.Keys orderKey, LineSlab.OrderMode orderMode, int id, string code, string state, string blockCode, ArrayList codes, string qualityCode, string batchCode, double thick, double weight, string externalcode, List<LineSlabValue.LineSlabStates> slabValueStates, DateTime processedDate)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo l'oggetto con l'id richiesto
			if (id >= 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_ID = " + id.ToString();
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il codice richiesto
			if (code != null)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_CODE = '" + DbInterface.QueryString(code) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo le lastre con lo stato richiesto
			if (state != null)
			{
				sqlWhere += sqlAnd + "ss.F_CODE= '" + DbInterface.QueryString(state) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo le lastre del blocco richiesto
			if (blockCode != null)
			{
				sqlWhere += sqlAnd + "T_WK_LINE_BLOCKS.F_CODE= '" + DbInterface.QueryString(blockCode) + "'";
				sqlAnd = " and ";
			}

			// Estrare solo le lastre con i codici richiesti
			if (codes != null && codes.Count > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_CODE in ('" + codes[0].ToString() + "'";
				for (int i = 1; i < codes.Count; i++)
					sqlWhere += ", '" + codes[i].ToString() + "'";

				sqlWhere += ")";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con la qualità richiesta
			if (qualityCode != null && qualityCode.Length > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_QUALITY_CODE = '" + DbInterface.QueryString(qualityCode) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il batch code richiesto
			if (batchCode != null && batchCode.Length > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_BATCH_CODE = '" + DbInterface.QueryString(batchCode) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con lo spessore richiesto
			if (thick > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_THICKNESS = '" + thick.ToString() + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il peso richiesto
			if (weight > 0)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_WEIGHT = '" + weight.ToString() + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'oggetto con il codice richiesto
			if (externalcode != null)
			{
				sqlWhere += sqlAnd + "T_WK_SLABS.F_EXTERNAL_CODE = '" + DbInterface.QueryString(externalcode) + "'";
				sqlAnd = " and ";
			}

			if (slabValueStates != null && slabValueStates.Count > 0)
			{
				//if (slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE)) 
				//{
				//    sqlOr = " or ( vs.F_CODE is null)";
				//    slabValueStates.Remove(LineSlabValue.LineSlabStates.NONE);
				//}

				sqlWhere += sqlAnd + "vs.F_CODE in ('" + slabValueStates[0].ToString() + "'";
				for (int i = 1; i < slabValueStates.Count; i++)
					sqlWhere += ", '" + slabValueStates[i].ToString() + "'";

				sqlWhere += ")";
				sqlAnd = " and ";
			}

			string sqlWhere2 = "";
			string sqlAnd2 = " where ";

			//Estrae solo l'oggetto processato nella data richiesta
			if (processedDate != DateTime.MaxValue)
			{
				if (slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE))
				{
					sqlWhere2 += sqlAnd2 + "YEAR(PROCESSED_DATE) = " + processedDate.Year.ToString() +
										" AND MONTH(PROCESSED_DATE) = " + processedDate.Month.ToString() +
										" AND DAY(PROCESSED_DATE) =  " + processedDate.Day.ToString();

					sqlAnd2 = " and ";
				}
				else
				{
					sqlWhere += sqlAnd + "YEAR(T_WK_SLAB_VALUES.F_PROCESSED_DATE) = " + processedDate.Year.ToString() +
										" AND MONTH(T_WK_SLAB_VALUES.F_PROCESSED_DATE) = " + processedDate.Month.ToString() +
										" AND DAY(T_WK_SLAB_VALUES.F_PROCESSED_DATE) =  " + processedDate.Day.ToString();

					sqlAnd = " and ";
				}				
			}

			switch (orderKey)
			{
				case LineSlab.Keys.F_NONE:
					break;
				case LineSlab.Keys.F_ID:
					sqlOrderBy = " order by " + (slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE) ? "tmpSlabVal" : "T_WK_SLABS") + ".F_ID ";
					break;
				case LineSlab.Keys.F_CODE:
					sqlOrderBy = " order by " + (slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE) ? "tmpSlabVal" : "T_WK_SLABS") + ".F_CODE ";
					break;
				case LineSlab.Keys.F_PRIORITY:
					sqlOrderBy = " order by " + (slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE) ? "tmpSlabVal" : "T_WK_SLABS") + ".F_PRIORITY ";
					break;
				case LineSlab.Keys.F_SLAB_STATE:
					sqlOrderBy = " order by " + (slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE) ? "tmpSlabVal" : "T_CO_SLAB_STATES") + ".F_CODE ";
					break;
				case LineSlab.Keys.F_BLOCK_CODE:
					sqlOrderBy = " order by " + (slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE) ? "tmpSlabVal" : "T_WK_LINE_BLOCKS") + ".F_CODE ";
					break;
			}
			
			switch (orderMode)
			{
				// Non fa niente
				case LineSlab.OrderMode.ASC:
				case LineSlab.OrderMode.NONE:
					sqlOrderBy += "";
					break;
				// Inverte l'ordinamento
				case LineSlab.OrderMode.DESC:
					sqlOrderBy += " desc";
					break;
			}

			//sqlWhere += sqlAnd + "T_CO_SLAB_VALUE_TYPES.F_CODE = '" + DbInterface.QueryString(mValType.ToString()) + "'";
			//sqlWhere += sqlAnd + "(T_CO_SLAB_VALUE_TYPES.F_CODE is null or T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "')";
			sqlWhere += sqlAnd + "T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "'";

			return GetDataFromDb((slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE) ? "select * from (" : "") +
								" select	T_WK_SLABS.*, " +
								"			T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE, " +
								"			ss.F_CODE as F_SLAB_STATE, " +
								"			T_WK_SLAB_VALUES.F_ID as F_ID_VAL, " +
								"			vs.F_CODE as F_SLAB_VALUE_STATE, " +
								"			T_WK_SLAB_VALUES.F_INSERT_DATE as INSERT_DATE, " +
								"			T_WK_SLAB_VALUES.F_PROCESSED_DATE as PROCESSED_DATE" +
								"	from	T_WK_SLABS " +
								"			inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
								"			inner join T_CO_SLAB_STATES ss on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID " +
								"			inner join T_WK_SLAB_VALUES on T_WK_SLABS.F_ID = T_WK_SLAB_VALUES.F_ID_T_WK_SLABS " +
								"			inner join T_CO_SLAB_STATES vs on vs.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_STATES " +
								"			inner join T_CO_SLAB_VALUE_TYPES on T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES " +
								sqlWhere +
								(slabValueStates != null && slabValueStates.Contains(LineSlabValue.LineSlabStates.NONE) ?
								" UNION " +
								" SELECT    T_WK_SLABS.* , " +
								"          T_WK_LINE_BLOCKS.F_CODE AS F_BLOCK_CODE , " +
								"          ss.F_CODE AS F_SLAB_STATE , " +
								"          -1 AS F_ID_VAL , " +
								"          '' AS F_SLAB_VALUE_STATE, " +
								"			T_WK_SLABS.F_INSERT_DATE as INSERT_DATE, " +
								"			T_WK_SLABS.F_PROCESSED_DATE as PROCESSED_DATE" +
								" FROM      T_WK_SLABS " +
								"          INNER JOIN T_WK_LINE_BLOCKS ON T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
								"          INNER JOIN T_CO_SLAB_STATES ss ON T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID " +
								" WHERE     T_WK_SLABS.F_ID NOT IN ( " +
								"          SELECT  F_ID_T_WK_SLABS " +
								"          FROM    T_WK_SLAB_VALUES " +
								"                  INNER JOIN T_CO_SLAB_VALUE_TYPES ON T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES " +
								"          WHERE   T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "' ) " +
								") AS tmpSlabVal " : "") + sqlWhere2 + sqlOrderBy);
		}

		public bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova più volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					//Scorre tutti gli oggetti caricati nella collection
					foreach (LineSlabValue lsv in mRecordset)
					{
						// Se manca l'id del valore, si tratta di inserimento
						if (lsv.gIdValue <= 0)
							lsv.gState = DbObject.ObjectStates.Inserted;

						switch (lsv.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_WK_SLAB_VALUES (" +
													" F_ID_T_CO_SLAB_VALUE_TYPES, " +
													" F_ID_T_CO_SLAB_STATES, " +
													" F_ID_T_WK_SLABS) " +
											" (select T_CO_SLAB_VALUE_TYPES.F_ID, T_CO_SLAB_STATES.F_ID, " + lsv.gId.ToString() + " " +
											" from T_CO_SLAB_VALUE_TYPES, T_CO_SLAB_STATES " +
											" where T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "' and" +
													" T_CO_SLAB_STATES.F_CODE = '" + lsv.gLineSlabValueState.ToString() + "'); " +
											" SELECT SCOPE_IDENTITY()";

								if (!mDbInterface.Execute(sqlInsert, out id))
									throw new ApplicationException(this.ToString() + ".UpdateDataToDb Error.");

								lsv.gIdValue = id;
								break;

							//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_WK_SLAB_VALUES set F_ID_T_CO_SLAB_STATES = " +
																		" (select F_ID from T_CO_SLAB_STATES" +
																		" where F_CODE = '" + lsv.gLineSlabValueState.ToString() + "')" +
																		", F_ID_T_CO_SLAB_VALUE_TYPES = " +
																		" (select F_ID from T_CO_SLAB_VALUE_TYPES" +
																		" where F_CODE = '" + mValType.ToString() + "')";

								sqlUpdate += " where F_ID = " + lsv.gIdValue.ToString();
							
								if (!mDbInterface.Execute(sqlUpdate))
									throw new ApplicationException(this.ToString() + "UpdateDataToDb Error.");

								break;
						}
					}

					//Scorre tutti gli oggetti cancellati
					foreach (LineSlabValue lsv in mDeleted)
					{
						switch (lsv.gState)
						{
							//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_WK_SLAB_VALUES where F_ID = " + lsv.gIdValue.ToString();

								if (!mDbInterface.Execute(sqlDelete))
									throw new ApplicationException(this.ToString() + ".UpdateDataToDb Error.");

								break;
						}
					}

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					// Verifica se necessario modificare lo stato della lastra in base alle stazioni di controllo attive
					CheckAndSetSlabState();

					return true;
				}
				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine(this.ToString() + ": Deadlock Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException(this.ToString() + ".UpdateDataToDb: Exceed Deadlock retrying", ex);
				}
				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine(this.ToString() + ": Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

		/// <summary>
		/// Get the next available slab
		/// </summary>
		/// <returns></returns>
		public override bool GetFirstAvailableSlab()
		{
			return GetFirstAvailableSlab(1);
		}

		/// <summary>
		/// Get the next available slab
		/// </summary>
		/// <returns></returns>
		public override bool GetFirstAvailableSlab(int maxSlabs)
		{
			string top = (maxSlabs > 0 ? "top " + maxSlabs.ToString() + "*" : "*");

			return GetDataFromDb("select " + top +
								" from " +
								" (select 	T_WK_SLABS.* ," +
								"			T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE ," +
								"			ss.F_CODE as F_SLAB_STATE ," +
								"			T_WK_SLAB_VALUES.F_ID AS F_ID_VAL , " +
								"			T_CO_SLAB_STATES.F_CODE AS F_SLAB_VALUE_STATE, " +
								"			T_WK_SLAB_VALUES.F_INSERT_DATE as INSERT_DATE, " +
								"			T_WK_SLAB_VALUES.F_PROCESSED_DATE as PROCESSED_DATE " +
								"	from    T_WK_SLABS" +
								"			inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID" +
								"			inner join T_CO_SLAB_STATES ss on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID" +
								"			inner join T_WK_SLAB_VALUES on T_WK_SLABS.F_ID = T_WK_SLAB_VALUES.F_ID_T_WK_SLABS" +
								"			inner join T_CO_SLAB_STATES on T_CO_SLAB_STATES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_STATES" +
								"			inner join T_CO_SLAB_VALUE_TYPES on T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES" +
								"	where   ( T_CO_SLAB_STATES.F_CODE = '" + LineSlabValue.LineSlabStates.SLAB_INSERTED.ToString() + "'" +
								"			  and T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "'" +
								"			)" +
								" union " +
								" select 	T_WK_SLABS.* ," +
								"			T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE ," +
								"			ss.F_CODE as F_SLAB_STATE ," +
								"			-1 AS F_ID_VAL , " +
								"			'' AS F_SLAB_VALUE_STATE, " +
								"			T_WK_SLABS.F_INSERT_DATE as INSERT_DATE, " +
								"			T_WK_SLABS.F_PROCESSED_DATE as PROCESSED_DATE" +
								"	from    T_WK_SLABS" +
								"			inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
								"			inner join T_CO_SLAB_STATES ss on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID " +
								"	where	T_WK_SLABS.F_ID NOT IN (" +
								"			SELECT  F_ID_T_WK_SLABS " +
								"			FROM T_WK_SLAB_VALUES " +
								"				INNER JOIN T_CO_SLAB_VALUE_TYPES ON T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES " +
								"			WHERE T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "') " + 
								"				and ss.F_CODE = '" + LineSlabValue.LineSlabStates.SLAB_INSERTED.ToString() + "') as tempTable" +
								" order by tempTable.F_PRIORITY");			
		}

		/// <summary>
		/// Get unprocessed slabs
		/// </summary>
		/// <returns></returns>
		public bool GetUnprocessedSlabValues()
		{
			return GetUnprocessedSlabValues(null);
		}

		/// <summary>
		/// Get unprocessed slabs
		/// </summary>
		/// <returns></returns>
		public bool GetUnprocessedSlabValues(string slabCode)
		{
			return GetDataFromDb("select * " +
								" from " +
								" (select 	T_WK_SLABS.* ," +
								"			T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE ," +
								"			ss.F_CODE as F_SLAB_STATE ," +
								"			T_WK_SLAB_VALUES.F_ID AS F_ID_VAL , " +
								"			T_CO_SLAB_STATES.F_CODE AS F_SLAB_VALUE_STATE, " +
								"			T_WK_SLAB_VALUES.F_INSERT_DATE as INSERT_DATE, " +
								"			T_WK_SLAB_VALUES.F_PROCESSED_DATE as PROCESSED_DATE " +
								"	from    T_WK_SLABS" +
								"			inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID" +
								"			inner join T_CO_SLAB_STATES ss on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID" +
								"			inner join T_WK_SLAB_VALUES on T_WK_SLABS.F_ID = T_WK_SLAB_VALUES.F_ID_T_WK_SLABS" +
								"			inner join T_CO_SLAB_STATES on T_CO_SLAB_STATES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_STATES" +
								"			inner join T_CO_SLAB_VALUE_TYPES on T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES" +
								"	where   ( T_CO_SLAB_STATES.F_CODE = '" + LineSlabValue.LineSlabStates.SLAB_INSERTED.ToString() + "'" +
								"			  and T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "'" +
								"			)" +
								" union " +
								" select 	T_WK_SLABS.* ," +
								"			T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE ," +
								"			ss.F_CODE as F_SLAB_STATE ," +
								"			-1 AS F_ID_VAL , " +
								"			'' AS F_SLAB_VALUE_STATE, " +
								"			T_WK_SLABS.F_INSERT_DATE  as INSERT_DATE, " +
								"			T_WK_SLABS.F_PROCESSED_DATE as PROCESSED_DATE " +
								"	from    T_WK_SLABS" +
								"			inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
								"			inner join T_CO_SLAB_STATES ss on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID " +
								"	where	T_WK_SLABS.F_ID NOT IN (" +
								"			SELECT  F_ID_T_WK_SLABS " +
								"			FROM T_WK_SLAB_VALUES " +
								"				INNER JOIN T_CO_SLAB_VALUE_TYPES ON T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES " +
								"			WHERE T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "') " +
								"				and ss.F_CODE = '" + LineSlabValue.LineSlabStates.SLAB_INSERTED.ToString() + "') as tempTable" +
								(slabCode != null ? " where F_CODE ='" + slabCode + "' " : "") +
								" order by tempTable.F_PRIORITY");
		}

		/// <summary>
		/// Get processed slabs
		/// </summary>
		/// <returns></returns>
		public bool GetProcessedSlabValues()
		{
			return GetProcessedSlabValues(null, DateTime.MaxValue);
		}

		/// <summary>
		/// Get processed slabs
		/// </summary>
		/// <param name="processedVal"></param>
		/// <returns></returns>
		public bool GetProcessedSlabValues(DateTime processedDate)
		{
			return GetProcessedSlabValues(null, processedDate);
		}

		/// <summary>
		/// Get processed slabs
		/// </summary>
		/// <param name="processedVal"></param>
		/// <returns></returns>
		public bool GetProcessedSlabValues(string slabCode)
		{
			return GetProcessedSlabValues(slabCode, DateTime.MaxValue);
		}

		/// <summary>
		/// Get processed slabs
		/// </summary>
		/// <returns></returns>
		public bool GetProcessedSlabValues(string slabCode, DateTime processedDate)
		{
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo l'oggetto con il codice richiesto
			if (slabCode != null && slabCode.Length > 0)
			{
				sqlWhere += sqlAnd + "F_CODE = '" + DbInterface.QueryString(slabCode) + "'";
				sqlAnd = " and ";
			}

			if (processedDate != DateTime.MaxValue)
			{
				sqlWhere += sqlAnd + "YEAR(PROCESSED_DATE) = " + processedDate.Year.ToString() +
								" AND MONTH(PROCESSED_DATE) = " + processedDate.Month.ToString() +
								" AND DAY(PROCESSED_DATE) =  " + processedDate.Day.ToString();

				sqlAnd = " and ";
			}

			return GetDataFromDb("select * " +
								" from " +
								" (select 	T_WK_SLABS.* ," +
								"			T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE ," +
								"			ss.F_CODE as F_SLAB_STATE ," +
								"			T_WK_SLAB_VALUES.F_ID AS F_ID_VAL , " +
								"			T_CO_SLAB_STATES.F_CODE AS F_SLAB_VALUE_STATE, " +
								"			T_WK_SLAB_VALUES.F_INSERT_DATE as INSERT_DATE, " +
								"			T_WK_SLAB_VALUES.F_PROCESSED_DATE as PROCESSED_DATE " +
								"	from    T_WK_SLABS" +
								"			inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID" +
								"			inner join T_CO_SLAB_STATES ss on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID" +
								"			inner join T_WK_SLAB_VALUES on T_WK_SLABS.F_ID = T_WK_SLAB_VALUES.F_ID_T_WK_SLABS" +
								"			inner join T_CO_SLAB_STATES on T_CO_SLAB_STATES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_STATES" +
								"			inner join T_CO_SLAB_VALUE_TYPES on T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES" +
								"	where   ( T_CO_SLAB_STATES.F_CODE <> '" + LineSlabValue.LineSlabStates.SLAB_INSERTED.ToString() + "'" +
								"			  and T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "'" +
								"			)" +
								" union " +
								" select 	T_WK_SLABS.* ," +
								"			T_WK_LINE_BLOCKS.F_CODE as F_BLOCK_CODE ," +
								"			ss.F_CODE as F_SLAB_STATE ," +
								"			-1 AS F_ID_VAL , " +
								"			ss.F_CODE AS F_SLAB_VALUE_STATE, " +
								"			T_WK_SLABS.F_INSERT_DATE as INSERT_DATE, " +
								"			T_WK_SLABS.F_PROCESSED_DATE as PROCESSED_DATE " +
								"	from    T_WK_SLABS" +
								"			inner join T_WK_LINE_BLOCKS on T_WK_SLABS.F_ID_T_WK_LINE_BLOCKS = T_WK_LINE_BLOCKS.F_ID " +
								"			inner join T_CO_SLAB_STATES ss on T_WK_SLABS.F_ID_T_CO_SLAB_STATES = ss.F_ID " +
								"	where	T_WK_SLABS.F_ID NOT IN (" +
								"			SELECT  F_ID_T_WK_SLABS " +
								"			FROM T_WK_SLAB_VALUES " +
								"				INNER JOIN T_CO_SLAB_VALUE_TYPES ON T_CO_SLAB_VALUE_TYPES.F_ID = T_WK_SLAB_VALUES.F_ID_T_CO_SLAB_VALUE_TYPES " +
								"			WHERE T_CO_SLAB_VALUE_TYPES.F_CODE = '" + mValType.ToString() + "') " +
								"				and ss.F_CODE = '" + LineSlabValue.LineSlabStates.SLAB_PROCESSED.ToString() + "') as tempTable" +
								//(slabCode != null ? " where F_CODE ='" + slabCode + "' " : "") +
								sqlWhere +
								" order by tempTable.F_PRIORITY desc");
		}


		/// <summary>
		/// Legge il file blob dal database
		/// </summary>
		/// <param name="lsv">record di cui leggere il file</param>
		/// <param name="fileName">nome del file su cui memorizzare il campo blob</param>
		/// <returns>
		/// 	true	lettura eseguita
		/// 	false	lettura non eseguita
		/// </returns>
		public bool GetBlobValueFromDb(LineSlabValue lsv, string fileName)
		{
			try
			{
				// Recupera l'immagine dal db
				if (!mDbInterface.GetBlobField("T_WK_SLAB_VALUES", "F_VALUE", " F_ID = " + lsv.gIdValue.ToString(), fileName))
					return false;
			}

				// Eccezione
			catch (Exception ex)
			{
				TraceLog.WriteLine("LineSlabValueCollection.GetBlobValueFromDb: Error", ex);
				return false;
			}

			lsv.gSetBlobValueLink(fileName);

			return true;
		}

		/// <summary>
		/// Scrive il file blob nel database
		/// </summary>
		/// <param name="lsv">record di cui scrivere il file</param>
		/// <param name="fileName">nome del file da cui prelevare il file</param>
		/// <returns>
		/// 	true	lettura eseguita
		/// 	false	lettura non eseguita
		/// </returns>
		public bool SetBlobValueToDb(LineSlabValue lsv, string fileName)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova più volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					if (!mDbInterface.WriteBlobField("T_WK_SLAB_VALUES", "F_VALUE", "", " F_ID = " + lsv.gIdValue.ToString(), fileName, ""))
						throw (new ApplicationException("LineSlabValueCollection.SetBlobValueToDb: error writing file"));

					if (transaction) mDbInterface.EndTransaction(true);

					lsv.gSetBlobValueLink(fileName);
					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("LineSlabValueCollection.SetBlobValueToDb Error.", ex);

					//Se deadlock e non c'è una transazione attiva, riprova più volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}
				//Eccezione di Timeout
				catch (TimeoutException ex)
				{
					TraceLog.WriteLine("LineSlabValueCollection.SetBlobValueToDb Timeout Error.", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					//Se deadlock e non c'è una transazione attiva, riprova più volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						Thread.Sleep(DbInterface.TIMEOUT_WAIT);
						retry++;
					}

					//Verifica se ripetere la transazione
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("LineSlabValueCollection.SetBlobValueToDb: Exceed Timeout retrying", ex);
				}
				// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("LineSlabValueCollection.SetBlobValueToDb: Error", ex);
					return false;
				}
			}

			return false;
		}
		#endregion
		
		#region Protected Methods

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		protected abstract bool GetDataFromDb(string sqlQuery);

		#endregion

		#region Private Methods

		/// <summary>
		/// Verifica se necessario modificare lo stato della lastra in base alle stazioni di controllo attive
		/// </summary>
		private void CheckAndSetSlabState()
		{
			string sqlUpdate;

			try
			{
				if (mActiveValues != null && mActiveValues.Count > 0)
				{
					// Imposta lo stato della lastra come SLAB_PROCESSED a tutte le lastre con tutti i valori non in stato SLAB_INSERTED
					sqlUpdate = "UPDATE  T_WK_SLABS " +
								"SET     F_ID_T_CO_SLAB_STATES = ( SELECT    F_ID " +
								"								  FROM      T_CO_SLAB_STATES " +
								"								  WHERE     T_CO_SLAB_STATES.F_CODE = 'SLAB_PROCESSED' " +
								"								) " +
								"WHERE	F_ID IN ( " +
								"		SELECT  F_ID " +
								"		FROM    T_WK_SLABS " +
								"		WHERE   ( SELECT    COUNT(*) " +
								"				  FROM      T_WK_SLAB_VALUES vv " +
								"							INNER JOIN T_CO_SLAB_STATES ON vv.F_ID_T_CO_SLAB_STATES = T_CO_SLAB_STATES.F_ID " +
								"				  WHERE     T_CO_SLAB_STATES.F_CODE <> 'SLAB_INSERTED' " +
								"							AND vv.F_ID_T_WK_SLABS = T_WK_SLABS.F_ID " +
								"				) >= " + mActiveValues.Count.ToString() + ") " +
								"		AND F_ID_T_CO_SLAB_STATES <> ( SELECT   F_ID " +
								"									   FROM     T_CO_SLAB_STATES " +
								"									   WHERE    T_CO_SLAB_STATES.F_CODE = 'SLAB_PROCESSED' " +
								"									 )";

					if (!mDbInterface.Execute(sqlUpdate))
						throw new ApplicationException(this.ToString() + "CheckAndSetSlabState Error.");



					// Imposta lo stato della lastra come SLAB_INSERTED a tutte le lastre con tutti almeno un valore in stato SLAB_INSERTED
					sqlUpdate = "UPDATE  T_WK_SLABS " +
								"SET     F_ID_T_CO_SLAB_STATES = (SELECT    F_ID " +
								"								  FROM      T_CO_SLAB_STATES " +
								"								  WHERE     T_CO_SLAB_STATES.F_CODE = 'SLAB_INSERTED' " +
								"								) " +
								"WHERE	F_ID IN ( " +
								"		SELECT  F_ID " +
								"		FROM    T_WK_SLABS " +
								"		WHERE   ( SELECT    COUNT(*) " +
								"				  FROM      T_WK_SLAB_VALUES vv " +
								"							INNER JOIN T_CO_SLAB_STATES ON vv.F_ID_T_CO_SLAB_STATES = T_CO_SLAB_STATES.F_ID " +
								"				  WHERE     T_CO_SLAB_STATES.F_CODE <> 'SLAB_INSERTED' " +
								"							AND vv.F_ID_T_WK_SLABS = T_WK_SLABS.F_ID " +
								"				) < " + mActiveValues.Count.ToString() + ") " +
								"		AND F_ID_T_CO_SLAB_STATES <> ( SELECT   F_ID " +
								"									   FROM     T_CO_SLAB_STATES " +
								"									   WHERE    T_CO_SLAB_STATES.F_CODE = 'SLAB_INSERTED' " +
								"									 )";

					if (!mDbInterface.Execute(sqlUpdate))
						throw new ApplicationException(this.ToString() + "CheckAndSetSlabState Error.");
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine(this.ToString() + ": Error", ex);
			}
		}
				

		#endregion
	}
}
