﻿using System;
using System.Collections.Generic;
using System.Text;
using Breton.DbAccess;

namespace Breton.LineBlocksSlabs
{
	public abstract class LineSlabValue : LineSlab
	{
		#region Variables
		private int mIdValue;
		private LineSlabStates mLineSlabValueState;

		//private bool mBlobValue;							//Indica se esiste il file
		private string mBlobValueLink;						//Link memorizzazione temporanea campo blob

		protected ValueTypes mValType;

		public enum ValueTypes
		{
			NONE,
			VAL_WEIGHT,
			VAL_THICKNESS,
			VAL_GLOSS,
		}

		public enum LineSlabStates
		{
			NONE,
			SLAB_INSERTED,
			SLAB_PROCESSED,
			SLAB_VALID,
			SLAB_INVALID,
			SLAB_PROCESSED_VAL,
			SLAB_PROCESSED_INV,
			SLAB_PROC_VAL_ERR,
			SLAB_PROC_INV_ERR
		}
		#endregion

		#region Constructors
		public LineSlabValue()
			: this(-1, LineSlabStates.NONE)
		{
		}

		public LineSlabValue(int idval, LineSlabStates slabstate)
			: this(0, "", -1, "", "", "", "", 0, 0, 0, 0, "", idval, slabstate)
		{
		}

		public LineSlabValue(int id, string code, int priority, string lineblockCode, string lineslabState, string qualityCode, string batchCode, double width, double length, double thickness, double weight, string externalcode, int idval, LineSlabStates slabstate)
			: this(id, code, priority, lineblockCode, lineslabState, qualityCode, batchCode, width, length, thickness, weight, externalcode, idval, slabstate, DateTime.MaxValue, DateTime.MaxValue)
		{

		}

		public LineSlabValue(int id, string code, int priority, string lineblockCode, string lineslabState, string qualityCode, string batchCode, double width, double length, double thickness, double weight, string externalcode, int idval, LineSlabStates slabstate, DateTime insertDate, DateTime processedDate)
			: base(id, code, priority, lineblockCode, lineslabState, qualityCode, batchCode, width, length, thickness, weight, externalcode, insertDate, processedDate)
		{
			mIdValue = idval;
			mLineSlabValueState = slabstate;

			//mBlobValue = false;
		}
		#endregion

		#region Properties

		public int gIdValue
		{
			get { return mIdValue; }
			set { mIdValue = value; }
		}

		public ValueTypes gValType
		{
			get { return mValType; }
		}

		public LineSlabStates gLineSlabValueState
		{
			get { return mLineSlabValueState; }
			set 
			{ 
				mLineSlabValueState = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
		}

		//public bool gBlobValue
		//{
		//    get { return mBlobValue; }
		//    set 
		//    { 
		//        mBlobValue = value;
		//        if (gState == DbObject.ObjectStates.Unchanged)
		//            gState = DbObject.ObjectStates.Updated;
		//    }
		//}

		public string gBlobValueLink
		{
			get { return mBlobValueLink; }
		}

		public void gSetBlobValueLink(string link)
		{
			mBlobValueLink = link;
		}
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is LineSlabValue)
			{
				LineSlabValue lsv = (LineSlabValue)obj;

				return mCode.CompareTo(lsv.gCode);
			}

			throw new ArgumentException("object is not an LineSlabValue");
		}

		#endregion

		#region Public Static Methods
		public static LineSlabValue.LineSlabStates SlabStatesCode(string slabState)
		{
			if (slabState.Trim() == LineSlabStates.SLAB_INSERTED.ToString())
				return LineSlabStates.SLAB_INSERTED;
			if (slabState.Trim() == LineSlabStates.SLAB_VALID.ToString())
				return LineSlabStates.SLAB_VALID;
			if (slabState.Trim() == LineSlabStates.SLAB_INVALID.ToString())
				return LineSlabStates.SLAB_INVALID;
			if (slabState.Trim() == LineSlabStates.SLAB_PROCESSED.ToString())
				return LineSlabStates.SLAB_PROCESSED;
			if (slabState.Trim() == LineSlabStates.SLAB_PROCESSED_VAL.ToString())
				return LineSlabStates.SLAB_PROCESSED_VAL;
			if (slabState.Trim() == LineSlabStates.SLAB_PROCESSED_INV.ToString())
				return LineSlabStates.SLAB_PROCESSED_INV;

			return LineSlabStates.NONE;
		}

		public static string SlabStatesCode(LineSlabValue.LineSlabStates slabState)
		{
			if (slabState == LineSlabStates.SLAB_INSERTED)
				return LineSlabStates.SLAB_INSERTED.ToString();
			if (slabState == LineSlabStates.SLAB_VALID)
				return LineSlabStates.SLAB_VALID.ToString();
			if (slabState == LineSlabStates.SLAB_INVALID)
				return LineSlabStates.SLAB_INVALID.ToString();
			if (slabState == LineSlabStates.SLAB_PROCESSED)
				return LineSlabStates.SLAB_PROCESSED.ToString();
			if (slabState == LineSlabStates.SLAB_PROCESSED_VAL)
				return LineSlabStates.SLAB_PROCESSED_VAL.ToString();
			if (slabState == LineSlabStates.SLAB_PROCESSED_INV)
				return LineSlabStates.SLAB_PROCESSED_INV.ToString();

			return "";
		}
		#endregion

		
	}
}
