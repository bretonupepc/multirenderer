using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Breton.DesignUtil
{
	public class Top
	{
		#region Variables
		public enum DimensionType
		{
			NULLO = 0,
			LINEARE = 1,
			DIAMETRO = 2,
			RAGGIO = 3,
			ANGOLARE = 4,
			ORIZZONTALE = 5,
			VERTICALE = 6
		}

		public struct Dimension
		{
			public string text;			// testo della dimensione
			public int handle;			// indice della dimensione
			public int textHeight;		// altezza del testo
			public int arrowDim;		// dimensione delle frecce
			public int decimalNumber;	// cifre decimali
			public DimensionType type;	// tipo di dimensione

			public Coordinata textPosition;
			public Coordinata p1Position;
			public Coordinata p2Position;
			public Coordinata vertexPosition;
		}

		public struct Text
		{
			public int handle;              // indice del testo
			public string text;             // testo
			public string charType;         // font testo
			public int dimension;           // dimensione del testo
			public byte r, g, b;			// red green blue
			public double x, y, z;		// posizione del testo
		}

		public struct Coordinata
		{
			public double x, y, z;		// Valori coordinata
		}

		private ArrayList mDimensions;
		private ArrayList mTexts;
		#endregion

		#region Constructor
		public Top()
		{
			mDimensions = new ArrayList();
			mTexts = new ArrayList();
		}
		#endregion

		#region Properties
		public ArrayList gDimensions
		{
			set { mDimensions = value; }
			get { return mDimensions; }
		}

		public ArrayList gTexts
		{
			set { mTexts = value; }
			get { return mTexts; }
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Aggiunge una dimensione alla lista
		/// </summary>
		/// <param name="text">testo</param>
		/// <param name="handle">indice</param>
		/// <param name="textHeight">altezza del testo</param>
		/// <param name="arrowDim">dimensione delle frecce</param>
		/// <param name="decimalNumber">cifre decimali</param>
		/// <param name="type">tipo</param>
		/// <param name="textPosition">posizione testo</param>
		/// <param name="p1Position">posizione punto 1</param>
		/// <param name="p2Position">posizione punto 2</param>
		/// <param name="vertexPosition">posizione vertice</param>
		/// <returns></returns>
		public bool AddDimension(string text, int handle, int textHeight, int arrowDim, int decimalNumber, DimensionType type, 
			Coordinata textPosition, Coordinata p1Position, Coordinata p2Position, Coordinata vertexPosition)
		{
			Dimension d = new Dimension();

			d.text = text;
			d.handle = handle;
			d.textHeight = textHeight;
			d.arrowDim = arrowDim;
			d.decimalNumber = decimalNumber;
			d.type = type;
			d.textPosition = textPosition;
			d.p1Position = p1Position;
			d.p2Position = p2Position;
			d.vertexPosition = vertexPosition;

			// Aggiunge la struttura alla lista
			if (mDimensions.Add(d) > 0)	return true;
			else						return false;
		}


		/// ******************************************************************************************
		/// AddText
		/// <summary>
		/// Aggiunge un testo
		/// </summary>
		/// <param name="handle">Indice del testo</param>
		/// <param name="text">testo</param>
		/// <param name="charType">font del testo</param>
		/// <param name="dim">dimensione testo</param>
		/// <param name="r">red</param>
		/// <param name="g">green</param>
		/// <param name="b">blue</param>
		/// <param name="x">posizione x</param>
		/// <param name="y">posizione y</param>
		/// <param name="z">posizione z</param>
		/// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
		/// *******************************************************************************************
		public bool AddText(int handle, string text, string charType, int dim, byte r, byte g, byte b, double x, double y, double z)
		{
			Text txt = new Text();
			txt.handle = handle;
			txt.text = text;
			txt.charType = charType;
			txt.dimension = dim;
			txt.r = r;
			txt.g = g;
			txt.b = b;
			txt.x = x;
			txt.y = y;
			txt.z = z;

			// Aggiunge la struttura alla lista
			if (mTexts.Add(txt) >= 0)	return true;
			else						return false;
		}

		#endregion
	}
}
