using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Breton.MathUtils;
using Breton.Polygons;
using TraceLoggers;

namespace Breton.DesignUtil
{
    public class ShapeGeometry
    {
        #region Variables
        private Path shapePath;
        private Path convexPath;
		private Rect externalRect;
        #endregion

        #region Constructor
        public ShapeGeometry() : this(null, null)
        { 
        }

        public ShapeGeometry(Path shPt, Path cvPt)
        {
            shapePath = shPt;
            convexPath = cvPt;
        }
	    #endregion

        #region Public Methods
        /// <summary>
        /// Carica il path contenente i lati del pezzo da analizzare  
        /// </summary>
        /// <param name="sh">Shape caricata dopo l'analisi del file xml</param>
        /// <returns></returns>
        public bool LoadShapePath(Shape sh)
        {
            Side s;

            try
            {
                if (shapePath == null)
                    shapePath = new Path();

                // Scorre la shape e carica l'oggetto Path
                for (int i = 0; i < sh.gContours.Count; i++)
                {
                    Shape.Contour c = (Shape.Contour)sh.gContours[i];

                    if (c.pricipalStruct)
                    {
                        // Linea
						if (c.line)
							s = new Segment(c.x0, c.y0, c.x1, c.y1);
						//Arco
						else
						{
							double alpha = MathUtil.gdRadianti(c.startAngle);
							double beta = MathUtil.gdRadianti(c.endAngle) - alpha;

							if (c.cw == 0)
								beta = (beta < 0d ? 2 * Math.PI + beta : beta);
							else
								beta = (beta > 0d ? beta - 2 * Math.PI : beta);

							s = new Arc(c.xC, c.yC, c.r, alpha, beta);
						}

                        shapePath.AddSide(s);
                    }
                }
				externalRect = shapePath.GetRectangle();

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ShapeGeometry.LoadShapePath", ex);
                return false;
            }
        }

        /// <summary>
        /// Carica il path contenente il percorso convesso del pezzo
        /// </summary>
        /// <param name="sh">Shape contenente il percorso convesso del pezzo</param>
        /// <returns></returns>
        public bool LoadConvexPath(Shape sh)
        {
            Side s;

            try
            {
                if (convexPath == null)
                    convexPath = new Path();

                // Scorre la shape e carica l'oggetto Path
                for (int i = 0; i < sh.gCutToSize.Count; i++)
                {
                    Shape.Cut cut = (Shape.Cut)sh.gCutToSize[i];

                    // Cerca il cutToSize5 che corrisponde al poligono convesso
                    if (cut.type == 5)
                    {
                        for (int j = 0; j < cut.sides.Count; j++)
                        {
                            Shape.Contour c = (Shape.Contour)cut.sides[j];

                            s = new Segment(c.x0, c.y0, c.x1, c.y1);
                            convexPath.AddSide(s);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ShapeGeometry.LoadConvexPath", ex);
                return false;
            }
        }

        /// <summary>
        /// Analizza le linee "diritte", che possono essere lucidate su una macchina rettilinea
        /// facendo il confronto tra il pezzo e il suo percorso convesso
        /// </summary>
        /// <param name="sh">Shape da analizzare</param>
        /// <returns></returns>
        public bool SetStraightLine(ref Shape sh)
        {
            Point[] intersec;
            ArrayList straightSides = new ArrayList();

            try
            {
                if (convexPath == null || shapePath == null)
                    return false;

                // Semplifica il percorso eliminando i punti allineati
                convexPath.Simplify(0d);

                for (int i = 0; i < convexPath.Count; i++)
                {
                    for (int j = 0; j < shapePath.Count; j++)
                    {
                        // Verifica se il lato � un segmento
                        if (shapePath.GetSide(j) is Segment)
                        {
                            // Verifica se il segmento interessato � sovrapposto a un segmento del percorso convesso
                            if (convexPath.GetSide(i).Intersect(shapePath.GetSide(j), out intersec) == -1)
                                straightSides.Add(shapePath.GetSide(j));
                        }
                    }
                }

                for (int i = 0; i < straightSides.Count; i++)
                {
                    Segment tmpSeg = (Segment)straightSides[i];
                    for (int j = 0; j < sh.gContours.Count; j++)
                    {
                        Shape.Contour tmpCont = (Shape.Contour)sh.gContours[j];

                        if (tmpCont.pricipalStruct && tmpCont.line)
                        {
                            if (tmpSeg.P1.X == tmpCont.x0 &&
                                tmpSeg.P1.Y == tmpCont.y0 &&
                                tmpSeg.P2.X == tmpCont.x1 &&
                                tmpSeg.P2.Y == tmpCont.y1)
                            {
                                tmpCont.straightLine = true;
                                sh.gContours[j] = tmpCont;
                                break;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ShapeGeometry.SetStraightLine Error:", ex);
                return false;
            }
            finally
            {
                convexPath = null;
                shapePath = null;
            }
        }

        /// <summary>
        /// Indica se il poligono � un rettangolo
        /// </summary>
        /// <returns></returns>
        public bool IsRectangle()
        {
            if (shapePath != null)
                return shapePath.IsRectangle();
            else
                return false;
        }

        /// <summary>
        /// Restituisce il valore dell'altezza del rettangolo
        /// </summary>
        /// <returns></returns>
		public double Width()
		{
			double val1, val2;

			//// Il valore dell'altezza � il valore del lato pi� corto
			//if (IsRectangle())
			//{
			// Legge il valore della lunghezza del primo lato
			val1 = externalRect.Width; // shapePath.GetSide(1).Length;
			// Legge il valore della lunghezza del secondo lato
			val2 = externalRect.Height;  //shapePath.GetSide(2).Length;

			if (val1 < val2)
				return val1;
			else
				return val2;
			//}
			//else
			//    return -1;
		}

        /// <summary>
        /// Restituisce il valore della larghezza del rettangolo
        /// </summary>
        /// <returns></returns>
		public double Length()
		{
			double val1, val2;

			//// Il valore della larghezza � il valore del lato pi� lungo
			//if (shapePath.IsRectangle())
			//{
			// Legge il valore della lunghezza del primo lato
			val1 = externalRect.Width; // shapePath.GetSide(1).Length;
			// Legge il valore della lunghezza del secondo lato
			val2 = externalRect.Height;  //shapePath.GetSide(2).Length;

			if (val1 > val2)
				return val1;
			else
				return val2;
			//}
			//else
			//    return -1;
		}

        #endregion

        #region Private Methods
        #endregion
    }
}
