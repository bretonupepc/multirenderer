using System;
using System.Globalization;
using System.Xml;
using System.Collections;
using Breton.MathUtils;
using Breton.Polygons;
using TraceLoggers;

namespace Breton.DesignUtil
{
	public static class XmlAnalizer
	{
		#region Public Methods

		///*********************************************************************
		/// <summary>
		/// Legge le lavorazioni da fare
		/// </summary>
		/// <param name="file">file xml da analizzare</param>
		/// <param name="shape">shape alla quale aggiungere le lavorazioni</param>
        /// <param name="assolute">indica se devono essere lette le posizioni assolute</param>
        /// <returns>
		///		true	lettura eseguito con successo
		///		false	errore nella lettura
		///	</returns>
		///*********************************************************************
		public static bool ReadShapeXml(string file, ref Shape shape, bool assolute)
		{
			XmlDocument doc = new XmlDocument();

			try
			{
				// Legge il file XML
				doc.Load(file);
				return ReadShapeXml(ref doc, ref shape, assolute);
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadShapeXml Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadShapeXml Error. ", ex);
				return false;
			}
			finally
			{
				doc = null;
			}
		}

		///*********************************************************************
		/// <summary>
		/// Legge le lavorazioni da fare
		/// </summary>
		/// <param name="doc">documento xml</param>
		/// <param name="shape">shape alla quale aggiungere le lavorazioni</param>
        /// <param name="assolute">indica se devono essere lette le posizioni assolute</param>
		/// <returns>
		///		true	lettura eseguito con successo
		///		false	errore nella lettura
		///	</returns>
		///*********************************************************************
        public static bool ReadShapeXml(ref XmlDocument doc, ref Shape shape, bool assolute)
        {
            // Lettura del nodo principale
            XmlNode node = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer/Top/Shapes/Shape");

            // nodo non trovato
            if (node == null)
                return ReadLabelXml(ref doc, ref shape, assolute);
            else
                return ReadShapeXml(node, ref shape, assolute);
        }

        ///*********************************************************************
        /// <summary>
        /// Legge le lavorazioni da fare
        /// </summary>
        /// <param name="node">nodo xml</param>
        /// <param name="shape">shape alla quale aggiungere le lavorazioni</param>
        /// <param name="assolute">indica se devono essere lette le posizioni assolute</param>
        /// <returns>
        ///		true	lettura eseguito con successo
        ///		false	errore nella lettura
        ///	</returns>
        ///*********************************************************************
        public static bool ReadShapeXml(XmlNode node, ref Shape shape, bool assolute)
		{
			int i = 0;
			
			try
			{
                if (node == null)
                    return false;

				// Carica i valori della shape
				if (node.Attributes.GetNamedItem("Id") != null)
					shape.gId = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);
                if (node.Attributes.GetNamedItem("Code") != null)
                    shape.gCode = node.Attributes.GetNamedItem("Code").Value;
                if (node.Attributes.GetNamedItem("Shape_Number") != null)
                    shape.gShapeNumber = int.Parse(node.Attributes.GetNamedItem("Shape_Number").Value, CultureInfo.InvariantCulture);
				if (node.Attributes.GetNamedItem("Description") != null)
					shape.gDescription = node.Attributes.GetNamedItem("Description").Value;
				if (node.Attributes.GetNamedItem("Thickness") != null)
                    shape.gThickness = double.Parse(node.Attributes.GetNamedItem("Thickness").Value, CultureInfo.InvariantCulture);
                if (node.Attributes.GetNamedItem("Lamination_Type") != null)
                    shape.gLaminationType = int.Parse(node.Attributes.GetNamedItem("Lamination_Type").Value, CultureInfo.InvariantCulture);
                if (node.Attributes.GetNamedItem("Shape_Join_ID") != null)
                    shape.gShapeNumberJoin = int.Parse(node.Attributes.GetNamedItem("Shape_Join_ID").Value, CultureInfo.InvariantCulture);
                if (node.Attributes.GetNamedItem("IsBackSplash") != null)
                    shape.gBackSplash = bool.Parse(node.Attributes.GetNamedItem("IsBackSplash").Value);
                if (node.Attributes.GetNamedItem("Angle") != null)
                    shape.gAngle = double.Parse(node.Attributes.GetNamedItem("Angle").Value, CultureInfo.InvariantCulture);
                if (node.Attributes.GetNamedItem("ApronLink") != null)
                {
                    if (node.Attributes.GetNamedItem("ApronLink").Value != "")
                    {
                        shape.IsVeletta = true;

                        Shape.Veletta newVel = new Shape.Veletta();
                        newVel.link = node.Attributes.GetNamedItem("ApronLink").Value;
                        newVel.height = double.Parse(node.Attributes.GetNamedItem("ApronHeight").Value, CultureInfo.InvariantCulture);
                        newVel.offset = double.Parse(node.Attributes.GetNamedItem("ApronOffset").Value, CultureInfo.InvariantCulture);
                        shape.gVeletta = newVel;
                    }
                    else
                    {
                        shape.IsVeletta = false;
                    }
                }

				if (shape.gCode == "LAM01")
					shape.gType = Shape.PieceType.LAMINATION;

				// Scorre tutti i nodi presenti nella shape
				foreach(XmlNode nd in node.ChildNodes)
				{
					if(nd.Name == "Material")
					{
						// Legge il codice del materiale impostato
						shape.gMaterialCode = nd.Attributes.GetNamedItem("Code").Value;
					}
					else if(nd.Name == "Structure")
					{
						// Indice che identifica ogni structure
                        if (nd.Attributes.GetNamedItem("Id") != null)
                            i = int.Parse(nd.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);

						// Legge le lavorazioni VGroove
						if (nd.Attributes.GetNamedItem("Code") != null && 
							(nd.Attributes.GetNamedItem("Code").Value == "VGR01" ||
							nd.Attributes.GetNamedItem("Code").Value == "VGR02" ||
							nd.Attributes.GetNamedItem("Code").Value == "VGR03" ||
							nd.Attributes.GetNamedItem("Code").Value == "VGR04" ||
							nd.Attributes.GetNamedItem("Code").Value == "VGR01Box" ||
							nd.Attributes.GetNamedItem("Code").Value == "VGR02Box"))
						{
                            if (!ReadVGroove(nd, ref shape, assolute))
                                return false;
						}
                        // Legge le stampe eulithe
                        else if (nd.Attributes.GetNamedItem("Code") != null &&
                                 nd.Attributes.GetNamedItem("Code").Value == "EUL01")
                        {
                            if (!ReadEulithe(nd, ref shape, assolute))
                                return false;
                        }
						// Legge le etichette
                        else if (nd.Attributes.GetNamedItem("Shape_Subtype") != null && 
							nd.Attributes.GetNamedItem("Shape_Subtype").Value == "Label")
						{
							if(!ReadLabel(nd, ref shape, assolute))
                                return false;
						}
                        // Se trova un simbolo catena imposta il pezzo come alzatina
                        else if (nd.Attributes.GetNamedItem("Shape_Subtype") != null &&
                            nd.Attributes.GetNamedItem("Shape_Subtype").Value == "Chain")
                        {
                            shape.gType = Shape.PieceType.VELETTA;
                        }
                        // Legge i fori bussola
                        else if (nd.Attributes.GetNamedItem("Code") != null &&
                            nd.Attributes.GetNamedItem("Code").Value == "SNP0009")
                        {
                            if (!ReadBussola(nd, ref shape, assolute))
                                return false;
                        }
						else if (nd.Attributes.GetNamedItem("Code") != null &&
							nd.Attributes.GetNamedItem("Code").Value == "R0001")
						{
							if (!ReadReinforcement(nd, ref shape, assolute))
								return false;
						}
						// Legge i symboli generici
						else if (nd.Attributes.GetNamedItem("Code") != null &&
							nd.Attributes.GetNamedItem("Code").Value == "Symbols")
						{
							if (!ReadSymbols(nd, ref shape, assolute))
								return false;
						}
                        else
                        {
                            // Legge le lavorazioni
							if (!ReadContour(nd, ref shape, false, i, assolute))
								return false;
                        }
                        
					}
					else if(nd.Name == "Principal_Structure")
					{
						// Legge le lavorazioni
						if(!ReadContour(nd, ref shape, true, 0, assolute))	return false;
					}
					else if(nd.Name == "Cut_To_Size")
					{
						// Legge i grezzi
						ReadCutToSize(nd, ref shape);
					}
					else if (nd.Name == "Matrix")
					{
						// Legge la matrice di rototraslazione
						ReadMatrix(nd, ref shape);
					}
					else if(nd.Name == "Holes_To_Cut")
					{
						// Legge i fori
						ReadHolesToCut(nd, ref shape);
					}
                    else if (nd.Name == "Texts")
                    {
                        // Legge i testi
                        ReadTexts(nd, ref shape, assolute);
                    }
					else if (nd.Name == "Dimensions")
					{
						// Legge le dimesioni
						ReadDimensions(nd, ref shape, assolute);
					}
				}

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadShapeXml Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadShapeXml Error. ", ex);
				return false;
			}
		}

		public static bool ReadProjectPropertiesXml(string file, ref Top top, string idTop)
		{
			XmlDocument doc = new XmlDocument();

			try
			{
				// Legge il file XML
				doc.Load(file);
				return ReadProjectPropertiesXml(ref doc, ref top, idTop);
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadProjectPropertiesXml Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadProjectPropertiesXml Error. ", ex);
				return false;
			}
			finally
			{
				doc = null;
			}
		}

		public static bool ReadProjectPropertiesXml(ref XmlDocument doc, ref Top top, string idTop)
		{
            //int i = 0;

			try
			{
				// Lettura del nodo principale
				XmlNode node = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

				// nodo non trovato
				if (node == null)
					throw new ApplicationException("Node not found");

				// Scorre tutti i nodi presenti nella shape
				foreach (XmlNode nd in node.ChildNodes)
				{
					if (nd.Name == "Top" && nd.Attributes.GetNamedItem("Id_Top") != null && nd.Attributes.GetNamedItem("Id_Top").Value == idTop)
					{
						foreach (XmlNode n in nd.ChildNodes)
						{
							if (n.Name == "Texts")
							{
								if (!ReadTexts(n, ref top, false))
									throw new ApplicationException("Error read texts");
							}
							else if (n.Name == "Dimensions")
							{
								if (!ReadDimensions(n, ref top, false))
									throw new ApplicationException("Error read dimensions");
							}
						}
					}
				}
				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadProjectPropertiesXml Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadShapeXml Error. ", ex);
				return false;
			}
		}

		public static RTMatrix GetMatrice(string xml)
		{
			RTMatrix mat = new RTMatrix();

			try
			{
				Breton.Polygons.Point p0 = new Breton.Polygons.Point();
				Breton.Polygons.Point p1 = new Breton.Polygons.Point();

				Breton.Polygons.Point p0ass = new Breton.Polygons.Point();
				Breton.Polygons.Point p1ass = new Breton.Polygons.Point();

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.Load(xml);
				XmlNode xmlRoot = xmlDoc.DocumentElement;
				XmlNode xmlPolygon = xmlRoot.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer/Top/Shapes/Shape/Principal_Structure/Polygon");
				XmlNode xmlPolygon_Assolute = xmlRoot.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer/Top/Shapes/Shape/Principal_Structure/Polygon_Assolute");
				XmlNode xmlCut_To_Size = xmlRoot.SelectSingleNode("descendant::Cut_To_Size[@Active='true']");

				XmlNode xmlShape = xmlRoot.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer/Top/Shapes/Shape");

				//la matrice nell'xml fornito da DM3 a volte � sbagliata. In attesa di sistemarla la ricalcolo ogni volta
				XmlNode nodoMatrix = null; // xmlShape.SelectSingleNode("Matrix");

				if (nodoMatrix == null)
				{
					for (int i = 0; i < xmlPolygon.ChildNodes.Count; i++)
					{
						XmlNode xmlNodeTmp = xmlPolygon.ChildNodes[i];

						if (xmlNodeTmp.Name == "Line" || xmlNodeTmp.Name == "Arc")
						{
                            double X0 = double.Parse(xmlNodeTmp.Attributes["X0"].Value, CultureInfo.InvariantCulture);
                            double Y0 = double.Parse(xmlNodeTmp.Attributes["Y0"].Value, CultureInfo.InvariantCulture);
                            double X1 = double.Parse(xmlNodeTmp.Attributes["X1"].Value, CultureInfo.InvariantCulture);
                            double Y1 = double.Parse(xmlNodeTmp.Attributes["Y1"].Value, CultureInfo.InvariantCulture);

							p0 = new Breton.Polygons.Point(X0, Y0);
							p1 = new Breton.Polygons.Point(X1, Y1);

							break;
						}
					}

					for (int i = 0; i < xmlPolygon_Assolute.ChildNodes.Count; i++)
					{
						XmlNode xmlNodeTmp = xmlPolygon_Assolute.ChildNodes[i];

						if (xmlNodeTmp.Name == "Line" || xmlNodeTmp.Name == "Arc")
						{
                            double X0 = double.Parse(xmlNodeTmp.Attributes["X0"].Value, CultureInfo.InvariantCulture);
                            double Y0 = double.Parse(xmlNodeTmp.Attributes["Y0"].Value, CultureInfo.InvariantCulture);
                            double X1 = double.Parse(xmlNodeTmp.Attributes["X1"].Value, CultureInfo.InvariantCulture);
                            double Y1 = double.Parse(xmlNodeTmp.Attributes["Y1"].Value, CultureInfo.InvariantCulture);

							p0ass = new Breton.Polygons.Point(X0, Y0);
							p1ass = new Breton.Polygons.Point(X1, Y1);

							break;
						}
					}

					Segment seg1 = new Segment(p0, p1);
					double a1 = seg1.Inclination;

					Segment seg2 = new Segment(p0ass, p1ass);
					double a2 = seg2.Inclination;


					mat = new RTMatrix();
					mat.RotoTranslRel( - p0.X, - p0.Y, 0d, 0d, 0d);
					mat.RotoTranslRel(0d, 0d, 0d, 0d, a2 - a1);
					mat.RotoTranslRel(p0ass.X, p0ass.Y, 0d, 0d, 0d);

				}
				else
				{

                    double offX = double.Parse(nodoMatrix.Attributes["OffX"].Value, CultureInfo.InvariantCulture);
                    double offY = double.Parse(nodoMatrix.Attributes["OffY"].Value, CultureInfo.InvariantCulture);
                    double angle = double.Parse(nodoMatrix.Attributes["Angle"].Value, CultureInfo.InvariantCulture);

					mat = new RTMatrix();
					mat.SetRotoTransl(offX, offY, angle);

				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.GetMatrice: ", ex);
				return null;
			}
			return mat;
		}
		#endregion

		#region Private Methods

        /// ***************************************************************************
        /// ReadContour
        /// <summary>
        /// Legge le lavorazioni da fare
        /// </summary>
        /// <param name="node">nodo da cui estrarre i valori interessati</param>
        /// <param name="shape">shape alla quale aggiungere le lavorazioni</param>
        /// <param name="prStruct">indica se la lavorazione fa parte del pezzo principale</param>
        /// <param name="id">indice dell'insieme di lastre</param>
        /// <param name="assolute">indica se devono essere lette le posizioni assolute</param>
        /// 
        /// <returns>true	lettura eseguito con successo  false	errore nella lettura</returns>
        /// ***************************************************************************
        private static bool ReadContour(XmlNode node, ref Shape shape, bool prStruct, int id, bool assolute)
        {
            Shape.Contour c = new Shape.Contour();
			double x0, y0, z0, x1, y1, z1, xc, yc, zc, r, endA, startA, angle;
            int idCont, cw = -1;
            bool join;
            double cutAngle_Angle, cutAngle_Height, cutAngle_Offset;
            string apronLink;

            try
            {
				// Legge il codice della structure
				if (node.Attributes.GetNamedItem("Code") != null)
					c.code = node.Attributes.GetNamedItem("Code").Value;

                // Legge la modalit� di inserimento
                if (node.Attributes.GetNamedItem("Insert_mode") != null)
                    c.insertMode = node.Attributes.GetNamedItem("Insert_mode").Value;

                // Legge la modalit� di inserimento
                if (node.Attributes.GetNamedItem("Shape_Subtype") != null)
                    c.subType = node.Attributes.GetNamedItem("Shape_Subtype").Value;

				if (c.code == "SNP0003" || c.code == "SNP0004")
					c.throughHole = true;

				if (c.code == "SNP0002")
					c.groove = true;

				if (c.code == "SNP0008" || c.code == "SNP0006")
					c.pocket = true;

                // Scorre tutti i nodi presenti nella shape
                foreach(XmlNode nd in node.ChildNodes)
                {
                    if (assolute)
                    {
                        if (nd.Name == "Polygon_Assolute")
                        {
                            foreach (XmlNode chn in nd.ChildNodes)
                            {
                                if (chn.Name == "Line")
                                {
                                    // Carica i valori del file xml
                                    c.line = true;
                                    c.pricipalStruct = prStruct;
                                    c.id = id;
                                    // Lettura del valore di CW
                                    if (ReadSide(chn, ref shape, out idCont, out x0, out y0, out z0, out x1, out y1, out z1, out xc, out yc, out zc, out r, out startA, out endA, out cw, out join, out angle, out  cutAngle_Angle, out cutAngle_Height, out cutAngle_Offset, out apronLink))
                                    {
                                        c.idContour = idCont;
                                        c.x0 = x0;
                                        c.y0 = y0;
                                        c.z0 = z0;
                                        c.x1 = x1;
                                        c.y1 = y1;
                                        c.z1 = z1;
                                        c.xC = xc;
                                        c.yC = yc;
                                        c.zC = zc;
                                        c.r = r;
                                        c.startAngle = startA;
                                        c.endAngle = endA;
                                        c.cw = cw;
                                        c.join = join;
										c.slope = angle;

                                        if (apronLink != "" && !shape.IsVeletta)
                                        {
                                            shape.HasVeletta = true;
                                        }

                                        c.apronLink = apronLink;
                                        c.cutAngle_Angle = cutAngle_Angle;
                                        c.cutAngle_Height = cutAngle_Height;
                                        c.cutAngle_Offset = cutAngle_Offset;
                                    }

                                    if (chn.Attributes.GetNamedItem("Processing") != null)
                                        c.processing = chn.Attributes.GetNamedItem("Processing").Value;
                                    else
                                        c.processing = "";

									if (chn.Attributes.GetNamedItem("Processing_Thickness") != null)
                                        c.processing_thick = double.Parse(chn.Attributes.GetNamedItem("Processing_Thickness").Value, CultureInfo.InvariantCulture);
									else
										c.processing_thick = -1;

									if (chn.Attributes.GetNamedItem("State") != null)
										c.state = chn.Attributes.GetNamedItem("State").Value;
									else
										c.state = "";

                                    // Aggiunge un lato
									shape.AddContours(c.id, c.idContour, c.code, c.line, c.pricipalStruct, c.processing, c.state, c.x0, c.y0, c.z0, c.x1, c.y1,
                                        c.z1, c.xC, c.yC, c.zC, c.r, c.startAngle, c.endAngle, c.cw, c.join, c.insertMode, c.subType, c.processing_thick, c.slope, 
										c.cutAngle_Angle, c.cutAngle_Height, c.cutAngle_Offset, c.apronLink, c.throughHole, c.pocket, c.groove);

                                    // Verifica se esiste un foro con l'id interessato
                                    if (shape.gHolesToCut != null && shape.gHolesToCut.Count > 0)
                                        FindHoleProcessingFromContour(shape.gHolesToCut, c);
                                }
                                else if (chn.Name == "Arc")
                                {
                                    // Carica i valori del file xml
                                    c.line = false;
                                    c.pricipalStruct = prStruct;
                                    c.id = id;
                                    if (ReadSide(chn, out idCont, out x0, out y0, out z0, out x1, out y1, out z1, out xc, out yc, out zc, out r, out startA, out endA, out cw, out join, out angle, out  cutAngle_Angle, out cutAngle_Height, out cutAngle_Offset, out apronLink))
                                    {
                                        c.idContour = idCont;
                                        c.x0 = x0;
                                        c.y0 = y0;
                                        c.z0 = z0;
                                        c.x1 = x1;
                                        c.y1 = y1;
                                        c.z1 = z1;
                                        c.xC = xc;
                                        c.yC = yc;
                                        c.zC = zc;
                                        c.r = r;
                                        c.startAngle = startA;
                                        c.endAngle = endA;
                                        c.cw = cw;
                                        c.join = join;
										c.slope = angle;

                                        if (apronLink != "" && !shape.IsVeletta)
                                        {
                                            shape.HasVeletta = true;
                                        }

                                        //c.apronLink = apronLink;
                                        //c.cutAngle_Angle = 0;
                                        //c.cutAngle_Height = 0;
                                        //c.cutAngle_Offset = 0;
                                        c.apronLink = apronLink;
                                        c.cutAngle_Angle = cutAngle_Angle;
                                        c.cutAngle_Height = cutAngle_Height;
                                        c.cutAngle_Offset = cutAngle_Offset;
                                    }

                                    if (chn.Attributes.GetNamedItem("Processing") != null)
                                        c.processing = chn.Attributes.GetNamedItem("Processing").Value;
                                    else
                                        c.processing = "";

									if (chn.Attributes.GetNamedItem("Processing_Thickness") != null)
                                        c.processing_thick = double.Parse(chn.Attributes.GetNamedItem("Processing_Thickness").Value, CultureInfo.InvariantCulture);
									else
										c.processing_thick = -1;

									if (chn.Attributes.GetNamedItem("State") != null)
										c.state = chn.Attributes.GetNamedItem("State").Value;
									else
										c.state = "";

                                    // Aggiunge un lato
                                    shape.AddContours(c.id, c.idContour, c.code, c.line, c.pricipalStruct, c.processing, c.state, c.x0, c.y0, c.z0, c.x1, c.y1,
                                        c.z1, c.xC, c.yC, c.zC, c.r, c.startAngle, c.endAngle, c.cw, c.join, c.insertMode, c.subType, c.processing_thick, c.slope,
										c.cutAngle_Angle, c.cutAngle_Height, c.cutAngle_Offset, c.apronLink, c.throughHole, c.pocket, c.groove);

                                    // Verifica se esiste un foro con l'id interessato
                                    if (shape.gHolesToCut != null && shape.gHolesToCut.Count > 0)
                                        FindHoleProcessingFromContour(shape.gHolesToCut, c);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (nd.Name == "Polygon")
                        {
                            foreach (XmlNode chn in nd.ChildNodes)
                            {
                                if (chn.Name == "Line")
                                {
                                    // Carica i valori del file xml
                                    c.line = true;
                                    c.pricipalStruct = prStruct;
                                    c.id = id;
                                    // Lettura del valore di CW
                                    if (ReadSide(chn, ref shape, out idCont, out x0, out y0, out z0, out x1, out y1, out z1, out xc, out yc, out zc, out r, out startA, out endA, out cw, out join, out angle, out  cutAngle_Angle, out  cutAngle_Height, out  cutAngle_Offset, out apronLink))
                                    {
                                        c.idContour = idCont;
                                        c.x0 = x0;
                                        c.y0 = y0;
                                        c.z0 = z0;
                                        c.x1 = x1;
                                        c.y1 = y1;
                                        c.z1 = z1;
                                        c.xC = xc;
                                        c.yC = yc;
                                        c.zC = zc;
                                        c.r = r;
                                        c.startAngle = startA;
                                        c.endAngle = endA;
                                        c.cw = cw;
                                        c.join = join;
										c.slope = angle;
                                        c.apronLink = apronLink;
                                        c.cutAngle_Angle = cutAngle_Angle;
                                        c.cutAngle_Height = cutAngle_Height;
                                        c.cutAngle_Offset = cutAngle_Offset;
                                    }

                                    if (chn.Attributes.GetNamedItem("Processing") != null)
                                        c.processing = chn.Attributes.GetNamedItem("Processing").Value;
                                    else
                                        c.processing = "";

									if (chn.Attributes.GetNamedItem("Processing_Thickness") != null)
                                        c.processing_thick = double.Parse(chn.Attributes.GetNamedItem("Processing_Thickness").Value, CultureInfo.InvariantCulture);
									else
										c.processing_thick = -1;

									if (chn.Attributes.GetNamedItem("State") != null)
										c.state = chn.Attributes.GetNamedItem("State").Value;
									else
										c.state = "";

                                    // Aggiunge un lato
                                    shape.AddContours(c.id, c.idContour, c.code, c.line, c.pricipalStruct, c.processing, c.state, c.x0, c.y0, c.z0, c.x1, c.y1,
                                        c.z1, c.xC, c.yC, c.zC, c.r, c.startAngle, c.endAngle, c.cw, c.join, c.insertMode, c.subType, c.processing_thick, c.slope,
										c.cutAngle_Angle, c.cutAngle_Height, c.cutAngle_Offset, c.apronLink, c.throughHole, c.pocket, c.groove);

                                    // Verifica se esiste un foro con l'id interessato
                                    if (shape.gHolesToCut != null && shape.gHolesToCut.Count > 0)
                                        FindHoleProcessingFromContour(shape.gHolesToCut, c);
                                }
                                else if (chn.Name == "Arc")
                                {
                                    // Carica i valori del file xml
                                    c.line = false;
                                    c.pricipalStruct = prStruct;
                                    c.id = id;
                                    if (ReadSide(chn, out idCont, out x0, out y0, out z0, out x1, out y1, out z1, out xc, out yc, out zc, out r, out startA, out endA, out cw, out join, out angle, out  cutAngle_Angle, out  cutAngle_Height, out  cutAngle_Offset, out apronLink))
                                    {
                                        c.idContour = idCont;
                                        c.x0 = x0;
                                        c.y0 = y0;
                                        c.z0 = z0;
                                        c.x1 = x1;
                                        c.y1 = y1;
                                        c.z1 = z1;
                                        c.xC = xc;
                                        c.yC = yc;
                                        c.zC = zc;
                                        c.r = r;
                                        c.startAngle = startA;
                                        c.endAngle = endA;
                                        c.cw = cw;
                                        c.join = join;
										c.slope = angle;
                                        c.apronLink = apronLink;
                                        c.cutAngle_Angle = cutAngle_Angle;
                                        c.cutAngle_Height = cutAngle_Height;
                                        c.cutAngle_Offset = cutAngle_Offset;
                                    }

                                    if (chn.Attributes.GetNamedItem("Processing") != null)
                                        c.processing = chn.Attributes.GetNamedItem("Processing").Value;
                                    else
                                        c.processing = "";

									if (chn.Attributes.GetNamedItem("Processing_Thickness") != null)
                                        c.processing_thick = double.Parse(chn.Attributes.GetNamedItem("Processing_Thickness").Value, CultureInfo.InvariantCulture);
									else
										c.processing_thick = -1;

									if (chn.Attributes.GetNamedItem("State") != null)
										c.state = chn.Attributes.GetNamedItem("State").Value;
									else
										c.state = "";

                                    // Aggiunge un lato
									shape.AddContours(c.id, c.idContour, c.code, c.line, c.pricipalStruct, c.processing, c.state, c.x0, c.y0, c.z0, c.x1, c.y1,
                                        c.z1, c.xC, c.yC, c.zC, c.r, c.startAngle, c.endAngle, c.cw, c.join, c.insertMode, c.subType, c.processing_thick, c.slope,
										c.cutAngle_Angle, c.cutAngle_Height, c.cutAngle_Offset, c.apronLink, c.throughHole, c.pocket, c.groove);

                                    // Verifica se esiste un foro con l'id interessato
                                    if (shape.gHolesToCut != null && shape.gHolesToCut.Count > 0)
                                        FindHoleProcessingFromContour(shape.gHolesToCut, c);
                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadContour Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadContour Error. ", ex);
                return false;
            }
        }

        /// **************************************************************************
        /// ReadCutToSize
        /// <summary>
        /// Legge i grezzi presenti nel xml
        /// </summary>
        /// <param name="node">nodo da cui estrarre i valori interessati</param>
        /// <param name="shape">shape alla quale aggiungere i grezzi</param>
        /// <returns>true	lettura eseguito con successo false	errore nella lettura</returns>
        /// ***************************************************************************
        private static bool ReadCutToSize(XmlNode node, ref Shape shape)
        {
            Shape.Cut c = new Shape.Cut();
            ArrayList sides = new ArrayList();
	
            try
            {
                // Carica i valori del file xml
                c.type = int.Parse(node.Attributes.GetNamedItem("Type").Value, CultureInfo.InvariantCulture);
                c.active = bool.Parse(node.Attributes.GetNamedItem("Active").Value);
                foreach (XmlNode chn in node.ChildNodes)
                {
                    if (chn.Name == "Polygon" && ReadSides(chn, sides))
                        c.sides = sides;
                }

                // Aggiunge un grezzo
                shape.AddCutToSize(c.type, c.active, c.sides);
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadCutToSize Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadCutToSize Error. ", ex);
                return false;
            }
        }

        /// ***************************************************************************
        /// ReadHolesToCut        
        /// <summary>
        /// Legge i fori presenti nel xml
        /// </summary>
        /// <param name="node">nodo da cui estrarre i valori interessati</param>
        /// <param name="shape">shape alla quale aggiungere i fori</param>
        /// <returns>true	lettura eseguito con successo false	errore nella lettura</returns>
        /// ***************************************************************************
        private static bool ReadHolesToCut(XmlNode node, ref Shape shape)
        {
            Shape.Holes h;
            ArrayList sides;
	
            try
            {
                // Scorre tutti i nodi presenti nella shape
                foreach(XmlNode nd in node.ChildNodes)
                {
                    if(nd.Name == "Structure")
                    {
                        foreach(XmlNode chn in nd.ChildNodes)
                        {
                            if (chn.Name == "Polygon")
                            {
                                h = new Shape.Holes();
                                // Carica i valori del file xml
                                if (nd.Attributes.GetNamedItem("Id") != null)
                                    h.id = int.Parse(nd.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);

                                if (nd.Attributes.GetNamedItem("Active") != null)
                                    h.active = bool.Parse(nd.Attributes.GetNamedItem("Active").Value);
                                
                                if (nd.Attributes.GetNamedItem("Is_Bridge") != null)
                                    h.isBridge = bool.Parse(nd.Attributes.GetNamedItem("Is_Bridge").Value);

                                sides = new ArrayList();
                                if (ReadSides(chn, sides))
                                    h.sides = sides;
                                
                                // Verifica se il foro che si sta inserirendo ha lavorazioni
                                if(shape.gContours != null && shape.gContours.Count > 0)
                                    FindHoleProcessingFromContours(shape.gContours, ref h);

                                // Aggiunge una lavorazione
                                shape.AddHolesToCut(h.id, h.active, h.sides, h.polish, h.processing, h.isBridge);
                            }							
                        }
                    }
                }
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadHolesToCut Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadHolesToCut Error. ", ex);
                return false;
            }
        }


        /// **************************************************************************
        /// ReadTexts
        /// <summary>
        /// Legge i testi presenti nel xml
        /// </summary>
        /// <param name="node">nodo da cui estrarre i valori interessati</param>
        /// <param name="shape">shape alla quale aggiungere i testi</param>
        /// <param name="assolute">indica se devono essere lette le posizioni assolute</param>
        /// <returns>true	lettura eseguita con successo  false	errore nella lettura</returns>
        /// **************************************************************************
        private static bool ReadTexts(XmlNode node, ref Shape shape, bool assolute)
        {
            Shape.Text t = new Shape.Text();
			
            try
            {
                // Scorre tutti i nodi presenti nella shape
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (nd.Name == "Text")
                    {					
                        if (nd.Attributes.GetNamedItem("handle") != null)
                            t.handle = int.Parse(nd.Attributes.GetNamedItem("handle").Value, CultureInfo.InvariantCulture);
                        if (nd.Attributes.GetNamedItem("text") != null)
                            t.text = nd.Attributes.GetNamedItem("text").Value;
                        if (nd.Attributes.GetNamedItem("CharType") != null)
                            t.charType = nd.Attributes.GetNamedItem("CharType").Value;
                        if (nd.Attributes.GetNamedItem("Dimension") != null)
                            t.dimension = int.Parse(nd.Attributes.GetNamedItem("Dimension").Value, CultureInfo.InvariantCulture);
                        if (nd.Attributes.GetNamedItem("R") != null)
                            t.r = byte.Parse(nd.Attributes.GetNamedItem("R").Value);
                        if (nd.Attributes.GetNamedItem("G") != null)
                            t.g = byte.Parse(nd.Attributes.GetNamedItem("G").Value);
                        if (nd.Attributes.GetNamedItem("B") != null)
                            t.b = byte.Parse(nd.Attributes.GetNamedItem("B").Value);

                        if (assolute)
                        {
                            // Legge la posizione del testo
                            XmlNode n = nd.SelectSingleNode("position_Assolute/Coordinata");
                            if (n != null)
                            {
                                if (n.Attributes.GetNamedItem("x") != null)
                                    t.x = double.Parse(n.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
                                if (n.Attributes.GetNamedItem("y") != null)
                                    t.y = double.Parse(n.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
                                if (n.Attributes.GetNamedItem("z") != null)
                                    t.z = double.Parse(n.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
                            }
                        }
                        else
                        {
                            // Legge la posizione del testo
                            XmlNode n = nd.SelectSingleNode("position/Coordinata");
                            if (n != null)
                            {
                                if (n.Attributes.GetNamedItem("x") != null)
                                    t.x = double.Parse(n.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
                                if (n.Attributes.GetNamedItem("y") != null)
                                    t.y = double.Parse(n.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
                                if (n.Attributes.GetNamedItem("z") != null)
                                    t.z = double.Parse(n.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
                            }
                        }

                        // Aggiunge il testo
                        shape.AddText(t.handle, t.text, t.charType, t.dimension, t.r, t.g, t.b, t.x, t.y, t.z);                        
                    }
                }
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadTexts Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadTexts Error. ", ex);
                return false;
            }
        }

		/// **************************************************************************
		/// ReadTexts
		/// <summary>
		/// Legge i testi presenti nel xml
		/// </summary>
		/// <param name="node">nodo da cui estrarre i valori interessati</param>
		/// <param name="top">top al quale aggiungere i testi</param>
		/// <param name="assolute">indica se devono essere lette le posizioni assolute</param>
		/// <returns>true	lettura eseguita con successo  false	errore nella lettura</returns>
		/// **************************************************************************
		private static bool ReadTexts(XmlNode node, ref Top top, bool assolute)
		{
			Top.Text t = new Top.Text();

			try
			{
				// Scorre tutti i nodi presenti nella shape
				foreach (XmlNode nd in node.ChildNodes)
				{
					if (nd.Name == "Text")
					{
						if (nd.Attributes.GetNamedItem("handle") != null)
                            t.handle = int.Parse(nd.Attributes.GetNamedItem("handle").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("text") != null)
							t.text = nd.Attributes.GetNamedItem("text").Value;
						if (nd.Attributes.GetNamedItem("CharType") != null)
							t.charType = nd.Attributes.GetNamedItem("CharType").Value;
						if (nd.Attributes.GetNamedItem("Dimension") != null)
                            t.dimension = int.Parse(nd.Attributes.GetNamedItem("Dimension").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("R") != null)
							t.r = byte.Parse(nd.Attributes.GetNamedItem("R").Value);
						if (nd.Attributes.GetNamedItem("G") != null)
							t.g = byte.Parse(nd.Attributes.GetNamedItem("G").Value);
						if (nd.Attributes.GetNamedItem("B") != null)
							t.b = byte.Parse(nd.Attributes.GetNamedItem("B").Value);

						if (assolute)
						{
							// Legge la posizione del testo
							XmlNode n = nd.SelectSingleNode("position_Assolute/Coordinata");
							if (n != null)
							{
								if (n.Attributes.GetNamedItem("x") != null)
                                    t.x = double.Parse(n.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
								if (n.Attributes.GetNamedItem("y") != null)
                                    t.y = double.Parse(n.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
								if (n.Attributes.GetNamedItem("z") != null)
                                    t.z = double.Parse(n.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
							}
						}
						else
						{
							// Legge la posizione del testo
							XmlNode n = nd.SelectSingleNode("position/Coordinata");
							if (n != null)
							{
								if (n.Attributes.GetNamedItem("x") != null)
                                    t.x = double.Parse(n.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
								if (n.Attributes.GetNamedItem("y") != null)
                                    t.y = double.Parse(n.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
								if (n.Attributes.GetNamedItem("z") != null)
                                    t.z = double.Parse(n.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
							}
						}

						// Aggiunge il testo
						top.AddText(t.handle, t.text, t.charType, t.dimension, t.r, t.g, t.b, t.x, t.y, t.z);
					}
				}
				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadTexts Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadTexts Error. ", ex);
				return false;
			}
		}

        /// ***************************************************************************
        /// ReadBussola        
        /// <summary>
        /// Legge i fori bussola presenti nel xml
        /// </summary>
        /// <param name="node">nodo da cui estrarre i valori interessati</param>
        /// <param name="shape">shape alla quale aggiungere i fori bussola</param>
        /// <param name="assolute">indica se devono essere lette le posizioni assolute</param>
        /// <returns>true lettura eseguito con successo     false errore nella lettura</returns>
        /// ***************************************************************************
        private static bool ReadBussola(XmlNode node, ref Shape shape, bool assolute)
        {
            Shape.Bussola b = new Shape.Bussola();
            ArrayList sides;
	
            try
            {
                // Scorre tutti i nodi presenti nella shape
                foreach(XmlNode nd in node.ChildNodes)
                {
                    if (assolute)
                    {
                        if (nd.Name == "Polygon_Assolute")
                        {
                            // Carica i valori del file xml
                            b.id = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);
                            sides = new ArrayList();
                            if (ReadSides(nd, sides))
                                b.sides = sides;

                            // Aggiunge un foro bussola alla lista
                            shape.AddBussola(b.id, b.sides);
                        }
                    }
                    else
                    {
                        if (nd.Name == "Polygon")
                        {
                            // Carica i valori del file xml
                            b.id = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);
                            sides = new ArrayList();
                            if (ReadSides(nd, sides))
                                b.sides = sides;

                            // Aggiunge un foro bussola alla lista
                            shape.AddBussola(b.id, b.sides);
                        }
                    }
                }
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadBussola Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadBussola Error. ", ex);
                return false;
            }
        }

        /***************************************************************************
         * ReadSide
         * Legge i lati presenti nel nodo xml
         * Parametri:
         *			n			: nodo xml
         *			x0,y0,z0	: punto iniziale
         *			x1,y1,z1	: punto finale
         *			xC,yC,zC	: punto centro
         *			r,sA,eA		: raggio, angolo iniziale, angolo finale
         *          join        : si tratta di un lato collegato S/N
         * Ritorna:
         *			true	lettura eseguita
         *			false	lettura fallita
         * **************************************************************************/
        private static bool ReadSide(XmlNode n, out int idCont, out double x0, out double y0, out double z0, out double x1, out double y1, out double z1, out double xC,
            out double yC, out double zC, out double r, out double sA, out double eA, out int cw, out bool join, out double angle, out double cutAngle_Angle, out double cutAngle_Height, out double cutAngle_Offset, out string apronLink)
        {
            Shape s = new Shape();
            return ReadSide(n, ref s, out idCont, out x0, out y0, out z0, out x1, out y1, out z1, out xC, out yC, out zC, out r, out sA, out eA, out cw, out join, out angle, out  cutAngle_Angle, out  cutAngle_Height, out  cutAngle_Offset, out apronLink);
        }

        private static bool ReadSide(XmlNode n, ref Shape s, out int idCont, out double x0, out double y0, out double z0, out double x1, out double y1, out double z1, out double xC,
            out double yC, out double zC, out double r, out double sA, out double eA, out int cw, out bool join, out double angle, out double cutAngle_Angle, out double cutAngle_Height, out double cutAngle_Offset, out string apronLink)
		{
            idCont = -1;
			x0 = 0d;
			y0 = 0d;
			z0 = 0d;
			x1 = 0d;
			y1 = 0d;
			z1 = 0d;
			xC = 0d;
			yC = 0d;
			zC = 0d;
			r = 0d;
			sA = 0d;
			eA = 0d;
			cw = -1;
            join = false;
			angle = 0d;
            cutAngle_Angle = 0;
            cutAngle_Height = 0;
            cutAngle_Offset = 0;
            apronLink = "";

			try
			{
                // Lettura del valore dell'id
                if (n.Attributes.GetNamedItem("Id") != null)
                    idCont = int.Parse(n.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di X0
				if(n.Attributes.GetNamedItem("X0") != null)
                    x0 = double.Parse(n.Attributes.GetNamedItem("X0").Value, CultureInfo.InvariantCulture);
				
				// Lettura del valore di Y0
				if(n.Attributes.GetNamedItem("Y0") != null)
                    y0 = double.Parse(n.Attributes.GetNamedItem("Y0").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di Z0
				if(n.Attributes.GetNamedItem("Z0") != null)
                    z0 = double.Parse(n.Attributes.GetNamedItem("Z0").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di X1
				if(n.Attributes.GetNamedItem("X1") != null)
                    x1 = double.Parse(n.Attributes.GetNamedItem("X1").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di Y1
				if(n.Attributes.GetNamedItem("Y1") != null)
                    y1 = double.Parse(n.Attributes.GetNamedItem("Y1").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di Z1
				if(n.Attributes.GetNamedItem("Z1") != null)
                    z1 = double.Parse(n.Attributes.GetNamedItem("Z1").Value, CultureInfo.InvariantCulture);

                // Lettura del valore di Cut_Angle
                if (n.Attributes.GetNamedItem("Cut_Angle") != null)
                {
                    string cutAngle = n.Attributes.GetNamedItem("Cut_Angle").Value;
                    string[] tmpCutAngle = cutAngle.Split(' ');
					if (tmpCutAngle.Length > 0)
                        cutAngle_Angle = double.Parse(tmpCutAngle[0].Replace("a�=", "").Trim(), CultureInfo.InvariantCulture);
					if (tmpCutAngle.Length > 1)
                        cutAngle_Height = double.Parse(tmpCutAngle[1].Replace("h=", "").Trim(), CultureInfo.InvariantCulture);
					if (tmpCutAngle.Length > 2)
                        cutAngle_Offset = double.Parse(tmpCutAngle[2].Replace("of=", "").Trim(), CultureInfo.InvariantCulture);
                }

                // Lettura del valore di LinkVeletta
                if (n.Attributes.GetNamedItem("LinkVeletta") != null)
                    apronLink = n.Attributes.GetNamedItem("LinkVeletta").Value;

                // Lettura del valore di ApronLink
                if (n.Attributes.GetNamedItem("ApronLink") != null)
                    apronLink = n.Attributes.GetNamedItem("ApronLink").Value;

				// Lettura del valore di XC
				if(n.Attributes.GetNamedItem("XC") != null)
                    xC = double.Parse(n.Attributes.GetNamedItem("XC").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di YC
				if(n.Attributes.GetNamedItem("YC") != null)
                    yC = double.Parse(n.Attributes.GetNamedItem("YC").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di ZC
				if(n.Attributes.GetNamedItem("ZC") != null)
                    zC = double.Parse(n.Attributes.GetNamedItem("ZC").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di Raggio
				if(n.Attributes.GetNamedItem("Radius") != null)
                    r = double.Parse(n.Attributes.GetNamedItem("Radius").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di start Angle
				if(n.Attributes.GetNamedItem("Start_Angle") != null)
                    sA = double.Parse(n.Attributes.GetNamedItem("Start_Angle").Value, CultureInfo.InvariantCulture);

				// Lettura del valore di end Angle
				if(n.Attributes.GetNamedItem("End_Angle") != null)
                    eA = double.Parse(n.Attributes.GetNamedItem("End_Angle").Value, CultureInfo.InvariantCulture);

				// Lettura del valore cw
				if(n.Attributes.GetNamedItem("CW") != null)
                    cw = int.Parse(n.Attributes.GetNamedItem("CW").Value, CultureInfo.InvariantCulture);

				// Lettura del valore Angle
				if (n.Attributes.GetNamedItem("Angle") != null)
                    angle = double.Parse(n.Attributes.GetNamedItem("Angle").Value, CultureInfo.InvariantCulture);

                // Verifica se c'� � attaccato a un'altra sagoma
                if (n.HasChildNodes)
                {
                    XmlNode joinNode = n.FirstChild;
                    s.gShapeIdJoin = int.Parse((joinNode.Attributes.GetNamedItem("Label").Value.Split('#'))[1], CultureInfo.InvariantCulture);
                    join = true;
                }

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadSide Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadSide Error. ", ex);
				return false;
			}
		}

		/***************************************************************************
		 * ReadSides
		 * Legge i lati presenti nel nodo xml
		 * Parametri:
		 *			n	: nodo xml
		 *			s	: ArrayList contenente tutti i lati
		 * Ritorna:
		 *			true	lettura eseguita
		 *			false	lettura fallita
		 * **************************************************************************/
        private static bool ReadSides(XmlNode n, ArrayList s)
		{
			Shape.Contour c = new Shape.Contour();
			double x0, y0, z0, x1, y1, z1, xc, yc, zc, r, endA, startA, angle;
			int idCont, cw;
            bool join;
            double cutAngle_Angle, cutAngle_Height, cutAngle_Offset;
            string apronLink;

			try
			{
				foreach(XmlNode chn in n.ChildNodes)
				{
					if (chn.Name == "Line")
					{
						c.line = true;
                        if (ReadSide(chn, out idCont, out x0, out y0, out z0, out x1, out y1, out z1, out xc, out yc, out zc, out r, out startA, out endA, out cw, out join, out angle, out  cutAngle_Angle, out  cutAngle_Height, out  cutAngle_Offset, out apronLink))
						{
                            c.idContour = idCont;
							c.x0 = x0;
							c.y0 = y0;
							c.z0 = z0;
							c.x1 = x1;
							c.y1 = y1;
							c.z1 = z1;
							c.xC = xc;
							c.yC = yc;
							c.zC = zc;
							c.r = r;
							c.startAngle = startA;
							c.endAngle = endA;
							c.cw = cw;
                            c.join = join;
							c.slope = angle;	// inclinazione di taglio
                            c.apronLink = apronLink;
                            c.cutAngle_Angle = cutAngle_Angle;
                            c.cutAngle_Height = cutAngle_Height;
                            c.cutAngle_Offset = cutAngle_Offset;
                        }

						// Aggiunge un lato
						s.Add(c);
					}
					else if (chn.Name == "Arc")
					{
						c.line = false;
                        if (ReadSide(chn, out idCont, out x0, out y0, out z0, out x1, out y1, out z1, out xc, out yc, out zc, out r, out startA, out endA, out cw, out join, out angle, out  cutAngle_Angle, out  cutAngle_Height, out  cutAngle_Offset, out apronLink))
						{
                            c.idContour = idCont;
							c.x0 = x0;
							c.y0 = y0;
							c.z0 = z0;
							c.x1 = x1;
							c.y1 = y1;
							c.z1 = z1;
							c.xC = xc;
							c.yC = yc;
							c.zC = zc;
							c.r = r;
							c.startAngle = startA;
							c.endAngle = endA;
							c.cw = cw;
                            c.join = join;
							c.slope = angle;	// inclinazione di taglio
                            c.apronLink = apronLink;
                            c.cutAngle_Angle = cutAngle_Angle;
                            c.cutAngle_Height = cutAngle_Height;
                            c.cutAngle_Offset = cutAngle_Offset;
                        }

						// Aggiunge un lato
						s.Add(c);
					}
				}
				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadSides Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadSides Error. ", ex);
				return false;
			}
		}

		/***************************************************************************
		 * ReadVGroove
		 * Legge le lavorazioni vgroove
		 * Parametri:
		 *			node		: nodo da cui estrarre i valori interessati
		 *			shape		: shape alla quale aggiungere le lavorazioni vgroove
         *          assolute    : indica se devono essere lette le dimensioni assolute
		 * Ritorna:
		 *			true	lettura eseguito con successo
		 *			false	errore nella lettura
		 * **************************************************************************/
        private static bool ReadVGroove(XmlNode node, ref Shape shape, bool assolute)
		{
			Shape.VGroove v = new Shape.VGroove();
            Shape.Contour c;
            ArrayList sides = new ArrayList();
            double l;
	
			try
			{
                v.id = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);
				v.type = node.Attributes.GetNamedItem("Code").Value;

				if (node.Attributes.GetNamedItem("Height") != null)
                    v.height = double.Parse(node.Attributes.GetNamedItem("Height").Value, CultureInfo.InvariantCulture);

				if (node.Attributes.GetNamedItem("Depth") != null)
                    v.depth = double.Parse(node.Attributes.GetNamedItem("Depth").Value, CultureInfo.InvariantCulture);
				
				// Scorre tutti i nodi presenti nella shape
				foreach(XmlNode nd in node.ChildNodes)
				{
                    if (assolute)
                    {
                        if (nd.Name == "Polygon_Assolute" && ReadSides(nd, sides))
                        {
                            v.sides = sides;
                            for (int i = 0; i < sides.Count; i++)
                            {
                                c = (Shape.Contour)sides[i];

                                l = Math.Sqrt(Math.Abs(c.x0 - c.x1) * Math.Abs(c.x0 - c.x1) + Math.Abs(c.y0 - c.y1) * Math.Abs(c.y0 - c.y1));
                                v.length = l;
                            }
                        }
                    }
                    else
                    {
                        if (nd.Name == "Polygon" && ReadSides(nd, sides))
                        {
                            v.sides = sides;
                            for (int i = 0; i < sides.Count; i++)
                            {
                                c = (Shape.Contour)sides[i];

                                l = Math.Sqrt(Math.Abs(c.x0 - c.x1) * Math.Abs(c.x0 - c.x1) + Math.Abs(c.y0 - c.y1) * Math.Abs(c.y0 - c.y1));
                                v.length = l;
                            }
                        }
                    }
				}
                shape.AddVGrooves(v.id, v.type, v.height, v.length, v.depth, v.sides);

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadVGroove Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadVGroove Error. ", ex);
				return false;
			}
		}

		/// <summary>
		/// Legge i simboli generici
		/// </summary>
		/// <param name="node">nodo da cui estrarre i valori interessati</param>
		/// <param name="shape">shape alla quale aggiungere il simbolo</param>
		/// <param name="assolute">indica se devono essere lette le dimensioni assolute</param>
		/// <returns>true	lettura eseguito con successo	false	errore nella lettura</returns>
		private static bool ReadSymbols(XmlNode node, ref Shape shape, bool assolute)
        {
            Shape.Symbol sy = new Shape.Symbol();
            ArrayList sides = new ArrayList();

            try
            {
                sy.id = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);
				if (node.Attributes.GetNamedItem("Shape_Subtype") != null)
					sy.type = node.Attributes.GetNamedItem("Shape_Subtype").Value;

                // Scorre tutti i nodi presenti nella shape
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (assolute)
                    {
                        if (nd.Name == "Polygon_Assolute" && ReadSides(nd, sides))
                        {
                            sy.sides = sides;
                        }
                    }
                    else
                    {
                        if (nd.Name == "Polygon" && ReadSides(nd, sides))
                        {
                            sy.sides = sides;
                        }
                    }
                }
                shape.AddSymbols(sy.id, sy.type, sy.sides);

                return true;
            }
            catch (XmlException ex)
            {
				TraceLog.WriteLine("XmlAnalizer.ReadSymbols Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
				TraceLog.WriteLine("XmlAnalizer.ReadSymbols Error. ", ex);
                return false;
            }
        }
        /***************************************************************************
		 * ReadEulithe
		 * Legge le stampe eulithe
		 * Parametri:
		 *			node		: nodo da cui estrarre i valori interessati
		 *			shape		: shape alla quale aggiungere le stampe eulithe
         *          assolute    : indica se devono essere lette le dimensioni assolute
		 * Ritorna:
		 *			true	lettura eseguito con successo
		 *			false	errore nella lettura
		 * **************************************************************************/
        private static bool ReadEulithe(XmlNode node, ref Shape shape, bool assolute)
        {
            Shape.Eulithe e = new Shape.Eulithe();
            ArrayList sides = new ArrayList();

            try
            {
                e.id = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);

                // Scorre tutti i nodi presenti nella shape
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (assolute)
                    {
                        if (nd.Name == "Polygon_Assolute" && ReadSides(nd, sides))
                        {
                            e.sides = sides;
                        }
                    }
                    else
                    {
                        if (nd.Name == "Polygon" && ReadSides(nd, sides))
                        {
                            e.sides = sides;
                        }
                    }
                }
                shape.AddEulithes(e.id, e.sides);

                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadEulithe Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("XmlAnalizer.ReadEulithe Error. ", ex);
                return false;
            }
        }

		/// <summary>
		/// Legge i rinforzi
		/// </summary>
		/// <param name="nd">nodo da cui estrarre i valori interessati</param>
		/// <param name="shape">shape alla quale aggiungere i rinforzi</param>
		/// <param name="assolute">indica se devono essere lette le dimensioni assolute</param>
		/// <returns>true	lettura eseguito con successo 			false	errore nella lettura</returns>
		private static bool ReadReinforcement(XmlNode node, ref Shape shape, bool assolute)
		{
			Shape.Reinforcement r = new Shape.Reinforcement();
			ArrayList sides = new ArrayList();

			try
			{
                r.id = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);
                r.depth = double.Parse(node.Attributes.GetNamedItem("Depth").Value, CultureInfo.InvariantCulture);

				// Scorre tutti i nodi presenti nella shape
				foreach (XmlNode nd in node.ChildNodes)
				{
					if (assolute)
					{
						if (nd.Name == "Polygon_Assolute" && ReadSides(nd, sides))
						{
							r.sides = sides;
						}
					}
					else
					{
						if (nd.Name == "Polygon" && ReadSides(nd, sides))
						{
							r.sides = sides;
						}
					}
				}
				shape.AddReinforcement(r.id, r.depth, r.sides);

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadReinforcement Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadReinforcement Error. ", ex);
				return false;
			}
		}

        /***************************************************************************
         * ReadLabel
         * Legge le etichette
         * Parametri:
         *			node		: nodo da cui estrarre i valori interessati
         *			shape		: shape alla quale aggiungere le etichette
         *          assolute    : indica se devono essere lette le dimensioni assolute
         * Ritorna:
         *			true	lettura eseguito con successo
         *			false	errore nella lettura
         * **************************************************************************/
        private static bool ReadLabel(XmlNode node, ref Shape shape, bool assolute)
		{
			Shape.Label l = new Shape.Label();
			ArrayList sides = new ArrayList();
	
			try
			{
                l.id = int.Parse(node.Attributes.GetNamedItem("Id").Value, CultureInfo.InvariantCulture);
				
				// Scorre tutti i nodi presenti nella shape
				foreach(XmlNode nd in node.ChildNodes)
				{
					if(nd.Name == "Origin")
					{
                        l.xOrigin = double.Parse(nd.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
                        l.yOrigin = double.Parse(nd.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
                        l.zOrigin = double.Parse(nd.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
					}
                    if (assolute)
                    {
                        if (nd.Name == "Polygon_Assolute" && ReadSides(nd, sides))
                            l.sides = sides;
                    }
                    else
                    {
                        if (nd.Name == "Polygon" && ReadSides(nd, sides))
                            l.sides = sides;
                    }
				}
                shape.AddLabels(l.id, l.xOrigin, l.yOrigin, l.zOrigin, l.sides);

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadVGroove Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadVGroove Error. ", ex);
				return false;
			}
		}
        ///****************************************************************
        /// ReadLabelXml
        /// <summary>
        /// Legge il file xml delle etichette
        /// </summary>
        /// <param name="doc">XmlDocument su cui effettuare la ricerca</param>
        /// <param name="shape">Shape su cui caricare i dati</param>
        /// <param name="assolute">posizionio assolute</param>
        /// <returns></returns>
        /// ***************************************************************
        private static bool ReadLabelXml(ref XmlDocument doc, ref Shape shape, bool assolute)
        {
            XmlNode labelNode = doc.SelectSingleNode("//Labels");

            foreach (XmlNode nd in labelNode.ChildNodes)
            {
				if (nd.Name == "Label")
					if (!ReadLabel(nd, ref shape, assolute))
						return false;
            }
			if (labelNode.ChildNodes.Count == 0)
				return false;
			else
				return true;
        }

        ///********************************************************************
        /// FindHoleProcessingFromContours
        /// <summary>
        /// Verifica se il foro interessato ha delle lavorazioni (� lucido)
        /// </summary>
        /// <param name="contours">ArrayList con tutte le lavorazioni</param>
        /// <param name="hole">Foro</param>
        /// <returns></returns>
        private static bool FindHoleProcessingFromContours(ArrayList contours, ref Shape.Holes hole)
        {
            Shape.Contour c;

            for (int i = 0; i < contours.Count; i++)
            {
                c = (Shape.Contour)contours[i];
                if (c.processing != "" && c.id == hole.id)
                {
                    hole.polish = true;
                    hole.processing = c.processing;
                    return true;
                }
            }

            return true;
        }

        ///********************************************************************
        /// FindHoleProcessingFromContour
        /// <summary>
        /// Verifica se la lavorazione interessata appartiene ad un foro
        /// </summary>
        /// <param name="holes">ArrayList con tutti i fori</param>
        /// <param name="c">lavorazione</param>
        /// <returns></returns>
        private static bool FindHoleProcessingFromContour(ArrayList holes, Shape.Contour c)
        {
            Shape.Holes h;

            for (int i = 0; i < holes.Count; i++)
            {
                h = (Shape.Holes)holes[i];
                if (c.processing != "" && c.id == h.id)
                {
                    h.polish = true;
                    h.processing = c.processing;
                    return true;
                }
            }

            return true;
        }


		private static bool ReadDimensions(XmlNode node, ref Shape shape, bool assolute)
		{
			Shape.Dimension d = new Shape.Dimension();
			XmlNode dim;

			try
			{
				// Scorre tutti i nodi presenti
				foreach (XmlNode nd in node.ChildNodes)
				{
					if (nd.Name == "Dimension")
					{
						if (nd.Attributes.GetNamedItem("Text") != null)
							d.text = nd.Attributes.GetNamedItem("Text").Value;
						if (nd.Attributes.GetNamedItem("Handle_Dimension") != null)
                            d.handle = int.Parse(nd.Attributes.GetNamedItem("Handle_Dimension").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("TextHeight") != null)
                            d.textHeight = int.Parse(nd.Attributes.GetNamedItem("TextHeight").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("ArrowDim") != null)
                            d.arrowDim = int.Parse(nd.Attributes.GetNamedItem("ArrowDim").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("DecimalNumber") != null)
                            d.decimalNumber = int.Parse(nd.Attributes.GetNamedItem("DecimalNumber").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("Dimension_Type") != null)
                            d.type = (Shape.DimensionType)(int.Parse(nd.Attributes.GetNamedItem("Dimension_Type").Value, CultureInfo.InvariantCulture));


						foreach (XmlNode n in nd.ChildNodes)
						{
							if (nd.Name == "TextPosition")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.textPosition.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.textPosition.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.textPosition.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
							else if (nd.Name == "P1position")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.p1Position.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.p1Position.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.p1Position.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
							else if (nd.Name == "P2position")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.p2Position.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.p2Position.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.p2Position.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
							else if (nd.Name == "vertexPosition")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.vertexPosition.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.vertexPosition.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.vertexPosition.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
						}

						// Aggiunge la dimensione
						shape.AddDimension(d.text, d.handle, d.textHeight, d.arrowDim, d.decimalNumber, d.type, d.textPosition, d.p1Position, d.p2Position, d.vertexPosition);
					}
				}
				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadDimensions Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadDimensions Error. ", ex);
				return false;
			}
		}

		private static bool ReadDimensions(XmlNode node, ref Top top, bool assolute)
		{
			Top.Dimension d = new Top.Dimension();
			XmlNode dim;

			try
			{
				// Scorre tutti i nodi presenti
				foreach (XmlNode nd in node.ChildNodes)
				{
					if (nd.Name == "Dimension")
					{
						if (nd.Attributes.GetNamedItem("Text") != null)
							d.text = nd.Attributes.GetNamedItem("Text").Value;
						if (nd.Attributes.GetNamedItem("Handle_Dimension") != null)
                            d.handle = int.Parse(nd.Attributes.GetNamedItem("Handle_Dimension").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("TextHeight") != null)
                            d.textHeight = int.Parse(nd.Attributes.GetNamedItem("TextHeight").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("ArrowDim") != null)
                            d.arrowDim = int.Parse(nd.Attributes.GetNamedItem("ArrowDim").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("DecimalNumber") != null)
                            d.decimalNumber = int.Parse(nd.Attributes.GetNamedItem("DecimalNumber").Value, CultureInfo.InvariantCulture);
						if (nd.Attributes.GetNamedItem("Dimension_Type") != null)
                            d.type = (Top.DimensionType)int.Parse(nd.Attributes.GetNamedItem("Dimension_Type").Value, CultureInfo.InvariantCulture);

						
						foreach (XmlNode n in nd.ChildNodes)
						{
							if (n.Name == "TextPosition")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.textPosition.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.textPosition.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.textPosition.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
							else if (n.Name == "P1position")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.p1Position.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.p1Position.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.p1Position.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
							else if (n.Name == "P2position")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.p2Position.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.p2Position.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.p2Position.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
							else if (n.Name == "vertexPosition")
							{
								if (assolute)
									dim = n.SelectSingleNode("Coordinata_Assolute");
								else
									dim = n.SelectSingleNode("Coordinata");

								if (dim != null)
								{
									if (dim.Attributes.GetNamedItem("x") != null)
                                        d.vertexPosition.x = double.Parse(dim.Attributes.GetNamedItem("x").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("y") != null)
                                        d.vertexPosition.y = double.Parse(dim.Attributes.GetNamedItem("y").Value, CultureInfo.InvariantCulture);
									if (dim.Attributes.GetNamedItem("z") != null)
                                        d.vertexPosition.z = double.Parse(dim.Attributes.GetNamedItem("z").Value, CultureInfo.InvariantCulture);
								}
							}
						}

						// Aggiunge la dimensione
						top.AddDimension(d.text, d.handle, d.textHeight, d.arrowDim, d.decimalNumber, d.type, d.textPosition, d.p1Position, d.p2Position, d.vertexPosition);
					}
				}
				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadDimensions Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadDimensions Error. ", ex);
				return false;
			}
		}

		private static bool ReadMatrix(XmlNode node, ref Shape shape)
		{
			Shape.Matrix m = new Shape.Matrix();

			try
			{
				if (node.Attributes.GetNamedItem("OffX") != null)
                    m.offsetX = double.Parse(node.Attributes.GetNamedItem("OffX").Value, CultureInfo.InvariantCulture);

				if (node.Attributes.GetNamedItem("OffY") != null)
                    m.offsetY = double.Parse(node.Attributes.GetNamedItem("OffY").Value, CultureInfo.InvariantCulture);

				if (node.Attributes.GetNamedItem("Angle") != null)
                    m.angle = double.Parse(node.Attributes.GetNamedItem("Angle").Value, CultureInfo.InvariantCulture);

				shape.gMatrix = m;

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadMatrix Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("XmlAnalizer.ReadMatrix Error. ", ex);
				return false;
			}
		}
        #endregion
    }
}