using System;
using System.Collections;
using System.Runtime.InteropServices; 

namespace Breton.DesignUtil
{
	public class Shape
	{
		#region Variables
		public enum DimensionType
		{
			NULLO = 0,
			LINEARE = 1,
			DIAMETRO = 2,
			RAGGIO = 3,
			ANGOLARE = 4,
			ORIZZONTALE = 5,
			VERTICALE = 6
		}

		public struct Cut
		{
            public Cut(Cut cut)
            {
                type = cut.type;
                active = cut.active;
                sides = new ArrayList();
                for (int i = 0; i < cut.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)cut.sides[i]);
                    sides.Add(contour);
                }
            }

            public int type;			// indica il tipo di grezzo
			public bool active;			// indica se il grezzo � attivo
			public ArrayList sides;		// linee e archi che formano il grezzo
		}

		public struct Holes
		{
            public Holes(Holes holes)
            {
                id = holes.id;
                active = holes.active;
                polish = holes.polish;
                isBridge = holes.isBridge;
                processing = holes.processing;
                sides = new ArrayList();
                for (int i = 0; i < holes.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)holes.sides[i]);
                    sides.Add(contour);
                }
            }

			public int id;					// id del foro
			public bool active;				// indica se il foro deve essere considerato dal CAM
            public bool polish;             // indica se � un foro lucido
            public bool isBridge;           // indica se il foro deve fatto con i ponticelli
            public string processing;       // tipo di lavorazione 
			public ArrayList sides;			// linee e archi che formano il foro 
		}

		public struct Contour
		{
            public Contour(Contour contour)
            {
                this.id = contour.id;
                this.idContour = contour.idContour;
                this.code = contour.code;
                this.line = contour.line;
                this.pricipalStruct = contour.pricipalStruct;
                this.processing = contour.processing;
                this.state = contour.state;
                this.x0 = contour.x0;
                this.y0 = contour.y0;
                this.z0 = contour.z0;
                this.x1 = contour.x1;
                this.y1 = contour.y1;
                this.z1 = contour.z1;
                this.xC = contour.xC;
                this.yC = contour.yC;
                this.zC = contour.zC;
                this.r = contour.r;
                this.startAngle = contour.startAngle;
                this.endAngle = contour.endAngle;
                this.cw = contour.cw;
                this.straightLine = contour.straightLine;
                this.join = contour.join;
                this.insertMode = contour.insertMode;
                this.subType = contour.subType;
                this.processing_thick = contour.processing_thick;
                this.slope = contour.slope;
                this.apronLink = contour.apronLink;
                this.cutAngle_Angle = contour.cutAngle_Angle;
                this.cutAngle_Height = contour.cutAngle_Height;
                this.cutAngle_Offset = contour.cutAngle_Offset;
				this.throughHole = contour.throughHole;
				this.groove = contour.groove;
				this.pocket = contour.pocket;
            }

			public int id;					// numero che differenzia i gruppi di lavorazione
            public int idContour;			// id del nodo "Line" o del nodo "Arc"
			public string code;				// Codice della structure che contiene la linea o l'arco
            public bool line;				// true se line, false se arc
			public bool pricipalStruct;		// true se Principal_Structure, false se Structure
			public string processing;		// tipo di lavorazione
			public string state;			// stato della lavorazione (se esiste)
			public double x0, y0, z0;		// punto 0
			public double x1, y1, z1;		// punto 1
			public double xC, yC, zC;		// centro arco
			public double r, startAngle,	// raggio, angolo iniziale, angolo finale
				endAngle;
			public int cw;					// indica il senso: orario = 1 o antiorario = 0;
            public bool straightLine;       // indica se il lato interessato � un lato che puo essere lucidato da una macchina rettilinea
            public bool join;               // indica se � un lato di attacco
            public string insertMode;       // modalit� di inserimento nel caso di fori lavabo
            public string subType;          // sottotipo di sagoma
			public double processing_thick;	// Spessore della lavorazione (presente solo nei casi di laminazione di tipo 1)
			public double slope;			// Inclinazione del taglio del grezzo
            public string apronLink;        // link alla veletta
            public double cutAngle_Angle;   // Angolo veletta
            public double cutAngle_Height;  // Altezza veletta
            public double cutAngle_Offset;  // Offset veletta
			public bool throughHole;		// indica se il lato fa parte di un foro passante
			public bool groove;				// indica se il lato � un canaletto
			public bool pocket;				// indica se il lato fa parte di un ribasso/tasca
        }

		public struct VGroove
		{
            public VGroove(VGroove vGroove)
            {
                this.id = vGroove.id;
                this.type = vGroove.type;
                this.height = vGroove.height;
                this.length = vGroove.length;
                this.depth = vGroove.depth;
                sides = new ArrayList();
                for (int i = 0; i < vGroove.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)vGroove.sides[i]);
                    sides.Add(contour);
                }
            }

			public int id;							// id del VGroove
			public string type;						// tipo di VGroove
            public double height, length, depth;	// altezza, lunghezza, profondit� VGroove
            public ArrayList sides;					// lati della lavorazione VGroove
		}

		public struct Reinforcement
		{
            public Reinforcement(Reinforcement reinforcement)
            {
                this.id = reinforcement.id;
                this.depth = reinforcement.depth;
                sides = new ArrayList();
                for (int i = 0; i < reinforcement.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)reinforcement.sides[i]);
                    sides.Add(contour);
                }
            }

			public int id;					// id del rinforzo
			public double depth;			// profondit� del rinforzo
			public ArrayList sides;         // lati del rinforzo
		}

        public struct Eulithe
        {
            public Eulithe(Eulithe eulithe)
            {
                this.id = eulithe.id;
                sides = new ArrayList();
                for (int i = 0; i < eulithe.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)eulithe.sides[i]);
                    sides.Add(contour);
                }
            }

            public int id;					// id dell'eulithe
            public ArrayList sides;         // lati della lavorazione eulithe
        }

		public struct Symbol
		{
            public Symbol(Symbol symbol)
            {
                this.id = symbol.id;
                this.type = symbol.type;
                sides = new ArrayList();
                for (int i = 0; i < symbol.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)symbol.sides[i]);
                    sides.Add(contour);
                }
            }

			public int id;					// id del simbolo generico
			public string type;				// tipo di simbolo
			public ArrayList sides;         // lati del simbolo
		}

		public struct Label
		{
            public Label(Label label)
            {
                this.id = label.id;
                this.xOrigin = label.xOrigin;
                this.yOrigin = label.yOrigin;
                this.zOrigin = label.zOrigin;
                sides = new ArrayList();
                for (int i = 0; i < label.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)label.sides[i]);
                    sides.Add(contour);
                }
            }

			public int id;					// id dell'etichetta
			public double xOrigin, yOrigin,	// coordinate x,y e z dell'origine
				zOrigin;
			public ArrayList sides;			// linee e archi che formano l'etichetta
		}

		public struct Lamination 
		{
            public Lamination(Lamination lamination)
            {
                this.ShapeNumber = lamination.ShapeNumber;
            }

			public int ShapeNumber;			// id della shape che rappresenta il pezzo di laminazione
		}

        public struct Text
        {
            public Text(Text text)
            {
                this.handle = text.handle;
                this.text = text.text;
                this.charType = text.charType;
                this.dimension = text.dimension;
                this.r = text.r;
                this.g = text.g;
                this.b = text.b;
                this.x = text.x;
                this.y = text.y;
                this.z = text.z;
            }

            public int handle;              // indice del testo
            public string text;             // testo
            public string charType;         // font testo
            public int dimension;           // dimensione del testo
            public byte r, g, b;            // red green blue
            public double x, y, z;          // posizione del testo
        }

        public struct Bussola
        {
            public Bussola(Bussola bussola)
            {
                this.id = bussola.id;
                sides = new ArrayList();
                for (int i = 0; i < bussola.sides.Count; i++)
                {
                    Contour contour = new Contour((Contour)bussola.sides[i]);
                    sides.Add(contour);
                }
            }

            public int id;					// id del foro bussola
            public ArrayList sides;			// linee e archi che formano il foro bussola
        }

		public struct Dimension
		{
            public Dimension(Dimension dimension)
            {
                this.text = dimension.text;
                this.handle = dimension.handle;
                this.textHeight = dimension.textHeight;
                this.arrowDim = dimension.arrowDim;
                this.decimalNumber = dimension.decimalNumber;
                this.type = dimension.type;
                this.textPosition = new Coordinata(dimension.textPosition);
                this.p1Position = new Coordinata(dimension.p1Position);
                this.p2Position = new Coordinata(dimension.p2Position);
                this.vertexPosition = new Coordinata(dimension.vertexPosition);
            }

			public string text;				// testo della dimensione
			public int handle;				// indice della dimensione
			public int textHeight;			// altezza del testo
			public int arrowDim;			// dimensione delle frecce
			public int decimalNumber;		// cifre decimali
			public DimensionType type;		// tipo di dimensione

			public Coordinata textPosition;	
			public Coordinata p1Position;
			public Coordinata p2Position;
			public Coordinata vertexPosition;
		}

		public struct Coordinata
		{
            public Coordinata(Coordinata coordinata)
            {
                this.x = coordinata.x;
                this.y = coordinata.y;
                this.z = coordinata.z;
            }
			public double x, y, z;			// Valori coordinata
		}

        public struct Veletta
        {
            public Veletta(Veletta veletta)
            {
                this.link = veletta.link;
                this.height = veletta.height;
                this.offset = veletta.offset;
            }
            public string link;					// id della veletta
            public double height;			    // profondit� della veletta
            public double offset;               // offset della veletta
        }

		public struct Matrix
		{
            public Matrix(Matrix matrix)
            {
                this.offsetX = matrix.offsetX;
                this.offsetY = matrix.offsetY;
                this.angle = matrix.angle;
            }
			public double offsetX, offsetY, angle;
		}
		
		private int mId;					// id della shape all'interno del top
        private int mShapeNumber;           // id univoco all'interno dell'ordine;
        private string mCode;
		private string mDescription;		// descrizione della shape
		private string mMaterialCode;		// materiale
		private double mThickness;			// spessore
        private double mAngle;  			// angolo assoluto della sagoma
        private bool mBackSplash;           // true = backsplash
        private int mLaminationType;        // tipo di laminazione
        private int mShapeNumberJoin;       // indica il pezzo a cui si fa riferimento (nel caso sia una laminazione) "ShapeNumber"
        private int mShapeIdJoin;           // indica il pezzo a cui si fa riferimento (nel caso sia un'alzatina) "Id"
        private bool isVeletta;             // true = veletta
        private bool hasVeletta;            // true = veletta presente su uno dei lati della sagoma
        private PieceType mType;            // tipo di pezzo
        private Veletta mVeletta;           // Dati veletta
        private Matrix mMatrix;				// Matrice di rototraslazione del pezzo

		private ArrayList mCutToSize;		// grezzi
		private ArrayList mHolesToCut;		// fori
		private ArrayList mContours;		// lavorazioni
		private ArrayList mDistinctContour;	// lavorazioni che compaiono una volta sola
        private ArrayList mVGrooves;		// lavorazioni VGroove
		private ArrayList mReinforcements;	// Rinforzi
		private ArrayList mSymbols;		// lavorazioni Eulithe
        private ArrayList mEulithes;		// lavorazioni Eulithe
		private ArrayList mLabels;			// etichette
        private ArrayList mTexts;           // testi
        private ArrayList mBussole;         // fori bussola
		private ArrayList mDimensions;		// dimensioni
		private ArrayList mLamination1;		// Laminazione di tipo 1
		private ArrayList mLamination2;		// Laminazione di tipo 2

        public enum PieceType
        {
            NORMAL,
            VELETTA,
            LAMINATION
        }
		#endregion

		#region Constructor
		public Shape()
		{
			mId = 0;
            mCode = "";
            mShapeNumber = 0;
			mDescription = "";
			mMaterialCode = "";
			mThickness = 0d;
            mAngle = 0d;
            mBackSplash = false;
            mLaminationType = 0;
            mShapeNumberJoin = 0;
            mType = PieceType.NORMAL;
            isVeletta = false;
            hasVeletta = false;

			mCutToSize = new ArrayList();
			mHolesToCut = new ArrayList();
			mContours = new ArrayList();
            mDistinctContour = new ArrayList();
			mVGrooves = new ArrayList();
			mReinforcements = new ArrayList();
			mSymbols = new ArrayList();
            mEulithes = new ArrayList();
			mLabels = new ArrayList();
            mTexts = new ArrayList();
            mBussole = new ArrayList();
			mDimensions = new ArrayList();
			mLamination1 = new ArrayList();
			mLamination2 = new ArrayList();
		}

        public Shape(Shape shape)
        {
            mId = shape.mId;
            mCode = shape.mCode;
            mShapeNumber = shape.mShapeNumber;
            mDescription = shape.mDescription;
            mMaterialCode = shape.mMaterialCode;
            mThickness = shape.mThickness;
            mAngle = shape.mAngle;
            mBackSplash = shape.mBackSplash;
            mLaminationType = shape.mLaminationType;
            mShapeNumberJoin = shape.mShapeNumberJoin;
            mType = shape.mType;
            isVeletta = shape.isVeletta;
            hasVeletta = shape.hasVeletta;

            mVeletta = new Veletta(shape.mVeletta);

            mCutToSize = new ArrayList();
            for (int i = 0; i < shape.mCutToSize.Count; i++)
            {
                Cut cut = new Cut((Cut)shape.mCutToSize[i]);
                mCutToSize.Add(cut);
            }

            mHolesToCut = new ArrayList();
            for (int i = 0; i < shape.mHolesToCut.Count; i++)
            {
                Holes holes = new Holes((Holes)shape.mHolesToCut[i]);
                mHolesToCut.Add(holes);
            }

            mContours = new ArrayList();
            for (int i = 0; i < shape.mContours.Count; i++)
            {
                Contour contour = new Contour((Contour)shape.mContours[i]);
                mContours.Add(contour);
            }

            mDistinctContour = new ArrayList();
            for (int i = 0; i < shape.mDistinctContour.Count; i++)
            {
                string processing = (string)shape.mDistinctContour[i];
                mDistinctContour.Add(processing);
            }

            mVGrooves = new ArrayList();
            for (int i = 0; i < shape.mVGrooves.Count; i++)
            {
                VGroove vGroove = new VGroove((VGroove)shape.mVGrooves[i]);
                mVGrooves.Add(vGroove);
            }

            mReinforcements = new ArrayList();
            for (int i = 0; i < shape.mReinforcements.Count; i++)
            {
                Reinforcement reinforcement = new Reinforcement((Reinforcement)shape.mReinforcements[i]);
                mReinforcements.Add(reinforcement);
            }

            mSymbols = new ArrayList();
            for (int i = 0; i < shape.mSymbols.Count; i++)
            {
                Symbol symbol = new Symbol((Symbol)shape.mSymbols[i]);
                mSymbols.Add(symbol);
            }

            mEulithes = new ArrayList();
            //for (int i = 0; i < shape.mDistinctContour.Count; i++)
			for (int i = 0; i < shape.gEulithes.Count; i++)
            {
                Symbol symbol = new Symbol((Symbol)shape.mEulithes[i]);
                mEulithes.Add(symbol);
            }
			
            mLabels = new ArrayList();
            for (int i = 0; i < shape.mLabels.Count; i++)
            {
                Label label = new Label((Label)shape.mLabels[i]);
                mLabels.Add(label);
            }

            mTexts = new ArrayList();
            for (int i = 0; i < shape.mTexts.Count; i++)
            {
                Text text = new Text((Text)shape.mTexts[i]);
                mTexts.Add(text);
            }

            mBussole = new ArrayList();
            for (int i = 0; i < shape.mBussole.Count; i++)
            {
                Bussola bussola = new Bussola((Bussola)shape.mBussole[i]);
                mBussole.Add(bussola);
            }

            mDimensions = new ArrayList();
            for (int i = 0; i < shape.mDimensions.Count; i++)
            {
                Dimension dimension = new Dimension((Dimension)shape.mDimensions[i]);
                mDimensions.Add(dimension);
            }

            mLamination1 = new ArrayList();
            for (int i = 0; i < shape.mLamination1.Count; i++)
            {
                Lamination lamination = new Lamination((Lamination)shape.mLamination1[i]);
                mLamination1.Add(lamination);
            }

            mLamination2 = new ArrayList();
            for (int i = 0; i < shape.mLamination2.Count; i++)
            {
                Lamination lamination = new Lamination((Lamination)shape.mLamination2[i]);
                mLamination2.Add(lamination);
            }
        }
		#endregion

		#region Properties
		public int gId
		{
			set{ mId = value;}
			get{ return mId;}
		}

        public int gShapeNumber
        {
            set { mShapeNumber = value; }
            get { return mShapeNumber; }
        }

        public string gCode
        {
            set { mCode = value; }
            get { return mCode; }
        }

		public string gDescription
		{
			set{ mDescription = value;}
			get{ return mDescription;}
		}

		public string gMaterialCode
		{
			set{ mMaterialCode = value;}
			get{ return mMaterialCode;}
		}
		
		public double gThickness
		{
			set{ mThickness = value;}
			get{ return mThickness;}
		}

        public double gAngle
        {
            set { mAngle = value; }
            get { return mAngle; }
        }

        public bool gBackSplash
        {
            set { mBackSplash = value; }
            get { return mBackSplash; }
        }

        public int gLaminationType
        {
            set { mLaminationType = value; }
            get { return mLaminationType; }
        }

        public int gShapeNumberJoin
        {
            set { mShapeNumberJoin = value; }
            get { return mShapeNumberJoin; }
        }

        public int gShapeIdJoin
        {
            set { mShapeIdJoin = value; }
            get { return mShapeIdJoin; }
        }

        public PieceType gType
        {
            set { mType = value; }
            get { return mType; }
        }

        public bool IsVeletta
        {
            set { isVeletta = value; }
            get { return isVeletta; }
        }

        public bool HasVeletta
        {
            set { hasVeletta = value; }
            get { return hasVeletta; }
        }

        public Veletta gVeletta
        {
            set { mVeletta = value; }
            get { return mVeletta; }
        }

		public Matrix gMatrix
		{
			set { mMatrix = value; }
			get { return mMatrix; }
		}

		public ArrayList gCutToSize
		{
			set{ mCutToSize = value;}
			get{ return mCutToSize;}
		}

		public ArrayList gHolesToCut
		{
			set{ mHolesToCut = value;}
			get{ return mHolesToCut;}
		}

		public ArrayList gContours
		{
			set{ mContours = value;}
			get{ return mContours;}
		}

	    public ArrayList gDistinctContour
		{
			set{ mDistinctContour = value;}
			get{ return mDistinctContour;}
		}

		public ArrayList gSymbols
		{
			set { mSymbols = value; }
			get { return mSymbols; }
		}

		public ArrayList gEulithes
		{
			set{ mEulithes = value;}
            get { return mEulithes; }
		}

        public ArrayList gVGrooves
        {
            set { mVGrooves = value; }
            get { return mVGrooves; }
        }

		public ArrayList gReinforcements
		{
			set { mReinforcements = value; }
			get { return mReinforcements; }
		}

		public ArrayList gLabels
		{
			set{ mLabels = value;}
			get{ return mLabels;}
		}

        public ArrayList gTexts
        {
            set { mTexts = value; }
            get { return mTexts; }
        }

        public ArrayList gBussole
        {
            set { mBussole = value; }
            get { return mBussole; }
        }

		public ArrayList gDimensions
		{
			set { mDimensions = value; }
			get { return mDimensions; }
		}

		public ArrayList gLamination1
		{
			set { mLamination1 = value; }
			get { return mLamination1; }
		}

		public ArrayList gLamination2
		{
			set { mLamination2 = value; }
			get { return mLamination2; }
		}
		#endregion

		#region Public Methods
        /// ******************************************************************************************
        /// AddCutToSize
        /// <summary>
        /// Aggiunge un grezzo alla lista
        /// </summary>
        /// <param name="type">tipo di grezzo</param>
        /// <param name="active">stato del grezzo</param>
        /// <param name="s">collection dei lati del grezzo</param>
        /// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddCutToSize(int type, bool active, ArrayList s)
		{
			Cut newCut = new Cut();
			newCut.type = type;
			newCut.active = active;
			newCut.sides = s;

			// Aggiunge la struttura alla lista
			if(mCutToSize.Add(newCut) >= 0)	return true;
			else							return false;
		}

        /// ******************************************************************************************
        /// AddHolesToCut
        /// <summary>
        /// Aggiunge un foro alla lista
        /// </summary>
        /// <param name="id">indice del foro</param>
        /// <param name="active">indica se il foro deve essere considerato dal CAM</param>
        /// <param name="s">collection dei lati del foro</param>
        /// <param name="pol">indica se si tratta di un foro lucido</param>
        /// <param name="proc">tipo di lavorazione</param>
        /// <param name="bridge">indica se il foro deve fatto con i ponticelli</param>
        /// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddHolesToCut(int id, bool active, ArrayList s, bool pol, string proc, bool bridge)
		{
			Holes newHole = new Holes();
			newHole.id = id;
			newHole.active = active;
			newHole.sides = s;
            newHole.polish = pol;
            newHole.processing = proc;
            newHole.isBridge = bridge;

			// Aggiunge la struttura alla lista
			if(mHolesToCut.Add(newHole) >= 0)	return true;
			else								return false;
		}

		/// *******************************************************************************************
        /// AddContours
        /// <summary>
        /// Aggiunge una lavorazione alla lista
		/// </summary>
        /// <param name="id">indica l'indice del gruppo di lavorazioni</param>
        /// <param name="idContour">indica l'indice del lato</param>
		/// <param name="code">codice della structure che contiente la linea o l'arco</param>
        /// <param name="line">indica se si tratta di una linea o di un'arco</param>
        /// <param name="pricipalStruct">indica se si tratta della struttura principale o no</param>
        /// <param name="processing">tipo di lavorazione</param>
		/// <param name="state">stato della lavorazione (se esiste)</param>
		/// <param name="X0">x del punto 0</param>
        /// <param name="Y0">y del punto 0</param>
        /// <param name="Z0">z del punto 0</param>
        /// <param name="X1">x del punto 1</param>
        /// <param name="Y1">y del punto 1</param>
        /// <param name="Z1">z del punto 1</param>
		/// <param name="Xc">x del centro</param>
        /// <param name="Yc">y del centro</param>
        /// <param name="Zc">z del centro</param>
		/// <param name="r">raggio</param>
		/// <param name="startA">angolo di partenza</param>
		/// <param name="endA">angolo finale</param>
		/// <param name="cw">senso della figura</param>
        /// <returns>true	inserimento eseguito con successo  false	errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddContours(int id, int idContour, string code, bool line, bool pricipalStruct, string processing, string state, double X0, double Y0, double Z0, double X1, double Y1,
            double Z1, double Xc, double Yc, double Zc, double r, double startA, double endA, int cw, bool join, string insertMode, string subType, double processing_thick, double slope, 
			double cutAngle_Angle, double cutAngle_Height, double cutAngle_Offset, string apronLink, bool throughHole, bool pocket, bool groove)
		{
			Contour newContour = new Contour();
			newContour.id = id;
            newContour.idContour = idContour;
			newContour.code = code;
			newContour.line = line;
			newContour.pricipalStruct = pricipalStruct;
			newContour.processing = processing;
			newContour.state = state;
			newContour.x0 = X0;
			newContour.y0 = Y0;
			newContour.z0 = Z0;
			newContour.x1 = X1;
			newContour.y1 = Y1;
			newContour.z1 = Z1;
			newContour.xC = Xc;
			newContour.yC = Yc;
			newContour.zC = Zc;
			newContour.r = r;
			newContour.startAngle = startA;
			newContour.endAngle = endA;
			newContour.cw = cw;
            newContour.straightLine = false;
            newContour.join = join;
            newContour.insertMode = insertMode;
            newContour.subType = subType;
			newContour.processing_thick = processing_thick;
			newContour.slope = slope;
            newContour.apronLink = apronLink;
            newContour.cutAngle_Angle = cutAngle_Angle;
            newContour.cutAngle_Height = cutAngle_Height;
            newContour.cutAngle_Offset = cutAngle_Offset;
			newContour.throughHole = throughHole;
			newContour.pocket = pocket;
			newContour.groove = groove;

			// Aggiunge la struttura alla lista delle lavorazioni singole
			if(processing != "" && !ExistProcessing(mContours, processing))
				mDistinctContour.Add(processing);

			// Aggiunge la struttura alla lista
			if(mContours.Add(newContour) >= 0)	return true;
			else								return false;
		}

        /// ******************************************************************************************
        /// AddVGrooves
        /// <summary>
        /// Aggiunge una lavorazione VGroove
        /// </summary>
        /// <param name="id">indice della lavorazione vgroove</param>
        /// <param name="h">altezza della lavorazione vgroove</param>
        /// <param name="l">lunghezza della lavorazione vgroove</param>
		/// <param name="d">profondit� della lavorazione vgroove</param>
        /// <param name="s">collection delle linee di vgroove</param>
        /// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddVGrooves(int id, string type, double h, double l, double d, ArrayList s)
		{
			VGroove newVGR = new VGroove();
			newVGR.id = id;
			newVGR.type = type;
            newVGR.height = h;
            newVGR.length = l;
			newVGR.depth = d;
            newVGR.sides = s;

			// Aggiunge la struttura alla lista
			if(mVGrooves.Add(newVGR) >= 0)	return true;
			else							return false;
		}

		/// ******************************************************************************************
		/// AddReinforcement
		/// <summary>
		/// Aggiunge un rinforzo
		/// </summary>
		/// <param name="id">indice del rinforzo</param>
		/// <param name="d">profondit� del rinforzo</param>
		/// <param name="s">collection delle linee di rinforzo</param>
		/// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
		/// *******************************************************************************************
		public bool AddReinforcement(int id, double d, ArrayList s)
		{
			Reinforcement newReinf = new Reinforcement();
			newReinf.id = id;
			newReinf.depth = d;
			newReinf.sides = s;

			// Aggiunge la struttura alla lista
			if (mReinforcements.Add(newReinf) >= 0) return true;
			else return false;
		}

		/// ******************************************************************************************
		/// AddSymbols
		/// <summary>
		/// Aggiunge un simbolo
		/// </summary>
		/// <param name="id">indice del simbolo</param>
		/// <param name="s">collection delle linee del simbolo</param>
		/// <returns>true	inserimento eseguito con successo   false errore nell'inserimento</returns>
		/// *******************************************************************************************
		public bool AddSymbols(int id, string type, ArrayList s)
		{
			Symbol newSym = new Symbol();
			newSym.id = id;
			newSym.type = type;
			newSym.sides = s;

			// Aggiunge la struttura alla lista
			if (mSymbols.Add(newSym) >= 0) return true;
			else return false;
		}

        /// ******************************************************************************************
        /// AddEulithes
        /// <summary>
        /// Aggiunge una stampa Eulithe
        /// </summary>
        /// <param name="id">indice della stampa eulithe</param>
        /// <param name="s">collection delle linee di eulithe</param>
        /// <returns>true	inserimento eseguito con successo   false errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddEulithes(int id, ArrayList s)
        {
            Eulithe newEUL = new Eulithe();
            newEUL.id = id;
            newEUL.sides = s;

            // Aggiunge la struttura alla lista
            if (mEulithes.Add(newEUL) >= 0) return true;
            else return false;
        }

        /// ******************************************************************************************
        /// AddLabels
        /// <summary>
        /// Aggiunge un etichetta
        /// </summary>
        /// <param name="id">Indice dell'etichetta testo</param>
        /// <param name="x">origine x</param>
        /// <param name="y">origine y</param>
        /// <param name="z">origine z</param>
        /// <param name="s">collection dei lati dell'etichetta</param>
        /// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddLabels(int id, double x, double y, double z, ArrayList s)
		{
			Label lbl = new Label();
			lbl.id = id;
			lbl.xOrigin = x;
			lbl.yOrigin = y;
			lbl.zOrigin = z;
			lbl.sides = s;

			// Aggiunge la struttura alla lista
			if(mLabels.Add(lbl) >= 0)	return true;
			else						return false;
		}

        /// ******************************************************************************************
        /// AddText
        /// <summary>
        /// Aggiunge un testo
        /// </summary>
        /// <param name="handle">Indice del testo</param>
        /// <param name="text">testo</param>
        /// <param name="charType">font del testo</param>
        /// <param name="dim">dimensione testo</param>
        /// <param name="r">red</param>
        /// <param name="g">green</param>
        /// <param name="b">blue</param>
        /// <param name="x">posizione x</param>
        /// <param name="y">posizione y</param>
        /// <param name="z">posizione z</param>
        /// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddText(int handle, string text, string charType, int dim, byte r, byte g, byte b, double x, double y, double z)
        {
            Text txt = new Text();
            txt.handle = handle;
            txt.text = text;
            txt.charType = charType;
            txt.dimension = dim;
            txt.r = r;
            txt.g = g;
            txt.b = b;
            txt.x = x;
            txt.y = y;
            txt.z = z;
            
            // Aggiunge la struttura alla lista
            if (mTexts.Add(txt) >= 0)   return true;
            else                        return false;
        }

        /// ******************************************************************************************
        /// AddBussola
        /// <summary>
        /// Aggiunge un foro bussola alla lista
        /// </summary>
        /// <param name="id">indice del foro bussola</param>
        /// <param name="s">collection dei lati del foro bussola</param>
        /// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
        /// *******************************************************************************************
        public bool AddBussola(int id, ArrayList s)
        {
            Bussola newBus = new Bussola();
            newBus.id = id;
            newBus.sides = s;

            // Aggiunge la struttura alla lista
            if (mBussole.Add(newBus) >= 0)  return true;
            else                            return false;
        }

		/// <summary>
		/// Aggiunge una dimensione alla lista
		/// </summary>
		/// <param name="text">testo</param>
		/// <param name="handle">indice</param>
		/// <param name="textHeight">altezza del testo</param>
		/// <param name="arrowDim">dimensione delle frecce</param>
		/// <param name="decimalNumber">cifre decimali</param>
		/// <param name="type">tipo</param>
		/// <param name="textPosition">posizione testo</param>
		/// <param name="p1Position">posizione punto 1</param>
		/// <param name="p2Position">posizione punto 2</param>
		/// <param name="vertexPosition">posizione vertice</param>
		/// <returns></returns>
		public bool AddDimension(string text, int handle, int textHeight, int arrowDim, int decimalNumber, DimensionType type,
				Coordinata textPosition, Coordinata p1Position, Coordinata p2Position, Coordinata vertexPosition)
		{
			Dimension d = new Dimension();

			d.text = text;
			d.handle = handle;
			d.textHeight = textHeight;
			d.arrowDim = arrowDim;
			d.decimalNumber = decimalNumber;
			d.type = type;
			d.textPosition = textPosition;
			d.p1Position = p1Position;
			d.p2Position = p2Position;
			d.vertexPosition = vertexPosition;

			// Aggiunge la struttura alla lista
			if (mDimensions.Add(d) > 0) return true;
			else return false;
		}

		/// ******************************************************************************************
		/// AddLamination1
		/// <summary>
		/// Aggiunge una laminazione di tipo 1 alla lista
		/// </summary>
		/// <param name="shapeNumber">indice della shape che rappresenta il pezzo di laminazione</param>
		/// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
		/// *******************************************************************************************
		public bool AddLamination1(int shapeNumber)
		{
			Lamination newLam = new Lamination();
			newLam.ShapeNumber = shapeNumber;
	
			// Aggiunge la struttura alla lista
			if (mLamination1.Add(newLam) >= 0)	return true;
			else								return false;
		}

		/// ******************************************************************************************
		/// AddLamination2
		/// <summary>
		/// Aggiunge una laminazione di tipo 2 alla lista
		/// </summary>
		/// <param name="shapeNumber">indice della shape che rappresenta il pezzo di laminazione</param>
		/// <returns>true	inserimento eseguito con successo false	errore nell'inserimento</returns>
		/// *******************************************************************************************
		public bool AddLamination2(int shapeNumber)
		{
			Lamination newLam = new Lamination();
			newLam.ShapeNumber = shapeNumber;

			// Aggiunge la struttura alla lista
			if (mLamination2.Add(newLam) >= 0) return true;
			else return false;
		}
		#endregion

		#region Private Methods
		/// *************************************************************************
        /// ExistProcessing
        /// <summary>
        /// Verifica se la lavorazione esiste nella lista
		/// </summary>
        /// <param name="a">arrayList sul quale si effettua la verifica</param>
        /// <param name="p">lavorazione da ricercare</param>
        /// <returns>true esiste    false non esiste</returns>
        /// *************************************************************************
        private bool ExistProcessing(ArrayList a, string p)
		{
			Contour c;

			for(int i=0;i<a.Count;i++)
			{
				c = (Contour)a[i];
				if(c.processing == p)
					return true;
			}

			return false;
		}

		#endregion
	}
}
