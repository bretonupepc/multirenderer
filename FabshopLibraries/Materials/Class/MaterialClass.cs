using System;
using System.Xml;
using DbAccess;
using TraceLoggers;

namespace Materials.Class
{
	public class MaterialClass: DbObject
	{
		#region Variables

		public const string CODE_PREFIX = "MATCL";
		public const string DEFAULT_CLASS = "DEFAULTCLAS";

		private int mId;
		private string mCode;
		private string mDescription;
		private double mSpecificGravity=0D;
		private string mType;


		public enum Type
		{
			NONE = -1,
			MATCLTYPE_COMPOUND = 0,		// Compound stone
			MATCLTYPE_NATURAL,			// Natural stone
			MATCLTYPE_CERAMIC,			// Ceramic stone
			MAX
		}


		public enum Keys {F_NONE = -1, F_ID, F_CODE, F_DESCRIPTION, F_SPECIFIC_GRAVITY, F_TYPE};

		private static string[] mKeys = {"F_ID", "F_CODE", "F_DESCRIPTION", "F_SPECIFIC_GRAVITY", "F_TYPE"};

		#endregion

		#region Constructors
		public MaterialClass()
			: this(0, "", "", 0D)
		{
		}

		public MaterialClass(int id, string code, string description, double specificGravity)
			: this(id, code, description, specificGravity, "")
		{
		}

		public MaterialClass(int id, string code, string description, double specificGravity, string type)
		{
			mId = id;
			mCode = code;
			mDescription = description;
			mSpecificGravity = specificGravity;
			mType = type;
		}	

		#endregion

		#region Properties
		public int gId
		{
			set
			{
				mId = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mId; }
		}

		public string gCode
		{
			set 
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public string gDescription
		{
			set 
			{
				mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		public double gSpecificGravity
		{
			set 
			{
				mSpecificGravity = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mSpecificGravity; }
		}

		public string gType
		{
			set
			{
				mType = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mType; }
		}
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is MaterialClass)
			{
				MaterialClass mat = (MaterialClass) obj;

				return mCode.CompareTo(mat.gCode);
			}

			throw new ArgumentException("object is not a MaterialClass");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(mKeys[index]);
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_ID])
				return mId.GetTypeCode();
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_CODE])
				return mCode.GetTypeCode();
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_DESCRIPTION])
				return mDescription.GetTypeCode();
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_SPECIFIC_GRAVITY])
				return mDescription.GetTypeCode();
			if (key.ToUpper() == mKeys[(int)MaterialClass.Keys.F_TYPE])
				return mType.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out object retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(mKeys[index], out retValue);
		}
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(mKeys[index], out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(mKeys[index], out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(mKeys[index], out retValue);
		}
	
		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out object retValue)
		{
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_ID])
			{
				retValue = (object) mId;
				return true;
			}

			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_CODE])
			{
				retValue = (object) mCode;
				return true;
			}

			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_DESCRIPTION])
			{
				retValue = (object) mDescription;
				return true;
			}

			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_SPECIFIC_GRAVITY])
			{
				retValue = (object) mSpecificGravity;
				return true;
			}

			if (key.ToUpper() == mKeys[(int)MaterialClass.Keys.F_TYPE])
			{
				retValue = (object)mType;
				return true;
			}

			return base.GetField(key, out retValue);
		}
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_ID])
			{
				retValue = mId;
				return true;
			}

			return base.GetField(key, out retValue);
		}
		public override bool GetField(string key, out double retValue)
		{
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_SPECIFIC_GRAVITY])
			{
				retValue = mSpecificGravity;
				return true;
			}

			return base.GetField(key, out retValue);
		}
		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_ID])
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_CODE])
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_DESCRIPTION])
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == mKeys[(int) MaterialClass.Keys.F_SPECIFIC_GRAVITY])
			{
				retValue = mSpecificGravity.ToString();
				return true;
			}

			if (key.ToUpper() == mKeys[(int)MaterialClass.Keys.F_TYPE])
			{
				retValue = mType;
				return true;
			}

			return base.GetField(key, out retValue);
		}

        /// <summary>
        /// Legge le informazioni riguardanti la classe di materiale
        /// </summary>
        /// <param name="fileXML">file da cui leggere</param>
        /// <returns></returns>
        public bool ReadFileXml(string fileXML)
        {
            XmlDocument doc = new XmlDocument();
            XmlNodeList l;

            try
            {
                // Legge il file XML
                doc.Load(fileXML);

                l = doc.SelectNodes("/Export/*");
                if (l != null && l.Count > 0)
                {
                    // Scorre tutti i sottonodi
                    foreach (XmlNode chn in l)
                    {
                        // Legge le informazioni della classe di materiale
                        if (chn.Name == "Material_class")
                        {
                            if (mCode == "" && chn.Attributes.GetNamedItem("Code") != null)
                                mCode = chn.Attributes.GetNamedItem("Code").Value;

                            if (mDescription == "" && chn.Attributes.GetNamedItem("Description") != null)
                                mDescription = chn.Attributes.GetNamedItem("Description").Value;

                            if (mSpecificGravity == 0d && chn.Attributes.GetNamedItem("Specific_gravity") != null)
                                mSpecificGravity = double.Parse(chn.Attributes.GetNamedItem("Specific_gravity").Value);

							if (mType == "" && chn.Attributes.GetNamedItem("Type") != null)
								mType = chn.Attributes.GetNamedItem("Type").Value;
                        }
                    }
                    return true;
                }

                l = doc.SelectNodes("/Slab/GeneralInfo/*");
                if (l != null && l.Count > 0)
                {
                    // Scorre tutti i sottonodi
                    foreach (XmlNode chn in l)
                    {
                        // Legge le informazioni della classe di materiale
                        if (chn.Name == "Material_class")
                        {
                            if (mCode == "" && chn.Attributes.GetNamedItem("Code") != null)
                                mCode = chn.Attributes.GetNamedItem("Code").Value;

                            if (mDescription == "" && chn.Attributes.GetNamedItem("Description") != null)
                                mDescription = chn.Attributes.GetNamedItem("Description").Value;

                            if (mSpecificGravity == 0d && chn.Attributes.GetNamedItem("Specific_gravity") != null)
                                mSpecificGravity = double.Parse(chn.Attributes.GetNamedItem("Specific_gravity").Value);

							if (mType == "" && chn.Attributes.GetNamedItem("Type") != null)
								mType = chn.Attributes.GetNamedItem("Type").Value;
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("MaterialClass.ReadFileXml error.", ex);

                return false;
            }
            finally
            {
                doc = null;
            }
        }

        /// <summary>
        /// Scrive la classe di materiali su un nodo xml
        /// </summary>
        /// <param name="n">nodo sul quale scrivere</param>
        /// <returns></returns>
        public bool WriteFileXml(XmlDocument doc, XmlNode n)
        {          
            try
            {
                // Crea il nodo principale
                XmlNode f = doc.CreateElement("Material_class");
                XmlAttribute code = doc.CreateAttribute("Code");
                XmlAttribute descr = doc.CreateAttribute("Description");
                XmlAttribute specif = doc.CreateAttribute("Specific_gravity");
				XmlAttribute type = doc.CreateAttribute("Type");

                code.Value = mCode;
                descr.Value = mDescription;
                specif.Value = mSpecificGravity.ToString();
				type.Value = mType;

                f.Attributes.Append(code);
                f.Attributes.Append(descr);
                f.Attributes.Append(specif);
				f.Attributes.Append(type);

                // Aggiunge il nodo
                n.AppendChild(f);

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("MaterialClass.WriteFileXml error.", ex);

                return false;
            }        
        }

        /// <summary>
        /// Scrive la classe di materiali su un file xml
        /// </summary>
        /// <param name="fileName">nome del file su cui salvare</param>
        /// <returns></returns>
        public bool WriteFileXml(string fileName)
        {
            XmlDocument writeDoc = new XmlDocument();

            try
            {
                //Write the XML delcaration.
                XmlNode f = writeDoc.CreateXmlDeclaration("1.0", "utf-16", "");
                writeDoc.AppendChild(f);

                // Crea il nodo principale
                f = writeDoc.CreateElement("Export");
                writeDoc.AppendChild(f);

                if (!WriteFileXml(writeDoc, f))
                    return false;

                writeDoc.Save(fileName);
                return true;                    
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("MaterialClass.WriteFileXml error.", ex);

                return false;
            }
            finally
            {
                writeDoc = null;
            }

        }
		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index >= mKeys.GetLowerBound(0) && index <= mKeys.GetUpperBound(0));
		}
		#endregion
    }
}
