using System;
using System.Xml;
using DbAccess;
using TraceLoggers;

namespace Materials.Class
{
	public class Material: DbObject
	{
		#region Variables

		public const string CODE_PREFIX = "MAT";
		public const string IMG_CODE_PREFIX = "IMGMT";
		public const string DEFAULT_MAT = "DEFAULTMAT";

		private int mId;
		private string mCode;
		private string mDescription;
		private double mSpecificGravity;
		private string mFinishingSurface;
		private double mCostM2, mCostM3, mLinearCost, mSlabCost;
		private string mOrigin, mBlockSize, mColor, mAvailability, 
			mPrevailingUse, mPolish, mComments, mTonality, mScale;
		private DateTime mInsertDate; 

		private int mImageId;						//Id immagine
		private string mImageLink;					//Link memorizzazione temporanea immagine
		private bool mImageChanged;					//immagine cambiata

		private string mSupplierCode;
		private bool mSupplierCodeChanged = false;
		private string mClassCode;
		private bool mClassCodeChanged = false;
		private string mStateMat;
		private bool mStateMatChanged = false;

		public enum Keys {F_NONE = -1, F_ID, F_CODE, F_DESCRIPTION, F_STATE, F_SPECIFIC_GRAVITY, 
			F_FINISHING_SURFACE, F_CLASS_CODE, F_COST_M2, F_COST_M3, F_LINEAR_COST, F_SLAB_COST, F_SUPPLIER,
			F_ORIGIN, F_BLOCK_SIZE, F_COLOR, F_AVAILABILITY, F_PREVALING_USE, F_POLISH, F_COMMENTS,
			F_SCALE, F_INSERT_DATE, F_TONALITY, F_MAX};

		public enum State
		{
			NONE = -1,
			MATERIAL_000,	// Disponibile
			MATERIAL_001,	// In attesa di conferma
			MATERIAL_002,	// Non disponibile
			MAX
		}

		#endregion 

		#region Constructors
		public Material(int id, string code, string description, double specificGravity, string finishingSurface,
			string classCode, double costM2, double costM3, double linearCost, double slabCost, string supplierCode,
			string origin, string blockSize, string color, string availability, string prevailingUse, string polish,
			string comments, string scale, DateTime insertDate, string tonality, string state)
		{
			mId = id;
			mCode = code;
			mDescription = description;
			mSpecificGravity = specificGravity;
			mFinishingSurface = finishingSurface;
			mClassCode = classCode;
			mCostM2 = costM2;
			mCostM3 = costM3;
			mLinearCost = linearCost;
			mSlabCost = slabCost;
			mSupplierCode = supplierCode;
			mOrigin = origin;
			mBlockSize = blockSize;
			mColor = color;
			mAvailability = availability;
			mPrevailingUse = prevailingUse;
			mPolish = polish;
			mComments = comments;
			mScale = scale;
			mInsertDate = insertDate;
			mTonality = tonality;

			mStateMat = state;
			mImageId=0;
			mImageLink="";
		}

		public Material():this(0, "", "", 0D, "", "" , 0D, 0D, 0D, 0D, "", "", "", "", "", "", "", "", "", DateTime.Now, "", "")
		{
		}
		#endregion

		#region Properties
		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set 
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public string gDescription
		{
			set 
			{
				mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		public double gSpecificGravity
		{
			set 
			{
				mSpecificGravity = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mSpecificGravity; }
		}

		public string gFinishingSurface
		{
			set 
			{
				mFinishingSurface = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mFinishingSurface; }
		}

		public string gClassCode
		{
			set 
			{
				mClassCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mClassCodeChanged = true;
			}
			get { return mClassCode; }
		}

		public bool gClassCodeModified
		{
			set { mClassCodeChanged = value; }
			get { return mClassCodeChanged; }
		}

		public int gImageId
		{
			set { mImageId = value; }
			get { return mImageId; }
		}

		public string gImageLink
		{
			get { return mImageLink; }
		}

		internal void iSetImageLink(string link)
		{
			mImageLink = link;
		}

		internal bool iImageChanged
		{
			set	{	mImageChanged = value;	}
			get {	return mImageChanged;	}
		}

		public double gCostM2
		{
			set 
			{
				mCostM2 = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCostM2; }
		}

		public double gCostM3
		{
			set 
			{
				mCostM3 = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCostM3; }
		}

		public double gLinearCost
		{
			set 
			{
				mLinearCost = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mLinearCost; }
		}

		public double gSlabCost
		{
			set 
			{
				mSlabCost = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mSlabCost; }
		}

		public string gSupplierCode
		{
			set 
			{
				mSupplierCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mSupplierCodeChanged = true;
			}
			get { return mSupplierCode; }
		}

		public bool gSupplierCodeChanged
		{
			set { mSupplierCodeChanged = value; }
			get { return mSupplierCodeChanged; }
		}

		public string gOrigin
		{
			set 
			{
				mOrigin = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mOrigin; }
		}

		public string gBlockSize
		{
			set 
			{
				mBlockSize = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mBlockSize; }
		}

		public string gColor
		{
			set 
			{
				mColor = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mColor; }
		}

		public string gAvailability
		{
			set 
			{
				mAvailability = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mAvailability; }
		}

		public string gPrevailingUse
		{
			set 
			{
				mPrevailingUse = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mPrevailingUse; }
		}

		public string gPolish
		{
			set 
			{
				mPolish = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mPolish; }
		}

		public string gComments
		{
			set 
			{
				mComments = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mComments; }
		}

		public string gScale
		{
			set 
			{
				mScale = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mScale; }
		}

		public string gTonality
		{
			set 
			{
				mTonality = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mTonality; }
		}

		public DateTime gInsertDate
		{
			set 
			{
				mInsertDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mInsertDate; }
		}

		public string gStateMat
		{
			set 
			{
				mStateMat = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mStateMatChanged = true;
			}
			get { return mStateMat; }
		}

		public bool gStateMatChanged
		{
			set { mStateMatChanged = value; }
			get { return mStateMatChanged; }
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Material)
			{
				Material mat = (Material) obj;

				return mCode.CompareTo(mat.gCode);
			}

			throw new ArgumentException("object is not a Material");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == Material.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_STATE.ToString())
				return mStateMat.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_DESCRIPTION.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_SPECIFIC_GRAVITY.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_FINISHING_SURFACE.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_CLASS_CODE.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_COST_M2.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_COST_M3.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_LINEAR_COST.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_SLAB_COST.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_SUPPLIER.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_ORIGIN.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_BLOCK_SIZE.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_COLOR.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_AVAILABILITY.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_PREVALING_USE.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_POLISH.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_COMMENTS.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_SCALE.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_INSERT_DATE.ToString())
				return mClassCode.GetTypeCode();
			if (key.ToUpper() == Material.Keys.F_TONALITY.ToString())
				return mClassCode.GetTypeCode();


			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Material.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out double retValue)
		{
			if (key.ToUpper() == Material.Keys.F_SPECIFIC_GRAVITY.ToString())
			{
				retValue = mSpecificGravity;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_COST_M2.ToString())
			{
				retValue = mCostM2;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_COST_M3.ToString())
			{
				retValue = mCostM3;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_LINEAR_COST.ToString())
			{
				retValue = mLinearCost;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_SLAB_COST.ToString())
			{
				retValue = mSlabCost;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Material.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_STATE.ToString())
			{
				retValue = mStateMat.ToString();
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_SPECIFIC_GRAVITY.ToString())
			{
				retValue = mSpecificGravity.ToString();
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_FINISHING_SURFACE.ToString())
			{
				retValue = mFinishingSurface;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_CLASS_CODE.ToString())
			{
				retValue = mClassCode;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_SUPPLIER.ToString())
			{
				retValue = mSupplierCode;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_ORIGIN.ToString())
			{
				retValue = mOrigin;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_BLOCK_SIZE.ToString())
			{
				retValue = mBlockSize;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_COLOR.ToString())
			{
				retValue = mColor;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_AVAILABILITY.ToString())
			{
				retValue = mAvailability;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_PREVALING_USE.ToString())
			{
				retValue = mPrevailingUse;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_POLISH.ToString())
			{
				retValue = mPolish;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_COMMENTS.ToString())
			{
				retValue = mComments;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_SCALE.ToString())
			{
				retValue = mScale;
				return true;
			}

			if (key.ToUpper() == Material.Keys.F_TONALITY.ToString())
			{
				retValue = mTonality;
				return true;
			}


			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{
			if (key.ToUpper() == Material.Keys.F_INSERT_DATE.ToString())
			{
				retValue = mInsertDate;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		//**********************************************************************
		// AssignImage
		// Assegna l'immagine
		// Parametri:
		//			fileName	: valore ritornato
		//**********************************************************************
		internal void AssignImage(string fileName)
		{
			mImageLink = fileName;
			mImageChanged = true;
		}

        /// <summary>
        /// Legge le informazioni riguardanti il materiale
        /// </summary>
        /// <param name="fileXML">file da cui leggere</param>
        /// <returns></returns>
        public bool ReadFileXml(string fileXML)
        {
            XmlDocument doc = new XmlDocument();
            XmlNodeList l;

            try
            {
                // Legge il file XML
                doc.Load(fileXML);

                l = doc.SelectNodes("/Export/*");
                if (l != null && l.Count > 0)
                {
                    // Scorre tutti i sottonodi
                    foreach (XmlNode chn in l)
                    {
                        // Legge le informazioni della classe di materiale
                        if (chn.Name == "Material")
                        {
                            if (mCode == "" && chn.Attributes.GetNamedItem("Code") != null)
                                mCode = chn.Attributes.GetNamedItem("Code").Value;

                            if (mDescription == "" && chn.Attributes.GetNamedItem("Description") != null)
                                mDescription = chn.Attributes.GetNamedItem("Description").Value;

                            if (mSpecificGravity == 0d && chn.Attributes.GetNamedItem("Specific_gravity") != null)
                                mSpecificGravity = double.Parse(chn.Attributes.GetNamedItem("Specific_gravity").Value);

                            if (mFinishingSurface == "" && chn.Attributes.GetNamedItem("Finishing_surface") != null)
                                mFinishingSurface = chn.Attributes.GetNamedItem("Finishing_surface").Value;

                            if (mCostM2 == 0d && chn.Attributes.GetNamedItem("Cost_M2") != null)
                                mCostM2 = double.Parse(chn.Attributes.GetNamedItem("Cost_M2").Value);

                            if (mCostM3 == 0d && chn.Attributes.GetNamedItem("Cost_M3") != null)
                                mCostM3 = double.Parse(chn.Attributes.GetNamedItem("Cost_M3").Value);

                            if (mLinearCost == 0d && chn.Attributes.GetNamedItem("Linear_cost") != null)
                                mLinearCost = double.Parse(chn.Attributes.GetNamedItem("Linear_cost").Value);

                            if (mSlabCost == 0d && chn.Attributes.GetNamedItem("Slab_cost") != null)
                                mSlabCost = double.Parse(chn.Attributes.GetNamedItem("Slab_cost").Value);

                            if (mOrigin == "" && chn.Attributes.GetNamedItem("Origin") != null)
                                mOrigin = chn.Attributes.GetNamedItem("Origin").Value;

                            if (mBlockSize == "" && chn.Attributes.GetNamedItem("Block_size") != null)
                                mBlockSize = chn.Attributes.GetNamedItem("Block_size").Value;

                            if (mColor == "" && chn.Attributes.GetNamedItem("Color") != null)
                                mColor = chn.Attributes.GetNamedItem("Color").Value;

                            if (mAvailability == "" && chn.Attributes.GetNamedItem("Availability") != null)
                                mAvailability = chn.Attributes.GetNamedItem("Availability").Value;

                            if (mPrevailingUse == "" && chn.Attributes.GetNamedItem("Prevailing_use") != null)
                                mPrevailingUse = chn.Attributes.GetNamedItem("Prevailing_use").Value;

                            if (mPolish == "" && chn.Attributes.GetNamedItem("Polish") != null)
                                mPolish = chn.Attributes.GetNamedItem("Polish").Value;

                            if (mComments == "" && chn.Attributes.GetNamedItem("Comment") != null)
                                mComments = chn.Attributes.GetNamedItem("Comment").Value;

                            if (mTonality == "" && chn.Attributes.GetNamedItem("Tonality") != null)
                                mTonality = chn.Attributes.GetNamedItem("Tonality").Value;

                            if (mScale == "" && chn.Attributes.GetNamedItem("Scale") != null)
                                mScale = chn.Attributes.GetNamedItem("Scale").Value;

                            if (mInsertDate == DateTime.MinValue && chn.Attributes.GetNamedItem("Insert_date") != null)
                                mInsertDate = DateTime.Parse(chn.Attributes.GetNamedItem("Insert_date").Value);

                            if (mStateMat == "" && chn.Attributes.GetNamedItem("State") != null)
                                mStateMat = chn.Attributes.GetNamedItem("State").Value;

                            if (mSupplierCode == "" && chn.Attributes.GetNamedItem("Supplier_code") != null)
                                mSupplierCode = chn.Attributes.GetNamedItem("Supplier_code").Value;

                            if (mClassCode == "" && chn.Attributes.GetNamedItem("Material_class_code") != null)
                                mClassCode = chn.Attributes.GetNamedItem("Material_class_code").Value;
                        }
                    }
                    return true;
                }

                l = doc.SelectNodes("/Slab/GeneralInfo/*");
                if (l != null && l.Count > 0)
                {
                    // Scorre tutti i sottonodi
                    foreach (XmlNode chn in l)
                    {
                        // Legge le informazioni della classe di materiale
                        if (chn.Name == "Material")
                        {
                            if (mCode == "" && chn.Attributes.GetNamedItem("Code") != null)
                                mCode = chn.Attributes.GetNamedItem("Code").Value;

                            if (mDescription == "" && chn.Attributes.GetNamedItem("Description") != null)
                                mDescription = chn.Attributes.GetNamedItem("Description").Value;

                            if (mSpecificGravity == 0d && chn.Attributes.GetNamedItem("Specific_gravity") != null)
                                mSpecificGravity = double.Parse(chn.Attributes.GetNamedItem("Specific_gravity").Value);

                            if (mFinishingSurface == "" && chn.Attributes.GetNamedItem("Finishing_surface") != null)
                                mFinishingSurface = chn.Attributes.GetNamedItem("Finishing_surface").Value;

                            if (mCostM2 == 0d && chn.Attributes.GetNamedItem("Cost_M2") != null)
                                mCostM2 = double.Parse(chn.Attributes.GetNamedItem("Cost_M2").Value);

                            if (mCostM3 == 0d && chn.Attributes.GetNamedItem("Cost_M3") != null)
                                mCostM3 = double.Parse(chn.Attributes.GetNamedItem("Cost_M3").Value);

                            if (mLinearCost == 0d && chn.Attributes.GetNamedItem("Linear_cost") != null)
                                mLinearCost = double.Parse(chn.Attributes.GetNamedItem("Linear_cost").Value);

                            if (mSlabCost == 0d && chn.Attributes.GetNamedItem("Slab_cost") != null)
                                mSlabCost = double.Parse(chn.Attributes.GetNamedItem("Slab_cost").Value);

                            if (mOrigin == "" && chn.Attributes.GetNamedItem("Origin") != null)
                                mOrigin = chn.Attributes.GetNamedItem("Origin").Value;

                            if (mBlockSize == "" && chn.Attributes.GetNamedItem("Block_size") != null)
                                mBlockSize = chn.Attributes.GetNamedItem("Block_size").Value;

                            if (mColor == "" && chn.Attributes.GetNamedItem("Color") != null)
                                mColor = chn.Attributes.GetNamedItem("Color").Value;

                            if (mAvailability == "" && chn.Attributes.GetNamedItem("Availability") != null)
                                mAvailability = chn.Attributes.GetNamedItem("Availability").Value;

                            if (mPrevailingUse == "" && chn.Attributes.GetNamedItem("Prevailing_use") != null)
                                mPrevailingUse = chn.Attributes.GetNamedItem("Prevailing_use").Value;

                            if (mPolish == "" && chn.Attributes.GetNamedItem("Polish") != null)
                                mPolish = chn.Attributes.GetNamedItem("Polish").Value;

                            if (mComments == "" && chn.Attributes.GetNamedItem("Comment") != null)
                                mComments = chn.Attributes.GetNamedItem("Comment").Value;

                            if (mTonality == "" && chn.Attributes.GetNamedItem("Tonality") != null)
                                mTonality = chn.Attributes.GetNamedItem("Tonality").Value;

                            if (mScale == "" && chn.Attributes.GetNamedItem("Scale") != null)
                                mScale = chn.Attributes.GetNamedItem("Scale").Value;

                            if (mInsertDate == DateTime.MinValue && chn.Attributes.GetNamedItem("Insert_date") != null)
                                mInsertDate = DateTime.Parse(chn.Attributes.GetNamedItem("Insert_date").Value);

                            if (mStateMat == "" && chn.Attributes.GetNamedItem("State") != null)
                                mStateMat = chn.Attributes.GetNamedItem("State").Value;

                            if (mSupplierCode == "" && chn.Attributes.GetNamedItem("Supplier_code") != null)
                                mSupplierCode = chn.Attributes.GetNamedItem("Supplier_code").Value;

                            if (mClassCode == "" && chn.Attributes.GetNamedItem("Material_class_code") != null)
                                mClassCode = chn.Attributes.GetNamedItem("Material_class_code").Value;
                        }
                    }
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("MaterialClass.ReadFileXml error.", ex);

                return false;
            }
            finally
            {
                doc = null;
            }
        }

        /// <summary>
        /// Scrive il materiale su un nodo xml
        /// </summary>
        /// <param name="n">nodo sul quale scrivere</param>
        /// <returns></returns>
        public bool WriteFileXml(XmlDocument doc, XmlNode n)
        {
            try
            {
                // Crea il nodo principale
                XmlNode f = doc.CreateElement("Material");
                XmlAttribute code = doc.CreateAttribute("Code");
                XmlAttribute descr = doc.CreateAttribute("Description");
                XmlAttribute specif = doc.CreateAttribute("Specific_gravity");
                XmlAttribute finish = doc.CreateAttribute("Finishing_surface");
                XmlAttribute costM2 = doc.CreateAttribute("Cost_M2");
                XmlAttribute costM3 = doc.CreateAttribute("Cost_M3");
                XmlAttribute linearCost = doc.CreateAttribute("Linear_cost");
                XmlAttribute slabCost = doc.CreateAttribute("Slab_cost");
                XmlAttribute origin = doc.CreateAttribute("Origin");
                XmlAttribute blockSize = doc.CreateAttribute("Block_size");
                XmlAttribute color = doc.CreateAttribute("Color");
                XmlAttribute availability = doc.CreateAttribute("Availability");
                XmlAttribute use = doc.CreateAttribute("Prevailing_use");
                XmlAttribute polish = doc.CreateAttribute("Polish");
                XmlAttribute comment = doc.CreateAttribute("Comment");
                XmlAttribute tonality = doc.CreateAttribute("Tonality");
                XmlAttribute scale = doc.CreateAttribute("Scale");
                XmlAttribute insDate = doc.CreateAttribute("Insert_date");
                XmlAttribute state = doc.CreateAttribute("State");
                XmlAttribute supplier = doc.CreateAttribute("Supplier_code");
                XmlAttribute matClass = doc.CreateAttribute("Material_class_code");


                code.Value = mCode;
                descr.Value = mDescription;
                specif.Value = mSpecificGravity.ToString();
                finish.Value = mFinishingSurface;
                costM2.Value = mCostM2.ToString();
                costM3.Value = mCostM3.ToString();
                linearCost.Value = mLinearCost.ToString();
                slabCost.Value = mSlabCost.ToString();
                origin.Value = mOrigin;
                blockSize.Value = mBlockSize;
                color.Value = mColor;
                availability.Value = mAvailability;
                use.Value = mPrevailingUse;
                polish.Value = mPolish;
                comment.Value = mComments;
                tonality.Value = mTonality;
                scale.Value = mScale;
                insDate.Value = mInsertDate.ToString("u");
                state.Value = mStateMat;
                supplier.Value = mSupplierCode;
                matClass.Value = mClassCode;

                f.Attributes.Append(code);
                f.Attributes.Append(descr);
                f.Attributes.Append(specif);

                f.Attributes.Append(finish);
                f.Attributes.Append(costM2);
                f.Attributes.Append(costM3);
                f.Attributes.Append(linearCost);
                f.Attributes.Append(slabCost);
                f.Attributes.Append(origin);
                f.Attributes.Append(blockSize);
                f.Attributes.Append(color);
                f.Attributes.Append(availability);
                f.Attributes.Append(use); 
                f.Attributes.Append(polish);
                f.Attributes.Append(comment);
                f.Attributes.Append(tonality);
                f.Attributes.Append(scale);
                f.Attributes.Append(insDate);
                f.Attributes.Append(state);
                f.Attributes.Append(supplier);
                f.Attributes.Append(matClass);

                // Aggiunge il nodo
                n.AppendChild(f);

                return true;
            }
            catch(Exception ex)
            {
                TraceLog.WriteLine("Material.WriteFileXml error.", ex);

                return false;
            }
        }

        /// <summary>
        /// Scrive il materiale su un file xml
        /// </summary>
        /// <param name="fileName">nome del file su cui salvare</param>
        /// <returns></returns>
        public bool WriteFileXml(string fileName)
        {
            XmlDocument writeDoc = new XmlDocument();

            try
            {
                //Write the XML delcaration.
                XmlNode f = writeDoc.CreateXmlDeclaration("1.0", "utf-16", "");
                writeDoc.AppendChild(f);

                // Crea il nodo principale
                f = writeDoc.CreateElement("Export");
                writeDoc.AppendChild(f);

                if (!WriteFileXml(writeDoc, f))
                    return false;

                writeDoc.Save(fileName);
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Material.WriteFileXml error.", ex);

                return false;
            }
            finally
            {
                writeDoc = null;
            }
        }

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}
		#endregion
    }
}
