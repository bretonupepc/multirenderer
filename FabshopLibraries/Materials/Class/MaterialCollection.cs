using System;
using System.Threading;
using System.Data.OleDb;

using System.Collections;
using System.Globalization;
using DbAccess;
using Materials.Class;
using TraceLoggers;

namespace Breton.Materials
{
	public class MaterialCollection: DbCollection
	{
		#region Constructors
		public MaterialCollection(DbInterface db): base(db)
		{
		}

		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Material obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (Material) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Material obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Material) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Material obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(Material.Keys.F_NONE, null, code, null, null);
		}

		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id dell'elemento</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	lettura non eseguita
		/// </returns>
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(Material.Keys.F_NONE, null, id, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Material.Keys.F_NONE, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Material.Keys orderKey, string classCode)
		{
			return GetDataFromDb(orderKey, classCode, null, null, null);
		}
		
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			ids	: id dei materiali che si vuole estrarre
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Material.Keys orderKey, ArrayList codes)
		{
			return GetDataFromDb(orderKey, null, null, null, codes);
		}

		public bool GetDataFromDb(Material.Keys orderKey, string classCode, string code, string state)
		{
			return GetDataFromDb(orderKey, classCode, code, state, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			classCode	: estrae solo gli elementi delle classe specificata
		//			code		: estrae solo l'elemento con il codice specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Material.Keys orderKey, string classCode, string code, string state, ArrayList codes)
		{
			return GetDataFromDb(orderKey, classCode, -1, code, state, codes);
		}

		public bool GetDataFromDb(Material.Keys orderKey, string classCode, int id, string code, string state, ArrayList codes)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE ";

            //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
            //{
            //    //Estrae solo i materiali appartenenti alla classe richiesta
            //    if (classCode != null)
            //    {
            //        sqlWhere += sqlAnd + "F_CLASS_CODE = '" + DbInterface.QueryString(classCode) + "'";
            //        sqlAnd = " AND ";
            //    }

            //    //Estrae solo il materiale con l'id richiesto
            //    if (id > 0)
            //    {
            //        sqlWhere += sqlAnd + "F_ID_MATERIAL = " + id;
            //        sqlAnd = " AND ";
            //    }

            //    //Estrae solo il materiale con il codice richiesto
            //    if (code != null)
            //    {
            //        sqlWhere += sqlAnd + "F_CODE = '" + DbInterface.QueryString(code) + "'";
            //        sqlAnd = " AND ";
            //    }

            //    // Estrare solo i materiali con i codici richiesti
            //    if (codes != null && codes.Count > 0)
            //    {
            //        sqlWhere += sqlAnd + "F_CODE in ('" + codes[0].ToString() + "'";
            //        for (int i = 1; i < codes.Count; i++)
            //            sqlWhere += ", '" + codes[i].ToString() + "'";

            //        sqlWhere += ")";
            //        sqlAnd = " and ";
            //    }

            //    switch (orderKey)
            //    {
            //        case Material.Keys.F_ID:
            //            sqlOrderBy = " ORDER BY F_ID_MATERIAL";
            //            break;
            //        case Material.Keys.F_CODE:
            //            sqlOrderBy = " ORDER BY F_CODE";
            //            break;
            //        case Material.Keys.F_DESCRIPTION:
            //            sqlOrderBy = " ORDER BY F_DESCRIPTION";
            //            break;
            //        case Material.Keys.F_CLASS_CODE:
            //            sqlOrderBy = " ORDER BY F_CLASS_CODE";
            //            break;
            //    }
            //    return GetDataFromDb("SELECT * FROM T_MATERIALS m " + sqlWhere + sqlOrderBy);
            //}
            //else
			{
				//Estrae solo i materiali appartenenti alla classe richiesta
				if (classCode != null)
				{
					sqlWhere += sqlAnd + "c.F_CODE = '" + DbInterface.QueryString(classCode) + "'";
					sqlAnd = " AND ";
				}

				//Estrae solo il materiale con l'id richiesto
				if (id > 0)
				{
					sqlWhere += sqlAnd + "m.F_ID = " + id;
					sqlAnd = " AND ";
				}

				//Estrae solo il materiale con il codice richiesto
				if (code != null)
				{
					sqlWhere += sqlAnd + "m.F_CODE = '" + DbInterface.QueryString(code) + "'";
					sqlAnd = " AND ";
				}

				//Estrae solo i materiali con lo stato richiesto
				if (state != null)
				{
					sqlWhere += sqlAnd + "s.F_CODE= '" + DbInterface.QueryString(state) + "'";
					sqlAnd = " AND ";
				}

				// Estrare solo i materiali con i codici richiesti
				if (codes != null && codes.Count > 0)
				{
					sqlWhere += sqlAnd + "m.F_CODE in ('" + codes[0].ToString() + "'";
					for (int i = 1; i < codes.Count; i++)
						sqlWhere += ", '" + codes[i].ToString() + "'";

					sqlWhere += ")";
					sqlAnd = " and ";
				}

				switch (orderKey)
				{
					case Material.Keys.F_ID:
						sqlOrderBy = " ORDER BY m.F_ID";
						break;
					case Material.Keys.F_CODE:
						sqlOrderBy = " ORDER BY m.F_CODE";
						break;
					case Material.Keys.F_DESCRIPTION:
						sqlOrderBy = " ORDER BY m.F_DESCRIPTION";
						break;
					case Material.Keys.F_CLASS_CODE:
						sqlOrderBy = " ORDER BY c.F_CODE";
						break;
				}
				return GetDataFromDb("SELECT m.*, c.F_CODE AS F_CLASS_CODE, s.F_CODE AS F_STATE, f.F_CODE AS F_SUPPLIER FROM T_AN_MATERIALS m " +
									 " JOIN T_LS_MATERIAL_CLASSES c ON c.F_ID = m.F_ID_T_LS_MATERIAL_CLASSES " +
									 " JOIN T_CO_MATERIAL_STATES s ON s.F_ID = m.F_ID_T_CO_MATERIAL_STATES " +
									 " LEFT OUTER JOIN T_AN_SUPPLIERS f ON f.F_ID = m.F_ID_T_AN_SUPPLIERS "
									 + sqlWhere + sqlOrderBy);
			}
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Material.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null, null);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
                    if (transaction) mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Material mat in mRecordset)
					{
						switch (mat.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:
                                //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
                                //{

                                //    sqlInsert = "INSERT INTO T_MATERIALS (" +
                                //        (mat.gCode.Trim() == "" ? "" : "F_CODE,") +
                                //        " F_DESCRIPTION, " +
                                //        " F_FINISHING_CODE, F_CLASS_CODE) " + " VALUES (" + (mat.gCode.Trim() == "" ? "" : "'" + DbInterface.QueryString(mat.gCode) + "',")
                                //        + " '" + DbInterface.QueryString(mat.gDescription) + "', '" + DbInterface.QueryString(mat.gFinishingSurface) + "', '" 
                                //        + DbInterface.QueryString(mat.gClassCode) + "');SELECT SCOPE_IDENTITY()";										
                                //}
                                //else
								{
									sqlInsert = "INSERT INTO T_AN_MATERIALS (" +
										(mat.gCode.Trim() == "" ? "" : "F_CODE,") +
										" F_DESCRIPTION, F_SPECIFIC_GRAVITY, " +
										" F_FINISHING_SURFACE, F_COST_M2, F_COST_M3, F_LINEAR_COST, F_SLAB_COST, F_ORIGIN, F_BLOCK_SIZE, " +
										" F_COLOR, F_AVAILABILITY, F_PREVAILING_USE, F_POLISH, F_COMMENTS, F_TONALITY, F_SCALE , F_ID_T_LS_MATERIAL_CLASSES, F_ID_T_CO_MATERIAL_STATES" +
										(mat.gSupplierCode.Trim() == "" ? "" : ", F_ID_T_AN_SUPPLIERS") + ") " + " (SELECT " + (mat.gCode.Trim() == "" ? "" : "'" + DbInterface.QueryString(mat.gCode) + "',")
										+ " '" + DbInterface.QueryString(mat.gDescription) + "', " + mat.gSpecificGravity.ToString(CultureInfo.InvariantCulture) + ", '" + DbInterface.QueryString(mat.gFinishingSurface) + "', " + mat.gCostM2.ToString(CultureInfo.InvariantCulture) + ", " +
										mat.gCostM3.ToString(CultureInfo.InvariantCulture) + ", " + mat.gLinearCost.ToString(CultureInfo.InvariantCulture) + ", " + mat.gSlabCost.ToString(CultureInfo.InvariantCulture) + ", '" + DbInterface.QueryString(mat.gOrigin) + "', '" +
										DbInterface.QueryString(mat.gBlockSize) + "', '" + DbInterface.QueryString(mat.gColor) + "', '" + DbInterface.QueryString(mat.gAvailability)
										+ "', '" + DbInterface.QueryString(mat.gPrevailingUse) + "', '" + DbInterface.QueryString(mat.gPolish) + "', '" + DbInterface.QueryString(mat.gComments) + "', '" +
										DbInterface.QueryString(mat.gTonality) + "', '" + DbInterface.QueryString(mat.gScale) + "'" + ", c.F_ID, s.F_ID" + (mat.gSupplierCode.Trim() == "" ? "" : ", f.F_ID")
										+ " FROM T_LS_MATERIAL_CLASSES c, T_CO_MATERIAL_STATES s" + (mat.gSupplierCode.Trim() == "" ? "" : ", T_AN_SUPPLIERS f") + " WHERE c.F_CODE = '" +
										DbInterface.QueryString(mat.gClassCode) + "' AND s.F_CODE = '" + DbInterface.QueryString(mat.gStateMat) + "'" + (mat.gSupplierCode.Trim() == "" ? "" : " AND f.F_CODE = '" + DbInterface.QueryString(mat.gSupplierCode) + "'") +
										");SELECT SCOPE_IDENTITY()";
								}

								if (!mDbInterface.Execute(sqlInsert, out id))
									throw new ApplicationException("Error in insert object");

								if (id == 0)
									throw new ApplicationException("Error in insert object");

								// Imposta l'id
								mat.gId = id;

                                //if (mDbInterface.gDbType != DbInterface.DbType.AcAdSQL)
								{
									// Scrive l'immagine su DB
									if (mat.iImageChanged)
									{
										if (!SetImageToDb(mat, mat.gImageLink))
											throw new ApplicationException("Error in insert image");
										mat.iImageChanged = false;
									}
								}
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
                                //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
                                //{
                                //    sqlUpdate = "UPDATE T_AN_MATERIALS SET F_CODE = '" + DbInterface.QueryString(mat.gCode) +
                                //                "', F_DESCRIPTION = '" + DbInterface.QueryString(mat.gDescription) +
                                //                ", F_FINISHING_SURFACE = '" + DbInterface.QueryString(mat.gFinishingSurface) + "'";
                                //}
                                //else
								{
									sqlUpdate = "UPDATE T_AN_MATERIALS SET F_CODE = '" + DbInterface.QueryString(mat.gCode) +
												"', F_DESCRIPTION = '" + DbInterface.QueryString(mat.gDescription) +
												"', F_SPECIFIC_GRAVITY = " + mat.gSpecificGravity.ToString(CultureInfo.InvariantCulture) +
												", F_FINISHING_SURFACE = '" + DbInterface.QueryString(mat.gFinishingSurface) +
												"', F_COST_M2 = " + mat.gCostM2.ToString(CultureInfo.InvariantCulture) +
												", F_COST_M3 = " + mat.gCostM3.ToString(CultureInfo.InvariantCulture) +
												", F_LINEAR_COST = " + mat.gLinearCost.ToString(CultureInfo.InvariantCulture) +
												", F_SLAB_COST = " + mat.gSlabCost.ToString(CultureInfo.InvariantCulture) +
												", F_ORIGIN = '" + DbInterface.QueryString(mat.gOrigin) +
												"', F_BLOCK_SIZE = '" + DbInterface.QueryString(mat.gBlockSize) +
												"', F_COLOR = '" + DbInterface.QueryString(mat.gColor) +
												"', F_AVAILABILITY = '" + DbInterface.QueryString(mat.gAvailability) +
												"', F_PREVAILING_USE = '" + DbInterface.QueryString(mat.gPrevailingUse) +
												"', F_POLISH = '" + DbInterface.QueryString(mat.gPolish) +
												"', F_COMMENTS = '" + DbInterface.QueryString(mat.gComments) +
												"', F_TONALITY = '" + DbInterface.QueryString(mat.gTonality) +
												"', F_SCALE = '" + DbInterface.QueryString(mat.gScale) + "'";
								}

								 
								//Verifica se � stato modificato anche il codice della classe
								if (mat.gClassCodeModified)
								{
                                    //if (mDbInterface.gDbType != DbInterface.DbType.AcAdSQL)
                                    //{
                                    //    sqlUpdate += ", F_CLASS_CODE = '" + DbInterface.QueryString(mat.gClassCode) + "')";
                                    //}
                                    //else
									{
										sqlUpdate += ", F_ID_T_LS_MATERIAL_CLASSES = "
											+ " (SELECT F_ID FROM T_LS_MATERIAL_CLASSES"
											+ " WHERE F_CODE = '" + DbInterface.QueryString(mat.gClassCode) + "')";
									}
								}

                                //if (mDbInterface.gDbType != DbInterface.DbType.AcAdSQL)
								{
									//Verifica se � stato modificato anche lo stato del materiale
									if (mat.gStateMatChanged)
										sqlUpdate += ", F_ID_T_CO_MATERIAL_STATES = "
											+ " (SELECT F_ID FROM T_CO_MATERIAL_STATES"
											+ " WHERE F_CODE = '" + DbInterface.QueryString(mat.gStateMat) + "')";

									// Verifica se � stato modificato il fornitore del materiale
									if (mat.gSupplierCodeChanged)
										sqlUpdate += ", F_ID_T_AN_SUPPLIERS = "
											+ " (SELECT F_ID FROM T_AN_SUPPLIERS"
											+ " WHERE F_CODE = '" + DbInterface.QueryString(mat.gSupplierCode) + "')";
								}
																
                                //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
                                //    sqlUpdate += " WHERE F_ID_MATERIAL = " + mat.gId.ToString();
                                //else
									sqlUpdate += " WHERE F_ID = " + mat.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									throw new ApplicationException("Error in update object");

                                //if (mDbInterface.gDbType != DbInterface.DbType.AcAdSQL)
								{
									// Scrive l'immagine su DB
									if (mat.iImageChanged)
									{
										if (!SetImageToDb(mat, mat.gImageLink))
											throw new ApplicationException("Error in update image");
										mat.iImageChanged = false;
									}
								}
								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Material mat in mDeleted)
					{
						switch (mat.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
                                //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
                                //    sqlDelete = "DELETE FROM T_MATERIALS WHERE F_ID_MATERIAL = " + mat.gId.ToString();
                                //else
									sqlDelete = "DELETE FROM T_AN_MATERIALS WHERE F_ID = " + mat.gId.ToString();

								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
                    if (transaction) mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("MaterialCollection: Deadlock Error", ex);

					// Annulla la transazione
                    if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("MaterialCollection: Exceed Deadlock retrying", ex);
				}

				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("MaterialCollection: Error", ex);

					// Annulla la transazione
                    if (transaction) mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

		//**********************************************************************
		// GetImageFromDb
		// Legge l'immagine dal database
		// Parametri:
		//			mat			: materiale di cui leggere l'immagine
		//			fileName	: nome del file su cui memorizzare l'immagine
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetImageFromDb(Material mat, string fileName)
		{
			// Nessuna immagine
            //if (mat.gImageId == 0 || mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
            //    return false;
			
			try
			{
				// Recupera l'immagine dal db
				if (!mDbInterface.GetBlobField("T_AN_MATERIAL_IMAGES", "F_IMAGE", 
						" F_ID = (SELECT F_ID_T_AN_MATERIAL_IMAGES FROM T_AN_MATERIALS WHERE F_ID = " + mat.gId.ToString() + ")", fileName))
					return false;
				
				// Assegna l'immagine
				mat.iSetImageLink(fileName);
				return true;
			}
				
			// Eccezione
			catch (Exception ex)
			{
				TraceLog.WriteLine("MaterialCollection.GetImageFromDb: Error", ex);
				return false;
			}
		}
		
		//**********************************************************************
		// SetImageToDb
		// Scrive l'immagine nel database
		// Parametri:
		//			mat			: materiale di cui scrivere l'immagine
		//			fileName	: nome del file da cui prelevare l'immagine
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool SetImageToDb(Material mat, string fileName)
		{
			int id=0;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

            //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
            //    return false;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					// Inserisce un nuovo record nelle immagini
					if (!mDbInterface.Execute("INSERT INTO T_AN_MATERIAL_IMAGES (F_IMAGE) VALUES(NULL)" +
												";SELECT SCOPE_IDENTITY()", out id))
						throw (new ApplicationException("MaterialCollection.SetImageToDb: error setting image mode"));

					// Scrive l'immagine su db
					if (!mDbInterface.WriteBlobField("T_AN_MATERIAL_IMAGES", "F_IMAGE", " F_ID = " + id.ToString(), fileName))
						throw (new ApplicationException("MaterialCollection.SetImageToDb: error writing image"));

					// Imposta il link all'immagine
					if (!mDbInterface.Execute("UPDATE T_AN_MATERIALS SET F_ID_T_AN_MATERIAL_IMAGES = " + id.ToString() + 
												" WHERE F_ID = " + mat.gId.ToString()))
						throw (new ApplicationException("MaterialCollection.SetImageToDb: error setting image mode"));

					if (transaction)	mDbInterface.EndTransaction(true);

					// Memorizza l'id dell'immagine
					mat.gImageId = id;
					mat.iSetImageLink(fileName);
					return true;
				}
				
				// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("MaterialCollection.SetImageToDb Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
					//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction)	mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("MaterialCollection.SetImageToDb: Error", ex);
					return false;
				}
			}

			return false;
		}
		

		//**********************************************************************
		// GetNewCode
		// Ritorna un nuovo codice per il materiale
		// Ritorna:
		//			codice materiale ritornato
		//			""	errore
		//**********************************************************************
		public string GetNewCode()
		{
			return base.GetNewCode(Material.CODE_PREFIX, true);
		}

		//**********************************************************************
		// GetNewImageCode
		// Ritorna un nuovo codice per le immagini del materiale
		// Ritorna:
		//			codice immagine materiale ritornato
		//			""	errore
		//**********************************************************************
		public string GetNewImageCode()
		{
			return base.GetNewCode(Material.IMG_CODE_PREFIX, true);
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
            if (key == Material.Keys.F_CODE.ToString())
                SortByCode();
            else if (key == Material.Keys.F_DESCRIPTION.ToString())
                SortByDescr();
                
		}

        public bool CheckExistingCode(string code, out bool exist)
        {
            //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
            //    return base.CheckExistingCode(code, "T_MATERIALS", out exist);
            //else
	            return base.CheckExistingCode(code, "T_AN_MATERIALS", out exist);
        }

		/// ******************************************************************
		/// <summary>
		/// Restituisce la descrizione del materiale
		/// </summary>
		/// <param name="db">interfaccia database</param>
		/// <param name="id">id del materiale</param>
		/// <returns></returns>
		/// ******************************************************************
		public static string GetMaterialDescription(DbInterface db, int id)
		{		
			OleDbDataReader dr = null;
			string description = "";

			// Verifica se la query � valida
			string sqlQuery = "";

            //if (db.gDbType == DbInterface.DbType.AcAdSQL)
            //    sqlQuery = "select F_DESCRIPTION from T_MATERIALS where F_ID =" + id.ToString();
            //else
				sqlQuery = "select F_DESCRIPTION from T_AN_MATERIALS where F_ID =" + id.ToString();

			try
			{
				if (!db.Requery(sqlQuery, out dr))
					return null;

				while (dr.Read())
				{
					description = dr["F_DESCRIPTION"].ToString();
				}
				return description;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("GetMaterialDescription: Error", ex);

				return null;
			}

			finally
			{
				db.EndRequery(dr);
			}
		}

		/// ******************************************************************
		/// <summary>
		/// Restituisce la descrizione del materiale
		/// </summary>
		/// <param name="db">interfaccia database</param>
		/// <param name="code">codice del materia</param>
		/// <returns></returns>
		/// ******************************************************************
		public static string GetMaterialDescription(DbInterface db, string code)
		{
			OleDbDataReader dr = null;
			string description = null;
			
			string sqlQuery = "";

            //if (db.gDbType == DbInterface.DbType.AcAdSQL)
            //    sqlQuery = "select F_DESCRIPTION from T_MATERIALS where F_CODE ='" + code + "'";
            //else
				sqlQuery = "select F_DESCRIPTION from T_AN_MATERIALS where F_CODE ='" + code.ToString() + "'";

			try
			{
				if (!db.Requery(sqlQuery, out dr))
					return null;

				while (dr.Read())
				{
					description = dr["F_DESCRIPTION"].ToString();
				}
				return description;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("GetMaterialDescription: Error", ex);

				return null;
			}

			finally
			{
				db.EndRequery(dr);
			}
		}

    	#endregion

		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Material mat;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
                    //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
                    //{
                    //    mat = new Material(int.Parse(dr["F_ID_MATERIAL"].ToString()), dr["F_CODE"].ToString(), dr["F_DESCRIPTION"].ToString(),
                    //            0d, dr["F_FINISHING_CODE"].ToString(),
                    //            dr["F_CLASS_CODE"].ToString(), 0d, 0d, 0d, 0d, "", "", "", "", "", "", "", "", "", DateTime.MinValue, "", "");

                    //    AddObject(mat);
                    //    mat.gState = DbObject.ObjectStates.Unchanged;
                    //}
                    //else
					{
						mat = new Material(int.Parse(dr["F_ID"].ToString()), dr["F_CODE"].ToString(), dr["F_DESCRIPTION"].ToString(),
								(double)dr["F_SPECIFIC_GRAVITY"], dr["F_FINISHING_SURFACE"].ToString(),
								dr["F_CLASS_CODE"].ToString(), (double)dr["F_COST_M2"], (double)dr["F_COST_M3"],
								(double)dr["F_LINEAR_COST"], (double)dr["F_SLAB_COST"],
								dr["F_SUPPLIER"].ToString(), dr["F_ORIGIN"].ToString(), dr["F_BLOCK_SIZE"].ToString(),
								dr["F_COLOR"].ToString(), dr["F_AVAILABILITY"].ToString(), dr["F_PREVAILING_USE"].ToString(),
								dr["F_POLISH"].ToString(), dr["F_COMMENTS"].ToString(), dr["F_SCALE"].ToString(),
								(DateTime)dr["F_INSERT_DATE"], dr["F_TONALITY"].ToString(), dr["F_STATE"].ToString());

						AddObject(mat);
						mat.gState = DbObject.ObjectStates.Unchanged;
						if (!(dr["F_ID_T_AN_MATERIAL_IMAGES"] is DBNull))
							mat.gImageId = (int)dr["F_ID_T_AN_MATERIAL_IMAGES"];
					}
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("MaterialCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

        /// <summary>
        /// Effettua l'ordinamento per la descrizione
        /// </summary>
        private void SortByDescr()
        {
            Material tmp;

            // Bubble Sort
            for (int i = 0; i < mRecordset.Count - 1; i++)
            {
                for (int j = i + 1; j < mRecordset.Count; j++)
                {
                    if (((Material)mRecordset[i]).gDescription.CompareTo(((Material)mRecordset[j]).gDescription) > 0)
                    {
                        tmp = (Material)mRecordset[i];
                        mRecordset[i] = mRecordset[j];
                        mRecordset[j] = tmp;
                    }
                }
            }
        }

        /// <summary>
        /// Effettua l'ordinamento per codice
        /// </summary>
        private void SortByCode()
        {
            Material tmp;

            // Bubble Sort
            for (int i = 0; i < mRecordset.Count - 1; i++)
            {
                for (int j = i + 1; j < mRecordset.Count; j++)
                {
                    if (((Material)mRecordset[i]).gCode.CompareTo(((Material)mRecordset[j]).gCode) > 0)
                    {
                        tmp = (Material)mRecordset[i];
                        mRecordset[i] = mRecordset[j];
                        mRecordset[j] = tmp;
                    }
                }
            }
        }

		#endregion
	}
}
