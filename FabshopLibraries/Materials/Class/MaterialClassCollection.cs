using System;
using System.Data.OleDb;
using System.Data.SqlClient;
using DbAccess;
using Materials.Class;
using TraceLoggers;

namespace Breton.Materials
{
	public class MaterialClassCollection : DbCollection
	{

		#region Constructors
		public MaterialClassCollection(DbInterface db)
			: base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out MaterialClass obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);

			obj = (MaterialClass)dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out MaterialClass obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (MaterialClass)mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(MaterialClass obj)
		{
			return (base.AddObject((DbObject)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(MaterialClass.Keys.F_NONE, code);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(MaterialClass.Keys.F_NONE, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(MaterialClass.Keys orderKey, string code)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";

            //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
            //{
            //    if (code != null)
            //        sqlWhere += " WHERE T_MATERIALS.F_CLASS_CODE = '" + DbInterface.QueryString(code) + "'";


            //    return GetDataFromDb("SELECT DISTINCT -1 as f_ID, F_CLASS_CODE as F_CODE, F_CLASS_CODE as F_DESCRIPTION " +
            //                        "FROM T_MATERIALS " + sqlWhere);
            //}
            //else
			{
				if (code != null)
					sqlWhere += " WHERE T_LS_MATERIAL_CLASSES.F_CODE = '" + DbInterface.QueryString(code) + "'";

				switch (orderKey)
				{
					case MaterialClass.Keys.F_ID:
						sqlOrderBy = "ORDER BY F_ID";
						break;
					case MaterialClass.Keys.F_CODE:
						sqlOrderBy = "ORDER BY F_CODE";
						break;
					case MaterialClass.Keys.F_DESCRIPTION:
						sqlOrderBy = "ORDER BY F_DESCRIPTION";
						break;
				}
				return GetDataFromDb("SELECT T_LS_MATERIAL_CLASSES.*, T_CO_MATERIAL_CLASS_TYPES.F_CODE as F_TYPE " +
									"FROM T_LS_MATERIAL_CLASSES " +
									"LEFT OUTER JOIN T_CO_MATERIAL_CLASS_TYPES ON T_CO_MATERIAL_CLASS_TYPES.F_ID = T_LS_MATERIAL_CLASSES.F_ID_T_CO_MATERIAL_CLASS_TYPES " + sqlWhere + sqlOrderBy);
			}
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id, retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			// In caso di database di tipo AcAdSQL esce senza fare niente
            //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
            //    return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (MaterialClass mat in mRecordset)
					{
						switch (mat.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "INSERT INTO T_LS_MATERIAL_CLASSES (" +
									(mat.gCode.Trim() == "" ? "" : "F_CODE,") +
									" F_DESCRIPTION, F_SPECIFIC_GRAVITY" + (mat.gType.Trim() == "" ? "" : ", F_ID_T_CO_MATERIAL_CLASS_TYPES") +
									") (SELECT" + (mat.gCode.Trim() == "" ? "" : "'" + DbInterface.QueryString(mat.gCode) + "',") +
									"'" + DbInterface.QueryString(mat.gDescription) + "', " + mat.gSpecificGravity.ToString() + (mat.gType.Trim() == "" ? "" : ", t.F_ID") +
									(mat.gType.Trim() == "" ? "" : " FROM T_CO_MATERIAL_CLASS_TYPES t WHERE F_CODE = '" + mat.gType.Trim() + "'") +
									") " +
									";SELECT SCOPE_IDENTITY()";

								if (!mDbInterface.Execute(sqlInsert, out id))
									throw new ApplicationException("MaterialClassCollection: Error during Db Insert operation");

								break;

							//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "UPDATE T_LS_MATERIAL_CLASSES SET F_CODE = '" + DbInterface.QueryString(mat.gCode) +
											"', F_DESCRIPTION = '" + DbInterface.QueryString(mat.gDescription) + "'" +
											", F_SPECIFIC_GRAVITY = " + mat.gSpecificGravity.ToString() +
											(mat.gType.Trim() == "" ? "" : ", F_ID_T_CO_MATERIAL_CLASS_TYPES = ( SELECT F_ID FROM T_CO_MATERIAL_CLASS_TYPES WHERE F_CODE = '" + mat.gType.Trim() + "')") +
											" WHERE F_ID = " + mat.gId.ToString();


								if (!mDbInterface.Execute(sqlUpdate))
									throw new ApplicationException("MaterialClassCollection: Error during Db Update operation");

								break;
						}

					}

					//Scorre tutti i materiali cancellati
					foreach (MaterialClass mat in mDeleted)
					{
						switch (mat.gState)
						{
							//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "DELETE FROM T_LS_MATERIAL_CLASSES WHERE F_ID = " + mat.gId.ToString();

								if (!mDbInterface.Execute(sqlDelete))
									throw new ApplicationException("MaterialClassCollection: Error during Db Delete operation");

								break;
						}
					}

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("MaterialClassCollection: Deadlock Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("MaterialClassCollection: Exceed Deadlock retrying", ex);
				}

				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("MaterialClassCollection: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		//**********************************************************************
		// GetNewCode
		// Ritorna un nuovo codice per la classe di materiale
		// Ritorna:
		//			codice classe materiale ritornato
		//			""	errore
		//**********************************************************************
		public string GetNewCode()
		{
			return base.GetNewCode(MaterialClass.CODE_PREFIX, true);
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		public bool CheckExistingCode(string code, out bool exist)
		{
            //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
            //{
            //    exist = false;
            //    return true;
            //}
            //else
				return base.CheckExistingCode(code, "T_LS_MATERIAL_CLASSES", out exist);
		}
		#endregion

		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			MaterialClass obj;

			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
                    //if (mDbInterface.gDbType == DbInterface.DbType.AcAdSQL)
                    //{
                    //    obj = new MaterialClass((int)dr["F_ID"], dr["F_CODE"].ToString(), dr["F_DESCRIPTION"].ToString(), 0D, "");
                    //}
                    //else
					{
						obj = new MaterialClass((int)dr["F_ID"], dr["F_CODE"].ToString(), dr["F_DESCRIPTION"].ToString(),
							(double)dr["F_SPECIFIC_GRAVITY"], (dr["F_TYPE"] == DBNull.Value ? "" : dr["F_TYPE"].ToString()));
					}

					AddObject(obj);
					obj.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("MaterialClassCollection: Error.", ex);
				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion


		#region Static Methods
		public static MaterialClass.Type GetEnumFromString(string type)
		{
			if (type != null)
			{
				if (type.Trim() == MaterialClass.Type.MATCLTYPE_COMPOUND.ToString())
					return MaterialClass.Type.MATCLTYPE_COMPOUND;
				else if (type.Trim() == MaterialClass.Type.MATCLTYPE_NATURAL.ToString())
					return MaterialClass.Type.MATCLTYPE_NATURAL;
				else if (type.Trim() == MaterialClass.Type.MATCLTYPE_CERAMIC.ToString())
					return MaterialClass.Type.MATCLTYPE_CERAMIC;
			}

			return MaterialClass.Type.NONE;
		}
		#endregion
	}
}
