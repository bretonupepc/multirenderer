using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Breton.Materials.Form;

namespace Materials
{
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem miMaterialClass;
		private System.Windows.Forms.MenuItem miMaterial;

		private System.ComponentModel.Container components = null;

		public Form1()
		{
			InitializeComponent();
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.miMaterialClass = new System.Windows.Forms.MenuItem();
			this.miMaterial = new System.Windows.Forms.MenuItem();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.miMaterialClass,
																					  this.miMaterial});
			this.menuItem1.Text = "Start";
			// 
			// miMaterialClass
			// 
			this.miMaterialClass.Index = 0;
			this.miMaterialClass.Text = "Material Class";
			this.miMaterialClass.Click += new System.EventHandler(this.miMaterialClass_Click);
			// 
			// miMaterial
			// 
			this.miMaterial.Index = 1;
			this.miMaterial.Text = "Materials";
			this.miMaterial.Click += new System.EventHandler(this.miMaterial_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(672, 376);
			this.IsMdiContainer = true;
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.Text = "Material Manage";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void miMaterialClass_Click(object sender, System.EventArgs e)
		{
			frmMaterialClass MatClass = new frmMaterialClass(true);
			MatClass.MdiParent = this;
			MatClass.Show();
		}

		private void miMaterial_Click(object sender, System.EventArgs e)
		{
			frmMaterial Mat = new frmMaterial(true);
			Mat.MdiParent = this;
			Mat.Show();
		}
	}
}
