using System;
using Breton.DbAccess;

namespace Breton.Phases
{
	public class WorkPhase : DbObject
	{
		#region Variables
        public const string CODE_PREFIX = "WKPH";

		private int mId;
		private string mCode;
		private int mSequence;
		private DateTime mDate;
		private DateTime mDateStart;
		private DateTime mDateStop;

        private string mTecnoInfo;			//Link memorizzazione temporanea file XML

		private string mStateWorkPhase;
		private bool mStateWorkPhaseChanged = false;
		private string mWorkOrderCode;
		private bool mWorkOrderCodeChanged = false;
		private string mDetailCode;
		private bool mDetailCodeChanged = false;
		private string mUsedObjectCode;
		private bool mUsedObjectCodeChanged = false;
        private string mPhaseDUsedObject;
        private bool mPhaseDUsedObjectChanged = false;
		

		//Chiavi dei campi
		public enum Keys 
		{F_NONE = -1, F_ID, F_CODE, F_SEQUENCE, F_DATE, F_START_DATE, F_STOP_DATE, F_WORK_ORDER_CODE, F_DETAIL_CODE, F_STATE, F_USED_OBJECT_CODE, F_MAX};

        public enum State
        {
            NONE = -1,
            //WORKPHASE_000,
            PHASE_LOADED,       //Informazioni della fase definite
	        PHASE_READY,       	//Fase pronta per l'esecuzione
	        PHASE_IN_PROGRESS,  //Fase in esecuzione
	        PHASE_COMPLETED,    //Fase completata
	        PHASE_STANDBY,      //Fase bloccata
            MAX
        }

		#endregion 

		#region Constructor
		public WorkPhase(int id, string code, int sequence, DateTime date, DateTime dateStart, DateTime dateEnd, string stateWorkPhase,
            string workOrderCode, string detailCode, string usedObjectCode, string phaseDUsedObject)
		{
			mId = id;
			mCode = code;
			mSequence = sequence;
			mDate = date;
			mDateStart = dateStart;
			mDateStop = dateEnd;
			mStateWorkPhase = stateWorkPhase;
            mWorkOrderCode = workOrderCode;
			mDetailCode = detailCode;
			mUsedObjectCode = usedObjectCode;
            mPhaseDUsedObject = phaseDUsedObject;
		}

        public WorkPhase() : this(0, "", 0, DateTime.MaxValue, DateTime.MaxValue, DateTime.MaxValue, "", "", "", "", "")
		{
		}
		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set 
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public int gSequence
		{
			set	
			{ 
				mSequence = value; 
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mSequence; }
		}

		public DateTime gDate
		{
			set 
			{
				mDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDate; }
		}

		public DateTime gDateStart
		{
			set 
			{
				mDateStart = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDateStart; }
		}

		public DateTime gDateStop
		{
			set 
			{
				mDateStop = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDateStop; }
		}

		public string gStateWorkPhase
		{
			set 
			{
				mStateWorkPhase = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mStateWorkPhaseChanged = true;
			}
			get { return mStateWorkPhase; }
		}

		public bool gStateWorkPhaseChanged
		{
			set { mStateWorkPhaseChanged = value; }
			get { return mStateWorkPhaseChanged; }
		}

		public string gWorkOrderCode
		{
			set 
			{
				mWorkOrderCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mWorkOrderCodeChanged = true;
			}
			get { return mWorkOrderCode; }
		}

        public bool gWorkOrderCodeChanged
		{
            set { mWorkOrderCodeChanged = value; }
            get { return mWorkOrderCodeChanged; }
		}

		public string gDetailCode
		{
			set 
			{
				mDetailCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mDetailCodeChanged = true;
			}
			get { return mDetailCode; }
		}

		public bool gDetailCodeChanged
		{
			set { mDetailCodeChanged = value; }
			get { return mDetailCodeChanged; }
		}

		public string gUsedObjectCode
		{
			set 
			{
				mUsedObjectCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mUsedObjectCodeChanged = true;
			}
			get { return mUsedObjectCode; }
		}

		public bool gUsedObjectCodeChanged
		{
			set { mUsedObjectCodeChanged = value; }
			get { return mUsedObjectCodeChanged; }
		}

        public string gPhaseDUsedObject
        {
            set
            {
                mPhaseDUsedObject = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;

                mPhaseDUsedObjectChanged = true;
            }
            get { return mPhaseDUsedObject; }
        }

        public bool gPhaseDUsedObjectChanged
        {
            set { mPhaseDUsedObjectChanged = value; }
            get { return mPhaseDUsedObjectChanged; }
        }

        public string gTecnoInfo
        {
            set
            {
                mTecnoInfo = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mTecnoInfo; }
        }

        internal void iTecnoInfo(string link)
        {
            mTecnoInfo = link;
        }

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
            if (obj is WorkPhase)
			{
				WorkPhase wp = (WorkPhase) obj;

				return mCode.CompareTo(wp.gCode);
			}

			throw new ArgumentException("object is not a Work Phase");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == WorkPhase.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_SEQUENCE.ToString())
				return mSequence.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_DATE.ToString())
				return mDate.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_START_DATE.ToString())
				return mDateStart.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_STOP_DATE.ToString())
				return mDateStop.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_STATE.ToString())
				return mStateWorkPhase.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_WORK_ORDER_CODE.ToString())
				return mWorkOrderCode.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_DETAIL_CODE.ToString())
				return mDetailCode.GetTypeCode();
			if (key.ToUpper() == WorkPhase.Keys.F_USED_OBJECT_CODE.ToString())
				return mUsedObjectCode.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out DateTime retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
            if (key.ToUpper() == WorkPhase.Keys.F_ID.ToString())
            {
                retValue = mId;
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_SEQUENCE.ToString())
            {
                retValue = mSequence;
                return true;
            }

            return base.GetField(key, out retValue);
        }

        public override bool GetField(string key, out string retValue)
        {
            if (key.ToUpper() == WorkPhase.Keys.F_ID.ToString())
            {
                retValue = mId.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_SEQUENCE.ToString())
            {
                retValue = mSequence.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_CODE.ToString())
            {
                retValue = mCode;
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_DATE.ToString())
            {
                retValue = mDate.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_START_DATE.ToString())
            {
                retValue = mDateStart.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_STOP_DATE.ToString())
            {
                retValue = mDateStop.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_DETAIL_CODE.ToString())
            {
                retValue = mDetailCode.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_STATE.ToString())
            {
                retValue = mStateWorkPhase.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_USED_OBJECT_CODE.ToString())
            {
                retValue = mUsedObjectCode.ToString();
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_WORK_ORDER_CODE.ToString())
            {
                retValue = mWorkOrderCode.ToString();
                return true;
            }

            return base.GetField(key, out retValue);
        }

        public override bool GetField(string key, out DateTime retValue)
        {
            if (key.ToUpper() == WorkPhase.Keys.F_DATE.ToString())
            {
                retValue = mDate;
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_START_DATE.ToString())
            {
                retValue = mDateStart;
                return true;
            }

            if (key.ToUpper() == WorkPhase.Keys.F_STOP_DATE.ToString())
            {
                retValue = mDateStop;
                return true;
            }
			return base.GetField(key, out retValue);
		}

		#endregion
		
		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		#endregion
	}

	/// <summary>
	/// Classe per la descrizione degli stati articoli nelle combobox
	/// </summary>
	public class WorkPhaseStateCombo
	{
		private WorkPhase.State mState;
		private string mStateDescription;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="state">state</param>
		/// <param name="description">state description</param>
		public WorkPhaseStateCombo(WorkPhase.State state, string description)
		{
			mState = state;
			mStateDescription = description;
		}

		public WorkPhase.State State
		{
			get { return mState; }
		}

		public string StateDescription
		{
			get { return mStateDescription; }
		}

		public override string ToString()
		{
			return StateDescription;
		}
	}
}
