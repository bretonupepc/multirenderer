using System;
using Breton.DbAccess;

namespace Breton.Phases
{
	public class WorkOrder: DbObject
	{
		#region Variables
        public const string CODE_PREFIX = "WKORD";

		private int mId;
		private string mCode;
		private int mSequence;
		private DateTime mDate;
		private DateTime mDateStart;
		private DateTime mDateStop;
		private string mDescription;
		

		private string mStateWorkOrd;
		private bool mStateWorkOrdChanged = false;
		private string mWorkCenterCode;
		private bool mWorkCenterCodeChanged = false;
		private string mWorkCenterProgCode;
		private bool mWorkCenterProgCodeChanged = false;
		private string mWorkCenterGroupCode;
		private bool mWorkCenterGroupCodeChanged = false;
        private int[] mWorkingTypesId;
        private bool mWorkingTypesIdChanged;
        private int[] mPhaseWorkingsId;
        private bool mPhaseWorkingsIdChanged;
		private string[] mTecnoWorkingTypes;

        //Chiavi dei campi
		public enum Keys
		{ F_NONE = -1, F_ID, F_CODE, F_SEQUENCE, F_DATE, F_START_DATE, F_STOP_DATE, F_DESCRIPTION, F_WORK_CENTER_CODE, F_WORK_CENTER_PROG_CODE, F_WORK_CENTER_GROUP_CODE, F_STATE, F_MAX };

		public enum State 
		{ 
			NONE = -1,
			//WORKORD_000,
			WKORD_LOADED,		//Ordine Caricato
			WKORD_READY,		//Ordine Pronto alla Esecuzione
            WKORD_IN_PROGRESS,	//Ordine in Esecuzione
			WKORD_COMPLETED,	//Ordine Completato
            WKORD_STANDBY,		//Ordine Bloccato
			MAX
		}

		#endregion 

		#region Constructor

		public WorkOrder(int id, string code, int sequence, DateTime date, DateTime dateStart, DateTime dateEnd, string stateWorkOrd,
			string description, string workCenterCode, string workCenterProgCode, string workCenterGroupCode, int[] workingTypesId, int[] phaseWorkingsId, string[] tecnoWorkings)
		{
			mId = id;
            mCode = code;
			mSequence = sequence;
			mDate = date;
			mDateStart = dateStart;
			mDateStop = dateEnd;
			mDescription = description;
			mStateWorkOrd = stateWorkOrd;
			mWorkCenterCode = workCenterCode;
			mWorkCenterProgCode = workCenterProgCode;
			mWorkCenterGroupCode = workCenterGroupCode;
            mWorkingTypesId = workingTypesId;
            mPhaseWorkingsId = phaseWorkingsId;
			mTecnoWorkingTypes = tecnoWorkings;
		}

        public WorkOrder(): this(0, "", 0, DateTime.MaxValue, DateTime.MaxValue, DateTime.MaxValue, "", "", "", "", "", null, null, null)
		{
		}

		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set 
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public int gSequence
		{
			set	
			{ 
				mSequence = value; 
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mSequence; }
		}

		public DateTime gDate
		{
			set 
			{
				mDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDate; }
		}

		public DateTime gDateStart
		{
			set 
			{
				mDateStart = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDateStart; }
		}

		public DateTime gDateStop
		{
			set 
			{
				mDateStop = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDateStop; }
		}

		public string gDescription
		{
			set 
			{
				mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

        public int[] gWorkingTypesId
        {
            set
            {
                mWorkingTypesId = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;

                mWorkingTypesIdChanged = true;
            }
            get { return mWorkingTypesId; }
        }

        public bool gWorkingTypesIdChanged
        {
            set { mWorkingTypesIdChanged = value; }
            get { return mWorkingTypesIdChanged; }
        }

        public int[] gPhaseWorkingsId
        {
            set
            {
                mPhaseWorkingsId = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;

                mPhaseWorkingsIdChanged = true;
            }
            get { return mPhaseWorkingsId; }
        }

        public bool gPhaseWorkingsIdChanged
        {
            set { mPhaseWorkingsIdChanged = value; }
            get { return mPhaseWorkingsIdChanged; }
        }

        public string gStateWorkOrd
		{
			set 
			{
				mStateWorkOrd = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mStateWorkOrdChanged = true;
			}
			get { return mStateWorkOrd; }
		}

		public bool gStateWorkOrdChanged
		{
			set { mStateWorkOrdChanged = value; }
			get { return mStateWorkOrdChanged; }
		}

		public string gWorkCenterCode
		{
			set 
			{
				mWorkCenterCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
				
				mWorkCenterCodeChanged = true;
			}
			get { return mWorkCenterCode; }
		}

		public bool gWorkCenterCodeChanged
		{
			set { mWorkCenterCodeChanged = value; }
			get { return mWorkCenterCodeChanged; }
		}

		public string gWorkCenterProgCode
		{
			set
			{
				mWorkCenterProgCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;

				mWorkCenterProgCodeChanged = true;
			}
			get { return mWorkCenterProgCode; }
		}

		public bool gWorkCenterProgCodeChanged
		{
			set { mWorkCenterProgCodeChanged = value; }
			get { return mWorkCenterProgCodeChanged; }
		}

		public string gWorkCenterGroupCode
		{
			set 
			{
				mWorkCenterGroupCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

                mWorkCenterGroupCodeChanged = true;
			}
			get { return mWorkCenterGroupCode; }
		}

		public bool gWorkCenterCodeGroupChanged
		{
            set { mWorkCenterGroupCodeChanged = value; }
            get { return mWorkCenterGroupCodeChanged; }
		}

		public string[] gTecnoWorkingTypes
		{
			set { mTecnoWorkingTypes = value; }
			get { return mTecnoWorkingTypes; }
		}
		
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
            if (obj is WorkOrder)
			{
				WorkOrder wo = (WorkOrder) obj;

				return mCode.CompareTo(wo.gCode);
			}

			throw new ArgumentException("object is not a Work Order");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == WorkOrder.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_SEQUENCE.ToString())
				return mSequence.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_DATE.ToString())
				return mDate.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_START_DATE.ToString())
				return mDateStart.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_STOP_DATE.ToString())
				return mDateStop.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_DESCRIPTION.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_STATE.ToString())
				return mStateWorkOrd.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_WORK_CENTER_CODE.ToString())
				return mWorkCenterCode.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_WORK_CENTER_PROG_CODE.ToString())
				return mWorkCenterProgCode.GetTypeCode();
			if (key.ToUpper() == WorkOrder.Keys.F_WORK_CENTER_GROUP_CODE.ToString())
				return mWorkCenterGroupCode.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out DateTime retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == WorkOrder.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_SEQUENCE.ToString())
			{
				retValue = mSequence;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == WorkOrder.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_SEQUENCE.ToString())
			{
				retValue = mSequence.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_DATE.ToString())
			{
				retValue = mDate.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_START_DATE.ToString())
			{
				retValue = mDateStart.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_STOP_DATE.ToString())
			{
				retValue = mDateStop.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_STATE.ToString())
			{
				retValue = mStateWorkOrd.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_WORK_CENTER_CODE.ToString())
			{
				retValue = mWorkCenterCode.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_WORK_CENTER_PROG_CODE.ToString())
			{
				retValue = mWorkCenterProgCode.ToString();
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_WORK_CENTER_GROUP_CODE.ToString())
			{
				retValue = mWorkCenterGroupCode.ToString();
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{
			if (key.ToUpper() == WorkOrder.Keys.F_DATE.ToString())
			{
				retValue = mDate;
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_START_DATE.ToString())
			{
				retValue = mDateStart;
				return true;
			}

			if (key.ToUpper() == WorkOrder.Keys.F_STOP_DATE.ToString())
			{
				retValue = mDateStop;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		#endregion
		
		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		#endregion
	}
}
