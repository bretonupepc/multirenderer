using System;
using System.Collections.Generic;
using System.Text;

namespace Breton.Phases
{
	public class ProductionProcess
	{
		#region Variables
		private int mId;
		private string mCode;
		private string mDescription;		
		#endregion

		#region Properties
		public int gId
		{
			get { return mId; }
			set { mId = value; }
		}
		
		public string gCode
		{
			get { return mCode; }
			set { mCode = value; }
		}

		public string gDescription
		{
			get { return mDescription; }
			set { mDescription = value; }
		}
		#endregion

		#region Constructor
		public ProductionProcess()
			: this(0, "", "")
		{
		}

		public ProductionProcess(int id, string code, string description)
		{
			mId = id;
			mCode = code;
			mDescription = description;
		}
		#endregion
	}

	public class ProductionProcessCollection : List<ProductionProcess>
	{ }
}
