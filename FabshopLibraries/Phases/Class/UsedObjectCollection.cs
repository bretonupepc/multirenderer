using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Text;
using Breton.DbAccess;
using Breton.Articles;
using Breton.TraceLoggers;

namespace Breton.Phases
{
    public class UsedObjectCollection : DbCollection
    {
        #region Constructor
        public UsedObjectCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
        public bool GetObject(out UsedObject obj)
		{
			bool ret;
            DbObject dbObj;

			ret = base.GetObject(out dbObj);

            obj = (UsedObject)dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
        public bool GetObjectTable(int tableId, out UsedObject obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
                obj = (UsedObject)mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}
		
		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
        public bool AddObject(UsedObject obj)
		{
            return (base.AddObject((UsedObject)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
            return GetDataFromDb(UsedObject.Keys.F_NONE, code, null, UsedObject.Type.None, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
            return GetDataFromDb(UsedObject.Keys.F_NONE, null, null, UsedObject.Type.None, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
        public bool GetDataFromDb(UsedObject.Keys orderKey)
		{
            return GetDataFromDb(orderKey, null, null, UsedObject.Type.None, null, null);
		}

        //**********************************************************************
        // GetDataFromDb
        // Legge i dati dal database non ordinati
        // Parametri
        //          ids     id da ricercare
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool GetDataFromDb(int[] ids)
        {
            return GetDataFromDb(UsedObject.Keys.F_NONE, null, null, UsedObject.Type.None, ids, null);
        }

        /// <summary>
        /// Legge i dati dal database degli oggetti con il codice richiesto
        /// </summary>
        /// <param name="codes">array con i codici da ricercare</param>
        /// <returns>true	lettura eseguita, false	lettura non eseguita</returns>
        public bool GetDataFromDb(string[] codes)
        {
            return GetDataFromDb(UsedObject.Keys.F_NONE, null, null, UsedObject.Type.None, null, codes);
        }

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		//			state		: estrae solo gli elementi dello stato specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
        public bool GetDataFromDb(UsedObject.Keys orderKey, string code, string articleCode, UsedObject.Type type, int[] ids, string[] codes)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE ";


			//Estrae solo l'used object del codice richiesto
			if (code != null)
			{
				sqlWhere += sqlAnd + "u.F_CODE = '" + DbInterface.QueryString(code) + "'";
				sqlAnd = " AND ";
			}

            //Estrae gli used object di un determinato articolo
            if (articleCode != null)
			{
                sqlWhere += sqlAnd + "a.F_CODE = '" + DbInterface.QueryString(articleCode) + "'";
				sqlAnd = " AND ";
			}

            if (ids != null)
                if (ids.Length > 0)
                {
                    sqlWhere += sqlAnd + "u.F_ID in (" + ids[0];
                    for (int i = 1; i < ids.Length; i++)
                        sqlWhere += "," + ids[i];
                    sqlWhere += ")";
                }

            if (codes != null)
                if (codes.Length > 0)
                {
                    sqlWhere += sqlAnd + "u.F_CODE in ('" + codes[0];
                    for (int i = 1; i < codes.Length; i++)
                        sqlWhere += "', '" + codes[i];
                    sqlWhere += "')";
                }

            switch (type)
            {
                case UsedObject.Type.Origin:
                    sqlWhere += sqlAnd + "aty.F_CODE = '" + Article.gArticleTypeCode(Article.ArticleType.SLAB) + "'";
                    sqlAnd = " and ";
                    break;

                case UsedObject.Type.Intermediate:
                    sqlWhere += sqlAnd + "aty.F_CODE = '" + Article.gArticleTypeCode(Article.ArticleType.SEMIFINISHED) + "'";
                    sqlAnd = " and ";
                    break;

                case UsedObject.Type.Final:
                    sqlWhere += sqlAnd + "aty.F_CODE = '" + Article.gArticleTypeCode(Article.ArticleType.COMPLETED) + "'";
                    sqlAnd = " and ";
                    break;
            }

			switch (orderKey)
			{
                case UsedObject.Keys.F_ID:
					sqlOrderBy = " order by u.F_ID";
					break;
                case UsedObject.Keys.F_CODE:
                    sqlOrderBy = " order by u.F_CODE";
					break;
                case UsedObject.Keys.F_ARTICLE_CODE:
                    sqlOrderBy = " order by a.F_CODE";
					break;			
			}
            return GetDataFromDb("select u.F_ID, u.F_CODE, a.F_CODE AS F_ARTICLE_CODE, aty.F_CODE AS F_ARTICLE_TYPE " +
				"from T_WK_USED_OBJECTS u " +
                "left outer join T_AN_ARTICLES a ON a.F_ID = u.F_ID_T_AN_ARTICLES " +
                "left outer join T_CO_ARTICLE_TYPES aty ON aty.F_ID = a.F_ID_T_CO_ARTICLE_TYPES" +
				sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// 
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();


			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if(transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti gli ordini di lavoro
					foreach (UsedObject usobj in mRecordset)
					{
                        switch (usobj.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
                                sqlInsert = "INSERT INTO T_WK_USED_OBJECTS (F_ID_T_AN_ARTICLES) " +
                                    (usobj.gArticleCode ==  null ? "VALUES(null)" : "(SELECT a.F_ID FROM T_AN_ARTICLES a WHERE a.F_CODE = '" + 
                                    usobj.gArticleCode.Trim() + "')") + ";SELECT SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
                                usobj.gId = id;

                                // Inserisce il file xml
                                if (usobj.gWorkingInfo != null && usobj.gWorkingInfo != "" && !SetWorkingInfoToDb(usobj, usobj.gWorkingInfo))
                                    throw new ApplicationException("Error on saving Xml to Db");

                                // Inserisce il file xml
                                if (usobj.gShapeInfo != null && usobj.gShapeInfo != "" && !SetShapeInfoToDb(usobj, usobj.gShapeInfo))
                                    throw new ApplicationException("Error on saving Xml to Db");
                                break;
					
								//Aggiornamento
                            case DbObject.ObjectStates.Updated:
                                sqlUpdate = "UPDATE T_WK_USED_OBJECTS SET " +
                                    "F_ID_T_AN_ARTICLES = " + (usobj.gArticleCode ==  null ? "null" :
                                    "(SELECT F_ID FROM T_AN_ARTICLES WHERE F_CODE = '" + usobj.gArticleCode.Trim() + "')");

                                sqlUpdate += " WHERE F_ID = " + usobj.gId.ToString();

                               if (!mDbInterface.Execute(sqlUpdate))
                                    return false;

                                // Inserisce il file xml
                                if (usobj.gWorkingInfo != null && usobj.gWorkingInfo != "" && !SetWorkingInfoToDb(usobj, usobj.gWorkingInfo))
                                    throw new ApplicationException("Error on saving Xml to Db");

                                // Inserisce il file xml
                                if (usobj.gShapeInfo != null && usobj.gShapeInfo != "" && !SetShapeInfoToDb(usobj, usobj.gShapeInfo))
                                    throw new ApplicationException("Error on saving Xml to Db");
                                break;
						}
					}

                    //Scorre tutti gli ordini di lavoro cancellati
                    foreach (UsedObject usobj in mDeleted)
                    {
                        switch (usobj.gState)
                        {
                            //Cancellazione
                            case DbObject.ObjectStates.Deleted:
                                sqlDelete = " delete from T_WK_USED_OBJECTS where F_ID = " + usobj.gId;


                                if (!mDbInterface.Execute(sqlDelete))
                                    return false;
                                break;
                        }
                    }

					// Termina la transazione con successo
					if(transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

                    // Rileggi i dati dal database
                    if (mSqlGetData != "")
                        GetDataFromDb(mSqlGetData);
                    else
                    {
                        int[] ids = new int[mRecordset.Count];
                        for (int i = 0; i < mRecordset.Count; i++)
                            ids[i] = ((UsedObject)mRecordset[i]).gId;

                        GetDataFromDb(ids);
                        mSqlGetData = "";
                    }

					return true;
				}
				    //Eccezione di deadlock
				catch (DeadLockException ex)
				{
                    TraceLog.WriteLine("UsedObjectCollection: Deadlock Error", ex);

					// Annulla la transazione
					if(transaction)	mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
                        throw new ApplicationException("Exceed Deadlock retrying", ex);
				}
					// Altre eccezioni
				catch (Exception ex)
				{
                    TraceLog.WriteLine("UsedObjectCollection: Error", ex);

					// Annulla la transazione
					if(transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

        //**********************************************************************
        // GetWorkingInfoFromDb
        // Legge il file dal database
        // Parametri:
        //			usobj		: used object di cui leggere l'XML
        //			fileName	: nome del file su cui memorizzare l'XML
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool GetWorkingInfoFromDb(UsedObject usobj, string fileName)
        {
            try
            {
                // Recupera l'XML dal db
                if (!mDbInterface.GetBlobField("T_WK_USED_OBJECTS", "F_WORKING_INFO", " F_ID = " + usobj.gId.ToString(), fileName))
                    return false;
            }

                // Eccezione
            catch (Exception ex)
            {
                TraceLog.WriteLine("UsedObjectCollection.GetWorkingInfoFromDb: Error", ex);
                return false;
            }

            usobj.iWorkingInfo(fileName);
            return true;
        }

        //**********************************************************************
        // SetWorkingInfoFromDb
        // Scrive il file nel database
        // Parametri:
        //			usobj		: used object di cui scrivere l'XML
        //			fileName	: nome del file da cui prelevare l'XML
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool SetWorkingInfoToDb(UsedObject usobj, string fileName)
        {
            bool transaction = !mDbInterface.TransactionActive();

            try
            {
                if (transaction) mDbInterface.BeginTransaction();

                // Scrive l'XML su db
                if (!mDbInterface.WriteBlobField("T_WK_USED_OBJECTS", "F_WORKING_INFO", " F_ID = " + usobj.gId.ToString(), fileName))
                    throw (new ApplicationException("error writing xml"));

                if (transaction) mDbInterface.EndTransaction(true);

                usobj.iWorkingInfo(fileName);
                return true;
            }

                // Eccezione
            catch (Exception ex)
            {
                if (transaction) mDbInterface.EndTransaction(false);
                TraceLog.WriteLine("UsedObjectCollection.SetWorkingInfoFromDb: Error", ex);
                return false;
            }
        }

        //**********************************************************************
        // GetShapeInfoFromDb
        // Legge il file dal database
        // Parametri:
        //			usobj		: used object di cui leggere l'XML
        //			fileName	: nome del file su cui memorizzare l'XML
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool GetShapeInfoFromDb(UsedObject usobj, string fileName)
        {
            try
            {
                // Recupera l'XML dal db
                if (!mDbInterface.GetBlobField("T_WK_USED_OBJECTS", "F_SHAPE_INFO", " F_ID = " + usobj.gId.ToString(), fileName))
                    return false;
            }

                // Eccezione
            catch (Exception ex)
            {
                TraceLog.WriteLine("UsedObjectCollection.GetShapeInfoFromDb: Error", ex);
                return false;
            }

            usobj.iShapeInfo(fileName);
            return true;
        }

        //**********************************************************************
        // SetShapeInfoFromDb
        // Scrive il file nel database
        // Parametri:
        //			usobj		: used object di cui scrivere l'XML
        //			fileName	: nome del file da cui prelevare l'XML
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool SetShapeInfoToDb(UsedObject usobj, string fileName)
        {
            bool transaction = !mDbInterface.TransactionActive();

            try
            {
                if (transaction) mDbInterface.BeginTransaction();

                // Scrive l'XML su db
                if (!mDbInterface.WriteBlobField("T_WK_USED_OBJECTS", "F_SHAPE_INFO", " F_ID = " + usobj.gId.ToString(), fileName))
                    throw (new ApplicationException("error writing xml"));

                if (transaction) mDbInterface.EndTransaction(true);

                usobj.iShapeInfo(fileName);
                return true;
            }

                // Eccezione
            catch (Exception ex)
            {
                if (transaction) mDbInterface.EndTransaction(false);
                TraceLog.WriteLine("UsedObjectCollection.SetShapeInfoFromDb: Error", ex);
                return false;
            }
        }

		public override void SortByField(string key)
		{
		}

		#endregion

        #region Private Methods

        //**********************************************************************
        // GetDataFromDb
        // Legge i dati dal database, secondo la query specificata
        // Parametri:
        //			sqlQuery	: query da eseguire
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        private bool GetDataFromDb(string sqlQuery)
        {
            OleDbDataReader dr = null;
            UsedObject usobj;
            UsedObject.Type type = UsedObject.Type.None;
           
            Clear();

            // Verifica se la query � valida
            if (sqlQuery == "")
                return false;
            
            try
            {
                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return false;

                while (dr.Read())
                {
                    if (dr["F_ARTICLE_TYPE"] == DBNull.Value || dr["F_ARTICLE_TYPE"].ToString().Trim() == Article.gArticleTypeCode(Article.ArticleType.SLAB))
                        type = UsedObject.Type.Origin;
                    else if (dr["F_ARTICLE_TYPE"].ToString().Trim() == Article.gArticleTypeCode(Article.ArticleType.SEMIFINISHED))
                        type = UsedObject.Type.Intermediate;
                    else if (dr["F_ARTICLE_TYPE"].ToString().Trim() == Article.gArticleTypeCode(Article.ArticleType.COMPLETED))
                        type = UsedObject.Type.Final;

                    usobj = new UsedObject((int)dr["F_ID"], dr["F_CODE"].ToString(), dr["F_ARTICLE_CODE"].ToString(), type);

                    AddObject(usobj);
                    usobj.gState = DbObject.ObjectStates.Unchanged;
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("UsedObjectCollection: Error", ex);

                return false;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
            }

            // Salva l'ultima query eseguita
            mSqlGetData = sqlQuery;
            return true;
        }


        #endregion
    }
}
