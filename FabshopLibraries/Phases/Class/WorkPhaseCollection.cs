using System;
using System.Data;
using System.Collections;
using System.Threading;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.Phases
{
	public class WorkPhaseCollection : DbCollection
	{
        #region Constructor
        public WorkPhaseCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out WorkPhase obj)
		{
			bool ret;
            DbObject dbObj;

			ret = base.GetObject(out dbObj);

            obj = (WorkPhase)dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
        public bool GetObjectTable(int tableId, out WorkPhase obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
                obj = (WorkPhase)mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}
		
		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
        public bool AddObject(WorkPhase obj)
		{
            return (base.AddObject((WorkPhase)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(WorkPhase.Keys.F_NONE, id, null, null, null, null, null);
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
            return GetDataFromDb(WorkPhase.Keys.F_NONE, -1, code, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
            return GetDataFromDb(WorkPhase.Keys.F_NONE, -1, null, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
        public bool GetDataFromDb(WorkPhase.Keys orderKey)
		{
			return GetDataFromDb(orderKey, -1, null, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		//			state		: estrae solo gli elementi dello stato specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(WorkPhase.Keys orderKey, string code, string detailCode, string workOrderCode, string usedObjectCode, string state)
		{
			return GetDataFromDb(orderKey, -1, code, detailCode, workOrderCode, usedObjectCode, state);
		}

        public bool GetDataFromDb(WorkPhase.Keys orderKey, int id, string code, string detailCode, string workOrderCode, string usedObjectCode, string state)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE ";

			//Estrae solo la fase con l'id richiesto
			if (id > 0)
			{
				sqlWhere += sqlAnd + "p.F_ID = " + id;
				sqlAnd = " AND ";
			}

			//Estrae solo il progetto del codice richiesto
			if (code != null)
			{
				sqlWhere += sqlAnd + "p.F_CODE = '" + DbInterface.QueryString(code) + "'";
				sqlAnd = " AND ";
			}

            //Estrae le fasi di lavoro di un pezzo
            if (detailCode != null)
			{
                sqlWhere += sqlAnd + "d.F_CODE = '" + DbInterface.QueryString(detailCode) + "'";
				sqlAnd = " AND ";
			}

            //Estrae solo la/e fase/i di lavoro di un'ordine di lavoro
            if (workOrderCode != null)
			{
                sqlWhere += sqlAnd + "o.F_CODE = '" + DbInterface.QueryString(workOrderCode) + "'";
				sqlAnd = " AND ";
			}

            //Estrae solo la/e fase/i relative a un determinato used object
            if (usedObjectCode != null)
			{
                sqlWhere += sqlAnd + "u.F_CODE= '" + DbInterface.QueryString(usedObjectCode) + "'";
				sqlAnd = " AND ";
			}

            //Estrae solo le fasi relative con un determinato stato
            if (state != null)
            {
                sqlWhere += sqlAnd + "s.F_CODE= '" + DbInterface.QueryString(state) + "'";
                sqlAnd = " AND ";
            }

			switch (orderKey)
			{
                case WorkPhase.Keys.F_ID:
					sqlOrderBy = " ORDER BY p.F_ID";
					break;
                case WorkPhase.Keys.F_CODE:
					sqlOrderBy = " ORDER BY p.F_CODE";
					break;
                case WorkPhase.Keys.F_DETAIL_CODE:
					sqlOrderBy = " ORDER BY d.F_CODE";
					break;
                case WorkPhase.Keys.F_WORK_ORDER_CODE:
					sqlOrderBy = " ORDER BY o.F_CODE";
					break;
                case WorkPhase.Keys.F_SEQUENCE:
					sqlOrderBy = " ORDER BY p.F_SEQUENCE";
					break;
                case WorkPhase.Keys.F_USED_OBJECT_CODE:
                    sqlOrderBy = " ORDER BY u.F_CODE";
                    break;
                case WorkPhase.Keys.F_STATE:
					sqlOrderBy = " ORDER BY s.F_CODE";
					break;				
			}
			return GetDataFromDb("SELECT p.F_ID, p.F_CODE, p.F_SEQUENCE, p.F_DATE, p.F_START_DATE, p.F_STOP_DATE, d.F_CODE AS F_DETAIL_CODE, o.F_CODE AS F_WORK_ORDER_CODE, s.F_CODE AS F_STATE, " +
                "u.F_CODE AS F_USED_OBJECT_CODE, pd.F_ID as F_PHASES_D_USED_OBJECT " +
				"FROM T_WK_WORK_PHASES_H p " +
                "JOIN T_WK_WORK_PHASES_D pd ON p.F_ID = pd.F_ID_T_WK_WORK_PHASES_H " +
                "JOIN T_WK_CUSTOMER_ORDER_DETAILS d ON d.F_ID = p.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
                "JOIN T_WK_WORK_ORDERS o ON o.F_ID = p.F_ID_T_WK_WORK_ORDERS " +
                "JOIN T_WK_USED_OBJECTS u ON u.F_ID = p.F_ID_T_WK_USED_OBJECTS " +
                "JOIN T_CO_WORK_PHASES_H_STATES s ON s.F_ID = p.F_ID_T_CO_WORK_PHASES_H_STATES " +
				sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// 
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();


			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if(transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti gli ordini di lavoro
					foreach (WorkPhase wkph in mRecordset)
					{
						switch (wkph.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
                                sqlInsert = "INSERT INTO T_WK_WORK_PHASES_H (F_ID_T_WK_CUSTOMER_ORDER_DETAILS, F_ID_T_WK_USED_OBJECTS, " +
                                    "F_ID_T_CO_WORK_PHASES_H_STATES, F_ID_T_WK_WORK_ORDERS, F_CODE, F_SEQUENCE, F_START_DATE, F_STOP_DATE) " +
                                    "(SELECT d.F_ID, u.F_ID, s.F_ID, o.F_ID, '" + wkph.gCode + "', " + wkph.gSequence + ", " +
                                    (wkph.gDateStart == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkph.gDateStart)) + ", " +
                                    (wkph.gDateStop == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkph.gDateStop)) + " " +
                                    "FROM T_WK_CUSTOMER_ORDER_DETAILS d, T_WK_USED_OBJECTS u, T_CO_WORK_PHASES_H_STATES s, T_WK_WORK_ORDERS o " +
                                    "WHERE d.F_CODE = '" + wkph.gDetailCode.Trim() + "' AND u.F_CODE = '" + wkph.gUsedObjectCode.Trim() + 
                                    "' AND s.F_CODE = '" + wkph.gStateWorkPhase.Trim() + "' AND o.F_CODE = '" + wkph.gWorkOrderCode.Trim() +  
                                    "');SELECT SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
                                wkph.gId = id;

                                // Crea i record della tabella T_WK_WORK_PHASES_D
                                if (!SetPhasesD(wkph))
                                    throw new ApplicationException("WorkPhaseCollection.SetPhasesD Error on creating work phase D");

                                // Inserisce il file xml
                                if (wkph.gTecnoInfo != null && wkph.gTecnoInfo != "" && !SetXmlParameterToDb(wkph, wkph.gTecnoInfo))
                                    throw new ApplicationException("Error on saving Xml to Db");
								
								break;
					
								//Aggiornamento
                            case DbObject.ObjectStates.Updated:
                                sqlUpdate = "UPDATE T_WK_WORK_PHASES_H SET " +
                                    "F_CODE = '" + DbInterface.QueryString(wkph.gCode) +
                                    "', F_SEQUENCE = " + wkph.gSequence.ToString() +
                                    ", F_START_DATE = " + (wkph.gDateStart == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkph.gDateStart)) +
                                    ", F_STOP_DATE = " + (wkph.gDateStart == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkph.gDateStart));

                                //Verifica se � stato modificato il pezzo d'origine
                                if (wkph.gDetailCodeChanged)
                                    sqlUpdate += ", F_ID_T_WK_CUSTOMER_ORDER_DETAILS = "
                                        + " (SELECT F_ID FROM T_WK_CUSTOMER_ORDER_DETAILS"
                                        + " WHERE F_CODE = '" + DbInterface.QueryString(wkph.gDetailCode.Trim()) + "')";

                                //Verifica se � stato modificato l'used object
                                if (wkph.gUsedObjectCodeChanged)
                                    sqlUpdate += ", F_ID_T_WK_USED_OBJECTS = "
                                        + " (SELECT F_ID FROM T_WK_USED_OBJECTS"
                                        + " WHERE F_CODE = '" + DbInterface.QueryString(wkph.gUsedObjectCode) + "')";

                                //Verifica se � stato modificato lo stato della fase di lavoro
                                if (wkph.gStateWorkPhaseChanged)
                                    sqlUpdate += ", F_ID_T_CO_WORK_PHASES_H_STATES = "
                                        + " (SELECT F_ID FROM T_CO_WORK_PHASES_H_STATES"
                                        + " WHERE F_CODE = '" + DbInterface.QueryString(wkph.gStateWorkPhase) + "')";

                                //Verifica se � stato modificato l'ordine di lavoro associato
                                if (wkph.gWorkOrderCodeChanged)
                                    sqlUpdate += ", F_ID_T_WK_WORK_ORDERS = "
                                        + " (SELECT F_ID FROM T_WK_WORK_ORDERS"
                                        + " WHERE F_CODE = '" + DbInterface.QueryString(wkph.gWorkOrderCode) + "')";

                                sqlUpdate += " WHERE F_ID = " + wkph.gId.ToString();

                               if (!mDbInterface.Execute(sqlUpdate))
                                    return false;

                                // Modifica i record della tabella T_WK_WORK_PHASES_D
                                if (!SetPhasesD(wkph))
                                    throw new ApplicationException("WorkPhaseCollection.SetPhasesD Error on updating work phase D");

                                // Inserisce il file xml
                                if (wkph.gTecnoInfo != null && wkph.gTecnoInfo != "" && !SetXmlParameterToDb(wkph, wkph.gTecnoInfo))
                                    throw new ApplicationException("Error on saving Xml to Db");
                                break;
						}
					}

                    //Scorre tutte le fasi di lavoro cancellat3
                    foreach (WorkPhase wkph in mDeleted)
                    {
                        switch (wkph.gState)
                        {
                            //Cancellazione
                            case DbObject.ObjectStates.Deleted:
                                sqlDelete = " delete from T_WK_WORK_PHASES_D where F_ID_T_WK_WORK_PHASES_h = " + wkph.gId.ToString() +
                                            " delete from T_WK_WORK_PHASES_H where F_ID = " + wkph.gId.ToString();

                                if (!mDbInterface.Execute(sqlDelete))
                                    return false;
                                break;
                        }
                    }

					// Termina la transazione con successo
					if(transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}
				    //Eccezione di deadlock
				catch (DeadLockException ex)
				{
                    TraceLog.WriteLine("WorkPhaseCollection: Deadlock Error", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
				}
					// Altre eccezioni
				catch (Exception ex)
				{
                    TraceLog.WriteLine("WorkPhaseCollection: Error", ex);

					// Annulla la transazione
					if(transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

        //**********************************************************************
        // GetXmlParameterFromDb
        // Legge il file parametri xml dal database
        // Parametri:
        //			wkph		: fase di lavoro di cui leggere l'XML
        //			fileName	: nome del file su cui memorizzare l'XML
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool GetXmlParameterFromDb(WorkPhase wkph, string fileName)
        {
            try
            {
                // Recupera l'XML dal db
                if (!mDbInterface.GetBlobField("T_WK_WORK_PHASES_H", "F_TECNO_INFO", " F_ID = " + wkph.gId.ToString(), fileName))
                    return false;
            }

                // Eccezione
            catch (Exception ex)
            {
                TraceLog.WriteLine("WorkPhaseCollection.GetXmlParameterFromDb: Error", ex);
                return false;
            }

            wkph.iTecnoInfo(fileName);
            return true;
        }

        //**********************************************************************
        // SetXmlParameterToDb
        // Scrive il file parametri xml nel database
        // Parametri:
        //			whph		: fase di lavoro di cui scrivere l'XML
        //			fileName	: nome del file da cui prelevare l'XML
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool SetXmlParameterToDb(WorkPhase wkph, string fileName)
        {
            bool transaction = !mDbInterface.TransactionActive();

            try
            {
                if (transaction) mDbInterface.BeginTransaction();

                // Scrive l'XML su db
                if (!mDbInterface.WriteBlobField("T_WK_WORK_PHASES_H", "F_TECNO_INFO", " F_ID = " + wkph.gId.ToString(), fileName))
                    throw (new ApplicationException("error writing xml"));

                if (transaction) mDbInterface.EndTransaction(true);

                wkph.iTecnoInfo(fileName);
                return true;
            }

                // Eccezione
            catch (Exception ex)
            {
                if (transaction) mDbInterface.EndTransaction(false);
                TraceLog.WriteLine("WorkPhaseCollection.SetXmlParameterToDb: Error", ex);
                return false;
            }
        }

        ///**************************************************************************
        /// CreateOptiShape
        /// <summary>
        /// Crea i formati che devono essere analizzati dall'ottimizzatore
        /// </summary>
        /// <param name="srcDet">Dettaglio di cui creare i formati per l'optimaster</param>
        ///**************************************************************************
        public bool CreateOptiShape(Breton.ImportOffer.Detail det)
        {
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();
            OleDbParameter par;
            ArrayList parameters = new ArrayList();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre una transazione
                    if (transaction) mDbInterface.BeginTransaction();

                    par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
                    par.Direction = System.Data.ParameterDirection.ReturnValue;
                    parameters.Add(par);

                    par = new OleDbParameter("@IdOrdDet", OleDbType.Integer);
                    par.Value = det.gId;
                    parameters.Add(par);

                    // Lancia la stored procedure
                    if (!mDbInterface.Execute("sp_opm_shapes_update", ref parameters))
                        throw new ApplicationException("WorkPhaseCollection.CreateOptiShape sp_opm_shapes_update Execute error.");

                    par = (OleDbParameter)parameters[0];
                    if (((int)par.Value) != 0)
                        throw new ApplicationException("WorkPhaseCollection.CreateOptiShape sp_opm_shapes_update Execute error: " + ((int)par.Value));

                    // Chiude la transazione
                    if (transaction)
                        mDbInterface.EndTransaction(true);

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("WorkPhaseCollection.CreateOptiShape Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("WorkPhaseCollection.CreateOptiShape Error.", ex);

                    return false;
                }
            }

            return false;
        }

		public override void SortByField(string key)
		{
		}

        ///**************************************************************************
        /// SetWorkPhaseHState
        /// <summary>
        /// Imposta lo stato per la work phase
        /// </summary>
        /// <param name="wk">work phase h da aggiornare</param>
        /// <param name="state">stato da impostare</param>
        ///**************************************************************************
        public bool SetWorkPhaseHState(WorkPhase wk, WorkPhase.State state)
        {
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();
            OleDbParameter par;
            ArrayList parameters = new ArrayList();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre una transazione
                    if (transaction) mDbInterface.BeginTransaction();

                    par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
                    par.Direction = System.Data.ParameterDirection.ReturnValue;
                    parameters.Add(par);

                    par = new OleDbParameter("@WorkPhaseCode", OleDbType.VarChar, 18);
                    par.Value = wk.gCode;
                    parameters.Add(par);

                    par = new OleDbParameter("@state_code", OleDbType.VarChar, 18);
                    par.Value = state.ToString();
                    parameters.Add(par);

                    par = new OleDbParameter("@StationCode", OleDbType.VarChar, 18);
                    par.Value = "";
                    parameters.Add(par);

                    // Lancia la stored procedure
                    if (!mDbInterface.Execute("sp_pro_update_work_phases_h_state", ref parameters))
                        throw new ApplicationException("WorkPhaseCollection.SetWorkPhaseHState sp_pro_update_work_phases_h_state Execute error.");

                    par = (OleDbParameter)parameters[0];
                    if (((int)par.Value) != 0)
                        throw new ApplicationException("WorkPhaseCollection.SetWorkPhaseHState sp_pro_update_work_phases_h_state Execute error: " + ((int)par.Value));

                    // Chiude la transazione
                    if (transaction)
                        mDbInterface.EndTransaction(true);

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("WorkPhaseCollection.SetWorkPhaseHState Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("WorkPhaseCollection.SetWorkPhaseHState Error.", ex);

                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Restitusce l'indice massimo della work phase h trovata. 
        /// Se si sono WKPH00001_001, WKPH00001_002 e WKPH00001_003 restituisce 3
        /// </summary>
        /// <param name="wk">work phase da analizzare</param>
        /// <returns></returns>
        public int GetMaxWorkPhaseCode(string code)
        {
            OleDbDataReader dr = null;

            int maxIndex = 0;

            string sqlQuery = "select right(rtrim(max(F_CODE)), 3) as VAL " +
                              "from T_WK_WORK_PHASES_H " +
                              "where F_CODE like '%" + code + "%'";

            try
            {
                if (!mDbInterface.Requery(sqlQuery, out dr))
                    throw new ApplicationException("Error executing query");

                while (dr.Read())
                    maxIndex = (dr["VAL"] == DBNull.Value ? 0 : Convert.ToInt32(dr["VAL"]));

                return maxIndex;
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("WorkPhaseCollection.GetMaxWorkPhaseCode: Error", ex);
                return -1;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

		#endregion

        #region Private Methods

        
        /// <summary>
        /// Riempie o modifica i record della tabella T_WK_WORK_PHASES_D con l'oggetto ottenuto dopo la lavorazione
        /// </summary>
        /// <param name="wkph">fase di lavoro di riferimento</param>
        /// <returns></returns>
        private bool SetPhasesD(WorkPhase wkph)
        {
            string sqlInsert; //, sqlUpdate;

            try
            {
                if (wkph.gState == DbObject.ObjectStates.Inserted)
                {
                    sqlInsert = "insert into T_WK_WORK_PHASES_D(F_ID_T_WK_WORK_PHASES_H, F_ID_T_WK_USED_OBJECTS) " +
                                "select " + wkph.gId + ", u.F_ID from T_WK_USED_OBJECTS u where u.F_CODE = '" + wkph.gPhaseDUsedObject + "';";

                    if (!mDbInterface.Execute(sqlInsert))
                        return false;
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        //**********************************************************************
        // GetDataFromDb
        // Legge i dati dal database, secondo la query specificata
        // Parametri:
        //			sqlQuery	: query da eseguire
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        private bool GetDataFromDb(string sqlQuery)
        {
            OleDbDataReader dr = null;
            OleDbDataReader drWorkPhase = null;
            string sqlWorkPhase;
            WorkPhase wkph;
           
            Clear();

            // Verifica se la query � valida
            if (sqlQuery == "")
                return false;
            
            try
            {
                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return false;

                while (dr.Read())
                {
                    sqlWorkPhase = "select u.F_CODE as F_PHASES_D_USED_OBJECT_CODE  from T_WK_USED_OBJECTS u, T_WK_WORK_PHASES_D pd " +
                                   "where u.F_ID = pd.F_ID_T_WK_USED_OBJECTS and pd.F_ID = " + dr["F_PHASES_D_USED_OBJECT"].ToString();
                    if (!mDbInterface.Requery(sqlWorkPhase, out drWorkPhase))
                        return false;


                    while (drWorkPhase.Read())
                    {

                        wkph = new WorkPhase((int)dr["F_ID"], dr["F_CODE"].ToString(), (int)dr["F_SEQUENCE"], (DateTime)dr["F_DATE"],
                            (dr["F_START_DATE"] == DBNull.Value ? DateTime.MaxValue : (DateTime)dr["F_START_DATE"]),
                            (dr["F_STOP_DATE"] == DBNull.Value ? DateTime.MaxValue : (DateTime)dr["F_STOP_DATE"]),
                            dr["F_STATE"].ToString(), dr["F_WORK_ORDER_CODE"].ToString(), dr["F_DETAIL_CODE"].ToString(),
                            dr["F_USED_OBJECT_CODE"].ToString(), drWorkPhase["F_PHASES_D_USED_OBJECT_CODE"].ToString());


                        AddObject(wkph);
                        wkph.gState = DbObject.ObjectStates.Unchanged;
                    }
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("WorkPhaseCollection: Error", ex);

                return false;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
                mDbInterface.EndRequery(drWorkPhase);
            }

            // Salva l'ultima query eseguita
            mSqlGetData = sqlQuery;
            return true;
        }


        #endregion
	}
}
