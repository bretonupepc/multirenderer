using System;
using System.Collections.Generic;
using System.Text;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.Phases
{
    public class UsedObject : DbObject
    {
        #region Variables
        public const string CODE_PREFIX = "USOBJ";

        public enum Type
        {
            None,
            Origin,
            Intermediate,
            Final
        }

        private int mId;
        private string mCode;
        private string mArticleCode;
        private Type mObjectType;

        private string mWorkingInfo;			//Link memorizzazione temporanea file XML
        private string mShapeInfo;              //Link memorizzazione temporanea file XML

        public enum Keys
        { F_NONE = -1, F_ID, F_CODE, F_ARTICLE_CODE, F_MAX };

        #endregion
        
        #region Constructor
        public UsedObject(int id, string code, string artCode, Type objType)
        {
            mId = id;
            mCode = code;
            mArticleCode = artCode;
            mObjectType = objType;
        }

        public UsedObject() : this(0, null, null, Type.None)
        {
        }
        #endregion

        #region Properties
        public int gId
        {
            set
            {
                mId = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mId; }
        }
        public string gCode
        {
            set
            {
                mCode = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mCode; }
        }

        public string gArticleCode
        {
            set
            {
                mArticleCode = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mArticleCode; }
        }

        public Type gObjectType
        {
            set { mObjectType = value; }
            get { return mObjectType; }
        }

        public string gWorkingInfo
        {
            set
            {
                mWorkingInfo = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mWorkingInfo; }
        }

        internal void iWorkingInfo(string link)
        {
            mWorkingInfo = link;
        }

        public string gShapeInfo
        {
            set
            {
                mShapeInfo = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mShapeInfo; }
        }

        internal void iShapeInfo(string link)
        {
            mShapeInfo = link;
        }

        #endregion

        #region IComparable interface

        //**********************************************************************
        // CompareTo
        // Esegue la comparazione con un altro oggetto dello stesso tipo
        // Parametri:
        //			obj	: oggetto
        //**********************************************************************
        public override int CompareTo(object obj)
        {
            if (obj is UsedObject)
            {
                UsedObject usobj = (UsedObject)obj;

                return mCode.CompareTo(usobj.gCode);
            }

            throw new ArgumentException("object is not a Used Object");
        }

        #endregion

        #region Public Methods

        //**********************************************************************
        // GetFieldType
        // Ritorna il tipo del campo
        // Parametri:
        //			index	: indice del campo
        // Ritorna:
        //			tipo del campo
        //**********************************************************************
        public override TypeCode GetFieldType(int index)
        {
            if (IsFieldIndexOk(index))
                return GetFieldType(((Keys)index).ToString());
            else
                return TypeCode.DBNull;
        }

        //**********************************************************************
        // GetFieldType
        // Ritorna il tipo del campo
        // Parametri:
        //			key	: nome del campo
        // Ritorna:
        //			tipo del campo
        //**********************************************************************
        public override TypeCode GetFieldType(string key)
        {
            if (key.ToUpper() == UsedObject.Keys.F_ID.ToString())
                return mId.GetTypeCode();
            if (key.ToUpper() == UsedObject.Keys.F_CODE.ToString())
                return mCode.GetTypeCode();
            if (key.ToUpper() == UsedObject.Keys.F_ARTICLE_CODE.ToString())
                return mArticleCode.GetTypeCode();
            
            return TypeCode.DBNull;
        }

        //**********************************************************************
        // GetField
        // Ritorna il valore del campo
        // Parametri:
        //			index		: indice del campo
        //			retValue	: valore ritornato
        // Ritorna:
        //			true	valore ritornato
        //			false	campo non valido
        //**********************************************************************
        public override bool GetField(int index, out int retValue)
        {
            if (IsFieldIndexOk(index))
                return base.GetField(index, out retValue);
            else
                return GetField(((Keys)index).ToString(), out retValue);
        }

        public override bool GetField(int index, out string retValue)
        {
            if (IsFieldIndexOk(index))
                return base.GetField(index, out retValue);
            else
                return GetField(((Keys)index).ToString(), out retValue);
        }


        //**********************************************************************
        // GetField
        // Ritorna il valore del campo
        // Parametri:
        //			key			: nome del campo
        //			retValue	: valore ritornato
        // Ritorna:
        //			true	valore ritornato
        //			false	campo non valido
        //**********************************************************************
        public override bool GetField(string key, out int retValue)
        {
            if (key.ToUpper() == UsedObject.Keys.F_ID.ToString())
            {
                retValue = mId;
                return true;
            }

            return base.GetField(key, out retValue);
        }

        public override bool GetField(string key, out string retValue)
        {
            if (key.ToUpper() == UsedObject.Keys.F_ID.ToString())
            {
                retValue = mId.ToString();
                return true;
            }

            if (key.ToUpper() == UsedObject.Keys.F_CODE.ToString())
            {
                retValue = mCode;
                return true;
            }

            if (key.ToUpper() == UsedObject.Keys.F_ARTICLE_CODE.ToString())
            {
                retValue = mArticleCode;
                return true;
            }

            return base.GetField(key, out retValue);
        }

        #endregion

        #region Private Methods
        //**********************************************************************
        // IsFieldIndexOk
        // Verifica se l'indice del campo � valido
        // Parametri:
        //			index	: indice
        // Ritorna:
        //			true	indice valido
        //			false	indice non valido
        //**********************************************************************
        private bool IsFieldIndexOk(int index)
        {
            return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
        }

        #endregion
    }
}
