using System;
using System.Data;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.Phases
{
	public class WorkOrderCollection: DbCollection
	{
		#region Constructor
		public WorkOrderCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out WorkOrder obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (WorkOrder) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out WorkOrder obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (WorkOrder) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}
		
		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(WorkOrder obj)
		{
			return (base.AddObject((WorkOrder) obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(WorkOrder.Keys.F_NONE, code, null, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(WorkOrder.Keys.F_NONE, null, null, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(WorkOrder.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null, null, null, null);
		}

        //**********************************************************************
        // GetDataFromDb
        // Legge i dati dal database, ordinati per la chiave specificata
        // Parametri:
        //			ids	    : array con gli id da ricercare
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool GetDataFromDb(int[] ids)
        {
            return GetDataFromDb(WorkOrder.Keys.F_NONE, null, null, null, null, ids, null);
        }

       
        /// <summary>
        /// Legge i dati dal database, ordinati per la chiave specificata
        /// </summary>
        /// <param name="codes">array con i codici da ricercare</param>
        /// <returns>true	lettura eseguita, false	lettura non eseguita</returns>
        public bool GetDataFromDb(string[] codes)
        {
            return GetDataFromDb(WorkOrder.Keys.F_NONE, null, null, null, null, null, codes);
        }

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		//			state		: estrae solo gli elementi dello stato specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(WorkOrder.Keys orderKey, string code, string workCenterGroupCode, string workCenterCode, string state, int[] ids, string[] codes)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";


			//Estrae solo l'ordine di lavoro del codice richiesto
			if (code != null)
			{
				sqlWhere += sqlAnd + "o.F_CODE = '" + DbInterface.QueryString(code) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'ordine di lavoro di un determinato gruppo di macchine
			if (workCenterGroupCode != null)
			{
				sqlWhere += sqlAnd + "wcg.F_CODE = '" + DbInterface.QueryString(workCenterGroupCode) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo l'ordine di lavoro di una determinata macchina
			if (workCenterCode != null)
			{
				sqlWhere += sqlAnd + "wc.F_CODE = '" + DbInterface.QueryString(workCenterCode) + "'";
				sqlAnd = " and ";
			}

			//Estrae solo gli ordini lavoro con lo stato richiesto
			if (state != null)
			{
				sqlWhere += sqlAnd + "s.F_CODE= '" + DbInterface.QueryString(state) + "'";
				sqlAnd = " and ";
			}

            if (ids != null)
                if (ids.Length > 0)
                {
                    sqlWhere += sqlAnd + "o.F_ID in (" + ids[0];
                    for (int i = 1; i < ids.Length; i++)
                        sqlWhere += "," + ids[i];
                    sqlWhere += ")";
                }

            if (codes != null)
                if (codes.Length > 0)
                {
                    sqlWhere += sqlAnd + "o.F_CODE in ('" + codes[0];
                    for (int i = 1; i < codes.Length; i++)
                        sqlWhere += "', '" + codes[i];
                    sqlWhere += "')";
                }

			switch (orderKey)
			{
				case WorkOrder.Keys.F_ID:
					sqlOrderBy = " order by o.F_ID";
					break;
				case WorkOrder.Keys.F_CODE:
					sqlOrderBy = " order by o.F_CODE";
					break;
				case WorkOrder.Keys.F_WORK_CENTER_CODE:
					sqlOrderBy = " order by wc.F_CODE";
					break;
				case WorkOrder.Keys.F_WORK_CENTER_GROUP_CODE:
					sqlOrderBy = " order by wcg.F_CODE";
					break;
				case WorkOrder.Keys.F_SEQUENCE:
					sqlOrderBy = " order by o.F_SEQUENCE";
					break;
				case WorkOrder.Keys.F_STATE:
					sqlOrderBy = " order by s.F_CODE";
					break;				
			}
			return GetDataFromDb("select o.*, wc.F_CODE as F_WORK_CENTER_CODE, wc1.F_CODE as F_WORK_CENTER_PROG_CODE, wcg.F_CODE as F_WORK_CENTER_GROUP_CODE, s.F_CODE as F_STATE " +
				"from T_WK_WORK_ORDERS o " +
                "left outer join T_AN_WORK_CENTER_GROUPS wcg on wcg.F_ID = o.F_ID_T_AN_WORK_CENTER_GROUPS " +
                "left outer join T_AN_WORK_CENTERS wc on wc.F_ID = o.F_ID_T_AN_WORK_CENTERS " +
				"left outer join T_AN_WORK_CENTERS wc1 on wc1.F_ID = o.F_ID_T_AN_WORK_CENTER_PROG " +
                "left outer join T_CO_WORK_ORDERS_STATES s on s.F_ID = o.F_ID_T_CO_WORK_ORDERS_STATES " +
				sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// 
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();


			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if(transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti gli ordini di lavoro
					foreach (WorkOrder wkord in mRecordset)
					{
						switch (wkord.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "INSERT INTO T_WK_WORK_ORDERS (" + (wkord.gWorkCenterCode.Trim() == "" ? "" : "F_ID_T_AN_WORK_CENTERS, ") +
									(wkord.gWorkCenterProgCode.Trim() == "" ? "" : "F_ID_T_AN_WORK_CENTER_PROG, ") + 
									"F_ID_T_AN_WORK_CENTER_GROUPS, F_ID_T_CO_WORK_ORDERS_STATES, F_SEQUENCE, F_START_DATE, F_STOP_DATE, " +
									"F_DESCRIPTION) " +
									"(SELECT " + (wkord.gWorkCenterCode.Trim() == "" ? "" : "wc1.F_ID, ") + (wkord.gWorkCenterProgCode.Trim() == "" ? "" : "wc2.F_ID, ") + "wcg.F_ID, " +
                                    "s.F_ID, " + wkord.gSequence + ", " + (wkord.gDateStart == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkord.gDateStart)) + ", " +
                                    (wkord.gDateStop == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkord.gDateStop)) + ", '" + DbInterface.QueryString(wkord.gDescription) + "' " +
									"FROM " + (wkord.gWorkCenterCode.Trim() == "" ? "" : "T_AN_WORK_CENTERS wc1, ") + (wkord.gWorkCenterProgCode.Trim() == "" ? "" : "T_AN_WORK_CENTERS wc2, ") + 
											"T_AN_WORK_CENTER_GROUPS wcg, T_CO_WORK_ORDERS_STATES s " +
                                    "WHERE " + (wkord.gWorkCenterCode.Trim() == "" ? "" : "wc1.F_CODE = '" + wkord.gWorkCenterCode.Trim() + 
										(wkord.gWorkCenterProgCode.Trim() == "" ? "" : "wc2.F_CODE = '" + wkord.gWorkCenterProgCode.Trim()) + "' AND ") + "wcg.F_CODE = '" + 
                                    wkord.gWorkCenterGroupCode.Trim() + "' AND s.F_CODE = '" + wkord.gStateWorkOrd.Trim() +  "');SELECT SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
                                wkord.gId = id;

                                // Inserisce i valori nella tabella T_WK_WORKING_TYPES
                                if ((wkord.gPhaseWorkingsId.Length > 0) && !SetWorkingTypes(wkord))
                                    throw new ApplicationException("WorkOrderCollection.SetWorkingTypes Error on creating working types");
								
								break;
					
								//Aggiornamento
                            case DbObject.ObjectStates.Updated:
                                sqlUpdate = "UPDATE T_WK_WORK_ORDERS SET " +
                                    "F_CODE = '" + DbInterface.QueryString(wkord.gCode) +
                                    "', F_SEQUENCE = " + wkord.gSequence.ToString() +
                                    ", F_START_DATE = " + (wkord.gDateStart == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkord.gDateStart)) +
                                    ", F_STOP_DATE = " + (wkord.gDateStart == DateTime.MaxValue ? "null" : mDbInterface.OleDbToDate(wkord.gDateStart));

                                //Verifica se � stato modificato il work center
                                if (wkord.gWorkCenterCodeChanged)
                                    sqlUpdate += ", F_ID_T_AN_WORK_CENTERS = "
                                        + " (SELECT F_ID FROM T_AN_WORK_CENTERS"
                                        + " WHERE F_CODE = '" + DbInterface.QueryString(wkord.gWorkCenterCode.Trim()) + "')";

								//Verifica se � stato modificato il work center programmato
								if (wkord.gWorkCenterProgCodeChanged)
									sqlUpdate += ", F_ID_T_AN_WORK_CENTER_PROG = "
										+ " (SELECT F_ID FROM T_AN_WORK_CENTERS"
										+ " WHERE F_CODE = '" + DbInterface.QueryString(wkord.gWorkCenterProgCode.Trim()) + "')";

                                //Verifica se � stato modificato il work center group
                                if (wkord.gWorkCenterCodeGroupChanged)
                                    sqlUpdate += ", F_ID_T_AN_WORK_CENTER_GROUPS = "
                                        + " (SELECT F_ID FROM T_AN_WORK_CENTER_GROUPS"
                                        + " WHERE F_CODE = '" + DbInterface.QueryString(wkord.gWorkCenterGroupCode) + "')";

                                //Verifica se � stato modificato lo stato dell'ordine di lavoro
                                if (wkord.gStateWorkOrdChanged)
                                    sqlUpdate += ", F_ID_T_CO_WORK_ORDERS_STATES = "
                                        + " (SELECT F_ID FROM T_CO_WORK_ORDERS_STATES"
                                        + " WHERE F_CODE = '" + DbInterface.QueryString(wkord.gStateWorkOrd) + "')";

                                sqlUpdate += " WHERE F_ID = " + wkord.gId.ToString();

                               if (!mDbInterface.Execute(sqlUpdate))
                                    return false;

                                // Modifica i valori nella tabella T_WK_WORKING_TYPES
                                if (wkord.gPhaseWorkingsIdChanged && wkord.gPhaseWorkingsId.Length > 0 && !SetWorkingTypes(wkord))
                                    throw new ApplicationException("WorkOrderCollection.SetWorkingTypes Error on updating working types");

                                break;
						}
					}

					//Scorre tutti gli ordini di lavoro cancellati
                    foreach (WorkOrder wkord in mDeleted)
                    {
                        switch (wkord.gState)
                        {
                            //Cancellazione
                            case DbObject.ObjectStates.Deleted:
                                sqlDelete = " delete from T_WK_WORKING_TYPES where F_ID_T_WK_WORK_ORDERS = " + wkord.gId.ToString() +
                                            " delete from T_WK_WORK_ORDERS where F_ID = " + wkord.gId.ToString();
                                    
                                if (!mDbInterface.Execute(sqlDelete))
                                    return false;
                                break;
                        }
                    }

					// Termina la transazione con successo
					if(transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					if (mSqlGetData != "")
                        GetDataFromDb(mSqlGetData);
                    else
                    {
                        int[] ids = new int[mRecordset.Count];
                        for (int i = 0; i < mRecordset.Count; i++)
                            ids[i] = ((WorkOrder)mRecordset[i]).gId;

                        GetDataFromDb(ids);
                        mSqlGetData = "";
                    }

					return true;
				}

					//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("WorkOrderCollection: Deadlock Error", ex);

					// Annulla la transazione
					if(transaction)	mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
                        throw new ApplicationException("WorkOrderCollection: Exceed Deadlock retrying", ex);
				}

					// Altre eccezioni
				catch (Exception ex)
				{
                    TraceLog.WriteLine("WorkOrderCollection: Error", ex);

					// Annulla la transazione
					if(transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

        //**********************************************************************
        // GetWorkingTypesFromPhase
        // Carica dal dataBase ti valori di WorkingTypes partendo dalla fase di
        // lavoro
        // Parametri:
        //			wkord	: ordine di lavoro di riferimento
        // Ritorna:
        //			true	scrittura eseguita
        //			false	scrittura non eseguita
        //**********************************************************************
        public int GetWorkingTypesFromPhase(int phaseWorkingTypes)
        {
            string sqlQuery;
            int wt = -1;
            OleDbDataReader dr = null;

            sqlQuery = "SELECT F_ID_T_LS_WORKING_TYPES FROM T_LS_PHASE_WORKINGS WHERE F_ID = " + phaseWorkingTypes.ToString();

            try
            {
                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return -1;

                while (dr.Read())
                {
                    wt = (int)dr["F_ID_T_LS_WORKING_TYPES"];
                }

                return wt;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("WorkOrderCollection.GetWorkingTypesFromPhase: Error", ex);

                return -1;
            }
            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

		public override void SortByField(string key)
		{
		}

		#endregion

        #region Private Methods

        //**********************************************************************
        // SetWorkingTypes
        // Riempie o modifica i record della tabella T_WK_WORKING_TYPES con i tipi di lavorazione 
        // che saranno eseguiti da ogni macchina
        // Parametri:
        //			wkord	: ordine di lavoro di riferimento
        // Ritorna:
        //			true	scrittura eseguita
        //			false	scrittura non eseguita
        //**********************************************************************
        private bool SetWorkingTypes(WorkOrder wkord)
        {
            string sqlInsert, sqlDelete;
            int id;

            try
            {
                if (wkord.gState == DbObject.ObjectStates.Updated)
                {
                    sqlDelete = "DELETE FROM T_WK_WORKING_TYPES WHERE F_ID_T_WK_WORK_ORDERS = " + wkord.gId.ToString();

                    if (!mDbInterface.Execute(sqlDelete))
                        return false;
                }
                wkord.gWorkingTypesId = new int[wkord.gPhaseWorkingsId.Length];
                for (int i = 0; i < wkord.gPhaseWorkingsId.Length; i++)
                {

                    sqlInsert = "INSERT INTO T_WK_WORKING_TYPES(F_ID_T_WK_WORK_ORDERS, F_ID_T_LS_PHASE_WORKINGS, F_SEQUENCE) " +
                                "VALUES(" + wkord.gId.ToString() + ", " + wkord.gPhaseWorkingsId[i].ToString() + ", " + i.ToString() + 
                                ");SELECT SCOPE_IDENTITY()";

                    if (!mDbInterface.Execute(sqlInsert, out id))
                        return false;

                    wkord.gWorkingTypesId[i] = id;
                }                
                return true;
            }
            catch 
            {
                return false;
            }
        }


        //**********************************************************************
        // GetWorkingTypesFromOrder
        // Carica dal dataBase ti valori di WorkingTypes partendo dall'ordine di
        // lavoro
        // Parametri:
        //			wkord	: ordine di lavoro di riferimento
        // Ritorna:
        //			true	scrittura eseguita
        //			false	scrittura non eseguita
        //**********************************************************************
        private bool GetWorkingTypesFromOrder(WorkOrder wkord, ref int[] wt)
        {
            string sqlQuery;
            int i = 0;
            OleDbDataReader drWT = null;
            wt = new int[0];

            sqlQuery = "SELECT F_ID FROM T_WK_WORKING_TYPES WHERE F_ID_T_WK_WORK_ORDERS = " + wkord.gId.ToString();

            try
            {
                if (!mDbInterface.Requery(sqlQuery, out drWT))
                    return false;

                while (drWT.Read())
                {
                    int[] tmp = new int[wt.Length + 1];
                    wt.CopyTo(tmp, 0);
                    wt = tmp;
                    wt[i] = (int)drWT["F_ID"];
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("WorkOrderCollection.GetWorkingTypesFromOrder: Error", ex);

                return false;
            }
            finally
            {
                //mDbInterface.EndRequery(drWT);
            }
        }

        //**********************************************************************
        // GetPhaseWorkingFromOrder
        // Carica dal dataBase ti valori di PhaseWorking partendo dall'ordine di
        // lavoro
        // Parametri:
        //			wkord	: ordine di lavoro di riferimento
        // Ritorna:
        //			true	scrittura eseguita
        //			false	scrittura non eseguita
        //**********************************************************************
        private bool GetPhaseWorkingFromOrder(WorkOrder wkord, ref int[] pw)
        {
            string sqlQuery;
            int i = 0;
            OleDbDataReader drPW = null;
            pw = new int[0];

            sqlQuery = "SELECT F_ID_T_LS_PHASE_WORKINGS FROM T_WK_WORKING_TYPES WHERE F_ID_T_WK_WORK_ORDERS = " + wkord.gId.ToString();

            try
            {
                if (!mDbInterface.Requery(sqlQuery, out drPW))
                    return false;

                while (drPW.Read())
                {
                    int[] tmp = new int[pw.Length + 1];
                    pw.CopyTo(tmp, 0);
                    pw = tmp;
                    pw[i] = (int)drPW["F_ID_T_LS_PHASE_WORKINGS"];
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("WorkOrderCollection.GetPhaseWorkingFromOrder: Error", ex);

                return false;
            }
            finally
            {
                //mDbInterface.EndRequery(drWT);
            }
        }

		//**********************************************************************
		// GetTecnoWorkingFromOrder
		// Carica dal dataBase i valori di TecnoWorking partendo dall'ordine di
		// lavoro
		// Parametri:
		//			wkord	: ordine di lavoro di riferimento
		// Ritorna:
		//			true	scrittura eseguita
		//			false	scrittura non eseguita
		//**********************************************************************
		private bool GetTecnoWorkingFromOrder(WorkOrder wkord, ref string[] tw)
        {
            string sqlQuery;
            int i = 0;
            OleDbDataReader drTW = null;
            tw = new string[0];

			sqlQuery = "SELECT T_CO_TECNO_WORKING_TYPES.F_CODE " +
						"FROM T_CO_TECNO_WORKING_TYPES " +
						"INNER JOIN T_LS_WORKING_TYPES ON T_CO_TECNO_WORKING_TYPES.F_ID = T_LS_WORKING_TYPES.F_ID_T_CO_TECNO_WORKING_TYPES " +
						"INNER JOIN T_LS_PHASE_WORKINGS ON T_LS_WORKING_TYPES.F_ID = T_LS_PHASE_WORKINGS.F_ID_T_LS_WORKING_TYPES " +
						"INNER JOIN T_WK_WORKING_TYPES ON T_LS_PHASE_WORKINGS.F_ID = T_WK_WORKING_TYPES.F_ID_T_LS_PHASE_WORKINGS " +
						"WHERE T_WK_WORKING_TYPES.F_ID_T_WK_WORK_ORDERS = " + wkord.gId.ToString();

            try
            {
				if (!mDbInterface.Requery(sqlQuery, out drTW))
                    return false;

				while (drTW.Read())
                {
                    string[] tmp = new string[tw.Length + 1];
                    tw.CopyTo(tmp, 0);
                    tw = tmp;
					tw[i] = drTW["F_CODE"].ToString();
                    i++;
                }
                return true;
            }
            catch (Exception ex)
            {
				TraceLog.WriteLine("WorkOrderCollection.GetTecnoWorkingFromOrder: Error", ex);

                return false;
            }
        }

        //**********************************************************************
        // GetDataFromDb
        // Legge i dati dal database, secondo la query specificata
        // Parametri:
        //			sqlQuery	: query da eseguire
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        private bool GetDataFromDb(string sqlQuery)
        {
            OleDbDataReader dr = null;
            WorkOrder wkord;
            int[] idWT, idPW;
			string[] tcnWk;

            Clear();

            // Verifica se la query � valida
            if (sqlQuery == "")
                return false;
            
            try
            {
                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return false;

                while (dr.Read())
                {
                    wkord = new WorkOrder((int)dr["F_ID"], dr["F_CODE"].ToString(), (int)dr["F_SEQUENCE"], (DateTime)dr["F_DATE"],
                        (dr["F_START_DATE"] == DBNull.Value ? DateTime.MaxValue : (DateTime)dr["F_START_DATE"]), 
                        (dr["F_STOP_DATE"] == DBNull.Value ? DateTime.MaxValue : (DateTime)dr["F_STOP_DATE"]), 
                        dr["F_STATE"].ToString(), dr["F_DESCRIPTION"].ToString(), dr["F_WORK_CENTER_CODE"].ToString(),
						dr["F_WORK_CENTER_PROG_CODE"].ToString(), dr["F_WORK_CENTER_GROUP_CODE"].ToString(), null, null, null);

                    idWT = wkord.gWorkingTypesId;
                    if (!GetWorkingTypesFromOrder(wkord, ref idWT))
                        return false;
                    else
                        wkord.gWorkingTypesId = idWT;

                    idPW = wkord.gPhaseWorkingsId;
                    if (!GetPhaseWorkingFromOrder(wkord, ref idPW))
                        return false;
                    else
                        wkord.gPhaseWorkingsId = idPW;

					tcnWk = wkord.gTecnoWorkingTypes;
					if (!GetTecnoWorkingFromOrder(wkord, ref tcnWk))
						return false;
					else
						wkord.gTecnoWorkingTypes = tcnWk;

                    AddObject(wkord);
                    wkord.gState = DbObject.ObjectStates.Unchanged;
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("WorkOrderCollection: Error", ex);

                return false;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
            }

            // Salva l'ultima query eseguita
            mSqlGetData = sqlQuery;
            return true;
        }


        #endregion
    }
}
