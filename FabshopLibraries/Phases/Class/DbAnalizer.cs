using System;
using System.Threading;
using System.Collections;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.Phases
{
	public class DbAnalizer
	{
		#region Variables
		private DbInterface mDbInterface;

		public enum WorkingTypes
		{
			CUT,
			CUT_HOLES,
            BUSSOLA,
			CONTOUR,
			LABEL_AUTO_PIECES,
			VGROOVE_PRINT,
			VGROOVE,
            CQF,
            EULITHE_PRINT,
            CONTOUR_LINEAR,
			REINFORCEMENT,
            NOT_MANAGED,
			LAMINATION_GLUE_1,
			LAMINATION_GLUE_2
		}

		public enum WorkingCenterTypes
		{
			COMBICUT,
			FK,
			NC,
			LABEL_AUTO_PIECES,
			VGROOVE,
			VGROOVE_PRINT,
            CQF,
			LABEL_MAN_PIECES,
			NOT_MANAGED,
			EDGE_POLISH,
			DRILLING,
			OPTIMA
		}

        public struct Working
        {
            public string tecnoWorking;
            public int executionOrder;
        }

		public struct WorkCenter
		{
			public string code;
			public string description;
		}
		#endregion

		#region Constructor
		public DbAnalizer(DbInterface db)
		{
			mDbInterface = db;
		}
		#endregion

		#region Public Methods
		/***************************************************************************
		* GetWorkingTypes
		* Legge i tipi di lavorazioni codificate nel DB
		* Parametri:
		*			wk		: array dove saranno restituite le lavorazioni trovate
		*			prid	: id del proceso di riferimento
		****************************************************************************/
		public void GetWorkingTypes(out ArrayList wk, int prId)
		{
			OleDbDataReader dr = null;

			//wk = new string[0];
            wk = new ArrayList();

			string sqlQuery = "SELECT DISTINCT tt.F_CODE AS F_WORKING, pw.F_EXECUTION_ORDER AS F_EXEC_ORDER " +
				"FROM T_LS_PHASE_WORKINGS pw " +
				"JOIN T_LS_WORKING_TYPES wt ON pw.F_ID_T_LS_WORKING_TYPES = wt.F_ID " +
				"JOIN T_CO_TECNO_WORKING_TYPES tt ON wt.F_ID_T_CO_TECNO_WORKING_TYPES = tt.F_ID " +
				"WHERE pw.F_ID_T_LS_PRODUCTION_PROCESSES =" + prId + " AND pw.F_DEFAULT = 0 " +
				"ORDER BY pw.F_EXECUTION_ORDER";			

			// Verifica se la query � valida
			if (sqlQuery == "")
				wk = null;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					wk = null;

				while (dr.Read())
				{
                    Working work = new Working();
                    work.tecnoWorking = dr["F_WORKING"].ToString().Trim();
                    work.executionOrder = (int)dr["F_EXEC_ORDER"];
                    wk.Add(work);
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("DbAnalizer.GetWorkingTypes: Error", ex);
				wk = null;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

        /// <summary>
        /// Funzione che carica il vettore con le lavorazioni tecno trovate
        /// </summary>
        /// <param name="wk">array dove saranno restituite le lavorazioni trovate</param>
        /// <param name="ids">array contenente gli id delle phase working</param>       
        public bool GetWorkingTypesFromId(out ArrayList tecnoWorking, out ArrayList designWorking, out ArrayList idPhase,
            out ArrayList descrPhase, int[] idsWt)//int[] idsPw)
        {
            OleDbDataReader dr = null;
            string sqlWhere = "where ", sqlAnd = "";

            tecnoWorking = new ArrayList();
            designWorking = new ArrayList();
            idPhase = new ArrayList();
            descrPhase = new ArrayList();

            string sqlQuery = "select tw.F_CODE as F_TECNO_CODE, dwt.F_CODE as F_DESIGN_CODE, pw.F_ID as F_ID_T_LS_PHASE_WORKINGS, pw.F_DESCRIPTION as F_WORKING " +
                             "from T_LS_DESIGN_WORKING_TYPES dwt " +
                             "right outer join T_LS_WORKING_TYPES on dwt.F_ID = T_LS_WORKING_TYPES.F_ID_T_LS_DESIGN_WORKING_TYPES " +
                             "inner join T_CO_TECNO_WORKING_TYPES tw on T_LS_WORKING_TYPES.F_ID_T_CO_TECNO_WORKING_TYPES = tw.F_ID " +
                             "inner join T_LS_PHASE_WORKINGS pw on T_LS_WORKING_TYPES.F_ID = pw.F_ID_T_LS_WORKING_TYPES " +
                             "inner join T_WK_WORKING_TYPES wt on pw.F_ID = wt.F_ID_T_LS_PHASE_WORKINGS ";

            if (idsWt != null)
                if (idsWt.Length > 0)
                {
                    sqlWhere += sqlAnd + " wt.F_ID in (" + idsWt[0];
                    for (int i = 1; i < idsWt.Length; i++)
                        sqlWhere += "," + idsWt[i];
                    sqlWhere += ")";
                    sqlAnd = " and ";
                }

            //if (idsPw != null)
            //    if (idsPw.Length > 0)
            //    {
            //        sqlWhere += sqlAnd + " pw.F_ID in (" + idsPw[0];
            //        for (int i = 1; i < idsPw.Length; i++)
            //            sqlWhere += "," + idsPw[i];
            //        sqlWhere += ")";
            //        sqlAnd = " and ";
            //    }

            // Verifica se la query � valida
            if (sqlQuery + sqlWhere == "")
            {
                tecnoWorking = null;
                designWorking = null;
                idPhase = null;
                descrPhase = null;
                return false;
            }

            try
            {
                if (!mDbInterface.Requery(sqlQuery + sqlWhere, out dr))
                {
                    tecnoWorking = null;
                    designWorking = null;
                    idPhase = null;
                    descrPhase = null;
                    return false;
                }

                while (dr.Read())
                {
                    tecnoWorking.Add(dr["F_TECNO_CODE"].ToString().Trim());
                    designWorking.Add(dr["F_DESIGN_CODE"].ToString().Trim());
                    idPhase.Add((int)dr["F_ID_T_LS_PHASE_WORKINGS"]);
                    descrPhase.Add(dr["F_WORKING"].ToString().Trim());
                }
                return true;
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("DbAnalizer.GetWorkingTypesFromId: Error", ex);
                tecnoWorking = null;
                designWorking = null;
                idPhase = null;
                descrPhase = null;
                return false;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

        /// <summary>
        /// Restituisce le informazioni necessarie del gruppo di lavoro
        /// </summary>
        /// <param name="wkGrCode"></param>
        /// <param name="id"></param>
        /// <param name="descr"></param>
        public bool GetWorkCenterGroupInfo(string wkGrCode, out int id, out string descr, out string tecnoCenterGroup)
        {
            OleDbDataReader dr = null;
            id = -1;
            descr = "";
            tecnoCenterGroup = "";

            string sqlQuery = "select wc.F_ID as F_ID_WORK_GROUP, wc.F_DESCRIPTION as F_WORK_GROUP, tc.F_CODE as F_TECNO_CODE " +
                              "from T_AN_WORK_CENTER_GROUPS wc, T_CO_TECNO_CENTER_GROUPS tc " +
                              "where tc.F_ID = wc.F_ID_T_CO_TECNO_CENTER_GROUPS and wc.F_CODE = '" + wkGrCode + "'";

            try
            {
                if (!mDbInterface.Requery(sqlQuery, out dr))
                {
                    id = -1;
                    descr = "";
                    tecnoCenterGroup = "";
                    return false;
                }

                while (dr.Read())
                {
                    id = (int)dr["F_ID_WORK_GROUP"];
                    descr = dr["F_WORK_GROUP"].ToString().Trim();
                    tecnoCenterGroup = dr["F_TECNO_CODE"].ToString().Trim();
                }
                return true;
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("DbAnalizer.GetWorkCenterGroupInfo: Error", ex);
                id = -1;
                descr = "";
                tecnoCenterGroup = "";
                return true;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

		/***************************************************************************
		 * GetDefaultCutToSize
		 * Legge tutte i possibili grezzi ordinati per deafult
		 * Parametri:
		 *			phase			: fasi di lavorazione
		 *			idPhase			: id della fase di lavorazione
		 * **************************************************************************/
		public void GetDefaultPhaseWorking(out string[] phase, out int[] idPhase, out string[] designWorkings, int idProdProc, string tecnoType, int executionOrder)
		{
			OleDbDataReader dr = null;
			phase = new string[0];
			idPhase = new int[0];
			designWorkings = new string[0];
			string sqlQuery  = "";
			int i = 0;

			try
			{
				sqlQuery = "select pw.F_DESCRIPTION as F_PHASE_DESCRIPTION, pw.F_ID as F_PHASE_ID, pw.F_DEFAULT, d.F_CODE as F_DESIGN_CODE " +
							"from T_CO_TECNO_WORKING_TYPES t " +
							"inner join T_LS_WORKING_TYPES wt on t.F_ID = wt.F_ID_T_CO_TECNO_WORKING_TYPES " +									
							"left outer join T_LS_DESIGN_WORKING_TYPES d on wt.F_ID_T_LS_DESIGN_WORKING_TYPES = d.F_ID " +
							"inner join T_LS_PHASE_WORKINGS pw on wt.F_ID = pw.F_ID_T_LS_WORKING_TYPES " +
							"inner join T_LS_PRODUCTION_PROCESSES pp on pp.F_ID = pw.F_ID_T_LS_PRODUCTION_PROCESSES " +
							"where t.F_CODE = '" + tecnoType + "' " +
							(idProdProc < 0 ? "" : " and pp.F_ID = " + idProdProc.ToString())+
							(executionOrder < 0 ? "" : " and pw.F_EXECUTION_ORDER = " + executionOrder.ToString()) +
							"order by pw.F_DEFAULT";
							
				if (!mDbInterface.Requery(sqlQuery, out dr))
				{
					phase = null;
					idPhase = null;
					designWorkings = null;
				}

				while (dr.Read())
				{
					string[] sTmp = new string[phase.Length + 1];
					int[] iTmp = new int[idPhase.Length + 1];
					string[] sTmp2 = new string[designWorkings.Length + 1];
					phase.CopyTo(sTmp,0);
					phase = sTmp;
					idPhase.CopyTo(iTmp,0);
					idPhase = iTmp;
                    designWorkings.CopyTo(sTmp2,0);
					designWorkings = sTmp2;
                    phase[i] = dr["F_PHASE_DESCRIPTION"].ToString().TrimEnd();
					idPhase[i] = (int)dr["F_PHASE_ID"];
                    designWorkings[i] = dr["F_DESIGN_CODE"].ToString().TrimEnd();
					i++;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("DbAnalizer.GetDefaultCutToSize: Error", ex);
				phase = null;
				idPhase = null;
				designWorkings = null;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/***************************************************************************
		 * GetDefaultHolesToCut
		 * Legge tutti i possibili fori ordinati per deafult
		 * Parametri:
		 *			phase			: fasi di lavorazione
		 *			idPhase			: id della fase di lavorazione
		 * **************************************************************************/
		public void GetDefaultHolesToCut(out string[] phase, out int[] idPhase, int idProdProc) //, int executionOrder)
		{
			OleDbDataReader dr = null;
			phase = new string[0];
			idPhase = new int[0];
			string sqlQuery  = "";
			int i = 0;

			try
			{
				sqlQuery = "SELECT pw.F_DESCRIPTION AS F_PHASE_DESCRIPTION, pw.F_ID AS F_PHASE_ID, pw.F_DEFAULT " +
					"FROM T_CO_TECNO_WORKING_TYPES t " +
					"JOIN T_LS_WORKING_TYPES wt ON t.F_ID = wt.F_ID_T_CO_TECNO_WORKING_TYPES " +									
					"JOIN T_LS_PHASE_WORKINGS pw ON wt.F_ID = pw.F_ID_T_LS_WORKING_TYPES " +
                    "JOIN T_LS_PRODUCTION_PROCESSES pp ON pp.F_ID = pw.F_ID_T_LS_PRODUCTION_PROCESSES " +
					"WHERE t.F_CODE = 'CUT_HOLES' and pp.F_ID = " + idProdProc.ToString() +
					//(executionOrder < 0 ? "" : " AND pw.F_EXECUTION_ORDER = " + executionOrder.ToString()) +
					"ORDER BY pw.F_DEFAULT";
			
				if (!mDbInterface.Requery(sqlQuery, out dr))
				{
					phase = null;
					idPhase = null;
				}

				while (dr.Read())
				{
					string[] sTmp = new string[phase.Length + 1];
					int[] iTmp = new int[idPhase.Length + 1];
					phase.CopyTo(sTmp,0);
					phase = sTmp;		
					idPhase.CopyTo(iTmp,0);
					idPhase = iTmp;
                    phase[i] = dr["F_PHASE_DESCRIPTION"].ToString().TrimEnd();
					idPhase[i] = (int)dr["F_PHASE_ID"];
					i++;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("DbAnalizer.GetDefaultHolesToCut: Error", ex);
				phase = null;
				idPhase = null;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		/***************************************************************************
		 * GetDefaultContour
		 * Legge tutte le possibili bordature
		 * Parametri:
		 *			designCont		: codice lavorazione in DesignMaster
		 *			phase			: fasi di lavorazione
		 *			idPhase			: id della fase di lavorazione
         *          linear          : indica se si tratta di una lavorazione lineare
		 * **************************************************************************/
        public void GetDefaultContour(string designCont, out string[] phase, out int[] idPhase, int idProdProc, int executionOrder, bool linear)
        {
            OleDbDataReader dr = null;
            phase = new string[0];
            idPhase = new int[0];
            string sqlQuery = "";
            int i = 0;

            try
            {
				sqlQuery = "SELECT pw.F_DESCRIPTION AS F_PHASE_DESCRIPTION, pw.F_ID AS F_PHASE_ID, pw.F_DEFAULT " +
					"FROM T_CO_TECNO_WORKING_TYPES t " +
					"JOIN T_LS_WORKING_TYPES wt ON t.F_ID = wt.F_ID_T_CO_TECNO_WORKING_TYPES " +
					"JOIN T_LS_PHASE_WORKINGS pw ON wt.F_ID = pw.F_ID_T_LS_WORKING_TYPES " +
					"JOIN T_LS_DESIGN_WORKING_TYPES d ON wt.F_ID_T_LS_DESIGN_WORKING_TYPES = d.F_ID " +
					"JOIN T_LS_PRODUCTION_PROCESSES pp ON pp.F_ID = pw.F_ID_T_LS_PRODUCTION_PROCESSES " +
					"WHERE t.F_CODE = '" + (linear ? "CONTOUR_LINEAR" : "CONTOUR") + "'" +
							" AND d.F_CODE = '" + designCont + "'" +
							" AND pp.F_ID = " + idProdProc.ToString() +
							(executionOrder < 0 ? "" : " AND pw.F_EXECUTION_ORDER = " + executionOrder.ToString()) +
                    "ORDER BY pw.F_DEFAULT";

                if (!mDbInterface.Requery(sqlQuery, out dr))
                {
                    phase = null;
                    idPhase = null;
                }

				//// Verifica se la ricerca della lavorazione lineare � fallita.
				//if (linear && (dr != null && !dr.HasRows))
				//{
				//    sqlQuery = "SELECT pw.F_DESCRIPTION AS F_PHASE_DESCRIPTION, pw.F_ID AS F_PHASE_ID, pw.F_DEFAULT " +
				//                       "FROM T_CO_TECNO_WORKING_TYPES t " +
				//                       "JOIN T_LS_WORKING_TYPES wt ON t.F_ID = wt.F_ID_T_CO_TECNO_WORKING_TYPES " +
				//                       "JOIN T_LS_PHASE_WORKINGS pw ON wt.F_ID = pw.F_ID_T_LS_WORKING_TYPES " +
				//                       "JOIN T_LS_DESIGN_WORKING_TYPES d ON wt.F_ID_T_LS_DESIGN_WORKING_TYPES = d.F_ID " +
				//                       "JOIN T_LS_PRODUCTION_PROCESSES pp ON pp.F_ID = pw.F_ID_T_LS_PRODUCTION_PROCESSES " +
				//                       "WHERE t.F_CODE = 'CONTOUR' " +
				//                                " AND d.F_CODE = '" + designCont + "'" +
				//                                " AND pp.F_ID = " + idProdProc.ToString() +
				//                                (executionOrder < 0 ? "" : " AND pw.F_EXECUTION_ORDER = " + executionOrder.ToString()) +
				//                       "ORDER BY pw.F_DEFAULT";

				//    if (!mDbInterface.Requery(sqlQuery, out dr))
				//    {
				//        phase = null;
				//        idPhase = null;
				//    }
				//}

                while (dr.Read())
                {
                    string[] sTmp = new string[phase.Length + 1];
                    int[] iTmp = new int[idPhase.Length + 1];
                    phase.CopyTo(sTmp, 0);
                    phase = sTmp;
                    idPhase.CopyTo(iTmp, 0);
                    idPhase = iTmp;
                    phase[i] = dr["F_PHASE_DESCRIPTION"].ToString().TrimEnd();
                    idPhase[i] = (int)dr["F_PHASE_ID"];
                    i++;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DbAnalizer.GetDefaultContour: Error", ex);
                phase = null;
                idPhase = null;
            }
            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

        /***************************************************************************
		 * GetDefaultWorking
		 * Legge tutte le lavorazioni
		 * Parametri:
		 *			phase			: fasi di lavorazione
		 *			idPhase			: id della fase di lavorazione
		 * **************************************************************************/
		public void GetDefaultWorking(out string[] phase, out int[] idPhase, string type, int idProdProc, int executionOrder)
        {
            OleDbDataReader dr = null;
            phase = new string[0];
            idPhase = new int[0];
            string sqlQuery = "";
            int i = 0;

            try
            {
                sqlQuery = "SELECT pw.F_DESCRIPTION AS F_PHASE_DESCRIPTION, pw.F_ID AS F_PHASE_ID, pw.F_DEFAULT " +
                    "FROM T_CO_TECNO_WORKING_TYPES t " +
                    "JOIN T_LS_WORKING_TYPES wt ON t.F_ID = wt.F_ID_T_CO_TECNO_WORKING_TYPES " +
                    "JOIN T_LS_PHASE_WORKINGS pw ON wt.F_ID = pw.F_ID_T_LS_WORKING_TYPES " +
                    "JOIN T_LS_PRODUCTION_PROCESSES pp ON pp.F_ID = pw.F_ID_T_LS_PRODUCTION_PROCESSES " +
                    "WHERE t.F_CODE = '" + type + "' and pp.F_ID = " + idProdProc.ToString() +
					(executionOrder < 0 ? "" : " AND pw.F_EXECUTION_ORDER = " + executionOrder.ToString()) +
                    "ORDER BY pw.F_DEFAULT";

                if (!mDbInterface.Requery(sqlQuery, out dr))
                {
                    phase = null;
                    idPhase = null;
                }

                while (dr.Read())
                {
                    string[] sTmp = new string[phase.Length + 1];
                    int[] iTmp = new int[idPhase.Length + 1];
                    phase.CopyTo(sTmp, 0);
                    phase = sTmp;
                    idPhase.CopyTo(iTmp, 0);
                    idPhase = iTmp;
                    phase[i] = dr["F_PHASE_DESCRIPTION"].ToString().TrimEnd();
                    idPhase[i] = (int)dr["F_PHASE_ID"];
                    i++;
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("DbAnalizer.GetDefaultWorking: Error", ex);
                phase = null;
                idPhase = null;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }


		///***************************************************************************
        /// GetDefaultMachine
        /// <summary>
		/// 
		/// </summary>
        /// <param name="tecnoWorking">lavorazione tecno interessata</param>
        /// <param name="idPhase">id della fase di lavorazione</param>
        /// <param name="tecnoWorkCenterGroup">codice macchine tecno</param>
        /// <param name="workCenterGroup">macchine ordinate di default</param>
        /// <param name="idWorkCenterGroup">id delle macchine ordinate di default</param>
		/// <param name="workCenterGroupCode">codice della macchina di default</param>
        ///***************************************************************************
        public void GetDefaultMachine(string tecnoWorking, int idPhase, out string[] tecnoWorkCenterGroup, out string[] workCenterGroup, out int[] idWorkCenterGroup, out string[] workCenterGroupCode)
		{
			OleDbDataReader dr = null;
			tecnoWorkCenterGroup = new string[0];
			workCenterGroup = new string[0];
			idWorkCenterGroup = new int[0];
            workCenterGroupCode = new string[0];
			string sqlQuery  = "";
			int i = 0;

			try
			{
                sqlQuery = "SELECT wcg.F_ID AS F_WORK_CENTER_GROUP_ID, wcg.F_DESCRIPTION AS F_WORK_CENTER_GROUP_DESCRIPTION, ix.F_DEFAULT AS F_DEFAULT, " +
                    "tc.F_CODE AS F_TECNO_WORK_CENTER_GROUP, wcg.F_CODE AS F_WORK_CENTER_GROUP_CODE " +
					"FROM T_LS_WORKING_TYPES wt " +
					"left outer JOIN T_LS_DESIGN_WORKING_TYPES d ON wt.F_ID_T_LS_DESIGN_WORKING_TYPES = d.F_ID " +
					"JOIN T_CO_TECNO_WORKING_TYPES tw ON wt.F_ID_T_CO_TECNO_WORKING_TYPES = tw.F_ID " +
					"JOIN T_CO_TECNO_CENTER_GROUPS tc " +
					"JOIN T_AN_WORK_CENTER_GROUPS wcg ON tc.F_ID = wcg.F_ID_T_CO_TECNO_CENTER_GROUPS " +
					"JOIN T_IX_PHASE_WORKINGS ix ON wcg.F_ID = ix.F_ID_T_AN_WORK_CENTER_GROUPS " +
					"JOIN T_LS_PHASE_WORKINGS pw ON ix.F_ID_T_LS_PHASE_WORKINGS = pw.F_ID ON wt.F_ID = pw.F_ID_T_LS_WORKING_TYPES " +
					"WHERE pw.F_ID = " + idPhase + //" AND tw.F_CODE = '" + tecnoWorking + "' " +
					" ORDER BY ix.F_DEFAULT";


				if (!mDbInterface.Requery(sqlQuery, out dr))
				{
					workCenterGroup = null;
					idWorkCenterGroup = null;
					tecnoWorkCenterGroup = null;
                    workCenterGroupCode = null;
				}

				while (dr.Read())
				{
					string[] sTmp = new string[workCenterGroup.Length + 1];
					int[] iTmp = new int[idWorkCenterGroup.Length + 1];
					string[] sTmp2 = new string[tecnoWorkCenterGroup.Length + 1];
                    string[] sTmp3 = new string[workCenterGroupCode.Length + 1];                    
					workCenterGroup.CopyTo(sTmp,0);
					workCenterGroup = sTmp;		
					idWorkCenterGroup.CopyTo(iTmp,0);
					idWorkCenterGroup = iTmp;	
					tecnoWorkCenterGroup.CopyTo(sTmp2,0);
					tecnoWorkCenterGroup = sTmp2;
                    workCenterGroupCode.CopyTo(sTmp3,0);
					workCenterGroupCode = sTmp3;
                    workCenterGroup[i] = dr["F_WORK_CENTER_GROUP_DESCRIPTION"].ToString().TrimEnd();
                    idWorkCenterGroup[i] = (int)dr["F_WORK_CENTER_GROUP_ID"];
                    tecnoWorkCenterGroup[i] = dr["F_TECNO_WORK_CENTER_GROUP"].ToString().TrimEnd();
                    workCenterGroupCode[i] = dr["F_WORK_CENTER_GROUP_CODE"].ToString().TrimEnd();                    
					i++;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("DbAnalizer.GetDefaultMachine: Error", ex);
				workCenterGroup = null;
				idWorkCenterGroup = null;
				tecnoWorkCenterGroup = null;
                workCenterGroupCode = null;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

        /// **********************************************************
        /// GetWorkCenterGroupDescription
        /// <summary>
        /// Ritorna la descrizione del gruppo di centri di lavoro
        /// </summary>
        /// <param name="code">Codice del gruppo di centri di lavoro</param>
        /// <returns>descrizione del gruppo del centri di lavori</returns>
        public string GetWorkCenterGroupDescription(string code)
        {
            OleDbDataReader dr = null;
            string sqlQuery = "";
            string retValue = null;

            try
            {
                sqlQuery = "select wcg.F_DESCRIPTION from T_AN_WORK_CENTER_GROUPS wcg where wcg.F_CODE = '" + code + "'";

                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return null;

                while (dr.Read())
                {
                    retValue = dr["F_DESCRIPTION"].ToString();
                }

                return retValue;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DbAnalizer.GetWorkCenterGroupDescription: Error", ex);
                return null;
            }
            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

		/// **********************************************************
		/// GetWorkCenterDescription
		/// <summary>
		/// Ritorna la descrizione del centro di lavoro
		/// </summary>
		/// <param name="code">Codice del centro di lavoro</param>
		/// <returns>descrizione del centro di lavori</returns>
		public string GetWorkCenterDescription(string code)
		{
			OleDbDataReader dr = null;
			string sqlQuery = "";
			string retValue = null;

			try
			{
				sqlQuery = "select wc.F_DESCRIPTION from T_AN_WORK_CENTERS wc where wc.F_CODE = '" + code + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return null;

				while (dr.Read())
				{
					retValue = dr["F_DESCRIPTION"].ToString();
				}

				return retValue;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("DbAnalizer.GetWorkCenterDescription: Error", ex);
				return null;
			}
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

        ///**************************************************************************
        /// CopyDetailProcess
        /// <summary>
        /// Esegue la copia delle fasi di lavoro di un pezzo in un altro
        /// </summary>
        /// <param name="srcDet">Dettaglio di origine</param>
        /// <param name="dstDet">Dettaglio di destinazione</param>
        ///**************************************************************************
        public bool CopyDetailProcess(Breton.ImportOffer.Detail srcDet, Breton.ImportOffer.Detail dstDet)
        {
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();
            OleDbParameter par;
            ArrayList parameters = new ArrayList();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre una transazione
                    if (transaction) mDbInterface.BeginTransaction();

                    par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
                    par.Direction = System.Data.ParameterDirection.ReturnValue;
                    parameters.Add(par);

                    par = new OleDbParameter("@id_src", OleDbType.Integer);
                    par.Value = srcDet.gId;
                    parameters.Add(par);

                    par = new OleDbParameter("@id_dst", OleDbType.Integer);
                    par.Value = dstDet.gId;
                    parameters.Add(par);

                    // Lancia la stored procedure
                    if (!mDbInterface.Execute("sp_tecno_CopyWorkSettings", ref parameters))
                        throw new ApplicationException("DbAnalizer.CopyDetailProcess sp_tecno_CopyWorkSettings Execute error.");

                    par = (OleDbParameter)parameters[0];
                    if (((int)par.Value) != 0)
                        throw new ApplicationException("DbAnalizer.CopyDetailProcess sp_tecno_CopyWorkSettings Execute error: " + ((int)par.Value));

                    // Chiude la transazione
                    if (transaction)
                        mDbInterface.EndTransaction(true);

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("DbAnalizer.CopyDetailProcess Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("DbAnalizer.CopyDetailProcess Error.", ex);

                    return false;
                }
            }

            return false;
        }

        ///**************************************************************************
        /// ExecQuery
        /// <summary>
        /// Esegue la query passata come parametro e restituisce un ArrayList con i valori
        /// </summary>
        /// <param name="sqlQuery">Query da eseguire</param>
        /// <returns>Arraylist con i valori</returns>
        ///**************************************************************************
        public ArrayList ExecQuery(string sqlQuery)
        {
            OleDbDataReader dr = null;
            ArrayList fields = new ArrayList();

            try
            {
                if (sqlQuery == "")
                    return null;

                if (!mDbInterface.Requery(sqlQuery, out dr))
                {
                    return null;
                }

                while (dr.Read())
                {
                    for (int i = 0; i < dr.FieldCount; i++)
                        fields.Add(dr[i]);
                }

                return fields;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DbAnalizer.ExecQuery: Error", ex);
                return null;
            }
            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

        /// <summary>
        /// Restituisce l'id del processo produttivo
        /// </summary>
        /// <param name="processCode"></param>
        /// <returns></returns>
        public int GetProcessId(string processCode)
        {
            ArrayList id;

            string sqlQuery = "select F_ID from T_LS_PRODUCTION_PROCESSES where F_CODE ='" + processCode + "';";

            // Esegue la query caricando l'id del processo
            id = ExecQuery(sqlQuery);

            if (id != null)
                return (int)id[0];
            else
                return -1;

        }

		/// <summary>
		/// Restituisce i processi produttivi
		/// </summary>
		/// <param name="processCode"></param>
		/// <returns></returns>
		public ProductionProcessCollection GetProductionProcesses()
		{
			OleDbDataReader dr = null;
			ProductionProcessCollection pps = new ProductionProcessCollection();

			try
			{
				string sqlQuery = "select * from T_LS_PRODUCTION_PROCESSES";

				if (!mDbInterface.Requery(sqlQuery, out dr))
				{
					return null;
				}

				while (dr.Read())
				{
					ProductionProcess pp = new ProductionProcess();
					pp.gId = (int)dr["F_ID"];
					pp.gCode = dr["F_CODE"].ToString();
					pp.gDescription = dr["F_DESCRIPTION"].ToString();

					pps.Add(pp);
				}

				return pps;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("DbAnalizer.GetProductionProcesses: Error", ex);
				return null;
			}
			finally
			{
				mDbInterface.EndRequery(dr);
			}

		}

		/// <summary>
		/// Restituisce un array list caricato con i centri di lavoro del gruppo di riferimento
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>
		public ArrayList GetWorkCenters(string workCenterGroupCode)
		{
			OleDbDataReader dr = null;
			ArrayList workCenters = new ArrayList();

			try
			{
				string sqlQuery = "select T_AN_WORK_CENTERS.F_CODE, T_AN_WORK_CENTERS.F_DESCRIPTION " +
									"from T_AN_WORK_CENTERS " +
									"inner join T_IX_WORK_CENTER_GROUPS on T_AN_WORK_CENTERS.F_ID = T_IX_WORK_CENTER_GROUPS.F_ID_T_AN_WORK_CENTERS " +
									"inner join T_AN_WORK_CENTER_GROUPS on T_IX_WORK_CENTER_GROUPS.F_ID_T_AN_WORK_CENTER_GROUPS = T_AN_WORK_CENTER_GROUPS.F_ID " +
									"where T_AN_WORK_CENTER_GROUPS.F_CODE = '" + workCenterGroupCode + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
				{
					return null;
				}

				while (dr.Read())
				{
					WorkCenter wk = new WorkCenter();
					wk.code = dr["F_CODE"].ToString();
					wk.description = dr["F_DESCRIPTION"].ToString();

					workCenters.Add(wk);
				}

				return workCenters;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("DbAnalizer.GetWorkCenters: Error", ex);
				return null;
			}
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}
		#endregion

	}
}
