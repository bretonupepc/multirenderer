using System;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa un pezzo confermato delle lastre, sul tipo FK
	/// </summary>
	///*************************************************************************
	public class PieceOk: Piece
	{
		#region Structs, Enums & Variables

		#endregion


		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		///**********************************************************************
		public PieceOk(int id, int idGroup, int idShape, PieceOk.State state, double posX, double posY, bool rotated, PieceType type, 
			double traslX, double traslY, double rotAngle, string code, double dimX, double dimY, bool isPolygon):
			base(0, id, idGroup, idShape, state, posX, posY, rotated, type, traslX, traslY, rotAngle, code, dimX, dimY, isPolygon)
		{
		}

		public PieceOk():this(0, 0, 0, PieceOk.State.Undef, 0d, 0d, false, PieceOk.PieceType.Undef, 0d, 0d, 0d, "", 0d, 0d, false)
		{
		}

		#endregion


		#region Properties

		#endregion


		#region IComparable interface

		#endregion


		#region Public Methods

		#endregion


		#region Private Methods

		#endregion

	}
}
