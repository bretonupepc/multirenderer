using System;
using System.Threading;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using System.Collections;
using Breton.DbAccess;
using Breton.TraceLoggers;
using Breton.Optimizers;
using Breton.Slabs;
using Breton.Parameters;
using Breton.Polygons;
using Breton.Users;

namespace Breton.OptiMasterInterface
{
	/// *************************************************************************
	/// <summary>
	/// Classe che implementa la copia di uno schema di ottimizzazione con i 
	/// formati disponibili, nelle lastre disponibili.
	/// </summary>
	/// *************************************************************************
	public class CopyOptimization : IDisposable
	{
		#region Structs, Enums & Variables

		// Codici di ritorno del preottimizzatore
		public enum ReturnedCode
		{
			Error = -1,
			Ok = 0,
			NoPieces,
			NoCompatibleShapes,
			Terminated,
			Suspended
		}

		private DbInterface mDbInterface;		// Interfaccia al database
		private int mIdOp;						// id ordine di produzione
		private int mIdOpSlabSrc;				// id lastra da copiare
		private OpSlab mOpSlabSrc;
		private bool mInitialize;
		private bool mAbort = false;
		private ReturnedCode mErr;
		private bool mEnd = false;
		private bool mActive = false;
		private int mNumPieces;					// Numero pezzi da ottimizzare per lastra
		private int mNumPiece;					// Pezzo attualmente in lettura

		private ProdOrderCollection mProdOrders;
		private ShapeCollection mShapes;
		private SlabGroupCollection mSlabGroups;
		private OpSlabCollection mOpSlabs;
		private OpSlab mOpSlab;
		private SlabCollection mSlabs;
		private Slab mSlab;
		private GroupCollection mGroups;
		private ArrayList[] mCompatibleShapes;
		private ArrayList mResult;
		private int mIndexResult;
		private string mTmpDirPath = System.IO.Path.GetTempPath() + "CopyOptimization";
		private DirectoryInfo mTmpDir;

		private Thread thdExecution;

		protected bool mDisposed = false;

		// Risultato ottimizzazione
		private struct Result
		{
			public int Id;						// Id formato
			public Optimizer.ShapeType Type;	// Tipo formato
			public int GroupId;					// Id gruppo
			public double OffsetX;				// Posizione X rispetto allo 0 lastra
			public double OffsetY;				// Posizione Y rispetto allo 0 lastra
			public double RotAngle;				// Angolo di rotazione (in radianti)
			public CompatibleShape CompShape;	// Shape di partenza
			public Piece OrigPiece;				// Pezzo originale

			/// **************************************************************************
			/// <summary>
			/// Costruttore del risultato
			/// </summary>
			/// <param name="id"></param>
			/// <param name="type"></param>
			/// <param name="groupId"></param>
			/// <param name="offsetX"></param>
			/// <param name="offsetY"></param>
			/// <param name="rotAngle"></param>
			/// <param name="compShape"></param>
			/// **************************************************************************
			public Result(int id, Optimizer.ShapeType type, int groupId, double offsetX, double offsetY, double rotAngle, 
							CompatibleShape compShape, Piece origPiece)
			{
				Id = id;
				Type = type;
				GroupId = groupId;
				OffsetX = offsetX;
				OffsetY = offsetY;
				RotAngle = rotAngle;
				CompShape = compShape;
				OrigPiece = origPiece;
			}
		}

		// Shape compatibile
		private struct CompatibleShape
		{
			public int gIdOpShape;
			public int gIdShape;
			public int gQty;
			public bool gEqual;

			public CompatibleShape(int idOpShape, int idShape, int qty, bool equal)
			{
				gIdOpShape = idOpShape;
				gIdShape = idShape;
				gQty = qty;
				gEqual = equal;
			}
		}
		
		#endregion


		#region Constructors & Disposers

		/// *********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		/// <param name="db">Interfaccia al database</param>
		/// <param name="idOpSlab">id ordine di produzione</param>
		/// <param name="idOpSlab">id lastra da copiare</param>
		/// *********************************************************************
		public CopyOptimization(DbInterface db, int idOp, int idOpSlab)
		{
			mDbInterface = new DbInterface();
			mDbInterface.Open(db.ConnectionString());
			mIdOp = idOp;
			mIdOpSlabSrc = idOpSlab;

			mProdOrders = new ProdOrderCollection(mDbInterface);
			mShapes = new ShapeCollection(mDbInterface);
			
			mSlabGroups = null;
			mOpSlabs = null;
			mSlabs = new SlabCollection(mDbInterface);
			mResult = new ArrayList();

			mTmpDir = new DirectoryInfo(mTmpDirPath);
			mTmpDir.Create();
			mTmpDir.Attributes = FileAttributes.Hidden;

        }


		#region IDisposable Members

		/// *********************************************************************
		/// <summary>
		/// Distruttore chiamato dall'esterno
		/// </summary>
		/// *********************************************************************
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// *********************************************************************
		/// <summary>
		/// Distruttore interno effettivo
		/// </summary>
		/// <param name="disposing">true = chiamata esplicita / false = Garbage Collector</param>
		/// *********************************************************************
		protected virtual void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.mDisposed)
			{
				StopCopy();

				mDbInterface.Close();
				mDbInterface.Dispose();
				mDbInterface = null;

				// Cancella la directory contenente i file temporanei
				if (mTmpDir.Exists)
					mTmpDir.Delete(true);

				mDisposed = true;
			}
		}

		public bool Disposed
		{
			get { return mDisposed; }
		}

		#endregion

        #endregion


		#region Properties

		/// *********************************************************************
		/// <summary>
		/// Ritorna se la fase di copia � in corso
		/// </summary>
		/// *********************************************************************
		public bool gActive
		{
			get { return mActive; }
		}

		/// *********************************************************************
		/// <summary>
		/// Ritorna se la fase di copia � terminata
		/// </summary>
		/// *********************************************************************
		public bool gEnd
		{
			get { return mEnd; }
		}

		/// *********************************************************************
		/// <summary>
		/// Ritorna se la fase di copia � in errore
		/// </summary>
		/// *********************************************************************
		public ReturnedCode gErr
		{
			get { return mErr; }
		}

		#endregion


		#region Public Methods

		/// ********************************************************************
		/// <summary>
		/// Avvia il processo di copia
		/// </summary>
		/// <returns>
		///		true	processo avviato
		///		false	impossibile avviare il processo
		///	</returns>
		/// ********************************************************************
		public bool StartCopy()
		{
			if (mActive)
				return false;

			mInitialize = false;
			mAbort = false;
			mEnd = false;
			mErr = ReturnedCode.Ok;
			mActive = true;

			thdExecution = new Thread(new ThreadStart(Execution));
			thdExecution.Start();

			return true;
		}

		/// ********************************************************************
		/// <summary>
		/// Arresta il processo di copia
		/// </summary>
		/// <returns>
		///		true	processo arrestato
		///		false	impossibile arrestare il processo
		/// </returns>
		/// ********************************************************************
		public bool StopCopy()
		{
			if (mActive)
			{
				// Richiede la fine del thread
				mAbort = true;

				// Attende la fine del thread
				thdExecution.Join();
				mActive = false;
			}

			return true;
		}

		#endregion


		#region Private Methods

		/// ********************************************************************
		/// <summary>
		/// Routine principale del thread di blocco/sblocco
		/// </summary>
		/// ********************************************************************
		private void Execution()
		{
			ReturnedCode err = ReturnedCode.Ok;

			while (!mAbort && err == ReturnedCode.Ok)
			{
				err = CopyingOptimization();

				// Tempo di attesa fra una operazione e la successiva
				Thread.Sleep(1000);
			}

			switch (err)
			{
				case ReturnedCode.Error:
				case ReturnedCode.NoPieces:
				case ReturnedCode.NoCompatibleShapes:
					mErr = err;
					break;

				case ReturnedCode.Ok:
					mErr = ReturnedCode.Ok;
					break;
				
				case ReturnedCode.Terminated:
				case ReturnedCode.Suspended:
					mErr = ReturnedCode.Ok;
					mEnd = true;
					break;
			}

			mActive = false;
		}


		/// ********************************************************************
		/// <summary>
		/// Esegue la copia dell'ottimizzazione
		/// </summary>
		/// <returns>
		///		codice di ritorno ReturnedCode
		/// </returns>
		/// ********************************************************************
		private ReturnedCode CopyingOptimization()
		{
			// Inizializzazione
			if (!mInitialize)
			{
				// Legge i pezzi della lastra da copiare
				if ((mNumPieces = GetMasterSlabPieces()) < 0)
					return ReturnedCode.Error;

				// Nessun pezzo da copiare
				if (mNumPieces == 0)
					return ReturnedCode.NoPieces;

				// Crea il vettore per gli shapes compatibili
				mCompatibleShapes = new ArrayList[mNumPieces];
				mNumPiece = 0;

				mInitialize = true;

				return ReturnedCode.Ok;
			}

			// Legge i formati compatibili per ogni pezzo
			if (mNumPiece == 0)
			{
				Group group;
				Piece piece;

				// Scorre tutte le filagne
				mGroups.MoveFirst();
				while (mGroups.GetObject(out group) && mNumPiece < mNumPieces)
				{
					// Scorre tutti i pezzi della filagna
					group.gPieces.MoveFirst();
					while (group.gPieces.GetObject(out piece))
					{
						// Legge i formati compatibili
						int numShapes = GetCompatibleShapes(piece, ref mCompatibleShapes[mNumPiece++]);

						// Errore
						if (numShapes < 0)
							return ReturnedCode.Error;

						// Mancano formati compatibili per questo pezzo
						if (numShapes == 0)
							return ReturnedCode.NoCompatibleShapes;

						group.gPieces.MoveNext();
					}

					// Passa alla filagna successiva
					mGroups.MoveNext();
				}

				return ReturnedCode.Ok;
			}

			// Ritorna la prossima soluzione
			int numPieces = GetNextResult();

			// Teminati i pezzi da copiare
			if (numPieces == 0)
				return ReturnedCode.Terminated;

			// Legge la prossima lastra
			bool end;
			if (!RecoverAvailableSlabs(out end))
				return ReturnedCode.Error;

			// Finite le lastre
			if (end)
				return ReturnedCode.Suspended;

			// Memorizza il risultato nel db
			double optSurface;
			if (!GetOptResultIntoDb(numPieces, out optSurface))
				return ReturnedCode.Error;

			// Nuova soluzione, aggiorna le informazioni
			if (!UpdateSolutionsInfo(optSurface))
				return ReturnedCode.Error;

			return ReturnedCode.Ok;
		}

		/// *********************************************************************
		/// <summary>
		/// Legge i pezzi della lastra da copiare
		/// </summary>
		/// <returns>
		///		numero di pezzi della lastra
		///		-1	errore nell'operazione
		/// </returns>
		/// *********************************************************************
		private int GetMasterSlabPieces()
		{
			Group group;
			Piece piece;
			int count = 0, g = 0;

			try
			{
				// Legge la lastra da copiare
				OpSlabCollection opSlabs = new OpSlabCollection(mDbInterface);
				if (!opSlabs.GetSingleElementFromDb(mIdOpSlabSrc))
					throw new ApplicationException("CopyOptimization.GetMasterSlabPieces get source slab error.");
				if (!opSlabs.GetObject(out mOpSlabSrc))
					throw new ApplicationException("CopyOptimization.GetMasterSlabPieces get source slab error.");

				// Legge i gruppi legati alla lastra
				mGroups = new GroupOpCollection(mDbInterface);
				if (!mGroups.GetSlabGroupsFromDb(mIdOpSlabSrc))
					throw new ApplicationException("CopyOptimization.GetMasterSlabPieces get groups error.");

				// Legge tutti i pezzi delle filagne
				mGroups.MoveFirst();
				while (mGroups.GetObject(out group))
				{
					group.gTableId = g++;

					// Legge i pezzi della filagna
					if (!group.GetPiecesFromDb(mDbInterface))
						throw new ApplicationException("CopyOptimization.GetMasterSlabPieces get pieces groups error.");

					// Legge i file xml dei pezzi
					while (group.gPieces.GetObject(out piece))
					{
						// Legge il poligono
						if (!group.gPieces.GetXmlPolygonFromDb(piece, mTmpDirPath + "\\PIECEID_" + piece.gId.ToString() + ".xml"))
							TraceLog.WriteLine("CopyOptimization.GetMasterSlabPieces: Error reading xml polygon from DB");

						group.gPieces.MoveNext();
					}

					// Conta il numero di pezzi
					count += group.gPieces.Count;

					// Passa alla filagna successiva
					mGroups.MoveNext();
				}

				return count;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("CopyOptimization.GetMasterSlabPieces: Error", ex);
				mCompatibleShapes = new ArrayList[0];
				return -1;
			}
		}

		/// *********************************************************************
		/// <summary>
		/// Legge i formati compatibili con il pezzo dato
		/// </summary>
		/// <param name="piece">pezzo di cui trovare i formati compatibili</param>
		/// <param name="compShapes">array di shape compatibili restituiti</param>
		/// <returns>
		///		numero shapes compatibili trovati
		///		-1	errore
		///	</returns>
		/// *********************************************************************
		private int GetCompatibleShapes(Piece piece, ref ArrayList compShapes)
		{
			OleDbDataReader dr = null;
			int idShape;
			float length, width;
			string origShape, origProject, codeOrder;

			// Inizializza l'array da restituire
			compShapes = new ArrayList();

			string sqlQuery = "select top 1 T_SHAPES.F_ID_SHAPE" +
							  "			, T_SHAPES.F_LENGTH" +
							  "			, T_SHAPES.F_WIDTH" +
							  "			, T_WK_CUSTOMER_ORDER_DETAILS.F_CODE_ORIGINAL_SHAPE" +
							  "			, T_WK_CUSTOMER_PROJECTS.F_CODE_ORIGINAL_PROJECT" +
							  "			, T_WK_CUSTOMER_ORDERS.F_CODE" +
							  " from T_OP_SHAPES" +
							  "			inner join T_SHAPES" +
							  "				on T_OP_SHAPES.F_ID_SHAPE = T_SHAPES.F_ID_SHAPE" +
							  "			inner join T_WK_WORK_PHASES_H" +
							  "				on T_SHAPES.F_ID_T_WK_WORK_PHASES_H = T_WK_WORK_PHASES_H.F_ID" +
							  "			inner join T_WK_CUSTOMER_ORDER_DETAILS" +
							  "				on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID" +
							  "			inner join T_WK_CUSTOMER_PROJECTS" +
							  "				on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID" +
							  "			inner join T_WK_CUSTOMER_ORDERS" +
							  "				on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = T_WK_CUSTOMER_ORDERS.F_ID" +
							  "	where T_OP_SHAPES.F_ID_OP_SHAPE = " + piece.gIdShape.ToString();

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr, true))
					throw new ApplicationException("CopyOptimization.GetCompatibleShapes get piece infos error.");

				// Legge le informazioni sul formato
				if (!dr.Read())
					throw new ApplicationException("CopyOptimization.GetCompatibleShapes get piece infos error.");

				idShape = dr.GetInt32(0);
				length = dr.GetFloat(1);
				width = dr.GetFloat(2);
				origShape = dr.GetString(3);
				origProject = dr.GetString(4);
				codeOrder = dr.GetString(5);
				
				mDbInterface.EndRequery(dr);

				// Estrae i formati direttamente compatibili (cio� che sono pezzi creati come copia dello stesso progetto)
				sqlQuery = "select	T_OP_SHAPES.F_ID_OP_SHAPE" +
						   "		, T_OP_SHAPES.F_ID_SHAPE" +
						   "		, T_OP_SHAPES.F_AVAILABLE_QUANTITY" +
						   " from T_OP_SHAPES" +
						   "			inner join T_SHAPES" +
						   "				on T_OP_SHAPES.F_ID_SHAPE = T_SHAPES.F_ID_SHAPE" +
						   "			inner join T_WK_WORK_PHASES_H" +
						   "				on T_SHAPES.F_ID_T_WK_WORK_PHASES_H = T_WK_WORK_PHASES_H.F_ID" +
						   "			inner join T_WK_CUSTOMER_ORDER_DETAILS" +
						   "				on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID" +
						   "			inner join T_WK_CUSTOMER_PROJECTS" +
						   "				on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID" +
						   "			inner join T_WK_CUSTOMER_ORDERS" +
						   "				on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = T_WK_CUSTOMER_ORDERS.F_ID" +
						   " where T_OP_SHAPES.F_ID_OP_SHAPE <> " + piece.gIdShape.ToString() +
						   " and T_OP_SHAPES.F_ID_OP = " + piece.gIdOp +
						   " and T_OP_SHAPES.F_AVAILABLE_QUANTITY > 0" +
						   " and T_WK_CUSTOMER_ORDER_DETAILS.F_CODE_ORIGINAL_SHAPE = '" + origShape + "'" +
						   " and T_WK_CUSTOMER_PROJECTS.F_CODE_ORIGINAL_PROJECT = '" + origProject + "'" +
						   " and T_WK_CUSTOMER_ORDERS.F_CODE = '" + codeOrder + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					throw new ApplicationException("CopyOptimization.GetCompatibleShapes get direct compatible shapes error.");

				// Aggiunge l'id dei formati letti all'array risultante
				while (dr.Read())
					compShapes.Add(new CompatibleShape(dr.GetInt32(0), dr.GetInt32(1), dr.GetInt32(2), true));

				mDbInterface.EndRequery(dr);

				// Estrae i formati compatibili geometricamente (cio� che non sono copie, ma hanno la stessa forma esterna)
				sqlQuery = "select	T_OP_SHAPES.F_ID_OP_SHAPE" +
						   "		, T_OP_SHAPES.F_ID_SHAPE" +
						   "		, T_OP_SHAPES.F_AVAILABLE_QUANTITY" +
						   " from T_OP_SHAPES" +
						   "			inner join T_SHAPES" +
						   "				on T_OP_SHAPES.F_ID_SHAPE = T_SHAPES.F_ID_SHAPE" +
						   "			inner join T_WK_WORK_PHASES_H" +
						   "				on T_SHAPES.F_ID_T_WK_WORK_PHASES_H = T_WK_WORK_PHASES_H.F_ID" +
						   "			inner join T_WK_CUSTOMER_ORDER_DETAILS" +
						   "				on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID" +
						   "			inner join T_WK_CUSTOMER_PROJECTS" +
						   "				on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID" +
						   "			inner join T_WK_CUSTOMER_ORDERS" +
						   "				on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = T_WK_CUSTOMER_ORDERS.F_ID" +
						   " where T_OP_SHAPES.F_ID_OP_SHAPE <> " + piece.gIdShape.ToString() +
						   " and T_OP_SHAPES.F_ID_OP = " + piece.gIdOp +
						   " and T_OP_SHAPES.F_AVAILABLE_QUANTITY > 0" +
						   " and (" +
						   "	T_WK_CUSTOMER_ORDER_DETAILS.F_CODE_ORIGINAL_SHAPE <> '" + origShape + "'" +
						   "	or T_WK_CUSTOMER_PROJECTS.F_CODE_ORIGINAL_PROJECT <> '" + origProject + "'" +
						   "	or T_WK_CUSTOMER_ORDERS.F_CODE <> '" + codeOrder + "'" +
						   " )" +
						   " and (" +
						   "	T_SHAPES.F_LENGTH = " + length.ToString() +
						   "	and T_SHAPES.F_WIDTH = " + width.ToString() +
						   "	or" +
						   "	T_SHAPES.F_LENGTH = " + width.ToString() +
						   "	and T_SHAPES.F_WIDTH = " + length.ToString() +
						   ")";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					throw new ApplicationException("CopyOptimization.GetCompatibleShapes get direct compatible shapes error.");

				ArrayList shapeCandidates = new ArrayList();
				// Aggiunge l'id dei formati letti all'array degli id da verificare
				while (dr.Read())
					shapeCandidates.Add(new CompatibleShape(dr.GetInt32(0), dr.GetInt32(1), dr.GetInt32(2), false));

				mDbInterface.EndRequery(dr);

				// Shape originario (da leggere dal database)
				Shape shapeOrig = new Shape();

				// Verifica se i formati letti sono geometricamente compatibili
				for (int i = 0; i < shapeCandidates.Count; i++)
				{
					CompatibleShape compShape = (CompatibleShape)shapeCandidates[i];
					// Se lo shape letto risulta essere geometricamente compatibile
					if (IsCompatibleShape(shapeOrig, compShape.gIdOpShape))
						// Lo aggiunge fra gli shape compatibili
						compShapes.Add(compShapes);
				}

				// Ritorna il numero di shapes compatibili trovati
				return compShapes.Count;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("CopyOptimization.GetCompatibleShapes: Error", ex);
				mDbInterface.EndRequery(dr);
				return -1;
			}
		}

		/// *********************************************************************
		/// <summary>
		/// Verifica se due shape sono compatibili geometricamente
		/// </summary>
		/// <param name="shapeOrig">shape originario</param>
		/// <param name="idShapeToCheck">id shape da verificare</param>
		/// <returns>
		///		true	shape compatibile con l'originario
		///		false	shape non compatibile
		/// </returns>
		/// *********************************************************************
		private bool IsCompatibleShape(Shape shapeOrig, int idShapeToCheck)
		{
			/*
			 * Funzione da sviluppare.
			 * Dovrebbe verificare se lo shape da controllare � geometricamente compatibile
			 * con lo shape originario, in particolare per quanto riguarda il percorso esterno.
			 */

			return false;
		}

		/// *********************************************************************
		/// <summary>
		/// Ritorna la prossima copia trovata.
		/// </summary>
		/// <returns>Numero di pezzi della copia trovata</returns>
		/// *********************************************************************
		private int GetNextResult()
		{
			Group group;
			Piece piece;
			int nPiece = 0;
			bool end = false;

			mResult.Clear();
			mIndexResult = 0;

			// Legge tutti i pezzi delle filagne
			mGroups.MoveFirst();
			while (mGroups.GetObject(out group))
			{
				// Legge tutti i pezzi della filagna
				group.gPieces.MoveFirst();
				while (group.gPieces.GetObject(out piece))
				{
					// Legge gli shape compatibili con il pezzo
					ArrayList shapes = mCompatibleShapes[nPiece];
					// Verifica se ci sono formati disponibili
					if (shapes.Count == 0)
					{
						end = true;
					}
					else
					{

						// Aggiunge il formato alla lista dei pezzi
						CompatibleShape compShape = (CompatibleShape)shapes[0];
						mResult.Add(new Result(compShape.gIdOpShape, Optimizer.ShapeType.Polygon, group.gTableId,
												piece.gTraslX, piece.gTraslY, piece.gRotAngle, compShape, piece));

						// Elimina il formato dai formati disponibili
						RemoveIdFromAvailableShapes(compShape);
					}

					// Passa al pezzo successivo
					group.gPieces.MoveNext();
					nPiece++;
				}

				// Passa alla filagna successiva
				mGroups.MoveNext();
			}

			// Se non ha trovato qualche formato
			if (end)
			{
				// Verifica se comunque � riuscito ad assegnare tutti i formati
				for (int i = 0; i < mCompatibleShapes.Length; i++)
				{
					// Se esitono dei formati ancora da assegnare
					if (mCompatibleShapes[i].Count > 0)
					{
						// Annulla tutto
						mResult.Clear();
						break;
					}
				}
			}

			// Ritorna il numero di pezzi del risultato trovato
			return mResult.Count;
		}

		/// *********************************************************************
		/// <summary>
		/// Ritorna il prossimo pezzo della copia trovata 
		/// </summary>
		/// <param name="id">Id formato</param>
		/// <param name="type">Tipo formato</param>
		/// <param name="groupId">Id gruppo</param>
		/// <param name="offsetX">Posizione X rispetto allo 0 lastra</param>
		/// <param name="offsetY">Posizione Y rispetto allo 0 lastra</param>
		/// <param name="rotAngle">Angolo di rotazione (in radianti)</param>
		/// <param name="compShape">shape compatibile</param>
		/// <param name="origPiece">pezzo originario</param>
		/// <returns>
		///		true	pezzo ritornato
		///		false	pezzo non ritornato, fine pezzi
		///	</returns>
		/// *********************************************************************
		private bool GetNextResult(out int id, out Optimizer.ShapeType type, out int groupId,
			out double offsetX, out double offsetY, out double rotAngle, 
			out CompatibleShape compShape, out Piece origPiece)
		{
			if (mIndexResult >= mResult.Count)
			{
				id = 0;
				type = Optimizer.ShapeType.Undef;
				groupId = 0;
				offsetX = 0d;
				offsetY = 0d;
				rotAngle = 0d;
				compShape = new CompatibleShape();
				origPiece = null;

				return false;
			}

			Result res = (Result)mResult[mIndexResult++];

			id = res.Id;
			type = res.Type;
			groupId = res.GroupId;
			offsetX = res.OffsetX;
			offsetY = res.OffsetY;
			rotAngle = res.RotAngle;
			compShape = res.CompShape;
			origPiece = res.OrigPiece;

			return true;
		}

		/// *********************************************************************
		/// <summary>
		/// Rimuove l'id specificato dagli shape disponibili
		/// </summary>
		/// <param name="compShape">shape da eliminare</param>
		/// *********************************************************************
		private void RemoveIdFromAvailableShapes(CompatibleShape compShape)
		{
			// Rimuove lo shape da tutte le liste
			for (int i = 0; i < mCompatibleShapes.Length; i++)
				// Cerca lo shape nella lista
				for (int j = 0; j < mCompatibleShapes[i].Count; j++)
				{
					CompatibleShape cs = (CompatibleShape)mCompatibleShapes[i][j];

					// Shape trovato
					if (cs.gIdOpShape == compShape.gIdOpShape)
					{
						// lo elimina ed esce dalla lista
						mCompatibleShapes[i].RemoveAt(j);
						break;
					}
				}
		}

		/// *********************************************************************
		/// <summary>
		/// Recupera le lastre disponibili.
		/// Nota:
		/// La funzione valorizza la collection mSlabs con la lista delle lastre
		/// compatibili con l'ordine di produzione e il gruppo di lastre attuale.
		/// </summary>
		/// <param name="endSlabs">true = lastre terminate</param>
		/// <returns>
		///     true    operazione eseguita con successo
		///     false   errore nell'operazione
		/// </returns>
		/// *********************************************************************
		private bool RecoverAvailableSlabs(out bool endSlabs)
		{
			mOpSlab = null;
			mSlabs.Clear();
			mSlab = null;
			endSlabs = false;

			// Legge l'ordine di produzione in esecuzione
			ProdOrder po;
			if (!GetActualProdOrder(out po))
				return false;

			// Legge i gruppi di lastre
			if (mSlabGroups == null)
			{
				mSlabGroups = new SlabGroupCollection(mDbInterface);
				if (!mSlabGroups.GetDataFromDb(po.gId))
				{
					TraceLog.WriteLine("CopyOptimization.RecoverAvailableShapes: Error reading slab groups.");
					return false;
				}

				mOpSlabs = new OpSlabCollection(mDbInterface);
			}

			// Cerca la prossima lastra
			while (!endSlabs)
			{
				// Legge le lastre del gruppo
				while (mOpSlabs.IsEOF() && !mSlabGroups.IsEOF())
				{
					// Legge il gruppo di lastre in esecuzione
					SlabGroup slabGroup;
					if (!mSlabGroups.GetObject(out slabGroup))
						return false;

					// Legge le lastre
					if (!mOpSlabs.GetDataFromDb(po.gId, slabGroup.gGroup, OpSlab.State.Available))
					{
						TraceLog.WriteLine("CopyOptimization.RecoverAvailableSlabs: Error reading slabs.");
						return false;
					}

					// Passa al gruppo successivo
					mSlabGroups.MoveNext();
				}

				// Verifica se non ha pi� lastre
				if (mOpSlabs.IsEOF())
					endSlabs = true;
				
				// Recupera le informazioni della lastra da ottimizzare
				else if (GetOptSlab())
				{
					// Se la lastra � compatibile, trovata
					if (IsCompatibleSlab(mSlab))
					{
						// Passa alla lastra successiva
						mOpSlabs.MoveNext();
						break;
					}
				}

				// Passa alla lastra successiva
				mOpSlabs.MoveNext();
			}

			// Se le lastre non sono terminate, allora ha trovato una lastra
			return true;
		}

		/// ***************************************************************************
		/// <summary>
		/// Legge le informazioni della lastra da ottimizzare
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		/// ***************************************************************************
		private bool GetOptSlab()
		{
			// Recupera la lastra da ottimizzare
			if (mOpSlabs.GetObject(out mOpSlab))
			{
				if (!mSlabs.GetSingleElementFromDb(mOpSlab.gIdArticle))
					return false;

				if (!mSlabs.GetObject(out mSlab))
					return false;

				if (!mSlabs.GetXmlParameterFromDb(mSlab, mTmpDirPath + "\\SLABID_" + mSlab.gId.ToString() + ".xml"))
					return false;

				if (!mSlab.ReadFileXml(mSlab.gXmlParameterLink, "", "", ""))
					return false;

				if (mSlab.gZone == null)
					return false;

				return true;
			}

			return false;
		}

		/// ***************************************************************************
		/// <summary>
		/// Verifica se la lastra � compatibile con la copia dei pezzi.
		/// </summary>
		/// <param name="slab">lastra da controllare</param>
		/// <returns>
		///		true	lastra compatibile con la copia dei pezzi
		///		false	lastra non compatibile con i pezzi da copiare
		/// </returns>
		/// ***************************************************************************
		private bool IsCompatibleSlab(Slab slab)
		{
			/*
			 * Funzione da sviluppare.
			 * Dovrebbe verificare se la lastra letta pu� contenere i pezzi da copiare.
			 */

			return true;
		}

		/// ***************************************************************************
		/// <summary>
		/// Restituisce l'attuale ordine di produzione
		/// </summary>
		/// <param name="po">ordine di produzione restituito</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		/// ***************************************************************************
		private bool GetActualProdOrder(out ProdOrder po)
		{
			po = null;

			// Legge l'ordine di produzione dal db
			if (mProdOrders.Count == 0)
				if (!mProdOrders.GetSingleElementFromDb(mIdOp))
					return false;

			// Legge l'ordine di produzione in esecuzione
			mProdOrders.MoveFirst();
			if (!mProdOrders.GetObject(out po))
				return false;

			return true;
		}

		///***************************************************************************
		/// <summary>
		/// Legge i risultati dell'ottimizzazione e li memorizza dentro il DB
		/// </summary>
		/// <param name="numPieces">numero pezzi della soluzione trovata</param>
		/// <param name="optSurface">superficie ottimizzata restituita</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///***************************************************************************
		private bool GetOptResultIntoDb(int numPieces, out double optSurface)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			int idOpShape, groupId;
			double offsetX, offsetY, rotAngle;
			CompatibleShape compShape;
			Optimizer.ShapeType shapeType;
			Group group;
			Piece piece, origPiece;
			Shape shape;

			optSurface = 0d;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Legge l'ordine di produzione in esecuzione
					ProdOrder po;
					if (!GetActualProdOrder(out po))
						throw new ApplicationException("CopyOptimization.GetOptResultIntoDb get production order error.");

					// Legge i gruppi precedenti legati alla lastra
					GroupCollection groups = new GroupOpCollection(mDbInterface);
					if (!groups.GetSlabGroupsFromDb(mOpSlab.gId))
						throw new ApplicationException("CopyOptimization.GetOptResultIntoDb get old groups error.");

					// Cancella tutti i gruppi letti
					while (groups.Count > 0)
						groups.DeleteObject(0);
					if (!groups.UpdateDataToDb())
						throw new ApplicationException("CopyOptimization.GetOptResultIntoDb delete old groups error.");

					// Recupera la soluzione
					for (int i = 0; i < numPieces; i++)
					{
						// Recupera il pezzo dalla soluzione
						GetNextResult(out idOpShape, out shapeType, out groupId, out offsetX, out offsetY, out rotAngle, out compShape, out origPiece);

						// Recupera il formato agganciato al pezzo
						if (!mShapes.GetSingleElementFromDb(compShape.gIdShape))
							throw new ApplicationException("CopyOptimization.GetOptResultIntoDb piece shape not found");
						if (!mShapes.GetObject(out shape))
							throw new ApplicationException("CopyOptimization.GetOptResultIntoDb piece shape not found");
						// Legge il poligono
						if (!mShapes.GetXmlPolygonFromDb(shape, mTmpDirPath + "\\SHAPEID_" + shape.gId.ToString() + ".xml"))
							TraceLog.WriteLine("CopyOptimization.GetOptResultIntoDb: Error reading xml polygon from DB");

						/***
						 *** ATTENZIONE: GESTISCE SOLO I PEZZI POLIGONALI 
						 *** OCCORRE INSERIRE LA GESTIONE DELLE FILAGNE
						 ***/
						// Crea il pezzo
						piece = new PieceOp(po.gId, 0, 0, idOpShape, Piece.State.Ready, 0d, 0d, false, Piece.PieceType.Good,
							offsetX, offsetY, rotAngle, shape.gLength, shape.gWidth, shapeType == Optimizer.ShapeType.Polygon);

						// Imposta il poligono e il file xml
						if (compShape.gEqual)
						{
							piece.gZone = origPiece.gZone;
							piece.iSetXmlPolygon(origPiece.gXmlPolygon);
						}
						else
						{
							piece.gZone = shape.gZone;
							piece.iSetXmlPolygon(shape.gXmlPolygon);
						}

						// Calcola la superficie ottimizzata
						if (piece.gZone != null)
							optSurface += piece.gZone.Surface;

						// Recupera la filagna
						if (!groups.GetObjectTable(groupId, out group))
						{
							/***
							 *** ATTENZIONE: GESTISCE SOLO I GRUPPI DI POLIGONI 
							 *** OCCORRE INSERIRE LA GESTIONE DELLE FILAGNE
							 ***/
							// Crea un nuovo gruppo
							group = new GroupOp(po.gId, 0, mOpSlab.gId, Group.GroupType.Polygon, 0d, 0d, 0d, 0d, 0d, 0d,
								Group.State.Ready, Group.LinkIndex.Single, 0d);
							group.gTableId = groupId;
							group.GetPiecesFromDb(mDbInterface);

							groups.AddObject(group);
						}

						// Aggiunge il pezzo al gruppo
						group.gPieces.AddObject(piece);
					}

					// Aggiorna tutti i nuovi gruppi nel DB
					if (!groups.UpdateDataToDb())
						throw new ApplicationException("CopyOptimization.GetOptResultIntoDb add new groups error.");

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("CopyOptimization.GetOptResultIntoDb Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("CopyOptimization.GetOptResultIntoDb Error.", ex);

					return false;
				}
			}

			return false;
		}
		
		/// ***************************************************************************
		/// <summary>
		/// Alla fine dell'ottimizzazione si aggiornano le tabelle utilizzate solo dal 
		/// preottimizzatore.
		/// </summary>
		/// <param name="optSurface">superficie ottimizzata</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		/// ***************************************************************************
		private bool UpdateSolutionsInfo(double optSurface)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Aggiorna lo stato delle lastre
					if (!mOpSlabs.SetOpSlabState(mOpSlab.gId,
												 OpSlab.State.Programming,
												 mOpSlabSrc.gCutType,
												 optSurface))
						throw new ApplicationException("CopyOptimization.UpdateSolutionsInfo update T_OP_SLABS state error.");

					// Legge l'ordine di produzione in esecuzione
					ProdOrder po;
					if (!GetActualProdOrder(out po))
						throw new ApplicationException("CopyOptimization.UpdateSolutionsInfo get production order error.");


					/*
					 * Aggiorna i risultati dell'ordine di produzione
					 */

					par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					par.Direction = System.Data.ParameterDirection.ReturnValue;
					parameters.Add(par);

					par = new OleDbParameter("@sequence_id", OleDbType.Integer);
					par.Value = 0;
					parameters.Add(par);

					par = new OleDbParameter("@id_op", OleDbType.Integer);
					par.Value = po.gId;
					parameters.Add(par);

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoUpdateResults", ref parameters))
						throw new ApplicationException("CopyOptimization.UpdateSolutionsInfo sp_PoUpdateResults Execute error.");

					// Verifica il codice di ritorno
					par = (OleDbParameter)parameters[0];
					if (((int)par.Value) != 0)
						throw new ApplicationException("CopyOptimization.UpdateSolutionsInfo sp_PoUpdateResults error: " + ((int)par.Value));

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("CopyOptimization.UpdateSolutionsInfo Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("CopyOptimization.UpdateSolutionsInfo Error.", ex);

					return false;
				}
			}

			return false;
		}

		#endregion
	}
}
