using System;
using System.Xml;
using Breton.DbAccess;
using Breton.Polygons;
using Breton.MathUtils;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe astratta che implementa un pezzo delle lastre, sul tipo FK
	/// </summary>
	///*************************************************************************
	public abstract class Piece: DbObject
	{
		#region Structs, Enums & Variables

		// Stato pezzo
		public enum State
		{   Undef = -1, 
            Ready,					// pezzo pronto
            InProgress,				// pezzo in lavoro
            Unloading,				// pezzo in scarico
            Completed,				// pezzo lavorato
    };

		// Tipo pezzo
		public enum PieceType
		{
			Undef = -1,
			Good,					// pezzo buono
			Discard,				// pezzo scarto
			Discard_Unload,			// pezzo scarto allo scarico
			Filling,				// pezzo ordine di riempimento (guadagno materiale)
			Run,					// pezzo ordine a correre
			Production				// pezzo ordine di riempimento (produzione)
		}

		protected int mId;										// id pezzo
		protected int mIdGroup;									// id gruppo
		protected int mIdShape;									// id formato
		protected State mPieceState;							// stato pezzo
		protected double mPosX;									// posizione X all'interno della lastra (solo per pezzi di filagne)
		protected double mPosY;									// posizione Y all'interno della lastra (solo per pezzi di filagne)
		protected bool mRotated;								// pezzo ruotato
		protected PieceType mType;								// tipo pezzo
		protected double mTraslX;								// traslazione pezzo in X (solo pezzi poligonali)
		protected double mTraslY;								// traslazione pezzo in Y (solo pezzi poligonali)
		protected double mRotAngle;								// angolo di rotazione (solo pezzi poligonali)
		protected string mCode;									// codice

		protected int mIdOp;									// id ordine di produzione

		protected double mDimX;									// dimensione X
		protected double mDimY;									// dimensione Y
		protected bool mIsPolygon;								// true = poligono / false = rettangolo

		protected string mXmlPolygon;							// Link memorizzazione poligono XML
		protected Zone mZone;									// poligono che descrive il pezzo

		public enum Keys 
		{F_NONE = -1, F_ID_PIECE, F_ID_GROUP, F_ID_SHAPE, F_STATE, F_X_POS, F_Y_POS, F_ROTATED, F_DISCARD, F_TRASL_X, F_TRASL_Y, F_ROT_ANGLE, F_CODE, F_MAX};

		#endregion

		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore base
		/// </summary>
		///**********************************************************************
		public Piece(int idOp, int id, int idGroup, int idShape, Piece.State state, double posX, double posY, bool rotated, PieceType type, double traslX, double traslY, double rotAngle, string code, double dimX, double dimY, bool isPolygon)
		{
			mId = id;
			mIdGroup = idGroup;
			mIdShape = idShape;
			mPieceState = state;
			mPosX = posX;
			mPosY = posY;
			mRotated = rotated;
			mType = type;
			mTraslX = traslX;
			mTraslY = traslY;
			mRotAngle = rotAngle;
			mCode = code;

			mDimX = dimX;
			mDimY = dimY;
			mIsPolygon = isPolygon;

			mIdOp = idOp;

			mXmlPolygon = "";
			mZone = null;
		}

		public Piece():this(0, 0, 0, 0, Piece.State.Undef, 0d, 0d, false, Piece.PieceType.Undef, 0d, 0d, 0d, "", 0d, 0d, false)
		{
		}

		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public int gIdGroup
		{
			set
			{
				mIdGroup = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mIdGroup;	}
		}

		public int gIdShape
		{
			set
			{
				mIdShape = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mIdShape; }
		}

		public int gIdOp
		{
			set
			{
				mIdOp = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mIdOp; }
		}

		public Piece.State gPieceState
		{
			set 
			{
				mPieceState = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mPieceState; }
		}

		public double gPosX
		{
			set
			{
				mPosX = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mPosX; }
		}

		public double gPosY
		{
			set
			{
				mPosY = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mPosY; }
		}

		public bool gRotated
		{
			set
			{
				mRotated = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mRotated; }
		}

		public PieceType gType
		{
			set
			{
				mType = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mType; }
		}

		public double gTraslX
		{
			set
			{
				mTraslX = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mTraslX; }
		}

		public double gTraslY
		{
			set
			{
				mTraslY = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mTraslY; }
		}

		public double gRotAngle
		{
			set
			{
				mRotAngle = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mRotAngle; }
		}

		public string gCode
		{
			set
			{
				mCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public double gDimX
		{
			set
			{
				mDimX = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDimX; }
		}

		public double gDimY
		{
			set
			{
				mDimY = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDimY; }
		}

		public string gXmlPolygon
		{
			get { return mXmlPolygon; }
		}

		public void iSetXmlPolygon(string link)
		{
			mXmlPolygon = link;
		}

		///*********************************************************************
		/// <summary>
		/// Poligono che descrive il pezzo
		/// </summary>
		///*********************************************************************
		public Zone gZone
		{
			get
			{
				// Verifica se il poligono � gi� stato letto
				if (mZone != null)
					return mZone;

				// Verifica se esiste un link al file poligono
				if (mXmlPolygon == "")
				{
					// Se � un rettangolo, lo crea direttamente
					if (!IsPolygon)
					{
						mZone = new Zone(new Rect(mDimX, mDimY));
						return mZone;
					}
					// Altrimenti non � possibile leggere il poligono
					return null;
				}

				// Legge il poligono dal file
				mZone = ReadXmlPolygon(mXmlPolygon);
				return mZone;
			}
			set { mZone = value; }

		}

		public bool IsPolygon
		{
			get { return mIsPolygon; }
			internal set { mIsPolygon = value; }
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Piece)
			{
				Piece piece = (Piece) obj;

				int cmp = 0;

				cmp = mId.CompareTo(piece.mId);

				return cmp;
			}

			throw new ArgumentException("object is not a Piece");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			return base.GetField(key, out retValue);
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		///*********************************************************************
		/// <summary>
		/// Legge il poligono che descrive il pezzo dal file
		/// </summary>
		/// <param name="fileXML">nome del file</param>
		/// <returns>zona che descrive il poligono</returns>
		///*********************************************************************
		private Zone ReadXmlPolygon(string fileXML)
		{
			XmlDocument doc = new XmlDocument();
			Zone zone = null;

			try
			{
				// Legge il file XML
				doc.Load(fileXML);

				XmlNode zoneNode = null;

				// Cerca il nodo della zona
				if ((zoneNode = doc.SelectSingleNode("//Zone")) != null)
				{
					// Legge la zona
					zone = new Zone();
					if (zone.ReadFileXml(zoneNode))
						return zone;
				}

				// Se non trova il nodo della zona, cerca il nodo Shape
				else if ((zoneNode = doc.SelectSingleNode("//Shape")) != null)
				{
				}

				// non ha letto alcuna zona
				return null;
			}

			catch (XmlException ex)
			{
				TraceLog.WriteLine("Piece.ReadXmlPolygon Error.", ex);
				return null;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Piece.ReadXmlPolygon Error.", ex);
				return null;
			}
			finally
			{
				doc = null;
			}
		}

		#endregion

	}
}
