using System;
using System.Threading;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public class SlabGroupCollection: DbCollection
	{
		#region Variables

		#endregion

		#region Constructors
		public SlabGroupCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out SlabGroup obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (SlabGroup) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out SlabGroup obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (SlabGroup) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(SlabGroup obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		///**********************************************************************
		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id elemento</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(SlabGroup.Keys.F_NONE, id, 0, 0);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(SlabGroup.Keys.F_NONE, 0, 0, 0);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(SlabGroup.Keys orderKey)
		{
			return GetDataFromDb(orderKey, 0, 0, 0);
		}

		///*********************************************************************
		/// <summary>
		/// Legge le lastre legate ad un ordine di produzione
		/// </summary>
		/// <param name="idOp">id ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
        public bool GetDataFromDb(int idOp)
		{
            return GetDataFromDb(SlabGroup.Keys.F_ID_OP_SLAB_GROUP, 0, idOp, 0);
		}
		
		///*********************************************************************
		/// <summary>
		/// Legge le lastre legate ad un ordine di produzione, ad un gruppo e
		/// ad uno stato
		/// </summary>
		/// <param name="idOp">id ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetDataFromDb(int idOp, int group)
		{
            return GetDataFromDb(SlabGroup.Keys.F_ID_OP_SLAB_GROUP, 0, idOp, group);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="id">estrae solo l'elemento con l'id specificato</param>
		/// <param name="idOp">estrae gli elementi dell'ordine di produzione indicato</param>
        /// <param name="group">estrae gli elementi del gruppo indicato</param>
        /// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(SlabGroup.Keys orderKey, int id, int idOp, int group)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo il materiale con il codice richiesto
			if (id > 0)
			{
                sqlWhere += sqlAnd + "F_ID_OP_SLAB_GROUP = " + id.ToString();
				sqlAnd = " and ";
			}

			if (idOp > 0)
			{
				sqlWhere += sqlAnd + "F_ID_OP = " + idOp.ToString();
				sqlAnd = " and ";
			}

			if (group > 0)
			{
				sqlWhere += sqlAnd + "F_GROUP = " + group.ToString();
				sqlAnd = " and ";
			}

			switch (orderKey)
			{
                case SlabGroup.Keys.F_ID_OP_SLAB_GROUP:
                    sqlOrderBy = " order by F_ID_OP_SLAB_GROUP";
					break;
				case SlabGroup.Keys.F_ID_OP:
					sqlOrderBy = " order by F_ID_OP";
					break;
				case SlabGroup.Keys.F_GROUP:
					sqlOrderBy = " order by F_GROUP";
					break;
			}
            return GetDataFromDb("select * from T_OP_SLAB_GROUPS" + sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (SlabGroup opSlab in mRecordset)
					{
						switch (opSlab.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
                                sqlInsert = "insert into T_OP_SLAB_GROUPS (" + 
									"F_ID_OP" +
									", F_GROUP" +
									", F_LENGTH" + 
									", F_WIDTH" +
									")" +
									"values (" + opSlab.gIdOp + 
									", " + opSlab.gGroup +
									", " + opSlab.gLength +
									", " + opSlab.gWidth +
									")" +
									";select scope_identity()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								opSlab.gId = id;
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
                                sqlUpdate = "update T_OP_SLAB_GROUPS set" +
                                    ", F_ID_OP = " + opSlab.gIdOp +
                                    ", F_GROUP = " + opSlab.gGroup +
                                    ", F_LENGTH = " + opSlab.gLength +
                                    ", F_WIDTH = " + opSlab.gWidth;
							
								sqlUpdate += " where F_ID_OP_SLAB_GROUP = " + opSlab.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;
						
								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (SlabGroup opSlab in mDeleted)
					{
						switch (opSlab.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_OP_SLAB_GROUPS where F_ID_OP_SLAB_GROUP = " + opSlab.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SlabGroupCollection: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("SlabGroupCollection: Error", ex);

					// Annulla la transazione
					if (transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion

		#region Private Methods
		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, secondo la query specificata
		/// </summary>
		/// <param name="sqlQuery">query</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	errore
		///	</returns>
		///*********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			SlabGroup opSlab;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					opSlab = new SlabGroup((int) dr["F_ID_OP_SLAB_GROUP"],
						(int) dr["F_ID_OP"],
						(int) dr["F_GROUP"],
						(double) ((float) dr["F_LENGTH"]),
						(double) ((float) dr["F_WIDTH"]));
					
					AddObject(opSlab);
					opSlab.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("SlabGroupCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}
}
