using System;
using Breton.DbAccess;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa una sequenza del preottimizzatore
	/// </summary>
	///*************************************************************************
	public class Sequence: DbObject
	{
		#region Structs, Enums & Variables

		// Tipologia di sviluppo delle sequenze
		public enum SeqType
		{
			Undef = -1,
			Simple = 0,		// sviluppo semplice (unico materiale)
			Complete = 1,	// sviluppo completo (materiali diversi)
			Max
		}

		// Modi possibili di definizione del materiale da usare in una sequenza
		public enum Mode
		{
			Undef = -1,							// modalita' non ammessa
			GuidedRefSlab = 0,					// guidato (definizione interattiva) su lastre tipo
			GuidedRealSlab = 1,					// guidato (definizione interattiva) su lastre reali
			SimpleRefSlab = 2,					// semplice (definizione preliminare) su lastre tipo
			SimpleRealSlab = 3,					// semplice (definizione preliminare) su lastre reali
			OptimalRefSlab = 4,					// libero (definizione preliminare) su lastre tipo
			OptimalRealSlab = 5,				// libero (definizione preliminare) su lastre reali
			Max
		}

		// Stati della Sequenza
		public enum State
		{
			Undef = -1,							// non definito
			Available = 0,						// disponibile
			Programmed = 1,						// programmato
			Confirmed = 2,						// confermato (utilizzabile in macchina)
			Busy = 3,							// in lavorazione
			Completed = 4,						// completato
			Suspended = 5,						// sospesa
			Locked = 6,							// prenotata
			Max
		}

		private int mId;
		private State mStateSeq;
		private DateTime mStartDate, mStopDate;
		private string mDescription;
		private int mMatId;
		private string mMatCode;
		private SeqType mSeqType;
		private Mode mMode;
		private ProdOrderCollection mProdOrders;

		public enum Keys 
		{
			F_NONE = -1, F_ID_PRO_SEQUENCE, F_ID_T_AN_MATERIALS, F_STATE, F_DT_START, F_DT_STOP, 
			F_DESCRIPTION, F_TYPE, F_MODE, F_MAT_CODE, F_MAX};

		#endregion

		#region Constructors
		///**********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		/// <param name="id"></param>
		/// <param name="matId"></param>
		/// <param name="matCode"></param>
		/// <param name="state"></param>
		/// <param name="startDate"></param>
		/// <param name="stopDate"></param>
		/// <param name="description"></param>
		/// <param name="type"></param>
		/// <param name="mode"></param>
		///**********************************************************************
		public Sequence(int id, int matId, string matCode, State state, DateTime startDate, DateTime stopDate, 
			string description, SeqType type, Mode mode)
		{
			mId = id;
			mStateSeq = state;
			mStartDate = startDate;
			mStopDate = stopDate;
			mDescription = description;
			mMatId = matId;
			mMatCode = matCode;
			mMode = mode;
			mSeqType = type;
			mProdOrders = null;
		}

		public Sequence():this(0, 0, "", State.Undef, DateTime.MinValue, DateTime.MinValue, "", SeqType.Undef, Mode.Undef)
		{
		}

		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public State gStateSeq
		{
			set
			{
				mStateSeq = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mStateSeq;	}
		}

		public DateTime gStartDate
		{
			set 
			{
				mStartDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mStartDate; }
		}

		public DateTime gStopDate
		{
			set 
			{
				mStopDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mStopDate; }
		}

		public string gDescription
		{
			set 
			{
				mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		public int gMatId
		{
			set 
			{
				mMatId = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mMatId; }
		}

		public string gMatCode
		{
			set 
			{
				mMatCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mMatCode; }
		}

		public Mode gMode
		{
			set 
			{
				mMode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mMode; }
		}

		public SeqType gSeqType
		{
			set
			{
				mSeqType = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mSeqType;	}
		}

		public ProdOrderCollection gProdOrders
		{
			get	{ return mProdOrders; }
		}
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Sequence)
			{
				Sequence seq = (Sequence) obj;

				int cmp = 0;

				cmp = mMatCode.CompareTo(seq.mMatCode);

				if (cmp == 0)
				{
					cmp = mSeqType.CompareTo(seq.mSeqType);

					if (cmp == 0)
						cmp = mMode.CompareTo(seq.mMode);
				}

				return cmp;
			}

			throw new ArgumentException("object is not a Sequence");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == Sequence.Keys.F_ID_PRO_SEQUENCE.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_STATE.ToString())
				return mStateSeq.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_DT_START.ToString())
				return mStartDate.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_DT_STOP.ToString())
				return mStopDate.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_DESCRIPTION.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_ID_T_AN_MATERIALS.ToString())
				return mMatId.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_MAT_CODE.ToString())
				return mMatCode.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_MODE.ToString())
				return mMode.GetTypeCode();
			if (key.ToUpper() == Sequence.Keys.F_TYPE.ToString())
				return mSeqType.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out DateTime retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Sequence.Keys.F_ID_PRO_SEQUENCE.ToString())
			{
				retValue = mId;
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_ID_T_AN_MATERIALS.ToString())
			{
				retValue = mMatId;
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_STATE.ToString())
			{
				retValue = (int) mStateSeq;
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_TYPE.ToString())
			{
				retValue = (int) mSeqType;
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_MODE.ToString())
			{
				retValue = (int) mMode;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Sequence.Keys.F_ID_PRO_SEQUENCE.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_ID_T_AN_MATERIALS.ToString())
			{
				retValue = mMatId.ToString();
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_STATE.ToString())
			{
				retValue = mStateSeq.ToString();
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_DT_START.ToString())
			{
				retValue = mStartDate.ToString();
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_DT_STOP.ToString())
			{
				retValue = mStopDate.ToString();
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_MAT_CODE.ToString())
			{
				retValue = mMatCode;
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_MODE.ToString())
			{
				retValue = mMode.ToString();
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_TYPE.ToString())
			{
				retValue = mSeqType.ToString();
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{
			if (key.ToUpper() == Sequence.Keys.F_DT_START.ToString())
			{
				retValue = mStartDate;
				return true;
			}

			if (key.ToUpper() == Sequence.Keys.F_DT_STOP.ToString())
			{
				retValue = mStopDate;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		///**********************************************************************
		/// <summary>
		/// Legge gli ordini di produzione associati alla sequenza dal database
		/// </summary>
		/// <param name="db">db interface</param>
		/// <param name="state">stato ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///**********************************************************************
		public bool GetProdOrdersFromDb(DbInterface db)
		{
			return GetProdOrdersFromDb(db, ProdOrder.State.Undef);
		}
		public bool GetProdOrdersFromDb(DbInterface db, ProdOrder.State state)
		{
			// Crea la lista degli ordini di produzione
			if (mProdOrders == null)
				mProdOrders = new ProdOrderCollection(db);

			mProdOrders.Clear();

			return mProdOrders.GetDataFromDb(ProdOrder.Keys.F_ID_OP, mId, state);
		}
		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}
		#endregion

	}
}
