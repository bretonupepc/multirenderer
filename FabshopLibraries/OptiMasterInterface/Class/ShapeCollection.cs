using System;
using System.Threading;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public class ShapeCollection: DbCollection
	{
		#region Variables

		private int mCountOptRecover;

		#endregion


		#region Constructors
		public ShapeCollection(DbInterface db): base(db)
		{
			mCountOptRecover = 0;
		}
		#endregion


		#region Properties

		/// **************************************************************************
		/// <summary>
		/// Ritorna il numero dei formati di ottimizzazione di recupero
		/// </summary>
		/// **************************************************************************
		public int CountOptRecover
		{
			get { return mCountOptRecover; }
		}
		#endregion


		#region Public Methods

		public override void Clear()
		{
			base.Clear();
			mCountOptRecover = 0;
		}

		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Shape obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (Shape) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Shape obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Shape) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Shape obj)
		{
			if (!base.AddObject((DbObject)obj))
				return false;

			// Conta i formati di recupero
			if (obj.gOptRecover)
				mCountOptRecover++;

			return true;
		}

		/// **************************************************************************
		/// <summary>
		/// Cancella l'oggetto indicato
		/// </summary>
		/// <param name="index">indice dell'oggetto da cancellare</param>
		/// <returns>
		///			true	oggetto cancellato
		///			false	errore nella cancellazione oggetto
		/// </returns>
		/// **************************************************************************
		public override bool DeleteObject(int index)
		{
			Shape sh;
			bool optRecover = false;

			// Verifica l'indice
			if (IsIndexOk(index))
			{
				sh = (Shape)mRecordset[index];
				optRecover = sh.gOptRecover;
			}

			if (!base.DeleteObject(index))
				return false;

			if (optRecover)
				mCountOptRecover--;

			return true;
		}

		///**********************************************************************
		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id elemento</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(Shape.Keys.F_NONE, id, 0, 0);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Shape.Keys.F_NONE, 0, 0, 0);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Shape.Keys orderKey)
		{
			return GetDataFromDb(orderKey, 0, 0, 0);
		}

		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, con lo stato indicato
		/// </summary>
		/// <param name="orderId">id ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetDataFromDb(int orderId)
		{
			return GetDataFromDb(Shape.Keys.F_ID_SHAPE, 0, orderId, 0);
		}

		///*********************************************************************
		/// <summary>
		/// Legge i formati legati ad un ordine di produzione
		/// </summary>
		/// <param name="orderId">id ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetProdOrderShapesFromDb(int orderProd)
		{
			return GetDataFromDb(Shape.Keys.F_ID_SHAPE, 0, 0, orderProd);
		}
		
		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="id">estrae solo l'elemento con l'id specificato</param>
		/// <param name="orderId">estrae gli elementi dell'ordine indicato</param>
		/// <param name="idOp">estrae gli elementi dell'ordine di produzione indicato</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(Shape.Keys orderKey, int id, int orderId, int idOp)
		{
			return GetDataFromDb(orderKey, id, orderId, idOp, -1, null, false);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="id">estrae solo l'elemento con l'id specificato</param>
		/// <param name="orderId">estrae gli elementi dell'ordine indicato</param>
		/// <param name="idOp">estrae gli elementi dell'ordine di produzione indicato</param>
		/// <param name="getAll">estrae tutti gli elementi indipendentemente dalla quantit� disponibile</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(Shape.Keys orderKey, int id, int orderId, int idOp, int idOpShape, string shapeCode, bool getAll)
		{
			string sqlQuery;
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo il materiale con il codice richiesto
			if (id > 0)
			{
				sqlWhere += sqlAnd + "F_ID_SHAPE = " + id.ToString();
				sqlAnd = " and ";
			}

			if (orderId > 0)
			{
				sqlWhere += sqlAnd + "o.F_ID_ORDER = " + orderId.ToString();
				sqlAnd = " and ";
			}

			if (idOp > 0)
			{
				if (idOpShape > 0)
				{
					sqlWhere += sqlAnd + "op.F_ID_OP_SHAPE = " + idOpShape.ToString();
					sqlAnd = " and ";
				}

				if (shapeCode != null && shapeCode.Length > 0)
				{
					sqlWhere += sqlAnd + "s.F_SHAPE_CODE like '%" + shapeCode.Trim().ToString() + "%'";
					sqlAnd = " and ";
				}

				sqlQuery = "select s.*, o.F_ORDER_CODE, o.F_SEQUENCE, m.F_CODE as F_MAT_CODE, op.* from T_SHAPES s" +
					" inner join T_ORDERS o on o.F_ID_ORDER = s.F_ID_ORDER" +
					" inner join T_AN_MATERIALS m on m.F_ID = s.F_ID_MATERIAL" +
					" inner join T_OP_SHAPES op on op.F_ID_SHAPE = s.F_ID_SHAPE";
				sqlWhere += sqlAnd + "op.F_ID_OP = " + idOp.ToString();
				if (!getAll)
				{
					sqlAnd = " and ";
					sqlWhere += sqlAnd + "op.F_AVAILABLE_QUANTITY > 0";
				}
			}
			else
				sqlQuery = "select s.*, o.F_ORDER_CODE, o.F_SEQUENCE, m.F_CODE as F_MAT_CODE" +
					", 0 as F_ID_OP, 0 as F_ID_OP_SHAPE, 0 as F_AVAILABLE_QUANTITY from T_SHAPES s" +
					" inner join T_ORDERS o on o.F_ID_ORDER = s.F_ID_ORDER" +
					" inner join T_AN_MATERIALS m on m.F_ID = s.F_ID_MATERIAL";

			switch (orderKey)
			{
				case Shape.Keys.F_ID_SHAPE:
					sqlOrderBy = " order by s.F_ID_SHAPE";
					break;
				case Shape.Keys.F_ORDER_CODE:
					sqlOrderBy = " order by o.F_ORDER_CODE";
					break;
				case Shape.Keys.F_MAT_CODE:
					sqlOrderBy = " order by m.F_CODE";
					break;
			}
			return GetDataFromDb(sqlQuery + sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Shape shape in mRecordset)
					{
						switch (shape.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_SHAPES (" + 
									"F_ID_ORDER" +
									", F_ID_MATERIAL" +
									", F_INITIAL_QUANTITY" +
									", F_ASSIGNED_QUANTITY" +
									", F_PRODUCED_QUANTITY" + 
									", F_SHAPE_TYPE" +
									", F_LENGTH" +
									", F_WIDTH" +
									", F_THICKNESS" +
									", F_PRIORITY" +
									", F_DESCRIPTION" +
									", F_STAMP" +
									", F_SHAPE_CODE" +
									", F_NOTE" +
									", F_ID_SHAPE_RUN" +
									")" +

                                    // De Conti 20161104
                                    //" values "+

                                    " (select" +
									" " + shape.gOrderId + 
									", F_ID" +
									", " + shape.gInitialQty + 
									", " + shape.gAssignedQty +
									", " + shape.gProducedQty +
									", " + ((int) shape.gShapeType) +
									", " + shape.gLength.ToString() +
									", " + shape.gWidth.ToString() +
									", " + shape.gThickness.ToString() +
									", " + shape.gPriority +
									", '" + shape.gDescription + "'" +
									", " + (shape.gPrint ? "1" : "0") +
									", '" + shape.gCode + "'" +
									", '" + shape.gNote + "'" +
									", " + shape.gIdRun +
									" from T_AN_MATERIALS where F_CODE = '" + shape.gMatCode + "')" +
									";select SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								shape.gId = id;
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_SHAPES set" +
									" F_ID_ORDER = " + shape.gOrderId +
									", F_ID_MATERIAL = (select F_ID from T_AN_MATERIALS where F_CODE = '" + shape.gMatCode + "')" +
									", F_INITIAL_QUANTITY = " + shape.gInitialQty +
									", F_ASSIGNED_QUANTITY = " + shape.gAssignedQty +
									", F_PRODUCED_QUANTITY = " + shape.gProducedQty +
									", F_SHAPE_TYPE = " + ((int) shape.gShapeType) +
									", F_LENGTH = " + shape.gLength +
									", F_WIDTH = " + shape.gWidth +
									", F_THICKNESS = " + shape.gThickness +
									", F_PRIORITY = " + shape.gPriority +
									", F_DESCRIPTION = '" + shape.gDescription + "'" +
									", F_STAMP = " + (shape.gPrint ? "1" : "0") +
									", F_SHAPE_CODE = '" + shape.gCode + "'" +
									", F_NOTE = '" + shape.gNote + "'" +
									", F_ID_SHAPE_RUN = " + shape.gIdRun;
							
								sqlUpdate += " where F_ID_SHAPE = " + shape.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;
						
								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Shape shape in mDeleted)
					{
						switch (shape.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_SHAPES where F_ID_SHAPE = " + shape.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ShapeCollection: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("ShapeCollection: Error", ex);

					// Annulla la transazione
					if (transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}
		
		///*********************************************************************
		/// <summary>
		/// Legge il poligono xml dal database
		/// </summary>
		/// <param name="shape">formato di cui leggere il poligono</param>
		/// <param name="fileName">nome del file su cui memorizzare il poligono</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	lettura non eseguita
		///	</returns>
		///*********************************************************************
		public bool GetXmlPolygonFromDb(Shape shape, string fileName)
		{
			try
			{
				// Recupera l'immagine dal db
				if (!mDbInterface.GetBlobField("T_SHAPES", "F_POLYGON", " F_ID_SHAPE = " + shape.gId.ToString(), fileName))
					return false;
			}
				
				// Eccezione
			catch (Exception ex)
			{
				TraceLog.WriteLine("ShapeCollection.GetXmlPolygonFromDb: Error", ex);
				return false;
			}

			shape.iSetXmlPolygon(fileName);
			return true;
		}

		///*********************************************************************
		/// <summary>
		/// Scrive il poligono xml nel database
		/// </summary>
		/// <param name="shape">formato di cui scrivere il poligono</param>
		/// <param name="fileName">nome del file da cui prelevare il poligono</param>
		/// <param name="noTrigger">disabilita la generazione del trigger di aggiornamento</param>
		/// <returns>
		///		true	scrittura eseguita
		///		false	scrittura non eseguita
		///	</returns>
		///*********************************************************************
		public bool SetXmlPolygonToDb(Shape shape, string fileName, bool noTrigger)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Scrive l'immagine su db
					if (!mDbInterface.WriteBlobField("T_SHAPES", "F_POLYGON", "", " F_ID_SHAPE = " + shape.gId.ToString(), fileName, 
						(noTrigger ? "" : "F_SHAPE_CODE ")))
						throw (new ApplicationException("ShapeCollection.SetXmlPolygonToDb: error writing image"));

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					shape.iSetXmlPolygon(fileName);
					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ShapeCollection.SetXmlPolygonToDb: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("ShapeCollection.SetXmlPolygonToDb: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}
			return false;
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion


		#region Private Methods
		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, secondo la query specificata
		/// </summary>
		/// <param name="sqlQuery">query</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	errore
		///	</returns>
		///*********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Shape shape;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					shape = new Shape((int)dr["F_ID_SHAPE"],
						(int)dr["F_ID_ORDER"], dr["F_ORDER_CODE"].ToString(),
						(int)dr["F_ID_MATERIAL"], dr["F_MAT_CODE"].ToString(),
						(int)dr["F_INITIAL_QUANTITY"], (int)dr["F_ASSIGNED_QUANTITY"], (int)dr["F_PRODUCED_QUANTITY"],
						(Shape.ShapeType)dr["F_SHAPE_TYPE"],
						(float)dr["F_LENGTH"], (float)dr["F_WIDTH"], (float)dr["F_THICKNESS"],
						(int)dr["F_PRIORITY"],
						(dr["F_DESCRIPTION"] == System.DBNull.Value ? "" : dr["F_DESCRIPTION"].ToString()),
						(bool)dr["F_STAMP"],
						dr["F_SHAPE_CODE"].ToString(),
						(dr["F_NOTE"] == System.DBNull.Value ? "" : dr["F_NOTE"].ToString()),
						(int)dr["F_ID_SHAPE_RUN"],
						(int)dr["F_ID_OP"], (int)dr["F_ID_OP_SHAPE"],
						(dr["F_ID_T_WK_WORK_PHASES_H"] == System.DBNull.Value ? 0 : (int)dr["F_ID_T_WK_WORK_PHASES_H"]),
						(int)dr["F_AVAILABLE_QUANTITY"], ((int)dr["F_SEQUENCE"]) < 0);

					AddObject(shape);

					shape.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("ShapeCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}
}
