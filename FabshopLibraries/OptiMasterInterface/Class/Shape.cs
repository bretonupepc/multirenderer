using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Breton.DbAccess;
using Breton.Polygons;
using Breton.MathUtils;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa una formato del preottimizzatore
	/// </summary>
	///*************************************************************************
	public class Shape: DbObject
	{
		#region Structs, Enums & Variables

		// Tipo formato
		public enum ShapeType
		{ Undef = -1, Rectangles, Polygons};

		private int mId;										// id formato
		private int mOrderId;									// id ordine
		private string mOrderCode;								// codice ordine
		private int mMatId;										// id materiale
		private string mMatCode;								// codice materiale
		private int mInitialQty, mAssignedQty, mProducedQty;	// quantit� iniziale, assegnata, prodotta
		private ShapeType mShapeType;							// tipo formato
		private float mLength, mWidth, mThickness;				// lunghezza, larghezza, spessore
		private int mPriority;									// priorit�
		private string mDescription;							// descrizione
		private bool mPrint;									// stampa etichetta
		private string mCode;									// codice formato
		private string mNote;									// note
		private string mXmlPolygon;								// Link memorizzazione poligono XML
		private int mIdRun;										// id formato a correre
        private Zone mZone;                                     // poligono che descrive il formato

		private int mIdOp;										// id ordine di produzione
		private int mIdOpShape;									// id formato ordine di produzione
		private int mIdWorkPhase;								// id fase di lavorazione
		private int mAvailableQty;								// quantit� disponibile
		private bool mOptRecover;								// pezzo per ottimizzazione di recupero

		public enum Keys 
		{
			F_NONE = -1, F_ID_SHAPE, F_ID_ORDER, F_ID_MATERIAL, F_INITIAL_QUANTITY, F_ASSIGNED_QUANTITY, F_PRODUCED_QUANTITY,
			F_SHAPE_TYPE, F_LENGTH, F_WIDTH, F_THICKNESS, F_PRIORITY, F_DESCRIPTION, F_STAMP, F_SHAPE_CODE, F_NOTE, F_ID_SHAPE_RUN,
			F_ORDER_CODE, F_MAT_CODE, F_ID_T_WK_WORK_PHASE_H, F_MAX};

		#endregion


		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		/// <param name="id"></param>
		/// <param name="orderId"></param>
		/// <param name="orderCode"></param>
		/// <param name="matId"></param>
		/// <param name="matCode"></param>
		/// <param name="initialQty"></param>
		/// <param name="assignedQty"></param>
		/// <param name="producedQty"></param>
		/// <param name="type"></param>
		/// <param name="length"></param>
		/// <param name="width"></param>
		/// <param name="thickness"></param>
		/// <param name="priority"></param>
		/// <param name="description"></param>
		/// <param name="print"></param>
		/// <param name="code"></param>
		/// <param name="note"></param>
		///**********************************************************************
		public Shape(int id, int orderId, string orderCode, int matId, string matCode, int initialQty, int assignedQty, int producedQty,
			ShapeType type, float length, float width, float thickness, int priority, string description, bool print, string code, string note,
			int idRun, int idOp, int idOpShape, int idWorkPhase, int availableQty, bool optRecover)
		{
			mId = id;
			mOrderId = orderId;
			mOrderCode = orderCode;
			mMatId = matId;
			mMatCode = matCode;
			mInitialQty = initialQty;
			mAssignedQty = assignedQty;
			mProducedQty = producedQty;
			mShapeType = type;
			mLength = length;
			mWidth = width;
			mThickness = thickness;
			mPriority = priority;
			mDescription = description;
			mPrint = print;
			mCode = code;
			mNote = note;
			mIdRun = idRun;

			mIdOp = idOp;
			mIdOpShape = idOpShape;
			mIdWorkPhase = idWorkPhase;
			mAvailableQty = availableQty;
			mOptRecover = optRecover;

			mXmlPolygon = "";
            mZone = null;
		}

		public Shape():this(0, 0, "", 0, "", 0, 0, 0, Shape.ShapeType.Undef, 0f, 0f, 0f, 0, "", false, "", "", 0, 0, 0, 0, 0, false)
		{
		}

		#endregion


		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public int gOrderId
		{
			set
			{
				mOrderId = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mOrderId;	}
		}

		public string gOrderCode
		{
			set
			{
				mOrderCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mOrderCode; }
		}

		public int gMatId
		{
			set 
			{
				mMatId = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mMatId; }
		}

		public string gMatCode
		{
			set 
			{
				mMatCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mMatCode; }
		}

		public int gInitialQty
		{
			set 
			{
				mInitialQty = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mInitialQty; }
		}

		public int gAssignedQty
		{
			set 
			{
				mAssignedQty = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mAssignedQty; }
		}

		public int gProducedQty
		{
			set 
			{
				mProducedQty = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mProducedQty; }
		}

		public ShapeType gShapeType
		{
			set 
			{
				mShapeType = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mShapeType; }
		}

		public float gLength
		{
			set 
			{
				mLength = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mLength; }
		}

		public float gWidth
		{
			set 
			{
				mWidth = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mWidth; }
		}

		public float gThickness
		{
			set 
			{
				mThickness = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mThickness; }
		}

		public int gPriority
		{
			set 
			{
				mPriority = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mPriority; }
		}

		public string gDescription
		{
			set 
			{
				mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		public bool gPrint
		{
			set 
			{
				mPrint = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mPrint; }
		}

		public string gCode
		{
			set
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mCode;	}
		}

		public string gNote
		{
			set
			{
				mNote = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mNote;	}
		}

		public int gIdRun
		{
			set
			{
				mIdRun = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mIdRun;	}
		}

		public int gIdOp
		{
			get { return mIdOp; }
		}

		public int gIdOpShape
		{
			get { return mIdOpShape; }
		}

		public int gIdWorkPhase
		{
			get { return mIdWorkPhase; }
		}

		public int gAvailableQty
		{
			get { return mAvailableQty; }
		}

		public bool gOptRecover
		{
			get { return mOptRecover; }
		}

		public string gXmlPolygon
		{
			get { return mXmlPolygon; }
		}

		internal void iSetXmlPolygon(string link)
		{
			mXmlPolygon = link;
		}

        ///*********************************************************************
        /// <summary>
        /// Poligono che descrive il formato
        /// </summary>
        ///*********************************************************************
        public Zone gZone
        {
            get
            {
                // Verifica se il poligono � gi� stato letto
                if (mZone != null)
                    return mZone;

                // Verifica se esiste un link al file poligono
                if (mXmlPolygon == "")
                {
                    // Se � un rettangolo, lo crea direttamente
					if (mShapeType == ShapeType.Rectangles)
					{
						mZone = new Zone(new Rect(mLength, mWidth));
						return mZone;
					}
                    // Altrimenti non � possibile leggere il poligono
                    return null;
                }

                // Legge il poligono dal file
                mZone = ReadXmlPolygon(mXmlPolygon);
                return mZone;
            }
            set { mZone = value; }

        }

		#endregion


		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Shape)
			{
				Shape shape = (Shape) obj;

				int cmp = 0;

				cmp = mMatCode.CompareTo(shape.mMatCode);

				if (cmp == 0)
				{
					cmp = mThickness.CompareTo(shape.mThickness);

					if (cmp == 0)
						cmp = mPriority.CompareTo(shape.mPriority);
				}

				return cmp;
			}

			throw new ArgumentException("object is not a Shape");
		}

		#endregion


		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == Shape.Keys.F_ID_SHAPE.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_ID_ORDER.ToString())
				return mOrderId.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_ORDER_CODE.ToString())
				return mOrderCode.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_ID_MATERIAL.ToString())
				return mMatId.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_MAT_CODE.ToString())
				return mMatCode.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_INITIAL_QUANTITY.ToString())
				return mInitialQty.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_ASSIGNED_QUANTITY.ToString())
				return mAssignedQty.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_PRODUCED_QUANTITY.ToString())
				return mProducedQty.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_SHAPE_TYPE.ToString())
				return mShapeType.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_LENGTH.ToString())
				return mLength.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_WIDTH.ToString())
				return mWidth.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_THICKNESS.ToString())
				return mThickness.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_PRIORITY.ToString())
				return mPriority.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_DESCRIPTION.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_STAMP.ToString())
				return mPrint.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_SHAPE_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_NOTE.ToString())
				return mNote.GetTypeCode();
			if (key.ToUpper() == Shape.Keys.F_ID_SHAPE_RUN.ToString())
				return mIdRun.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out bool retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Shape.Keys.F_ID_SHAPE.ToString())
			{
				retValue = mId;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ID_ORDER.ToString())
			{
				retValue = mOrderId;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ID_MATERIAL.ToString())
			{
				retValue = mMatId;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_INITIAL_QUANTITY.ToString())
			{
				retValue = mInitialQty;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ASSIGNED_QUANTITY.ToString())
			{
				retValue = mAssignedQty;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_PRODUCED_QUANTITY.ToString())
			{
				retValue = mProducedQty;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_SHAPE_TYPE.ToString())
			{
				retValue = (int) mShapeType;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_PRIORITY.ToString())
			{
				retValue = mPriority;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ID_SHAPE_RUN.ToString())
			{
				retValue = mIdRun;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Shape.Keys.F_ID_SHAPE.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ID_ORDER.ToString())
			{
				retValue = mOrderId.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ORDER_CODE.ToString())
			{
				retValue = mOrderCode;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ID_MATERIAL.ToString())
			{
				retValue = mMatId.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_MAT_CODE.ToString())
			{
				retValue = mMatCode;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_INITIAL_QUANTITY.ToString())
			{
				retValue = mInitialQty.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ASSIGNED_QUANTITY.ToString())
			{
				retValue = mAssignedQty.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_PRODUCED_QUANTITY.ToString())
			{
				retValue = mProducedQty.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_SHAPE_TYPE.ToString())
			{
				retValue = mShapeType.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_LENGTH.ToString())
			{
				retValue = mLength.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_WIDTH.ToString())
			{
				retValue = mWidth.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_PRIORITY.ToString())
			{
				retValue = mPriority.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_STAMP.ToString())
			{
				retValue = mPrint.ToString();
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_SHAPE_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_NOTE.ToString())
			{
				retValue = mNote;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_ID_SHAPE_RUN.ToString())
			{
				retValue = mIdRun.ToString();
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out double retValue)
		{
			if (key.ToUpper() == Shape.Keys.F_LENGTH.ToString())
			{
				retValue = (double) mLength;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_WIDTH.ToString())
			{
				retValue = (double) mWidth;
				return true;
			}

			if (key.ToUpper() == Shape.Keys.F_THICKNESS.ToString())
			{
				retValue = (double) mThickness;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out bool retValue)
		{
			if (key.ToUpper() == Shape.Keys.F_STAMP.ToString())
			{
				retValue = mPrint;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		///*********************************************************************
		/// <summary>
		/// Verifica se lo shape ha gi� le dimensioni, altrimenti imposta quelle massime della zona
		/// </summary>
		/// <returns>
		///		true	dimensioni impostate
		///		false	dimensioni non impostate
		/// </returns>
		///*********************************************************************
		public bool CheckShapeDimension()
		{
			// Verifica se le misure sono gi� impostate
			if (mLength > 0f && mWidth > 0f)
				return false;

			// Verifica di avere un poligono valido associato
			if (gZone == null)
				return false;

			// Calcola il rettangolo massimo e imposta le misure
			Rect maxRect = mZone.GetRectangle();
			if (maxRect != null)
			{
				gLength = (float)maxRect.Width;
				gWidth = (float)maxRect.Height;
				return true;
			}
			else
				return false;
		}

		///*********************************************************************
		/// <summary>
		/// Verifica se il file XML del formato � completo
		/// </summary>
		/// <param name="fileXML">nome del file</param>
		/// <param name="symbolsToImport">Lista dei simboli Design da importare</param>
		/// <returns>
		///		== -1	file non ok, impossibile generare i nodi mancanti
		///		!= -1	operazione eseguita con successo: nodi generati
		///				0			nessun nodo generato
		///				1 (bit 0)	generato nodo Zone
		///				2 (bit 1)	generato nodo Construction
		///				4 (bit 2)	generato nodo Shape
		/// </returns>
		///*********************************************************************
		public int CheckXmlFile(string fileXML, List<string> symbolsToImport)
		{
			XmlDocument doc = new XmlDocument();
			XmlAttribute attr;
			Zone zone = null;
			bool creaRectangle = false;
			Breton.DesignUtil.Shape designShape = null;

			int mod = 0;

			try
			{
				if (fileXML == "")
				{
					if (mShapeType != ShapeType.Rectangles)
					{
						TraceLog.WriteLine("Shape.ReadXmlPolygon Error, file not exist.");
						return -1;
					}
					else
						creaRectangle = true;
				}
				else
				{
					// Legge il file XML
					doc.Load(fileXML);

					// Legge l'XML del Design
					designShape = new Breton.DesignUtil.Shape();
					if (!Breton.DesignUtil.XmlAnalizer.ReadShapeXml(ref doc, ref designShape, false))
						if (mShapeType != ShapeType.Rectangles)
						{
							TraceLog.WriteLine("Shape.ReadXmlPolygon Error, file XML not correct.");
							return -1;
						}
						else
							creaRectangle = true;
				}
			}

			catch (XmlException ex)
			{
				if (mShapeType != ShapeType.Rectangles)
				{
					TraceLog.WriteLine("Shape.ReadXmlPolygon Error.", ex);
					return -1;
				}
				else
					creaRectangle = true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Shape.ReadXmlPolygon Error.", ex);
				return -1;
			}

			try
			{
				/*
				 * Crea l'XML del rettangolo
				 */
				if (creaRectangle)
				{
					Rect rect = new Rect(mLength, mWidth);

					XmlNode exportNode = doc.CreateNode(XmlNodeType.Element, "EXPORT", "");
					doc.AppendChild(exportNode);

					XmlNode camNode = doc.CreateNode(XmlNodeType.Element, "CAM", "");
					exportNode.AppendChild(camNode);

					XmlNode versionNode = doc.CreateNode(XmlNodeType.Element, "Version", "");
					attr = doc.CreateAttribute("V");
					attr.Value = "1_1_0";
					versionNode.Attributes.Append(attr);
					camNode.AppendChild(versionNode);

					XmlNode projectNode = doc.CreateNode(XmlNodeType.Element, "Project", "");
					versionNode.AppendChild(projectNode);

					XmlNode offerNode = doc.CreateNode(XmlNodeType.Element, "Offer", "");
					projectNode.AppendChild(offerNode);

					XmlNode topNode = doc.CreateNode(XmlNodeType.Element, "Top", "");
					offerNode.AppendChild(topNode);

					XmlNode shapesNode = doc.CreateNode(XmlNodeType.Element, "Shapes", "");
					topNode.AppendChild(shapesNode);

					XmlNode shapeNode = doc.CreateNode(XmlNodeType.Element, "Shape", "");
					shapesNode.AppendChild(shapeNode);

					XmlNode cutNode = doc.CreateNode(XmlNodeType.Element, "Cut_To_Size", "");
					attr = doc.CreateAttribute("Type");
					attr.Value = "1";
					cutNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Active");
					attr.Value = "true";
					cutNode.Attributes.Append(attr);
					shapeNode.AppendChild(cutNode);

					XmlNode originNode = doc.CreateNode(XmlNodeType.Element, "Origin", "");
					attr = doc.CreateAttribute("x");
					attr.Value = "0";
					originNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("y");
					attr.Value = "0";
					originNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("z");
					attr.Value = "0";
					originNode.Attributes.Append(attr);
					cutNode.AppendChild(originNode);

					XmlNode polygonNode = doc.CreateNode(XmlNodeType.Element, "Polygon", "");
					attr = doc.CreateAttribute("Id");
					attr.Value = "1";
					polygonNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("CW");
					attr.Value = "0";
					polygonNode.Attributes.Append(attr);
					cutNode.AppendChild(polygonNode);

					XmlNode lineNode = doc.CreateNode(XmlNodeType.Element, "Line", "");
					attr = doc.CreateAttribute("Id");
					attr.Value = "1";
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X0");
					attr.Value = rect.Left.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y0");
					attr.Value = rect.Top.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X1");
					attr.Value = rect.Right.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y1");
					attr.Value = rect.Top.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("ISJOIN");
					attr.Value = "false";
					lineNode.Attributes.Append(attr);
					polygonNode.AppendChild(lineNode);

					lineNode = doc.CreateNode(XmlNodeType.Element, "Line", "");
					attr = doc.CreateAttribute("Id");
					attr.Value = "2";
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X0");
					attr.Value = rect.Right.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y0");
					attr.Value = rect.Top.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X1");
					attr.Value = rect.Right.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y1");
					attr.Value = rect.Bottom.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("ISJOIN");
					attr.Value = "false";
					lineNode.Attributes.Append(attr);
					polygonNode.AppendChild(lineNode);

					lineNode = doc.CreateNode(XmlNodeType.Element, "Line", "");
					attr = doc.CreateAttribute("Id");
					attr.Value = "3";
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X0");
					attr.Value = rect.Right.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y0");
					attr.Value = rect.Bottom.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X1");
					attr.Value = rect.Left.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y1");
					attr.Value = rect.Bottom.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("ISJOIN");
					attr.Value = "false";
					lineNode.Attributes.Append(attr);
					polygonNode.AppendChild(lineNode);

					lineNode = doc.CreateNode(XmlNodeType.Element, "Line", "");
					attr = doc.CreateAttribute("Id");
					attr.Value = "4";
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X0");
					attr.Value = rect.Left.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y0");
					attr.Value = rect.Bottom.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("X1");
					attr.Value = rect.Left.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("Y1");
					attr.Value = rect.Top.ToString();
					lineNode.Attributes.Append(attr);
					attr = doc.CreateAttribute("ISJOIN");
					attr.Value = "false";
					lineNode.Attributes.Append(attr);
					polygonNode.AppendChild(lineNode);

					mod += 4;

					// Legge l'XML del Design
					if (designShape == null)
						designShape = new Breton.DesignUtil.Shape();

					if (!Breton.DesignUtil.XmlAnalizer.ReadShapeXml(ref doc, ref designShape, false))
					{
						TraceLog.WriteLine("Shape.ReadXmlPolygon Error, file XML not correct.");
						// Manca la parte del design
						return -1;
					}
				}

				XmlNode node = null;
				zone = new Zone();

				// Cerca il nodo della zona
				if ((node = doc.SelectSingleNode("//Zone")) != null)
				{
					// Legge la zona dal file XML
					if (!zone.ReadFileXml(node))
						return -1;
				}
				else
				{
					// Converte lo shape design in una zona
					if (!DesignShape2Zone(designShape, out zone))
						return -1;

					if (!AddXmlZone(zone, ref doc))
						return -1;

					mod += 1;
				}

				// Cerca il nodo della Construction
				if ((node = doc.SelectSingleNode("//Construction")) == null)
				{
					// Converte lo shape design in una zona
					if (!DesignShape2XmlConstruction(designShape, symbolsToImport, ref doc))
						return -1;

					mod += 2;
				}

				// Se sono state fatte delle modifiche, le salva nel file 
				if (mod != 0)
					doc.Save(fileXML);

				// Imposta la zona
				mZone = zone;

				// ok
				return mod;
			}

			catch (XmlException ex)
			{
				TraceLog.WriteLine("Shape.ReadXmlPolygon Error.", ex);
				return -1;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Shape.ReadXmlPolygon Error.", ex);
				return -1;
			}
			finally
			{
				doc = null;
			}
		}

		#endregion


		#region Private Methods
		
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

        ///*********************************************************************
        /// <summary>
        /// Legge il poligono che descrive il formato dal file
        /// </summary>
        /// <param name="fileXML">nome del file</param>
        /// <returns>zona che descrive il poligono</returns>
        ///*********************************************************************
        private Zone ReadXmlPolygon(string fileXML)
        {
            XmlDocument doc = new XmlDocument();
            Zone zone = null;

            try
            {
                // Legge il file XML
                doc.Load(fileXML);

                XmlNode zoneNode = null;

                // Cerca il nodo della zona
                if ((zoneNode = doc.SelectSingleNode("//Zone")) != null)
                {
                    // Legge la zona
                    zone = new Zone();
                    if (zone.ReadFileXml(zoneNode))
                        return zone;
                }
                
                // Se non trova il nodo della zona, cerca il nodo Shape
                else if ((zoneNode = doc.SelectSingleNode("//Shape")) != null)
                {
                }

                // non ha letto alcuna zona
                return null;
            }

            catch (XmlException ex)
            {
                TraceLog.WriteLine("Shape.ReadXmlPolygon Error.", ex);
                return null;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Shape.ReadXmlPolygon Error.", ex);
                return null;
            }
            finally
            {
                doc = null;
            }
        }

		///*********************************************************************
		/// <summary>
		/// Converte uno shape del Design in una Zona
		/// </summary>
		/// <param name="designShape">shape design da convertire</param>
		/// <param name="zone">zona restituita</param>
		/// <returns></returns>
		///*********************************************************************
		private bool DesignShape2Zone(Breton.DesignUtil.Shape designShape, out Zone zone)
		{
			Side s;
			zone = null;
			Path path = null;

			try
			{
				// Legge tutti i cuts
				ArrayList cuts = designShape.gCutToSize;
				for (int i = 0; i < cuts.Count; i++)
				{
					Breton.DesignUtil.Shape.Cut cut = (Breton.DesignUtil.Shape.Cut)cuts[i];
					// Verifica se il cut � attivo
					if (cut.active)
					{
						// Crea il percorso
						path = new Path();

						// Legge tutti i lati
						ArrayList sides = cut.sides;
						for (int j = 0; j < sides.Count; j++)
						{
							Breton.DesignUtil.Shape.Contour contour = (Breton.DesignUtil.Shape.Contour)sides[j];

							// Segmento
							if (contour.line)
								s = new Segment(contour.x0, contour.y0, contour.x1, contour.y1);

							// Arco
							else
							{
								double alpha = MathUtil.gdRadianti(contour.startAngle);
								double beta = MathUtil.gdRadianti(contour.endAngle) - alpha;

								if (contour.cw == 0)
									beta = (beta < 0d ? 2 * Math.PI + beta : beta);
								else
									beta = (beta > 0d ? beta - 2 * Math.PI : beta);

								s = new Arc(contour.xC, contour.yC, contour.r, alpha, beta);
							}
							path.AddSide(s);
						}
						// Legge solo il primo cut
						break;
					}
				}

				// Percorso non creato
				if (path == null)
					return false;

				// Crea la zona con il percorso esterno appena letto
				zone = new Zone(path);

				// Legge tutti gli holes
				ArrayList holes = designShape.gHolesToCut;
				for (int i = 0; i < holes.Count; i++)
				{
					Breton.DesignUtil.Shape.Holes hole = (Breton.DesignUtil.Shape.Holes)holes[i];
					// Verifica se il hole � attivo
					// (anche se non � attivo, lo inserisce comunque, perch� verr� tagliato su un altra macchina
					// per cui pu� essere posizionato sopra un difetto)
					if (hole.active || !hole.active)
					{
						// Crea il percorso
						path = new Path();

						// Legge tutti i lati del foro
						ArrayList sides = hole.sides;
						for (int j = 0; j < sides.Count; j++)
						{
							Breton.DesignUtil.Shape.Contour contour = (Breton.DesignUtil.Shape.Contour)sides[j];

							// Segmento
							if (contour.line)
								s = new Segment(contour.x0, contour.y0, contour.x1, contour.y1);

							// Arco
							else
							{
								double alpha = MathUtil.gdRadianti(contour.startAngle);
								double beta = MathUtil.gdRadianti(contour.endAngle) - alpha;

								if (contour.cw == 0)
									beta = (beta < 0d ? 2 * Math.PI + beta : beta);
								else
									beta = (beta > 0d ? beta - 2 * Math.PI : beta);

								s = new Arc(contour.xC, contour.yC, contour.r, alpha, beta);
							}
							path.AddSide(s);
						}

						// Aggiunge il foro alla zona
						zone.AddZone(new Zone(path));
					}
				}

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("Shape.DesignShape2Zone Error.", ex);
				return false;
			}

		}

		///*********************************************************************
		/// <summary>
		/// Converte uno shape del Design in un nodo XML Construction e lo 
		/// aggiunge al documento XML.
		/// </summary>
		/// <param name="zone">zona da aggiungere</param>
		/// <param name="symbolsToImport">Lista dei simboli Design da importare</param>
		/// <param name="doc">documento XML</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		private bool DesignShape2XmlConstruction(Breton.DesignUtil.Shape designShape, List<string> symbolsToImport, ref XmlDocument doc)
		{
			XmlAttribute attr;

			try
			{
				XmlNode exportNode = null, node;
				XmlText textNode;

				// Elimina il nodo della zona se esiste gi�
				if ((node = doc.SelectSingleNode("//Construction")) != null)
				{
					XmlNode parentNode = node.ParentNode;
					parentNode.RemoveChild(node);
				}

				if ((exportNode = doc.SelectSingleNode("/EXPORT")) == null)
					throw (new ApplicationException("Shape.DesignShape2XmlConstruction: EXPORT Node not found"));

				XmlNode constructionNode = doc.CreateNode(XmlNodeType.Element, "Construction", "");
				attr = doc.CreateAttribute("ConstructionName");
				attr.Value = "";
				constructionNode.Attributes.Append(attr);
				exportNode.AppendChild(constructionNode);

				XmlNode polygonNode = null;


				//--------------------------------------------------------------------------
				// Legge tutti i cuts
				//--------------------------------------------------------------------------
				ArrayList cuts = designShape.gCutToSize;
				for (int i = 0; i < cuts.Count; i++)
				{
					Breton.DesignUtil.Shape.Cut cut = (Breton.DesignUtil.Shape.Cut)cuts[i];
					// Verifica se il cut � attivo
					if (cut.active)
					{
						// Crea il nodo poligono
						polygonNode = doc.CreateNode(XmlNodeType.Element, "Polygon", "");

						// Legge tutti i lati
						ArrayList sides = cut.sides;
						for (int j = 0; j < sides.Count; j++)
						{
							Breton.DesignUtil.Shape.Contour contour = (Breton.DesignUtil.Shape.Contour)sides[j];

							XmlNode sideNode = doc.CreateNode(XmlNodeType.Element, "Side", "");

							// Segmento
							if (contour.line)
							{

								node = doc.CreateElement("X1");
								textNode = doc.CreateTextNode(contour.x0.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Y1");
								textNode = doc.CreateTextNode(contour.y0.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("X2");
								textNode = doc.CreateTextNode(contour.x1.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Y2");
								textNode = doc.CreateTextNode(contour.y1.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								if (contour.slope != 0)
								{
									node = doc.CreateElement("CutIncl");
									textNode = doc.CreateTextNode(contour.slope.ToString(CultureInfo.InvariantCulture));
									node.AppendChild(textNode);
									sideNode.AppendChild(node);
								}
							}
							// Arco
							else
							{
								double alpha = MathUtil.gdRadianti(contour.startAngle);
								double beta = MathUtil.gdRadianti(contour.endAngle) - alpha;

								if (contour.cw == 0)
									beta = (beta < 0d ? 2 * Math.PI + beta : beta);
								else
									beta = (beta > 0d ? beta - 2 * Math.PI : beta);

								sideNode = doc.CreateNode(XmlNodeType.Element, "Side", "");

								node = doc.CreateElement("Xc");
								textNode = doc.CreateTextNode(contour.xC.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Yc");
								textNode = doc.CreateTextNode(contour.yC.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Radius");
								textNode = doc.CreateTextNode(contour.r.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Alpha");
								textNode = doc.CreateTextNode(alpha.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Beta");
								textNode = doc.CreateTextNode(beta.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								if (contour.slope != 0)
								{
									node = doc.CreateElement("CutIncl");
									textNode = doc.CreateTextNode(contour.slope.ToString(CultureInfo.InvariantCulture));
									node.AppendChild(textNode);
									sideNode.AppendChild(node);
								}
							}

							polygonNode.AppendChild(sideNode);
						}
						// Legge solo il primo cut
						break;
					}
				}

				// Poligono non creato
				if (polygonNode == null)
					return false;

				// Aggiunge il poligono
				constructionNode.AppendChild(polygonNode);


				//--------------------------------------------------------------------------
				// Legge tutti gli holes
				//--------------------------------------------------------------------------
				ArrayList holes = designShape.gHolesToCut;
				for (int i = 0; i < holes.Count; i++)
				{
					Breton.DesignUtil.Shape.Holes hole = (Breton.DesignUtil.Shape.Holes)holes[i];
					// Verifica se il hole � attivo
					if (hole.active)
					{
						polygonNode = doc.CreateNode(XmlNodeType.Element, "Polygon", "");
					}
					else
					{
						polygonNode = doc.CreateNode(XmlNodeType.Element, "NoCutPolygon", "");
					}

					// Legge tutti i lati del foro
					ArrayList sides = hole.sides;
					for (int j = 0; j < sides.Count; j++)
					{
						Breton.DesignUtil.Shape.Contour contour = (Breton.DesignUtil.Shape.Contour)sides[j];

						XmlNode sideNode = doc.CreateNode(XmlNodeType.Element, "Side", "");

						// Segmento
						if (contour.line)
						{

							node = doc.CreateElement("X1");
							textNode = doc.CreateTextNode(contour.x0.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Y1");
							textNode = doc.CreateTextNode(contour.y0.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("X2");
							textNode = doc.CreateTextNode(contour.x1.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Y2");
							textNode = doc.CreateTextNode(contour.y1.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							// Foro con ponticelli
							if (hole.isBridge)
							{
								node = doc.CreateElement("BridgeCut");
								textNode = doc.CreateTextNode("1");
								node.AppendChild(textNode);
								sideNode.AppendChild(node);
							}

							if (contour.slope != 0)
							{
								node = doc.CreateElement("CutIncl");
								textNode = doc.CreateTextNode(contour.slope.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);
							}
						}
						// Arco
						else
						{
							double alpha = MathUtil.gdRadianti(contour.startAngle);
							double beta = MathUtil.gdRadianti(contour.endAngle) - alpha;

							if (contour.cw == 0)
								beta = (beta < 0d ? 2 * Math.PI + beta : beta);
							else
								beta = (beta > 0d ? beta - 2 * Math.PI : beta);

							sideNode = doc.CreateNode(XmlNodeType.Element, "Side", "");

							node = doc.CreateElement("Xc");
							textNode = doc.CreateTextNode(contour.xC.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Yc");
							textNode = doc.CreateTextNode(contour.yC.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Radius");
							textNode = doc.CreateTextNode(contour.r.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Alpha");
							textNode = doc.CreateTextNode(alpha.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Beta");
							textNode = doc.CreateTextNode(beta.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							// Foro con ponticelli
							if (hole.isBridge)
							{
								node = doc.CreateElement("BridgeCut");
								textNode = doc.CreateTextNode("1");
								node.AppendChild(textNode);
								sideNode.AppendChild(node);
							}

							if (contour.slope != 0)
							{
								node = doc.CreateElement("CutIncl");
								textNode = doc.CreateTextNode(contour.slope.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);
							}
						}

						polygonNode.AppendChild(sideNode);
					}

					// Aggiunge il poligono
					constructionNode.AppendChild(polygonNode);
				}


				//--------------------------------------------------------------------------
				// Legge tutti i rinforzi
				//--------------------------------------------------------------------------
				ArrayList reinforcements = designShape.gReinforcements;
				for (int i = 0; i < reinforcements.Count; i++)
				{
					Breton.DesignUtil.Shape.Reinforcement reinforcement = (Breton.DesignUtil.Shape.Reinforcement)reinforcements[i];
					polygonNode = doc.CreateNode(XmlNodeType.Element, "Reinforcement", "");

					// Profondit� rinforzo
					attr = doc.CreateAttribute("Depth");
					attr.Value = reinforcement.depth.ToString(CultureInfo.InvariantCulture);
					polygonNode.Attributes.Append(attr); ;

					// Verifica che il rinforzo abbia 4 lati
					if (reinforcement.sides.Count != 4)
						throw new ApplicationException("Reinforcement sides number is wrong.");

					// Legge tutti i lati del rinforzo
					ArrayList sides = reinforcement.sides;
					for (int j = 0; j < sides.Count; j++)
					{
						Breton.DesignUtil.Shape.Contour contour = (Breton.DesignUtil.Shape.Contour)sides[j];

						XmlNode sideNode = doc.CreateNode(XmlNodeType.Element, "Side", "");

						// Segmento
						if (contour.line)
						{
							node = doc.CreateElement("X1");
							textNode = doc.CreateTextNode(contour.x0.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Y1");
							textNode = doc.CreateTextNode(contour.y0.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("X2");
							textNode = doc.CreateTextNode(contour.x1.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);

							node = doc.CreateElement("Y2");
							textNode = doc.CreateTextNode(contour.y1.ToString(CultureInfo.InvariantCulture));
							node.AppendChild(textNode);
							sideNode.AppendChild(node);
						}
						// Arco
						else
						{
							throw new ApplicationException("Arc is not possible in reinforcement element.");
						}

						polygonNode.AppendChild(sideNode);
					}

					// Aggiunge il poligono
					constructionNode.AppendChild(polygonNode);
				}


				//--------------------------------------------------------------------------
				// Legge tutti i simboli da importare
				//--------------------------------------------------------------------------
				ArrayList symbols = designShape.gSymbols;
				for (int i = 0; i < symbols.Count; i++)
				{
					Breton.DesignUtil.Shape.Symbol symbol = (Breton.DesignUtil.Shape.Symbol)symbols[i];
					// Verifica se il simbolo � da importare
					if (symbolsToImport != null && symbol.type != null && symbolsToImport.Contains(symbol.type.ToUpper()))
					{
						// Crea il nodo poligono
						polygonNode = doc.CreateNode(XmlNodeType.Element, "NoCutPolygon", "");

						// Legge tutti i lati
						ArrayList sides = symbol.sides;
						for (int j = 0; j < sides.Count; j++)
						{
							Breton.DesignUtil.Shape.Contour contour = (Breton.DesignUtil.Shape.Contour)sides[j];

							XmlNode sideNode = doc.CreateNode(XmlNodeType.Element, "Side", "");

							// Segmento
							if (contour.line)
							{
								node = doc.CreateElement("X1");
								textNode = doc.CreateTextNode(contour.x0.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Y1");
								textNode = doc.CreateTextNode(contour.y0.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("X2");
								textNode = doc.CreateTextNode(contour.x1.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Y2");
								textNode = doc.CreateTextNode(contour.y1.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);
							}
							// Arco
							else
							{
								double alpha = MathUtil.gdRadianti(contour.startAngle);
								double beta = MathUtil.gdRadianti(contour.endAngle) - alpha;

								if (contour.cw == 0)
									beta = (beta < 0d ? 2 * Math.PI + beta : beta);
								else
									beta = (beta > 0d ? beta - 2 * Math.PI : beta);

								sideNode = doc.CreateNode(XmlNodeType.Element, "Side", "");

								node = doc.CreateElement("Xc");
								textNode = doc.CreateTextNode(contour.xC.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Yc");
								textNode = doc.CreateTextNode(contour.yC.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Radius");
								textNode = doc.CreateTextNode(contour.r.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Alpha");
								textNode = doc.CreateTextNode(alpha.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);

								node = doc.CreateElement("Beta");
								textNode = doc.CreateTextNode(beta.ToString(CultureInfo.InvariantCulture));
								node.AppendChild(textNode);
								sideNode.AppendChild(node);
							}

							polygonNode.AppendChild(sideNode);
						}

						// Aggiunge il poligono
						constructionNode.AppendChild(polygonNode);
					}
				}

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("Shape.DesignShape2XmlConstruction Error.", ex);
				return false;
			}

		}

		///*********************************************************************
		/// <summary>
		/// Aggiunge il nodo zona al documento XML
		/// </summary>
		/// <param name="zone">zona da aggiungere</param>
		/// <param name="doc">documento XML</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		private bool AddXmlZone(Zone zone, ref XmlDocument doc)
		{
			try
			{
				XmlNode node = null;

				// Elimina il nodo della zona se esiste gi�
				if ((node = doc.SelectSingleNode("//Zone")) != null)
					doc.RemoveChild(node);

				if ((node = doc.SelectSingleNode("/EXPORT")) == null)
					throw (new ApplicationException("Shape.AddXmlZone: EXPORT Node not found"));

				// Aggiunge la zona al documento ZML
				zone.WriteFileXml(node);

				// non ha letto alcuna zona
				return true;
			}

			catch (XmlException ex)
			{
				TraceLog.WriteLine("Shape.AddXmlZone Error.", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Shape.AddXmlZone Error.", ex);
				return false;
			}
		}

		#endregion


	}
}
