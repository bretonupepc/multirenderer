using System;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;
using Breton.Parameters;
using Breton.Optimizers;

namespace Breton.OptiMasterInterface
{
	public class OptParameterCollection: ParameterCollection
	{
		#region Variables

		private int mIdOp;				// id ordine di produzione

		#endregion

		#region Constructors & Disposers
		
		///*********************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="db"></param>
		/// <param name="idOp"></param>
		///*********************************************************************
		public OptParameterCollection(DbInterface db, int idOp): base(db, false)
		{
			mIdOp = idOp;
			mTable = "T_OP_PARAMS";
		}

		#endregion

		#region Properties

		public int gIdOp
		{
			set	{ mIdOp = value; }
			get	{ return mIdOp; }
		}

		#endregion

		#region Public Methods

        ///*********************************************************************
        /// <summary>
        /// Ritorna un parametro dell'ottimizzatore
        /// </summary>
        /// <param name="parName">nome del parametro</param>
        /// <param name="obj">parametro ritornato</param>
        /// <returns>
        ///     true    parametro ritornato con successo
        ///     false   parametro non trovato
        /// </returns>
        ///*********************************************************************
        public bool GetOptParameter(Optimizer.OptParamName parName, out Parameter obj)
        {
            Parameter par;
            obj = null;

            //Cerca il primo parametro che corrisponde al nome cercato
            MoveFirst();
            while (GetObject(out par))
            {
                if (par.gName.TrimEnd() == parName.ToString())
                {
                    obj = par;
                    break;
                }
                MoveNext();
            }

            //oggetto non trovato
            return obj != null;
        }


		/**********************************************************************
		 * GetDataFromDb
		 * Legge i dati dal database, ordinati per la chiave specificata
		 * Parametri:
		 *			station		: nome stazione, null nessuna, "" attuale, "*" tutti
		 *			application	: nome applicazione, null nessuna, "" attuale, "*" tutte
		 *			group		: nome gruppo, null nessuno, "*" tutti
		 *			name		: nome parametro, "*" tutti
		 * Ritorna:
		 * 			true	lettura eseguita
		 * 			false	lettura non eseguita
		 ***********************************************************************/
		public override bool GetDataFromDb(string station, string application, string group, string name)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE";

			if (mIdOp > 0)
			{
				sqlWhere += sqlAnd + " F_ID_OP = " + mIdOp;
				sqlAnd = " AND";
			}

			if (station != "*")
			{
				if (station == "")
					station = Environment.MachineName;

				if (station == null)
					sqlWhere += sqlAnd + " F_STATION IS NULL";
				else
					sqlWhere += sqlAnd + " F_STATION = '" + station + "'";

				sqlAnd = " AND";
			}
			mStation = station;

			if (application != "*")
			{
				if (application == "")
					application = System.Windows.Forms.Application.ProductName;

				if (application == null)
					sqlWhere += sqlAnd + " F_APPLICATION IS NULL";
				else
					sqlWhere += sqlAnd + " F_APPLICATION = '" + application + "'";
				
				sqlAnd = " AND";
			}
			mApplication = application;

			if (group != "*")
			{
				if (group == null)
					sqlWhere += sqlAnd + " F_GROUP IS NULL";
				else
					sqlWhere += sqlAnd + " F_GROUP = '" + group + "'";
				
				sqlAnd = " AND";
			}
			mGroup = group;

			if (name != "*")
			{
				sqlWhere += sqlAnd + " F_NAME = '" + name + "'";
				sqlAnd = " AND";
			}
			mName = name;

			sqlOrderBy = " ORDER BY F_STATION, F_APPLICATION, F_GROUP, F_NAME";

			bool ret = GetDataFromDb("SELECT * FROM " + mTable + sqlWhere + sqlOrderBy);

			// Dopo la lettura il recordset � disordinato
			mSorted = false;
			
			return ret;
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int retry = 0;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Parameter par in mRecordset)
					{
						switch (par.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								string station = (par.gStation == null ? "NULL" : "'" + par.gStation + "'");
								string application = (par.gApplication == null ? "NULL" : "'" + par.gApplication + "'");
								string group = (par.gGroup == null ? "NULL" : "'" + par.gGroup + "'");
								string name = "'" + par.gName + "'";
								string type = ((int) par.gType).ToString();
								string val = "'" + par.gStringValue + "'";
								string description = (par.gDescription == null ? "NULL" : "'" + par.gDescription + "'");

								sqlInsert = "INSERT INTO " + mTable +
									" (F_ID_OP, F_STATION, F_APPLICATION, F_GROUP, F_NAME, F_TYPE, F_VALUE, F_DESCRIPTION)" +
									" VALUES(" + mIdOp + "," + station + "," + application + "," + group + "," + name +
									"," + type + "," + val + "," + description + ")";
								if (!mDbInterface.Execute(sqlInsert))
									throw new ApplicationException("OptParameterCollection: Error during Db Insert operation");
							
								break;
						
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
								string sqlWhere = " WHERE F_ID_OP = " + mIdOp;
								sqlWhere += " AND F_STATION " + (par.gStation == null ? "IS NULL": "= '" + par.gStation + "'");
								sqlWhere += " AND F_APPLICATION " + (par.gApplication == null ? "IS NULL": "= '" + par.gApplication + "'");
								sqlWhere += " AND F_GROUP " + (par.gGroup == null ? "IS NULL": "= '" + par.gGroup + "'");
								sqlWhere += " AND F_NAME = '" + par.gName + "'";

								type = ((int) par.gType).ToString();
								val = "'" + par.gStringValue + "'";
								description = (par.gDescription == null ? "NULL" : "'" + par.gDescription + "'");

								sqlUpdate = "UPDATE " + mTable + " SET F_TYPE = " + type + 
									", F_VALUE = " + val + ", F_DESCRIPTION = " + description + sqlWhere;
								if (!mDbInterface.Execute(sqlUpdate))
									throw new ApplicationException("OptParameterCollection: Error during Db Update operation");
							
								break;
						}

					}

					//Scorre tutti i materiali cancellati
					foreach (Parameter par in mDeleted)
					{
						switch (par.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								string sqlWhere = " WHERE F_ID_OP = " + mIdOp;
								sqlWhere += " AND F_STATION " + (par.gStation == null ? "IS NULL": "= '" + par.gStation + "'");
								sqlWhere += " AND F_APPLICATION " + (par.gApplication == null ? "IS NULL": "= '" + par.gApplication + "'");
								sqlWhere += " AND F_GROUP " + (par.gGroup == null ? "IS NULL": "= '" + par.gGroup + "'");
								sqlWhere += " AND F_NAME = '" + par.gName + "'";

								sqlDelete = "DELETE FROM " + mTable + sqlWhere;
								if (!mDbInterface.Execute(sqlDelete))
									throw new ApplicationException("OptParameterCollection: Error during Db Delete operation");

								break;
						}
					}

					// Termina la transazione con successo
					mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

					//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OptParameterCollection: Deadlock Error", ex);

					// Annulla la transazione
					mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("OptParameterCollection: Exceed Deadlock retrying", ex);
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("OptParameterCollection: Error", ex);

					// Annulla la transazione
					mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion

		#region Private Methods

		#endregion
	}
}
