#define LOCK_TRANSACTION

using System;
using System.Threading;
using System.Data.OleDb;
using System.Collections;
using Breton.DbAccess;
using Breton.TraceLoggers;
using Breton.Slabs;
using Breton.MathUtils;
using Breton.Users;

namespace Breton.OptiMasterInterface
{
	public class SequenceCollection: DbCollection
	{
		#region Variables

		private User mUser;
		private int mLastId;
		#endregion


		#region Constructors

		public SequenceCollection(DbInterface db, User user): base(db)
		{
			mUser = user;
		}
		
		#endregion


		#region Propreties
		public int gLastId
		{
			set { mLastId = value; }
			get { return mLastId; }
		}
		#endregion


		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Sequence obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (Sequence) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Sequence obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Sequence) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Sequence obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		///**********************************************************************
		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id elemento</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(Sequence.Keys.F_NONE, id, Sequence.State.Undef);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Sequence.Keys.F_NONE, 0, Sequence.State.Undef);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Sequence.Keys orderKey)
		{
			return GetDataFromDb(orderKey, 0, Sequence.State.Undef);
		}

		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, con lo stato indicato
		/// </summary>
		/// <param name="state">stato della sequenza</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetDataFromDb(Sequence.State state)
		{
			return GetDataFromDb(Sequence.Keys.F_ID_PRO_SEQUENCE, 0, state);
		}

		public bool GetDataFromDb(Sequence.Keys orderKey, int id, Sequence.State state)
		{
			return GetDataFromDb(orderKey, id, state, null);
		}
		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="id">estrae solo l'elemento con l'id specificato</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(Sequence.Keys orderKey, int id, Sequence.State state, string[] orders)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo il materiale con il codice richiesto
			if (id > 0)
			{
				sqlWhere += sqlAnd + "seq.F_ID_PRO_SEQUENCE = " + id.ToString();
				sqlAnd = " and ";
			}

			if (state != Sequence.State.Undef)
			{
				sqlWhere += sqlAnd + "seq.F_STATE = " + ((int)state).ToString();
				sqlAnd = " and ";
			}

			//Estrae solo gli articoli richiesti
			if (orders != null && orders.Length > 0)
			{
				sqlWhere += sqlAnd + "T_ORDERS.F_ORDER_CODE in ('" + orders[0] + "'";

				for (int i = 1; i < orders.Length; i++)
					sqlWhere += ", '" + orders[i] + "'";
				sqlWhere += ")";

				sqlAnd = " and ";
			}

			if (mUser != null && !mUser.gIsAdministrator)
				sqlWhere += sqlAnd + " seq.F_ID_T_AN_USERS = " + mUser.gId.ToString();

			switch (orderKey)
			{
				case Sequence.Keys.F_ID_PRO_SEQUENCE:
					sqlOrderBy = " order by seq.F_ID_PRO_SEQUENCE";
					break;
				case Sequence.Keys.F_DESCRIPTION:
					sqlOrderBy = " order by seq.F_DESCRIPTION";
					break;
				case Sequence.Keys.F_MAT_CODE:
					sqlOrderBy = " order by m.F_CODE";
					break;
			}
			return GetDataFromDb("select distinct seq.*, m.F_CODE AS F_MAT_CODE from T_PRO_SEQUENCES seq" +
								" left outer join T_AN_MATERIALS m on m.F_ID = seq.F_ID_T_AN_MATERIALS" +
								" left outer join T_OP_ORDERS on seq.F_ID_PRO_SEQUENCE = T_OP_ORDERS.F_ID_PRO_SEQUENCE" +
								" left outer join T_ORDERS on T_OP_ORDERS.F_ID_ORDER = T_ORDERS.F_ID_ORDER "
								 + sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Sequence seq in mRecordset)
					{
						switch (seq.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_PRO_SEQUENCES (" +
									(seq.gMatCode != null && seq.gMatCode.Length > 0 ? "F_ID_T_AN_MATERIALS, " : "") +
									"F_STATE, " +
									"F_DT_START, " +
									"F_DT_STOP, " +
									"F_DESCRIPTION, " +
									"F_TYPE, " +
									"F_MODE" +
									")" +
									" (select " +
									(seq.gMatCode != null && seq.gMatCode.Length > 0 ? "F_ID, " : "") +
									((int)seq.gStateSeq) +
									", " + (seq.gStartDate == DateTime.MinValue ? "null" : mDbInterface.OleDbToDate(seq.gStartDate)) +
									", " + (seq.gStopDate == DateTime.MinValue ? "null" : mDbInterface.OleDbToDate(seq.gStopDate)) +
									", '" + seq.gDescription + "'" +
									", " + ((int)seq.gSeqType) +
									", " + ((int)seq.gMode) +
									(seq.gMatCode != null && seq.gMatCode.Length > 0 ? " from T_AN_MATERIALS where F_CODE = '" + seq.gMatCode + "'" : "") + ")" +
									";select scope_identity()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								seq.gId = id;
								mLastId = id;
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_PRO_SEQUENCES set" +
									" F_ID_T_AN_MATERIALS = (select F_ID from T_AN_MATERIALS where F_CODE = '" + seq.gMatCode + "')" +
									", F_STATE = " + ((int) seq.gStateSeq) +
									", F_DT_START = " + (seq.gStartDate == DateTime.MinValue ? "null" : mDbInterface.OleDbToDate(seq.gStartDate)) + 
									", F_DT_STOP = " + (seq.gStopDate == DateTime.MinValue ? "null" : mDbInterface.OleDbToDate(seq.gStopDate)) + 
									", F_DESCRIPTION = '" + seq.gDescription + "'" +
									", F_TYPE = " + ((int) seq.gSeqType) +
									", F_MODE = " + ((int) seq.gMode);

								sqlUpdate += " where F_ID_PRO_SEQUENCE = " + seq.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;
						
								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Sequence seq in mDeleted)
					{
						switch (seq.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_PRO_SEQUENCES where F_ID_PRO_SEQUENCE = " + seq.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("SequenceCollection: Error", ex);

					// Annulla la transazione
					if (transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Sospende le sequenze indicate
		/// </summary>
		/// <param name="ids">array delle ids delle sequenze da sospendere</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		public bool SuspendSequences(int[] ids)
		{
			string sqlCmd;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			// Verifica che ci siano delle sequenze da eseguire
			if (ids == null || ids.Length == 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					// Esegue la sospensione delle sole sequenze che non abbiano lastre in lista di estrazione
					sqlCmd = "update T_PRO_SEQUENCES set F_STATE = " + (int)Sequence.State.Suspended +
							 " from T_PRO_SEQUENCES seq" +
							 " where seq.F_STATE not in (" + (int)Sequence.State.Suspended +
							 ", " + (int)Sequence.State.Locked +
							 ")";

					sqlCmd += " and seq.F_ID_PRO_SEQUENCE in (" + ids[0];
					for (int i = 1; i < ids.Length; i++)
						sqlCmd += ", " + ids[i];
					sqlCmd += ")";

					if (!mDbInterface.Execute(sqlCmd))
						throw new ApplicationException("SequenceCollection.SuspendSequences: Execute error.");

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection.SuspendSequences: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("SequenceCollection.SuspendSequences: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Blocca le sequenze indicate
		/// </summary>
		/// <param name="ids">array delle ids delle sequenze da bloccare</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		public bool LockSequence(int[] ids)
		{
			int retry = 0;
#if LOCK_TRANSACTION
			bool transaction = !mDbInterface.TransactionActive();
#else
			bool transaction = false;
#endif
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			// Verifica che ci siano delle sequenze da eseguire
			if (ids == null || ids.Length == 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					for (int i = 0; i < ids.Length; i++)
					{
						// Salva i risultati attuali
						par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
						par.Direction = System.Data.ParameterDirection.ReturnValue;
						parameters.Add(par);

						par = new OleDbParameter("@sequence_id", OleDbType.Integer);
						par.Value = ids[i];
						parameters.Add(par);

						par = new OleDbParameter("@id_op", OleDbType.Integer);
						par.Value = 0;
						parameters.Add(par);

						// Lancia la stored procedure
						if (!mDbInterface.Execute("sp_PoLockSequence", ref parameters))
							throw new ApplicationException("SequenceCollection.LockSequence: Execute error.");

						// Verifica il codice di ritorno
						par = (OleDbParameter)parameters[0];
						if (((int)par.Value) != 0)
							throw new ApplicationException("SequenceCollection.LockSequence sp_PoConfirmSequence error: " + ((int)par.Value));
					}

					// Crea gli articoli per gli eventuali residui
					if (!CreateResidueArticles(0))
						throw new ApplicationException("SequenceCollection.LockSequence Error creating residue articles");

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection.LockSequence: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("SequenceCollection.LockSequence: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Sblocca le sequenze indicate
		/// </summary>
		/// <param name="ids">array delle ids delle sequenze da sbloccare</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		public bool UnlockSequence(int[] ids)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			// Verifica che ci siano delle sequenze da eseguire
			if (ids == null || ids.Length == 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					for (int i = 0; i < ids.Length; i++)
					{
						// Salva i risultati attuali
						par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
						par.Direction = System.Data.ParameterDirection.ReturnValue;
						parameters.Add(par);

						par = new OleDbParameter("@sequence_id", OleDbType.Integer);
						par.Value = ids[i];
						parameters.Add(par);

						par = new OleDbParameter("@id_op", OleDbType.Integer);
						par.Value = 0;
						parameters.Add(par);

						// Lancia la stored procedure
						if (!mDbInterface.Execute("sp_PoUnlockSequence", ref parameters))
							throw new ApplicationException("SequenceCollection.UnlockSequence: Execute error.");

						// Verifica il codice di ritorno
						par = (OleDbParameter)parameters[0];
						if (((int)par.Value) != 0)
							throw new ApplicationException("SequenceCollection.UnlockSequence sp_PoConfirmSequence error: " + ((int)par.Value));
					}

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection.UnlockSequence: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("SequenceCollection.UnlockSequence: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Crea gli articoli associati ai pezzi di recupero
		/// </summary>
		/// <param name="idSequence">id sequenza da sbloccare</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		///*********************************************************************
		public bool CreateResidueArticles(int idSequence)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbDataReader dr = null;
			SlabCollection slabs;
			PieceCollection pieces;
			ShapeCollection shapes;
			Slab slab;
			Piece piece;
			Shape shape;
			const string residueLabelFile = "\\ResidueLabelFile.xml";
			const double labelPosX = 20d, labelPosY = 50d;
			const double labelDimX = 90d, labelDimY = 40d;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Recupera tutti i recuperi per i quali non � stato ancora creato l'articolo 
					string szSQL = "select T_WK_RECOVERS.F_ID" +
									", T_WK_RECOVERS.F_ID_T_AN_ARTICLES_SRC" +
									", T_WK_RECOVERS.F_ID_T_PIECES" +
									", T_PIECES.F_ID_SHAPE" +
									" from T_WK_RECOVERS" +
									"	inner join T_AN_ARTICLES on T_WK_RECOVERS.F_ID_T_AN_ARTICLES_SRC = T_AN_ARTICLES.F_ID" +
									"	inner join T_PIECES on T_WK_RECOVERS.F_ID_T_PIECES = T_PIECES.F_ID_PIECE" +
									"	inner join T_OP_SLABS on T_AN_ARTICLES.F_ID = T_OP_SLABS.F_ID_T_AN_ARTICLES" +
									"	inner join T_OP on T_OP_SLABS.F_ID_OP = T_OP.F_ID_OP" +
									" where T_WK_RECOVERS.F_ID_T_AN_ARTICLES_DST is null";
					if (idSequence > 0)
						szSQL += " and T_OP.F_ID_PRO_SEQUENCE = " + idSequence.ToString();
					
					// Lancia la stored procedure
					if (!mDbInterface.Requery(szSQL, out dr))
						throw new ApplicationException("SequenceCollection.CreateResidueArticles: select error.");

					// Verifica se c'� qualcosa da fare
					if (!dr.HasRows)
					{
						mDbInterface.EndRequery(dr);
						return true;
					}

					ArrayList recoverIds = new ArrayList();
					ArrayList slabIds = new ArrayList();
					ArrayList pieceIds = new ArrayList();
					ArrayList shapeIds = new ArrayList();

					while (dr.Read())
					{
						recoverIds.Add(dr["F_ID"]);
						slabIds.Add(dr["F_ID_T_AN_ARTICLES_SRC"]);
						pieceIds.Add(dr["F_ID_T_PIECES"]);
						shapeIds.Add(dr["F_ID_SHAPE"]);
					}

					mDbInterface.EndRequery(dr);

					slabs = new SlabCollection(mDbInterface);
					pieces = new PieceOkCollection(mDbInterface);
					shapes = new ShapeCollection(mDbInterface);

					// Apre la transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Scorre tutti i residui da creare
					for (int i = 0; i < recoverIds.Count; i++)
					{
						slabs.Clear();
						pieces.Clear();
						shapes.Clear();

						// Legge la lastra
						if (!slabs.GetSingleElementFromDb((int) slabIds[i]))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: slab reading error.");
						if (!slabs.GetObject(out slab))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: slab reading error.");

						// Legge il file XML di definizione della lastra
						string tmpSlabFile = System.IO.Path.GetTempFileName();
						if (!slabs.GetXmlParameterFromDb(slab, tmpSlabFile))
							throw (new ApplicationException("SequenceCollection.CreateResidueArticles: error reading xml slab file"));

						// Legge il pezzo
						if (!pieces.GetSingleElementFromDb((int) pieceIds[i]))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: piece reading error.");
						if (!pieces.GetObject(out piece))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: piece reading error.");

						// Legge il formato
						if (!shapes.GetSingleElementFromDb((int)shapeIds[i]))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: shape reading error.");
						if (!shapes.GetObject(out shape))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: shape reading error.");

						// Legge il file XML di definizione del formato
						string tmpShapeFile = System.IO.Path.GetTempFileName();
						// Verifica se il formato contiene il file xml completo
						int mod = shape.CheckXmlFile(tmpShapeFile, null);
						if (mod == -1)
							throw (new ApplicationException("SequenceCollection.CreateResidueArticles: error creating xml polygon"));

						// Se sono state fatte delle modifiche, le salva nel db
						if (mod != 0)
							// Scrive il poligono
							if (!shapes.SetXmlPolygonToDb(shape, tmpShapeFile, false))
								throw (new ApplicationException("SequenceCollection.CreateResidueArticles: error writing xml polygon into db"));

						// Legge il rettangolo che definisce il residuo
						Polygons.Zone resZone = shape.gZone;
						if (resZone == null)
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: Zone piece is null.");
						resZone.SetRotoTransl(piece.gTraslX, piece.gTraslY, piece.gRotAngle);
						Rect resRect = resZone.GetRectangleRT();

						// Crea il residuo
						Residue residue = Residue.CreateResidueRect(slab, resRect, piece.gCode);
						if (residue == null)
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: Error creating residue.");

						// Salva il residuo creato nel database
						slabs.AddObject(residue);
						if (!slabs.UpdateDataToDb())
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: Error saving residue in database.");

						// Verifica che l'id assegnato sia valido
						if (residue.gId == 0)
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: Error, residue id is 0.");

						// Salva il file xml nel database
						if (!slabs.SetXmlParameterToDb(residue, residue.gXmlParameterLink))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: Error saving xml residue in database.");

						// Memorizza l'id dell'articolo creato nella tabella dei residui
						szSQL = "update T_WK_RECOVERS set F_ID_T_AN_ARTICLES_DST = " + residue.gId.ToString() +
								" where F_ID = " + ((int)recoverIds[i]).ToString();
						if (!mDbInterface.Execute(szSQL))
							throw new ApplicationException("SequenceCollection.CreateResidueArticles: Residue id updating error.");

						// Crea l'etichetta del residuo
						string tmpResidueLabel = "";
						string labelFile = System.Windows.Forms.Application.StartupPath + residueLabelFile;
						if (System.IO.File.Exists(labelFile))
						{
							double labelCenterX = Math.Max(labelDimX, labelDimY) / 2d + labelPosX;
							double labelCenterY = Math.Max(labelDimX, labelDimY) / 2d + labelPosY;

							tmpResidueLabel = Residue.CreateResidueLabel((int)recoverIds[i], residue, labelFile, mDbInterface, labelCenterX, labelCenterY);
							if (tmpResidueLabel == "")
								throw new ApplicationException("SequenceCollection.CreateResidueArticles: Error creating residue label.");
							if (!mDbInterface.WriteBlobField("T_WK_RECOVERS", "F_XML_LABEL_INFO", "", 
															" F_ID = " + ((int)recoverIds[i]).ToString(), tmpResidueLabel, ""))
								throw new ApplicationException("SequenceCollection.CreateResidueArticles: Error writing residue label into db.");
						}

						// Cancella i file temporanei
						GenUtils.DeleteFile(tmpShapeFile);
						GenUtils.DeleteFile(residue.gXmlParameterLink);
						GenUtils.DeleteFile(tmpSlabFile);
						GenUtils.DeleteFile(tmpResidueLabel);
					}

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection.CreateResidueArticles: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("SequenceCollection.CreateResidueArticles: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

				finally
				{
					mDbInterface.EndRequery(dr);
				}

			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Rimuove le lastre inutilizzate dalla sequenza indicata
		/// </summary>
		/// <param name="idSequence">id della sequenza</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		public bool RemoveUnusedSlabs(int idSequence)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			// Verifica che l'id sequenza sia valido
			if (idSequence <= 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					string szSQL = "delete T_OP_SLABS" +
									" from T_OP_SLABS" +
									"	inner join T_OP" +
									"		on T_OP_SLABS.F_ID_OP = T_OP.F_ID_OP" +
									" where T_OP.F_ID_PRO_SEQUENCE = " + idSequence.ToString() +
									" and T_OP_SLABS.F_STATE <> " + ((int)OpSlab.State.Confirmed).ToString();

					// Lancia la stored procedure
					if (!mDbInterface.Execute(szSQL))
						throw new ApplicationException("SequenceCollection.RemoveUnusedSlabs: Execute error.");

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection.RemoveUnusedSlabs: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("SequenceCollection.RemoveUnusedSlabs: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Verifica se la sequenza pu� essere cancellata
		/// </summary>
		/// <param name="idSequence">id della sequenza</param>
		/// <returns>
		///		true	la sequenza � cancellabile
		///		false	la sequenza non � cancellabile
		/// </returns>
		///*********************************************************************
		public bool IsErasable(int idSequence)
		{
			OleDbDataReader dr = null;

			// Verifica che l'id sequenza sia valido
			if (idSequence <= 0)
				return true;

			try
			{
				// Verifica se ci sono lastre bloccate
				string szSQL = "select count(*)" +
								" from T_OP_SLABS" +
								"	inner join T_OP" +
								"		on T_OP_SLABS.F_ID_OP = T_OP.F_ID_OP" +
								" where T_OP.F_ID_PRO_SEQUENCE = " + idSequence.ToString() +
								" and T_OP_SLABS.F_STATE = " + ((int)OpSlab.State.Confirmed).ToString();

				if (!mDbInterface.Requery(szSQL, out dr, true))
					throw new ApplicationException("SequenceCollection.IsErasable: Requery error.");

				// Verifica se ci sono lastre bloccate
				if (dr.Read())
					if (dr.GetInt32(0) == 0)
						return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("SequenceCollection.IsErasable: Error", ex);
				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Restituisce gli ordini gi� associati alla sequenza
		/// </summary>
		/// <param name="sequenceId">id della sequenza</param>
		/// <param name="idOrder">lista degli id degli ordini associati</param>
		/// <returns></returns>
		///*********************************************************************
		public bool GetSequenceOrders(int sequenceId, out int[] idOrder)
		{
			OleDbDataReader dr = null;
			idOrder = new int[0];
			int i = 0;

			// Verifica che l'id sequenza sia valido
			if (sequenceId <= 0)
				return false;

			try
			{
				// Verifica se ci sono lastre bloccate
				string szSQL = "select F_ID_ORDER " +
								"from T_OP_ORDERS " +
								" where F_ID_PRO_SEQUENCE = " + sequenceId.ToString();

				if (!mDbInterface.Requery(szSQL, out dr, true))
					throw new ApplicationException("SequenceCollection.IsErasable: Requery error.");

				// Verifica se ci sono lastre bloccate
				while (dr.Read())
				{
					int[] tmp = new int[idOrder.Length + 1];
					idOrder.CopyTo(tmp, 0);
					idOrder = tmp;
					idOrder[i] = dr.GetInt32(0);
					i++;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("SequenceCollection.GetSequenceOrders: Error", ex);
				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Add an order to sequence
		/// </summary>
		/// <param name="sequenceId">id sequence</param>
		/// <param name="idOrder">ids order array to add</param>
		/// <returns>true if succedeed
		///			 false otherwise</returns>
		///*********************************************************************
		public bool AddOrders(int sequenceId, int[] idOrder)
		{
			int retry = 0;
			string sqlCmd;
			bool transaction = !mDbInterface.TransactionActive();

			// Verifica che ci siano ordini da aggiungere
			if (idOrder.Length == 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					for (int i = 0; i < idOrder.Length; i++)
					{
						// Verifica che sia un id ordine valido
						if (idOrder[i] > 0)
						{
							// Inserisce un nuovo record nelle immagini
							sqlCmd = "insert into T_OP_ORDERS (F_ID_PRO_SEQUENCE, F_ID_ORDER) values(" + sequenceId.ToString() +
								", " + idOrder[i].ToString() + ")";
							if (!mDbInterface.Execute(sqlCmd))
								throw (new ApplicationException("SequenceCollection.AddOrders: error adding orders"));
						}
					}

					if (transaction) mDbInterface.EndTransaction(true);
					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection.AddOrders Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("SequenceCollection.AddOrders: Error", ex);
					return false;
				}
			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Remove and order from sequence
		/// </summary>
		/// <param name="sequenceId">id sequence</param>
		/// <param name="idOrder">ids order to remove</param>
		/// <param name="noRemovableOrders">ids order not removable</param>
		/// <returns>true if succedeed
		///			 false otherwise
		/// </returns>
		///*********************************************************************
		public bool RemoveOrders(int sequenceId, int[] idOrder, out int[] noRemovableOrders)
		{
			int retry = 0, count = 0;
			string sqlCmd, sep = "(";
			bool transaction = !mDbInterface.TransactionActive();
			ArrayList noRem = new ArrayList();

			// Verifica che ci siano ordini da eliminare
			if (idOrder.Length == 0)
			{
				noRemovableOrders = new int[0];
				return true;
			}

			// Crea la query
			sqlCmd = "delete from T_OP_ORDERS where F_ID_PRO_SEQUENCE = " + sequenceId.ToString() + " and F_ID_ORDER in ";
			for (int i = 0; i < idOrder.Length; i++)
			{
				// Verifica che sia un id ordine valido
				if (idOrder[i] > 0)
				{
					// Verifica se l'ordine � eliminabile
					if (IsOrderRemovable(sequenceId, idOrder[i]))
					{
						sqlCmd += sep + idOrder[i].ToString();
						sep = ", ";
						count++;
					}
					else
						noRem.Add(idOrder[i]);
				}
			}
			sqlCmd += ")";

			// Copia gli id degli ordini non cancellabili
			noRemovableOrders = (int[]) noRem.ToArray(typeof(int));

			// Verifica che sia stato passato almeno un id ordine da cancellare
			if (count == 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					if (!mDbInterface.Execute(sqlCmd))
						throw (new ApplicationException("SequenceCollection.AddOrders: error removing orders"));

					if (transaction) mDbInterface.EndTransaction(true);
					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("SequenceCollection.RemoveOrders Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("SequenceCollection.RemoveOrders: Error", ex);
					return false;
				}
			}

			return false;
		}

		/// ********************************************************************
		/// <summary>
		/// Richiesta di elaborazione lista di prelievo
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore
		/// </returns>
		/// ********************************************************************
		public bool ElabRequest()
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par = null;
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					par.Direction = System.Data.ParameterDirection.ReturnValue;
					parameters.Add(par);

					par = new OleDbParameter("@pc_name", OleDbType.VarChar, 18);
					par.Value = Environment.MachineName;
					parameters.Add(par);

					par = new OleDbParameter("@app_name", OleDbType.VarChar, 18);
					par.Value = System.Windows.Forms.Application.ProductName;
					parameters.Add(par);

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_schedule_elab_req", ref parameters))
						throw new ApplicationException(this.ToString() + ".ElabRequest sp_schedule_elab_req Execute error.");

					par = (OleDbParameter)parameters[0];
					if (((int)par.Value) != 0)
						throw new ApplicationException(this.ToString() + ".ElabRequest sp_schedule_elab_req error: " + ((int)par.Value));

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}

				// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine(this.ToString() + ".ElabRequest Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
					//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine(this.ToString() + ".ElabRequest Error.", ex);

					return false;
				}
			}

			return false;
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion


		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Sequence seq;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					seq = new Sequence((int) dr["F_ID_PRO_SEQUENCE"],
						(dr["F_ID_T_AN_MATERIALS"] == System.DBNull.Value ? 0 : (int)dr["F_ID_T_AN_MATERIALS"]),
						(dr["F_MAT_CODE"] == System.DBNull.Value ? "" : dr["F_MAT_CODE"].ToString()),
						(Sequence.State) dr["F_STATE"],
						(dr["F_DT_START"] == System.DBNull.Value ? DateTime.MinValue : (DateTime) dr["F_DT_START"]),
						(dr["F_DT_STOP"] == System.DBNull.Value ? DateTime.MinValue : (DateTime) dr["F_DT_STOP"]),
						(dr["F_DESCRIPTION"] == System.DBNull.Value ? "" : dr["F_DESCRIPTION"].ToString()),
						(Sequence.SeqType) dr["F_TYPE"],
						(Sequence.Mode) dr["F_MODE"]);
					
					AddObject(seq);
					seq.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("SequenceCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		///*********************************************************************
		/// <summary>
		/// Verifica se un ordine � removibile dalla sequenza
		/// </summary>
		/// <param name="sequenceId">id sequenza</param>
		/// <param name="idOrder">id ordine</param>
		/// <returns>
		///		true	l'ordine � removibile
		///		false	l'ordine non � removibile
		/// </returns>
		///*********************************************************************
		private bool IsOrderRemovable(int sequenceId, int idOrder)
		{
			OleDbDataReader dr = null;

			// Verifica se ci sono pezzi dell'ordine su lastre gi� confermate
			string sqlCmd = "select count(*) from T_OP_SLABS" +
							"	inner join T_OP" +
							"		on T_OP.F_ID_OP = T_OP_SLABS.F_ID_OP" +
							"	inner join T_OP_GROUPS" +
							"		on T_OP_SLABS.F_ID_OP_SLAB = T_OP_GROUPS.F_ID_OP_SLAB" +
							"	inner join T_OP_PIECES" +
							"		on T_OP_GROUPS.F_ID_OP_GROUP = T_OP_PIECES.F_ID_OP_GROUP" +
							"	inner join T_OP_SHAPES" +
							"		on T_OP_PIECES.F_ID_OP_SHAPE = T_OP_SHAPES.F_ID_OP_SHAPE" +
							"	inner join T_SHAPES" +
							"		on T_OP_SHAPES.F_ID_SHAPE = T_SHAPES.F_ID_SHAPE" +
							"	inner join T_OP_ORDERS" +
							"		on T_SHAPES.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER" +
							" where T_OP_ORDERS.F_ID_PRO_SEQUENCE = T_OP.F_ID_PRO_SEQUENCE" +
							" and T_OP_SLABS.F_STATE = " + ((int)OpSlab.State.Confirmed).ToString() +
							" and T_OP.F_ID_PRO_SEQUENCE = " + sequenceId.ToString() +
							" and T_OP_ORDERS.F_ID_ORDER = " + idOrder.ToString();

			try
			{
				if (!mDbInterface.Requery(sqlCmd, out dr))
					return false;

				// Se non ci sono lastre confermate, l'ordine pu� essere eliminato
				if (dr.Read())
					if (dr.GetInt32(0) == 0)
						return true;

				// Altrimenti l'ordine non � cancellabile
				return false;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("SequenceCollection.IsOrderRemovable: Error", ex);
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			return false;
		}

		#endregion
	}
}
