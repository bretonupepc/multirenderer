using System;
using System.Xml;
using Breton.DbAccess;
using Breton.Polygons;
using Breton.MathUtils;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa un gruppo (filagna) delle lastre, tipo FK
	/// </summary>
	///*************************************************************************
	public class GroupOk: Group
	{
		#region Structs, Enums & Variables

		#endregion


		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		///**********************************************************************
		public GroupOk(int id, int idSlab, GroupType type, double posX, double posY, double length, double width,
			double residue1, double residue2, State groupState, LinkIndex link, double offsetAlign, string code)
			:base(0, id, idSlab, type, posX, posY, length, width, residue1, residue2, groupState, link, offsetAlign, code)
		{ }

		public GroupOk():this(0, 0, GroupType.Undef, 0d, 0d, 0d, 0d, 0d, 0d, State.Undef, LinkIndex.Single, 0d, "")
		{
		}

		#endregion


		#region Properties

		#endregion


		#region IComparable interface

		#endregion


		#region Public Methods

		///**********************************************************************
		/// <summary>
		/// Legge i pezzi associati al gruppo dal database
		/// </summary>
		/// <param name="db">db interface</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///**********************************************************************
		public override bool GetPiecesFromDb(DbInterface db)
		{
			// Crea la lista dei pezzi
			if (mPieces == null)
				mPieces = new PieceOkCollection(db);

			return base.GetPiecesFromDb(db);
		}

		#endregion


		#region Private & Protected Methods

		#endregion

	}
}
