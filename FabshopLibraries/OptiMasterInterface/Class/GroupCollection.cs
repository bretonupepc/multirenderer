using System;
using System.Threading;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public abstract class GroupCollection: DbCollection
	{
		#region Variables

		#endregion


		#region Constructors

		public GroupCollection(DbInterface db): base(db)
		{
		}
		
		#endregion


		#region Public Methods

		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Group obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (Group) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Group obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Group) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Group obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		///**********************************************************************
		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id elemento</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			int[] ids = new int[1];
			ids[0] = id;
			return GetDataFromDb(Group.Keys.F_NONE, ids, null, null);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Group.Keys.F_NONE, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Group.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null);
		}

		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, della lastra indicata
		/// </summary>
		/// <param name="groupId">vettore id gruppi</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetSlabGroupsFromDb(int slabId)
		{
			int[] ids = new int[1];
			ids[0] = slabId;
			return GetDataFromDb(Group.Keys.F_ID_GROUP, null, ids, null);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="ids">estrae gli elemento con gli id indicati</param>
		/// <param name="slabIds">estrae gli elementi delle lastre indicate</param>
		/// <param name="opIds">estrae gli elementi degli ordini di produzione indicati</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public abstract bool GetDataFromDb(Group.Keys orderKey, int[] ids, int[] slabIds, int[] opIds);

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion


		#region Private Methods
		
		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, secondo la query specificata
		/// </summary>
		/// <param name="sqlQuery">query</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	errore
		///	</returns>
		///*********************************************************************
		protected abstract bool GetDataFromDb(string sqlQuery);

		#endregion
	}
}
