using System;
using System.Threading;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using System.Collections;
using Breton.DbAccess;
using Breton.TraceLoggers;
using Breton.Optimizers;
using Breton.Slabs;
using Breton.Parameters;
using Breton.Polygons;
using Breton.Users;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa il preottimizzatore
	/// </summary>
	///*************************************************************************
	public class PreOptimizer: IDisposable
	{
		#region Structs, Enums & Variables

		// Codici di ritorno del preottimizzatore
		public enum ReturnedCode
		{
			Error = -1,
			Ok = 0,
			NoSolutions,
			Started,
			Busy,
			NewSolution,
			Terminated,
			Suspended,
			SlabEmpty
		}

		private DbInterface mDbInterface;		// Interfaccia al database

		private SequenceCollection mSequences;
		private ProdOrderCollection mProdOrders;
		private ShapeCollection mShapes;
		private OptParameterCollection mParameters;
        private SlabGroupCollection mSlabGroups;
        private OpSlabCollection mOpSlabs;
        private OpSlab mOpSlab;
        private SlabCollection mSlabs;
        private Slab mSlab;
		private User mUser;

		// Migliore ottimizzazione trovata
		private double mBestSlabSurface;
		private double mBestOptSurface;
		private int mBestSlabId;

        private Optimizer mOptimizer;

        private string mTmpDirPath = System.IO.Path.GetTempPath() + "Preoptimizer";
        private DirectoryInfo mTmpDir;

		private DateTime mStartOpt;				// data/ora di inizio preottimizzazione
		private bool mRunning;					// preottimizzazione in corso

        protected bool mDisposed = false;

		#endregion

		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		/// <param name="db">Interfaccia al database</param>
		///**********************************************************************
		public PreOptimizer(DbInterface db, User user)
		{
			mUser = user;
			mDbInterface = db;
			mSequences = new SequenceCollection(mDbInterface, mUser);
			mProdOrders = new ProdOrderCollection(mDbInterface);
			mShapes = new ShapeCollection(mDbInterface);
			mParameters = new OptParameterCollection(mDbInterface, 0);
            mSlabGroups = new SlabGroupCollection(mDbInterface);
            mOpSlabs = new OpSlabCollection(mDbInterface);
            mSlabs = new SlabCollection(mDbInterface);
            mOptimizer = null;

            mTmpDir = new DirectoryInfo(mTmpDirPath);
            mTmpDir.Create();
            mTmpDir.Attributes = FileAttributes.Hidden;
        }

        #region IDisposable Members

        ///*************************************************************************
        /// <summary>
        /// Distruttore chiamato dall'esterno
        /// </summary>
        ///*************************************************************************
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ///*************************************************************************
        /// <summary>
        /// Distruttore interno effettivo
        /// </summary>
        /// <param name="disposing">true = chiamata esplicita / false = Garbage Collector</param>
        ///*************************************************************************
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.mDisposed)
            {
				if (mOptimizer != null)
				{
					mOptimizer.StopOptimizer();
					mOptimizer.Dispose();
				}
                // Cancella la directory contenente i file temporanei
                if (mTmpDir.Exists)
                    mTmpDir.Delete(true);

                mDisposed = true;
            }
        }

        #endregion

        #endregion

		#region Properties

		///*************************************************************************
		/// <summary>
		/// Ritorna se la preottimizzazione � in corso
		/// </summary>
		///*************************************************************************
		public bool IsRunning
		{
			get { return mRunning;  }
		}

		///*************************************************************************
		/// <summary>
		/// Ritorna da quanto tempo il preottimizzatore sta eseguendo il calcoli
		/// (Tempo di calcolo in secondi)
		/// </summary>
		///*************************************************************************
		public int ComputingTime
		{
			get
			{
				//Verifica se l'ottimizzatore � in funzione
				if (!IsRunning)
					return 0;

				//Tempo di elaborazione
				return (int)((DateTime.Now.Ticks - mStartOpt.Ticks) / 10000000);
			}
		}

		///*************************************************************************
		/// <summary>
		/// Ritorna il tempo di calcolo dell'ottimizzazione attuale
		/// </summary>
		///*************************************************************************
		public int OptimizerTime
		{
			get 
			{
				if (mOptimizer != null)
					return mOptimizer.ComputingTime;
				return 0;
			}
		}

		///*************************************************************************
		/// <summary>
		/// Ritorna il timeout impostato per l'ottimizzazione attuale
		/// </summary>
		///*************************************************************************
		public int OptimizerTimeout
		{
			get 
			{
				if (mOptimizer != null)
					return mOptimizer.Timeout;
				return 0;
			}
		}
		#endregion

		#region Public Methods

		///**********************************************************************
		/// <summary>
		/// Ritorna il numero totale di pezzi da realizzare e quelli realizzati.
		/// </summary>
		/// <param name="allSeq">
		///		true = valuta tutte le sequenze confermate, in esecuzione, completate
		///				e sospese
		///		false = valuta solo le sequence in esecuzione
		/// </param>
		/// <returns>
		///		>= 0	numero di pezzi totali da realizzare.
		///		-1		errore
		///	</returns>
		///**********************************************************************
		public int Quantity(bool allSeq)
		{
			return Quantity(allSeq, -1);
		}

		///**********************************************************************
		/// <summary>
		/// Ritorna il numero totale di pezzi da realizzare e quelli realizzati.
		/// </summary>
		/// <param name="idSeq">
		///		indica l'id della sequenza interessata
		/// </param>
		/// <returns>
		///		>= 0	numero di pezzi totali da realizzare.
		///		-1		errore
		///	</returns>
		///**********************************************************************
		public int Quantity(int idSeq)
		{
			return Quantity(false, idSeq);
		}

		private int Quantity(bool allSeq, int idSeq)
		{
			OleDbDataReader dr = null;
			string sqlQuery = "select isnull(sum(T_SHAPES.F_INITIAL_QUANTITY), 0) as REQ_QTA from T_OP_SHAPES" +
							  "		inner join T_SHAPES" +
							  "			on T_SHAPES.F_ID_SHAPE = T_OP_SHAPES.F_ID_SHAPE" +
							  "		inner join T_ORDERS" +
							  "			on T_ORDERS.F_ID_ORDER = T_SHAPES.F_ID_ORDER" +
							  "		inner join T_OP" +
							  "			on T_OP.F_ID_OP = T_OP_SHAPES.F_ID_OP" +
							  "		inner join T_PRO_SEQUENCES" +
							  "			on T_OP.F_ID_PRO_SEQUENCE = T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE" +
							  " where T_ORDERS.F_SEQUENCE >= 0";

			if (idSeq > 0)
				sqlQuery += " and T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE = " + idSeq.ToString();
			else
			{
				sqlQuery += " and T_OP.F_STATE in (";

				if (allSeq)
					sqlQuery += ((int)ProdOrder.State.Busy).ToString() +
						", " + ((int)ProdOrder.State.Suspended).ToString() +
						", " + ((int)ProdOrder.State.Confirmed).ToString() +
						", " + ((int)ProdOrder.State.Completed).ToString() +
						")";
				else
					sqlQuery += ((int)ProdOrder.State.Busy).ToString() +
						", " + ((int)ProdOrder.State.Suspended).ToString() +
						")";
			}

			if (mUser != null && !mUser.gIsAdministrator)
				sqlQuery += " and T_PRO_SEQUENCES.F_ID_T_AN_USERS = " + mUser.gId.ToString();

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return -1;

				// Ritorna la quantit� di formati
				if (dr.Read())
					return (int) dr[0];
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("PreOptimizer.Quantity: Error", ex);
				return -1;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			return -1;
		}

		///**********************************************************************
		/// <summary>
		/// Ritorna il numero totale di pezzi residui.
		/// </summary>
		/// <param name="allSeq">
		///		true = valuta tutte le sequenze confermate, in esecuzione, completate
		///				e sospese
		///		false = valuta solo le sequence in esecuzione
		/// </param>
		/// <returns>
		///		>= 0	numero di pezzi totali residui.
		///		-1		errore
		///	</returns>
		///**********************************************************************
		public int AvailableQuantity(bool allSeq)
		{
			return AvailableQuantity(allSeq, -1);
		}

		///**********************************************************************
		/// <summary>
		/// Ritorna il numero totale di pezzi residui.
		/// </summary>
		/// <param name="idSeq">
		///		indica l'id della sequenza interessata
		/// </param>
		/// <returns>
		///		>= 0	numero di pezzi totali residui.
		///		-1		errore
		///	</returns>
		///**********************************************************************
		public int AvailableQuantity(int idSeq)
		{
			return AvailableQuantity(false, idSeq);
		}

		private int AvailableQuantity(bool allSeq, int idSeq)
		{
			OleDbDataReader dr = null;

			string sqlQuery = "select isnull(sum(T_OP_SHAPES.F_AVAILABLE_QUANTITY), 0) as REQ_QTA from T_OP_SHAPES" +
							  "		inner join T_SHAPES" +
							  "			on T_SHAPES.F_ID_SHAPE = T_OP_SHAPES.F_ID_SHAPE" +
							  "		inner join T_ORDERS" +
							  "			on T_ORDERS.F_ID_ORDER = T_SHAPES.F_ID_ORDER" +
							  "		inner join T_OP" +
							  "			on T_OP.F_ID_OP = T_OP_SHAPES.F_ID_OP" +
							  "		inner join T_PRO_SEQUENCES" +
							  "			on T_OP.F_ID_PRO_SEQUENCE = T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE" +
							  " where T_ORDERS.F_SEQUENCE >= 0";

			if (idSeq > 0)
				sqlQuery += " and T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE = " + idSeq.ToString();
			else
			{
				sqlQuery += " and T_OP.F_STATE in (";

				if (allSeq)
					sqlQuery += ((int)ProdOrder.State.Busy).ToString() +
						", " + ((int)ProdOrder.State.Suspended).ToString() +
						", " + ((int)ProdOrder.State.Confirmed).ToString() +
						", " + ((int)ProdOrder.State.Completed).ToString() +
						")";
				else
					sqlQuery += ((int)ProdOrder.State.Busy).ToString() +
						", " + ((int)ProdOrder.State.Suspended).ToString() +
						")";
			}

			if (mUser != null && !mUser.gIsAdministrator)
				sqlQuery += " and T_PRO_SEQUENCES.F_ID_T_AN_USERS = " + mUser.gId.ToString();

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return -1;

				// Ritorna la quantit� di formati
				if (dr.Read())
					return (int) dr[0];
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("PreOptimizer.AvailableQuantity: Error", ex);
				return -1;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			return -1;
		}

        /// ********************************************************************
        /// <summary>
        /// Ritorna il tipo-sequenza relativo alla sequenza attiva
        /// </summary>
        /// <returns>modo sequenza</returns>
        /// ********************************************************************
        public Sequence.SeqType GetSequenceType()
        {
  			// Legge la sequenza in esecuzione
            Sequence seq;
            if (!mSequences.GetObject(out seq))
                return Sequence.SeqType.Undef;

            return seq.gSeqType;
        }

        /// ********************************************************************
        /// <summary>
        /// Ritorna il modo-sequenza relativo alla sequenza attiva
        /// </summary>
        /// <returns>modo sequenza</returns>
        /// ********************************************************************
        public Sequence.Mode GetSequenceMode()
        {
  			// Legge la sequenza in esecuzione
            Sequence seq;
            if (!mSequences.GetObject(out seq))
                return Sequence.Mode.Undef;

            return seq.gMode;
        }

        ///*********************************************************************
        /// <summary>
        /// Rimuove le lastre virtuali create
        /// </summary>
        /// <returns>
        ///     true    operazione eseguita con successo
        ///     false   errore nell'operazione
        /// </returns>
        ///*********************************************************************
		public bool RemoveUnNeededSlabs()
		{
			return RemoveUnNeededSlabs(-1);
		}
		
		public bool RemoveUnNeededSlabs(int idSeq)	
        {
            int retry = 0;
            bool transaction = true; // !mDbInterface.TransactionActive();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre la transazione
                    if (transaction) mDbInterface.BeginTransaction();

                    // Imposta le sequenze in esecuzione come confermate
					string sqlQuery = "delete T_OP_SLABS from T_OP_SLABS" +
						" inner join T_OP on T_OP.F_ID_OP = T_OP_SLABS.F_ID_OP" +
						" inner join T_PRO_SEQUENCES on T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE = T_OP.F_ID_PRO_SEQUENCE" +
						" where T_OP_SLABS.F_STATE = " + ((int)OpSlab.State.Virtual_Available).ToString() +
						" and T_PRO_SEQUENCES.F_STATE = " + ((int)Sequence.State.Completed).ToString();
					if (mUser != null && !mUser.gIsAdministrator)
						sqlQuery += " and T_PRO_SEQUENCES.F_ID_T_AN_USERS = " + mUser.gId.ToString();

					if (idSeq > 0) sqlQuery += " and T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE = " + idSeq.ToString();

                    if (!mDbInterface.Execute(sqlQuery))
                        throw (new ApplicationException("PreOptimizer.RemoveUnNeededSlabs: Error deleting unneeded slabs"));

                    // Chiude la transazione
                    if (transaction) mDbInterface.EndTransaction(true);

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("PreOptimizer.RemoveUnNeededSlabs Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("PreOptimizer.RemoveUnNeededSlabs: Error", ex);
                    return false;
                }
            }

            return false;
        }

        /// ********************************************************************
        /// <summary>
        /// Avvia il processo di preottimizzazione
        /// </summary>
        /// <returns>
        ///     true    operazione eseguita con successo
        ///     false   errore nell'operazione
        /// </returns>
        /// ********************************************************************
        public bool StartPreOpt()
        {
			return StartPreOpt(-1);
		}

		public bool StartPreOpt(int IdSeq)
		{
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre la transazione
                    if (transaction) mDbInterface.BeginTransaction();

                    // Imposta le sequenze in esecuzione come confermate
					string sqlQuery = "update T_PRO_SEQUENCES" +
						" set F_STATE = " + ((int)Sequence.State.Busy).ToString() +
						", F_DT_START = (getdate()) " +
						" where (" +
						"	F_STATE = " + ((int)Sequence.State.Confirmed).ToString() +
						"	or F_STATE = " + ((int)Sequence.State.Suspended).ToString() + ")" +
						(IdSeq <= 0 ? "" : " and F_ID_PRO_SEQUENCE = '" + IdSeq.ToString() + "' ");

					if (mUser != null && !mUser.gIsAdministrator)
						sqlQuery += " and F_ID_T_AN_USERS = " + mUser.gId.ToString();

                    if (!mDbInterface.Execute(sqlQuery))
                        throw (new ApplicationException("PreOptimizer.StartPreOpt: Error changing T_PRO_SEQUENCES states"));

					// Esegue un aggiornamento fittizio degli shape in modo da impostare lo stato completato negli OP che gi� lo sono
					sqlQuery = "update T_OP_SHAPES" +
						" set F_AVAILABLE_QUANTITY = F_AVAILABLE_QUANTITY" +
						" from T_OP_SHAPES" +
						"	inner join T_OP on T_OP_SHAPES.F_ID_OP = T_OP.F_ID_OP" +
						"	inner join T_PRO_SEQUENCES on T_OP.F_ID_PRO_SEQUENCE = T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE" +
						" where (" +
						"	T_OP.F_STATE = " + ((int)Sequence.State.Busy).ToString() +
						")";
					if (mUser != null && !mUser.gIsAdministrator)
						sqlQuery += " and T_PRO_SEQUENCES.F_ID_T_AN_USERS = " + mUser.gId.ToString();

					if (!mDbInterface.Execute(sqlQuery))
						throw (new ApplicationException("PreOptimizer.StartPreOpt: Error changing T_PRO_SEQUENCES states"));

					// Chiude la transazione
                    if (transaction) mDbInterface.EndTransaction(true);

					mRunning = true;
					mStartOpt = DateTime.Now;

					return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("PreOptimizer.StartPreOpt Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("PreOptimizer.StartPreOpt: Error", ex);
                    return false;
                }
            }

            return false;
        }

        /// ********************************************************************
        /// <summary>
        /// Arresta il processo di preottimizzazione
        /// </summary>
        /// <returns>
        ///     true    operazione eseguita con successo
        ///     false   errore nell'operazione
        /// </returns>
        /// ********************************************************************
		public bool StopPreOpt()
		{
			return StopPreOpt(-1);
		}

		public bool StopPreOpt(int IdSeq)
        {
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre la transazione
                    if (transaction) mDbInterface.BeginTransaction();

					// Imposta le sequenze come completate
					string sqlQuery = "update T_PRO_SEQUENCES" +
						" set F_STATE = " + ((int)Sequence.State.Completed).ToString() +
						"	, F_DT_STOP = (getdate()) " +
						" where F_STATE = " + ((int)Sequence.State.Busy).ToString() +
						" and (select count(*) from T_OP " +
						"		where T_OP.F_STATE not in	(" + ((int)ProdOrder.State.Completed).ToString() +
						"									, " + ((int)ProdOrder.State.Locked).ToString() +
						"									)" +
						"		and T_OP.F_ID_PRO_SEQUENCE = T_PRO_SEQUENCES.F_ID_PRO_SEQUENCE) = 0 " +
						(IdSeq <= 0 ? "" : " and F_ID_PRO_SEQUENCE = '" + IdSeq.ToString() + "' ");

					if (mUser != null && !mUser.gIsAdministrator)
						sqlQuery += " and T_PRO_SEQUENCES.F_ID_T_AN_USERS = " + mUser.gId.ToString();


					if (!mDbInterface.Execute(sqlQuery))
						throw (new ApplicationException("PreOptimizer.StopPreOpt: Error changing T_PRO_SEQUENCES states"));


                    // Imposta le sequenze in esecuzione come sospese
                    sqlQuery = "update T_PRO_SEQUENCES" +
                        " set F_STATE = " + ((int)Sequence.State.Suspended).ToString() +
                        ", F_DT_STOP = (getdate()) " +
                        " where F_STATE = " + ((int)Sequence.State.Busy).ToString() +
						(IdSeq <= 0 ? "" : " and F_ID_PRO_SEQUENCE = '" + IdSeq.ToString() + "' ");

					if (mUser != null && !mUser.gIsAdministrator)
						sqlQuery += " and T_PRO_SEQUENCES.F_ID_T_AN_USERS = " + mUser.gId.ToString();


                    if (!mDbInterface.Execute(sqlQuery))
                        throw (new ApplicationException("PreOptimizer.StopPreOpt: Error changing T_PRO_SEQUENCES states"));

                    // Chiude la transazione
                    if (transaction) mDbInterface.EndTransaction(true);

					mRunning = false;

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("PreOptimizer.StopPreOpt Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("PreOptimizer.StopPreOpt: Error", ex);
                    return false;
                }
            }

            return false;
        }

        /// ********************************************************************
        /// <summary>
        /// Avvia l'ottimizzazione
        /// </summary>
        /// <returns>codice di ritorno del processo</returns>
        /// ********************************************************************
		public ReturnedCode Start()
		{
			return Start(-1);
		}

		public ReturnedCode Start(int idSeq)
        {
			ReturnedCode err;

            while (true)
            {
                // Verifica se deve ricaricare tutto
                if (mSlabGroups.IsEOF())
                {
                    // Ricarica tutto dal database
                    ReturnedCode rc = RecoverAvailableShapes(idSeq);
                    if (rc != ReturnedCode.Ok)
                    {
                        // Dati non caricati, esce
                        OnStandBy();
                        return rc;
                    }
                }
				
                // Recupera le lastre
                if (!RecoverAvailableSlabs(idSeq))
                {
                    OnStandBy();
					return ReturnedCode.Error;
                }

                // Se ha trovato delle lastre, esce dal loop
                if (AnyValidSlab())
                    break;


                // Se si tratta di ottimizzazione con lastre tipo, le crea
                Sequence.Mode seqMode = GetSequenceMode();
                if (seqMode == Sequence.Mode.GuidedRefSlab
                    || seqMode == Sequence.Mode.OptimalRefSlab
                    || seqMode == Sequence.Mode.SimpleRefSlab)
                {
                    // Crea delle lastre virtuali
                    if (CreateVirtualSlabs())
                    {
                        // Recupera le lastre
                        if (!RecoverAvailableSlabs())
                        {
                            OnStandBy();
                            return ReturnedCode.Error;
                        }
                        // Se ha trovato delle lastre, esce dal loop
                        if (AnyValidSlab())
                            break;

                        OnStandBy();
                        return ReturnedCode.Error;
                    }
                    OnStandBy();
                    return ReturnedCode.Error;
                }

                // Altrimenti per le ottimizzazioni con lastre reali
                else 
                {
                    // passa al gruppo successivo
                    mSlabGroups.MoveNext();
                    // Finiti i gruppi di lastre
                    if (mSlabGroups.IsEOF())
                    {
						StandByOp();
                        return ReturnedCode.Suspended;
                    }
                }
            }

            // Trovati formati e lastre, avvio dell'ottimizzatore
            if (!StartOptimizer(out err))
            {
				OnStandBy();
				if (err == ReturnedCode.SlabEmpty)
				{
					// Nessuna soluzione, sospende la lastra
					if (!SuspendActualSlab())
						return ReturnedCode.Error;
					return ReturnedCode.SlabEmpty;
				}
				else
					return ReturnedCode.Error;
            }

            // Ottimizzatore avviato
            return ReturnedCode.Started;
        }

        ///*********************************************************************
        /// <summary>
        /// Verifica la fine dell'esecuzione dell'ottimizzazione
        /// </summary>
        /// <returns>codice di ritorno del processo</returns>
        ///*********************************************************************
		public ReturnedCode IsCompleted()
		{
			ReturnedCode err;

			// Verifica se l'ottimizzazione � completata
			if (mOptimizer.IsCompleted)
			{
				// Memorizza il risultato dell'ottimizzazione dentro il DB
				if (!GetOptResultIntoDb())
					return ReturnedCode.Error;

				Sequence.Mode seqMode = GetSequenceMode();

				// Sviluppo ottimale o guidato
				if (seqMode == Sequence.Mode.OptimalRefSlab
					|| seqMode == Sequence.Mode.OptimalRealSlab
					|| seqMode == Sequence.Mode.GuidedRefSlab
					|| seqMode == Sequence.Mode.GuidedRealSlab)
				{
					// Prima ottimizzazione
					if (mSlabGroups.IsStartPosition() || mBestOptSurface == 0d)
					{
						// Salva il risultato
						if (!SavePartialResult(mOpSlab.gId, mOptimizer.SlabSurface, mOptimizer.BestResultSurface))
							return ReturnedCode.Error;
					}
					// Ottimizzazioni successive
					else
					{
						// Verifica il risultato con il migliore finora trovato
						if (!VerifyPartialResult(mOpSlab.gId, mOptimizer.SlabSurface, mOptimizer.BestResultSurface))
							return ReturnedCode.Error;
					}

					// Passa al gruppo di lastre successivo
					mSlabGroups.MoveNext();

					// Se sono finiti i gruppi di lastre
					if (mSlabGroups.IsEOF())
					{
						// Ripristina il miglior risultato
						if (!RestoreBestPartialResult())
							return ReturnedCode.Error;

						// Rilascia l'ottimizzatore
						mOptimizer.LeaveAccess();

						// Esce
						if (mBestOptSurface > 0d)
						{
							// Nuova soluzione, aggiorna le informazioni
							if (!UpdateSolutionsInfo())
								return ReturnedCode.Error;

							// Azzera la migliore superficie trovata
							mBestOptSurface = 0d;
							mBestSlabSurface = 0d;
							mBestSlabId = 0;

							return ReturnedCode.NewSolution;
						}
						else
						{
							// Nessuna soluzione, sospende la lastra
							if (!SuspendActualSlab())
								return ReturnedCode.Error;

							// Azzera la migliore superficie trovata
							mBestOptSurface = 0d;
							mBestSlabSurface = 0d;
							mBestSlabId = 0;
							
							return ReturnedCode.SlabEmpty;
						}
					}

					// Ci sono altri gruppi di lastre da provare
					else
					{
						// Recupera le lastre
						if (!RecoverAvailableSlabs())
						{
							StandByOp();
							return ReturnedCode.Error;
						}

						// Verifica se ha trovato delle lastre
						if (!AnyValidSlab())
						{
							// Ottimizzazione con lastre tipo
					        if (seqMode == Sequence.Mode.GuidedRefSlab 
								|| seqMode == Sequence.Mode.OptimalRefSlab 
								|| seqMode == Sequence.Mode.SimpleRefSlab)
							{
								// Crea delle lastre virtuali
								if (!CreateVirtualSlabs())
								{
									StandByOp();
									mOptimizer.LeaveAccess();
									return ReturnedCode.Suspended;
								}
								// Recupera le lastre
								if (!RecoverAvailableSlabs())
								{
									StandByOp();
									mOptimizer.LeaveAccess();
									return ReturnedCode.Suspended;
								}
								// Verifica se ha trovato delle lastre
								if (!AnyValidSlab())
								{
									StandByOp();
									mOptimizer.LeaveAccess();
									return ReturnedCode.Suspended;
								}
							}

							// Ottimizzazione con lastre reali
							else
							{
								// Cerca un gruppo di lastre con delle lastre valide
					            mSlabGroups.MoveNext();
					            while (!mSlabGroups.IsEOF())
								{
									// Recupera le lastre
									if (!RecoverAvailableSlabs())
									{
										StandByOp();
										mOptimizer.LeaveAccess();
										return ReturnedCode.Suspended;
									}
									// Verifica se ha trovato delle lastre
					                if (AnyValidSlab())
										break;

									// Altrimenti passa al gruppo successivo
					                mSlabGroups.MoveNext();
					            }

								// Se non ha trovato altre lastre
					            if (mSlabGroups.IsEOF())
								{
									// Ripristina il miglior risultato
									if (!RestoreBestPartialResult())
										return ReturnedCode.Error;

									// Rilascia l'ottimizzatore
									mOptimizer.LeaveAccess();

									// Esce
									if (mBestOptSurface > 0d)
									{
										// Nuova soluzione, aggiorna le informazioni
										if (!UpdateSolutionsInfo())
											return ReturnedCode.Error;

										// Azzera la migliore superficie trovata
										mBestOptSurface = 0d;
										mBestSlabSurface = 0d;
										mBestSlabId = 0;

										return ReturnedCode.NewSolution;
									}
									else
									{
										// Nessuna soluzione, sospende la lastra
										if (!SuspendActualSlab())
											return ReturnedCode.Error;

										// Azzera la migliore superficie trovata
										mBestOptSurface = 0d;
										mBestSlabSurface = 0d;
										mBestSlabId = 0;

										return ReturnedCode.SlabEmpty;
									}
								}
					        }
					    }

			            // Avvio dell'ottimizzatore
						if (!StartOptimizer(out err))
						{
							OnStandBy();
							mOptimizer.LeaveAccess();
							if (err == ReturnedCode.SlabEmpty)
							{
								// Nessuna soluzione, sospende la lastra
								if (!SuspendActualSlab())
									return ReturnedCode.Error;
								return ReturnedCode.SlabEmpty;
							}
							else
								return ReturnedCode.Error;
						}

						// Preottimizzazione ancora in corso
						return ReturnedCode.Busy;
					}
				}

				// Sviluppo semplice
				else
				{
					// Salva il risultato
					if (!SavePartialResult(mOpSlab.gId, mOptimizer.SlabSurface, mOptimizer.BestResultSurface))
						return ReturnedCode.Error;

					// Ripristina il miglior risultato
				    if (!RestoreBestPartialResult())
						return ReturnedCode.Error;

					// Va alla fine delle lastre, in modo da ricaricare tutto (shape, lastre, ...)
					OnStandBy();

					// Rilascia l'ottimizzatore
					mOptimizer.LeaveAccess();

					// Esce
					if (mBestOptSurface > 0d)
					{
						// Nuova soluzione, aggiorna le informazioni
						if (!UpdateSolutionsInfo())
							return ReturnedCode.Error;

						// Azzera la migliore superficie trovata
						mBestOptSurface = 0d;
						mBestSlabSurface = 0d;
						mBestSlabId = 0;

						return ReturnedCode.NewSolution;
					}
					else
					{
						// Nessuna soluzione, sospende la lastra
						if (!SuspendActualSlab())
							return ReturnedCode.Error;

						// Azzera la migliore superficie trovata
						mBestOptSurface = 0d;
						mBestSlabSurface = 0d;
						mBestSlabId = 0;
						
						return ReturnedCode.SlabEmpty;
					}
				}
			}

			// Ottimizzazione ancora in corso
			else
				return ReturnedCode.Busy;
		}

		#endregion

		#region Private Methods

		///**********************************************************************
        /// <summary>
        /// Se l'ordine di produzione appartiente ad una sequenza che usa lastre tipo,
        /// si generano un certo numero di lastre tipo delle misure richieste.
        /// Nota:
        /// non implementato
        /// </summary>
        /// <returns>
        ///     true    operazione eseguita con successo
        ///     false   errore
        /// </returns>
		///**********************************************************************
        private bool CreateVirtualSlabs()
        {
            // funzione non implementata
            return false;
        }

		///**********************************************************************
		/// <summary>
		/// operazione da eseguire sugli stati e ordini di produzione all'esaurimento
		/// di tutti i pezzi da realizzare
		/// </summary>
		/// <returns>codice di ritorno del processo</returns>
		///**********************************************************************
		private ReturnedCode OnProcessTerminated()
		{
			return OnProcessTerminated(-1);
		}
		
		private ReturnedCode OnProcessTerminated(int idSeq)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction)	mDbInterface.BeginTransaction();

					if (!StopPreOpt())
						throw (new ApplicationException("PreOptimizer.OnProcessTerminated: Error changing sequences states"));

					// Chiude la transazione
					if (transaction)	mDbInterface.EndTransaction(true);

					// Verifica se esistono ordini di produzione sospesi
					int suspOrd = mProdOrders.StateCount(ProdOrder.State.Suspended, idSeq);
					if (suspOrd < 0)
						return ReturnedCode.Error;
				
					else if (suspOrd > 0)
						return ReturnedCode.Suspended;

					else
						return ReturnedCode.Terminated;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.OnProcessTerminated Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction)	mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.OnProcessTerminated: Error", ex);
					return ReturnedCode.Error;
				}
			}

			return ReturnedCode.Error;
		}


		///**********************************************************************
		/// <summary>
		/// Recupera i formati che possono essere utilizzati divisi per spessore.
		/// 
		/// Note:
		/// la funzione valorizza il recordset RsOp con la lista degli OP che sono validi;
		/// in particolare il record corrente corrisponde all'OP per il quale sono stati 
		/// estratti i formati; anche il recordset RsShapes e' valorizzato con i formati 
		/// relativi all'OP corrente; infine il recordset RsParam resta valorizzato con 
		/// parametri associati all'OP.
		/// </summary>
		/// <returns>codice di ritorno del processo</returns>
		///**********************************************************************
		private ReturnedCode RecoverAvailableShapes()
		{
			return RecoverAvailableShapes(-1);
		}

		private ReturnedCode RecoverAvailableShapes(int idSeq)
		{
			// Cancella tutti i formati in memoria
			mShapes.Clear();
			mSequences.Clear();
			mParameters.Clear();
            mSlabGroups.Clear();

			if (idSeq > 0)
			{
				if (!mSequences.GetDataFromDb( Sequence.Keys.F_ID_PRO_SEQUENCE, idSeq, Sequence.State.Busy))
				{
					TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error reading sequences.");
					return ReturnedCode.Error;
				}
			}
			else
			{
				// Legge le sequenze in esecuzione
				if (!mSequences.GetDataFromDb(Sequence.State.Busy))
				{
					TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error reading sequences.");
					return ReturnedCode.Error;
				}
			}

			// Tutte le sequenze terminate
			if (mSequences.Count == 0)
				return OnProcessTerminated(idSeq);

			// Legge gli ordini di produzioni in esecuzione
			Sequence seq;
			ProdOrder po = null;

			// Scorre tutte le sequenze
			mSequences.MoveFirst();
			while (mSequences.GetObject(out seq))
			{
				// Legge gli ordini di produzione della sequenza in esecuzione
				if (!seq.GetProdOrdersFromDb(mDbInterface, ProdOrder.State.Busy))
				{
					TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error reading production orders.");
					return ReturnedCode.Error;
				}

				// Scorre tutti gli ordini di produzione
				while (seq.gProdOrders.GetObject(out po))
				{
					// Legge i formati dell'ordine di produzione
					if (!mShapes.GetProdOrderShapesFromDb(po.gId))
					{
						TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error reading shapes.");
						return ReturnedCode.Error;
					}

					// Se ha trovato almeno un formato
					if ((mShapes.Count - mShapes.CountOptRecover) > 0)
					{
						// Legge T_OP_SLAB_GROUPS
						if (!mSlabGroups.GetDataFromDb(po.gId))
						{
							TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error reading slab groups.");
							return ReturnedCode.Error;
						}

						// Se trova almeno una lastra
						if (mSlabGroups.Count > 0)
							// esce dal ciclo
							break;
					}
					else
						// Se non ha trovato nessun formato, l'ordine di produzione � terminato
						po.gStateOrd = ProdOrder.State.Completed;

					// Passa all'ordine di produzione successivo
					seq.gProdOrders.MoveNext();
				}
				
				// Se non si tratta del primo ordine, allora almeno uno � stato dichiarato terminato
				if (!seq.gProdOrders.IsStartPosition())
					if (!seq.gProdOrders.UpdateDataToDb())
					{
						TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error updating production order state.");
						return ReturnedCode.Error;
					}

				// Se ha trovato almeno un formato
				if ((mShapes.Count - mShapes.CountOptRecover) > 0)
					// Se trova almeno una lastra
					if (mSlabGroups.Count > 0)
						// esce dal ciclo
						break;

				// Passa alla sequenza successiva
				mSequences.MoveNext();
			}

			// Nessun formato trovato, processo terminato
			if ((mShapes.Count - mShapes.CountOptRecover) <= 0)
				return OnProcessTerminated(idSeq);

			// Nessuna lastra trovata, processo terminato
			if (mSlabGroups.Count <= 0)
				return OnProcessTerminated(idSeq);

			// Legge i parametri
			mParameters.gIdOp = po.gId;
			if (!mParameters.GetDataFromDb(null, "OptiMaster"))
			{
				TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error reading parameters.");
				return ReturnedCode.Error;
			}

			// Legge T_OP_SLAB_GROUPS
			//if (!mSlabGroups.GetDataFromDb(po.gId))
			//{
			//    TraceLog.WriteLine("PreOptimizer.RecoverAvailableShapes: Error reading slab groups.");
			//    return ReturnedCode.Error;
			//}
		            
			return ReturnedCode.Ok;
		}

        ///*********************************************************************
        /// <summary>
        /// Recupera le lastre disponibili con i formati da eseguire.
        /// Nota:
        /// La funzione valorizza la collection mSlabs con la lista delle lastre
        /// compatibili con l'ordine di produzione e il gruppo di lastre attuale.
        /// </summary>
        /// <returns>
        ///     true    operazione eseguita con successo
        ///     false   errore nell'operazione
        /// </returns>
        ///*********************************************************************
		private bool RecoverAvailableSlabs()
		{
			return RecoverAvailableSlabs(-1);
		}
		private bool RecoverAvailableSlabs(int idSeq)
        {
            mOpSlabs.Clear();
            mOpSlab = null;
            mSlabs.Clear();
            mSlab = null;

            // Legge l'ordine di produzione in esecuzione
            ProdOrder po;
            if (!GetActualProdOrder(out po))
                return false;

            // Legge il gruppo di lastre in esecuzione
            SlabGroup slabGroup;
            if (!mSlabGroups.GetObject(out slabGroup))
                return false;

            // Legge le lastre
            if (!mOpSlabs.GetDataFromDb(po.gId, slabGroup.gGroup, OpSlab.State.Available))
            {
                TraceLog.WriteLine("PreOptimizer.RecoverAvailableSlabs: Error reading slabs.");
                return false;
            }

            // Recupera le informazioni della lastra da ottimizzare
			return GetOptSlab();
        }

        ///*********************************************************************
        /// <summary>
        /// Operazioni da eseguire quando il calcolo deve andare in stand by.
        /// </summary>
        ///*********************************************************************
        private void OnStandBy()
        {
            // Va alla fine per far ricaricare tutto alla successiva ripartenza.
            mSlabGroups.MoveLast();
            mSlabGroups.MoveNext();
        }

        ///*********************************************************************
        /// <summary>
        /// Verifica se il numero di lastre estratte e' non nullo.
        /// Questo metodo va chiamato dopo RecoverAvailableShapes per verificare 
        /// se esistono lastre ottimizzabili.
        /// </summary>
        /// <returns>
        ///     true    lastre utilizzabili
        ///     false   nessuna lastra utilizzabile
        /// </returns>
        ///*********************************************************************
        private bool AnyValidSlab()
        {
            return !mOpSlabs.IsEOF();
        }

        ///*********************************************************************
        /// <summary>
        /// Avvia l'ottimizzatore
        /// </summary>
        /// <returns>
        ///     true    ottimizzatore avviato
        ///     false   errore
        /// </returns>
        ///*********************************************************************
		
        private bool StartOptimizer(out ReturnedCode err)
        {
			Parameter par;

			err = ReturnedCode.Error;

			// Legge il tipo di ottimizzazione 
			mParameters.GetOptParameter(Optimizer.OptParamName.F_OTT_TIPO, out par);
						
            // Crea l'ottimizzatore
			if ((Optimizer.OptTypesEnum)par.gIntValue == Optimizer.OptTypesEnum.Polygons)
				mOptimizer = new OptPolygons();
			else
				mOptimizer = new OptRectangles();

            // Guadagna l'accesso all'ottimizzatore
            if (!mOptimizer.GetAccess())
            {
                TraceLog.WriteLine("PreOptimizer.StartOptimizer: Cannot acquire access to Optimizer");
                return false;
            }

            // Passa i formati all'ottimizzatore
            Shape sh;
            mShapes.MoveFirst();
            while (mShapes.GetObject(out sh))
            {
                // Recupera il file xml del poligono dal database
                if (sh.gXmlPolygon == "")
                    if (!mShapes.GetXmlPolygonFromDb(sh, mTmpDirPath + "\\SHAPEID_" + sh.gId.ToString() + ".xml"))
                        TraceLog.WriteLine("PreOptimizer.StartOptimizer: Error readin xml polygon from DB");


                // Verifica che il poligono sia stato recuperato
                if (sh.gZone != null)
                    if (!mOptimizer.AddShape(sh.gIdOpShape, 
                                        sh.gAvailableQty, 
                                        sh.gPriority, 
                                        DateTime.Now, 
                                        Optimizer.RotationType.Free, 
                                        0d,
                                        (sh.gShapeType == Shape.ShapeType.Rectangles ? 
											Optimizer.ShapeType.Rectangle : Optimizer.ShapeType.Polygon),
                                        sh.gWidth, 
                                        sh.gLength,
                                        sh.gZone, 
										sh.gOptRecover))
                        TraceLog.WriteLine("PreOptimizer.StartOptimizer: Error adding shape to Optimizer");

				// Imposta l'id di ricerca
				sh.gTableId = sh.gIdOpShape;

                // Passa al formato successivo
                mShapes.MoveNext();
            }
            
            // Passa i parametri all'ottimizzatore
            SetOptParameters(mParameters);

            // Passa la lastra all'ottimizzatore
            if (!mOptimizer.DefineSlab(mSlab.gId, mSlab.gZone, mSlab.gDimZ))
            {
                TraceLog.WriteLine("PreOptimizer.StartOptimizer: Cannot defining slab to Optimizer");
                mOptimizer.LeaveAccess();
				err = ReturnedCode.SlabEmpty;
                return false;
            }

            // Avvia l'ottimizzatore
            if (!mOptimizer.StartOptimizer(-1, -1))
            {
                TraceLog.WriteLine("PreOptimizer.StartOptimizer: Error starting optimizer");
                mOptimizer.LeaveAccess();
                return false;
            }

            // Ottimizzatore avviato
			err = ReturnedCode.Ok;
            return true;
        }

        ///***************************************************************************
        /// <summary>
        /// Imposta i parametri dell'ottimizzatore
        /// </summary>
        ///***************************************************************************
        private void SetOptParameters(OptParameterCollection parameters)
        {
            Parameter par;

            /*
             * Parametri generali
             */

            // coefficiente (a) della funzione dell'area del pezzo ( a*x^2 + b*x )
            /*
             * ATTENZIONE: il valore 99 al parametro a � inteso come un flag, per dire all'ottimizzatore
             * di applicare il suo calcolo interno per trovare il parametro a. In questo caso l'ottimizzatore
             * genera un valore di a prossimo allo 0 (ordine 10E-7) per cui impostare 99 � praticamente come
             * impostare 0.
             */
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_COEFF_A, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_COEFF_A, par.gDoubleValue);
            // coefficiente (b) della funzione dell'area del pezzo ( a*x^2 + b*x )
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_COEFF_B, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_COEFF_B, par.gDoubleValue);
            // coefficiente (c) della funzione del tempo ( c*t^2 + d*t)
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_COEFF_C, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_COEFF_C, par.gDoubleValue);
            // coefficiente (d) della funzione del tempo ( c*t^2 + d*t)
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_COEFF_D, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_COEFF_D, par.gDoubleValue);
            // coefficiente (e) del calcolo del punteggio finale ( e*area * f*priorita)
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_COEFF_E, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_COEFF_E, par.gDoubleValue);
            // coefficiente (f) del calcolo del punteggio finale ( e*area * f*priorita)
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_COEFF_F, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_COEFF_F, par.gDoubleValue);
            // dimensione utensile
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_SFRIDO, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_SFRIDO, par.gDoubleValue);
			// diametro disco
			if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_DISK_DIAMETER, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_DISK_DIAMETER, par.gDoubleValue);
			// entita' (mm.) di riduzione lastra
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_RID_LASTRA, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_RID_LASTRA, par.gDoubleValue);
            // entita' (mm.) di aumento difetti
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_RID_DIFETTI, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_RID_DIFETTI, par.gDoubleValue);
            // entita' (mm.) di semplificazione lastra vettorializzata
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_SEMPL_LASTRA, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_SEMPL_LASTRA, par.gDoubleValue);
            // Massimo numero di formati per l'ottimizzatore
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_MAX_FORMATI, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_MAX_FORMATI, par.gIntValue);
            // abilita (1) o disabilita (0) la rotazione dei pezzi dell'ordine
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_ROTAZ, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_ROTAZ, par.gBoolValue);
            // step angolo di rotazione pezzi
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_STEP_ANGLE, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_STEP_ANGLE, par.gDoubleValue);
            // timeout ottimizzazione (in sec.)
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_TIMEOUT, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_TIMEOUT, par.gIntValue);
			// timeout calcolo ottimizzazione (in sec.)
			if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_TIMEOUT_COMPUTE, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_TIMEOUT_COMPUTE, par.gIntValue);
			// tipo ottimizzazione
			if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_TIPO, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_TIPO, par.gIntValue);
			// disabilita i difetti delle lastre
			if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_DISABLE_DEFECTS, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_DISABLE_DEFECTS, par.gBoolValue);
			// abilita i file di log dell'ottimizzatore
			if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_LOG_FILES, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_LOG_FILES, par.gIntValue);
			
            /*
             * Parametri ottimizzatore poligoni
             */

            // Considera sfrido e diametro disco oppure solo sfrido disco
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_POL_SOLO_SFRIDO, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_POL_SOLO_SFRIDO, par.gBoolValue);
            // Delta aggiuntivo Sbordamento disco
			if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_POL_DELTA_SBORDAMENTO, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_POL_DELTA_SBORDAMENTO, par.gDoubleValue);

            /*
             * Parametri ottimizzatore a filagne
             */

            // numero massimo di tagli lungo l'asse X (lunghezza lastra)
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_TAGLI_X, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_TAGLI_X, par.gIntValue);
            // numero massimo di tagli lungo l'asse Y (altezza lastra)
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_TAGLI_Y, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_TAGLI_Y, par.gIntValue);
            // lunghezza del taglio ridotto in cm.
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_YR, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_YR, par.gDoubleValue);
            // numero massimo di tagli ridotti lungo l'asse X (lunghezza) in cm.
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_XC, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_XC, par.gIntValue);
            // numero massimo di tagli ridotti lungo l'asse Y (altezza) in cm.
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_YC, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_YC, par.gIntValue);
            // lunghezza minima dei pezzi normali in cm.
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_XMIN, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_XMIN, par.gDoubleValue);
            // larghezza minima dei pezzi normali in cm.
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_YMIN, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_YMIN, par.gDoubleValue);
            // entita' (cm.) di riduzione del primo e ultimo tratto di ogni riga
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_RIDX, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_RIDX, par.gDoubleValue);
            // entita' (cm.) di riduzione del primo e ultimo tratto di ogni colonna
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_RIDY, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_RIDY, par.gDoubleValue);
            // abilita (1) o disabilita (0) l'eliminazione dei difetti prima di posizionare i pezzi a correre
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_PEZZI_DIF, out par))
                mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_PEZZI_DIF, par.gBoolValue);
            // altezza minima delle filagne per utilizzo scarico a ventose; se 0 si gestisce la modalita' scarico a ventose.
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_WIDTH_VENT, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_WIDTH_VENT, par.gDoubleValue);
            // lunghezza minima delle filagne per utilizzo con scaricatore a ventose
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_LENGTH_VENT, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_LENGTH_VENT, par.gDoubleValue);
            // minimo interspazio (cm.), maggiore dello sfrido, tra due filagne
            if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_INTERSP, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_INTERSP, par.gDoubleValue);
			// modo di ottimizzazione a filagne
			if (parameters.GetOptParameter(Optimizer.OptParamName.F_OTT_FIL_MODO, out par))
				mOptimizer.SetParameter(Optimizer.OptParamName.F_OTT_FIL_MODO, par.gIntValue);
        }
		
        ///***************************************************************************
		/// <summary>
		/// Salva il risultato parziale dell'ottimizzazione su di una lastra.
		/// </summary>
		/// <param name="idSlab">identificatore della lastra</param>
		/// <param name="slabSurface">area totale della lastra</param>
		/// <param name="optSurface">area ottimizzata</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
        ///***************************************************************************
		private bool SavePartialResult(int idSlab, double slabSurface, double optSurface)
		{		    
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Rimuove i parametri esistenti
					parameters.Clear();

					// Salva i risultati attuali
					//par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					//par.Direction = System.Data.ParameterDirection.ReturnValue;
					//parameters.Add(par);

					par = new OleDbParameter("@slabid", OleDbType.Integer);
					par.Value = idSlab;
					parameters.Add(par);
					
					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoSavePartialResult", ref parameters))
						throw new ApplicationException("PreOptimizer.SavePartialResult sp_PoSavePartialResult Execute error.");

					// Verifica il codice di ritorno
					//par = (OleDbParameter) parameters[0];
					//if (((int) par.Value) != 0)
					//    throw new ApplicationException("PreOptimizer.SavePartialResult sp_PoSavePartialResult error: " + ((int) par.Value));

					// Salva i dati 
					mBestSlabId = idSlab;
					mBestSlabSurface = slabSurface;
					mBestOptSurface = optSurface;

					// Riporta le sequenze sospese, in esecuzione
					string sqlQuery = "update T_PRO_SEQUENCES" +
						" set F_STATE = " + ((int)Sequence.State.Busy).ToString() +
						" where (" +
						"	F_STATE = " + ((int)Sequence.State.Confirmed).ToString() +
						"	or F_STATE = " + ((int)Sequence.State.Suspended).ToString() +
						")";
					if (mUser != null && !mUser.gIsAdministrator)
						sqlQuery += " and F_ID_T_AN_USERS = " + mUser.gId.ToString();

					if (!mDbInterface.Execute(sqlQuery))
						throw (new ApplicationException("PreOptimizer.SavePartialResult: Error changing T_PR_SEQUENCES states"));

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}
			
					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.SavePartialResult Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction)	mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.SavePartialResult Error.", ex);

					return false;
				}
			}

			return false;
		}

        ///***************************************************************************
		/// <summary>
		/// Verifica se il nuovo risultato � migliore del precedente.
		/// </summary>
		/// <param name="idSlab">identificatore della lastra</param>
		/// <param name="slabSurface">area totale della lastra</param>
		/// <param name="optSurface">area ottimizzata</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
        ///***************************************************************************
		private bool VerifyPartialResult(int idSlab, double slabSurface, double optSurface)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Verifica se il nuovo risultato � migliore del precedente
					if ((mBestOptSurface / mBestSlabSurface) < (optSurface / slabSurface))
					{
						// Rimuove i risultati temporanei
						if (!mDbInterface.Execute("sp_PoRemoveTempResults"))
							throw new ApplicationException("PreOptimizer.VerifyPartialResult sp_PoRemoveTempResults Execute error.");

						// Rimuove i parametri esistenti
						parameters.Clear();

						// Salva i risultati attuali
						//par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
						//par.Direction = System.Data.ParameterDirection.ReturnValue;
						//parameters.Add(par);

						par = new OleDbParameter("@slabid", OleDbType.Integer);
						par.Value = idSlab;
						parameters.Add(par);

						// Lancia la stored procedure
						if (!mDbInterface.Execute("sp_PoSavePartialResult", ref parameters))
							throw new ApplicationException("PreOptimizer.VerifyPartialResult sp_PoSavePartialResult Execute error.");

						// Verifica il codice di ritorno
						//par = (OleDbParameter) parameters[0];
						//if (((int) par.Value) != 0)
						//    throw new ApplicationException("PreOptimizer.VerifyPartialResult sp_PoSavePartialResult error: " + ((int) par.Value));

						// Salva i dati 
						mBestSlabId = idSlab;
						mBestSlabSurface = slabSurface;
						mBestOptSurface = optSurface;
					}

					// Altrimenti cancella il nuovo risultato
					else
					{
						// Rimuove il nuovo risultato
						if (!mDbInterface.Execute("delete T_OP_GROUPS where F_ID_OP_SLAB = " + idSlab))
							throw new ApplicationException("PreOptimizer.VerifyPartialResult delete T_OP_GROUPS error.");
					}

					// Riporta le sequenze sospese, in esecuzione
					string sqlQuery = "update T_PRO_SEQUENCES" +
						" set F_STATE = " + ((int)Sequence.State.Busy).ToString() +
						" where (" +
						"	F_STATE = " + ((int)Sequence.State.Confirmed).ToString() +
						"	or F_STATE = " + ((int)Sequence.State.Suspended).ToString() +
						")";
					if (mUser != null && !mUser.gIsAdministrator)
						sqlQuery += " and F_ID_T_AN_USERS = " + mUser.gId.ToString();

					if (!mDbInterface.Execute(sqlQuery))
						throw (new ApplicationException("PreOptimizer.VerifyPartialResult: Error changing T_PR_SEQUENCES states"));

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}
			
					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.VerifyPartialResult Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction)	mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.VerifyPartialResult Error.", ex);

					return false;
				}
			}

			return false;
		}

        ///***************************************************************************
		/// <summary>
		/// Reimposta il risultato parziale migliore.
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
        ///***************************************************************************
		private bool RestoreBestPartialResult()
		{		    
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Rimuove i parametri esistenti
					parameters.Clear();

					// Salva i risultati attuali
					//par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					//par.Direction = System.Data.ParameterDirection.ReturnValue;
					//parameters.Add(par);

					par = new OleDbParameter("@slabid", OleDbType.Integer);
					par.Value = mBestSlabId;
					parameters.Add(par);

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoRestorePartialResult", ref parameters))
						throw new ApplicationException("PreOptimizer.RestoreBestPartialResult sp_PoRestorePartialResult Execute error.");

					// Verifica il codice di ritorno
					//par = (OleDbParameter) parameters[0];
					//if (((int) par.Value) != 0)
					//    throw new ApplicationException("PreOptimizer.RestoreBestPartialResult sp_PoSavePartialResult error: " + ((int) par.Value));

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}
			
					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.RestoreBestPartialResult Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction)	mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.RestoreBestPartialResult Error.", ex);

					return false;
				}
			}

			return false;
		}

        ///***************************************************************************
		/// <summary>
		/// Azzera tutti i risultati parziali.
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
        ///***************************************************************************
		private bool ResetPartialResults()
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoRemoveTempResults"))
						throw new ApplicationException("PreOptimizer.ResetPartialResults sp_PoRemoveTempResults Execute error.");

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.ResetPartialResults Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.ResetPartialResults Error.", ex);

					return false;
				}
			}

			return false;
		}

		///***************************************************************************
		/// <summary>
		/// Legge le informazioni della lastra da ottimizzare
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		///***************************************************************************
		private bool GetOptSlab()
		{
			// Recupera la lastra da ottimizzare
			if (mOpSlabs.GetObject(out mOpSlab))
			{
				if (!mSlabs.GetSingleElementFromDb(mOpSlab.gIdArticle))
					return false;

				if (!mSlabs.GetObject(out mSlab))
					return false;

				if (!mSlabs.GetXmlParameterFromDb(mSlab, mTmpDirPath + "\\SLABID_" + mSlab.gId.ToString() + ".xml"))
					return false;

				if (!mSlab.ReadFileXml(mSlab.gXmlParameterLink, mTmpDirPath + "\\SLABID_" + mSlab.gId.ToString() + ".txt", "", ""))
					return false;

				if (mSlab.gZone == null)
					return false;
			}

			return true;
		}

		/// ***************************************************************************
		/// <summary>
		/// Alla fine dell'ottimizzazione si aggiornano le tabelle utilizzate solo dal 
		/// preottimizzatore.
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		/// ***************************************************************************
		private bool UpdateSolutionsInfo()
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Aggiorna lo stato delle lastre
					if (!mOpSlabs.SetOpSlabState(mBestSlabId, 
												 OpSlab.State.Programming, 
												 (mOptimizer is OptPolygons ? OpSlab.CutType.Polygons : OpSlab.CutType.Strips),
												 mBestOptSurface))
						throw new ApplicationException("PreOptimizer.UpdateSolutionsInfo update T_OP_SLABS state error.");

					// Legge l'ordine di produzione in esecuzione
					ProdOrder po;
					if (!GetActualProdOrder(out po))
						throw new ApplicationException("PreOptimizer.UpdateSolutionsInfo get production order error.");


					/*
					 * Aggiorna i risultati dell'ordine di produzione
					 */

					// Rimuove i parametri esistenti
					parameters.Clear();

					par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					par.Direction = System.Data.ParameterDirection.ReturnValue;
					parameters.Add(par);

					par = new OleDbParameter("@sequence_id", OleDbType.Integer);
					par.Value = 0;
					parameters.Add(par);

					par = new OleDbParameter("@id_op", OleDbType.Integer);
					par.Value = po.gId;
					parameters.Add(par);

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoUpdateResults", ref parameters))
						throw new ApplicationException("PreOptimizer.UpdateSolutionsInfo sp_PoUpdateResults Execute error.");

					// Verifica il codice di ritorno
					par = (OleDbParameter) parameters[0];
					if (((int) par.Value) != 0)
						throw new ApplicationException("PreOptimizer.UpdateSolutionsInfo sp_PoUpdateResults error: " + ((int)par.Value));

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.UpdateSolutionsInfo Error.", ex);
					
					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.UpdateSolutionsInfo Error.", ex);

					return false;
				}
			}

			return false;
		}

		///***************************************************************************
		/// <summary>
		/// Sospende la lastra attualmente in ottimizzazione.
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		///***************************************************************************
		private bool SuspendActualSlab()
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Aggiorna lo stato delle lastre
					if (!mOpSlabs.SetOpSlabState(mOpSlab.gId,
												 OpSlab.State.Suspended,
												 OpSlab.CutType.Undef,
												 0d))
						throw new ApplicationException("PreOptimizer.SuspendActualSlab update T_OP_SLABS state error.");

					// Chiude la transazione
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.SuspendActualSlab Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.SuspendActualSlab Error.", ex);

					return false;
				}
			}

			return false;
		}

		///***************************************************************************
		/// <summary>
		/// Restituisce l'attuale ordine di produzione
		/// </summary>
		/// <param name="seq">sequenza di produzione restituita</param>
		/// <param name="po">ordine di produzione restituito</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		///***************************************************************************
		private bool GetActualProdOrder(out ProdOrder po)
		{
			Sequence seq;
			return GetActualProdOrder(out seq, out po);
		}
		private bool GetActualProdOrder(out Sequence seq, out ProdOrder po)
		{
			seq = null;
			po = null;

			// Legge la sequenza in esecuzione
			if (!mSequences.GetObject(out seq))
				return false;

			// Legge l'ordine di produzione in esecuzione
			if (!seq.gProdOrders.GetObject(out po))
				return false;

			return true;
		}

		///***************************************************************************
		/// <summary>
		/// Imposta lo stato di Stand-by per l'OP corrente; in genere questa operazione
		/// e' la conseguenza di una mancanza di lastre compatibili.
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///***************************************************************************
		private bool StandByOp()
		{

			OnStandBy();

			// Legge l'attuale ordine di produzione
			ProdOrder po;
			if (!GetActualProdOrder(out po))
				return false;

			// Cambia lo stato dell'ordine di produzione
			if (!mProdOrders.SetProdOrderState(po.gId, ProdOrder.State.Suspended))
				return false;

			return true;
		}

		///***************************************************************************
		/// <summary>
		/// Legge i risultati dell'ottimizzazione e li memorizza dentro il DB
		/// </summary>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///***************************************************************************
		private bool GetOptResultIntoDb()
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			int idOpShape, groupId, quantity, priority;
			double offsetX, offsetY, rotAngle, angle, width, length, lengthMax;
			Zone polygon;
			DateTime date;
			Optimizer.RotationType rotation;
			Optimizer.ShapeType shapeType;
			Group group;
			Piece piece;
			Shape shape;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre una transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Legge l'ordine di produzione in esecuzione
					ProdOrder po;
					if (!GetActualProdOrder(out po))
						throw new ApplicationException("PreOptimizer.GetOptResultIntoDb get production order error.");

					// Legge i gruppi precedenti legati alla lastra
					GroupCollection groups = new GroupOpCollection(mDbInterface);
					if (!groups.GetSlabGroupsFromDb(mOpSlab.gId))
						throw new ApplicationException("PreOptimizer.GetOptResultIntoDb get old groups error.");
					
					// Cancella tutti i gruppi letti
					while (groups.Count > 0)
						groups.DeleteObject(0);
					if (!groups.UpdateDataToDb())
						throw new ApplicationException("PreOptimizer.GetOptResultIntoDb delete old groups error.");

					// Recupera il numero di pezzi della miglior soluzione
					int numPieces = mOptimizer.GetBestSolution();
					// Recupera la miglior soluzione
					for (int i = 0; i < numPieces; i++)
					{
						// Recupera il pezzo dalla soluzione
						mOptimizer.GetBestSolution(out idOpShape, out shapeType, out groupId, out offsetX, out offsetY, out rotAngle);

						// Recupera il formato agganciato al pezzo
						if (shapeType == Optimizer.ShapeType.RunRectangle)
						{
							mOptimizer.GetShapeRun(idOpShape, out quantity, out priority, out date, out rotation, out angle, out shapeType,
								out width, out length, out lengthMax);
							polygon = null;
						}
						else
						{
							mOptimizer.GetShape(idOpShape, out quantity, out priority, out date, out rotation, out angle, out shapeType,
								out width, out length, out polygon);
							lengthMax = 0d;
						}

						/***
						 *** ATTENZIONE: GESTISCE SOLO I PEZZI POLIGONALI 
						 *** OCCORRE INSERIRE LA GESTIONE DELLE FILAGNE
						 ***/
						// Crea il pezzo
						piece = new PieceOp(po.gId, 0, 0, idOpShape, Piece.State.Ready, 0d, 0d, false, Piece.PieceType.Good, 
							offsetX, offsetY, rotAngle,	length, width, shapeType == Optimizer.ShapeType.Polygon);
						// Imposta il poligono
						piece.gZone = polygon;
						// Recupera il formato padre
						if (!mShapes.GetObjectTable(idOpShape, out shape))
							throw new ApplicationException("PreOptimizer.GetOptResultIntoDb piece shape not found");
						// Imposta il file xml
						piece.iSetXmlPolygon(shape.gXmlPolygon);

						// Recupera la filagna
						if (!groups.GetObjectTable(groupId, out group))
						{
							/***
							 *** ATTENZIONE: GESTISCE SOLO I GRUPPI DI POLIGONI 
							 *** OCCORRE INSERIRE LA GESTIONE DELLE FILAGNE
							 ***/
							// Crea un nuovo gruppo
							group = new GroupOp(po.gId, 0, mOpSlab.gId, Group.GroupType.Polygon, 0d, 0d, 0d, 0d, 0d, 0d,
								Group.State.Ready, Group.LinkIndex.Single, 0d);
							group.gTableId = groupId;
							group.GetPiecesFromDb(mDbInterface);

							groups.AddObject(group);
						}

						// Aggiunge il pezzo al gruppo
						group.gPieces.AddObject(piece);
					}

					// Aggiorna tutti i nuovi gruppi nel DB
					if (!groups.UpdateDataToDb())
						throw new ApplicationException("PreOptimizer.GetOptResultIntoDb add new groups error.");

					// Chiude la transazione
					if (transaction)
						mDbInterface.EndTransaction(true);

					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PreOptimizer.GetOptResultIntoDb Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("PreOptimizer.GetOptResultIntoDb Error.", ex);

					return false;
				}
			}

			return false;
		}

		#endregion
	
    }
}
