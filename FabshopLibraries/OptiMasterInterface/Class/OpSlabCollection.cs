using System;
using System.Threading;
using System.Data.OleDb;
using System.Collections;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public class OpSlabCollection: DbCollection
	{
		#region Variables

		#endregion

		#region Constructors
		public OpSlabCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out OpSlab obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (OpSlab) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out OpSlab obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (OpSlab) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(OpSlab obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		///**********************************************************************
		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id elemento</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(OpSlab.Keys.F_NONE, id, 0, 0, OpSlab.State.Undef);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(OpSlab.Keys.F_NONE, 0, 0, 0, OpSlab.State.Undef);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(OpSlab.Keys orderKey)
		{
			return GetDataFromDb(orderKey, 0, 0, 0, OpSlab.State.Undef);
		}

		///*********************************************************************
		/// <summary>
		/// Legge le lastre legate ad un ordine di produzione
		/// </summary>
		/// <param name="idOp">id ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetDataFromDb(int idOp)
		{
			return GetDataFromDb(OpSlab.Keys.F_ID_OP_SLAB, 0, idOp, 0, OpSlab.State.Undef);
		}
		
		///*********************************************************************
		/// <summary>
		/// Legge le lastre legate ad un ordine di produzione e ad un gruppo
		/// </summary>
		/// <param name="idOp">id ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetDataFromDb(int idOp, int group)
		{
			return GetDataFromDb(OpSlab.Keys.F_ID_OP_SLAB, 0, idOp, group, OpSlab.State.Undef);
		}

		///*********************************************************************
		/// <summary>
		/// Legge le lastre legate ad un ordine di produzione, ad un gruppo e
		/// ad uno stato
		/// </summary>
		/// <param name="idOp">id ordine</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetDataFromDb(int idOp, int group, OpSlab.State state)
		{
			return GetDataFromDb(OpSlab.Keys.F_ID_OP_SLAB, 0, idOp, group, state);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="id">estrae solo l'elemento con l'id specificato</param>
		/// <param name="orderId">estrae gli elementi dell'ordine indicato</param>
		/// <param name="idOp">estrae gli elementi dell'ordine di produzione indicato</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(OpSlab.Keys orderKey, int id, int idOp, int group, OpSlab.State state)
		{
			return GetDataFromDb(orderKey, id, idOp, null, group, state);
		}
		public bool GetDataFromDb(OpSlab.Keys orderKey, int id, int idOp, int[] idArticles, int group, OpSlab.State state)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo il materiale con il codice richiesto
			if (id > 0)
			{
				sqlWhere += sqlAnd + "F_ID_OP_SLAB = " + id.ToString();
				sqlAnd = " and ";
			}

			//Estrae solo gli articoli richiesti
			if (idArticles != null && idArticles.Length > 0)
			{
				sqlWhere += sqlAnd + "F_ID_T_AN_ARTICLES in (" + idArticles[0];

				for (int i = 1; i < idArticles.Length; i++)
					sqlWhere += ", " + idArticles[i];
				sqlWhere += ")";

				sqlAnd = " and ";
			}

			if (idOp > 0)
			{
				sqlWhere += sqlAnd + "F_ID_OP = " + idOp.ToString();
				sqlAnd = " and ";
			}

			if (group > 0)
			{
				sqlWhere += sqlAnd + "F_GROUP = " + group.ToString();
				sqlAnd = " and ";
			}

			if (state != OpSlab.State.Undef)
			{
				sqlWhere += sqlAnd + "F_STATE = " + ((int) state).ToString();
				sqlAnd = " and ";
			}

			switch (orderKey)
			{
				case OpSlab.Keys.F_ID_OP_SLAB:
					sqlOrderBy = " order by F_ID_OP_SLAB";
					break;
				case OpSlab.Keys.F_ID_OP:
					sqlOrderBy = " order by F_ID_OP";
					break;
				case OpSlab.Keys.F_GROUP:
					sqlOrderBy = " order by F_GROUP";
					break;
				case OpSlab.Keys.F_STATE:
					sqlOrderBy = " order by F_STATE";
					break;
			}
			return GetDataFromDb("select * from T_OP_SLABS" + sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (OpSlab opSlab in mRecordset)
					{
						switch (opSlab.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_OP_SLABS (" + 
									" F_ID_OP" +
									", F_ID_T_AN_ARTICLES" +
									", F_GROUP" +
									", F_STATE" + 
									", F_CUT_TYPE" +
									", F_TOT_AREA" +
									", F_USED_AREA" +
									")" +
									" values (" + opSlab.gIdOp + 
									", " + opSlab.gIdArticle + 
									", " + opSlab.gGroup +
									", " + ((int) opSlab.gSlabState) +
									", " + ((int) opSlab.gCutType) +
									", " + opSlab.gTotArea.ToString() +
									", " + opSlab.gUsedArea.ToString() +
									")" +
									";select scope_identity()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								opSlab.gId = id;
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_OP_SLABS set" +
									" F_ID_OP = " + opSlab.gIdOp +
									", F_ID_T_AN_ARTICLES = " + opSlab.gIdArticle +
									", F_GROUP = " + opSlab.gGroup +
									", F_STATE = " + ((int) opSlab.gSlabState) +
									", F_CUT_TYPE = " + ((int) opSlab.gCutType) +
									", F_TOT_AREA = " + opSlab.gTotArea.ToString() +
									", F_USED_AREA = " + opSlab.gUsedArea.ToString();

								sqlUpdate += " where F_ID_OP_SLAB = " + opSlab.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;
						
								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (OpSlab opSlab in mDeleted)
					{
						switch (opSlab.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_OP_SLABS where F_ID_OP_SLAB = " + opSlab.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OpSlabCollection: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("OpSlabCollection: Error", ex);

					// Annulla la transazione
					if (transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		/// <summary>
		/// Ricarica i dati con l'ultima query eseguita 
		/// </summary>
		/// <returns>
		///		true	lettura eseguita
		///		false	lettura non eseguita
		/// </returns>
		public bool RefreshData()
		{
			return GetDataFromDb(mSqlGetData);
		}

		///**********************************************************************
		/// <summary>
		/// Seleziona le lastre per l'ordine di produzione specificato
		/// </summary>
		/// <param name="prodOrderId">id ordine di produzione</param>
		/// <param name="selectedSlabs">id lastre da selezionare</param>
		/// <param name="cut">cut type</param>
		/// <param name="usedArea">used surface</param>
		/// <returns>
		///			true if succedeed
		///			false otherwise
		///	</returns>
		///**********************************************************************
		public bool SelectOpSlabs(int prodOrderId, int[] selectedSlabs)
		{
			int retry = 0;
			string sqlCmd;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbDataReader dr = null;
			string sqlIdList;

			if (selectedSlabs.Length > 0)
			{
				sqlIdList = "( " + selectedSlabs[0];
				for (int i = 1; i < selectedSlabs.Length; i++)
					sqlIdList += "," + selectedSlabs[i];
				sqlIdList += ") ";
			}
			else
				sqlIdList = "( 0 )";

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Verifica se ci sono delle lastre nuove da inserire che sono gi� in lista di estrazione
					sqlCmd = "select  ( select    count(F_ID_T_AN_ARTICLES)" +
							"		  from      T_WK_USE_LISTS" +
							"		  where     F_ID_T_AN_ARTICLES in " + sqlIdList +
							"					and F_ID_T_AN_ARTICLES not in ( select  F_ID_T_AN_ARTICLES" +
							"													from    T_OP_SLABS" +
							"													where   F_ID_OP = " + prodOrderId.ToString() + " )" +
							"		)" +
							"		+ ( select  count(F_ID_T_AN_ARTICLES)" +
							"			from    T_WK_ARTICLE_USE_LISTS" +
							"			where   F_ID_T_AN_ARTICLES in " + sqlIdList +
							"					and F_ID_T_AN_ARTICLES not in ( select  F_ID_T_AN_ARTICLES" +
							"													from    T_OP_SLABS" +
							"													where   F_ID_OP = " + prodOrderId.ToString() + " )" +
							"		  )";

					if (!mDbInterface.Requery(sqlCmd, out dr))
						throw (new ApplicationException("OpSlabCollection.SelectOpSlabs: error reading T_WK_USE_LISTS or T_WK_ARTICLE_USE_LISTS slabs"));

					// Se ci sono lastre in lista di estrazione, errore
					if (dr.Read())
						if (dr.GetInt32(0) > 0)
							throw (new ApplicationException("OpSlabCollection.SelectOpSlabs: slab in T_WK_USE_LISTS or T_WK_ARTICLE_USE_LISTS"));

					// Elimina le lastre non selezionate, che non siano gi� bloccate
					sqlCmd = "delete from T_OP_SLABS" +
							 " where F_ID_OP = " + prodOrderId.ToString() +
							 " and F_ID_T_AN_ARTICLES not in " + sqlIdList +
							 " and F_STATE <> " + ((int)OpSlab.State.Confirmed).ToString();

					if (!mDbInterface.Execute(sqlCmd))
						throw (new ApplicationException("OpSlabCollection.SelectOpSlabs: error deleting old slabs"));

					// Inserisco le lastre nuove selezionate
					sqlCmd = "insert  into T_OP_SLABS" +
							"		(" +
							"		  F_ID_OP" +
							"		, F_ID_T_AN_ARTICLES" +
							"		, F_GROUP" +
							"		, F_STATE" +
							"		, F_CUT_TYPE" +
							"		, F_TOT_AREA" +
							"		, F_USED_AREA" +
							"		)" +
							"		select  " + prodOrderId.ToString() +
							"			  , F_ID" +
							"			  , 0" +
							"			  , 0" +
							"			  , 0" +
							"			  , F_SURFACE" +
							"			  , 0" +
							"		from    T_AN_ARTICLES" +
							"		where   F_ID in " + sqlIdList +
							"				and F_ID not in ( select    F_ID_T_AN_ARTICLES" +
							"								  from      T_OP_SLABS" +
							"								  where     F_ID_OP = " + prodOrderId.ToString() + " )" +
							"				and F_ID not in ( select    F_ID_T_AN_ARTICLES" +	// non inserisce le lastre in lista di estrazione
							"								  from      T_WK_USE_LISTS )" +
							"				and F_ID not in ( select    F_ID_T_AN_ARTICLES" +	// non inserisce le lastre in lista di estrazione
							"								  from      T_WK_ARTICLE_USE_LISTS )";


					if (!mDbInterface.Execute(sqlCmd))
						throw (new ApplicationException("OpSlabCollection.SelectOpSlabs: error inserting new slabs"));

					if (transaction) mDbInterface.EndTransaction(true);
					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OpSlabCollection.SelectOpSlabs Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("OpSlabCollection.SelectOpSlabs: Error", ex);
					return false;
				}
				finally
				{
					mDbInterface.EndRequery(dr);
				}
			}

			return false;
		}

		///**********************************************************************
		/// <summary>
		/// Set state for OpSlab
		/// </summary>
		/// <param name="OpSlabId">id opSlab</param>
		/// <param name="state">state</param>
		/// <param name="cut">cut type</param>
		/// <param name="usedArea">used surface</param>
		/// <returns>
		///			true if succedeed
		///			false otherwise
		///	</returns>
		///**********************************************************************
		public bool SetOpSlabState(int OpSlabId, OpSlab.State state, OpSlab.CutType cut, double usedArea)
		{
			int retry = 0;
			string sqlCmd;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Aggiorna lo stato dell'ordine di produzione
					sqlCmd = "update T_OP_SLABS set F_STATE = " + ((int)state).ToString();
					
					// Cambia lo stato
					if (cut != OpSlab.CutType.Undef)
						sqlCmd += ", F_CUT_TYPE = " + ((int)cut).ToString();

					// Setta l'area utilizzata
					if (usedArea >= 0d)
						sqlCmd += ", F_USED_AREA = " + usedArea.ToString();

					sqlCmd += " where F_ID_OP_SLAB = " + OpSlabId.ToString();
					if (!mDbInterface.Execute(sqlCmd))
						throw (new ApplicationException("OpSlabCollection.SetOpSlabState: error changing slab state"));

					if (transaction) mDbInterface.EndTransaction(true);
					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OpSlabCollection.SetOpSlabState Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("OpSlabCollection.SetOpSlabState: Error", ex);
					return false;
				}
			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Blocca la lastra indicata
		/// </summary>
		/// <param name="idSlab">id della lastra da bloccare</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		public bool LockSlab(int idSlab)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			// Verifica che l'id lastra sia valido
			if (idSlab <= 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					// Blocca la lastra
					par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					par.Direction = System.Data.ParameterDirection.ReturnValue;
					parameters.Add(par);

					par = new OleDbParameter("@slabid", OleDbType.Integer);
					par.Value = idSlab;
					parameters.Add(par);

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoLockSlab", ref parameters))
						throw new ApplicationException("OpSlabCollection.LockSlab: Execute error.");

					// Verifica il codice di ritorno
					par = (OleDbParameter)parameters[0];
					if (((int)par.Value) != 0)
						throw new ApplicationException("OpSlabCollection.LockSlab sp_PoLockSlab error: " + ((int)par.Value));

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OpSlabCollection.LockSlab: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("OpSlabCollection.LockSlab: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Sblocca la lastra indicata
		/// </summary>
		/// <param name="idSlab">id della lastra da sbloccare</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		/// </returns>
		///*********************************************************************
		public bool UnlockSlab(int idSlab)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			// Verifica che l'id lastra sia valido
			if (idSlab == 0)
				return true;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					// Salva i risultati attuali
					par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					par.Direction = System.Data.ParameterDirection.ReturnValue;
					parameters.Add(par);

					par = new OleDbParameter("@slabid", OleDbType.Integer);
					par.Value = idSlab;
					parameters.Add(par);

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoUnlockSlab", ref parameters))
						throw new ApplicationException("OpSlabCollection.UnlockSlab: Execute error.");

					// Verifica il codice di ritorno
					par = (OleDbParameter)parameters[0];
					if (((int)par.Value) != 0)
						throw new ApplicationException("OpSlabCollection.UnlockSlab sp_PoUnlockSlab error: " + ((int)par.Value));

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OpSlabCollection.UnlockSlab: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("OpSlabCollection.UnlockSlab: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}

			return false;
		}

		///**********************************************************************
		/// <summary>
		/// Ritorna il numero totale delle lastre di un ordine di produzione.
		/// </summary>
		/// <param name="idOp">id ordine di produzione</param>
		/// <param name="state">stato delle lastre da considerare</param>
		/// <returns>
		///		>= 0	numero di pezzi totali da realizzare.
		///		-1		errore
		///	</returns>
		///**********************************************************************
		public int SlabQuantity(int idOp, OpSlab.State state)
		{
			OleDbDataReader dr = null;
			string sqlQuery = "select count(F_ID_OP_SLAB) as REQ_QTA from T_OP_SLABS" +
							  " where F_ID_OP = " + idOp.ToString();

			if (state != OpSlab.State.Undef)
				sqlQuery += " and F_STATE = " + ((int)state).ToString();

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return -1;

				// Ritorna la quantit� di formati
				if (dr.Read())
					return (int)dr[0];
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("OpSlabCollection.SlabQuantity: Error", ex);
				return -1;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			return -1;
		}

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion

		#region Private Methods
		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, secondo la query specificata
		/// </summary>
		/// <param name="sqlQuery">query</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	errore
		///	</returns>
		///*********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			OpSlab opSlab;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					opSlab = new OpSlab((int) dr["F_ID_OP_SLAB"],
						(int) dr["F_ID_OP"],
						(int) dr["F_ID_T_AN_ARTICLES"],
						(int) dr["F_GROUP"],
						(OpSlab.State) dr["F_STATE"],
						(OpSlab.CutType) dr["F_CUT_TYPE"],
						(double) dr["F_TOT_AREA"],
						(double) dr["F_USED_AREA"]);
					
					AddObject(opSlab);
					opSlab.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("OpSlabCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}
}
