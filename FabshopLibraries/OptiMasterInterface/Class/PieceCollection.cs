using System;
using System.Threading;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public abstract class PieceCollection: DbCollection
	{
		#region Variables

		#endregion


		#region Constructors

		public PieceCollection(DbInterface db): base(db)
		{
		}
		
		#endregion


		#region Public Methods

		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Piece obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (Piece) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Piece obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Piece) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Piece obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		///**********************************************************************
		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id elemento</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			int[] ids = new int[1];
			ids[0] = id;
			return GetDataFromDb(Piece.Keys.F_NONE, ids, null, null, null);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Piece.Keys.F_NONE, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Piece.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null, null);
		}

		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, dei gruppi indicati
		/// </summary>
		/// <param name="groupId">vettore id gruppi</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetGroupPiecesFromDb(int[] groupId)
		{
			return GetDataFromDb(Piece.Keys.F_ID_PIECE, null, groupId, null, null);
		}

		///*********************************************************************
		/// <summary>
		/// Legge i pezzi associati a dei formati
		/// </summary>
		/// <param name="shapeId">vettore id formati</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///*********************************************************************
		public bool GetShapePiecesFromDb(int[] shapeId)
		{
			return GetDataFromDb(Piece.Keys.F_ID_PIECE, null, null, shapeId, null);
		}
		
		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="ids">estrae gli elemento con gli id indicati</param>
		/// <param name="groupId">estrae gli elementi dei gruppi indicati</param>
		/// <param name="shapeId">estrae gli elementi dei formati indicati</param>
		/// <param name="opIds">estrae gli elementi degli ordini di produzione indicati</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public abstract bool GetDataFromDb(Piece.Keys orderKey, int[] ids, int[] groupIds, int[] shapeIds, int[] opIds);

		///*********************************************************************
		/// <summary>
		/// Legge il poligono xml dal database
		/// </summary>
		/// <param name="piece">formato di cui leggere il poligono</param>
		/// <param name="fileName">nome del file su cui memorizzare il poligono</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	lettura non eseguita
		///	</returns>
		///*********************************************************************
		public abstract bool GetXmlPolygonFromDb(Piece piece, string fileName);
		
		///*********************************************************************
		/// <summary>
		/// Scrive il poligono xml nel database
		/// </summary>
		/// <param name="piece">formato di cui scrivere il poligono</param>
		/// <param name="fileName">nome del file da cui prelevare il poligono</param>
		/// <returns>
		///		true	scrittura eseguita
		///		false	scrittura non eseguita
		///	</returns>
		///*********************************************************************
		public abstract bool SetXmlPolygonToDb(Piece piece, string fileName);

		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion


		#region Private Methods
		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, secondo la query specificata
		/// </summary>
		/// <param name="sqlQuery">query</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	errore
		///	</returns>
		///*********************************************************************
		protected abstract bool GetDataFromDb(string sqlQuery);

		#endregion
	}
}
