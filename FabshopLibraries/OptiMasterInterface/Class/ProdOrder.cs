using System;
using Breton.DbAccess;

namespace Breton.OptiMasterInterface
{
	public class ProdOrder: DbObject
	{
		#region Variables, Struct & Enums

		public enum State
        { Undef = -1, Available, Programmed, Confirmed, Busy, Completed, Suspended, Locked, Max };

		public enum ShapeType
		{ Undef = -1, Rectangles, Polygons};

		private int mId;
		private int mSequenceId;
		private State mStateOrd;
		private DateTime mStartDate, mStopDate;
		private string mDescription;
		private int mMatId;
		private string mMatCode;
		private double mThickness;
		private ShapeType mShapeType;
		private int mWorkCenterGroupId;
		private string mWorkCenterGroupCode;
		private OpSlabCollection mOpSlabs;

		public enum Keys {F_NONE = -1, F_ID_OP, F_ID_PRO_SEQUENCE, F_STATE, F_DT_START, F_DT_STOP, 
			F_DESCRIPTION, F_ID_T_AN_MATERIALS, F_MAT_CODE, F_THICKNESS, F_SHAPE_TYPE, F_ID_T_AN_WORK_CENTER_GROUPS, F_WORK_CENTER_GROUP_CODE, F_MAX};

		#endregion 

		#region Constructors
		///**********************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="id"></param>
		/// <param name="sequenceId"></param>
		/// <param name="state"></param>
		/// <param name="startDate"></param>
		/// <param name="stopDate"></param>
		/// <param name="description"></param>
		/// <param name="matId"></param>
		/// <param name="matCode"></param>
		/// <param name="thickness"></param>
		///**********************************************************************
		public ProdOrder(int id, int sequenceId, State state, DateTime startDate, DateTime stopDate, 
			string description, int matId, string matCode, double thickness, ShapeType shapeType, 
			int workCenterGroupId, string workCenterGroupCode)
		{
			mId = id;
			mSequenceId = sequenceId;
			mStateOrd = state;
			mStartDate = startDate;
			mStopDate = stopDate;
			mDescription = description;
			mMatId = matId;
			mMatCode = matCode;
			mThickness = thickness;
			mShapeType = shapeType;
			mWorkCenterGroupId = workCenterGroupId;
			mWorkCenterGroupCode = workCenterGroupCode;
			mOpSlabs = null;
		}

		public ProdOrder():this(0, 0, State.Undef, DateTime.MinValue, DateTime.MinValue, "", 0, "", 0d, ShapeType.Undef, 0, "")
		{
		}
		#endregion

		#region Properties
		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public int gSequenceId
		{
			set 
			{
				mSequenceId = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mSequenceId; }
		}

		public State gStateOrd
		{
			set
			{
				mStateOrd = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mStateOrd;	}
		}

		public DateTime gStartDate
		{
			set 
			{
				mStartDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mStartDate; }
		}

		public DateTime gStopDate
		{
			set 
			{
				mStopDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mStopDate; }
		}

		public string gDescription
		{
			set 
			{
				mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		public int gMatId
		{
			set 
			{
				mMatId = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mMatId; }
		}

		public string gMatCode
		{
			set 
			{
				mMatCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mMatCode; }
		}

		public double gThickness
		{
			set 
			{
				mThickness = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mThickness; }
		}

		public ShapeType gShapeType
		{
			set
			{
				mShapeType = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mShapeType;	}
		}

		public int gWorkCenterGroupId
		{
			set
			{
				mWorkCenterGroupId = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mWorkCenterGroupId; }
		}

		public string gWorkCenterGroupCode
		{
			set
			{
				mWorkCenterGroupCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mWorkCenterGroupCode; }
		}

		public OpSlabCollection gOpSlabs
		{
			get { return mOpSlabs; }
			set { mOpSlabs = value; }
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is ProdOrder)
			{
				ProdOrder po = (ProdOrder) obj;

				int cmp = 0;

				cmp = mSequenceId.CompareTo(po.mSequenceId);

				if (cmp == 0)
				{
					cmp = mMatCode.CompareTo(po.mMatCode);

					if (cmp == 0)
					{
						cmp = mThickness.CompareTo(po.mThickness);

						if (cmp == 0)
						{
							cmp = mShapeType.CompareTo(po.mShapeType);

							if (cmp == 0)
							{
								cmp = mWorkCenterGroupCode.CompareTo(po.mWorkCenterGroupCode);
							}

						}
					}
				}

				return cmp;
			}

			throw new ArgumentException("object is not a ProdOrder");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == ProdOrder.Keys.F_ID_OP.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_ID_PRO_SEQUENCE.ToString())
				return mSequenceId.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_STATE.ToString())
				return mStateOrd.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_DT_START.ToString())
				return mStartDate.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_DT_STOP.ToString())
				return mStopDate.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_DESCRIPTION.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_ID_T_AN_MATERIALS.ToString())
				return mMatId.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_MAT_CODE.ToString())
				return mMatCode.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_THICKNESS.ToString())
				return mThickness.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_SHAPE_TYPE.ToString())
				return mShapeType.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_ID_T_AN_WORK_CENTER_GROUPS.ToString())
				return mWorkCenterGroupId.GetTypeCode();
			if (key.ToUpper() == ProdOrder.Keys.F_WORK_CENTER_GROUP_CODE.ToString())
				return mWorkCenterGroupCode.GetTypeCode();

			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out DateTime retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == ProdOrder.Keys.F_ID_OP.ToString())
			{
				retValue = mId;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_ID_PRO_SEQUENCE.ToString())
			{
				retValue = mSequenceId;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_ID_T_AN_MATERIALS.ToString())
			{
				retValue = mMatId;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_STATE.ToString())
			{
				retValue = (int) mStateOrd;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_SHAPE_TYPE.ToString())
			{
				retValue = (int) mShapeType;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_ID_T_AN_WORK_CENTER_GROUPS.ToString())
			{
				retValue = mWorkCenterGroupId;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out double retValue)
		{
			if (key.ToUpper() == ProdOrder.Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == ProdOrder.Keys.F_ID_OP.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_ID_PRO_SEQUENCE.ToString())
			{
				retValue = mSequenceId.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_ID_T_AN_MATERIALS.ToString())
			{
				retValue = mMatId.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_STATE.ToString())
			{
				retValue = mStateOrd.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_DT_START.ToString())
			{
				retValue = mStartDate.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_DT_STOP.ToString())
			{
				retValue = mStopDate.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_MAT_CODE.ToString())
			{
				retValue = mMatCode;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_SHAPE_TYPE.ToString())
			{
				retValue = mShapeType.ToString();
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_WORK_CENTER_GROUP_CODE.ToString())
			{
				retValue = mWorkCenterGroupCode;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{
			if (key.ToUpper() == ProdOrder.Keys.F_DT_START.ToString())
			{
				retValue = mStartDate;
				return true;
			}

			if (key.ToUpper() == ProdOrder.Keys.F_DT_STOP.ToString())
			{
				retValue = mStopDate;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		///**********************************************************************
		/// <summary>
		/// Legge le lastre associate all'ordine di produzione dal database
		/// </summary>
		/// <param name="db">db interface</param>
		/// <param name="state">stato lastre</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///**********************************************************************
		public bool GetOpSlabsFromDb(DbInterface db)
		{
			return GetOpSlabsFromDb(db, OpSlab.State.Undef);
		}
		public bool GetOpSlabsFromDb(DbInterface db, OpSlab.State state)
		{
			// Crea la lista degli ordini di produzione
			if (mOpSlabs == null)
				mOpSlabs = new OpSlabCollection(db);

			mOpSlabs.Clear();

			return mOpSlabs.GetDataFromDb(mId, 0, state);
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}
		#endregion

	}
}
