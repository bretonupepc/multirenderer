using System;
using System.Xml;
using Breton.DbAccess;
using Breton.Polygons;
using Breton.MathUtils;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe astratta che implementa un gruppo (filagna) delle lastre, tipo FK
	/// </summary>
	///*************************************************************************
	public abstract class Group: DbObject
	{
		#region Structs, Enums & Variables

		// Stato gruppo
		public enum State
		{   Undef = -1, 
            Ready,					// gruppo pronto
            InProgress,				// gruppo in lavoro
            Unloading,				// gruppo in scarico
            Completed,				// gruppo lavorato
    };

		// Tipo gruppo
		public enum GroupType
		{
			Undef = -1,
			Quadrants,				// quadrante
			Longitudinal,			// filagna longitudinale
			Transversal,			// filagna trasversale
			LongitudinalRaf,		// raffilo longitudinale
			TransversalRaf,			// raffilo trasversale
			Polygon					// poligono
		}

		public enum LinkIndex
		{
			RaffSide2 = -5,					// raffilo lato 2
			RaffSide1 = -4,					// raffilo lato 1
			NoCuts = -3,					// filagna senza tagli
			Grouped = -2,					// filagna raggruppata
			RaffBreaked = -1,				// raffilo rotto
			Single = 0,						// filagna singola
			StartGrouped = 1,				// inizio gruppo filagne
			Ortho = 1000					// filagna ortogonale
		}

		protected int mIdOp;										// id ordine di produzione
		protected int mId;										// id gruppo
		protected int mIdSlab;									// id lastra
		protected GroupType mType;								// tipo gruppo
		protected double mPosX;									// posizione X all'interno della lastra
		protected double mPosY;									// posizione Y all'interno della lastra
		protected double mLength;									// dimensione X
		protected double mWidth;									// dimensione Y
		protected double mResidue1;								// dimensione residuo di testa
		protected double mResidue2;								// dimensione residuo di coda
		protected State mGroupState;								// stato gruppo
		protected LinkIndex mLink;								// indice link gruppo
		protected double mOffsetAlign;							// offset allineamento
		protected string mCode;									// codice

		protected PieceCollection mPieces;						// pezzi associati al gruppo

		public enum Keys
		{ F_NONE = -1, F_ID_GROUP, F_ID_SLAB, F_TYPE, F_X_POS, F_Y_POS, F_LENGTH, F_WIDTH, F_DIM_RES_1, F_DIM_RES_2, F_STATE, F_LINK_INDEX, F_OFFSET_ALIGN, F_CODE, F_MAX };

		#endregion


		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore base
		/// </summary>
		///**********************************************************************
		public Group(int idOp, int id, int idSlab, GroupType type, double posX, double posY, double length, double width, double residue1, double residue2, State groupState, LinkIndex link, double offsetAlign, string code)
		{
			mIdOp = idOp;
			mId = id;
			mIdSlab = idSlab;
			mType = type;
			mGroupState = groupState;
			mPosX = posX;
			mPosY = posY;
			mLength = length;
			mWidth = width;
			mResidue1 = residue1;
			mResidue2 = residue2;
			mGroupState = groupState;
			mLink = link;
			mOffsetAlign = offsetAlign;
			mCode = code;

			mPieces = null;
		}

		public Group():this(0, 0, 0, GroupType.Undef, 0d, 0d, 0d, 0d, 0d, 0d, State.Undef, LinkIndex.Single, 0d, "")
		{
		}

		#endregion


		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public int gIdSlab
		{
			set
			{
				mIdSlab = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mIdSlab;	}
		}

		public int gIdOp
		{
			set
			{
				mIdOp = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mIdOp; }
		}

		public GroupType gType
		{
			set
			{
				mType = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mType; }
		}

		public double gPosX
		{
			set
			{
				mPosX = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mPosX; }
		}

		public double gPosY
		{
			set
			{
				mPosY = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mPosY; }
		}

		public double gLength
		{
			set
			{
				mLength = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mLength; }
		}

		public double gWidth
		{
			set
			{
				mWidth = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mWidth; }
		}

		public double gResidue1
		{
			set
			{
				mResidue1 = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mResidue1; }
		}

		public double gResidue2
		{
			set
			{
				mResidue2 = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mResidue2; }
		}

		public Group.State gGroupState
		{
			set
			{
				mGroupState = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mGroupState; }
		}

		public LinkIndex gLinkIndex
		{
			set
			{
				mLink = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mLink; }
		}

		public double gOffsetAlign
		{
			set
			{
				mOffsetAlign = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mOffsetAlign; }
		}

		public string gCode
		{
			set
			{
				mCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public PieceCollection gPieces
		{
			get { return mPieces; }
			set { mPieces = value; }
		}

		#endregion


		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Group)
			{
				Group group = (Group) obj;

				int cmp = 0;

				cmp = mId.CompareTo(group.mId);

				return cmp;
			}

			throw new ArgumentException("object is not a Group");
		}

		#endregion


		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	
		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			return base.GetField(key, out retValue);
		}
		
		///**********************************************************************
		/// <summary>
		/// Legge i pezzi associati al gruppo dal database
		/// </summary>
		/// <param name="db">db interface</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///**********************************************************************
		public virtual bool GetPiecesFromDb(DbInterface db)
		{
			// Crea la lista dei pezzi
			if (mPieces == null)
				return false;

			mPieces.Clear();

			int[] ids = new int[1];
			ids[0] = mId;
			return mPieces.GetGroupPiecesFromDb(ids);
		}

		#endregion


		#region Private & Protected Methods

		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		///***************************************************************************
		/// <summary>
		/// Imposta l'id del gruppo ai pezzi collegati
		/// </summary>
		///***************************************************************************
		internal void SetGroupIdToPieces()
		{
			if (mPieces == null)
				return;

			Piece p;
			mPieces.MoveFirst();
			while (mPieces.GetObject(out p))
			{
				p.gIdGroup = mId;
				mPieces.MoveNext();
			}
		}
		#endregion

	}
}
