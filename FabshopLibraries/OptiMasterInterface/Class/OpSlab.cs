using System;
using Breton.DbAccess;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa una lastra del preottimizzatore
	/// </summary>
	///*************************************************************************
	public class OpSlab: DbObject
	{
		#region Structs, Enums & Variables

		// Stato lastra
		public enum State
		{   Undef = -1, 
            Available, 
            Programming, 
            Confirmed, 
            Working, 
            Completed, 
            Suspended, 
            Locked, 
            Programmed,
            Virtual = 1000,
            Virtual_Available = Virtual,
            Virtual_Programming, 
            Virtual_Confirmed, 
            Virtual_Working, 
            Virtual_Completed, 
            Virtual_Suspended, 
            Virtual_Locked, 
            Virtual_Programmed
    };

		// Tipo formato
		public enum CutType
		{ Undef = 0, Quadrants, Strips, Polygons, Profiles };

		private int mId;										// id lastra
		private int mIdOp;										// id ordine di produzione
		private int mIdArticle;									// id tabella articoli
		private int mGroup;										// gruppo
		private State mSlabState;								// stato lastra
		private CutType mCutType;								// tipo taglio
		private double mTotArea;								// area totale lastra
		private double mUsedArea;								// area utilizzata lastra

		private GroupOpCollection mOpGroups;					// Gruppi associati alla lastra

		public enum Keys 
		{F_NONE = -1, F_ID_OP_SLAB, F_ID_OP, F_ID_T_AN_ARTICLES, F_GROUP, F_STATE, F_CUT_TYPE, F_TOT_AREA, F_USED_AREA, F_MAX};

		#endregion

		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		/// <param name="id"></param>
		/// <param name="idOp"></param>
		/// <param name="idArticle"></param>
		/// <param name="group"></param>
		/// <param name="state"></param>
		/// <param name="cutType"></param>
		///**********************************************************************
		public OpSlab(int id, int idOp, int idArticle, int group, OpSlab.State state, OpSlab.CutType cutType, double totArea, double usedArea)
		{
			mId = id;
			mIdOp = idOp;
			mIdArticle = idArticle;
			mGroup = group;
			mSlabState = state;
			mCutType = cutType;
			mTotArea = totArea;
			mUsedArea = usedArea;

			mOpGroups = null;
		}

		public OpSlab():this(0, 0, 0, 0, OpSlab.State.Undef, OpSlab.CutType.Undef, 0d, 0d)
		{
		}

		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public int gIdOp
		{
			set
			{
				mIdOp = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mIdOp;	}
		}

		public int gIdArticle
		{
			set
			{
				mIdArticle = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mIdArticle; }
		}

		public int gGroup
		{
			set 
			{
				mGroup = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mGroup; }
		}

		public OpSlab.State gSlabState
		{
			set 
			{
				mSlabState = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mSlabState; }
		}

		public OpSlab.CutType gCutType
		{
			set 
			{
				mCutType = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCutType; }
		}

		public double gTotArea
		{
			set
			{
				mTotArea = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mTotArea; }
		}

		public double gUsedArea
		{
			set
			{
				mUsedArea = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mUsedArea; }
		}

		public GroupOpCollection gOpGroups
		{
			get { return mOpGroups; }
			set { mOpGroups = value; }
		}
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is OpSlab)
			{
				OpSlab slab = (OpSlab) obj;

				int cmp = 0;

				cmp = mIdOp.CompareTo(slab.mIdOp);

				if (cmp == 0)
				{
					cmp = mGroup.CompareTo(slab.mGroup);

					if (cmp == 0)
						cmp = mCutType.CompareTo(slab.mCutType);
				}

				return cmp;
			}

			throw new ArgumentException("object is not a OpSlab");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			return base.GetField(key, out retValue);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i pezzi associati al gruppo dal database
		/// </summary>
		/// <param name="db">db interface</param>
		/// <returns>
		///		true	ok
		///		false	errore
		///	</returns>
		///**********************************************************************
		public bool GetGroupsFromDb(DbInterface db)
		{
			// Crea la lista dei gruppi
			if (mOpGroups == null)
				mOpGroups = new GroupOpCollection(db);

			mOpGroups.Clear();

			int[] ids = new int[1];
			ids[0] = mId;
			return mOpGroups.GetDataFromDb(Group.Keys.F_NONE, null, ids, null);
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}
		#endregion

	}
}
