using System;
using Breton.DbAccess;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa una gruppo di lastre del preottimizzatore
	/// </summary>
	///*************************************************************************
	public class SlabGroup: DbObject
	{
		#region Structs, Enums & Variables

		private int mId;										// id lastra
		private int mIdOp;										// id ordine di produzione
		private int mGroup;										// gruppo
        private double mLength;                                 // Lunghezza
        private double mWidth;                                  // Larghezza

		public enum Keys 
		{F_NONE = -1, F_ID_OP_SLAB_GROUP, F_ID_OP, F_GROUP, F_LENGTH, F_WIDTH, F_MAX};

		#endregion

		#region Constructors & Disposers
        
        ///**********************************************************************
        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idOp"></param>
        /// <param name="group"></param>
        /// <param name="length"></param>
        /// <param name="width"></param>
        ///**********************************************************************
        public SlabGroup(int id, int idOp, int group, double length, double width)
		{
			mId = id;
			mIdOp = idOp;
			mGroup = group;
			mLength = length;
			mWidth = width;
		}

		public SlabGroup():this(0, 0, 0, 0d, 0d)
		{
		}

		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public int gIdOp
		{
			set
			{
				mIdOp = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get	{ return mIdOp;	}
		}

		public int gGroup
		{
			set 
			{
				mGroup = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mGroup; }
		}

		public double gLength
		{
			set 
			{
				mLength = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mLength; }
		}

		public double gWidth
		{
			set 
			{
				mWidth = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mWidth; }
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is SlabGroup)
			{
				SlabGroup slab = (SlabGroup) obj;

				int cmp = 0;

				cmp = mIdOp.CompareTo(slab.mIdOp);

				if (cmp == 0)
				{
					cmp = mGroup.CompareTo(slab.mGroup);
				}

				return cmp;
			}

			throw new ArgumentException("object is not a SlabGroup");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			return base.GetField(key, out retValue);
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}
		#endregion

	}
}
