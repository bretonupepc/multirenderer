using System;
using System.Threading;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public class GroupOkCollection: GroupCollection
	{
		#region Variables

		#endregion


		#region Constructors

		public GroupOkCollection(DbInterface db): base(db)
		{
		}
		
		#endregion


		#region Public Methods

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="ids">estrae gli elemento con gli id indicati</param>
		/// <param name="slabIds">estrae gli elementi delle lastre indicate</param>
		/// <param name="opIds">estrae gli elementi degli ordini di produzione indicati</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb(Group.Keys orderKey, int[] ids, int[] slabIds, int[] opIds)
		{
			string sqlQuery;
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo i gruppi richiesti
			if (ids != null && ids.Length > 0)
			{
				sqlWhere += sqlAnd + "F_ID_GROUP in (" + ids[0];

				for (int i = 1; i < ids.Length; i++)
					sqlWhere += ", " + ids[i];
				sqlWhere += ")";

				sqlAnd = " and ";
			}

			// Estrae i gruppi delle lastre richieste
			if (slabIds != null && slabIds.Length > 0)
			{
				sqlWhere += sqlAnd + "F_ID_SLAB in (" + slabIds[0];

				for (int i = 1; i < slabIds.Length; i++)
					sqlWhere += ", " + slabIds[i];
				sqlWhere += ")";

				sqlAnd = " and ";
			}

			// Estrae i gruppi degli ordini di produzione
			if (opIds != null && opIds.Length > 0)
				return GetDataFromDb("");

			switch (orderKey)
			{
				case Group.Keys.F_ID_GROUP:
					sqlOrderBy = " order by F_ID_GROUP";
					break;
				case Group.Keys.F_ID_SLAB:
					sqlOrderBy = " order by F_ID_SLAB";
					break;
			}

			sqlQuery = "select g.* from T_GROUPS g";
			
			return GetDataFromDb(sqlQuery + sqlWhere + sqlOrderBy);
		}

		///**********************************************************************
		/// <summary>
		/// Aggiorna i dati nel database
		/// </summary>
		/// <returns>
		///				true	operazione eseguita con successo
		///				false	altrimenti
		/// </returns>
		///**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Group group in mRecordset)
					{
						switch (group.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_GROUPS (" +
									" F_ID_SLAB" +
									", F_TYPE" +
									", F_X_POS" +
									", F_Y_POS" +
									", F_LENGTH" +
									", F_WIDTH" +
									", F_DIM_RES_1" +
									", F_DIM_RES_2" +
									", F_STATE" +
									", F_LINK_INDEX" +
									", F_OFFSET_ALIGN" +
									(group.gCode == "" ? "" : ", F_CODE") +
									")" +
									" values " +
									"(" + group.gIdSlab +
									", " + ((int)group.gType) +
									", " + group.gPosX.ToString() +
									", " + group.gPosY.ToString() +
									", " + group.gLength.ToString() +
									", " + group.gWidth.ToString() +
									", " + group.gResidue1.ToString() +
									", " + group.gResidue2.ToString() +
									", " + ((int)group.gGroupState) +
									", " + ((int)group.gLinkIndex) +
									", " + group.gOffsetAlign.ToString() +
									(group.gCode == "" ? "" : ", '" + group.gCode + "'") +
									")" +
									";select SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								group.gId = id;
								group.SetGroupIdToPieces();

								// Aggiorna i pezzi
								if (group.gPieces != null)
									if (!group.gPieces.UpdateDataToDb())
										throw new ApplicationException("GroupOkCollection.UpdateDataToDb update pieces error.");

								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_GROUPS set" +
									" F_ID_SLAB = " + group.gIdSlab +
									", F_TYPE =" + ((int)group.gType) +
									", F_X_POS =" + group.gPosX.ToString() +
									", F_Y_POS =" + group.gPosY.ToString() +
									", F_LENGTH =" + group.gLength.ToString() +
									", F_WIDTH =" + group.gWidth.ToString() +
									", F_DIM_RES_1 =" + group.gResidue1.ToString() +
									", F_DIM_RES_2 =" + group.gResidue2.ToString() +
									", F_STATE =" + ((int)group.gGroupState) +
									", F_LINK_INDEX =" + ((int)group.gLinkIndex) +
									", F_OFFSET_ALIGN =" + group.gOffsetAlign.ToString() +
									", F_CODE = '" + group.gCode + "'";
							
								sqlUpdate += " where F_ID_GROUP = " + group.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;

								// Aggiorna i pezzi
								if (group.gPieces != null)
									if (!group.gPieces.UpdateDataToDb())
										throw new ApplicationException("GroupOkCollection.UpdateDataToDb update pieces error.");

								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Group group in mDeleted)
					{
						switch (group.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_GROUPS where F_ID_GROUP = " + group.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					if (mSqlGetData != "")
						GetDataFromDb(mSqlGetData);
					else
					{
						int[] ids = new int[mRecordset.Count];
						for (int i = 0; i < mRecordset.Count; i++)
							ids[i] = ((Group)mRecordset[i]).gId;

						GetDataFromDb(Group.Keys.F_ID_GROUP, ids, null, null);
						mSqlGetData = "";
					}

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("GroupOkCollection: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("GroupOkCollection: Error", ex);

					// Annulla la transazione
					if (transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		#endregion


		#region Private Methods

		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, secondo la query specificata
		/// </summary>
		/// <param name="sqlQuery">query</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	errore
		///	</returns>
		///*********************************************************************
		protected override bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Group group;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					group = new GroupOk((int) dr["F_ID_GROUP"],
						(int) dr["F_ID_SLAB"],
						(Group.GroupType)dr["F_TYPE"],
						(double)(float)dr["F_X_POS"],
						(double)(float)dr["F_Y_POS"],
						(double)(float)dr["F_LENGTH"],
						(double)(float)dr["F_WIDTH"],
						(double)(float)dr["F_DIM_RES_1"],
						(double)(float)dr["F_DIM_RES_2"],
						(Group.State)dr["F_STATE"],
						(Group.LinkIndex)dr["F_LINK_INDEX"],
						(double)(float)dr["F_OFFSET_ALIGN"],
						(dr["F_CODE"] == System.DBNull.Value ? "" : dr["F_CODE"].ToString()));

					AddObject(group);
					group.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("GroupOkCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}
}
