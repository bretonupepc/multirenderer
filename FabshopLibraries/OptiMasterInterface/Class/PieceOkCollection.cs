using System;
using System.Threading;
using System.Data.OleDb;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public class PieceOkCollection: PieceCollection
	{
		#region Variables

		#endregion


		#region Constructors

		public PieceOkCollection(DbInterface db): base(db)
		{
		}
		
		#endregion


		#region Public Methods
		
		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="ids">estrae gli elemento con gli id indicati</param>
		/// <param name="groupId">estrae gli elementi dei gruppi indicati</param>
		/// <param name="shapeId">estrae gli elementi dei formati indicati</param>
		/// <param name="opIds">estrae gli elementi degli ordini di produzione indicati</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb(Piece.Keys orderKey, int[] ids, int[] groupIds, int[] shapeIds, int[] opIds)
		{
			string sqlQuery;
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo il pezzo con il codice richiesto
			if (ids != null && ids.Length > 0)
			{
				sqlWhere += sqlAnd + "F_ID_PIECE in (" + ids[0];

				for (int i = 1; i < ids.Length; i++)
					sqlWhere += ", " + ids[i];
				sqlWhere += ")";

				sqlAnd = " and ";
			}

			// Estrae i pezzi delle filagne richieste
			if (groupIds != null && groupIds.Length > 0)
			{
				sqlWhere += sqlAnd + "F_ID_GROUP in (" + groupIds[0];

				for (int i = 1; i < groupIds.Length; i++)
					sqlWhere += ", " + groupIds[i];
				sqlWhere += ")";

				sqlAnd = " and ";
			}

			// Estrae i pezzi dei formati richiesti
			if (shapeIds != null && shapeIds.Length > 0)
			{
				sqlWhere += sqlAnd + "F_ID_SHAPE in (" + shapeIds[0];

				for (int i = 1; i < shapeIds.Length; i++)
					sqlWhere += ", " + shapeIds[i];
				sqlWhere += ")";

				sqlAnd = " and ";
			}

			// Estrae i pezzi degli ordini di produzione
			if (opIds != null && opIds.Length > 0)
				return GetDataFromDb("");

			switch (orderKey)
			{
				case Piece.Keys.F_ID_PIECE:
					sqlOrderBy = " order by p.F_ID_PIECE";
					break;
				case Piece.Keys.F_ID_GROUP:
					sqlOrderBy = " order by p.F_ID_GROUP";
					break;
				case Piece.Keys.F_ID_SHAPE:
					sqlOrderBy = " order by p.F_ID_SHAPE";
					break;
			}

			sqlQuery = "select p.*, s.F_LENGTH, s.F_WIDTH from T_PIECES p" +
				" inner join T_SHAPES s on s.F_ID_SHAPE = p.F_ID_SHAPE";
			
			return GetDataFromDb(sqlQuery + sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Piece piece in mRecordset)
					{
						switch (piece.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_PIECES (" + 
									" F_ID_GROUP" +
									", F_ID_SHAPE" +
									", F_STATE" +
									", F_X_POS" +
									", F_Y_POS" + 
									", F_ROTATED" +
									", F_DISCARD" +
									", F_TRASL_X" +
									", F_TRASL_Y" +
									", F_ROT_ANGLE" +
									(piece.gCode == "" ? "" : ", F_CODE") +
									")" +
									" values " +
									"(" + piece.gIdGroup +
									", " + piece.gIdShape + 
									", " + ((int)piece.gPieceState) + 
									", " + piece.gPosX.ToString() +
									", " + piece.gPosY.ToString() +
									", " + (piece.gRotated ? 1 : 0) +
									", " + ((int) piece.gType) +
									", " + piece.gTraslX.ToString() +
									", " + piece.gTraslY.ToString() +
									", " + piece.gRotAngle.ToString() +
									(piece.gCode == "" ? "" : ", '" + piece.gCode + "'") +
									")" +
									";select SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								piece.gId = id;

								// Aggiorna il poligono dei pezzi
								if (piece.gXmlPolygon != "")
									if (!SetXmlPolygonToDb(piece, piece.gXmlPolygon))
										throw new ApplicationException("PieceOkCollection.UpdateDataToDb update polygon piece error.");

								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_PIECES set" +
									" F_ID_GROUP = " + piece.gIdGroup +
									", F_ID_SHAPE = " + piece.gIdShape +
									", F_STATE = " + (int)piece.gPieceState +
									", F_X_POS = " + piece.gPosX +
									", F_Y_POS = " + piece.gPosY +
									", F_ROTATED = " + (piece.gRotated ? "1" : "0") +
									", F_DISCARD = " + ((int)piece.gType) +
									", F_TRASL_X = " + piece.gTraslX.ToString() +
									", F_TRASL_Y = " + piece.gTraslY.ToString() +
									", F_ROT_ANGLE = " + piece.gRotAngle.ToString() +
									", F_CODE = '" + piece.gCode + "'";
							
								sqlUpdate += " where F_ID_PIECE = " + piece.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;

								// Aggiorna il poligono dei pezzi
								if (piece.gXmlPolygon != "")
									if (!SetXmlPolygonToDb(piece, piece.gXmlPolygon))
										throw new ApplicationException("PieceOkCollection.UpdateDataToDb update polygon piece error.");

								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Piece piece in mDeleted)
					{
						switch (piece.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_PIECES where F_ID_PIECE = " + piece.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					if (mSqlGetData != "")
						GetDataFromDb(mSqlGetData);
					else
					{
						int[] ids = new int[mRecordset.Count];
						for (int i = 0; i < mRecordset.Count; i++)
							ids[i] = ((Piece)mRecordset[i]).gId;

						GetDataFromDb(Piece.Keys.F_ID_PIECE, ids, null, null, null);
						mSqlGetData = "";
					}

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PieceOkCollection: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("PieceOkCollection: Error", ex);

					// Annulla la transazione
					if (transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}
		
		///*********************************************************************
		/// <summary>
		/// Legge il poligono xml dal database
		/// </summary>
		/// <param name="piece">formato di cui leggere il poligono</param>
		/// <param name="fileName">nome del file su cui memorizzare il poligono</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	lettura non eseguita
		///	</returns>
		///*********************************************************************
		public override bool GetXmlPolygonFromDb(Piece piece, string fileName)
		{
			try
			{
				// Recupera l'immagine dal db
				if (!mDbInterface.GetBlobField("T_PIECES", "F_POLYGON", " F_ID_PIECE = " + piece.gId.ToString(), fileName))
					return false;
			}
				
				// Eccezione
			catch (Exception ex)
			{
				TraceLog.WriteLine("PieceOkCollection.GetXmlPolygonFromDb: Error", ex);
				return false;
			}

			piece.iSetXmlPolygon(fileName);
			return true;
		}
		
		///*********************************************************************
		/// <summary>
		/// Scrive il poligono xml nel database
		/// </summary>
		/// <param name="piece">formato di cui scrivere il poligono</param>
		/// <param name="fileName">nome del file da cui prelevare il poligono</param>
		/// <returns>
		///		true	scrittura eseguita
		///		false	scrittura non eseguita
		///	</returns>
		///*********************************************************************
		public override bool SetXmlPolygonToDb(Piece piece, string fileName)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Scrive l'immagine su db
					if (!mDbInterface.WriteBlobField("T_PIECES", "F_POLYGON", " F_ID_PIECE = " + piece.gId.ToString(), fileName))
						throw (new ApplicationException("PieceOkCollection.SetXmlPolygonToDb: error writing image"));

					// Termina la transazione con successo
					if (transaction) mDbInterface.EndTransaction(true);

					piece.iSetXmlPolygon(fileName);
					return true;
				}

					//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("PieceOkCollection.SetXmlPolygonToDb: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

				// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("PieceOkCollection.SetXmlPolygonToDb: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					return false;
				}

			}
			return false;
		}

		#endregion


		#region Private Methods
		///*********************************************************************
		/// <summary>
		/// Legge i dati dal database, secondo la query specificata
		/// </summary>
		/// <param name="sqlQuery">query</param>
		/// <returns>
		///		true	lettura eseguita
		///		false	errore
		///	</returns>
		///*********************************************************************
		protected override bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Piece piece;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					piece = new PieceOk((int)dr["F_ID_PIECE"],
						(int)dr["F_ID_GROUP"],
						(int)dr["F_ID_SHAPE"],
						(Piece.State)dr["F_STATE"],
						(double)dr["F_X_POS"],
						(double)dr["F_Y_POS"],
						((int)dr["F_ROTATED"] == 1 ? true : false),
						(Piece.PieceType)dr["F_DISCARD"],
						(double)dr["F_TRASL_X"],
						(double)dr["F_TRASL_Y"],
						(double)dr["F_ROT_ANGLE"],
						(dr["F_CODE"] == System.DBNull.Value ? "" : dr["F_CODE"].ToString()),
						(double)(float)dr["F_LENGTH"],
						(double)(float)dr["F_WIDTH"],
						(dr["F_POLYGON"] != System.DBNull.Value));

					AddObject(piece);
					piece.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("PieceOkCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}
}
