using System;
using System.Threading;
using System.Data.OleDb;
using System.IO;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Breton.DbAccess;
using Breton.TraceLoggers;

namespace Breton.OptiMasterInterface
{
	public class ProdOrderCollection: DbCollection, IDisposable
	{
		#region Variables

		private struct OpShapesType
		{
			public readonly int SequenceId;
			public readonly int OpOrderId;
			public readonly int OrderId;
			public readonly int ShapeId;
			public readonly int MaterialId;
			public readonly string MaterialCode;
			public readonly float Thickness;
			public readonly int ShapeType;
			public readonly int AvaliableQty;

			public OpShapesType(int sequenceId, int opOrderId, int orderId, int shapeId, int materialId, string materialCode, float thickness, int shapeType, int availableQty)
			{
				SequenceId = sequenceId;
				OpOrderId = opOrderId;
				OrderId = orderId;
				ShapeId = shapeId;
				MaterialId = materialId;
				MaterialCode = materialCode;
				Thickness = thickness;
				ShapeType = shapeType;
				AvaliableQty = availableQty;
			}
		}

		private struct SlabsInfo
		{
			public readonly int IdOpSlab;
			public readonly int IdArt;
			public readonly double Length;
			public readonly double Width;
			public readonly double Surface;
			public readonly int Defects;

			public SlabsInfo(int idOpSlab, int idArt, double length, double width, double surface, int defects)
			{
				IdOpSlab = idOpSlab;
				IdArt = idArt;
				Length = length;
				Width = width;
				Surface = surface;
				Defects = defects;
			}
			public SlabsInfo(double length, double width): this(0, 0, length, width, 0d, 0)
			{}

		}

		private string mTmpDirPath = System.IO.Path.GetTempPath() + "ProdOrderCollection";
		private DirectoryInfo mTmpDir;

		protected bool mDisposed = false;

		#endregion


		#region Constructors

		///*************************************************************************
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="db">db interface</param>
		///*************************************************************************
		public ProdOrderCollection(DbInterface db)
			: base(db)
		{
			// Crea la directory temporanea per la memorizzazione dei files
			mTmpDir = new DirectoryInfo(mTmpDirPath);
			mTmpDir.Create();
			mTmpDir.Attributes = FileAttributes.Hidden;
		}
		
		#endregion


		#region IDisposable Members

		///*************************************************************************
		/// <summary>
		/// Distruttore chiamato dall'esterno
		/// </summary>
		///*************************************************************************
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		///*************************************************************************
		/// <summary>
		/// Distruttore interno effettivo
		/// </summary>
		/// <param name="disposing">true = chiamata esplicita / false = Garbage Collector</param>
		///*************************************************************************
		protected virtual void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if (!this.mDisposed)
			{
				// Cancella la directory contenente i file temporanei
				if (mTmpDir.Exists)
					mTmpDir.Delete(true);

				mDisposed = true;
			}
		}

		#endregion


		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out ProdOrder obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (ProdOrder) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out ProdOrder obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (ProdOrder) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(ProdOrder obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		///**********************************************************************
		/// <summary>
		/// Legge un singolo elemento dal database
		/// </summary>
		/// <param name="id">id elemento</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetSingleElementFromDb(int id)
		{
			return GetDataFromDb(ProdOrder.Keys.F_NONE, 0, id, ProdOrder.State.Undef);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(ProdOrder.Keys.F_NONE, 0, 0, ProdOrder.State.Undef);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(ProdOrder.Keys orderKey)
		{
			return GetDataFromDb(orderKey, 0, 0, ProdOrder.State.Undef);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <param name="sequenceId">estrae solo gli elementi della sequenza specificata</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(ProdOrder.Keys orderKey, int sequenceId)
		{
			return GetDataFromDb(orderKey, sequenceId, 0, ProdOrder.State.Undef);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database non ordinati
		/// </summary>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(ProdOrder.Keys orderKey, int sequenceId, ProdOrder.State state)
		{
			return GetDataFromDb(orderKey, sequenceId, 0, state);
		}

		///**********************************************************************
		/// <summary>
		/// Legge i dati dal database, ordinati per la chiave specificata
		/// </summary>
		/// <param name="orderKey">Chiave di ordinamento</param>
		/// <param name="sequenceId">Estrae solo gli elementi della sequenza specificata</param>
		/// <param name="id">estrae solo l'elemento con l'id specificato</param>
		/// <param name="state">estrae solo gli elementi con lo stato specificato</param>
		/// <returns>	
		///				true	operazione eseguita con successo
		///				false	altrimenti
		///	</returns>
		///**********************************************************************
		public bool GetDataFromDb(ProdOrder.Keys orderKey, int sequenceId, int id, ProdOrder.State state)
		{
			return GetDataFromDb(orderKey, sequenceId, (id > 0 ? new int[] { id } : null), state);
		}

		public bool GetDataFromDb(ProdOrder.Keys orderKey, int sequenceId, int[] ids, ProdOrder.State state)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " where ";

			//Estrae solo i materiali appartenenti alla classe richiesta
			if (sequenceId > 0)
			{
				sqlWhere += sqlAnd + "[F_ID_PRO_SEQUENCE] = " + sequenceId.ToString();
				sqlAnd = " and ";
			}

			//Estrae solo il materiale con il codice richiesto
			if (ids != null && ids.Length > 0)
			{
				sqlWhere += sqlAnd + "[F_ID_OP] in (" + ids[0];

				for (int i = 1; i < ids.Length; i++)
					sqlWhere += ", " + ids[i];
				sqlWhere += ")";

				sqlAnd = " and ";
			}


			if (state != ProdOrder.State.Undef)
			{
				sqlWhere += sqlAnd + "[F_STATE] = " + ((int) state).ToString();
				sqlAnd = " and ";
			}

			switch (orderKey)
			{
				case ProdOrder.Keys.F_ID_OP:
					sqlOrderBy = " order by [T_OP].[F_ID_OP]";
					break;
				case ProdOrder.Keys.F_ID_PRO_SEQUENCE:
					sqlOrderBy = " order by [T_OP].[F_CODE]";
					break;
				case ProdOrder.Keys.F_DESCRIPTION:
					sqlOrderBy = " order by [T_OP].[F_DESCRIPTION]";
					break;
				case ProdOrder.Keys.F_MAT_CODE:
					sqlOrderBy = " order by [T_AN_MATERIALS].[F_CODE]";
					break;
			}
			return GetDataFromDb("select  [T_OP].[F_ID_OP]" +
								"		  , [T_OP].[F_ID_PRO_SEQUENCE]" +
								"		  , [T_OP].[F_ID_T_AN_MATERIALS]" +
								"		  , [T_OP].[F_STATE]" +
								"		  , [T_OP].[F_DT_START]" +
								"		  , [T_OP].[F_DT_STOP]" +
								"		  , [T_OP].[F_DESCRIPTION]" +
								"		  , [T_OP].[F_THICKNESS]" +
								"		  , [T_OP].[F_SHAPE_TYPE]" +
								"		  , [T_OP].[F_ID_T_AN_WORK_CENTER_GROUPS]" +
								"		  , [T_AN_MATERIALS].[F_CODE] as F_MAT_CODE" +
								"		  , [T_AN_WORK_CENTER_GROUPS].[F_CODE] as F_WORK_CENTER_GROUP_CODE" +
								"	from    [T_OP]" +
								"			inner join [T_AN_MATERIALS]" +
								"				on [T_AN_MATERIALS].[F_ID] = [T_OP].[F_ID_T_AN_MATERIALS]" +
								"			inner join [T_AN_WORK_CENTER_GROUPS]" +
								"				on [T_OP].[F_ID_T_AN_WORK_CENTER_GROUPS] = [T_AN_WORK_CENTER_GROUPS].[F_ID]" + sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (ProdOrder po in mRecordset)
					{
						switch (po.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
								sqlInsert = "insert into T_OP (" + 
									"F_ID_PRO_SEQUENCE" +
									", F_STATE" +
									", F_DT_START" +
									", F_DT_STOP" + 
									", F_DESCRIPTION" +
									", F_THICKNESS" +
									", F_SHAPE_TYPE" +
									", F_ID_T_AN_MATERIALS" +
									")" +
									" (select " + po.gSequenceId + 
									", " + ((int) po.gStateOrd) + 
									", " + (po.gStartDate == DateTime.MinValue ? "NULL" : mDbInterface.OleDbToDate(po.gStartDate)) + 
									", " + (po.gStopDate == DateTime.MinValue ? "NULL" : mDbInterface.OleDbToDate(po.gStopDate)) + 
									", '" + po.gDescription + "'" +
									", " + po.gThickness +
									", " + ((int) po.gShapeType) +
									", F_ID" +
									" from T_AN_MATERIALS where F_CODE = '" + po.gMatCode + "')" +
									";select SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								po.gId = id;
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
								sqlUpdate = "update T_OP SET F_ID_PRO_SEQUENCE = " + po.gSequenceId +
									", F_STATE = " + ((int) po.gStateOrd) +
									", F_DT_START = " + (po.gStartDate == DateTime.MinValue ? "NULL" : mDbInterface.OleDbToDate(po.gStartDate)) + 
									", F_DT_STOP = " + (po.gStopDate == DateTime.MinValue ? "NULL" : mDbInterface.OleDbToDate(po.gStopDate)) + 
									", F_DESCRIPTION = '" + po.gDescription + "'" +
									", F_THICKNESS = " + po.gThickness +
									", F_SHAPE_TYPE = " + ((int) po.gShapeType) +
									", F_ID_T_AN_MATERIALS = (select F_ID from T_AN_MATERIALS where F_CODE = '" + po.gMatCode + "')" ;
							
								sqlUpdate += " where F_ID_OP = " + po.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;
						
								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (ProdOrder po in mDeleted)
					{
						switch (po.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
								sqlDelete = "delete from T_OP where F_ID_OP = " + po.gId.ToString();
								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if (transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

				//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ProdOrderCollection: Deadlock Error", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						// Annulla la transazione
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("ProdOrderCollection: Error", ex);

					// Annulla la transazione
					if (transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

		///**********************************************************************
		/// <summary>
		/// Generate the production orders
		/// </summary>
		/// <param name="sequenceId">id sequence</param>
		/// <param name="materialId">id material</param>
		/// <param name="reset">regenerate all production orders</param>
		/// <param name="workCenterGroupParams">list of work center group parameters</param>
		/// <param name="symbolsToImport">list of symbols shape to import</param>
		/// <returns>
		///			true if succedeed
		///			false otherwise
		///	</returns>
		///**********************************************************************
		public bool GenerateProdOrders(int sequenceId, int materialId, bool reset, List<WorkCenterGroupParam> workCenterGroupParams,
			List<string> symbolsToImport)
		{
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbParameter par;
			ArrayList parameters = new ArrayList();
			bool rc = false;

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction) mDbInterface.BeginTransaction();

					/*
					 * Lancia la stored procedure per la generazione degli ordini di produzione
					 */

					par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
					par.Direction = System.Data.ParameterDirection.ReturnValue;
					parameters.Add(par);

					par = new OleDbParameter("@SequenceId", OleDbType.Integer);
					par.Value = sequenceId;
					parameters.Add(par);

					par = new OleDbParameter("@MaterialId", OleDbType.Integer);
					par.Value = materialId;
					parameters.Add(par);

					par = new OleDbParameter("@Reset", OleDbType.Integer);
					par.Value = (reset ? 1 : 0);
					parameters.Add(par);

					// Lancia la stored procedure
					if (!mDbInterface.Execute("sp_PoGenerateProdOrders", ref parameters))
						throw new ApplicationException("ProdOrderCollection.GenerateProdOrders sp_PoGenerateProdOrders Execute error.");

					// Verifica il codice di ritorno
					par = (OleDbParameter)parameters[0];
					if (((int)par.Value) != 0)
						throw new ApplicationException("ProdOrderCollection.GenerateProdOrders sp_PoGenerateProdOrders error: " + ((int)par.Value));

					// Aggiorna i parametri di tutti i gruppi di centri di lavoro
					foreach (WorkCenterGroupParam wcgp in workCenterGroupParams)
					{
						// Imposta i parametri specifici per tutti gli ordini di produzione generati
						if (!SetParameter(sequenceId, 0, wcgp.Code, "F_OTT_TIPO", wcgp.OptType.ToString(CultureInfo.InvariantCulture)))
							throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating parameters"));
						if (!SetParameter(sequenceId, 0, wcgp.Code, "F_OTT_SFRIDO", wcgp.DiskThickness.ToString(CultureInfo.InvariantCulture)))
							throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating parameters"));
						if (!SetParameter(sequenceId, 0, wcgp.Code, "F_OTT_DISK_DIAMETER", wcgp.DiskDiameter.ToString(CultureInfo.InvariantCulture)))
							throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating parameters"));
						if (!SetParameter(sequenceId, 0, wcgp.Code, "F_OTT_DISABLE_DEFECTS", (wcgp.DisableDefects ? "1" : "0")))
							throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating parameters"));
						if (!SetParameter(sequenceId, 0, wcgp.Code, "F_OTT_TIMEOUT_COMPUTE", wcgp.TimeMin.ToString(CultureInfo.InvariantCulture)))
							throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating parameters"));
						if (!SetParameter(sequenceId, 0, wcgp.Code, "F_OTT_TIMEOUT", wcgp.TimeMax.ToString(CultureInfo.InvariantCulture)))
							throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating parameters"));
						if (!SetParameter(sequenceId, 0, wcgp.Code, "F_OTT_POL_SOLO_SFRIDO", (wcgp.OnlyDiskThickness ? "1" : "0")))
							throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating parameters"));
					}
					if (transaction) mDbInterface.EndTransaction(true);
					rc = true;
					retry = DbInterface.DEADLOCK_RETRY;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ProdOrderCollection.GenerateProdOrders Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("ProdOrderCollection.GenerateProdOrders: Error", ex);
					return false;
				}
			}


			if (rc)
			{
				try
				{
					/* 
					 * Esegue la conversione dei file XML dei formati
					 */

					// Legge tutti gli ordini di produzione della sequenza
					if (!GetDataFromDb(ProdOrder.Keys.F_ID_OP, sequenceId))
						throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error reading production orders"));

					//Scorre tutti gli ordini di produzione generati
					foreach (ProdOrder prodOrd in mRecordset)
					{
						// Legge i formati dell'ordine di produzione
						ShapeCollection shapes = new ShapeCollection(mDbInterface);
						shapes.GetProdOrderShapesFromDb(prodOrd.gId);

						// Scorre tutti i formati
						bool dimensionChanged = false;
						Shape shape;
						shapes.MoveFirst();
						while (shapes.GetObject(out shape))
						{
							string xmlFile = mTmpDirPath + "\\SHAPEID_" + shape.gId.ToString() + ".xml";

							// Legge il poligono
							if (!shapes.GetXmlPolygonFromDb(shape, xmlFile))
							{
								// File XML non trovato
								//throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error reading xml polygon"));
							}

							// Verifica se il formato contiene il file xml completo
							int mod = shape.CheckXmlFile(xmlFile, symbolsToImport);
							if (mod == -1)
								throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error creating xml polygon"));

							// Se sono state fatte delle modifiche, le salva nel db
							if (mod != 0)
							{
								// Legge il poligono
								if (!shapes.SetXmlPolygonToDb(shape, xmlFile, true))
									throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error writing xml polygon into db"));
							}

							// Verifica le dimensioni del formato, ed eventualmente le imposta
							dimensionChanged |= shape.CheckShapeDimension();

							// Passa al formato successivo
							shapes.MoveNext();
						}

						// Se sono state modificate delle dimensioni, aggiorna il database
						if (dimensionChanged)
							if (!shapes.UpdateDataToDb())
								throw (new ApplicationException("ProdOrderCollection.GenerateProdOrders: error updating shapes dimensions"));
					}
				}

					// Eccezione
				catch (Exception ex)
				{
					TraceLog.WriteLine("ProdOrderCollection.GenerateProdOrders: Error", ex);
					return false;
				}

				return true;
			}
			else
				return false;
		}

		///**********************************************************************
		/// <summary>
		/// Imposta un parametro di ottimizzazione degli ordini di produzione
		/// </summary>
		/// <param name="sequenceId">id sequenza</param>
		/// <param name="opId">id ordine di produzione; se 0 imposta tutti</param>
		/// <param name="workCenterGroupCode">codice gruppo centri di lavoro; se "" imposta tutti</param>
		/// <param name="parName">nome parametro</param>
		/// <param name="parValue">valore parametro</param>
		/// <returns>
		///		true	parametro impostato con successo
		///		false	errore nell'impostazione del parametro
		/// </returns>
		///**********************************************************************
		private bool SetParameter(int sequenceId, int opId, string workCenterGroupCode, string parName, string parValue)
		{
			return ProdOrderCollection.SetParameter(mDbInterface, sequenceId, opId, workCenterGroupCode, parName, parValue);
		}

		public static bool SetParameter(DbInterface db, int sequenceId, int opId, string workCenterGroupCode, string parName, string parValue)
		{
			OleDbParameter par;
			ArrayList parameters = new ArrayList();

			try
			{
				/*
				 * Lancia la stored procedure per la modifica del parametro
				 */

				par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
				par.Direction = System.Data.ParameterDirection.ReturnValue;
				parameters.Add(par);

				par = new OleDbParameter("@SequenceId", OleDbType.Integer);
				par.Value = sequenceId;
				parameters.Add(par);

				par = new OleDbParameter("@OpId", OleDbType.Integer);
				par.Value = opId;
				parameters.Add(par);

				par = new OleDbParameter("@WorkCenterGroup", OleDbType.VarChar, 18);
				par.Value = workCenterGroupCode;
				parameters.Add(par);

				par = new OleDbParameter("@ParName", OleDbType.VarChar, 64);
				par.Value = parName;
				parameters.Add(par);

				par = new OleDbParameter("@ParValue", OleDbType.VarChar, 128);
				par.Value = parValue;
				parameters.Add(par);

				// Lancia la stored procedure
				if (!db.Execute("sp_PoSetParameter", ref parameters))
					throw new ApplicationException("ProdOrderCollection.SetParameter sp_PoSetParameter Execute error.");

				// Verifica il codice di ritorno
				par = (OleDbParameter)parameters[0];
				if (((int)par.Value) != 0)
					throw new ApplicationException("ProdOrderCollection.SetParameter sp_PoSetParameter error: " + ((int)par.Value));

				return true;

			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("ProdOrderCollection.GenerateProdOrders: Error", ex);
				return false;
			}
		}

		///**********************************************************************
		/// <summary>
		/// Select slabs for production order
		/// </summary>
		/// <param name="prodOrderId">id production order</param>
		/// <param name="selectedSlabs">selected slabs id</param>
		/// <returns>
		///			true if succedeed
		///			false otherwise
		///	</returns>
		///**********************************************************************
		public bool SelectSlabsProdOrders(int prodOrderId, int[] selectedSlabs)
		{
			int retry = 0;
			string sqlCmd;
			bool transaction = !mDbInterface.TransactionActive();
			OleDbDataReader dr = null;
			SlabGroupCollection slabGroups = new SlabGroupCollection(mDbInterface);
			OpSlabCollection opSlabs = new OpSlabCollection(mDbInterface);

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction)	mDbInterface.BeginTransaction();

					// Seleziona le lastre indicate
					if (!opSlabs.SelectOpSlabs(prodOrderId, selectedSlabs))
						throw (new ApplicationException("ProdOrderCollection.SelectSlabsProdOrders: error selecting slabs"));

					// Legge tutti i gruppi di lastre precedentemente selezionati
					if (!slabGroups.GetDataFromDb(prodOrderId))
						throw (new ApplicationException("ProdOrderCollection.SelectSlabsProdOrders: error getting old slab groups"));
					
					// Elimina tutti i gruppi precedentemente creati
					while (!slabGroups.IsEmpty())
						slabGroups.DeleteObject(0);

					// Legge le lastre selezionate, ordinate in base alle dimensioni
					sqlCmd = "select T_OP_SLABS.F_ID_OP_SLAB" +
							 "		, T_AN_ARTICLES.F_ID" +
							 "		, round(T_AN_ARTICLES.F_DIM_X / 10, 0) * 10" +
							 "		, round(T_AN_ARTICLES.F_DIM_Y / 10, 0) * 10" +
							 "		, T_AN_ARTICLES.F_SURFACE" +
							 "		, T_AN_ARTICLES.F_DEFECTS" +
							 " from T_OP_SLABS" +
							 "		inner join T_AN_ARTICLES" +
							 "			on T_OP_SLABS.F_ID_T_AN_ARTICLES = T_AN_ARTICLES.F_ID" +
							 " where T_OP_SLABS.F_ID_OP = " + prodOrderId.ToString() +
							 " order by round(T_AN_ARTICLES.F_DIM_Y / 10, 0) * 10" +
							 "			, round(T_AN_ARTICLES.F_DIM_X / 10, 0) * 10" +
							 "			, T_AN_ARTICLES.F_DEFECTS desc" +
							 "			, T_AN_ARTICLES.F_ID";
					if (!mDbInterface.Requery(sqlCmd, out dr))
						throw (new ApplicationException("ProdOrderCollection.SelectSlabsProdOrders: error reading selected slabs"));

					// Introduce i dati letti in un array
					System.Collections.ArrayList slabs = new System.Collections.ArrayList();

					SlabsInfo slab;
					while (dr.Read())
					{
						slab = new SlabsInfo(dr.GetInt32(0),
											 dr.GetInt32(1),
											 dr.GetDouble(2),
											 dr.GetDouble(3),
											 dr.GetDouble(4),
											 dr.GetInt32(5));
					
						slabs.Add(slab);
					}
					mDbInterface.EndRequery(dr);

					// Crea i gruppi di lastre
					int group = 0, defects = -1;
					double width = -1d, length = -1d;
					string sqlIdSlabGroupList = "";
					string sqlSeparator = "";
					for (int i = 0; i < slabs.Count; i++)
					{
						// Verifica il cambio gruppo
						slab = (SlabsInfo) slabs[i];
						if (slab.Defects > 0 || slab.Defects != defects || slab.Width != width || slab.Length != length)
						{
							// Assegna le lastre precedenti al gruppo precedente
							if (sqlIdSlabGroupList.Length > 0)
							{
								sqlCmd = "update T_OP_SLABS" +
										 " set F_GROUP = " + group.ToString() +
										 " where F_ID_OP_SLAB in (" + sqlIdSlabGroupList + ")";

								if (!mDbInterface.Execute(sqlCmd))
									throw (new ApplicationException("ProdOrderCollection.SelectSlabsProdOrders: error updating slab group"));

								sqlIdSlabGroupList = sqlSeparator = "";
							}

							// Nuovo gruppo
							group++;
							width = slab.Width;
							length = slab.Length;
							defects = slab.Defects;

							// Aggiunge un nuovo gruppo di lastre
							slabGroups.AddObject(new SlabGroup(0, prodOrderId, group, length, width));
						}

						// Aggiunge una nuova lastra
						sqlIdSlabGroupList += sqlSeparator + slab.IdOpSlab.ToString();
						sqlSeparator = ",";
					}

					// Assegna le lastre precedenti al gruppo precedente
					if (sqlIdSlabGroupList.Length > 0)
					{
						sqlCmd = "update T_OP_SLABS" +
								 " set F_GROUP = " + group.ToString() +
								 " where F_ID_OP_SLAB in (" + sqlIdSlabGroupList + ")";

						if (!mDbInterface.Execute(sqlCmd))
							throw (new ApplicationException("ProdOrderCollection.SelectSlabsProdOrders: error updating slab group"));

						sqlIdSlabGroupList = sqlSeparator = "";
					}

					// Aggiorna i gruppi di lastre nel db
					if (!slabGroups.UpdateDataToDb())
						throw (new ApplicationException("ProdOrderCollection.SelectSlabsProdOrders: error inserting slabs groups"));

					if (transaction) mDbInterface.EndTransaction(true);
					return true;
				}
				
					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ProdOrderCollection.SelectSlabsProdOrders Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						mDbInterface.EndRequery(dr);
						if (transaction)	mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}
				
						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					mDbInterface.EndRequery(dr);
					if (transaction)	mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("ProdOrderCollection.SelectSlabsProdOrders: Error", ex);
					return false;
				}

				finally
				{
					mDbInterface.EndRequery(dr);
				}
			}

			return false;
		}

		///**********************************************************************
		/// <summary>
		/// Ritorna il numero di ordini di produzione con un certo stato
		/// </summary>
		/// <param name="state">stato da verificare</param>
		/// <param name="sequenceId">eventuale numero di sequenza, se 0 tutte le sequenze</param>
		/// <returns>
		///			>= 0	numero di ordini di produzione
		///			-1		errore
		///	</returns>
		///**********************************************************************
		public int StateCount(ProdOrder.State state)
		{
			return StateCount(state, 0);
		}
		public int StateCount(ProdOrder.State state, int sequenceId)
		{
			OleDbDataReader dr = null;
			string sWhere = " where ";

			string sqlQuery = "select isnull(count(*), 0) from T_OP";

			if (state != ProdOrder.State.Undef)
			{
				sqlQuery += sWhere + "F_STATE = " + ((int)state).ToString();
				sWhere = " and ";
			}

			if (sequenceId > 0)
			{
				sqlQuery += sWhere + "F_ID_PRO_SEQUENCE = " + sequenceId.ToString();
				sWhere = " and ";
			}

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return -1;

				// Ritorna la quantit� di formati
				if (dr.Read())
					return (int) dr[0];
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("ProdOrderCollection.StateCount: Error", ex);
				return -1;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			return -1;
		}

		///**********************************************************************
		/// <summary>
		/// Set state for production order
		/// </summary>
		/// <param name="prodOrderId">id production order</param>
		/// <param name="state">state</param>
		/// <returns>
		///			true if succedeed
		///			false otherwise
		///	</returns>
		///**********************************************************************
		public bool SetProdOrderState(int prodOrderId, ProdOrder.State state)
		{
			int retry = 0;
			string sqlCmd;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					// Apre la transazione
					if (transaction) mDbInterface.BeginTransaction();

					// Aggiorna lo stato dell'ordine di produzione
					sqlCmd = "update T_OP set F_STATE = " + ((int) state).ToString() +
						" where F_ID_OP = " + prodOrderId.ToString();
					if (!mDbInterface.Execute(sqlCmd))
						throw (new ApplicationException("ProdOrderCollection.SetProdOrderState: error changing prod order state"));

					if (transaction) mDbInterface.EndTransaction(true);
					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ProdOrderCollection.SetProdOrderState Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("ProdOrderCollection.SetProdOrderState: Error", ex);
					return false;
				}
			}

			return false;
		}

		///*********************************************************************
		/// <summary>
		/// Verifica se l'ordine di produzione pu� essere cancellato
		/// </summary>
		/// <param name="idProdOrd">id dell'ordine di produzione</param>
		/// <returns>
		///		true	l'ordine di produzione � cancellabile
		///		false	l'ordine di produzione non � cancellabile
		/// </returns>
		///*********************************************************************
		public bool IsErasable(int idProdOrd)
		{
			OleDbDataReader dr = null;

			// Verifica che l'id sia valido
			if (idProdOrd <= 0)
				return true;

			try
			{
				// Verifica se ci sono lastre bloccate
				string szSQL = "select count(*)" +
								" from T_OP_SLABS" +
								"	inner join T_OP" +
								"		on T_OP_SLABS.F_ID_OP = T_OP.F_ID_OP" +
								" where T_OP.F_ID_OP = " + idProdOrd.ToString() +
								" and T_OP_SLABS.F_STATE = " + ((int)OpSlab.State.Confirmed).ToString();

				if (!mDbInterface.Requery(szSQL, out dr, true))
					throw new ApplicationException("ProdOrderCollection.IsErasable: Requery error.");

				// Verifica se ci sono lastre bloccate
				if (dr.Read())
					if (dr.GetInt32(0) == 0)
						return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("ProdOrderCollection.IsErasable: Error", ex);
				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			return false;
		}


		///**********************************************************************
		/// <summary>
		/// Restituisce i gruppi di centri di lavoro associabili agli ordini di 
		/// produzione
		/// </summary>
		/// <param name="workCenterGroupCodes">codici gruppi di centri di lavoro restituiti</param>
		/// <param name="workCenterGroupDescriptions">descrizioni gruppi di centri di lavoro restituiti</param>
		/// <returns>
		///			true if succedeed
		///			false otherwise
		///	</returns>
		///**********************************************************************
		public bool GetWorkCenterGroups4Op(out List<string> workCenterGroupCodes, out List<string> workCenterGroupDescriptions)
		{
			OleDbDataReader dr = null;

			workCenterGroupCodes = new List<string>();
			workCenterGroupDescriptions = new List<string>();

			try
			{
				// Legge le lastre selezionate, ordinate in base alle dimensioni
				string sqlCmd = "select distinct" +
							"			[T_AN_WORK_CENTER_GROUPS].[F_CODE]" +
							"		  , [T_AN_WORK_CENTER_GROUPS].[F_DESCRIPTION]" +
							" from    [T_AN_WORK_CENTER_GROUPS]" +
							"		inner join [T_IX_PHASE_WORKINGS]" +
							"			on [T_AN_WORK_CENTER_GROUPS].[F_ID] = [T_IX_PHASE_WORKINGS].[F_ID_T_AN_WORK_CENTER_GROUPS]" +
							"		inner join [T_LS_PHASE_WORKINGS]" +
							"			on [T_IX_PHASE_WORKINGS].[F_ID_T_LS_PHASE_WORKINGS] = [T_LS_PHASE_WORKINGS].[F_ID]" +
							"		inner join [T_LS_WORKING_TYPES]" +
							"			on [T_LS_PHASE_WORKINGS].[F_ID_T_LS_WORKING_TYPES] = [T_LS_WORKING_TYPES].[F_ID]" +
							"		inner join [T_CO_TECNO_WORKING_TYPES]" +
							"			on [T_LS_WORKING_TYPES].[F_ID_T_CO_TECNO_WORKING_TYPES] = [T_CO_TECNO_WORKING_TYPES].[F_ID]" +
							"		inner join [T_CO_TECNO_CENTER_GROUPS]" +
							"			on [T_AN_WORK_CENTER_GROUPS].[F_ID_T_CO_TECNO_CENTER_GROUPS] = [T_CO_TECNO_CENTER_GROUPS].[F_ID]" +
							" where   [T_CO_TECNO_WORKING_TYPES].[F_CODE] in ( 'CUT', 'CUT_HOLES' )" +
							"		and [T_CO_TECNO_CENTER_GROUPS].[F_CODE] in ( 'FK', 'COMBICUT', 'OPTIMA' )";

				if (!mDbInterface.Requery(sqlCmd, out dr))
					throw (new ApplicationException("ProdOrderCollection.SelectSlabsProdOrders: error reading selected slabs"));

				while (dr.Read())
				{
					workCenterGroupCodes.Add(dr.GetString(0).Trim());
					workCenterGroupDescriptions.Add((dr.IsDBNull(1) ? "" : dr.GetString(1).Trim()));
				}

				mDbInterface.EndRequery(dr);

				return true;
			}

				// Eccezione
			catch (Exception ex)
			{
				mDbInterface.EndRequery(dr);
				TraceLog.WriteLine("ProdOrderCollection.GetWorkCenterGroups4Op: Error", ex);
				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}


		public override void SortByField(int index)
		{
		}

		public override void SortByField(string key)
		{
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			ProdOrder po;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					po = new ProdOrder((int)dr["F_ID_OP"],
						(int)dr["F_ID_PRO_SEQUENCE"],
						(ProdOrder.State)dr["F_STATE"],
						(dr["F_DT_START"] == System.DBNull.Value ? DateTime.MinValue : (DateTime)dr["F_DT_START"]),
						(dr["F_DT_STOP"] == System.DBNull.Value ? DateTime.MinValue : (DateTime)dr["F_DT_STOP"]),
						dr["F_DESCRIPTION"].ToString(),
						(int)dr["F_ID_T_AN_MATERIALS"],
						dr["F_MAT_CODE"].ToString(),
						(float)dr["F_THICKNESS"],
						(ProdOrder.ShapeType)dr["F_SHAPE_TYPE"],
						(int)dr["F_ID_T_AN_WORK_CENTER_GROUPS"],
						dr["F_WORK_CENTER_GROUP_CODE"].ToString());
					
					AddObject(po);
					po.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("ProdOrderCollection.GetDataFromDb: Query: " + sqlQuery +
					", Error: ", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}

	public class WorkCenterGroupParam
	{
		public string Code;
		public string Description;
		public int OptType;
		public double DiskThickness;
		public double DiskDiameter;
		public bool OnlyDiskThickness;
		public bool DisableDefects;
		public int TimeMin;
		public int TimeMax;

		public WorkCenterGroupParam(string code, string description)
			: this(code, description, (int) Optimizers.Optimizer.OptTypesEnum.Polygons, 0d, 0d, false, false, 0, 0)
		{ }
		public WorkCenterGroupParam(string code, string description, int optType, double diskThickness, double diskDiameter, bool onlyDiskThickness,
			bool disableDefects, int timeMin, int timeMax)
		{
			Code = code.Trim();
			Description = description.Trim();
			OptType = optType;
			DiskThickness = diskThickness;
			DiskDiameter = diskDiameter;
			OnlyDiskThickness = onlyDiskThickness;
			DisableDefects = disableDefects;
			TimeMin = timeMin;
			TimeMax = timeMax;
		}

	}
}
