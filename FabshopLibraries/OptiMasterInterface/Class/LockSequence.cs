using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Threading;
using Breton.DbAccess;
using Breton.TraceLoggers;
using Breton.Users;

namespace Breton.OptiMasterInterface
{
	public class LockSequence : IDisposable
	{
		#region Structs, Enums & Variables

		private bool mInitialize;
		private int mTotProdOrders;
		private int mActProdOrder;
		private int mTotSlabs;
		private int mActSlab;
		private bool mLock, mUnlock;
		private bool mAbort = false;
		private bool mErr = false;
		private bool mEnd = false;
		private bool mActive = false;
		private bool mUsePreOpt = true;

		private SequenceCollection mSequences;
		private Sequence mSequence;
		private int mIdSequence;
		private ProdOrder mProdOrder;
		private PreOptCam.clsConnector preOptConn;
		private DbInterface mDbInterface;
		private string mClientName;
		private User mUser;
		private ArrayList mIdSlabs;

		private Thread thdExecution;

		protected bool mDisposed = false;

		#endregion


		#region Constructors & Disposers

		public LockSequence(int idSequence, DbInterface dbInterface, string clientName, User user)
			: this(idSequence, dbInterface, clientName, user, true)
		{
		}
		public LockSequence(int idSequence, DbInterface dbInterface, string clientName, User user, bool usepreopt)
		{
			mUser = user;
			mDbInterface = new DbInterface();
			mDbInterface.Open(dbInterface.ConnectionString());
			mSequences = new SequenceCollection(mDbInterface, mUser);
			mIdSequence = idSequence;
			mClientName = clientName;
			mIdSlabs = null;
			mUsePreOpt = usepreopt;
		}


		#region IDisposable Members

		///*************************************************************************
		/// <summary>
		/// Distruttore chiamato dall'esterno
		/// </summary>
		///*************************************************************************
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		///*************************************************************************
		/// <summary>
		/// Distruttore interno effettivo
		/// </summary>
		/// <param name="disposing">true = chiamata esplicita / false = Garbage Collector</param>
		///*************************************************************************
		protected virtual void Dispose(bool disposing)
		{
            // Check to see if Dispose has already been called.
			if (!this.mDisposed)
			{
				StopLock();
				StopUnlock();

				mDbInterface.Close();
				mDbInterface.Dispose();
				mDbInterface = null;

				// Sconnette il visualizzatore
				if (preOptConn != null)
				{
					preOptConn.gbUnConnect(true);
					preOptConn = null;
				}

				mDisposed = true;
			}
		}

		public bool Disposed
		{
			get { return mDisposed; }
		}

		#endregion


		#endregion


		#region Properties

		public bool gLocking
		{
			get { return mLock; }
		}

		public bool gUnlocking
		{
			get { return mUnlock; }
		}

		public bool gActive
		{
			get { return mActive; }
		}

		public bool gEnd
		{
			get { return mEnd; }
		}

		public bool gErr
		{
			get { return mErr; }
		}

		public int gTotProdOrders
		{
			get { return mTotProdOrders; }
		}

		public int gActProdOrder
		{
			get { return mActProdOrder; }
		}

		public int gTotSlabs
		{
			get { return mTotSlabs; }
		}

		public int gActSlab
		{
			get { return mActSlab; }
		}

		#endregion


		#region Public Methods

		/// ********************************************************************
		/// <summary>
		/// Avvia il processo di blocco
		/// </summary>
		/// <returns>
		///		true	processo avviato
		///		false	impossibile avviare il processo
		///	</returns>
		/// ********************************************************************
		public bool StartLock(ArrayList idSlabs)
		{
			if (mLock || mUnlock)
				return false;
			
			mIdSlabs = idSlabs;
			mIdSlabs.Sort();

			return StartLock();
		}
		public bool StartLock()
		{
			if (mLock || mUnlock)
				return false;

			mInitialize = false;
			mAbort = false;
			mEnd = false;
			mErr = false;
			mLock = true;
			mActive = true;

			thdExecution = new Thread(new ThreadStart(Execution));
			thdExecution.Start();

			return true;
		}

		/// ********************************************************************
		/// <summary>
		/// Arresta il processo di blocco
		/// </summary>
		/// <returns>
		///		true	processo arrestato
		///		false	impossibile arrestare il processo
		/// </returns>
		/// ********************************************************************
		public bool StopLock()
		{
			if (mLock)
			{
				if (mActive)
				{
					// Richiede la fine del thread
					mAbort = true;

					// Attende la fine del thread
					thdExecution.Join();
					mActive = false;
				}

				mLock = false;
			}

			mIdSlabs = null;
			return true;
		}

		/// ********************************************************************
		/// <summary>
		/// Avvia il processo di sblocco
		/// </summary>
		/// <returns>
		///		true	processo avviato
		///		false	impossibile avviare il processo
		///	</returns>
		/// ********************************************************************
		public bool StartUnlock(ArrayList idSlabs)
		{
			if (mLock || mUnlock)
				return false;
			
			mIdSlabs = idSlabs;
			mIdSlabs.Sort();

			return StartUnlock();
		}
		public bool StartUnlock()
		{
			if (mLock || mUnlock)
				return false;

			mInitialize = false;
			mAbort = false;
			mEnd = false;
			mErr = false;
			mUnlock = true;
			mActive = true;

			thdExecution = new Thread(new ThreadStart(Execution));
			thdExecution.Start();

			return true;
		}

		/// ********************************************************************
		/// <summary>
		/// Arresta il processo di sblocco
		/// </summary>
		/// <returns>
		///		true	processo arrestato
		///		false	impossibile arrestare il processo
		///	</returns>
		/// ********************************************************************
		public bool StopUnlock()
		{
			if (mUnlock)
			{
				if (mActive)
				{
					// Richiede la fine del thread
					mAbort = true;

					// Attende la fine del thread
					thdExecution.Join();
					mActive = false;
				}

				mUnlock = false;
			}

			mIdSlabs = null;
			return true;
		}

		#endregion


		#region Private Methods

		/// ********************************************************************
		/// <summary>
		/// Routine principale del thread di blocco/sblocco
		/// </summary>
		/// ********************************************************************
		private void Execution()
		{
			bool end = false, err = false;

			while (!mAbort && !end && !err && (mLock || mUnlock))
			{
				if (mLock)
					err = !LockingSequence(out end, mUsePreOpt);
				else if (mUnlock)
					err = !UnlockingSequence(out end);

				// Tempo di attesa fra una operazione e la successiva
				Thread.Sleep(1000);
			}

			// Richiesta elaborazione lista di prelievo
			if (end && !err)
				err = !mSequences.ElabRequest();

			mErr = err;
			mEnd = end;
			mActive = false;
		}

		/// ********************************************************************
		/// <summary>
		/// Blocco della sequenza
		/// </summary>
		/// <param name="end">operazione completata</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore
		/// </returns>
		/// ********************************************************************
		private bool LockingSequence(out bool end)
		{
			return LockingSequence(out end, true);
		}

		private bool LockingSequence(out bool end, bool usePreOpt)
		{
			OpSlab opSlab;

			end = false;

			try
			{
				// Inizializzazione
				if (!mInitialize)
				{
					// Legge la sequenza da confermare
					mSequences = new SequenceCollection(mDbInterface, mUser);
					if (!mSequences.GetSingleElementFromDb(mIdSequence))
						return false;

					if (!mSequences.GetObject(out mSequence))
						return false;

					// Legge gli ordini di produzione della sequenza, completati
					if (!mSequence.GetProdOrdersFromDb(mDbInterface))
						return false;

					mTotProdOrders = mSequence.gProdOrders.StateCount(ProdOrder.State.Undef, mSequence.gId);
					mActProdOrder = 0;

					if (!mSequence.gProdOrders.GetObject(out mProdOrder))
						end = true;
					else
						mActProdOrder++;

					// Crea il collegamento all'oggetto visualizzatore
					if (mUsePreOpt && preOptConn == null)
					{
						preOptConn = new PreOptCam.clsConnector();
						preOptConn.gbConnect();
					}

					mInitialize = true;
				}

				// Trova il prossimo ordine di produzione
				while (!end && (mProdOrder.gOpSlabs == null || mProdOrder.gOpSlabs.IsEOF()))
				{
					// Legge le lastre dal DB
					if (mProdOrder.gOpSlabs == null)
					{
						// Legge le lastre in programmazione dell'ordine di produzione
						if (!mProdOrder.GetOpSlabsFromDb(mDbInterface, OpSlab.State.Programming))
							return false;

						mTotSlabs = mProdOrder.gOpSlabs.SlabQuantity(mProdOrder.gId, OpSlab.State.Undef);
						mActSlab = mProdOrder.gOpSlabs.SlabQuantity(mProdOrder.gId, OpSlab.State.Confirmed);
					}

					// Se le lastre sono finite, passa al prossimo ordine di produzione
					if (mProdOrder.gOpSlabs.IsEOF())
					{
						// Imposta l'ordine di produzione bloccato
						if (mProdOrder.gStateOrd == ProdOrder.State.Completed)
						{
							int slabRemaining = mProdOrder.gOpSlabs.SlabQuantity(mProdOrder.gId, OpSlab.State.Programming);
							// Se non rimangono altre lastre in programmazione
							if (slabRemaining == 0)
								// Imposta l'ordine di produzione bloccato
								if (!mSequence.gProdOrders.SetProdOrderState(mProdOrder.gId, ProdOrder.State.Locked))
									return false;
						}
						// Passa all'ordine di produzione successivo
						mSequence.gProdOrders.MoveNext();
						if (!mSequence.gProdOrders.GetObject(out mProdOrder))
						{
							end = true;
							break;
						}
						else
							mActProdOrder++;

					}
				}

				// Fine ordini di produzione
				if (end)
				{
					// Se la sequenza � completata
					if (mSequence.gStateSeq == Sequence.State.Completed)
					{
						mTotProdOrders = mSequence.gProdOrders.StateCount(ProdOrder.State.Undef, mSequence.gId);
						int lockedProdOrder = mSequence.gProdOrders.StateCount(ProdOrder.State.Locked, mSequence.gId);

						// Blocca la sequenza se tutti gli ordini di produzione risultano bloccati
						if (lockedProdOrder == mTotProdOrders)
						{
							// Elimina le lastre inutilizzate dalla sequenza
							if (!mSequences.RemoveUnusedSlabs(mIdSequence))
								return false;

							// Imposta la sequenza come bloccata
							mSequence.gStateSeq = Sequence.State.Locked;
							if (!mSequences.UpdateDataToDb())
								return false;
						}
					}

					// Sconnette il visualizzatore
					if (mUsePreOpt && preOptConn != null)
					{
						preOptConn.gbUnConnect(true);
						preOptConn = null;
					}

					// Fine
					return true;
				}

				// Trova la lastra nell'elenco di lastre da bloccare (se null, le blocca tutte)
				bool eof;
				mActSlab++;
				while (!(eof = !mProdOrder.gOpSlabs.GetObject(out opSlab)) && mIdSlabs != null)
				{
					int i;
					if ((i = mIdSlabs.BinarySearch(opSlab.gId)) >= 0)
						break;

					mActSlab++;

					// Passa alla lastra successiva
					mProdOrder.gOpSlabs.MoveNext();
				}

				// Verifica se ha terminato la lista di lastre
				if (eof)
					return true;

				/* 
				 * Crea il toolpath per i pezzi della lastra
				 */

				bool confirmOk = false;

				if (mUsePreOpt)
				{
					// Conferma le lastre dell'ordine di produzione
					preOptConn.gPreOptimizer.gClear();
					preOptConn.gPreOptimizer.glIdSequence = mIdSequence;
					preOptConn.gPreOptimizer.glIdOp = mProdOrder.gId;
					preOptConn.gPreOptimizer.gsClientName = mClientName;
					confirmOk = preOptConn.gPreOptimizer.gbConfirmSlab(opSlab.gId);
					// Se c'� stato un errore, ferma
					if (!confirmOk)
						return false;
				}

				/*
				 * Esegue il bloccaggio della lastra
				 */

				if (!mProdOrder.gOpSlabs.LockSlab(opSlab.gId))
					return false;

				// Crea gli articoli per gli eventuali residui
				if (!mSequences.CreateResidueArticles(mIdSequence))
					return false;

				// Passa alla lastra successiva
				mProdOrder.gOpSlabs.MoveNext();

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("LockSequence.LockingSequence: ", ex);
				return false;
			}
		}

		/// ********************************************************************
		/// <summary>
		/// Sblocco della sequenza
		/// </summary>
		/// <param name="end">operazione completata</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore
		/// </returns>
		/// ********************************************************************
		private bool UnlockingSequence(out bool end)
		{
			OpSlab opSlab;

			end = false;

			try
			{
				// Inizializzazione
				if (!mInitialize)
				{
					// Legge la sequenza da sbloccare
					mSequences = new SequenceCollection(mDbInterface, mUser);
					if (!mSequences.GetSingleElementFromDb(mIdSequence))
						return false;

					if (!mSequences.GetObject(out mSequence))
						return false;

					// Legge gli ordini di produzione della sequenza
					if (!mSequence.GetProdOrdersFromDb(mDbInterface))
						return false;

					mTotProdOrders = mSequence.gProdOrders.StateCount(ProdOrder.State.Undef, mSequence.gId);
					mActProdOrder = mTotProdOrders;

					if (!mSequence.gProdOrders.GetObject(out mProdOrder))
					{
						mActProdOrder = 0;
						end = true;
					}

					mInitialize = true;
				}

				// Trova il prossimo ordine di produzione
				while (!end && (mProdOrder.gOpSlabs == null || mProdOrder.gOpSlabs.IsEOF()))
				{
					// Legge le lastre dal DB
					if (mProdOrder.gOpSlabs == null)
					{
						// Legge le lastre confermate dell'ordine di produzione
						if (!mProdOrder.GetOpSlabsFromDb(mDbInterface, OpSlab.State.Confirmed))
							return false;

						mTotSlabs = mProdOrder.gOpSlabs.SlabQuantity(mProdOrder.gId, OpSlab.State.Undef);
						mActSlab = mProdOrder.gOpSlabs.SlabQuantity(mProdOrder.gId, OpSlab.State.Confirmed);
					}

					// Se le lastre sono finite, passa al prossimo ordine di produzione
					if (mProdOrder.gOpSlabs.IsEOF())
					{
						// Passa all'ordine di produzione successivo
						mSequence.gProdOrders.MoveNext();
						if (!mSequence.gProdOrders.GetObject(out mProdOrder))
						{
							mActProdOrder = 0;
							end = true;
							break;
						}
						else
							mActProdOrder--;
					}
				}

				// Fine ordini di produzione
				if (end)
				{
					// Fine
					return true;
				}

				// Trova la lastra nell'elenco di lastre da bloccare (se null, le sblocca tutte)
				bool eof;
				while (!(eof = !mProdOrder.gOpSlabs.GetObject(out opSlab)) && mIdSlabs != null)
				{
					int i;
					if ((i = mIdSlabs.BinarySearch(opSlab.gId)) >= 0)
						break;

					mActSlab--;

					// Passa alla lastra successiva
					mProdOrder.gOpSlabs.MoveNext();
				}

				// Verifica se ha terminato la lista di lastre
				if (eof)
					return true;

				/*
				 * Esegue lo sbloccaggio della lastra
				 */

				if (!mProdOrder.gOpSlabs.UnlockSlab(opSlab.gId))
					return false;

				mActSlab--;

				// Passa alla lastra successiva
				mProdOrder.gOpSlabs.MoveNext();

				// Se l'ordine � bloccato, lo sblocca
				if (mProdOrder.gStateOrd == ProdOrder.State.Locked)
				{
					// Imposta l'ordine di produzione completato
					if (!mSequence.gProdOrders.SetProdOrderState(mProdOrder.gId, ProdOrder.State.Completed))
						return false;

					mProdOrder.gStateOrd = ProdOrder.State.Completed;
				}

				// Se la sequenza � bloccata, la sblocca
				if (mSequence.gStateSeq == Sequence.State.Locked)
				{
					// Imposta la sequenza come completata
					mSequence.gStateSeq = Sequence.State.Completed;
					if (!mSequences.UpdateDataToDb())
						return false;
				}

				return true;
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("LockSequence.UnlockingSequence: ", ex);
				return false;
			}

		}

		#endregion
	}
}
