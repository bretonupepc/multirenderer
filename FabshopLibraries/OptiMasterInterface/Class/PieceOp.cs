using System;

namespace Breton.OptiMasterInterface
{
	///*************************************************************************
	/// <summary>
	/// Classe che implementa un pezzo confermato delle lastre, sul tipo FK
	/// </summary>
	///*************************************************************************
	public class PieceOp: Piece
	{
		#region Structs, Enums & Variables

		#endregion


		#region Constructors & Disposers

		///**********************************************************************
		/// <summary>
		/// Costruttore
		/// </summary>
		///**********************************************************************
		public PieceOp(int idOp, int id, int idGroup, int idShape, PieceOk.State state, double posX, double posY, bool rotated, PieceType type, 
			double traslX, double traslY, double rotAngle, double dimX, double dimY, bool isPolygon):
			base(idOp, id, idGroup, idShape, state, posX, posY, rotated, type, traslX, traslY, rotAngle, "", dimX, dimY, isPolygon)
		{
		}

		public PieceOp():this(0, 0, 0, 0, PieceOp.State.Undef, 0d, 0d, false, PieceOp.PieceType.Undef, 0d, 0d, 0d, 0d, 0d, false)
		{
		}

		#endregion


		#region Properties

		#endregion


		#region IComparable interface

		#endregion


		#region Public Methods

		#endregion


		#region Private Methods

		#endregion

	}
}
