﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Breton.RenderMaster;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using TraceLoggers;

namespace RenderMaster.Class
{
    internal class ShapeContourSweep
    {

        #region >-------------- Constants and Enums

        private const double COMPARISON_TOLERANCE = Render.COMPARISON_TOLERANCE;


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private List<ProfileSweep> _profileSweeps = new List<ProfileSweep>();

        private int _mainId = -1;

        private bool _isClockwise;

        private bool _isExternal;

        private string _currentProfileName = "NONE";

        private ProfileSweep  _currentProfileSweep = null;

        private Dictionary<string, CompositeCurve> _dxfProfiles = null;



        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public List<ProfileSweep> ProfileSweeps
        {
            get { return _profileSweeps; }
            set { _profileSweeps = value; }
        }

        public int MainId
        {
            get { return _mainId; }
            set { _mainId = value; }
        }

        public bool IsClockwise
        {
            get { return _isClockwise; }
            set { _isClockwise = value; }
        }

        public Dictionary<string, CompositeCurve> DxfProfiles
        {
            set { _dxfProfiles = value; }
        }

        public bool IsExternal
        {
            get { return _isExternal; }
            set { _isExternal = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        void SortRails()
        {
            if (ProfileSweeps.Count == 0)
                return;

            try
            {
                if (ProfileSweeps.Count > 2)
                {
                    var firstSweep = ProfileSweeps[0];
                    var lastSweep = ProfileSweeps[ProfileSweeps.Count - 1];

                    if (firstSweep.Thickness == lastSweep.Thickness
                        && firstSweep.DxfProfileName == lastSweep.DxfProfileName
                        && firstSweep.Rail.StartPoint == lastSweep.Rail.EndPoint)
                    {
                        ProfileSweeps.Remove(firstSweep);
                        lastSweep.Rail.CurveList.AddRange(firstSweep.Rail.CurveList);
                    }

                }

                //foreach (var sweep in ProfileSweeps)
                //{
                //    if (IsClockwise && sweep.Rail.SortAndOrient())
                //}

                //if (ProfileSweeps.Count == 1)
                //{
                //    CompositeCurve rail = ProfileSweeps[0].Rail;
                //    rail.Regen(1e-1);
                //    ProfileSweeps[0].IsClockwise = rail.Domain.IsDecreasing;
                //    //if (rail.IsClosed)
                //    //{
                //    //    bool cw = rail.IsOrientedClockwise(Plane.XY);
                //    //}
                //}
                //else
                //{
                ////    List<ProfileSweep> orientedSweeps = new List<ProfileSweep>();
                ////    var currentSweep = ProfileSweeps[0];

                ////ProfileSweeps[0].Rail.SortAndOrient();

                ////    ProfileSweeps.Remove(currentSweep);
                ////    orientedSweeps.Add(currentSweep);

                ////    var adjacentSweep = ProfileSweeps.FirstOrDefault(p => (p.Thickness == currentSweep.Thickness)
                ////                                                          &&
                ////                                                          (p.DxfProfileName ==
                ////                                                           currentSweep.DxfProfileName)
                ////                                                          &&
                ////                                                          ((p.StartPoint == currentSweep.EndPoint ||
                ////                                                            p.EndPoint == currentSweep.StartPoint)
                ////                                                              ));
                ////    if (adjacentSweep != null)
                ////    {
                ////        ProfileSweeps.Remove(adjacentSweep);

                ////        if (adjacentSweep.StartPoint == currentSweep.EndPoint)
                ////            orientedSweeps.Add(adjacentSweep);
                ////        else
                ////        {
                ////            orientedSweeps.Insert(0, adjacentSweep);
                ////        }
                ////    }

                    ////foreach (var sweep in ProfileSweeps)
                    ////{
                    ////    orientedSweeps.Add(sweep);
                    ////}
                    ////ProfileSweeps = orientedSweeps;

                    ////CompositeCurve rail = new CompositeCurve();
                    ////foreach (var sweep in ProfileSweeps)
                    ////{
                    ////    rail.CurveList.AddRange(sweep.Rail.CurveList);
                    ////}
                    ////if (rail.IsClosed)
                    ////{
                    ////    rail.Regen(1e-2);
                    ////    ProfileSweeps[0].IsClockwise = rail.Domain.IsDecreasing;

                    ////    //ProfileSweeps[0].IsClockwise = rail.IsOrientedClockwise(Plane.XY);
                    ////}

                ////bool clockwiseRail = rail.Domain.IsDecreasing;

                //////bool or = rail.IsOrientedClockwise(Plane.XY);

                ////bool clockwise = (IsExternal && clockwiseRail) || (!IsExternal && !clockwiseRail);
                ////foreach (var sweep in ProfileSweeps)
                ////{
                ////    sweep.IsClockwise = clockwise;
                ////}
                //}


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + " SortRails error", ex);
            }

        }
        public Mesh CreateMesh()
        {
            if (ProfileSweeps.Count == 0)
                return null;

            SortRails();

            List<Mesh> meshes = new List<Mesh>();

            Mesh contourMesh = null;

            foreach (var sweep in ProfileSweeps)
            {
                var newMesh = sweep.CreateMesh();
                if (newMesh != null)
                {
                    meshes.Add((Mesh)newMesh.Clone());
                }
            }

            contourMesh = (Mesh)meshes[0];

            if (meshes.Count > 1)
            {
                for (int i = 1; i < meshes.Count; i++)
                {
                    contourMesh.MergeWith((Mesh)meshes[i].Clone());
                }

                contourMesh.Weld();
            }

            //var contourMesh = ProfileSweeps[0].CreateMesh();
            //for (int i = 1; i < ProfileSweeps.Count; i++)
            //{
            //    contourMesh.MergeWith(ProfileSweeps[i].CreateMesh(), false);
            //}
            
            return contourMesh;
        }

        public void AddCurve(ICurve curve, double thickness, string profileName)
        {
            if (curve == null)
                return;

            Point3D lastStartPoint = null;
            Point3D lastEndPoint = null;

            const double POINT_DISTANCE = 1e-4;

            if (_currentProfileSweep != null)
            {
                lastStartPoint = _currentProfileSweep.StartPoint;
                lastEndPoint = _currentProfileSweep.EndPoint;
            }

            if (string.IsNullOrEmpty(profileName) || !_dxfProfiles.ContainsKey(profileName))
            // Profilo lineare
            {
                if (_currentProfileSweep == null ||
                    Math.Abs(_currentProfileSweep.Thickness - thickness) > COMPARISON_TOLERANCE)
                {

                    _currentProfileSweep = new ProfileSweep(thickness);
                    _currentProfileSweep.IsClockwise = IsClockwise;
                    ProfileSweeps.Add(_currentProfileSweep);


                    ////if (lastStartPoint != null && lastEndPoint != null && ProfileSweeps.Count == 2)
                    ////{
                    ////    if (curve.StartPoint == lastStartPoint || curve.EndPoint == lastEndPoint)
                    ////    {
                    ////        ProfileSweeps[0].Rail.Reverse();
                    ////        ProfileSweeps[0].Rail.Regen(Render.CHORDAL_ERROR);
                    ////    }
                    ////}

                    _currentProfileSweep.Rail.CurveList.Add(curve);

                }
                else if (_currentProfileSweep != null && _currentProfileSweep.Rail.CurveList.Count > 0)
                {
                    if (_currentProfileSweep.EndPoint.DistanceTo(curve.StartPoint) < POINT_DISTANCE)
                    {
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }
                    ////else if (_currentProfileSweep.StartPoint == curve.EndPoint)
                    ////{
                    ////    _currentProfileSweep.Rail.Reverse();
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Add(curve);
                    ////}
                    ////else if (_currentProfileSweep.StartPoint == curve.StartPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Insert(0, curve);
                    ////}
                    ////else if (_currentProfileSweep.EndPoint == curve.EndPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Add(curve);
                    ////}

                    else
                    {

                        _currentProfileSweep = new ProfileSweep(thickness);
                        _currentProfileSweep.IsClockwise = IsClockwise;
                        ProfileSweeps.Add(_currentProfileSweep);
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }

                }
            }
            else //Profilo sagomato
            {
                if (_currentProfileSweep == null || _currentProfileSweep.DxfProfileName != profileName)
                {
                    _currentProfileSweep = new ProfileSweep(_dxfProfiles[profileName]);
                    _currentProfileSweep.DxfProfileName = profileName;
                    _currentProfileSweep.IsClockwise = IsClockwise;
                    ProfileSweeps.Add(_currentProfileSweep);
                    _currentProfileSweep.Rail.CurveList.Add(curve);

                }

                else if (_currentProfileSweep != null && _currentProfileSweep.Rail.CurveList.Count > 0)
                {
                    //if (_currentProfileSweep.EndPoint.Equals(curve.StartPoint))
                    if (_currentProfileSweep.EndPoint.DistanceTo(curve.StartPoint) < POINT_DISTANCE)
                    {
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }
                    else if (_currentProfileSweep.StartPoint.DistanceTo(curve.EndPoint) < POINT_DISTANCE)
                    {
                        // Sequenza invertita: i segmenti sono orientati correttamente, ma arrivano in sequenza rovesciata
                        _currentProfileSweep.Rail.CurveList.Insert(0,curve);
                        _currentProfileSweep.RevertedSequence = true;
                    }

                    ////else if (_currentProfileSweep.StartPoint == curve.EndPoint)
                    ////{
                    ////    _currentProfileSweep.Rail.CurveList.Insert(0, curve);
                    ////}
                    ////else if (_currentProfileSweep.StartPoint == curve.StartPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Insert(0, curve);
                    ////}
                    ////else if (_currentProfileSweep.EndPoint == curve.EndPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Add(curve);
                    ////}
                    else
                    {
                        _currentProfileSweep = new ProfileSweep(_dxfProfiles[profileName]);
                        _currentProfileSweep.DxfProfileName = profileName;
                        _currentProfileSweep.IsClockwise = IsClockwise;
                        ProfileSweeps.Add(_currentProfileSweep);
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }

                }


            }
        }

        public void AddCurve(ICurve curve, double thickness, string profileName, double processingThickness)
        {
            if (curve == null)
                return;

            Point3D lastStartPoint = null;
            Point3D lastEndPoint = null;

            const double POINT_DISTANCE = 1e-4;

            if (_currentProfileSweep != null)
            {
                lastStartPoint = _currentProfileSweep.StartPoint;
                lastEndPoint = _currentProfileSweep.EndPoint;
            }

            if (string.IsNullOrEmpty(profileName) || !_dxfProfiles.ContainsKey(profileName))
            // Profilo lineare
            {
                if (_currentProfileSweep == null ||
                    Math.Abs(_currentProfileSweep.Thickness - thickness) > COMPARISON_TOLERANCE)
                {

                    _currentProfileSweep = new ProfileSweep(thickness);
                    _currentProfileSweep.IsClockwise = IsClockwise;
                    ProfileSweeps.Add(_currentProfileSweep);


                    ////if (lastStartPoint != null && lastEndPoint != null && ProfileSweeps.Count == 2)
                    ////{
                    ////    if (curve.StartPoint == lastStartPoint || curve.EndPoint == lastEndPoint)
                    ////    {
                    ////        ProfileSweeps[0].Rail.Reverse();
                    ////        ProfileSweeps[0].Rail.Regen(Render.CHORDAL_ERROR);
                    ////    }
                    ////}

                    _currentProfileSweep.Rail.CurveList.Add(curve);

                }
                else if (_currentProfileSweep != null && _currentProfileSweep.Rail.CurveList.Count > 0)
                {
                    if (_currentProfileSweep.EndPoint.DistanceTo(curve.StartPoint) < POINT_DISTANCE)
                    {
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }
                    ////else if (_currentProfileSweep.StartPoint == curve.EndPoint)
                    ////{
                    ////    _currentProfileSweep.Rail.Reverse();
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Add(curve);
                    ////}
                    ////else if (_currentProfileSweep.StartPoint == curve.StartPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Insert(0, curve);
                    ////}
                    ////else if (_currentProfileSweep.EndPoint == curve.EndPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Add(curve);
                    ////}

                    else
                    {

                        _currentProfileSweep = new ProfileSweep(thickness);
                        _currentProfileSweep.IsClockwise = IsClockwise;
                        ProfileSweeps.Add(_currentProfileSweep);
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }

                }
            }
            else //Profilo sagomato
            {
                double verticalOffset = 0;
                if (Math.Abs(processingThickness - thickness) > 1e-6) // Bordo del piano in presenza di laminazione 1
                {
                    verticalOffset = thickness - processingThickness;
                }
                else if (thickness < 0) // Bordo della laminazione con tipo laminazione 2
                {
                    verticalOffset = thickness;
                }
                //= (Math.Abs(processingThickness - thickness) > 1e-6) ? thickness - processingThickness : 0;

                if (_currentProfileSweep == null || _currentProfileSweep.DxfProfileName != profileName || Math.Abs(_currentProfileSweep.VerticalOffset - verticalOffset) > 1e-6)
                {
                    _currentProfileSweep = new ProfileSweep(_dxfProfiles[profileName], verticalOffset);
                    _currentProfileSweep.DxfProfileName = profileName;
                    _currentProfileSweep.IsClockwise = IsClockwise;
                    ProfileSweeps.Add(_currentProfileSweep);
                    _currentProfileSweep.Rail.CurveList.Add(curve);

                }

                else if (_currentProfileSweep != null && _currentProfileSweep.Rail.CurveList.Count > 0)
                {
                    //if (_currentProfileSweep.EndPoint.Equals(curve.StartPoint))
                    if (_currentProfileSweep.EndPoint.DistanceTo(curve.StartPoint) < POINT_DISTANCE)
                    {
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }
                    else if (_currentProfileSweep.StartPoint.DistanceTo(curve.EndPoint) < POINT_DISTANCE)
                    {
                        // Sequenza invertita: i segmenti sono orientati correttamente, ma arrivano in sequenza rovesciata
                        _currentProfileSweep.Rail.CurveList.Insert(0, curve);
                        _currentProfileSweep.RevertedSequence = true;
                    }

                    ////else if (_currentProfileSweep.StartPoint == curve.EndPoint)
                    ////{
                    ////    _currentProfileSweep.Rail.CurveList.Insert(0, curve);
                    ////}
                    ////else if (_currentProfileSweep.StartPoint == curve.StartPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Insert(0, curve);
                    ////}
                    ////else if (_currentProfileSweep.EndPoint == curve.EndPoint)
                    ////{
                    ////    curve.Reverse();
                    ////    _currentProfileSweep.Rail.CurveList.Add(curve);
                    ////}
                    else
                    {
                        _currentProfileSweep = new ProfileSweep(_dxfProfiles[profileName]);
                        _currentProfileSweep.DxfProfileName = profileName;
                        _currentProfileSweep.IsClockwise = IsClockwise;
                        ProfileSweeps.Add(_currentProfileSweep);
                        _currentProfileSweep.Rail.CurveList.Add(curve);
                    }

                }


            }
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
