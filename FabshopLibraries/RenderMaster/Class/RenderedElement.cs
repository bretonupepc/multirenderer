﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RenderMaster.Class
{
    public enum ProjectElementType
    {
        None,
        Backsplash, // Alzatina
        Rib         // Veletta
    }
    public class RenderedElement
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private ProjectElementType _elementType = ProjectElementType.None;

        private double _originalProjectAngle = 0;

        private bool _isUpDownRequired = false;

        private bool _isFlipped = false;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public RenderedElement(ProjectElementType elementType, double originalProjectAngle = 0,
            bool isUpDownRequired = false)
        {
            _elementType = elementType;
            _originalProjectAngle = originalProjectAngle;
            _isUpDownRequired = isUpDownRequired;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public ProjectElementType ElementType
        {
            get { return _elementType; }
        }

        public double OriginalProjectAngle
        {
            get { return _originalProjectAngle; }
        }

        public bool IsUpDownRequired
        {
            get { return _isUpDownRequired; }
        }

        public bool IsFlipped
        {
            get { return _isFlipped; }
            set { _isFlipped = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
