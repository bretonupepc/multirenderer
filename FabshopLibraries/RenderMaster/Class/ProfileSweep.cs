﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Breton.RenderMaster;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using TraceLoggers;

namespace RenderMaster.Class
{
    internal class ProfileSweep
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private CompositeCurve _profile = new CompositeCurve();

        private CompositeCurve _rail = new CompositeCurve();

        private bool _isClockwise;

        private double _thickness;

        private string _dxfProfileName = string.Empty;

        private bool _hasProfile = false;

        private bool _revertedSequence = false;

        private double _verticalOffset = 0;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ProfileSweep()
        {
            
        }

        public ProfileSweep(double thickness)
        {
            _thickness = thickness;
            //Line path = new Line(Plane.YZ, new Point2D(0,0),new Point2D(0,_thickness));
            Line path = new Line(new Point3D(0,0,0),new Point3D(0,0,_thickness));
            //LinearPath path = new LinearPath(0.1, thickness);
            //path.Translate(-0.1, 0, 0);
            //path.Rotate(Math.PI/2, Vector3D.AxisX);
            //path.Regen(Render.CHORDAL_ERROR);
            Profile.CurveList.Add(path);
            Profile.Regen(Render.CHORDAL_ERROR);
            _hasProfile = false;
        }

        public ProfileSweep(CompositeCurve profile)
        {
            _profile = profile;
            _hasProfile = true;

        }
        public ProfileSweep(CompositeCurve profile, double verticalOffset): this(profile)
        {
            _verticalOffset = verticalOffset;

        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Must be defined on XZ Plane, Y=0
        /// </summary>
        public CompositeCurve Profile
        {
            get { return _profile; }
            set { _profile = value; }
        }

        public CompositeCurve Rail
        {
            get { return _rail; }
            set { _rail = value; }
        }

        public double Thickness
        {
            get { return _thickness; }
            set { _thickness = value; }
        }

        public bool IsClockwise
        {
            get { return _isClockwise; }
            set { _isClockwise = value; }
        }

        public Point3D EndPoint
        {
            get
            {
                if (Rail.CurveList.Count > 0)
                    return Rail.EndPoint;
                return null;
            }
        }

        public Point3D StartPoint
        {
            get
            {
                if (Rail.CurveList.Count > 0)
                    return Rail.StartPoint;
                return null;
            }
        }

        public string DxfProfileName
        {
            get { return _dxfProfileName; }
            set { _dxfProfileName = value; }
        }

        public bool RevertedSequence
        {
            get { return _revertedSequence; }
            set { _revertedSequence = value; }
        }

        public double VerticalOffset
        {
            get { return _verticalOffset; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public Mesh CreateMesh()
        {
            if (Profile.CurveList.Count == 0 || Rail.CurveList.Count == 0)
                return null;

            Mesh newMesh = null;

            try
            {
                //if (_hasProfile)
                //{
                //    Profile.Regen(Render.CHORDAL_ERROR);
                //    Rail.Regen(Render.CHORDAL_ERROR);
                //    double profileHeight = Profile.BoxSize.X;
                //    LinearPath path = new LinearPath(Rail.Vertices);
                //    ICurve[] newRail = path.QuickOffset(-profileHeight, Plane.XY,cornerType.Round);
                //    Rail = new CompositeCurve(newRail[0]);
                //    if (_isClockwise)
                //        Rail.CurveList[0].Reverse();
                //}

                Rail.Regen(Render.CHORDAL_ERROR);

                Point3D startPoint = (Point3D)Rail.CurveList[0].StartPoint.Clone();
                Point3D endPoint = Rail.CurveList[Rail.CurveList.Count - 1].EndPoint;
                Vector3D startTangent = (Vector3D)Rail.CurveList[0].StartTangent.Clone();
                Vector3D endTangent = Rail.CurveList[0].StartTangent;

                double startAngle = (startTangent.AngleInXY) - Math.PI/2;//* (_isClockwise ? -1 : -1);

                if (_revertedSequence)
                    startAngle += Math.PI;

                //startAngle += Math.PI;

                var profile = (CompositeCurve) Profile.Clone();

                //profile.Reverse();

                profile.Rotate(startAngle, Vector3D.AxisZ);

                profile.Translate(startPoint.X, startPoint.Y, 0);


                profile.Translate(0, 0, VerticalOffset);


                profile.Regen(Render.CHORDAL_ERROR);

                bool meshDone = false;

                List<Point3D> railPoints = new List<Point3D>();
                LinearPath railPath = null;




                if (!_hasProfile)
                {
                    foreach (ICurve curve in Rail.CurveList)
                    {
                        Arc arc = curve as Arc;
                        if (arc != null)
                        {
                            arc.Regen(Render.CHORDAL_ERROR);
                            railPoints.AddRange(arc.Vertices);
                        }
                        else
                        {
                            Line line = curve as Line;
                            if (line != null)
                            {
                                railPoints.AddRange(line.Vertices);
                            }
                            else
                            {
                                Circle circle = curve as Circle;
                                if (circle != null)
                                {
                                    circle.Regen(Render.CHORDAL_ERROR);
                                    railPoints.AddRange(circle.Vertices);
                                }
                            }

                        }
                    }

                    railPath = new LinearPath(railPoints);
                }


                try
                {
                    if (_hasProfile)
                        newMesh = Mesh.Sweep(Rail, profile, Render.MESH_CHORDAL_ERROR, _hasProfile,
                            Mesh.natureType.RichSmooth);
                    else
                    {
                        newMesh = Mesh.Sweep(railPath, profile, Render.MESH_CHORDAL_ERROR, _hasProfile,
                            Mesh.natureType.RichSmooth);
                    }
                    meshDone = true;
                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("ProfileSweep CreateMesh error ", ex);
                }
                if (!meshDone)
                {
                    try
                    {
                        if (_hasProfile)
                        {
                            newMesh = Mesh.Sweep(Rail, profile, Render.MESH_CHORDAL_ERROR, false,
                                Mesh.natureType.RichSmooth);
                        }
                        else
                        {
                            newMesh = Mesh.Sweep(railPath, profile, Render.MESH_CHORDAL_ERROR, false,
                                Mesh.natureType.RichSmooth);
                        }
                    }
                    catch 
                    {
                        
                    }
                }


                ////Transformation rotoTranslation = new Align3D(Plane.YZ, new Plane(Rail.StartTangent));
                ////profile.TransformBy(rotoTranslation);
                ////profile.Translate(startPoint.X, startPoint.Y, 0);

                //////profile.Rotate(rail.StartTangent.AngleInXY, Vector3D.AxisZ);
                //////profile.Translate(rail.StartPoint.X, rail.StartPoint.Y, rail.StartPoint.Z);

                ////profile.Regen(Render.CHORDAL_ERROR);
                ////newMesh = Mesh.Sweep(Rail, profile, Render.MESH_CHORDAL_ERROR, _hasProfile, Mesh.natureType.RichSmooth);


            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + " CreateMesh error ", ex);
                newMesh = null;
            }
            return newMesh;

        }

        public Mesh GetStartCap()
        {
            Mesh newMesh = null;

            if (!_hasProfile || Rail.CurveList.Count == 0)
                return null;

            try
            {
                newMesh = Mesh.CreatePlanar(Profile, Render.CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                if (_isClockwise)
                    newMesh.Scale(-1, 1, 1);

                Plane meshPlane;
                if (Profile.IsPlanar(Render.CHORDAL_ERROR, out meshPlane))
                {
                    Transformation rotoTranslation = new Align3D(meshPlane, new Plane(Rail.StartTangent));
                    newMesh.TransformBy(rotoTranslation);
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + " GetStartCap error", ex );
                newMesh = null;
            }

            return newMesh;
        }

        public Mesh GetEndCap()
        {
            Mesh newMesh = null;

            if (!_hasProfile || Rail.CurveList.Count == 0)
                return null;

            try
            {
                newMesh = Mesh.CreatePlanar(Profile, Render.CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                if (_isClockwise)
                    newMesh.Scale(-1, 1, 1);

                Plane meshPlane;
                if (Profile.IsPlanar(Render.CHORDAL_ERROR, out meshPlane))
                {
                    Transformation rotoTranslation = new Align3D(meshPlane, new Plane(Rail.EndTangent));
                    newMesh.TransformBy(rotoTranslation);
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(this.ToString() + " GetStartCap error", ex);
                newMesh = null;
            }

            return newMesh;
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
