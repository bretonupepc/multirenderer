using System;
using Breton.SmartResources;

namespace Breton.RenderMaster
{
    /// <summary>
    /// Public variable containing the project resource.
    /// </summary>
    public class ProjResource
    {
        internal static readonly SmartResource gResource;

        /// <summary>
        /// Constructor
        /// </summary>
        static ProjResource()
        {
            gResource = new SmartResource("Resources.RenderMasterResource");
        }

    }
}
