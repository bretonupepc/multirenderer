﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using devDept.Eyeshot;
using TraceLoggers;

namespace RenderMaster.Printing
{
    public class PrintableDocument: PrintDocument
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _title = string.Empty;

        private bool _showBorder ;

        private bool _showTitle;

        private bool _showDateTime;

        private bool _blackAndWhitePrint = true;

        private bool _rasterPrint = false;

        //private int _marginBottom = 10;
        //private int _marginTop = 10;
        //private int _marginRight = 10;
        //private int _marginLeft = 10;

        

        //private bool _landscapeOrientation = false;
        //private string _paperFormat = "A4";

        private BretonHiddenLinesViewPrint _viewPrint = null;

        private ViewportLayout _layout = null;

        private Font _titlesFont = new Font("Tahoma", 12, FontStyle.Regular);

        private Pen _penEdge, _penSilho, _penWire;

        private PrintPreviewForm _printPreviewForm = null;

        private PrintPageEventArgs _lastPrintPageEventArgs = null;

        private bool _showDialog = true;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public PrintableDocument()
        {
            _penSilho = new Pen(Color.Black, 3.0f);
            _penSilho.SetLineCap(LineCap.Round, LineCap.Round, DashCap.Round);

            _penEdge = new Pen(Color.Black, 1.0f);
            _penEdge.SetLineCap(LineCap.Round, LineCap.Round, DashCap.Round);

            _penWire = new Pen(Color.Black, 1.0f);
            _penWire.SetLineCap(LineCap.Round, LineCap.Round, DashCap.Round);
        }

        public PrintableDocument(bool landscape = false, int bottom = 10, int top = 10, int right = 10, int left = 10, bool blackAndWhite = true, string paper = "A4"): this()
        {
            LandscapeOrientation = landscape;
            MarginBottom = bottom;
            MarginLeft = left;
            MarginRight = right;
            MarginTop = top;
            BlackAndWhitePrint = blackAndWhite;
            PaperFormat = paper;
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public bool ShowBorder
        {
            get { return _showBorder; }
            set { _showBorder = value; }
        }

        public bool ShowTitle
        {
            get { return _showTitle; }
            set { _showTitle = value; }
        }

        public bool ShowDateTime
        {
            get { return _showDateTime; }
            set { _showDateTime = value; }
        }

        public bool BlackAndWhitePrint
        {
            get { return _blackAndWhitePrint; }
            set { _blackAndWhitePrint = value; }
        }

        public int MarginBottom
        {
            get
            {
                return (int)(Math.Round(((double)DefaultPageSettings.Margins.Bottom) * 25.4 / 100, 0));
            }
            set
            {
                DefaultPageSettings.Margins.Bottom = (int)(Math.Round(((double)value) * 100 / 25.4, 0));  
            }
        }

        public int MarginTop
        {
            get
            {
                return (int)(Math.Round(((double)DefaultPageSettings.Margins.Top) * 25.4 / 100, 0));
            }
            set
            {
                DefaultPageSettings.Margins.Top = (int)(Math.Round(((double)value) * 100 / 25.4, 0));  
            }
        }

        public int MarginRight
        {
            get
            {
                return (int)(Math.Round(((double)DefaultPageSettings.Margins.Right) * 25.4 / 100, 0));
            }
            set
            {
                DefaultPageSettings.Margins.Right = (int)(Math.Round(((double)value) * 100 / 25.4, 0));  
            }
        }

        public int MarginLeft
        {
            get
            {
                return (int)(Math.Round(((double)DefaultPageSettings.Margins.Left) * 25.4 / 100, 0));
            }
            set
            {
                DefaultPageSettings.Margins.Left = (int)(Math.Round(((double)value) * 100 / 25.4, 0));  
            }
        }

        public bool LandscapeOrientation
        {
            get { return DefaultPageSettings.Landscape; }
            set { DefaultPageSettings.Landscape = value; }
        }

        public string PaperFormat
        {
            get { return DefaultPageSettings.PaperSize.Kind.ToString(); }
            set
            {
                PaperKind kind;
                if (Enum.TryParse(value, true, out kind))
                {
                    PaperSize pSize = GetPaperSize(kind);
                    if (pSize != null)
                    {
                        DefaultPageSettings.PaperSize = pSize;
                    }
                }
            }
        }

        //public Margins Margins
        //{
        //    get { return DefaultPageSettings.Margins; }
        //    set
        //    {
        //        DefaultPageSettings.Margins = value;
        //    }
        //}

        public ViewportLayout Layout
        {
            get { return _layout; }
            set
            {
                bool hasChanged = (_layout != value);
                if (hasChanged)
                {
                    DetachLayoutEvents();
                }

                _layout = value; 

                if (hasChanged)
                {
                    AttachLayoutEvents();
                }

                
            }
        }

        public Font TitlesFont
        {
            get { return _titlesFont; }
            set { _titlesFont = value; }
        }

        public bool RasterPrint
        {
            get { return _rasterPrint; }
            set { _rasterPrint = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public void ShowPreview(bool asDialog = true)
        {
            if (Layout == null)
                return;

            var currentCursor = Layout.Cursor;

            Layout.Cursor = Cursors.WaitCursor;

            _showDialog = asDialog;
            OriginAtMargins = false;
            _viewPrint = new BretonHiddenLinesViewPrint(new HiddenLinesViewSettings(Layout.Viewports[0], Layout, 0.1, true, _penSilho, _penEdge, _penWire, false));
            _viewPrint.Settings.KeepEntityColor = !BlackAndWhitePrint;
            _viewPrint.Settings.KeepEntityLineWeight = true;

            _viewPrint.DoWork(Layout);

            Layout.Cursor = currentCursor;
            PrintPreview();

            //_layout.StartWork(_viewPrint);

        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        protected override void OnPrintPage(PrintPageEventArgs e)
        {
            
            _lastPrintPageEventArgs = new PrintPageEventArgs(e.Graphics, e.MarginBounds, e.PageBounds, e.PageSettings);

            base.OnPrintPage(e);


            e.Graphics.Clear(Color.White);

            RectangleF printable = e.MarginBounds;

            Pen borderPen = new Pen(Color.Black, 1);

            Brush titlesBrush = Brushes.Black;

            SizeF stringSize;

            float titlesBorderHeight = 10F;

            //printable.X += e.PageSettings.HardMarginX;
            //printable.Y += e.PageSettings.HardMarginY;

            int marginFromBorder = 5;

            if (ShowBorder)
            {
                Rectangle borderRectangle = new Rectangle((int)(printable.X - marginFromBorder),
                    (int)(printable.Y - marginFromBorder),
                    (int)( printable.Width + 2 * marginFromBorder),
                    (int)(printable.Height + 2 * marginFromBorder));

                e.Graphics.DrawRectangle(borderPen, borderRectangle);
            }

            if (ShowDateTime || ShowTitle)
            {
                string title = !string.IsNullOrEmpty(Title) ? Title : "Generic";


                stringSize = e.Graphics.MeasureString(title, TitlesFont);

                titlesBorderHeight = stringSize.Height * 1.4F;

                e.Graphics.DrawLine(borderPen, (int)(printable.X - marginFromBorder),
                    (int)(printable.Y + printable.Height + marginFromBorder - titlesBorderHeight),
                    (int)(printable.X + printable.Width + marginFromBorder),
                    (int)(printable.Y + printable.Height + marginFromBorder - titlesBorderHeight));
            }

            if (ShowTitle)
            {
                string title = Title;

                e.Graphics.DrawString(title, TitlesFont, titlesBrush, printable.X, printable.Y + printable.Height + marginFromBorder - titlesBorderHeight * .86F);
            }

            if (ShowDateTime)
            {
                string title = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();

                stringSize = e.Graphics.MeasureString(title, TitlesFont);

                e.Graphics.DrawString(title, TitlesFont, titlesBrush, printable.X + printable.Width - stringSize.Width, printable.Y + printable.Height + marginFromBorder - titlesBorderHeight * .86F);
            }


            if (ShowDateTime || ShowTitle)
            {
                printable.Height -= titlesBorderHeight;
            }

            printable.Inflate(-10F, -10F);

            if (_rasterPrint)
            {
                PrintPageRaster(e, printable);
            }
            else
            {
                PrintPageVector(e, printable);
            }

        }

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void PrintPreview()
        {
            try
            {

                using (_printPreviewForm = new PrintPreviewForm())
                {
                    _printPreviewForm.Document = this;

                    //if (_showDialog)
                    //{
                        _printPreviewForm.ShowDialog(_layout.Parent, false);
                    //}
                    //else
                    //{
                    //    _printPreviewForm.Show(_layout.Parent);
                    //}

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void PrintDirect()
        {
            try
            {

                using (_printPreviewForm = new PrintPreviewForm())
                {
                    _printPreviewForm.Document = this;

                    //if (_showDialog)
                    //{
					_printPreviewForm.DirectPrint();
                    //_printPreviewForm.ShowDialog(_layout.Parent, true);
                    //}
                    //else
                    //{
                    //    _printPreviewForm.Show(_layout.Parent);
                    //}

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void PrintPageVector(PrintPageEventArgs e, RectangleF printable)
        {
            if (_viewPrint != null)
            {
                _viewPrint.Settings.KeepEntityColor = !BlackAndWhitePrint;
                _viewPrint.PrintRect = printable;

                _viewPrint.Print(e);
            }
        }

        private void PrintPageRaster(PrintPageEventArgs e, RectangleF printable)
        {

            RectangleF printRect1 = printable;

            try
            {
                // Draws the first view with a 4x resolution for better quality
                Bitmap bmp = _layout.RenderToBitmap(new Size((int) printRect1.Width*2, (int) printRect1.Height*2), false, true);

                Size scaledSize;
                ScaleImageSizeToPrintRect(printRect1, bmp.Size, out scaledSize);

                if (BlackAndWhitePrint)
                {
                    bmp = MakeGrayscale(bmp);
                }

                e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                e.Graphics.DrawImage(bmp, (int) (printRect1.Left + (printRect1.Width - scaledSize.Width)/2),
                    (int) (printRect1.Top + (printRect1.Height - scaledSize.Height)/2), scaledSize.Width,
                    scaledSize.Height);

                //_layout.Refresh();
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("PrintPageRaster error ", ex);
            }
        }

        private void ScaleImageSizeToPrintRect(RectangleF printRect, Size imageSize, out Size scaledSize)
        {
            double width, height;
            double ratio;

            // fit the width of the image inside the width of the print Rectangle
            ratio = printRect.Width / imageSize.Width;
            width = imageSize.Width * ratio;
            height = imageSize.Height * ratio;

            // fit the other dimension
            if (height > printRect.Height)
            {
                ratio = printRect.Height / height;
                width *= ratio;
                height *= ratio;
            }

            scaledSize = new Size((int)width, (int)height);
        }

        private static Bitmap MakeGrayscale(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][] 
                  {
                     new float[] {.3f, .3f, .3f, 0, 0},
                     new float[] {.59f, .59f, .59f, 0, 0},
                     new float[] {.11f, .11f, .11f, 0, 0},
                     new float[] {0, 0, 0, 1, 0},
                     new float[] {0, 0, 0, 0, 1}
                  });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }

        private PaperSize GetPaperSize(PaperKind kind)
        {
            foreach (PaperSize paper in PrinterSettings.PaperSizes)
            {
                if (paper.Kind == kind)
                    return paper;
            }

            return null;
        }

        private void AttachLayoutEvents()
        {
            if (_layout != null)
            {
                _layout.WorkCompleted += LayoutOnWorkCompleted;
            }
        }

        private void DetachLayoutEvents()
        {
            if (_layout != null)
            {
                _layout.WorkCompleted -= LayoutOnWorkCompleted;
            }
        }

        private void LayoutOnWorkCompleted(object sender, WorkCompletedEventArgs workCompletedEventArgs)
        {
            PrintPreview();

        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }

    class BretonHiddenLinesViewPrint : HiddenLinesViewOnPaper
    {
        public BretonHiddenLinesViewPrint(HiddenLinesViewSettings data)
            : base(data)
        {
        }

        protected override void WorkCompleted(ViewportLayout viewportLayout)
        {
            // Avoid the automatic printing
        }

        

        public HiddenLinesViewSettings Settings
        {
            get { return _hdlViewSettings; }
        }
    }

}
