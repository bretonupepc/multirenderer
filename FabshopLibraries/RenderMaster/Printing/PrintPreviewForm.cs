﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RenderMaster.Printing
{
    public partial class PrintPreviewForm : System.Windows.Forms.Form
    {
        private const double ZOOM_FACTOR = 1.5;
        private bool _blackWhite = true;
        private bool _isChecking = false;
        private PageSetupDialog pageSetupDialog = null;

        private bool _printOnLoad = false;
        public PrintPreviewForm()
        {
            InitializeComponent();
            printPreviewControl1.AutoZoom = true;
            BlackWhite = true;
        }

        public PrintableDocument Document
        {
            get { return (PrintableDocument)printPreviewControl1.Document; }
            set
            {
                printPreviewControl1.Document = value;
                btnSetupPage.Enabled = btnPrint.Enabled = (value != null);
            }
        }

        public bool BlackWhite
        {
            get { return _blackWhite; }
            set
            {
                _blackWhite = value;
                chkBlackWhite.Checked = _blackWhite;
                chkColor.Checked = !_blackWhite;
                if (Document != null)
                {
                    Document.BlackAndWhitePrint = _blackWhite;
                    RefreshPrint();
                }
            }
        }

        private void RefreshPrint()
        {
            printPreviewControl1.InvalidatePreview();
            //Document = Document;
        }


        private void ZoomOut()
        {
            printPreviewControl1.Zoom = printPreviewControl1.Zoom/ZOOM_FACTOR;
        }

        private void ZoomIn()
        {
            printPreviewControl1.Zoom = printPreviewControl1.Zoom * ZOOM_FACTOR;
        }

        private void ZoomFit()
        {
            printPreviewControl1.AutoZoom = true;
        }

        private void ZoomDimension()
        {
            printPreviewControl1.Zoom = 1.0;
        }


        private void btnZoomFit_ButtonClick(object sender, EventArgs e)
        {
            ZoomFit();

        }

        private void btnZoomDimension_ButtonClick(object sender, EventArgs e)
        {
            ZoomDimension();
        }

        private void btnZoomIn_ButtonClick(object sender, EventArgs e)
        {
            ZoomIn();

        }

        private void btnZoomOut_ButtonClick(object sender, EventArgs e)
        {
            ZoomOut();
        }

        private void chkColor_ButtonCheckedChanged(object sender, EventArgs e)
        {
            if (!_isChecking)
            {
                _isChecking = true;
                BlackWhite = !chkColor.Checked;
                _isChecking = false;
            }
        }

        private void chkBlackWhite_ButtonCheckedChanged(object sender, EventArgs e)
        {
            _isChecking = true;
            BlackWhite = chkBlackWhite.Checked;
            _isChecking = false;
        }

        private void btnSetupPage_ButtonClick(object sender, EventArgs e)
        {
            SetupPage();

        }

        private void btnPrint_ButtonClick(object sender, EventArgs e)
        {
            DirectPrint();
        }

        public void DirectPrint()
        {
            var printDialog = new PrintDialog();
            printDialog.Document = Document;
            printDialog.AllowCurrentPage = true;
            printDialog.UseEXDialog = true;
            printDialog.AllowSomePages = true;


            if (printDialog.ShowDialog(this) == DialogResult.OK)
            {
                Document.Print();
            }
        }


        private void SetupPage()
        {
            if (pageSetupDialog == null)
            {
                pageSetupDialog = new PageSetupDialog();
                pageSetupDialog.AllowMargins = true;
                pageSetupDialog.EnableMetric = true;
                pageSetupDialog.AllowPrinter = true;
            }

            pageSetupDialog.Document = Document;

            if (pageSetupDialog.ShowDialog(this) == DialogResult.OK)
            {
                Document = (PrintableDocument)pageSetupDialog.Document;
                
            }
        }

        private void btnExit_ButtonClick(object sender, EventArgs e)
        {
            Close();
        }


        public new void ShowDialog(IWin32Window owner, bool immediatePrint)
        {
            _printOnLoad = immediatePrint;
            base.ShowDialog(owner);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (_printOnLoad)
            {
                DirectPrint();
            }
        }
    }
}
