using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using Breton.DesignUtil;
using Breton.RenderMaster;
using devDept.Eyeshot;
using devDept.Graphics;
using RenderMaster.Printing;
using RenderMaster.Properties;


namespace Breton.RenderWindowUc
{

    /// <summary>
    /// Controllo per la visualizzazione del Render.
    /// </summary>
    public partial class RenderWindowUc : UserControl
    {
		private const string LICENSE_CODE = "EYEPRO-8126-LUFAW-9ET8A-MH9P2";
        Camera vista;

        private bool _loading = true;


        /// <summary>
        /// Costruttore della classe.
        /// </summary>
        public RenderWindowUc()
        {
            InitializeComponent();
            _viewportLayout.Unlock(LICENSE_CODE);
            _viewportLayout.Anchor = AnchorStyles.Top | AnchorStyles.Left;

            _viewportLayout.Viewports[0].Camera.ProjectionMode = projectionType.Orthographic;

            tsChkLight1.Checked = _viewportLayout.Light1.Active;
            tsChkLight2.Checked = _viewportLayout.Light2.Active;
            tsChkLight3.Checked = _viewportLayout.Light3.Active;
            tsChkLight4.Checked = _viewportLayout.Light4.Active;

            _loading = false;



        }

        private void RenderMaster_Load(object sender, EventArgs e)
        {

            //TODO DeC
            // Carica le lingue del form
            ////ProjResource.gResource.ChangeLanguage(lingua);
            ////ProjResource.gResource.LoadStringControl(this);

            DrawToolStrip();

        }

        private void cmdZoomTutto_Click(object sender, EventArgs e)
        {

            _viewportLayout.ZoomFit();
            _viewportLayout.Refresh();

        }

        private void cmdZoomFinestra_CheckedChanged(object sender, EventArgs e)
        {

            bool bolCheck = cmdZoomFinestra.Checked;
            if(cmdZoomPan.Checked) cmdZoomPan.Checked = false;
            cmdZoomFinestra.Checked = bolCheck;
            if (bolCheck)
            {

                _viewportLayout.ActionMode = actionType.ZoomWindow;

            }

            else
            {

                _viewportLayout.ActionMode = actionType.None;

            }

        }

        private void cmdZoomIn_Click(object sender, EventArgs e)
        {

            _viewportLayout.ZoomIn(25);
            _viewportLayout.Invalidate();

        }

        private void cmdZoomOut_Click(object sender, EventArgs e)
        {

            _viewportLayout.ZoomOut(25);
            _viewportLayout.Invalidate();

        }

        private void cmdZoomPan_CheckedChanged(object sender, EventArgs e)
        {

            bool bolCheck = cmdZoomPan.Checked;
            if (cmdZoomFinestra.Checked) cmdZoomFinestra.Checked = false;
            cmdZoomPan.Checked = bolCheck;

            if (bolCheck)
            {

                _viewportLayout.ActionMode = actionType.Pan;

            }

            else
            {

                _viewportLayout.ActionMode = actionType.None;

            }

        }

        // Gestione click destro per uscire dai comandi azione (Zoom Finestra, Pan)
        private void vppRender_MouseClick(object sender, MouseEventArgs e)
        {

            if (e.Button.ToString() == "Right")
            {

                cmdZoomFinestra.Checked = false;
                cmdZoomPan.Checked = false;

                //vppRender1.Action = devDept.Eyeshot.Viewport.actionType.None;

            }

        }

        private void cmdVistaIsometrica_Click(object sender, EventArgs e)
        {

            _viewportLayout.SetView(viewType.Isometric);
            _viewportLayout.Refresh();

        }

        private void cmdVistaFrontale_Click(object sender, EventArgs e)
        {

            _viewportLayout.SetView(viewType.Front);
            _viewportLayout.Refresh();

        }

        private void cmdVistaAlto_Click(object sender, EventArgs e)
        {

            _viewportLayout.SetView(viewType.Top);
            _viewportLayout.Refresh();

        }

        private void cmdVistaLaterale_Click(object sender, EventArgs e)
        {

            _viewportLayout.SetView(viewType.Left);
            _viewportLayout.Refresh();

        }

        private void cmdSalvaVista_Click(object sender, EventArgs e)
        {
            _viewportLayout.SaveView(out vista);
        }

        private void cmdRipristinaVista_Click(object sender, EventArgs e)
        {
            _viewportLayout.RestoreView(vista);
            _viewportLayout.Invalidate();

        }

        private void cmdPrintPreview_Click(object sender, EventArgs e)
        {
            //_viewportLayout.PrintPreview(new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height));
            bool landscape = true, blackwhite = false;
            int bottom = 10, top = 10, left = 10, right = 10;
            string paper = "A4";

            PrintPreview(ref landscape, ref bottom, ref top, ref right, ref left, ref blackwhite, ref paper, true, false, false, false, string.Empty, this.Font);
        }

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            //vppRender1.PageSetup();
            //_viewportLayout.Print();
            bool landscape = true, blackwhite = false;
            int bottom = 10, top = 10, left = 10, right = 10;
            string paper = "A4";
			
            Print(ref landscape, ref bottom, ref top, ref right, ref left, ref blackwhite, ref paper, true, false, false, false, string.Empty, this.Font);
        }
        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalit� grigio</param>
        /// <param name="paper">Formato carta</param>
        /// <param name="rasterPrint">Stampa raster</param>
        /// <param name="showBorder">Disegna bordo di tampa</param>
        /// <param name="showTitle">Visualizza titolo stampa</param>
        /// <param name="showDateTime">Visualizza data e ora</param>
        /// <param name="title">Titolo stampa</param>
        /// <param name="titlesFont">Font titoli stampa</param>
        private void PrintPreview(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper,
            bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont)
        {
            PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left, blackwhite, paper);

            printDocument.Layout = _viewportLayout;

            printDocument.RasterPrint = rasterPrint;
            printDocument.ShowBorder = showBorder;
            printDocument.ShowTitle = showTitle;
            printDocument.ShowDateTime = showDateTime;
            printDocument.Title = title;

            if (titlesFont != null)
                printDocument.TitlesFont = titlesFont;

            printDocument.ShowPreview();

            // Save printer options
            landscape = printDocument.LandscapeOrientation;
            bottom = printDocument.MarginBottom;
            top = printDocument.MarginTop;
            right = printDocument.MarginRight;
            left = printDocument.MarginLeft;
            blackwhite = printDocument.BlackAndWhitePrint;
            paper = printDocument.PaperFormat;

            printDocument.Layout = null;

            printDocument = null;
        }

        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalit� grigio</param>
        /// <param name="paper">Formato carta</param>
        /// <param name="rasterPrint">Stampa raster</param>
        /// <param name="showBorder">Disegna bordo di tampa</param>
        /// <param name="showTitle">Visualizza titolo stampa</param>
        /// <param name="showDateTime">Visualizza data e ora</param>
        /// <param name="title">Titolo stampa</param>
        /// <param name="titlesFont">Font titoli stampa</param>
        private void Print(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper,
            bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont)
        {
            PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left, blackwhite, paper);

            printDocument.Layout = this._viewportLayout;

            printDocument.RasterPrint = rasterPrint;
            printDocument.ShowBorder = showBorder;
            printDocument.ShowTitle = showTitle;
            printDocument.ShowDateTime = showDateTime;
            printDocument.Title = title;

            if (titlesFont != null)
                printDocument.TitlesFont = titlesFont;

            printDocument.PrintDirect();

            // Save printer options
            landscape = printDocument.LandscapeOrientation;
            bottom = printDocument.MarginBottom;
            top = printDocument.MarginTop;
            right = printDocument.MarginRight;
            left = printDocument.MarginLeft;
            blackwhite = printDocument.BlackAndWhitePrint;
            paper = printDocument.PaperFormat;

            printDocument.Layout = null;

            printDocument = null;
        }



   
        #region Variables
        private string lingua = "ITA";
        #endregion

        #region Propriet�

        /// <summary>
        /// Lingua attiva.
        /// </summary>
        public string Lingua
        {
            get { return lingua; }
            set {
                  lingua = value;
                  //CaricaInterfacciaMultiLingua();
                }
        }

        /// <summary>
        /// Procedura del colore di sfondo inferiore dell'area di disegno.
        /// </summary>
        public Color BackgroundBottomColor
        {
            get { return _viewportLayout.Background.BottomColor; }
            set { _viewportLayout.Background.BottomColor = value; }
        }

        /// <summary>
        /// Procedura del colore di sfondo superiore dell'area di disegno.
        /// </summary>
        public Color BackgroundTopColor
        {
            get { return _viewportLayout.Background.TopColor; }
            set { _viewportLayout.Background.TopColor = value; }
        }

        /// <summary>
        /// Mostrare l'origine S/N.
        /// </summary>
        public bool ShowOrigin
        {
            get { return _viewportLayout.OriginSymbol.Visible; }
            set { _viewportLayout.OriginSymbol.Visible = value; }
        }

        /// <summary>
        /// Mostrare l'UCS S/N.
        /// </summary>
        public bool ShowUcsIcon
        {
            get { return _viewportLayout.CoordinateSystemIcon.Visible; }
            set { _viewportLayout.CoordinateSystemIcon.Visible = value; }
        }
        #endregion



        #region Gestione Toolbar men�/buttons

        private void DrawToolStrip()
        {
            Color color1 = System.Drawing.SystemColors.Control;
            Color color2 = System.Drawing.SystemColors.ControlDark;
            Color color3 = System.Drawing.SystemColors.ControlLightLight;

            Bitmap bmp = new Bitmap(10, 45);

            Rectangle rc1 = Rectangle.Empty;
            rc1.Height = bmp.Height / 3;
            rc1.Width = bmp.Size.Width;
            Rectangle rc2 = Rectangle.Empty;
            rc2.Height = bmp.Height / 3;
            rc2.Width = bmp.Size.Width;
            rc2.Offset(0, rc1.Bottom + 1);
            Rectangle rc3 = Rectangle.Empty;
            rc3.Height = (bmp.Height / 3);
            rc3.Width = bmp.Size.Width;
            rc3.Offset(0, rc2.Bottom + 1);

            Rectangle line1 = Rectangle.Empty;
            line1.Height = 1;
            line1.Width = bmp.Size.Width;
            line1.Offset(0, 0);
            Rectangle line2 = Rectangle.Empty;
            line2.Height = 1;
            line2.Width = bmp.Size.Width;
            line2.Offset(0, 1);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color3, color1, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
                    g.FillRectangle(b, rc1);
                using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color1, color1, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
                    g.FillRectangle(b, rc2);
                using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color1, color2, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
                    g.FillRectangle(b, rc3);
                using (Brush b = new System.Drawing.SolidBrush(System.Drawing.SystemColors.ControlDark))
                    g.FillRectangle(b, line1);
                using (Brush b = new System.Drawing.SolidBrush(System.Drawing.SystemColors.ControlLight))
                    g.FillRectangle(b, line2);
            }

            tlsMain.BackgroundImage = bmp;
            tlsMain.BackgroundImageLayout = ImageLayout.Stretch;

        }

        #endregion

        #region protected methods for events


        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            _viewportLayout.Viewports[0].DisplayMode = displayType.Wireframe;
            _viewportLayout.ShowCurveDirection = true;
            _viewportLayout.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _viewportLayout.Viewports[0].DisplayMode = displayType.Rendered;
            _viewportLayout.Invalidate();
        }

        private void btnShowNormals_Click(object sender, EventArgs e)
        {
            _viewportLayout.ShowNormals = !_viewportLayout.ShowNormals;
            _viewportLayout.Invalidate();

        }

        private void tsChkLight_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            if (button != null)
            {
                button.Image = button.Checked ? Resources.BulbOn : Resources.BulbOff;

                if (_loading) return;

                LightSettings settings = null;
                switch (button.Name)
                {
                    case "tsChkLight1":
                        settings = _viewportLayout.Light1;
                        break;

                    case "tsChkLight2":
                        settings = _viewportLayout.Light2;
                        break;

                    case "tsChkLight3":
                        settings = _viewportLayout.Light3;
                        break;

                    case "tsChkLight4":
                        settings = _viewportLayout.Light4;
                        break;

                }

                if (settings != null)
                {
                    if (settings.Active != button.Checked)
                    {
                        settings.Active = button.Checked;
                        _viewportLayout.Invalidate();
                    }
                }
            }
        }

    }

}