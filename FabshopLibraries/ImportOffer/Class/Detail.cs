using System;
using DbAccess;

namespace ImportOffer.Class
{
	public class Detail : DbObject
	{
		#region Variables
		public const string CODE_PREFIX = "ORDET";

		private int mId;
		private string mCode;
		private double mThickness;
        private double mFinalThickness;
		private string mCodeOriginalShape;
        private string mDescription;
        private int mRevision;
        private double mLength;
        private double mWidth;

		private string mShapeInfo;			//Link memorizzazione temporanea file XML

		private string mCustomerProjectCode;
		private bool mCustomerProjectCodeChanged = false;
		private string mStateDet;
		private bool mStateDetChanged = false;
		private string mArticleCode;
		private bool mArticleCodeChanged = false;
		private string mMaterialCode;
		private bool mMaterialCodeChanged = false;

        private ImportState mImportState;           // Stato del dettaglio(pezzo) per l'importazione
        private ImportOpertation mImportOperation;  // Operazione da fare nell'importazione

		//Chiavi dei campi
		public enum Keys 
		{F_NONE = -1, F_ID, F_CODE, F_CUSTOMER_PROJECT_CODE, F_MATERIAL_CODE, F_ARTICLE_CODE, F_STATE, F_THICKNESS, F_CODE_ORIGINAL_SHAPE, 
            F_DESCRIPTION, F_REVISION, F_FINAL_THICKNESS, F_LENGTH, F_WIDTH, F_MAX};

		public enum State 
		{ 
			NONE = -1,
            SHAPE_LOADED,       //Formato Caricato
	        SHAPE_IN_PROGRESS, 	//Formato in Lavorazione
	        SHAPE_COMPLETED,    //Formato Completato
            SHAPE_STANDBY,      //Formato Bloccato
            SHAPE_ANALIZED,     //Formato Analizzato
            SHAPE_RELOADED,     //Formato Ricaricato
            //ORDET_000,
			MAX
		}

        // Stati dei dettagli utilizzati per l'importazione
        public enum ImportState
        {
            NONE = 0,
            LOADED,             // Caricato o ricaricato
            ANALIZED,           // Analizzato
            OPTIMIZING,         // In ottimizzazione
            OPTIMIZED,          // Ottimizzato
            USE_LIST,           // In lista di estrazione (in programmazione, confermata)
            USE_LIST_PROCESSED, // In lista di estrazione (processata)
            IN_PROGRESS,        // In lavorazione
            COMPLETED           // Completato
        }

        // Operazioni possibili nell'importazione
        public enum ImportOpertation
        {
            NONE = 0,
            DELETE,
            INSERT,
            UPDATE
        }

		#endregion 

		#region Constructor
		public Detail(int id, string code, string customerProjectCode, string materialCode, string articleCode, string stateDet,
					  double thickness, string codeOriginalShape, string description, int revision, double finalThickness, double length,
                      double width)
		{
			mId = id;
			mCode = code;
			mThickness = thickness;
			mCodeOriginalShape = codeOriginalShape;
            mDescription = description;
            mRevision = revision;
            mFinalThickness = finalThickness;
            mLength = length;
            mWidth = width;

			mCustomerProjectCode = customerProjectCode;
			mMaterialCode = materialCode;
			mArticleCode = articleCode;
			mStateDet = stateDet;

			mShapeInfo = "";
		}

        public Detail(Detail det) : this(det.gId, det.gCode, det.gCustomerProjectCode, det.gMaterialCode, det.gArticleCode, det.gStateDet, det.gThickness,
                                        det.gCodeOriginalShape, det.gDescription, det.gRevision, det.gFinalThickness, det.gLength, det.gWidth)
        {
        }

		public Detail() : this(0, "", "", "", "", "", 0d, "", "", 0, 0d, 0d, 0d)
		{
		}
		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set 
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public double gThickness
		{
			set 
			{
				mThickness = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mThickness; }
		}

        public double gFinalThickness
        {
            set
            {
                mFinalThickness = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mFinalThickness; }
        }

        public double gLength
        {
            set
            {
                mLength = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mLength; }
        }

        public double gWidth
        {
            set
            {
                mWidth = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mWidth; }
        }

		public string gCodeOriginalShape
		{
			set 
			{
				mCodeOriginalShape = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCodeOriginalShape; }
		}

        public string gDescription
        {
            set
            {
                mDescription = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mDescription; }
        }

        public int gRevision 
        {
            set
            {
                mRevision = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mRevision; }
        }

		public string gStateDet
		{
			set 
			{
				mStateDet = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mStateDetChanged = true;
			}
			get { return mStateDet; }
		}

		public bool gStateDetChanged
		{
			set { mStateDetChanged = value; }
			get { return mStateDetChanged; }
		}

		public string gCustomerProjectCode
		{
			set 
			{
				mCustomerProjectCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mCustomerProjectCodeChanged = true;
			}
			get { return mCustomerProjectCode; }
		}

		public bool gCustomerProjectCodeChanged
		{
			set { mCustomerProjectCodeChanged = value; }
			get { return mCustomerProjectCodeChanged; }
		}

		public string gArticleCode
		{
			set 
			{
				mArticleCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mArticleCodeChanged = true;
			}
			get { return mArticleCode; }
		}

		public bool gArticleCodeChanged
		{
			set { mArticleCodeChanged = value; }
			get { return mArticleCodeChanged; }
		}

		public string gMaterialCode
		{
			set 
			{
				mMaterialCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mMaterialCodeChanged = true;
			}
			get { return mMaterialCode; }
		}

		public bool gMaterialCodeChanged
		{
			set { mMaterialCodeChanged = value; }
			get { return mMaterialCodeChanged; }
		}

		public string gShapeInfo
		{
			set 
			{
				mShapeInfo = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mShapeInfo; }
		}

		internal void iShapeInfo(string link)
		{
			mShapeInfo = link;
		}

        public ImportState gImportState
        {
            set { mImportState = value; }
            get { return mImportState; }
        }

        public ImportOpertation gImportOperation
        {
            set { mImportOperation = value; }
            get { return mImportOperation; }
        }
		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Detail)
			{
				Detail det = (Detail) obj;

				return mCode.CompareTo(det.gCode);
			}

			throw new ArgumentException("object is not a Detail");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == Detail.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == Detail.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == Detail.Keys.F_CUSTOMER_PROJECT_CODE.ToString())
				return mCustomerProjectCode.GetTypeCode();
			if (key.ToUpper() == Detail.Keys.F_MATERIAL_CODE.ToString())
				return mMaterialCode.GetTypeCode();
			if (key.ToUpper() == Detail.Keys.F_ARTICLE_CODE.ToString())
				return mArticleCode.GetTypeCode();
			if (key.ToUpper() == Detail.Keys.F_STATE.ToString())
				return mStateDet.GetTypeCode();
			if (key.ToUpper() == Detail.Keys.F_THICKNESS.ToString())
				return mThickness.GetTypeCode();
			if (key.ToUpper() == Detail.Keys.F_CODE_ORIGINAL_SHAPE.ToString())
				return mCodeOriginalShape.GetTypeCode();
            if (key.ToUpper() == Detail.Keys.F_DESCRIPTION.ToString())
                return mDescription.GetTypeCode();
            if (key.ToUpper() == Detail.Keys.F_REVISION.ToString())
                return mRevision.GetTypeCode();
            if (key.ToUpper() == Detail.Keys.F_FINAL_THICKNESS.ToString())
                return mFinalThickness.GetTypeCode();
            if (key.ToUpper() == Detail.Keys.F_LENGTH.ToString())
                return mLength.GetTypeCode();
            if (key.ToUpper() == Detail.Keys.F_WIDTH.ToString())
                return mWidth.GetTypeCode();
		
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Detail.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}
            
            if (key.ToUpper() == Detail.Keys.F_REVISION.ToString())
            {
                retValue = mRevision;
                return true;
            }

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Detail.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Detail.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == Detail.Keys.F_CUSTOMER_PROJECT_CODE.ToString())
			{
				retValue = mCustomerProjectCode;
				return true;
			}

			if (key.ToUpper() == Detail.Keys.F_MATERIAL_CODE.ToString())
			{
				retValue = mMaterialCode;
				return true;
			}

			if (key.ToUpper() == Detail.Keys.F_ARTICLE_CODE.ToString())
			{
				retValue = mArticleCode;
				return true;
			}

			if (key.ToUpper() == Detail.Keys.F_STATE.ToString())
			{
				retValue = mStateDet;
				return true;
			}

			if (key.ToUpper() == Detail.Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness.ToString();
				return true;
			}

			if (key.ToUpper() == Detail.Keys.F_CODE_ORIGINAL_SHAPE.ToString())
			{
				retValue = mCodeOriginalShape;
				return true;
			}

            if (key.ToUpper() == Detail.Keys.F_DESCRIPTION.ToString())
            {
                retValue = mDescription;
                return true;
            }

            if (key.ToUpper() == Detail.Keys.F_REVISION.ToString())
            {
                retValue = mRevision.ToString();
                return true;
            }

            if (key.ToUpper() == Detail.Keys.F_FINAL_THICKNESS.ToString())
            {
                retValue = mFinalThickness.ToString();
                return true;
            }

            if (key.ToUpper() == Detail.Keys.F_LENGTH.ToString())
            {
                retValue = mLength.ToString();
                return true;
            }

            if (key.ToUpper() == Detail.Keys.F_WIDTH.ToString())
            {
                retValue = mWidth.ToString();
                return true;
            }

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out double retValue)
		{
			if (key.ToUpper() == Detail.Keys.F_THICKNESS.ToString())
			{
				retValue = mThickness;
				return true;
			}

            if (key.ToUpper() == Detail.Keys.F_FINAL_THICKNESS.ToString())
            {
                retValue = mFinalThickness;
                return true;
            }

            if (key.ToUpper() == Detail.Keys.F_LENGTH.ToString())
            {
                retValue = mLength;
                return true;
            }

            if (key.ToUpper() == Detail.Keys.F_WIDTH.ToString())
            {
                retValue = mWidth;
                return true;
            }
			return base.GetField(key, out retValue);
		}

        /// <summary>
        /// Effettua la copia del dettaglio passato sul oggetto
        /// </summary>
        /// <param name="sourceDet">Dettaglio sorgente</param>
        public void Copy(Detail sourceDet)
        {
            this.gThickness = sourceDet.gThickness;
            this.gFinalThickness = sourceDet.gFinalThickness;
            this.gCodeOriginalShape = sourceDet.gCodeOriginalShape;
            this.gDescription = sourceDet.gDescription;
            this.gRevision = sourceDet.gRevision;
            this.gCustomerProjectCode = sourceDet.gCustomerProjectCode;
            this.gStateDet = sourceDet.gStateDet;
            this.gMaterialCode = sourceDet.gMaterialCode;
            this.gShapeInfo = sourceDet.gShapeInfo;
            this.gLength = sourceDet.gLength;
            this.gWidth = sourceDet.gWidth;
        }

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		#endregion
    }
}
