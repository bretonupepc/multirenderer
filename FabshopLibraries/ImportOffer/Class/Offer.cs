using System;
using System.Xml;
using Breton.ImportOffer;
using DbAccess;
using TraceLoggers;

namespace ImportOffer.Class
{
	public class Offer :DbObject
	{
		#region Variables
		public const string CODE_PREFIX = "OFF";

		private int mId;
		private DateTime mDate;
		private string mNote;
		private string mCode;
        private string mDescription;
        private int mRevision;                      // Valore che indica se l'offerta � stata modificata
		private string mXmlParameterLink;			// Link memorizzazione temporanea file XML

		private string mStateOff;
		private bool mStateOffChanged;

		//Chiavi dei campi
		public enum Keys 
		{F_NONE = -1, F_ID, F_CODE, F_DATE, F_STATE, F_XML, F_NOTE, F_DESCRIPTION, F_REVISION, F_MAX};

		public enum State
		{
			NONE = -1,
			OFFER_TO_ANALYZE,	// da analizzare
			OFFER_ANALYZED,	    // analizzato
			OFFER_ERROR,	    // analizzato con problemi
            OFFER_RELOADED,     // ricaricato dopo aver fatto modifiche
			MAX
		}

		public enum errorType
		{
			DUPLICATE_CODE = 10,
		}

		#endregion 

		#region Constructors
		
		public Offer(int id, string code, DateTime insertDate, string state, string note, string description, int revision)
		{
			mId = id;
			mCode = code;
			mDate = insertDate;
			mStateOff = state;
			mNote = note;
            mDescription = description;
            mRevision = revision;

			mXmlParameterLink="";
		}


		public Offer():this(0, "", DateTime.Now, "", "", "", 0)
		{
		}

		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set 
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public DateTime gInsertDate
		{
			set 
			{
				mDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDate; }
		}

		public string gNote
		{
			set 
			{
				mNote = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mNote; }
		}

        public string gDescription
		{
			set 
			{
                mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
            get { return mDescription; }
		}

        public int gRevision
        {
            set
            {
                mRevision = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mRevision; }
        }

		public string gStateOff
		{
			set 
			{
				mStateOff = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mStateOffChanged = true;
			}
			get { return mStateOff; }
		}

		public bool gStateOffChanged
		{
			set { mStateOffChanged = value; }
			get { return mStateOffChanged; }
		}

		public string gXmlParameterLink
		{
			set 
			{
				mXmlParameterLink = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mXmlParameterLink; }
		}

		internal void iSetXmlParameterLink(string link)
		{
			mXmlParameterLink = link;
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Offer)
			{
				Offer off = (Offer) obj;

				return mCode.CompareTo(off.gCode);
			}

			throw new ArgumentException("object is not an Offer");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == Offer.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == Offer.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == Offer.Keys.F_STATE.ToString())
				return mStateOff.GetTypeCode();
			if (key.ToUpper() == Offer.Keys.F_DATE.ToString())
				return mDate.GetTypeCode();
			if (key.ToUpper() == Offer.Keys.F_NOTE.ToString())
				return mNote.GetTypeCode();
            if (key.ToUpper() == Offer.Keys.F_DESCRIPTION.ToString())
                return mDescription.GetTypeCode();
            if (key.ToUpper() == Offer.Keys.F_REVISION.ToString())
                return mRevision.GetTypeCode();
		
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Offer.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}
            if (key.ToUpper() == Offer.Keys.F_REVISION.ToString())
            {
                retValue = mRevision;
                return true;
            }

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Offer.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Offer.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}
			
			if (key.ToUpper() == Offer.Keys.F_STATE.ToString())
			{
				retValue = mStateOff;
				return true;
			}

			if (key.ToUpper() == Offer.Keys.F_NOTE.ToString())
			{
				retValue = mNote;
				return true;
			}

            if (key.ToUpper() == Offer.Keys.F_DESCRIPTION.ToString())
            {
                retValue = mDescription;
                return true;
            }

			if (key.ToUpper() == Offer.Keys.F_DATE.ToString())
			{
				retValue = mDate.ToString();
				return true;
			}

            if (key.ToUpper() == Offer.Keys.F_REVISION.ToString())
            {
                retValue = mRevision.ToString();
                return true;
            }

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{
			if (key.ToUpper() == Offer.Keys.F_DATE.ToString())
			{
				retValue = mDate;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		/***************************************************************************
		 * ReadFileXml
		 * Legge il codice dal file XML
		 * Parametri:
		 *			fileXML			: nome del file XML
		 * Ritorna:
		 *			true	lettura eseguita con successo
		 *			false	errore nella lettura
		 * **************************************************************************/
		public bool ReadFileXml(string fileXML, DbInterface db, out errorType errorCode)
		{
			XmlDocument doc = new XmlDocument();
			OfferCollection mOffer = new OfferCollection(db, null);
			bool exist;
			
			errorCode = 0;
			try
			{
				// Legge il file XML
				doc.Load(fileXML);

				XmlNode chn = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

				// Legge il codice dell'offerta
				if(chn.Name == "Offer")
				{
                    //if (mOffer.CheckExistingCode(chn.Attributes.GetNamedItem("F_CODE").Value, "T_WK_XML_OFFERS", out exist) && !exist)
                    //    mCode = chn.Attributes.GetNamedItem("F_CODE").Value;
                    //else
                    //    errorCode = errorType.DUPLICATE_CODE;

                    mCode = chn.Attributes.GetNamedItem("F_CODE").Value;

                    //DEC
				    exist = false;
                    //mOffer.CheckExistingCode(mCode, "T_WK_XML_OFFERS", out exist);

                    if (exist)
                        errorCode = errorType.DUPLICATE_CODE;

                    if (chn.Attributes.GetNamedItem("Code") != null)
                        if (chn.Attributes.GetNamedItem("Code").Value.Length > 50)
                            mDescription = chn.Attributes.GetNamedItem("Code").Value.Substring(0,50);
                        else
                            mDescription = chn.Attributes.GetNamedItem("Code").Value;

					if (chn.Attributes.GetNamedItem("Adjustement_index") != null)
						mRevision = int.Parse(chn.Attributes.GetNamedItem("Adjustement_index").Value);
					else
						mRevision = -1;
				}

				return true;
			}

			catch (XmlException ex)
			{
				TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
				return false;
			}
			catch(ApplicationException ex)
			{
                TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
                return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
				return false;
			}
			finally
			{
				doc = null;
			}
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		#endregion

	}
}
