using System;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Collections;
using Breton.DesignUtil;
using Breton.Materials;
using DbAccess;
using ImportOffer.Class;
using Materials.Class;
using TraceLoggers;

namespace Breton.ImportOffer
{
	public class Project : DbObject
	{
		#region Variables
		public const string CODE_PREFIX = "PRJ";
		
		private int mId;
		private string mCode;
		private DateTime mDate;
		private DateTime mDeliveryDate;
		private string mDescription;
		private string mCodeOriginalProject;
        private int mRevision;
		private int mPreviewImageId;
		private string mPreviewImageLink;

		private string mCustomerOrderCode;
		private bool mCustomerOrderCodeChanged = false;
		private string mStatePrj;
		private bool mStatePrjChanged = false;
		
		private DetailCollection mDetails;
		//private ArticleCollection mArticles;
		private DbInterface mCollectionDbInterface;

		private const string DEFAULT_TMP_DIR = "Temp_Detail_Xml\\";
		private const string XML_FILE_NAME = "PROJECTANALIZE";
		private string mTempDir = System.IO.Path.GetTempPath() + DEFAULT_TMP_DIR;

        private ImportState mImportState;           // Stato del progetto per l'importazione
        private ImportOpertation mImportOperation;  // Operazione da fare nell'importazione
        private int mDetailsState;                  // Valore con la somma degli stati dei vari progetti

		//Chiavi dei campi
		public enum Keys 
		{F_NONE = -1, F_ID, F_CODE, F_CUSTOMER_ORDER_CODE, F_STATE, F_DATE, F_DELIVERY_DATE, F_DESCRIPTION, 
            F_CODE_ORIGINAL_PROJECT, F_REVISION, F_MAX};

		public enum State 
		{ 
			NONE = -1,
            PROJ_LOADED,        //Progetto Caricato
	        PROJ_IN_PROGRESS,  	//Progetto in Fase di realizzazione
	        PROJ_COMPLETED,     //Progetto Completato
	        PROJ_NDISP,         //Progetto Non Disponibile
            PROJ_ANALIZED,      //Progetto Analizzato
            PROJ_RELOADED,      //Progetto Ricaricato
			//PROJ_000,
			MAX
		}

        // Stati dei progetti utilizzati per l'importazione
        public enum ImportState
        {
            NONE = 0,
            ELIMINABLE,                 // Eliminabile
            NOT_ELIMINABLE_NO_PIECE,    // Non eliminabile direttamente ma senza l'esistenza del pezzo fisico
            NOT_ELIMINABLE_PIECE        // Non eliminabile direttamente con l'esistenza del pezzo fisico
        }

        // Operazioni possibili nell'importazione
        public enum ImportOpertation
        {
            NONE = 0,
            DELETE,
            INSERT,
            UPDATE
        }
		public enum Message
		{
			NONE,
			DA_CONFERMARE,
			NO_MATERIAL,
			NO_CUSTOMER,
			NO_CUT_TO_SIZE
		}

		#endregion 

		#region Constructor
		public Project(DbInterface db, int id, string code, string customerOrderCode, string statePrj, DateTime date, DateTime deliveryDate, string description,
            string codeOriginalProject, int revision)
		{
			mId = id;
			mCode = code;
			mDate = date;
			mDeliveryDate = deliveryDate;
			mDescription = description;
			mCodeOriginalProject = codeOriginalProject;
            mRevision = revision;

			mCustomerOrderCode = customerOrderCode;
			mStatePrj = statePrj;

			mPreviewImageId = 0;
			mPreviewImageLink = "";

			mCollectionDbInterface = db;
			mDetails = new DetailCollection(db);
		}

        public Project(DbInterface db, Project prj): this(db, prj.gId, prj.gCode, prj.gCustomerOrderCode, prj.gStatePrj, prj.gDate, 
               prj.gDeliveryDate, prj.gDescription, prj.gCodeOriginalProject, prj.gRevision)
        {
        }

		public Project(DbInterface db) : this(db, 0, "", "", "", DateTime.Now, DateTime.MaxValue, "", "", 0)
		{
		}
		#endregion

		#region Properties

		public int gId
		{
			set	{ mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set 
			{
				mCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public DateTime gDate
		{
			set 
			{
				mDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDate; }
		}

		public DateTime gDeliveryDate
		{
			set 
			{
				mDeliveryDate = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDeliveryDate; }
		}

		public string gDescription
		{
			set 
			{
				mDescription = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		public string gCodeOriginalProject
		{
			set 
			{
				mCodeOriginalProject = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;
			}
			get { return mCodeOriginalProject; }
		}

        public int gRevision
        {
            set
            {
                mRevision = value;
                if (gState == DbObject.ObjectStates.Unchanged)
                    gState = DbObject.ObjectStates.Updated;
            }
            get { return mRevision; }
        }

		public string gStatePrj
		{
			set 
			{
				mStatePrj = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mStatePrjChanged = true;
			}
			get { return mStatePrj; }
		}

		public bool gStatePrjChanged
		{
			set { mStatePrjChanged = value; }
			get { return mStatePrjChanged; }
		}

		public string gCustomerOrderCode
		{
			set 
			{
				mCustomerOrderCode = value;
				if (gState==DbObject.ObjectStates.Unchanged)
					gState=DbObject.ObjectStates.Updated;

				mCustomerOrderCodeChanged = true;
			}
			get { return mCustomerOrderCode; }
		}

		public bool gCustomerOrderCodeChanged
		{
			set { mCustomerOrderCodeChanged = value; }
			get { return mCustomerOrderCodeChanged; }
		}

        public DetailCollection gDetails
        {
            set { mDetails = value; }
            get { return mDetails; }
        }

        public ImportState gImportState
        {
            set { mImportState = value; }
            get { return mImportState; }
        }

        public ImportOpertation gImportOperation
        {
            set { mImportOperation = value; }
            get { return mImportOperation; }
        }

        public int gDetailsState
        {
            set { mDetailsState = value; }
            get { return mDetailsState; }
        }

		public string TempDir
		{
			get { return mTempDir; }
			set
			{
				mTempDir = value.Trim();
				if (!mTempDir.EndsWith("\\"))
					mTempDir += "\\";
			}
		}

		public int gPreviewImageId
		{
			get { return mPreviewImageId; }
			set { mPreviewImageId = value; }
		}

		public string gPreviewImageLink
		{
			get { return mPreviewImageLink; }
			set { mPreviewImageLink = value; }
		}

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Project)
			{
				Project prj = (Project) obj;

				return mCode.CompareTo(prj.gCode);
			}

			throw new ArgumentException("object is not a Project");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == Project.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == Project.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == Project.Keys.F_STATE.ToString())
				return mStatePrj.GetTypeCode();
			if (key.ToUpper() == Project.Keys.F_DATE.ToString())
				return mDate.GetTypeCode();
			if (key.ToUpper() == Project.Keys.F_DELIVERY_DATE.ToString())
				return mDeliveryDate.GetTypeCode();
			if (key.ToUpper() == Project.Keys.F_DESCRIPTION.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == Project.Keys.F_CODE_ORIGINAL_PROJECT.ToString())
				return mCodeOriginalProject.GetTypeCode();
            if (key.ToUpper() == Project.Keys.F_REVISION.ToString())
                return mRevision.GetTypeCode();
			if (key.ToUpper() == Project.Keys.F_CUSTOMER_ORDER_CODE.ToString())
				return mCustomerOrderCode.GetTypeCode();
		
			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}
	

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Project.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}
            if (key.ToUpper() == Project.Keys.F_REVISION.ToString())
            {
                retValue = mRevision;
                return true;
            }

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Project.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Project.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == Project.Keys.F_DATE.ToString())
			{
				retValue = mDate.ToString();
				return true;
			}

			if (key.ToUpper() == Project.Keys.F_DELIVERY_DATE.ToString())
			{
				retValue = mDeliveryDate.ToString();
				return true;
			}

			if (key.ToUpper() == Project.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == Project.Keys.F_CODE_ORIGINAL_PROJECT.ToString())
			{
				retValue = mCodeOriginalProject;
				return true;
			}

            if (key.ToUpper() == Project.Keys.F_REVISION.ToString())
            {
                retValue = mRevision.ToString();
                return true;
            }

			if (key.ToUpper() == Project.Keys.F_STATE.ToString())
			{
				retValue = mStatePrj;
				return true;
			}

			if (key.ToUpper() == Project.Keys.F_CUSTOMER_ORDER_CODE.ToString())
			{
				retValue = mCustomerOrderCode;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{
			if (key.ToUpper() == Project.Keys.F_DATE.ToString())
			{
				retValue = mDate;
				return true;
			}

			if (key.ToUpper() == Project.Keys.F_DELIVERY_DATE.ToString())
			{
				retValue = mDeliveryDate;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		///***************************************************************************
		/// <summary>
		/// Carica il progetto dal file XML
		/// </summary>
		/// <param name="fileXml">file xml</param>
		/// <param name="node">nodo del file XML</param>
		/// <param name="msg">messaggio ritornato</param>
		/// <returns>
		///		true	lettura eseguita con successo
		///		false	errore nella lettura
		/// </returns>
		///***************************************************************************
		public bool ReadXmlNode(string fileXml, XmlNode node, out Message msg)
		{
            //Article art = new Article();
			Detail det;
			XmlDocument originalDoc = new XmlDocument();
			int qty;
			//Coordinate coord;
			string matCode;
			string shapeNumber;

			msg = Message.NONE;

			try
			{
				// Legge il nodo XML
				XmlNode chn = node.SelectSingleNode("Shapes");

				foreach(XmlNode nd in chn.ChildNodes)
				{
                    if (nd.Name == "Shape")
                    {
                        qty = int.Parse(nd.Attributes.GetNamedItem("Quantity").Value); // Quanti sono i dettagli da caricare di questo tipo
						shapeNumber = nd.Attributes.GetNamedItem("Shape_Number").Value;

						if (fileXml != "" && fileXml != null)
						{
							originalDoc.Load(fileXml);
							if (DeleteAllShapeNodes_Except(ref originalDoc, shapeNumber))
								originalDoc.Save(mTempDir + XML_FILE_NAME + "_" + gCodeOriginalProject.Trim() + "_" + shapeNumber.Trim() + ".xml");
						}

                        for (int i = 0; i < qty; i++)
                        {
                            det = new Detail();
                            //art = new Article();
                            ////coord = new Coordinate();
                            //mArticles = new ArticleCollection(mCollectionDbInterface);

							matCode = MaterialCode(nd, out msg);
							if (matCode == null)	// Materiale non assegnato
							{
								msg = Message.NO_MATERIAL;
								throw new ApplicationException("Some detail have no associate material");
							}

							if (!ExistCutToSize(nd))
							{
								msg = Message.NO_CUT_TO_SIZE;
								throw new ApplicationException("Some detail have no cut to size");
							}

                            // Aggiunge il pezzo trovato al progetto
                            det.gMaterialCode = matCode;													// Materiale
                            det.gThickness = double.Parse(nd.Attributes.GetNamedItem("Thickness").Value);	// Spessore
                            det.gStateDet = Detail.State.SHAPE_LOADED.ToString();							// Stato dettaglio di default
                            det.gCodeOriginalShape = nd.Attributes.GetNamedItem("Shape_Number").Value;		// Codice del dettaglio nel file xml
                            if (nd.Attributes.GetNamedItem("Description").Value.Length > 50)
                                det.gDescription = nd.Attributes.GetNamedItem("Description").Value.Substring(0, 50);    // Descrizione del dettaglio
                            else
                                det.gDescription = nd.Attributes.GetNamedItem("Description").Value;	        // Descrizione del dettaglio
							det.gShapeInfo = mTempDir + XML_FILE_NAME + "_" + gCodeOriginalProject.Trim() + "_" + shapeNumber.Trim() + ".xml";	// Xml del dettagli

                            if (nd.Attributes.GetNamedItem("Adjustement_index") != null)
                                det.gRevision = int.Parse(nd.Attributes.GetNamedItem("Adjustement_index").Value);       // Revisione

                            mDetails.AddObject(det);
                        }
                    }
				}

                // Imposta le informazioni dei pezzi
				if (fileXml != "" && fileXml != null)
					SetDetailsInfo();

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("Project.ReadXmlNode Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Project.ReadXmlNode Error. ", ex);
				return false;
			}
			finally
			{
				originalDoc = null;
			}
		}

		/***************************************************************************
		 * UpdateDetailToDb()
		 * Salva i progetti sul database
		 * Parametri:
		 *			projectCode	:codice del progetto di riferimento
		 * Ritorna:
		 *			true		:salvataggio eseguito con successo
		 *			false		:errore nel salvataggio
		 * **************************************************************************/
		public bool UpdateDetailToDb(int projectId)
		{
			Detail det;
			string projectCode = null;
		
			// Scorre tutti i dettagli
			mDetails.MoveFirst();
			while (!mDetails.IsEOF())
			{
				// Legge il dettaglio attuale
				mDetails.GetObject(out det);
				projectCode = mDetails.CodeFromId(projectId, "T_WK_CUSTOMER_PROJECTS");
				if(projectCode != null)
					det.gCustomerProjectCode = projectCode;
				else
					return false;

				mDetails.MoveNext();
			}

			if (!mDetails.UpdateDataToDb())
				return false;

			if (projectCode != null && !mDetails.GetDataFromDb(Detail.Keys.F_CODE, projectCode))
				return false;

			// Crea gli articoli per i pezzi
			if (!mDetails.CreateArticles())
				return false;

			return true;
		}

        ///***************************************************************************
        // * UpdateArticleToDb()
        // * Salva l'articolo sul database
        // * 
        // * Ritorna:
        // *			il codice dell'articolo salvato
        // * **************************************************************************/
        //public string UpdateArticleToDb()
        //{
        //    //Article art;
        //    int articleId;

        //    if (!mArticles.UpdateDataToDb())
        //        return null;
        //    else
        //    {
        //        articleId = mArticles.gLastId;
        //        return mArticles.CodeFromId(articleId, "T_AN_ARTICLES");
        //    }
        //}

		/// <summary>
		/// Effettua la copia del progetto passato sul oggetto
		/// </summary>
		/// <param name="sourcePrj">Progetto sorgente</param>
		public void Copy(Project sourcePrj)
		{
			this.gCodeOriginalProject = sourcePrj.gCodeOriginalProject;
			this.gDeliveryDate = sourcePrj.gDeliveryDate;
			this.gDescription = sourcePrj.gDescription;
			this.gRevision = sourcePrj.gRevision;
		}
		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		//**********************************************************************
		// GetMaterialCode
		// Imposta il codice del materiale per il dettaglio interessato
		// Parametri:
		//			node	: nodo da analizzare
		// Ritorna
		//			codice del materiale
		//**********************************************************************
		private string MaterialCode(XmlNode node, out Message msg)
		{
			string matCode;
			bool exist;

			msg = Project.Message.NONE;
			try
			{
				// Scorre tutti i sottonodi
				foreach (XmlNode chn in node.ChildNodes)
				{
					if(chn.Name == "Material")
					{
						matCode = chn.Attributes.GetNamedItem("Code").Value;
                        
                        if (matCode == null || matCode.Length == 0)
                            return null;

						if (mCollectionDbInterface != null)
						{
							mDetails.CheckExistingCode(matCode, "T_AN_MATERIALS", out exist);
							if (!exist)
							{
								// Inserisce nuovo materiale
								if (!NewMaterial(chn, mCollectionDbInterface, out msg))
									return null;
							}
							else
							{
								// Aggiorna il materiale esistente
								if (!UpdateMaterial(chn, mCollectionDbInterface, out msg))
									return null;
							}
						}

						return matCode;
					}
				}
				return null;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Project.GetMaterialCode Error.", ex);
				return null;
			}
		}

		/// <summary>
		/// Verifica se nel pezzo sono presenti i grezzi di taglio corretti
		/// </summary>
		/// <param name="nd">nodo shape da verificare</param>
		/// <returns></returns>
		private bool ExistCutToSize(XmlNode nd)
		{
			bool find = false;

			try
			{
				// Se non ha figli ritorna false
				if (!nd.HasChildNodes)
					return false;

				// Scorre tutti i sottonodi
				foreach (XmlNode chn in nd.ChildNodes)
				{
					if (chn.Name == "Cut_To_Size")
					{
						foreach (XmlNode n in chn.ChildNodes)
						{
							// Se il nodo trovato � diverso da Origin o Polygon ritorna errore
							if (n.Name != "Origin" && n.Name != "Polygon" && n.Name != "#comment")
								return false;
						}		

						find = true;
					}
				}

				if (find)
					return true;
				else
					return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Project.ExistCutToSize Error.", ex);
				return false;
			}
		}

		//**********************************************************************
		// NewCustomer
		// Inserisce il nuovo materiale trovato
		// Parametri:
		//			node	: nodo del file xml con i dati relativi al materiale
		//			db		: DbInterface per la connessione al database
		//**********************************************************************
		private bool NewMaterial(XmlNode node, DbInterface db, out Message msg)
		{
			Material mat = new Material();
			MaterialCollection mMaterials = new MaterialCollection(db);

			msg = Project.Message.NONE;
			try
			{
				mat.gClassCode = node.Attributes.GetNamedItem("Class_Code").Value;
				mat.gCode = node.Attributes.GetNamedItem("Code").Value;
				mat.gDescription = node.Attributes.GetNamedItem("Description").Value;
				mat.gFinishingSurface = node.Attributes.GetNamedItem("Finishing_surface").Value;
				mat.gCostM2 = double.Parse(node.Attributes.GetNamedItem("M2_Cost").Value);
				mat.gCostM3 = double.Parse(node.Attributes.GetNamedItem("M3_Cost").Value);
				mat.gLinearCost = double.Parse(node.Attributes.GetNamedItem("Linear_Cost").Value);
				mat.gSlabCost = double.Parse(node.Attributes.GetNamedItem("Slab_Cost").Value);
				mat.gSupplierCode = node.Attributes.GetNamedItem("Supplier").Value;
				mat.gPolish = node.Attributes.GetNamedItem("Polish").Value;
				mat.gComments = node.Attributes.GetNamedItem("Comments").Value;
				mat.gTonality = node.Attributes.GetNamedItem("Tonality").Value;
				mat.gScale = node.Attributes.GetNamedItem("Scale").Value;
				mat.gStateMat = Material.State.MATERIAL_001.ToString();

				mMaterials.AddObject(mat);
				if (!mMaterials.UpdateDataToDb())
				{
					TraceLog.WriteLine("Project.NewMaterial: Error UpdateDataToDb");
					return false;
				}
				msg = Message.DA_CONFERMARE;
				return true;
			}
			catch(Exception ex)
			{
				TraceLog.WriteLine("Project.NewMaterial Error. ", ex);
				return false;
			}
		}

		//**********************************************************************
		// UpdateMaterial
		// Modifica lo stato del materiale trovato
		// Parametri:
		//			node	: nodo del file xml con i dati relativi al materiale
		//			db		: DbInterface per la connessione al database
		//**********************************************************************
		private bool UpdateMaterial(XmlNode node, DbInterface db, out Message msg)
		{
			Material mat;
			MaterialCollection mMaterials = new MaterialCollection(db);
		
			msg = Project.Message.NONE;
			if (mMaterials.GetSingleElementFromDb(node.Attributes.GetNamedItem("Code").Value)
				&& mMaterials.GetObject(out mat))
				if(mat.gStateMat.Trim() == Material.State.MATERIAL_002.ToString())
				{
					mat.gStateMat = Material.State.MATERIAL_001.ToString();
					if(mMaterials.UpdateDataToDb())
					{
						msg = Message.DA_CONFERMARE;
						return true;
					}
					else
					{
						TraceLog.WriteLine("Project.UpdateMaterial. Error UpdateDataToDb");
						return false;
					}
				}
				else if(mat.gStateMat.Trim() == Material.State.MATERIAL_001.ToString())
				{
					msg = Message.DA_CONFERMARE;
					return true;
				}
				else if(mat.gStateMat.Trim() == Material.State.MATERIAL_000.ToString())
				{
					return true;
				}

			return false;
		}

		//**********************************************************************
		// DeleteAllShapeNodes_Except
		// Cancella tutti i nodi shape che non hanno l'id interessato
		// Parametri:
		//			doc		    : xml document da modificare
        //			shapeNumber	: shape number da non eliminare
		//**********************************************************************
		private bool DeleteAllShapeNodes_Except(ref XmlDocument doc, string shapeNumber)
		{
			ArrayList nodes = new ArrayList();
			XmlNode node = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer/Top/Shapes");
			
			try
			{
				foreach(XmlNode nd in node.ChildNodes)
				{
                    if (nd.Name != "Shape" || nd.Attributes.GetNamedItem("Shape_Number").Value != shapeNumber)
						nodes.Add(nd);
				}

				for(int i=0;i<nodes.Count;i++)
				{
					node.RemoveChild((XmlNode)nodes[i]);
				}

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
				return false;
			}
		}

        /// <summary>
        /// Imposta il valore dello spessore finale del 
        /// </summary>
        private void SetDetailsInfo()
        {
            ShapeGeometry spGeo;
            Detail det;
            ArrayList shapes = new ArrayList();
            
            // Carica tutte le shape
            mDetails.MoveFirst();
            while (!mDetails.IsEOF())
            {
                mDetails.GetObject(out det);

                Shape s = new Shape();
                spGeo = new ShapeGeometry();
                // Analisi del file xml
                XmlAnalizer.ReadShapeXml(det.gShapeInfo, ref s, false);
                shapes.Add(s);

                spGeo.LoadShapePath(s);
                det.gWidth = spGeo.Width();
                det.gLength = spGeo.Length();

                mDetails.MoveNext();
            }

            SetFinalThickness(shapes);
        }
                 
        /// <summary>
        /// Imposta il valore dello spessore finale dei pezzi 
        /// </summary>
        private void SetFinalThickness(ArrayList shapes)
        {
            Detail det;
            double maxThick = 0d;

            // Scorre tutti i pezzi trovati
            for (int i = 0; i < shapes.Count; i++)
            {
                Shape s1 = (Shape)shapes[i];

                switch(s1.gType)
                {
                    // Tipo di sagoma normale
                    case Shape.PieceType.NORMAL:
                        // Scorre tutti i pezzi per verificare se ci sono pezzi che devono essere attaccati a questo
                        for (int j = 0; j < shapes.Count; j++)
                        {
                            Shape s2 = (Shape)shapes[j];

                            switch (s2.gType)
                            {
                                // Verifica se l'alzatina � collegata al pezzo principale
                                case Shape.PieceType.VELETTA:
                                    if (s2.gShapeIdJoin == s1.gId)
                                    {
                                        double tmpHeight;
                                        ShapeGeometry spGeo = new ShapeGeometry();
                                        spGeo.LoadShapePath(s2);
                                        tmpHeight = spGeo.Width();
                                        if (tmpHeight > maxThick)
                                            maxThick = tmpHeight;

                                    }
                                    break;

                                // Verifica se la laminazione � collegata al pezzo principale
                                case Shape.PieceType.LAMINATION:
                                    if (s2.gShapeNumberJoin == s1.gShapeNumber)
                                    {
                                        if (s1.gThickness + s2.gThickness > maxThick)
                                            maxThick = s1.gThickness + s2.gThickness;
                                    }
                                    break;
                            }
                        }

                        // Verifica i vgroove
                        for (int j = 0; j < s1.gVGrooves.Count; j++)
                        {
                            Shape.VGroove v = (Shape.VGroove)s1.gVGrooves[j];

                            if (v.height > maxThick)
                                maxThick = v.height;

                        }

                        mDetails.MoveFirst();
                        while (!mDetails.IsEOF())
                        {
                            mDetails.GetObject(out det);
                            if (det.gCodeOriginalShape.Trim() == s1.gShapeNumber.ToString())
                            {
                                if (maxThick > 0)
                                    det.gFinalThickness = maxThick;
                                else
                                    det.gFinalThickness = det.gThickness;
                                break;
                            }
                            mDetails.MoveNext();
                        }

                        break;

                    // Sagoma di tipo alzatina o laminazione
                    case Shape.PieceType.VELETTA:
                    case Shape.PieceType.LAMINATION:
                        mDetails.MoveFirst();
                        while (!mDetails.IsEOF())
                        {
                            mDetails.GetObject(out det);
                            if (det.gCodeOriginalShape.Trim() == s1.gShapeNumber.ToString())
                            {
                                det.gFinalThickness = det.gThickness;
                                break;
                            }
                            mDetails.MoveNext();
                        }
                        break;
                }

                maxThick = 0d;
            }
        }

		#endregion
	}
}
