using System;
using System.Data.OleDb;

using System.Collections;
using System.Collections.Generic;
using DbAccess;
using ImportOffer.Class;
using TraceLoggers;

namespace Breton.ImportOffer
{
	public class OrderCollection : DbCollection
	{
		#region Variables
        private object mUser;
		#endregion

		#region Constructor
		public OrderCollection(DbInterface db, object user)
			: base(db)
		{
			mUser = user;
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Order obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);

			obj = (Order)dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Order obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Order)mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Order obj)
		{
			return (base.AddObject((DbObject)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			string[] codes = { code };
			return GetDataFromDb(Order.Keys.F_NONE, null, codes, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Order.Keys.F_NONE, null, null, null, null);
		}

		/// <summary>
		/// Carica i dati dal database da un offerta
		/// </summary>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <param name="offerCode">codice dell'offera</param>
		/// <returns></returns>
		public bool GetDataFromDb(Order.Keys orderKey, string offerCode)
		{
			return GetDataFromDb(orderKey, offerCode, null, null, null);
		}

		/// <summary>
		/// Carica i dati dal database da un offerta
		/// </summary>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <param name="orderStates">Stati dell'ordine</param>
		/// <returns></returns>
		public bool GetDataFromDb(Order.Keys orderKey, string[] orderStates)
		{
			return GetDataFromDb(orderKey, null, null, null, null, orderStates, true);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Order.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null, null);
		}

		/// <summary>
		/// Legge i dati da database
		/// </summary>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <param name="top">numero di dati da leggere</param>
		/// <returns></returns>
		public bool GetDataFromDb(Order.Keys orderKey, int top)
		{
			return GetDataFromDb(orderKey, null, null, null, null, null, false, top);
		}

		/// <summary>
		/// Legge i dati dal database da una lista di codici
		/// </summary>
		/// <param name="codes">lista dei codici dei dettagli interessati</param>
		/// <returns></returns>
		public bool GetDataFromDb(string[] codes)
		{
			return GetDataFromDb(Order.Keys.F_NONE, null, codes, null, null);
		}

        //public bool GetFilterDataFromDb(Order.Keys orderKey, string code, string customerCode, string customerName, string state,
        //    string fromIns, string toIns, bool isDateIns, string fromDel, string toDel, bool isDateDel, string descr, Order.AdvanceState scheduleState, 
        //    Order.AdvanceState optiState, Order.AdvanceState useListState, bool schedule)
        //{
        //    string sqlOrderBy = "";
        //    string sqlWhere = "";
        //    string sqlAnd = " where ";
        //    string sqlAdvanced = "";

        //    //Estrae solo gli ordini del codice richiesto
        //    if (code.Length > 0)
        //    {
        //        sqlWhere += sqlAnd + "T_WK_CUSTOMER_ORDERS.F_CODE like '%" + DbInterface.QueryString(code.Trim()) + "%'";
        //        sqlAnd = " and ";
        //    }

        //    //Estrae solo gli ordini del cliente richiesto
        //    if (customerCode.Length > 0)
        //    {
        //        sqlWhere += sqlAnd + "T_AN_CUSTOMERS.F_CODE like '%" + DbInterface.QueryString(customerCode.Trim()) + "%'";
        //        sqlAnd = " and ";
        //    }

        //    //Estrae solo gli ordini del cliente richiesto
        //    if (customerName.Length > 0)
        //    {
        //        sqlWhere += sqlAnd + "T_AN_CUSTOMERS.F_NAME like '%" + DbInterface.QueryString(customerName.Trim()) + "%'";
        //        sqlAnd = " and ";
        //    }

        //    //Estrae solo gli ordini inseriti nel periodo richiesto
        //    if (isDateIns)
        //    {
        //        sqlWhere += sqlAnd + "T_WK_CUSTOMER_ORDERS.F_DATE between " + fromIns + " and " + toIns;
        //        sqlAnd = " and ";
        //    }

        //    //Estrae solo gli ordini con data di consegna compresa nel periodo richiesto
        //    if (isDateDel)
        //    {
        //        sqlWhere += sqlAnd + "T_WK_CUSTOMER_ORDERS.F_DELIVERY_DATE between " + fromDel + " and " + toDel;
        //        sqlAnd = " and ";
        //    }

        //    //Estrae solo gli ordini con la descrizione richiesta
        //    if (descr.Length > 0)
        //    {
        //        sqlWhere += sqlAnd + "T_WK_CUSTOMER_ORDERS.F_DESCRIPTION like '%" + DbInterface.QueryString(descr.Trim()) + "%'";
        //        sqlAnd = " and ";
        //    }

        //    //Estrae solo le lastre dello stato richiesto
        //    if (state != null && state.Length > 0)
        //    {
        //        sqlWhere += sqlAnd + "T_CO_CUSTOMER_ORDER_STATES.F_CODE = '" + state + "'";
        //        sqlAnd = " and ";
        //    }

        //    if (schedule)
        //    {
        //        sqlWhere += sqlAnd + " T_WK_SCHEDULES.F_ID is null and s.F_CODE not in ('" + Order.State.ORDER_COMPLETED.ToString() + "', '" + Order.State.ORDER_UNLOADED.ToString() + "') ";
        //        sqlAnd = " AND ";
        //    }			

        //    //if (mUser != null && !mUser.gIsAdministrator)
        //    //    sqlWhere += sqlAnd + " T_WK_XML_OFFERS.F_ID_T_AN_USERS = " + mUser.gId.ToString();

        //    switch (orderKey)
        //    {
        //        case Order.Keys.F_ID:
        //            sqlOrderBy = " ORDER BY T_WK_CUSTOMER_ORDERS.F_ID desc";
        //            break;
        //        case Order.Keys.F_CODE:
        //            sqlOrderBy = " ORDER BY T_WK_CUSTOMER_ORDERS.F_CODE";
        //            break;
        //        case Order.Keys.F_CUSTOMER_CODE:
        //            sqlOrderBy = " ORDER BY T_AN_CUSTOMERS.F_CODE";
        //            break;
        //        case Order.Keys.F_STATE:
        //            sqlOrderBy = " ORDER BY T_CO_CUSTOMER_ORDER_STATES.F_CODE";
        //            break;
        //        case Order.Keys.F_DESCRIPTION:
        //            sqlOrderBy = " ORDER BY T_WK_CUSTOMER_ORDERS.F_DESCRIPTION";
        //            break;
        //        case Order.Keys.F_DATE:
        //            sqlOrderBy = " ORDER BY T_WK_CUSTOMER_ORDERS.F_DATE";
        //            break;
        //        case Order.Keys.F_DELIVERY_DATE:
        //            sqlOrderBy = " ORDER BY T_WK_CUSTOMER_ORDERS.F_DELIVERY_DATE";
        //            break;
        //    }

        //    if (scheduleState != Order.AdvanceState.NONE || optiState != Order.AdvanceState.NONE || useListState != Order.AdvanceState.NONE)
        //    {
        //        sqlWhere = sqlWhere.Replace("where", "");


        //        if (scheduleState != Order.AdvanceState.NONE)
        //        {
        //            // Crea la query base
        //            sqlAdvanced = " select distinct ord.*, " +
        //                                             "T_AN_CUSTOMERS.F_CODE AS F_CUSTOMER_CODE, " +
        //                                             "T_WK_XML_OFFERS.F_CODE AS F_OFFER_CODE, " +
        //                                             "T_CO_CUSTOMER_ORDER_STATES.F_CODE AS F_STATE " +
        //                            "from T_WK_CUSTOMER_ORDERS ord " +
        //                            "inner join T_AN_CUSTOMERS ON ord.F_ID_T_AN_CUSTOMERS = T_AN_CUSTOMERS.F_ID " +
        //                            "inner join T_CO_CUSTOMER_ORDER_STATES ON ord.F_ID_T_CO_CUSTOMER_ORDER_STATES = T_CO_CUSTOMER_ORDER_STATES.F_ID " +
        //                            "inner join T_WK_XML_OFFERS ON ord.F_ID_T_WK_XML_OFFERS = T_WK_XML_OFFERS.F_ID " +
        //                            "left outer join T_ORDERS on ord.F_ID = T_ORDERS.F_ID_T_WK_CUSTOMER_ORDERS " +
        //                            "left outer join T_SHAPES on T_ORDERS.F_ID_ORDER = T_SHAPES.F_ID_ORDER ";

        //            #region Schedule Mode

        //            switch (scheduleState)
        //            {
        //                case Order.AdvanceState.NO_SCHEDULED:
        //                    sqlAdvanced += " where (select count(distinct T_WK_CUSTOMER_ORDER_DETAILS.F_ID) " +
        //                                                "from T_WK_SCHEDULES " +
        //                                                "inner join T_WK_WORK_PHASES_H on T_WK_SCHEDULES.F_ID_T_WK_WORK_PHASES_H = T_WK_WORK_PHASES_H.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
        //                                                "where T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = ord.F_ID) = 0 " + sqlWhere + sqlOrderBy;

        //                    break;

        //                case Order.AdvanceState.PARTIAL_SCHEDULED:
        //                    sqlAdvanced += " where (select count(distinct T_WK_CUSTOMER_ORDER_DETAILS.F_ID) " +
        //                                                "from T_WK_SCHEDULES " +
        //                                                "inner join T_WK_WORK_PHASES_H on T_WK_SCHEDULES.F_ID_T_WK_WORK_PHASES_H = T_WK_WORK_PHASES_H.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
        //                                                "where T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = ord.F_ID) > 0 and " +
        //                                            "(select count(distinct T_WK_CUSTOMER_ORDER_DETAILS.F_ID) " +
        //                                                "from T_WK_SCHEDULES " +
        //                                                "inner join T_WK_WORK_PHASES_H on T_WK_SCHEDULES.F_ID_T_WK_WORK_PHASES_H = T_WK_WORK_PHASES_H.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
        //                                                "where T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = ord.F_ID) < " +
        //                                            "(select count(T_WK_CUSTOMER_ORDER_DETAILS.F_ID) " +
        //                                                "from T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                                "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
        //                                                "where T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = ord.F_ID) ";

        //                    #region Opti state

        //                    // Verifica gli stati di ottimizzazione
        //                    switch (optiState)
        //                    {
        //                        // Estrae solo gli ordini che non sono ancora in ottimizzazione
        //                        case Order.AdvanceState.NO_OPTI:
        //                            sqlAdvanced += " and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select T_ORDERS.F_ID_ORDER " +
        //                                                "from T_SHAPES " +
        //                                                "inner join T_ORDERS on T_SHAPES.F_ID_ORDER = T_ORDERS.F_ID_ORDER " +
        //                                                "left outer join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
        //                                                "where T_OP_ORDERS.F_ID_OP_ORDER is null " +
        //                                                "group by T_ORDERS.F_ID_ORDER)" + sqlWhere + sqlOrderBy;

        //                            break;

        //                        // Estrae solo gli ordini in ottimizzazione                    
        //                        case Order.AdvanceState.OPTIMIZING:
        //                            sqlAdvanced += " and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select F_ID_ORDER " +
        //                                                "from T_SHAPES " +
        //                                                "inner join T_ORDERS on T_SHAPES.F_ID_ORDER = T_ORDERS.F_ID_ORDER " +
        //                                                "inner join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
        //                                                "group by F_ID_ORDER " +
        //                                                "having sum(F_ASSIGNED_QUANTITY) = 0)" + sqlWhere + sqlOrderBy;
        //                            break;

        //                        // Verifica se l'ordine � parzialmente in ottimizzazione
        //                        case Order.AdvanceState.PARTIAL_OPTIMIZED:
        //                            sqlAdvanced += "and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select F_ID_ORDER from T_SHAPES " +
        //                                                "group by F_ID_ORDER " +
        //                                                "having sum(F_ASSIGNED_QUANTITY) < sum(F_INITIAL_QUANTITY) " +
        //                                                        "and sum(F_ASSIGNED_QUANTITY) > 0) " + sqlWhere + sqlOrderBy;
        //                            break;

        //                        case Order.AdvanceState.OPTIMIZED:
        //                            sqlAdvanced += "and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select F_ID_ORDER from T_SHAPES " +
        //                                                "group by F_ID_ORDER " +
        //                                                "having sum(F_ASSIGNED_QUANTITY) = sum(F_INITIAL_QUANTITY) " +
        //                                                    "and sum(F_ASSIGNED_QUANTITY) > 0) " + sqlWhere + sqlOrderBy;
        //                            break;

        //                    }
        //                    #endregion

        //                    break;

        //                case Order.AdvanceState.TOTAL_SCHEDULE:
        //                    sqlAdvanced += " where (select count(distinct T_WK_CUSTOMER_ORDER_DETAILS.F_ID) " +
        //                                                "from T_WK_SCHEDULES " +
        //                                                "inner join T_WK_WORK_PHASES_H on T_WK_SCHEDULES.F_ID_T_WK_WORK_PHASES_H = T_WK_WORK_PHASES_H.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID " +
        //                                                "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
        //                                                "where T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = ord.F_ID) = " +
        //                                            "(select count(T_WK_CUSTOMER_ORDER_DETAILS.F_ID) " +
        //                                                "from T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                                "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
        //                                                "where T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = ord.F_ID) ";

        //                    #region Opti state

        //                    // Verifica gli stati di ottimizzazione
        //                    switch (optiState)
        //                    {
        //                        // Estrae solo gli ordini che non sono ancora in ottimizzazione
        //                        case Order.AdvanceState.NO_OPTI:
        //                            sqlAdvanced += " and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select T_ORDERS.F_ID_ORDER " +
        //                                                "from T_SHAPES " +
        //                                                "inner join T_ORDERS on T_SHAPES.F_ID_ORDER = T_ORDERS.F_ID_ORDER " +
        //                                                "left outer join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
        //                                                "where T_OP_ORDERS.F_ID_OP_ORDER is null " +
        //                                                "group by T_ORDERS.F_ID_ORDER)" + sqlWhere + sqlOrderBy;

        //                            break;

        //                        // Estrae solo gli ordini in ottimizzazione                    
        //                        case Order.AdvanceState.OPTIMIZING:
        //                            sqlAdvanced += " and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select F_ID_ORDER " +
        //                                                "from T_SHAPES " +
        //                                                "inner join T_ORDERS on T_SHAPES.F_ID_ORDER = T_ORDERS.F_ID_ORDER " +
        //                                                "inner join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
        //                                                "group by F_ID_ORDER " +
        //                                                "having sum(F_ASSIGNED_QUANTITY) = 0)" + sqlWhere + sqlOrderBy;
        //                            break;

        //                        // Verifica se l'ordine � parzialmente in ottimizzazione
        //                        case Order.AdvanceState.PARTIAL_OPTIMIZED:
        //                            sqlAdvanced += "and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select F_ID_ORDER from T_SHAPES " +
        //                                                "group by F_ID_ORDER " +
        //                                                "having sum(F_ASSIGNED_QUANTITY) < sum(F_INITIAL_QUANTITY) " +
        //                                                        "and sum(F_ASSIGNED_QUANTITY) > 0) " + sqlWhere + sqlOrderBy;
        //                            break;

        //                        case Order.AdvanceState.OPTIMIZED:
        //                            sqlAdvanced += "and T_ORDERS.F_ID_ORDER in " +
        //                                                "(select F_ID_ORDER from T_SHAPES " +
        //                                                "group by F_ID_ORDER " +
        //                                                "having sum(F_ASSIGNED_QUANTITY) = sum(F_INITIAL_QUANTITY) " +
        //                                                    "and sum(F_ASSIGNED_QUANTITY) > 0) " + sqlWhere + sqlOrderBy;
        //                            break;

        //                    }
        //                    #endregion

        //                    break;


        //            }

        //            #endregion
        //        }
        //        else
        //        {
        //            // Crea la query base
        //            sqlAdvanced = " select distinct T_WK_CUSTOMER_ORDERS.*, " +
        //                                             "T_AN_CUSTOMERS.F_CODE AS F_CUSTOMER_CODE, " +
        //                                             "T_WK_XML_OFFERS.F_CODE AS F_OFFER_CODE, " +
        //                                             "T_CO_CUSTOMER_ORDER_STATES.F_CODE AS F_STATE " +
        //                            "from T_WK_CUSTOMER_ORDERS " +
        //                            "inner join T_AN_CUSTOMERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_AN_CUSTOMERS = T_AN_CUSTOMERS.F_ID " +
        //                            "inner join T_CO_CUSTOMER_ORDER_STATES ON T_WK_CUSTOMER_ORDERS.F_ID_T_CO_CUSTOMER_ORDER_STATES = T_CO_CUSTOMER_ORDER_STATES.F_ID " +
        //                            "inner join T_WK_XML_OFFERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_WK_XML_OFFERS = T_WK_XML_OFFERS.F_ID " +
        //                            "inner join T_ORDERS on T_WK_CUSTOMER_ORDERS.F_ID = T_ORDERS.F_ID_T_WK_CUSTOMER_ORDERS " +
        //                            "inner join T_SHAPES on T_ORDERS.F_ID_ORDER = T_SHAPES.F_ID_ORDER ";

        //            #region Extraction List Mode

        //            // Verifica gli stati di ottimizzazione
        //            switch (optiState)
        //            {
        //                // Estrae solo gli ordini in ottimizzazione                    
        //                case Order.AdvanceState.OPTIMIZING:
        //                    sqlAdvanced += " and T_ORDERS.F_ID_ORDER in " +
        //                                        "(select T_ORDERS.F_ID_ORDER " +
        //                                        "from T_SHAPES " +
        //                                        "inner join T_ORDERS on T_SHAPES.F_ID_ORDER = T_ORDERS.F_ID_ORDER " +
        //                                        "inner join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
        //                                        "group by T_ORDERS.F_ID_ORDER " +
        //                                        "having sum(F_ASSIGNED_QUANTITY) = 0)" + sqlWhere + sqlOrderBy;
        //                    break;

        //                // Verifica se l'ordine � parzialmente in ottimizzazione
        //                case Order.AdvanceState.PARTIAL_OPTIMIZED:
        //                    sqlAdvanced += "where T_ORDERS.F_ID_ORDER in " +
        //                                        "(select F_ID_ORDER from T_SHAPES " +
        //                                        "group by F_ID_ORDER " +
        //                                        "having sum(F_ASSIGNED_QUANTITY) < sum(F_INITIAL_QUANTITY) " +
        //                                                "and sum(F_ASSIGNED_QUANTITY) > 0) ";
        //                    #region Use List State
        //                    switch (useListState)
        //                    {
        //                        case Order.AdvanceState.NO_USE_LIST:
        //                            sqlAdvanced += "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = 'START_PROGRAMMED') > 0 " +
        //                            "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = 'START_PROGRAMMED') = 0 "
        //                            + sqlWhere + sqlOrderBy;
        //                            break;

        //                        case Order.AdvanceState.PARTIAL_USE_LIST:
        //                            sqlAdvanced += "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                           "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                           "T_CO_ARTICLE_STATES.F_CODE = 'START_PROGRAMMED') > 0 " +
        //                            "and " +
        //                                    "(select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > 0" +
        //                            "and " +
        //                                    "((select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                          "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                          "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > " +
        //                                    "(select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "'))" + sqlWhere + sqlOrderBy;
        //                            break;

        //                        case Order.AdvanceState.TOTAL_USE_LIST:
        //                            sqlAdvanced += "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > 0 " +
        //                            "and " +
        //                                    "((select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                          "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                          "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') = " +
        //                                    "(select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "'))" + sqlWhere + sqlOrderBy;
        //                            break;
        //                    }
        //                    #endregion

        //                    break;

        //                case Order.AdvanceState.OPTIMIZED:
        //                    sqlAdvanced += "where T_ORDERS.F_ID_ORDER in " +
        //                                        "(select F_ID_ORDER from T_SHAPES " +
        //                                        "group by F_ID_ORDER " +
        //                                        "having sum(F_ASSIGNED_QUANTITY) = sum(F_INITIAL_QUANTITY) " +
        //                                                "and sum(F_ASSIGNED_QUANTITY) > 0) ";
        //                    #region Use List State
        //                    switch (useListState)
        //                    {
        //                        case Order.AdvanceState.NO_USE_LIST:
        //                            sqlAdvanced += "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > 0 " +
        //                            "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') = 0 "
        //                            + sqlWhere + sqlOrderBy;
        //                            break;

        //                        case Order.AdvanceState.PARTIAL_USE_LIST:
        //                            sqlAdvanced += "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                           "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                           "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > 0 " +
        //                            "and " +
        //                                    "(select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > 0" +
        //                            "and " +
        //                                    "((select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                          "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                          "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > " +
        //                                    "(select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "'))" + sqlWhere + sqlOrderBy;
        //                            break;

        //                        case Order.AdvanceState.TOTAL_USE_LIST:
        //                            sqlAdvanced += "and (select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') > 0 " +
        //                            "and " +
        //                                    "((select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                          "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                          "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "') = " +
        //                                    "(select count(*) " +
        //                                    "from T_WK_CUSTOMER_ORDERS o1 " +
        //                                    "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = o1.F_ID " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "inner join T_WK_USED_OBJECTS on T_WK_USED_OBJECTS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS " +
        //                                    "inner join T_AN_ARTICLES on T_AN_ARTICLES.F_ID = T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES " +
        //                                    "inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
        //                                    "inner join T_CO_ARTICLE_STATES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_STATES = T_CO_ARTICLE_STATES.F_ID " +
        //                                    "where 	T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002') and " +
        //                                            "o1.F_CODE = T_WK_CUSTOMER_ORDERS.F_CODE and " +
        //                                            "T_AN_ARTICLES.F_ID in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS) and " +
        //                                            "T_CO_ARTICLE_STATES.F_CODE = '" + Articles.Article.ArticleStates.START_PROGRAMMED.ToString() + "'))" + sqlWhere + sqlOrderBy;
        //                            break;
        //                    }
        //                    #endregion

        //                    break;

        //            }
        //            #endregion
        //        }

        //        return GetDataFromDb(sqlAdvanced);
        //    }
        //    else
        //        return GetDataFromDb("select distinct T_WK_CUSTOMER_ORDERS.*, " +
        //                                    "T_AN_CUSTOMERS.F_CODE AS F_CUSTOMER_CODE, " +
        //                                    "T_WK_XML_OFFERS.F_CODE AS F_OFFER_CODE, " +
        //                                    "T_CO_CUSTOMER_ORDER_STATES.F_CODE AS F_STATE " +
        //                             "from T_WK_CUSTOMER_ORDERS " +
        //                             "inner join T_AN_CUSTOMERS ON T_AN_CUSTOMERS.F_ID = T_WK_CUSTOMER_ORDERS.F_ID_T_AN_CUSTOMERS " +
        //                             "inner join T_CO_CUSTOMER_ORDER_STATES ON T_CO_CUSTOMER_ORDER_STATES.F_ID = T_WK_CUSTOMER_ORDERS.F_ID_T_CO_CUSTOMER_ORDER_STATES " +
        //                             "inner join T_WK_XML_OFFERS ON T_WK_XML_OFFERS.F_ID = T_WK_CUSTOMER_ORDERS.F_ID_T_WK_XML_OFFERS " +
        //                            (schedule ? "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDERS.F_ID = T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS " +
        //                                    "inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
        //                                    "inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
        //                                    "left outer join T_WK_SCHEDULES on T_WK_WORK_PHASES_H.F_ID = T_WK_SCHEDULES.F_ID_T_WK_WORK_PHASES_H " : "") +
        //                             sqlWhere + sqlOrderBy);
        //}

		public bool GetDataFromDb(Order.Keys orderKey, string offerCode, string[] codes, string customerCode, string state)
		{
			return GetDataFromDb(orderKey, offerCode, codes, customerCode, state, null, false);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		//			offercode	: estrae solo l'elemento associato all'offerta richiesta
		//			customerCode: estrae solo l'elemento del cliente richiesto
		//			state		: estrae solo l'elemento con lo stato richiesto
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Order.Keys orderKey, string offerCode, string[] codes, string customerCode, string state, string[] states, bool schedule)
		{
			return GetDataFromDb(orderKey, offerCode, codes, customerCode, state, states, schedule, -1);
		}
		
		/// <summary>
		/// Legge i dati dal database
		/// </summary>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <param name="offerCode">estrae solo l'elemento associato all'offerta richiesta</param>
		/// <param name="codes"></param>
		/// <param name="customerCode"></param>
		/// <param name="state"></param>
		/// <param name="states"></param>
		/// <param name="schedule"></param>
		/// <param name="top"></param>
		/// <returns></returns>
		public bool GetDataFromDb(Order.Keys orderKey, string offerCode, string[] codes, string customerCode, string state, string[] states, bool schedule, int top)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE ";

			//Estrae solo l'ordine associato all'offerta richiesta
			if (offerCode != null)
			{
				sqlWhere += sqlAnd + "ofr.F_CODE = '" + DbInterface.QueryString(offerCode) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo gli ordini dei codici richiesti
			if (codes != null)
			{
				sqlWhere += sqlAnd + "ord.F_CODE in ('" + codes[0] + "'";
				for (int i = 1; i < codes.Length; i++)
					sqlWhere += ", '" + codes[i] + "'";

				sqlWhere += ")";
				sqlAnd = " AND ";
			}

			//Estrae solo l'ordine del cliente richiesto
			if (customerCode != null)
			{
				sqlWhere += sqlAnd + "c.F_CODE = '" + DbInterface.QueryString(customerCode) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo l'ordine con gli stati richiesti
			if (state != null)
			{
				sqlWhere += sqlAnd + "s.F_CODE= '" + DbInterface.QueryString(state) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo l'ordine con gli stati richiesti
			if (states != null)
			{
				sqlWhere += sqlAnd + "s.F_CODE in ('" + states[0] + "'";
				for (int i = 1; i < states.Length; i++)
					sqlWhere += ", '" + states[i] + "'";

				sqlWhere += ")";
				sqlAnd = " AND ";
			}

            //if (mUser != null && !mUser.gIsAdministrator)
            //    sqlWhere += sqlAnd + " ofr.F_ID_T_AN_USERS = " + mUser.gId.ToString();

			if (schedule)
			{
				sqlWhere += sqlAnd + " T_WK_SCHEDULES.F_ID is null and s.F_CODE not in ('" + Order.State.ORDER_COMPLETED.ToString() + "', '" + Order.State.ORDER_UNLOADED.ToString() + "') ";
				sqlAnd = " AND ";
			}			


			switch (orderKey)
			{
				case Order.Keys.F_ID:
					sqlOrderBy = " ORDER BY ord.F_ID desc";
					break;
				case Order.Keys.F_ID_T_WK_XML_OFFERS:
					sqlOrderBy = " ORDER BY ord.F_ID_T_WK_XML_OFFERS desc";
					break;
				case Order.Keys.F_CODE:
					sqlOrderBy = " ORDER BY ord.F_CODE";
					break;
				case Order.Keys.F_CUSTOMER_CODE:
					sqlOrderBy = " ORDER BY c.F_CODE";
					break;
				case Order.Keys.F_STATE:
					sqlOrderBy = " ORDER BY s.F_CODE";
					break;
				case Order.Keys.F_DESCRIPTION:
					sqlOrderBy = " ORDER BY ord.F_DESCRIPTION";
					break;
				case Order.Keys.F_DATE:
					sqlOrderBy = " ORDER BY ord.F_DATE";
					break;
				case Order.Keys.F_DELIVERY_DATE:
					sqlOrderBy = " ORDER BY ord.F_DELIVERY_DATE";
					break;
			}
			return GetDataFromDb("SELECT distinct " + (top > 0 ? "top " + top : "") + " ord.*, c.F_CODE AS F_CUSTOMER_CODE, ofr.F_CODE AS F_OFFER_CODE, s.F_CODE AS F_STATE " +
				"FROM T_WK_CUSTOMER_ORDERS ord " +
				"inner join T_AN_CUSTOMERS c ON c.F_ID = ord.F_ID_T_AN_CUSTOMERS " +
				"inner join T_CO_CUSTOMER_ORDER_STATES s ON s.F_ID = ord.F_ID_T_CO_CUSTOMER_ORDER_STATES " +
				"inner join T_WK_XML_OFFERS ofr ON ofr.F_ID = ord.F_ID_T_WK_XML_OFFERS " +
				(schedule ? "inner join T_WK_CUSTOMER_PROJECTS on ord.F_ID = T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS " +
							"inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
							"inner join T_WK_WORK_PHASES_H on T_WK_CUSTOMER_ORDER_DETAILS.F_ID = T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
							"left outer join T_WK_SCHEDULES on T_WK_WORK_PHASES_H.F_ID = T_WK_SCHEDULES.F_ID_T_WK_WORK_PHASES_H ": "") +
				sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// 
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "", sqlUpdateUser = "";
			int id;
			int retry = 0;
			string delivDate = "null";
			bool transaction = !mDbInterface.TransactionActive();
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					//Scorre tutti i materiali
					foreach (Order ord in mRecordset)
					{
						switch (ord.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:
								if (transaction) mDbInterface.BeginTransaction();

								if (ord.gDeliveryDate != DateTime.MaxValue)
									delivDate = mDbInterface.OleDbToDate(ord.gDeliveryDate);
								sqlInsert = "INSERT INTO T_WK_CUSTOMER_ORDERS (F_CODE, F_DELIVERY_DATE, F_DESCRIPTION, F_WORK_NUMBER, F_REVISION, F_ID_T_AN_CUSTOMERS, " +
									"F_ID_T_CO_CUSTOMER_ORDER_STATES, F_ID_T_WK_XML_OFFERS) " +
									"(SELECT '" + DbInterface.QueryString(ord.gCode) + "', " + delivDate + ", '" + DbInterface.QueryString(ord.gDescription) + "', '" + 
									DbInterface.QueryString(ord.gWorkNumber) + "', " + ord.gRevision.ToString() + 
									", c.F_ID, s.F_ID, ofr.F_ID FROM T_AN_CUSTOMERS c, T_CO_CUSTOMER_ORDER_STATES s, T_WK_XML_OFFERS ofr " +
									"WHERE c.F_CODE = '" + DbInterface.QueryString(ord.gCustomerCode) + "' AND s.F_CODE = '" + DbInterface.QueryString(ord.gStateOrd) +
									"' AND ofr.F_CODE = '" + DbInterface.QueryString(ord.gOfferCode) + "');SELECT SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

                                //// Aggiorna l'utente che sar� il gestore di questa offerta
                                //sqlUpdateUser = "update T_WK_XML_OFFERS " +
                                //                "set F_ID_T_AN_USERS = " + mUser.gId.ToString() +
                                //                "where F_CODE = '" + DbInterface.QueryString(ord.gOfferCode) + "'";

                                //if (!mDbInterface.Execute(sqlUpdateUser))
                                //    return false;

								// Imposta l'id
								ord.gId = id;

								// Termina la transazione con successo
								if (transaction) mDbInterface.EndTransaction(true);

								// Aggiornamento dei Progetti sul db
								if (!ord.UpdateProjectToDb(ord.gCode))
									throw new ApplicationException("Error un update project to db");

								break;

							//Aggiornamento
							case DbObject.ObjectStates.Updated:
								if (transaction) mDbInterface.BeginTransaction();

								if (ord.gDeliveryDate != DateTime.MaxValue)
									delivDate = mDbInterface.OleDbToDate(ord.gDeliveryDate);
								sqlUpdate = "UPDATE T_WK_CUSTOMER_ORDERS SET " +
									"F_CODE = '" + ord.gCode +
									"', F_DELIVERY_DATE = " + delivDate +
									", F_DESCRIPTION = '" + DbInterface.QueryString(ord.gDescription) +
									"', F_WORK_NUMBER = '" + DbInterface.QueryString(ord.gWorkNumber) +
									"', F_REVISION = " + ord.gRevision.ToString() +
									", F_DATE = " + mDbInterface.OleDbToDate(ord.gDate);

								//Verifica se � stato modificato il cliente dell'ordine
								if (ord.gCustomerCodeChanged)
									sqlUpdate += ", F_ID_T_AN_CUSTOMERS = "
										+ " (SELECT F_ID FROM T_AN_CUSTOMERS"
										+ " WHERE F_CODE = '" + DbInterface.QueryString(ord.gCustomerCode) + "')";

								//Verifica se � stato modificato lo stato dell'ordine
								if (ord.gStateOrdChanged)
									sqlUpdate += ", F_ID_T_CO_CUSTOMER_ORDER_STATES = "
										+ " (SELECT F_ID FROM T_CO_CUSTOMER_ORDER_STATES"
										+ " WHERE F_CODE = '" + DbInterface.QueryString(ord.gStateOrd) + "')";

								//Verifica se � stato modificato l'offerta dell'ordine
								if (ord.gOfferCodeChanged)
									sqlUpdate += ", F_ID_T_WK_XML_OFFERS = "
										+ " (SELECT F_ID FROM T_WK_XML_OFFERS"
										+ " WHERE F_CODE = '" + DbInterface.QueryString(ord.gOfferCode) + "')";

								sqlUpdate += " WHERE F_ID = " + ord.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;
								
								// Termina la transazione con successo
								if (transaction) mDbInterface.EndTransaction(true); 
								
								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Order ord in mDeleted)
					{
						switch (ord.gState)
						{
							//Cancellazione
							case DbObject.ObjectStates.Deleted:
								if (transaction) mDbInterface.BeginTransaction();

								sqlDelete = "DELETE FROM T_WK_CUSTOMER_ORDERS " +
											"WHERE F_ID = " + ord.gId.ToString() + ";";

								if (!mDbInterface.Execute(sqlDelete))
									return false;

								sqlDelete = "update T_WK_XML_OFFERS " +
											"set F_ID_T_CO_XML_OFFER_STATES = (select F_ID " +
																			  "from T_CO_XML_OFFER_STATES " +
																			  "where F_CODE = 'OFFER_TO_ANALYZE')" +
											"where F_CODE = '" + ord.gOfferCode.ToString() + "';";

								if (!mDbInterface.Execute(sqlDelete))
									return false;

								// Termina la transazione con successo
								if (transaction) mDbInterface.EndTransaction(true);

								break;
						}
					}

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

					//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OrderCollection: Deadlock Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("OrderCollection: Exceed Deadlock retrying", ex);
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("OrderCollection: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

		public override void SortByField(string key)
		{
		}

		/// <summary>
		/// Funzione che restituisce un array caricato con i codici delle lastre utilizzate per questo ordine
		/// </summary>
		/// <param name="slabs">array da caricare</param>
		/// <returns></returns>
		public bool GetOrderSlabs(string orderCode, out string[] slabs, bool bUseList)
		{
			OleDbDataReader dr = null;
			string sqlQuery = "";
			slabs = new string[0];
			int i = 0;

			try
			{
				// Cerca tutti gli articoli che saranno utilizzati da questo ordine che siano di tipo lastra o recupero
				sqlQuery = "select distinct a.F_CODE as F_SLAB_CODE " +
							"from T_WK_CUSTOMER_PROJECTS p " +
							"join T_WK_CUSTOMER_ORDERS o on p.F_ID_T_WK_CUSTOMER_ORDERS = o.F_ID " +
							"join T_WK_CUSTOMER_ORDER_DETAILS d on p.F_ID = d.F_ID_T_WK_CUSTOMER_PROJECTS " +
							"join T_WK_WORK_PHASES_H ph on d.F_ID = ph.F_ID_T_WK_CUSTOMER_ORDER_DETAILS " +
							"join T_WK_USED_OBJECTS u on u.F_ID = ph.F_ID_T_WK_USED_OBJECTS " +
							"join T_AN_ARTICLES a on a.F_ID = u.F_ID_T_AN_ARTICLES " +
							"join T_CO_ARTICLE_TYPES aty on a.F_ID_T_CO_ARTICLE_TYPES = aty.F_ID " +
							"where o.F_CODE = '" + orderCode + "' and aty.F_CODE in ('TYPART0001','TYPART0002') " +
							(bUseList ? "and a.F_ID not in (select F_ID_T_AN_ARTICLES from T_WK_USE_LISTS)" : "") +
							"order by a.F_CODE";

				if (!mDbInterface.Requery(sqlQuery, out dr))
				{
					slabs = null;
					return false;
				}

				while (dr.Read())
				{
					string[] sTmp = new string[slabs.Length + 1];
					slabs.CopyTo(sTmp, 0);
					slabs = sTmp;
					slabs[i] = dr["F_SLAB_CODE"].ToString().Trim();
					i++;
				}

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("OrderCollection.GetOrderSlabs Error: ", ex);
				slabs = null;
				return false;
			}
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}

		public bool GetOrderSlabs(string orderCode, out string[] slabs)
		{
			return GetOrderSlabs(orderCode, out slabs, false);
		}

		//**********************************************************************
		// RefreshData
		// Ricarica i dati con l'ultima query eseguita
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool RefreshData()
		{
			return GetDataFromDb(mSqlGetData);
		}

		/// <summary>
		/// Verifica lo stato in cui si trova il pezzo e ne imposta il valore
		/// </summary>
		/// <param name="det">Dettaglio da analizzare</param>
		/// <returns></returns>
		public bool SetExtendedState(ref Order ord)
		{
		    // Verifica se il pezzo � completato
		    if (ord.gStateOrd.Trim() == Order.State.ORDER_COMPLETED.ToString())
		    {
		        ord.gStateExtendedOrd = Order.ExtendedState.COMPLETED.ToString();
		        return true;
		    }
		    // Verifica se il pezzo � in lavoro
		    else if (ord.gStateOrd.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
		    {
		        ord.gStateExtendedOrd = Order.ExtendedState.IN_PROGRESS.ToString();
		        return true;
		    }
			//else if (IsUseList(det, out tmpState))
			//{
			//    switch (tmpState)
			//    {
			//        case Order.ExtendedState.NO_USE_LIST:
			//            break;
			//    }
			//    det.gImportState = Detail.ImportState.USE_LIST_PROCESSED;
			//    return true;
			//}
			//else if (IsUseList(det, false))
			//{
			//    det.gImportState = Detail.ImportState.USE_LIST;
			//    return true;
			//}
			// Verifica se l'ordine � stato ottimizzato
			else if (OptiState(ord))
			{
				//ord.gStateExtendedOrd = Order.ExtendedState.OPTIMIZED.ToString();
				return true;
			}
			// Verifica se il pezzo � stato analizzato dal tecno master
			else if (ord.gStateOrd.Trim() == Order.State.ORDER_ANALIZED.ToString())
			{
				ord.gStateExtendedOrd = Order.ExtendedState.ANALIZED.ToString();
				return true;

			}
			else if (ord.gStateOrd.Trim() == Order.State.ORDER_LOADED.ToString())
			{
				ord.gStateExtendedOrd = Order.ExtendedState.LOADED.ToString();
				return true;
			}
			else
				ord.gStateExtendedOrd = Order.ExtendedState.NONE.ToString();

		    return false;
		}

		public bool SetExtendedStates()
		{
			List<string> ordersCode = new List<string>();
			List<string> orders;
			List<int> assignedQty, initialQty;
			Order ord;

			this.MoveFirst();
			while (!IsEOF())
			{
				GetObject(out ord);

				if (ord.gStateOrd.Trim() == Order.State.ORDER_COMPLETED.ToString())
					ord.gStateExtendedOrd = Order.ExtendedState.COMPLETED.ToString();
				// Verifica se il pezzo � in lavoro
				else if (ord.gStateOrd.Trim() == Order.State.ORDER_IN_PROGRESS.ToString())
					ord.gStateExtendedOrd = Order.ExtendedState.IN_PROGRESS.ToString();
				// Verifica se l'ordine � stato ottimizzato
				else
					ordersCode.Add(ord.gCode.Trim());

				MoveNext();
			}

			if (OptiState(ref ordersCode, out orders, out assignedQty, out initialQty))
			{
				this.MoveFirst();
				while (!IsEOF())
				{
					GetObject(out ord);

					if (orders.Contains(ord.gCode.Trim()))
					{
						int i = orders.IndexOf(ord.gCode.Trim());

						if (assignedQty[i] == 0)
							ord.gStateExtendedOrd = Order.ExtendedState.OPTIMIZING.ToString();
						else if (assignedQty[i] < initialQty[i])
							ord.gStateExtendedOrd = Order.ExtendedState.PARTIAL_OPTIMIZED.ToString();
						else if (assignedQty[i] == initialQty[i])
							ord.gStateExtendedOrd = Order.ExtendedState.OPTIMIZED.ToString();
					}

					MoveNext();
				}
			}

			this.MoveFirst();
			while (!IsEOF())
			{
				GetObject(out ord);

				if (ordersCode.Contains(ord.gCode.Trim()))
				{
					// Verifica se il pezzo � stato analizzato dal tecno master
					if (ord.gStateOrd.Trim() == Order.State.ORDER_ANALIZED.ToString())
					    ord.gStateExtendedOrd = Order.ExtendedState.ANALIZED.ToString();
					else if (ord.gStateOrd.Trim() == Order.State.ORDER_LOADED.ToString())
					    ord.gStateExtendedOrd = Order.ExtendedState.LOADED.ToString();
					else
					    ord.gStateExtendedOrd = Order.ExtendedState.NONE.ToString();

				}
				MoveNext();
			}

			return true;
		}

		/// <summary>
		/// Restituisce gli articoli (lastra o recupero) in cui sono contenuti pezzi dell'ordine
		/// </summary>
		/// <param name="orderCode">ordine</param>
		/// <returns></returns>
		public string[] GetSourceArticleFromOrder(string orderCode)
		{
			OleDbDataReader dr = null;
			string[] articles = null;
			string sqlQuery = "";
			int i = 0;
			try
			{
				// Cerca le lastre contenute nell'ordine
				sqlQuery = "select  T_AN_ARTICLES.F_CODE as F_ARTICLE_CODE" +
							"from T_WK_CUSTOMER_ORDERS" +
							"inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDERS.F_ID = T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS" +
							"inner join T_WK_CUSTOMER_ORDER_DETAILS on T_WK_CUSTOMER_PROJECTS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS " +
							"inner join T_WK_WORK_PHASES_H on T_WK_WORK_PHASES_H.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = T_WK_CUSTOMER_ORDER_DETAILS.F_ID " +
							"inner join T_WK_USED_OBJECTS on T_WK_WORK_PHASES_H.F_ID_T_WK_USED_OBJECTS = T_WK_USED_OBJECTS.F_ID " +
							"inner join T_AN_ARTICLES on T_WK_USED_OBJECTS.F_ID_T_AN_ARTICLES = T_AN_ARTICLES.F_ID " +
							"inner join T_CO_ARTICLE_TYPES on T_AN_ARTICLES.F_ID_T_CO_ARTICLE_TYPES = T_CO_ARTICLE_TYPES.F_ID " +
							"where T_WK_CUSTOMER_ORDERS.F_CODE = '" + orderCode + "' and T_CO_ARTICLE_TYPES.F_CODE in ('TYPART0001','TYPART0002')";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return null;

				while (dr.Read())
				{
					string[] tmp = new string[articles.Length + 1];
					articles.CopyTo(tmp, 0);
					articles = tmp;
					articles[i] = dr["F_ARTICLE_CODE"].ToString().Trim(); ;
					i++;
				}

				return articles;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("OrderCollection.GetSourceArticleFromOrder Error: ", ex);
				return null;
			}
			finally
			{
				mDbInterface.EndRequery(dr);
			}
		}
		#endregion

		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Order ord;

			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					ord = new Order(this.mDbInterface, (int)dr["F_ID"], dr["F_CODE"].ToString(), dr["F_CUSTOMER_CODE"].ToString(), dr["F_OFFER_CODE"].ToString(),
						dr["F_STATE"].ToString(), (DateTime)dr["F_DATE"], (dr["F_DELIVERY_DATE"] != System.DBNull.Value ? (DateTime)dr["F_DELIVERY_DATE"] : DateTime.MaxValue),
						dr["F_DESCRIPTION"].ToString(), dr["F_WORK_NUMBER"].ToString(), (int)dr["F_REVISION"]);

					AddObject(ord);
					ord.gState = DbObject.ObjectStates.Unchanged;

				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("OrderCollection: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			//MoveFirst();
			//while (!IsEOF())
			//{
			//    tecnoCountAll = 0;
			//    tecnoCountSome = 0;
			//    GetObject(out ord);
			//    ord.gProjects.GetDataFromDb(Project.Keys.F_CODE, ord.gCode);
			//    ord.gProjects.MoveFirst();
			//    while (!ord.gProjects.IsEOF())            // tutto l'ordine va a false
			//    {
			//        ord.gProjects.GetObject(out prj);

			//        if (prj.gTecnoAnalyze == (int)Project.Tecno.ALL)
			//            tecnoCountAll += 1;
			//        else if (prj.gTecnoAnalyze == (int)Project.Tecno.SOME)
			//            tecnoCountSome += 1;

			//        ord.gProjects.MoveNext();
			//    }
			//    if (tecnoCountAll == ord.gProjects.Count)
			//        ord.gTecnoAnalyze = (int)Order.Tecno.ALL;
			//    else if (tecnoCountAll == 0 && tecnoCountSome == 0)
			//        ord.gTecnoAnalyze = (int)Order.Tecno.NONE;
			//    else if (tecnoCountSome <= ord.gProjects.Count || tecnoCountAll < ord.gProjects.Count)
			//        ord.gTecnoAnalyze = (int)Order.Tecno.SOME;

			//    MoveNext();
			//}
			//MoveFirst();

			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}
		
		/// <summary>
		///  Indica se il dettaglio si trova in lista di estrazione
		/// </summary>
		/// <param name="det">dettaglio da verificare</param>
		/// <param name="state">stato da ricercare. Se true si ricercher� tra gli stati "processato" altrimenti 
		///			tra gli stati "In Programmazione" e "Confermata"</param>
		/// <returns></returns>
		private bool IsUseList(Order ord, out Order.UseListState uselState, bool state)
		{
		    OleDbDataReader dr = null;
		    string sqlQuery;
		    string[] sourceArticles = null;
			uselState = Order.UseListState.NONE;

		    try
		    {
		        // Verifica che il dettaglio sia associato a una lastra
		        sourceArticles = GetSourceArticleFromOrder(ord.gCode);

		        if (sourceArticles == null || sourceArticles.Length == 0)
		            return false;

				string sqlWhere = "where T_AN_ARTICLES.F_CODE in ('" + sourceArticles[0] + "'";
				for (int i = 1; i < sourceArticles.Length; i++)
					sqlWhere += ", '" + sourceArticles[i] + "'";
				sqlWhere += ")";

				sqlQuery = "select count(*) " +
							"from T_AN_ARTICLES " +
							"inner join T_WK_USE_LISTS on T_WK_USE_LISTS.F_ID_T_AN_ARTICLES = T_AN_ARTICLES.F_ID " +
							"inner join T_CO_USE_LISTS_STATES on T_WK_USE_LISTS.F_ID_T_CO_USE_LISTS_STATES = T_CO_USE_LISTS_STATES.F_ID " +
							sqlWhere;

		        if (!mDbInterface.Requery(sqlQuery, out dr))
		            return false;

		        if (dr.Read())
				{
					int num = (int)dr[0];

					if (num == 0)
						uselState = Order.UseListState.NO_USE_LIST;
					else if (num < sourceArticles.Length)
						uselState = Order.UseListState.PARTIAL_USE_LIST;
					else if (num == sourceArticles.Length)
						uselState = Order.UseListState.USE_LIST;
				}

				return true;
		    }
		    catch (Exception ex)
		    {
		        TraceLog.WriteLine("OrderCollection.IsUseList. Error:", ex);

		        return false;
		    }
		    finally
		    {
		        mDbInterface.EndRequery(dr);
		        dr = null;
		    }
		}

		/// <summary>
		/// Indica se l'ordine � in ottimizzazione ma non � ancora stato piazzato nessun pezzo
		/// </summary>
		/// <param name="ord">ordine</param>
		/// <returns></returns>
		private bool OptiState(Order ord)
		{
			OleDbDataReader dr = null;
			string sqlQuery;
			int aq, iq;

			ord.gStateExtendedOrd = Order.ExtendedState.NONE.ToString();

			try
			{
				sqlQuery = "select sum(F_ASSIGNED_QUANTITY),sum(F_INITIAL_QUANTITY) " +
							"from T_WK_CUSTOMER_ORDERS " +
							"inner join T_AN_CUSTOMERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_AN_CUSTOMERS = T_AN_CUSTOMERS.F_ID " +
							"inner join T_CO_CUSTOMER_ORDER_STATES ON T_WK_CUSTOMER_ORDERS.F_ID_T_CO_CUSTOMER_ORDER_STATES = T_CO_CUSTOMER_ORDER_STATES.F_ID " +
							"inner join T_WK_XML_OFFERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_WK_XML_OFFERS = T_WK_XML_OFFERS.F_ID " +
							"inner join T_ORDERS on T_WK_CUSTOMER_ORDERS.F_ID = T_ORDERS.F_ID_T_WK_CUSTOMER_ORDERS " +
							"inner join T_SHAPES on T_ORDERS.F_ID_ORDER = T_SHAPES.F_ID_ORDER " +
							"where T_ORDERS.F_ID_ORDER in (select T_ORDERS.F_ID_ORDER " +
							"								from T_SHAPES " +
							"								inner join T_ORDERS on T_SHAPES.F_ID_ORDER = T_ORDERS.F_ID_ORDER " +
							"								inner join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
							"								group by T_ORDERS.F_ID_ORDER )" +
							"and T_WK_CUSTOMER_ORDERS.F_CODE = '" + ord.gCode + "'";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				if (dr.Read())
				{
					if (dr[0] == DBNull.Value || dr[1] == DBNull.Value)
						return false;
					aq = (int)dr[0];
					iq = (int)dr[1];

					if (aq == 0)
						ord.gStateExtendedOrd = Order.ExtendedState.OPTIMIZING.ToString();
					else if (aq < iq)
						ord.gStateExtendedOrd = Order.ExtendedState.PARTIAL_OPTIMIZED.ToString();
					else if (aq == iq)
						ord.gStateExtendedOrd = Order.ExtendedState.OPTIMIZED.ToString();
					
					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("OrderCollection.IsOptimizing. Error:", ex);

				return false;
			}
			finally
			{
				mDbInterface.EndRequery(dr);
				dr = null;
			}
		}

		/// <summary>
		/// Indica se l'ordine � in ottimizzazione ma non � ancora stato piazzato nessun pezzo
		/// </summary>
		/// <param name="ord">ordine</param>
		/// <returns></returns>
		private bool OptiState(ref List<string> orders, out List<string> codes, out List<int> assignedQty, out List<int> initialQty)
		{
			OleDbDataReader dr = null;
			string sqlQuery;

			codes = new List<string>();
			assignedQty = new List<int>();
			initialQty = new List<int>();

			//ord.gStateExtendedOrd = Order.ExtendedState.NONE.ToString();
			string list = "";
			string comma = "";
			foreach (string o in orders)
			{
				list += comma + "'" + o +"'";
				comma = ",";
			}

			try
			{
				sqlQuery = "select T_WK_CUSTOMER_ORDERS.F_CODE, sum(F_ASSIGNED_QUANTITY),sum(F_INITIAL_QUANTITY) " +
							"from T_WK_CUSTOMER_ORDERS " +
							"inner join T_AN_CUSTOMERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_AN_CUSTOMERS = T_AN_CUSTOMERS.F_ID " +
							"inner join T_CO_CUSTOMER_ORDER_STATES ON T_WK_CUSTOMER_ORDERS.F_ID_T_CO_CUSTOMER_ORDER_STATES = T_CO_CUSTOMER_ORDER_STATES.F_ID " +
							"inner join T_WK_XML_OFFERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_WK_XML_OFFERS = T_WK_XML_OFFERS.F_ID " +
							"inner join T_ORDERS on T_WK_CUSTOMER_ORDERS.F_ID = T_ORDERS.F_ID_T_WK_CUSTOMER_ORDERS " +
							"inner join T_SHAPES on T_ORDERS.F_ID_ORDER = T_SHAPES.F_ID_ORDER " +
							"where T_ORDERS.F_ID_ORDER in (select T_ORDERS.F_ID_ORDER " +
							"								from T_SHAPES " +
							"								inner join T_ORDERS on T_SHAPES.F_ID_ORDER = T_ORDERS.F_ID_ORDER " +
							"								inner join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
							"								group by T_ORDERS.F_ID_ORDER )" +
							"and T_WK_CUSTOMER_ORDERS.F_CODE in (" + list + ") " +
							"group by T_WK_CUSTOMER_ORDERS.F_CODE";

				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					if (dr[1] == DBNull.Value || dr[2] == DBNull.Value)
						continue;

					codes.Add(dr[0].ToString().Trim());
					assignedQty.Add((int)dr[1]);
					initialQty.Add((int)dr[2]);

					// Rimuove dalla lista l'item appena analizzato
					orders.Remove(dr[0].ToString().Trim());
				}

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("OrderCollection.IsOptimizing. Error:", ex);

				return false;
			}
			finally
			{
				mDbInterface.EndRequery(dr);
				dr = null;
			}
		}
		#endregion
	}
}
