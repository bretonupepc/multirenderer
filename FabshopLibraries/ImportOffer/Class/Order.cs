using System;
using System.Collections;
using System.Xml;
using Breton.ImportOffer;
using DbAccess;
using TraceLoggers;

namespace ImportOffer.Class
{
	public class Order : DbObject
	{
		#region Variables
		public const string CODE_PREFIX = "ORD";

		private int mId;
		private string mCode;
		private DateTime mDate;
		private DateTime mDeliveryDate;
		private string mDescription;
		private string mWorkNumber;
		private int mRevision;

		private string mCustomerCode;
		private bool mCustomerCodeChanged = false;
		private string mOfferCode;
		private bool mOfferCodeChanged = false;
		private string mStateOrd;
		private bool mStateOrdChanged = false;
		private string mStateExtendedOrd;

		private ProjectCollection mProjects;
		private DbInterface mCollectionDbInterface;

		private const string DEFAULT_TMP_DIR = "Temp_Detail_Xml\\";
		private const string XML_FILE_NAME = "ORDER_ANALIZE.xml";
		private string mTempDir = System.IO.Path.GetTempPath() + DEFAULT_TMP_DIR;
		//private bool mTecnoAnalyze = false;
		//private int mTecnoAnalyze = 0;

		//Chiavi dei campi
		public enum Keys
		{ F_NONE = -1, F_ID, F_CODE, F_CUSTOMER_CODE, F_ID_T_WK_XML_OFFERS, F_OFFER_CODE, F_STATE, F_DATE, F_DELIVERY_DATE, F_WORK_NUMBER, F_DESCRIPTION, F_REVISION, F_MAX };

		public enum State
		{
			NONE = -1,
			ORDER_LOADED,       //Ordine Caricato
			ORDER_CONFIRMED,    //Ordine Confermato
			ORDER_IN_PROGRESS,  //Ordine in fase di realizzazione
			ORDER_COMPLETED,    //Ordine Completato
			ORDER_UNLOADED,     //Ordine Scaricato
			ORDER_NDISP,        //Ordine Non Disponibile
			ORDER_ANALIZED,     //Ordine Analizzato
			ORDER_RELOADED,     //Ordine Ricaricato
			MAX
		}

		public enum AdvanceState
		{
			NONE = -1,
			NO_OPTI,			// Non in ottimizzazione
			OPTIMIZING,         // In ottimizzazione
			PARTIAL_OPTIMIZED,  // Parzialmente ottimizzato
			OPTIMIZED,          // Ottimizzato
			NO_USE_LIST,        // Non in lista di estrazione
			PARTIAL_USE_LIST,   // Parzialmente in lista di estrazione
			TOTAL_USE_LIST,     // Tutto in lista di estrazione
			NO_SCHEDULED,		// Non schedulato
			PARTIAL_SCHEDULED,	// Parzialmente schedulato
			TOTAL_SCHEDULE,		// Tutto schedulato
			MAX
		}

		public enum ExtendedState
		{
			NONE = -1,
			LOADED,				// Ordine Caricato
			ANALIZED,			// Ordine Analizzato
			OPTIMIZING,			// Ordine in ottimizzazione
			PARTIAL_OPTIMIZED,  // Parzialmente ottimizzato
			OPTIMIZED,          // Ottimizzato
			IN_PROGRESS,		// Ordine in fase di realizzazione
			COMPLETED,			// Ordine Completato
		}

		public enum UseListState
		{
			NONE = -1,
			NO_USE_LIST,		// Non � in lista di estrazione
			PARTIAL_USE_LIST,	// Parzialmente in lista di estrazione
			USE_LIST			// In lista di estrazione
		}

		public enum Tecno
		{
			NONE = 0,
			ALL,
			SOME
		}

		#endregion

		#region Constructor
		

		public Order(DbInterface db, int id, string code, string customerCode, string offerCode, string stateOrd, DateTime date,
			DateTime deliveryDate, string description, string workNumber, int revision)
		{
			mId = id;
			mCode = code;
			mDate = date;
			mDeliveryDate = deliveryDate;
			mDescription = description;
			mWorkNumber = workNumber;
			mRevision = revision;

			mCustomerCode = customerCode;
			mOfferCode = offerCode;
			mStateOrd = stateOrd;

			mCollectionDbInterface = db;
			mProjects = new ProjectCollection(db);
		}
		
		public Order(DbInterface db, int id, string code, string customerCode, string offerCode, string stateOrd, DateTime date,
			DateTime deliveryDate, string description, int revision)
			: this(db, id, code, customerCode, offerCode, stateOrd, date, deliveryDate, description, "", revision)
		{
		}

		public Order(DbInterface db)
			: this(db, 0, "", "", "", "", DateTime.Now, DateTime.MaxValue, "", 0)
		{
		}
		#endregion

		#region Properties

		public int gId
		{
			set { mId = value; }
			get { return mId; }
		}

		public string gCode
		{
			set
			{
				mCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mCode; }
		}

		public DateTime gDate
		{
			set
			{
				mDate = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDate; }
		}

		public DateTime gDeliveryDate
		{
			set
			{
				mDeliveryDate = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDeliveryDate; }
		}

		public string gDescription
		{
			set
			{
				mDescription = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mDescription; }
		}

		public string gWorkNumber
		{
			set
			{
				mWorkNumber = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mWorkNumber; }
		}

		public int gRevision
		{
			set
			{
				mRevision = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;
			}
			get { return mRevision; }
		}

		public string gStateOrd
		{
			set
			{
				mStateOrd = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;

				mStateOrdChanged = true;
			}
			get { return mStateOrd; }
		}

		public bool gStateOrdChanged
		{
			set { mStateOrdChanged = value; }
			get { return mStateOrdChanged; }
		}

		public string gCustomerCode
		{
			set
			{
				mCustomerCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;

				mCustomerCodeChanged = true;
			}
			get { return mCustomerCode; }
		}

		public bool gCustomerCodeChanged
		{
			set { mCustomerCodeChanged = value; }
			get { return mCustomerCodeChanged; }
		}

		public string gOfferCode
		{
			set
			{
				mOfferCode = value;
				if (gState == DbObject.ObjectStates.Unchanged)
					gState = DbObject.ObjectStates.Updated;

				mOfferCodeChanged = true;
			}
			get { return mOfferCode; }
		}

		public bool gOfferCodeChanged
		{
			set { mOfferCodeChanged = value; }
			get { return mOfferCodeChanged; }
		}

		//public int gTecnoAnalyze
		//{
		//    set { mTecnoAnalyze = value; }
		//    get { return mTecnoAnalyze; }
		//}

		public ProjectCollection gProjects
		{
			set { mProjects = value; }
			get { return mProjects; }
		}

		public string TempDir
		{
			get { return mTempDir; }
			set 
			{ 
				mTempDir = value.Trim();
				if (!mTempDir.EndsWith("\\"))
					mTempDir += "\\";
			}
		}

		public string gStateExtendedOrd
		{
			set { mStateExtendedOrd = value; }
			get { return mStateExtendedOrd; }
		}
		

		#endregion

		#region IComparable interface

		//**********************************************************************
		// CompareTo
		// Esegue la comparazione con un altro oggetto dello stesso tipo
		// Parametri:
		//			obj	: oggetto
		//**********************************************************************
		public override int CompareTo(object obj)
		{
			if (obj is Order)
			{
				Order ord = (Order)obj;

				return mCode.CompareTo(ord.gCode);
			}

			throw new ArgumentException("object is not an Order");
		}

		#endregion

		#region Public Methods

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			index	: indice del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(int index)
		{
			if (IsFieldIndexOk(index))
				return GetFieldType(((Keys)index).ToString());
			else
				return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetFieldType
		// Ritorna il tipo del campo
		// Parametri:
		//			key	: nome del campo
		// Ritorna:
		//			tipo del campo
		//**********************************************************************
		public override TypeCode GetFieldType(string key)
		{
			if (key.ToUpper() == Order.Keys.F_ID.ToString())
				return mId.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_CODE.ToString())
				return mCode.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_STATE.ToString())
				return mStateOrd.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_DATE.ToString())
				return mDate.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_DELIVERY_DATE.ToString())
				return mDeliveryDate.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_DESCRIPTION.ToString())
				return mDescription.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_WORK_NUMBER.ToString())
				return mWorkNumber.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_REVISION.ToString())
				return mRevision.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_CUSTOMER_CODE.ToString())
				return mCustomerCode.GetTypeCode();
			if (key.ToUpper() == Order.Keys.F_OFFER_CODE.ToString())
				return mOfferCode.GetTypeCode();


			return TypeCode.DBNull;
		}

		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			index		: indice del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(int index, out int retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out double retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}

		public override bool GetField(int index, out string retValue)
		{
			if (IsFieldIndexOk(index))
				return base.GetField(index, out retValue);
			else
				return GetField(((Keys)index).ToString(), out retValue);
		}


		//**********************************************************************
		// GetField
		// Ritorna il valore del campo
		// Parametri:
		//			key			: nome del campo
		//			retValue	: valore ritornato
		// Ritorna:
		//			true	valore ritornato
		//			false	campo non valido
		//**********************************************************************
		public override bool GetField(string key, out int retValue)
		{
			if (key.ToUpper() == Order.Keys.F_ID.ToString())
			{
				retValue = mId;
				return true;
			}
			if (key.ToUpper() == Order.Keys.F_REVISION.ToString())
			{
				retValue = mRevision;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out string retValue)
		{
			if (key.ToUpper() == Order.Keys.F_ID.ToString())
			{
				retValue = mId.ToString();
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_CODE.ToString())
			{
				retValue = mCode;
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_DATE.ToString())
			{
				retValue = mDate.ToString();
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_DELIVERY_DATE.ToString())
			{
				retValue = mDeliveryDate.ToString();
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_DESCRIPTION.ToString())
			{
				retValue = mDescription;
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_WORK_NUMBER.ToString())
			{
				retValue = mWorkNumber;
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_REVISION.ToString())
			{
				retValue = mRevision.ToString();
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_STATE.ToString())
			{
				retValue = mStateOrd;
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_CUSTOMER_CODE.ToString())
			{
				retValue = mCustomerCode;
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_OFFER_CODE.ToString())
			{
				retValue = mOfferCode;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		public override bool GetField(string key, out DateTime retValue)
		{
			if (key.ToUpper() == Order.Keys.F_DATE.ToString())
			{
				retValue = mDate;
				return true;
			}

			if (key.ToUpper() == Order.Keys.F_DELIVERY_DATE.ToString())
			{
				retValue = mDeliveryDate;
				return true;
			}

			return base.GetField(key, out retValue);
		}

		///***************************************************************************
		/// <summary>
		/// Carica l'ordine dal file XML
		/// </summary>
		/// <param name="fileXML">nome del file XML</param>
		/// <param name="msg">eventuale messaggio di ritorno</param>
		/// <returns>
		///		true	lettura eseguita con successo
		///		false	errore nella lettura
		/// </returns>
		///***************************************************************************
		public bool ReadFileXml(string fileXML, out Project.Message msg)
		{
			XmlDocument doc = new XmlDocument();
			XmlDocument originalDoc = new XmlDocument();
			bool exist;
			Project prj;
			int qty;
			string idProject;

			// Nome file xml parziale da salvare in seguito nel database
			string tmpXmlFile = mTempDir + XML_FILE_NAME;

			msg = Project.Message.NONE;
			try
			{
				// Legge il file XML
				doc.Load(fileXML);

				XmlNode chn = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

				// Imposto il codice dell'ordine
				if (chn.Attributes.GetNamedItem("F_CODE") != null)
					mCode = chn.Attributes.GetNamedItem("F_CODE").Value;

				// Imposto la data di consegna dell'ordine
				if (chn.Attributes.GetNamedItem("Terms_of_delivery") != null)
					mDeliveryDate = DateTime.Parse(chn.Attributes.GetNamedItem("Terms_of_delivery").Value);

				if (chn.Attributes.GetNamedItem("Code") != null)
					if (chn.Attributes.GetNamedItem("Code").Value.Length > 50)
						mDescription = chn.Attributes.GetNamedItem("Code").Value.Substring(0, 50);
					else
						mDescription = chn.Attributes.GetNamedItem("Code").Value;

				if (chn.Attributes.GetNamedItem("Number") != null)
					if (chn.Attributes.GetNamedItem("Number").Value.Length > 50)
						mWorkNumber = chn.Attributes.GetNamedItem("Number").Value.Substring(0, 50);
					else
						mWorkNumber = chn.Attributes.GetNamedItem("Number").Value;

				if (chn.Attributes.GetNamedItem("Adjustement_index") != null)
					mRevision = int.Parse(chn.Attributes.GetNamedItem("Adjustement_index").Value);


				foreach (XmlNode nd in chn.ChildNodes)
				{
					if (nd.Name == "Customer")
					{
						//Imposta il codice del cliente all'ordine
						if (nd.Attributes.GetNamedItem("F_CODE").Value != null && nd.Attributes.GetNamedItem("F_CODE").Value != "0")
							this.gCustomerCode = nd.Attributes.GetNamedItem("F_CODE").Value;
						else
						{
							if (mCollectionDbInterface != null)
							{
								msg = Project.Message.NO_CUSTOMER;
								throw new ApplicationException("No associate customer");
							}
							else
								this.gCustomerCode = "void";
						}

						if (mCollectionDbInterface != null)
						{
							OrderCollection orders = new OrderCollection(mCollectionDbInterface, null);

							orders.CheckExistingCode(nd.Attributes.GetNamedItem("F_CODE").Value, "T_AN_CUSTOMERS", out exist);
							if (!exist)	// Inserisce un nuovo cliente
							{
								if (!NewCustomer(nd, mCollectionDbInterface)) return false;
							}
							else		// Aggiorna i dati del cliente
							{
								if (!UpdateCustomer(nd, mCollectionDbInterface)) return false;
							}
						}
					}
					if (nd.Name == "Top")
					{
						qty = int.Parse(nd.Attributes.GetNamedItem("Quantity").Value);	// Quanti sono i progetti da caricare di questo tipo

						if (tmpXmlFile != "")
						{
							originalDoc.Load(fileXML);
							idProject = nd.Attributes.GetNamedItem("Id_Top").Value;
							if (DeleteAllProjectNodes_Except(ref originalDoc, idProject))
								originalDoc.Save(tmpXmlFile);
						}

						for (int i = 0; i < qty; i++)
						{
							prj = new Project(mCollectionDbInterface);
							prj.TempDir = mTempDir;
							prj.gStatePrj = Project.State.PROJ_LOADED.ToString();					    // Stato progetto di default
							prj.gCodeOriginalProject = nd.Attributes.GetNamedItem("Id_Top").Value;      // Codice del progetto nel file xml
							prj.gDeliveryDate = mDeliveryDate;
							if (nd.Attributes.GetNamedItem("Code").Value.Length > 50)
								prj.gDescription = nd.Attributes.GetNamedItem("Code").Value.Substring(0, 50);			// Descrizione
							else
								prj.gDescription = nd.Attributes.GetNamedItem("Code").Value;		    // Descrizione
							if (!prj.ReadXmlNode(tmpXmlFile, nd, out msg))// Legge il progetto per caricare tutti i pezzi
								throw new ApplicationException("Error creating Project");
							mProjects.AddObject(prj);

							if (nd.Attributes.GetNamedItem("Adjustement_index") != null)
								prj.gRevision = int.Parse(nd.Attributes.GetNamedItem("Adjustement_index").Value);   // Revisione
						}
					}
				}

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("Order.ReadFileXml Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Order.ReadFileXml Error. ", ex);
				return false;
			}
			finally
			{
				doc = null;
				originalDoc = null;
			}
		}

		/***************************************************************************
		 * UpdatePrjoectToDb()
		 * Salva i progetti sul database
		 * Parametri:
		 *			orderCode	:codice dell'ordine di riferimento
		 * Ritorna:
		 *			true	:salvataggio eseguito con successo
		 *			false	:errore nel salvataggio
		 * **************************************************************************/
		public bool UpdateProjectToDb(string orderCode)
		{
			Project prj;

			// Scorre tutte le classi di materiale lette
			mProjects.MoveFirst();
			while (!mProjects.IsEOF())
			{
				// Legge la classe di materiale attuale
				mProjects.GetObject(out prj);
				prj.gCustomerOrderCode = orderCode;
				mProjects.MoveNext();
			}

			if (!mProjects.UpdateDataToDb())
				return false;

			return true;
		}

		/// <summary>
		/// Effettua la copia dell'ordine passato sul oggetto
		/// </summary>
		/// <param name="sourceOrd">Ordine sorgente</param>
		public void Copy(Order sourceOrd)
		{
			this.gCustomerCode = sourceOrd.gCustomerCode;
			this.gDeliveryDate = sourceOrd.gDeliveryDate;
			this.gDescription = sourceOrd.gDescription;
			this.gRevision = sourceOrd.gRevision;
		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// IsFieldIndexOk
		// Verifica se l'indice del campo � valido
		// Parametri:
		//			index	: indice
		// Ritorna:
		//			true	indice valido
		//			false	indice non valido
		//**********************************************************************
		private bool IsFieldIndexOk(int index)
		{
			return (index > (int)Keys.F_NONE && index < (int)Keys.F_MAX);
		}

		//**********************************************************************
		// NewCustomer
		// Inserisce il nuovo cliente trovato
		// Parametri:
		//			node	: nodo del file xml con i dati relativi al cliente
		//			db		: DbInterface per la connessione al database
		//**********************************************************************
		private bool NewCustomer(XmlNode node, DbInterface db)
		{
				return true;
		}

		//**********************************************************************
		// UpdateCustomer
		// Aggiorna il cliente trovato
		// Parametri:
		//			node	: nodo del file xml con i dati relativi al cliente
		//			db		: DbInterface per la connessione al database
		//**********************************************************************
		private bool UpdateCustomer(XmlNode node, DbInterface db)
		{

				return true;
		}

		//**********************************************************************
		// DeleteAllProjectNodes_Except
		// Cancella tutti i nodi progetto che non hanno l'id interessato
		// Parametri:
		//			doc		: xml document da modificare
		//			idPrj	: id del progetto da non eliminare
		//**********************************************************************
		private bool DeleteAllProjectNodes_Except(ref XmlDocument doc, string idPrj)
		{
			ArrayList nodes = new ArrayList();
			XmlNode node = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

			try
			{
				foreach (XmlNode nd in node.ChildNodes)
				{
					if (nd.Name != "Top" || nd.Attributes.GetNamedItem("Id_Top").Value != idPrj)
						nodes.Add(nd);
				}

				for (int i = 0; i < nodes.Count; i++)
				{
					node.RemoveChild((XmlNode)nodes[i]);
				}

				return true;
			}
			catch (XmlException ex)
			{
				TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
				return false;
			}
		}

		#endregion
	}

	/// <summary>
	/// Classe per la descrizione degli stati degli oggetti in lista di estrazione
	/// </summary>
	public class OrderStateCombo
	{
		private Order.State mState;
		private string mStateDescription;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="state">state</param>
		/// <param name="description">state description</param>
		public OrderStateCombo(Order.State state, string description)
		{
			mState = state;
			mStateDescription = description;
		}

		public Order.State State
		{
			get { return mState; }
		}

		public string StateDescription
		{
			get { return mStateDescription; }
		}

		public override string ToString()
		{
			return StateDescription;
		}
	}
}
