using System;
using System.Data.OleDb;

using System.Collections;
using DbAccess;
using ImportOffer.Class;
using TraceLoggers;

namespace Breton.ImportOffer
{
	public class OfferCollection: DbCollection
    {
        #region Variables
        #endregion

        #region Constructors
        public OfferCollection(DbInterface db, object user): base(db)
		{
            //mUser = user;
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Offer obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (Offer) dbObj;
			return ret;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Offer obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Offer) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Offer obj)
		{
			return (base.AddObject((DbObject) obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			return GetDataFromDb(Offer.Keys.F_NONE, code, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Offer.Keys.F_NONE, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Offer.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null);
		}

		/// <summary>
		/// Legge i dati da database
		/// </summary>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <param name="top">numero di dati da leggere</param>
		/// <returns></returns>
		public bool GetDataFromDb(Offer.Keys orderKey, int top)
		{
			return GetDataFromDb(orderKey, "", null, top);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		//			state		: estrae solo gli elementi dello stato specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Offer.Keys orderKey, string code, string state)
		{
			return GetDataFromDb(orderKey, code, state, -1);
		}

		public bool GetDataFromDb(Offer.Keys orderKey, string code, string state, int top)
		{
			string[] codes;

			if(code == "" || code == null)
				codes = null;
			else
				codes = new string[1] { code };

			return GetDataFromDb(orderKey, codes, state, top);
		}

		public bool GetDataFromDb(Offer.Keys orderKey, string[] codes, string state, int top)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE ";

			//Estrae solo le offerte con i codici richiesti
			if (codes != null)
			{
				sqlWhere += sqlAnd + "o.F_CODE in ('" + codes[0] + "'";
				for (int i = 1; i < codes.Length; i++)
					sqlWhere += ", '" + codes[i] + "'";

				sqlWhere += ")";
				sqlAnd = " AND ";
			}

			//Estrae solo le offerte con lo stato richiesto
			if (state != null)
			{
				sqlWhere += sqlAnd + "s.F_CODE= " + DbInterface.QueryString(state);
				sqlAnd = " AND ";
			}

            //// Estra tutte le offerte associate a questo utente e quelle che non hanno nessun utente associato
            //if (mUser != null && !mUser.gIsAdministrator)
            //    sqlWhere += sqlAnd + " u.F_ID = " + mUser.gId.ToString() + " or o.F_ID_T_AN_USERS is null";

			switch (orderKey)
			{
				case Offer.Keys.F_ID:
					sqlOrderBy = " order by o.F_ID desc";
					break;
				case Offer.Keys.F_CODE:
                    sqlOrderBy = " order by o.F_CODE";
					break;
				case Offer.Keys.F_NOTE:
                    sqlOrderBy = " order by o.F_NOTE";
					break;
                case Offer.Keys.F_DESCRIPTION:
                    sqlOrderBy = " order by o.F_DESCRIPTION";
                    break;
				case Offer.Keys.F_DATE:
                    sqlOrderBy = " order by o.F_DATE";
					break;
				case Offer.Keys.F_STATE:
                    sqlOrderBy = " order by s.F_CODE";
					break;
			}

		    string sqlCommand = "select " + (top > 0 ? "top " + top : "") +
		                        " o.F_ID, o.F_CODE, o.F_NOTE, o.F_DESCRIPTION, o.F_DATE, o.F_REVISION " +
		                        "from T_WK_XML_OFFERS o " +
		                        //"join T_CO_XML_OFFER_STATES s on s.F_ID = o.F_ID_T_CO_XML_OFFER_STATES " +
		                        //"left outer join T_AN_USERS u on o.F_ID_T_AN_USERS = u.F_ID" +
		                        sqlWhere + sqlOrderBy;
			return GetDataFromDb(sqlCommand);
		}

        public bool GetFilterDataFromDb(Offer.Keys orderKey, string code, string customerCode, string customerName,
                    string fromIns, string toIns, bool isDateIns, string descr,
                    double spessore, string materiale)
        {

            string sqlOrderBy = "";
            string sqlWhere = "";
            string sqlAnd = " where ";
            string sqlAdvanced = "";

            //Estrae solo gli ordini del codice richiesto
            if (code.Length > 0)
            {
                sqlWhere += sqlAnd + "T_WK_XML_OFFERS.F_CODE like '%" + DbInterface.QueryString(code.Trim()) + "%'";
                sqlAnd = " and ";
            }

            //Estrae solo gli ordini del cliente richiesto
            if (customerCode.Length > 0)
            {
                sqlWhere += sqlAnd + "T_AN_CUSTOMERS.F_CODE like '%" + DbInterface.QueryString(customerCode.Trim()) + "%'";
                sqlAnd = " and ";
            }

            //Estrae solo gli ordini del cliente richiesto
            if (customerName.Length > 0)
            {
                sqlWhere += sqlAnd + "T_AN_CUSTOMERS.F_NAME like '%" + DbInterface.QueryString(customerName.Trim()) + "%'";
                sqlAnd = " and ";
            }

            //Estrae solo le offerte inserite nel periodo richiesto
            if (isDateIns)
            {
                sqlWhere += sqlAnd + "T_WK_XML_OFFERS.F_DATE between " + fromIns + " and " + toIns;
                sqlAnd = " and ";
            }

            //Estrae solo le offerte con la descrizione richiesta
            if (descr.Length > 0)
            {
                sqlWhere += sqlAnd + "T_WK_XML_OFFERS.F_DESCRIPTION like '%" + DbInterface.QueryString(descr.Trim()) + "%'";
                sqlAnd = " and ";
            }

            //Estrae solo le offerte con lo spessore richiesto
            if (spessore > 0)
            {
                sqlWhere += sqlAnd + "T_WK_CUSTOMER_ORDER_DETAILS.F_THICKNESS = " + DbInterface.QueryString(spessore.ToString());
                sqlAnd = " and ";
            }

            //Estrae solo le offerte col materiale richiesto
            if (materiale.Length > 0)
            {
                sqlWhere += sqlAnd + "T_AN_MATERIALS.F_DESCRIPTION like '%" + DbInterface.QueryString(materiale.Trim()) + "%'";
                sqlAnd = " and ";
            }

            //if (mUser != null && !mUser.gIsAdministrator)
            //    sqlWhere += sqlAnd + " T_WK_XML_OFFERS.F_ID_T_AN_USERS = " + mUser.gId.ToString();

            switch (orderKey)
            {
                case Offer.Keys.F_ID:
                    sqlOrderBy = " order by T_WK_XML_OFFERS.F_ID";
                    break;
                case Offer.Keys.F_CODE:
                    sqlOrderBy = " order by T_WK_XML_OFFERS.F_CODE";
                    break;
                case Offer.Keys.F_NOTE:
                    sqlOrderBy = " order by T_WK_XML_OFFERS.F_NOTE";
                    break;
                case Offer.Keys.F_DESCRIPTION:
                    sqlOrderBy = " order by T_WK_XML_OFFERS.F_DESCRIPTION";
                    break;
                case Offer.Keys.F_DATE:
                    sqlOrderBy = " order by T_WK_XML_OFFERS.F_DATE";
                    break;
            }

            return GetDataFromDb("select distinct T_WK_XML_OFFERS.F_ID, T_WK_XML_OFFERS.F_CODE, T_WK_XML_OFFERS.F_DATE, " +
                "T_WK_XML_OFFERS.F_DESCRIPTION, T_WK_XML_OFFERS.F_NOTE, " +
                "T_WK_XML_OFFERS.F_REVISION, T_CO_XML_OFFER_STATES.F_CODE AS F_STATE," +
                "T_WK_CUSTOMER_ORDERS.F_CODE AS F_ORDER_CODE, T_AN_CUSTOMERS.F_CODE AS F_CUSTOMER_CODE, " +
                "T_WK_CUSTOMER_ORDER_DETAILS.F_THICKNESS AS F_THICKNESS, " +
                "T_AN_MATERIALS.F_DESCRIPTION AS F_DESCRIPTION_MATERIALS " +
                "from T_WK_XML_OFFERS " +
                "join T_CO_XML_OFFER_STATES on T_CO_XML_OFFER_STATES.F_ID = T_WK_XML_OFFERS.F_ID_T_CO_XML_OFFER_STATES " +
                "inner join T_WK_CUSTOMER_ORDERS ON T_WK_XML_OFFERS.F_ID = T_WK_CUSTOMER_ORDERS.F_ID_T_WK_XML_OFFERS " +
                "inner join T_AN_CUSTOMERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_AN_CUSTOMERS = T_AN_CUSTOMERS.F_ID " +
                "inner join T_WK_CUSTOMER_PROJECTS ON T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = T_WK_CUSTOMER_ORDERS.F_ID " +
                "inner join T_WK_CUSTOMER_ORDER_DETAILS ON T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
                "inner join T_AN_MATERIALS ON T_AN_MATERIALS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_AN_MATERIALS " +
                sqlWhere + sqlOrderBy);

        }

		//**********************************************************************
		// GetDataFromOrders
		// Recupera, se presenti, i dati di cliente, spessore, materiale
		// Parametri:
		//			id	: id dell'offerta
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
        public bool GetDataFromOrders(int id, ref ArrayList custDesc, ref ArrayList mat, ref ArrayList spess)
        {

            string sqlWhere = "";
            string sqlAnd = " where ";

            sqlWhere += sqlAnd + "T_WK_XML_OFFERS.F_ID = " + DbInterface.QueryString(id.ToString()) + "";

            string strSQL = "select distinct T_WK_XML_OFFERS.F_ID, T_WK_XML_OFFERS.F_CODE, T_WK_XML_OFFERS.F_DATE, " +
                "T_WK_XML_OFFERS.F_DESCRIPTION, T_WK_XML_OFFERS.F_NOTE, " +
                "T_WK_XML_OFFERS.F_REVISION, T_CO_XML_OFFER_STATES.F_CODE AS F_STATE," +
                "T_WK_CUSTOMER_ORDERS.F_CODE AS F_ORDER_CODE, T_AN_CUSTOMERS.F_CODE AS F_CUSTOMER_CODE, " +
                "T_AN_CUSTOMERS.F_NAME AS F_CUSTOMER_NAME, " +
                "T_WK_CUSTOMER_ORDER_DETAILS.F_THICKNESS AS F_THICKNESS, " +
                "T_AN_MATERIALS.F_DESCRIPTION AS F_DESCRIPTION_MATERIALS " +
                "from T_WK_XML_OFFERS " +
                "join T_CO_XML_OFFER_STATES on T_CO_XML_OFFER_STATES.F_ID = T_WK_XML_OFFERS.F_ID_T_CO_XML_OFFER_STATES " +
                "inner join T_WK_CUSTOMER_ORDERS ON T_WK_XML_OFFERS.F_ID = T_WK_CUSTOMER_ORDERS.F_ID_T_WK_XML_OFFERS " +
                "inner join T_AN_CUSTOMERS ON T_WK_CUSTOMER_ORDERS.F_ID_T_AN_CUSTOMERS = T_AN_CUSTOMERS.F_ID " +
                "inner join T_WK_CUSTOMER_PROJECTS ON T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = T_WK_CUSTOMER_ORDERS.F_ID " +
                "inner join T_WK_CUSTOMER_ORDER_DETAILS ON T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
                "inner join T_AN_MATERIALS ON T_AN_MATERIALS.F_ID = T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_AN_MATERIALS " +
                sqlWhere;

            OleDbDataReader dr = null;

            Clear();

            try
            {
                if (!mDbInterface.Requery(strSQL, out dr))
                    return false;

                while (dr.Read())
                {
                    custDesc.Add(dr["F_CUSTOMER_NAME"].ToString());
                    mat.Add(dr["F_DESCRIPTION_MATERIALS"].ToString());
                    spess.Add(dr["F_THICKNESS"].ToString());

                }

                if (custDesc.Count == 0)
                {

                    custDesc.Add("");
                    mat.Add("");
                    spess.Add("");

                }

            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("OfferCollection: Error", ex);

                return false;
            }

            finally
            {
                mDbInterface.EndRequery(dr);
            }

            // Salva l'ultima query eseguita
            mSqlGetData = strSQL;
            return true;

        }


		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		//
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();
            ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
                    if (transaction) mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Offer off in mRecordset)
					{
						switch (off.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
                                //sqlInsert = "INSERT INTO T_WK_XML_OFFERS (F_CODE, F_NOTE, F_DESCRIPTION, F_REVISION, F_ID_T_CO_XML_OFFER_STATES) " +//, F_ID_T_AN_USERS)" + 
                                //    "(SELECT '" + DbInterface.QueryString(off.gCode) + "', '" + DbInterface.QueryString(off.gNote) +
                                //    "', '" + DbInterface.QueryString(off.gDescription) + "', " + off.gRevision.ToString()+ ", s.F_ID "+ //, " + mUser.gId + 
                                //    " FROM T_CO_XML_OFFER_STATES s " +
                                //    "WHERE s.F_CODE = '" + DbInterface.QueryString(off.gStateOff) + "');" + 
                                //    "SELECT SCOPE_IDENTITY()";
								sqlInsert = "INSERT INTO T_WK_XML_OFFERS (F_CODE, F_NOTE, F_DESCRIPTION, F_REVISION, F_ID_T_CO_XML_OFFER_STATES) " +//, F_ID_T_AN_USERS)" + 
									" VALUES ('" + DbInterface.QueryString(off.gCode) + "', '" + DbInterface.QueryString(off.gNote) +
                                    "', '" + DbInterface.QueryString(off.gDescription) + "', " + off.gRevision.ToString()+ ", 1 ); " + //, " + mUser.gId + 
									" SELECT SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								off.gId = id;
								
								if (!SetXmlParameterToDb(off, off.gXmlParameterLink))
									throw new ApplicationException("Error on saving Xml to Db");
								
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
                                sqlUpdate = "UPDATE T_WK_XML_OFFERS SET F_CODE = '" + DbInterface.QueryString(off.gCode) +
                                    "', F_NOTE = '" + DbInterface.QueryString(off.gNote) +
                                    "', F_DESCRIPTION = '" + DbInterface.QueryString(off.gDescription) +
                                    "', F_REVISION = " + off.gRevision.ToString() +
									", F_DATE = " + mDbInterface.OleDbToDate(off.gInsertDate);

								//Verifica se � stato modificato lo stato dell'offerta
								if (off.gStateOffChanged)
									sqlUpdate += ", F_ID_T_CO_XML_OFFER_STATES = "
										+ " (SELECT F_ID FROM T_CO_XML_OFFER_STATES" 
										+ " WHERE F_CODE = '" + DbInterface.QueryString(off.gStateOff) + "')";

							
								sqlUpdate += " WHERE F_ID = " + off.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;

								if(!SetXmlParameterToDb(off, off.gXmlParameterLink))
									throw new ApplicationException("Error on saving Xml to Db");

								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Offer off in mDeleted)
					{
						switch (off.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
                                //parameters.Clear();

                                //par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
                                //par.Direction = System.Data.ParameterDirection.ReturnValue;
                                //parameters.Add(par);

                                //par = new OleDbParameter("@offer_id", OleDbType.Integer);
                                //par.Value = off.gId;
                                //parameters.Add(par);

                                //// Lancia la stored procedure
                                //if (!mDbInterface.Execute("sp_tecno_delete_offers", ref parameters))
                                //    throw new ApplicationException("UpdateDataToDb sp_tecno_delete_offers Execute error.");
                                
                                //sqlDelete = "UPDATE T_WK_CUSTOMER_ORDER_DETAILS " +
                                //    "SET F_ID_T_AN_ARTICLES = null " +
                                //    "WHERE F_ID IN " +
                                //    "(SELECT p.F_ID " +
                                //    "FROM T_WK_CUSTOMER_PROJECTS p, T_WK_CUSTOMER_ORDERS o, T_WK_XML_OFFERS x " +
                                //    "WHERE p.F_ID_T_WK_CUSTOMER_ORDERS = o.F_ID AND " +
                                //    "o.F_ID_T_WK_XML_OFFERS = x.F_ID AND x.F_ID = " + off.gId.ToString() + "); " +
                                //    "DELETE FROM T_AN_ARTICLES " +
                                //    "WHERE F_ID_T_AN_STOCK_HUS = null AND " +
                                //    "F_ID IN (SELECT d.F_ID_T_AN_ARTICLES " +
                                //    "FROM T_WK_CUSTOMER_ORDER_DETAILS d, T_WK_CUSTOMER_PROJECTS p, " +
                                //    "T_WK_CUSTOMER_ORDERS o, T_WK_XML_OFFERS x " +
                                //    "WHERE d.F_ID_T_WK_CUSTOMER_PROJECTS = p.F_ID AND " +
                                //    "p.F_ID_T_WK_CUSTOMER_ORDERS = o.F_ID AND " +
                                //    "o.F_ID_T_WK_XML_OFFERS = x.F_ID AND x.F_ID = " + off.gId.ToString() + "); " +
                                //    "DELETE FROM T_WK_CUSTOMER_ORDER_DETAILS " +
                                //    "WHERE F_ID_T_WK_CUSTOMER_PROJECTS IN " + 
                                //    "(SELECT p.F_ID " +
                                //    "FROM T_WK_CUSTOMER_PROJECTS p, T_WK_CUSTOMER_ORDERS o, T_WK_XML_OFFERS x " +
                                //    "WHERE p.F_ID_T_WK_CUSTOMER_ORDERS = o.F_ID AND " +
                                //    "o.F_ID_T_WK_XML_OFFERS = x.F_ID AND x.F_ID = " + off.gId.ToString() + "); " +
                                //    "DELETE FROM T_WK_CUSTOMER_PROJECTS " +
                                //    "WHERE F_ID_T_WK_CUSTOMER_ORDERS IN " + 
                                //    "(SELECT o.F_ID " +
                                //    "FROM T_WK_CUSTOMER_ORDERS o, T_WK_XML_OFFERS x " +
                                //    "WHERE o.F_ID_T_WK_XML_OFFERS = x.F_ID AND x.F_ID = " + off.gId.ToString() + "); " +
                                //    "DELETE FROM T_WK_CUSTOMER_ORDERS " +
                                //    "WHERE F_ID_T_WK_XML_OFFERS = " + off.gId.ToString() + "; " +
                                //    "DELETE FROM T_WK_XML_OFFERS " +
                                //    "WHERE F_ID = " + off.gId.ToString() + ";";

                                sqlDelete = "DELETE FROM T_WK_XML_OFFERS " +
                                            "WHERE F_ID = " + off.gId.ToString() + ";";

                                if (!mDbInterface.Execute(sqlDelete))
                                    return false;
								break;
						}
					}

					// Termina la transazione con successo
                    if (transaction) mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

					//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("OfferCollection: Deadlock Error", ex);

					// Annulla la transazione
                    if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("OfferCollection: Exceed Deadlock retrying", ex);
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("OfferCollection: Error", ex);

					// Annulla la transazione
                    if (transaction) mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}

			}

			return false;
		}

        //**********************************************************************
        // RefreshData
        // Ricarica i dati con l'ultima query eseguita
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool RefreshData()
        {
            return GetDataFromDb(mSqlGetData);
        }

		//**********************************************************************
		// GetXmlParameterFromDb
		// Legge il file parametri xml dal database
		// Parametri:
		//			off			: offerta di cui leggere l'XML
		//			fileName	: nome del file su cui memorizzare l'XML
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetXmlParameterFromDb(Offer off, string fileName)
		{
			try
			{
				// Recupera l'XML dal db
				if (!mDbInterface.GetBlobField("T_WK_XML_OFFERS", "F_XML", " F_ID = " + off.gId.ToString(), fileName))
					return false;
			}
				
				// Eccezione
			catch (Exception ex)
			{
				TraceLog.WriteLine("OfferCollection.GetXmlParameterFromDb: Error", ex);
				return false;
			}

			off.iSetXmlParameterLink(fileName);
			return true;
		}
		
		//**********************************************************************
		// SetXmlParameterToDb
		// Scrive il file parametri xml nel database
		// Parametri:
		//			off			: articolo di cui scrivere l'XML
		//			fileName	: nome del file da cui prelevare l'XML
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool SetXmlParameterToDb(Offer off, string fileName)
		{
			bool transaction = !mDbInterface.TransactionActive();

			try
			{
				if(transaction)	mDbInterface.BeginTransaction();

				// Scrive l'XML su db
				if (!mDbInterface.WriteBlobField("T_WK_XML_OFFERS", "F_XML", " F_ID = " + off.gId.ToString(), fileName))
					throw (new ApplicationException("error writing xml"));

				if(transaction)	mDbInterface.EndTransaction(true);

				off.iSetXmlParameterLink(fileName);
				return true;
			}
				
				// Eccezione
			catch (Exception ex)
			{
				if(transaction)	mDbInterface.EndTransaction(false);
				TraceLog.WriteLine("OfferCollection.SetXmlParameterToDb: Error", ex);
				return false;
			}
		}

		public override void SortByField(string key)
		{
		}

		public bool IsCorrectUser(Offer off, out string user)
		{
		    user = string.Empty;
			return true;

		}

		#endregion

		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Offer off;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
                    //off = new Offer((int)dr["F_ID"], dr["F_CODE"].ToString(), (DateTime) dr["F_DATE"], 
                    //    dr["F_STATE"].ToString(), dr["F_NOTE"].ToString(), dr["F_DESCRIPTION"].ToString(), 
                    //    (int)dr["F_REVISION"]);
					off = new Offer((int)dr["F_ID"], dr["F_CODE"].ToString(), (DateTime) dr["F_DATE"], 
						"", dr["F_NOTE"].ToString(), dr["F_DESCRIPTION"].ToString(), 
                        (int)dr["F_REVISION"]);
					
					AddObject(off);
					off.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("OfferCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
			
			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}
}
