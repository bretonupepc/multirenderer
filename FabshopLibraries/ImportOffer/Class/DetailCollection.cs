using System;
using System.Collections;
using System.Data.OleDb;
using System.Globalization;
using System.Threading;
using DbAccess;
using TraceLoggers;

namespace ImportOffer.Class
{
	public class DetailCollection: DbCollection
	{
		#region Constructor
		public DetailCollection(DbInterface db): base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Detail obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);
			
			obj = (Detail) dbObj;
			return ret;
		}

        /// <summary>
        /// Restituisce l'oggetto contenuto nella collection interessata uguale a quello passato (appartenente ad un'altra collection)
        /// </summary>
        /// <param name="det">dettaglio da ricercare</param>
        /// <returns></returns>
        public Detail GetSameObject(Detail det)
        {
            Detail retDet;
            int cursor = mCursor;

            MoveFirst();
            while (!IsEOF())
            {
                retDet = (Detail)mRecordset[mCursor];
                if (retDet.gCode == det.gCode)
                {
                    mCursor = cursor;
                    return retDet;
                }
                MoveNext();
            }

            mCursor = cursor;
            return null;
        }

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Detail obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Detail) mRecordset[i];
				return true;
			}
			
			//oggetto non trovato
			obj = null;
			return false;
		}
		
		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Detail obj)
		{
			return (base.AddObject((Detail) obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
            string[] codes = { code };
            return GetDataFromDb(Detail.Keys.F_NONE, codes, null, null, null, 0d, null, null);
		}

        //**********************************************************************
        // GetDataFromDbFromArtCode
        // Legge dal database l'oggetto con riferimento all'articolo interessato
        // Parametri:
        //			articleCode	: codice dell'articolo
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool GetDataFromDbFromArtCode(string articleCode)
        {
            return GetDataFromDb(Detail.Keys.F_NONE, null, null, null, null, 0d, articleCode, null);
        }

        public bool GetDataFromDbFromOrderCode(string orderCode)
        {
            return GetDataFromDb(Detail.Keys.F_NONE, null, null, null, null, 0d, null, orderCode);
        }

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
            return GetDataFromDb(Detail.Keys.F_NONE, null, null, null, null, 0d, null, null);
		}

        /// <summary>
        /// Legge i dati dal database da una lista di codici
        /// </summary>
        /// <param name="codes">lista dei codici dei dettagli interessati</param>
        /// <returns></returns>
        public bool GetDataFromDb(string[] codes)
        {
            return GetDataFromDb(Detail.Keys.F_NONE, codes, null, null, null, 0d, null, null);
        }

		/// <summary>
		/// Legge i dati dal database da una lista di codici
		/// </summary>
		/// <param name="codes">lista dei codici dei dettagli interessati</param>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <returns></returns>
		public bool GetDataFromDb(Detail.Keys orderKey, string[] codes)
		{
			return GetDataFromDb(orderKey, codes, null, null, null, 0d, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Detail.Keys orderKey)
		{
            return GetDataFromDb(orderKey, null, null, null, null, 0d, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderCode	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Detail.Keys orderKey, string projectCode)
		{
            return GetDataFromDb(orderKey, null, projectCode, null, null, 0d, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		//			state		: estrae solo gli elementi dello stato specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Detail.Keys orderKey, string[] codes, string customerProjectCode, string materialCode, string state,
            double thickness, string articleCode, string orderCode)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE ";


			//Estrae solo il progetto del codice richiesto
			if (codes != null)
			{
                sqlWhere += sqlAnd + "d.F_CODE in ('" + codes[0] + "'";
                for (int i = 1; i < codes.Length; i++)
                    sqlWhere += ", '" + codes[i] + "'";

                sqlWhere += ")";
                sqlAnd = " AND ";
			}

			//Estrae solo il pezzo del progetto cliente richiesto
			if (customerProjectCode != null)
			{
				sqlWhere += sqlAnd + "p.F_CODE = '" + DbInterface.QueryString(customerProjectCode) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo i pezzi del materiale richiesto
			if (materialCode != null)
			{
				sqlWhere += sqlAnd + "m.F_CODE = '" + DbInterface.QueryString(materialCode) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo i pezzi con lo stato richiesto
			if (state != null)
			{
				sqlWhere += sqlAnd + "s.F_CODE= '" + DbInterface.QueryString(state) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo i pezzi dello spessore richiesto
			if (thickness > 0)
			{
				sqlWhere += sqlAnd + "d.F_THICKNESS = " + thickness.ToString();
				sqlAnd = " AND ";
			}

            //Estrae solo i pezzi riferiti all'articolo richiesto
            if (articleCode != null)
            {
                sqlWhere += sqlAnd + "a.F_CODE= '" + DbInterface.QueryString(articleCode) + "'";
                sqlAnd = " AND ";
            }

            //Estrae solo i pezzi riferiti all'ordine richiesto
            if (orderCode != null)
            {
                sqlWhere += sqlAnd + "o.F_CODE= '" + DbInterface.QueryString(orderCode) + "'";
                sqlAnd = " AND ";
            }

			switch (orderKey)
			{
				case Detail.Keys.F_ID:
                    sqlOrderBy = " order by d.F_ID";
					break;
				case Detail.Keys.F_CODE:
                    sqlOrderBy = " order by d.F_CODE";
					break;
				case Detail.Keys.F_CUSTOMER_PROJECT_CODE:
                    sqlOrderBy = " order by p.F_CODE";
					break;
				case Detail.Keys.F_MATERIAL_CODE:
                    sqlOrderBy = " order by m.F_CODE";
					break;
				case Detail.Keys.F_ARTICLE_CODE:
                    sqlOrderBy = " order by a.F_CODE";
					break;
				case Detail.Keys.F_STATE:
                    sqlOrderBy = " order by s.F_CODE";
					break;
				case Detail.Keys.F_THICKNESS:
                    sqlOrderBy = " order by d.F_THICKNESS";
					break;
				case Detail.Keys.F_CODE_ORIGINAL_SHAPE:
                    sqlOrderBy = " order by d.F_CODE_ORIGINAL_SHAPE";
					break;
                case Detail.Keys.F_DESCRIPTION:
                    sqlOrderBy = " order by d.F_DESCRIPTION";
                    break;
				
			}
			return GetDataFromDb("select d.F_ID, d.F_CODE, d.F_THICKNESS, d.F_CODE_ORIGINAL_SHAPE, d.F_DESCRIPTION, d.F_REVISION, d.F_FINAL_THICKNESS, d.F_LENGTH, d.F_WIDTH, " +
								"p.F_CODE as F_CUSTOMER_PROJECT_CODE, s.F_CODE as F_STATE, m.F_CODE as F_MATERIAL_CODE, a.F_CODE as F_ARTICLE_CODE " +
				"from T_WK_CUSTOMER_ORDER_DETAILS d " +
				"join T_WK_CUSTOMER_PROJECTS p on p.F_ID = d.F_ID_T_WK_CUSTOMER_PROJECTS " +
				"join T_WK_CUSTOMER_ORDERS o on o.F_ID = p.F_ID_T_WK_CUSTOMER_ORDERS " +
				"join T_CO_CUSTOMER_ORDER_DETAIL_STATES s on s.F_ID = d.F_ID_T_CO_CUSTOMER_ORDER_DETAIL_STATES " +
				"left outer join T_AN_ARTICLES a on a.F_ID = d.F_ID_T_AN_ARTICLES " +
				"join T_AN_MATERIALS m on m.F_ID = d.F_ID_T_AN_MATERIALS " +
				sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// 
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();


			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if(transaction)	mDbInterface.BeginTransaction();

					//Scorre tutti i materiali
					foreach (Detail det in mRecordset)
					{
						switch (det.gState)
						{
								//Inserimento
							case DbObject.ObjectStates.Inserted:
                                sqlInsert = "INSERT INTO T_WK_CUSTOMER_ORDER_DETAILS (F_THICKNESS, F_CODE_ORIGINAL_SHAPE, F_DESCRIPTION, F_REVISION, " + 
                                    " F_FINAL_THICKNESS, F_LENGTH, F_WIDTH, F_ID_T_WK_CUSTOMER_PROJECTS, F_ID_T_AN_MATERIALS, " + 
                                    (det.gArticleCode != null && det.gArticleCode.Length > 0 ? " F_ID_T_AN_ARTICLES," : "") + " F_ID_T_CO_CUSTOMER_ORDER_DETAIL_STATES) " +
									"(SELECT " + det.gThickness.ToString(CultureInfo.InvariantCulture) + ", '" + DbInterface.QueryString(det.gCodeOriginalShape) + "', '" + DbInterface.QueryString(det.gDescription) + "', " +
									det.gRevision.ToString() + ", " + det.gFinalThickness.ToString(CultureInfo.InvariantCulture) + ", " + det.gLength.ToString(CultureInfo.InvariantCulture) + ", " + det.gWidth.ToString(CultureInfo.InvariantCulture) + ",p.F_ID, m.F_ID, " + 
                                    (det.gArticleCode != null && det.gArticleCode.Length > 0 ? " a.F_ID," : "") + "s.F_ID " +
                                    "FROM T_WK_CUSTOMER_PROJECTS p, T_AN_MATERIALS m, " + (det.gArticleCode != null && det.gArticleCode.Length > 0 ? "T_AN_ARTICLES a, " : "") + " T_CO_CUSTOMER_ORDER_DETAIL_STATES s " +
                                    "WHERE p.F_CODE = '" + DbInterface.QueryString(det.gCustomerProjectCode) + "' AND m.F_CODE = '" + DbInterface.QueryString(det.gMaterialCode) +
                                    "' " + (det.gArticleCode != null && det.gArticleCode.Length > 0 ? "AND a.F_CODE = '" + DbInterface.QueryString(det.gArticleCode) + "' " : "") +
                                    " AND s.F_CODE = '" + DbInterface.QueryString(det.gStateDet) + "');SELECT SCOPE_IDENTITY()";

								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								det.gId = id;

                                if (det.gShapeInfo != null && det.gShapeInfo != "" && !SetXmlParameterToDb(det, det.gShapeInfo))
									throw new ApplicationException("Error on saving Xml to Db");
								
								break;
					
								//Aggiornamento
							case DbObject.ObjectStates.Updated:
                                sqlUpdate = "UPDATE T_WK_CUSTOMER_ORDER_DETAILS SET " +
                                    "F_CODE = '" + DbInterface.QueryString(det.gCode) +
									"', F_THICKNESS = " + det.gThickness.ToString(CultureInfo.InvariantCulture) +
                                    ", F_CODE_ORIGINAL_SHAPE = '" + DbInterface.QueryString(det.gCodeOriginalShape) +
                                    "', F_DESCRIPTION = '" + DbInterface.QueryString(det.gDescription) +
									"', F_REVISION = " + det.gRevision.ToString(CultureInfo.InvariantCulture) +
									", F_FINAL_THICKNESS = " + det.gFinalThickness.ToString(CultureInfo.InvariantCulture) +
									", F_LENGTH = " + det.gLength.ToString(CultureInfo.InvariantCulture) +
									", F_WIDTH = " + det.gWidth.ToString(CultureInfo.InvariantCulture);

								//Verifica se � stato modificato il progetto per il pezzo
								if (det.gCustomerProjectCodeChanged)
									sqlUpdate += ", F_ID_T_WK_CUSTOMER_PROJECTS = "
										+ " (SELECT F_ID FROM T_WK_CUSTOMER_PROJECTS" 
										+ " WHERE F_CODE = '" + DbInterface.QueryString(det.gCustomerProjectCode) + "')";

								//Verifica se � stato modificato il materiale del pezzo
								if (det.gMaterialCodeChanged)
									sqlUpdate += ", F_ID_T_AN_MATERIALS = "
										+ " (SELECT F_ID FROM T_AN_MATERIALS" 
										+ " WHERE F_CODE = '" + DbInterface.QueryString(det.gMaterialCode) + "')";

								//Verifica se � stato modificato l'articolo associato al pezzo
								if (det.gArticleCodeChanged)
									sqlUpdate += ", F_ID_T_AN_ARTICLES = "
										+ " (SELECT F_ID FROM T_AN_ARTICLES" 
										+ " WHERE F_CODE = '" + DbInterface.QueryString(det.gArticleCode) + "')";

								//Verifica se � stato modificato lo stato del progetto
								if (det.gStateDetChanged)
                                    sqlUpdate += ", F_ID_T_CO_CUSTOMER_ORDER_DETAIL_STATES = "
										+ " (SELECT F_ID FROM T_CO_CUSTOMER_ORDER_DETAIL_STATES" 
										+ " WHERE F_CODE = '" + DbInterface.QueryString(det.gStateDet) + "')";
							
								sqlUpdate += " WHERE F_ID = " + det.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;

                                if (det.gShapeInfo != null && det.gShapeInfo != "" && !SetXmlParameterToDb(det, det.gShapeInfo))
									throw new ApplicationException("Error on saving Xml to Db");

								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Detail det in mDeleted)
					{
						switch (det.gState)
						{
								//Cancellazione
							case DbObject.ObjectStates.Deleted:
                                sqlDelete = "DELETE FROM T_WK_CUSTOMER_ORDER_DETAILS " +
                                            "WHERE F_ID = " + det.gId.ToString() + "; ";
                                            //"DELETE FROM T_AN_ARTICLES " +
                                            //"WHERE F_ID_T_AN_STOCK_HUS is null AND " + 
                                            //"F_CODE = '" +DbInterface.QueryString(det.gArticleCode) + "';";

								if (!mDbInterface.Execute(sqlDelete))
									return false;
								break;
						}
					}

					// Termina la transazione con successo
					if(transaction)	mDbInterface.EndTransaction(true);

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

					//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("DetailCollection: Deadlock Error", ex);

					// Annulla la transazione
					if(transaction)	mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("DetailCollection: Exceed Deadlock retrying", ex);
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("DetailCollection: Error", ex);

					// Annulla la transazione
					if(transaction)	mDbInterface.EndTransaction(false);
					
					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

		//**********************************************************************
		// GetXmlParameterFromDb
		// Legge il file parametri xml dal database
		// Parametri:
		//			det			: dettaglio di cui leggere l'XML
		//			fileName	: nome del file su cui memorizzare l'XML
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetXmlParameterFromDb(Detail det, string fileName)
		{
			try
			{
				// Recupera l'XML dal db
				if (!mDbInterface.GetBlobField("T_WK_CUSTOMER_ORDER_DETAILS", "F_SHAPE_INFO", " F_ID = " + det.gId.ToString(), fileName))
					return false;
			}
				
				// Eccezione
			catch (Exception ex)
			{
				TraceLog.WriteLine("DetailCollection.GetXmlParameterFromDb: Error", ex);
				return false;
			}

			det.iShapeInfo(fileName);
			return true;
		}
		
		//**********************************************************************
		// SetXmlParameterToDb
		// Scrive il file parametri xml nel database
		// Parametri:
        //			det			: dettaglio di cui scrivere l'XML
		//			fileName	: nome del file da cui prelevare l'XML
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool SetXmlParameterToDb(Detail det, string fileName)
		{
			bool transaction = !mDbInterface.TransactionActive();

			try
			{
				if(transaction)	mDbInterface.BeginTransaction();

				// Scrive l'XML su db
				if (!mDbInterface.WriteBlobField("T_WK_CUSTOMER_ORDER_DETAILS", "F_SHAPE_INFO", " F_ID = " + det.gId.ToString(), fileName))
					throw (new ApplicationException("error writing xml"));

				if(transaction)	mDbInterface.EndTransaction(true);

				det.iShapeInfo(fileName);
				return true;
			}
				
				// Eccezione
			catch (Exception ex)
			{
				if(transaction)	mDbInterface.EndTransaction(false);
				TraceLog.WriteLine("DetailCollection.SetXmlParameterToDb: Error", ex);
				return false;
			}
		}

		public override void SortByField(string key)
		{
		}

        //**********************************************************************
        // RefreshData
        // Ricarica i dati con l'ultima query eseguita
        // Ritorna:
        //			true	lettura eseguita
        //			false	lettura non eseguita
        //**********************************************************************
        public bool RefreshData()
        {
            return GetDataFromDb(mSqlGetData);
        }

        /// <summary>
        /// Restituisce tutti i dettagli contenuti nell'articolo interessato
        /// </summary>
        /// <param name="articleCode">codice articolo da ricercare</param>
        /// <param name="details">array da caricare</param>
        /// <returns></returns>
        public bool GetArticleDetails(string articleCode, out string[] details)
        {
            OleDbDataReader dr = null;
            string sqlQuery = "";
            details = new string[0];
            int i = 0;

            try
            {
                // Cerca tutti gli ordini contenuti in questo articolo (lastra o recupero)
                sqlQuery = "select distinct d.F_CODE as F_DETAIL_CODE " +
                            "from T_AN_ARTICLES a " +
                            "inner join T_CO_ARTICLE_TYPES aty on a.F_ID_T_CO_ARTICLE_TYPES = aty.F_ID " +
                            "inner join T_WK_USED_OBJECTS u on a.F_ID = u.F_ID_T_AN_ARTICLES " +
                            "inner join T_WK_WORK_PHASES_H w on u.F_ID = w.F_ID_T_WK_USED_OBJECTS " +
                            "inner join T_WK_CUSTOMER_ORDER_DETAILS d on w.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = d.F_ID " +
                            "where a.F_CODE = '" + articleCode + "' and aty.F_CODE in ('TYPART0001','TYPART0002') " +
                            "order by d.F_CODE";

                if (!mDbInterface.Requery(sqlQuery, out dr))
                {
                    details = null;
                    return false;
                }

                while (dr.Read())
                {
                    string[] sTmp = new string[details.Length + 1];
                    details.CopyTo(sTmp, 0);
                    details = sTmp;
                    details[i] = dr["F_DETAIL_CODE"].ToString().Trim();
                    i++;
                }

                if (details.Length == 0)
                    details = null;

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DetailCollection.GetArticleDetails Error: ", ex);
                details = null;
                return false;
            }
            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

        /// <summary>
        /// Restituisce l'articolo (lastra o recupero) in cui � contenuto il dettaglio interessato
        /// </summary>
        /// <param name="detailCode">dettaglio interessato</param>
        /// <returns></returns>
        public string GetSourceArticleFromDetail(string detailCode)
        {
            OleDbDataReader dr = null;
            string article = null;
            string sqlQuery = "";

            try
            {
                // Cerca l'articolo (lastra o recupero) che contiene il dettaglio
                sqlQuery = "select a.F_CODE as F_ARTICLE_CODE " +
                            "from T_WK_CUSTOMER_ORDER_DETAILS d " +
                            "inner join T_WK_WORK_PHASES_H w on w.F_ID_T_WK_CUSTOMER_ORDER_DETAILS = d.F_ID " +
                            "inner join T_WK_USED_OBJECTS u on u.F_ID = w.F_ID_T_WK_USED_OBJECTS " +
                            "inner join T_AN_ARTICLES a on u.F_ID_T_AN_ARTICLES = a.F_ID " +
                            "inner join T_CO_ARTICLE_TYPES aty on a.F_ID_T_CO_ARTICLE_TYPES = aty.F_ID " +
                            "where d.F_CODE = '" + detailCode + "' and aty.F_CODE in ('TYPART0001','TYPART0002')";

                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return null;

                while (dr.Read())
                {
                    article = dr["F_ARTICLE_CODE"].ToString().Trim();
                }

                return article;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DetailCollection.GetSourceArticleFromDetail Error: ", ex);
                return null;
            }
            finally
            {
                mDbInterface.EndRequery(dr);
            }
        }

        /// <summary>
        /// Imposta un pezzo scartato
        /// </summary>
        /// <param name="det">dettaglio scartato</param>
        /// <returns></returns>
        public bool DiscardDetail(Detail det)
        {
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();
            OleDbParameter par;
            ArrayList parameters = new ArrayList();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre una transazione
                    if (transaction) mDbInterface.BeginTransaction();

                    par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
                    par.Direction = System.Data.ParameterDirection.ReturnValue;
                    parameters.Add(par);

                    par = new OleDbParameter("@sRequest", OleDbType.VarChar, 18);
                    par.Value = "DISCARD";
                    parameters.Add(par);

                    par = new OleDbParameter("@sArticle", OleDbType.VarChar, 18);
                    par.Value = det.gArticleCode;
                    parameters.Add(par);

                    par = new OleDbParameter("@iMode", OleDbType.Integer);
                    par.Value = 1;
                    parameters.Add(par);

                    // Lancia la stored procedure
                    if (!mDbInterface.Execute("sp_pro_discard_piece", ref parameters))
                        throw new ApplicationException("DetailCollection.DiscardDetail sp_pro_discard_piece Execute error.");

                    par = (OleDbParameter)parameters[0];
                    if (((int)par.Value) != 0)
                        throw new ApplicationException("DetailCollection.DiscardDetail sp_pro_discard_piece Execute error: " + ((int)par.Value));

                    // Chiude la transazione
                    if (transaction)
                        mDbInterface.EndTransaction(true);

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("DetailCollection.DiscardDetail Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("DetailCollection.DiscardDetail Error.", ex);

                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Imposta un pezzo come rotto o distrutto
        /// </summary>
        /// <param name="det">dettaglio distrutto</param>
        /// <returns></returns>
        public bool DestroyDetail(Detail det)
        {
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();
            OleDbParameter par;
            ArrayList parameters = new ArrayList();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre una transazione
                    if (transaction) mDbInterface.BeginTransaction();

                    par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
                    par.Direction = System.Data.ParameterDirection.ReturnValue;
                    parameters.Add(par);

                    par = new OleDbParameter("@sRequest", OleDbType.VarChar, 18);
                    par.Value = "DESTROY";
                    parameters.Add(par);

                    par = new OleDbParameter("@sArticle", OleDbType.VarChar, 18);
                    par.Value = det.gArticleCode;
                    parameters.Add(par);

                    par = new OleDbParameter("@iMode", OleDbType.Integer);
                    par.Value = 1;
                    parameters.Add(par);

                    // Lancia la stored procedure
                    if (!mDbInterface.Execute("sp_pro_discard_piece", ref parameters))
                        throw new ApplicationException("DetailCollection.DestroyDetail sp_pro_discard_piece Execute error.");

                    par = (OleDbParameter)parameters[0];
                    if (((int)par.Value) != 0)
                        throw new ApplicationException("DetailCollection.DestroyDetail sp_pro_discard_piece Execute error: " + ((int)par.Value));

                    // Chiude la transazione
                    if (transaction)
                        mDbInterface.EndTransaction(true);

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("DetailCollection.DestroyDetail Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("DetailCollection.DestroyDetail Error.", ex);

                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Crea tutti gli articoli associati ai dettagli attualmente nella collection
        /// </summary>
        /// <returns></returns>
        public bool CreateArticles()
        {
            try
            {
                foreach (Detail det in mRecordset)
                {
                    if (det.gArticleCode == null || det.gArticleCode.Length == 0 )
                        CreateArticlePief(det);
                }

                RefreshData();

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DetailCollection.CreateArticles ", ex);
                return false;
            }
        }

        /// <summary>
        /// Verifica lo stato in cui si trova il pezzo e ne imposta il valore
        /// </summary>
        /// <param name="det">Dettaglio da analizzare</param>
        /// <returns></returns>
        public bool SetImportState(ref Detail det)
        {
            // Verifica se il pezzo � completato
            if (det.gStateDet.Trim() == Detail.State.SHAPE_COMPLETED.ToString())
            {
                det.gImportState = Detail.ImportState.COMPLETED;
                return true;
            }
            // Verifica se il pezzo � in lavoro
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_IN_PROGRESS.ToString())
            {
                det.gImportState = Detail.ImportState.IN_PROGRESS;
                return true;
            }
            // Verifica se il pezzo � in lista di estrazione processato
            else if (IsUseList(det, true))
            {
                det.gImportState = Detail.ImportState.USE_LIST_PROCESSED;
                return true;
            }
            // Verifica se il pezzo � in lista di estrazione negli stati in programmazione o confermata
            else if (IsUseList(det, false))
            {
                det.gImportState = Detail.ImportState.USE_LIST;
                return true;
            }
            // Verifica se l'ordine contenente il pezzo � stato ottimizzato
            else if (IsOptimized(det))
            {
                det.gImportState = Detail.ImportState.OPTIMIZED;
                return true;
            }
            // Verifica se l'ordine contente il pezzo si trova in ottimizzazione
            else if (IsOptimizing(det) && det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
            {
                det.gImportState = Detail.ImportState.OPTIMIZING;
                return true;
            }
            // Verifica se il pezzo � stato analizzato dal tecno master
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_ANALIZED.ToString())
            {
                det.gImportState = Detail.ImportState.ANALIZED;
                return true;
 
            }
            else if (det.gStateDet.Trim() == Detail.State.SHAPE_LOADED.ToString() ||
                det.gStateDet.Trim() == Detail.State.SHAPE_RELOADED.ToString())
            {
                det.gImportState = Detail.ImportState.LOADED;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Ordina la collection per lo stato in cui si trova partendo dallo stato meno avanzato
        /// </summary>
        public void SortByImportState()
        {
            Detail tmp;

            // Bubble Sort
            for (int i = 0; i < mRecordset.Count - 1; i++)
            {
                for (int j = i + 1; j < mRecordset.Count; j++)
                {
                    if ((int)((Detail)mRecordset[i]).gImportState >= (int)((Detail)mRecordset[i]).gImportState)
                    {
                        tmp = (Detail)mRecordset[i];
                        mRecordset[i] = mRecordset[j];
                        mRecordset[j] = tmp;
                    }
                }
            }
        }

		#endregion

		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;

			Detail det;
			
			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					det = new Detail((int)dr["F_ID"], dr["F_CODE"].ToString(), dr["F_CUSTOMER_PROJECT_CODE"].ToString(),dr["F_MATERIAL_CODE"].ToString(),
						dr["F_ARTICLE_CODE"].ToString(), dr["F_STATE"].ToString(), (double) dr["F_THICKNESS"], dr["F_CODE_ORIGINAL_SHAPE"].ToString(),
                        dr["F_DESCRIPTION"].ToString(), (int)dr["F_REVISION"], (double)dr["F_FINAL_THICKNESS"], (double)dr["F_LENGTH"], (double)dr["F_WIDTH"]);

					AddObject(det); 
					det.gState = DbObject.ObjectStates.Unchanged;
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("DetailCollection: Error", ex);

				return false;
			}
			
			finally
			{
				mDbInterface.EndRequery(dr);
			}
            
            // Salva l'ultima query eseguita
            mSqlGetData = sqlQuery;
			return true;
		}

        /// <summary>
        /// Verifica se il dettaglio � stato analizzato dal Tecno Master
        /// </summary>
        /// <param name="detCode">codice del dettaglio</param>
        /// <returns></returns>
        private bool IsTecnoAnalyzed(string detCode)
        {
            OleDbDataReader drTecno = null;

            try
            {
                string sqlQuery = "select wp.* from T_WK_WORK_PHASES_H wp where F_ID_T_WK_CUSTOMER_ORDER_DETAILS = " +
                                  "(select F_ID from T_WK_CUSTOMER_ORDER_DETAILS where F_CODE = '" + detCode + "')";

                if (!mDbInterface.Requery(sqlQuery, out drTecno))
                    return false;

                return drTecno.HasRows;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DetailCollection.IsTecnoAnalyzed. Error:", ex);

                return false;
            }
            finally
            {
                mDbInterface.EndRequery(drTecno);
            }
        }

        /// <summary>
        /// Crea l'articolo da associare al dettaglio
        /// </summary>
        /// <param name="det"></param>
        /// <returns></returns>
        private bool CreateArticlePief(Detail det)
        {
            int retry = 0;
            bool transaction = !mDbInterface.TransactionActive();
            OleDbParameter par;
            ArrayList parameters = new ArrayList();

            //Riprova pi� volte
            while (retry < DbInterface.DEADLOCK_RETRY)
            {
                try
                {
                    // Apre una transazione
                    if (transaction)    
                        mDbInterface.BeginTransaction();

                    par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
                    par.Direction = System.Data.ParameterDirection.ReturnValue;
                    parameters.Add(par);

                    par = new OleDbParameter("@id_det", OleDbType.VarChar, 18);
                    par.Value = det.gId;
                    parameters.Add(par);

                    // Lancia la stored procedure
                    if (!mDbInterface.Execute("sp_pro_create_article_pief", ref parameters))
                        throw new ApplicationException("DetailCollection.CreateArticlePief sp_pro_create_article_pief Execute error.");

                    par = (OleDbParameter)parameters[0];
                    if (((int)par.Value) != 0)
                        throw new ApplicationException("DetailCollection.CreateArticlePief sp_pro_create_article_pief Execute error: " + ((int)par.Value));

                    // Chiude la transazione
                    if (transaction)
                        mDbInterface.EndTransaction(true);

                    return true;
                }

                    // Errore di deadlock
                catch (DeadLockException ex)
                {
                    TraceLog.WriteLine("DetailCollection.CreateArticlePief Error.", ex);

                    //Se deadlock e non c'� una transazione attiva, riprova pi� volte
                    if (transaction && retry < DbInterface.DEADLOCK_RETRY)
                    {
                        if (transaction) mDbInterface.EndTransaction(false);
                        Thread.Sleep(DbInterface.DEADLOCK_WAIT);
                        retry++;
                    }

                        //altrimenti risolleva l'eccezione di deadlock
                    else
                        throw ex;
                }

                catch (Exception ex)
                {
                    if (transaction) mDbInterface.EndTransaction(false);
                    TraceLog.WriteLine("DetailCollection.CreateArticlePief Error.", ex);

                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Indica se il dettaglio si trova in lista di estrazione
        /// </summary>
        /// <param name="det">dettaglio da verificare</param>
        /// <param name="state">stato da ricercare. Se true si ricercher� tra gli stati "processato" altrimenti 
        /// tra gli stati "In Programmazione" e "Confermata"</param>
        /// <returns></returns>
        private bool IsUseList(Detail det, bool state)
        {
            OleDbDataReader dr = null;
            string sqlQuery;
            string sourceArticle;

            try
            {
                // Verifica che il dettaglio sia associato a una lastra
                sourceArticle = GetSourceArticleFromDetail(det.gCode);

                if (sourceArticle == null || sourceArticle.Length == 0)
                    return false;

                sqlQuery = "select * " +
                            "from T_AN_ARTICLES " +
                            "inner join T_WK_USE_LISTS on T_WK_USE_LISTS.F_ID_T_AN_ARTICLES = T_AN_ARTICLES.F_ID " +
                            "inner join T_CO_USE_LISTS_STATES on T_WK_USE_LISTS.F_ID_T_CO_USE_LISTS_STATES = T_CO_USE_LISTS_STATES.F_ID " +
                            "where T_AN_ARTICLES.F_CODE = '" + sourceArticle + "' " +
                            (state ? "and T_CO_USE_LISTS_STATES.F_CODE = 'USELIST_PROCESSED'" : "");                

                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return false;

                if (dr.Read())
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DetailCollection.IsUseList. Error:", ex);

                return false;
            }
            finally 
            {
                mDbInterface.EndRequery(dr);
                dr = null;
            }
        }

        /// <summary>
        /// Indica se il dettaglio � ottimizzato e quindi se si trova su una lastra
        /// </summary>
        /// <param name="det"></param>
        /// <returns></returns>
        private bool IsOptimized(Detail det)
        {
            string sourceArticle;
       
            sourceArticle = GetSourceArticleFromDetail(det.gCode);

            if (sourceArticle == null || sourceArticle.Length == 0)
                return false;
            else
                return true;

        }

        /// <summary>
        /// Indica se l'ordine contente il dettaglio si trova in ottimizzazione
        /// </summary>
        /// <param name="det">dettaglio interessato</param>
        /// <returns></returns>
        private bool IsOptimizing(Detail det)
        {
            OleDbDataReader dr = null;
            string sqlQuery;

            try
            {
                sqlQuery = "select *" +
                            "from T_WK_CUSTOMER_ORDER_DETAILS " +
                            "inner join T_WK_CUSTOMER_PROJECTS on T_WK_CUSTOMER_ORDER_DETAILS.F_ID_T_WK_CUSTOMER_PROJECTS = T_WK_CUSTOMER_PROJECTS.F_ID " +
                            "inner join T_WK_CUSTOMER_ORDERS on T_WK_CUSTOMER_PROJECTS.F_ID_T_WK_CUSTOMER_ORDERS = T_WK_CUSTOMER_ORDERS.F_ID " +
                            "inner join T_ORDERS on T_WK_CUSTOMER_ORDERS.F_ID = T_ORDERS.F_ID_T_WK_CUSTOMER_ORDERS " +
                            "inner join T_OP_ORDERS on T_ORDERS.F_ID_ORDER = T_OP_ORDERS.F_ID_ORDER " +
                            "where T_WK_CUSTOMER_ORDER_DETAILS.F_CODE = '" + det.gCode + "'";

                if (!mDbInterface.Requery(sqlQuery, out dr))
                    return false;

                if (dr.Read())
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DetailCollection.IsOptimizing. Error:", ex);

                return false;
            }
            finally
            {
                mDbInterface.EndRequery(dr);
                dr = null;
            }
        }

		#endregion
    }
}
