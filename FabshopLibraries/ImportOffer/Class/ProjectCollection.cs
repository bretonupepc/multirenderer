using System;
using System.Data.OleDb;

using System.Collections;
using System.Threading;
using DbAccess;
using ImportOffer.Class;
using TraceLoggers;

namespace Breton.ImportOffer
{
	public class ProjectCollection : DbCollection
	{
		#region Constructor
		public ProjectCollection(DbInterface db)
			: base(db)
		{
		}
		#endregion

		#region Public Methods
		//**********************************************************************
		// GetObject
		// Ritorna l'oggetto attualmente puntato
		// Parametri:
		//			obj	: oggetto ritornato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetObject(out Project obj)
		{
			bool ret;
			DbObject dbObj;

			ret = base.GetObject(out dbObj);

			obj = (Project)dbObj;
			return ret;
		}

		/// <summary>
		/// Restituisce l'oggetto contenuto nella collection interessata uguale a quello passato (appartenente ad un'altra collection)
		/// </summary>
		/// <param name="prj">progetto da ricercare</param>
		/// <returns></returns>
		public Project GetSameObject(Project prj)
		{
			Project retPrj;
			int cursor = mCursor;

			MoveFirst();
			while (!IsEOF())
			{
				retPrj = (Project)mRecordset[mCursor];
				if (retPrj.gCode == prj.gCode)
				{
					mCursor = cursor;
					return retPrj;
				}
				MoveNext();
			}

			mCursor = cursor;
			return null;
		}

		//**********************************************************************
		// GetObjectTable
		// Ritorna l'oggetto identificato dall'id nella tabella di visualizzazione
		// Parametri:
		//			tableId	: id nella tabella
		//			obj		: oggetto ritornato
		// Ritorna:
		//			true	oggetto ritornato
		//			false	errore nella ricerca dell'oggetto
		//**********************************************************************
		public bool GetObjectTable(int tableId, out Project obj)
		{
			//Cerca l'indice dell'oggetto
			int i = TableIdSearch(tableId);
			if (i >= 0)
			{
				obj = (Project)mRecordset[i];
				return true;
			}

			//oggetto non trovato
			obj = null;
			return false;
		}

		//**********************************************************************
		// AddObject
		// Aggiunge un oggetto in fondo alla struttura
		// Parametri:
		//			obj	: oggetto da aggiungere
		// Ritorna:
		//			true	operazione eseguita
		//			false	operazione non eseguita
		//**********************************************************************
		public bool AddObject(Project obj)
		{
			return (base.AddObject((DbObject)obj));
		}

		//**********************************************************************
		// GetSingleElementFromDb
		// Legge un singolo elemento dal database
		// Parametri:
		//			code	: codice elemento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetSingleElementFromDb(string code)
		{
			string[] codes = { code };
			return GetDataFromDb(Project.Keys.F_NONE, codes, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database non ordinati
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool GetDataFromDb()
		{
			return GetDataFromDb(Project.Keys.F_NONE, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Project.Keys orderKey)
		{
			return GetDataFromDb(orderKey, null, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderCode	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Project.Keys orderKey, string orderCode)
		{
			return GetDataFromDb(orderKey, null, orderCode, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderCode	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Project.Keys orderKey, string orderCode, string[] states)
		{
			return GetDataFromDb(orderKey, null, orderCode, states, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderCode	: chiave di ordinamento
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(string orderCode, string codeOriginalProject)
		{
			return GetDataFromDb(Project.Keys.F_NONE, null, orderCode, null, codeOriginalProject);
		}

		/// <summary>
		/// Legge i dati dal database da una lista di codici
		/// </summary>
		/// <param name="codes">lista dei codici dei dettagli interessati</param>
		/// <returns></returns>
		public bool GetDataFromDb(string[] codes)
		{
			return GetDataFromDb(Project.Keys.F_NONE, codes, null, null, null);
		}

		/// <summary>
		/// Legge i dati dal database da una lista di codici
		/// </summary>
		/// <param name="codes">lista dei codici dei dettagli interessati</param>
		/// <param name="orderKey">chiave di ordinamento</param>
		/// <returns></returns>
		public bool GetDataFromDb(Project.Keys orderKey, string[] codes)
		{
			return GetDataFromDb(orderKey, codes, null, null, null);
		}

		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, ordinati per la chiave specificata
		// Parametri:
		//			orderKey	: chiave di ordinamento
		//			code		: estrae solo l'elemento con il codice specificato
		//			state		: estrae solo gli elementi dello stato specificato
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool GetDataFromDb(Project.Keys orderKey, string[] codes, string customerOrderCode, string[] states, string originalProject)
		{
			string sqlOrderBy = "";
			string sqlWhere = "";
			string sqlAnd = " WHERE ";

			//Estrae solo i progetti con i codici richiesti
			if (codes != null)
			{
				sqlWhere += sqlAnd + "pr.F_CODE in ('" + codes[0] + "'";
				for (int i = 1; i < codes.Length; i++)
					sqlWhere += ", '" + codes[i] + "'";

				sqlWhere += ")";
				sqlAnd = " AND ";
			}

			//Estrae solo il progetto dell'ordine cliente richiesto
			if (customerOrderCode != null)
			{
				sqlWhere += sqlAnd + "co.F_CODE = '" + DbInterface.QueryString(customerOrderCode) + "'";
				sqlAnd = " AND ";
			}

			//Estrae solo i progetti con gli stati richiesti
			if (states != null)
			{
				sqlWhere += sqlAnd + "s.F_CODE in ('" + states[0] + "'";
				for (int i = 1; i < states.Length; i++)
					sqlWhere += ", '" + states[i] + "'";

				sqlWhere += ")";
				sqlAnd = " AND ";
			}


			//Estrae solo i progetti con il codice originale richiesto
			if (originalProject != null)
			{
				sqlWhere += sqlAnd + "pr.F_CODE_ORIGINAL_PROJECT = '" + DbInterface.QueryString(originalProject) + "'";
				sqlAnd = " AND ";
			}

			switch (orderKey)
			{
				case Project.Keys.F_ID:
					sqlOrderBy = " ORDER BY pr.F_ID";
					break;
				case Project.Keys.F_CODE:
					sqlOrderBy = " ORDER BY pr.F_CODE";
					break;
				case Project.Keys.F_CUSTOMER_ORDER_CODE:
					sqlOrderBy = " ORDER BY co.F_CODE";
					break;
				case Project.Keys.F_STATE:
					sqlOrderBy = " ORDER BY s.F_CODE";
					break;
				case Project.Keys.F_DESCRIPTION:
					sqlOrderBy = " ORDER BY pr.F_DESCRIPTION";
					break;
				case Project.Keys.F_CODE_ORIGINAL_PROJECT:
					sqlOrderBy = " ORDER BY pr.F_CODE_ORIGINAL_PROJECT";
					break;
				case Project.Keys.F_DATE:
					sqlOrderBy = " ORDER BY pr.F_DATE";
					break;
				case Project.Keys.F_DELIVERY_DATE:
					sqlOrderBy = " ORDER BY pr.F_DELIVERY_DATE";
					break;
			}
			return GetDataFromDb("SELECT pr.*, co.F_CODE AS F_CUSTOMER_ORDER_CODE, s.F_CODE AS F_STATE " +
				"FROM T_WK_CUSTOMER_PROJECTS pr " +
				"JOIN T_WK_CUSTOMER_ORDERS co ON co.F_ID = pr.F_ID_T_WK_CUSTOMER_ORDERS " +
				"JOIN T_CO_CUSTOMER_PROJECT_STATES s ON s.F_ID = pr.F_ID_T_CO_CUSTOMER_PROJECT_STATES " +
				sqlWhere + sqlOrderBy);
		}

		//**********************************************************************
		// UpdateDataToDb
		// Aggiorna i dati nel database
		// 
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public override bool UpdateDataToDb()
		{
			string sqlInsert = "", sqlUpdate = "", sqlDelete = "";
			int id;
			int retry = 0;
			string delivDate = "null";
			bool transaction = !mDbInterface.TransactionActive();
			ArrayList parameters = new ArrayList();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					//Scorre tutti i materiali
					foreach (Project prj in mRecordset)
					{
						switch (prj.gState)
						{
							//Inserimento
							case DbObject.ObjectStates.Inserted:
								if (transaction) mDbInterface.BeginTransaction();

								if (prj.gDeliveryDate != DateTime.MaxValue)
									delivDate = mDbInterface.OleDbToDate(prj.gDeliveryDate);
								sqlInsert = "INSERT INTO T_WK_CUSTOMER_PROJECTS (F_DELIVERY_DATE, F_DESCRIPTION, F_CODE_ORIGINAL_PROJECT, " +
									"F_REVISION, F_ID_T_WK_CUSTOMER_ORDERS, F_ID_T_CO_CUSTOMER_PROJECT_STATES) " +
									"(SELECT " + delivDate + ", '" + DbInterface.QueryString(prj.gDescription) + "', '" +
									DbInterface.QueryString(prj.gCodeOriginalProject) + "', " + prj.gRevision.ToString() + ", co.F_ID, s.F_ID " +
									"FROM T_WK_CUSTOMER_ORDERS co, T_CO_CUSTOMER_PROJECT_STATES s " +
									"WHERE co.F_CODE = '" + DbInterface.QueryString(prj.gCustomerOrderCode) + "' AND s.F_CODE = '" + DbInterface.QueryString(prj.gStatePrj) +
									"');SELECT SCOPE_IDENTITY()";
								if (!mDbInterface.Execute(sqlInsert, out id))
									return false;

								// Imposta l'id
								prj.gId = id;

								// Aggiornamento dei dettagli sul db
								if (!prj.UpdateDetailToDb(prj.gId))
									throw new ApplicationException("Error un update detail to db");

								// Termina la transazione con successo
								if (transaction) mDbInterface.EndTransaction(true);

								break;

							//Aggiornamento
							case DbObject.ObjectStates.Updated:
								if (transaction) mDbInterface.BeginTransaction();

								if (prj.gDeliveryDate != DateTime.MaxValue)
									delivDate = mDbInterface.OleDbToDate(prj.gDeliveryDate);
								sqlUpdate = "UPDATE T_WK_CUSTOMER_PROJECTS SET " +
									"F_CODE = '" + DbInterface.QueryString(prj.gCode) +
									"', F_DELIVERY_DATE = " + delivDate +
									", F_DESCRIPTION = '" + DbInterface.QueryString(prj.gDescription) +
									"', F_CODE_ORIGINAL_PROJECT = '" + DbInterface.QueryString(prj.gCodeOriginalProject) +
									"', F_REVISION = " + prj.gRevision.ToString();

								//Verifica se � stato modificato l'ordine per il progetto
								if (prj.gCustomerOrderCodeChanged)
									sqlUpdate += ", F_ID_T_WK_CUSTOMER_ORDERS = "
										+ " (SELECT F_ID FROM T_WK_CUSTOMER_ORDERS"
										+ " WHERE F_CODE = '" + DbInterface.QueryString(prj.gCustomerOrderCode) + "')";

								//Verifica se � stato modificato lo stato del progetto
								if (prj.gStatePrjChanged)
									sqlUpdate += ", F_ID_T_CO_CUSTOMER_PROJECT_STATES = "
										+ " (SELECT F_ID FROM T_CO_CUSTOMER_PROJECT_STATES"
										+ " WHERE F_CODE = '" + DbInterface.QueryString(prj.gStatePrj) + "')";

								sqlUpdate += " WHERE F_ID = " + prj.gId.ToString();

								if (!mDbInterface.Execute(sqlUpdate))
									return false;

								// Termina la transazione con successo
								if (transaction) mDbInterface.EndTransaction(true);

								break;
						}
					}

					//Scorre tutti i materiali cancellati
					foreach (Project prj in mDeleted)
					{
						switch (prj.gState)
						{
							//Cancellazione
							case DbObject.ObjectStates.Deleted:
								if (transaction) mDbInterface.BeginTransaction();

								//par = new OleDbParameter("RETURN_VALUE", OleDbType.Integer);
								//par.Direction = System.Data.ParameterDirection.ReturnValue;
								//parameters.Add(par);

								//par = new OleDbParameter("@proj_id", OleDbType.Integer);
								//par.Value = prj.gId;
								//parameters.Add(par);

								//// Lancia la stored procedure
								//if (!mDbInterface.Execute("sp_tecno_delete_offers", ref parameters))
								//    throw new ApplicationException("UpdateDataToDb sp_tecno_delete_offers Execute error.");

								//sqlDelete = "DELETE FROM T_WK_CUSTOMER_ORDER_DETAILS " +
								//    "WHERE F_ID_T_WK_CUSTOMER_PROJECTS = " + prj.gId.ToString() + "; " +
								//    "DELETE FROM T_AN_ARTICLES " +
								//    "WHERE F_ID_T_AN_STOCK_HUS = null AND " + 
								//    "F_ID IN (SELECT d.F_ID_T_AN_ARTICLES " +
								//    "FROM T_WK_CUSTOMER_ORDER_DETAILS d, T_WK_CUSTOMER_PROJECTS p " +
								//    "WHERE d.F_ID_T_WK_CUSTOMER_PROJECTS = p.F_ID AND " +
								//    "p.F_ID = " + prj.gId.ToString() + "); " +
								//    "DELETE FROM T_WK_CUSTOMER_PROJECTS " +
								//    "WHERE F_ID = " + prj.gId.ToString() + ";";

								sqlDelete = "DELETE FROM T_WK_CUSTOMER_PROJECTS " +
											"WHERE F_ID = " + prj.gId.ToString() + ";";

								if (!mDbInterface.Execute(sqlDelete))
									return false;

								// Termina la transazione con successo
								if (transaction) mDbInterface.EndTransaction(true);

								break;
						}
					}

					// Elimina l'insieme dei record cancellati
					mDeleted.Clear();

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return true;
				}

					//Eccezione di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ProjectCollection: Deadlock Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);
					//Verifica se ripetere la transazione
					retry++;
					if (retry > DbInterface.DEADLOCK_RETRY)
						throw new ApplicationException("ProjectCollection: Exceed Deadlock retrying", ex);
				}

					// Altre eccezioni
				catch (Exception ex)
				{
					TraceLog.WriteLine("ProjectCollection: Error", ex);

					// Annulla la transazione
					if (transaction) mDbInterface.EndTransaction(false);

					// Rileggi i dati dal database
					GetDataFromDb(mSqlGetData);

					return false;
				}
			}

			return false;
		}

		public override void SortByField(string key)
		{
		}

		/// <summary>
		/// Ordina la collection per lo stato di importazione. In testa saranno riportati i progetti pi� eliminabili degli altri
		/// </summary>
		public void SortByImportState()
		{
			Project tmp;

			// Bubble Sort
			for (int i = 0; i < mRecordset.Count - 1; i++)
			{
				for (int j = i + 1; j < mRecordset.Count; j++)
				{
					if ((int)((Project)mRecordset[i]).gImportState >= (int)((Project)mRecordset[i]).gImportState &&
						((Project)mRecordset[i]).gDetailsState > ((Project)mRecordset[j]).gDetailsState)
					{
						tmp = (Project)mRecordset[i];
						mRecordset[i] = mRecordset[j];
						mRecordset[j] = tmp;
					}
				}
			}
		}

		//**********************************************************************
		// RefreshData
		// Ricarica i dati con l'ultima query eseguita
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		public bool RefreshData()
		{
			return GetDataFromDb(mSqlGetData);
		}

		/// <summary>
		/// Imposta lo stato del progetto
		/// </summary>
		/// <param name="prj">progetto da analizzare</param>
		/// <returns></returns>
		public bool SetImportState(ref Project prj)
		{
			Detail det;

			try
			{
				// Imposta di default lo stato Eliminabile
				prj.gImportState = Project.ImportState.ELIMINABLE;

				prj.gDetails.MoveFirst();
				while (!prj.gDetails.IsEOF())
				{
					prj.gDetails.GetObject(out det);
					prj.gDetails.SetImportState(ref det);

					// Aggiorna il valore degli stati del progetto
					prj.gDetailsState += (int)det.gImportState;

					// Appena trova lo stato di un pezzo � Completato o In Lavorazione o In lista di estrazione (processato)
					if (det.gImportState == Detail.ImportState.COMPLETED ||
						det.gImportState == Detail.ImportState.IN_PROGRESS ||
						det.gImportState == Detail.ImportState.USE_LIST_PROCESSED)
						prj.gImportState = Project.ImportState.NOT_ELIMINABLE_PIECE;
					// Se invece trova lo stato di un pezzo In ottimizzazione o Ottimizzato o In lista di estrazione(in prog, conf) 
					// e non � gi� stato impostato lo stato NOT_ELIMINABLE_PIECE
					else if ((det.gImportState == Detail.ImportState.USE_LIST ||
						det.gImportState == Detail.ImportState.OPTIMIZED)
						//|| det.gImportState == Detail.ImportState.OPTIMIZING) 
						&& prj.gImportState != Project.ImportState.NOT_ELIMINABLE_PIECE)
						prj.gImportState = Project.ImportState.NOT_ELIMINABLE_NO_PIECE;

					prj.gDetails.MoveNext();
				}

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ProjectCollection.SetImportState", ex);
				return false;
			}
		}

		///*********************************************************************
		/// <summary>
		/// Scrive l'immagine nel database
		/// </summary>
		/// <param name="art">articolo di cui scrivere l'immagine</param>
		/// <param name="fileName">nome del file da cui prelevare l'immagine</param>
		/// <returns>
		///		true	operazione eseguita con successo
		///		false	errore nell'operazione
		///	</returns>
		///*********************************************************************
		public bool SetPreviewImageToDb(Project prj, string fileName)
		{
			int id = 0;
			int retry = 0;
			bool transaction = !mDbInterface.TransactionActive();

			//Riprova pi� volte
			while (retry < DbInterface.DEADLOCK_RETRY)
			{
				try
				{
					if (transaction) mDbInterface.BeginTransaction();

					// Inserisce un nuovo record nelle immagini
					if (!mDbInterface.Execute("insert into T_AN_BLOBS (F_IMAGE) values(null)" +
												";select scope_identity()", out id))
						throw (new ApplicationException("ProjectCollection.SetPreviewImageToDb: error writing image"));

					// Scrive l'immagine su db
					if (!mDbInterface.WriteBlobField("T_AN_BLOBS", "F_IMAGE", "", " F_ID = " + id.ToString(), fileName, ""))
						throw (new ApplicationException("ProjectCollection.SetPreviewImageToDb: error writing image"));

					// Imposta il link all'immagine
					if (!mDbInterface.Execute("update T_WK_CUSTOMER_PROJECTS set F_ID_T_AN_BLOBS_PREVIEW = " + id.ToString() +
												" where F_ID = " + prj.gId.ToString()))
						throw (new ApplicationException("ProjectCollection.SetPreviewImageToDb: error setting image mode"));

					if (transaction) mDbInterface.EndTransaction(true);

					// Memorizza l'id dell'immagine
					prj.gPreviewImageId = id;
					prj.gPreviewImageLink = fileName;
					return true;
				}

					// Errore di deadlock
				catch (DeadLockException ex)
				{
					TraceLog.WriteLine("ProjectCollection.SetPreviewImageToDb Error.", ex);

					//Se deadlock e non c'� una transazione attiva, riprova pi� volte
					if (transaction && retry < DbInterface.DEADLOCK_RETRY)
					{
						if (transaction) mDbInterface.EndTransaction(false);
						Thread.Sleep(DbInterface.DEADLOCK_WAIT);
						retry++;
					}

						//altrimenti risolleva l'eccezione di deadlock
					else
						throw ex;
				}

					// Eccezione
				catch (Exception ex)
				{
					if (transaction) mDbInterface.EndTransaction(false);
					TraceLog.WriteLine("ProjectCollection.SetPreviewImageToDb: Error", ex);
					return false;
				}
			}

			return false;
		}

		/// <summary>
		/// Legge l'immagine dal database
		/// </summary>
		/// <param name="art">articolo di cui leggere l'immagine</param>
		/// <param name="fileName">nome del file su cui memorizzare l'immagine</param>
		/// <returns>
		///			true	lettura eseguita
		///			false	lettura non eseguita
		/// </returns>
		public bool GetPreviewImageFromDb(Project prj, string fileName)
		{
			// Nessuna immagine
			if (prj.gPreviewImageId == 0)
				return false;

			try
			{
				// Recupera l'immagine dal db
				if (!mDbInterface.GetBlobField("T_AN_BLOBS", "F_IMAGE",
						" F_ID = (select F_ID_T_AN_BLOBS_PREVIEW from T_WK_CUSTOMER_PROJECTS where F_ID = " + prj.gId.ToString() + ")", fileName))
					return false;

				// Assegna l'immagine
				prj.gPreviewImageLink = fileName;
				
				return true;
			}

				// Eccezione
			catch (Exception ex)
			{
				TraceLog.WriteLine("ProjectCollection.GetPreviewImageFromDb: Error", ex);
				return false;
			}

		}
		#endregion

		#region Private Methods
		//**********************************************************************
		// GetDataFromDb
		// Legge i dati dal database, secondo la query specificata
		// Parametri:
		//			sqlQuery	: query da eseguire
		// Ritorna:
		//			true	lettura eseguita
		//			false	lettura non eseguita
		//**********************************************************************
		private bool GetDataFromDb(string sqlQuery)
		{
			OleDbDataReader dr = null;
			Project prj;

			Clear();

			// Verifica se la query � valida
			if (sqlQuery == "")
				return false;

			try
			{
				if (!mDbInterface.Requery(sqlQuery, out dr))
					return false;

				while (dr.Read())
				{
					prj = new Project(this.mDbInterface, (int)dr["F_ID"], dr["F_CODE"].ToString(), dr["F_CUSTOMER_ORDER_CODE"].ToString(),
						dr["F_STATE"].ToString(), (DateTime)dr["F_DATE"], (dr["F_DELIVERY_DATE"] != System.DBNull.Value ? (DateTime)dr["F_DELIVERY_DATE"] : DateTime.MaxValue),
						dr["F_DESCRIPTION"].ToString(), dr["F_CODE_ORIGINAL_PROJECT"].ToString(), (int)dr["F_REVISION"]);

					AddObject(prj);
					prj.gState = DbObject.ObjectStates.Unchanged;

					if (!(dr["F_ID_T_AN_BLOBS_PREVIEW"] is DBNull))
						prj.gPreviewImageId = (dr["F_ID_T_AN_BLOBS_PREVIEW"] == DBNull.Value ? -1 : (int)dr["F_ID_T_AN_BLOBS_PREVIEW"]);
				}
			}

			catch (Exception ex)
			{
				TraceLog.WriteLine("ProjectCollection: Error", ex);

				return false;
			}

			finally
			{
				mDbInterface.EndRequery(dr);
			}

			//MoveFirst();
			//while (!IsEOF())
			//{
			//    tecnoCount = 0;
			//    GetObject(out prj);
			//    prj.gDetails.GetDataFromDb(Detail.Keys.F_CODE, prj.gCode);
			//    prj.gDetails.MoveFirst();
			//    while (!prj.gDetails.IsEOF())            
			//    {
			//        prj.gDetails.GetObject(out det);

			//        if (det.gTecnoAnalyze)
			//            tecnoCount += 1;

			//        prj.gDetails.MoveNext();
			//    }

			//    if(tecnoCount == 0)
			//        prj.gTecnoAnalyze = (int)Project.Tecno.NONE;
			//    else if (tecnoCount == prj.gDetails.Count)
			//        prj.gTecnoAnalyze = (int)Project.Tecno.ALL;
			//    else if (tecnoCount < prj.gDetails.Count)
			//        prj.gTecnoAnalyze = (int)Project.Tecno.SOME;

			//    MoveNext();
			//}
			//MoveFirst();

			// Salva l'ultima query eseguita
			mSqlGetData = sqlQuery;
			return true;
		}

		#endregion
	}
}