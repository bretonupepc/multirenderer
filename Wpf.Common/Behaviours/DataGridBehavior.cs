﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Wpf.Common.Behaviours
{
    public static class DataGridBehavior
    {
        public static bool GetScrollSelectedIntoView(DataGrid dataGrid)
        {
            return (bool)dataGrid.GetValue(ScrollSelectedIntoViewProperty);
        }

        public static void SetScrollSelectedIntoView(DataGrid dataGrid, bool value)
        {
            dataGrid.SetValue(ScrollSelectedIntoViewProperty, value);
        }

        public static readonly DependencyProperty ScrollSelectedIntoViewProperty =
            DependencyProperty.RegisterAttached("ScrollSelectedIntoView", typeof(bool), typeof(DataGridBehavior),
                                                new UIPropertyMetadata(false, OnScrollSelectedIntoViewChanged));

        private static void OnScrollSelectedIntoViewChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selector = d as Selector;
            if (selector == null) return;

            if (e.NewValue is bool == false)
                return;

            if ((bool)e.NewValue)
            {
                selector.AddHandler(Selector.SelectionChangedEvent, new RoutedEventHandler(DataGridSelectionChangedHandler));
            }
            else
            {
                selector.RemoveHandler(Selector.SelectionChangedEvent, new RoutedEventHandler(DataGridSelectionChangedHandler));
            }
        }

        private static void DataGridSelectionChangedHandler(object sender, RoutedEventArgs e)
        {
            if (!(sender is DataGrid)) return;

            var dataGrid = (sender as DataGrid);
            if (dataGrid.SelectedItem != null)
            {
                dataGrid.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        dataGrid.UpdateLayout();
                        if (dataGrid.SelectedItem != null)
                            dataGrid.ScrollIntoView(dataGrid.SelectedItem);
                    }));
            }
        }
    }
}
