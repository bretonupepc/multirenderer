﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Wpf.Common.Behaviours
{
    public static class ListViewBehavior
    {
        public static bool GetScrollSelectedIntoView(ListView ListView)
        {
            return (bool)ListView.GetValue(ScrollSelectedIntoViewProperty);
        }

        public static void SetScrollSelectedIntoView(ListView ListView, bool value)
        {
            ListView.SetValue(ScrollSelectedIntoViewProperty, value);
        }

        public static readonly DependencyProperty ScrollSelectedIntoViewProperty =
            DependencyProperty.RegisterAttached("ScrollSelectedIntoView", typeof(bool), typeof(ListViewBehavior),
                                                new UIPropertyMetadata(false, OnScrollSelectedIntoViewChanged));

        private static void OnScrollSelectedIntoViewChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var selector = d as Selector;
            if (selector == null) return;

            if (e.NewValue is bool == false)
                return;

            if ((bool)e.NewValue)
            {
                selector.AddHandler(Selector.SelectionChangedEvent, new RoutedEventHandler(ListViewSelectionChangedHandler));
            }
            else
            {
                selector.RemoveHandler(Selector.SelectionChangedEvent, new RoutedEventHandler(ListViewSelectionChangedHandler));
            }
        }

        private static void ListViewSelectionChangedHandler(object sender, RoutedEventArgs e)
        {
            if (!(sender is ListView)) return;

            var ListView = (sender as ListView);
            if (ListView.SelectedItem != null)
            {
                ListView.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        ListView.UpdateLayout();
                        if (ListView.SelectedItem != null)
                            ListView.ScrollIntoView(ListView.SelectedItem);
                    }));
            }
        }
    }
}
