
using System;

namespace Wpf.Common
{
    /// <summary>
    /// Represents a typed-argument <see cref="EventArgs"/>.
    /// </summary>
    /// <typeparam name="T">The argument type.</typeparam>
    public class EventArgs<T> : EventArgs
    {
        #region >-------------- Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EventArgs{T}"/> class.
        /// </summary>
        /// <param name="argument">The given argument.</param>
        public EventArgs(T argument)
        {
            Value = argument;
        }

        #endregion Public Constructors -----------<

        #region >-------------- Public Properties

        /// <summary>
        /// Gets or sets the argument.
        /// </summary>
        public T Value
        {
            get { return _value; }
            set { _value = value; }
        }

        #endregion Public Properties -----------<

        #region >----------- Protected Fields

        protected T _value;

        #endregion Protected Fields --------<
    }
}