﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Wpf.Common
{
    public sealed class BooleanToVisibilityConverter : BooleanConverter<Visibility>
    {
        public BooleanToVisibilityConverter() :
            base(Visibility.Visible, Visibility.Collapsed) { }
    }

    public sealed class BooleanToVisibilityInverseConverter : BooleanConverter<Visibility>
    {
        public BooleanToVisibilityInverseConverter() :
            base(Visibility.Collapsed, Visibility.Visible) { }
    }
}
