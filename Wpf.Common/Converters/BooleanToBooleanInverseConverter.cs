﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Wpf.Common
{
    public sealed class BooleanToBooleanInverseConverter : BooleanConverter<bool>
    {
        public BooleanToBooleanInverseConverter() :
            base(false, true) { }
    }
}
