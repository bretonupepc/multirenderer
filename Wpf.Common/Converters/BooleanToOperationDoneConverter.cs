﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Wpf.Common
{
    public sealed class BooleanToOperationDoneConverter : BooleanConverter<string>
    {
        public BooleanToOperationDoneConverter() :
            base("Done", "Not generated") { }
    }
}
