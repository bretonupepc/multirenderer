﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Wpf.Common
{
    public sealed class ObjectNotNullToVisibilityConverter : IValueConverter
    {

        public object Convert(object value, Type targetType,
                object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility result = (value != null) ? Visibility.Visible : Visibility.Collapsed;


            return result;

        }



        public object ConvertBack(object value, Type targetType,

            object parameter, System.Globalization.CultureInfo culture)
        {

            throw new NotImplementedException();

        }
    }
}
