﻿namespace Wpf.Common
{
    public delegate void SignalEventHandler();

    public delegate void StringInvoker(string value);
    public delegate void IntInvoker(int value);
    public delegate void BooleanInvoker(bool value);


}
