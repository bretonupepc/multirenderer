﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Wpf.Common.Extensions
{
    /// <summary>
    /// Estensioni per Dependency Object
    /// </summary>
    public static class VisualExtensions
    {
        /// <summary>
        /// Ricerca nel Visual Tree il primo discendente di un tipo specificato
        /// </summary>
        /// <typeparam name="T">Tipo da ricercare</typeparam>
        /// <param name="depObj">Dependency object</param>
        /// <returns>Visual trovato, oppure null</returns>
        public static T GetChildOfType<T>(this DependencyObject depObj)
            where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }



    }
}
