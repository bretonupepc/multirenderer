﻿using System.ComponentModel;

using MessageProvider;

namespace MessageProvider
{
    internal class XamlLocalizer : INotifyPropertyChanged
    {
        public static XamlLocalizer Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new XamlLocalizer();
                }
                return _instance;
            }
        }

        private static XamlLocalizer _instance = null;

        public void RefreshAll()
        {
            OnPropertyChanged("");
        }



        public string OK { get { return ResourceHelper.GetXamlString("OK", "Ok"); } }

        public string YES { get { return ResourceHelper.GetXamlString("YES", "Sì"); } }
        public string NO { get { return ResourceHelper.GetXamlString("NO", "No"); } }
        public string CANCEL { get { return ResourceHelper.GetXamlString("CANCEL", "Annulla"); } }





        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
