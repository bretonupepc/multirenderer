﻿#pragma checksum "..\..\MessageWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "AF654C8726723321F36243C78F769F8B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MessageProvider;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Wpf.Presentation.UserControls.BUTTONS;


namespace MessageProvider {
    
    
    /// <summary>
    /// MessageWindow
    /// </summary>
    public partial class MessageWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 49 "..\..\MessageWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock MessageTitle;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\MessageWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image BoxImage;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\MessageWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Message;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\MessageWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton ButtonOk;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\MessageWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton ButtonYes;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\MessageWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton ButtonNo;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\MessageWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton ButtonCancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/MessageProvider;component/messagewindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MessageWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 35 "..\..\MessageWindow.xaml"
            ((System.Windows.Controls.Border)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.MainBorder_OnMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.MessageTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.BoxImage = ((System.Windows.Controls.Image)(target));
            return;
            case 4:
            this.Message = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.ButtonOk = ((Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton)(target));
            
            #line 75 "..\..\MessageWindow.xaml"
            this.ButtonOk.Click += new System.Windows.RoutedEventHandler(this.ButtonOk_OnClick);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ButtonYes = ((Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton)(target));
            
            #line 91 "..\..\MessageWindow.xaml"
            this.ButtonYes.Click += new System.Windows.RoutedEventHandler(this.ButtonYes_OnClick);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ButtonNo = ((Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton)(target));
            
            #line 107 "..\..\MessageWindow.xaml"
            this.ButtonNo.Click += new System.Windows.RoutedEventHandler(this.ButtonNo_OnClick);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ButtonCancel = ((Wpf.Presentation.UserControls.BUTTONS.MessageBoxSpecializedButton)(target));
            
            #line 127 "..\..\MessageWindow.xaml"
            this.ButtonCancel.Click += new System.Windows.RoutedEventHandler(this.ButtonCancel_OnClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

