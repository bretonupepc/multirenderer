﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Timer = System.Timers.Timer;

namespace MessageProvider
{
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window, INotifyPropertyChanged
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private string _caption = string.Empty;

        private string _messageContent = string.Empty;

        private MessageBoxButton _buttons = MessageBoxButton.OK;

        private MessageBoxImage _messageImage = MessageBoxImage.None;

        private MessageBoxResult _result = MessageBoxResult.None;


        private Visibility _okVisibility = Visibility.Visible;
        private Visibility _yesVisibility = Visibility.Collapsed;
        private Visibility _noVisibility = Visibility.Collapsed;
        private Visibility _cancelVisibility = Visibility.Collapsed;

        private string _okButtonCaption = string.Empty;

        private string _yesButtonCaption = string.Empty;

        private string _noButtonCaption = string.Empty;

        private string _cancelButtonCaption = string.Empty;

        private int _remainingSeconds;

        private DispatcherTimer _replyTimeoutTimer = new DispatcherTimer();


        private MessageBoxResult _timeoutAutoReplyResult;

        private int _secondsToWait = 10;

        private int _secondsWaited = 0;

        private bool _autoReplyModeOn = false;

        private string _warningMessage = string.Empty;

        private string _warningMessageFormatString = string.Empty;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public MessageWindow()
        {
            InitializeComponent();

            DataContext = this;

            WarningMessageFormatString = ResourceHelper.GetString("TIMEOUT_WARNING_MESSAGE",
                        "Tempo rimanente per effettuare una scelta: {0} s ...");

            _replyTimeoutTimer.Interval = TimeSpan.FromSeconds(1d);
            _replyTimeoutTimer.Tick += _replyTimeoutTimer_Tick;


        }

        void _replyTimeoutTimer_Tick(object sender, EventArgs e)
        {
            SecondsWaited++;
            WarningMessage = string.Format(WarningMessageFormatString, _secondsToWait - SecondsWaited);
            if (SecondsWaited >= _secondsToWait)
            {
                _replyTimeoutTimer.Stop();
                HideMessageBox(_timeoutAutoReplyResult);
            }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                e.Handled = true;
                _replyTimeoutTimer.Stop();
                Close();
            }
        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value;
                OnPropertyChanged("Caption");
            }
        }

        public string MessageContent
        {
            get { return _messageContent; }
            set
            {
                _messageContent = value;
                OnPropertyChanged("MessageContent");

            }
        }

        public MessageBoxButton Buttons
        {
            get { return _buttons; }
            set
            {
                _buttons = value;
                SetupButtons();

            }
        }

        //public Image BoxImage
        //{
        //    get { return _boxImage; }
        //    set
        //    {
        //        _boxImage = value;
        //        OnPropertyChanged("BoxImage");

        //    }
        //}

        public MessageBoxResult Result
        {
            get { return _result; }
            set
            {
                _result = value;
                OnPropertyChanged("Result");

            }
        }

        public Visibility OkVisibility
        {
            get { return _okVisibility; }
            set { _okVisibility = value; }
        }

        public Visibility YesVisibility
        {
            get { return _yesVisibility; }
            set { _yesVisibility = value; }
        }

        public Visibility NoVisibility
        {
            get { return _noVisibility; }
            set { _noVisibility = value; }
        }

        public Visibility CancelVisibility
        {
            get { return _cancelVisibility; }
            set { _cancelVisibility = value; }
        }

        public MessageBoxImage MessageImage
        {
            get { return _messageImage; }
            set
            {
                _messageImage = value;
                SetupBoxImage();
            }
        }

        public string OkButtonCaption
        {
            get { return string.IsNullOrEmpty(_okButtonCaption) ? XamlLocalizer.Instance.OK : _okButtonCaption; }
            set
            {
                _okButtonCaption = value;
                OnPropertyChanged("OkButtonCaption");
            }
        }

        public string YesButtonCaption
        {
            get { return string.IsNullOrEmpty(_yesButtonCaption) ? XamlLocalizer.Instance.YES : _yesButtonCaption; }
            set
            {
                _yesButtonCaption = value;
                OnPropertyChanged("YesButtonCaption");
            }
        }

        public string NoButtonCaption
        {
            get { return string.IsNullOrEmpty(_noButtonCaption) ? XamlLocalizer.Instance.NO : _noButtonCaption; }
            set
            {
                _noButtonCaption = value;
                OnPropertyChanged("NoButtonCaption");
            }
        }

        public string CancelButtonCaption
        {
            get { return string.IsNullOrEmpty(_cancelButtonCaption) ? XamlLocalizer.Instance.CANCEL : _cancelButtonCaption; }
            set
            {
                _cancelButtonCaption = value;
                OnPropertyChanged("CancelButtonCaption");
            }
        }

        public int RemainingSeconds
        {
            get { return _remainingSeconds; }
            set
            {
                _remainingSeconds = value;
                OnPropertyChanged("RemainingSeconds");
            }
        }

        public bool AutoReplyModeOn
        {
            get { return _autoReplyModeOn; }
            set
            {
                _autoReplyModeOn = value;
                if (_autoReplyModeOn)
                {
                    WarningMessage = string.Format(WarningMessageFormatString, _secondsToWait - SecondsWaited);
                    _replyTimeoutTimer.Start();
                }
                else
                {
                    _replyTimeoutTimer.Stop();
                }
                OnPropertyChanged("AutoReplyModeOn");
                
            }
        }

        public MessageBoxResult TimeoutAutoReplyResult
        {
            get { return _timeoutAutoReplyResult; }
            set
            {
                _timeoutAutoReplyResult = value;
                SetupDefaultButton();
            }
        }


        public int SecondsToWait
        {
            get { return _secondsToWait; }
            set
            {
                _secondsToWait = value;
                OnPropertyChanged("SecondsToWait");
                RemainingSeconds = _secondsToWait;
            }
        }

        public string WarningMessage
        {
            get { return _warningMessage; }
            set
            {
                _warningMessage = value;
                OnPropertyChanged("WarningMessage");
            }
        }

        public string WarningMessageFormatString
        {
            get { return _warningMessageFormatString; }
            set { _warningMessageFormatString = value; }
        }

        private int SecondsWaited
        {
            get { return _secondsWaited; }
            set
            {
                _secondsWaited = value;
                RemainingSeconds = SecondsToWait - SecondsWaited;
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods


        private void HideMessageBox(MessageBoxResult result)
        {
            _replyTimeoutTimer.Stop();
            Result = result;
            Hide();
        }

        private void SetupButtons()
        {
            switch (_buttons)
            {
                case MessageBoxButton.OK:
                    OkVisibility = Visibility.Visible;
                    YesVisibility = Visibility.Collapsed;
                    NoVisibility = Visibility.Collapsed;
                    CancelVisibility = Visibility.Collapsed;
                    break;

                case MessageBoxButton.OKCancel:
                    OkVisibility = Visibility.Visible;
                    YesVisibility = Visibility.Collapsed;
                    NoVisibility = Visibility.Collapsed;
                    CancelVisibility = Visibility.Visible;
                    break;

                case MessageBoxButton.YesNo:
                    OkVisibility = Visibility.Collapsed;
                    YesVisibility = Visibility.Visible;
                    NoVisibility = Visibility.Visible;
                    CancelVisibility = Visibility.Collapsed;
                    break;

                case MessageBoxButton.YesNoCancel:
                    OkVisibility = Visibility.Collapsed;
                    YesVisibility = Visibility.Visible;
                    NoVisibility = Visibility.Visible;
                    CancelVisibility = Visibility.Visible;
                    break;
            }
        }

        private void SetupDefaultButton()
        {
            switch (TimeoutAutoReplyResult)
            {
                case MessageBoxResult.OK:
                    ButtonOk.IsTimedButton = true;
                    break;
                case MessageBoxResult.Cancel:
                    ButtonCancel.IsTimedButton = true;
                    break;
                case MessageBoxResult.Yes:
                    ButtonYes.IsTimedButton = true;
                    break;
                case MessageBoxResult.No:
                    ButtonNo.IsTimedButton = true;
                    break;
                case MessageBoxResult.None:
                    break;
            }
        }


        private void SetupBoxImage()
        {
            switch (_messageImage)
            {
                case MessageBoxImage.Error:
                    BoxImage.Source = new BitmapImage(new Uri(@"/MessageProvider;component/RESOURCES/IMAGES/messagebox_critical.png", UriKind.RelativeOrAbsolute));
                    break;

                case MessageBoxImage.Warning:
                    BoxImage.Source = new BitmapImage(new Uri(@"/MessageProvider;component/RESOURCES/IMAGES/messagebox_warning.png", UriKind.RelativeOrAbsolute));
                    break;

                case MessageBoxImage.Information:
                    BoxImage.Source = new BitmapImage(new Uri(@"/MessageProvider;component/RESOURCES/IMAGES/messagebox_info.png", UriKind.RelativeOrAbsolute));
                    break;

                case MessageBoxImage.None:
                    BoxImage = null;
                    break;

                case MessageBoxImage.Question:
                    BoxImage.Source = new BitmapImage(new Uri(@"/MessageProvider;component/RESOURCES/IMAGES/user.png", UriKind.RelativeOrAbsolute));
                    break;

            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates

        private void ButtonOk_OnClick(object sender, RoutedEventArgs e)
        {
            HideMessageBox(MessageBoxResult.OK);
            //Result = MessageBoxResult.OK;
            //Hide();
        }

        private void ButtonYes_OnClick(object sender, RoutedEventArgs e)
        {
            HideMessageBox(MessageBoxResult.Yes);
            //Result = MessageBoxResult.Yes;
            //Hide();
        }

        private void ButtonNo_OnClick(object sender, RoutedEventArgs e)
        {
            HideMessageBox(MessageBoxResult.No);
            //Result = MessageBoxResult.No;
            //Hide();
        }

        private void ButtonCancel_OnClick(object sender, RoutedEventArgs e)
        {
            HideMessageBox(MessageBoxResult.Cancel);
            //Result = MessageBoxResult.Cancel;
            //Hide();
        }

        private void MainBorder_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        #endregion Delegates  -----------<

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
