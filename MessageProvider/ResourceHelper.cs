﻿using System;
using System.Resources;

namespace MessageProvider
{
    public class ResourceHelper
    {
        // for translation purposes
        //TODO: Remove once translation strings extracted
        //public static Dictionary<string, string> LocalizationEntries = new Dictionary<string, string>();


        private static bool _designMode;
        private static bool _designModeChecked = false;

        private static bool DesignMode
        {
            get
            {
                if (!_designModeChecked)
                {
                    _designMode = (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv");
                    _designModeChecked = true;
                }
                return _designMode;
            }
        }

        private static ResourceManager _rm = null;

        private static ResourceManager Rm
        {
            get
            {
                if (_rm == null)
                {
                    _rm = Properties.Resources.ResourceManager;
                }
                return _rm;
            }
        }

        public static string GetString(string name)
        {
            return GetString(name, string.Empty);
        }

        public static string GetString(string name, string backupValue)
        {
            //TODO: Remove once translation strings extracted
            #region DEBUG to build dictionary entries

            //if (!LocalizationEntries.ContainsKey(name))
            //{
            //    if (string.IsNullOrEmpty(Rm.GetString(name)) && backupValue != "IGNORE")
            //        LocalizationEntries.Add(name, backupValue);
            //}

            #endregion

            string localized = string.Empty;
            try
            {
                localized = Rm.GetString(name);
                if (string.IsNullOrEmpty(localized))
                {
                    localized = backupValue;
                }
            }

            catch (Exception ex)
            {
                localized = backupValue;
            }

            return FixNewLine(localized);
        }

        private static string FixNewLine(string localized)
        {
            return localized.Replace(@"\n", Environment.NewLine);
        }

        public static string GetXamlString(string name, string backupValue)
        {
            if (DesignMode)
            {
                return backupValue;
            }

            string localized = GetString(name, backupValue);

            return localized;
        }



    }
}

