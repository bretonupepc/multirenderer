﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MessageProvider
{
    public interface IMessageProvider
    {

        MessageBoxResult ShowMessage(string message);
        MessageBoxResult ShowMessage(string message, string caption);
        MessageBoxResult ShowMessage(string message, string caption, MessageBoxButton button);
        //MessageBoxResult ShowMessage(string message, string caption, MessageBoxButton button, MessageBoxImage icon);

        MessageBoxResult ShowMessage(string message, string caption, MessageBoxButton button, MessageBoxImage icon,
            string okButtonCaption = "", string yesButtonCaption = "", string noButtonCaption = "", string cancelButtonCaption = "");

        void SetMessageReplyTimeout(int seconds, MessageBoxResult autoReplyResult, string warningMessageFormatString);

    }
}
