﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using Timer = System.Timers.Timer;

namespace MessageProvider
{
    public class MessageBoxProvider: IMessageProvider
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private Window _owner = null;

        private MessageBoxResult _timeoutReplyResult;

        private int _secondsToWait = 10;

        private bool _autoReplyModeOn = false;

        private string _warningMessageFormatString = string.Empty;


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public Window Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }


        public bool AutoReplyModeOn
        {
            get { return _autoReplyModeOn; }
            set { _autoReplyModeOn = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        public MessageBoxResult ShowMessage(string message)
        {
            return ShowMessage(message, string.Empty, MessageBoxButton.OK, MessageBoxImage.None);
        }

        public MessageBoxResult ShowMessage(string message, string caption)
        {
            return ShowMessage(message, caption, MessageBoxButton.OK, MessageBoxImage.None);
        }

        public MessageBoxResult ShowMessage(string message, string caption, MessageBoxButton button)
        {
            return ShowMessage(message, string.Empty, button, MessageBoxImage.None);
        }

        //public MessageBoxResult ShowMessage(string message, string caption, MessageBoxButton button, MessageBoxImage icon)
        //{
        //    MessageBoxResult result = MessageBoxResult.None;

        //    //using (MessageWindow window = new MessageWindow())
        //    {
        //        MessageWindow window = new MessageWindow();

        //        window.MessageContent = message;
        //        window.Caption = caption;
        //        window.MessageImage = icon;
        //        window.Buttons = button;
        //        window.Owner = Owner;

        //        if (Owner != null)
        //        {
        //            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
        //        }
        //        else
        //        {
        //            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        //        }

        //        bool? value = window.ShowDialog();

        //        result = window.Result;

        //        window.Close();
        //    }


        //    return result;

        //}
        
        public MessageBoxResult ShowMessage(string message, string caption, MessageBoxButton button, MessageBoxImage icon,
            string okButtonCaption = "", string yesButtonCaption = "", string noButtonCaption = "", string cancelButtonCaption = "")
        {
            MessageBoxResult result = MessageBoxResult.None;

            //using (MessageWindow window = new MessageWindow())
            {
                MessageWindow window = new MessageWindow();

                window.MessageContent = message;
                window.Caption = caption;
                window.MessageImage = icon;

                if (AutoReplyModeOn)
                {
                    window.SecondsToWait = _secondsToWait;
                    window.TimeoutAutoReplyResult = _timeoutReplyResult;
                    if (!string.IsNullOrEmpty(_warningMessageFormatString))
                    {
                        window.WarningMessageFormatString = _warningMessageFormatString;
                    }
                }

                window.AutoReplyModeOn = _autoReplyModeOn;

                window.Buttons = button;

                if (!string.IsNullOrEmpty(okButtonCaption))
                {
                    window.OkButtonCaption = okButtonCaption;
                }

                if (!string.IsNullOrEmpty(yesButtonCaption))
                {
                    window.YesButtonCaption = yesButtonCaption;
                }

                if (!string.IsNullOrEmpty(noButtonCaption))
                {
                    window.NoButtonCaption = noButtonCaption;
                }

                if (!string.IsNullOrEmpty(cancelButtonCaption))
                {
                    window.CancelButtonCaption = cancelButtonCaption;
                }



                window.Owner = Owner;

                if (Owner != null)
                {
                    window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                }
                else
                {
                    window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                }

                //if (AutoReplyModeOn)
                //{
                //    window.SecondsToWait = _secondsToWait;
                //    window.TimeoutAutoReplyResult = _timeoutReplyResult;
                //    if (!string.IsNullOrEmpty(_warningMessageFormatString))
                //    {
                //        window.WarningMessageFormatString = _warningMessageFormatString;
                //    }
                //}

                //window.AutoReplyModeOn = _autoReplyModeOn;

                window.Topmost = true;

                bool? value = window.ShowDialog();

                result = window.Result;

                window.Close();

                AutoReplyModeOn = false;
            }


            return result;

        }



        public void SetMessageReplyTimeout(int seconds, MessageBoxResult autoReplyResult, string warningMessageFormatString)
        {
            _secondsToWait = seconds;
            _timeoutReplyResult = autoReplyResult;
            _warningMessageFormatString = warningMessageFormatString;
            AutoReplyModeOn = true;

        }
    }
}
