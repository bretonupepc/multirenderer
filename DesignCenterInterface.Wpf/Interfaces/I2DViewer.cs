﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using Breton.DesignCenterInterface;
using Breton.MathUtils;
using Breton.Polygons;
using ColorPalette = Breton.DesignCenterInterface.Wpf.ColorPalette;
using Point = Breton.Polygons.Point;

namespace DesignCenterInterface.Interfaces
{
    public interface I2DViewer
    {
        void SaveCurrentView();
        void RestoreLastSavedView();

        void Lock();
        void Unlock();

        void ShowAxis(bool show);
        void ShowOriginSymbol(bool show);

        void InitializeViewer();

        void HighLight(int idBlock, int idPath, int idSide, int idVertex, double thickness,
            CBasicPaint.HighLigthType type);


        bool ManualRotoTrasl(int block, RTMatrix matrix);


        IEntitiesSelection SelectEntities(int idBlock, int idPath, int idSide, int idVertex);
        void DeleteEntities(int idBlock, int idPath);

        IEntitiesSelection GetSelectedEntities();


        bool CreateThumbnail(string filename, int dimx, int dimy, bool transparent, ImageFormat imageFormat = null);

        void Print(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper);

        void Print(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper,
            bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont);



        bool SaveDxf(string filename);


        // private eyeLayer
        //void AddLayer(CLayer clayer);
        //void SetLayer(CLayer clayer);
        //void FindLayer(string layerName);
        //bool DisableLayer(string strLayer);
        //bool EnableLayer(string strLayer);



        void DrawPath(Path path, int idBlock, int idPath);




        Point ViewCenter { get; set; }
        double ViewSize { get; set; }

        ColorPalette Palette { get; }

        List<IEntitiesSelection> Selections { get; }
    }
}
