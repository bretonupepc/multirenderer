﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Breton.DesignCenterInterface.Wpf;
using Breton.Polygons;

namespace DesignCenterInterface.Interfaces
{
    public interface IEntitiesSelection
    {

        string Name { get; set; }

        IEntitiesSelection Clone();

        void AddItem(object entity, bool checkIfAlreadyExists);
        void AddItem(object entity);

        void FilterSelect(object layout, EntitiesFilter filter, bool clearBefore = true);

        void ApplyFilter(EntitiesFilter filter);

        void Rotate(double angle, Point rotationCenter);

        void Traslate(double deltaX, double deltaY);
    }
}
