﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Breton.MathUtils;
using Breton.Polygons;

namespace DesignCenterInterface.Class
{
    public class HatchedPath: Path
    {
        private string _hatchName = string.Empty;

        public HatchedPath() : base()
        {
            
        }

        public HatchedPath(Path p)
            : base(p)
        {

        }

        public HatchedPath(HatchedPath p)
            :this (p as Path)
        {
            _hatchName = p.HatchName;
        }

        public HatchedPath(Rect r)
            : base(r)
        {

        }

        public string HatchName
        {
            get { return _hatchName; }
            set { _hatchName = value; }
        }

    }
}
