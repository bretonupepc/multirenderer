using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using Breton.DesignCenterInterface.Wpf;
using Breton.MathUtils;
using Breton.Polygons;
using BretonViewportLayout;
using BretonViewportLayout.Common;
using BretonViewportLayout.Wpf;
using DesignCenterInterface.Class;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Eyeshot.Translators;
using System.Xml.Serialization;
using devDept.Geometry;
using devDept.Graphics;
using TraceLoggers;
using Arc = Breton.Polygons.Arc;
using Color = System.Drawing.Color;
using Cursor = System.Windows.Forms.Cursor;
using DragDropEffects = System.Windows.Forms.DragDropEffects;
using DragEventArgs = System.Windows.Forms.DragEventArgs;
using MouseButton = System.Windows.Input.MouseButton;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;
using Path = Breton.Polygons.Path;
using Point = Breton.Polygons.Point;
using Rect = Breton.MathUtils.Rect;
using Region = devDept.Eyeshot.Entities.Region;
using Size = System.Drawing.Size;
using Vector2D = devDept.Geometry.Vector2D;

namespace Breton.DesignCenterInterface
{

    /// <summary>
    /// CBasicPaint gestisce la visualizzazione di percorsi e l'interazione con l'utente.
    /// </summary>
    public class CBasicPaint
    {
        #region >-------------- Constants and Enums

        private enum Action
        { 
            DEFAULT,
            MOVE,
            ROTATE
        }

        public enum HighLigthType
        { 
            NONE,
            NORMAL,
            DOT
        }

        public enum SnapMode
        {
            NONE = 0,
            //APPARENTINT,
            //CEN,
            END = 1,
            //INS,
            //INTERS,
            MID = 2,
            NEA = 4,
            NODE = 8, // == Point
            //PER,
            //QUA,
            //TANG,
            ALL = 16
			
        }

        /// <summary>
        /// Orientamento assi
        /// </summary>
        public enum AxisDirection
        {
            LEFT_BOTTOM,	
            LEFT_TOP,
            RIGHT_BOTTOM,			
            RIGHT_TOP
        }

        public enum DimensionType
        { 
            LINEAR = 1,
            ORIZONTAL = 2,
            VERTICAL = 3,
            ANGULAR,
            RADIAL,
            DIAMETER
        }

        private const float HIGHLIGHT_PIXEL_THICKNESS = 2.5F;

        private const double CURVES_CHORDAL_ERROR = 1e-2;

        private const double LAYER_Z_SHIFT = 1e-3;

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration


        public event MouseMoveAfterHandler evMouseAfter;

        public event CBasicPaintEventArgsHandler evClick;

        public event CBasicPaintEventArgsHandler evStartMove;
        public event CBasicPaintEventArgsHandler evMove;
        public event CBasicPaintEventArgsHandler evEndMove;

        public event CBasicPaintEventArgsHandler evStartRotate;
        public event CBasicPaintEventArgsHandler evRotate;
        public event CBasicPaintEventArgsHandler evEndRotate;

        public event DragDropHandler evDragDrop;
        public event DragEnterHandler evDragEnter;
        public event DragLeaveHandler evDragLeave;

        public static event EventHandler<EventArgs> OutOfMemoryRaised;

        private static void OnOutOfMemoryRaised()
        {
            var handler = OutOfMemoryRaised;
            if (handler != null)
            {
                handler(null, new EventArgs());
            }
        }


        #region >-------------- Delegates

        public delegate void DragDropHandler(object sender, CBasicPaintEventArgs ev, Block bDragDrop);
        public delegate void DragEnterHandler(object sender, CBasicPaintEventArgs ev, Block bDragDrop);
        public delegate void DragLeaveHandler(object sender, CBasicPaintEventArgs ev, Block bDragDrop);
        public delegate void MouseMoveAfterHandler(object sender, double x, double y, double z);

        #endregion Delegates  -----------<

        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        #region >-------------- >----- CBasicPaint geometries

        private const double START_ROTATE_MINIMUM_DISTANCE = 20.0;
        private Point _userPoint = new Point();          //punto cliccato dall'utente
        private Point _lastOkPoint = new Point();        //ultimo punto valido in mm
        private List<RTMatrix> _lastMatrix = new List<RTMatrix>();

        private double _xDragDrop = 0;					//delta x da applicare al D&D nella finestra di destinazione
        private double _yDragDrop = 0;					//delta y da applicare al D&D nella finestra di destinazione

        private Point _startZoomWindowPoint = null;
        private bool _zoomWindow = false;
        private DraftingViewportLayout.SnapPoint _lastSnapPoint;
        private bool _drawMouseCoordinates = true;
        private Segment _rotationReferenceSegment = null;

        private bool _applyLayerZShift = true;


        #endregion

        #region >-------------- >----- CBasicPaint properties

        private bool _locked = false;
        private Color _backColor;
        private bool _showLayout;
        private bool _orthoMode = false;


        private string _mouseTooltip = string.Empty;
        private bool _drawMouseTooltip = true;
        private Color _rotateReferenceColor = Color.DodgerBlue;

        private double _viewTopRotationAngle = 0;

        #endregion

        #region >-------------- >----- CBasicPaint logics

        private CBasicPaintEventArgs basicPaintEventArgs = null;
        EyeSelection _selActive = new EyeSelection(); //selezione di entità che si stanno muovendo o ruotando, usata per modificare velocemente le coordinate
        private Block _blockDragDrop;	//blocco di cui si sta facendo il drag&drop


        private List<SnapMode> _osnapModes;
        private List<string> _snapLayers;	


        private AxisDirection _axisDirection;
        private Action currentAction = Action.DEFAULT;
        private bool _canStartRotate = false;

        private bool _selectionEnabled = true;

        double resizingSymbolHalfSide = 3.0;

        #endregion

        #region >-------------- >----- Eyeshot related

        private Breton2DViewer _viewer = null;

        private DraftingViewportLayout _viewportLayout = null;

        private Camera _savedCamera = null;

        private double _cLayerTranslactionFactor = 0.001;

        private List<int> _lastSelectedBlocks = new List<int>();

        private int _lastSelectedBlock = 0;

        #endregion

        #region >-------------- >----- Misc

        private readonly Dictionary<SnapMode, ObjectSnapType> SnapModeMap = new Dictionary<SnapMode, ObjectSnapType>()
        {
            {SnapMode.END, ObjectSnapType.End},
            {SnapMode.MID, ObjectSnapType.Mid},
            {SnapMode.NEA, ObjectSnapType.Near},
            {SnapMode.NODE, ObjectSnapType.Point}

        };

        private readonly Dictionary<ObjectSnapType, SnapMode> SnapModeInverseMap = new Dictionary<ObjectSnapType, SnapMode>()
        {
            {ObjectSnapType.End, SnapMode.END},
            {ObjectSnapType.Mid, SnapMode.MID},
            {ObjectSnapType.Near, SnapMode.NEA},
            {ObjectSnapType.Point, SnapMode.NODE}

        };

        //private readonly float[] DashDotLineType = new float[] { 5, -1.5f, 0.25f, -1.5f };
        private readonly float[] DashLineType = new float[] { 20f, -20f };
        private int _activeLayerIndex = 0;
        private Timer mTimer;

        private Timer _regenTimer;

        #endregion


        protected bool UseHilightColorOnColorRegions = true;

        private BackgroundSettings _defaultBackgroundSettings = null;

        private static bool _useAntialiasingText = true;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public CBasicPaint()
        {
        }

        /// <summary>
        /// Classe di interfaccia per editing grafico
        /// </summary>
        /// <param name="viewer">UserControl visualizzatore 2D Breton</param>
        public CBasicPaint(Breton2DViewer viewer)
            : this(viewer, (AxisDirection) AxisDirection.LEFT_BOTTOM)
        {

        }

        public CBasicPaint(Breton2DViewer viewer, bool useTouchLayout, bool showRuler = false, bool showRulerCoordinates = false)
            : this(viewer, (AxisDirection) AxisDirection.LEFT_BOTTOM, useTouchLayout, showRuler, showRulerCoordinates)
        {
        }

        /// <summary>
        /// Classe di interfaccia per editing grafico
        /// </summary>
        /// <param name="viewer">UserControl visualizzatore 2D Breton</param>
        /// <param name="ax">Orientazione assi</param>
        /// <param name="useTouchLayout">Abilita interfaccia touch</param>
        /// <param name="showRuler">Visualizza righello</param>
        /// <param name="showRulerCoordinates">Visualizza finestrella coordinate se il righello è attivato</param>
        public CBasicPaint(Breton2DViewer viewer, AxisDirection ax, bool useTouchLayout = false, bool showRuler = false, bool showRulerCoordinates = false)
        {
            _viewer = viewer;
            _viewportLayout = viewer.BretonLayout;

            // TODO
            //UseAntialiasingText = _viewportLayout.IsFsaaAvailable;

            _axisDirection = ax;

            //_backColor = _viewportLayout.Viewports[0].Background.TopColor;
            var colorBrush = _viewportLayout.Viewports[0].Background.TopColor as SolidColorBrush;
            if (colorBrush != null)
                _backColor = RenderContextUtility.ConvertColor(colorBrush);
            ////_backColor = mDisplayObject.BaseControl.ActiveDocument.Palette.Background;

            Viewer.UseTouchLayout = useTouchLayout;



            //InitVectorDraw();
            InitEyeshot(showRuler, showRulerCoordinates);



            mSelections = new List<CSelection>();
            mLayers = new SortedList<string, CLayer>();
            mBlocks = new SortedList<int, Block>();
            mPhoto = new CPhoto();
            mText = new SortedList<int, CText>();
            _osnapModes = new List<SnapMode>();

            mTimer = new Timer();
            mTimer.Tick += mTimer_Tick;
            mTimer.Interval = 500;

            _regenTimer = new Timer();
            _regenTimer.Interval = 500;
            _regenTimer.Tick += new EventHandler(_regenTimer_Tick);

        }

        void _regenTimer_Tick(object sender, EventArgs e)
        {
            _regenTimer.Stop();

			_viewportLayout.Entities.RegenAllCurved(CURVES_CHORDAL_ERROR);
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields

        protected readonly double TOLERANCE = 1e-6;

        protected SortedList<int, Block> mBlocks;
        protected List<CSelection> mSelections;							// array elementi selezionati
        protected SortedList<string, CLayer> mLayers;					// array dei layers presenti
        protected CPhoto mPhoto;
        protected SortedList<int, CText> mText;					        // array dei text presenti
        protected bool move = false;
        protected bool rotate = false;
        protected bool dragdrop = false;
        protected bool regen = false;
        protected int indexBlock = -1;
        protected int indexText = -1;
        protected Point3D _mousePlanePosition;
        protected System.Drawing.Point _mousePosition;


        protected Point _mouseWorldPosition;

        protected int regionColorAlphaLevel = 100;

        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        #region >-------------- >----- Environment

        public GraphicCursorMode CursorMode
        {
            get { return _viewportLayout.CursorMode; }
            set { _viewportLayout.CursorMode = value; }
        }

        public int CursorCrossSize
        {
            get { return _viewportLayout.CursorCrossSize; }
            set { _viewportLayout.CursorCrossSize = value; }
        }

        /// <summary>
        /// Indica se il componente è bloccato o no
        /// </summary>
        public bool Locked
        {
            get { return _viewportLayout.ShowLocker; }
            set
            {
                _viewportLayout.ShowLocker = value;
                SetLockedStatus(value);
            }
        }

        private void SetLockedStatus(bool locked)
        {
            //if (_backgroundSettings == null)
            //{
            //    _backgroundSettings = (BackgroundSettings)_viewportLayout.Background.Clone();
            //}

            if (locked)
            {
                _viewportLayout.Viewports[0].Background.TopColor = RenderContextUtility.ConvertColor(Color.LightGray);
                _viewportLayout.Viewports[0].Background.StyleMode = backgroundStyleType.Solid;
            }
            else
            {
                _viewportLayout.Viewports[0].Background = (BackgroundSettings)_defaultBackgroundSettings.Clone();
            }

            DragDrop = !locked;

            _viewportLayout.Invalidate();
        }

        /// <summary>
        /// Color used for axis labels
        /// </summary>
        public System.Windows.Media.Brush AxisLabelsColor
        {
            get { return _viewportLayout.Viewports[0].CoordinateSystemIcon.LabelColor; }
            set { _viewportLayout.Viewports[0].CoordinateSystemIcon.LabelColor = value; }
        }

        /// <summary>
        /// Color used for rubberband line
        /// </summary>
        public Color RubberbandColor
        {
            get { return _viewportLayout.MoveRotateRubberbandLine.DrawColor; }
            set { _viewportLayout.MoveRotateRubberbandLine.DrawColor = value; }
        }

        /// <summary>
        /// Color used for snap symbols
        /// </summary>
        public Color SnapSymbolColor
        {
            get { return _viewportLayout.SnapSymbolColor; }
            set { _viewportLayout.SnapSymbolColor = value; }
        }

        /// <summary>
        /// Draw current world coordinates besides cursor
        /// </summary>
        public bool DrawMouseCoordinates
        {
            get { return _viewportLayout.DisplayMouseCoordinates; }
            set { _viewportLayout.DisplayMouseCoordinates = value; }
        }

        public string MouseTooltip
        {
            get { return _viewportLayout.MouseTooltip; }
            set { _viewportLayout.MouseTooltip = value; }
        }

        public Color CursorTipsColor
        {
            get { return _viewportLayout.CursorTipsColor; }
            set
            {
                _viewportLayout.CursorTipsColor = value;
                _viewportLayout.UpdateViewportLayout();
            }
        }


        /// <summary>
        /// Draw action tooltip besides cursor
        /// </summary>
        public bool DrawMouseTooltip
        {
            get { return _viewportLayout.DisplayMouseTooltip; }
            set { _viewportLayout.DisplayMouseTooltip = value; }
        }

        /// <summary>
        /// Auto-focusing on mouse enter
        /// </summary>
        public bool ActivateOnEnter
        {
            get { return _viewportLayout.ActivateOnEnter; }
            set { _viewportLayout.ActivateOnEnter = value; }
        }


        #endregion

        #region >-------------- >----- Geometries

        /// <summary>
        /// Imposta o ritorna l'offset x nella finestra sorgente del blocco di sta facendo il drag and drop
        /// </summary>
        public double XDragDrop
        {
            set { _xDragDrop = value; }
            get { return _xDragDrop; }
        }

        /// <summary>
        /// Imposta o ritorna l'offset y nella finestra sorgente del blocco di sta facendo il drag&drop
        /// </summary>
        public double YDragDrop
        {
            set { _yDragDrop = value; }
            get { return _yDragDrop; }
        }

        /// <summary>
        /// Ritorna il punto di Snap
        /// </summary>
        public CSnapPoint SnapPoint
        {
            get
            {
                if (_lastSnapPoint == null)
                    return null;

                CSnapPoint snapPoint = new CSnapPoint(_lastSnapPoint.X, _lastSnapPoint.Y, SnapModeInverseMap[_lastSnapPoint.Type]);

                return snapPoint;
            }
        }

        /// <summary>
        /// Get/Set punto centrale della vista corrente del disegno
        /// Sostituito da SaveCurrentView + RestoreLastSavedView
        /// </summary>
        [Obsolete]
        public Point ViewCenter
        {
            get
            {
                Camera current = _viewportLayout.Viewports[0].Camera;
                var currentLocation = current.Location;
                return new Point(currentLocation.X, currentLocation.Y);
            }
            set
            {
                
                Camera current = _viewportLayout.Viewports[0].Camera;
                var currentLocation = current.Location;

                current.Move(value.X - currentLocation.X, value.Y - currentLocation.Y, 0);
                current.UpdateLocation();
                Redraw();
            }

        }

        /// <summary>
        /// Get/Set dimensione dell'altezza della vista corrente del disegno
        /// Sostituito da SaveCurrentView + RestoreLastSavedView
        /// </summary>
        [Obsolete]
        public double ViewSize
        {
            get
            {
                double viewHeight = 0;
                try
                {
                    Point3D topLeftOnPlane, bottomLeftOnPlane;
                    System.Drawing.Point topLeft = new System.Drawing.Point(0, 0);
                    System.Drawing.Point bottomLeft = new System.Drawing.Point(0,
                        _viewportLayout.Viewports[0].Size.Height);
                    _viewportLayout.ScreenToPlane(topLeft, Plane.XY, out topLeftOnPlane);
                    _viewportLayout.ScreenToPlane(bottomLeft, Plane.XY, out bottomLeftOnPlane);

                    viewHeight = Point3D.Distance(topLeftOnPlane, bottomLeftOnPlane);
                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("CBasicPaint Get ViewSize error ", ex);
                }
                return viewHeight;

            }
            set
            {
                try
                {
                    double heightRatio = value/ViewSize;
                    double currentPixelHeight = _viewportLayout.Viewports[0].Size.Height;
                    double currentPixelWidth = _viewportLayout.Viewports[0].Size.Width;
                    double newPixelHeight = currentPixelHeight*heightRatio;
                    double newPixelWidth = currentPixelWidth*heightRatio;
                    System.Drawing.Point bottomLeft =
                        new System.Drawing.Point((int) ((currentPixelWidth - newPixelWidth)/2),
                            (int) ((currentPixelHeight - newPixelHeight)/2));
                    System.Drawing.Point topRight = new System.Drawing.Point((int) (currentPixelWidth - bottomLeft.X),
                        (int) (currentPixelHeight - bottomLeft.Y));

                    _viewportLayout.ZoomWindow(bottomLeft, topRight);
                    Redraw();
                }

                catch (Exception ex)
                {
                    TraceLog.WriteLine("CBasicPaint Set ViewSize error ", ex);
                }

            }
        }

        #endregion

        #region >-------------- >----- Logics & Interaction

        /// <summary>
        /// Ritorna la lista dei blocchi correntemente in uso
        /// </summary>
        public SortedList<int, Block> Blocks
        {
            get { return mBlocks; }
        }

        /// <summary>
        /// Ritorna la lista delle selezioni correntemente in uso 
        /// </summary>
        public List<CSelection> Selections
        {
            get { return mSelections; }
        }

        /// <summary>
        /// Ritorna la lista dei layers in uso 
        /// </summary>
        public SortedList<string, CLayer> Layers
        {
            get { return mLayers; }
        }

        /// <summary>
        /// Ritorna la foto caricata
        /// </summary>
        public CPhoto Photo
        {
            set { mPhoto = value; }
            get { return mPhoto; }
        }

        /// <summary>
        /// Ritorna la lista dei testi inseriti 
        /// </summary>
        public SortedList<int, CText> Texts
        {
            get { return mText; }
        }

        /// <summary>
        /// flag che indica se l'azione Move è attiva 
        /// </summary>
        public bool Move
        {
            get { return move; }
            set
            { 
                move = value;
                currentAction = Action.DEFAULT;

                _viewportLayout.MoveRotateRubberbandLine.DrawEntity = false;
                Redraw();
            }
        }

        /// <summary>
        /// flag che indica se l'azione Rotate  attiva 
        /// </summary>
        public bool Rotate
        {
            get { return rotate; }
            set
            { 
                rotate = value;
                currentAction = Action.DEFAULT;

                _viewportLayout.MoveRotateRubberbandLine.DrawEntity = false;
                Redraw();
            }
        }

        /// <summary>
        /// flag che indica se l'azione Drag&Drop è attiva
        /// </summary>
        public bool DragDrop
        {
            set
            {
                dragdrop = value;
            }
            get
            {
                return dragdrop;
            }
        }

        /// <summary>
        /// Imposta o ritorna il blocco di cui si sta facendo il drag and drop
        /// </summary>
        public Block BlockDragDrop
        {
            set
            {
                _blockDragDrop = value;
                //if (_blockDragDrop == null)
                //{
                //    if (DragEntityList != null)
                //        DragEntityList.Clear();
                //}
                //CreateDragDropBlockEntities(_blockDragDrop);
            }
            get { return _blockDragDrop; }
        }

        private void CreateDragDropBlockEntities(Block block)
        {
            DragEntityList.Clear();
            if (block == null)
            {
                return;
            }
            EyeSelection selTmp = null;
            selTmp = SelectEntities(block.Id, -1, -1, -1);

            if (selTmp != null && selTmp.Count != 0)
            {
                if (block.AllowDragDrop)
                {
                    DragEntityList = new List<Entity>(selTmp);
                }
            }
        }

        /// <summary>
        /// Set Ortho cursor movement
        /// </summary>
        public bool OrthoMode
        {
            get { return _viewportLayout.OrthoMode; }
            set
            {
                _viewportLayout.OrthoMode = value;
                _viewportLayout.OrthoModeStarted = false;
                //_viewportLayout.OrthoMode = false;
            }
        }

        /// <summary>
        /// Riordina le entità in base al layer prima del rendering video
        /// </summary>
        public EntitiesSortByLayer SortEntities
        {
            get { return _viewportLayout.SortEntities; }
            set { _viewportLayout.SortEntities = value; }
        }

        protected List<Entity> DragEntityList
        {
            get { return _viewportLayout.DragEntityList; }
            set { _viewportLayout.DragEntityList = value; }
        }

        public bool ApplyLayerZShift
        {
            get { return _applyLayerZShift; }
            set { _applyLayerZShift = value; }
        }

        public Color RegionHilightColor
        {
            get
            {
                return ColorRegion.RegionHilightColor;
            }
            set { ColorRegion.RegionHilightColor = value; }
        }

        public bool RegionHilightOnlyBorder
        {
            get
            {
                return ColorRegion.HilightOnlyBorder;
            }
            set { ColorRegion.HilightOnlyBorder = value; }
        }

        public UM MeasureUnit
        {
            get
            {
                return _viewportLayout.MeasureUnit;
            }
            set { _viewportLayout.MeasureUnit = value; }
        }

        public Breton2DViewer Viewer
        {
            get { return _viewer; }
        }

        public static bool UseAntialiasingText
        {
            get { return _useAntialiasingText; }
            set { _useAntialiasingText = value; }
        }

        /// <summary>
        /// Angolo di rotazione in gradi della vista Top (piano XY)
        /// </summary>
        public double ViewTopRotationAngle
        {
            get { return _viewportLayout.ViewTopRotationAngle; }
            set
            {
                _viewportLayout.ViewTopRotationAngle = value;
            }
        }

        public bool SelectionEnabled
        {
            get { return _selectionEnabled; }
            set { _selectionEnabled = value; }
        }

        #endregion

        #region S >-------------- >----- Misc


        #endregion



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        #region >-------------- >----- Graphics & Blocks operations

        /// <summary>
        /// Aggiunge la foto
        /// </summary>
        public void LoadPhoto()
        {
            LoadPhoto(null, 0, 0);
        }
        /// <summary>
        /// Aggiunge la foto
        /// </summary>
        /// <param name="block">Blocco di riferimento</param>
        /// <param name="x">Coordinata X di inserimento</param>
        /// <param name="y">Coordinata Y di inserimento</param>
        public void LoadPhoto(Block block, double x, double y)
        {
            //return;
            try
            {
                CPhoto photo;

                if (block != null && block.Photo != null)
                    photo = block.Photo;
                else
                    photo = mPhoto;

                if (photo == null) return;

                if (File.Exists(photo.Path))
                {
                    Bitmap cloneImg = null;
                    using (var image = Image.FromFile(photo.Path))
                    {
                        cloneImg = new Bitmap(image, image.Width, image.Height);
                        image.Dispose();
                    }
                    int layerIndex = 0;


                    Point3D coord = new Point3D(x, y, 0);

                    Picture img = new Picture(Plane.XY, coord, photo.Width, photo.Height, cloneImg);

                    CLayer lay = GetLayer(photo.Layer);
                    img.Translate(0, 0, _cLayerTranslactionFactor * lay.ZLevel);

                    img.Lighted = false;

                    Layer layer = FindEyeLayer(photo.Layer);
                    if (layer != null)
                        layerIndex = _viewportLayout.Layers.IndexOf(layer);

                    img.EntityData = photo.Name;

                    //img.Translate(0, 0, -_cLayerTranslactionFactor);


                    _viewportLayout.Entities.Add(img, layerIndex);

                    _viewportLayout.Invalidate();

                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".LoadPhoto error: ", ex);
                if (ex is OutOfMemoryException)
                {
                    OnOutOfMemoryRaised();
                }

            }

        }

        /// <summary>
        /// Forza il ridisegno della finestra
        /// </summary>
        public void Repaint()
        {
            try
            {
                //ciclo per tutti i blocchi
                foreach (KeyValuePair<int, Block> b in mBlocks)
                {					
                    DeleteEntities(b.Key, false);

                    //ciclo per tutti i path dei blocchi
                    for (int iPath = 0; iPath < b.Value.GetPaths().Values.Count; iPath++)
                    {
                        Path p = b.Value.GetPaths().Values[iPath];
                        DrawPath(p, b.Value.Id, iPath, false);
                    }
                }

                Redraw(true);
            }
            catch (Exception ex) 
            {
                TraceLog.WriteLine("CBasicPaint.Repaint ", ex); 
            }
        }

        /// <summary>
        /// Disegna un singolo blocco
        /// </summary>
        /// <param name="block_no">id del blocco da disegnare</param>
        /// <param name="redraw">Ridisegno immediato</param>
        public void Repaint(int block_no, bool redraw = true)
        {
            if (mBlocks.ContainsKey(block_no))
            {
                Block b = mBlocks[block_no];

                Repaint(b, redraw);
            }
        }

        /// <summary>
        /// Disegna un singolo blocco
        /// </summary>
        /// <param name="block_no">id del blocco da disegnare</param>
        /// <param name="redraw">Ridisegno immediato</param>
        public void Repaint(Block b, bool redraw = true)
        {
            try
            {
                DeleteEntities(b.Id, false);

                //ciclo per tutti i path del blocco
                for (int iPath = 0; iPath < b.GetPaths().Values.Count; iPath++)
                {
                    Path p = b.GetPaths().Values[iPath];
                    DrawPath(p, b.Id, b.GetPaths().Keys[iPath], false);
                }

                if (redraw)
                    Redraw(true);
            }
            catch (Exception ex) 
            {
                TraceLog.WriteLine("CBasicPaint.Repaint ", ex); 
            }
        }
		
        /// <summary>
        /// Disegna un singolo path
        /// </summary>
        /// <param name="block_no">id del blocco da disegnare</param>
        /// <param name="path_no">path_no</param>
        /// <param name="redraw">Ridisegno immediato</param>
        public void Repaint(int block_no, int path_no, bool redraw = true)
        {

            try
            {
                Block b = mBlocks[block_no];

                DeleteEntities(block_no, path_no, false);

                //cerco il path del blocco da disegnare
                Path p = b.GetPath(path_no);

                DrawPath(p, b.Id, path_no, false);

                if (redraw)
                    Redraw();
            }
            catch (Exception ex) 
            { 
                TraceLog.WriteLine("CBasicPaint.Repaint ", ex); 
            }
        }

        /// <summary>
        /// Crea un blocco vuoto, lo aggiunge alla lista interna e ne ritorna il riferimento
        /// </summary>
        /// <returns>blocco creato</returns>
        public int AddBlock()
        {

            try
            {

                if (mBlocks == null)
                    mBlocks = new SortedList<int, Block>();

                Block b = new Block();
                indexBlock += 1;
                b.Id = indexBlock;
                mBlocks.Add(indexBlock, b);

                return indexBlock;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint.AddBlock: ", ex);
                return -1;
            }

        }

        /// <summary>
        /// Ritorna il blocco con l'id specificato
        /// </summary>
        /// <param name="block_no">id del blocco da cercare</param>
        /// <returns>blocco trovato</returns>
        public Block GetBlock(int block_no)
        {

            try
            {

                if (!mBlocks.ContainsKey(block_no))
                    return null;

                return mBlocks[block_no];

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("GetBlock ERROR: ", ex);
                return null;
            }

        }

        /// <summary>
        /// Ritorna il blocco con il codice specificato
        /// </summary>
        /// <param name="code">codice del blocco da cercare</param>
        /// <returns>blocco trovato</returns>
        public Block GetBlock(string code)
        {
            try
            {
                foreach (KeyValuePair<int, Block> b in Blocks)
                {
                    if (b.Value.Code.Trim() == code.Trim())
                        return b.Value;
                }

                return null;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }
        }

        /// <summary>
        /// Rimuove un blocco dalla lista
        /// </summary>
        /// <param name="b">blocco da rimuovere</param>
        /// <returns>true: il blocco è stato rimosso correttamente</returns>
        public bool RemoveBlock(int block_no, bool redraw = true)
        {

            try
            {

                if (mBlocks == null)
                    return false;

                //rimuovo il blocco dalla lista
                if (!mBlocks.Remove(block_no))
                    return false;
                
                //rimuovo il blocco dalla selezione. verificare come ridisegnare
                UnSelect(block_no);

                DeleteEntities(block_no, redraw);

                return true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }

        }

        /// <summary>
        /// Associa un percorso a un blocco esistente nella lista interna
        /// </summary>
        /// <param name="block_no">blocco a cui aggiungere il path</param>
        /// <param name="path">path da aggiungere</param>
        /// <returns>-1 il path non è stato inserito, altrimenti restituisce l'indice nella lista</returns>
        public int AddPath(int block_no, Path path)
        {
            try
            {
                if (mBlocks == null)
                    return -1;

                if (!mBlocks.ContainsKey(block_no))
                    return -1;

                return ((Block)mBlocks[block_no]).AddPath(path);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return -1;
            }
        }

        /// <summary>
        /// Rimuove un path da un blocco
        /// </summary>
        /// <param name="block_no">blocco da cui rimuovere il path</param>
        /// <param name="path_no">path da rimuovere</param>
        /// <returns>true: il path è stato rimosso correttamente</returns>
        public bool RemovePath(int block_no, int path_no, bool redraw = false)
        {

            try
            {

                if (mBlocks == null)
                    return false;
                if (!mBlocks.ContainsKey(block_no))
                    return false;

                DeleteEntities(block_no, path_no, redraw, true);

                return ((Block)mBlocks[block_no]).RemovePath(path_no);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }

        }

        ///***************************************************************************
        /// <summary>
        /// Aggiunge un nuovo layer
        /// </summary>
        /// <param name="layer">layer da aggiungere</param>
        /// <returns>
        ///		true	layer aggiunto
        ///		false	errore
        ///	</returns>
        ///***************************************************************************
        public bool AddLayer(CLayer layer)
        {
            try
            {
                if (mLayers.ContainsKey(layer.Name))
                    throw (new ApplicationException("Layer [" + layer.Name + "] already exists!"));

                mLayers.Add(layer.Name, layer);
                if (AddEyeLayer(layer) == null)
                    throw (new ApplicationException("Error adding layer [" + layer.Name + "] in Eyeshot!"));

                return true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".AddLayer: ", ex);
                return false;
            }
        }


        /// <summary>
        /// Modifica un layer esistente
        /// </summary>
        /// <param name="layer">layer modificato</param>
        /// <returns>
        ///		true	layer modificato
        ///		false	errore
        /// </returns>
        public bool SetLayer(CLayer layer)
        {
            try
            {
                if (!mLayers.ContainsKey(layer.Name))
                    throw (new ApplicationException("Layer [" + layer.Name + "] not exists!"));

                if (SetEyeLayer(layer) == null)
                    throw (new ApplicationException("Error update layer [" + layer.Name + "] in VectorDraw!"));

                return true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".SetLayer: ", ex);
                return false;
            }
        }

        ///***************************************************************************
        /// <summary>
        /// Ritorna il layer richiesto
        /// </summary>
        /// <param name="layerName">nome del layer</param>
        /// <returns>Layer se esiste, null altrimenti</returns>
        ///***************************************************************************
        public CLayer GetLayer(string layerName)
        {
            try
            {
                if (!mLayers.ContainsKey(layerName))
                    return null;

                return mLayers[layerName];
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".GetLayer: ", ex);
                return null;
            }
        }

        ///***************************************************************************
        /// <summary>
        /// Rimuove il layer indicato
        /// </summary>
        /// <param name="layerName">nome del layer</param>
        /// <returns>
        ///			true	layer cancellato
        ///			false	errore
        ///	</returns>
        ///***************************************************************************
        public bool RemoveLayer(string layerName)
        {
            try
            {
                //rimuovo il blocco dalla lista
                if (!mLayers.Remove(layerName))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".RemoveLayer: ", ex);
                return false;
            }
        }

        /// <summary>
        /// Definisce le caratteristiche del layer indicato
        /// </summary>
        /// <param name="layer">layer</param>
        /// <param name="visible">true: il layer è visibile</param>
        /// <param name="unlocked">true: il layer è sbloccato</param>
        /// <param name="redraw"></param>
        public void LayerMode(CLayer layer, bool visible, bool unlocked, bool redraw = true)
        {
            // ricerca il layer e se esiste nella lista corrente, imposta le nuove proprieta'
            if (mLayers.ContainsKey(layer.Name))
            {
                if (visible)
                {
                    EnableEyeLayer(layer.Name, redraw);
                }
                else
                {
                    DisableEyeLayer(layer.Name);
                }
                
                layer.Visible = visible;
                layer.Unlocked = unlocked;
            }
        }

        public bool Select(int block_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, -1, -1, -1, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, -1, -1, -1, type);
        }
        public bool Select(int block_no, int path_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, -1, -1, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, int path_no, HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, -1, -1, type);
        }
        public bool Select(int block_no, int path_no, int elem_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, -1, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, int path_no, int elem_no,  HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, -1, type);
        }
        public bool Select(int block_no, int path_no, int elem_no, int vertex_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, vertex_no, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, int path_no, int elem_no, int vertex_no, HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, vertex_no, type);
        }

        public bool UnSelect()
        {
            // cancella la lista degli elementi selezionati.
            return _Unselect(-1, -1, -1, -1);
        }
        public bool UnSelect(int block_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, -1, -1, -1);
        }
        public bool UnSelect(int block_no, int path_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, path_no, -1, -1);
        }
        public bool UnSelect(int block_no, int path_no, int elem_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, path_no, elem_no, -1);
        }
        public bool UnSelect(int block_no, int path_no, int elem_no, int vertex_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, path_no, elem_no, vertex_no);
        }

        public void StopActionMove()
        {
            List<int> lb = new List<int>();
            for (int i = 0; i < mSelections.Count; i++)
                lb.Add(mSelections[i].SelectedBlock);

            //ripristino le matrici iniziali
            for (int i = 0; i < lb.Count; i++)
            {
                Block b = mBlocks[lb[i]];
                RTMatrix m = _lastMatrix[i];
                double offX, offY, angolo;
                m.GetRotoTransl(out offX, out offY, out angolo);
                b.SetRotoTransl(offX, offY, angolo);
                b = mBlocks[lb[i]];
                Repaint(b);
                currentAction = Action.DEFAULT;

                _viewportLayout.MoveRotateRubberbandLine.DrawEntity = false;

            }
        }

        public void StopActionRotate()
        {
            List<int> lb = new List<int>();
            for (int i = 0; i < mSelections.Count; i++)
                lb.Add(mSelections[i].SelectedBlock);

            //ripristino le matrici iniziali
            for (int i = 0; i < lb.Count; i++)
            {
                Block b = mBlocks[lb[i]];
                RTMatrix m = _lastMatrix[i];
                double offX, offY, angolo;
                m.GetRotoTransl(out offX, out offY, out angolo);
                b.SetRotoTransl(offX, offY, angolo);
                b = mBlocks[lb[i]];
                Repaint(b);
                currentAction = Action.DEFAULT;

                _viewportLayout.MoveRotateRubberbandLine.DrawEntity = false;

                Redraw();
            }
        }

        /// <summary>
        /// Movimento manuale di uno o più blocchi
        /// </summary>
        /// <param name="blocks">Blocchi da muovere</param>
        /// <param name="deltaX">Movimento in X</param>
        /// <param name="deltaY">Movimento in Y</param>
        /// <returns></returns>
        public bool ManualMove(List<int> blocks, double deltaX, double deltaY)
        {			
            try
            {
				_selActive = new EyeSelection("Blocks");
				EyeSelection selTmp = null;

                foreach (int idb in blocks)
                {
                    Block bl = mBlocks[idb];

                    // De Conti 03/11/2016
                    if (!bl.AllowMove) continue;

                    bl.MatrixRT.Save();
                    bl.RotoTranslRel(deltaX, deltaY, 0d, 0d, 0d);

					selTmp = SelectEntities(idb, -1, -1, -1);

					if (selTmp != null)
						for (int i = 0; i < selTmp.Count; i++)
							_selActive.AddItem(selTmp[i], true);
                }
				
                // Essendo il movimento manuale imposta che non è stato premuto nessun tasto del mouse
                basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

                OnMoveEvent(basicPaintEventArgs, blocks, null, null, null);

                if (!basicPaintEventArgs.Accepted)
                {
                    foreach (int idb in blocks)
                    {
                        Block bl = mBlocks[idb];

                        // De Conti 03/11/2016
                        if (!bl.AllowMove) continue;

                        bl.MatrixRT.Restore();
                    }
                }
                else
                {
                    _selActive.Translate(deltaX, deltaY, 0);
                    _viewportLayout.Entities.Regen();
                    _viewportLayout.Invalidate();
                }

                // Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
                OnEndMoveEvent(basicPaintEventArgs, blocks, null, null, null);

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint.ManualMove ", ex);
                return false;
            }
        }

        public bool ManualMove(int block, double deltaX, double deltaY)
        {
            List<int> blocks = new List<int>();
            blocks.Add(block);

            if (!ManualMove(blocks, deltaX, deltaY))
                return false;

            return true;
        }

        public virtual void ClearAll()
        {
			try
			{
				_userPoint = new Point();
				_lastOkPoint = new Point();
				_lastMatrix = new List<RTMatrix>();
				_xDragDrop = 0;
				_yDragDrop = 0;
				_startZoomWindowPoint = null;
				_zoomWindow = false;
				_rotationReferenceSegment = null;
				_canStartRotate = false;
				currentAction = Action.DEFAULT;



				this._activeLayerIndex = 0;
				_blockDragDrop = null;
				_canStartRotate = false;

				if (_lastMatrix != null)
					_lastMatrix.Clear();

				_lastOkPoint = null;
				_lastOkPoint = null;
				_mousePlanePosition = null;
				_mouseWorldPosition = null;

                //if (_selActive != null)
                //{
                //    _selActive.ClearSelection();
                //}
				_selActive = new EyeSelection();

				_viewportLayout.OrthoModeStarted = false;

				_viewportLayout.ShowDrag = false;
				_viewportLayout.ShowLocker = false;
				_viewportLayout.Locked = false;


				if (_viewportLayout.Selections != null)
				{
					foreach (var selection in _viewportLayout.Selections)
					{
						selection.ClearSelection();
					}
					_viewportLayout.Selections.Clear();
				}

				_viewportLayout.ClearTemporaryEntities();
				if (_viewportLayout.DragEntityList != null)
				{
					_viewportLayout.DragEntityList.Clear();
				}


				_snapLayers = new List<string>();
				_userPoint = null;

				_savedCamera = null;

				mBlocks = null;
				mSelections = null;							// array elementi selezionati
				mLayers = null;					// array dei layers presenti
				mPhoto = null;
				mText = null;					        // array dei text presenti
				move = false;
				rotate = false;
				dragdrop = false;
				regen = false;
				indexBlock = -1;
				indexText = -1;

				mSelections = new List<CSelection>();
				mLayers = new SortedList<string, CLayer>();
				mBlocks = new SortedList<int, Block>();
				mPhoto = new CPhoto();
				mText = new SortedList<int, CText>();
				_osnapModes = new List<SnapMode>();

				////foreach (var ent in _viewportLayout.Entities)
				////{
				////    if (ent is Picture)
				////    {
				////        (ent as Picture).Image.Dispose();
				////    }
				////    ent.Dispose();
				////}

				////_viewportLayout.Entities.Clear();
				////_viewportLayout.Materials.Clear();
				////_viewportLayout.Layers.Clear();

				////_viewportLayout.Entities.Regen();

				_viewportLayout.Clear();

				if (_viewportLayout.Entities != null)
					_viewportLayout.Entities.UpdateBoundingBox();

				//_viewportLayout.ZoomFit();

				_viewportLayout.Invalidate();
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("CBasicPaint.ClearAll", ex);
			}
        }


        /// <summary>
        /// Rotazione manuale di uno o più elementi
        /// </summary>
        /// <param name="blocks">Blocchi da ruotare</param>
        /// <param name="angle">Angolo di rotazione</param>
        /// <returns></returns>
        public bool ManualRotate(List<int> blocks, double pernoX, double pernoY, double angle)
        {
            try
            {
				_selActive = new EyeSelection("Blocks");
				EyeSelection selTmp = null;
				
                foreach (int idb in blocks)
                {
                    Block bl = mBlocks[idb];

                    //De Conti 03/11/2016
                    if (!bl.AllowRotate) continue;

                    bl.MatrixRT.Save();
                    bl.RotoTranslRel(0d, 0d, pernoX, pernoY, angle);

					selTmp = SelectEntities(idb, -1, -1, -1);

					if (selTmp != null)
						for (int i = 0; i < selTmp.Count; i++)
							_selActive.AddItem(selTmp[i], true);
                }

                // Essendo il movimento manuale imposta che non è stato premuto nessun tasto del mouse
                basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

                OnRotateEvent(basicPaintEventArgs, blocks, null, null, null);

                if (!basicPaintEventArgs.Accepted)
                {
                    foreach (int idb in blocks)
                    {
                        Block bl = mBlocks[idb];

                        // De Conti 03/11/2016
                        if (!bl.AllowRotate) continue;

                        bl.MatrixRT.Restore();
                    }
                }
                else
                {

                    _selActive.Rotate(angle, Vector3D.AxisZ, new Point3D(pernoX, pernoY));
					_viewportLayout.Entities.Regen();
					_viewportLayout.Invalidate();
                }

                // Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
                OnEndRotateEvent(basicPaintEventArgs, blocks, null, null, null);

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint.ManualRotate ", ex);
                return false;
            }
        }

        public bool ManualRotate(int block, double pernoX, double pernoY, double angle)
        {
            List<int> blocks = new List<int>();
            blocks.Add(block);

            if (!ManualRotate(blocks, pernoX, pernoY, angle))
                return false;

            return true;
        }

        /// <summary>
        /// Posizionamento manuale di uno o più elementi
        /// </summary>
        /// <param name="blocks">Lista di blocchi da posizionare</param>
        /// <param name="matrixes">Lista di matrici per i blocchi</param>
        /// <returns></returns>
        public bool ManualRotoTrasl(List<int> blocks, List<RTMatrix> matrixes)
        {
            int i = 0;

            foreach (int b in blocks)
            {
                RTMatrix matrix = matrixes[i];
                if (!ManualRotoTrasl(b, matrix))
                    return false;
                i++;
            }

            return true;
        }

        public bool ManualRotoTrasl(int block, RTMatrix matrix)
        {
            double x, y, a;

            try
            {
                _selActive = SelectEntities(block, -1, -1, -1);

                matrix.GetRotoTransl(out x, out y, out a);

                foreach (CSelection cs in mSelections)
                {
                    Block bl = mBlocks[block];
                    //bl.MatrixRT.Save();
                    bl.SetRotoTransl(x, y, a);
                }

                _selActive.Translate(x, y, 0);
                _selActive.Rotate(a, Vector3D.AxisZ);
				_viewportLayout.Entities.Regen();
                _viewportLayout.Invalidate();

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint.ManualRotoTrasl ", ex);
                return false;
            }
        }

        #endregion

        #region >-------------- >----- Drawing operations

        /// <summary>
        /// Aggiunge un testo
        /// Ad ogni \n interno al testo viene creata una nuova riga
        /// </summary>
        /// <param name="text"></param>
        /// <param name="lineSpacing">Distanza tra righe in rapporto all'altezza riga</param>
        /// <param name="data">Dati addizionali</param>
        /// <param name="containingPath">Percorso all'interno del quale posizionare il testo</param>
        /// <param name="fitInsidePath">Mantiene in testo all'interno del Path, eventualmente ruotandolo di 90° antiorario</param>
        /// <returns></returns>
        public int AddText(CText text, double lineSpacing = 1.25, object data = null, Path containingPath = null, bool fitInsidePath = false)
        {
            try
            {

                if (mText == null)
                    mText = new SortedList<int,CText>();

                indexText += 1;
                mText.Add(indexText, text);

                if (AddEyeText(indexText, text, lineSpacing, data, containingPath, fitInsidePath) == null)
                    throw (new ApplicationException("Error adding text Eyeshot!"));

                return indexText;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".AddText: ", ex);
                return -1;
            }
        }

        /// <summary>
        /// Aggiunge un testo allineato con la vista Top
        /// Ad ogni \n interno al testo viene creata una nuova riga
        /// </summary>
        /// <param name="text">Testo</param>
        /// <param name="lineSpacing">Distanza tra righe in rapporto all'altezza riga</param>
        /// <param name="data">Dati addizionali</param>
        /// <param name="rotateLeft">Rotazione 90° antioraria</param>
        /// <returns></returns>
        public int AddViewTopText(CText text, double lineSpacing = 1.25, object data = null, bool rotateLeft = false)
        {
            try
            {

                if (mText == null)
                    mText = new SortedList<int, CText>();

                indexText += 1;
                mText.Add(indexText, text);

                if (AddEyeText(indexText, text, lineSpacing, data) == null)
                    throw (new ApplicationException("Error adding text Eyeshot!"));

                return indexText;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".AddText: ", ex);
                return -1;
            }
        }


        public bool AddDimension(DimensionType type, Point p1, Point p2, Point textPosition, 
            double textHeight = 40.0,
            double arrowHeadSize = 20.0,
            int numberOfDecimals = 0,
            Color color = default(Color), 
            bool leftToRightOrientation = true)
        {
            if (color.Equals(default(Color)))
                color = Color.Red;

            Point3D dimStart = new Point3D(p1.X, p1.Y);
            Point3D dimEnd = new Point3D(p2.X, p2.Y);
            Point3D position = new Point3D(textPosition.X, textPosition.Y);

            Plane dimPlane = Plane.XY;

            switch (type)
            {
                case DimensionType.LINEAR:
                    Vector3D axisX = new Vector3D(dimStart,dimEnd);
                    Vector3D axisY = Vector3D.Cross(Vector3D.AxisZ, axisX);

                    dimPlane = new Plane(dimStart, axisX, axisY);

                    if (leftToRightOrientation)
                    {
                        if (dimStart.X > dimEnd.X)
                        {
                            Utility.Swap(ref dimStart, ref dimEnd);
                            dimPlane.Rotate(Math.PI, Vector3D.AxisZ);
                        }
                    }


                    break;

                case DimensionType.ORIZONTAL:
                    dimPlane = new Plane(new Point3D(0, 0, 0), new Point3D(1, 0, 0), new Point3D(0, 1, 0));
                    break;

                case DimensionType.VERTICAL:
                    dimPlane = new Plane(new Point3D(0, 0, 0), new Point3D(0, 1, 0), new Point3D(-1, 0, 0));


                    break;

            }

            if (leftToRightOrientation)
            {
                
            }


            var dimensionEntity = new LinearDim(dimPlane, dimStart, dimEnd, position, textHeight);
            dimensionEntity.ArrowheadSize = arrowHeadSize;
            dimensionEntity.NumberOfDecimals = numberOfDecimals;
            dimensionEntity.ColorMethod = colorMethodType.byEntity;
            dimensionEntity.Color = color;


            _viewportLayout.Entities.Add(dimensionEntity, _viewportLayout.ActiveLayer.Name);

            _viewportLayout.Invalidate();

            //_viewportLayout.DrawingDimension = false;

            return true;
        }

        public void DrawInteractiveDimension(DimensionType type, Point p1, Point p2, double textHeight = 40.0)
        {
            switch (type)
            {
                case DimensionType.LINEAR:
                    _viewportLayout.DimensionMode = DimensionMode.Aligned;
                    break;

                case DimensionType.ORIZONTAL:
                    _viewportLayout.DimensionMode = DimensionMode.Horizontal;
                    break;

                case DimensionType.VERTICAL:
                    _viewportLayout.DimensionMode = DimensionMode.Vertical;
                    break;
            }
            _viewportLayout.DimensionPoint1 = new Point3D(p1.X, p1.Y);
            _viewportLayout.DimensionPoint2 = new Point3D(p2.X, p2.Y);
            _viewportLayout.DimensionTextHeight = textHeight;
            _viewportLayout.DrawingDimension = true;
        }

        public void CancelDrawingInteractiveDimension()
        {
            _viewportLayout.DrawingDimension = false;
        }

        public void AddLine(double xFrom, double yFrom, double xTo, double yTo)
        {
            var v = EyeAddLine(xFrom, yFrom, xTo, yTo);
            _viewportLayout.Invalidate();
        }

        public void AddArc(Point center, double radius, double startAngle, double endAngle, double amplAngle)
        {
            var arc = EyeAddArc(center, radius, startAngle, endAngle, amplAngle);
            if (arc != null)
                _viewportLayout.Invalidate();
        }

        public void AddRectangle(double posX, double posY, double height, double width)
        {
            EyeAddRectangle(posX, posY, height, width);
        }

        /// <summary>
        /// Disegna una linea generica
        /// </summary>
        /// <param name="segment">lato da disegnare</param>
        public void DrawGenericLine(Segment segment)
        {
            DrawGenericLine(segment, Color.White);
        }

        public void DrawGenericLine(Segment segment, Color color)
        {
            DrawGenericLine(segment, color, false);
        }

        public void DrawGenericLine(Segment segment, Color color, bool dottedLine, bool redraw = true)
        {
            _viewportLayout.TemporaryLine.StartPoint = new Point2D(segment.P1.X, segment.P1.Y);
            _viewportLayout.TemporaryLine.EndPoint = new Point2D(segment.P2.X, segment.P2.Y);
            _viewportLayout.TemporaryLine.DrawColor = color;
            _viewportLayout.TemporaryLine.DottedBorder = dottedLine;

            _viewportLayout.TemporaryLine.DrawEntity = true;

            if (redraw)
                _viewportLayout.Invalidate();
        }

        /// <summary>
        /// Nasconde la linea generica
        /// </summary>
        public void DeleteGenericLine(bool redraw = true)
        {
            _viewportLayout.TemporaryLine.DrawEntity = false;
            
            if (redraw)
                _viewportLayout.Invalidate();

        }


        /// <summary>
        /// Disegna arco generico
        /// </summary>
        /// <param name="center">Centro</param>
        /// <param name="startPoint">Punto iniziale</param>
        /// <param name="endPoint">Punto finale</param>
        /// <param name="color">Colore</param>
        /// <param name="dottedLine">Linea tratteggiata</param>
        /// <param name="redraw">Ridisegno immediato</param>
        public void DrawGenericArc(Point center, Point startPoint, Point3D endPoint, Color color, bool dottedLine, bool redraw = true)
        {
            _viewportLayout.TemporaryArc.ArcCenter = new Point2D(center.X, center.Y);
            _viewportLayout.TemporaryArc.StartPoint = new Point2D(startPoint.X, startPoint.Y);
            _viewportLayout.TemporaryArc.EndPoint = new Point2D(endPoint.X, endPoint.Y);
            _viewportLayout.TemporaryArc.DrawColor = color;
            _viewportLayout.TemporaryArc.DottedBorder = dottedLine;

            _viewportLayout.TemporaryArc.DrawEntity = true;

            if (redraw)
                _viewportLayout.Invalidate();
        }

        /// <summary>
        /// Elimina arco generico
        /// </summary>
        /// <param name="redraw">Ridisegno immediato</param>
        public void DeleteGenericArc(bool redraw = true)
        {
            _viewportLayout.TemporaryArc.DrawEntity = false;

            if (redraw)
                _viewportLayout.Invalidate();

        }


        /// <summary>
        /// Disegna un cerchio generico
        /// </summary>
        /// <param name="cx">coordinata x del centro</param>
        /// <param name="cy">coordinata y del centro</param>
        /// <param name="radius">raggio</param>
        public void DrawGenericCircle(double cx, double cy, double radius)
        {
            DrawGenericCircle(cx, cy, radius, false, Color.White);
        }

        public void DrawGenericCircle(double cx, double cy, double radius, bool fill, Color color)
        {
            DrawGenericCircle(cx, cy, radius, fill, color, true, false);
        }


        public void DrawGenericCircle(double cx, double cy, double radius, bool fill, Color color, bool drawBorder, bool dottedBorder)
        {
            _viewportLayout.TemporaryCircle.StartPoint = new Point2D(cx, cy);
            _viewportLayout.TemporaryCircle.Radius = radius;
            _viewportLayout.TemporaryCircle.Fill = fill;
            _viewportLayout.TemporaryCircle.DrawColor = color;
            _viewportLayout.TemporaryCircle.DrawBorder = drawBorder;
            _viewportLayout.TemporaryCircle.DottedBorder = dottedBorder;

            _viewportLayout.TemporaryCircle.DrawEntity = true;

            _viewportLayout.Invalidate();

        }




    /// <summary>
        /// Nasconde il cerchio generico
        /// </summary>
    /// <param name="redraw">Ridisegno immediato</param>
        public void DeleteGenericCircle(bool redraw = true)
        {
            _viewportLayout.TemporaryCircle.DrawEntity = false;
           
            if (redraw)
                _viewportLayout.Invalidate();

        }


        /// <summary>
        /// Disegna un rettangolo generico
        /// </summary>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <param name="fill"></param>
        /// <param name="color"></param>
        public void DrawGenericRectangle(double cx, double cy, double height, double width, bool fill, Color color)
        {
            DrawGenericRectangle(cx, cy, height, width, fill, color, true, false);
        }

        public void DrawGenericRectangle(double cx, double cy, double height, double width, bool fill, Color color, bool drawBorder, bool dottedBorder)
        {
            _viewportLayout.TemporaryRectangle.StartPoint = new Point2D(cx, cy);
            _viewportLayout.TemporaryRectangle.EndPoint = new Point2D(cx + width, cy +height);
            _viewportLayout.TemporaryRectangle.Fill = fill;
            _viewportLayout.TemporaryRectangle.DrawColor = color;
            _viewportLayout.TemporaryRectangle.DrawBorder = drawBorder;
            _viewportLayout.TemporaryRectangle.DottedBorder = dottedBorder;

            _viewportLayout.TemporaryRectangle.DrawEntity = true;

            _viewportLayout.Invalidate();
        }


        public void DrawGenericRectangleWithResizers(double cx, double cy, double height, double width, bool fill, Color color, bool drawBorder, bool dottedBorder)
        {
            _viewportLayout.TemporaryRectangle.StartPoint = new Point2D(cx, cy);
            _viewportLayout.TemporaryRectangle.EndPoint = new Point2D(cx + width, cy + height);
            _viewportLayout.TemporaryRectangle.Fill = fill;
            _viewportLayout.TemporaryRectangle.DrawColor = color;
            _viewportLayout.TemporaryRectangle.DrawBorder = drawBorder;
            _viewportLayout.TemporaryRectangle.DottedBorder = dottedBorder;

            _viewportLayout.TemporaryRectangle.DrawEntity = true;

            var symbols = _viewportLayout.TemporaryResizingSymbols;
            if (symbols.Count != 8)
            {
                symbols.Clear();
                for (int i=0; i<8; i++)
                    symbols.Add(new TemporaryEntity(TemporaryEntityType.Rectangle, new Point2D(), new Point2D())
                        {DrawBorder = true, DrawColor = color, DrawEntity = true, Fill = true});
            }
            UpdateResizingSymbolsLocation(symbols, cx, cy, width, height);


            _viewportLayout.Invalidate();
        }

        private void UpdateResizingSymbolsLocation(List<TemporaryEntity> symbols, double cx, double cy, double width, double height)
        {
            Point center = new Point(cx, cy);

            

            symbols[0].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[0].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);

            center.X += width/2;
            symbols[1].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[1].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);

            center.X += width/2;
            symbols[2].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[2].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);

            center.Y += height/2;
            symbols[3].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[3].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);

            center.Y += height/2;
            symbols[4].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[4].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);

            center.X -= width/2;
            symbols[5].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[5].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);

            center.X -= width/2;
            symbols[6].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[6].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);

            center.Y -= height/2;
            symbols[7].StartPoint = new Point2D(center.X - resizingSymbolHalfSide, center.Y -resizingSymbolHalfSide);
            symbols[7].EndPoint = new Point2D(center.X + resizingSymbolHalfSide, center.Y +resizingSymbolHalfSide);


        }

        public int IsMouseOverResizingSymbol()
        {
            Point current = CursorPosition();
            int index = -1;

            try
            {
                var location = _viewportLayout.MouseLocation;
                Point3D first, second;
                _viewportLayout.ScreenToPlane(location, Plane.XY, out first);
                _viewportLayout.ScreenToPlane(new System.Drawing.Point(location.X + _viewportLayout.RESIZER_SYMBOL_SIZE/2, (int) location.Y), Plane.XY, out second);
                double distance = (second.DistanceTo(first));


                foreach (var symbol in _viewportLayout.TemporaryResizingSymbols)
                {
                    //if (symbol.DrawEntity &&
                    //    (current.X >= symbol.StartPoint.X && current.X <= symbol.EndPoint.X
                    //     && current.Y >= symbol.StartPoint.Y && current.Y <= symbol.EndPoint.Y))
                    if (symbol.DrawEntity &&
                        (Math.Abs(current.X - symbol.OriginPoint.X) <= distance
                         && Math.Abs(current.Y - symbol.OriginPoint.Y) <= distance))
                    {
                        index = _viewportLayout.TemporaryResizingSymbols.IndexOf(symbol);
                        break;
                    }
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("IsMouseOverResizingSymbol error" , ex);
            }

            return index;
        }

        /// <summary>
        /// Nasconde il rettangolo generico
        /// </summary>
        public void DeleteGenericRectangle(bool redraw = true)
        {
            _viewportLayout.TemporaryRectangle.DrawEntity = false;
		    
            if (redraw)
                _viewportLayout.Invalidate();

        }

        /// <summary>
        /// Nasconde il rettangolo generico
        /// </summary>
        public void DeleteGenericRectangleWithResizers(bool redraw = true)
        {
            _viewportLayout.TemporaryRectangle.DrawEntity = false;
            _viewportLayout.TemporaryResizingSymbols.Clear();

            if (redraw)
                _viewportLayout.Invalidate();

        }


        #endregion

        #region >-------------- >----- Utilities & Viewer actions

        /// <summary>
        /// Salva corrente inquadratura camera 
        /// </summary>
        public void SaveCurrentView()
        {
            if (_viewportLayout.Viewports[0] != null && _viewportLayout.Viewports[0].Camera != null)
            {
                _savedCamera = (Camera)_viewportLayout.Viewports[0].Camera.Clone();
            }
        }

        /// <summary>
        /// Ripristina inquadratura camera salvata
        /// </summary>
        public void RestoreLastSavedView()
        {
            if (_savedCamera != null && _viewportLayout.Viewports[0] != null)
            {
                _viewportLayout.Viewports[0].Camera = (Camera) _savedCamera.Clone();
                _viewportLayout.Invalidate();
            }

        }

        /// <summary>
        /// Imposta cursore default
        /// </summary>
        public void SetDefaultCursor()
        {
            _viewportLayout.SetDefaultCursor();
        }


        public object GetDxfFileEntities(string dxfFilepath)
        {
            bool retVal = false;

            if (!File.Exists(dxfFilepath))
                return null;

            EntityList entities = new EntityList();

            var ra = new ReadAutodesk(dxfFilepath);
            try
            {
                
                ra.DoWork();


                entities.AddRange((Entity[])ra.Entities.Clone());


                retVal = true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint GetDxfFileEntities error ", ex);

            }

            if (retVal)
                return entities;

            return null;
        }

        //public object InsertDxfFile(string dxfFilepath, double xPosition, double yPosition, RTMatrix transformMatrix, string layerName, object data)
        //{
        //    bool retVal = false;

        //    if (!File.Exists(dxfFilepath))
        //        return null;

        //    EntityList entities = new EntityList();

        //    try
        //    {
        //        var ra = new ReadAutodesk(dxfFilepath);
        //        ra.DoWork();


        //        entities.AddRange((Entity[])ra.Entities.Clone());

        //        foreach (var ent in entities)
        //        {
        //            ent.ColorMethod = colorMethodType.byLayer;
        //        }
                
        //        Layer layer = FindEyeLayer(layerName);

        //        int layerIndex = (layer != null) ? _viewportLayout.Layers.IndexOf(layer) : 0;

        //        RTMatrix localMatrix = new RTMatrix(xPosition, yPosition, 0);

        //        RTMatrix resultMatrix = transformMatrix*localMatrix;

        //        double x, y, rot;

        //        resultMatrix.GetRotoTransl(out x, out y, out rot);

        //        entities.Rotate(rot, Vector3D.AxisZ, new Point3D(0, 0, 0));

        //        entities.Translate(x, y, _cLayerTranslactionFactor * layerIndex);

        //        entities.Regen();

        //        _viewportLayout.Entities.AddRange(entities, layerIndex);


        //        _viewportLayout.Invalidate();

        //        retVal = true;

        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("CBasicPaint InsertDxfFile error ", ex);

        //    }

        //    if (retVal)
        //        return entities;

        //    return null;
        //}

        public bool InsertEntities(object inputEntities, double xPosition, double yPosition, RTMatrix transformMatrix, string layerName, object data)
        {
            bool retVal = false;

            if (!(inputEntities is EntityList) )
                return false;

            EntityList entityList = new EntityList();



            try
            {
                foreach (var ent in (EntityList)inputEntities)
                {
                    entityList.Add((Entity)ent.Clone());
                }

                foreach (var ent in entityList)
                {
                    ent.ColorMethod = colorMethodType.byLayer;
                }
                
                Layer layer = FindEyeLayer(layerName);

                int layerIndex = (layer != null) ? _viewportLayout.Layers.IndexOf(layer) : 0;

                RTMatrix localMatrix = new RTMatrix(xPosition, yPosition, 0);

                RTMatrix resultMatrix = transformMatrix*localMatrix;

                double x, y, rot;

                resultMatrix.GetRotoTransl(out x, out y, out rot);

                if (_axisDirection == AxisDirection.LEFT_TOP || _axisDirection == AxisDirection.RIGHT_BOTTOM)
                {
                    entityList.Rotate(Math.PI, Vector3D.AxisX, new Point3D(0, 0, 0));
                    entityList.Rotate(Math.PI, Vector3D.AxisZ, new Point3D(0, 0, 0));
                }

                entityList.Rotate(rot, Vector3D.AxisZ, new Point3D(0, 0, 0));

                entityList.Translate(x, y, _cLayerTranslactionFactor * layerIndex);

                entityList.Regen();

                _viewportLayout.Entities.AddRange(entityList, layerIndex);


                _viewportLayout.Invalidate();

                retVal = true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint InsertEntities error ", ex);

            }
            return retVal;
        }

        #endregion

        #region >-------------- >----- Viewer related

        /// <summary>
        /// Visualizzazione di parti dell'oggetto di disegno (Vector Draw)
        /// </summary>
        /// <param name="show">bool: true: visualizza / false: non visualizza </param>
        public void ShowVDLayout()
        {
            ShowVDLayout(_showLayout);
        }

        public void ShowVDLayout(bool show)
        {
            _showLayout = show;

            //TODO: Definire eventuali elementi da visualizzare/nascondere

            //_viewportLayout.Viewports[0].ToolBar.Visible = false;
            //_viewportLayout.Viewports[0].ViewCubeIcon.Visible = false;
            _viewportLayout.OrientationMode = orientationType.UpAxisZ;
            Viewer.KeyShortcutsEnabled = show;
        }


        /// <summary>
        /// zoom del disegno
        /// </summary>
        public void ZoomAll() 
        {
            if (_viewportLayout == null || _viewportLayout.windowHandle == IntPtr.Zero) return;

            ////if (_viewportLayout.Height <= 0 || _viewportLayout.Width <= 0) return;

            //int margin = 

            _viewportLayout.ZoomFit();
            _viewportLayout.Invalidate();
        }

        /// <summary>
        /// zoom del disegno
        /// </summary>
        /// <param name="fact">fattore di zoom</param>
        public void ZoomAll(double fact)
        {
            if (_viewportLayout == null) return;

            double widthMargin = Math.Abs(_viewportLayout.Width*( 1- fact) /2);
            double heightMargin = Math.Abs(_viewportLayout.Height* (1 - fact)/2);
            int margin = (int) Math.Min(widthMargin, heightMargin);

            if (margin < 4) margin = 4;

            _viewportLayout.ZoomFit((fact <= 1) ? margin : -margin);

            _viewportLayout.Invalidate();
            return;
        }


        /// <summary>
        /// zoom finestra
        /// </summary>
        public void ZoomWindow(Point firstCorner, Point secondCorner)
        {
            if (_viewportLayout == null) return;

            Point3D p1 = _viewportLayout.WorldToScreen(firstCorner.X, firstCorner.Y, 0);
            Point3D p2 = _viewportLayout.WorldToScreen(secondCorner.X, secondCorner.Y, 0);

            _viewportLayout.ZoomWindow(new System.Drawing.Point((int)p1.X, (int)_viewportLayout.Height - (int)p1.Y), new System.Drawing.Point((int)p2.X, (int)_viewportLayout.Height - (int)p2.Y));
        }

        /// <summary>
        /// zoom finestra
        /// </summary>
        public void ZoomWindow()
        {
            _zoomWindow = true;
            _startZoomWindowPoint = null;
        }

        /// <summary>
        /// zoom finestra
        /// </summary>
        public void CancelZoomWindow()
        {
            _zoomWindow = false;
            _startZoomWindowPoint = null;
        }


        /// <summary>
        /// zoom del disegno
        /// </summary>
        /// <param name="val">percentuale di zoom in</param>
        public void ZoomIn(double val)
        {
            if (_viewportLayout == null) return;

            if (val <= 0)
                val = 5;

            var bounds = _viewportLayout.Viewports[0].GetBounds(_viewportLayout.Viewports[0]);
            bounds.Inflate((int)(bounds.Width*(1 - val/100)), (int)(bounds.Height*(1 - val/100)));

            int pixelAmount = (int) ((double) _viewportLayout.Width*val/500);
            _viewportLayout.ZoomIn(pixelAmount);

            _viewportLayout.Invalidate();
        }

        /// <summary>
        /// zoom del disegno
        /// </summary>
        /// <param name="val">percentuale di zoom out</param>
        public void ZoomOut(double val)
        {
            if (_viewportLayout == null) return;

            if (val <= 0)
                val = 5;


            int pixelAmount = (int)((double)_viewportLayout.Width * val / 500);
            _viewportLayout.ZoomOut(pixelAmount);

            _viewportLayout.Invalidate();

        }

        /// <summary>
        /// Gestisce la modalità ortho
        /// </summary>
        /// <param name="ortho">true il cursore si muove ortogonalmente</param>
        public void Ortho(bool ortho)
        {
            OrthoMode = ortho;
        }

        /// <summary>
        /// Restituisce il punto del cursore
        /// </summary>
        /// <returns></returns>
        public Point CursorPosition() 
        { 
            if (_viewportLayout == null) return null;

            //TODO
            //var p = _viewportLayout.PointToClient(Cursor.Position);
            //var p = _viewportLayout.GetMousePosition(_viewportLayout);

            //var p = Mouse.GetPosition(_viewportLayout);

            var p = _viewportLayout.MouseLocation;


            var mouseLocation = Cursor.Position;
            Point3D planePoint;
            //_viewportLayout.Viewports[0].ScreenToPlane(mouseLocation, Plane.XY, out planePoint);
            _viewportLayout.Viewports[0].ScreenToPlane(new System.Drawing.Point((int)p.X, (int)p.Y),  Plane.XY, out planePoint);

            return new Point(planePoint.X, planePoint.Y);
        }

        public Point DragMoveCursorPosition()
        {
            if (_viewportLayout == null) return null;

            //TODO
            //var p = _viewportLayout.PointToClient(Cursor.Position);
            //var p = _viewportLayout.GetMousePosition(_viewportLayout);

            //var p = Mouse.GetPosition(_viewportLayout);

            var p = _viewportLayout.MoveDragMouseLocation;


            var mouseLocation = Cursor.Position;
            Point3D planePoint;
            //_viewportLayout.Viewports[0].ScreenToPlane(mouseLocation, Plane.XY, out planePoint);
            _viewportLayout.Viewports[0].ScreenToPlane(new System.Drawing.Point((int)p.X, (int)p.Y), Plane.XY, out planePoint);

            return new Point(planePoint.X, planePoint.Y);
        }

        /// <summary>
        /// Imposta modalità e layers di snap a oggetti
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="enable"></param>
        /// <param name="snapLayers"></param>
        public void SnapsMode(SnapMode mode, bool enable, List<string> snapLayers)
        {
            _snapLayers = snapLayers;
            _viewportLayout.SnapLayers = snapLayers;

            // Disabilita gli snap
            if (mode == SnapMode.NONE)
            {
                _osnapModes.Clear();
                _viewportLayout.SnapModes.Clear();
                _viewportLayout.ObjectSnapEnabled = false;
                return;
            }
                // Attiva tutti gli snap
            else if (mode == SnapMode.ALL)
            {
                _osnapModes.Clear();
                _osnapModes.Add(mode);
                _viewportLayout.SnapModes.Clear();

                foreach(var osnapMode in SnapModeMap.Values)
                    _viewportLayout.SnapModes.Add(osnapMode);

                _viewportLayout.ObjectSnapEnabled = true;

                return;
            }

            if (enable)
            {
                // Se già non esiste aggiunge la nuova modalità di snap
                if (_osnapModes.IndexOf(mode) < 0)
                    _osnapModes.Add(mode);
                if (!_viewportLayout.SnapModes.Contains(SnapModeMap[mode]))
                    _viewportLayout.SnapModes.Add(SnapModeMap[mode]);
            }
            else
            {
                // Se lo trova lo elimina
                if (_osnapModes.IndexOf(mode) >= 0)
                    _osnapModes.Remove(mode);
                if (_viewportLayout.SnapModes.Contains(SnapModeMap[mode]))
                    _viewportLayout.SnapModes.Remove(SnapModeMap[mode]);

            }
            _viewportLayout.ObjectSnapEnabled = enable;
        }

        /// <summary>
        /// Procedura di lettura della scala
        /// </summary>
        public double GetScala()
        {
            return _viewportLayout.ViewSize;
        }

        /// <summary>
        /// Crea un immagine di anteprima degli oggetti disegnati sul componente
        /// </summary>
        /// <param name="filename">file in cui salvare l'immagine</param>
        /// <param name="dimX">dimensione X dell'immagine</param>
        /// <param name="dimY">dimensione Y dell'immagine</param>
        /// <param name="transparent">indica se lo sfondo dell'immagine deve essere trasparente</param>
        /// <param name="imageFormat">Formato dell'immagine (default = png)</param>
        /// <param name="pixelsMargin">Margine esterno in pixel</param>
        /// <returns></returns>
        public bool CreateThumbnail(string filename, int dimX, int dimY, bool transparent, ImageFormat imageFormat = null, int pixelsMargin = 12)
        {
            bool retVal = false;
            Image newImage = null;

            try
            {
                _viewportLayout.Entities.Regen();

                bool showCoordinatesIcon = _viewportLayout.Viewports[0].CoordinateSystemIcon.Visible;
                bool showOrigin = _viewportLayout.Viewports[0].OriginSymbol.Visible;
                bool showPlanarReflections = _viewportLayout.Rendered.PlanarReflections;

                _viewportLayout.Viewports[0].CoordinateSystemIcon.Visible = false;
                _viewportLayout.Viewports[0].OriginSymbol.Visible = false;
                _viewportLayout.Rendered.PlanarReflections = false;

                _viewportLayout.Entities.Regen();
                _viewportLayout.Entities.UpdateBoundingBox();

                // Per evitare di tagliare bordi con spessori linea evidenti
                if (pixelsMargin < 12)
                    pixelsMargin = 12;


                _viewportLayout.ZoomFit((int)(pixelsMargin * 2));
                _viewportLayout.Invalidate();


                Point3D topLeft = _viewportLayout.WorldToScreen(_viewportLayout.Entities.BoxMin);
                Point3D bottomRight = _viewportLayout.WorldToScreen(_viewportLayout.Entities.BoxMax);
                System.Drawing.Rectangle box = new Rectangle((int)topLeft.X, (int)topLeft.Y, (int)(bottomRight.X - topLeft.X),
                    (int)(bottomRight.Y - topLeft.Y));
                box.Inflate(pixelsMargin, pixelsMargin);



                Image screenshot = _viewportLayout.RenderToBitmap(box,new Size(dimX, dimY), !transparent, true);



                int maxDim = Math.Max(screenshot.Width, screenshot.Height);
                double mappedWidth = screenshot.Width;
                double mappedHeight = screenshot.Height;

                if (screenshot.Width == dimX && mappedHeight > dimY)
                {
                    mappedHeight = dimY;
                    mappedWidth = (double)screenshot.Width * dimY / screenshot.Height;
                    mappedWidth = Math.Ceiling(mappedWidth);
                }
                else if (screenshot.Height == dimY && mappedWidth > dimX)
                {
                    mappedWidth = dimX;
                    mappedHeight = (double)screenshot.Height * dimX / screenshot.Width;
                    mappedHeight = Math.Ceiling(mappedHeight);
                }

                Bitmap canvas = null;
                Color backColor = Color.White;

                canvas = new Bitmap(dimX, dimY);

                Rectangle destRect = new Rectangle((int)(dimX - mappedWidth) / 2, (int)(dimY - mappedHeight) / 2, (int)mappedWidth, (int)mappedHeight);

                Rectangle sourceRect = new Rectangle(0, 0, screenshot.Width, screenshot.Height);


                using (Graphics gfx = Graphics.FromImage(canvas))
                {

                    ImageAttributes attr = new ImageAttributes();

                    attr.SetColorKey(backColor, backColor);


                    gfx.SmoothingMode = SmoothingMode.None;
                    gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    gfx.CompositingMode = CompositingMode.SourceCopy;
                    gfx.CompositingQuality = CompositingQuality.HighQuality;

                    gfx.DrawImage(screenshot, destRect, sourceRect.X, sourceRect.Y, sourceRect.Width, sourceRect.Height, GraphicsUnit.Pixel, attr);

                    newImage = canvas;
                }


                _viewportLayout.Viewports[0].CoordinateSystemIcon.Visible = showCoordinatesIcon;
                _viewportLayout.Viewports[0].OriginSymbol.Visible = showOrigin;
                _viewportLayout.Rendered.PlanarReflections = showPlanarReflections;


                ImageFormat outputFormat = imageFormat ?? ImageFormat.Png;

                newImage.Save(filename, outputFormat);

                retVal = true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".CreateThumbnail", ex);
            }
            finally
            {
                _viewportLayout.Invalidate();
            }

            return retVal;
        }
        public bool CreateThumbnail_OLD(string filename, int dimx, int dimy, bool transparent, ImageFormat imageFormat = null)
        {
            var selectedEntities = _viewportLayout.Entities.Where(e => e.Selected).ToList();
            if (selectedEntities.Count == 0)
                return false;

            try
            {
                // Seleziona il disegno

                EyeSelection aSelSet = new EyeSelection();
                aSelSet.AddRange(selectedEntities);
                aSelSet.UpdateBoundingBox();
                Point3D bottomLeft = _viewportLayout.WorldToScreen(aSelSet.BoxMin);
                Point3D topRight = _viewportLayout.WorldToScreen(aSelSet.BoxMax);
                System.Drawing.Rectangle box = new Rectangle((int)bottomLeft.X, (int)topRight.Y, (int)(topRight.X - bottomLeft.X),
                    (int)(bottomLeft.Y - topRight.Y));
                box.Inflate(2,2);

                Bitmap outBitmap =  _viewportLayout.RenderToBitmap(box, new Size(dimx, dimy), !transparent, true);

                ImageFormat outputFormat = imageFormat ?? ImageFormat.Png;

                outBitmap.Save(filename, outputFormat);

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".CreateThumbnail", ex);
                return false;
            }
        }

        /// <summary>
        /// Disabilita il componente
        /// </summary>
        public void Disable()
        {
            Locked = true;
        }

        /// <summary>
        /// Abilita il componente
        /// </summary>
        public void Enable()
        {
            Locked = false;
        }


        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalità grigio</param>
        /// <param name="paper">Formato carta</param>
        public void Print(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper)
        {
            //TODO print
            ////PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left,blackwhite, paper);

            ////printDocument.Layout = _viewportLayout;

            ////printDocument.ShowPreview();

            ////// Save printer options
            ////landscape = printDocument.LandscapeOrientation;
            ////bottom = printDocument.MarginBottom;
            ////top = printDocument.MarginTop;
            ////right = printDocument.MarginRight;
            ////left = printDocument.MarginLeft;
            ////blackwhite = printDocument.BlackAndWhitePrint;
            ////paper = printDocument.PaperFormat;
        }

        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalità grigio</param>
        /// <param name="paper">Formato carta</param>
        /// <param name="rasterPrint">Stampa raster</param>
        /// <param name="showBorder">Disegna bordo di tampa</param>
        /// <param name="showTitle">Visualizza titolo stampa</param>
        /// <param name="showDateTime">Visualizza data e ora</param>
        /// <param name="title">Titolo stampa</param>
        /// <param name="titlesFont">Font titoli stampa</param>
        public void Print(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper,
            bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont )
        {
            //TODO print
            //PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left, blackwhite, paper);

            //printDocument.Layout = _viewportLayout;

            //printDocument.RasterPrint = rasterPrint;
            //printDocument.ShowBorder = showBorder;
            //printDocument.ShowTitle = showTitle;
            //printDocument.ShowDateTime = showDateTime;
            //printDocument.Title = title;

            //if (titlesFont != null)
            //    printDocument.TitlesFont = titlesFont;

            //printDocument.ShowPreview();

            //// Save printer options
            //landscape = printDocument.LandscapeOrientation;
            //bottom = printDocument.MarginBottom;
            //top = printDocument.MarginTop;
            //right = printDocument.MarginRight;
            //left = printDocument.MarginLeft;
            //blackwhite = printDocument.BlackAndWhitePrint;
            //paper = printDocument.PaperFormat;

            //printDocument.Layout = null;

            //printDocument = null;
        }

        /// <summary>
        /// Effettua il salvataggio in dxf del componente
        /// </summary>
        /// <param name="filename">nome del file da salvare</param>
        /// <param name="outlineRegions">Crea solo contorni regioni</param>
        /// <param name="ignorePictures">Ignora immagini</param>
        /// <returns>
        ///		true	:	salvataggio eseguito correttamente
        ///		false	:	errore nel salvare il dxf
        /// </returns>
        public bool SaveDxf(string filename, bool outlineRegions = true, bool ignorePictures = true)
        {
            bool retVal = false;
            try
            {
                WriteAutodeskExt dxfWriter = new WriteAutodeskExt(_viewportLayout, filename, lineWeightUnitsType.Millimeters, outlineRegions, ignorePictures);

                dxfWriter.DoWork();

                retVal = true;
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SaveDxf error ", ex);
            }


            return retVal;
        }

        #endregion


        #region >-------------- >----- Misc

        /// <summary>
        /// Elimina tutti i riferimenti ai blocchi e azzera l'area di lavoro
        /// </summary>
        public virtual void Reset()
        {
            try
            {
                // elimina tutte le informazioni
                mBlocks.Clear();
                mSelections.Clear();
                mLayers.Clear();
                mPhoto = new CPhoto();
                mText.Clear();
                move = false;
                rotate = false;
                currentAction = Action.DEFAULT;
                indexBlock = -1;
                indexText = -1;
                Locked = false;

                // TODO: Modifiche
                //_viewportLayout.Entities.Clear();
                _viewportLayout.Clear();

                ShowVDLayout();

                ShowOriginSymbol(true);

                ShowAxis(false);

                SetAxisDirection(_axisDirection);
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + " Reset Error: ", ex);
            }

        }

        /// <summary>
        /// Gestisce la modalit� la visualizzazione dei quadratini blu nella selezione di VD
        /// </summary>
        /// <param name="grip">true i quadrativi vengono visualizzati</param>
        public void EnableAutoGripOn(bool grip)
        {
            //TODO: Definire eventuale equivalente
            ////mDisplayObject.BaseControl.ActiveDocument.EnableAutoGripOn = grip;
        }

        #endregion


        #region >--------------  Public Methods - basic and operation

        #endregion


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods

        /// <summary>
        /// Metodo dedicato al riordinamento dei layers secondo criterio definito dall'applicazione
        /// Utilizzabile per impiegare criteri di Sort per livello nella visualizzazione delle entità grafiche
        /// </summary>
        protected virtual void RearrangeLayers()
        {
        }


        /// <summary>
        /// Resetta i layers del viewport Layout 
        /// </summary>
        protected void ClearLayoutLayers()
        {
            _viewportLayout.Layers.Clear();
            _viewportLayout.Layers[0].Name = "0";
        }

        internal void SaveLayerXml(string layerXml)
        {
            List<CLayer> lays = new List<CLayer>();
	
            foreach (KeyValuePair<string, CLayer> l in Layers)
                lays.Add(l.Value);

            Serialize(lays, layerXml);
        }

        internal void LoadConfiguredLayer(string layerXml)
        {						
            List<CLayer> lays;
            Deserialize(out lays, layerXml);

            if (lays != null)
            {
                // Scorre tutti i layers letti
                foreach (CLayer c in lays)
                {
                    // Se il layer non viene trovato lo crea
                    if (!Layers.ContainsKey(c.Name))
                    {
                        AddLayer(new CLayer(c.Name, c.Visible, c.Unlocked, c.Fill, c.UnFill, Color.FromArgb(c.RGBColor), c.LineType, c.LineWidth));
                    }
                        // Altrimenti lo aggiorna con i parametri trovati nella configurazione
                    else
                    {
                        Layers[c.Name].Visible = c.Visible;
                        Layers[c.Name].Unlocked = c.Unlocked;
                        Layers[c.Name].Fill = c.Fill;
                        Layers[c.Name].UnFill = c.UnFill;
                        Layers[c.Name].Color = Color.FromArgb(c.RGBColor);
                        Layers[c.Name].LineType = c.LineType;
                        Layers[c.Name].LineWidth = c.LineWidth;

                        SetLayer(Layers[c.Name]);

                    }
                }
            }
        }

        /// <summary>
        /// Inizializzazione dei layers in base alla lista impostata
        /// </summary>
        protected void InitLayers()
        {
            
        }

        #region >--------------  Protected Methods - Event handlers


        // Implementation will call these methods in order to generate appropriate event
        protected virtual void OnClickEvent(CBasicPaintEventArgs e, MouseButton mb, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evClick != null)
            {
                evClick(this, e, block_no, path_no, elem_no, vertex_no);

                if (e != null && e.Accepted == false)
                {
                    //System.Windows.Forms.MessageBox.Show("non accettato");
                }
                else if (e != null && e.Accepted == true)
                {
                    //System.Windows.Forms.MessageBox.Show("accettato!!!");
                }
            }
        }

        protected virtual void OnStartMoveEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (e != null)
            {
                if (evStartMove != null)
                    evStartMove(this, e, block_no, path_no, elem_no, vertex_no);
            }
        }

        protected virtual void OnMoveEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evMove != null)
                evMove(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnEndMoveEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evEndMove != null)
                evEndMove(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnStartRotateEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evStartRotate != null)
                evStartRotate(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnRotateEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evRotate != null)
                evRotate(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnEndRotateEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evEndRotate != null)
                evEndRotate(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnDragDropEvent(CBasicPaintEventArgs e, Block b)
        {
            if (evDragDrop != null)
                evDragDrop(this, e, b);
        }

        protected virtual void OnDragEnterEvent(CBasicPaintEventArgs e)
        {
            if (evDragEnter != null)
                evDragEnter(this, e, _blockDragDrop);
        }

        protected virtual void OnDragLeaveEvent(CBasicPaintEventArgs e)
        {
            if (evDragLeave != null)
                evDragLeave(this, e, _blockDragDrop);
        }

        protected virtual void OnMouseMoveAfter(double x, double y, double z) //string eCoord)
        {
            if (evMouseAfter != null)
                evMouseAfter(this, x, y, z);
        }

        #endregion

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        #region >-------------- >----- Environment

        private void InitEyeshot(bool showRuler = true, bool showRulerCoordinates = true)
        {
            // TODO: Definire eventualmente eventi non nativi
            // Non Eyeshot events
            ////mDisplayObject.BaseControl.ActiveDocument.ActionStart += new vdDocument.ActionStartEventHandler(VD_ActionStart);
            ////mDisplayObject.BaseControl.ActiveDocument.ActionFinish += new vdDocument.ActionFinishEventHandler(VD_ActionFinish);
            ////mDisplayObject.BaseControl.MouseMoveAfter += new MouseMoveAfterEventHandler(VD_MouseMoveAfter);


            AttachViewportEvents();


            // TODO: Definire cursore
            //dimensioni cursore

            _viewportLayout.PickBoxSize = 20;
            CursorCrossSize = 30;
            CursorMode = GraphicCursorMode.ViewFinder;

            _viewportLayout.Layers[0].Color = Color.White;
            _viewportLayout.Layers[0].Name = "0";

            _viewportLayout.SortEntities = EntitiesSortByLayer.None;

            //DEVDEPT displayType.Rendered
            //_viewportLayout.Viewports[0].DisplayMode = displayType.Rendered;

            ActivateOnEnter = false;

            //colore cursore ??


            _viewportLayout.AllowDrop = true;

            ShowVDLayout(false);

            ShowOriginSymbol(true);

            ShowAxis(false);

            SetAxisDirection(_axisDirection);

            if (_defaultBackgroundSettings == null)
            {
                _defaultBackgroundSettings = (BackgroundSettings)_viewportLayout.Viewports[0].Background.Clone();
            }

            //if (Viewer.UseTouchLayout)
            //{
            //    Viewer.BretonLayout.SetTouchMode();
            //}
            Viewer.BretonLayout.SetupUserInterface(showRuler, showRulerCoordinates);


        }

        private void AttachViewportEvents()
        {
            _viewportLayout.MouseMove += _viewportLayout_MouseMove;
            //_viewportLayout.MouseMove += _viewportLayout_MouseMove;
            _viewportLayout.MouseDown += _viewportLayout_MouseDown;
            //_viewportLayout.MouseDown += _viewportLayout_MouseDown;
            _viewportLayout.MouseUp += _viewportLayout_MouseUp;
            _viewportLayout.MouseWheel += _viewportLayout_MouseWheel;

            _viewportLayout.DragEnter +=  _viewportLayout_DragEnter;
            _viewportLayout.DragLeave += _viewportLayout_DragLeave;
            _viewportLayout.Drop += _viewportLayout_DragDrop;
            _viewportLayout.DragOver += _viewportLayout_DragOver;

        }

        void _viewportLayout_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var mousePos = RenderContextUtility.ConvertPoint(_viewportLayout.GetMousePosition(e));

            bool isToolbarVisible = _viewportLayout.GetToolBar().Visible;
            bool isMouseOverToolbar = _viewportLayout.GetToolBar().Contains(mousePos);

            if ((isToolbarVisible && isMouseOverToolbar)
                || _viewportLayout.ActionMode != actionType.None)
            {
                //e.Handled = false;
                return;
            }
            try
            {
                List<int> lb = new List<int>();
                Breton.Polygons.Point bretonCurrentPoint = null;
                List<int> lidblock = new List<int>();
                List<int> lidpath = new List<int>();
                List<int> lidside = new List<int>();
                List<int> lidpoint = new List<int>();

                //recupero il punto cliccato dall'utente
                if (currentAction == Action.DEFAULT)
                {
                    bretonCurrentPoint = _viewportLayout.DownWorldPosition;
                    _viewportLayout.StartDraggingPoint = new Point3D(_viewportLayout.DownWorldPosition.X,
                        _viewportLayout.DownWorldPosition.Y);

                    //Verifico se è attiva la funzione di ZoomWindow
                    if (_zoomWindow)
                    {
                        _startZoomWindowPoint = bretonCurrentPoint;
                        return;
                    }

                    if (e.ChangedButton == MouseButton.Left)
                        basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Left);
                    else if (e.ChangedButton == MouseButton.Right)
                        basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Right);
                    else if (e.ChangedButton == MouseButton.Middle)
                        basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Center);
                    //else if (e. == MouseButtons.None)
                    //    basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);
                    else 
                        basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

                    _viewportLayout.Selections.Clear();

                    if (SelectionEnabled)
                    {
                        _selActive = new EyeSelection("MYSEL");
                        _viewportLayout.Selections.Add(_selActive);

                        int[] entitiesIndexes = _viewportLayout.GetEntitiesUnderCursor(_viewportLayout.DownMouseLocation);

                        for (int i = 0; i < entitiesIndexes.Length; i++)
                        {
                            Entity ent = _viewportLayout.Entities[entitiesIndexes[i]];
                            if (!(ent is Picture))
                            {
                                _selActive.Add(ent);
                            }
                        }

                        // Identifica tutte le entità a cui il punto cliccato appartiene
                        GetElementsBlockFromPoint(bretonCurrentPoint, _selActive, out lidblock, out lidpath, out lidside,
                            out lidpoint);

                        OnClickEvent(basicPaintEventArgs, e.ChangedButton, lidblock, lidpath, lidside, lidpoint);

                        for (int i = 0; i < mSelections.Count; i++)
                            lb.Add(mSelections[i].SelectedBlock);
                    }
                    else
                    {
                        OnClickEvent(basicPaintEventArgs, e.ChangedButton, lidblock, lidpath, lidside, lidpoint);
                    }
                }
                else
                    return;

                if (OrthoMode)
                {
                    _viewportLayout.OrthoModeStarted = true;
                }

                //clicco per iniziare un movimento

                #region MOVE

                if (move && currentAction == Action.DEFAULT && e.LeftButton == MouseButtonState.Pressed)
                {
                    OnStartMoveEvent(basicPaintEventArgs, lb, null, null, null);

                    if (basicPaintEventArgs.Accepted)
                    {
                        currentAction = Action.MOVE;

                        //memorizzo i punti iniziali e le matrici
                        SaveStartMatrix();
                        _userPoint = bretonCurrentPoint;
                        _lastOkPoint = bretonCurrentPoint;

                        _viewportLayout.MoveRotateRubberbandLine.StartPoint = new Point2D(bretonCurrentPoint.X,
                            bretonCurrentPoint.Y);
                        _viewportLayout.MoveRotateRubberbandLine.DrawColor = _viewportLayout.Palette[6];
                        _viewportLayout.MoveRotateRubberbandLine.EndPoint = new Point2D(bretonCurrentPoint.X,
                            bretonCurrentPoint.Y);
                        _viewportLayout.MoveRotateRubberbandLine.DottedBorder = true;
                        _viewportLayout.MoveRotateRubberbandLine.DrawEntity = true;


                        _selActive = new EyeSelection("Blocks");
                        EyeSelection selTmp = null;

                        List<int> idBlocks = new List<int>();

                        // Prepara la lista univoca di blocchi
                        foreach (CSelection cs in mSelections)
                        {
                            if (!idBlocks.Contains(cs.SelectedBlock))
                                idBlocks.Add(cs.SelectedBlock);
                        }

                        // Scorre tutti i blocchi e prepara la vdSelection
                        foreach (int idB in idBlocks)
                        {
                            selTmp = SelectEntities(idB, -1, -1, -1);

                            if (selTmp != null)
                            {
                                for (int i = 0; i < selTmp.Count; i++)
                                {
                                    _selActive.AddItem(selTmp[i], true);
                                }
                            }
                        }
                    }
                    else
                    {
                        //ripristino le matrici iniziali
                        for (int i = 0; i < lb.Count; i++)
                        {

                            Block b = mBlocks[lb[i]];
                            RTMatrix m = _lastMatrix[i];
                            double offX, offY, angolo;
                            m.GetRotoTransl(out offX, out offY, out angolo);
                            b.SetRotoTransl(offX, offY, angolo);
                            b = mBlocks[lb[i]];

                            Repaint(b);
                            currentAction = Action.DEFAULT;
                        }
                    }
                }
                #endregion MOVE

                //clicco per iniziare una rotazione
                #region ROTATE

                else if (rotate && currentAction == Action.DEFAULT && e.LeftButton == MouseButtonState.Pressed )
                {
                    _canStartRotate = false;
                    _rotationReferenceSegment = null;

                    OnStartRotateEvent(basicPaintEventArgs, lb, null, null, null);

                    if (basicPaintEventArgs.Accepted)
                    {

                        currentAction = Action.ROTATE;

                        //memorizzo i punti iniziali e le matrici
                        SaveStartMatrix();
                        _userPoint = bretonCurrentPoint;
                        _lastOkPoint = bretonCurrentPoint;


                        _viewportLayout.MoveRotateRubberbandLine.StartPoint = new Point2D(bretonCurrentPoint.X,
                            bretonCurrentPoint.Y);
                        _viewportLayout.MoveRotateRubberbandLine.DrawColor = _viewportLayout.Palette[6];
                        _viewportLayout.MoveRotateRubberbandLine.EndPoint = new Point2D(bretonCurrentPoint.X,
                            bretonCurrentPoint.Y);
                        _viewportLayout.MoveRotateRubberbandLine.DottedBorder = true;

                        _viewportLayout.MoveRotateRubberbandLine.DrawEntity = true;


                        _selActive = new EyeSelection("Blocks");
                        EyeSelection selTmp = null;



                        List<int> idBlocks = new List<int>();

                        // Prepara la lista univoca di blocchi
                        foreach (CSelection cs in mSelections)
                        {
                            if (!idBlocks.Contains(cs.SelectedBlock))
                                idBlocks.Add(cs.SelectedBlock);
                        }

                        foreach (int idB in idBlocks)
                        {
                            selTmp = SelectEntities(idB, -1, -1, -1);

                            if (selTmp != null)
                            {
                                for (int i = 0; i < selTmp.Count; i++)
                                {
                                    _selActive.AddItem(selTmp[i], true);
                                }
                            }
                        }
                    }
                    else
                    {
                        //ripristino le matrici iniziali
                        for (int i = 0; i < lb.Count; i++)
                        {

                            Block b = mBlocks[lb[i]];
                            RTMatrix m = _lastMatrix[i];
                            double offX, offY, angolo;
                            m.GetRotoTransl(out offX, out offY, out angolo);
                            b.MatrixRT.SetRotoTransl(offX, offY, angolo);
                            b = mBlocks[lb[i]];

                            Repaint(b);
                            currentAction = Action.DEFAULT;
                        }
                    }
                }

                #endregion ROTATE

                //selezione (generalmente per il drag & drop)
                else if (!move && !rotate)
                {
                    if (basicPaintEventArgs.Accepted && dragdrop)
                    {
                        //Cancel = true;
                        PrepareAndStartDrag(lidblock, bretonCurrentPoint);
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("_viewportLayout_MouseDown ERROR: ", ex);
            }
        }

        void _viewportLayout_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var mousePos = RenderContextUtility.ConvertPoint(_viewportLayout.GetMousePosition(e));
            try
            {
                System.Drawing.Point mousePosition = mousePos;

                DraftingViewportLayout.SnapPoint snapPoint;

                Point3D currentWorldPosition;
                _viewportLayout.ScreenToPlane(mousePosition, Plane.XY, out currentWorldPosition);

                if (_zoomWindow && _startZoomWindowPoint != null)
                {
                    DrawGenericRectangle(_startZoomWindowPoint.X, _startZoomWindowPoint.Y,
                        currentWorldPosition.Y - _startZoomWindowPoint.Y,
                        currentWorldPosition.X - _startZoomWindowPoint.X,
                        Viewer.ZoomWindowFill, Viewer.ZoomWindowColor, Viewer.ZoomWindowShowBorder,
                        Viewer.ZoomWindowDottedBorder);

                    NotifyCursorCoordinates();

                    return;
                }

                if (_osnapModes.Count > 0)
                {
                    snapPoint = GetSnapPoint(mousePosition);

                    if (snapPoint != null)
                    {

                    }

                    _lastSnapPoint = snapPoint;

                }

                if (!move && !rotate)
                {
                    NotifyCursorCoordinates();

                    //if (_viewportLayout.Dragging)
                    //{
                    //    _viewportLayout.DrawDragEntities();
                    //}
                    return;
                }

                #region MOVE

                if (move && currentAction == Action.MOVE)
                {
                    if (mSelections.Count > 0)
                    {
                        Point3D punto = (Point3D)currentWorldPosition.Clone();

                        // Verifica se siamo in modalità Ortho
                        if (OrthoMode)
                        {
                            punto.X = _viewportLayout.MoveOrthoWorldPosition.X;
                            punto.Y = _viewportLayout.MoveOrthoWorldPosition.Y;
                        }
                        ////{
                        ////    if (Math.Abs(punto.X - _userPoint.X) > Math.Abs(punto.Y - _userPoint.Y))
                        ////        punto.Y = _userPoint.Y;
                        ////    else
                        ////        punto.X = _userPoint.X;
                        ////}

                        double deltaX = punto.X - _lastOkPoint.X;
                        double deltaY = punto.Y - _lastOkPoint.Y;

                        _lastOkPoint = new Point(punto.X, punto.Y);

                        List<int> lb = new List<int>();

                        foreach (CSelection cs in mSelections)
                        {
                            Block b = mBlocks[cs.SelectedBlock];
                            if (b.AllowMove)
                            {
                                lb.Add(b.Id);
                                b.MatrixRT.Save();
                                b.RotoTranslRel(deltaX, deltaY, 0d, 0d, 0d);
                            }
                        }

                        OnMoveEvent(basicPaintEventArgs, lb, null, null, null);

                        if (!basicPaintEventArgs.Accepted)
                        {
                            foreach (CSelection cs in mSelections)
                            {
                                Block b = mBlocks[cs.SelectedBlock];
                                // De Conti 03/11/2016
                                if (b.AllowMove)
                                    b.MatrixRT.Restore();
                            }
                        }
                        else
                        {
                            EyeSelection moveSelection = new EyeSelection("Move");
                            foreach (int idBlock in lb)
                            {
                                var selTmp = SelectEntities(idBlock, -1, -1, -1);

                                if (selTmp != null)
                                    for (int i = 0; i < selTmp.Count; i++)
                                        moveSelection.AddItem(selTmp[i], true);
                            }

                            moveSelection.Translate(deltaX, deltaY, 0);
                            //_selActive.Translate(deltaX, deltaY, 0);


                            _viewportLayout.MoveRotateRubberbandLine.EndPoint = new Point2D(_lastOkPoint.X,
                                _lastOkPoint.Y);
                            _viewportLayout.Entities.Regen();
                            _viewportLayout.Invalidate();

                        }
                    }
                }
                #endregion MOVE

                #region ROTATE

                else if (rotate && currentAction == Action.ROTATE)
                {
                    Point3D punto = (Point3D)currentWorldPosition.Clone();
                    if (OrthoMode)
                    {
                        punto.X = _viewportLayout.MoveOrthoWorldPosition.X;
                        punto.Y = _viewportLayout.MoveOrthoWorldPosition.Y;
                    }

                    Point3D pivotPoint = new Point3D(_userPoint.X, _userPoint.Y);
                    Point3D pPrec = new Point3D(_lastOkPoint.X, _lastOkPoint.Y);
                    double rotationAngle = 0;

                    _viewportLayout.MoveRotateRubberbandLine.EndPoint.X = punto.X;
                    _viewportLayout.MoveRotateRubberbandLine.EndPoint.Y = punto.Y;

                    List<System.Drawing.Point> screenPoints = new List<System.Drawing.Point>() { new System.Drawing.Point(0, 0), new System.Drawing.Point((int)START_ROTATE_MINIMUM_DISTANCE, 0) };
                    var worldPoints = _viewportLayout.ScreenToPlane(screenPoints, Plane.XY);
                    double worldStartRotateMinimumDistance = worldPoints[0].DistanceTo(worldPoints[1]);

                    if (!_canStartRotate)
                    {
                        _canStartRotate = _viewportLayout.MoveRotateRubberbandLine.StartPoint.DistanceTo(
                            _viewportLayout.MoveRotateRubberbandLine.EndPoint) > worldStartRotateMinimumDistance;
                        // Define reference segment
                        if (_canStartRotate)
                        {
                            _rotationReferenceSegment = new Segment(_userPoint, new Point(punto.X, punto.Y));
                            //_rotationReferenceSegment = new Segment(_userPoint, new Point(punto.X + worldStartRotateMinimumDistance, _userPoint.Y));
                        }
                    }

                    if (_canStartRotate)
                    {
                        _viewportLayout.MoveRotateRubberbandLine.DrawColor = _viewportLayout.Palette[6];
                        if (_rotationReferenceSegment != null)
                        {
                            double distance = _viewportLayout.MoveRotateRubberbandLine.StartPoint.DistanceTo(
                                _viewportLayout.MoveRotateRubberbandLine.EndPoint) + worldStartRotateMinimumDistance;// * 10;
                            //referenceSegment = new Segment(referenceSegment.P1,
                            //    referenceSegment.PointAtDistanceFromStart(distance));
                            _rotationReferenceSegment.Extend(Side.EN_VERTEX.EN_VERTEX_END, distance - _rotationReferenceSegment.Length);
                            DrawGenericLine(_rotationReferenceSegment, _rotateReferenceColor, true, false);
                            DrawGenericArc(_userPoint,
                                _rotationReferenceSegment.PointAtDistanceFromStart(_viewportLayout.MoveRotateRubberbandLine.StartPoint.DistanceTo(_viewportLayout.MoveRotateRubberbandLine.EndPoint)),
                                punto, _rotateReferenceColor, true, false);
                        }
                    }
                    else
                        _viewportLayout.MoveRotateRubberbandLine.DrawColor = _rotateReferenceColor;


                    if (mSelections.Count > 0 && _canStartRotate)
                    {

                        //{
                        //    if (Math.Abs(punto.X - _userPoint.X) > Math.Abs(punto.Y - _userPoint.Y))
                        //        punto.Y = _userPoint.Y;
                        //    else
                        //        punto.X = _userPoint.X;
                        //}

                        var vect1 = new Vector2D(pivotPoint, punto);
                        var vect2 = (pPrec != pivotPoint) ? new Vector2D(pivotPoint, pPrec) : vect1;
                        rotationAngle = Vector2D.SignedAngleBetween(vect2, vect1);


                        _lastOkPoint = new Point(punto.X, punto.Y);

                        List<int> lb = new List<int>();

                        foreach (CSelection cs in mSelections)
                        {
                            Block b = mBlocks[cs.SelectedBlock];
                            if (b.AllowRotate)
                            {
                                lb.Add(b.Id);
                                b.MatrixRT.Save();
                                b.RotoTranslRel(0d, 0d, _userPoint.X, _userPoint.Y, rotationAngle);
                            }
                        }

                        OnRotateEvent(basicPaintEventArgs, lb, null, null, null);

                        if (!basicPaintEventArgs.Accepted)
                        {
                            foreach (CSelection cs in mSelections)
                            {
                                Block b = mBlocks[cs.SelectedBlock];
                                if (b.AllowRotate)
                                    b.MatrixRT.Restore();
                            }
                        }
                        else
                        {
                            var center = new Point3D(pivotPoint.X, pivotPoint.Y);

                            // Seguente non funziona per un bug Eyeshot, dovrebbe essere sistemato in Night Build 18/06/2015
                            // Update: ok con Night Build 322 del 18/06/2015

                            EyeSelection rotateSelection = new EyeSelection("Rotate");
                            foreach (int idBlock in lb)
                            {
                                var selTmp = SelectEntities(idBlock, -1, -1, -1);

                                if (selTmp != null)
                                    for (int i = 0; i < selTmp.Count; i++)
                                        rotateSelection.AddItem(selTmp[i], true);
                            }

                            rotateSelection.Rotate(rotationAngle, Vector3D.AxisZ, center);

                            //_selActive.Rotate(rotationAngle, Vector3D.AxisZ, center);


                            //foreach (var ent in _selActive)
                            //{
                            //    ent.Rotate(rotationAngle, Vector3D.AxisZ, center);
                            //}
                        }
                    }

                    _viewportLayout.Entities.Regen();
                    _viewportLayout.Invalidate();
                    NotifyCursorCoordinates();

                }

                #endregion ROTATE

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ViewportLayout_MouseMove ERROR: ", ex);
            }
        }

        // TODO: non disponibile in Eyeshot
        /// <summary>
        /// Crea un nuovo disegno
        /// </summary>
        /// <returns></returns>
        //private void NewActiveDocument() 
        //{ 
        //    mDisplayObject.BaseControl.ActiveDocument.New();
        //    mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.BkColor = _backColor;
        //}


        /// <summary>
        /// Imposta la direzione degli assi
        /// </summary>
        /// <param name="ax"></param>
        private void SetAxisDirection(AxisDirection ax)
        {
            switch (ax)
            {
                case AxisDirection.LEFT_BOTTOM:
                    _viewportLayout.Viewports[0].CoordinateSystemIcon.Position = coordinateSystemPositionType.BottomLeft;
                    _viewportLayout.Viewports[0].InitialView = viewType.Top;
                    _viewportLayout.AxisMode = AxisOrientation.LeftBottom;
                    break;

                case AxisDirection.RIGHT_TOP:
                    _viewportLayout.Viewports[0].CoordinateSystemIcon.Position = coordinateSystemPositionType.TopRight;
                    _viewportLayout.Viewports[0].InitialView = viewType.Top;
                    //_viewportLayout.Viewports[0].RotateCamera(Plane.XY.AxisZ, 180, false);
                    _viewportLayout.Viewports[0].Camera.Rotation = new Quaternion(-0.5, 0.5, -0.5, 0.5);
                    _viewportLayout.AxisMode = AxisOrientation.RightTop;
                    break;

                case AxisDirection.LEFT_TOP:
                    //_viewportLayout.Viewports[0].CoordinateSystemIcon.Position = coordinateSystemPositionType.TopRight;
                    _viewportLayout.Viewports[0].InitialView = viewType.Top;
                    _viewportLayout.Viewports[0].Camera.Rotation = new Quaternion(-0.5, -0.5, 0.5, 0.5);
                    _cLayerTranslactionFactor = -0.001;
                    _viewportLayout.AxisMode = AxisOrientation.LeftTop;
                    break;

                case AxisDirection.RIGHT_BOTTOM:
                    _viewportLayout.Viewports[0].CoordinateSystemIcon.Position = coordinateSystemPositionType.BottomRight;
                    _viewportLayout.Viewports[0].InitialView = viewType.Top;
                    _viewportLayout.Viewports[0].Camera.Rotation = new Quaternion(0.5, -0.5, -0.5, 0.5);
                    _cLayerTranslactionFactor = -0.001;
                    _viewportLayout.AxisMode = AxisOrientation.RightBottom;
                    break;


            }


        }

        /// <summary>
        /// Visualizzazione degli assi
        /// </summary>
        /// <param name="show">bool: true: visualizza / false: non visualizza</param>
        private void ShowAxis(bool show)
        {
            if (_viewportLayout == null) return;
                _viewportLayout.Viewports[0].CoordinateSystemIcon.Visible = show;
        }

        private void ShowOriginSymbol(bool show)
        {
            if (_viewportLayout == null) return;
                _viewportLayout.Viewports[0].OriginSymbol.Visible = show;
        }

        #endregion

        #region >-------------- >----- Logics

        private void Serialize(List<CLayer> list, string layerXml)
        {
            if (layerXml == null || layerXml.Length == 0)
                layerXml = Directory.GetCurrentDirectory() + "\\LayerConfig.xml";

            XmlSerializer serializer = new XmlSerializer(typeof(List<CLayer>));

            TextWriter writer = new StreamWriter(layerXml);
            serializer.Serialize(writer, list);
            writer.Close();
        }
		

        private void Deserialize(out List<CLayer> list, string layerXml)
        {
            list = null;

            if (layerXml == null || layerXml.Length == 0)
                layerXml = Directory.GetCurrentDirectory() + "\\LayerConfig.xml";
			
            if (!File.Exists(layerXml))
                return;

            XmlSerializer deserializer = new XmlSerializer(typeof(List<CLayer>));

            TextReader reader = new StreamReader(layerXml);
            object obj = deserializer.Deserialize(reader);
            list = (List<CLayer>)obj;
            reader.Close();
        }

        private bool _Select(int block_no, int path_no, int elem_no, int vertex_no, HighLigthType type)
        {
            try
            {
                CSelection selBlock = new CSelection(block_no, path_no, elem_no, vertex_no);
                if (selBlock == null)    // selezione non valida
                    return false;

                //non inserisco le stesse selezioni
                bool addOK = true;
                foreach (CSelection cs in mSelections)
                    if (cs.SelectedBlock == block_no && cs.SelectedPath == path_no &&
                        cs.SelectedElement == elem_no && cs.SelectedVertex == vertex_no)
                        addOK = false;

                if(addOK)
                    mSelections.Add(selBlock);

                ////double spessoreSelezione = (GetScala() * 3) / _viewer.Height;
                //double spessoreSelezione = (GetScala() / 1000) * 5;
                double spessoreSelezione = HIGHLIGHT_PIXEL_THICKNESS;

                HighLight(block_no, path_no, elem_no, vertex_no, spessoreSelezione, type);

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }

        }

        private bool _Unselect(int block_no, int path_no, int elem_no, int vertex_no)
        {
            
            foreach(CSelection csel in mSelections)
            {

                if (csel.SelectedBlock == block_no)
                {
                    //se va deselezionato un blocco rimuovo dalla lista
                    mSelections.Remove(csel);                    
                    break;
                }

                else if (csel.SelectedPath == path_no)
                {
                    csel.SelectedPath = -1;
                    csel.SelectedElement = -1;
                    csel.SelectedVertex = -1;
                }

                else if (csel.SelectedElement == elem_no)
                {
                    csel.SelectedElement = -1;
                    csel.SelectedVertex = -1;
                }

                else if (csel.SelectedVertex == vertex_no)
                    csel.SelectedVertex = -1;

            }

            HighLight(block_no, path_no, elem_no, vertex_no, 0, HighLigthType.NONE);

            

            return true;
        }

        private static bool MatchSel(CSelection cSel)
        {
            //mblock_no = block_no;
            //mpath_no = path_no;
            //melem_no = elem_no;
            //mvertex_no = vertex_no;
            return true;
            //if (cSel.SelectedBlock != null)
            //{
                
            //    //cSel.SelectedBlock = mblock;
            //}

        }

        /// <summary>
        /// Verifica se un punto è interno a un blocco della lista e restituisce il blocco
        /// </summary>
        /// <param name="p">punto per il controllo</param>
        /// <returns>id blocco</returns>
        private int GetBlockFromPoint(Point p)
        {
            try
            {

                //verifico se il punto è interno a un blocco della lista
                for (int ib = 0; ib < mBlocks.Values.Count; ib++)
                {
                    Block b = mBlocks.Values[ib];
                    RTMatrix m = b.MatrixRT;

                    for (int i = 0; i < b.GetPaths().Values.Count; i++)
                    {
                        Path path = b.GetPaths().Values[i];
                        if (path.IsInsideRT(p))
                            return b.Id;
                    }
                }

                return -1;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return -1;
            }
        }

        
        /// <summary>
        /// Cerca il path, il side e il vertex del blocco pi� vicino al punto se sono in tolleranza
        /// </summary>
        /// <param name="pUser">punto per il controllo</param>
        /// <param name="sel">elementi selezionati in VD</param>
        /// <param name="idb">lista id blocchi trovati</param>
        /// <param name="idp">lista id path trovati</param>
        /// <param name="ids">lista id side trovati</param>
        /// <param name="idv">lista id vertici trovati</param>
        private void GetElementsBlockFromPoint(Point pUser, EntityList sel, out List<int> idb, out List<int> idp, out List<int> ids, out List<int> idv)
        {

            idb = new List<int>();
            idp = new List<int>();
            ids = new List<int>();
            idv = new List<int>();

            try
            {

                if (sel.Count == 0)
                {
                    //verifico se il punto è interno a un blocco della lista
                    for (int ib = 0; ib < mBlocks.Values.Count; ib++)
                    {

                        Block b = mBlocks.Values[ib];

                        for (int i = 0; i < b.GetPaths().Values.Count; i++)
                        {
                            Path path = b.GetPaths().Values[i];
                            if (b.AllowSelect && path.IsInsideRT(pUser))
                            {
                                Layer l = FindEyeLayer(path.Layer);
                                //vdLayer l = FindVdLayer(path.Layer);
                                if (l != null && l.Visible)
                                {
                                    idb.Add(b.Id);
                                    break;
                                }
                            }
                        }
                    }

                }
                else
                {

                    for (int i = 0; i < sel.Count; i++)
                    {
                        Entity f = sel[i];
                        //vdFigure f = sel[i];
                        ////if (f.Layer.Frozen) continue;
                        if (!_viewportLayout.Layers[f.LayerIndex].Visible) continue;
                        string fLabel = f.EntityData.ToString();
                        string[] split = fLabel.Split('#');
                        if (split.Length == 3)
                        {

                            int id = Convert.ToInt32(split[0]);
                            Block b = GetBlock(id);
                            if (b != null && b.AllowSelect)
                                idb.Add(id);
                            else
                            {
                                idb.Add(-1);
								//continue;
                            }

                            id = Convert.ToInt32(split[1]);
                            Path p = b.GetPath(id);
                            if (p != null && p.AllowSelect)
                                idp.Add(id);
                            else
                            {
                                idp.Add(-1);
                                //continue;
                            }

                            id = Convert.ToInt32(split[2]);
                            Side s = p.GetSide(id);
							if (s != null && s.AllowSelect)
							{
								ids.Add(id);

								Side sRT = p.GetSideRT(id);
								double size = ((GetScala() * _viewportLayout.PickBoxSize) / _viewportLayout.Height) / 2;
								double distPuP1 = pUser.Distance(sRT.P1);
								double distPuP2 = pUser.Distance(sRT.P2);
								if (distPuP1 <= size && distPuP1 < distPuP2)
									idv.Add(1);
								else if (distPuP2 <= size && distPuP2 < distPuP1)
									idv.Add(2);
								else
									idv.Add(-1);
							}
							else
							{
								ids.Add(-1);
								//continue;
							}
                        }
                    }
                }
            }
            catch (Exception ex) { TraceLog.WriteLine("GetElementsBlockFromPoint ERROR: ", ex); }
        }

        /// <summary>
        /// Forza il ridisegno, con facoltativo Regen delle entità
        /// </summary>
        /// <param name="regen">Rigenera le entità</param>
        private void Redraw(bool regen = false)
        {
            if (_viewportLayout != null)
            {
                //if (_viewportLayout.Height > 0 && _viewportLayout.Width > 0)
                {
                    if (regen)
                    {
                        _viewportLayout.Entities.Regen();
                        //_viewportLayout.Entities.RegenAllCurved();
                    }

                    _viewportLayout.Invalidate();

                }
            } 
        }

        /// <summary>
        /// Forza il ridisegno con il ricalcolo
        /// </summary>
        private void Regen() 
        {
            Redraw(true);

        }

        /// <summary>
        /// Procedura di evidenziazione delle entità dati gli id dell'etichetta
        /// </summary>
        /// <param name="idBlock">primo elemento dell'etichetta</param>
        /// <param name="idPath">secondo elemento dell'etichetta</param>
        /// <param name="idSide">terzo elemento dell'etichetta</param>
        /// <param name="idVertex">quarto elemento dell'etichetta</param>
        /// <param name="thickness">spessore selezione. 0 per deselezionare</param>
        protected void HighLight(int idBlock, int idPath, int idSide, int idVertex, double thickness, HighLigthType type)
        {

            try
            {
                //elimino i cerchi se lo spessore è 0
                if (Math.Abs(thickness) < TOLERANCE)
                {
                    EntitiesFilter filtro = new EntitiesFilter();
                    filtro.TypeNames.Add(typeof(LinearPath).Name);
                    filtro.TypeNames.Add(typeof(Quad).Name);
                    filtro.EntityDataList.Add("VERTICE");

                    _viewportLayout.Selections.Clear();


                    EyeSelection mysel = new EyeSelection("GetRow2");
                    _viewportLayout.Selections.Add(mysel);

                    mysel.FilterSelect(_viewportLayout, filtro);

                    for (int intI = 0; intI < mysel.Count; intI++)
                    {
                        _viewportLayout.Entities.Remove(mysel[intI]);
                    }

                
                }

                EyeSelection sel = SelectEntities(idBlock, idPath, idSide, idVertex);
                
                for (int intI = 0; intI < sel.Count; intI++)
                {

                    if (idVertex != -1)
                    {
                        double x = 0, y = 0;
                        if (sel[intI].GetType().Name.ToUpper() == "LINE")
                        {
                            ZTopLine l = sel[intI] as ZTopLine;
                            if (l != null)
                            {
                                if (idVertex == 1)
                                {
                                    x = l.StartPoint.X;
                                    y = l.StartPoint.Y;
                                }
                                else if (idVertex == 2)
                                {
                                    x = l.EndPoint.X;
                                    y = l.EndPoint.Y;
                                }
                            }
                        }
                        else
                        {
                            ZTopArc a = sel[intI] as ZTopArc;
                            if (a != null)
                            {
                                if (idVertex == 1)
                                {
                                    x = a.StartPoint.X;
                                    y = a.StartPoint.Y;
                                }
                                else if (idVertex == 2)
                                {
                                    x = a.EndPoint.X;
                                    y = a.EndPoint.Y;
                                }
                            }
                        
                        }

                        RectangularRegion rect = EyeAddRectangle(x - (thickness / 2), y - (thickness / 2), thickness, thickness);
                        rect.EntityData = "VERTICE";

                    }
                    else
                    {
                        //sel[intI].LineWeight = (float)thickness;
                        sel[intI].LineWeight = (thickness > 0) ? (float) thickness : 0.01F;

                        //sel[intI].Color = Color.MediumSpringGreen;

                        if (type == HighLigthType.NORMAL)
                        {
                            //System.Diagnostics.Debug.Print("COLOR MODE: " + sel[intI].ColorMethod +
                            //    " TYPE: " + sel[intI].GetType().ToString() + 
                            //    "      Color: " + sel[intI].Color);

                            //var dummy = sel[intI].XData[0];
                            sel[intI].LineTypeMethod = colorMethodType.byLayer;
                            sel[intI].LineWeightMethod = colorMethodType.byEntity;
                            //System.Diagnostics.Debug.Print("COLOR MODE: " + sel[intI].ColorMethod +
                            //    " TYPE: " + sel[intI].GetType().ToString() +
                            //    "      Color: " + sel[intI].Color);

                            if (sel[intI] is ColorRegion)
                            {
                                if (UseHilightColorOnColorRegions)
                                    (sel[intI] as ColorRegion).HighLight(true);
                            }
                            else
                            {
                                sel[intI].HiLight(true);
                            }
                        }
                        else if (type == HighLigthType.DOT)
                        {
                            sel[intI].LineTypeMethod = colorMethodType.byEntity;

                            //if (sel[intI] is ICurve)
                            if (!_viewportLayout.LineTypes.ContainsKey("Dash"))
                            {
                                _viewportLayout.LineTypes.Add("Dash", new float[] { 20f, -20f });

                            }
                            if (_viewportLayout.LineTypes.ContainsKey("Dash"))
                                sel[intI].LineTypeName = "Dash";

                            sel[intI].LineWeightMethod = colorMethodType.byEntity;

                            if (sel[intI] is ColorRegion)
                            {
                                (sel[intI] as ColorRegion).HighLight(true);
                            }
                            else
                            {
                                sel[intI].HiLight(true);
                            }

                            //sel[intI].Regen(1e-2);
                        }
                        else if (type == HighLigthType.NONE)
                        {
                            sel[intI].LineTypeMethod = colorMethodType.byLayer;
                            sel[intI].LineWeightMethod = colorMethodType.byLayer;


                            //sel[intI].LineTypePattern = DashDotLineType;
                            if (sel[intI] is ColorRegion)
                            {
                                (sel[intI] as ColorRegion).HighLight(false);
                            }
                            else
                            {
                                sel[intI].HiLight(false);
                            }


                        }
                        //sel[intI].Regen(1e-2);
                    }
                }

                Redraw(true);

            }

            catch (Exception ex) { TraceLog.WriteLine("HighLight ERROR: ", ex); }

        }

        /// <summary>
        /// Procedura di selezione delle entità dati gli id dell'etichetta
        /// </summary>
        /// <param name="idBlock">primo elemento dell'etichetta</param>
        /// <param name="idPath">secondo elemento dell'etichetta</param>
        /// <param name="idSide">terzo elemento dell'etichetta</param>
        /// <param name="idVertex">quarto elemento dell'etichetta</param>
        private EyeSelection SelectEntities(int idBlock, int idPath, int idSide, int idVertex)
        {

            try
            {

                EntityList entList = _viewportLayout.Entities;

                _viewportLayout.Selections.Clear();

                EyeSelection sel = new EyeSelection(idBlock.ToString());
                _viewportLayout.Selections.Add(sel);

                string label = idBlock.ToString(CultureInfo.InvariantCulture) + "#";

                if (idPath != -1)
                    label = label + idPath.ToString(CultureInfo.InvariantCulture) + "#";

                if (idSide != -1)
                    label = label + idSide.ToString(CultureInfo.InvariantCulture);


                List<Entity> searchedEntities =
                    entList.Where(e => e.EntityData != null && e.EntityData.ToString().StartsWith(label)).ToList();

                foreach (var ent in searchedEntities)
                {
                    sel.Add(ent);
                }

                return sel;

            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SelectEntities ERROR: ", ex);
                return null;
            }

        }

        /// <summary>
        /// Elimina le entità data l'etichetta o una parte di essa
        /// </summary>
        /// <param name="idBlock">Primo elemento dell'etichetta</param>
        /// <param name="redraw">Ridisegno immediato</param>
        private void DeleteEntities(int idBlock, bool redraw = true)
        {

            try
            {
                DeleteEntities(idBlock, -1, redraw);
            }
            catch (Exception ex) { TraceLog.WriteLine("DeleteEntities ERROR: ", ex); }

        }

        /// <summary>
        /// Elimina le entità data l'etichetta o una parte di essa
        /// </summary>
        /// <param name="idBlock">primo elemento dell'etichetta</param>
        /// <param name="idPath">secondo elemento dell'etichetta</param>
        /// <param name="redraw">Ridisegno immediato</param>
        /// <param name="refreshHoleContainer">Ridisegna l'eventuale path contenitore</param>
        private void DeleteEntities(int idBlock, int idPath, bool redraw = true, bool refreshHoleContainer = false)
        {

            try
            {
                //System.Diagnostics.Debug.Print(string.Format("BEFORE: Entities count = {0}", _viewportLayout.Entities.Count));

                EyeSelection sel = SelectEntities(idBlock, idPath, -1, -1);

                for (int i = sel.Count - 1; i >=0; i--)
                {
                    if (_viewportLayout.Entities.Contains(sel[i]))
                    {
                        Entity entity = sel[i];
                        bool ret = _viewportLayout.Entities.Remove(entity);

                        entity.Dispose();
                        
                    }
                }

                // Se è un path di tipo UnFill, va aggiornato il contenitore Fill parent
                if (refreshHoleContainer && mBlocks.ContainsKey(idBlock))
                {
                    Path path = mBlocks[idBlock].GetPath(idPath);
                    CLayer layer = GetLayer(path.Layer);
                    if (layer != null && layer.UnFill)
                    {
                        DrawPath(path, idBlock, idPath, redraw);
                    }
                }

                if (redraw)
                    Redraw();

                //System.Diagnostics.Debug.Print(string.Format("AFTER: Entities count = {0}", _viewportLayout.Entities.Count));

            }
            catch (Exception ex) 
            { 
                TraceLog.WriteLine("DeleteEntities ERROR: ", ex); 
            }
        }

        private void NotifyCursorCoordinates()
        {
            Point coordsPoint = null;
            if (OrthoMode)
            {
                coordsPoint = _viewportLayout.MoveOrthoWorldPosition;
            }
            else
            {
                coordsPoint = _viewportLayout.MoveWorldPosition;
            }
            OnMouseMoveAfter(Math.Round(coordsPoint.X, 4), Math.Round(coordsPoint.Y, 4), Math.Round(0.0, 4));
        }

        #endregion

        #region >-------------- >----- Graphics & Drawing

        /// <summary>
        /// Disegna un path sul relativo layer 
        /// </summary>
        /// <param name="path">path da disegnare</param>
        /// <param name="idBlock">id del blocco</param>
        /// <param name="idPath">id del path</param>
        private void DrawPath(Path path, int idBlock, int idPath, bool redraw = true)
        {

            try
            {

                CLayer pathLayer = GetLayer(path.Layer);

                if (pathLayer == null) return;

                // Se già esiste, cancella prima l'esistente (necessario per gestione fori, per non duplicare i parents di tipo Fill)
                if (pathLayer.Fill)
                {
                    var ent =
                        _viewportLayout.Entities.FirstOrDefault(
                            e =>
                                e.EntityData.ToString() ==
                                idBlock.ToString(CultureInfo.InvariantCulture) + "#" +
                                idPath.ToString(CultureInfo.InvariantCulture) + "#-1");

                    if (ent != null)
                        DeleteEntities(idBlock, idPath, false);
                }

                // Se è un foro, ridisegna il path pieno che lo contiene
                if (pathLayer.UnFill)
                {
                    foreach (KeyValuePair<int,Path> kvp in mBlocks[idBlock].GetPaths())
                    {
                        Path innerPath = kvp.Value;
                        int innerPathId = kvp.Key;
                        var innerPathLayer = GetLayer(innerPath.Layer);
                        if (innerPathLayer != null && innerPathLayer.Fill && innerPath.Contains(path))
                        {
                            DrawPath(innerPath, idBlock, innerPathId, redraw);
                            return;
                        }
                    }
                    // Se non trovo parent, esco in ogni caso
                    return;
                }

                Layer layS = FindEyeLayer(path.Layer);

                if (layS == null)
                    return;

                if (!layS.Visible)
                    layS.Visible = true;

                _viewportLayout.ActiveLayer = layS;

                if (mBlocks[idBlock].mColor.Count > 0)
                {
                    _viewportLayout.ActiveColor = (mBlocks[idBlock].mColor.Values[idPath]);
                }
                else
                {
                    _viewportLayout.ActiveColor = layS.Color;
                }

                //Debug.Assert(path.Layer != "PARTITION_CUTS");


                CLayer cl = GetLayer(path.Layer);

                //if ((cl.Fill || cl.UnFill) /*&& path.IsClosed*/)
                if (cl != null && cl.Fill) 
                {
                    var poly = GetCompositeCurveFromPath(path, idBlock, idPath);

                    Region polyRegion = null;
                    if (cl.Fill)
                    {
                        if (cl.Name.ToUpperInvariant().Contains("SHAPE") || cl.Name.ToUpperInvariant().Contains("PIECE"))
                        {
                            //polyRegion = new ZTopRegion(poly, Plane.XY, 2f);
                            polyRegion = new ColorRegion(poly, Plane.XY);
                        }
                        else
                        {
                            polyRegion = new ZTopRegion(poly, Plane.XY, 1F);
                        }

                        // Ricerco eventuali fori all'interno di questo Fill
                        foreach (KeyValuePair<int, Path> kvp in mBlocks[idBlock].GetPaths())
                        {
                            Path innerPath = kvp.Value;
                            int innerPathId = kvp.Key;
                            var innerPathLayer = GetLayer(innerPath.Layer);
                            if (innerPathLayer != null && innerPathLayer.UnFill && path.Contains(innerPath))
                            {
                                var innerPoly = GetCompositeCurveFromPath(innerPath, idBlock, innerPathId);
                                innerPoly.Regen(1e-2);
                                var innerRegion = new Region(innerPoly, Plane.XY);
                                var difference = Region.Difference(polyRegion, innerRegion);
                                if (difference != null && difference.Length > 0)
                                {
                                    polyRegion = new ColorRegion(difference[0]);
                                }
                            }
                        }

                    }
                    else
                    {
                        //polyRegion = new ZTopRegion(poly, Plane.XY, 2f);
                        polyRegion = new ColorRegion(poly, Plane.XY);
                    }


                    polyRegion.ColorMethod = colorMethodType.byEntity;
                    polyRegion.LineWeightMethod = colorMethodType.byEntity;
                    //polyRegion.LineWeight = _viewportLayout.ActiveLayer.LineWeight;
                    polyRegion.LineWeight = 1.2F;


                    if (cl.Fill)
                    {
                        if (cl.Name.ToUpperInvariant().Contains("SLAB"))
                            polyRegion.Color = Color.FromArgb(150, _viewportLayout.ActiveColor);
                        else   
                            polyRegion.Color = Color.FromArgb(regionColorAlphaLevel, _viewportLayout.ActiveColor);
                    }
                    else
                    {
                        Color backColor = RenderContextUtility.ConvertColor(_viewportLayout.Viewports[0].Background.TopColor);
                        polyRegion.Color = Color.FromArgb(backColor.R + 1, backColor.G + 1, backColor.B + 1);

                    }

                    // De Conti 14/04/2016
                    polyRegion.Translate(0, 0, cl.ZLevel * _cLayerTranslactionFactor);

                    if (path.Attributes.ContainsKey("HATCHNAME"))
                    {
                        
                        AddHatchRegion(path, idBlock, idPath, polyRegion, cl);
                    }

                    polyRegion.Regen(1e-2);


                    polyRegion.EntityData = idBlock.ToString(CultureInfo.InvariantCulture) + "#" + idPath.ToString(CultureInfo.InvariantCulture) + "#-1";
                    _viewportLayout.Entities.Add(polyRegion, _viewportLayout.ActiveLayer.Name);


                    if (redraw)
                        _viewportLayout.Invalidate();
                }
                else
                {
                    for (int i = 0; i < path.Count; i++)
                    {
                        Side s = path.GetSideRT(i);

                        if (s is Arc)
                        {
                            var a = EyeDrawArc(s, redraw);
                            //compongo il codice univoco dell'entità
                            if (a!= null)
                                a.EntityData = idBlock.ToString(CultureInfo.InvariantCulture) + "#" + idPath.ToString(CultureInfo.InvariantCulture) + "#" + i.ToString(CultureInfo.InvariantCulture);
                        }
                        else 
                        {
                            {
                                var l = EyeDrawZTopLine(s, redraw);
                                //compongo il codice univoco dell'entit�
                                l.EntityData = idBlock.ToString(CultureInfo.InvariantCulture) + "#" + idPath.ToString(CultureInfo.InvariantCulture) + "#" + i.ToString(CultureInfo.InvariantCulture);
                            }

                        }
                    }
                }

                if (mLayers[path.Layer].Visible)
                    EnableEyeLayer(path.Layer, redraw);
                else
                    DisableEyeLayer(path.Layer);

                layS = FindEyeLayer("0");
                _viewportLayout.ActiveLayer = layS;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawPath ERROR: ", ex);
            }

        }

        private void AddHatchRegion(Path path, int idBlock, int idPath, Region polyRegion, CLayer cl)
        {
            var hr = new HatchRegion(polyRegion.ContourList);


            hr.HatchName = path.Attributes["HATCHNAME"];
            hr.Color = Color.Black;
            hr.ColorMethod = colorMethodType.byEntity;
            hr.LineWeight = polyRegion.LineWeight;
            hr.LineWeightMethod = polyRegion.LineTypeMethod;
            hr.HatchScale = 10;
            hr.Translate(0, 0, -cl.ZLevel * _cLayerTranslactionFactor);

            _viewportLayout.Entities.Add(hr, _viewportLayout.ActiveLayer.Name);

            var selectedEntities = _viewportLayout.Entities.Where(e => e.Selected);


            _viewportLayout.Entities.ClearSelection();

            var actualColor = _viewportLayout.SelectionColor;

            _viewportLayout.SelectionColor = polyRegion.Color;

            hr.Selected = true;

            //var acadList = new EntityList();
            //acadList.Add(hr);

            string fileName = "tempHatch.dwg";
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            var waA = new WriteAutodesk(_viewportLayout, fileName, lineWeightUnits:lineWeightUnitsType.Millimeters, selectedOnly: true);
            //var waA = new WriteAutodesk(acadList, new List<Layer>(), new Dictionary<string, devDept.Eyeshot.Block>(), new Dictionary<string, TextStyle>(), fileName,linearUnitsType.Millimeters);
            
            _viewportLayout.DoWork(waA);

            //var lastSelected =  _viewportLayout.Entities.Where(e => e.Selected).ToList();

            //foreach (var ent in lastSelected)
            //{
            //    ent.Selected = false;
            //}
            //_viewportLayout.Invalidate();



            //for (int i = lastSelected.Count - 1; i >= 0; i--)
            //{
            //    var entity = lastSelected[i];
            //    _viewportLayout.Entities.Remove(entity);

            //}



            _viewportLayout.Entities.DeleteSelected();

            _viewportLayout.SelectionColor = actualColor;

            foreach (var ent in selectedEntities)
            {
                ent.Selected = true;
            }

            //_viewportLayout.Clear();
            var ra1 = new ReadAutodesk(fileName, null, false, false);
            _viewportLayout.DoWork(ra1);

            foreach (var ent in ra1.Entities)
            {
                ent.EntityData = idBlock.ToString(CultureInfo.InvariantCulture) + "#" +
                                 idPath.ToString(CultureInfo.InvariantCulture) + "#-1";
            }
            ra1.AddToScene(_viewportLayout);
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

        }

        //private List<Entity> CreateBlockEntities(Path path, int idBlock, int idPath, bool redraw = true)
        //{

        //    try
        //    {

        //        CLayer pathLayer = GetLayer(path.Layer);

        //        if (pathLayer == null) return;

        //        // Se già esiste, cancella prima l'esistente (necessario per gestione fori, per non duplicare i parents di tipo Fill)
        //        if (pathLayer.Fill)
        //        {
        //            var ent =
        //                _viewportLayout.Entities.FirstOrDefault(
        //                    e =>
        //                        e.EntityData.ToString() ==
        //                        idBlock.ToString(CultureInfo.InvariantCulture) + "#" +
        //                        idPath.ToString(CultureInfo.InvariantCulture) + "#-1");

        //            if (ent != null)
        //                DeleteEntities(idBlock, idPath, false);
        //        }

        //        // Se è un foro, ridisegna il path pieno che lo contiene
        //        if (pathLayer.UnFill)
        //        {
        //            foreach (KeyValuePair<int,Path> kvp in mBlocks[idBlock].GetPaths())
        //            {
        //                Path innerPath = kvp.Value;
        //                int innerPathId = kvp.Key;
        //                var innerPathLayer = GetLayer(innerPath.Layer);
        //                if (innerPathLayer != null && innerPathLayer.Fill && innerPath.Contains(path))
        //                {
        //                    DrawPath(innerPath, idBlock, innerPathId, redraw);
        //                    return;
        //                }
        //            }
        //            // Se non trovo parent, esco in ogni caso
        //            return;
        //        }

        //        Layer layS = FindEyeLayer(path.Layer);

        //        if (layS == null)
        //            return;

        //        if (!layS.Visible)
        //            layS.Visible = true;

        //        _viewportLayout.ActiveLayer = layS;

        //        if (mBlocks[idBlock].mColor.Count > 0)
        //        {
        //            _viewportLayout.ActiveColor = (mBlocks[idBlock].mColor.Values[idPath]);
        //        }
        //        else
        //        {
        //            _viewportLayout.ActiveColor = layS.Color;
        //        }

        //        //Debug.Assert(path.Layer != "PARTITION_CUTS");


        //        CLayer cl = GetLayer(path.Layer);

        //        //if ((cl.Fill || cl.UnFill) /*&& path.IsClosed*/)
        //        if (cl != null && cl.Fill) 
        //        {
        //            var poly = GetCompositeCurveFromPath(path, idBlock, idPath);

        //            Region polyRegion = null;
        //            if (cl.Fill)
        //            {
        //                if (cl.Name.ToUpperInvariant().Contains("SHAPE"))
        //                {
        //                    //polyRegion = new ZTopRegion(poly, Plane.XY, 2f);
        //                    polyRegion = new ColorRegion(poly, Plane.XY);
        //                }
        //                else
        //                {
        //                    polyRegion = new ZTopRegion(poly, Plane.XY, 1F);
        //                }

        //                // Ricerco eventuali fori all'interno di questo Fill
        //                foreach (KeyValuePair<int, Path> kvp in mBlocks[idBlock].GetPaths())
        //                {
        //                    Path innerPath = kvp.Value;
        //                    int innerPathId = kvp.Key;
        //                    var innerPathLayer = GetLayer(innerPath.Layer);
        //                    if (innerPathLayer != null && innerPathLayer.UnFill && path.Contains(innerPath))
        //                    {
        //                        var innerPoly = GetCompositeCurveFromPath(innerPath, idBlock, innerPathId);
        //                        var innerRegion = new Region(innerPoly, Plane.XY);
        //                        var difference = Region.Difference(polyRegion, innerRegion);
        //                        if (difference != null && difference.Length > 0)
        //                        {
        //                            polyRegion = new ColorRegion(difference[0]);
        //                        }
        //                    }
        //                }

        //            }
        //            else
        //            {
        //                //polyRegion = new ZTopRegion(poly, Plane.XY, 2f);
        //                polyRegion = new ColorRegion(poly, Plane.XY);
        //            }


        //            polyRegion.ColorMethod = colorMethodType.byEntity;
        //            polyRegion.LineWeightMethod = colorMethodType.byEntity;
        //            //polyRegion.LineWeight = _viewportLayout.ActiveLayer.LineWeight;
        //            polyRegion.LineWeight = 6;


        //            if (cl.Fill)
        //            {
        //                polyRegion.Color = Color.FromArgb(100, _viewportLayout.ActiveColor);
        //            }
        //            else
        //            {
        //                Color backColor = _viewportLayout.Viewports[0].Background.TopColor;
        //                polyRegion.Color = Color.FromArgb(backColor.R + 1, backColor.G + 1, backColor.B + 1);

        //            }

        //            polyRegion.Regen(1e-2);

        //            polyRegion.EntityData = idBlock.ToString(CultureInfo.InvariantCulture) + "#" + idPath.ToString(CultureInfo.InvariantCulture) + "#-1";
        //            _viewportLayout.Entities.Add(polyRegion,_viewportLayout.ActiveLayer.Name);

        //            if (redraw)
        //                _viewportLayout.Invalidate();
        //        }
        //        else
        //        {
        //            for (int i = 0; i < path.Count; i++)
        //            {
        //                Side s = path.GetSideRT(i);

        //                if (s is Arc)
        //                {
        //                    var a = EyeDrawArc(s, redraw);
        //                    //compongo il codice univoco dell'entità
        //                    a.EntityData = idBlock.ToString(CultureInfo.InvariantCulture) + "#" + idPath.ToString(CultureInfo.InvariantCulture) + "#" + i.ToString(CultureInfo.InvariantCulture);
        //                }
        //                else 
        //                {
        //                    {
        //                        var l = EyeDrawZTopLine(s, redraw);
        //                        //compongo il codice univoco dell'entit�
        //                        l.EntityData = idBlock.ToString(CultureInfo.InvariantCulture) + "#" + idPath.ToString(CultureInfo.InvariantCulture) + "#" + i.ToString(CultureInfo.InvariantCulture);
        //                    }

        //                }
        //            }
        //        }

        //        if (mLayers[path.Layer].Visible)
        //            EnableEyeLayer(path.Layer);
        //        else
        //            DisableEyeLayer(path.Layer);

        //        layS = FindEyeLayer("0");
        //        _viewportLayout.ActiveLayer = layS;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("DrawPath ERROR: ", ex);
        //    }

        //}

        private Line EyeDrawLine(Side s1)
        {
            return EyeAddLine(s1.P1.X, s1.P1.Y, s1.P2.X, s1.P2.Y);
        }

        private ZTopLine EyeDrawZTopLine(Side s1, bool redraw = true)
        {
            return AddEyeZTopLine(s1.P1.X, s1.P1.Y, s1.P2.X, s1.P2.Y, redraw);
        }

        private devDept.Eyeshot.Entities.Arc EyeDrawArc(Side s1, bool redraw = true)
        {
            return EyeAddArc(s1.CenterPoint, s1.Radius, s1.StartAngle, s1.EndAngle, s1.AmplAngle, redraw);
        }


        private ZTopLine AddEyeZTopLine(double xFrom, double yFrom, double xTo, double yTo, bool redraw = true)
        {
            ZTopLine line = new ZTopLine(new Point3D(xFrom, yFrom, 0), new Point3D(xTo, yTo, 0));
            try
            {

                line.ColorMethod = colorMethodType.byEntity;
                line.Color = _viewportLayout.ActiveColor;

                line.SetColor(line.Color);


                line.LineWeightMethod = colorMethodType.byEntity;

                line.LineTypeMethod = colorMethodType.byLayer;

                if (_viewportLayout.ActiveLayer != null)
                {
                    int layerIndex = _viewportLayout.Layers.IndexOf(_viewportLayout.ActiveLayer);
                    line.LineWeight = _viewportLayout.ActiveLayer.LineWeight;

                    //if (_applyLayerZShift)
                    //    line.Translate(0, 0, layerIndex * LAYER_Z_SHIFT);
                    line.Translate(0, 0, layerIndex * _cLayerTranslactionFactor);

                    _viewportLayout.Entities.Add(line, layerIndex);
                    _viewportLayout.Entities.Regen();

                }
                else
                {
                    _viewportLayout.Entities.Add(line);
                }


                if (redraw)
                    _viewportLayout.Invalidate();

            }
            catch (Exception ex)
            {
                
            }

            return line;
        }

        private Line EyeAddLine(double xFrom, double yFrom, double xTo, double yTo)
        {
            Line line = new Line(new Point3D(xFrom, yFrom, 0), new Point3D(xTo, yTo, 0));
            try
            {

                line.ColorMethod = colorMethodType.byEntity;
                line.Color = _viewportLayout.ActiveColor;

                line.SetColor(line.Color);


                line.LineWeightMethod = colorMethodType.byEntity;

                line.LineTypeMethod = colorMethodType.byLayer;

                if (_viewportLayout.ActiveLayer != null)
                {
                    int layerIndex = _viewportLayout.Layers.IndexOf(_viewportLayout.ActiveLayer);
                    line.LineWeight = _viewportLayout.ActiveLayer.LineWeight;

                    //if (_applyLayerZShift)
                    //    line.Translate(0, 0, layerIndex * LAYER_Z_SHIFT);
                    line.Translate(0, 0, layerIndex * _cLayerTranslactionFactor);


                    _viewportLayout.Entities.Add(line, layerIndex);

                }
                else
                {
                    _viewportLayout.Entities.Add(line);
                }

                var comparer = new EntitySortByLayerIndex();
                _viewportLayout.Entities.Sort(comparer);
                _viewportLayout.Entities.Regen();

            }
            catch (Exception ex)
            {

            }

            return line;
        }


        /// <summary>
        /// Aggiunge un arco al documento attivo
        /// </summary>
        /// <param name="center">centro arco</param>
        /// <param name="radius">raggio</param>
        /// <param name="startAngle">angolo di partenza</param>
        /// <param name="endAngle">angolo finale</param>
        /// <returns></returns>
        private devDept.Eyeshot.Entities.Arc EyeAddArc(Point center, double radius, double startAngle, double endAngle, double amplAngle, bool redraw = true)
        {
            if (Math.Abs(radius) < 1e-6)
                return null;

            double start = amplAngle >= 0d ? startAngle : endAngle;
            double end = amplAngle >= 0d ? endAngle : startAngle;

            if (end < start)
            {
                end += Math.PI*2;
            }
            var arc = new devDept.Eyeshot.Entities.Arc(Plane.XY, new Point3D(center.X, center.Y, 0), radius, start, end);
            
            arc.Regen(1e-2);

            arc.ColorMethod = colorMethodType.byEntity;
            arc.Color = _viewportLayout.ActiveColor;

            arc.SetColor(arc.Color);


            arc.LineWeightMethod = colorMethodType.byEntity;

            arc.LineTypeMethod = colorMethodType.byLayer;


            if (_viewportLayout.ActiveLayer != null)
            {
                int layerIndex = _viewportLayout.Layers.IndexOf(_viewportLayout.ActiveLayer);
                arc.LineWeight = _viewportLayout.ActiveLayer.LineWeight;

                //if (_applyLayerZShift)
                //    arc.Translate(0, 0, layerIndex * LAYER_Z_SHIFT);
                arc.Translate(0, 0, layerIndex * _cLayerTranslactionFactor);


                //TODO: test
                arc.Translate(0, 0, 20);
                arc.Regen(1e-2);
                _viewportLayout.Entities.Add(arc, layerIndex);
            }
            else
            {
                _viewportLayout.Entities.Add(arc);
            }

            if (redraw)
                _viewportLayout.Invalidate();

            return arc;

        }

        /// <summary>
        /// Aggiunge un cerchio al documento attivo
        /// </summary>
        /// <param name="centerX">X centro</param>
        /// <param name="centerY">Y centro</param>
        /// <param name="radius">raggio</param>
        /// <returns></returns>
        private Circle EyeAddCircle(double centerX, double centerY, double radius)
        {

            Circle circle = new Circle(Plane.XY, new Point3D(centerX, centerY, 0), radius);
            circle.ColorMethod = colorMethodType.byEntity;
            circle.Color = _viewportLayout.ActiveColor;

            circle.SetColor(circle.Color);

            _viewportLayout.Entities.Add(circle);

            return circle;

        }

        /// <summary>
        /// Aggiunge un rettangolo al documento attivo
        /// </summary>
        /// <param name="posX">posizione X</param>
        /// <param name="posY">posizione Y</param>
        /// <param name="height">altezza</param>
        /// <param name="width">larghezza</param>
        /// <returns></returns>
        private RectangularRegion EyeAddRectangle(double posX, double posY, double height, double width)
        {
            var rect = new RectangularRegion(posX, posY, width, height);

            rect.ColorMethod = colorMethodType.byEntity;
            rect.Color = _viewportLayout.ActiveColor;

            rect.SetColor(rect.Color);


            _viewportLayout.Entities.Add(rect, _viewportLayout.ActiveLayer.Name);

            _viewportLayout.Invalidate();

            return rect;

        }


        #endregion

        /// <summary>
        /// Procedura di aggiunta di un testo 
        /// </summary>
        /// <param name="key">Chiave nella lista dei testi</param>
        /// <param name="ctext">CText da inserire</param>
        /// <param name="lineSpacing">Distanza tra righe in rapporto all'altezza riga</param>
        /// <param name="data">Dati addizionali</param>
        /// <param name="containingPath">Percorso all'interno del quale posizionare il testo</param>
        /// <param name="fitInsidePath">Mantiene in testo all'interno del Path, eventualmente ruotandolo di 90° antiorario</param>
        /// <returns></returns>
        private Text AddEyeText(int key, CText ctext, double lineSpacing = 1.25, object data = null, Path containingPath = null, bool fitInsidePath = false)
        {
            try
            {
                Text text = null;

                double z = 0;



                var bLayer = Layers.Values.FirstOrDefault(l => l.Name == ctext.Layer);
                if (bLayer != null)
                {
                    z = bLayer.ZLevel * _cLayerTranslactionFactor;
                }


                var content = ctext.Text.Split("\n".ToCharArray()[0]);
                if (content.Length > 1)
                {
                    string newContent = ctext.Text.Replace("\n", Environment.NewLine);
                    text = UseAntialiasingText 
                        ? new AntialiasingMultilineText(new Point3D(ctext.X, ctext.Y, 0), newContent, ctext.Text.Length * ctext.Dimension, ctext.Dimension, ctext.Dimension * lineSpacing) 
                        : new MultilineText(new Point3D(ctext.X, ctext.Y, 0), newContent, ctext.Text.Length * ctext.Dimension, ctext.Dimension, ctext.Dimension * lineSpacing);
                }
                else
                {
                    text = UseAntialiasingText 
                        ? new AntialiasingText(new Point3D(ctext.X, ctext.Y, 0), ctext.Text, ctext.Dimension) 
                        : new Text(new Point3D(ctext.X, ctext.Y, 0), ctext.Text, ctext.Dimension);
                }


                text.Color = ctext.Color;
                Layer layer = FindEyeLayer(ctext.Layer);
                if (layer != null)
                {
                    text.LayerIndex = _viewportLayout.Layers.IndexOf(layer);

                }
                text.EntityData = key.ToString(CultureInfo.InvariantCulture);

                text.Simplify = false;

                text.ColorMethod = colorMethodType.byEntity;

                text.Billboard = true;

                if (text is MultilineText)
                {
                    switch (ctext.Alignment)
                    {
                        case TextAlignment.Left:
                            text.Alignment = Text.alignmentType.MiddleLeft;
                            break;
                        case TextAlignment.Right:
                            text.Alignment = Text.alignmentType.MiddleRight;
                            break;
                        case TextAlignment.Center:
                            text.Alignment = Text.alignmentType.MiddleCenter;
                            break;
                    }
                }
                else
                {
                    text.Alignment = Text.alignmentType.MiddleCenter;
                }

                // 2160909
                if (fitInsidePath && containingPath != null)
                {
                    text.Regen(new RegenParams(1e-2, _viewportLayout));
                    var size = text.BoxSize;
                    Rect boundingPath = containingPath.GetRectangleRT();

                    bool isRotated = (ViewTopRotationAngle == 90 || ViewTopRotationAngle == 270);


                    double rotationAngle = -ViewTopRotationAngle / 180 * Math.PI + Math.PI / 2;

                    //if ((isRotated) && (_axisDirection == AxisDirection.LEFT_TOP || _axisDirection == AxisDirection.RIGHT_TOP))
                    //    rotationAngle += Math.PI;
                    if (_axisDirection == AxisDirection.LEFT_TOP || _axisDirection == AxisDirection.RIGHT_TOP)
                        rotationAngle += Math.PI;

                    if (isRotated)
                    {
                        if (size.X > boundingPath.Height)
                        {
                            if (size.X < boundingPath.Width || boundingPath.Width > boundingPath.Height)
                            {
                                switch (_axisDirection)
                                {
                                    case AxisDirection.LEFT_BOTTOM:
                                        break;

                                    case AxisDirection.LEFT_TOP:
                                        text.Backward = true;
                                        break;

                                    case AxisDirection.RIGHT_BOTTOM:
                                        text.TransformBy(new Mirror(new Plane(text.InsertionPoint, new Vector3D(0, 1, 0))));
                                        rotationAngle += Math.PI;
                                        break;

                                    case AxisDirection.RIGHT_TOP:
                                        break;
                                }

                                //if (_axisDirection == AxisDirection.LEFT_TOP ||
                                //    _axisDirection == AxisDirection.RIGHT_BOTTOM)
                                //    text.Backward = true;

                                text.Rotate(rotationAngle, new Vector3D(0, 0, 1), text.InsertionPoint);
                                text.Billboard = false;
                            }
                        }
                    }
                    else
                    {
                        if (size.X > boundingPath.Width)
                        {
                            if (size.X < boundingPath.Height || boundingPath.Height > boundingPath.Width)
                            {
                                switch (_axisDirection)
                                {
                                    case AxisDirection.LEFT_BOTTOM:
                                        break;

                                    case AxisDirection.LEFT_TOP:
                                        text.Backward = true;
                                        rotationAngle += Math.PI;
                                        break;

                                    case AxisDirection.RIGHT_BOTTOM:
                                        text.TransformBy(new Mirror(new Plane(text.InsertionPoint, new Vector3D(0, 1, 0))));
                                        break;

                                    case AxisDirection.RIGHT_TOP:
                                        break;
                                }

                                //if (_axisDirection == AxisDirection.LEFT_TOP ||
                                //   _axisDirection == AxisDirection.RIGHT_BOTTOM)
                                //    text.Backward = true;

                                text.Rotate(rotationAngle, new Vector3D(0, 0, 1), text.InsertionPoint);
                                text.Billboard = false;
                            }
                        }
                    }

                }

                

                //if (data != null)
                //{
                //    text.EntityData = data;
                //}


                ////switch (_axisDirection)
                ////{
                ////    case AxisDirection.LEFT_BOTTOM:
                ////        break;

                ////    case AxisDirection.LEFT_TOP:
                ////        text.UpsideDown = true;
                ////        break;

                ////    case AxisDirection.RIGHT_BOTTOM:
                ////        text.TransformBy(new Mirror(new Plane(text.InsertionPoint, new Vector3D(0, 1, 0))));
                ////        text.Rotate(Math.PI, new Vector3D(0, 0, 1), text.InsertionPoint);
                ////        break;

                ////    case AxisDirection.RIGHT_TOP:
                ////        text.Rotate(Math.PI, new Vector3D(0, 0, 1), text.InsertionPoint);
                ////        break;
                ////}

                ////// 20160909
                ////int invertRotationFactor = (_viewportLayout.AxisMode == AxisOrientation.LeftTop ||
                ////            _viewportLayout.AxisMode == AxisOrientation.RightBottom)
                ////            ? -1
                ////            : 1;
                ////bool isRotated = (ViewTopRotationAngle == 90 || ViewTopRotationAngle == 270);
                ////if (isRotated)
                ////{
                ////    var angleDegrees = - (ViewTopRotationAngle/180*Math.PI) * invertRotationFactor;
                ////    text.Rotate(angleDegrees, new Vector3D(0, 0, 1), text.InsertionPoint);
                ////}


                text.Translate(0, 0, z);


                _viewportLayout.Entities.Add(text, text.LayerIndex);

                //text.Regen(1e-2);
                //_viewportLayout.Entities.Regen();

                return text;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(" AddEyeText ERROR: ", ex);
                return null;
            }
        }

        private void SaveStartMatrix()
        {

            try
            {

                if (mSelections.Count == 0) return;

                _lastMatrix.Clear();

                foreach (CSelection cs in mSelections)
                {

                    Block b = mBlocks[cs.SelectedBlock];

                    if (b == null) continue;
                
                    double offX, offY, angle;
                    b.GetRotoTransl(out offX, out offY, out angle);
                    RTMatrix m = new RTMatrix();
                    m.SetRotoTransl(offX, offY, angle);
                    _lastMatrix.Add(m);
                    
                }

            }
            catch (Exception ex) { TraceLog.WriteLine("SaveStartMatrix ERROR: ", ex); }

        }

        /// <summary>
        /// Procedura di creazione di un layer
        /// </summary>
        /// <param name="clayer">layer da creare</param>
        /// <returns>Layer creato</returns>
        private Layer AddEyeLayer(CLayer clayer)
        {
            try
            {

                Layer layer = _viewportLayout.Layers.FirstOrDefault(l => l.Name == clayer.Name);

                if (layer == null)
                {
                    int layerIndex = _viewportLayout.Layers.Add(clayer.Name, clayer.Color);

                    layer = _viewportLayout.Layers[layerIndex];

                    if (clayer.LineType == "DASHDOT")
                    {
                        layer.LineTypeName = "Dash";
                    }

                    //vedere come gestire i tipi linea
                    if (clayer.LineType == "DASHDOT")
                        layer.LineTypeName = "Dash";

                    layer.LineWeight = (clayer.LineWidth > 0) ? (float)clayer.LineWidth / 30 : 0.01F;
                }

                return layer;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }
        }


        /// <summary>
        /// Imposta le proprietà del layer
        /// </summary>
        /// <param name="clayer"></param>
        /// <returns></returns>
        private Layer SetEyeLayer(CLayer clayer)
        {
            Layer layer = null;
            try
            {
                layer = _viewportLayout.Layers.FirstOrDefault(l => l.Name.Trim() == clayer.Name.Trim());

                if (layer != null)
                {
                    layer.Color = clayer.Color;

                    if (clayer.LineType == "DASHDOT")
                    {
                        layer.LineTypeName = "Dash";
                    }

                    layer.LineWeight = (clayer.LineWidth > 0) ? (float)clayer.LineWidth / 30 : 0.01F;

                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("SetEyeLayer ERROR: ", ex);
            }

            return layer;
        }


        /// <summary>
        /// Procedura di ricerca di un layer data la chiave
        /// </summary>
        /// <param name="layerName">layer name</param>
        /// <returns>Layer trovato</returns>
        private Layer FindEyeLayer(string layerName)
        {
            Layer foundLayer = null;
            try
            {
                foundLayer =_viewportLayout.Layers.FirstOrDefault(l => l.Name == layerName);
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + " FindEyeLayer error : ", ex);
            }

            return foundLayer;
        }

        /// <summary>
        /// Procedura di spegnimento di un layer
        /// </summary>
        /// <param name="strLayer">layer name</param>
        /// <returns>true: il layer è stato spento</returns>
        private bool DisableEyeLayer(string strLayer)
        {
            try
            {
                bool bolDisabilitato = false;
                bool wasFrozen = false;


                Layer layer = FindEyeLayer(strLayer);

                if (layer != null)
                {
                    bolDisabilitato = true;
                    wasFrozen = !layer.Visible;

                    layer.Visible = false;
                }

                if (!mLayers[strLayer].IsDrawn || !wasFrozen)
                {
                    Redraw();
                    mLayers[strLayer].IsDrawn = true;
                }

                return bolDisabilitato;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisableEyeLayer ERROR: ", ex);
                return false;
            }
        }


        /// <summary>
        /// Procedura di abilitazione (visualizzazione) di un layer 
        /// </summary>
        /// <param name="strLayer">layer name</param>
        /// <param name="redraw"></param>
        /// <returns>true: il layer è visibile</returns>
        private bool EnableEyeLayer(string strLayer, bool redraw = true)
        {
            try
            {
                bool bolAbilitato = false;
                bool wasFrozen = true;

                _viewportLayout.ActiveLayer = FindEyeLayer("0");

                Layer layer = FindEyeLayer(strLayer);

                if (layer != null)
                {
                    bolAbilitato = true;
                    wasFrozen = !layer.Visible;
                    layer.Visible = true;
                }

                if (!mLayers[strLayer].IsDrawn || wasFrozen)
                {
                    if (redraw) Redraw();
                    mLayers[strLayer].IsDrawn = true;
                }

                return bolAbilitato;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("EnableEyeLayer ERROR: ", ex);
                return false;
            }
        }

        /// <summary>
        /// Restituisce il punto di snap relativo a un punto
        /// </summary>
        /// <param name="mouseLocation">punto </param>
        /// <returns></returns>
        private DraftingViewportLayout.SnapPoint GetSnapPoint(System.Drawing.Point mouseLocation)
        {

            List<DraftingViewportLayout.SnapPoint> snapPoints = _viewportLayout.GetSnapPoints(mouseLocation);

            var snapPoint = _viewportLayout.FindClosestPoint(snapPoints, mouseLocation);
            return snapPoint;
        }

        private static CompositeCurve GetCompositeCurveFromPath(Path path, int idBlock, int idPath)
        {
            //TraceLog.WriteLine("GetCompositeCurveFromPath START");

            CompositeCurve poly = new CompositeCurve();

            for (int i = 0; i < path.Count; i++)
            {
                Side s = path.GetSideRT(i);

                //TraceLog.WriteLine(string.Format("P1: X={0}, Y={1};  P2: X={2}, Y={3}", s.P1.X, s.P1.Y, s.P2.X, s.P2.Y));

                if (s is Arc)
                {
                    if (s.Radius > 1e-6)
                    {
                        devDept.Eyeshot.Entities.Arc arco;

                        var center = new Point3D(s.CenterPoint.X, s.CenterPoint.Y);
                        double startAngle = s.StartAngle;
                        double endAngle = s.EndAngle;

                        if (s.AmplAngle < 0d)
                        {
                            startAngle = s.EndAngle;
                            endAngle = s.StartAngle;
                        }
                        while (startAngle > endAngle)
                        {
                            endAngle += Math.PI * 2;
                        }

                        arco = new devDept.Eyeshot.Entities.Arc(center, s.Radius, startAngle, endAngle);
                        poly.CurveList.Add(arco);
                    }

                }
                else
                {
                    Line line = new Line(new Point3D(s.P1.X, s.P1.Y), new Point3D(s.P2.X, s.P2.Y));

                    poly.CurveList.Add(line);
                }
            }

			poly.SortAndOrient(CURVES_CHORDAL_ERROR);
			
			poly.Regen(CURVES_CHORDAL_ERROR);

            //TraceLog.WriteLine("GetCompositeCurveFromPath END");

            return poly;
        }

        #endregion

        #region >-------------- >----- Viewport layout event handlers

        void _viewportLayout_MouseWheel(object sender, MouseWheelEventArgs e)
        {
			_regenTimer.Stop();
			_regenTimer.Start();
        }

        void _viewportLayout_DragOver(object sender, System.Windows.DragEventArgs e)
        {
            //_viewportLayout.renderContext.DrawLine(new Point2D(0,0), new Point2D(200,200));
            //_viewportLayout.MoveDragMouseLocation = new System.Drawing.Point(e.X, e.Y);

            _viewportLayout.ForcePaint();
            //_viewportLayout.Invalidate();
            //_viewportLayout.renderContext.DrawCurrentBuffer();
            //if (dragdrop)
            //{
            //    _viewportLayout.DrawDragEntities();
            //    _viewportLayout.Invalidate();
            //}
            //throw new NotImplementedException();
        }

        void _viewportLayout_DragDrop(object sender, System.Windows.DragEventArgs e)
        {
            if (_blockDragDrop == null) return;
            try
            {

                //var point = CursorPosition();
                var point = DragMoveCursorPosition();

                Point3D punto = _viewportLayout.CurrentWorldPoint;

                basicPaintEventArgs = new CBasicPaintEventArgs(null);

                double angolo = _blockDragDrop.RotAngle;

                _blockDragDrop.MatrixRT.Reset();

                _blockDragDrop.RotoTranslRel(-_xDragDrop, -_yDragDrop, 0, 0, 0);
                _blockDragDrop.RotoTranslRel(point.X, point.Y, 0, 0, angolo);

                OnDragDropEvent(basicPaintEventArgs, _blockDragDrop);

                _xDragDrop = 0;
                _yDragDrop = 0;

                Redraw();

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
            }
            finally
            {
            }
        }

        void _viewportLayout_DragLeave(object sender, EventArgs e)
        {
            try
            {
                basicPaintEventArgs = new CBasicPaintEventArgs(null);
                OnDragLeaveEvent(basicPaintEventArgs);

                if (!basicPaintEventArgs.Accepted)
                {
                    //Cancel = true;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DragLeave ERROR: ", ex);
            }
        }

        void _viewportLayout_DragEnter(object sender, System.Windows.DragEventArgs e)
        {
            try
            {
                e.Effects = System.Windows.DragDropEffects.Copy;
                basicPaintEventArgs = new CBasicPaintEventArgs(null);
                OnDragEnterEvent(basicPaintEventArgs);

                if (!basicPaintEventArgs.Accepted)
                {
                    e.Effects = System.Windows.DragDropEffects.None;
                    _viewportLayout.ShowDrag = false;
                    if (_viewportLayout.DragEntityList != null)
                    {
                        _viewportLayout.DragEntityList.Clear();
                    }
                }
                else
                {

                    DragBlockInfo content = e.Data.GetData(typeof(DragBlockInfo)) as DragBlockInfo;
                    if (content != null)
                    {
                        // De Conti 28/04/2016
                        //if (sender != content.DragSource)
                        {

                            //var current = new System.Drawing.Point(e.X, e.Y);
                            var current = RenderContextUtility.ConvertPoint(e.GetPosition(_viewportLayout));

                            //current = _viewportLayout.PointToClient(current);

                            Point3D p;
                            _viewportLayout.ScreenToPlane(current, Plane.XY, out p);

                            _viewportLayout.StartDraggingPoint = p;
                            _viewportLayout.StartDraggingPoint = (Point3D)content.Offset.Clone();

                            _viewportLayout.Dragging = true;

                            _viewportLayout.DragEntityList = new List<Entity>(content.DragEntities);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DragEnter ERROR: ", ex);
            }
        }

        void _viewportLayout_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var mousePos = RenderContextUtility.ConvertPoint(_viewportLayout.GetMousePosition(e));
            try
            {
                _viewportLayout.Dragging = false;


                //recupero il punto cliccato dall'utente
                Point3D punto;
                _viewportLayout.ScreenToPlane(mousePos, Plane.XY, out punto);

                Breton.Polygons.Point p = new Breton.Polygons.Point(punto.X, punto.Y);

                if (_zoomWindow && _startZoomWindowPoint != null)
                {
                    DeleteGenericRectangle();
                    ZoomWindow(_startZoomWindowPoint, p);
                    _startZoomWindowPoint = null;
                    _zoomWindow = false;
                }

                if (e.ChangedButton == MouseButton.Left)
                    basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Left);
                else if (e.ChangedButton == MouseButton.Right)
                    basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Right);
                else if (e.ChangedButton == MouseButton.Middle)
                    basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Center);
                else //if (e.Button == MouseButtons.None)
                    basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);


                if (e.ChangedButton == MouseButton.Right)
                    _viewportLayout.DrawingDimension = false;

                if (OrthoMode)
                {
                    _viewportLayout.OrthoModeStarted = false;
                }



                List<int> lb = new List<int>();
                for (int i = 0; i < mSelections.Count; i++)
                    lb.Add(mSelections[i].SelectedBlock);

                if (move && currentAction == Action.MOVE)
                {
                    if (e.ChangedButton == MouseButton.Left)
                    {
                        for (int i = 0; i < mSelections.Count; i++)
                            Select(mSelections[i].SelectedBlock);

                        // Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
                        OnEndMoveEvent(basicPaintEventArgs, lb, null, null, null);

                        _viewportLayout.MoveRotateRubberbandLine.DrawEntity = false;
                        currentAction = Action.DEFAULT;
                    }
                }
                else if (rotate && currentAction == Action.ROTATE)
                {
                    if (e.ChangedButton == MouseButton.Left)
                    {
                        for (int i = 0; i < mSelections.Count; i++)
                            Select(mSelections[i].SelectedBlock);

                        _viewportLayout.MoveRotateRubberbandLine.DrawEntity = false;
                        DeleteGenericLine(false);
                        DeleteGenericArc(false);

                        Redraw();

                        // Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
                        OnEndRotateEvent(basicPaintEventArgs, lb, null, null, null);

                        currentAction = Action.DEFAULT;
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint.ViewportLayout_MouseUp: ", ex);
            }
        }

        //void _viewportLayout_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if ((_viewportLayout.ToolBar.Visible && _viewportLayout.ToolBar.Contains(e.Location))
        //        || _viewportLayout.ActionMode != actionType.None) return;
        //    try
        //    {
        //        List<int> lb = new List<int>();
        //        Breton.Polygons.Point bretonCurrentPoint = null;
        //        List<int> lidblock = new List<int>();
        //        List<int> lidpath = new List<int>();
        //        List<int> lidside = new List<int>();
        //        List<int> lidpoint = new List<int>();

        //        //recupero il punto cliccato dall'utente
        //        if (currentAction == Action.DEFAULT)
        //        {
        //            bretonCurrentPoint = _viewportLayout.DownWorldPosition;
        //            _viewportLayout.StartDraggingPoint = new Point3D(_viewportLayout.DownWorldPosition.X,
        //                _viewportLayout.DownWorldPosition.Y);

        //            //Verifico se è attiva la funzione di ZoomWindow
        //            if (_zoomWindow)
        //            {
        //                _startZoomWindowPoint = bretonCurrentPoint;
        //                return;
        //            }

        //            if (e.Button == MouseButtons.Left)
        //                basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Left);
        //            else if (e.Button == MouseButtons.Right)
        //                basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Right);
        //            else if (e.Button == MouseButtons.Middle)
        //                basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Center);
        //            else if (e.Button == MouseButtons.None)
        //                basicPaintEventArgs = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

        //            _viewportLayout.Selections.Clear();

        //            if (SelectionEnabled)
        //            {
        //                _selActive = new EyeSelection("MYSEL");
        //                _viewportLayout.Selections.Add(_selActive);

        //                int[] entitiesIndexes = _viewportLayout.GetEntitiesUnderCursor(_viewportLayout.DownMouseLocation);

        //                for (int i = 0; i < entitiesIndexes.Length; i++)
        //                {
        //                    Entity ent = _viewportLayout.Entities[entitiesIndexes[i]];
        //                    if (!(ent is Picture))
        //                    {
        //                        _selActive.Add(ent);
        //                    }
        //                }

        //                // Identifica tutte le entità a cui il punto cliccato appartiene
        //                GetElementsBlockFromPoint(bretonCurrentPoint, _selActive, out lidblock, out lidpath, out lidside,
        //                    out lidpoint);

        //                OnClickEvent(basicPaintEventArgs, e.Button, lidblock, lidpath, lidside, lidpoint);

        //                for (int i = 0; i < mSelections.Count; i++)
        //                    lb.Add(mSelections[i].SelectedBlock);
        //            }
        //            else
        //            {
        //                OnClickEvent(basicPaintEventArgs, e.Button, lidblock, lidpath, lidside, lidpoint);
        //            }
        //        }
        //        else
        //            return;

        //        if (OrthoMode)
        //        {
        //            _viewportLayout.OrthoModeStarted = true;
        //        }

        //        //clicco per iniziare un movimento

        //        #region MOVE

        //        if (move && currentAction == Action.DEFAULT && e.Button == MouseButtons.Left)
        //        {
        //            OnStartMoveEvent(basicPaintEventArgs, lb, null, null, null);

        //            if (basicPaintEventArgs.Accepted)
        //            {
        //                currentAction = Action.MOVE;

        //                //memorizzo i punti iniziali e le matrici
        //                SaveStartMatrix();
        //                _userPoint = bretonCurrentPoint;
        //                _lastOkPoint = bretonCurrentPoint;

        //                _viewportLayout.MoveRotateRubberbandLine.StartPoint = new Point2D(bretonCurrentPoint.X,
        //                    bretonCurrentPoint.Y);
        //                _viewportLayout.MoveRotateRubberbandLine.DrawColor = _viewportLayout.Palette[6];
        //                _viewportLayout.MoveRotateRubberbandLine.EndPoint = new Point2D(bretonCurrentPoint.X,
        //                    bretonCurrentPoint.Y);
        //                _viewportLayout.MoveRotateRubberbandLine.DottedBorder = true;
        //                _viewportLayout.MoveRotateRubberbandLine.DrawEntity = true;


        //                _selActive = new EyeSelection("Blocks");
        //                EyeSelection selTmp = null;

        //                List<int> idBlocks = new List<int>();

        //                // Prepara la lista univoca di blocchi
        //                foreach (CSelection cs in mSelections)
        //                {
        //                    if (!idBlocks.Contains(cs.SelectedBlock))
        //                        idBlocks.Add(cs.SelectedBlock);
        //                }

        //                // Scorre tutti i blocchi e prepara la vdSelection
        //                foreach (int idB in idBlocks)
        //                {
        //                    selTmp = SelectEntities(idB, -1, -1, -1);

        //                    if (selTmp != null)
        //                    {
        //                        for (int i = 0; i < selTmp.Count; i++)
        //                        {
        //                            _selActive.AddItem(selTmp[i], true);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                //ripristino le matrici iniziali
        //                for (int i = 0; i < lb.Count; i++)
        //                {

        //                    Block b = mBlocks[lb[i]];
        //                    RTMatrix m = _lastMatrix[i];
        //                    double offX, offY, angolo;
        //                    m.GetRotoTransl(out offX, out offY, out angolo);
        //                    b.SetRotoTransl(offX, offY, angolo);
        //                    b = mBlocks[lb[i]];

        //                    Repaint(b);
        //                    currentAction = Action.DEFAULT;
        //                }
        //            }
        //        } 
        //            #endregion MOVE

        //            //clicco per iniziare una rotazione
        //            #region ROTATE

        //        else if (rotate && currentAction == Action.DEFAULT && e.Button == MouseButtons.Left)
        //        {
        //            _canStartRotate = false;
        //            _rotationReferenceSegment = null;

        //            OnStartRotateEvent(basicPaintEventArgs, lb, null, null, null);

        //            if (basicPaintEventArgs.Accepted)
        //            {

        //                currentAction = Action.ROTATE;

        //                //memorizzo i punti iniziali e le matrici
        //                SaveStartMatrix();
        //                _userPoint = bretonCurrentPoint;
        //                _lastOkPoint = bretonCurrentPoint;


        //                _viewportLayout.MoveRotateRubberbandLine.StartPoint = new Point2D(bretonCurrentPoint.X,
        //                    bretonCurrentPoint.Y);
        //                _viewportLayout.MoveRotateRubberbandLine.DrawColor = _viewportLayout.Palette[6];
        //                _viewportLayout.MoveRotateRubberbandLine.EndPoint = new Point2D(bretonCurrentPoint.X,
        //                    bretonCurrentPoint.Y);
        //                _viewportLayout.MoveRotateRubberbandLine.DottedBorder = true;

        //                _viewportLayout.MoveRotateRubberbandLine.DrawEntity = true;


        //                _selActive = new EyeSelection("Blocks");
        //                EyeSelection selTmp = null;



        //                List<int> idBlocks = new List<int>();

        //                // Prepara la lista univoca di blocchi
        //                foreach (CSelection cs in mSelections)
        //                {
        //                    if (!idBlocks.Contains(cs.SelectedBlock))
        //                        idBlocks.Add(cs.SelectedBlock);
        //                }

        //                foreach (int idB in idBlocks)
        //                {
        //                    selTmp = SelectEntities(idB, -1, -1, -1);

        //                    if (selTmp != null)
        //                    {
        //                        for (int i = 0; i < selTmp.Count; i++)
        //                        {
        //                            _selActive.AddItem(selTmp[i], true);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                //ripristino le matrici iniziali
        //                for (int i = 0; i < lb.Count; i++)
        //                {

        //                    Block b = mBlocks[lb[i]];
        //                    RTMatrix m = _lastMatrix[i];
        //                    double offX, offY, angolo;
        //                    m.GetRotoTransl(out offX, out offY, out angolo);
        //                    b.MatrixRT.SetRotoTransl(offX, offY, angolo);
        //                    b = mBlocks[lb[i]];

        //                    Repaint(b);
        //                    currentAction = Action.DEFAULT;
        //                }
        //            }
        //        } 

        //            #endregion ROTATE

        //            //selezione (generalmente per il drag & drop)
        //        else if (!move && !rotate)
        //        {
        //            if (basicPaintEventArgs.Accepted && dragdrop)
        //            {
        //                //Cancel = true;
        //                PrepareAndStartDrag(lidblock, bretonCurrentPoint);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("_viewportLayout_MouseDown ERROR: ", ex);
        //    }
        //}

        public void PrepareAndStartDrag(Block block, double x, double y)
        {
            UnSelect();

            if (BlockDragDrop != null)
            {

                if (_selActive.Count > 0)
                    _selActive.Clear();

                EyeSelection selTmp = null;
                selTmp = SelectEntities(BlockDragDrop.Id, -1, -1, -1);

                if (selTmp != null && selTmp.Count != 0)
                {
                    //calcolo il punto cliccato dall'utente con la matrice inversa, per averlo rispetto al path in origine
                    Block b = block;
                    if (b.AllowDragDrop)
                    {
                        //BlockDragDrop = mBlocks[lidblock[blockIndex]];

                        RTMatrix mInv = b.MatrixRT.InvMat();

                        mInv.PointTransform(x, y, out _xDragDrop, out _yDragDrop);

                        for (int j = 0; j < selTmp.Count; j++)
                        {
                            _selActive.AddItem(selTmp[j], true);
                        }

                        //TODO: DragDrop

                        DragEntityList = new List<Entity>(_selActive);

                        //DragBlockInfo info = new DragBlockInfo(DragEntityList, _viewportLayout.StartDraggingPoint, _viewportLayout);
                        DragBlockInfo info = new DragBlockInfo(DragEntityList, new Point3D(x, y, 0), _viewportLayout);
                        _viewportLayout.Dragging = true;

                        //_viewportLayout.StartDraggingPoint = new Point3D(x, y, 0);

                        //_viewportLayout.Dragging = true;
                        System.Windows.DragDrop.DoDragDrop(_viewportLayout, info, System.Windows.DragDropEffects.Copy);
                    }
                }
            }
            else
            {
                
            }

        }

        private void PrepareAndStartDrag(List<int> lidblock, Point bretonCurrentPoint)
        {
            int blockIndex = 0;


            if (BlockDragDrop == null)
            {
                //if (lidblock.Count > 0
                //    //&& _lastSelectedBlocks.Count == lidblock.Count 
                //    && lidblock.Contains(_lastSelectedBlock))
                //{
                //    blockIndex = lidblock.IndexOf(_lastSelectedBlock);
                //    if (lidblock.Count > blockIndex + 1)
                //    {
                //        blockIndex++;
                //    }
                //    else
                //    {
                //        blockIndex = 0;
                //    }
                //}
                //_lastSelectedBlock = lidblock[blockIndex];

                if (_selActive.Count > 0)
                    _selActive.Clear();

                EyeSelection selTmp = null;
                if (lidblock.Count > 0 && lidblock.Contains(blockIndex))
                    selTmp = SelectEntities(lidblock[blockIndex], -1, -1, -1);

                if (selTmp != null && selTmp.Count != 0)
                {
                    //calcolo il punto cliccato dall'utente con la matrice inversa, per averlo rispetto al path in origine
                    Block b = mBlocks[lidblock[blockIndex]];
                    if (b.AllowDragDrop)
                    {
                        //BlockDragDrop = mBlocks[lidblock[blockIndex]];

                        RTMatrix mInv = b.MatrixRT.InvMat();
                        mInv.PointTransform(bretonCurrentPoint.X, bretonCurrentPoint.Y, out _xDragDrop, out _yDragDrop);

                        for (int j = 0; j < selTmp.Count; j++)
                        {
                            _selActive.AddItem(selTmp[j], true);
                        }

                        //TODO: DragDrop

                        DragEntityList = new List<Entity>(_selActive);

                        DragBlockInfo info = new DragBlockInfo(DragEntityList, _viewportLayout.StartDraggingPoint, _viewportLayout);
                        _viewportLayout.Dragging = true;


                        ////_viewportLayout.DoDragDrop(_selActive, DragDropEffects.Copy);
                        System.Windows.DragDrop.DoDragDrop(_viewportLayout, info, System.Windows.DragDropEffects.Copy);
                        //_viewportLayout.StartDraggingPoint = new Point3D(_xDragDrop, _yDragDrop);
                    }
                }
            }
            else
            {
                if (_selActive.Count > 0)
                    _selActive.Clear();

                EyeSelection selTmp = null;

                selTmp = SelectEntities(BlockDragDrop.Id, -1, -1, -1);

                if (selTmp != null && selTmp.Count != 0)
                {
                    //calcolo il punto cliccato dall'utente con la matrice inversa, per averlo rispetto al path in origine
                    Block b = mBlocks[BlockDragDrop.Id];
                    if (b.AllowDragDrop)
                    {

                        RTMatrix mInv = b.MatrixRT.InvMat();
                        mInv.PointTransform(bretonCurrentPoint.X, bretonCurrentPoint.Y, out _xDragDrop, out _yDragDrop);

                        for (int j = 0; j < selTmp.Count; j++)
                        {
                            _selActive.AddItem(selTmp[j], true);
                        }

                        //TODO: DragDrop

                        DragEntityList = new List<Entity>(_selActive);

                        DragBlockInfo info = new DragBlockInfo(DragEntityList, _viewportLayout.StartDraggingPoint, _viewportLayout);
                        _viewportLayout.Dragging = true;

                        //_viewportLayout.DoDragDrop(info, DragDropEffects.Copy);
                        System.Windows.DragDrop.DoDragDrop(_viewportLayout, info, System.Windows.DragDropEffects.Copy);
                    }
                }
            }





        }

        public DragBlockInfo GetDragBlockInfo(Block block )
        {
            DragBlockInfo info = null;
            EyeSelection sel = SelectEntities(block.Id, -1, -1, -1);
            var dragEntityList = new List<Entity>(sel);
            info = new DragBlockInfo(dragEntityList, new Point3D(block.TranslPoint.X, block.TranslPoint.Y), _viewportLayout);
            return info;
        }

        void _viewportLayout_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                System.Drawing.Point mousePosition = e.Location;

                DraftingViewportLayout.SnapPoint snapPoint;

                Point3D currentWorldPosition;
                _viewportLayout.ScreenToPlane(mousePosition, Plane.XY, out currentWorldPosition);

                if (_zoomWindow && _startZoomWindowPoint != null)
                {
                    DrawGenericRectangle(_startZoomWindowPoint.X, _startZoomWindowPoint.Y,
                        currentWorldPosition.Y - _startZoomWindowPoint.Y,
                        currentWorldPosition.X - _startZoomWindowPoint.X,
                        Viewer.ZoomWindowFill, Viewer.ZoomWindowColor, Viewer.ZoomWindowShowBorder,
                        Viewer.ZoomWindowDottedBorder);

                    NotifyCursorCoordinates();

                    return;
                }

                if (_osnapModes.Count > 0)
                {
                    snapPoint = GetSnapPoint(mousePosition);

                    if (snapPoint != null)
                    {
                        
                    }

                    _lastSnapPoint = snapPoint;

                }

                if (!move && !rotate)
                {
                    NotifyCursorCoordinates();

                    //if (_viewportLayout.Dragging)
                    //{
                    //    _viewportLayout.DrawDragEntities();
                    //}
                    return;
                }

                #region MOVE

                if (move && currentAction == Action.MOVE)
                {
                    if (mSelections.Count > 0)
                    {
                        Point3D punto = (Point3D) currentWorldPosition.Clone();

                        // Verifica se siamo in modalità Ortho
                        if (OrthoMode)
                        {
                            punto.X = _viewportLayout.MoveOrthoWorldPosition.X;
                            punto.Y = _viewportLayout.MoveOrthoWorldPosition.Y;
                        }
                        ////{
                        ////    if (Math.Abs(punto.X - _userPoint.X) > Math.Abs(punto.Y - _userPoint.Y))
                        ////        punto.Y = _userPoint.Y;
                        ////    else
                        ////        punto.X = _userPoint.X;
                        ////}

                        double deltaX = punto.X - _lastOkPoint.X;
                        double deltaY = punto.Y - _lastOkPoint.Y;

                        _lastOkPoint = new Point(punto.X, punto.Y);

                        List<int> lb = new List<int>();

                        foreach (CSelection cs in mSelections)
                        {
                            Block b = mBlocks[cs.SelectedBlock];
                            if (b.AllowMove)
                            {
                                lb.Add(b.Id);
                                b.MatrixRT.Save();
                                b.RotoTranslRel(deltaX, deltaY, 0d, 0d, 0d);
                            }
                        }

                        OnMoveEvent(basicPaintEventArgs, lb, null, null, null);

                        if (!basicPaintEventArgs.Accepted)
                        {
                            foreach (CSelection cs in mSelections)
                            {
                                Block b = mBlocks[cs.SelectedBlock];
                                // De Conti 03/11/2016
                                if (b.AllowMove)
                                    b.MatrixRT.Restore();
                            }
                        }
                        else
                        {
                            EyeSelection moveSelection = new EyeSelection("Move");
                            foreach (int idBlock in lb)
                            {
                                var selTmp = SelectEntities(idBlock, -1, -1, -1);

                                if (selTmp != null)
                                    for (int i = 0; i < selTmp.Count; i++)
                                        moveSelection.AddItem(selTmp[i], true);
                            }

                            moveSelection.Translate(deltaX, deltaY, 0);
                            //_selActive.Translate(deltaX, deltaY, 0);


                            _viewportLayout.MoveRotateRubberbandLine.EndPoint = new Point2D(_lastOkPoint.X,
                                _lastOkPoint.Y);
                            _viewportLayout.Entities.Regen();
                            _viewportLayout.Invalidate();

                        }
                    }
                } 
                    #endregion MOVE

                #region ROTATE

                else if (rotate && currentAction == Action.ROTATE)
                {
                    Point3D punto = (Point3D) currentWorldPosition.Clone();
                    if (OrthoMode)
                    {
                        punto.X = _viewportLayout.MoveOrthoWorldPosition.X;
                        punto.Y = _viewportLayout.MoveOrthoWorldPosition.Y;
                    }

                    Point3D pivotPoint = new Point3D(_userPoint.X, _userPoint.Y);
                    Point3D pPrec = new Point3D(_lastOkPoint.X, _lastOkPoint.Y);
                    double rotationAngle = 0;

                    _viewportLayout.MoveRotateRubberbandLine.EndPoint.X = punto.X;
                    _viewportLayout.MoveRotateRubberbandLine.EndPoint.Y = punto.Y;

                    List<System.Drawing.Point> screenPoints = new List<System.Drawing.Point>(){new System.Drawing.Point(0,0), new System.Drawing.Point((int)START_ROTATE_MINIMUM_DISTANCE,0)};
                    var worldPoints = _viewportLayout.ScreenToPlane(screenPoints, Plane.XY);
                    double worldStartRotateMinimumDistance = worldPoints[0].DistanceTo(worldPoints[1]);

                    if (!_canStartRotate)
                    {
                        _canStartRotate = _viewportLayout.MoveRotateRubberbandLine.StartPoint.DistanceTo(
                            _viewportLayout.MoveRotateRubberbandLine.EndPoint) > worldStartRotateMinimumDistance;
                        // Define reference segment
                        if (_canStartRotate)
                        {
                            _rotationReferenceSegment = new Segment(_userPoint, new Point(punto.X, punto.Y));
                            //_rotationReferenceSegment = new Segment(_userPoint, new Point(punto.X + worldStartRotateMinimumDistance, _userPoint.Y));
                        }
                    }

                    if (_canStartRotate)
                    {
                        _viewportLayout.MoveRotateRubberbandLine.DrawColor = _viewportLayout.Palette[6];
                        if (_rotationReferenceSegment != null)
                        {
                            double distance = _viewportLayout.MoveRotateRubberbandLine.StartPoint.DistanceTo(
                                _viewportLayout.MoveRotateRubberbandLine.EndPoint) + worldStartRotateMinimumDistance;// * 10;
                            //referenceSegment = new Segment(referenceSegment.P1,
                            //    referenceSegment.PointAtDistanceFromStart(distance));
                            _rotationReferenceSegment.Extend(Side.EN_VERTEX.EN_VERTEX_END, distance - _rotationReferenceSegment.Length);
                            DrawGenericLine(_rotationReferenceSegment, _rotateReferenceColor, true, false);
                            DrawGenericArc(_userPoint,
                                _rotationReferenceSegment.PointAtDistanceFromStart(_viewportLayout.MoveRotateRubberbandLine.StartPoint.DistanceTo(_viewportLayout.MoveRotateRubberbandLine.EndPoint)),
                                punto, _rotateReferenceColor, true, false);
                        }
                    }
                    else
                        _viewportLayout.MoveRotateRubberbandLine.DrawColor = _rotateReferenceColor;


                    if (mSelections.Count > 0 && _canStartRotate)
                    {

                        //{
                        //    if (Math.Abs(punto.X - _userPoint.X) > Math.Abs(punto.Y - _userPoint.Y))
                        //        punto.Y = _userPoint.Y;
                        //    else
                        //        punto.X = _userPoint.X;
                        //}

                        var vect1 = new Vector2D(pivotPoint, punto);
                        var vect2 = (pPrec != pivotPoint) ? new Vector2D(pivotPoint, pPrec) : vect1;
                        rotationAngle = Vector2D.SignedAngleBetween(vect2, vect1);


                        _lastOkPoint = new Point(punto.X, punto.Y);

                        List<int> lb = new List<int>();

                        foreach (CSelection cs in mSelections)
                        {
                            Block b = mBlocks[cs.SelectedBlock];
                            if (b.AllowRotate)
                            {
                                lb.Add(b.Id);
                                b.MatrixRT.Save();
                                b.RotoTranslRel(0d, 0d, _userPoint.X, _userPoint.Y, rotationAngle);
                            }
                        }

                        OnRotateEvent(basicPaintEventArgs, lb, null, null, null);

                        if (!basicPaintEventArgs.Accepted)
                        {
                            foreach (CSelection cs in mSelections)
                            {
                                Block b = mBlocks[cs.SelectedBlock];
                                if (b.AllowRotate)
                                    b.MatrixRT.Restore();
                            }
                        }
                        else
                        {
                            var center = new Point3D(pivotPoint.X, pivotPoint.Y);

                            // Seguente non funziona per un bug Eyeshot, dovrebbe essere sistemato in Night Build 18/06/2015
                            // Update: ok con Night Build 322 del 18/06/2015

                            EyeSelection rotateSelection = new EyeSelection("Rotate");
                            foreach (int idBlock in lb)
                            {
                                var selTmp = SelectEntities(idBlock, -1, -1, -1);

                                if (selTmp != null)
                                    for (int i = 0; i < selTmp.Count; i++)
                                        rotateSelection.AddItem(selTmp[i], true);
                            }

                            rotateSelection.Rotate(rotationAngle, Vector3D.AxisZ, center);
                            
                            //_selActive.Rotate(rotationAngle, Vector3D.AxisZ, center);
                            
                            
                            //foreach (var ent in _selActive)
                            //{
                            //    ent.Rotate(rotationAngle, Vector3D.AxisZ, center);
                            //}
                        }
                    }

                    _viewportLayout.Entities.Regen();
                    _viewportLayout.Invalidate();
                    NotifyCursorCoordinates();

                }

                #endregion ROTATE

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ViewportLayout_MouseMove ERROR: ", ex);
            }
        }

        private void mTimer_Tick(object sender, EventArgs e)
        {
            Regen();
            mTimer.Stop();
        }



        #endregion Private Methods ----------<



    }
}