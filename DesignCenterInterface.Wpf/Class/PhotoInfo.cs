﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignCenterInterface.Class
{
    internal class PhotoInfo
    {
        private int _blockNo;
        private double _coordX;
        private double _coordY;

        internal PhotoInfo(int blockNo, double coordX, double coordY)
        {
            _blockNo = blockNo;
            _coordX = coordX;
            _coordY = coordY;
        }

        public int BlockNo
        {
            get { return _blockNo; }
        }

        public double CoordX
        {
            get { return _coordX; }
        }

        public double CoordY
        {
            get { return _coordY; }
        }
    }
}
