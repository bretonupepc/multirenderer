using System;
using System.Collections.Generic;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Breton.BCamPath;
using Breton.DesignCenterInterface;
using Breton.Polygons;
using System.Xml;
using Breton.MathUtils;
using BretonViewportLayout.Wpf;
using DesignCenterInterface.Class;
using TraceLoggers;
using Point = Breton.Polygons.Point;

namespace Breton.DesignCenterInterface
{
    public class CBPExtension : Breton.DesignCenterInterface.CBasicPaint
    {
        #region Variables

        protected Block mSlab;
		private List<Block> mExtraSlabCuts;
		private Point referencePoint1;
		private Point referencePoint2;
		private const string mCollisionCode = "COLLISION";
		public const string SlabCutsCode = "SLAB_CUTS";

		private bool mCollisionEnable = false;
		private bool mMagnetEnable = false;
		private Breton.BCamPath.Tools mTools;
		private LockPiece mMovingPiece, mLockPiece;

        private PhotoInfo _photoInfo = null;

        private const string REMNANT_BLOCK_ATTRIBUTE_KEY = "IsRemnant";


        public enum LayersType
        {
            PHOTO,                     
            SLAB,                      
            SLAB_NOIMAGE,                      
            DEFECT,
            REFERENCE_POINT1,
            REFERENCE_POINT2,
            SHAPE,
            SHAPE_HOLE,         
            RAW,       
            RAW_HOLE,    
            DISC,                      
            DISC_REM,                  
            COMBINED,                      
            COMBINED_REM,        
            MILL,                                                 
            MILL_REM,                  
            FINAL_TOOL,
            FINAL_TOOL_REM,            
            EDGE_FINISHING_TOOL,       
            EDGE_FINISHING_DIM,
            RENDER_PATH,
			COLLISION,
			RAW_SMALL,
			RAW_BIG1,
			RAW_BIG2,
			RAW_INTEREST_ZONE,
			SLAB_CUTS,
			TECNO_INFO,
			TEXT,
			SYMBOL,
			TABLE,
			REPOSITORY,
			SUCKERBAR,
			SUCKERON,
			SUCKEROFF,
			BLIND_HOLE,
			PARTITION,
			PARTITION_CUTS,					// Tagli di separazione
			GEO_HOLES,						// Foretti da fare
			HOLES,							// Foretti
			SLOPED_CUTS_NEG,				// Tagli inclinati negativi
			SLOPED_CUTS_POS,				// Tagli inclinati positivi
			SLOPED_CUTS_NEG_REM,			// Asportazione tagli inclinati negativi
			SLOPED_CUTS_POS_REM,			// Asportazione tagli inclinati positivi
			INTERRUPTED_CUTS,				// Tagli interrotti
			INTERRUPTED_SLOPED_CUTS_POS,	// Tagli interrotti inclinati positivi
			INTERRUPTED_SLOPED_CUTS_NEG,	// Tagli interrotti inclinati negativi
			BRIDGE,							// Tagli ponticello
			PAUSE,							// Pausa
			RODDING,						// Operazione di rodding sul pezzo
			RODDING_TOOL,					// Postazione di rodding
			MACHINE_BOUNDARY,				// Limiti macchina
			RESIDUAL,						// Parti di lastra residue
			UNLOAD,							// Scarico lastra
            TEMP,							// usato per test
            GROOVE,							// canaletto/fuga
            GROOVE_TOOL,
            GROOVE_TOOL_REM,
            GROOVE_CUTS,
			POCKET							// Tasca o foro ribassato


            #region Tipi pezzi FkScarNet
            #region Filagne

            ,FK_PIECE_FIL_QUAD0,              //Colore pezzo scarto
            FK_PIECE_FIL_QUAD1,            //Colore pezzo prima scelta
            FK_PIECE_FIL_QUAD2,             //Colore pezzo senza formato
            FK_PIECE_FIL_QUAD3,             //Colore pezzo riempimento
            FK_PIECE_FIL_QUAD4,               //Colore pezzo a correre

            FK_PIECE_FIL_LQUAD1,            //Colore pezzo in lavoro
            FK_PIECE_FIL_LQUAD2,             //Colore pezzo tagliato

            FK_PIECE_FIL_SQUAD1,          //Colore pezzo in lavoro (Verde chiaro)
            FK_PIECE_FIL_SQUAD2,           //Colore pezzo in scarico (gialletto)
            FK_PIECE_FIL_SQUAD3,            //Colore pezzo scaricato  (Verde scuro)

            FK_PIECE_FIL_SQUAD4,               //Colore pezzo a correre in lavoro
            FK_PIECE_FIL_SQUAD5,           //Colore pezzo a correre scaricato
            FK_PIECE_FIL_SQUAD6,            //Colore pezzo riempimento in lavoro
            FK_PIECE_FIL_SQUAD7,            //Colore pezzo riempimento scaricato
            FK_PIECE_FIL_SQUAD8,             //Colore pezzo scarto in lavoro
            FK_PIECE_FIL_SQUAD9,          //Colore pezzo scarto scaricato
            FK_PIECE_FIL_MISC               // Altre info 

            #endregion Filagne

            #region Poligoni

            , FK_PIECE_POL_QUAD0,              //Colore pezzo scarto
            FK_PIECE_POL_QUAD1,            //Colore pezzo prima scelta
            FK_PIECE_POL_QUAD2,             //Colore pezzo senza formato
            FK_PIECE_POL_QUAD3,             //Colore pezzo riempimento
            FK_PIECE_POL_QUAD4,               //Colore pezzo a correre

            FK_PIECE_POL_LQUAD1,            //Colore pezzo in lavoro
            FK_PIECE_POL_LQUAD2,             //Colore pezzo tagliato

            FK_PIECE_POL_SQUAD1,          //Colore pezzo in lavoro (Verde chiaro)
            FK_PIECE_POL_SQUAD2,           //Colore pezzo in scarico (gialletto)
            FK_PIECE_POL_SQUAD3,            //Colore pezzo scaricato  (Verde scuro)

            FK_PIECE_POL_SQUAD4,               //Colore pezzo a correre in lavoro
            FK_PIECE_POL_SQUAD5,           //Colore pezzo a correre scaricato
            FK_PIECE_POL_SQUAD6,            //Colore pezzo riempimento in lavoro
            FK_PIECE_POL_SQUAD7,            //Colore pezzo riempimento scaricato
            FK_PIECE_POL_SQUAD8,             //Colore pezzo scarto in lavoro
            FK_PIECE_POL_SQUAD9,          //Colore pezzo scarto scaricato
            FK_PIECE_POL_MISC               // Altre info 

            #endregion Poligoni

            , PIECE_LABEL

            //Remnants
            , REMNANT


            //Const lFORE_QUAD0 As Long = &HFF00              ' Colore pezzo scarto
//Const lFORE_QUAD1 As Long = &HFF00FF            ' Colore pezzo prima scelta
//Const lFORE_QUAD2 As Long = &HFF0000            ' Colore pezzo senza formato
//Const lFORE_QUAD3 As Long = &HFF7F00            ' Colore pezzo riempimento
//Const lFORE_QUAD4 As Long = &H80&               ' Colore pezzo a correre

//Const lFORE_LQUAD1 As Long = &HFFFF00           ' Colore pezzo in lavoro
//Const lFORE_LQUAD2 As Long = &HFFFFF            ' Colore pezzo tagliato

//Const lFORE_SQUAD1 As Long = &HFF00FF           ' Colore pezzo in lavoro (Verde chiaro)
//Const lFORE_SQUAD2 As Long = &HFF4000           ' Colore pezzo in scarico (gialletto)
//Const lFORE_SQUAD3 As Long = &HFF70B0           ' Colore pezzo scaricato  (Verde scuro)

//Const lFORE_SQUAD4 As Long = &H80&              ' Colore pezzo a correre in lavoro
//Const lFORE_SQUAD5 As Long = &H8080FF           ' Colore pezzo a correre scaricato
//Const lFORE_SQUAD6 As Long = &HFF7F00           ' Colore pezzo riempimento in lavoro
//Const lFORE_SQUAD7 As Long = &HFFBF3F           ' Colore pezzo riempimento scaricato
//Const lFORE_SQUAD8 As Long = &HFF00             ' Colore pezzo scarto in lavoro
//Const lFORE_SQUAD9 As Long = &HFFFF80           ' Colore pezzo scarto scaricato

            #endregion

        }
       #endregion

        #region Constructors

        /// <summary>
        /// Classe di interfaccia specializzata per editing grafico
        /// </summary>
        /// <param name="viewer">UserControl visualizzatore 2D Breton</param>
        /// <param name="ax">Orientazione assi</param>
        public CBPExtension(Breton2DViewer viewer, AxisDirection ax)
            : base(viewer, ax)
        {
            InitLayer();
            mExtraSlabCuts = new List<Block>();
        }

        /// <summary>
        /// Classe di interfaccia specializzata per editing grafico
        /// </summary>
        /// <param name="viewer">UserControl visualizzatore 2D Breton</param>
        public CBPExtension(Breton2DViewer viewer)
            : this(viewer, AxisDirection.LEFT_BOTTOM)
        { }

        /// <summary>
        /// Classe di interfaccia specializzata per editing grafico
        /// </summary>
        /// <param name="viewer">UserControl visualizzatore 2D Breton</param>
        /// <param name="useTouchLayout">Usa interfaccia touch con toolbar</param>
        public CBPExtension(Breton2DViewer viewer, bool useTouchLayout)
            : this(viewer, AxisDirection.LEFT_BOTTOM, useTouchLayout)
        {


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewer">UserControl visualizzatore 2D Breton</param>
        /// <param name="axisDirection">Imposta Origine/Orientazione assi</param>
        /// <param name="useTouchLayout">Usa interfaccia touch con toolbar</param>
        /// <param name="showRuler">Visualizza righello</param>
        /// <param name="showRulerCoordinates">Visualizza finestrelle coordinate se righello abilitato</param>
        public CBPExtension(Breton2DViewer viewer, AxisDirection axisDirection, bool useTouchLayout = false, bool showRuler = false, bool showRulerCoordinates = false)
            : base(viewer, axisDirection, useTouchLayout, showRuler, showRulerCoordinates)
        {
            InitLayer();
            mExtraSlabCuts = new List<Block>();

        }


        #endregion 

        #region Properties

        /// <summary>
        /// Lastra corrente
        /// </summary>
        public Block Slab
        {
            get { return mSlab; }
        }

		/// <summary>
		/// Tagli extra sulla lastra
		/// </summary>
		public List<Block> ExtraSlabCuts
		{
			get { return mExtraSlabCuts; }
		}

		/// <summary>
		/// Primo punto di riferimento
		/// </summary>
		public Point ReferencePoint1
		{
			get { return referencePoint1; }
		}

		/// <summary>
		/// Secondo punto di riferimento
		/// </summary>
		public Point ReferencePoint2
		{
			get { return referencePoint2; }
		}

		/// <summary>
		/// Indica se la collisione tra le varie entit� � abilitata
		/// </summary>
		public bool CollisionEnable
		{
			get { return mCollisionEnable; }
			set { mCollisionEnable = value; }
		}

		/// <summary>
		/// Indica se la modalit� calamita � attiva
		/// </summary>
		public bool MagnetEnable
		{
			get { return mMagnetEnable; }
			set { mMagnetEnable = value; }
		}

		/// <summary>
		/// Pezzo in movimento
		/// </summary>
		public LockPiece gMovingPiece
		{
			get { return mMovingPiece; }
			set { mMovingPiece = value; }
		}

		/// <summary>
		/// Pezzo fisso a cui agganciare
		/// </summary>
		public LockPiece gLockPiece
		{
			get { return mLockPiece; }
			set { mLockPiece = value; }
		}

		public List<Block> ListBlocks
		{
			get 
			{
				if (Blocks == null)
					return null;
				else
				{
					List<Block> blocks = new List<Block>();
					foreach (KeyValuePair<int, Block> b in Blocks)
					{
						blocks.Add(b.Value);
					}
					return blocks;
				}
			}
		}

        #endregion

        #region Public Methods

        /// <summary>
        /// Esegue il reset delle liste presenti e inizializza i layer impostati
        /// </summary>
        public override void Reset()
        {
            mSlab = null;
			mExtraSlabCuts = new List<Block>();
            base.Reset();

            InitLayer();
        }

        public override void ClearAll()
        {

            this.Blocks.Clear();
            mSlab = null;
			mExtraSlabCuts = new List<Block>();
            base.ClearAll();

            ////InitLayer();
        }


        /// <summary>
        /// Effettua la copia degli elementi di una cbasicpaintExtension
        /// </summary>
        /// <param name="cbpe"></param>
		public void CopyTo(CBPExtension cbpe)
		{
			cbpe.Reset();

			foreach (KeyValuePair<int, Path> p in this.Slab.GetPaths())
			{
                if (p.Value.Layer.Trim() == this.GetLayer(CBPExtension.LayersType.SLAB).Name.Trim())
                {
                    cbpe.AddSlab(this.Slab.Name, p.Value);
                    cbpe.Slab.MatrixRT = new Breton.MathUtils.RTMatrix(this.Slab.MatrixRT);
                }
                else if (p.Value.Layer.Trim() == this.GetLayer(CBPExtension.LayersType.DEFECT).Name.Trim())
                    cbpe.AddDefect(p.Value);
                else if (p.Value.Layer.Trim() == this.GetLayer(CBPExtension.LayersType.REFERENCE_POINT1).Name.Trim())
                    cbpe.AddReferencePoint(ReferencePoint1, true);
                else if (p.Value.Layer.Trim() == this.GetLayer(CBPExtension.LayersType.REFERENCE_POINT2).Name.Trim())
                    cbpe.AddReferencePoint(ReferencePoint2, false);
			}

			foreach (KeyValuePair<int, Block> b in this.Blocks)
			{
				if (b.Value.Code.Contains("PIEF"))
					cbpe.AddShape(b.Value);

				if (IsRemnantBlock(b.Value))
					cbpe.AddShape(b.Value);



				if (b.Value.Photo != null)
				{
					CPhoto photo = new CPhoto(b.Value.Photo);
					cbpe.GetBlockByCode(b.Value.Code).Photo = photo;
				}
			}

			cbpe.Photo = this.mPhoto;

            cbpe._photoInfo = this._photoInfo;
		}

		/// <summary>
		/// Rimuove tutti i pezzi all'interno di una lastra
		/// </summary>
		public void ClearSlab()
		{
			ArrayList keyValues = new ArrayList();

			foreach(KeyValuePair<int, Block> b in Blocks)
			{
				if (b.Value != Slab)
					keyValues.Add(b.Value.Id);
			}

			for (int i = 0; i < keyValues.Count; i++)
			{
				this.RemoveBlock((int)keyValues[i]);
			}
		}

        /// <summary>
        /// Dato un blocco con una serie di percorsi, aggiunge la shape e disegna i percorsi sui layer assegnati.
        /// </summary>
        /// <param name="b">il blocco da disegnare</param>
        /// <returns></returns>
		public int AddShape(Block b)
        {
            int idB = -1;

            try
            {
                for (int i = 0; i < b.Count; i++)
                {
                    Path p = b[i];

                    //if (i == 0)
                    //    idB = this.AddBlock();

                    if (p.Layer.Trim() == CBPExtension.LayersType.SHAPE.ToString())
                    {
                        if (idB >= 0)
                            this.AddPath(idB, p);
                        else
                            idB = this.AddShape(p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.SHAPE_HOLE.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddShapeHole(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.RAW.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddRaw(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.RAW_HOLE.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddRawHole(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.DISC.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathDisc(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.DISC_REM.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathDiscRem(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.COMBINED.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathFinishingMill(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.COMBINED_REM.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathFinishingMillRem(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.MILL.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathMill(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.MILL_REM.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathMillRem(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.FINAL_TOOL.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddFinalPath(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.FINAL_TOOL_REM.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddFinalPathRem(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.EDGE_FINISHING_TOOL.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathEdgeFinish(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.EDGE_FINISHING_DIM.ToString())
                    {
                        if (idB == -1)
                            idB = this.AddBlock();

                        this.AddPathEdgeFinishRem(idB, p);
                    }
                    else if (p.Layer.Trim() == CBPExtension.LayersType.REMNANT.ToString())
                    {
                        if (idB >= 0)
                            this.AddPath(idB, p);
                        else
                            idB = this.AddRemnant(p);
                    }
                        //else if (p.Layer.Trim() == CBPExtension.LayersType.RENDER_PATH.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.RAW_SMALL.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.RAW_BIG1.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.RAW_BIG2.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.RAW_INTEREST_ZONE.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.SLAB_CUTS.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.TECNO_INFO.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.TEXT.ToString() ||
                        //            p.Layer.Trim() == CBPExtension.LayersType.SYMBOL.ToString())
                        //{
                        //    this.AddPath(idB, p);
                        //}
                    else
                    {
                        if (i == 0 || idB == -1)
                            idB = this.AddBlock();
                        this.AddPath(idB, p);
                    }
                }

                double x, y, a;
                b.GetRotoTransl(out x, out y, out a);
                this.Blocks[idB].SetRotoTransl(x, y, a);
                this.Blocks[idB].Name = b.Name;
                this.Blocks[idB].Code = b.Code;

                // Aggiunge eventuali attributi
                if (b.Attributes != null)
                {
                    foreach (KeyValuePair<string, string> att in b.Attributes)
                        //this.Blocks[idB].Attributes.Add(att.Key, att.Value);
                        this.Blocks[idB].SetAttribute(att.Key, att.Value);
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("CBPExtension AddShape error ", ex);
            }


			return idB;
		}



        #region Slabs
		/// <summary>
		/// Aggiunge una lastra e il relativo percorso 
		/// </summary>
		/// <param name="name">codice della lastra</param>
		/// <param name="slabPath">percorso della lastra</param>
		public int AddSlab(string name, Path slabPath)
		{
			return AddSlab(name, slabPath, 0, 0);
		}

        /// <summary>
        /// Aggiunge una lastra e il relativo percorso 
        /// </summary>
        /// <param name="name">codice della lastra</param>
        /// <param name="slabPath">percorso della lastra</param>
        /// <param name="x">traslazione in x</param>
        /// <param name="y">traslazione in y</param>
        /// <returns>true: lastra aggiunta correttamente / false: errore in aggiunta lastra</returns>
        public int AddSlab(string name, Path slabPath, double x, double y)
        {
            try
            {
                if (mSlab == null)
                    mSlab = new Block();

                slabPath.AllowSelect = false;
                slabPath.Layer = LayersType.SLAB.ToString();
                mSlab.AddPath(slabPath);

                mSlab.Name = name;

                mSlab.AllowSelect = false;
                mSlab.AllowMove = false;
                mSlab.AllowRotate = false;

				if (x != 0 && y != 0)
					mSlab.SetRotoTransl(x, y, 0);

                int block_no = AddBlock();
                mSlab.Id = block_no;
                mBlocks[block_no] = mSlab;

                return block_no;
            }
            catch (Exception e) { return -1; }
        }

        /// <summary>
        /// Rimuove la lastra corrente
        /// </summary>
        public void RemoveSlab()
        {
            if (mSlab == null)
                return;

            base.Reset();

            mSlab = null;
        }

        /// <summary>
        /// Restituisce la lastra corrente
        /// </summary>
        /// <returns></returns>
        public Block GetSlab()
        {
            return mSlab;
        }

		public Path GetSlabPath()
		{
			SortedList<int, Path> lstPaths = mSlab.GetPaths();

			foreach (KeyValuePair<int, Path> p in lstPaths)
			{
				if (p.Value.Layer == LayersType.SLAB.ToString())
				{
					return p.Value;
				}
			}

			return null;
		}
        #endregion

        #region Defect

		/// <summary>
		/// Aggiunge il percorso di tipo difetto
		/// </summary>
		/// <param name="defect">Percorso da aggiungere</param>
		/// <returns></returns>
		public int AddDefect(Path defect)
		{
			return AddDefect(defect, 0, 0);
		}

        /// <summary>
        /// Aggiunge il percorso di tipo difetto
        /// </summary>
        /// <param name="defect">Percorso da aggiungere</param>
        /// <param name="oX">traslazione in x</param>
        /// <param name="oY">traslazione in y</param>
        /// <returns></returns>
        public int AddDefect(Path defect, double oX, double oY)
        {
            defect.AllowSelect = false;
			if (oX != 0 && oY != 0)
				defect.SetRotoTransl(oX, oY, 0);
            defect.Layer = LayersType.DEFECT.ToString();
            
            return mSlab.AddPath(defect);   
        }

        /// <summary>
        /// Rimuove il difetto specificato
        /// </summary>
        /// <param name="defect_no">Numero chiave del difetto da rimuovere</param>
        public void RemoveDefect(int defect_no)
        {
            SortedList<int, Path> lstPaths = mSlab.GetPaths();

            if (defect_no == -1)    // eliminazione di tutti i difetti
            {
                for (int i = 0; i < lstPaths.Count; i++)
                {
                    Path p = lstPaths[lstPaths.Keys[i]];
                    if (p.Layer == LayersType.DEFECT.ToString())
                    {
                        base.RemovePath(mSlab.Id, defect_no);
                    }
                }
            }
            else
            {
                for (int i = 0; i < lstPaths.Count; i++)
                {
                    Path p = lstPaths[lstPaths.Keys[i]];
                    if (p.Layer == LayersType.DEFECT.ToString() && lstPaths.Keys[i] == defect_no)
                    {
                        base.RemovePath(mSlab.Id, defect_no);
                    }
                }
            }
        }

        /// <summary>
        /// Restituisce il difetto lastra specificato
        /// </summary>
        /// <param name="defect_no">Numero chiave del difetto</param>
        /// <returns>Lista con uno o pi� difetti</returns>
        public List<Path> GetDefect(int defect_no)
        {
            SortedList<int, Path> lstPaths = mSlab.GetPaths();
            List<Path> lstDefects = new List<Path>();
            
            if (defect_no == -1)            // voglio tutti i difetti della lastra corrente
            {
                for (int i = 0; i < lstPaths.Count; i++)
                {
                    Path p = lstPaths[lstPaths.Keys[i]];
                
                    if (p.Layer == LayersType.DEFECT.ToString())
                    {
                        lstDefects.Add(p);
                    }
                }
            }
            else
            {
                for (int i = 0; i < lstPaths.Count; i++)
                {
                    Path p = lstPaths[lstPaths.Keys[i]];

                    if (p.Layer == LayersType.DEFECT.ToString() && lstPaths.Keys[i] == defect_no)
                    {
                        lstDefects.Add(p);
                    }
                }
            }

            return lstDefects;
        }

        #endregion

        #region Reference Point

        /// <summary>
        /// Aggiunge il punto di riferimento
        /// </summary>
        /// <param name="point">Punto di riferimento</param>
        /// <param name="p1">true: per il P1 / false: per il P2</param>
        /// <returns></returns>
        public int AddReferencePoint(Breton.Polygons.Point point, bool p1)
        {
            return AddReferencePoint(point, p1, 0, 0);
        }

        /// <summary>
        /// Aggiunge il punto di riferimento
        /// </summary>
        /// <param name="point">Punto di riferimento</param>
        /// <param name="p1">true: per il P1 / false: per il P2</param>
        /// <param name="oX">traslazione in x</param>
        /// <param name="oY">traslazione in y</param>
        /// <returns></returns>
        public int AddReferencePoint(Breton.Polygons.Point point, bool p1, double oX, double oY)
        {
            Path p = new Path();
            Side s;
            if (p1)
            {
                // disegno un + con un cerchio intorno
                s = new Segment(point.X - 20, point.Y, point.X + 20, point.Y);
                p.AddSideRef(s);
                s = new Segment(point.X, point.Y - 20, point.X, point.Y + 20);
                p.AddSideRef(s);
				s = new Arc(point.X, point.Y, 20, 0, 2 * Math.PI);
				p.AddSideRef(s);

                p.Layer = LayersType.REFERENCE_POINT1.ToString();
				referencePoint1 = new Point(point);
				referencePoint1 = referencePoint1.RotoTrasl(new Point(oX, oY), 0);
            }
            else
            {
                // disegno un una X con un quadrato intorno
                s = new Segment(point.X - 20, point.Y - 20, point.X + 20, point.Y - 20);
                p.AddSideRef(s);
                s = new Segment(point.X + 20, point.Y - 20, point.X + 20, point.Y + 20);
                p.AddSideRef(s);
                s = new Segment(point.X + 20, point.Y + 20, point.X - 20, point.Y + 20);
                p.AddSideRef(s);
                s = new Segment(point.X - 20, point.Y + 20, point.X - 20, point.Y - 20);
                p.AddSideRef(s);
                s = new Segment(point.X - 20, point.Y - 20, point.X + 20, point.Y + 20);
                p.AddSideRef(s);
                s = new Segment(point.X + 20, point.Y - 20, point.X - 20, point.Y + 20);
                p.AddSideRef(s);
                
                p.Layer = LayersType.REFERENCE_POINT2.ToString();
				referencePoint2 = new Point(point);
				referencePoint2 = referencePoint2.RotoTrasl(new Point(oX, oY), 0);
            }

            p.AllowSelect = false;
            if (oX != 0 && oY != 0)
                p.SetRotoTransl(oX, oY, 0);


            return mSlab.AddPath(p);
        }

        #endregion

        #region Shape

        /// <summary>
        /// Aggiunge il percorso di tipo shape (pezzo finito)
        /// </summary>
        /// <param name="extPath">Percorso da aggiungere</param>
        /// <param name="layerType">Layer di destinazione</param>
        /// <returns></returns>
        public int AddShape(Path extPath, LayersType layerType = LayersType.SHAPE)
        {
            int block_no = AddBlock();

            mBlocks[block_no].Id = block_no;

            mBlocks[block_no].AllowSelect = true;
            mBlocks[block_no].AllowMove = true;
            mBlocks[block_no].AllowRotate = true;
            mBlocks[block_no].AllowDragDrop = true;
           
            extPath.AllowSelect = false;            // il path esterno della shape non pu� essere, ad esempio, cancellato
            AddPathType(block_no, extPath, layerType.ToString());
            
            return block_no;
        }

        public int AddRemnant(Path extPath, LayersType layerType = LayersType.REMNANT)
        {
            int block_no = AddBlock();

            mBlocks[block_no].Id = block_no;

            mBlocks[block_no].AllowSelect = true;
            mBlocks[block_no].AllowMove = false;
            mBlocks[block_no].AllowRotate = false;
            mBlocks[block_no].AllowDragDrop = false;

            mBlocks[block_no].SetAttribute(REMNANT_BLOCK_ATTRIBUTE_KEY, "true");

           
            extPath.AllowSelect = false;            // il path esterno della shape non pu� essere, ad esempio, cancellato
            AddPathType(block_no, extPath, layerType.ToString());
            
            return block_no;
        }

        /// <summary>
        /// Aggiunge il percorso foro di una forma finita
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddShapeHole(int shape_no, Path hole)
        {
            hole.AllowSelect = true;
            return AddHole(shape_no, hole, LayersType.SHAPE_HOLE.ToString());
        }

        /// <summary>
        /// Rimuove la shape indicata
        /// </summary>
        /// <param name="block_no">numero della shape da eliminare</param>
        public void RemoveShape(int block_no)
        {
            if (mBlocks == null)
                return;
                                    
            // rimuovo dalla lista di CBasicPaint
            //mBlocks.RemoveAt(block_no);

            base.RemoveBlock(block_no);
        }

        /// <summary>
        /// Rimuove il percorso del foro indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemoveShapeHole(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.SHAPE_HOLE.ToString());
        }

        /// <summary>
        /// Restituisce la shape specificata
        /// </summary>
        /// <param name="shape_no">Numero della shape</param>
        /// <returns>Blocco shape richiesto</returns>
        public Block GetShape(int shape_no)
        {
            return mBlocks[shape_no];
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <returns>Percorso trovato</returns>
        public Path GetShapePath(int shape_no)
        {
            try
            {
                SortedList<int, Path> lstPaths = mBlocks[shape_no].GetPaths();
                Path path = new Path();

                // vedo tutti i percorsi per trovare il blocco particolare
                for (int i = 0; i < lstPaths.Count; i++)
                {
                    Path p = lstPaths[lstPaths.Keys[i]];
                        if (p.Layer == LayersType.SHAPE.ToString())
                            return p;
                }
                return null;

            }
            catch (Exception e) { return null; }
        }

        #endregion

        #region Raw

        /// <summary>
        /// Aggiunge il percorso di un grezzo
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddRaw(int shape_no, Path extPath)
        {
            extPath.AllowSelect = true;

			for (int i = 0; i < extPath.Count; i++)
				extPath.GetSide(i).AllowSelect = true;

            return AddPathType(shape_no, extPath, LayersType.RAW.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso foro di un grezzo
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro grezzo</param>
        /// <param name="hole">percorso del foro grezzo</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddRawHole(int shape_no, Path hole)
        {
            hole.AllowSelect = true;

			for (int i = 0; i < hole.Count; i++)
				hole.GetSide(i).AllowSelect = true;

            return AddHole(shape_no, hole, LayersType.RAW_HOLE.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di tipo grezzo indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemoveRaw(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.RAW.ToString());
        }

        /// <summary>
        /// Rimuove il percorso del foro di tipo grezzo indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemoveRawHole(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.RAW_HOLE.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetRaw(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.RAW.ToString());
        }


		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no">Shape di cui si vuole il percorso</param>
		/// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
		/// <returns>Lista dei percorsi trovati</returns>
		public List<Path> GetRawHole(int shape_no, int path_no)
		{
			return GetPathType(shape_no, path_no, LayersType.RAW_HOLE.ToString());
		}

		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no">Shape di cui si vuole il percorso</param>
		/// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
		/// <returns>Lista dei percorsi trovati</returns>
		public List<Path> GetRawSmall(int shape_no, int path_no)
		{
			return GetPathType(shape_no, path_no, LayersType.RAW_SMALL.ToString());
		}

		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no">Shape di cui si vuole il percorso</param>
		/// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
		/// <returns>Lista dei percorsi trovati</returns>
		public List<Path> GetRawInterestZone(int shape_no, int path_no)
		{
			return GetPathType(shape_no, path_no, LayersType.RAW_INTEREST_ZONE.ToString());
		}

        #endregion

        #region Disc

        /// <summary>
        /// Aggiunge il percorso disco
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathDisc(int shape_no, Path extPath)
        {
            extPath.AllowSelect = true;
            return AddPathType(shape_no, extPath, LayersType.DISC.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso foro con un taglio disco
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathDiscHole(int shape_no, Path hole)
        {
            hole.AllowSelect = true;
            return AddHole(shape_no, hole, LayersType.DISC.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di tipo disco indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathDisc(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.DISC.ToString());  
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathDisc(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.DISC.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione del disco
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathDiscRem(int shape_no, Path extPath)
        {
            extPath.AllowSelect = false;
            return AddPathType(shape_no, extPath, LayersType.DISC_REM.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione del taglio disco di un foro
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathDiscHoleRem(int shape_no, Path hole)
        {
            hole.AllowSelect = false;
            return AddHole(shape_no, hole, LayersType.DISC_REM.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di asportazione tipo disco indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathDiscRem(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.DISC_REM.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathDiscRem(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.DISC_REM.ToString());
        }
        
        #endregion

        #region Finishing Mill

        /// <summary>
        /// Aggiunge il percorso di finitura disco con fresa
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathFinishingMill(int shape_no, Path extPath)
        {
            extPath.AllowSelect = true;
            return AddPathType(shape_no, extPath, LayersType.COMBINED.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso foro di una finitura disco con fresa
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso di finitura</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathFinishingMillHole(int shape_no, Path hole)
        {
            hole.AllowSelect = true;
            return AddHole(shape_no, hole, LayersType.COMBINED.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di tipo finitura disco con fresa indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathFinishingMill(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.COMBINED.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathFinishingMill(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.COMBINED.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione finitura disco con fresa
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathFinishingMillRem(int shape_no, Path extPath)
        {
            extPath.AllowSelect = false;
            return AddPathType(shape_no, extPath, LayersType.COMBINED_REM.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione di una finitura disco con fresa per un foro
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathFinishingMillRemHole(int shape_no, Path hole)
        {
            hole.AllowSelect = false;
            return AddHole(shape_no, hole, LayersType.COMBINED_REM.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di tipo asportazione finitura disco con fresa indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathFinishingMillRem(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.COMBINED_REM.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathFinishingMillRem(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.COMBINED_REM.ToString());
        }

        #endregion

        #region Mill

        /// <summary>
        /// Aggiunge il percorso fresa
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathMill(int shape_no, Path extPath)
        {
            extPath.AllowSelect = true;
            return AddPathType(shape_no, extPath, LayersType.MILL.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso foro appartenente ad un taglio fresa
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathMillHole(int shape_no, Path hole)
        {
            hole.AllowSelect = true;
            return AddHole(shape_no, hole, LayersType.MILL.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di tipo fresa indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso da eliminare</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathMill(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.MILL.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathMill(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.MILL.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione fresa
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathMillRem(int shape_no, Path extPath)
        {
            extPath.AllowSelect = false;
            return AddPathType(shape_no, extPath, LayersType.MILL_REM.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione di un taglio fresa per un foro
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathMillRemHole(int shape_no, Path hole)
        {
            hole.AllowSelect = false;
            return AddHole(shape_no, hole, LayersType.MILL_REM.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di asportazione indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathMillRem(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.MILL_REM.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathMillRem(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.MILL_REM.ToString());
        }

        #endregion

        #region Final path

        /// <summary>
        /// Aggiunge il percorso scelto dall'utente
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddFinalPath(int shape_no, Path extPath)
        {
            extPath.AllowSelect = true;
            return AddPathType(shape_no, extPath, LayersType.FINAL_TOOL.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso foro appartenente al taglio finale scelto dall'utente
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddFinalPathHole(int shape_no, Path hole)
        {
            hole.AllowSelect = true;
            return AddHole(shape_no, hole, LayersType.FINAL_TOOL.ToString());
        }

        /// <summary>
        /// Rimuove il percorso indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemoveFinalPath(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.FINAL_TOOL.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetFinalPath(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.FINAL_TOOL.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione scelto dall'utente
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddFinalPathRem(int shape_no, Path extPath)
        {
            extPath.AllowSelect = false;
            return AddPathType(shape_no, extPath, LayersType.FINAL_TOOL_REM.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di asportazione del foro ddel taglio scelto dall'utente
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddFinalPathRemHole(int shape_no, Path hole)
        {
            hole.AllowSelect = false;
            return AddHole(shape_no, hole, LayersType.FINAL_TOOL_REM.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di asportazione indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemoveFinalPathRem(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.FINAL_TOOL_REM.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetFinalPathRem(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.FINAL_TOOL_REM.ToString());
        }

        #endregion

        #region Edge Finish

        /// <summary>
        /// Aggiunge il percorso di lavorazione
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathEdgeFinish(int shape_no, Path extPath)
        {
            extPath.AllowSelect = true;
            return AddPathType(shape_no, extPath, LayersType.EDGE_FINISHING_TOOL.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso foro appartenente relativo ad una lavorazione
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathEdgeFinishHole(int shape_no, Path hole)
        {
            hole.AllowSelect = true;
            return AddHole(shape_no, hole, LayersType.EDGE_FINISHING_TOOL.ToString());
        }

        /// <summary>
        /// Rimuove il percorso indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathEdgeFinish(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.EDGE_FINISHING_TOOL.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathEdgeFinish(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.EDGE_FINISHING_TOOL.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di ingompre per la lavorazione
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il percorso</param>
        /// <param name="extPath">percorso</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathEdgeFinishRem(int shape_no, Path extPath)
        {
            extPath.AllowSelect = false;
            return AddPathType(shape_no, extPath, LayersType.EDGE_FINISHING_DIM.ToString());
        }

        /// <summary>
        /// Aggiunge il percorso di ingombre relativo alla lavorazione su un foro
        /// </summary>
        /// <param name="shape_no">numero della shape a cui aggiungere il foro</param>
        /// <param name="hole">percorso del foro</param>
        /// <returns>indice del percorso aggiunto</returns>
        public int AddPathEdgeFinishRemHole(int shape_no, Path hole)
        {
            hole.AllowSelect = false;
            return AddHole(shape_no, hole, LayersType.EDGE_FINISHING_DIM.ToString());
        }

        /// <summary>
        /// Rimuove il percorso di asportazione indicato
        /// </summary>
        /// <param name="shape_no">Numero della shape da cui rimuovere il percorso</param>
        /// <param name="path_no">Numero del percorso</param>
        /// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
        public bool RemovePathEdgeFinishRem(int shape_no, int path_no)
        {
            return Remove(shape_no, path_no, LayersType.EDGE_FINISHING_DIM.ToString());
        }

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
        /// <returns>Lista dei percorsi trovati</returns>
        public List<Path> GetPathEdgeFinishRem(int shape_no, int path_no)
        {
            return GetPathType(shape_no, path_no, LayersType.EDGE_FINISHING_DIM.ToString());
        }

        #endregion

        #region Render

        /// <summary>
        /// Restituisce il percorso richiesto
        /// </summary>
        /// <param name="shape_no">Shape di cui si vuole il percorso</param>
        /// <returns>Percorso trovato</returns>
        public Path GetRenderPath(string code) // int shape_no
        {
            try
            {
                SortedList<int, Path> lstPaths = GetBlockByCode(code).GetPaths(); // mBlocks[shape_no].GetPaths();
                Path path = new Path();

                // vedo tutti i percorsi per trovare il blocco particolare
                for (int i = 0; i < lstPaths.Count; i++)
                {
                    Path p = lstPaths[lstPaths.Keys[i]];
                    if (p.Layer == LayersType.RENDER_PATH.ToString())
                        return p;
                }
                return null;

            }
            catch (Exception e) { return null; }
        }

        #endregion

		#region Collision
		/// <summary>
		/// Aggiunge un percorso di tipo punto di collisione
		/// </summary>
		/// <param name="collision">percorso della collisione</param>
		/// <returns>indice del percorso aggiunto</returns>
		public int AddCollision(Path collision)
		{
			Block colBlock = GetBlockByCode(mCollisionCode);

			if (colBlock == null)
			{
				colBlock = this.Blocks[AddBlock()];
				colBlock.Code = mCollisionCode;				
			}

			collision.AllowSelect = false;
			return AddPathType(colBlock.Id, collision, LayersType.COLLISION.ToString());
		}

		/// <summary>
		/// Rimuove i percorsi delle collisioni
		/// </summary>
		/// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
		public bool RemoveCollisions()
		{
			Block colBlock = GetBlockByCode(mCollisionCode);

			if (colBlock != null)
				return Remove(colBlock.Id, -1, LayersType.COLLISION.ToString());
			else 
				return false;
		}

		/// <summary>
		/// Restituisce il block con le collisioni
		/// </summary>
		/// <returns>Lista dei percorsi trovati</returns>
		public Block GetBlockCollision()
		{
			try
			{
				return GetBlockByCode(mCollisionCode);
			}
			catch (Exception e) { return null; }
		}
		#endregion

		#region Slab cuts
		/// <summary>
		/// Aggiunge un percorso di tipo  punto di collisione
		/// </summary>
		/// <param name="slabCut">percorso della collisione</param>
		/// <returns>indice del percorso aggiunto</returns>
		public int AddSlabCut(Path slabCut)
		{
			Block cutBlock = new Block();
			cutBlock.AllowSelect = true;
			cutBlock.AllowMove = false;
			cutBlock.AllowRotate = false;
			cutBlock.AllowDragDrop = false;

			slabCut.Layer = LayersType.SLAB_CUTS.ToString();
			slabCut.AllowSelect = true;
			int bp = cutBlock.AddPath(slabCut);

			int block_no = AddBlock();
			cutBlock.Id = block_no;
			cutBlock.Name = SlabCutsCode + block_no.ToString();
			mBlocks[block_no] = cutBlock;
			mExtraSlabCuts.Add(cutBlock);

			return block_no;
		}


		/// <summary>
		/// Rimuove i percorsi dei tagli lastre
		/// </summary>
		/// <param name="pathcut">taglio da eliminare (-1 per tutti)</param>
		/// <returns>true: cancellazione avvenuta / false: percorso non trovato</returns>
		public bool RemoveSlabCuts(int cutblock)
		{
			mExtraSlabCuts.Remove(Blocks[cutblock]);
			return base.RemoveBlock(cutblock);			
		}		
		#endregion

		#region Photo

		/// <summary>
        /// Aggiunge la foto della lastra
        /// </summary>
        /// <param name="pathPhoto">percorso su disco del file jpg della foto</param>
        /// <param name="dimX">Larghezza della foto</param>
        /// <param name="dimY">Lunghezza della foto</param>
        /// <param name="name">Codice della foto</param>
        public void AddPhoto(string pathPhoto, double dimX, double dimY, string name)
        {
            base.Photo = new CPhoto(pathPhoto, dimX, dimY, name, LayersType.PHOTO.ToString());
        }

		/// <summary>
		/// Aggiunge la foto del blocco
		/// </summary>
		/// <param name="shape_no">Numero del blocco a cui inserire la foto</param>
		/// <param name="pathPhoto">percorso su disco del file jpg della foto</param>
		/// <param name="dimX">Larghezza della foto</param>
		/// <param name="dimY">Lunghezza della foto</param>
		/// <param name="name">Codice della foto</param>
		public void AddBlockPhoto(int shape_no, string pathPhoto, double dimX, double dimY, string name)
		{
			AddBlockPhoto(mBlocks[shape_no], pathPhoto, dimX, dimY, name);
		}

		/// <summary>
		/// Aggiunge la foto del blocco
		/// </summary>
		/// <param name="b">Blocco a cui inserire la foto</param>
		/// <param name="pathPhoto">percorso su disco del file jpg della foto</param>
		/// <param name="dimX">Larghezza della foto</param>
		/// <param name="dimY">Lunghezza della foto</param>
		/// <param name="name">Codice della foto</param>
		public void AddBlockPhoto(Block b, string pathPhoto, double dimX, double dimY, string name)
		{
			b.Photo = new CPhoto(pathPhoto, dimX, dimY, name, LayersType.PHOTO.ToString());
		}
        #endregion

        #region layer

        ///***************************************************************************
        /// <summary>
        /// Ritorna il layer richiesto
        /// </summary>
        /// <param name="layer">tipo layer</param>
        /// <returns>Layer se esiste, null altrimenti</returns>
        ///***************************************************************************
        public CLayer GetLayer(LayersType layer)
        {
            return GetLayer(layer.ToString());
        }

        ///***************************************************************************
        /// <summary>
        /// Rimuove il layer indicato
        /// </summary>
        /// <param name="layer">tipo layer</param>
        /// <returns>
        ///			true	layer cancellato
        ///			false	errore
        ///	</returns>
        ///***************************************************************************
        public bool RemoveLayer(LayersType layer)
        {
            return RemoveLayer(layer.ToString());
        }

        #endregion

		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no"></param>
		/// <param name="path_no"></param>
		/// <param name="layer"></param>
		/// <param name="paths"></param>
		/// <returns></returns>
		public bool GetPathType(int shape_no, int path_no, string layer, ref List<Path> paths)
		{
			List<Path> lpaths = GetPathType(shape_no, path_no, layer);

			for (int i = 0; i < lpaths.Count; i++)
				paths.Add(lpaths[i]);

			return true;
		}

		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no">Shape di cui si vuole il percorso</param>
		/// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
		/// <returns>Lista dei percorsi trovati</returns>
		public List<Path> GetTecnoPaths(int shape_no, int path_no)
		{
			return GetPathType(shape_no, path_no, LayersType.TECNO_INFO.ToString());
		}

		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no">Shape di cui si vuole il percorso</param>
		/// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
		/// <returns>Lista dei percorsi trovati</returns>
		public List<Path> GetToolPaths(int shape_no, int path_no)
		{
			return GetPathType(shape_no, path_no, LayersType.FINAL_TOOL.ToString());
		}

		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no">Shape di cui si vuole il percorso</param>
		/// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
		/// <returns>Lista dei percorsi trovati</returns>
		public List<Path> GetSlopedCutNegPaths(int shape_no, int path_no)
		{
			return GetPathType(shape_no, path_no, LayersType.SLOPED_CUTS_NEG.ToString());
		}

		/// <summary>
		/// Restituisce il percorso richiesto
		/// </summary>
		/// <param name="shape_no">Shape di cui si vuole il percorso</param>
		/// <param name="path_no">Numero del percorso, -1 per tutti i percorsi</param>
		/// <returns>Lista dei percorsi trovati</returns>
		public List<Path> GetSlopedCutPosPaths(int shape_no, int path_no)
		{
			return GetPathType(shape_no, path_no, LayersType.SLOPED_CUTS_POS.ToString());
		}
		
		public void DrawDrill(double r)
		{
			DrawGenericCircle(CursorPosition().X, CursorPosition().Y, r, true, System.Drawing.Color.White);
		}

		public void DeleteDrill()
		{
			DeleteGenericCircle();
		}

		public void DrawRecover(Point startPoint)
		{
			DrawGenericRectangle(startPoint.X, startPoint.Y, CursorPosition().Y - startPoint.Y, CursorPosition().X - startPoint.X, true, System.Drawing.Color.LimeGreen);
		}

		public void DeleteRecover()
		{
			DeleteGenericRectangle();
		}

		/// <summary>
		/// Restituisce il blocco dal codice
		/// </summary>
		/// <param name="code"></param>
		/// <returns></returns>
		public Block GetBlockByCode(string code)
		{
			foreach (KeyValuePair<int, Block> b in Blocks)
			{
				if (b.Value.Code.Trim() == code.Trim())
					return b.Value;
			}
			return null;
		}

        /// <summary>
        /// Restituisce il blocco dal nome
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public Block GetBlockByName(string name)
        {
            foreach (KeyValuePair<int, Block> b in Blocks)
            {
                if (b.Value.Name == name)
                    return b.Value;
            }
            return null;
        }


		public bool InsertSlab(string code, double dimX, double dimY, double originX, double originY, string jpgFile, Path slabPath, List<Path> defectsPaths, Point pt1, Point pt2, bool immediateLoadPhoto = true)
		{
			// Aggiunge il percorso lastra e i difetti
			int slab_num = InsertSlabDefectsRefPointPath(this, slabPath, defectsPaths, pt1, pt2, code, originX, originY);
			
			// Inserisce la foto
			this.AddBlockPhoto(mBlocks[slab_num], jpgFile, dimX, dimY, code);

		    this._photoInfo = new PhotoInfo(slab_num, 0, 0);

            if (immediateLoadPhoto)
			    this.LoadPhoto(mBlocks[slab_num], 0, 0);

			//this.AddPhoto(jpgFile, dimX, dimY, code);
			//this.LoadPhoto();

			// Disegna i vari path
            //DEVDEPT
            this.Repaint();
            this.ZoomAll();

			return true;
		}

		public int InsertBlock(Block b)
		{
			int idBlock = this.AddShape(b);	// accetta e  inserisce la shape nella finestra di destinazione

			// Esegue il repaint 
			this.Repaint(idBlock);

			this.ZoomAll();

			return idBlock;
		}

		/// <summary>
		/// Verifica la vicinanza del pezzo con altri pezzi
		/// </summary>
		public void CheckMagnet(Block movingBlock)
		{
			CheckMagnet(movingBlock, false, 0, 0);
		}

		public void CheckMagnet(Block movingBlock, bool allowSlopeLock, double piecethickness, double rem_dist)
		{
			bool intersect = false;
			Breton.Polygons.Point[] points;
			List<int> blocks = new List<int>();
			List<int> sides = new List<int>();
			Side sideMoving = null, sideLock = null;	// La coppia di lati pi� vicina del grezzo
			ToolInfo toolMoving = null, toolLock = null;
			List<Path> cutNeg, cutPos;

			// Recupera la zona d'interesse del pezzo che si va a muovere
			List<Path> paths = this.GetRawInterestZone(movingBlock.Id, -1);
			Path pIZ = paths[0];
			Side s1 = null, s2 = null;

			try
			{
				// Nasconde la linea eventualmente gi� disegnata
				this.DeleteGenericLine();
				mLockPiece = new LockPiece();
				mMovingPiece = new LockPiece();

				// Verifica quali pezzi, e relativi lati, fanno parte totalmente o parzialmente del pezzo che si sta muovendo
				foreach (KeyValuePair<int, Block> bl in this.Blocks)
				{
					// Verifica che il blocco che si sta analizzando non sia ne la lastra ne lo stesso pezzo 
					if (bl.Value != this.Slab && bl.Value.Code != movingBlock.Code)
					{
						// Estrae tutti i grezzi del pezzo (per ora ci sar� solo un grezzo, quello scelto)
						List<Path> raws = this.GetTecnoPaths(bl.Value.Id, -1);
						// Estrae tutti i toolpath del pezzo
						List<Path> toolpaths = this.GetToolPaths(bl.Value.Id, -1);

						// Aggiunge alla ricerca anche i percorsi di taglio inclinati
						cutNeg = this.GetSlopedCutPosPaths(bl.Value.Id, -1);
						if (cutNeg != null && cutNeg.Count > 0)
						{
							for (int i = 0; i < cutNeg.Count; i++)
								toolpaths.Add(cutNeg[i]);
						}

						cutPos = this.GetSlopedCutNegPaths(bl.Value.Id, -1);
						if (cutPos != null && cutPos.Count > 0)
						{
							for (int i = 0; i < cutPos.Count; i++)
								toolpaths.Add(cutPos[i]);
						}						

						// Cicla per tutti i lati del pezzo che si sta verificando
						for (int i = 0; i < toolpaths.Count; i++)
						{
							// Nikreg 2016_02_09: Non viene più fatto questo controllo: Se un toolpath ha più di un side significa che non è fatto con il disco
							if (//toolpaths[i].Count > 1 ||
								((ToolPath)toolpaths[i]).Info.Tool.Type != EN_TOOL_TYPE.EN_TOOL_TYPE_DISK ||
								((ToolPath)toolpaths[i]).LinkToTecnoPath.Type == Path.EN_PATH_MODE.EN_PATH_MODE_INTERIOR)
								continue;

							for (int k = 0; k < toolpaths[i].Count; k++)
							{
								s1 = toolpaths[i].GetSideRT(k);

								// Verifica che il lato sia un segmento
								if (s1 is Segment)
								{
									// Cicla per tutti i lati del pezzo che si sta muovendo
									for (int j = 0; j < pIZ.Count; j++)
									{
										s2 = pIZ.GetSideRT(j);

										// Verifica che il lato sia un segmento
										if (s2 is Segment)
										{
											// Se i due lati si intersecano prendo il lato s1 come uno dei lati da analizzare
											if (s1.Intersect(s2, out points) > 0)
											{
												intersect = true;
												break;
											}
										}
									}

									// Verifica se il lato del pezzo "fisso" fa parte dell'area d'interesse
									if (intersect)
									{
										sides.Add(i);
										blocks.Add(bl.Value.Id);
										intersect = false;
									}
									else
									{
										if (pIZ.IsInsideRT(s1.P1) && pIZ.IsInsideRT(s1.P2))
										{
											sides.Add(i);
											blocks.Add(bl.Value.Id);
										}
									}
								}
							}
						}
					}
				}

				if (blocks.Count == 0 || sides.Count == 0)
					return;

				// Dopo aver verificato quali sono i segmenti nella zona d'interesse, inizia la verifica della coppia di lati pi� vicina

				// Recupera i percorsi utensile del pezzo che si va a muovere
				paths = this.GetToolPaths(movingBlock.Id, -1);
				cutNeg = this.GetSlopedCutPosPaths(movingBlock.Id, -1);
				if (cutNeg != null && cutNeg.Count > 0)
				{
					for (int i = 0; i < cutNeg.Count; i++)
						paths.Add(cutNeg[i]);
				}

				cutPos = this.GetSlopedCutNegPaths(movingBlock.Id, -1);
				if (cutPos != null && cutPos.Count > 0)
				{
					for (int i = 0; i < cutPos.Count; i++)
						paths.Add(cutPos[i]);
				}						

				double dist = 0;

				// Cerca i 2 lati pi� vicini
				for (int i = 0; i < paths.Count; i++)
				{
					if (paths[i].Count > 1 ||
						((ToolPath)paths[i]).Info.Tool.Type != EN_TOOL_TYPE.EN_TOOL_TYPE_DISK ||
						((ToolPath)paths[i]).LinkToTecnoPath.Type == Path.EN_PATH_MODE.EN_PATH_MODE_INTERIOR)
						continue;

					for (int l = 0; l < paths[i].Count; l++)
					{
						Side s = paths[i].GetSideRT(l);

						if (s is Segment)
						{
							for (int j = 0; j < blocks.Count; j++)
							{
								List<Path> toolpaths = this.GetToolPaths(blocks[j], -1);
								cutNeg = this.GetSlopedCutPosPaths(blocks[j], -1);
								if (cutNeg != null && cutNeg.Count > 0)
								{
									for (int k = 0; k < cutNeg.Count; k++)
										toolpaths.Add(cutNeg[k]);
								}

								cutPos = this.GetSlopedCutNegPaths(blocks[j], -1);
								if (cutPos != null && cutPos.Count > 0)
								{
									for (int k = 0; k < cutPos.Count; k++)
										toolpaths.Add(cutPos[k]);
								}

								for (int a = 0; a < toolpaths[sides[j]].Count; a++)
								{
									Side sRef = toolpaths[sides[j]].GetSideRT(a);

									double newDist = 0;

									double midDist = s.MiddlePoint.Distance(sRef.MiddlePoint);
									double p1Dist = s.P1.Distance(sRef.P2);
									double p2Dist = s.P2.Distance(sRef.P1);

									if (midDist < p1Dist && midDist < p2Dist)
										newDist = midDist;
									else if (p1Dist <= midDist && p1Dist <= p2Dist)
										newDist = p1Dist;
									else if (p2Dist <= midDist && p2Dist <= p1Dist)
										newDist = p2Dist;

									// Verifica anche se l'inclinazione � compresa in un certo range
									if (Math.Abs(s.Inclination - sRef.Inclination) >= Math.PI - MathUtil.gdRadianti(10) &&
										Math.Abs(s.Inclination - sRef.Inclination) <= Math.PI + MathUtil.gdRadianti(10))
									{
										// Verifica se i 2 lati che si stanno considerando sono pi� vicini dei precedenti
										if (dist > newDist || dist == 0)
										{
											// Verifica che siano utensili congruenti, che i lati abbiano inclinazioni opposte
											if (((ToolPath)toolpaths[sides[j]]).Info.Tool == ((ToolPath)paths[i]).Info.Tool &&
												(Math.Abs((((ToolPathSegment)sRef).Info.Slope)) == Math.Abs((((ToolPathSegment)s).Info.Slope)) ||
												allowSlopeLock))
											{

												// Imposta le informazioni relative al pezzo fisso
												toolLock = ((ToolPath)toolpaths[sides[j]]).Info.Tool;
												sideLock = sRef;
												mLockPiece.LockBlock = this.GetBlock(blocks[j]);

												// Imposta le informazioni relative al pezzo che si sta muovendo
												toolMoving = ((ToolPath)paths[i]).Info.Tool;
												sideMoving = s;


												dist = newDist;
											}
										}
									}
								}
							}
						}
					}
				}

				// Se sono stati trovati i 2 lati cerca qual'� il punto pi� vicino cercando tra i 2 vertici e il punto medio
				if (sideLock != null && sideMoving != null)
				{
					Segment offsetLockSeg = null;
					Segment offsetMovingSeg = null;

					// Crea il nuovo lato per il controllo
					if (((ToolPathSegment)sideLock).Info.Slope != 0 || ((ToolPathSegment)sideMoving).Info.Slope != 0)
					{
						offsetLockSeg = GetOffsetSegment(sideLock, piecethickness, toolLock, rem_dist);
						offsetMovingSeg = GetOffsetSegment(sideMoving, piecethickness, toolMoving, 0);
					}
					else
					{
						offsetLockSeg = new Segment(sideLock.P1, sideLock.P2);
						offsetMovingSeg = new Segment(sideMoving.P1, sideMoving.P2);
					}

					//double midDist = sideLock.MiddlePoint.Distance(sideMoving.MiddlePoint);
					//double p1Dist = sideLock.P1.Distance(sideMoving.P2);
					//double p2Dist = sideLock.P2.Distance(sideMoving.P1);
					double midDist = offsetLockSeg.MiddlePoint.Distance(offsetMovingSeg.MiddlePoint);
					double p1Dist = offsetLockSeg.P1.Distance(offsetMovingSeg.P2);
					double p2Dist = offsetLockSeg.P2.Distance(offsetMovingSeg.P1);

					if (midDist < p1Dist && midDist < p2Dist)
					{
						this.DrawGenericLine(new Segment(sideLock.MiddlePoint, sideMoving.MiddlePoint));
						//mLockPiece.LockPoint1 = new CSnapPoint(sideLock.MiddlePoint.X, sideLock.MiddlePoint.Y, CBasicPaint.SnapMode.MID);
						//mMovingPiece.LockPoint1 = new CSnapPoint(sideMoving.MiddlePoint.X, sideMoving.MiddlePoint.Y, CBasicPaint.SnapMode.MID);
						mLockPiece.LockPoint1 = new CSnapPoint(offsetLockSeg.MiddlePoint.X, offsetLockSeg.MiddlePoint.Y, CBasicPaint.SnapMode.MID);
						mMovingPiece.LockPoint1 = new CSnapPoint(offsetMovingSeg.MiddlePoint.X, offsetMovingSeg.MiddlePoint.Y, CBasicPaint.SnapMode.MID);
					}
					else if (p1Dist <= midDist && p1Dist <= p2Dist)
					{
						this.DrawGenericLine(new Segment(sideLock.P1, sideMoving.P2));
						//mLockPiece.LockVertex = new CSnapPoint(sideLock.P1.X, sideLock.P1.Y, CBasicPaint.SnapMode.END);
						mLockPiece.LockVertex = new CSnapPoint(offsetLockSeg.P1.X, offsetLockSeg.P1.Y, CBasicPaint.SnapMode.END);
					}
					else if (p2Dist <= midDist && p2Dist <= p1Dist)
					{
						this.DrawGenericLine(new Segment(sideLock.P2, sideMoving.P1));
						//mLockPiece.LockVertex = new CSnapPoint(sideLock.P2.X, sideLock.P2.Y, CBasicPaint.SnapMode.END);
						mLockPiece.LockVertex = new CSnapPoint(offsetLockSeg.P2.X, offsetLockSeg.P2.Y, CBasicPaint.SnapMode.END);
					}

					// Imposta le variabili per effettuare l'aggancia
					//mLockPiece.LockSide = sideLock;
					mLockPiece.LockSide = offsetLockSeg;
					mMovingPiece.LockBlock = movingBlock;
					mMovingPiece.LockSide = offsetMovingSeg;
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("SlabOperations.CheckMagnet ", ex);
			}
		}

		private Segment GetOffsetSegment(Side seg, double piecethickness, ToolInfo tool, double rem_dist)
		{
			Segment retSeg = null;

			if (((ToolPathSegment)seg).Info.Slope == 0)
			{
				//retSeg = new Segment(seg.P1, seg.P2);
				double val = tool.Radius;
				retSeg = (Segment)seg.OffsetSide(val + rem_dist, -1);
			}
			else if (((ToolPathSegment)seg).Info.Slope > 0) // && ((ToolPathSegment)sideMoving).Info.Slope > 0)
			{
				double max_slope = ((ToolPathSegment)seg).Info.Slope;
				double val = tool.Radius / Math.Cos(max_slope);

				retSeg = (Segment)seg.OffsetSide(val + rem_dist, -1);
			}
			else if (((ToolPathSegment)seg).Info.Slope < 0) // || ((ToolPathSegment)sideMoving).Info.Slope < 0)
			{
				double min_slope = ((ToolPathSegment)seg).Info.Slope;
				double val = tool.Radius / Math.Cos(Math.Abs(min_slope));

				double thickness_corrected = piecethickness - tool.Radius * Math.Sin(Math.Abs(min_slope));
				if (thickness_corrected > 0)
				{
					double offset_correction = thickness_corrected * Math.Tan(Math.Abs(min_slope));
					val = val + offset_correction;
				}

				retSeg = (Segment)seg.OffsetSide(val + rem_dist, -1);
			}

			return retSeg;
		}

		/// <summary>
		/// Aggancia i pezzi tra di loro
		/// </summary>
		public void LockPieces()
		{
			Breton.Polygons.Point movingPoint = null, lockPoint = null;

			if (mLockPiece.LockSide == null || mMovingPiece.LockSide == null)
				return;

			double i1 = mLockPiece.LockSide.Inclination;
			double i2 = mMovingPiece.LockSide.Inclination;

			// Calcola la rotazione necessaria
			double alpha = (i1 - i2) - Math.PI;

			if (Math.Abs(alpha) == 2 * Math.PI)
				alpha = 0d;

			// Calcola la traslazione necessaria
			Segment newSegment = new Segment((Segment)mLockPiece.LockSide);

			if (mLockPiece.LockVertex != null)
			{
				// Verifica se il vertice a cui agganciare il pezzo � P1 o P2
				if (mLockPiece.LockVertex == mLockPiece.LockSide.P1)
				{
					movingPoint = mMovingPiece.LockSide.P2;
					lockPoint = newSegment.P1;
				}
				else if (mLockPiece.LockVertex == mLockPiece.LockSide.P2)
				{
					movingPoint = mMovingPiece.LockSide.P1;
					lockPoint = newSegment.P2;
				}
			}
			else
			{
				movingPoint = mMovingPiece.LockPoint1;
				lockPoint = newSegment.MiddlePoint;
			}

			// Nasconde la linea eventualmente gi� disegnata
			this.DeleteGenericLine();

			bool TmpCollision = mCollisionEnable;
			// Se i due lati sono uguali, annulla il controllo collisione
			if (mMovingPiece.LockSide.Length == mLockPiece.LockSide.Length)
			{
				mCollisionEnable = true;
			}

			this.ManualRotate(mMovingPiece.LockBlock.Id, movingPoint.X, movingPoint.Y, alpha);

			double deltaX = lockPoint.X - movingPoint.X;
			double deltaY = lockPoint.Y - movingPoint.Y;

			this.ManualMove(mMovingPiece.LockBlock.Id, deltaX, deltaY);

			// Reimposta l'eventuale valore per l'abilitazione della collisione
			mCollisionEnable = TmpCollision;

			mMovingPiece = new LockPiece();
			mLockPiece = new LockPiece();
		}

		/// <summary>
		/// Verifica se c'è collisione tra il/i pezzo/i che si sta/nno muovendo
		/// Il controllo viene effettuato tra il percorso di asportazione e il grezzo
		/// </summary>
		/// <param name="blocks"></param>
		/// <param name="cbpe"></param>
		/// <returns></returns>
		public bool IsCollision(List<Block> movingBlocks)
		{
			return IsCollision(movingBlocks, null, null, null);
		}

		public bool IsCollision(List<Block> movingBlocks, List<string> movingBlocksLayers, List<Block> fixedBlocks, List<string> fixedBlocksLayers)
		{
			List<Breton.Polygons.Point> intersectPoint;

			try
			{
				// Se la collisione è consentita ritorna sempre false
				if (mCollisionEnable)
				{
					this.RemoveCollisions();
					return false;
				}

				// Scorre i pezzi che si stanno muovendo
				foreach (Block b in movingBlocks)
				{
					// Scorre i percorsi del pezzo per estrarre i percorsi di asportazione e per il grezzo
					SortedList<int, Path> paths = b.GetPaths();
					foreach (KeyValuePair<int, Path> p in paths)
					{
						if (fixedBlocks == null)
						{
							// Verifica la collisione delle asportazioni del pezzo 
							// che si sta muovendo con i grezzi degli altri pezzi
							if (p.Value.Layer == CBPExtension.LayersType.FINAL_TOOL_REM.ToString())
							{
								foreach (KeyValuePair<int, Block> bl in this.Blocks)
								{
									// Verifica che il blocco che si sta analizzando non sia ne la lastra
									// ne lo stesso pezzo ne uno degli altri pezzi che si sta muovendo insieme

									if (bl.Value != this.Slab &&
										bl.Value.Code != b.Code &&
										!movingBlocks.Exists(delegate(Block x) { return x.Code == bl.Value.Code; }))
									{
										// Estrae tutti i grezzi del pezzo (per ora ci sar� solo un grezzo, quello scelto)
										List<Path> raws = this.GetRaw(bl.Value.Id, -1);

										foreach (Path r in raws)
										{
											// Controllo di collisione										
											if (p.Value.IntersectRT(r, out intersectPoint))
												return true;
										}
									}
								}
							}
							// Verifica la collisione tra il grezzo del pezzo che si 
							// sta muovendo con le asportazioni degli altri pezzi
							else if (p.Value.Layer == CBPExtension.LayersType.RAW.ToString())
							{
								foreach (KeyValuePair<int, Block> bl in this.Blocks)
								{
									// Verifica che il blocco che si sta analizzando non sia lo stesso pezzo 
									// ne uno degli altri pezzi che si sta muovendo insieme
									if (bl.Value.Code != b.Code &&
										!movingBlocks.Exists(delegate(Block x) { return x.Code == bl.Value.Code; }))
									{
										// Estrae tutti i grezzi del pezzo (per ora ci sar� solo un grezzo, quello scelto)
										List<Path> rems = this.GetFinalPathRem(bl.Value.Id, -1);

										foreach (Path r in rems)
										{
											// Controllo di collisione
											if (p.Value.IntersectRT(r))
												return true;
										}
									}
								}

								// Verifica la collisione del grezzo del pezzo che si sta muovendo con il bordo della lastra
								SortedList<int, Path> slabPaths = this.Slab.GetPaths();
								foreach (KeyValuePair<int, Path> s in slabPaths)
								{
									if (s.Value.Layer == CBPExtension.LayersType.SLAB.ToString() && p.Value.IntersectRT(s.Value, out intersectPoint))
										return true;
								}
							}
                            // Verifica la collisione tra il grezzo del pezzo che si 
                            // sta muovendo con le asportazioni degli altri pezzi
                            else if (p.Value.Layer == LayersType.REMNANT.ToString())
                            {
                                foreach (KeyValuePair<int, Block> bl in this.Blocks)
                                {
                                    // Verifica che il blocco che si sta analizzando non sia lo stesso pezzo 
                                    // ne uno degli altri pezzi che si sta muovendo insieme
                                    if (bl.Value.Code != b.Code &&
                                        !movingBlocks.Exists(delegate(Block x) { return x.Code == bl.Value.Code; }))
                                    {
                                        // Estrae tutti i grezzi del pezzo (per ora ci sar� solo un grezzo, quello scelto)
                                        List<Path> rems = this.GetFinalPathRem(bl.Value.Id, -1);

                                        foreach (Path r in rems)
                                        {
                                            // Controllo di collisione
                                            if (p.Value.IntersectRT(r))
                                                return true;
                                        }
                                    }
                                }

                                //// Verifica la collisione del grezzo del pezzo che si sta muovendo con il bordo della lastra
                                //SortedList<int, Path> slabPaths = this.Slab.GetPaths();
                                //foreach (KeyValuePair<int, Path> s in slabPaths)
                                //{
                                //    if (s.Value.Layer == CBPExtension.LayersType.SLAB.ToString() && p.Value.IntersectRT(s.Value, out intersectPoint))
                                //        return true;
                                //}
                            }

						}
						else
						{
							if (movingBlocksLayers.Contains(p.Value.Layer))
							{
								foreach (Block bl in fixedBlocks)
								{
									SortedList<int, Path> fixpaths = bl.GetPaths();
									foreach (KeyValuePair<int, Path> fp in fixpaths)
									{
										if (fixedBlocksLayers.Contains(fp.Value.Layer))
										{
											// Controllo di collisione										
											if (p.Value.IntersectRT(fp.Value, out intersectPoint))
												return true;
										}
									}
								}
							}
						}

					}
				}

				this.RemoveCollisions();
				return false;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("SlabOperations.IsCollision. Error ", ex);
				return false;
			}
		}
        #endregion

        #region Private Methods

        /// <summary>
        /// Aggiunge un percorso
        /// </summary>
        /// <param name="shape_no">Shape alla quale aggiungere il percorso</param>
        /// <param name="extPath">Path esterno da aggiungere</param>
        /// <param name="layer">Layer su cui aggiungere il percorso</param>
        /// <returns>Indice del percorso aggiunto</returns>
        private int AddPathType(int shape_no, Path extPath, string layer)
        {
            try
            {
                // Set della matrice del percorso aggiunto come quella del blocco a cui si riferisce
				//extPath.MatrixRT = mBlocks[shape_no].MatrixRT;
                // Assegno il layer
                extPath.Layer = layer;

                // Aggiungo il percorso esterno
                int path_no = base.AddPath(shape_no, extPath);
                return path_no;
            }
            catch (Exception e) { return -1; }
        }

        /// <summary>
        /// Aggiunge un percorcorso di tipo foro alla shape specificata, sul layer indicato
        /// </summary>
        /// <param name="shape_no">Shape a cui aggiungere il foro</param>
        /// <param name="hole">Percorso del foro</param>
        /// <param name="layer">layer di identificazione del tipo di percorso foro</param>
        /// <returns>Indice di inserimento</returns>
        private int AddHole(int shape_no, Path hole, string layer)
        {
            // Set della matrice del percorso aggiunto come quella del blocco a cui si riferisce
            hole.MatrixRT = mBlocks[shape_no].MatrixRT;
            // Assegno il layer
            hole.Layer = layer;
            // aggiungo il percorso foro
            int path_no = base.AddPath(shape_no, hole);
            return path_no;  
        }

        /// <summary>
        /// Rimuove il percorso specificato o tutti i percorsi della shape specificata
        /// </summary>
        /// <param name="shape_no">Shape da cui rimuovere il/i percorso/i </param>
        /// <param name="path_no">Chiave del percorso da rimuove, -1 per rimuovere tutti i percorsi</param> 
        /// <param name="layer">layer di identificazione del tipo di percorso</param>
        /// <returns>true: percorso rimosso / false: percorso non trovato</returns>
        private bool Remove(int shape_no, int path_no, string layer)
        {
            try
            {
				ArrayList keyValues = new ArrayList();
                SortedList<int, Path> lstPaths = mBlocks[shape_no].GetPaths();
                
                if (path_no == -1)   // rimuovo tutti i percorsi della sagoma specificata, da un determinato layer
                {
                    //ciclo per tutti i blocchi
					foreach (KeyValuePair<int, Path> p in lstPaths)
					{
						if (p.Value.Layer == layer)
							keyValues.Add(p.Key);
					}
					for (int i = 0; i < keyValues.Count; i++)
					{
						base.RemovePath(shape_no, (int)keyValues[i]);
					}
                    return true;
                }
                else
                {

                    //ciclo per tutti i blocchi
					foreach (KeyValuePair<int, Path> p in lstPaths)
					{
						if (p.Value.Layer == layer && p.Key == path_no)
							keyValues.Add(p.Key);
					}
					for (int i = 0; i < keyValues.Count; i++)
					{
						base.RemovePath(shape_no, (int)keyValues[i]);
					}                                   
                }
                return false;                // cancellazione riuscita
            }
            catch (Exception e) { return false; }
        }

        /// <summary>
        /// Restituisce il path richiesto
        /// </summary>
        /// <param name="shape_no">numero della shape di cui si vuole il percorso</param>
        /// <param name="path_no">chiave del path</param>
        /// <param name="layer">layer di identificazione del tipo di percorso</param>
        /// <returns>Lista dei path trovati</returns>
		private List<Path> GetPathType(int shape_no, int path_no, string layer)
		{
            try
            {
                SortedList<int, Path> lstPaths = mBlocks[shape_no].GetPaths();
                List<Path> paths = new List<Path>();

                // vedo tutti i percorsi per trovare il blocco particolare
                for (int i = 0; i < lstPaths.Count; i++)
                {
                    Path p = lstPaths[lstPaths.Keys[i]];
                    if (path_no == -1)                                                                       // voglio tutti i percorsi
                    {
                        if (p.Layer == layer)
                            paths.Add(p);
                    }
                    else                                                                                     // voglio un percorso particolare
                    {
                        if (p.Layer == layer && lstPaths.Keys[i] == path_no)
                            paths.Add(p);
                    }
                }

                return paths;
            }
            catch (Exception e) { return null; }
        }

		

        /// <summary>
        /// Aggiunta dei Layer e Inizializzazione per il componente di disegno
        /// </summary>
		protected virtual void InitLayer()
		{
			InitLayer("");
		}

        protected virtual void InitLayer(string layerXml)
        {
            // inizializzazione dei (19) layer
            CLayer layer = new CLayer(LayersType.PHOTO.ToString(), true, true, System.Drawing.Color.Olive, "", 30);
            layer.ZLevel = -2;
            AddLayer(layer);
            layer = new CLayer(LayersType.SLAB.ToString(), true, true, System.Drawing.Color.SaddleBrown, "", 30);
            layer.ZLevel = -1;
            //layer = new CLayer(LayersType.SLAB.ToString(), true, true, true, System.Drawing.Color.SaddleBrown, "", 30); 
            AddLayer(layer);

            //layer = new CLayer(LayersType.SLAB_NOIMAGE.ToString(), true, true, System.Drawing.Color.SaddleBrown, "", 30);
            ////layer = new CLayer(LayersType.SLAB.ToString(), true, true, true, System.Drawing.Color.SaddleBrown, "", 30); 
            //AddLayer(layer);

            layer = new CLayer(LayersType.DEFECT.ToString(), true, true, true, System.Drawing.Color.Sienna, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.REFERENCE_POINT1.ToString(), true, true, System.Drawing.Color.SaddleBrown, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.REFERENCE_POINT2.ToString(), true, true, System.Drawing.Color.SaddleBrown, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.SHAPE.ToString(), true, true, true, false, Color.Red, "", 30);
            //layer = new CLayer(LayersType.SHAPE.ToString(), true, true, System.Drawing.Color.Red, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.SHAPE_HOLE.ToString(), true, true, false, true,  System.Drawing.Color.Red, "", 30);
            //layer = new CLayer(LayersType.SHAPE_HOLE.ToString(), true, true, System.Drawing.Color.Red, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.RAW.ToString(), true, true, System.Drawing.Color.Green, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.RAW_HOLE.ToString(), true, true, System.Drawing.Color.Green, "", 30);
            AddLayer(layer);
			layer = new CLayer(LayersType.BLIND_HOLE.ToString(), true, true, System.Drawing.Color.White, "", 30);
			AddLayer(layer);
            layer = new CLayer(LayersType.DISC.ToString(), true, true, System.Drawing.Color.Gold, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.DISC_REM.ToString(), true, true, System.Drawing.Color.Orange, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.COMBINED.ToString(), true, true, System.Drawing.Color.DodgerBlue, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.COMBINED_REM.ToString(), true, true, System.Drawing.Color.DarkCyan, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.MILL.ToString(), true, true, System.Drawing.Color.DeepPink, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.MILL_REM.ToString(), true, true, System.Drawing.Color.DarkOrchid, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.FINAL_TOOL.ToString(), true, true, System.Drawing.Color.CornflowerBlue, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.FINAL_TOOL_REM.ToString(), true, true, true, System.Drawing.Color.Blue, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.EDGE_FINISHING_TOOL.ToString(), true, true, System.Drawing.Color.DarkSeaGreen, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.EDGE_FINISHING_DIM.ToString(), true, true, System.Drawing.Color.Lime, "", 30); 
            AddLayer(layer);
			layer = new CLayer(LayersType.COLLISION.ToString(), true, true, System.Drawing.Color.Red, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SLAB_CUTS.ToString(), true, true, System.Drawing.Color.Red, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.TEXT.ToString(), true, true, System.Drawing.Color.Black, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SYMBOL.ToString(), true, true, System.Drawing.Color.Yellow, "", 30);
			AddLayer(layer);

			//layer = new CLayer(LayersType.RENDER_PATH.ToString(), true, true, System.Drawing.Color.White, "", 30);
			//AddLayer(layer);
			//layer = new CLayer(LayersType.RAW_SMALL.ToString(), true, true, System.Drawing.Color.White, "", 30);
			//AddLayer(layer);
			//layer = new CLayer(LayersType.RAW_BIG1.ToString(), true, true, System.Drawing.Color.White, "", 30);
			//AddLayer(layer);
			//layer = new CLayer(LayersType.RAW_BIG2.ToString(), true, true, System.Drawing.Color.White, "", 30);
			//AddLayer(layer);
			//layer = new CLayer(LayersType.RAW_INTEREST_ZONE.ToString(), true, true, System.Drawing.Color.White, "", 30);
			//AddLayer(layer);
			//layer = new CLayer(LayersType.TEMP.ToString(), true, true, System.Drawing.Color.White, "", 30);
			//AddLayer(layer);

			layer = new CLayer(LayersType.TABLE.ToString(), true, true, System.Drawing.Color.LimeGreen, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.REPOSITORY.ToString(), true, true, System.Drawing.Color.Yellow, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SUCKERBAR.ToString(), true, true, System.Drawing.Color.Green, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SUCKERON.ToString(), true, true, true, System.Drawing.Color.LightGreen, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SUCKEROFF.ToString(), true, true, true, System.Drawing.Color.Green, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.PARTITION.ToString(), true, true, System.Drawing.Color.Fuchsia, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.PARTITION_CUTS.ToString(), true, true, System.Drawing.Color.Red, "", 80);
			AddLayer(layer);
			layer = new CLayer(LayersType.GEO_HOLES.ToString(), true, true, System.Drawing.Color.Aquamarine, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.INTERRUPTED_CUTS.ToString(), true, true, System.Drawing.Color.LimeGreen, "", 80);
			AddLayer(layer);
			layer = new CLayer(LayersType.BRIDGE.ToString(), true, true, System.Drawing.Color.DarkOrange, "", 80);
			AddLayer(layer);
			layer = new CLayer(LayersType.SLOPED_CUTS_NEG.ToString(), true, true, System.Drawing.Color.DarkCyan, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SLOPED_CUTS_POS.ToString(), true, true, System.Drawing.Color.Yellow, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SLOPED_CUTS_NEG_REM.ToString(), true, true, true, System.Drawing.Color.DarkCyan, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.SLOPED_CUTS_POS_REM.ToString(), true, true, true, System.Drawing.Color.Yellow, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.INTERRUPTED_SLOPED_CUTS_NEG.ToString(), true, true, System.Drawing.Color.LightSeaGreen, "", 80);
			AddLayer(layer);
			layer = new CLayer(LayersType.INTERRUPTED_SLOPED_CUTS_POS.ToString(), true, true, System.Drawing.Color.DarkKhaki, "", 80);
			AddLayer(layer);
			layer = new CLayer(LayersType.HOLES.ToString(), true, true, true, System.Drawing.Color.Aquamarine, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.RODDING.ToString(), true, true, System.Drawing.Color.White, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.RODDING_TOOL.ToString(), true, true, true, System.Drawing.Color.White, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.MACHINE_BOUNDARY.ToString(), true, true, System.Drawing.Color.Yellow, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.RESIDUAL.ToString(), true, true, true, System.Drawing.Color.LightGray, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.PAUSE.ToString(), true, true, true, System.Drawing.Color.Gray, "", 30);
			AddLayer(layer);
			layer = new CLayer(LayersType.UNLOAD.ToString(), true, true, true, System.Drawing.Color.Goldenrod, "", 30);
			AddLayer(layer);
            layer = new CLayer(LayersType.GROOVE.ToString(), true, true, System.Drawing.Color.Orange, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.GROOVE_TOOL.ToString(), true, true, System.Drawing.Color.OrangeRed, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.GROOVE_TOOL_REM.ToString(), true, true, true, System.Drawing.Color.Orange, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.GROOVE_CUTS.ToString(), true, true, System.Drawing.Color.BlueViolet, "", 80);
            AddLayer(layer);
			layer = new CLayer(LayersType.POCKET.ToString(), true, true, System.Drawing.Color.Maroon, "", 30);
			AddLayer(layer);

            // 20161031 Remnants
            layer = new CLayer(LayersType.REMNANT.ToString(), true, false, true, false, System.Drawing.Color.Green, "", 30);
            AddLayer(layer);


			LoadConfiguredLayer(layerXml);

            RearrangeLayers();
        }

        protected override void RearrangeLayers()
        {
            if (mLayers.Count > 0)
            {
                var rearranged = mLayers.Values.OrderByDescending(l => l.Fill).ThenByDescending(l => l.UnFill).ToList();
                mLayers.Clear();
                ClearLayoutLayers();
                for (int i = 0; i < rearranged.Count; i++)
                {
                    AddLayer(rearranged[i]);
                }
            }
        }


		/// <summary>
		/// Inserisce nel componente CBasicPaintExtesion i percorsi lastra e gli eventuali difetti
		/// </summary>
		/// <param name="cbpe">Componente CBasicPaintExtesion in cui inserire i path</param>
		/// <param name="slabPath">percorso della lastra</param>
		/// <param name="defectsPaths">percorsi dei difetti</param>
		/// <param name="pt1">punto di riferimento 1</param>
		/// <param name="pt2">punto di riferimento 2</param>
		/// <param name="file">File xml con i path lastra e difetti</param>
		/// <param name="name">Codice lastra</param>
		/// <param name="originX">Origine X</param>
		/// <param name="originY">Origine Y</param>	
		/// <returns>restituisce l'id del blocco creato</returns>
		private int InsertSlabDefectsRefPointPath(CBPExtension cbpe, Path slabPath, List<Path> defectsPaths, Point pt1, Point pt2, string name, double originX, double originY)
		{
			// Inserisce il percorso della lastra
			int num_slab = cbpe.AddSlab(name, slabPath, originX, originY);

			// Verifica ora se sono presenti difetti e in caso li inserisce
			foreach (Path dp in defectsPaths)
			{
				cbpe.AddDefect(dp, originX, originY);
			}

			// Aggiunge i punti di riferimento per i bollini
			if (pt1 != null && pt2 != null)
			{
				cbpe.AddReferencePoint(pt1, true); //, originX, originY);
				cbpe.AddReferencePoint(pt2, false); //, originX, originY);
			}

			return num_slab;
		}

        private static bool IsRemnantBlock(Block block)
        {
            if (block != null)
            {
                var attributeValue = block.GetAttributeValue(REMNANT_BLOCK_ATTRIBUTE_KEY);
                if (!string.IsNullOrEmpty(attributeValue) && attributeValue.ToLowerInvariant() == "true")
                    return true;
            }

            return false;
        }

        public void SelectBlock(Block block)
        {
            if (block == null || block.Id == -1) return;
            Select(block.Id);
        }
        #endregion
	}

	public class LockPiece
	{
		public CSnapPoint LockPoint1;
		public CSnapPoint LockVertex;
		public Block LockBlock;
		public Breton.Polygons.Path LockPath;
		public Side LockSide;
		public ToolInfo ToolSide;

		public LockPiece()
		{
		}
	}

}