using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Breton.MathUtils;
using Breton.TraceLoggers;
using Breton.Polygons;
using devDept.Eyeshot;
using devDept.Graphics;
using VectorDraw.Professional.Control;
using VectorDraw.Professional.vdPrimaries;
using VectorDraw.Professional.vdFigures;
using VectorDraw.Professional.Constants;
using VectorDraw.Professional.vdCollections;
using VectorDraw.Professional.vdObjects;
using VectorDraw.Geometry;
using VectorDraw.Render;
using DesignCenterInterface.Properties;
using System.Xml.Serialization;
using Path = Breton.Polygons.Path;
using Point = Breton.Polygons.Point;
using vdFramedControl = vdControls.vdFramedControl;


namespace Breton.DesignCenterInterface
{

    /// <summary>
    /// CBasicPaint gestisce la visualizzazione di percorsi e l'interazione con l'utente.
    /// </summary>
    public class CBasicPaint_Eyeshot

    {
        #region Variables

        protected SortedList<int, Block> mBlocks;
        protected List<CSelection> mSelections;							// array elementi selezionati
        protected SortedList<string, CLayer> mLayers;					// array dei layers presenti
        protected vdFramedControl mDisplayObject = null;     // oggetto utilizzato per il display
        protected CPhoto mPhoto;
        protected SortedList<int, CText> mText;					        // array dei text presenti
        protected bool move = false;
        protected bool rotate = false;
        protected bool dragdrop = false;
        protected bool regen = false;
        protected int indexBlock = -1;
        protected int indexText = -1;

        //TODO devDept: Class changed.
        //protected ViewportLayoutEx cadLayout;
        protected ViewportLayout cadLayout;


        private CBasicPaintEventArgs ev = null;

        private Point _user = new Point();          //punto cliccato dall'utente
        private Point _lastOK = new Point();        //ultimo punto valido in mm
        private List<RTMatrix> _lastMatrix = new List<RTMatrix>();
        private Block _blockDragDrop;// = new Block();	//blocco di cui si sta facendo il drag&drop
        private double _xDragDrop = 0;					//delta x da applicare al D&D nella finestra di destinazione
        private double _yDragDrop = 0;					//delta y da applicare al D&D nella finestra di destinazione
		private List<SnapMode> _osnapModes;
		private List<string> _snapLayers;	
		private AxisDirection _axisDirection;
		private bool _locked = false;
		private Color _backColor;
		private Point _startZoomWindow = null;
		private bool _zoomWindow = false;
		private bool _showVDLayout;


		private OsnapPoint _lastSnapPoint;
        private vdSelection _selActive = null;			//selezione di entit� che si stanno muovendo o ruotando, usata per modificare velocemente le coordinate
        private vdLine _lineMoveRotate = new vdLine();	//linea per il movimento o per la rotazione
		private vdLine _line1, _line2, _line3, _line4;		// Linee per gli snap
		private vdLine _genericLine;
		private vdCircle _genericCircle;
		private vdRect _genericRectangle;



        private Timer mTimer;

        //private enum Action
        //{ 
        //    DEFAULT,
        //    MOVE,
        //    ROTATE
        //}

        //public enum HighLigthType
        //{ 
        //    NONE,
        //    NORMAL,
        //    DOT
        //}

        //public enum SnapMode
        //{
        //    ALL,
        //    APPARENTINT,
        //    CEN,
        //    END,
        //    INS,
        //    INTERS,
        //    MID,
        //    NEA,
        //    NODE,
        //    PER,
        //    QUA,
        //    TANG,
        //    NONE
        //}

        ///// <summary>
        ///// Orientamento assi
        ///// </summary>
        //public enum AxisDirection
        //{
        //    LEFT_BOTTOM,	
        //    LEFT_TOP,
        //    RIGHT_BOTTOM,			
        //    RIGHT_TOP
        //}

        //public enum DimensionType
        //{ 
        //    LINEAR = 1,
        //    ORIZONTAL = 2,
        //    VERTICAL = 3,
        //    ANGULAR,
        //    RADIAL,
        //    DIAMETER
        //}

        private Action currentAction = CBasicPaint.Action.DEFAULT;

        #endregion

        #region Properties

		/// <summary>
		/// Ritorna la lista dei blocchi correntemente in uso
		/// </summary>
		public SortedList<int, Block> Blocks
        {
            get { return mBlocks; }
        }

		/// <summary>
		/// Ritorna la lista delle selezioni correntemente in uso 
		/// </summary>
        public List<CSelection> Selections
        {
            get { return mSelections; }
        }

		/// <summary>
		/// Ritorna la lista dei layers in uso 
		/// </summary>
        public SortedList<string, CLayer> Layers
        {
            get { return mLayers; }
        }

		/// <summary>
		/// Ritorna la foto caricata
		/// </summary>
        public CPhoto Photo
        {
            set { mPhoto = value; }
            get { return mPhoto; }
        }

		/// <summary>
		/// Ritorna la lista dei testi inseriti 
		/// </summary>
		public SortedList<int, CText> Texts
        {
            get { return mText; }
        }

		/// <summary>
		/// Riferimento all'oggetto utilizzato per il display dei percorsi (es. Vector Draw). 
		/// </summary>
        public Object DisplayObject
        {
            get { return mDisplayObject; }
        }

		/// <summary>
		/// flag che indica se l'azione Move � attiva 
		/// </summary>
        public bool Move
        {
            set
            { 
                move = value;
                currentAction = Action.DEFAULT;

				//lineMoveRotate.Deleted = true;
				//Redraw();
            }
            get { return move; }
        }

		/// <summary>
		/// flag che indica se l'azione Rotate � attiva 
		/// </summary>
        public bool Rotate
        {
            set
            { 
                rotate = value;
                currentAction = Action.DEFAULT;

                _lineMoveRotate.Deleted = true;
                Redraw();
            }
            get { return rotate; }
        }

		/// <summary>
		/// flag che indica se l'azione Drag&Drop � attiva
		/// </summary>
		public bool DragDrop
        {
            set{ dragdrop = value; }
            get { return dragdrop; }
        }

		/// <summary>
		/// Imposta o ritorna il blocco di cui si sta facendo il drag and drop
		/// </summary>
        public Block BlockDragDrop
        {
            set { _blockDragDrop = value; }
            get { return _blockDragDrop; }
        }

      	/// <summary>
		/// Imposta o ritorna l'offset x nella finestra sorgente del blocco di sta facendo il drag and drop
		/// </summary>
        public double XDragDrop
        {
            set { _xDragDrop = value; }
            get { return _xDragDrop; }
        }

		/// <summary>
		/// Imposta o ritorna l'offset y nella finestra sorgente del blocco di sta facendo il drag&drop
		/// </summary>
        public double YDragDrop
        {
            set { _yDragDrop = value; }
            get { return _yDragDrop; }
        }

		/// <summary>
		/// Ritorna il punto di Snap
		/// </summary>
		public CSnapPoint SnapPoint
		{
			get
			{
				if (_lastSnapPoint == null)
					return null;

				CSnapPoint s = new CSnapPoint(_lastSnapPoint.x, _lastSnapPoint.y, SnapMode.NONE);

				switch (_lastSnapPoint.Mode)
				{
					case OsnapMode.END:
						s.Mode = SnapMode.END;
						break;

					case OsnapMode.MID:
						s.Mode = SnapMode.MID;
						break;

					case OsnapMode.NEA:
						s.Mode = SnapMode.NEA;
						break;
				}

				return s;
			}
		}

		/// <summary>
		/// Indica se il componente � bloccato o no
		/// </summary>
		public bool Locked
		{
			get { return _locked; }
		}



        //TODO: De Conti

        /// <summary>
        /// Get/Set punto centrale della vista corrente del disegno
        /// </summary>
        public Point ViewCenter
        {
            get
            {
                gPoint p = mDisplayObject.BaseControl.ActiveDocument.ViewCenter;
                return new Point(p.x, p.y);
            }
            set
            {
                gPoint p = new gPoint();
                p.x = value.X;
                p.y = value.Y;
                mDisplayObject.BaseControl.ActiveDocument.ViewCenter = p;
            }

        }
        /// <summary>
        /// Get/Set dimensione dell'altezza della vista corrente del disegno
        /// </summary>
        public double ViewSize
        {
            get
            {
                double size = mDisplayObject.BaseControl.ActiveDocument.ViewSize;
                return size;
            }
            set
            {
                mDisplayObject.BaseControl.ActiveDocument.ViewSize = value;
            }
        }

        #endregion

        #region CBasicPaint

        #region Constructors

        public CBasicPaint_Eyeshot()
        {
        }

        /// <summary>
        /// costruttore
        /// </summary>
        /// <param name="VD">l'oggetto di disegno (Vector Draw)</param>
		public CBasicPaint_Eyeshot(vdFramedControl VD): this(VD, AxisDirection.LEFT_BOTTOM)
        {

		}

        public CBasicPaint_Eyeshot(vdFramedControl VD, AxisDirection ax)
        {
            mDisplayObject = VD;
			_axisDirection = ax;
			_backColor = mDisplayObject.BaseControl.ActiveDocument.Palette.Background;

            InitVectorDraw();

            mSelections = new List<CSelection>();
            mLayers = new SortedList<string, CLayer>();
            mBlocks = new SortedList<int, Block>();
            mPhoto = new CPhoto();
            mText = new SortedList<int, CText>();
			_osnapModes = new List<SnapMode>();

            mTimer = new Timer();
            mTimer.Tick += new EventHandler(mTimer_Tick);
            mTimer.Interval = 500;
        }

        #endregion

        #region Public Methods - basic and operation

        /// <summary>
        /// Elimina tutti i riferimenti ai blocchi e azzera l'area di lavoro
        /// </summary>
        public virtual void Reset()
        {
            try
            {
                // elimina tutte le informazioni
                mBlocks.Clear();
                mSelections.Clear();
                mLayers.Clear();
                mPhoto = new CPhoto();
                mText.Clear();
                move = false;
                rotate = false;
                currentAction = Action.DEFAULT;
                indexBlock = -1;
                indexText = -1;
				_locked = false;

                NewActiveDocument();				
				ShowVDLayout();
				ShowAxis(true);
				SetAxisDirection(_axisDirection);
            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }

        }

        /// <summary>
        /// Aggiunge la foto
        /// </summary>
		public void LoadPhoto()
		{
			LoadPhoto(null, 0, 0);
		}
		/// <summary>
		/// Aggiunge la foto
		/// </summary>
		/// <param name="b">Blocco di riferimento</param>
		/// <param name="x">Coordinata X di inserimento</param>
		/// <param name="y">Coordinata Y di inserimento</param>
        public void LoadPhoto(Block b, double x, double y)
        {
            try
            {
				CPhoto photo;

				if (b != null && b.Photo != null)
					photo = b.Photo;
				else
					photo = mPhoto;

				if (photo == null) return;

				if (File.Exists(photo.Path))
                {
                    gPoint coord = new gPoint(x, y, 0);
                    vdImage img = new vdImage();

                    img.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
                    img.setDocumentDefaults();
					img.Layer = FindVdLayer(photo.Layer);

                    img.InsertionPoint = coord;

					img.Width = photo.Width;
					img.Height = photo.Height;
					img.ImageDefinition = mDisplayObject.BaseControl.ActiveDocument.Images.Add(photo.Path);

					img.Label = photo.Name;
                    mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(img);

                }
                else
                {

                }

            }
			catch (Exception ex) { TraceLog.WriteLine(ToString() + ".LoadPhoto: ", ex); }

        }

        /// <summary>
        /// Forza il ridisegno della finestra
        /// </summary>
        public void Repaint()
        {
            try
            {
				//ciclo per tutti i blocchi
                foreach (KeyValuePair<int, Block> b in mBlocks)
                {					
                    DeleteEntities(b.Key);

                    //ciclo per tutti i path dei blocchi
                    for (int iPath = 0; iPath < b.Value.GetPaths().Values.Count; iPath++)
                    {
                        Path p = b.Value.GetPaths().Values[iPath];
                        DrawPath(p, b.Value.Id, iPath);
                    }
                }

                Redraw();
            }
            catch (Exception ex) 
			{
				TraceLog.WriteLine("CBasicPaint.Repaint ", ex); 
			}
        }

        /// <summary>
        /// Disegna un singolo blocco
        /// </summary>
        /// <param name="block_no">id del blocco da disegnare</param>
		public void Repaint(int block_no)
		{
			if (mBlocks.ContainsKey(block_no))
			{
				Block b = mBlocks[block_no];

				Repaint(b);
			}
		}

        /// <summary>
        /// Disegna un singolo blocco
        /// </summary>
        /// <param name="block_no">id del blocco da disegnare</param>
        public void Repaint(Block b)
        {
            try
            {
                DeleteEntities(b.Id);

                //ciclo per tutti i path del blocco
                for (int iPath = 0; iPath < b.GetPaths().Values.Count; iPath++)
                {
                    Path p = b.GetPaths().Values[iPath];
                    DrawPath(p, b.Id, b.GetPaths().Keys[iPath]);
                }

				Redraw();
            }
            catch (Exception ex) 
			{
				TraceLog.WriteLine("CBasicPaint.Repaint ", ex); 
			}
        }
		
        /// <summary>
        /// Disegna un singolo path
        /// </summary>
        /// <param name="block_no">id del blocco da disegnare</param>
        /// <param name="path_no">path_no</param>
        public void Repaint(int block_no, int path_no)
        {

            try
            {
                Block b = mBlocks[block_no];

                DeleteEntities(block_no, path_no);

                //cerco il path del blocco da disegnare
                Path p = b.GetPath(path_no);

                DrawPath(p, b.Id, path_no);

                Redraw();
            }
			catch (Exception ex) 
			{ 
				TraceLog.WriteLine("CBasicPaint.Repaint ", ex); 
			}
        }

		/// <summary>
        /// Crea un blocco vuoto, lo aggiunge alla lista interna e ne ritorna il riferimento
        /// </summary>
        /// <returns>blocco creato</returns>
        public int AddBlock()
        {

            try
            {

                if (mBlocks == null)
                    mBlocks = new SortedList<int, Block>();

                Block b = new Block();
                indexBlock += 1;
                b.Id = indexBlock;
                mBlocks.Add(indexBlock, b);

                return indexBlock;

            }
            catch (Exception ex)
            {
				TraceLog.WriteLine("CBasicPaint.AddBlock: ", ex);
                return -1;
            }

        }

        /// <summary>
        /// Ritorna il blocco con l'id specificato
        /// </summary>
        /// <param name="block_no">id del blocco da cercare</param>
        /// <returns>blocco trovato</returns>
        public Block GetBlock(int block_no)
        {

            try
            {

                if (!mBlocks.ContainsKey(block_no))
                    return null;

                return mBlocks[block_no];

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }

        }

		/// <summary>
		/// Ritorna il blocco con il codice specificato
		/// </summary>
		/// <param name="code">codice del blocco da cercare</param>
		/// <returns>blocco trovato</returns>
		public Block GetBlock(string code)
		{
			try
			{
				foreach (KeyValuePair<int, Block> b in Blocks)
				{
					if (b.Value.Code.Trim() == code.Trim())
						return b.Value;
				}

				return null;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ERROR: ", ex);
				return null;
			}
		}
		
        /// <summary>
        /// Associa un percorso a un blocco esistente nella lista interna
        /// </summary>
        /// <param name="block_no">blocco a cui aggiungere il path</param>
        /// <param name="path">path da aggiungere</param>
        /// <returns>-1 il path non � stato inserito, altrimenti restituisce l'indice nella lista</returns>
        public int AddPath(int block_no, Path path)
        {
            try
            {
                if (mBlocks == null)
                    return -1;

                if (!mBlocks.ContainsKey(block_no))
                    return -1;

                return ((Block)mBlocks[block_no]).AddPath(path);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return -1;
            }
        }

        /// <summary>
        /// Rimuove un blocco dalla lista
        /// </summary>
        /// <param name="b">blocco da rimuovere</param>
        /// <returns>true: il blocco � stato rimosso correttamente</returns>
        public bool RemoveBlock(int block_no)
        {

            try
            {

                if (mBlocks == null)
                    return false;

                //rimuovo il blocco dalla lista
                if (!mBlocks.Remove(block_no))
                    return false;
                
                //rimuovo il blocco dalla selezione. verificare come ridisegnare
                UnSelect(block_no);

                DeleteEntities(block_no);

                return true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }

        }

        /// <summary>
        /// Rimuove un path da un blocco
        /// </summary>
        /// <param name="block_no">blocco da cui rimuovere il path</param>
        /// <param name="path_no">path da rimuovere</param>
        /// <returns>true: il path � stato rimosso correttamente</returns>
        public bool RemovePath(int block_no, int path_no)
        {

            try
            {

                if (mBlocks == null)
                    return false;
                if (!mBlocks.ContainsKey(block_no))
                    return false;

                DeleteEntities(block_no, path_no);

                return ((Block)mBlocks[block_no]).RemovePath(path_no);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }

        }

        ///***************************************************************************
        /// <summary>
        /// Aggiunge un nuovo layer
        /// </summary>
        /// <param name="layer">layer da aggiungere</param>
        /// <returns>
        ///		true	layer aggiunto
        ///		false	errore
        ///	</returns>
        ///***************************************************************************
        public bool AddLayer(CLayer layer)
        {
            try
            {
                if (mLayers.ContainsKey(layer.Name))
                    throw (new ApplicationException("Layer [" + layer.Name + "] already exists!"));

                mLayers.Add(layer.Name, layer);
                if (AddVdLayer(layer) == null)
                    throw (new ApplicationException("Error adding layer [" + layer.Name + "] in VectorDraw!"));

                return true;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".AddLayer: ", ex);
                return false;
            }
        }

		/// <summary>
		/// Modifica un layer esistente
		/// </summary>
		/// <param name="layer">layer modificato</param>
		/// <returns>
		///		true	layer modificato
		///		false	errore
		/// </returns>
		public bool SetLayer(CLayer layer)
		{
			try
			{
				if (!mLayers.ContainsKey(layer.Name))
					throw (new ApplicationException("Layer [" + layer.Name + "] not exists!"));

				if (SetVdLayer(layer) == null)
					throw (new ApplicationException("Error update layer [" + layer.Name + "] in VectorDraw!"));

				return true;

			}
			catch (Exception ex)
			{
				TraceLog.WriteLine(ToString() + ".SetLayer: ", ex);
				return false;
			}
		}

        ///***************************************************************************
        /// <summary>
        /// Ritorna il layer richiesto
        /// </summary>
        /// <param name="layerName">nome del layer</param>
        /// <returns>Layer se esiste, null altrimenti</returns>
        ///***************************************************************************
        public CLayer GetLayer(string layerName)
        {
            try
            {
                if (!mLayers.ContainsKey(layerName))
                    return null;

                return mLayers[layerName];
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".GetLayer: ", ex);
                return null;
            }
        }

        ///***************************************************************************
        /// <summary>
        /// Rimuove il layer indicato
        /// </summary>
        /// <param name="layerName">nome del layer</param>
        /// <returns>
        ///			true	layer cancellato
        ///			false	errore
        ///	</returns>
        ///***************************************************************************
        public bool RemoveLayer(string layerName)
        {
            try
            {
                //rimuovo il blocco dalla lista
                if (!mLayers.Remove(layerName))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".RemoveLayer: ", ex);
                return false;
            }
        }

        /// <summary>
        /// Definisce le caratteristiche del layer indicato
        /// </summary>
        /// <param name="layer">layer</param>
        /// <param name="visible">true: il layer � visibile</param>
        /// <param name="unlocked">true: il layer � sbloccato</param>
        public void LayerMode(CLayer layer, bool visible, bool unlocked)
        {
            // ricerca il layer e se esiste nella lista corrente, imposta le nuove proprieta'
            if (mLayers.ContainsKey(layer.Name))
            {
                if (visible)
                {
                    EnableLayer(layer.Name);
                }
                else
                {
                    DisableLayer(layer.Name);
                }
                layer.Visible = visible;
                layer.Unlocked = unlocked;
            }
        }

		internal void SaveLayerXml(string layerXml)
		{
			List<CLayer> lays = new List<CLayer>();
	
			foreach (KeyValuePair<string, CLayer> l in Layers)
				lays.Add(l.Value);

			Serialize(lays, layerXml);
		}

		internal void LoadConfiguredLayer(string layerXml)
		{						
			List<CLayer> lays;
			Deserialize(out lays, layerXml);

			if (lays != null)
			{
				// Scorre tutti i layers letti
				foreach (CLayer c in lays)
				{
					// Se il layer non viene trovato lo crea
					if (!Layers.ContainsKey(c.Name))
					{
						AddLayer(new CLayer(c.Name, c.Visible, c.Unlocked, c.Fill, c.UnFill, Color.FromArgb(c.RGBColor), c.LineType, c.LineWidth));
					}
					// Altrimenti lo aggiorna con i parametri trovati nella configurazione
					else
					{
						Layers[c.Name].Visible = c.Visible;
						Layers[c.Name].Unlocked = c.Unlocked;
						Layers[c.Name].Fill = c.Fill;
						Layers[c.Name].UnFill = c.UnFill;
						Layers[c.Name].Color = Color.FromArgb(c.RGBColor);
						Layers[c.Name].LineType = c.LineType;
						Layers[c.Name].LineWidth = c.LineWidth;

						SetLayer(Layers[c.Name]);

					}
				}
			}
		}

		private void Serialize(List<CLayer> list, string layerXml)
		{
			if (layerXml == null || layerXml.Length == 0)
				layerXml = Directory.GetCurrentDirectory() + "\\LayerConfig.xml";

			XmlSerializer serializer = new XmlSerializer(typeof(List<CLayer>));

			TextWriter writer = new StreamWriter(layerXml);
			serializer.Serialize(writer, list);
			writer.Close();
		}
		

		private void Deserialize(out List<CLayer> list, string layerXml)
		{
			list = null;

			if (layerXml == null || layerXml.Length == 0)
				layerXml = Directory.GetCurrentDirectory() + "\\LayerConfig.xml";
			
			if (!File.Exists(layerXml))
				return;

			XmlSerializer deserializer = new XmlSerializer(typeof(List<CLayer>));

			TextReader reader = new StreamReader(layerXml);
			object obj = deserializer.Deserialize(reader);
			list = (List<CLayer>)obj;
			reader.Close();
		}

        public int AddText(CText text)
        {
            try
            {
                //if (mLayers.ContainsKey(text.Layer))
                //    throw (new ApplicationException("Layer [" + text.Layer + "] already exists!"));

                if (mText == null)
                    mText = new SortedList<int,CText>();

                indexText += 1;
                mText.Add(indexText, text);

                if (AddVdText(indexText, text) == null)
                    throw (new ApplicationException("Error adding text VectorDraw!"));

                return indexText;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(ToString() + ".AddText: ", ex);
                return -1;
            }
        }

		public bool AddDimension(DimensionType type, Point p1, Point p2)
		{
			gPoints points = new gPoints();
			points.Add(p1.X, p1.Y, 0);
			points.Add(p2.X, p2.Y, 0);

			vdDimstyle DimStyle = mDisplayObject.BaseControl.ActiveDocument.DimStyles[0];
			DimStyle.TextHeight = 40;
			DimStyle.ArrowSize = 20;
			DimStyle.DecimalPrecision = 0;
			DimStyle.ExtLineVisible = true;
			DimStyle.LineIsInvisible = false;
			DimStyle.DimLineColor.FromSystemColor(Color.Red);
			DimStyle.TextColor.FromSystemColor(Color.Red);

			switch (type)
			{
				case DimensionType.LINEAR:
					return mDisplayObject.BaseControl.ActiveDocument.CommandAction.CmdDim(VdConstDimType.dim_Aligned, points, "USER", 0);

				case DimensionType.ORIZONTAL:
					return mDisplayObject.BaseControl.ActiveDocument.CommandAction.CmdDim(VdConstDimType.dim_Rotated, points, "USER", 0);

				case DimensionType.VERTICAL:
					return mDisplayObject.BaseControl.ActiveDocument.CommandAction.CmdDim(VdConstDimType.dim_Rotated, points, "USER", Math.PI/2);
			}

			return false;
		}

        public bool Select(int block_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, -1, -1, -1, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, -1, -1, -1, type);
        }
        public bool Select(int block_no, int path_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, -1, -1, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, int path_no, HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, -1, -1, type);
        }
        public bool Select(int block_no, int path_no, int elem_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, -1, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, int path_no, int elem_no,  HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, -1, type);
        }
        public bool Select(int block_no, int path_no, int elem_no, int vertex_no)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, vertex_no, HighLigthType.NORMAL);
        }
        public bool Select(int block_no, int path_no, int elem_no, int vertex_no, HighLigthType type)
        {
            // aggiungi alla lista di selezione, le informazioni passate.
            return _Select(block_no, path_no, elem_no, vertex_no, type);
        }

        public bool UnSelect()
        {
            // cancella la lista degli elementi selezionati.
            return _Unselect(-1, -1, -1, -1);
        }
        public bool UnSelect(int block_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, -1, -1, -1);
        }
        public bool UnSelect(int block_no, int path_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, path_no, -1, -1);
        }
        public bool UnSelect(int block_no, int path_no, int elem_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, path_no, elem_no, -1);
        }
        public bool UnSelect(int block_no, int path_no, int elem_no, int vertex_no)
        {
            // elimina l'elemento indicato dalla lista di selezione. alla lista di selezione..
            return _Unselect(block_no, path_no, elem_no, vertex_no);
        }

        public void StopActionMove()
        {
            List<int> lb = new List<int>();
            for (int i = 0; i < mSelections.Count; i++)
                lb.Add(mSelections[i].SelectedBlock);

            //ripristino le matrici iniziali
            for (int i = 0; i < lb.Count; i++)
            {
                Block b = mBlocks[lb[i]];
                RTMatrix m = _lastMatrix[i];
                double offX, offY, angolo;
                m.GetRotoTransl(out offX, out offY, out angolo);
                b.SetRotoTransl(offX, offY, angolo);
                b = mBlocks[lb[i]];
                Repaint(b);
                currentAction = Action.DEFAULT;

				//lineMoveRotate.Deleted = true;
				//Redraw();
            }
        }

        public void StopActionRotate()
        {
            List<int> lb = new List<int>();
            for (int i = 0; i < mSelections.Count; i++)
                lb.Add(mSelections[i].SelectedBlock);

            //ripristino le matrici iniziali
            for (int i = 0; i < lb.Count; i++)
            {
                Block b = mBlocks[lb[i]];
                RTMatrix m = _lastMatrix[i];
                double offX, offY, angolo;
                m.GetRotoTransl(out offX, out offY, out angolo);
                b.SetRotoTransl(offX, offY, angolo);
                b = mBlocks[lb[i]];
                Repaint(b);
                currentAction = Action.DEFAULT;

                _lineMoveRotate.Deleted = true;
                Redraw();
            }
        }

		/// <summary>
		/// Movimento manuale di uno o pi� blocchi
		/// </summary>
		/// <param name="blocks">Blocchi da muovere</param>
		/// <param name="deltaX">Movimento in X</param>
		/// <param name="deltaY">Movimento in Y</param>
		/// <returns></returns>
		public bool ManualMove(List<int> blocks, double deltaX, double deltaY)
		{			
			try
			{
				foreach (int idb in blocks)
				{
					Block bl = mBlocks[idb];
					bl.MatrixRT.Save();
					bl.RotoTranslRel(deltaX, deltaY, 0d, 0d, 0d);
				}
				
				// Essendo il movimento manuale imposta che non � stato premuto nessun tasto del mouse
				ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

				OnMoveEvent(ev, blocks, null, null, null);

				if (!ev.Accepted)
				{
					foreach (int idb in blocks)
					{
						Block bl = mBlocks[idb];
						bl.MatrixRT.Restore();
					}
				}
				else
				{
					Matrix m = new Matrix();
					m.TranslateMatrix(deltaX, deltaY, 0);
					_selActive.Transformby(m);
					_selActive.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
				}

				// Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
				OnEndMoveEvent(ev, blocks, null, null, null);

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("CBasicPaint.ManualMove ", ex);
				return false;
			}
		}

		public bool ManualMove(int block, double deltaX, double deltaY)
		{
			List<int> blocks = new List<int>();
			blocks.Add(block);

			if (!ManualMove(blocks, deltaX, deltaY))
				return false;

			return true;
		}

		/// <summary>
		/// Rotazione manuale di uno o pi� elementi
		/// </summary>
		/// <param name="blocks">Blocchi da ruotare</param>
		/// <param name="angle">Angolo di rotazione</param>
		/// <returns></returns>
		public bool ManualRotate(List<int> blocks, double pernoX, double pernoY, double angle)
		{
			try
			{
				foreach (int idb in blocks)
				{
					Block bl = mBlocks[idb];
					bl.MatrixRT.Save();
					bl.RotoTranslRel(0d, 0d, pernoX, pernoY, angle);
				}

				// Essendo il movimento manuale imposta che non � stato premuto nessun tasto del mouse
				ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

				OnRotateEvent(ev, blocks, null, null, null);

				if (!ev.Accepted)
				{
					foreach (int idb in blocks)
					{
						Block bl = mBlocks[idb];
						bl.MatrixRT.Restore();
					}
				}
				else
				{
					Matrix m = new Matrix();
					m.TranslateMatrix(-pernoX, -pernoY, 0);
					m.RotateZMatrix(angle);
					m.TranslateMatrix(pernoX, pernoY, 0);
					_selActive.Transformby(m);
					_selActive.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
				}

				// Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
				OnEndRotateEvent(ev, blocks, null, null, null);

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("CBasicPaint.ManualRotate ", ex);
				return false;
			}
		}

		public bool ManualRotate(int block, double pernoX, double pernoY, double angle)
		{
			List<int> blocks = new List<int>();
			blocks.Add(block);

			if (!ManualRotate(blocks, pernoX, pernoY, angle))
				return false;

			return true;
		}

		/// <summary>
		/// Posizionamento manuale di uno o pi� elementi
		/// </summary>
		/// <param name="blocks">Lista di blocchi da posizionare</param>
		/// <param name="matrixes">Lista di matrici per i blocchi</param>
		/// <returns></returns>
		public bool ManualRotoTrasl(List<int> blocks, List<RTMatrix> matrixes)
		{
			int i = 0;

			foreach (int b in blocks)
			{
				RTMatrix matrix = matrixes[i];
				if (!ManualRotoTrasl(b, matrix))
					return false;
				i++;
			}

			return true;
		}

		public bool ManualRotoTrasl(int block, RTMatrix matrix)
		{
			double x, y, a;

			try
			{
				_selActive = SelectEntities(block, -1, -1, -1);

				matrix.GetRotoTransl(out x, out y, out a);

				foreach (CSelection cs in mSelections)
				{
					Block bl = mBlocks[block];
					//bl.MatrixRT.Save();
					bl.SetRotoTransl(x, y, a);
				}

				Matrix m = new Matrix();
				m.TranslateMatrix(x, y, 0);
				m.RotateZMatrix(a);
				_selActive.Transformby(m);
				_selActive.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
				mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("CBasicPaint.ManualRotoTrasl ", ex);
				return false;
			}
		}
        #endregion

        #region Private methods

        private void SaveStartMatrix()
        {

            try
            {

                if (mSelections.Count == 0) return;

                _lastMatrix.Clear();

                foreach (CSelection cs in mSelections)
                {

                    Block b = mBlocks[cs.SelectedBlock];

                    if (b == null) continue;
                
                    double offX, offY, angle;
                    b.GetRotoTransl(out offX, out offY, out angle);
                    RTMatrix m = new RTMatrix();
                    m.SetRotoTransl(offX, offY, angle);
                    _lastMatrix.Add(m);
                    
                }

            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }

        }

        /// <summary>
        /// Disegna un path sul relativo layer 
        /// </summary>
        /// <param name="path">path da disegnare</param>
        /// <param name="idBlock">id del blocco</param>
        /// <param name="idPath">id del path</param>
        private void DrawPath(Path path, int idBlock, int idPath)
        {
			try
			{
				vdLayer layS = FindVdLayer(path.Layer);

				if (layS == null)
					return;

				if (layS.Frozen)
					layS.Frozen = false;

				mDisplayObject.BaseControl.ActiveDocument.ActiveLayer = layS;

				if (mBlocks[idBlock].mColor.Count > 0)
				{
					mDisplayObject.BaseControl.ActiveDocument.ActivePenColor.FromSystemColor(mBlocks[idBlock].mColor.Values[idPath]);
				}
				else
				{
					mDisplayObject.BaseControl.ActiveDocument.ActivePenColor.ColorIndex = layS.PenColor.ColorIndex;
				}

				CLayer cl = GetLayer(path.Layer);

				if (cl.Fill || cl.UnFill)
				{
					vdPolyline poly = new vdPolyline();
					vdSelection sel = new vdSelection();

					for (int i = 0; i < path.Count; i++)
					{
						Side s = path.GetSideRT(i);

						if (s is Arc)
						{
							vdArc arco = new vdArc();

							arco.Center = new gPoint(s.CenterPoint.X, s.CenterPoint.Y);
							if (s.AmplAngle >= 0d)
							{
								arco.StartAngle = s.StartAngle;
								arco.EndAngle = s.EndAngle;
							}
							else
							{
								arco.StartAngle = s.EndAngle;
								arco.EndAngle = s.StartAngle;
							}
							arco.Radius = s.Radius;
							arco.Label = idBlock.ToString() + "#" + idPath.ToString() + "#" + i.ToString();
							sel.AddItem(arco, false, vdSelection.AddItemCheck.Nochecking);
						}
						else
						{
							vdLine line = new vdLine(new gPoint(s.P1.X, s.P1.Y), new gPoint(s.P2.X, s.P2.Y));
							line.Label = idBlock.ToString() + "#" + idPath.ToString() + "#" + i.ToString();
							sel.AddItem(line, false, vdSelection.AddItemCheck.Nochecking);
						}
					}

					vdHatchProperties htcProp = new vdHatchProperties(VdConstFill.VdFillModeSolid);
					
					if (cl.Fill)
						htcProp.Solid2dTransparency = 100;

					//if (cl.UnFill)
					//{
					//    //htcProp.Solid2dTransparency = 100;
					//    htcProp.FillColor = new vdColor(mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.BkColor);
					//}

					poly.HatchProperties = htcProp;

					poly.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					poly.setDocumentDefaults();
					vdEntities e = poly.JoinEntities(sel, 0.01);
					mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem((vdFigure)poly);
					poly.Label = idBlock.ToString() + "#" + idPath.ToString() + "#-1";
					e.EraseAll();
					mDisplayObject.BaseControl.ActiveDocument.ClearEraseItems();
				}
				else
				{
					for (int i = 0; i < path.Count; i++)
					{
						Side s = path.GetSideRT(i);

						if (s is Arc)
						{
							vdArc a = DrawArc(s);
							//compongo il codice univoco dell'entit�
							a.Label = idBlock.ToString() + "#" + idPath.ToString() + "#" + i.ToString();
						}
						else
						{
							vdLine l = DrawLine(s);
							//compongo il codice univoco dell'entit�
							l.Label = idBlock.ToString() + "#" + idPath.ToString() + "#" + i.ToString();

						}
					}
				}

				if (mLayers[path.Layer].Visible)
					EnableLayer(path.Layer);
				else
					DisableLayer(path.Layer);

				layS = FindVdLayer("0");
				mDisplayObject.BaseControl.ActiveDocument.ActiveLayer = layS;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ERROR: ", ex);
			}

        }

        private bool _Select(int block_no, int path_no, int elem_no, int vertex_no, HighLigthType type)
        {
            try
            {
                CSelection selBlock = new CSelection(block_no, path_no, elem_no, vertex_no);
                if (selBlock == null)    // selezione non valida
                    return false;

                //non inserisco le stesse selezioni
                bool addOK = true;
                foreach (CSelection cs in mSelections)
                    if (cs.SelectedBlock == block_no && cs.SelectedPath == path_no &&
                        cs.SelectedElement == elem_no && cs.SelectedVertex == vertex_no)
                        addOK = false;

                if(addOK)
                    mSelections.Add(selBlock);

				double spessoreSelezione = (GetScala() * 3) / mDisplayObject.Height;
                //double spessoreSelezione = (GetScala() / 1000) * 5;

                HighLight(block_no, path_no, elem_no, vertex_no, spessoreSelezione, type);

                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }

        }

        private bool _Unselect(int block_no, int path_no, int elem_no, int vertex_no)
        {
            
            foreach(CSelection csel in mSelections)
            {

                if (csel.SelectedBlock == block_no)
                {
                    //se va deselezionato un blocco rimuovo dalla lista
                    mSelections.Remove(csel);                    
                    break;
                }

                else if (csel.SelectedPath == path_no)
                {
                    csel.SelectedPath = -1;
                    csel.SelectedElement = -1;
                    csel.SelectedVertex = -1;
                }

                else if (csel.SelectedElement == elem_no)
                {
                    csel.SelectedElement = -1;
                    csel.SelectedVertex = -1;
                }

                else if (csel.SelectedVertex == vertex_no)
                    csel.SelectedVertex = -1;

            }

            HighLight(block_no, path_no, elem_no, vertex_no, 0, HighLigthType.NORMAL);

            return true;
        }

        private static bool MatchSel(CSelection cSel)
        {
            //mblock_no = block_no;
            //mpath_no = path_no;
            //melem_no = elem_no;
            //mvertex_no = vertex_no;
            return true;
            //if (cSel.SelectedBlock != null)
            //{
                
            //    //cSel.SelectedBlock = mblock;
            //}

        }

        /// <summary>
        /// Verifica se un punto � interno a un blocco della lista e restituisce il blocco
        /// </summary>
        /// <param name="p">punto per il controllo</param>
        /// <returns>id blocco</returns>
        private int GetBlockFromPoint(Point p)
        {
            try
            {

                //verifico se il punto � interno a un blocco della lista
                for (int ib = 0; ib < mBlocks.Values.Count; ib++)
                {
                    Block b = mBlocks.Values[ib];
                    RTMatrix m = b.MatrixRT;

                    for (int i = 0; i < b.GetPaths().Values.Count; i++)
                    {
                        Path path = b.GetPaths().Values[i];
                        if (path.IsInsideRT(p))
                            return b.Id;
                    }
                }

                return -1;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return -1;
            }
        }

        
        /// <summary>
        /// Cerca il path, il side e il vertex del blocco pi� vicino al punto se sono in tolleranza
        /// </summary>
        /// <param name="pUser">punto per il controllo</param>
        /// <param name="sel">elementi selezionati in VD</param>
        /// <param name="idb">lista id blocchi trovati</param>
        /// <param name="idp">lista id path trovati</param>
        /// <param name="ids">lista id side trovati</param>
        /// <param name="idv">lista id vertici trovati</param>
        private void GetElementsBlockFromPoint(Point pUser, vdSelection sel, out List<int> idb, out List<int> idp, out List<int> ids, out List<int> idv)
        {

            idb = new List<int>();
            idp = new List<int>();
            ids = new List<int>();
            idv = new List<int>();

            try
            {

                if (sel.Count == 0)
                {
                    //verifico se il punto � interno a un blocco della lista
                    for (int ib = 0; ib < mBlocks.Values.Count; ib++)
                    {

                        Block b = mBlocks.Values[ib];

                        for (int i = 0; i < b.GetPaths().Values.Count; i++)
                        {
                            Path path = b.GetPaths().Values[i];
							if (b.AllowSelect && path.IsInsideRT(pUser))
                            {
                                
                                vdLayer l = FindVdLayer(path.Layer);
                                if (l != null && !l.Frozen)
                                {
                                    idb.Add(b.Id);
                                    break;
                                }
                            }
                        }
                    }

                }
                else
                {

                    for (int i = 0; i < sel.Count; i++)
                    {

                        vdFigure f = sel[i];
                        if (f.Layer.Frozen) continue;
                        string[] split = f.Label.Split('#');
                        if (split.Length == 3)
                        {

                            int id = Convert.ToInt32(split[0]);
                            Block b = GetBlock(id);
							if (b != null && b.AllowSelect)
								idb.Add(id);
							else
							{
								idb.Add(-1);
								continue;
							}

                            id = Convert.ToInt32(split[1]);
                            Path p = b.GetPath(id);
							if (p != null && p.AllowSelect)
								idp.Add(id);
							else
							{
								idp.Add(-1);
								continue;
							}

                            id = Convert.ToInt32(split[2]);
                            Side s = p.GetSide(id);
							if (s != null && s.AllowSelect)
								ids.Add(id);
							else
							{
								ids.Add(-1);
								continue;
							}

                            Side sRT = p.GetSideRT(id);
                            double size = ((GetScala() * mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.GlobalProperties.PickSize) / mDisplayObject.Height) / 2;
                            double distPuP1 = pUser.Distance(sRT.P1);
                            double distPuP2 = pUser.Distance(sRT.P2);
                            if (distPuP1 <= size && distPuP1 < distPuP2)
                                idv.Add(1);
                            else if(distPuP2 <= size && distPuP2 < distPuP1)
                                idv.Add(2);
                            else
                                idv.Add(-1);

                        }

                    }

                }

            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }
        }

        #endregion

        #region Events

        public delegate void DragDropHandler(object sender, CBasicPaintEventArgs ev, Block bDragDrop);
        public delegate void DragEnterHandler(object sender, CBasicPaintEventArgs ev, Block bDragDrop);
		public delegate void DragLeaveHandler(object sender, CBasicPaintEventArgs ev, Block bDragDrop);
        public delegate void MouseMoveAfterHandler(object sender, double x, double y, double z); // string strCoord);

        public event MouseMoveAfterHandler evMouseAfter;

        public event CBasicPaintEventArgsHandler evClick;

        public event CBasicPaintEventArgsHandler evStartMove;
        public event CBasicPaintEventArgsHandler evMove;
        public event CBasicPaintEventArgsHandler evEndMove;

        public event CBasicPaintEventArgsHandler evStartRotate;
        public event CBasicPaintEventArgsHandler evRotate;
        public event CBasicPaintEventArgsHandler evEndRotate;

        public event DragDropHandler evDragDrop;
        //public event CBasicPaintEventArgsHandler evDragOver;
        public event DragEnterHandler evDragEnter;
		public event DragLeaveHandler evDragLeave;

        // Implementation will call these methods in order to generate appropriate event
        protected virtual void OnClickEvent(CBasicPaintEventArgs e, MouseButtons mb, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evClick != null)
            {
                evClick(this, e, block_no, path_no, elem_no, vertex_no);

                if (e != null && e.Accepted == false)
                {
                    //System.Windows.Forms.MessageBox.Show("non accettato");
                }
                else if (e != null && e.Accepted == true)
                {
                    //System.Windows.Forms.MessageBox.Show("accettato!!!");
                }
            }
        }

        protected virtual void OnStartMoveEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (e != null)
            {
                if (evStartMove != null)
                    evStartMove(this, e, block_no, path_no, elem_no, vertex_no);
            }
        }

        protected virtual void OnMoveEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
			if (evMove != null)
				evMove(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnEndMoveEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evEndMove != null)
                evEndMove(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnStartRotateEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evStartRotate != null)
                evStartRotate(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnRotateEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evRotate != null)
                evRotate(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnEndRotateEvent(CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (evEndRotate != null)
                evEndRotate(this, e, block_no, path_no, elem_no, vertex_no);
        }

        protected virtual void OnDragDropEvent(CBasicPaintEventArgs e, Block b)
        {
            if (evDragDrop != null)
                evDragDrop(this, e, b);
        }

        protected virtual void OnDragEnterEvent(CBasicPaintEventArgs e)
        {
            if (evDragEnter != null)
                evDragEnter(this, e, _blockDragDrop);
        }

		protected virtual void OnDragLeaveEvent(CBasicPaintEventArgs e)
        {
            if (evDragLeave != null)
				evDragLeave(this, e, _blockDragDrop);
        }

        protected virtual void OnMouseMoveAfter(double x, double y, double z) //string eCoord)
        {
            if (evMouseAfter != null)
                evMouseAfter(this, x, y, z);
        }

        #endregion

        #endregion

        #region Eventi Vector Draw

        private void VD_MouseDown(MouseEventArgs e, ref bool Cancel)
        {
            try
            {
                List<int> lb = new List<int>();
                Point p = null;
                List<int> lidblock = new List<int>();
                List<int> lidpath = new List<int>();
                List<int> lidside = new List<int>();
                List<int> lidpoint = new List<int>();

                //recupero il punto cliccato dall'utente
                if (currentAction == Action.DEFAULT)
                {
                    gPoint punto = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();
                    p = new Point(punto.x, punto.y);

					//Verifico se � attiva la funzione di ZoomWindow
					if (_zoomWindow)
					{
						_startZoomWindow = p;
						return;
					}

                    if (e.Button == MouseButtons.Left)
                        ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Left);
                    else if (e.Button == MouseButtons.Right)
                        ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Right);
                    else if (e.Button == MouseButtons.Middle)
                        ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Center);
                    else if (e.Button == MouseButtons.None)
                        ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

                    mDisplayObject.BaseControl.ActiveDocument.Selections.RemoveAll();

                    //in base al viewsize in drawing unit, e alle dimensioni in pixel del controllo e del cursore calcolo i mm dell'altezza del cursore
                    double size = ((GetScala() * mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.GlobalProperties.PickSize) / mDisplayObject.Height) / 2;

                    gPoints points = new gPoints();
                    points.Add(new gPoint(punto.x - size, punto.y - size));
                    points.Add(new gPoint(punto.x + size, punto.y + size));

                    _selActive = mDisplayObject.BaseControl.ActiveDocument.Selections.Add(String.Concat("MYSEL"));

                    _selActive.Select(RenderSelect.SelectingMode.CrossingWindowRectangle, points);

                    for (int i = _selActive.Count - 1; i >= 0; i--)
                        if (_selActive[i].GetType().ToString().ToUpper() == "VECTORDRAW.PROFESSIONAL.VDFIGURES.VDIMAGE")
                            _selActive.RemoveItem(_selActive[i]);

                    //gestire anche AllowDragDrop del blocco
					// Identifica tutte le entit� a cui il punto cliccato appartiene
                    GetElementsBlockFromPoint(p, _selActive, out lidblock, out lidpath, out lidside, out lidpoint);

                    OnClickEvent(ev, e.Button, lidblock, lidpath, lidside, lidpoint);

					for (int i = 0; i < mSelections.Count; i++)
						lb.Add(mSelections[i].SelectedBlock);
                }
                else
                    return;

                //clicco per iniziare un movimento
                if (move && currentAction == Action.DEFAULT)
                {
                    OnStartMoveEvent(ev, lb, null, null, null);

                    if (ev.Accepted)
                    {
                        currentAction = Action.MOVE;

                        //memorizzo i punti iniziali e le matrici
                        SaveStartMatrix();
                        _user = p;
                        _lastOK = p;
						
                        //lineMoveRotate = AddLine(pUser.X, pUser.Y, p.X, p.Y);
                        //lineMoveRotate.PenColor.ColorIndex = 6;

                        _selActive = new vdSelection("Blocks");
                        vdSelection selTmp = null;
						List<int> idBlocks = new List<int>();

						// Prepara la lista univoca di blocchi
						foreach (CSelection cs in mSelections)
						{
							if (!idBlocks.Contains(cs.SelectedBlock))
								idBlocks.Add(cs.SelectedBlock);
						}

						// Scorre tutti i blocchi e prepara la vdSelection
						foreach (int idB in idBlocks)
						{
							selTmp = SelectEntities(idB, -1, -1, -1);

							if (selTmp != null)
							{
								for (int i = 0; i < selTmp.Count; i++)
									_selActive.AddItem(selTmp[i], true, vdSelection.AddItemCheck.Nochecking);
							}
						}
                    }
                    else
                    {
                        //ripristino le matrici iniziali
                        for (int i = 0; i < lb.Count; i++)
                        {

                            Block b = mBlocks[lb[i]];
                            RTMatrix m = _lastMatrix[i];
                            double offX, offY, angolo;
                            m.GetRotoTransl(out offX, out offY, out angolo);
                            b.SetRotoTransl(offX, offY, angolo);
                            b = mBlocks[lb[i]];
                            //DeleteEntities(b.Id);
                            Repaint(b);
                            currentAction = Action.DEFAULT;
                        }
                    }
                }
                //clicco per iniziare una rotazione
                else if (rotate && currentAction == Action.DEFAULT)
                {
                    OnStartRotateEvent(ev, lb, null, null, null);

                    if (ev.Accepted)
                    {

                        currentAction = Action.ROTATE;

                        //memorizzo i punti iniziali e le matrici
                        SaveStartMatrix();
                        _user = p;
                        _lastOK = p;

                        _lineMoveRotate = AddLine(_user.X, _user.Y, p.X, p.Y);
                        _lineMoveRotate.PenColor.ColorIndex = 6;

                        _selActive = new vdSelection("Blocks");
                        vdSelection selTmp = null;
						List<int> idBlocks = new List<int>();

						// Prepara la lista univoca di blocchi
						foreach (CSelection cs in mSelections)
						{
							if (!idBlocks.Contains(cs.SelectedBlock))
								idBlocks.Add(cs.SelectedBlock);
						}

						foreach (int idB in idBlocks)
						{
							selTmp = SelectEntities(idB, -1, -1, -1);

							if (selTmp != null)
							{
								for (int i = 0; i < selTmp.Count; i++)
									_selActive.AddItem(selTmp[i], true, vdSelection.AddItemCheck.Nochecking);
							}
						}
                    }
                    else
                    {
                        //ripristino le matrici iniziali
                        for (int i = 0; i < lb.Count; i++)
                        {

                            Block b = mBlocks[lb[i]];
                            RTMatrix m = _lastMatrix[i];
                            double offX, offY, angolo;
                            m.GetRotoTransl(out offX, out offY, out angolo);
                            b.MatrixRT.SetRotoTransl(offX, offY, angolo);
                            b = mBlocks[lb[i]];
                            //DeleteEntities(b.Id);
                            Repaint(b);
                            currentAction = Action.DEFAULT;
                        }
                    }
                }
                //selezione (generalmente per il drag & drop)
                else if (!move && !rotate)
                {
					if (ev.Accepted && dragdrop)
					{
						Cancel = true;
						_selActive.RemoveAll();

						vdSelection selTmp = null;
						selTmp = SelectEntities(lidblock[0], -1, -1, -1);

						if (selTmp != null && selTmp.Count != 0)
						{
							//calcolo il punto cliccato dall'utente con la matrice inversa, per averlo rispetto al path in origine
							Block b = mBlocks[lidblock[0]];
							RTMatrix mInv = b.MatrixRT.InvMat();
							mInv.PointTransform(p.X, p.Y, out _xDragDrop, out _yDragDrop);

							for (int j = 0; j < selTmp.Count; j++)
								_selActive.AddItem(selTmp[j], true, vdSelection.AddItemCheck.Nochecking);

							mDisplayObject.BaseControl.ActiveDocument.CommandAction.DoDragDrop(_selActive, vdSelection.DragDropEffects.Copy, new gPoint(p.X, p.Y), (int)_selActive.GetBoundingBox().Width, (int)_selActive.GetBoundingBox().Height, 3);
						}
					}
                }
            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }
        }

        private void VD_MouseUp(MouseEventArgs e, ref bool Cancel)
        {
            try
            {
                //recupero il punto cliccato dall'utente
                gPoint punto = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();
                Point p = new Point(punto.x, punto.y);

				if (_zoomWindow && _startZoomWindow != null)
				{
					DeleteGenericRectangle();
					ZoomWindow(_startZoomWindow, p);
					_startZoomWindow = null;
					_zoomWindow = false;
				}

                if (e.Button == MouseButtons.Left)
                    ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Left);
                else if (e.Button == MouseButtons.Right)
                    ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Right);
                else if (e.Button == MouseButtons.Middle)
                    ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_Center);
                else if (e.Button == MouseButtons.None)
                    ev = new CBasicPaintEventArgs(CBasicPaintEventArgs.enMouseKey.MouseKey_None);

                List<int> lb = new List<int>();
                for (int i = 0; i < mSelections.Count; i++)
                    lb.Add(mSelections[i].SelectedBlock);

                if (move && currentAction == Action.MOVE)
                {
                    if (e.Button == MouseButtons.Left)
                    {
						for (int i = 0; i < mSelections.Count; i++)
						    Select(mSelections[i].SelectedBlock);
						
						// Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
						OnEndMoveEvent(ev, lb, null, null, null);

						currentAction = Action.DEFAULT;
                    }
                }
                else if (rotate && currentAction == Action.ROTATE)
                {
                    if (e.Button == MouseButtons.Left)
                    {
                        for (int i = 0; i < mSelections.Count; i++)
                            Select(mSelections[i].SelectedBlock);

                        _lineMoveRotate.Deleted = true;
                        Redraw();

						// Attenzione, l'evento viene inviato anche se non c'era nessun blocco selezionato
						OnEndRotateEvent(ev, lb, null, null, null);
                        
						currentAction = Action.DEFAULT;
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("CBasicPaint.VD_MouseUp: ", ex);
            }
        }

        private void VD_ActionStart(object sender, string actionName, ref bool Cancel)
        {

            //disabilito la finestra e il muovi delle linee
            if (actionName == "BaseAction_ActionGetRectFromPointSelectDCS" ||
                actionName == "BaseAction_CmdMoveGripPoints")
            {
                Cancel = true;
            }

            else if (actionName == "CmdMove")
            {

                if (Cancel == false)
                {
                }

            }
            else if (actionName == "CmdRotate")
            {
            }
            else if (actionName == "CmdErase")
            {

            }
            else if (actionName == "CmdSelect")
            {
            }
            else if (actionName == "DoDragDrop")
            {
                if (_blockDragDrop.AllowDragDrop == false)
                {
					((vdFramedControl)DisplayObject).Cursor = Cursors.No;
                    Cancel = true;
                }
            }
            else
            {
            }

        }

        private void VD_ActionFinish(object sender, object actionName)
        {
            if (actionName.ToString() == "VectorDraw.Professional.CommandActions.ActionGetTranfromSelection")
            {

            }
		}

		private void VD_MouseMove(object sender, MouseEventArgs e)
		{
			try
			{
				OsnapPoint snapPoint;
				
				gPoint point = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();

				if (_zoomWindow && _startZoomWindow != null)
				{
					DrawGenericRectangle(_startZoomWindow.X, _startZoomWindow.Y, point.y - _startZoomWindow.Y, point.x - _startZoomWindow.X, false, Color.White);
					return;
				}

				if (_osnapModes.Count > 0)
				{
					DeleteSnapPoint();
					snapPoint = GetSnapPoint(new Point(point.x, point.y));

					if (snapPoint != null)
					{						
						if (snapPoint != _lastSnapPoint)
						{
							DrawSnapPoint(snapPoint);
							
							_lastSnapPoint = snapPoint;
						}
					}
					else
					{
						bool change = false;
						gPoint origP = new gPoint(0, 0);
						if (_line1.StartPoint != origP || _line1.EndPoint != origP)
						{
							_line1.StartPoint = origP;
							_line1.EndPoint = origP;
							change = true;
						}
						else if (_line2.StartPoint != origP || _line2.EndPoint != origP)
						{
							_line2.StartPoint = origP;
							_line2.EndPoint = origP;
							change = true;
						}
						else if (_line3.StartPoint != origP || _line3.EndPoint != origP)
						{
							_line3.StartPoint = origP;
							_line3.EndPoint = origP;
							change = true;
						}
						else if (_line4.StartPoint != origP || _line4.EndPoint != origP)
						{
							_line4.StartPoint = origP;
							_line4.EndPoint = origP;
							change = true;
						}

						if (change)
							mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();

						_lastSnapPoint = null;
					}
				}

				if (!move && !rotate) return;

				if (move && currentAction == Action.MOVE)
				{
					if (mSelections.Count > 0)
					{
						gPoint punto = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();

						// Verifica se siamo in modalit� Ortho
						if (mDisplayObject.BaseControl.ActiveDocument.OrthoMode)
						{
							if (Math.Abs(punto.x - _user.X) > Math.Abs(punto.y - _user.Y))
								punto.y = _user.Y;
							else
								punto.x = _user.X;
						}

						double deltaX = punto.x - _lastOK.X;
						double deltaY = punto.y - _lastOK.Y;

						_lastOK = new Point(punto.x, punto.y);

						List<int> lb = new List<int>();

						foreach (CSelection cs in mSelections)
						{
							Block b = mBlocks[cs.SelectedBlock];
							lb.Add(b.Id);
							b.MatrixRT.Save();
							b.RotoTranslRel(deltaX, deltaY, 0d, 0d, 0d);
						}

						OnMoveEvent(ev, lb, null, null, null);

						if (!ev.Accepted)
						{
							foreach (CSelection cs in mSelections)
							{
								Block b = mBlocks[cs.SelectedBlock];
								b.MatrixRT.Restore();
							}
						}
						else
						{
							Matrix m = new Matrix();
							m.TranslateMatrix(deltaX, deltaY, 0);
							_selActive.Transformby(m);

							_selActive.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);

							mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();

							//gPoint min = new gPoint(selActive.GetBoundingBox().Min.x - Photo.Width / 4, selActive.GetBoundingBox().Min.y - Photo.Height / 4);
							//gPoint max = new gPoint(selActive.GetBoundingBox().Max.x + Photo.Width / 4, selActive.GetBoundingBox().Max.y + Photo.Height / 4);
							//Box box = new Box(min, max);
							//mDisplayObject.BaseControl.ActiveDocument.Invalidate(box);

							//lineMoveRotate.EndPoint = new gPoint(pLastOK.X, pLastOK.Y);

							//mDisplayObject.BaseControl.ActiveDocument.Redraw(false);
						}
					}
				}
				else if (rotate && currentAction == Action.ROTATE)
				{
					if (mSelections.Count > 0)
					{
						gPoint punto = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();
						gPoint perno = new gPoint(_user.X, _user.Y);
						gPoint pPrec = new gPoint(_lastOK.X, _lastOK.Y);
						double angolo;

						if (mDisplayObject.BaseControl.ActiveDocument.OrthoMode)
						{
							if (Math.Abs(punto.x - _user.X) > Math.Abs(punto.y - _user.Y))
								punto.y = _user.Y;
							else
								punto.x = _user.X;
						}

						angolo = punto.GetAngle(perno) - pPrec.GetAngle(perno);
						_lastOK = new Point(punto.x, punto.y);

						List<int> lb = new List<int>();

						foreach (CSelection cs in mSelections)
						{
							Block b = mBlocks[cs.SelectedBlock];
							lb.Add(b.Id);
							b.MatrixRT.Save();
							b.RotoTranslRel(0d, 0d, _user.X, _user.Y, angolo);
						}

						OnRotateEvent(ev, lb, null, null, null);

						if (!ev.Accepted)
						{
							foreach (CSelection cs in mSelections)
							{
								Block b = mBlocks[cs.SelectedBlock];
								b.MatrixRT.Restore();
							}
						}
						else
						{
							Matrix m = new Matrix();
							m.TranslateMatrix(-_user.X, -_user.Y, 0);
							m.RotateZMatrix(angolo);
							m.TranslateMatrix(_user.X, _user.Y, 0);
							_selActive.Transformby(m);
							_selActive.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);

							_lineMoveRotate.EndPoint = new gPoint(_lastOK.X, _lastOK.Y);

							mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();

							//Redraw();
						}
					}
				}

			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ERROR: ", ex);
			}			
		}		

        private void VD_DragEnter(DragEventArgs e, ref bool Cancel)
        {
			try
			{
				e.Effect = DragDropEffects.Copy;
				ev = new CBasicPaintEventArgs(null);
				OnDragEnterEvent(ev);

				if (!ev.Accepted)
				{
					e.Effect = DragDropEffects.None;
					Cancel = true;
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ERROR: ", ex);
			}
        }

		private void VD_DragLeave(EventArgs e, ref bool Cancel)
        {
			try
			{
				ev = new CBasicPaintEventArgs(null);
				OnDragLeaveEvent(ev);

				if (!ev.Accepted)
				{
					Cancel = true;
				}
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ERROR: ", ex);
			}
        }

        private void VD_DragDrop(DragEventArgs e, ref bool Cancel)
        {
            try
            {

                Cancel = true;

                gPoint punto = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();

                ev = new CBasicPaintEventArgs(null);

                double angolo = _blockDragDrop.RotAngle;

                _blockDragDrop.MatrixRT.Reset();

                _blockDragDrop.RotoTranslRel(-_xDragDrop, -_yDragDrop, 0, 0, 0);
                _blockDragDrop.RotoTranslRel(punto.x, punto.y, 0, 0, angolo);

                OnDragDropEvent(ev, _blockDragDrop);

                _xDragDrop = 0;
                _yDragDrop = 0;

                Redraw();

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
            }
            finally
            {
            }

        }

        private void VD_MouseWheel(object sender, MouseEventArgs e)
        {
            try
            {
                mTimer.Stop();
                mTimer.Start();
            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }
        }

        private void VD_DragOver(DragEventArgs e, ref  bool Cancel)
        {
            try
            {
                //mDisplayObject.BaseControl.AllowDrop = true;
            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }
        }

        private void VD_MouseMoveAfter(MouseEventArgs e)
        {
            try
            {
                gPoint ccspt = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();

                OnMouseMoveAfter(Math.Round(ccspt.x, 4), Math.Round(ccspt.y, 4), Math.Round(ccspt.z, 4));
            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }
        }

        #endregion

        #region Eventi Componenti
        private void mTimer_Tick(object sender, EventArgs e)
        {
            Regen();
            mTimer.Stop();
        }
        #endregion

        #region mDisplayObject methods

        /// <summary>
        /// Crea un nuovo disegno
        /// </summary>
        /// <returns></returns>
        private void NewActiveDocument() 
		{ 
			mDisplayObject.BaseControl.ActiveDocument.New();
			mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.BkColor = _backColor;
		}

        /// <summary>
        /// Inizializza l'interfaccia dell'oggetto di disegno (Vector Draw)
        /// </summary>
        private void InitVectorDraw()
        {

            mDisplayObject.BaseControl.ActiveDocument.ActionStart += new vdDocument.ActionStartEventHandler(VD_ActionStart);
            mDisplayObject.BaseControl.ActiveDocument.ActionFinish += new vdDocument.ActionFinishEventHandler(VD_ActionFinish);
            mDisplayObject.BaseControl.MouseMove += new MouseEventHandler(VD_MouseMove);

            mDisplayObject.BaseControl.MouseMoveAfter += new MouseMoveAfterEventHandler(VD_MouseMoveAfter);

            mDisplayObject.BaseControl.vdMouseDown += new MouseDownEventHandler(VD_MouseDown);
            mDisplayObject.BaseControl.vdMouseUp += new MouseUpEventHandler(VD_MouseUp);
            mDisplayObject.BaseControl.vdDragEnter += new DragEnterEventHandler(VD_DragEnter);
			mDisplayObject.BaseControl.vdDragLeave += new DragLeaveEventHandler(VD_DragLeave);
            mDisplayObject.BaseControl.vdDragDrop += new DragDropEventHandler(VD_DragDrop);
            mDisplayObject.BaseControl.vdDragOver += new DragOverEventHandler(VD_DragOver);
            mDisplayObject.BaseControl.MouseWheel += new MouseEventHandler(VD_MouseWheel);

            //dimensioni cursore
            mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.GlobalProperties.PickSize = 20;

            //colore cursore
            mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.GlobalProperties.CursorPickColor = Color.FromArgb(255, 255, 255);
            mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.GlobalProperties.CursorAxisColor = Color.FromArgb(255, 255, 255);

            mDisplayObject.BaseControl.AllowDrop = true;

            ShowVDLayout(false);
            ShowAxis(true);
			SetAxisDirection(_axisDirection);
			
        }

		/// <summary>
		/// Imposta la direzione degli assi
		/// </summary>
		/// <param name="ax"></param>
		private void SetAxisDirection(AxisDirection ax)
		{
			switch (ax)
			{
				case AxisDirection.LEFT_BOTTOM:
				case AxisDirection.LEFT_TOP:
				case AxisDirection.RIGHT_TOP:
					break;

				case AxisDirection.RIGHT_BOTTOM:
					mDisplayObject.BaseControl.ActiveDocument.CommandAction.View3D("VBOTTOM");
					break;
			}
		}


        /// <summary>
        /// Visualizzazione di parti dell'oggetto di disegno (Vector Draw)
        /// </summary>
        /// <param name="show">bool: true: visualizza / false: non visualizza </param>
		public void ShowVDLayout()
		{
			ShowVDLayout(_showVDLayout);
		}

        public void ShowVDLayout(bool show)
		{
			_showVDLayout = show;
            
			mDisplayObject.SetLayoutStyle(vdFramedControl.LayoutStyle.CommandLine, show);
            mDisplayObject.SetLayoutStyle(vdFramedControl.LayoutStyle.PropertyGrid, show);
            mDisplayObject.SetLayoutStyle(vdFramedControl.LayoutStyle.StatusBar, show);
            mDisplayObject.SetLayoutStyle(vdFramedControl.LayoutStyle.LayoutTab, show);
            mDisplayObject.SetLayoutStyle(vdFramedControl.LayoutStyle.VericalScroll, show);
            mDisplayObject.SetLayoutStyle(vdFramedControl.LayoutStyle.HorizodalScroll, show);

            EnableAutoGripOn(false);
        }

        /// <summary>
        /// Gestisce la modalit� la visualizzazione dei quadratini blu nella selezione di VD
        /// </summary>
        /// <param name="grip">true i quadrativi vengono visualizzati</param>
        public void EnableAutoGripOn(bool grip) { mDisplayObject.BaseControl.ActiveDocument.EnableAutoGripOn = grip; }

        /// <summary>
        /// Visualizzazione degli assi
        /// </summary>
        /// <param name="show">bool: true: visualizza / false: non visualizza</param>
        private void ShowAxis(bool show) { mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ShowUCSAxis = show; }

        /// <summary>
        /// zoom del disegno
        /// </summary>
		public void ZoomAll() { mDisplayObject.BaseControl.ActiveDocument.CommandAction.Zoom("E", "USER", "USER");// mDisplayObject.BaseControl.ActiveDocument.ZoomAll(); 
		}

        /// <summary>
        /// zoom finestra
        /// </summary>
        public void ZoomWindow(Point firstCorner, Point secondCorner)
        {
            mDisplayObject.BaseControl.ActiveDocument.CommandAction.Zoom("W", new gPoint(firstCorner.X, firstCorner.Y), new gPoint(secondCorner.X, secondCorner.Y));// mDisplayObject.BaseControl.ActiveDocument.ZoomAll(); 
        }

		/// <summary>
		/// zoom finestra
		/// </summary>
		public void ZoomWindow()
		{
			_zoomWindow = true;
			_startZoomWindow = null;
		}

		/// <summary>
		/// zoom finestra
		/// </summary>
		public void CancelZoomWindow()
		{
			_zoomWindow = false;
			_startZoomWindow = null;
		}

        /// <summary>
		/// zoom del disegno
		/// </summary>
		/// <param name="fact">fattore di zoom</param>
		public void ZoomAll(double fact) 
		{ 
			mDisplayObject.BaseControl.ActiveDocument.ZoomAll(); 
			mDisplayObject.BaseControl.ActiveDocument.ZoomScale(fact); 
		}

		/// <summary>
		/// zoom del disegno
		/// </summary>
		/// <param name="val">percentuale di zoom in</param>
		public void ZoomIn(double val)
		{
			if (val <= 0)
				val = 5;

			double xmin = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Xmin;
			double ymin = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Ymin;
			double xmax = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Xmax;
			double ymax = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Ymax;

			double xtot = xmax - xmin;
			double ytot = ymax - ymin;
			double xval = (xtot * val) / 100;
			double yval = (ytot * val) / 100;

			gPoint ll = new gPoint(xmin + xval, ymin + yval, 0);
			gPoint tr = new gPoint(xmax - xval, ymax - yval, 0);

			mDisplayObject.BaseControl.ActiveDocument.CommandAction.Zoom("W", ll, tr);
		}

		/// <summary>
		/// zoom del disegno
		/// </summary>
		/// <param name="val">percentuale di zoom out</param>
		public void ZoomOut(double val)
		{
			if (val <= 0)
				val = 5;

			double xmin = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Xmin;
			double ymin = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Ymin;
			double xmax = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Xmax;
			double ymax = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.ClipBounds.Ymax;

			double xtot = xmax - xmin;
			double ytot = ymax - ymin;
			double xval = (xtot * val) / 100;
			double yval = (ytot * val) / 100;

			gPoint ll = new gPoint(xmin - xval, ymin - yval, 0);
			gPoint tr = new gPoint(xmax + xval, ymax + yval, 0);

			mDisplayObject.BaseControl.ActiveDocument.CommandAction.Zoom("W", ll, tr);
		}

        /// <summary>
        /// Gestisce la modalit� ortho
        /// </summary>
        /// <param name="ortho">true il cursore si muove ortogonalmente</param>
        public void Ortho(bool ortho) { mDisplayObject.BaseControl.ActiveDocument.OrthoMode = ortho; }

		/// <summary>
		/// Restituisce il punto del cursore
		/// </summary>
		/// <returns></returns>
		public Point CursorPosition() 
		{ 
			gPoint p = mDisplayObject.BaseControl.ActiveDocument.CCS_CursorPos();
			return new Point(p.x, p.y); 
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="mode"></param>
		public void SnapsMode(SnapMode mode, bool enable, List<string> snapLayers)
		{
			// Disabilita gli snap
			if (mode == SnapMode.NONE)
			{
				_osnapModes.Clear();
				return;
			}
			// Attiva tutti gli snap
			else if (mode == SnapMode.ALL)
			{
				_osnapModes.Clear();
				_osnapModes.Add(mode);
				return;
			}

			if (enable)
			{
				// Se gi� non esiste aggiunge la nuova modalit� di snap
				if (_osnapModes.IndexOf(mode) < 0)
					_osnapModes.Add(mode);
			}
			else
			{
				// Se lo trova lo elimina
				if (_osnapModes.IndexOf(mode) >= 0)
					_osnapModes.Remove(mode);
			}

			_snapLayers = snapLayers;

			if (_line1 == null)
			{
				_line1 = AddLine(0, 0, 0, 0);
				_line1.PenColor.ColorIndex = 6;
				_line1.PenWidth = 0;
			}
			if (_line2 == null)
			{
				_line2 = AddLine(0, 0, 0, 0); 
				_line2.PenColor.ColorIndex = 6;
				_line2.PenWidth = 0;
			}
			if (_line3 == null)
			{
				_line3 = AddLine(0, 0, 0, 0);
				_line3.PenColor.ColorIndex = 6;
				_line3.PenWidth = 0;
			}
			if (_line4 == null)
			{
				_line4 = AddLine(0, 0, 0, 0);
				_line4.PenColor.ColorIndex = 6;
				_line4.PenWidth = 0;
			}
		}

		/// <summary>
		/// Restituisce il punto di snap relativo a un punto
		/// </summary>
		/// <param name="point">punto </param>
		/// <returns></returns>
		private OsnapPoint GetSnapPoint(Point point)
		{
			gPoints points = new gPoints();
			vdSelection snapSel;
			OsnapPoint snapPoint = null; // = new OsnapPoint();
			//List<Breton.Polygons.Point> validSnapPoint = new List<Breton.Polygons.Point>();

			double size = ((GetScala() * mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.GlobalProperties.PickSize) / mDisplayObject.Height) / 2;

			points.Add(new gPoint(point.X - size, point.Y - size));
			points.Add(new gPoint(point.X + size, point.Y + size));

			// Trova le entit� interessate (quelle comprese nel cursore)
			snapSel = mDisplayObject.BaseControl.ActiveDocument.Selections.Add(String.Concat("MYSEL"));
            snapSel.RemoveAll();
			snapSel.Select(RenderSelect.SelectingMode.CrossingWindowRectangle, points);
			for (int i = snapSel.Count - 1; i >= 0; i--)
				if (snapSel[i].GetType().ToString().ToUpper() == "VECTORDRAW.PROFESSIONAL.VDFIGURES.VDIMAGE")
					snapSel.RemoveItem(_selActive[i]);

			double min = 0d;
			OsnapPoint p = null;

			// Trova i punti di snap validi
			for (int i = 0; i < snapSel.Count; i++)
			{
				vdFigure f = snapSel[i];
				if (_snapLayers != null)
				{
					bool find = false;
					foreach (string lay in _snapLayers)
					{
						if (f.Layer.Name == lay)
							find = true;
					}

					if (!find)
						continue;
				}
				else if (f.Layer.Frozen) 
					continue;

				//List<Breton.Polygons.Point> gripPoints = new List<Breton.Polygons.Point>();
				OsnapPoints gripPoints = new OsnapPoints(); // = f.GetGripPoints();
				if (f is vdLine)
				{
					vdLine l = (vdLine)f;
					foreach (SnapMode sn in _osnapModes)
					{
						if (sn == SnapMode.END)
						{
							OsnapPoint s = new OsnapPoint();
							s.x = l.StartPoint.x;
							s.y = l.StartPoint.y;
							s.Mode = OsnapMode.END;
							gripPoints.AddItem(s);
							OsnapPoint e = new OsnapPoint();
							e.x = l.EndPoint.x;
							e.y = l.EndPoint.y;
							e.Mode = OsnapMode.END;
							gripPoints.AddItem(e);
						}
						else if (sn == SnapMode.MID)
						{
							Segment seg = new Segment(l.StartPoint.x, l.StartPoint.y, l.EndPoint.x, l.EndPoint.y);
							OsnapPoint m = new OsnapPoint();
							m.x = seg.MiddlePoint.X;
							m.y = seg.MiddlePoint.Y;
							m.Mode = OsnapMode.MID;
							gripPoints.AddItem(m);
						}
						else if (sn == SnapMode.NEA)
						{
							// Calcola la proiezione del punto sul segmento per trovare il punto interessato
							OsnapPoint n = new OsnapPoint();
							double x, y, a, b, c;
							Segment seg = new Segment(l.StartPoint.x, l.StartPoint.y, l.EndPoint.x, l.EndPoint.y);
							seg.StraightLine(out a, out b, out c);
							MathUtil.ProjectedPointToStraight(a, b, c, point.X, point.Y, out x, out y);
							if (seg.IsInSide(new Point(x, y)))
							{
								n.x = x;
								n.y = y;
								n.Mode = OsnapMode.NEA;
								gripPoints.AddItem(n);
							}							
						}
					}					
				}
				else if (f is vdArc)
				{
					vdArc a = (vdArc)f;
					foreach (SnapMode sn in _osnapModes)
					{
						if (sn == SnapMode.END)
						{
							OsnapPoint s = new OsnapPoint();
							s.x = a.getStartPoint().x;
							s.y = a.getStartPoint().y;
							s.Mode = OsnapMode.END;
							gripPoints.AddItem(s);
							OsnapPoint e = new OsnapPoint();
							e.x = a.getEndPoint().x;
							e.y = a.getEndPoint().y;
							e.Mode = OsnapMode.END;
							gripPoints.AddItem(e);
						}
						else if (sn == SnapMode.MID)
						{
							Arc arc = new Arc(a.Center.x, a.Center.y, a.Radius, a.StartAngle, a.EndAngle - a.StartAngle);
							OsnapPoint m = new OsnapPoint();
							m.x = arc.MiddlePoint.X;
							m.y = arc.MiddlePoint.Y;
							m.Mode = OsnapMode.MID;
							gripPoints.AddItem(m);
						}
						else if (sn == SnapMode.NEA)
						{
							// Crea un segmento che va dal centro dell'arco al punto del mouse e lo estende per intersecare l'arco toccato
							Point[] inter;
							OsnapPoint n = new OsnapPoint();
							Arc arc = new Arc(a.Center.x, a.Center.y, a.Radius, a.StartAngle, a.EndAngle - a.StartAngle);
							Segment seg = new Segment(point.X, point.Y, a.Center.x, a.Center.y);
							seg.Extend(Side.EN_VERTEX.EN_VERTEX_START, 10000);
							seg.Intersect(arc, out inter);

							if (inter.Length > 0)
							{
								n.x = inter[0].X;
								n.y = inter[0].Y;
								n.Mode = OsnapMode.NEA;
								gripPoints.AddItem(n);
							}
						}
					}	
				}

				foreach (OsnapPoint pt in gripPoints)
				{
					double dist = point.Distance(new Point(pt.x, pt.y));
                    if (dist < min || min == 0d)
                    {
						snapPoint = pt;
                        min = dist;
                    }
				}
			}

			return snapPoint;
		}

        /// <summary>
        /// Forza il ridisegno 
        /// </summary>
        private void Redraw() { mDisplayObject.BaseControl.Redraw(); }

        /// <summary>
        /// Forza il ridisegno con il ricalcolo
        /// </summary>
        private void Regen() 
		{ 
			if (mDisplayObject.BaseControl.ActiveDocument != null)
				mDisplayObject.BaseControl.ActiveDocument.CommandAction.RegenAll(); 
		}

        /// <summary>
        /// Procedura di lettura della scala
        /// </summary>
        public double GetScala() { return mDisplayObject.BaseControl.ActiveDocument.ViewSize; }

        /// <summary>
        /// Procedura di evidenziazione delle entit� dati gli id dell'etichetta
        /// </summary>
        /// <param name="idBlock">primo elemento dell'etichetta</param>
        /// <param name="idPath">secondo elemento dell'etichetta</param>
        /// <param name="idSide">terzo elemento dell'etichetta</param>
        /// <param name="idVertex">quarto elemento dell'etichetta</param>
        /// <param name="thickness">spessore selezione. 0 per deselezionare</param>
        private void HighLight(int idBlock, int idPath, int idSide, int idVertex, double thickness, HighLigthType type)
        {

            try
            {
                //elimino i cerchi se lo spessore � 0
                if (thickness == 0)
                {
                    vdFilterObject filtro = new vdFilterObject();
                    filtro.Types.AddItem("VDRECT");
                    filtro.Labels.AddItem("VERTICE");

                    mDisplayObject.BaseControl.ActiveDocument.Selections.RemoveAll();
                    vdSelection mysel = mDisplayObject.BaseControl.ActiveDocument.Selections.Add("GetRow2");
                    mysel.FilterSelect(filtro);
                    mysel.ApplyFilter(filtro);
                    for (int intI = 0; intI < mysel.Count; intI++)
                        mysel[intI].Deleted = true;

                    mDisplayObject.BaseControl.ActiveDocument.ClearEraseItems();
                
                }

                vdSelection sel = SelectEntities(idBlock, idPath, idSide, idVertex);
                
                for (int intI = 0; intI < sel.Count; intI++)
                {

                    if (idVertex != -1)
                    {
                        double x = 0, y = 0;
                        if (sel[intI]._TypeName.ToUpper() == "VDLINE")
                        {
                            vdLine l = (vdLine)sel[intI];
                            if (idVertex == 1)
                            {
                                x = l.StartPoint.x;
                                y = l.StartPoint.y;
                            }
                            else if (idVertex == 2)
                            {
                                x = l.EndPoint.x;
                                y = l.EndPoint.y;
                            }
                        }
                        else
                        {
                            vdArc a = (vdArc)sel[intI];
                            if (idVertex == 1)
                            {
                                x = a.getStartPoint().x;
                                y = a.getStartPoint().y;
                            }
                            else if (idVertex == 2)
                            {
                                x = a.getEndPoint().x;
                                y = a.getEndPoint().y;
                            }
                        
                        }

                        vdHatchProperties htcProp = new vdHatchProperties(VdConstFill.VdFillModeSolid);
                        vdRect rect = AddRectangle(x - (thickness / 2), y - (thickness / 2), thickness, thickness);
                        rect.HatchProperties = htcProp;
                        rect.Label = "VERTICE";

                    }
                    else
                    {
                        sel[intI].PenWidth = thickness;
                        
                        if (type == HighLigthType.NORMAL)
                            sel[intI].LineType = mDisplayObject.BaseControl.ActiveDocument.LineTypes.ByLayer;
                        else if (type == HighLigthType.DOT)
                            sel[intI].LineType = mDisplayObject.BaseControl.ActiveDocument.LineTypes.DPIDot;
                        sel[intI].Invalidate();
                    }
                }

                Redraw();

            }

            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }

        }

        /// <summary>
        /// Procedura di selezione delle entit� dati gli id dell'etichetta
        /// </summary>
        /// <param name="idBlock">primo elemento dell'etichetta</param>
        /// <param name="idPath">secondo elemento dell'etichetta</param>
        /// <param name="idSide">terzo elemento dell'etichetta</param>
        /// <param name="idVertex">quarto elemento dell'etichetta</param>
        /// <param name="thickness">spessore selezione. 0 per deselezionare</param>
        private vdSelection SelectEntities(int idBlock, int idPath, int idSide, int idVertex)
        {

            try
            {

                vdEntities ent = mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities;

                mDisplayObject.BaseControl.ActiveDocument.Selections.RemoveAll();
                vdSelection sel = mDisplayObject.BaseControl.ActiveDocument.Selections.Add(idBlock.ToString());

                string label = idBlock.ToString() + "#";

                if (idPath != -1)
                    label = label + idPath.ToString() + "#";

                if (idSide != -1)
                    label = label + idSide.ToString();

                for (int i = 0; i < ent.Count; i++)
                {

                    vdFigure figura = ent[i];

                    int lung = 0;
                    if (figura.Label.Length >= label.Length)
                        lung = label.Length;
                    else
                        lung = figura.Label.Length;

                    if (figura.Label.Substring(0, lung) == label)
                        sel.AddItem(figura, true, vdSelection.AddItemCheck.Nochecking);

                }

                return sel;

            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }

        }

        /// <summary>
        /// Procedura eliminare le entit� data l'etichetta o una parte di essa
        /// </summary>
        /// <param name="idBlock">primo elemento dell'etichetta</param>
        private void DeleteEntities(int idBlock)
        {

            try
            {
                DeleteEntities(idBlock, -1);
            }
            catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }

        }

        /// <summary>
        /// Procedura eliminare le entit� data l'etichetta o una parte di essa
        /// </summary>
        /// <param name="idBlock">primo elemento dell'etichetta</param>
        /// <param name="idPath">secondo elemento dell'etichetta</param>
        private void DeleteEntities(int idBlock, int idPath)
        {

            try
            {

                vdSelection sel = SelectEntities(idBlock, idPath, -1, -1);

                for (int i = 0; i < sel.Count; i++)
                    sel[i].Deleted = true;

                ClearEraseItems();

                Redraw();
            }
            catch (Exception ex) 
			{ 
				TraceLog.WriteLine("ERROR: ", ex); 
			}
        }

        /// <summary>
        /// Pulisce la memoria dalle entit� eliminate
        /// </summary>
        private void ClearEraseItems() { mDisplayObject.BaseControl.ActiveDocument.ClearEraseItems(); }

		/// <summary>
		/// Crea un immagine di anteprima degli oggetti disegnati sul componente
		/// </summary>
		/// <param name="filename">file in cui salvare l'immagine</param>
		/// <param name="dimx">dimensione X dell'immagine</param>
		/// <param name="dimy">dimensione Y dell'immagine</param>
		/// <param name="transparent">indica se lo sfondo dell'immagine deve essere trasparente</param>
		/// <returns></returns>
		public bool CreateThumbnail(string filename, int dimx, int dimy, bool transparent)
		{
			try
			{
				// Seleziona il disegno
				mDisplayObject.BaseControl.ActiveDocument.CommandAction.CmdSelect("WP");
				vdSelection aSelset = mDisplayObject.BaseControl.ActiveDocument.Selections.FindName("VDRAW_PREVIOUS_SELSET");
				Box Bbox = aSelset.GetBoundingBox();
				Bbox.AddWidth(5.0);

				// Crea l'oggetto immagine
				Image img = new Bitmap(dimx, dimy);
				Graphics graph = Graphics.FromImage(img);
				Bbox.TransformBy(mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.World2ViewMatrix);
				mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.RenderToGraphics(graph, Bbox, img.Width, img.Height);

				if (transparent)
				{
					// Imposta lo sfondo trasparente
					Bitmap transp = new Bitmap(img);
					transp.MakeTransparent(mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.BkColor);
					transp.Save(filename);
				}
				else
					img.Save(filename);

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine(ToString() + ".CreateThumbnail", ex);
				return false;
			}
		}

		/// <summary>
		/// Disabilita il componente
		/// </summary>
		public void Disable()
		{
			if (_locked)
				return;

			mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.BkColor = Color.Gray;
			_locked = true;
			dragdrop = false;

			gPoint coord = new gPoint(mDisplayObject.BaseControl.ActiveDocument.Images[0].Width - Resources.Lucchetto.Width, -Resources.Lucchetto.Height, 0);
			vdImage img = new vdImage();

			img.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
			img.setDocumentDefaults();
			vdLayer l = FindVdLayer("LOCK");
			if (l == null)
				l = AddVdLayer(new CLayer("LOCK"));
			img.Layer = l;

			img.InsertionPoint = coord;

			img.Width = Resources.Lucchetto.Width;
			img.Height = Resources.Lucchetto.Height;
			if (!File.Exists(System.IO.Path.GetTempPath() + "lock.gif"))
				Resources.Lucchetto.Save(System.IO.Path.GetTempPath() + "lock.gif");
			img.ImageDefinition = mDisplayObject.BaseControl.ActiveDocument.Images.Add(System.IO.Path.GetTempPath() + "lock.gif");

			img.Label = "lock.gif";
			mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(img);

			Regen();
		}

		/// <summary>
		/// Abilita il componente
		/// </summary>
		public void Enable()
		{
			if (!_locked)
				return;

			for (int i = 0; i < mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.Count; i++)
			{
				if (mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i] is vdImage)
				{
					vdImage img = (vdImage)mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i];

					if (img.Label == "lock.gif")
					{
						mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.RemoveAt(i);
						break;
					}
				}
			}

			mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.BkColor = Color.Black;
			dragdrop = true;
			_locked = false;
			Regen();
		}

		/// <summary>
		/// Effettua la copia delle entit� in un componente di appoggio che sar� utilizzato per stampa, dxf ecc...
		/// </summary>
		/// <param name="control"></param>
		private void CopyControlContent(ref vdFramedControl control)
		{
			control.BaseControl.ActiveDocument.New();
			for (int i = 0; i < mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.Count; i++)
			{
				if (mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i].Layer.Frozen) //== vdFigure.VisibilityEnum.Invisible)
					continue;

				if (mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i] is vdLine )
				{
					vdLine oldline = (vdLine)mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i];
					vdLine line = new vdLine(oldline.StartPoint, oldline.EndPoint);
					line.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					line.setDocumentDefaults();
					control.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(line);
				}
				else if (mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i] is vdPolyline)
				{
					vdPolyline oldPolyline = (vdPolyline)mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i];

					vdPolyline pLine = new vdPolyline();
					vdEntities ent = oldPolyline.Explode();
					vdSelection sel = new vdSelection();
					
					for (int k = 0; k < ent.Count; k++)
						sel.AddItem(ent[k], false, vdSelection.AddItemCheck.Nochecking); ;
					
					pLine.JoinEntities(sel, 0.01);

					pLine.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					pLine.setDocumentDefaults();
					control.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(pLine);
				}
				else if (mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i] is vdArc)
				{
					vdArc oldarc = (vdArc)mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i];
					vdArc arco = new vdArc();
					arco.Center = new gPoint(oldarc.Center);
					arco.StartAngle = oldarc.StartAngle;
					arco.EndAngle = oldarc.EndAngle;
					arco.Radius = oldarc.Radius;
					arco.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					arco.setDocumentDefaults();
					control.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(arco);
				}
				else if (mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i] is vdText)
				{
					vdDocument documentoAttivo = mDisplayObject.BaseControl.ActiveDocument;
					vdText oldtext = (vdText)mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i];
					vdText text = new vdText();
					text.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					text.setDocumentDefaults();
					text.TextString = oldtext.TextString;
					text.Height = oldtext.Height;
					text.PenColor.FromSystemColor(Color.Black);
					text.InsertionPoint = oldtext.InsertionPoint;
					text.Layer = oldtext.Layer;
					text.Label = oldtext.Label;
					control.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(text);
				}
				else if (mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i] is vdImage)
				{
					vdImage oldimg = (vdImage)mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities[i];
					vdImage img = new vdImage();

					img.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
					img.setDocumentDefaults();
					img.Layer = oldimg.Layer;

					img.InsertionPoint = oldimg.InsertionPoint;

					img.Width = oldimg.Width;
					img.Height = oldimg.Height;
					img.ImageDefinition = oldimg.ImageDefinition; // mDisplayObject.BaseControl.ActiveDocument.Images.Add(mPhoto.Path);

					img.Label = oldimg.Label;
					control.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(img);
				}
			}
		}

		/// <summary>
		/// Stampa il componente
		/// </summary>
		public void Print(vdFramedControl control, 
						ref bool landscape, 
						ref int bottom, 
						ref int top, 
						ref int right, 
						ref int left, 
						ref bool blackwhite,
						ref string paper)
		{
			CopyControlContent(ref control);

			vdPrint printer = control.BaseControl.ActiveDocument.ActiveLayOut.Printer;

			// Set printer options
			printer.LandScape = landscape;
			printer.margins.Bottom = bottom;
			printer.margins.Top = top;
			printer.margins.Right = right; 
			printer.margins.Left = left;
			printer.OutInBlackWhite = blackwhite;
			if (paper != null && paper != "")
				printer.SelectPaper(paper);
			printer.InitializePreviewFormProperties(true, true, false, false);
			
			vdInsert ins = new vdInsert();
			ins.SetUnRegisterDocument(control.BaseControl.ActiveDocument);
			ins.setDocumentDefaults();			
			Box areastampa = ins.BoundingBox;
			printer.PrintWindow = areastampa;
			printer.DialogPreview();

			// Save printer options
			landscape = printer.LandScape;
			bottom = printer.margins.Bottom;
			top = printer.margins.Top;
			right = printer.margins.Right;
			left = printer.margins.Left;
			blackwhite = printer.OutInBlackWhite;
			paper = printer.GetPaper();
		}

		/// <summary>
		/// Effettua il salvataggio in dxf del componente
		/// </summary>
		/// <param name="filename">nome del file da salvare</param>
		/// <returns>
		///		true	:	salvataggio eseguito correttamente
		///		false	:	errore nel salvare il dxf
		/// </returns>
		public bool SaveDxf(vdFramedControl control, string filename)
		{
			CopyControlContent(ref control);

			return control.BaseControl.ActiveDocument.SaveAs(filename);
		}
        #region Gestione layer VD

        /// <summary>
        /// Inizializzazione dei layers in base alla lista impostata
        /// </summary>
        protected void InitLayers()
        {
            
        }

        /// <summary>
        /// Procedura di creazione di un layer in VD
        /// </summary>
        /// <param name="clayer">layer da creare</param>
        /// <returns>vdLayer creato</returns>
        private vdLayer AddVdLayer(CLayer clayer)
        {
            try
            {

                vdDocument documentoAttivo = mDisplayObject.BaseControl.ActiveDocument;
                vdLayer layer = documentoAttivo.Layers.Add(clayer.Name);
                layer.PenColor.SystemColor = clayer.Color;

                //vedere come gestire i tipi linea
                if (clayer.LineType == "DASHDOT")
                    layer.LineType = mDisplayObject.BaseControl.ActiveDocument.LineTypes.DPIDashDot;

                layer.LineWeight = (VdConstLineWeight)clayer.LineWidth; // VdConstLineWeight.LW_30;     // spessore della linea

                return layer;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="clayer"></param>
		/// <returns></returns>
		private vdLayer SetVdLayer(CLayer clayer)
		{
			try
			{
				for (int i = 0; i < mDisplayObject.BaseControl.ActiveDocument.Layers.Count; i++)
				{
					vdLayer layer = mDisplayObject.BaseControl.ActiveDocument.Layers[i];

					if (layer.Name.Trim() != clayer.Name.Trim())
						continue;

					layer.PenColor.SystemColor = clayer.Color;

					//vedere come gestire i tipi linea
					if (clayer.LineType == "DASHDOT")
						layer.LineType = mDisplayObject.BaseControl.ActiveDocument.LineTypes.DPIDashDot;

					layer.LineWeight = (VdConstLineWeight)clayer.LineWidth; // VdConstLineWeight.LW_30;     // spessore della linea

					return layer;
				}

				return null;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ERROR: ", ex);
				return null;
			}
		}

        /// <summary>
        /// Procedura di ricerca di un layer di VD data la chiave
        /// </summary>
        /// <param name="strChiave">layer name</param>
        /// <returns>vdLayer trovato</returns>
        private vdLayer FindVdLayer(string strChiave)
        {
            try
            {
                return mDisplayObject.BaseControl.ActiveDocument.Layers.FindName(strChiave);
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }
        }

        /// <summary>
        /// Procedura di disabilitazione di un layer di VD
        /// </summary>
        /// <param name="strLayer">layer name</param>
        /// <returns>true: il layer � stato disabilitato</returns>
        private bool DisableLayer(string strLayer)
        {
            try
            {
                bool bolDisabilitato = false;
				bool wasFrozen = false;

                mDisplayObject.BaseControl.ActiveDocument.ActiveLayer = FindVdLayer("0");

                vdLayer layer = FindVdLayer(strLayer);

                if (layer != null)
                {
                    bolDisabilitato = true;
					wasFrozen = layer.Frozen;
                    layer.Frozen = true;
                }

				if (!mLayers[strLayer].IsDrawn || !wasFrozen)
				{
					Redraw();
					mLayers[strLayer].IsDrawn = true;
				}

                return bolDisabilitato;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }
        }

        /// <summary>
        /// Procedura di abilitazione (visualizzazione) di un layer di VD
        /// </summary>
        /// <param name="strLayer">layer name</param>
        /// <returns>true: il layer � stato abilitato</returns>
        private bool EnableLayer(string strLayer)
        {
            try
            {
                bool bolAbilitato = false;
				bool wasFrozen = true;

                mDisplayObject.BaseControl.ActiveDocument.ActiveLayer = FindVdLayer("0");

                vdLayer layer = FindVdLayer(strLayer);

                if (layer != null)
                {
                    bolAbilitato = true;
					wasFrozen = layer.Frozen;
                    layer.Frozen = false;
                }

				if (!mLayers[strLayer].IsDrawn || wasFrozen)
				{
					Redraw();
					mLayers[strLayer].IsDrawn = true;
				}

                return bolAbilitato;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return false;
            }
        }

        /// <summary>
        /// Procedura di attivazione di un layer di VD
        /// </summary>
        /// <param name="strChiave">layer name</param>
        /// <returns>vdLayer attivato</returns>
        private vdLayer ActivateLayer(string strChiave)
        {
            try
            {
                vdLayer layer = FindVdLayer(strChiave);

                if (layer != null)
                {
                    mDisplayObject.BaseControl.ActiveDocument.ActiveLayer = layer;
                }

                return layer;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }

        }

        #endregion

        #region Gestione text VD

        /// <summary>
        /// Procedura di aggiunta di un testo in VD
        /// </summary>
        /// <param name="key">Chiave nella lista dei testi</param>
        /// <param name="ctext">CText da inserire</param>
        /// <returns></returns>
        private vdText AddVdText(int key, CText ctext)
        {
            try
            {

                vdDocument documentoAttivo = mDisplayObject.BaseControl.ActiveDocument;
                vdText text = new vdText();
                text.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
                text.setDocumentDefaults();
                text.TextString = ctext.Text;
                text.Height = ctext.Dimension;
                text.PenColor.FromSystemColor(ctext.Color);
                text.InsertionPoint = new gPoint(ctext.X, ctext.Y);
                text.Layer = FindVdLayer(ctext.Layer);
                text.Label = key.ToString();
                mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(text);
                
                return text;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ERROR: ", ex);
                return null;
            }
        }		

        #endregion

        #region Disegno in VD

        private vdLine DrawLine(Side s1)
        {
            return AddLine(s1.P1.X, s1.P1.Y, s1.P2.X, s1.P2.Y);
        }

        private vdArc DrawArc(Side s1)
        {
			return AddArc(s1.CenterPoint, s1.Radius, s1.StartAngle, s1.EndAngle, s1.AmplAngle);
        }

        private vdLine AddLine(double p_1, double p_2, double p_3, double p_4)
        {
            vdLine line = new vdLine(new gPoint(p_1, p_2), new gPoint(p_3, p_4));
            line.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
            line.setDocumentDefaults();
            mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(line);

            return line;
        }

        /// <summary>
        /// Aggiunge un arco al documento attivo
        /// </summary>
        /// <param name="center">centro arco</param>
        /// <param name="radius">raggio</param>
        /// <param name="startAngle">angolo di partenza</param>
        /// <param name="endAngle">angolo finale</param>
        /// <returns></returns>
        private vdArc AddArc(Point center, double radius, double startAngle, double endAngle, double amplAngle)
        {

            vdArc arco = new vdArc();

            arco.Center = new gPoint(center.X, center.Y);
			if (amplAngle >= 0d)
			{
				arco.StartAngle = startAngle;
				arco.EndAngle = endAngle;
			}
			else
			{
				arco.StartAngle = endAngle;
				arco.EndAngle = startAngle;
			}
            arco.Radius = radius;
            arco.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
            arco.setDocumentDefaults();
            mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(arco);

            return arco;

        }

        /// <summary>
        /// Aggiunge un cerchio al documento attivo
        /// </summary>
        /// <param name="centerX">X centro</param>
        /// <param name="centerY">Y centro</param>
        /// <param name="radius">raggio</param>
        /// <returns></returns>
        private vdCircle AddCircle(double centerX, double centerY, double radius)
        {

            vdCircle circle = new vdCircle();
            circle.Radius = radius;
            circle.Center.x = centerX;
            circle.Center.y = centerY;
            circle.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
            circle.setDocumentDefaults();
            mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(circle);

            return circle;

        }

        /// <summary>
        /// Aggiunge un cerchio al documento attivo
        /// </summary>
        /// <param name="posX">posizione X</param>
        /// <param name="posY">posizione Y</param>
        /// <param name="height">altezza</param>
        /// <param name="width">larghezza</param>
        /// <returns></returns>
        private vdRect AddRectangle(double posX, double posY, double height, double width)
        {

            vdRect rect = new vdRect();
            rect.Height = height;
            rect.Width = width;
            rect.InsertionPoint = new gPoint(posX, posY);
            rect.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
            rect.setDocumentDefaults();
            mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Entities.AddItem(rect);

            return rect;

        }

		/// <summary>
		/// Disegna un punto di Snap
		/// </summary>
		/// <param name="snapPoint">punto di snap</param>
		private void DrawSnapPoint(OsnapPoint snapPoint)
		{
			double size = ((GetScala() * mDisplayObject.BaseControl.ActiveDocument.ActiveLayOut.Render.GlobalProperties.PickSize) / mDisplayObject.Height) / 2;
			double spessoreSelezione = (GetScala() * 2) / mDisplayObject.Height;

			if (snapPoint.Mode == OsnapMode.END)
			{
				_line1.visibility = vdFigure.VisibilityEnum.Visible;
				_line2.visibility = vdFigure.VisibilityEnum.Visible;
				_line3.visibility = vdFigure.VisibilityEnum.Visible;
				_line4.visibility = vdFigure.VisibilityEnum.Visible;
				_line1.StartPoint = new gPoint(snapPoint.x - size, snapPoint.y + size);
				_line1.EndPoint = new gPoint(snapPoint.x + size, snapPoint.y + size);
				_line2.StartPoint = new gPoint(snapPoint.x + size, snapPoint.y + size);
				_line2.EndPoint = new gPoint(snapPoint.x + size, snapPoint.y - size);
				_line3.StartPoint = new gPoint(snapPoint.x + size, snapPoint.y - size);
				_line3.EndPoint = new gPoint(snapPoint.x - size, snapPoint.y - size);
				_line4.StartPoint = new gPoint(snapPoint.x - size, snapPoint.y - size);
				_line4.EndPoint = new gPoint(snapPoint.x - size, snapPoint.y + size);
				_line1.PenWidth = spessoreSelezione;
				_line2.PenWidth = spessoreSelezione;
				_line3.PenWidth = spessoreSelezione;
				_line4.PenWidth = spessoreSelezione;
				mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
			}
			else if (snapPoint.Mode == OsnapMode.MID)
			{
				_line1.visibility = vdFigure.VisibilityEnum.Visible;
				_line2.visibility = vdFigure.VisibilityEnum.Visible;
				_line3.visibility = vdFigure.VisibilityEnum.Visible;				
				_line1.StartPoint = new gPoint(snapPoint.x - size, snapPoint.y - size);
				_line1.EndPoint = new gPoint(snapPoint.x, snapPoint.y + size);
				_line2.StartPoint = new gPoint(snapPoint.x, snapPoint.y + size);
				_line2.EndPoint = new gPoint(snapPoint.x + size, snapPoint.y - size);
				_line3.StartPoint = new gPoint(snapPoint.x + size, snapPoint.y - size);
				_line3.EndPoint = new gPoint(snapPoint.x - size, snapPoint.y - size);
				_line1.PenWidth = spessoreSelezione;
				_line2.PenWidth = spessoreSelezione;
				_line3.PenWidth = spessoreSelezione;
				mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
			}
			else if (snapPoint.Mode == OsnapMode.NEA)
			{
				_line1.visibility = vdFigure.VisibilityEnum.Visible;
				_line2.visibility = vdFigure.VisibilityEnum.Visible;
				_line3.visibility = vdFigure.VisibilityEnum.Visible;
				_line4.visibility = vdFigure.VisibilityEnum.Visible;

				_line1.StartPoint = new gPoint(snapPoint.x - size, snapPoint.y + size);
				_line1.EndPoint = new gPoint(snapPoint.x + size, snapPoint.y + size);
				_line2.StartPoint = new gPoint(snapPoint.x + size, snapPoint.y + size);
				_line2.EndPoint = new gPoint(snapPoint.x - size, snapPoint.y - size);
				_line3.StartPoint = new gPoint(snapPoint.x - size, snapPoint.y - size);
				_line3.EndPoint = new gPoint(snapPoint.x + size, snapPoint.y - size);
				_line4.StartPoint = new gPoint(snapPoint.x + size, snapPoint.y - size);
				_line4.EndPoint = new gPoint(snapPoint.x - size, snapPoint.y + size);

				_line1.PenWidth = spessoreSelezione;
				_line2.PenWidth = spessoreSelezione;
				_line3.PenWidth = spessoreSelezione;
				_line4.PenWidth = spessoreSelezione;
				mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
			}
		}

		public void DeleteSnapPoint()
		{
			if (_line1 != null)
				_line1.visibility = vdFigure.VisibilityEnum.Invisible;
			if (_line2 != null)
				_line2.visibility = vdFigure.VisibilityEnum.Invisible;
			if (_line3 != null)
				_line3.visibility = vdFigure.VisibilityEnum.Invisible;
			if (_line4 != null)
				_line4.visibility = vdFigure.VisibilityEnum.Invisible;
		}

		/// <summary>
		/// Disegna una linea generica
		/// </summary>
		/// <param name="s">lato da disegnare</param>
		public void DrawGenericLine(Segment s)
		{
			DrawGenericLine(s, Color.White);
		}

		public void DrawGenericLine(Segment s, Color c)
		{
			if (_genericLine == null)
			{
				_genericLine = AddLine(0, 0, 0, 0);
				_genericLine.PenColor = new vdColor(c);
				_genericLine.PenWidth = 0;
			}

			_genericLine.visibility = vdFigure.VisibilityEnum.Visible;
			_genericLine.StartPoint = new gPoint(s.P1.X, s.P1.Y);
			_genericLine.EndPoint = new gPoint(s.P2.X, s.P2.Y); ;
			mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
		}

		/// <summary>
		/// Nasconde la linea disegnata
		/// </summary>
		public void DeleteGenericLine()
		{
			if (_genericLine != null)
				_genericLine.visibility = vdFigure.VisibilityEnum.Invisible;
		}

		/// <summary>
		/// Disegna un cerchio generico
		/// </summary>
		/// <param name="cx">coordinata x del centro</param>
		/// <param name="cy">coordinata y del centro</param>
		/// <param name="r">raggio</param>
		protected void DrawGenericCircle(double cx, double cy, double r, bool fill, Color color)
		{
			if (_genericCircle == null)
			{
				_genericCircle = AddCircle(cx, cy, r);
				_genericCircle.PenColor = new vdColor(color);
				_genericCircle.PenWidth = 0;
			}

			if (fill)
			{
				vdHatchProperties htcProp = new vdHatchProperties(VdConstFill.VdFillModeSolid);
				htcProp.Solid2dTransparency = 150;
				_genericCircle.HatchProperties = htcProp;
			}

			_genericCircle.visibility = vdFigure.VisibilityEnum.Visible;
			_genericCircle.Center = new gPoint(cx, cy);
			_genericCircle.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
			mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
		}

		/// <summary>
		/// Nasconde il cerchio disegnato
		/// </summary>
		protected void DeleteGenericCircle()
		{
			if (_genericCircle != null)
				_genericCircle.visibility = vdFigure.VisibilityEnum.Invisible;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cx"></param>
		/// <param name="cy"></param>
		/// <param name="h"></param>
		/// <param name="w"></param>
		/// <param name="fill"></param>
		/// <param name="color"></param>
		protected void DrawGenericRectangle(double cx, double cy, double h, double w, bool fill, Color color)
		{
			if (_genericRectangle == null)
			{
				_genericRectangle = AddRectangle(cx, cy, h, w);
				_genericRectangle.PenColor = new vdColor(color);
				_genericRectangle.PenWidth = 0;
			}

			if (fill)
			{
				vdHatchProperties htcProp = new vdHatchProperties(VdConstFill.VdFillModeSolid);
				htcProp.Solid2dTransparency = 150;
				_genericRectangle.HatchProperties = htcProp;
			}

			_genericRectangle.visibility = vdFigure.VisibilityEnum.Visible;
			_genericRectangle.InsertionPoint.x = cx;
			_genericRectangle.InsertionPoint.y = cy;
			_genericRectangle.Height = h;
			_genericRectangle.Width = w;
			_genericRectangle.SetUnRegisterDocument(mDisplayObject.BaseControl.ActiveDocument);
			mDisplayObject.BaseControl.ActiveDocument.ActionLayout.Invalidate();
		}

		/// <summary>
		/// Nasconde il rettangolo disegnato
		/// </summary>
		protected void DeleteGenericRectangle()
		{
			if (_genericRectangle != null)
			{
				_genericRectangle.visibility = vdFigure.VisibilityEnum.Invisible;
				_genericRectangle.Deleted = true;
				_genericRectangle = null; 
			}
		}
        #endregion

        #region Clear the VDF memory

        private void ClearMemory()
        {
            // NOTE : In big drawings this will take some time.
            object ByteArrayDoc = null;
            MemoryStream memorystream = mDisplayObject.BaseControl.ActiveDocument.ToStream();
            //Document is saved to memory in a ByteArray object
            if (memorystream == null)
            {
                //MessageBox.Show("Method Failed", "Clearing Memory"); return;
            }
            ByteArrayDoc = memorystream.ToArray();
            int size = (int)memorystream.Length;
            memorystream.Close();
            //Document is saved in memory in the ByteArray.

            MemoryStream memorystream2 = new MemoryStream((byte[])ByteArrayDoc);
            memorystream2.Position = 0;
            mDisplayObject.BaseControl.ActiveDocument.LoadFromMemory(memorystream2);
            memorystream2.Close();
            //MessageBox.Show("Deleted and Removed items doesn't exist any more. Undo/Redo doesn't work. \n\r \n\rNote: In big drawings this might take some time.", "Memory cleared !!!");
            //Document is "LOADED" again from the ByteArray in the memory.
        }

        #endregion

        #endregion
	}
}