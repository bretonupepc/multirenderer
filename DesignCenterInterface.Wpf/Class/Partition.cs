using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Xml;
using System.Drawing;

using Breton.MathUtils;
using Breton.Polygons;
using TraceLoggers;

namespace Breton.DesignCenterInterface
{
    public class Partition
    {
        #region Variables

        private string mOfferCode;          // per il nodo principale
        private string mCode;
        private int mIdSag;
        private string mName;
        private List<Partition> mLstPartition;
        private List<Block> mLstBlocks;
        private string mType;
        private string mAction;
        private RTMatrix mMatrix;
        private PointF[] mBoundingBox;
        private double mAngle;
        private bool mMakeMove;             //true=lo spostamento sulla bancata � avvenuto
        private List<DCIToolPath> mLstToolPath;
        private System.IO.MemoryStream mMemoryStream;


        // attributi dell'xml tipici della photo
        private double mLength;
        private double mHeight;
        private double mOriginX;
        private double mOriginY;
        private int mLevel;

        public enum ActionType
        { 
            MOVE,
            REMOVE,
            NONE
        }

		public enum Type
		{
			PHOTO,
			RAW_SLAB,
			SLAB,
			BLOCK,
			REF_POINT,
			NONE
		}

        // ALTRE VARIABILI
        private Block blkTrasl;
        private Block blkFound;         // per la ricerca di un blocco particolare
        ArrayList origin = new ArrayList();
        private string code;
        Partition pTmp;                                         // usata per le ricerche
        List<Partition> lstPrts;                                // usata per le ricerche
        private int count;
        private Partition mParent;

        #endregion

        #region Properties

        public string gOfferCode
        {
            set { mOfferCode = value; }
            get { return mOfferCode; }
        }

        public int gLevel
        {
            set { mLevel = value; }
            get { return mLevel; }
        }

        public string gCode
        {
            set { mCode = value; }
            get { return mCode; }
        }

        public int gIdSag
        {
            set { mIdSag = value; }
            get { return mIdSag; }
        }

        public string gName
        {
            set { mName = value; }
            get { return mName; }
        }

        public string gAction
        {
            set { mAction = value; }
            get { return mAction; }
        }

        public List<Partition> gPartitions
        {
            set { mLstPartition = value; }
            get { return mLstPartition; }
        }

        public List<Block> gBlocks
        {
            set { mLstBlocks = value; }
            get { return mLstBlocks; }
        }

        public PointF[] gBoundingBox
        {
            set { mBoundingBox = value; }
            get { return mBoundingBox; }
        }

        public double gAngle
        {
            set { mAngle = value; }
            get { return mAngle; }
        }

        public string gType
        {
            set { mType = value; }
            get { return mType; }
        }

        public RTMatrix gMatrix
        {
            set { mMatrix = value; }
            get { return mMatrix; }
        }

        public bool gMakeMove
        {
            set { mMakeMove = value; }
            get { return mMakeMove; }
        }

        public List<DCIToolPath> gToolPath
        {
            set { mLstToolPath = value; }
            get { return mLstToolPath; }
        }

        public double gLength
        {
            set { mLength = value; }
            get { return mLength; }
        }
        public double gHeight
        {
            set { mHeight = value; }
            get { return mHeight; }
        }
        public double gOriginX
        {
            set { mOriginX = value; }
            get { return mOriginX; }
        }
        public double gOriginY
        {
            set { mOriginY = value; }
            get { return mOriginY; }
        }

        public Partition gParent
        {
            set { mParent = value; }
            get { return mParent; }
        }

        public System.IO.MemoryStream gMemoryStream
        {
            set { mMemoryStream = value; }
            get { return mMemoryStream; }
        }


        #endregion

        #region Constructors

        public Partition()
            : this("", -1, "", null, null, null, 0, "", null, 0, 0, 0, 0)
        {
            
        }

        public Partition(string code, int id, string name, List<Partition> partitions, List<Block> blocks, PointF[] bboxPoint, double angle,
            string type, RTMatrix matrix, double length, double height, double originX, double originY)
        {
            mOfferCode = "";
            mCode = code;
            mIdSag = id;
            mName = name;

            if (partitions == null)
                mLstPartition = new List<Partition>();
            else
                mLstPartition = partitions;

            if (blocks == null)
                mLstBlocks = new List<Block>();
            else
                mLstBlocks = blocks;

            if (bboxPoint == null)
                mBoundingBox = new PointF[4];
            else
                mBoundingBox = bboxPoint;

            mAngle = angle;

            mType = type;
            mAction = ActionType.NONE.ToString();

            if (matrix == null)
                mMatrix= new RTMatrix();
            else
                mMatrix = matrix;

            mMakeMove = false;
            mLstToolPath = new List<DCIToolPath>();

            // parametri tipici della photo
            mLength = length;
            mHeight = height;
            mOriginX = originX;
            mOriginY = originY;
            mMemoryStream = new System.IO.MemoryStream();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// NON USATO!!!
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        //public bool WriteXmlPartition(XmlTextWriter writer)
        //{
        //    try
        //    {
        //        mLstPartition = this.gPartitions;

        //        WriteXml2(writer, this.gType, this.gCode, this.gIdSag, this.gName, this.gBoundingBox, this.gAngle,
        //            this.gAction, this.gLength, this.gHeight, this.gOriginX, this.gOriginY);

        //        //for (int x = 0; x < this.gBlocks.Count; x++)   // in realt� ha un solo blocco
        //        //{
        //        //    this.gBlocks[x].WriteFileXml(writer);      // dati geometrici del blocco photo
        //        //}

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("Partition->WriteXmlPartition - ERROR: ", ex);
        //        return false;
        //    }
        //    finally
        //    {
        //    }
        //}

        /// <summary>
        /// NON USATO!!!
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        //public Partition InsertNodePartition(Partition p, int level)
        //{
        //    for (int i = 0; i <level; i++)
        //    {
        //        pTmp = InsertNodePartition(p.gPartitions[i], i);
        //    }
        //    pTmp = p;
        //    return pTmp;
        //}

        /// <summary>
        /// restituisce la lista di partizioni sotto il livello passato
        /// </summary>
        /// <param name="?"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public List<Partition> GetPartitionLevel(Partition p, int numLev)
        {
            count = 0;
            lstPrts = new List<Partition>();
            FindPartitions(p, numLev);
            return lstPrts;
        }

        public Partition GetPartitionByCode(Partition p, string code)
        {
            pTmp = new Partition();
            FindPartition(p, code);
            return pTmp;
        }

        public List<Partition> GetPartitions(Partition p, string type)
        {
            lstPrts = new List<Partition>();
            FindPartitions(p, type);
            return lstPrts;
        }

        //public List<Partition> GetPartitions2(Partition p, string type)
        //{
        //    lstPrts = new List<Partition>();
        //    FindPartitions2(p, type);
        //    return lstPrts;
        //}

        //public List<Partition> ReadXmlPartition(XmlDocument doc)
        //{
        //    try
        //    {
        //        this.gParent = null;

        //        //XmlNode mainNode = doc.SelectSingleNode("MainTop");

        //        //ReadXml(mainNode, 0);
                
        //        foreach (XmlNode nd in doc.ChildNodes)
        //        {
        //            ReadXml(nd, 0);
        //        }

        //        return this.gPartitions;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("Partition->WriteXmlPartition - ERROR: ", ex);
        //        return null;
        //    }
        //}

        public void ModifyBlock(Partition p, Block block2Trnsl)
        {
            try
            {
                blkTrasl = block2Trnsl;
                FindBlock(p, block2Trnsl.Code);
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Partition->WriteXmlPartition - ERROR: ", ex);
            }
        }

        public Block FindBlockByCode(Partition p, string blkCode)
        {
            FindBlockCode(p, blkCode);
            return blkFound;
        }

        public Block FindBlockByName(Partition p, string blkName)
        {
            FindBlockName(p, blkName);
            return blkFound;
        }

        public ArrayList FindOriginByCode(Partition p, string blkCode)
        {
            FindOriginCode(p, blkCode);
            return origin;
        }

        public ArrayList FindOriginByName(Partition p, string blkName)
        {
            FindOriginName(p, blkName);
            return origin;
        }

        public string FindCodePartitionByName(Partition p, string blkName)
        {
            FindCodeByName(p, blkName);
            return code;
        }

        #endregion

		#region Static Methods
		public static bool WriteXmlPartitions(string fileXML, List<Partition> photos, SortedList<int, Block> projectBlocks, string offerCode)
		{
			XmlTextWriter writer = new XmlTextWriter(fileXML, null);

			try
			{				
				writer.Formatting = Formatting.Indented;
				writer.WriteStartElement("MainTop");
				writer.WriteAttributeString("OfferCode", offerCode);

				for (int i = 0; i < photos.Count; i++)
				{
					Partition photo = photos[i];
					WriteXmlPartition(writer, photo);
				}

                // Scrive il nodo xml riguardante  dei blocchi
                writer.WriteStartElement("Project");
                foreach (KeyValuePair<int, Block> b in projectBlocks)
                {
                    writer.WriteStartElement("Block");
                    writer.WriteAttributeString("Code", b.Value.Code);
                    writer.WriteAttributeString("Name", b.Value.Name);
                    b.Value.WriteFileXml(writer);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();

				return true;
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("Partition.WriteXmlPartition Error: ", ex);
				return false;
			}
			finally
			{
				writer.Flush();
				writer.Close();
			}
		}

		private static void WriteXmlPartition(XmlTextWriter xmlWriter, Partition part)
		{
			xmlWriter.WriteStartElement("Partition");
			xmlWriter.WriteAttributeString("Type", part.gType);
			xmlWriter.WriteAttributeString("Code", part.gCode);
			xmlWriter.WriteAttributeString("IdSag", part.gIdSag.ToString());
			xmlWriter.WriteAttributeString("Name", part.gName);
			xmlWriter.WriteAttributeString("Action", part.gAction);
			xmlWriter.WriteAttributeString("MakeMove", part.gMakeMove.ToString());

			for (int i = 0; i < part.gBoundingBox.Length; i++)
			{
				xmlWriter.WriteAttributeString("Pbox" + i.ToString() + "_X", part.gBoundingBox[i].X.ToString());
				xmlWriter.WriteAttributeString("Pbox" + i.ToString() + "_Y", part.gBoundingBox[i].Y.ToString());
			}

			xmlWriter.WriteAttributeString("Angle", part.gAngle.ToString());

			// attributi tipici della photo
			if (part.gLength != 0)
				xmlWriter.WriteAttributeString("Length", part.gLength.ToString());
			else
				xmlWriter.WriteAttributeString("Length", "-1");                 // per dire che non c'�

			if (part.gHeight != 0)
				xmlWriter.WriteAttributeString("Height", part.gHeight.ToString());
			else
				xmlWriter.WriteAttributeString("Height", "-1");                 // per dire che non c'�

			xmlWriter.WriteAttributeString("OriginX", part.gOriginX.ToString());
			xmlWriter.WriteAttributeString("OriginY", part.gOriginY.ToString());

			// stampa dati geometrici del Block relativo alla partizione considerata. // path[1] per il grezzo...
			xmlWriter.WriteStartElement("Geometry");
			for (int x = 0; x < part.gBlocks.Count; x++)
			{
				part.gBlocks[x].WriteFileXml(xmlWriter);
			}
			xmlWriter.WriteEndElement();
			
			// stampa dati geometrici del ToolPath relativo alla partizione considerata.
			xmlWriter.WriteStartElement("ToolPathCollection");
			for (int x = 0; x < part.gToolPath.Count; x++)
			{
				part.gToolPath[x].WriteFileXml(xmlWriter);
			}
			xmlWriter.WriteEndElement();

			for (int i = 0; i < part.gPartitions.Count; i++)
			{
				WriteXmlPartition(xmlWriter, part.gPartitions[i]);
			}

            //inserisco nel CData il file di VD
            if (part.gMemoryStream.Length > 0)
            {
                xmlWriter.WriteStartElement("VDDraw");
                System.IO.BinaryReader br = new System.IO.BinaryReader(part.gMemoryStream);
                char[] buffer = br.ReadChars((int)part.gMemoryStream.Length);
                string stbuffer = new String(buffer);
                xmlWriter.WriteCData(stbuffer);
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
		}

		#endregion

		#region private methods

        public bool ReadXmlPartition(string fileXML, out List<Partition> partitions)
        {
            List<Block> tmpBlocks;
            return ReadXmlPartition(fileXML, out partitions, out tmpBlocks);
        }

        public bool ReadXmlPartition(string fileXML, out List<Partition> partitions, out List<Block> projectBlocks)
        {
            try
            {
                partitions = new List<Partition>();
                this.gParent = null;

                XmlDocument doc = new XmlDocument();
                doc.Load(fileXML);
                XmlNode mainNode = doc.SelectSingleNode("MainTop");

                if (mainNode.Attributes[0].Name == "OfferCode")
                {
                    this.gOfferCode = mainNode.Attributes[0].Value;
                }

                ReadXml(mainNode, 0);

                projectBlocks = new List<Block>();
                XmlNode project = doc.SelectSingleNode("/MainTop/Project");
                if (project != null)
                {
                    foreach (XmlNode block in project.ChildNodes)
                    {
                        if (block.Name == "Block")
                        {
                            Block b = new Block();
                            b.Code = block.Attributes.GetNamedItem("Code").Value.ToString();
                            b.Name = block.Attributes.GetNamedItem("Name").Value.ToString();
                            XmlNode pathColl = block.SelectSingleNode("PathCollection");
                            b.ReadFileXml(pathColl);
                            projectBlocks.Add(b);
                        }
                    }
                }

                partitions = this.gPartitions;
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Partition.ReadXmlPartition ", ex);
                projectBlocks = null;
                partitions = null;
                return false;
            }
        }

        private void ReadXml(XmlNode xmlNode, int level)
        {
            
            this.gLevel = level;

            PointF[] bboxTmp = new PointF[4];
            for (int i = 0; i < xmlNode.Attributes.Count; i++)
            {
                if (xmlNode.Attributes[i].Name == "Type")
                {
                    this.gType = xmlNode.Attributes[i].Value;
                }
                else if (xmlNode.Attributes[i].Name == "Code")
                {
                    this.gCode = xmlNode.Attributes[i].Value;
                }
                else if (xmlNode.Attributes[i].Name == "IdSag")
                {
                    this.gIdSag = Convert.ToInt32(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Name")
                {
                    this.gName = xmlNode.Attributes[i].Value;
                }
                else if (xmlNode.Attributes[i].Name == "Pbox0_X")
                {
                    bboxTmp[0].X = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Pbox0_Y")
                {
                    bboxTmp[0].Y = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Pbox1_X")
                {
                    bboxTmp[1].X = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Pbox1_Y")
                {
                    bboxTmp[1].Y = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Pbox2_X")
                {
                    bboxTmp[2].X = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Pbox2_Y")
                {
                    bboxTmp[2].Y = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Pbox3_X")
                {
                    bboxTmp[3].X = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Pbox3_Y")
                {
                    bboxTmp[3].Y = float.Parse(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Angle")
                {
                    this.gAngle = Convert.ToDouble(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Action")
                {
                    this.gAction = xmlNode.Attributes[i].Value;
                }
                else if (xmlNode.Attributes[i].Name == "MakeMove")
                {
                    this.mMakeMove = Convert.ToBoolean(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Length")
                {
                    this.gLength = Convert.ToDouble(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "Height")
                {
                    this.gHeight = Convert.ToDouble(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "OriginX")
                {
                    this.gOriginX = Convert.ToDouble(xmlNode.Attributes[i].Value);
                }
                else if (xmlNode.Attributes[i].Name == "OriginY")
                {
                    this.gOriginY = Convert.ToDouble(xmlNode.Attributes[i].Value);
                }

            }
            if (bboxTmp.Length > 0)
                this.gBoundingBox = bboxTmp;

            if (xmlNode.HasChildNodes)
            {
                foreach (XmlNode nd in xmlNode.ChildNodes)
                {
                    //caricare il path
                    if (nd.Name == "Geometry")
                    {
                        //XmlNode pathNode = nd.SelectSingleNode("PathCollection");
                        foreach (XmlNode npc in nd.ChildNodes)
                        {
                            if (npc.Name == "PathCollection")
                            {
                                PathCollection pc = new PathCollection();
                                pc.ReadFileXml(npc);  // mi serve per la matrice

                                this.gMatrix = pc.MatrixRT;

                                Block bTmp = new Block();
                                foreach (XmlNode ndp in npc.ChildNodes)
                                {
                                    if (ndp.Name == "Path")
                                    {
                                        Path pp = new Path();
                                        pp.ReadFileXml(ndp);
                                        pp.MatrixRT = this.gMatrix;
                                        bTmp.MatrixRT = this.gMatrix;
                                        bTmp.AddPath(pp);
                                    }
                                    else if (ndp.Name == "Block_Attr")
                                    {
                                        bTmp.Name = ndp.Attributes.GetNamedItem("Name").Value;
                                        bTmp.Code = ndp.Attributes.GetNamedItem("Code").Value;
                                    }
                                }
                                this.gBlocks.Add(bTmp);
                            }
                        }
                    }
                    else if (nd.Name == "Partition")
                    {
                        Partition pTmp = new Partition();       //  L'ERRORE POTREBBE ESSERE QUIIIII
                        pTmp.ReadXml(nd, level + 1);

                        pTmp.gParent = this;

                        this.gPartitions.Add(pTmp);
                    }
                    else if (nd.Name == "VDDraw")  //carico in memoria il file di vd della bancata
                    {

                        for (int i = 0; i < nd.ChildNodes.Count; i++)
                        {
                            if (nd.ChildNodes[i].NodeType == XmlNodeType.CDATA)
                            {
                                //string content = nd.ChildNodes[i].InnerText.ToString().Replace("?<?xml", "<?xml");

                                string content = nd.ChildNodes[i].Value;
                                if (content.Substring(0, 1) != "<")
                                    content = nd.ChildNodes[i].Value.Substring(1);

                                /*
                                System.IO.FileStream file = System.IO.File.Open("c:\\aa.vdml", System.IO.FileMode.Create);
                                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(file);

                                streamWriter.Write(content);

                                streamWriter.Flush();
                                streamWriter.Close();
                                */

                                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                                byte[] b = encoding.GetBytes(content);

                                mMemoryStream = new System.IO.MemoryStream(b);

                                /*
                                file = System.IO.File.Open("c:\\aa1.vdml", System.IO.FileMode.Create);
                                streamWriter = new System.IO.StreamWriter(file);
                                System.IO.BinaryReader br = new System.IO.BinaryReader(mMemoryStream);
                                char[] buffer = br.ReadChars((int)mMemoryStream.Length);
                                string stbuffer = new String(buffer);

                                streamWriter.Write(stbuffer.Replace("?<?xml", "<?xml"));

                                streamWriter.Flush();
                                streamWriter.Close();
                                */

                                break;
                            }
                        }
                    
                    }
                }
            } 
        }

        private void FindPartition(Partition p, string code)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (code == p.gPartitions[i].gCode)
                    {
                        pTmp = p.gPartitions[i];
                    }
                    FindPartition(p.gPartitions[i], code);
                }
            }
        }

        private void FindPartitions(Partition p, int numLev)
        {
            if (count == numLev)
            {
                lstPrts = p.gPartitions;
                return;
            }
            else
            {
                count++;
                FindPartitions(p.gPartitions[count-1], numLev);
            }
        }

        private void FindPartitions(Partition p, string type)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (type == p.gPartitions[i].gType)
                    {
                        lstPrts.Add(p.gPartitions[i]);
                    }
                    FindPartitions(p.gPartitions[i], type);
                }
            }
        }

        private void FindPartitions2(Partition p, string type)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (type == p.gPartitions[i].gType)
                    {
                        // aggiungo solo se contengono partizioni di tipo BLOCK o niente, altrimenti non sarebbe una lastra "finale"
                        if (p.gPartitions[i].gPartitions.Count == 0 || p.gPartitions[i].gPartitions[0].gType == "BLOCK")
                        {
                            if(!lstPrts.Contains(p.gPartitions[i]))
                                lstPrts.Add(p.gPartitions[i]);
                        }
                        else
                        {
                            FindPartitions2(p.gPartitions[i], type);
                            /*
                            for (int x = 0; x < p.gPartitions[i].gPartitions.Count; x++)
                            {
                                if (p.gPartitions[i].gPartitions[x] == null || p.gPartitions[i].gPartitions[x].gType == "BLOCK")
                                    lstPrts.Add(p.gPartitions[i]);
                            }
                             * */
                        }
                    }
                    FindPartitions2(p.gPartitions[i], type);
                }
            }
        }

        private void FindBlock(Partition p, string blkCode)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (blkCode == p.gPartitions[i].gCode)
                    {
                        p.gPartitions[i].gBlocks[0] = blkTrasl;
                    }
                    FindBlock(p.gPartitions[i], blkCode);       // per arrivare ad avere i blocchi che non hanno pi� partizioni
                }
            }
        }

        private void FindBlockCode(Partition p, string blkCode)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (blkCode == p.gPartitions[i].gCode)
                    {
                        blkFound = p.gPartitions[i].gBlocks[0];
                        return;
                    }
                    FindBlockCode(p.gPartitions[i], blkCode);       // per arrivare ad avere i blocchi che non hanno pi� partizioni
                }
            }
        }

        private void FindBlockName(Partition p, string blkName)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (blkName == p.gPartitions[i].gName)
                    {
                        blkFound = p.gPartitions[i].gBlocks[0];
                        return;
                    }
                    FindBlockName(p.gPartitions[i], blkName);       // per arrivare ad avere i blocchi che non hanno pi� partizioni
                }
            }
        }

        private void FindOriginCode(Partition p, string blkCode)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (blkCode == p.gPartitions[i].gCode)
                    {
                        origin.Add(p.gPartitions[i].mOriginX);
                        origin.Add(p.gPartitions[i].mOriginY);
                        return;
                    }
                    FindOriginCode(p.gPartitions[i], blkCode);      // per arrivare ad avere i blocchi che non hanno pi� partizioni
                }
            }
        }

        private void FindOriginName(Partition p, string blkName)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (blkName == p.gPartitions[i].gName)
                    {
                        origin.Add(p.gPartitions[i].mOriginX);
                        origin.Add(p.gPartitions[i].mOriginY);
                    }
                    FindOriginName(p.gPartitions[i], blkName);      // per arrivare ad avere i blocchi che non hanno pi� partizioni
                }
            }
        }

        private void FindCodeByName(Partition p, string blkName)
        {
            if (p.gPartitions != null)
            {
                for (int i = 0; i < p.gPartitions.Count; i++)
                {
                    if (blkName.Contains("slab_") && //p.gPartitions[i].gCode.Contains("slab_") && 
                        (blkName.Split('_')).Length == 2)      // quando cerco RAW_SLAB il code non contiene il suffisso slab_
                                                     // inoltre controllo il fatto che non sia una sottolastra tipo slab_SLAB01...035_1
                    {
                        blkName = blkName.Substring(5);
                    }
                    if (blkName == p.gPartitions[i].gCode) //.gName)
                    {
                        code = p.gPartitions[i].gCode;
                        return;
                    }
                    FindCodeByName(p.gPartitions[i], blkName);       // per arrivare ad avere i blocchi che non hanno pi� partizioni
                }
            }
        }

        //elimina tutti i blocchi con lo stesso codice passato in ingresso
        public void DeleteBlockByCode(string blkName)
        {
            try
            {

                for (int i = this.gBlocks.Count - 1; i >= 0; i--)
                {
                    Block b = this.gBlocks[i];
                    if (b.Code == blkName)
                        this.gBlocks.Remove(b);
                }

            }
            catch (Exception ex) { TraceLog.WriteLine("ERRORE: ", ex); }
            }

        #endregion
    }
}