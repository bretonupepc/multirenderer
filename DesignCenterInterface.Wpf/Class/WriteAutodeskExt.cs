﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Eyeshot.Translators;
using devDept.Geometry;

namespace DesignCenterInterface.Class
{
    public class WriteAutodeskExt: WriteAutodesk
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private bool _outlineRegions = false;

        private bool _ignorePictures = false;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public WriteAutodeskExt(ViewportLayout layout, string filename, devDept.Geometry.lineWeightUnitsType linearUnitsType, 
            bool outlineRegions = false, bool ignorePictures = false)
            : base(layout, filename, lineWeightUnits:linearUnitsType)
        {
            _outlineRegions = outlineRegions;
            _ignorePictures = ignorePictures;
        }

        #endregion Constructors  -----------<

        protected override IList<Entity> SortEntitiesForWriteAutodesk(IList<Entity> entitiesToSave)
        {
            List<Entity> entityList = new List<Entity>();

            if (_outlineRegions || _ignorePictures)
            {
                foreach (Entity ent in entitiesToSave)
                {
                    System.Diagnostics.Debug.Print(ent.GetType().Name);
                    string typeName = ent.GetType().Name;
                    if (typeName.Contains("Region"))
                    {
                        if (_outlineRegions)
                        {
                            Region region = ent as Region;
                            if (region != null)
                            {
                                var curves = region.ContourList;
                                foreach (var curve in curves)
                                {
                                    Entity curveEnt = curve as Entity;
                                    if (curveEnt != null)
                                    {
                                        curveEnt.Color = region.Color;
                                        curveEnt.ColorMethod = region.ColorMethod;
                                        curveEnt.LineWeight = region.LineWeight;
                                        curveEnt.LineWeightMethod = region.LineWeightMethod;
                                        curveEnt.LayerIndex = region.LayerIndex;
                                        entityList.Add(curveEnt);
                                    }
                                }
                            }
                        }
                        else
                        {
                            entityList.Add(ent);
                        }
                    } 
                    else if (typeName.Contains("Picture"))
                    {
                        if (!_ignorePictures)
                        {
                            entityList.Add(ent);
                        }
                    }
                    else
                    {
                        entityList.Add(ent);
                    }
                }
                return entityList;
            }
            else
            {
                return base.SortEntitiesForWriteAutodesk(entitiesToSave);
            }
            return base.SortEntitiesForWriteAutodesk(entitiesToSave);
        }

        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
