using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Drawing;
using Breton.MathUtils;

using Breton.Polygons;
using TraceLoggers;

namespace Breton.DesignCenterInterface
{
    public class Block
    {
        /// <summary>
        /// Block � un insieme di percorsi che devono essere movimentati in modo atomico
        /// </summary>
        #region Variables

        protected int mId;						// Id del blocco corrispondente alla shape di ShopMaster
        protected string mCode;					// Codice del blocco aggiunto
        protected string mName;					// nome del blocco aggiunto
        protected double mAngle;				// Angolo di rotazione del blocco
        protected SortedList<int, Path> mPaths;	// lista dei percorsi
        protected RTMatrix mMatrixRT;			// matrice di rototraslazione
        protected bool bAllowMove;				// indica se il blocco puo' essere movimentato
        protected bool bAllowRotate;			// indica se il blocco puo' essere ruotato
        protected bool bAllowSelect;            // indica se il blocco puo' essere selezionato
        protected bool bAllowDragDrop;          // indica se il blocco puo' essere selezionato
		protected CPhoto mPhoto;				// indica la foto del blocco

        private int indexPath = -1;              // indice univoco del path
        internal SortedList<int, Color> mColor = new SortedList<int,Color>(); // lista dei percorsi con relativi colori

		private SortedList<string, string> mAttributes;	// Lista degli attributi del path

        #endregion

        #region Constructors

        //**************************************************************************
        // Costruisce l'insieme vuoto
        //**************************************************************************
        public Block()
        {
            mId = -1;
            mCode = "";
            mName = "";
            mAngle = 0;
            mPaths = new SortedList<int, Path>();
            mMatrixRT = new RTMatrix();
            bAllowMove = false;
            bAllowRotate =  false;
            bAllowSelect =  false;
            bAllowDragDrop = false;
			mAttributes = new SortedList<string, string>();
        }

        //**************************************************************************
        // Costruisce l'insieme di percorsi come copia di un altro insieme
        // Parametri:
        //			p	: percorso da copiare
        //**************************************************************************
        public Block(Block  pc)
        {
            mId = pc.mId;
            mCode = pc.mCode;
            mName = pc.mName;
            mAngle = pc.mAngle;
            bAllowMove = pc.bAllowMove;
            bAllowRotate = pc.bAllowRotate;
            bAllowSelect = pc.bAllowSelect;
            bAllowDragDrop = pc.bAllowDragDrop;
            mPaths = new SortedList<int, Path>();

			mMatrixRT = new RTMatrix(pc.MatrixRT);

			mAttributes = new SortedList<string, string>();
			// Aggiunge tutti gli attributi
			foreach (KeyValuePair<string, string> a in pc.mAttributes)
				mAttributes.Add(a.Key, a.Value);

			if (pc.Count > 0)
				// Aggiunge tutti i percorsi
				foreach (KeyValuePair<int, Path> p in pc.mPaths)
					AddPath(p.Value);
        }

		//**************************************************************************
		// Costruisce l'insieme di percorsi come copia di un altro insieme
		// Parametri:
		//			p	: percorso da copiare
		//**************************************************************************
		public Block(PathCollection pc)
			:this()
		{
			mMatrixRT = new RTMatrix(pc.MatrixRT);

			mAttributes = new SortedList<string, string>();
			// Aggiunge tutti gli attributi
			foreach (KeyValuePair<string, string> a in pc.Attributes)
				mAttributes.Add(a.Key, a.Value);

			// Aggiunge tutti i percorsi
			for (int i = 0; i < pc.Count; i++)
			{
				// Aggiunge il path al blocco
				AddPath(pc.GetPath(i));
			}
		}

        #endregion

        #region Properties

        //**************************************************************************
        // Code
        // Codice del blocco
        //**************************************************************************
        public int Id
        {
            set { mId = value; }
            get { return mId; }
        }

        //**************************************************************************
        // Code
        // Codice del blocco
        //**************************************************************************
        public string Code
        {
            set { mCode = value; }
            get { return mCode; }
        }

        //**************************************************************************
        // Name
        // Nome (codice) del blocco
        //**************************************************************************
        public string Name
        {
            set { mName = value; }
            get { return mName; }
        }

        //**************************************************************************
        // AllowMove
        // abilitazione alla movimentazione del blocco e dei percorsi contenuti
        //**************************************************************************
        public bool AllowMove
        {
            set { bAllowMove = value; }
            get { return bAllowMove; }
        }

        //**************************************************************************
        // AllowRotate
        // abilitazione alla rotazione del blocco e dei percorsi contenuti
        //**************************************************************************
        public bool AllowRotate
        {
            set { bAllowRotate = value; }
            get { return bAllowRotate; }
        }

        //**************************************************************************
        // AllowSelect
        // abilitazione alla selezione del blocco e dei percorsi contenuti
        //**************************************************************************
        public bool AllowSelect
        {
            set { bAllowSelect = value; }
            get { return bAllowSelect; }
        }

        //**************************************************************************
        // AllowSelect
        // abilitazione alla selezione del blocco e dei percorsi contenuti
        //**************************************************************************
        public bool AllowDragDrop
        {
            set { bAllowDragDrop = value; }
            get { return bAllowDragDrop; }
        }

        //**************************************************************************
        // Count
        // Ritorna il numero di percorsi
        //**************************************************************************
        public int Count
        {
            get { return mPaths.Count; }
        }

        //**************************************************************************
        // MatrixRT
        // Ritorna la matrice di rototraslazione
        //**************************************************************************
        public RTMatrix MatrixRT
        {
            set 
			{ 
				mMatrixRT = value;
				// Imposta la stessa matrice di rototraslazione per tutti i percorsi
				foreach (KeyValuePair<int, Path> p in mPaths)
					p.Value.MatrixRT = mMatrixRT;
			}
            get { return mMatrixRT; }
        }

        //**************************************************************************
        // Angle
        // Angolo di rotazione del blocco
        //**************************************************************************
		//public double Angle
		//{
		//    set { mAngle = value; }
		//    get { return mAngle; }
		//}

        //**************************************************************************
        // RotAngle
        // Ritorna l'angolo di rotazione del percorso
        //**************************************************************************
        public double RotAngle
        {
            get
            {
                double offsetX = 0d, offsetY = 0d, angle = 0d;

                mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);

                return angle;
            }
        }

        //**************************************************************************
        // TranslPoint
        // Ritorna il punto di traslazione del percorso
        //**************************************************************************
        public Breton.Polygons.Point TranslPoint
        {
            get
            {
                double offsetX = 0d, offsetY = 0d, angle = 0d;

                mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);

                return new Breton.Polygons.Point(offsetX, offsetY);
            }
        }

        //**************************************************************************
        // Surface
        // Restituisce l'area dell'insieme di percorsi (senza segno)
        //**************************************************************************
        public double Surface
        {
            get { return Math.Abs(SurfaceSign); }
        }
        //**************************************************************************
        // SurfaceSign
        // Restituisce l'area dell'insieme di percorsi (con segno)
        //**************************************************************************
        public double SurfaceSign
        {
            get
            {
                double area = 0d;

                // Somma le aree di tutti i percorsi
				foreach (KeyValuePair<int, Path> p in mPaths)
					area += p.Value.SurfaceSign;

                return area;
            }
        }

		/// <summary>
		/// Restituisce il path specifico
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		public Path this[int i]
		{
			get
			{
				foreach (KeyValuePair<int, Path> p in mPaths)
					if (i-- == 0) return p.Value;

				return null;
			}
		}

		/// <summary>
		/// Ritorna la foto del blocco
		/// </summary>
		public CPhoto Photo
		{
			set { mPhoto = value; }
			get { return mPhoto; }
		}


		///**************************************************************************
		/// <summary>
		/// Restituisce la lista degli attributi del path
		/// </summary>
		///**************************************************************************
		public SortedList<string, string> Attributes
		{
			get { return mAttributes; }
		}

        #endregion

        #region Public Methods

		///***************************************************************************
		/// <summary>
		/// Aggiunge un path
		/// </summary>
		/// <param name="p">path da aggiungere</param>
		/// <returns>chiave del path</returns>
		///***************************************************************************
		public int AddPath(Path p)
        {
			return AddPathRef(p.Clone());
        }
		
		///***************************************************************************
		/// <summary>
		/// Aggiunge un path, per riferimento, senza crearne una copia
		/// </summary>
		/// <param name="p">path da aggiungere</param>
		/// <returns>chiave del path</returns>
		///***************************************************************************
		public int AddPathRef(Path p)
		{
            p.MatrixRT = mMatrixRT;
			indexPath++;
			mPaths.Add(indexPath, p);			
			return indexPath;
		}

        //**************************************************************************
        // RemovePath
        // Rimuove un percorso
        // Parametri:
        //			p	: percorso da rimuovere
        // Restituisce:
        //			true	percorso eliminato
        //			false	non � possibile eliminare il percorso
        //**************************************************************************
        public bool RemovePath(Path p)
        {
            if (mPaths.IndexOfValue(p) == -1)
                return false;

            mPaths.RemoveAt(mPaths.IndexOfValue(p));

            return true;
        }

        //**************************************************************************
        // RemovePath
        // Rimuove un percorso
        // Parametri:
        //			i	: chiave percorso da rimuovere
        // Restituisce:
        //			true	percorso eliminato
        //			false	non � possibile eliminare il percorso
        //**************************************************************************
        public bool RemovePath(int key)
        {
            return mPaths.Remove(key);
        }

        //*************************************************************************
        // GetPath
        // Ritorna il percorso specificato passandogli l'id
        // Parametri:
        //			i	: chiave percorso
        // Restituisce:
        //			percorso
        //**************************************************************************
        public Path GetPath(int key)
        {
            if (!mPaths.ContainsKey(key))
                return null;

            return mPaths[key];

        }

        //**************************************************************************
        // GetPaths
        // Ritorna tutti i paths
        // Restituisce:
        //			mPaths
        //**************************************************************************
        public SortedList<int, Path> GetPaths()
        {
            return mPaths;
        }


        //public void ReversePathOrder()
        //{
        //    mPaths.Reverse();
        //}

        //**************************************************************************
        // GetRectangle
        // Ritorna il rettangolo che contiene l'insieme dei percorsi
        //**************************************************************************
        public Rect GetRectangle()
        {
			Rect rett = null;

            if (mPaths.Count == 0)
                return null;

			foreach (KeyValuePair<int, Path> p in mPaths)
			{
				if (rett == null)
					rett = p.Value.GetRectangle();
				else
					rett = rett + p.Value.GetRectangle();
            }

            return rett;
        }

        //**************************************************************************
        // GetRectangleRT
        // Ritorna il rettangolo che contiene l'insieme dei percorsi rototraslati
        //**************************************************************************
        public Rect GetRectangleRT()
        {
			Rect rett= null;

            if (mPaths.Count == 0)
                return null;

			foreach (KeyValuePair<int, Path> p in mPaths)
			{
				if (rett == null)
					rett = p.Value.GetRectangleRT();
				else
					rett = rett + p.Value.GetRectangleRT();
            }

            return rett;
        }

        //**************************************************************************
        // RotoTranslAbs
        // Rototraslazione assoluta 
        // Parametri:
        //			offsetX	: offset di traslazione X del percorso
        //			offsetY	: offset di traslazione Y del percorso
        //			centerX	: ascissa X del centro di rotazione
        //			centerY	: ascissa Y del centro di rotazione
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
		public void RotoTranslAbs(double offsetX, double offsetY, double centerX, double centerY, double angle)
		{
			mMatrixRT.RotoTranslAbs(offsetX, offsetY, centerX, centerY, angle);
		}

        //**************************************************************************
        // RotoTranslRel
        // Rototraslazione relativa 
        // Parametri:
        //			offsetX	: offset di traslazione X del percorso
        //			offsetY	: offset di traslazione Y del percorso
        //			centerX	: ascissa X del centro di rotazione
        //			centerY	: ascissa Y del centro di rotazione
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
        public void RotoTranslRel(double offsetX, double offsetY, double centerX, double centerY, double angle)
        {
            mMatrixRT.RotoTranslRel(offsetX, offsetY, centerX, centerY, angle);
        }

        //**************************************************************************
        // GetRotTrasl
        // Legge la traslazione e l'angolo di rotazione del percorso
        // Parametri:
        //			offsetX	: traslazione X
        //			offsetY	: traslazione Y
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
        public void GetRotoTransl(out double offsetX, out double offsetY, out double angle)
        {
            mMatrixRT.GetRotoTransl(out offsetX, out offsetY, out angle);
        }

        //**************************************************************************
        // SetRotoTransl
        // Setta la traslazione e l'angolo di rotazione del percorso
        // Parametri:
        //			offsetX	: traslazione X
        //			offsetY	: traslazione Y
        //			angle	: angolo di rotazione in radianti
        //**************************************************************************
        public void SetRotoTransl(double offsetX, double offsetY, double angle)
        {
            mMatrixRT.SetRotoTransl(offsetX, offsetY, angle);
        }

		//**************************************************************************
		// Symmetry
		// Setta la traslazione e l'angolo di rotazione del percorso
		// Parametri:
		//			offsetX	: traslazione X
		//			offsetY	: traslazione Y
		//			angle	: angolo di rotazione in radianti
		//**************************************************************************
		public void Symmetry(double x1, double y1, double x2, double y2)
		{
			mMatrixRT.Symmetry(x1, y1, x2, y2);
		}

		/// <summary>
		/// Determina se un blocco e` interno al blocco
		/// </summary>
		/// <param name="block">blocco da controllare se � contenuto</param>
		/// <returns>
		///		true	il percorso � completamente contenuto
		///		false	il percorso non � contenuto
		/// </returns>
		///**************************************************************************
		public bool Contains(Block blk)
		{
			// Scorre tutti i path del blocco e li confronta con tutti i path del blocco passato come parametro
			foreach (KeyValuePair<int, Path> pt1 in mPaths)
			{
				foreach (KeyValuePair<int, Path> pt2 in blk.mPaths)
				{
					if (pt1.Value.Contains(pt2.Value))
						return true; 
				}
			}
			
			return false;
		}

		/// <summary>
		/// Determina se un blocco e` interno al blocco rototraslato
		/// </summary>
		/// <param name="block">blocco da controllare se � contenuto</param>
		/// <returns>
		///		true	il percorso � completamente contenuto
		///		false	il percorso non � contenuto
		/// </returns>
		///**************************************************************************
		public bool ContainsRT(Block blk)
		{
			foreach (KeyValuePair<int, Path> pt1 in mPaths)
			{
				foreach (KeyValuePair<int, Path> pt2 in blk.mPaths)
				{
					if (pt1.Value.ContainsRT(pt2.Value))
						return true;
				}
			}

			return false;
		}

        //**************************************************************************
        // Normalize
        // Ritorna l'insieme dei percorsi normalizzato
        // Parametri:
        //			offset	: punto di normalizzazione
        //**************************************************************************
        public Block Normalize()
        {
            Rect r = GetRectangle();
            Breton.Polygons.Point offset = new Breton.Polygons.Point(r.Left, r.Top);

            Block pc = new Block();

			foreach (KeyValuePair<int, Path> p in mPaths)
				pc.AddPathRef(p.Value.Normalize(offset));
			
            pc.MatrixRT = new RTMatrix(mMatrixRT);

            return pc;
        }

        //**************************************************************************
        // MayAdd
        // Verifica se � possibile aggiungere il percorso alla zona
        // Parametri:
        //			p		: percorso da aggiungere
        // Restituisce:
        //			true	: il percorso pu� essere inseribile
        //			false	: il percorso non pu� essere inserito
        //**************************************************************************
        public bool MayAdd(Path p)
        {
            return MayInsert(mPaths.Count, p);
        }
        //**************************************************************************
        // MayInsert
        // Verifica se il percorso � inseribile
        // Parametri:
        //			index	: posizione di inserimento
        //			p		: percorso da inserire
        // Restituisce:
        //			true	: il percorso pu� essere inseribile
        //			false	: il percorso non pu� essere inserito
        //**************************************************************************
        public bool MayInsert(int index, Path p)
        {
            return true;
        }

        //**************************************************************************
        // ReadFileXml
        // Legge il lato dal file XML
        // Parametri:
        //			n	: nodo XML contenete il lato
        // Ritorna:
        //			true	lettura eseguita con successo
        //			false	errore nella lettura
        //**************************************************************************
        public bool ReadFileXml(XmlNode n)
        {
            Path p;

            try
            {
                XmlNodeList l = n.ChildNodes;

                // Scorre tutti i sottonodi
                foreach (XmlNode chn in l)
                {
                    // Legge i nodi dei polygoni
                    if (chn.Name == "Path")
                    {
                        p = new Path();
                        if (p.ReadFileXml(chn))
                        {
							p.MatrixRT = mMatrixRT;
                            indexPath++;
                            mPaths.Add(indexPath, p);
                        }
                    }

                        // Legge il nodo della matrice
                    else if (chn.Name == "MatrixRT")
                    {
                        double[,] m = new double[3, 3];

                        m[0, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m00").Value);
                        m[0, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m01").Value);
                        m[0, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m02").Value);
                        m[1, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m10").Value);
                        m[1, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m11").Value);
                        m[1, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m12").Value);
                        m[2, 0] = Convert.ToDouble(chn.Attributes.GetNamedItem("m20").Value);
                        m[2, 1] = Convert.ToDouble(chn.Attributes.GetNamedItem("m21").Value);
                        m[2, 2] = Convert.ToDouble(chn.Attributes.GetNamedItem("m22").Value);

                        mMatrixRT.Matrix = m;
                    }
					else if (chn.Name == "Attributes")
					{
						foreach (XmlNode attr in chn.ChildNodes)
						{
							XmlAttribute val = attr.Attributes["value"];
							mAttributes.Add(attr.Name, val.Value);
						}
					}

                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("PathCollection.ReadFileXml Error.", ex);
                return false;
            }
        }

        //**************************************************************************
        // WriteFileXml
        // Scrive il blocco su file XML
        // Parametri:
        //			w: oggetto per scrivere il file XML
        //**************************************************************************
        public void WriteFileXml(XmlTextWriter w)
        {
            w.WriteStartElement("PathCollection");

			foreach (KeyValuePair<string, string> a in mAttributes)
			{
				w.WriteStartElement(a.Key);
				w.WriteAttributeString("value", a.Value);
				w.WriteEndElement();
			}
			
			double[,] m = mMatrixRT.Matrix;
            // Scrive la matrice di rototraslazione
            w.WriteStartElement("MatrixRT");
            w.WriteAttributeString("m00", m[0, 0].ToString());
            w.WriteAttributeString("m01", m[0, 1].ToString());
            w.WriteAttributeString("m02", m[0, 2].ToString());
            w.WriteAttributeString("m10", m[1, 0].ToString());
            w.WriteAttributeString("m11", m[1, 1].ToString());
            w.WriteAttributeString("m12", m[1, 2].ToString());
            w.WriteAttributeString("m20", m[2, 0].ToString());
            w.WriteAttributeString("m21", m[2, 1].ToString());
            w.WriteAttributeString("m22", m[2, 2].ToString());
            w.WriteEndElement();

            w.WriteStartElement("Block_Attr");
            w.WriteAttributeString("Name", this.Name);
            w.WriteAttributeString("Code", this.Code);
            w.WriteEndElement();

            // Scrive tutti i lati
			foreach (KeyValuePair<int, Path> p in mPaths)
				p.Value.WriteFileXml(w, false);

            w.WriteEndElement();
        }

        //**************************************************************************
        // WriteFileXml
        // Scrive il blocco su file XML
        // Parametri:
        //			baseNode: oggetto per scrivere il file XML
        //**************************************************************************
        public void WriteFileXml(XmlNode baseNode)
        {
            XmlAttribute attr;
            XmlDocument doc = baseNode.OwnerDocument;
            XmlNode node = doc.CreateNode(XmlNodeType.Element, "PathCollection", "");

			// Inserisce gli attributi del path
			XmlNode attributes = doc.CreateNode(XmlNodeType.Element, "Attributes", ""); ;
			foreach (KeyValuePair<string, string> a in mAttributes)
			{
				XmlNode attributeValue = doc.CreateNode(XmlNodeType.Element, a.Key, ""); ;
				attr = doc.CreateAttribute("value");
				attr.Value = a.Value;
				attributeValue.Attributes.Append(attr);
				attributes.AppendChild(attributeValue);
			}
			node.AppendChild(attributes);

            // Scrive la matrice di rototraslazione
			double[,] m = mMatrixRT.Matrix;
			XmlNode matrix = doc.CreateNode(XmlNodeType.Element, "MatrixRT", "");

            attr = doc.CreateAttribute("m00");
            attr.Value = m[0, 0].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m01");
            attr.Value = m[0, 1].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m02");
            attr.Value = m[0, 2].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m10");
            attr.Value = m[1, 0].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m11");
            attr.Value = m[1, 1].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m12");
            attr.Value = m[1, 2].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m20");
            attr.Value = m[2, 0].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m21");
            attr.Value = m[2, 1].ToString();
            matrix.Attributes.Append(attr);
            attr = doc.CreateAttribute("m22");
            attr.Value = m[2, 2].ToString();
            matrix.Attributes.Append(attr);

            node.AppendChild(matrix);

            XmlNode blAttNd = doc.CreateNode(XmlNodeType.Element, "Block_Attr", ""); ;
            attr = doc.CreateAttribute("Name");
            attr.Value = this.Name;
            blAttNd.Attributes.Append(attr);
            attr = doc.CreateAttribute("Code");
            attr.Value = this.Code;
            blAttNd.Attributes.Append(attr);
            node.AppendChild(blAttNd);

            // Scrive tutti i percorsi
            foreach (KeyValuePair<int, Path> p in mPaths)
                p.Value.WriteFileXml(node, false);

            baseNode.AppendChild(node);
        }

        //**************************************************************************
        // GetColor
        // Restituisce il colore di un determinato percorso
        // Parametri:
        //			keyPath: Chiave del percorso
        //**************************************************************************
        public Color GetColor(int keyPath)
        {
            try
            {
                foreach (KeyValuePair<int, Color> c in this.mColor)
                    if (keyPath == c.Key)
                        return mColor.Values[keyPath];

                return Color.Empty;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Block.GetColor Error.", ex);
                return Color.Empty;
            }
        }

        //**************************************************************************
        // WriteFileXml
        // Scrive il lato su file XML
        // Parametri:
        //			c: Colore
        //**************************************************************************
        public void SetColor(Color c)
        {
            try
            {
                if (mColor == null)
                    mColor = new SortedList<int, Color>();

                SetColor(-1, c);
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Block.SetColor Error.", ex);
            }
        }

        //**************************************************************************
        // WriteFileXml
        // Scrive il lato su file XML
        // Parametri:
        //			keyPath: Chiave del percorso
        //          c: Colore
        //**************************************************************************
        public void SetColor(int keyPath, Color c)
        {
            try
            {
                if (mColor == null)
                    mColor = new SortedList<int, Color>();

                if (keyPath == -1)       // Coloro tutti i percorsi del blocco nel colore specificato
                {
                    mColor.Clear();     // azzero per non avere chiavi doppie
                    foreach (KeyValuePair<int, Path> p in this.mPaths)
                        mColor.Add(p.Key, c);
                }
                else                    // Coloro il percorso specifico nel colore indicato
                {
                    if (this.mPaths.ContainsKey(keyPath))
                        mColor.Add(keyPath, c);
                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Block.SetColor Error.", ex);
            }
        }

        //**************************************************************************
        // WriteFileXml
        // Esegue il reset dei colori cambiati - Verranno considerati quelli attivi per i layer
        //**************************************************************************
        public void ResetColor()
        {
            try
            {
                mColor.Clear();
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Block.ResetColor Error.", ex);
            }
        }

        #endregion




        public void SetAttribute(string key, string value)
        {
            if (mAttributes == null)
            {
                mAttributes = new SortedList<string, string>();
            }
            if (mAttributes.ContainsKey(key))
            {
                mAttributes[key] = value;
            }
            else
            {
                mAttributes.Add(key, value);
            }
        }

        public string GetAttributeValue(string key)
        {
            if (mAttributes == null)
            {
                return string.Empty;
            }

            if (mAttributes.ContainsKey(key))
            {
                return mAttributes[key];
            }

            return string.Empty;
        }

    }


}