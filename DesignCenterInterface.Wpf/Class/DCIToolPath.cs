using System;
using System.Collections.Generic;
using System.Text;

using Breton.Polygons;

namespace Breton.DesignCenterInterface
{
    public class DCIToolPath : Breton.Polygons.Path
    {

        #region Variables

        private string mType;           //tipo utensile (fresa, disco...)

        #endregion

        #region Properties

        /// <summary>
        /// Tipo di utensile
        /// </summary>
        public string Type
        {
            set { mType = value; }
            get { return mType; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Costruttore per l'estensione di Path
        /// </summary>
        /// <param name="type">Tipo di utensile</param>
        public DCIToolPath(string type)
            : base()
        {
            mType = type;
        }

        #endregion

        #region Public Methods
        
        // WriteXml

        // ReadXml

        #endregion



    }
}