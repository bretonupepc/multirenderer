using System;
using System.Collections.Generic;
using System.Drawing;
using Breton.DesignCenterInterface;
using System.Xml.Serialization;

namespace Breton.DesignCenterInterface
{
    public enum TextAlignment
    {
        Left,
        Right,
        Center
    }

    /// <summary>
    /// CBasicPaintEventArgs definisce i parametri relativi ad eventi della classe CBasicPaint
    /// </summary>
    public class CBasicPaintEventArgs : EventArgs
    {

        #region Variables

        public enum enMouseKey
        {
            MouseKey_None,
            MouseKey_Left,
            MouseKey_Center,
            MouseKey_Right
        }
        private enMouseKey mMouseKey;

        private CSelection mSelection;
        private bool mAccepted;

        #endregion

        #region Properties

        public CSelection Selection
        {
            set { mSelection = value; }
            get { return mSelection; }
        }

        public bool Accepted
        {
            set { mAccepted = value; }
            get { return mAccepted; }
        }

        public enMouseKey MouseKey
        {
            get { return mMouseKey; }
        }
        #endregion

        #region Constructors

        public CBasicPaintEventArgs(CSelection selection)
        {
            mSelection = selection;
            mAccepted = false;
            mMouseKey = enMouseKey.MouseKey_None;
        }
        public CBasicPaintEventArgs(CSelection selection, enMouseKey mkey)
        {
            mSelection = selection;
            mAccepted = false;
            mMouseKey = mkey;
        }
        public CBasicPaintEventArgs(enMouseKey mkey)
        {
            mSelection = null;
            mAccepted = false;
            mMouseKey = mkey;
        }

        #endregion
    }

    // delegates
    public delegate void CBasicPaintEventArgsHandler(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no);

    /// <summary>
    /// CSelection gestisce le informazioni di selezione.
    /// </summary>
    public class CSelection
    {
        #region Variables

        private int lSelectedBlock;
        private int lSelectedPath;
        private int lSelectedElement;
        private int lSelectedVertex;

        #endregion

        #region Properties
        public int SelectedBlock
        {
            internal set { lSelectedBlock = value; }
            get { return lSelectedBlock; }
        }
        public int SelectedPath
        {
            internal set { lSelectedPath = value; }
            get { return lSelectedPath; }
        }
        public int SelectedElement
        {
            internal set { lSelectedElement = value; }
            get { return lSelectedElement; }
        }
        public int SelectedVertex
        {
            internal set { lSelectedVertex = value; }
            get { return lSelectedVertex; }
        }
        #endregion

        #region Constructors

        public CSelection()
        {
            lSelectedBlock = -1;
            lSelectedPath = -1;
            lSelectedElement = -1;
            lSelectedVertex = -1;
        }

        public CSelection(int block_no)
        {
            lSelectedBlock = block_no;
            lSelectedPath = -1;
            lSelectedElement = -1;
            lSelectedVertex = -1;
        }

        public CSelection(int block_no, int path_no)
        {
            lSelectedBlock = block_no;
            lSelectedPath = path_no;
            lSelectedElement = -1;
            lSelectedVertex = -1;
        }

        public CSelection(int block_no, int path_no, int elem_no)
        {
            lSelectedBlock = block_no;
            lSelectedPath = path_no;
            lSelectedElement = elem_no;
            lSelectedVertex = -1;
        }

        public CSelection(int block_no, int path_no, int elem_no, int vertex_no)
        {
            lSelectedBlock = block_no;
            lSelectedPath = path_no;
            lSelectedElement = elem_no;
            lSelectedVertex = vertex_no;
        }

        #endregion

    }

    /// <summary>
    /// CPhoto gestisce le informazioni dela photo.
    /// </summary>
    public class CPhoto
    {
        #region Variables
        private string path;
        private double width;
        private double height;
        private string name;
        private string layer;
        #endregion

        #region Properties
        public double Width
        {
            set { width = value; }
            get { return width; }
        }
        public double Height
        {
            set { height = value; }
            get { return height; }
        }

        public string Path
        {
            set { path = value; }
            get { return path; }
        }

        public string Name
        {
            set { name = value; }
            get { return name; }
        }

        public string Layer
        {
            set { layer = value; }
            get { return layer; }
        }

        #endregion

        #region Constructors

		public CPhoto()
			: this("", 0, 0, "", "")
        {
        }

		public CPhoto(string path)
			: this(path, 0, 0, "", "")
        {
        }

		public CPhoto(string path, double width, double height)
			: this(path, width, height, "", "")
        {
        }

		public CPhoto(string path, double width, double height, string name)
			: this(path, width, height, name, "")
		{
		}

		public CPhoto(CPhoto ph)
			: this(ph.path, ph.width, ph.height, ph.name, ph.layer)
		{
		}

        public CPhoto(string path, double width, double height, string name, string layer)
        {
            this.path = path;
            this.width = width;
            this.height = height;
            this.name = name;
            this.layer = layer;
        }	

        #endregion
    }

    public class CLayer
    {
        #region Variables
        private bool bVisible;
        private bool bUnlocked;
		private bool bFill;
		private bool bUnFill;
		private bool bIsDrawn;
        private string sName;
        private Color mColor;
        private string sLineType;
        private int iLineWidth;

        private int _zLevel = 0;
        #endregion

        #region Properties
		[XmlElement("Visible")]
        public bool Visible
        {
            set { bVisible = value; }
            get { return bVisible; }
        }

		[XmlElement("Unlocked")]
        public bool Unlocked
        {
            set { bUnlocked = value; }
            get { return bUnlocked; }
        }

		[XmlElement("Fill")]
		public bool Fill
		{
			set { bFill = value; }
			get { return bFill; }
		}

		[XmlElement("UnFill")]
		public bool UnFill
		{
			set { bUnFill = value; }
			get { return bUnFill; }
		}

		[XmlIgnore]
		public bool IsDrawn
		{
			set { bIsDrawn = value; }
			get { return bIsDrawn; }
		}

		[XmlAttribute("Name")]
        public string Name
        {
            set { sName = value; }
            get { return sName; }
        }

		[XmlIgnore]
        public Color Color
        {
            set { mColor = value; }
            get { return mColor; }
        }

		[XmlElement("ColorRGB")]
		public int RGBColor
		{
			set { mColor = Color.FromArgb(value); }
			get { return mColor.ToArgb(); }
		}

		[XmlElement("LineType")]
        public string LineType
        {
            set { sLineType = value; }
            get { return sLineType; }
        }

		[XmlElement("LineWidth")]
        public int LineWidth
        {
            set { iLineWidth = value; }
            get { return iLineWidth; }
        }

        public int ZLevel
        {
            get { return _zLevel; }
            set { _zLevel = value; }
        }

        #endregion

        #region Constructors

		public CLayer()
			: this("")
		{ 
		}
        
		public CLayer(string name)
			: this(name, true, true)
		{ 
		}       

		public CLayer(string name, bool visible, bool unlocked)
			: this(name, visible, unlocked, new Color())
		{
		}

		public CLayer(string name, bool visible, bool unlocked, Color color)
			: this(name, visible, unlocked, color, "")
		{
		}

		public CLayer(string name, bool visible, bool unlocked, Color color, string lineType)
			: this(name, visible, unlocked, color, lineType, 0)
		{ }

		public CLayer(string name, bool visible, bool unlocked, Color color, string lineType, int lineWidth)
			: this(name, visible, unlocked, false, color, lineType, lineWidth)
		{ }

		public CLayer(string name, bool visible, bool unlocked, bool fill, Color color, string lineType, int lineWidth)
			: this(name, visible, unlocked, fill, false, color, lineType, lineWidth)
		{ }

		public CLayer(string name, bool visible, bool unlocked, bool fill, bool unfill, Color color, string lineType, int lineWidth, int zLevel = 0)
		{
			bVisible = visible;
			bUnlocked = unlocked;
			bFill = fill;
			bUnFill = unfill;
			sName = name;
			mColor = (color == Color.Empty ? Color.Black : color);
			sLineType = lineType;
			iLineWidth = lineWidth;
			bIsDrawn = false;
		    _zLevel = zLevel;
		}
        #endregion
    }

    public class CText
    {
        #region Variables
        private string mText;
        private Color mColor;
        private int mDim;
        private double mpX;
        private double mpY;
        private string mLayer;

        private TextAlignment _alignment = TextAlignment.Center;
        #endregion

        #region Properties

        public string Text
        {
            set { mText = value; }
            get { return mText; }
        }
        public Color Color
        {
            set { mColor = value; }
            get { return mColor; }
        }
        public int Dimension
        {
            set { mDim = value; }
            get { return mDim; }
        }
        public double X
        {
            set { mpX = value; }
            get { return mpX; }
        }
        public double Y
        {
            set { mpY = value; }
            get { return mpY; }
        }
        public string Layer
        {
            set { mLayer = value; }
            get { return mLayer; }
        }

        public TextAlignment Alignment
        {
            get { return _alignment; }
            set { _alignment = value; }
        }

        #endregion

        #region Constructors

        public CText()
        {
            mText = "";
            mColor = new Color();
            mDim = 0;
            mpX = 0;
            mpY = 0;
            mLayer = "";
        }

        public CText(string text, Color color, int dimension, double X, double Y, string layer, TextAlignment alignment = TextAlignment.Center)
        {
            mText = text;
            mColor = color;
            mDim = dimension;
            mpX = X;
            mpY = Y;
            mLayer = layer;
        }

        #endregion

    }

	public class CSnapPoint : Breton.Polygons.Point
	{
		CBasicPaint.SnapMode mMode;

		public CSnapPoint(double x, double y, CBasicPaint.SnapMode mode ) : base(x, y)
		{
			mMode = mode;
		}

		public CSnapPoint() : this(0d, 0d, CBasicPaint.SnapMode.NONE)
		{}

		public CSnapPoint(CSnapPoint p): this(p.X, p.Y, p.Mode)
		{}

		public CBasicPaint.SnapMode Mode
		{
			set 
			{
				mMode = value;
			}
			get 
			{
				return mMode;
			}
		}
	}

}