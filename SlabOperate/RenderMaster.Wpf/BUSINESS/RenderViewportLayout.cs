﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using BretonViewportLayout;
using BretonViewportLayout.Common;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using devDept.Graphics;
using TraceLoggers;
using Point = System.Drawing.Point;

namespace RenderMaster.Wpf.BUSINESS
{
    public class RenderViewportLayout: ViewportLayout
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private bool _dragging;

        private Point3D _startDraggingPoint;

        private bool _showDrag;

        Point3D moveFrom;

        Point3D moveTo;



        private List<Entity> _dragEntityList;

        private Point _moveDragMouseLocation;

        public Point MoveDragMouseLocation
        {
            get { return _moveDragMouseLocation; }
            set { _moveDragMouseLocation = value; }
        }

        public List<Entity> DragEntityList
        {
            get { return _dragEntityList; }
            set { _dragEntityList = value; }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed || ActionMode != actionType.None ||
                GetToolBar().Contains(RenderContextUtility.ConvertPoint(GetMousePosition(e))))
            {
                base.OnMouseDown(e);
                return;
                
            } 

            // gets 3D start point
            ScreenToPlane(RenderContextUtility.ConvertPoint(GetMousePosition(e)), Plane.XY, out _startDraggingPoint);
            base.OnMouseDown(e);
        }

        protected override void OnDragOver(DragEventArgs drgevent)
        {
            ShowDrag = true;


            ScreenToPlane(RenderContextUtility.ConvertPoint(drgevent.GetPosition(this)), Plane.XY, out moveTo);


            DrawDragEntities();

            //var mousePos = drgevent.GetPosition(this);

            //_moveDragMouseLocation = new Point((int)mousePos.X, (int)mousePos.Y);

            ////_moveDragMouseLocation = this.PointToClient(_moveDragMouseLocation);

            base.OnDragOver(drgevent);
        }

        protected override void OnDragLeave(DragEventArgs e)
        {
            ShowDrag = false;
            Invalidate();
            //ForcePaint();
            base.OnDragLeave(e);
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            Dragging = false;
            ShowDrag = false;
            DragEntityList = null;
            Invalidate();
            base.OnMouseUp(e);
        }

        protected override void OnDrop(DragEventArgs e)
        {
            Dragging = false;
            ShowDrag = false;
            DragEntityList = null;
            Invalidate();

            //ForcePaint();
            base.OnDrop(e);
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            DragBlockInfo content = e.Data.GetData(typeof(DragBlockInfo)) as DragBlockInfo;
            if (content != null)
            {
                // De Conti 28/04/2016
                //if (sender != content.DragSource)
                {

                    //var current = new System.Drawing.Point(e.X, e.Y);
                    var current = RenderContextUtility.ConvertPoint(e.GetPosition(this));

                    //current = _viewportLayout.PointToClient(current);

                    Point3D p;
                    ScreenToPlane(current, Plane.XY, out p);

                    StartDraggingPoint = p;
                    StartDraggingPoint = (Point3D)content.Offset.Clone();

                    Dragging = true;

                    DragEntityList = new List<Entity>(content.DragEntities);
                }
            }
            base.OnDragEnter(e);
        }


        protected override void DrawOverlay(DrawSceneParams data)
        {
            try
            {
                if (Dragging && ShowDrag)
                {
                    DrawDragEntities();
                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Render draw overlay error ", ex);
            }
            finally
            {
                base.DrawOverlay(data);
            }
        }

        public void DrawDragEntities()
        {

            //Point3D moveTo;

            //ScreenToPlane(RenderContextUtility.ConvertPoint(GetMousePosition(e)), xyPlane, out moveTo);


            Vector3D tempMovement = new Vector3D(_startDraggingPoint, moveTo);

            // Show temp entity for current movement state
            foreach (Entity ent in this.DragEntityList)
            {
                //if (Layers.Count > ent.LayerIndex && Layers[ent.LayerIndex].Name.ToUpperInvariant().Contains("RAW"))
                {
                    Entity tempEntity = (Entity)ent.Clone();
                    tempEntity.Color = ent.GetColor();


                    ////Vector3D tempMovement = new Vector3D(points[0], CurrentWorldPoint);


                    tempEntity.Translate(tempMovement);

                    DrawDragEntity(tempEntity);

                    Invalidate();
                }

            }
        }

        private void DrawDragEntity(Entity tempEntity)
        {
            if (tempEntity is ICurve)
            {
                Draw(tempEntity as ICurve, tempEntity.Color, lineThickness: 2F);
            }
        }

        private void Draw(ICurve theCurve, Color color = default(Color), bool drawBorder = true, bool fill = false, bool dottedLines = false, float lineThickness = 1)
        {
            if (theCurve is CompositeCurve)
            {
                CompositeCurve compositeCurve = theCurve as CompositeCurve;
                Entity[] explodedCurves = compositeCurve.Explode();
                foreach (Entity ent in explodedCurves)

                    DrawScreenCurve((ICurve)ent, color, drawBorder, fill, dottedLines, lineThickness);
            }
            else
            {
                DrawScreenCurve(theCurve, color, drawBorder, fill, dottedLines, lineThickness);
            }
        }
        public Color DrawingColor = Color.WhiteSmoke;

        private void DrawScreenCurve(ICurve curve, Color color, bool drawBorder = true, bool fill = false, bool dottedBorder = false, float pixelThickness = 1)
        {
            const int subd = 100;

            var originalColor = renderContext.CurrentWireColor;

            var originalThickness = renderContext.CurrentLineWidth;

            renderContext.SetLineSize(pixelThickness);


            if (color == default(Color))
            {
                color = DrawingColor;
            }

            Point3D[] pts = new Point3D[subd + 1];

            for (int i = 0; i <= subd; i++)
            {
                pts[i] = WorldToScreen(curve.PointAt(curve.Domain.ParameterAt((double)i / subd)));
            }

            if (fill)
            {
                renderContext.SetState(blendStateType.Blend);
                renderContext.SetColorWireframe(Color.FromArgb(40, color));
                renderContext.SetState(rasterizerStateType.CCW_PolygonFill_CullFaceBack_NoPolygonOffset);
                renderContext.DrawTrianglesFan(pts, new Vector3D(0, 0, 1));
                renderContext.SetState(blendStateType.NoBlend);
            }
            if (drawBorder)
            {
                renderContext.SetColorWireframe(color);
                if (dottedBorder)
                {
                    renderContext.SetLineStipple(1, 0x0F0F, Viewports[0].Camera);
                    renderContext.EnableLineStipple(true);
                }

                renderContext.DrawLineStrip(pts);

                if (dottedBorder)
                {
                    renderContext.EnableLineStipple(false);
                }


                renderContext.SetLineSize(originalThickness);

                renderContext.SetColorWireframe(originalColor);
            }
        }



        private Point3D MoveDragWorldPosition
        {
            get
            {
                Point3D p;
                ScreenToPlane(MoveDragMouseLocation, Plane.XY, out p);
                return p;
            }
        }




        public bool Dragging
        {
            get { return _dragging; }
            set { _dragging = value; }
        }

        public Point3D StartDraggingPoint
        {
            get { return _startDraggingPoint; }
            set { _startDraggingPoint = value; }
        }

        public bool ShowDrag
        {
            get { return _showDrag; }
            set { _showDrag = value; }
        }

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
