﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RenderMaster.Wpf.BUSINESS
{
    public struct ToBigRender
    {
        public int idSag;
        public Bitmap imgSag;

        public string imgSagPath;

        /// <summary>
        /// Costruttore con parametri
        /// </summary>
        /// <param name="id">idSag</param>
        /// <param name="sag">imgSag</param>
        public ToBigRender(int id, Bitmap sag)
        {
            this.idSag = id;
            this.imgSag = sag;
            this.imgSagPath = string.Empty;
        }

        /// <summary>
        /// Costruttore con path immagine
        /// </summary>
        /// <param name="id">Id sagoma</param>
        /// <param name="sagPath">Image path sagoma</param>
        public ToBigRender(int id, string sagPath)
        {
            this.idSag = id;
            this.imgSagPath = sagPath;
            this.imgSag = null;
        }

    }

}
