﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RenderMaster.Wpf.BUSINESS
{
    public class MeshMoveEventArgs
    {
        private int _meshNumber;
        private double _x;
        private double _y;

        public MeshMoveEventArgs(int meshNumber, double x, double y)
        {
            _meshNumber = meshNumber;
            _x = x;
            _y = y;
        }

        public int MeshNumber
        {
            get { return _meshNumber; }
        }

        public double X
        {
            get { return _x; }
        }

        public double Y
        {
            get { return _y; }
        }
    }
}
