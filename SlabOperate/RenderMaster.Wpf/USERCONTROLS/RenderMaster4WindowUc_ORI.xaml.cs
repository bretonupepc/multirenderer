﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using devDept.Eyeshot;
using devDept.Graphics;

namespace RenderMaster.Wpf.USERCONTROLS
{
    /// <summary>
    /// Interaction logic for RenderMasterUc.xaml
    /// </summary>
    public partial class RenderMaster4WindowUc_ORI : UserControl
    {
        #region >-------------- Constants and Enums

        private const string LICENSE_CODE = "PROWPF-0612-VGS0V-UNV3T-4HMMS";

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        Camera vista;

        private bool _loading = true;

        private string lingua = "ITA";

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public RenderMaster4WindowUc_ORI()
        {
            InitializeComponent();

            vppRender1.Unlock(LICENSE_CODE);

            vppRender1.Viewports[0].Camera.ProjectionMode = projectionType.Orthographic;

            tsChkLight1.IsChecked = vppRender1.Light1.Active;
            tsChkLight2.IsChecked = vppRender1.Light2.Active;
            tsChkLight3.IsChecked = vppRender1.Light3.Active;
            tsChkLight4.IsChecked = vppRender1.Light4.Active;

            _loading = false;

        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public string Lingua
        {
            get { return lingua; }
            set { lingua = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates

        void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                default:
                    break;
            }
        }


        private void cmdZoomTutto_Click(object sender, EventArgs e)
        {

            vppRender1.ZoomFit();
            vppRender1.Refresh();

        }

        private void cmdZoomFinestra_CheckedChanged(object sender, EventArgs e)
        {

            bool bolCheck = cmdZoomFinestra.IsChecked.HasValue && (bool)cmdZoomFinestra.IsChecked;

            if (cmdZoomPan.IsChecked.HasValue && (bool)cmdZoomPan.IsChecked) cmdZoomPan.IsChecked = false;

            cmdZoomFinestra.IsChecked = bolCheck;

            if (bolCheck)
            {
                vppRender1.ActionMode = actionType.ZoomWindow;
            }
            else
            {
                vppRender1.ActionMode = actionType.None;
            }

        }

        private void cmdZoomIn_Click(object sender, EventArgs e)
        {

            vppRender1.ZoomIn(25);
            vppRender1.Invalidate();

        }

        private void cmdZoomOut_Click(object sender, EventArgs e)
        {

            vppRender1.ZoomOut(25);
            vppRender1.Invalidate();

        }

        private void cmdZoomPan_CheckedChanged(object sender, EventArgs e)
        {

            bool bolCheck = cmdZoomPan.IsChecked.HasValue && (bool)cmdZoomPan.IsChecked;
            if (cmdZoomFinestra.IsChecked.HasValue && (bool)cmdZoomFinestra.IsChecked) cmdZoomFinestra.IsChecked = false;
            cmdZoomPan.IsChecked = bolCheck;

            if (bolCheck)
            {

                vppRender1.ActionMode = actionType.Pan;

            }

            else
            {

                vppRender1.ActionMode = actionType.None;

            }

        }

        // Gestione click destro per uscire dai comandi azione (Zoom Finestra, Pan)
        //private void vppRender_MouseClick(object sender, MouseEventArgs e)
        //{

        //    if (e.RightButton .Button.ToString() == "Right")
        //    {

        //        cmdZoomFinestra.Checked = false;
        //        cmdZoomPan.Checked = false;

        //        //vppRender1.Action = devDept.Eyeshot.Viewport.actionType.None;

        //    }

        //}


        private void cmdVistaIsometrica_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Isometric);
            vppRender1.Refresh();

        }

        private void cmdVistaFrontale_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Front);
            vppRender1.Refresh();

        }

        private void cmdVistaAlto_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Top);
            vppRender1.Refresh();

        }

        private void cmdVistaLaterale_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Left);
            vppRender1.Refresh();

        }

        private void cmdSalvaVista_Click(object sender, EventArgs e)
        {
            vppRender1.SaveView(out vista);
        }

        private void cmdRipristinaVista_Click(object sender, EventArgs e)
        {
            vppRender1.RestoreView(vista);
            vppRender1.Invalidate();

        }

        private void VppRender1_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Right)
            {

                cmdZoomFinestra.IsChecked = false;
                cmdZoomPan.IsChecked = false;

                //vppRender1.Action = devDept.Eyeshot.Viewport.actionType.None;

            }
        }

        private void Button1_OnClick(object sender, RoutedEventArgs e)
        {
            vppRender1.Viewports[0].DisplayMode = displayType.Wireframe;
            vppRender1.ShowCurveDirection = true;
            vppRender1.Invalidate();

        }

        private void Button2_OnClick(object sender, RoutedEventArgs e)
        {
            vppRender1.Viewports[0].DisplayMode = displayType.Rendered;
            vppRender1.Invalidate();
        }

        private void BtnShowNormals_OnClick(object sender, RoutedEventArgs e)
        {
            vppRender1.ShowNormals = !vppRender1.ShowNormals;
            vppRender1.Invalidate();
        }

        #endregion Delegates  -----------<
    }
}
