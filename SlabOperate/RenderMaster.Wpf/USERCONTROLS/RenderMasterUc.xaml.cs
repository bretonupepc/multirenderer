﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Breton.DesignUtil;
using BretonViewportLayout;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using devDept.Graphics;
using RenderMaster.Wpf.BUSINESS;
using RenderMaster.Wpf.VIEWMODELS;
using RenderMaster.Wpf.WINDOWS;
using TraceLoggers;
using Wpf.Common;
using Brush = System.Windows.Media.Brush;
using Color = System.Windows.Media.Color;
using MouseButton = System.Windows.Input.MouseButton;
using Point = System.Drawing.Point;


namespace RenderMaster.Wpf.USERCONTROLS
{
    /// <summary>
    /// Interaction logic for RenderMasterUc.xaml
    /// </summary>
        public partial class RenderMasterUc : UserControl
        {
            #region >-------------- Constants and Enums

        private const string LICENSE_CODE = "PROWPF-0612-VGS0V-UNV3T-4HMMS";

        #endregion Constants and Enums -----------<

            #region >-------------- Events declaration

        public static event EventHandler<MeshMoveEventArgs> MoveStarted;
        public static event EventHandler<MeshMoveEventArgs> Move;

        public static event EventHandler<MeshMoveEventArgs> MoveCompleted;
            

        #endregion Events declaration  -----------<

            #region >-------------- Private Fields

        private WpfRenderWindow _renderWindow = null;

        private Window _mainParent = null;

        private object _locker = new object();

        Camera vista;

        private Render render;

        private RenderMasterViewModel _viewModel;

        private int _entityIndex;

        private Point3D _moveFrom;

        private bool _moveStarted = false;

        private int _meshNumber = -1;


        #endregion Private Fields --------<

            #region >-------------- Constructors

        public RenderMasterUc()
        {
            InitializeComponent();

            vppRender1.Unlock(LICENSE_CODE);

            vppRender1.AllowDrop = true;

            vppRender1.Viewports[0].Camera.ProjectionMode = projectionType.Orthographic;

            //this.DataContextChanged += RenderMasterUc_DataContextChanged;

            Loaded += RenderMasterUc_Loaded;

            AttachViewerEvents();
        }

        private void AttachViewerEvents()
        {
            vppRender1.MouseDown += vppRender1_MouseDown;
            vppRender1.MouseMove += vppRender1_MouseMove;
            vppRender1.MouseUp += vppRender1_MouseUp;
            //vppRender1.DragEnter += vppRender1_DragEnter;
        }


        void vppRender1_DragEnter(object sender, DragEventArgs e)
        {
            DragBlockInfo content = e.Data.GetData(typeof(DragBlockInfo)) as DragBlockInfo;
            if (content != null)
            {
                // De Conti 28/04/2016
                //if (sender != content.DragSource)
                {

                    //////var current = new System.Drawing.Point(e.X, e.Y);
                    ////var current = RenderContextUtility.ConvertPoint(e.GetPosition(vppRender1));

                    //////current = _viewportLayout.PointToClient(current);

                    ////Point3D p;
                    ////vppRender1.ScreenToPlane(current, Plane.XY, out p);

                    ////vppRender1.StartDraggingPoint = p;
                    ////vppRender1.StartDraggingPoint = (Point3D)content.Offset.Clone();

                    ////vppRender1.Dragging = true;

                    ////vppRender1.DragEntityList = new List<Entity>(content.DragEntities);
                }
            }
        }

        void vppRender1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && _moveStarted && _meshNumber != -1)
            {
                OnMoveCompleted(_meshNumber);
            }
            _entityIndex = -1;
            _moveFrom = null;
            _moveStarted = false;
            _meshNumber = -1;
        }

        private void OnMoveCompleted(int meshNumber)
        {
            var handler = MoveCompleted;
            if (handler != null)
            {
                handler (this, new MeshMoveEventArgs(meshNumber, 0,0));
            }
        }

        void vppRender1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed
                || vppRender1.ActionMode != actionType.None
                || vppRender1.GetToolBar().Contains(RenderContextUtility.ConvertPoint(vppRender1.GetMousePosition(e))))
                return;

            if (_moveFrom == null)
                return;


            // if we found an entity and the left mouse button is down
            if (_entityIndex != -1 && e.LeftButton == MouseButtonState.Pressed)
            {

                if (!_moveStarted)
                {
                    var meshEntity = vppRender1.Entities[_entityIndex] as Mesh;
                    if (meshEntity != null && meshEntity.EntityData != null)
                    {
                        if (int.TryParse(meshEntity.EntityData.ToString(), out _meshNumber))
                        {
                            _moveStarted = true;

                            OnMoveStarted(_meshNumber, _moveFrom.X, _moveFrom.Y);
                        }
                    }
                }

                // current 3D point
                Point3D moveTo;

                vppRender1.ScreenToPlane(RenderContextUtility.ConvertPoint(vppRender1.GetMousePosition(e)), Plane.XY, out moveTo);

                Vector3D delta = Vector3D.Subtract(moveTo, _moveFrom);

                // sets start as current
                _moveFrom = moveTo;

                OnMove(_meshNumber, delta.X, delta.Y);

            }
        }

        private void OnMove(int meshNumber, double x, double y)
        {
            var handler = Move;
            if (handler != null)
                handler(this, new MeshMoveEventArgs(meshNumber, x, y));
        }

        private void OnMoveStarted(int meshNumber, double x, double y)
        {
            var handler = MoveStarted;
            if (handler != null)
                handler(this, new MeshMoveEventArgs(meshNumber, x, y));
        }


        void vppRender1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed 
                || vppRender1.ActionMode != actionType.None 
                || vppRender1.GetToolBar().Contains(RenderContextUtility.ConvertPoint(vppRender1.GetMousePosition(e)))
                )
                return;

            try
            {
                // gets the entity index
                _entityIndex =
                    vppRender1.GetEntityUnderMouseCursor(
                        RenderContextUtility.ConvertPoint(vppRender1.GetMousePosition(e)));

                if (_entityIndex < 0)

                    return;

                // gets 3D start point
                vppRender1.ScreenToPlane(RenderContextUtility.ConvertPoint(vppRender1.GetMousePosition(e)), Plane.XY,
                    out _moveFrom);
                _moveStarted = false;
                _meshNumber = -1;
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("Render Mouse down error", ex);
            }
        }


        void RenderMasterUc_Loaded(object sender, RoutedEventArgs e)
        {
            // Get PresentationSource
            PresentationSource presentationSource = PresentationSource.FromVisual((Visual)sender);

            // Subscribe to PresentationSource's ContentRendered event
            if (presentationSource != null)
                presentationSource.ContentRendered += presentationSource_ContentRendered;
        }

        void presentationSource_ContentRendered(object sender, EventArgs e)
        {
            // Don't forget to unsubscribe from the event
            ((PresentationSource)sender).ContentRendered -= presentationSource_ContentRendered;
            Loaded -= RenderMasterUc_Loaded;

            vppRender1.ZoomFit();
            vppRender1.Invalidate();

            var model = DataContext as RenderMasterViewModel;
            if (model != null)
            {
                model.ParentWindow = Window.GetWindow(this);
            }
        }

        void RenderMasterUc_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            dynamic vm = e.NewValue;
            if (vm != null)
            {
                _viewModel = vm.ProjectViewModel.RenderViewModel.RenderMasterViewModel;
                //_viewModel.View = this;
            }
        }

        #endregion Constructors  -----------<


            #region >-------------- Protected Fields



            #endregion Protected Fields --------<

            #region >-------------- Public Fields



            #endregion Public Fields --------<


            #region >-------------- Public Properties

        public RenderMasterViewModel ViewModel
        {
            get { return _viewModel; }
            set
            {
                AttachModelEvents();
                _viewModel = value;
                DetachModelEvents();
            }
        }

        public Window MainParent
        {
            get { return _mainParent; }
            set { _mainParent = value; }
        }

        #endregion Public Properties -----------<

            #region >-------------- Protected Properties



            #endregion Protected Properties --------<

            #region >-------------- Public Methods

        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalità grigio</param>
        /// <param name="paper">Formato carta</param>
        /// <param name="rasterPrint">Stampa raster</param>
        /// <param name="showBorder">Disegna bordo di tampa</param>
        /// <param name="showTitle">Visualizza titolo stampa</param>
        /// <param name="showDateTime">Visualizza data e ora</param>
        /// <param name="title">Titolo stampa</param>
        /// <param name="titlesFont">Font titoli stampa</param>
        //private void PrintPreview(ref bool landscape,
        //    ref int bottom,
        //    ref int top,
        //    ref int right,
        //    ref int left,
        //    ref bool blackwhite,
        //    ref string paper,
        //    bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont)
        //{
        //    PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left, blackwhite, paper);

        //    printDocument.Layout = vppRender1;

        //    printDocument.RasterPrint = rasterPrint;
        //    printDocument.ShowBorder = showBorder;
        //    printDocument.ShowTitle = showTitle;
        //    printDocument.ShowDateTime = showDateTime;
        //    printDocument.Title = title;

        //    if (titlesFont != null)
        //        printDocument.TitlesFont = titlesFont;

        //    printDocument.ShowPreview();

        //    // Save printer options
        //    landscape = printDocument.LandscapeOrientation;
        //    bottom = printDocument.MarginBottom;
        //    top = printDocument.MarginTop;
        //    right = printDocument.MarginRight;
        //    left = printDocument.MarginLeft;
        //    blackwhite = printDocument.BlackAndWhitePrint;
        //    paper = printDocument.PaperFormat;

        //    printDocument.Layout = null;

        //    printDocument = null;
        //}

        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalità grigio</param>
        /// <param name="paper">Formato carta</param>
        /// <param name="rasterPrint">Stampa raster</param>
        /// <param name="showBorder">Disegna bordo di tampa</param>
        /// <param name="showTitle">Visualizza titolo stampa</param>
        /// <param name="showDateTime">Visualizza data e ora</param>
        /// <param name="title">Titolo stampa</param>
        /// <param name="titlesFont">Font titoli stampa</param>
        //private void Print(ref bool landscape,
        //    ref int bottom,
        //    ref int top,
        //    ref int right,
        //    ref int left,
        //    ref bool blackwhite,
        //    ref string paper,
        //    bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont)
        //{
        //    PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left, blackwhite, paper);

        //    printDocument.Layout = vppRender1;

        //    printDocument.RasterPrint = rasterPrint;
        //    printDocument.ShowBorder = showBorder;
        //    printDocument.ShowTitle = showTitle;
        //    printDocument.ShowDateTime = showDateTime;
        //    printDocument.Title = title;

        //    if (titlesFont != null)
        //        printDocument.TitlesFont = titlesFont;

        //    printDocument.PrintDirect();

        //    // Save printer options
        //    landscape = printDocument.LandscapeOrientation;
        //    bottom = printDocument.MarginBottom;
        //    top = printDocument.MarginTop;
        //    right = printDocument.MarginRight;
        //    left = printDocument.MarginLeft;
        //    blackwhite = printDocument.BlackAndWhitePrint;
        //    paper = printDocument.PaperFormat;

        //    printDocument.Layout = null;

        //    printDocument = null;
        //}







        #region Disegno sagome
        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="shapes">Vettore di sagome, contiene le sagome da disegnare.</param>
        /// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="azzeraArea">Indica se l'area deve essere azzerata prima di disegnare.</param>
        //public void DrawTop(Shape[] shapes, Bitmap[] pathImage, string pathProfiliLav, bool azzeraArea)
        //{

        //    if (azzeraArea) vppRender1.Entities.Clear();

        //    vppRender1.OriginSymbol.Visible = false;

        //    //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
        //    render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca);

        //    vppRender1.ZoomFit();

        //}

        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="shapes">Vettore di sagome, contiene le sagome da disegnare.</param>
        /// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        //public void DrawTop(Shape[] shapes, Bitmap[] pathImage, string pathProfiliLav)
        //{

        //    vppRender1.Entities.Clear();
        //    vppRender1.Materials.Clear();

        //    vppRender1.OriginSymbol.Visible = false;

        //    //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
        //    render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca);

        //    vppRender1.ZoomFit();

        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="shapes"></param>
        /// <param name="pathImage"></param>
        /// <param name="pathProfiliLav"></param>
        //public void DrawTopOnFiles(Shape[] shapes, string[] pathImage, string pathProfiliLav)
        //{

        //    vppRender1.Entities.Clear();
        //    vppRender1.Materials.Clear();

        //    vppRender1.OriginSymbol.Visible = false;

        //    //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
        //    render.DrawTopOnFiles(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca);

        //    vppRender1.ZoomFit();
        //    vppRender1.Invalidate();
        //}




        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente non ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="pathXML">Vettore di percorsi, contiene i percorsi dei files XML da elaborare.</param>
        /// <param name="pathImage">ArrayList di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="azzeraArea">Indica se l'area deve essere azzerata prima di disegnare.</param>
        //public void DrawTop(string[] pathXML, Bitmap[] pathImage, string pathProfiliLav, bool azzeraArea)
        //{

        //    try
        //    {

        //        Shape[] shapes = new Shape[pathXML.Length];

        //        for (int i = 0; i < pathXML.Length; i++)
        //        {

        //            Shape tmp = new Shape();
        //            XmlAnalizer.ReadShapeXml(pathXML[i], ref tmp, true);
        //            shapes[i] = tmp;

        //        }

        //        DrawTop(shapes, pathImage, pathProfiliLav, azzeraArea);

        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("DrawTop Error. ", ex);
        //    }

        //}

        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente non ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="pathXML">Vettore di percorsi, contiene i percorsi dei files XML da elaborare.</param>
        /// <param name="pathImage">ArrayList di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        //public void DrawTop(string[] pathXML, Bitmap[] pathImage, string pathProfiliLav)
        //{

        //    try
        //    {

        //        Shape[] shapes = new Shape[pathXML.Length];

        //        for (int i = 0; i < pathXML.Length; i++)
        //        {

        //            Shape tmp = new Shape();
        //            XmlAnalizer.ReadShapeXml(pathXML[i], ref tmp, true);
        //            shapes[i] = tmp;

        //        }

        //        DrawTop(shapes, pathImage, pathProfiliLav);

        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("DrawTop Error. ", ex);
        //    }

        //}
        #endregion

        /// <summary>
        /// Procedura di modifica del materiale di una sagoma.
        /// </summary>
        /// <param name="shapeNumber">Vettore di interi, contiene gli indici delle Sagome.</param>
        /// <param name="pathImage">Vettore di Bitmap, contiene le singole immagini.</param>
        //public void ChangeMaterial(int[] shapeNumber, Bitmap[] pathImage)
        //{

        //    try
        //    {

        //        render.ChangeMaterial(shapeNumber, pathImage, vppRender1);

        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("ChangMaterial Error. ", ex);
        //    }

        //}

        /// <summary>
        /// Procedura di modifica del materiale di una sagoma.
        /// </summary>
        /// <param name="shapeNumber">id sagoma.</param>
        /// <param name="pathImage">immagine</param>
        //public void ChangeMaterial(int shapeNumber, Bitmap pathImage)
        //{

        //    try
        //    {

        //        render.ChangeMaterial(shapeNumber, pathImage, vppRender1);

        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("ChangeMaterial Error. ", ex);
        //    }

        //}

        #region Gestione Toolbar menù/buttons

        //private void DrawToolStrip()
        //{
        //    Color color1 = System.Drawing.SystemColors.Control;
        //    Color color2 = System.Drawing.SystemColors.ControlDark;
        //    Color color3 = System.Drawing.SystemColors.ControlLightLight;

        //    Bitmap bmp = new Bitmap(10, 45);

        //    Rectangle rc1 = Rectangle.Empty;
        //    rc1.Height = bmp.Height / 3;
        //    rc1.Width = bmp.Size.Width;
        //    Rectangle rc2 = Rectangle.Empty;
        //    rc2.Height = bmp.Height / 3;
        //    rc2.Width = bmp.Size.Width;
        //    rc2.Offset(0, rc1.Bottom + 1);
        //    Rectangle rc3 = Rectangle.Empty;
        //    rc3.Height = (bmp.Height / 3);
        //    rc3.Width = bmp.Size.Width;
        //    rc3.Offset(0, rc2.Bottom + 1);

        //    Rectangle line1 = Rectangle.Empty;
        //    line1.Height = 1;
        //    line1.Width = bmp.Size.Width;
        //    line1.Offset(0, 0);
        //    Rectangle line2 = Rectangle.Empty;
        //    line2.Height = 1;
        //    line2.Width = bmp.Size.Width;
        //    line2.Offset(0, 1);

        //    using (Graphics g = Graphics.FromImage(bmp))
        //    {
        //        using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color3, color1, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
        //            g.FillRectangle(b, rc1);
        //        using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color1, color1, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
        //            g.FillRectangle(b, rc2);
        //        using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color1, color2, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
        //            g.FillRectangle(b, rc3);
        //        using (Brush b = new System.Drawing.SolidBrush(System.Drawing.SystemColors.ControlDark))
        //            g.FillRectangle(b, line1);
        //        using (Brush b = new System.Drawing.SolidBrush(System.Drawing.SystemColors.ControlLight))
        //            g.FillRectangle(b, line2);
        //    }

        //    tlsMain.BackgroundImage = bmp;
        //    tlsMain.BackgroundImageLayout = ImageLayout.Stretch;

        //}

        #endregion

        #region protected methods for events


        /// <summary>
        /// Svuota il layout
        /// </summary>
        public void ClearLayout()
        {
            if (render != null)
            {
                render.ClearProfiles();
            }

            foreach (var ent in vppRender1.Entities)
            {
                ent.MaterialName = string.Empty;
            }
            vppRender1.Entities.ClearSelection();
            vppRender1.Entities.Clear();

            while (vppRender1.Materials.Count > 0)
            {
                var element = vppRender1.Materials.ElementAt(0);
                vppRender1.Materials.Remove(element.Key);
                element.Value.Dispose();
            }
            vppRender1.Layers.Clear(new Layer("Default"));

            //this.vppRender1.Clear();

            if (_renderWindow != null)
            {
                _renderWindow.ResetLayout();
            }
        }

        #endregion Public Methods -----------<

            #region >-------------- Protected Methods



            #endregion Protected Methods --------<

            #region >-------------- Private Methods

        private void DetachModelEvents()
        {
            if (_viewModel != null)
            {
                _viewModel.PropertyChanged -= _viewModel_PropertyChanged;
            }
        }

        private void AttachModelEvents()
        {
            if (_viewModel != null)
            {
                _viewModel.PropertyChanged += _viewModel_PropertyChanged;
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates

        void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                default:
                    break;
            }
        }


        //private void cmdZoomTutto_Click(object sender, EventArgs e)
        //{

        //    vppRender1.ZoomFit();
        //    vppRender1.Refresh();

        //}

        //private void cmdZoomFinestra_CheckedChanged(object sender, EventArgs e)
        //{

        //    bool bolCheck = cmdZoomFinestra.IsChecked.HasValue && (bool)cmdZoomFinestra.IsChecked;

        //    if (cmdZoomPan.IsChecked.HasValue && (bool)cmdZoomPan.IsChecked) cmdZoomPan.IsChecked = false;

        //    cmdZoomFinestra.IsChecked = bolCheck;

        //    if (bolCheck)
        //    {
        //        vppRender1.ActionMode = actionType.ZoomWindow;
        //    }
        //    else
        //    {
        //        vppRender1.ActionMode = actionType.None;
        //    }

        //}

        //private void cmdZoomIn_Click(object sender, EventArgs e)
        //{

        //    vppRender1.ZoomIn(25);
        //    vppRender1.Invalidate();

        //}

        //private void cmdZoomOut_Click(object sender, EventArgs e)
        //{

        //    vppRender1.ZoomOut(25);
        //    vppRender1.Invalidate();

        //}

        //private void cmdZoomPan_CheckedChanged(object sender, EventArgs e)
        //{

        //    bool bolCheck =  cmdZoomPan.IsChecked.HasValue && (bool)cmdZoomPan.IsChecked;
        //    if (cmdZoomFinestra.IsChecked.HasValue && (bool)cmdZoomFinestra.IsChecked) cmdZoomFinestra.IsChecked = false;
        //    cmdZoomPan.IsChecked = bolCheck;

        //    if (bolCheck)
        //    {

        //        vppRender1.ActionMode = actionType.Pan;

        //    }

        //    else
        //    {

        //        vppRender1.ActionMode = actionType.None;

        //    }

        //}

        // Gestione click destro per uscire dai comandi azione (Zoom Finestra, Pan)
        //private void vppRender_MouseClick(object sender, MouseEventArgs e)
        //{

        //    if (e.RightButton .Button.ToString() == "Right")
        //    {

        //        cmdZoomFinestra.Checked = false;
        //        cmdZoomPan.Checked = false;

        //        //vppRender1.Action = devDept.Eyeshot.Viewport.actionType.None;

        //    }

        //}


        private void cmdVistaIsometrica_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Isometric);
            vppRender1.Refresh();

        }

        private void cmdVistaFrontale_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Front);
            vppRender1.Refresh();

        }

        private void cmdVistaAlto_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Top);
            vppRender1.Refresh();

        }

        private void cmdVistaLaterale_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Left);
            vppRender1.Refresh();

        }

        private void cmdSalvaVista_Click(object sender, EventArgs e)
        {
            vppRender1.SaveView(out vista);
        }

        private void cmdRipristinaVista_Click(object sender, EventArgs e)
        {
            vppRender1.RestoreView(vista);
            vppRender1.Invalidate();

        }

        private void VppRender1_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Right)
            {

                ////cmdZoomFinestra.IsChecked = false;
                ////cmdZoomPan.IsChecked = false;

                //vppRender1.Action = devDept.Eyeshot.Viewport.actionType.None;

            }
        }

        private void Button1_OnClick(object sender, RoutedEventArgs e)
        {
            vppRender1.Viewports[0].DisplayMode = displayType.Wireframe;
            vppRender1.ShowCurveDirection = true;
            vppRender1.Invalidate();

        }

        private void Button2_OnClick(object sender, RoutedEventArgs e)
        {
            vppRender1.Viewports[0].DisplayMode = displayType.Rendered;
            vppRender1.Invalidate();
        }

        private void BtnShowNormals_OnClick(object sender, RoutedEventArgs e)
        {
            vppRender1.ShowNormals = !vppRender1.ShowNormals;
            vppRender1.Invalidate();
        }

        #endregion Delegates  -----------<

        //private void cmdPrintPreview_Click(object sender, EventArgs e)
            //{
            //    //vppRender1.PrintPreview(new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height));
            //    bool landscape = true, blackwhite = false;
            //    int bottom = 10, top = 10, left = 10, right = 10;
            //    string paper = "A4";

            //    PrintPreview(ref landscape, ref bottom, ref top, ref right, ref left, ref blackwhite, ref paper, true, false, false, false, string.Empty, this.Font);
            //}

            //private void cmdPrint_Click(object sender, EventArgs e)
            //{
            //    //vppRender1.PageSetup();
            //    //vppRender1.Print();
            //    bool landscape = true, blackwhite = false;
            //    int bottom = 10, top = 10, left = 10, right = 10;
            //    string paper = "A4";

            //    Print(ref landscape, ref bottom, ref top, ref right, ref left, ref blackwhite, ref paper, true, false, false, false, string.Empty, this.Font);
            //}

        //protected virtual void OnRenderDoubleClick()
            //{
            //    try
            //    {
            //        if (!WpfRenderWindow.gCreate)
            //        {
            //            MainParent = this.ParentForm;
            //            _renderWindow = WpfRenderWindow.Instance;
            //            _renderWindow.Init(Lingua, render, vppRender1);
            //            if (MainParent != null)
            //            {
            //                _renderWindow.Show(MainParent);
            //            }
            //            else
            //            {
            //                _renderWindow.Show();
            //            }

            //            _renderWindow.CreateContent();
            //        }
            //        else
            //        {
            //            if (_renderWindow != null)
            //            {
            //                if (_renderWindow.Visible)
            //                    _renderWindow.Activate();
            //                else
            //                    _renderWindow.Show();
            //            }
            //        }

            //    }
            //    catch (Exception ex)
            //    {
            //        TraceLog.WriteLine("OnRenderDoubleClick Error. ", ex);
            //    }
            //}

            #endregion
        }


}
