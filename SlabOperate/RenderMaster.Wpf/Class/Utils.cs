﻿using System;
using System.Collections.Generic;
using System.Text;
using devDept.Eyeshot.Entities;
using devDept.Geometry;

namespace RenderMaster.Class
{
    internal class Utils
    {
        public static int Fillet(double x0, double y0, double x1, double y1, double x2, double y2, double x3, double y3,
            double r, ref double xc, ref double yc, ref double p0x, ref double p0y, ref double p1x, ref double p1y,
            ref double ang0, ref double ang1)
        {
            double a1=0, b1=0, c1=0;
            double a2=0, b2=0, c2=0;
            double c1p, c2p;
            double d1, d2;
            double xa=0, xb=0, ya=0, yb=0, d, rr;
            double mpx, mpy, pcx, pcy;

            Linecoef(ref a1, ref b1, ref c1, x0, y0, x1, y1);
            Linecoef(ref a2, ref b2, ref c2, x2, y2, x3, y3);

            if ((a1 * b2) == (a2 * b1)) /* Parallel or coincident lines */
            {

                return -1;

            }

            mpx = (x2 + x3) / 2;
            mpy = (y2 + y3) / 2;

            d1 = Linetopoint(a1, b1, c1, mpx, mpy); /* Find distance p1p2 to p3 */

            if (d1 == 0.0)
            {

                return -2;

            }

            mpx = (x0 + x1) / 2;
            mpy = (y0 + y1) / 2;

            d2 = Linetopoint(a2, b2, c2, mpx, mpy); /* Find distance p3p4 to p2 */

            if (d2 == 0.0)
            {

                return -3;

            }

            rr = r;

            if (d1 <= 0)
            {

                rr = -rr;

            }

            c1p = c1 - rr * Math.Sqrt((a1 * a1) + (b1 * b1)); /* Line parallel l1 at d */

            rr = r;

            if (d2 <= 0.0)
            {

                rr = -rr;

            }

            c2p = c2 - rr * Math.Sqrt((a2 * a2) + (b2 * b2)); /* Line parallel l2 at d */
            d = a1 * b2 - a2 * b1;
            xc = (c2p * b1 - c1p * b2) / d; /* Intersect constructed lines */
            yc = (c1p * a2 - c2p * a1) / d; /* to find center of arc */
            pcx = xc;
            pcy = yc;

            Pointperp(ref xa, ref ya, a1, b1, c1, pcx, pcy); /* Clip or extend lines as required */
            Pointperp(ref xb, ref yb, a2, b2, c2, pcx, pcy);

            p0x = xa;
            p0y = ya;

            p1x = xb;
            p1y = yb;

            double gv1x = xa - xc;
            double gv1y = ya - yc;
            double gv2x = xb - xc;
            double gv2y = yb - yc;

            ang0 = GetGradi(Math.Atan2(gv1y, gv1x));
            ang1 = Dot2(gv1x, gv1y, gv2x, gv2y);

            if (Cross2(gv1x, gv1y, gv2x, gv2y) < 0)
            {

                ang1 = -ang1;

            }

            ang1 = GetGradi(ang1);

            if (ang0 < 0)
            {

                ang0 = ang0 + 360;
            }

            return 0;

        }

        public static double GetGradi(double radianti)
        {
            return radianti * 180 / Math.PI;
        }

        public static double Cross2(double v1x, double v1y, double v2x, double v2y)
        {
            return (v1x * v2y - v2x * v1y);
        }

        public static double Dot2(double v1x, double v1y, double v2x, double v2y)
        {
            double d, t;

            d = Math.Sqrt(((v1x * v1x) + (v1y * v1y)) * ((v2x * v2x) + (v2y * v2y)));

            if (d != 0)
            {

                t = (v1x * v2x + v1y * v2y) / d;

                return Math.Acos(t);//arccos(t);

            }

            else
            {

                return 0;

            }
           
        }

        public static double Linetopoint(double a, double b, double c, double x, double y)
        {
            double d, lp;

            d = Math.Sqrt((a * a) + (b * b));

            if (d == 0.0)
            {

                lp = 0.0;

            }

            else
            {

                lp = (a * x + b * y + c) / d;

            }

            return lp;
            
        }

        public static void Pointperp(ref double x, ref double y, double a, double b, double c, double x1, double y1)
        {
            double d, cp;

            x = 0.0;
            y = 0.0;
            d = a * a + b * b;
            cp = a * y1 - b * x1;

            if (d != 0.0)
            {

                x = (-a * c - b * cp) / d;
                y = (a * cp - b * c) / d;

            }
            
        }

        public static void Linecoef(ref double a, ref double b, ref double c, double x0, double y0, double x1, double y1)
        {
            a = y1 - y0;
            b = x0 - x1;
            c = (x1 * y0) - (x0 * y1);
            
        }

        public static double GetAngolo(double x0, double y0, double x1, double y1)
        {
            if (x0 == x1)
            {

                if (y0 < y1)
                {

                    return 90;

                }

                else
                {

                    return 270;

                }

            }

            else if (y0 == y1)
            {

                if (x0 < x1)
                {

                    return 0;

                }

                else
                {

                    return 180;

                }

            }

            else
            {

                //double angolo = GetGradi(Math.Abs(y1 - y0) / Math.Abs(x1 - x0));

                if (y0 < y1)
                {

                    if (x0 < x1)
                    {

                        return 90 - GetGradi(Math.Atan(Math.Abs(x1 - x0) / Math.Abs(y1 - y0)));

                    }

                    else
                    {

                        return 180 - GetGradi(Math.Atan(Math.Abs(y1 - y0) / Math.Abs(x1 - x0)));

                    }

                }

                else
                {

                    if (x0 < x1)
                    {

                        return 360 - GetGradi(Math.Atan(Math.Abs(y1 - y0) / Math.Abs(x1 - x0)));

                    }

                    else
                    {

                        return 270 - GetGradi(Math.Atan(Math.Abs(x1 - x0) / Math.Abs(y1 - y0)));

                    }

                }

            }
        }

        public static bool IsClockwiseOriented(Arc arc)
        {
            LinearPath path = new LinearPath(new Point3D[] {arc.StartPoint, arc.MidPoint, arc.EndPoint, arc.StartPoint});
            return path.IsOrientedClockwise(Plane.XY);
        }
    }
}
