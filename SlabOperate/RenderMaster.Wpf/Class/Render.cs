using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Threading;
using System.Windows.Forms.VisualStyles;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Eyeshot.Translators;
using devDept.Geometry;
using devDept.Graphics;
using Breton.DesignUtil;
using RenderMaster.Class;
using RenderMaster.Wpf.Class;
using TraceLoggers;
using Wpf.Common;

namespace RenderMaster.Wpf
{
    
    /// <summary>
    /// Classe contenente le funzionalit� di Render su un oggetto EyeShot
    /// </summary>
    public class Render
    {
        #region ----------> Rendering progress
        public event EventHandler<EventArgs<object>> MaterialChanged;

        public event EventHandler<EventArgs<int>> TopRenderStarted;
        public event EventHandler<EventArgs<int>> TopRenderProgress;
        public event EventHandler<EventArgs> TopRenderCompleted;

        private int _totalTopRenderSteps = 10;
        private int _currentTopRenderStep = 0;


        private int CurrentTopRenderStep
        {
            get { return _currentTopRenderStep; }
            set
            {
                _currentTopRenderStep = value;
                OnTopRenderProgress(_currentTopRenderStep);
            }
        }

        #region Events raising

        private void OnTopRenderStarted(int steps)
        {
            var handler = TopRenderStarted;
            if (handler != null)
            {
                handler(null, new EventArgs<int>(steps));
            }
        }

        private void OnTopRenderProgress(int step)
        {
            var handler = TopRenderProgress;
            if (handler != null)
            {

                handler(null, new EventArgs<int>(step <= _totalTopRenderSteps ? step : _totalTopRenderSteps));
            }
        }

        private void OnTopRenderCompleted()
        {
            CurrentTopRenderStep = _totalTopRenderSteps;

            var handle = TopRenderCompleted;
            if (handle != null)
            {
                handle(this, new EventArgs());
            }
        }

        private void OnMaterialChanged(List<object> objects)
        {
            var handler = MaterialChanged;
            if (handler != null)
            {
                handler(this, new EventArgs<object>(objects));
            }
        }


        #endregion

        #endregion

        #region Variables
		private const double ANGLE_TOOLERANCE = 3;

		public const double COMPARISON_TOLERANCE = 1e-4;

        public const double  CHORDAL_ERROR = 1e-2;

        public const double  MESH_CHORDAL_ERROR = 1e-2;

        public const int TEXTURE_SIZE = 1024;

        // De Conti 20170310
        /// <summary>
        /// Determina il rapporto di risoluzione in pixel della texture impiegata nel render del singolo pezzo
        /// rispetto alle dimensioni originali della porzione del bitmap della lastra interessata dal pezzo
        /// Quindi:
        ///     Valore = 1.0 ==> Rapporto 1:1
        ///     Valore minore di 1.0 ==> Downsampling del bitmap
        ///     Valore maggiore di 1.0 ==> Oversampling del bitmap
        /// Non sono ammessi valori inferiori o uguali a 0
        /// </summary>
        public const double TEXTURE_SIZE_RATIO = 1.0;



        Dictionary<string, CompositeCurve> dxfProfiles = new Dictionary<string, CompositeCurve>();


        struct ArrL
        {
            public int indice;                                      // Indice di inizio nuova figura nell' ArrayList al.
            public string tipo;                                     // Tipi di Sagoma:
                                                                    // Sagoma Principale (SP).
                                                                    // Sagoma Figlia (Codice della sagoma).
            public string sottoTipo;                                // Sotto tipo per la sagoma figlia

            public int cw;                                          // Direzione della sagoma:
                                                                    // Sagoma Principale 0.
                                                                    // Sagoma Figlia (1).

            public double spessore;                                 // Spessore della sagoma.
            public bool passante;                                   // Sagoma passante S/N (nel caso di Sagome Figlie).
            public string insertMode;                               // Modalit� di inserimento fori lavabo
        };

        struct ArrLineeCan
        {

            public Point3D puntoOr;                                // Punto della linea originale
            public Point3D punto1;                                 // Punto della linea a +5.
            public Point3D punto2;                                 // Punto della linea a -5.

        };

        #endregion

        private struct shapeTmp
        {
            public Mesh rMesh;
            public string veletta;
            public List<string> velette;
        };




        #region Disegno sagome

        /// <summary>
        /// Procedura di disegno delle sagome.
        /// </summary>
        /// <param name="shapes">Vettore di sagome, contiene le sagome da disegnare.</param>
        /// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="profonditaRibasso">Profondit� del ribasso.</param>
        /// <param name="profonditaCanaletti">Profondit� del canaletto.</param>
        /// <param name="profonditaBussole">Profondit� della bussola.</param>
        /// <param name="profonditaTasca">Profondit� della tasca.</param>
        /// <param name="prbMain">ProgressBar utilizzata per mostrare l'avanzamento nel caricamento del disegno a video.</param>
        internal void DrawTop(Shape[] shapes, Bitmap[] pathImage, string pathProfiliLav, ViewportLayout vppRender, double profonditaRibasso, double profonditaCanaletti, double profonditaBussole, double profonditaTasca)//, System.Windows.Forms.ToolStripProgressBar prbMain)
        {
            _totalTopRenderSteps = shapes.Length * 14;
            OnTopRenderStarted(_totalTopRenderSteps);
            //prbMain.Value = 0;
            //prbMain.Maximum = shapes.Length * 14;                                   // Numero di sagome * numero di conteggi
            //prbMain.Visible = true;

            double profonditaT = profonditaTasca;
            double profonditaR = profonditaRibasso;
            double profonditaC = profonditaCanaletti;
            double profonditaB = profonditaBussole;

            try
            {
                //elimino i contour non utilizzati (tipo Symbol)
                for (int intS = 0; intS < shapes.Length; intS++)
                {

                    Shape shape = shapes[intS];

                    for (int intI = shape.gContours.Count - 1; intI >= 0; intI--)
                    {
                        if (((Shape.Contour) shape.gContours[intI]).code == "Symbols")
                        {
                            shape.gContours.RemoveAt(intI);
                        }
                    }
                }

                //for (int intS = 0; intS < shapes.Length; intS++)
                //    InsertFillet(shapes[intS]);

                #region  Ciclo per tutte le sagome per tenere in memoria lo spessore per ogni sagoma.

                Dictionary<int, double> dSpessori = new Dictionary<int, double>();
                    // Per ogni sagoma salvo id e spessore

                for (int intS = 0; intS < shapes.Length; intS++)
                {

                    try
                    {

                        dSpessori.Add(shapes[intS].gId, shapes[intS].gThickness);

                    }

                    catch
                    {
                    }

                }

                #endregion

                #region  Ciclo per tutte le sagome.

                List<shapeTmp> lstObjects = new List<shapeTmp>();

                Dictionary<string, string> dicVelette = new Dictionary<string, string>();

                for (int intS = 0; intS < shapes.Length; intS++)
                {
                    //if (intS != 7) continue;

                    ArrayList al = new ArrayList(); // ArrayList che contiene le entit� per ogni sagoma (Shape).
                    ArrayList alUp = new ArrayList();
                        // ArrayList che contiene le entit� per ogni sagoma (Shape) avente velette.
                    ArrayList alIndice = new ArrayList(); // ArrayList che contiene gli ArrL.
                    ArrayList alIndiceUp = new ArrayList(); // ArrayList che contiene gli ArrL.

                    ArrayList alBussole = new ArrayList(); // ArrayList che contiene le entit� per ogni Bussola.
                    ArrayList alIndiceBussole = new ArrayList(); // ArrayList che contiene gli ArrL.
                    ArrayList alCanaletti = new ArrayList(); // ArrayList che contiene gli array dei canaletti.
                    bool bolBussola = false;
                    bool bolTasca = false;
                    bool bolRibasso = false;
                    //double angoloPrimoElemento = -361;

                    Shape shape = new Shape(shapes[intS]);

                    int id = -1;
                    int idUp = -1;
                    //se la sagoma ha meno di 4 elementi e ci sono dei segmenti non funziona
                    //con il cerchio s�. verifico quindi se � una sagoma di soli archi
                    bool bolElabora = true;
					//if (shape.gContours.Count <= 3)
					//{
					//    for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
					//    {
					//        if (((Shape.Contour) shape.gContours[intI]).line)
					//        {
					//            bolElabora = false;
					//            break;
					//        }
					//    }
					//}

                    //if (shape.gContours.Count > 3)
                    if (bolElabora)
                    {
                        Color coloreSagoma = new Color(); // Colore di disegno delle Sagome.

                        Line lAttacco = CreateEyeLine(0, 0, 1, 1, Color.Red);
                            // Linea di collegamento tra sagome utilizzata per il BackSplash

                        #region Ciclo per ogni contorno delle sagome.

                        //if (shape.IsVeletta)
                        //{
                        //    Shape ShapeDuplicated = new Shape();
                        //    Shape.Contour prevContour;
                        //    Shape.Contour nextContour;

                        //    coloreSagoma = Color.Black;

                        //    for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                        //    {

                        //        Shape.Contour contour = (Shape.Contour)shape.gContours[intI];
                        //        Shape.Contour contourDuplicated = new Shape.Contour();

                        //        contourDuplicated.x0 = contour.x0;
                        //        contourDuplicated.x1 = contour.x1;
                        //        contourDuplicated.y0 = contour.y0;
                        //        contourDuplicated.y1 = contour.y1;
                        //        contourDuplicated.z0 = contour.z0;
                        //        contourDuplicated.z1 = contour.z1;

                        //        if (intI == 0)
                        //        {
                        //            prevContour = (Shape.Contour)shape.gContours[shape.gContours.Count - 1];
                        //            nextContour = (Shape.Contour)shape.gContours[intI + 1];
                        //        }
                        //        else if (intI == shape.gContours.Count - 1)
                        //        {
                        //            prevContour = (Shape.Contour)shape.gContours[intI - 1];
                        //            nextContour = (Shape.Contour)shape.gContours[0];
                        //        }
                        //        else
                        //        {
                        //            prevContour = (Shape.Contour)shape.gContours[intI - 1];
                        //            nextContour = (Shape.Contour)shape.gContours[intI + 1];
                        //        }

                        //        if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 || prevContour.cutAngle_Offset != 0)
                        //        {
                        //            if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                        //                (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                        //            {
                        //                contourDuplicated.z0 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //            else
                        //            {
                        //                contourDuplicated.z1 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //        }
                        //        else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 || nextContour.cutAngle_Offset != 0)
                        //        {
                        //            if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                        //                (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                        //            {
                        //                contourDuplicated.z0 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //            else
                        //            {
                        //                contourDuplicated.z1 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //        }
                        //        else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 || contour.cutAngle_Offset != 0)
                        //        {
                        //            contourDuplicated.z0 += shape.gThickness;// shape.gVeletta.height;
                        //            contourDuplicated.z1 += shape.gThickness;// shape.gVeletta.height;
                        //        }
                        //        else
                        //        {
                        //            down = true;
                        //        }

                        //        DrawLine(contourDuplicated, coloreSagoma, ref al, shape.gAngle);

                        //        if (al.Count != 1)
                        //        {
                        //            if (id != contour.id)
                        //            {

                        //                ArrL newArrL = new ArrL();
                        //                newArrL.indice = al.Count - 1;

                        //                if (contour.pricipalStruct)
                        //                {

                        //                    newArrL.tipo = "PS";
                        //                    newArrL.cw = 0;
                        //                    newArrL.spessore = shape.gVeletta.height;
                        //                    newArrL.passante = true;

                        //                }

                        //                alIndice.Add(newArrL);

                        //                id = contour.id;

                        //            }
                        //        }

                        //        else
                        //        {
                        //            ArrL newArrL = new ArrL();
                        //            newArrL.indice = 0;

                        //            if (contour.pricipalStruct)
                        //            {

                        //                newArrL.tipo = "PS";// contour.pricipalStruct ? "PS" : contour.code;
                        //                newArrL.cw = 0;
                        //                newArrL.spessore = shape.gThickness + shape.gVeletta.height;
                        //                newArrL.passante = true;

                        //            }

                        //            alIndice.Add(newArrL);

                        //            id = contour.id;
                        //        }
                        //    }
                        //}

                        List<string> lstVelette = new List<string>();
                        Shape.Contour prevContour;
                        Shape.Contour nextContour;

                        ArrayList upperContour = new ArrayList();

                        if (shape.HasVeletta)
                        {
                            for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                            {
                                Shape.Contour contour = (Shape.Contour) shape.gContours[intI];
                                Shape.Contour contourTmp = new Shape.Contour();
                                contourTmp.apronLink = contour.apronLink;
                                contourTmp.code = contour.code;
                                contourTmp.cutAngle_Angle = contour.cutAngle_Angle;
                                contourTmp.cutAngle_Height = contour.cutAngle_Height;
                                contourTmp.cutAngle_Offset = contour.cutAngle_Offset;
                                contourTmp.cw = contour.cw;
                                contourTmp.endAngle = contour.endAngle;
                                contourTmp.id = contour.id;
                                contourTmp.idContour = contour.idContour;
                                contourTmp.insertMode = contour.insertMode;
                                contourTmp.join = contour.join;
                                contourTmp.line = contour.line;
                                contourTmp.pricipalStruct = contour.pricipalStruct;
                                contourTmp.processing = contour.processing;
                                contourTmp.r = contour.r;
                                contourTmp.startAngle = contour.startAngle;
                                contourTmp.straightLine = contour.straightLine;
                                contourTmp.subType = contour.subType;
                                contourTmp.x0 = contour.x0;
                                contourTmp.x1 = contour.x1;
                                contourTmp.xC = contour.xC;
                                contourTmp.y0 = contour.y0;
                                contourTmp.y1 = contour.y1;
                                contourTmp.yC = contour.yC;
                                contourTmp.z0 = contour.z0;
                                contourTmp.z1 = contour.z1;
                                contourTmp.zC = contour.zC;
                                upperContour.Add(contourTmp);
                            }
                        }

                        for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                        {

                            Shape.Contour contour = (Shape.Contour) shape.gContours[intI];

                            #region HasVeletta

                            if (shape.HasVeletta)
                            {
                                double x0Par = 0;
                                double y0Par = 0;
                                double x1Par = 0;
                                double y1Par = 0;

                                if (intI == 0)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[shape.gContours.Count - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }
                                else if (intI == shape.gContours.Count - 1)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[0];
                                }
                                else
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }

                                if (contour.apronLink != "")
                                {
                                    FindParallelLine(ref x0Par, ref y0Par, ref x1Par, ref y1Par, contour.x0, contour.y0,
                                        contour.x1, contour.y1, shape.gThickness);
                                    double angolo = Utils.GetAngolo(contour.x0, contour.y0, contour.x1, contour.y1);

                                    while (angolo >= 360)
                                    {
                                        angolo -= 360;
                                    }

                                    //dicVelette.Add(contour.apronLink, contour.x0.ToString() + "#" + contour.y0.ToString() + "#" + contour.x1.ToString() + "#" + contour.y1.ToString() + "#" + angolo.ToString() + "#" + shape.gThickness.ToString());
                                    dicVelette.Add(((Shape.Contour) upperContour[intI]).apronLink,
                                        ((Shape.Contour) upperContour[intI]).x0.ToString() + "#" +
                                        ((Shape.Contour) upperContour[intI]).y0.ToString() + "#" +
                                        ((Shape.Contour) upperContour[intI]).x1.ToString() + "#" +
                                        ((Shape.Contour) upperContour[intI]).y1.ToString() + "#" + angolo.ToString() +
                                        "#" + shape.gThickness.ToString());

                                    if (nextContour.line)
                                    {
                                        if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                        {
                                            nextContour.x0 = x0Par;
                                            nextContour.y0 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                        {
                                            nextContour.x0 = x1Par;
                                            nextContour.y0 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                        {
                                            nextContour.x1 = x0Par;
                                            nextContour.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                        {
                                            nextContour.x1 = x1Par;
                                            nextContour.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        if (intI == 0)
                                        {
                                            shape.gContours[intI + 1] = nextContour;
                                        }
                                        else if (intI == shape.gContours.Count - 1)
                                        {
                                            shape.gContours[0] = nextContour;
                                            ;
                                        }
                                        else
                                        {
                                            shape.gContours[intI + 1] = nextContour;
                                            ;
                                        }

                                        shape.gContours[intI] = contour;
                                    }
                                    else
                                    {

                                        Shape.Contour contourToAddUp = new Shape.Contour();
                                        contourToAddUp.line = true;
                                        contourToAddUp.apronLink = "";
                                        contourToAddUp.processing = "";
                                        contourToAddUp.pricipalStruct = contour.pricipalStruct;
                                        contourToAddUp.code = contour.code;

                                        Shape.Contour contourToAdd = new Shape.Contour();
                                        contourToAdd.line = true;
                                        contourToAdd.apronLink = "";
                                        contourToAdd.processing = "";
                                        contourToAdd.pricipalStruct = contour.pricipalStruct;
                                        contourToAdd.code = contour.code;

                                        if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                        {
                                            contourToAddUp.x0 = nextContour.x0;
                                            contourToAddUp.y0 = nextContour.y0;
                                            contourToAddUp.x1 = nextContour.x0;
                                            contourToAddUp.y1 = nextContour.y0;

                                            contourToAdd.x0 = nextContour.x0;
                                            contourToAdd.y0 = nextContour.y0;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                        {
                                            contourToAddUp.x0 = nextContour.x0;
                                            contourToAddUp.y0 = nextContour.y0;
                                            contourToAddUp.x1 = nextContour.x0;
                                            contourToAddUp.y1 = nextContour.y0;

                                            contourToAdd.x1 = nextContour.x0;
                                            contourToAdd.y1 = nextContour.y0;
                                            contourToAdd.x0 = x1Par;
                                            contourToAdd.y0 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                        {
                                            contourToAddUp.x0 = nextContour.x1;
                                            contourToAddUp.y0 = nextContour.y1;
                                            contourToAddUp.x1 = nextContour.x1;
                                            contourToAddUp.y1 = nextContour.y1;

                                            contourToAdd.x0 = nextContour.x1;
                                            contourToAdd.y0 = nextContour.y1;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                        {
                                            contourToAddUp.x0 = nextContour.x1;
                                            contourToAddUp.y0 = nextContour.y1;
                                            contourToAddUp.x1 = nextContour.x1;
                                            contourToAddUp.y1 = nextContour.y1;

                                            contourToAdd.x0 = nextContour.x1;
                                            contourToAdd.y0 = nextContour.y1;
                                            contourToAdd.x1 = x1Par;
                                            contourToAdd.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        shape.gContours[intI] = contour;
                                        shape.gContours.Insert(intI + 1, contourToAdd);
                                        upperContour.Insert(intI + 1, contourToAddUp);
                                    }

                                    if (prevContour.line)
                                    {
                                        if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                        {
                                            prevContour.x0 = x0Par;
                                            prevContour.y0 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                        {
                                            prevContour.x0 = x1Par;
                                            prevContour.y0 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                        {
                                            prevContour.x1 = x0Par;
                                            prevContour.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                        {
                                            prevContour.x1 = x1Par;
                                            prevContour.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        if (intI == 0)
                                        {
                                            shape.gContours[shape.gContours.Count - 1] = prevContour;
                                        }
                                        else if (intI == shape.gContours.Count - 1)
                                        {
                                            shape.gContours[intI - 1] = prevContour;
                                        }
                                        else
                                        {
                                            shape.gContours[intI - 1] = prevContour;
                                        }
                                        shape.gContours[intI] = contour;
                                    }
                                    else
                                    {
                                        Shape.Contour contourToAddUp = new Shape.Contour();
                                        contourToAddUp.line = true;
                                        contourToAddUp.apronLink = "";
                                        contourToAddUp.processing = "";
                                        contourToAddUp.pricipalStruct = contour.pricipalStruct;
                                        contourToAddUp.code = contour.code;

                                        Shape.Contour contourToAdd = new Shape.Contour();
                                        contourToAdd.line = true;
                                        contourToAdd.apronLink = "";
                                        contourToAdd.processing = "";
                                        contourToAdd.pricipalStruct = contour.pricipalStruct;
                                        contourToAdd.code = contour.code;

                                        if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                        {
                                            contourToAddUp.x0 = prevContour.x0;
                                            contourToAddUp.y0 = prevContour.y0;
                                            contourToAddUp.x1 = prevContour.x0;
                                            contourToAddUp.y1 = prevContour.y0;

                                            contourToAdd.x0 = prevContour.x0;
                                            contourToAdd.y0 = prevContour.y0;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                        {
                                            contourToAddUp.x0 = prevContour.x1;
                                            contourToAddUp.y0 = prevContour.y1;
                                            contourToAddUp.x1 = prevContour.x1;
                                            contourToAddUp.y1 = prevContour.y1;

                                            contourToAdd.x0 = prevContour.x1;
                                            contourToAdd.y0 = prevContour.y1;
                                            contourToAdd.x1 = x1Par;
                                            contourToAdd.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                        {
                                            contourToAddUp.x0 = prevContour.x0;
                                            contourToAddUp.y0 = prevContour.y0;
                                            contourToAddUp.x1 = prevContour.x0;
                                            contourToAddUp.y1 = prevContour.y0;

                                            contourToAdd.x0 = prevContour.x0;
                                            contourToAdd.y0 = prevContour.y0;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                        {
                                            contourToAddUp.x0 = prevContour.x1;
                                            contourToAddUp.y0 = prevContour.y1;
                                            contourToAddUp.x1 = prevContour.x1;
                                            contourToAddUp.y1 = prevContour.y1;

                                            contourToAdd.x0 = prevContour.x1;
                                            contourToAdd.y0 = prevContour.y1;
                                            contourToAdd.x1 = x1Par;
                                            contourToAdd.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        shape.gContours[intI] = contour;
                                        shape.gContours.Insert(intI, contourToAdd);
                                        upperContour.Insert(intI + 1, contourToAddUp);
                                    }
                                }
                            }
                                #endregion

                                #region IsVeletta

                            else if (shape.IsVeletta)
                            {
                                if (dicVelette.ContainsKey(shape.gVeletta.link))
                                {
                                    string valTmp = dicVelette[shape.gVeletta.link];
                                    string[] valori = valTmp.Split('#');
                                    double x0Tmp = 0;
                                    double y0Tmp = 0;
                                    double x1Tmp = 0;
                                    double y1Tmp = 0;
                                    double angoloTmp = 0;
                                    double.TryParse(valori[0], out x0Tmp);
                                    double.TryParse(valori[1], out y0Tmp);
                                    double.TryParse(valori[2], out x1Tmp);
                                    double.TryParse(valori[3], out y1Tmp);
                                    double.TryParse(valori[4], out angoloTmp);

                                    Transformation transformation = new Transformation();
                                    transformation.Rotation(Utility.DegToRad(angoloTmp), Vector3D.AxisZ);
                                    ////transformation.RotateZ(angoloTmp);

                                    Point3D p0 = new Point3D(contour.x0, contour.y0);
                                    Point3D p1 = new Point3D(contour.x1, contour.y1);

                                    ////transformation.Apply(ref p0);
                                    ////transformation.Apply(ref p1);
                                    p0 = transformation*p0;
                                    p1 = transformation*p1;

                                    ////transformation.Reset();
                                    transformation.Identity();

                                    ////transformation.Translate(x0Tmp, y0Tmp, 0);
                                    transformation.Translation(x0Tmp, y0Tmp, 0);

                                    ////transformation.Apply(ref p0);
                                    ////transformation.Apply(ref p1);
                                    p0 = transformation*p0;
                                    p1 = transformation*p1;

                                    contour.x0 = p0.X;
                                    contour.x1 = p1.X;
                                    contour.y0 = p0.Y;
                                    contour.y1 = p1.Y;

                                    shape.gContours[intI] = contour;

                                }

                                if (intI == 0)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[shape.gContours.Count - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }
                                else if (intI == shape.gContours.Count - 1)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[0];
                                }
                                else
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }

                                if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 ||
                                    prevContour.cutAngle_Offset != 0)
                                {
                                    if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                                        (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                                    {
                                        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                    else
                                    {
                                        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                }
                                else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 ||
                                         nextContour.cutAngle_Offset != 0)
                                {
                                    if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                                        (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                                    {
                                        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                    else
                                    {
                                        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                }
                                else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 ||
                                         contour.cutAngle_Offset != 0)
                                {
                                    contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                }
                                else
                                {
                                    //double x0Par = 0;
                                    //double y0Par = 0;
                                    //double x1Par = 0;
                                    //double y1Par = 0;

                                    //FindParallelLine(ref x0Par, ref y0Par, ref x1Par, ref y1Par, contour.x0, contour.y0, contour.x1, contour.y1, shape.gVeletta.height - shape.gThickness);

                                    //if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                    //{
                                    //    nextContour.x0 = x0Par;
                                    //    nextContour.y0 = y0Par;
                                    //    contour.x0 = x0Par;
                                    //    contour.y0 = y0Par;
                                    //}
                                    //else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                    //{
                                    //    nextContour.x0 = x1Par;
                                    //    nextContour.y0 = y1Par;
                                    //    contour.x1 = x1Par;
                                    //    contour.y1 = y1Par;
                                    //}
                                    //else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                    //{
                                    //    nextContour.x1 = x0Par;
                                    //    nextContour.y1 = y0Par;
                                    //    contour.x0 = x0Par;
                                    //    contour.y0 = y0Par;
                                    //}
                                    //else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                    //{
                                    //    nextContour.x1 = x1Par;
                                    //    nextContour.y1 = y1Par;
                                    //    contour.x1 = x1Par;
                                    //    contour.y1 = y1Par;
                                    //}

                                    //if (intI == 0)
                                    //{
                                    //    shape.gContours[intI + 1] = nextContour;
                                    //}
                                    //else if (intI == shape.gContours.Count - 1)
                                    //{
                                    //    shape.gContours[0] = nextContour; ;
                                    //}
                                    //else
                                    //{
                                    //    shape.gContours[intI + 1] = nextContour; ;
                                    //}

                                    //if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                    //{
                                    //    prevContour.x0 = x0Par;
                                    //    prevContour.y0 = y0Par;
                                    //    contour.x0 = x0Par;
                                    //    contour.y0 = y0Par;
                                    //}
                                    //else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                    //{
                                    //    prevContour.x0 = x1Par;
                                    //    prevContour.y0 = y1Par;
                                    //    contour.x1 = x1Par;
                                    //    contour.y1 = y1Par;
                                    //}
                                    //else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                    //{
                                    //    prevContour.x1 = x0Par;
                                    //    prevContour.y1 = y0Par;
                                    //    contour.x0 = x0Par;
                                    //    contour.y0 = y0Par;
                                    //}
                                    //else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                    //{
                                    //    prevContour.x1 = x1Par;
                                    //    prevContour.y1 = y1Par;
                                    //    contour.x1 = x1Par;
                                    //    contour.y1 = y1Par;
                                    //}

                                    //if (intI == 0)
                                    //{
                                    //    shape.gContours[shape.gContours.Count - 1] = prevContour;
                                    //}
                                    //else if (intI == shape.gContours.Count - 1)
                                    //{
                                    //    shape.gContours[intI - 1] = prevContour;
                                    //}
                                    //else
                                    //{
                                    //    shape.gContours[intI - 1] = prevContour;
                                    //}

                                    //down = true;
                                }

                            }

                            #endregion
                        }

                        if (shape.IsVeletta)
                        {
                            for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                            {

                                Shape.Contour contour = (Shape.Contour) shape.gContours[intI];

                                if (intI == 0)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[shape.gContours.Count - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }
                                else if (intI == shape.gContours.Count - 1)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[0];
                                }
                                else
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }

                                if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 ||
                                    prevContour.cutAngle_Offset != 0)
                                {
                                    if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                                        (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                                    {
                                        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                    else
                                    {
                                        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                    }

                                    shape.gContours[intI] = contour;

                                }
                                else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 ||
                                         nextContour.cutAngle_Offset != 0)
                                {
                                    if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                                        (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                                    {
                                        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                    else
                                    {
                                        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                    }

                                    shape.gContours[intI] = contour;
                                }
                                else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 ||
                                         contour.cutAngle_Offset != 0)
                                {
                                    contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    contour.z1 += shape.gThickness; // shape.gVeletta.height;

                                    shape.gContours[intI] = contour;
                                }
                                else
                                {
                                    double x0Par = 0;
                                    double y0Par = 0;
                                    double x1Par = 0;
                                    double y1Par = 0;

                                    FindParallelLine(ref x0Par, ref y0Par, ref x1Par, ref y1Par, contour.x0,
                                        contour.y0, contour.x1, contour.y1,
                                        shape.gVeletta.height - shape.gThickness);

                                    if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                    {
                                        nextContour.x0 = x0Par;
                                        nextContour.y0 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                    {
                                        nextContour.x0 = x1Par;
                                        nextContour.y0 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }
                                    else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                    {
                                        nextContour.x1 = x0Par;
                                        nextContour.y1 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                    {
                                        nextContour.x1 = x1Par;
                                        nextContour.y1 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }

                                    if (intI == 0)
                                    {
                                        shape.gContours[intI + 1] = nextContour;
                                    }
                                    else if (intI == shape.gContours.Count - 1)
                                    {
                                        shape.gContours[0] = nextContour;
                                        ;
                                    }
                                    else
                                    {
                                        shape.gContours[intI + 1] = nextContour;
                                        ;
                                    }

                                    if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                    {
                                        prevContour.x0 = x0Par;
                                        prevContour.y0 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                    {
                                        prevContour.x0 = x1Par;
                                        prevContour.y0 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }
                                    else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                    {
                                        prevContour.x1 = x0Par;
                                        prevContour.y1 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                    {
                                        prevContour.x1 = x1Par;
                                        prevContour.y1 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }

                                    if (intI == 0)
                                    {
                                        shape.gContours[shape.gContours.Count - 1] = prevContour;
                                    }
                                    else if (intI == shape.gContours.Count - 1)
                                    {
                                        shape.gContours[intI - 1] = prevContour;
                                    }
                                    else
                                    {
                                        shape.gContours[intI - 1] = prevContour;
                                    }

                                    shape.gContours[intI] = contour;

                                    //down = true;
                                }
                            }
                        }

                        for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                        {

                            Shape.Contour contour = (Shape.Contour) shape.gContours[intI];

                            if (contour.apronLink != "")
                                lstVelette.Add(contour.apronLink);

                            bool down = false;

                            if (shape.IsVeletta)
                            {
                                if (intI == 0)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[shape.gContours.Count - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }
                                else if (intI == shape.gContours.Count - 1)
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[0];
                                }
                                else
                                {
                                    prevContour = (Shape.Contour) shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour) shape.gContours[intI + 1];
                                }

                                if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 ||
                                    prevContour.cutAngle_Offset != 0)
                                {
                                    //if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                                    //    (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                                    //{
                                    //    contour.z0 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                    //else
                                    //{
                                    //    contour.z1 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                }
                                else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 ||
                                         nextContour.cutAngle_Offset != 0)
                                {
                                    //if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                                    //    (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                                    //{
                                    //    contour.z0 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                    //else
                                    //{
                                    //    contour.z1 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                }
                                else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 ||
                                         contour.cutAngle_Offset != 0)
                                {
                                    //contour.z0 += shape.gThickness;// shape.gVeletta.height;
                                    //contour.z1 += shape.gThickness;// shape.gVeletta.height;
                                }
                                else
                                {
                                    down = true;
                                }
                            }

                            coloreSagoma = Color.Black;

                            if (contour.line)
                            {

                                DrawLine(contour, coloreSagoma, ref al, shape.gAngle);
                                if (shape.HasVeletta)
                                {
                                    DrawLine((Shape.Contour) upperContour[intI], coloreSagoma, ref alUp, shape.gAngle);
                                }

                                if (shape.gBackSplash && contour.join)
                                {

                                    Transformation transformation = new Transformation();

                                    ////transformation.RotateZ(-shape.gAngle);
                                    transformation.Rotation(Utility.DegToRad(-shape.gAngle), Vector3D.AxisZ);

                                    Point3D p0 = new Point3D(contour.x0, contour.y0);
                                    Point3D p1 = new Point3D(contour.x1, contour.y1);

                                    ////transformation.Apply(ref p0);
                                    ////transformation.Apply(ref p1);
                                    p0 = transformation*p0;
                                    p1 = transformation*p1;

                                    ////lAttacco = CreateEyeLine(p0, p1, coloreSagoma);
                                    lAttacco = CreateEyeLine(p0, p1, coloreSagoma);

                                }

                            }

                            else
                            {

                                DrawArc(contour, ref al, shape.gAngle, coloreSagoma);
                                if (shape.HasVeletta)
                                {
                                    DrawArc((Shape.Contour) upperContour[intI], ref alUp, shape.gAngle, coloreSagoma);
                                }


                            }

                            if (al.Count != 1)
                            {

                                if (id != contour.id)
                                {

                                    ArrL newArrL = new ArrL();
                                    newArrL.indice = al.Count - 1;

                                    if (contour.code == "LAM01")
                                    {

                                        newArrL.tipo = "LAM01";
                                        newArrL.cw = 0;
                                        newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;

                                    }

                                    else if (contour.pricipalStruct)
                                    {

                                        newArrL.tipo = "PS";
                                        newArrL.cw = 0;
                                        if (shape.IsVeletta)
                                        {
                                            newArrL.spessore = shape.gVeletta.height;
                                        }
                                        else
                                        {
                                            newArrL.spessore = shape.gThickness;
                                        }
                                        newArrL.passante = true;

                                    }

                                    else if (contour.code.Substring(0, 2) != "CP" &&
                                             contour.code != "SNP0002" &&
                                             contour.code != "SNP0008" &&
                                             contour.subType != "Groove" &&
                                             contour.subType != "Pocket")
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        //newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;
                                        newArrL.insertMode = contour.insertMode;

                                        if (contour.code == "SNP0004" && contour.insertMode == "RIBASSATO")
                                            bolRibasso = true;

                                    }

                                    else if (contour.code == "SNP0002")
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaB;
                                        newArrL.passante = false;
                                        newArrL.insertMode = contour.insertMode;
                                        bolBussola = true;

                                    }

                                    else if (contour.subType != "Pocket")
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaC;
                                        newArrL.passante = false;
                                        newArrL.insertMode = contour.insertMode;

                                    }

                                    else
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaT;
                                        newArrL.passante = false;
                                        newArrL.insertMode = contour.insertMode;
                                        bolTasca = true;

                                    }

                                    alIndice.Add(newArrL);

                                    id = contour.id;

                                }

                            }

                            else
                            {

                                ArrL newArrL = new ArrL();
                                newArrL.indice = 0;

                                if (contour.code == "LAM01")
                                {

                                    newArrL.tipo = "LAM01";
                                    newArrL.cw = 0;
                                    newArrL.spessore = shape.gThickness;
                                    newArrL.passante = true;

                                }

                                else if (contour.pricipalStruct)
                                {

                                    newArrL.tipo = "PS"; // contour.pricipalStruct ? "PS" : contour.code;
                                    newArrL.cw = 0;
                                    if (shape.IsVeletta)
                                    {
                                        //if (!down)
                                        //{
                                        newArrL.spessore = shape.gThickness + shape.gVeletta.height;
                                        //}
                                        ////else
                                        ////{
                                        ////    newArrL.spessore = shape.gVeletta.height + 10;
                                        ////}
                                    }
                                    else
                                    {
                                        newArrL.spessore = shape.gThickness;
                                    }
                                    newArrL.passante = true;

                                }

                                else if (contour.code.Substring(0, 2) != "CP" &&
                                         contour.code != "SNP0002" &&
                                         contour.code != "SNP0008" &&
                                         contour.subType != "Groove" &&
                                         contour.subType != "Pocket")
                                {

                                    newArrL.tipo = contour.code; // contour.pricipalStruct ? "PS" : contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    //newArrL.spessore = shape.gThickness;
                                    newArrL.passante = true;
                                    newArrL.insertMode = contour.insertMode;

                                    if (contour.code == "SNP0004" && contour.insertMode == "RIBASSATO")
                                        bolRibasso = true;

                                }

                                else if (contour.code == "SNP0002")
                                {

                                    newArrL.tipo = contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaB;
                                    newArrL.passante = false;
                                    newArrL.insertMode = contour.insertMode;
                                    bolBussola = true;

                                }

                                else if (contour.subType != "Pocket")
                                {

                                    newArrL.tipo = contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaC;
                                    newArrL.passante = false;
                                    newArrL.insertMode = contour.insertMode;

                                }

                                else
                                {

                                    newArrL.tipo = contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaT;
                                    newArrL.passante = false;
                                    newArrL.insertMode = contour.insertMode;
                                    bolTasca = true;

                                }

                                alIndice.Add(newArrL);

                                id = contour.id;

                            }

                            if (shape.HasVeletta)
                            {
                                if (alUp.Count != 1)
                                {
                                    if (idUp != ((Shape.Contour) upperContour[intI]).id)
                                    {

                                        ArrL newArrL = new ArrL();
                                        newArrL.indice = alUp.Count - 1;

                                        if (((Shape.Contour) upperContour[intI]).code == "LAM01")
                                        {

                                            newArrL.tipo = "LAM01";
                                            newArrL.cw = 0;
                                            newArrL.spessore = shape.gThickness;
                                            newArrL.passante = true;

                                        }

                                        else if (((Shape.Contour) upperContour[intI]).pricipalStruct)
                                        {

                                            newArrL.tipo = "PS";
                                            newArrL.cw = 0;
                                            if (shape.IsVeletta)
                                            {
                                                newArrL.spessore = shape.gVeletta.height;
                                            }
                                            else
                                            {
                                                newArrL.spessore = shape.gThickness;
                                            }
                                            newArrL.passante = true;

                                        }

                                        else if (((Shape.Contour) upperContour[intI]).code.Substring(0, 2) != "CP" &&
                                                 ((Shape.Contour) upperContour[intI]).code != "SNP0002" &&
                                                 ((Shape.Contour) upperContour[intI]).code != "SNP0008" &&
                                                 ((Shape.Contour) upperContour[intI]).subType != "Groove" &&
                                                 ((Shape.Contour) upperContour[intI]).subType != "Pocket")
                                        {

                                            newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            //newArrL.spessore = shape.gThickness;
                                            newArrL.passante = true;
                                            newArrL.insertMode = contour.insertMode;

                                            if (((Shape.Contour) upperContour[intI]).code == "SNP0004" &&
                                                ((Shape.Contour) upperContour[intI]).insertMode == "RIBASSATO")
                                                bolRibasso = true;

                                        }

                                        else if (((Shape.Contour) upperContour[intI]).code == "SNP0002")
                                        {

                                            newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            newArrL.spessore = profonditaB;
                                            newArrL.passante = false;
                                            newArrL.insertMode = ((Shape.Contour) upperContour[intI]).insertMode;
                                            bolBussola = true;

                                        }

                                        else if (contour.subType != "Pocket")
                                        {

                                            newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            newArrL.spessore = profonditaC;
                                            newArrL.passante = false;
                                            newArrL.insertMode = ((Shape.Contour) upperContour[intI]).insertMode;

                                        }

                                        else
                                        {

                                            newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            newArrL.spessore = profonditaT;
                                            newArrL.passante = false;
                                            newArrL.insertMode = ((Shape.Contour) upperContour[intI]).insertMode;
                                            bolTasca = true;

                                        }

                                        alIndiceUp.Add(newArrL);

                                        idUp = ((Shape.Contour) upperContour[intI]).id;

                                    }

                                }

                                else
                                {

                                    ArrL newArrL = new ArrL();
                                    newArrL.indice = 0;

                                    if (((Shape.Contour) upperContour[intI]).code == "LAM01")
                                    {

                                        newArrL.tipo = "LAM01";
                                        newArrL.cw = 0;
                                        newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;

                                    }

                                    else if (((Shape.Contour) upperContour[intI]).pricipalStruct)
                                    {

                                        newArrL.tipo = "PS"; // contour.pricipalStruct ? "PS" : contour.code;
                                        newArrL.cw = 0;
                                        if (shape.IsVeletta)
                                        {
                                            //if (!down)
                                            //{
                                            newArrL.spessore = shape.gThickness + shape.gVeletta.height;
                                            //}
                                            ////else
                                            ////{
                                            ////    newArrL.spessore = shape.gVeletta.height + 10;
                                            ////}
                                        }
                                        else
                                        {
                                            newArrL.spessore = shape.gThickness;
                                        }
                                        newArrL.passante = true;

                                    }

                                    else if (((Shape.Contour) upperContour[intI]).code.Substring(0, 2) != "CP" &&
                                             ((Shape.Contour) upperContour[intI]).code != "SNP0002" &&
                                             ((Shape.Contour) upperContour[intI]).code != "SNP0008" &&
                                             ((Shape.Contour) upperContour[intI]).subType != "Groove" &&
                                             ((Shape.Contour) upperContour[intI]).subType != "Pocket")
                                    {

                                        newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                            // contour.pricipalStruct ? "PS" : contour.code;
                                        newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        //newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;
                                        newArrL.insertMode = contour.insertMode;

                                        if (((Shape.Contour) upperContour[intI]).code == "SNP0004" &&
                                            ((Shape.Contour) upperContour[intI]).insertMode == "RIBASSATO")
                                            bolRibasso = true;

                                    }

                                    else if (((Shape.Contour) upperContour[intI]).code == "SNP0002")
                                    {

                                        newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                        newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaB;
                                        newArrL.passante = false;
                                        newArrL.insertMode = ((Shape.Contour) upperContour[intI]).insertMode;
                                        bolBussola = true;

                                    }

                                    else if (contour.subType != "Pocket")
                                    {

                                        newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                        newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaC;
                                        newArrL.passante = false;
                                        newArrL.insertMode = ((Shape.Contour) upperContour[intI]).insertMode;

                                    }

                                    else
                                    {

                                        newArrL.tipo = ((Shape.Contour) upperContour[intI]).code;
                                        newArrL.sottoTipo = ((Shape.Contour) upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaT;
                                        newArrL.passante = false;
                                        newArrL.insertMode = ((Shape.Contour) upperContour[intI]).insertMode;
                                        bolTasca = true;

                                    }

                                    alIndiceUp.Add(newArrL);

                                    idUp = ((Shape.Contour) upperContour[intI]).id;

                                }
                            }

                            //}
                        }

                        #endregion

                        id = -1;

                        #region Ciclo per ogni contorno delle bussole.

                        for (int intI = 0; intI <= shape.gBussole.Count - 1; intI++)
                        {

                            Shape.Bussola bussola = (Shape.Bussola) shape.gBussole[intI];

                            coloreSagoma = Color.Blue;

                            for (int intJ = 0; intJ < bussola.sides.Count; intJ++)
                            {

                                Shape.Contour contour = (Shape.Contour) bussola.sides[intJ];

                                if (contour.line)
                                {

                                    DrawLine(contour, coloreSagoma, ref alBussole, shape.gAngle);

                                }

                                else
                                {

                                    DrawArc(contour, ref alBussole, shape.gAngle, coloreSagoma);

                                }

                                if (alBussole.Count != 1)
                                {

                                    if (id != bussola.id)
                                    {

                                        ArrL newArrL = new ArrL();
                                        newArrL.indice = alBussole.Count - 1;

                                        newArrL.tipo = contour.code;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaB;
                                        newArrL.passante = false;

                                        alIndiceBussole.Add(newArrL);

                                        id = bussola.id;

                                    }

                                }

                                else
                                {

                                    ArrL newArrL = new ArrL();
                                    newArrL.indice = 0;

                                    newArrL.tipo = contour.code;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaB;
                                    newArrL.passante = false;

                                    alIndiceBussole.Add(newArrL);

                                    id = bussola.id;

                                }

                            }

                        }

                        #endregion

                        #region Definizione del materiale.

                        Material mat = null;

                        //string m = pathImage[intS];
                        using (Bitmap m = (Bitmap) pathImage[intS].Clone())
                        {
                            if (m != null)
                            {

                                //if (System.IO.File.Exists(m))
                                //{

                                ////m.Tag = shapes[intS].gId.ToString();

                                ////////Texture t = new Texture(m, Texture.minifyingFunctionType.Linear);
                                ////Texture t = new Texture(m);

                                ////int mText = vppRender.Textures.Add(t);
                                ////mat = new Material("mat" + m.Tag, mText);


                                //using (Bitmap bitmap = (Bitmap)BuildSquareBitmap(m))
                                Bitmap bitmap = (Bitmap) BuildSquareBitmap(m);
                                {
                                    mat = new Material(bitmap);

                                    string matKey = shapes[intS].gId.ToString();
                                    mat.Description = matKey;
                                    mat.MinifyingFunction = textureFilteringFunctionType.Linear;
                                    if (!vppRender.Materials.ContainsKey(matKey))
                                    {
                                        vppRender.Materials.Add(matKey, mat);
                                    }
                                }

                                //mat = new Material("mat" + mText.ToString(), mText);

                                //}

                                //else
                                //{

                                //    mat = new Material("NOMAT");

                                //}

                            }

                            else
                            {

                                ////mat = new Material("NOMAT");
                                mat = new Material();
                                string matKey = "NOMAT";
                                mat.Description = matKey;
                                if (!vppRender.Materials.ContainsKey(matKey))
                                    vppRender.Materials.Add(matKey, mat);

                            }
                        }


                        #endregion

                        Mesh model = null; // Mesh contenente il modello da disegnare.
                        Mesh modelSopra = null; // Mesh contenente il modello da disegnare sopra.
                        Mesh modelRibasso = null; // Mesh contenente il modello del ribasso.
                        Mesh modelRibassoT = null; // Mesh contenente il modello del ribasso per le tasche.

                        if (shape.IsVeletta)
                        {
                            DisegnaFacciaSottoVeletta(al, alIndice, mat, vppRender, ref model);
                        }
                        else
                        {
                            //TODO:DEC
                            DisegnaFacciaSotto(al, alIndice, mat, vppRender, ref model);
                            //model.FlipNormal();
                            //model.Regen(CHORDAL_ERROR);
                        }
                        //if (model.Normals != null)
                        {
                            model.FlipNormal();
                            model.Regen(CHORDAL_ERROR);
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        Mesh[] models = null;

                        if (shape.IsVeletta)
                        {
                            models = DisegnaBordiVeletta(al, shape, mat, pathProfiliLav, vppRender, profonditaR,
                                profonditaT);
                        }
                        else if (shape.HasVeletta)
                        {
                            models = DisegnaBordi(al, alUp, shape, mat, pathProfiliLav, vppRender, profonditaR,
                                profonditaT);
                        }
                        else
                        {
                            models = DisegnaBordi(al, shape, mat, pathProfiliLav, vppRender, profonditaR, profonditaT);
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        //Mesh[] modelsFittizio = DisegnaRaccordiFittizi(al, shape, mat, pathProfiliLav, vppRender, profonditaR, profonditaT);
                        //prbMain.Value++;

                        Mesh[] modelsBussole = null;

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        if (bolBussola)
                            modelsBussole = DisegnaBordiBussole(alBussole, alIndiceBussole, shape, mat, vppRender);
                        
                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (shape.IsVeletta)
                        {
                            DisegnaFacciaSopraVeletta(al, alIndice, alBussole, alIndiceBussole, mat, vppRender,
                                ref modelSopra, ref alCanaletti);
                        }
                        else if (shape.HasVeletta)
                        {
                            DisegnaFacciaSopra(alUp, alIndiceUp, alBussole, alIndiceBussole, mat, vppRender,
                                ref modelSopra, ref alCanaletti);
                        }
                        else
                        {
                            DisegnaFacciaSopra(al, alIndice, alBussole, alIndiceBussole, mat, vppRender, ref modelSopra,
                                ref alCanaletti);
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        if (bolRibasso) DisegnaRibasso(al, alIndice, mat, vppRender, ref modelRibasso, profonditaR);
                        
                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        if (bolTasca) DisegnaRibassoTasca(al, alIndice, mat, vppRender, ref modelRibassoT, profonditaT);
                        
                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        Mesh[] modelsCanaletti = null;

                        if (alCanaletti.Count > 0)
                        {

                            modelsCanaletti = DisegnaBordiCanaletti(alCanaletti, shape, mat, vppRender, profonditaC);

                        }

                        //prbMain.Value++; 
                        CurrentTopRenderStep++;


                        Mesh[] modelVGroove = null;

                        if (shape.gVGrooves.Count > 0)
                        {

                            modelVGroove = DisegnaVGroove(shape, mat, vppRender, shape.gAngle);

                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        #region Unione delle parti disegnate

                        //29/06/2015

                        //if (model != null) // Faccia sotto
                        //{
                        //    model.FlipNormal();
                        //    model.EdgeStyle = Mesh.edgeStyleType.Sharp;
                        //    model.ComputeEdges();
                        //    model.Regen(CHORDAL_ERROR);
                        //}
                        //

                        if (models != null)
                        {

                            for (int intI = 0; intI < models.Length; intI++)
                            {
                                if (model == null && intI == 0)
                                {
                                    model = models[intI];
                                }
                                else if (models[intI] != null)
                                {
                                    model.MergeWith(models[intI], false);
                                }
                            }

                        }
                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelsBussole != null)
                        {

                            for (int intI = 0; intI < modelsBussole.Length; intI++)
                            {

                                model.MergeWith(modelsBussole[intI], false);

                            }

                        }
                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelsCanaletti != null)
                        {

                            for (int intI = 0; intI < modelsCanaletti.Length; intI++)
                            {

                                model.MergeWith(modelsCanaletti[intI], false);

                            }

                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelVGroove != null)
                        {

                            for (int intI = 0; intI < modelVGroove.Length; intI++)
                            {

                                model.MergeWith(modelVGroove[intI], false);

                            }

                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelRibasso != null)
                        {

                            model.MergeWith(modelRibasso, false);

                        }

                        if (modelRibassoT != null)
                        {

                            model.MergeWith(modelRibassoT, false);

                        }

                        if (modelSopra != null)
                        {
                            if (shape.HasVeletta)
                            {
                                model.MergeWith(modelSopra, false);

                                //vppRender.Entities.Add(modelSopra);
                            }
                            else
                            {
                                if (model == null)
                                {
                                    model = modelSopra;
                                }
                                else
                                {
                                    model.MergeWith(modelSopra, false);
                                }
                            }
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        #endregion

                        ////model.EdgeStyleMode = Mesh.edgeStyleType.Sharp;
                        /// 
                        /// 

                        model.UpdateNormals();
                        bool isMaterialApplied = false;
                        ////try
                        ////{
                        ////    var sectionPlane = (Plane) Plane.XY.Clone();
                        ////    //sectionPlane.Translate(0, 0, shape.gThickness / 2);
                        ////    sectionPlane.Translate(0, 0, 0.1);
                        ////    Mesh[] parts;
                        ////    var retVal = model.SplitBy(sectionPlane, false, out parts);

                        ////    string mirrorMatName = mat.Description + "_Mirror";

                        ////    if (!vppRender.Materials.ContainsKey(mirrorMatName))
                        ////    {
                        ////        var mirrorMat = (Material) mat.Clone();
                        ////        var texture = (Image) mat.TextureImage.Clone();
                        ////        //texture.RotateFlip(RotateFlipType.Rotate180FlipXY);
                        ////        mirrorMat.TextureImage = texture;
                        ////        vppRender.Materials.Add(mirrorMatName, mirrorMat);
                        ////    }

                        ////    parts[1].Weld();
                        ////    parts[1].EdgeStyle = Mesh.edgeStyleType.Sharp;
                        ////    parts[1].ComputeEdges();
                        ////    //parts[1].Scale(1, 1, -1);
                        ////    //parts[1].FlipOutside();
                        ////    //parts[1].UpdateNormals();
                        ////    //parts[1].Scale(1, 1, -1);
                        ////    //parts[1].ComputeEdges();
                        ////    parts[1].ApplyMaterial(mirrorMatName, textureMappingType.Cubic, 1, 1);
                        ////    parts[1].FlipNormal();
                        ////    parts[1].Regen(CHORDAL_ERROR);

                        ////    parts[0].Weld();
                        ////    //parts[0].FlipOutside();
                        ////    parts[0].ComputeEdges();
                        ////    parts[0].Regen(CHORDAL_ERROR);
                        ////    parts[0].ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);


                        ////    parts[0].MergeWith(parts[1], true);
                        ////    model = parts[0];
                        ////    isMaterialApplied = true;
                        ////}
                        ////catch (Exception ex)
                        ////{
                        ////    TraceLog.WriteLine("DrawTop splitBy Error. ", ex);
                         
                        ////}

                        //if (!isMaterialApplied)
                        //model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);
                        model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1, model.BoxMin, model.BoxMax);


                        model.EdgeStyle = Mesh.edgeStyleType.Sharp;


                        //model.ComputeEdges();
                        //model.Regen(CHORDAL_ERROR);

                        //vppRender.Entities.RegenAllCurved(1e-3);
                        //vppRender.Entities.Regen();
                        //var eList = new List<Entity>();
                        //var layerList = new List<Layer>();
                        //layerList.AddRange(vppRender.Layers);
                        //eList.Add(model);
                        //string name = "Model " + intS + ".stl";
                        //var writer = new WriteSTL(eList, layerList, new Dictionary<string, Block>(), name);
                        //writer.DoWork();




                        //model.NormalAveragingMode = Mesh.normalAveragingType.AveragedByAngle;

                        #region Gestione BackSplash

                        if (shape.gBackSplash)
                        {

                            // Leggo il punto di partenza della linea di attacco per spostare la sagoma in 0, 0
                            ////Point3D pMin = lAttacco.Start;
                            Point3D pMin = lAttacco.StartPoint;

                            // Sposto la sagoma in 0, 0
                            ////model.Move(-pMin.X, -pMin.Y, -pMin.Z);
                            model.Translate(-pMin.X, -pMin.Y, -pMin.Z);

                            // Leggo l'angolo dell'entit� di attacco
                            ////double angolo = GetAngolo(lAttacco.Start.X, lAttacco.Start.Y, lAttacco.End.X, lAttacco.End.Y);
                            double angolo = GetAngolo(lAttacco.StartPoint.X, lAttacco.StartPoint.Y, lAttacco.EndPoint.X,
                                lAttacco.EndPoint.Y);

                            // Ruoto la sagoma di un angolo pari al negativo dell'angolo dell'entit� di attacco,
                            // cos� porto la sagoma ad avere l'entit� di attacco in corrispondenza dell'asse X
                            ////model.RotateZ(-angolo);
                            model.Rotate(Utility.DegToRad(-angolo), Vector3D.AxisZ);

                            // Ruoto la sagoma di un angolo di 90� intorno all'asse X
                            ////model.RotateX(90);
                            model.Rotate(Utility.DegToRad(90), Vector3D.AxisX);

                            // Ruoto la sagoma di un angolo pari all'angolo dell'entit� di attacco,
                            // cos� riporto la sagoma alla posizione di partenza
                            ////model.RotateZ(angolo);
                            model.Rotate(Utility.DegToRad(angolo), Vector3D.AxisZ);

                            // Risposto la sagoma alla posizione originale
                            ////model.Move(pMin.X, pMin.Y, pMin.Z);
                            model.Translate(pMin.X, pMin.Y, pMin.Z);

                            try
                            {
                                var deltaZ = 0.0;
                                if (dSpessori.ContainsKey(shape.gShapeIdJoin))
                                {
                                    deltaZ = dSpessori[shape.gShapeIdJoin];
                                    // Alzo la sagoma di un valore pari allo spessore della sagoma di attacco
                                    ////model.MoveZ(dSpessori[shape.gShapeIdJoin]);
                                    model.Translate(0, 0, dSpessori[shape.gShapeIdJoin]);
                                }
                                

                            }

                            catch (Exception ex)
                            {
                                TraceLog.WriteLine("DrawTop error ", ex);
                            }

                        }

                        #endregion

                        ////model.RotateZ(shape.gAngle);
                        model.Rotate(Utility.DegToRad(shape.gAngle), Vector3D.AxisZ);

                        ////model.Name = shape.gShapeNumber.ToString();
                        model.EntityData = shape.gShapeNumber.ToString();


                        shapeTmp shpTmp = new shapeTmp();
                        if (shape.IsVeletta)
                            shpTmp.veletta = shape.gVeletta.link;
                        else
                            shpTmp.veletta = "";
                        shpTmp.velette = lstVelette;
                        shpTmp.rMesh = (Mesh)model.Clone();
                        lstObjects.Add(shpTmp);

                    }
                }

                for (int i = 0; i < lstObjects.Count; i++)
                {
                    try
                    {
                        shapeTmp shpTmp = lstObjects[i];


                        Mesh model = shpTmp.rMesh;

                        //De conti
                        ////model.ApplyMaterial(model.MaterialName, textureMappingType.Cubic, 1, 1);
                        model.Regen(CHORDAL_ERROR);

                        //TraceLog.WriteLine("MESH: " + model.Dump());

                        vppRender.Entities.Add(model);
                    }
                    catch (Exception ex)
                    {
                        TraceLog.WriteLine(string.Format("DrawTop error mesh #", i), ex);
                    }


                }

                #endregion


            }
            catch (Exception ex)
            {

                TraceLog.WriteLine("DrawTop Error. ", ex);
            }
            finally
            {
                //vppRender.SetView(viewType.Isometric);

                vppRender.DisplayMode = displayType.Rendered;



                //TODO DeConti: Scopo??
                ////vppRender.RescaleModel();
                vppRender.Entities.Regen();

                vppRender.Viewports[0].OriginSymbol.Visible = false;

                vppRender.Invalidate();

                //prbMain.Visible = false;

                OnTopRenderCompleted();
            }
        }

        internal void DrawTopOnFiles(Shape[] shapes, string[] pathImage, string pathProfiliLav, ViewportLayout vppRender, double profonditaRibasso, double profonditaCanaletti, double profonditaBussole, double profonditaTasca, Dictionary<int, RenderedElement> elements)//, System.Windows.Forms.ToolStripProgressBar prbMain)
        {
            _totalTopRenderSteps = shapes.Length * 14;
            OnTopRenderStarted(_totalTopRenderSteps);
            //prbMain.Value = 0;
            //prbMain.Maximum = shapes.Length * 14;                                   // Numero di sagome * numero di conteggi
            //prbMain.Visible = true;

            double profonditaT = profonditaTasca;
            double profonditaR = profonditaRibasso;
            double profonditaC = profonditaCanaletti;
            double profonditaB = profonditaBussole;

            try
            {
                //elimino i contour non utilizzati (tipo Symbol)
                for (int intS = 0; intS < shapes.Length; intS++)
                {

                    Shape shape = shapes[intS];

                    for (int intI = shape.gContours.Count - 1; intI >= 0; intI--)
                    {
                        if (((Shape.Contour)shape.gContours[intI]).code == "Symbols")
                        {
                            shape.gContours.RemoveAt(intI);
                        }
                    }
                }

                //for (int intS = 0; intS < shapes.Length; intS++)
                //    InsertFillet(shapes[intS]);

                #region  Ciclo per tutte le sagome per tenere in memoria lo spessore per ogni sagoma.

                Dictionary<int, double> dSpessori = new Dictionary<int, double>();
                // Per ogni sagoma salvo id e spessore

                for (int intS = 0; intS < shapes.Length; intS++)
                {

                    try
                    {

                        dSpessori.Add(shapes[intS].gId, shapes[intS].gThickness);

                    }

                    catch
                    {
                    }

                }

                #endregion

                #region  Ciclo per tutte le sagome.

                List<shapeTmp> lstObjects = new List<shapeTmp>();

                Dictionary<string, string> dicVelette = new Dictionary<string, string>();

                for (int intS = 0; intS < shapes.Length; intS++)
                {
                    //if (intS != 1) continue;

                    ArrayList al = new ArrayList(); // ArrayList che contiene le entit� per ogni sagoma (Shape).
                    ArrayList alUp = new ArrayList();
                    // ArrayList che contiene le entit� per ogni sagoma (Shape) avente velette.
                    ArrayList alIndice = new ArrayList(); // ArrayList che contiene gli ArrL.
                    ArrayList alIndiceUp = new ArrayList(); // ArrayList che contiene gli ArrL.

                    ArrayList alBussole = new ArrayList(); // ArrayList che contiene le entit� per ogni Bussola.
                    ArrayList alIndiceBussole = new ArrayList(); // ArrayList che contiene gli ArrL.
                    ArrayList alCanaletti = new ArrayList(); // ArrayList che contiene gli array dei canaletti.
                    bool bolBussola = false;
                    bool bolTasca = false;
                    bool bolRibasso = false;
                    //double angoloPrimoElemento = -361;

                    Shape shape = new Shape(shapes[intS]);

                    int id = -1;
                    int idUp = -1;
                    //se la sagoma ha meno di 4 elementi e ci sono dei segmenti non funziona
                    //con il cerchio s�. verifico quindi se � una sagoma di soli archi
                    bool bolElabora = true;
                    //if (shape.gContours.Count <= 3)
                    //{
                    //    for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                    //    {
                    //        if (((Shape.Contour) shape.gContours[intI]).line)
                    //        {
                    //            bolElabora = false;
                    //            break;
                    //        }
                    //    }
                    //}

                    //if (shape.gContours.Count > 3)
                    if (bolElabora)
                    {
                        Color coloreSagoma = new Color(); // Colore di disegno delle Sagome.

                        Line lAttacco = CreateEyeLine(0, 0, 1, 1, Color.Red);
                        // Linea di collegamento tra sagome utilizzata per il BackSplash

                        #region Ciclo per ogni contorno delle sagome.

                        //if (shape.IsVeletta)
                        //{
                        //    Shape ShapeDuplicated = new Shape();
                        //    Shape.Contour prevContour;
                        //    Shape.Contour nextContour;

                        //    coloreSagoma = Color.Black;

                        //    for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                        //    {

                        //        Shape.Contour contour = (Shape.Contour)shape.gContours[intI];
                        //        Shape.Contour contourDuplicated = new Shape.Contour();

                        //        contourDuplicated.x0 = contour.x0;
                        //        contourDuplicated.x1 = contour.x1;
                        //        contourDuplicated.y0 = contour.y0;
                        //        contourDuplicated.y1 = contour.y1;
                        //        contourDuplicated.z0 = contour.z0;
                        //        contourDuplicated.z1 = contour.z1;

                        //        if (intI == 0)
                        //        {
                        //            prevContour = (Shape.Contour)shape.gContours[shape.gContours.Count - 1];
                        //            nextContour = (Shape.Contour)shape.gContours[intI + 1];
                        //        }
                        //        else if (intI == shape.gContours.Count - 1)
                        //        {
                        //            prevContour = (Shape.Contour)shape.gContours[intI - 1];
                        //            nextContour = (Shape.Contour)shape.gContours[0];
                        //        }
                        //        else
                        //        {
                        //            prevContour = (Shape.Contour)shape.gContours[intI - 1];
                        //            nextContour = (Shape.Contour)shape.gContours[intI + 1];
                        //        }

                        //        if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 || prevContour.cutAngle_Offset != 0)
                        //        {
                        //            if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                        //                (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                        //            {
                        //                contourDuplicated.z0 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //            else
                        //            {
                        //                contourDuplicated.z1 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //        }
                        //        else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 || nextContour.cutAngle_Offset != 0)
                        //        {
                        //            if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                        //                (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                        //            {
                        //                contourDuplicated.z0 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //            else
                        //            {
                        //                contourDuplicated.z1 += shape.gThickness;// shape.gVeletta.height;
                        //            }
                        //        }
                        //        else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 || contour.cutAngle_Offset != 0)
                        //        {
                        //            contourDuplicated.z0 += shape.gThickness;// shape.gVeletta.height;
                        //            contourDuplicated.z1 += shape.gThickness;// shape.gVeletta.height;
                        //        }
                        //        else
                        //        {
                        //            down = true;
                        //        }

                        //        DrawLine(contourDuplicated, coloreSagoma, ref al, shape.gAngle);

                        //        if (al.Count != 1)
                        //        {
                        //            if (id != contour.id)
                        //            {

                        //                ArrL newArrL = new ArrL();
                        //                newArrL.indice = al.Count - 1;

                        //                if (contour.pricipalStruct)
                        //                {

                        //                    newArrL.tipo = "PS";
                        //                    newArrL.cw = 0;
                        //                    newArrL.spessore = shape.gVeletta.height;
                        //                    newArrL.passante = true;

                        //                }

                        //                alIndice.Add(newArrL);

                        //                id = contour.id;

                        //            }
                        //        }

                        //        else
                        //        {
                        //            ArrL newArrL = new ArrL();
                        //            newArrL.indice = 0;

                        //            if (contour.pricipalStruct)
                        //            {

                        //                newArrL.tipo = "PS";// contour.pricipalStruct ? "PS" : contour.code;
                        //                newArrL.cw = 0;
                        //                newArrL.spessore = shape.gThickness + shape.gVeletta.height;
                        //                newArrL.passante = true;

                        //            }

                        //            alIndice.Add(newArrL);

                        //            id = contour.id;
                        //        }
                        //    }
                        //}

                        List<string> lstVelette = new List<string>();
                        Shape.Contour prevContour;
                        Shape.Contour nextContour;

                        ArrayList upperContour = new ArrayList();

                        #region HasVeletta
                        if (shape.HasVeletta)
                        {
                            for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                            {
                                Shape.Contour contour = (Shape.Contour)shape.gContours[intI];
                                Shape.Contour contourTmp = new Shape.Contour();
                                contourTmp.apronLink = contour.apronLink;
                                contourTmp.code = contour.code;
                                contourTmp.cutAngle_Angle = contour.cutAngle_Angle;
                                contourTmp.cutAngle_Height = contour.cutAngle_Height;
                                contourTmp.cutAngle_Offset = contour.cutAngle_Offset;
                                contourTmp.cw = contour.cw;
                                contourTmp.endAngle = contour.endAngle;
                                contourTmp.id = contour.id;
                                contourTmp.idContour = contour.idContour;
                                contourTmp.insertMode = contour.insertMode;
                                contourTmp.join = contour.join;
                                contourTmp.line = contour.line;
                                contourTmp.pricipalStruct = contour.pricipalStruct;
                                contourTmp.processing = contour.processing;
                                contourTmp.r = contour.r;
                                contourTmp.startAngle = contour.startAngle;
                                contourTmp.straightLine = contour.straightLine;
                                contourTmp.subType = contour.subType;
                                contourTmp.x0 = contour.x0;
                                contourTmp.x1 = contour.x1;
                                contourTmp.xC = contour.xC;
                                contourTmp.y0 = contour.y0;
                                contourTmp.y1 = contour.y1;
                                contourTmp.yC = contour.yC;
                                contourTmp.z0 = contour.z0;
                                contourTmp.z1 = contour.z1;
                                contourTmp.zC = contour.zC;
                                upperContour.Add(contourTmp);
                            }
                        }
                        #endregion HasVeletta

                        for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                        {

                            Shape.Contour contour = (Shape.Contour)shape.gContours[intI];

                            #region HasVeletta

                            if (shape.HasVeletta)
                            {
                                double x0Par = 0;
                                double y0Par = 0;
                                double x1Par = 0;
                                double y1Par = 0;

                                if (intI == 0)
                                {
                                    prevContour = (Shape.Contour)shape.gContours[shape.gContours.Count - 1];
                                    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                }
                                else if (intI == shape.gContours.Count - 1)
                                {
                                    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour)shape.gContours[0];
                                }
                                else
                                {
                                    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                }

                                if (contour.apronLink != "")
                                {
                                    FindParallelLine(ref x0Par, ref y0Par, ref x1Par, ref y1Par, contour.x0, contour.y0,
                                        contour.x1, contour.y1, shape.gThickness);
                                    double angolo = Utils.GetAngolo(contour.x0, contour.y0, contour.x1, contour.y1);

                                    while (angolo >= 360)
                                    {
                                        angolo -= 360;
                                    }

                                    //dicVelette.Add(contour.apronLink, contour.x0.ToString() + "#" + contour.y0.ToString() + "#" + contour.x1.ToString() + "#" + contour.y1.ToString() + "#" + angolo.ToString() + "#" + shape.gThickness.ToString());
                                    dicVelette.Add(((Shape.Contour)upperContour[intI]).apronLink,
                                        ((Shape.Contour)upperContour[intI]).x0.ToString() + "#" +
                                        ((Shape.Contour)upperContour[intI]).y0.ToString() + "#" +
                                        ((Shape.Contour)upperContour[intI]).x1.ToString() + "#" +
                                        ((Shape.Contour)upperContour[intI]).y1.ToString() + "#" + angolo.ToString() +
                                        "#" + shape.gThickness.ToString());

                                    if (nextContour.line)
                                    {
                                        if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                        {
                                            nextContour.x0 = x0Par;
                                            nextContour.y0 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                        {
                                            nextContour.x0 = x1Par;
                                            nextContour.y0 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                        {
                                            nextContour.x1 = x0Par;
                                            nextContour.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                        {
                                            nextContour.x1 = x1Par;
                                            nextContour.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        if (intI == 0)
                                        {
                                            shape.gContours[intI + 1] = nextContour;
                                        }
                                        else if (intI == shape.gContours.Count - 1)
                                        {
                                            shape.gContours[0] = nextContour;
                                            ;
                                        }
                                        else
                                        {
                                            shape.gContours[intI + 1] = nextContour;
                                            ;
                                        }

                                        shape.gContours[intI] = contour;
                                    }
                                    else
                                    {

                                        Shape.Contour contourToAddUp = new Shape.Contour();
                                        contourToAddUp.line = true;
                                        contourToAddUp.apronLink = "";
                                        contourToAddUp.processing = "";
                                        contourToAddUp.pricipalStruct = contour.pricipalStruct;
                                        contourToAddUp.code = contour.code;

                                        Shape.Contour contourToAdd = new Shape.Contour();
                                        contourToAdd.line = true;
                                        contourToAdd.apronLink = "";
                                        contourToAdd.processing = "";
                                        contourToAdd.pricipalStruct = contour.pricipalStruct;
                                        contourToAdd.code = contour.code;

                                        if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                        {
                                            contourToAddUp.x0 = nextContour.x0;
                                            contourToAddUp.y0 = nextContour.y0;
                                            contourToAddUp.x1 = nextContour.x0;
                                            contourToAddUp.y1 = nextContour.y0;

                                            contourToAdd.x0 = nextContour.x0;
                                            contourToAdd.y0 = nextContour.y0;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                        {
                                            contourToAddUp.x0 = nextContour.x0;
                                            contourToAddUp.y0 = nextContour.y0;
                                            contourToAddUp.x1 = nextContour.x0;
                                            contourToAddUp.y1 = nextContour.y0;

                                            contourToAdd.x1 = nextContour.x0;
                                            contourToAdd.y1 = nextContour.y0;
                                            contourToAdd.x0 = x1Par;
                                            contourToAdd.y0 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                        {
                                            contourToAddUp.x0 = nextContour.x1;
                                            contourToAddUp.y0 = nextContour.y1;
                                            contourToAddUp.x1 = nextContour.x1;
                                            contourToAddUp.y1 = nextContour.y1;

                                            contourToAdd.x0 = nextContour.x1;
                                            contourToAdd.y0 = nextContour.y1;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                        {
                                            contourToAddUp.x0 = nextContour.x1;
                                            contourToAddUp.y0 = nextContour.y1;
                                            contourToAddUp.x1 = nextContour.x1;
                                            contourToAddUp.y1 = nextContour.y1;

                                            contourToAdd.x0 = nextContour.x1;
                                            contourToAdd.y0 = nextContour.y1;
                                            contourToAdd.x1 = x1Par;
                                            contourToAdd.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        shape.gContours[intI] = contour;
                                        shape.gContours.Insert(intI + 1, contourToAdd);
                                        upperContour.Insert(intI + 1, contourToAddUp);
                                    }

                                    if (prevContour.line)
                                    {
                                        if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                        {
                                            prevContour.x0 = x0Par;
                                            prevContour.y0 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                        {
                                            prevContour.x0 = x1Par;
                                            prevContour.y0 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                        {
                                            prevContour.x1 = x0Par;
                                            prevContour.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                        {
                                            prevContour.x1 = x1Par;
                                            prevContour.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        if (intI == 0)
                                        {
                                            shape.gContours[shape.gContours.Count - 1] = prevContour;
                                        }
                                        else if (intI == shape.gContours.Count - 1)
                                        {
                                            shape.gContours[intI - 1] = prevContour;
                                        }
                                        else
                                        {
                                            shape.gContours[intI - 1] = prevContour;
                                        }
                                        shape.gContours[intI] = contour;
                                    }
                                    else
                                    {
                                        Shape.Contour contourToAddUp = new Shape.Contour();
                                        contourToAddUp.line = true;
                                        contourToAddUp.apronLink = "";
                                        contourToAddUp.processing = "";
                                        contourToAddUp.pricipalStruct = contour.pricipalStruct;
                                        contourToAddUp.code = contour.code;

                                        Shape.Contour contourToAdd = new Shape.Contour();
                                        contourToAdd.line = true;
                                        contourToAdd.apronLink = "";
                                        contourToAdd.processing = "";
                                        contourToAdd.pricipalStruct = contour.pricipalStruct;
                                        contourToAdd.code = contour.code;

                                        if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                        {
                                            contourToAddUp.x0 = prevContour.x0;
                                            contourToAddUp.y0 = prevContour.y0;
                                            contourToAddUp.x1 = prevContour.x0;
                                            contourToAddUp.y1 = prevContour.y0;

                                            contourToAdd.x0 = prevContour.x0;
                                            contourToAdd.y0 = prevContour.y0;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                        {
                                            contourToAddUp.x0 = prevContour.x1;
                                            contourToAddUp.y0 = prevContour.y1;
                                            contourToAddUp.x1 = prevContour.x1;
                                            contourToAddUp.y1 = prevContour.y1;

                                            contourToAdd.x0 = prevContour.x1;
                                            contourToAdd.y0 = prevContour.y1;
                                            contourToAdd.x1 = x1Par;
                                            contourToAdd.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }
                                        else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                        {
                                            contourToAddUp.x0 = prevContour.x0;
                                            contourToAddUp.y0 = prevContour.y0;
                                            contourToAddUp.x1 = prevContour.x0;
                                            contourToAddUp.y1 = prevContour.y0;

                                            contourToAdd.x0 = prevContour.x0;
                                            contourToAdd.y0 = prevContour.y0;
                                            contourToAdd.x1 = x0Par;
                                            contourToAdd.y1 = y0Par;
                                            contour.x0 = x0Par;
                                            contour.y0 = y0Par;
                                        }
                                        else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                        {
                                            contourToAddUp.x0 = prevContour.x1;
                                            contourToAddUp.y0 = prevContour.y1;
                                            contourToAddUp.x1 = prevContour.x1;
                                            contourToAddUp.y1 = prevContour.y1;

                                            contourToAdd.x0 = prevContour.x1;
                                            contourToAdd.y0 = prevContour.y1;
                                            contourToAdd.x1 = x1Par;
                                            contourToAdd.y1 = y1Par;
                                            contour.x1 = x1Par;
                                            contour.y1 = y1Par;
                                        }

                                        shape.gContours[intI] = contour;
                                        shape.gContours.Insert(intI, contourToAdd);
                                        upperContour.Insert(intI + 1, contourToAddUp);
                                    }
                                }
                            }
                            #endregion

                            #region IsVeletta

                            else if (shape.IsVeletta)
                            {
                                if (dicVelette.ContainsKey(shape.gVeletta.link))
                                {
                                    string valTmp = dicVelette[shape.gVeletta.link];
                                    string[] valori = valTmp.Split('#');
                                    double x0Tmp = 0;
                                    double y0Tmp = 0;
                                    double x1Tmp = 0;
                                    double y1Tmp = 0;
                                    double angoloTmp = 0;
                                    double.TryParse(valori[0], out x0Tmp);
                                    double.TryParse(valori[1], out y0Tmp);
                                    double.TryParse(valori[2], out x1Tmp);
                                    double.TryParse(valori[3], out y1Tmp);
                                    double.TryParse(valori[4], out angoloTmp);

                                    Transformation transformation = new Transformation();
                                    transformation.Rotation(Utility.DegToRad(angoloTmp), Vector3D.AxisZ);
                                    ////transformation.RotateZ(angoloTmp);

                                    Point3D p0 = new Point3D(contour.x0, contour.y0);
                                    Point3D p1 = new Point3D(contour.x1, contour.y1);

                                    ////transformation.Apply(ref p0);
                                    ////transformation.Apply(ref p1);
                                    p0 = transformation * p0;
                                    p1 = transformation * p1;

                                    ////transformation.Reset();
                                    transformation.Identity();

                                    ////transformation.Translate(x0Tmp, y0Tmp, 0);
                                    transformation.Translation(x0Tmp, y0Tmp, 0);

                                    ////transformation.Apply(ref p0);
                                    ////transformation.Apply(ref p1);
                                    p0 = transformation * p0;
                                    p1 = transformation * p1;

                                    contour.x0 = p0.X;
                                    contour.x1 = p1.X;
                                    contour.y0 = p0.Y;
                                    contour.y1 = p1.Y;

                                    shape.gContours[intI] = contour;

                                }

                                ////if (intI == 0)
                                ////{
                                ////    prevContour = (Shape.Contour)shape.gContours[shape.gContours.Count - 1];
                                ////    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                ////}
                                ////else if (intI == shape.gContours.Count - 1)
                                ////{
                                ////    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                ////    nextContour = (Shape.Contour)shape.gContours[0];
                                ////}
                                ////else
                                ////{
                                ////    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                ////    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                ////}

                                ////if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 ||
                                ////    prevContour.cutAngle_Offset != 0)
                                ////{
                                ////    if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                                ////        (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                                ////    {
                                ////        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                ////    }
                                ////    else
                                ////    {
                                ////        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                ////    }
                                ////}
                                ////else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 ||
                                ////         nextContour.cutAngle_Offset != 0)
                                ////{
                                ////    if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                                ////        (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                                ////    {
                                ////        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                ////    }
                                ////    else
                                ////    {
                                ////        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                ////    }
                                ////}
                                ////else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 ||
                                ////         contour.cutAngle_Offset != 0)
                                ////{
                                ////    contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                ////    contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                ////}
                                ////else
                                ////{
                                ////    //double x0Par = 0;
                                ////    //double y0Par = 0;
                                ////    //double x1Par = 0;
                                ////    //double y1Par = 0;

                                ////    //FindParallelLine(ref x0Par, ref y0Par, ref x1Par, ref y1Par, contour.x0, contour.y0, contour.x1, contour.y1, shape.gVeletta.height - shape.gThickness);

                                ////    //if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                ////    //{
                                ////    //    nextContour.x0 = x0Par;
                                ////    //    nextContour.y0 = y0Par;
                                ////    //    contour.x0 = x0Par;
                                ////    //    contour.y0 = y0Par;
                                ////    //}
                                ////    //else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                ////    //{
                                ////    //    nextContour.x0 = x1Par;
                                ////    //    nextContour.y0 = y1Par;
                                ////    //    contour.x1 = x1Par;
                                ////    //    contour.y1 = y1Par;
                                ////    //}
                                ////    //else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                ////    //{
                                ////    //    nextContour.x1 = x0Par;
                                ////    //    nextContour.y1 = y0Par;
                                ////    //    contour.x0 = x0Par;
                                ////    //    contour.y0 = y0Par;
                                ////    //}
                                ////    //else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                ////    //{
                                ////    //    nextContour.x1 = x1Par;
                                ////    //    nextContour.y1 = y1Par;
                                ////    //    contour.x1 = x1Par;
                                ////    //    contour.y1 = y1Par;
                                ////    //}

                                ////    //if (intI == 0)
                                ////    //{
                                ////    //    shape.gContours[intI + 1] = nextContour;
                                ////    //}
                                ////    //else if (intI == shape.gContours.Count - 1)
                                ////    //{
                                ////    //    shape.gContours[0] = nextContour; ;
                                ////    //}
                                ////    //else
                                ////    //{
                                ////    //    shape.gContours[intI + 1] = nextContour; ;
                                ////    //}

                                ////    //if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                ////    //{
                                ////    //    prevContour.x0 = x0Par;
                                ////    //    prevContour.y0 = y0Par;
                                ////    //    contour.x0 = x0Par;
                                ////    //    contour.y0 = y0Par;
                                ////    //}
                                ////    //else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                ////    //{
                                ////    //    prevContour.x0 = x1Par;
                                ////    //    prevContour.y0 = y1Par;
                                ////    //    contour.x1 = x1Par;
                                ////    //    contour.y1 = y1Par;
                                ////    //}
                                ////    //else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                ////    //{
                                ////    //    prevContour.x1 = x0Par;
                                ////    //    prevContour.y1 = y0Par;
                                ////    //    contour.x0 = x0Par;
                                ////    //    contour.y0 = y0Par;
                                ////    //}
                                ////    //else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                ////    //{
                                ////    //    prevContour.x1 = x1Par;
                                ////    //    prevContour.y1 = y1Par;
                                ////    //    contour.x1 = x1Par;
                                ////    //    contour.y1 = y1Par;
                                ////    //}

                                ////    //if (intI == 0)
                                ////    //{
                                ////    //    shape.gContours[shape.gContours.Count - 1] = prevContour;
                                ////    //}
                                ////    //else if (intI == shape.gContours.Count - 1)
                                ////    //{
                                ////    //    shape.gContours[intI - 1] = prevContour;
                                ////    //}
                                ////    //else
                                ////    //{
                                ////    //    shape.gContours[intI - 1] = prevContour;
                                ////    //}

                                ////    //down = true;
                                ////}

                            }

                            #endregion
                        }

                        if (shape.IsVeletta)
                        {
                            for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                            {

                                Shape.Contour contour = (Shape.Contour)shape.gContours[intI];

                                if (intI == 0)
                                {
                                    prevContour = (Shape.Contour)shape.gContours[shape.gContours.Count - 1];
                                    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                }
                                else if (intI == shape.gContours.Count - 1)
                                {
                                    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour)shape.gContours[0];
                                }
                                else
                                {
                                    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                }

                                if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 ||
                                    prevContour.cutAngle_Offset != 0)
                                {
                                    if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                                        (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                                    {
                                        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                    else
                                    {
                                        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                    }

                                    shape.gContours[intI] = contour;

                                }
                                else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 ||
                                         nextContour.cutAngle_Offset != 0)
                                {
                                    if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                                        (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                                    {
                                        contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    }
                                    else
                                    {
                                        contour.z1 += shape.gThickness; // shape.gVeletta.height;
                                    }

                                    shape.gContours[intI] = contour;
                                }
                                else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 ||
                                         contour.cutAngle_Offset != 0)
                                {
                                    contour.z0 += shape.gThickness; // shape.gVeletta.height;
                                    contour.z1 += shape.gThickness; // shape.gVeletta.height;

                                    shape.gContours[intI] = contour;
                                }
                                else
                                {
                                    double x0Par = 0;
                                    double y0Par = 0;
                                    double x1Par = 0;
                                    double y1Par = 0;

                                    FindParallelLine(ref x0Par, ref y0Par, ref x1Par, ref y1Par, contour.x0,
                                        contour.y0, contour.x1, contour.y1,
                                        shape.gVeletta.height - shape.gThickness);

                                    if (nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0)
                                    {
                                        nextContour.x0 = x0Par;
                                        nextContour.y0 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (nextContour.x0 == contour.x1 && nextContour.y0 == contour.y1)
                                    {
                                        nextContour.x0 = x1Par;
                                        nextContour.y0 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }
                                    else if (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0)
                                    {
                                        nextContour.x1 = x0Par;
                                        nextContour.y1 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (nextContour.x1 == contour.x1 && nextContour.y1 == contour.y1)
                                    {
                                        nextContour.x1 = x1Par;
                                        nextContour.y1 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }

                                    if (intI == 0)
                                    {
                                        shape.gContours[intI + 1] = nextContour;
                                    }
                                    else if (intI == shape.gContours.Count - 1)
                                    {
                                        shape.gContours[0] = nextContour;
                                        ;
                                    }
                                    else
                                    {
                                        shape.gContours[intI + 1] = nextContour;
                                        ;
                                    }

                                    if (prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0)
                                    {
                                        prevContour.x0 = x0Par;
                                        prevContour.y0 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (prevContour.x0 == contour.x1 && prevContour.y0 == contour.y1)
                                    {
                                        prevContour.x0 = x1Par;
                                        prevContour.y0 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }
                                    else if (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0)
                                    {
                                        prevContour.x1 = x0Par;
                                        prevContour.y1 = y0Par;
                                        contour.x0 = x0Par;
                                        contour.y0 = y0Par;
                                    }
                                    else if (prevContour.x1 == contour.x1 && prevContour.y1 == contour.y1)
                                    {
                                        prevContour.x1 = x1Par;
                                        prevContour.y1 = y1Par;
                                        contour.x1 = x1Par;
                                        contour.y1 = y1Par;
                                    }

                                    if (intI == 0)
                                    {
                                        shape.gContours[shape.gContours.Count - 1] = prevContour;
                                    }
                                    else if (intI == shape.gContours.Count - 1)
                                    {
                                        shape.gContours[intI - 1] = prevContour;
                                    }
                                    else
                                    {
                                        shape.gContours[intI - 1] = prevContour;
                                    }

                                    shape.gContours[intI] = contour;

                                    //down = true;
                                }
                            }
                        }

                        for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                        {

                            Shape.Contour contour = (Shape.Contour)shape.gContours[intI];

                            if (contour.apronLink != "")
                                lstVelette.Add(contour.apronLink);

                            bool down = false;

                            if (shape.IsVeletta)
                            {
                                if (intI == 0)
                                {
                                    prevContour = (Shape.Contour)shape.gContours[shape.gContours.Count - 1];
                                    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                }
                                else if (intI == shape.gContours.Count - 1)
                                {
                                    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour)shape.gContours[0];
                                }
                                else
                                {
                                    prevContour = (Shape.Contour)shape.gContours[intI - 1];
                                    nextContour = (Shape.Contour)shape.gContours[intI + 1];
                                }

                                if (prevContour.cutAngle_Angle != 0 || prevContour.cutAngle_Height != 0 ||
                                    prevContour.cutAngle_Offset != 0)
                                {
                                    //if ((prevContour.x0 == contour.x0 && prevContour.y0 == contour.y0) ||
                                    //    (prevContour.x1 == contour.x0 && prevContour.y1 == contour.y0))
                                    //{
                                    //    contour.z0 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                    //else
                                    //{
                                    //    contour.z1 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                }
                                else if (nextContour.cutAngle_Angle != 0 || nextContour.cutAngle_Height != 0 ||
                                         nextContour.cutAngle_Offset != 0)
                                {
                                    //if ((nextContour.x0 == contour.x0 && nextContour.y0 == contour.y0) ||
                                    //    (nextContour.x1 == contour.x0 && nextContour.y1 == contour.y0))
                                    //{
                                    //    contour.z0 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                    //else
                                    //{
                                    //    contour.z1 += shape.gThickness;// shape.gVeletta.height;
                                    //}
                                }
                                else if (contour.cutAngle_Angle != 0 || contour.cutAngle_Height != 0 ||
                                         contour.cutAngle_Offset != 0)
                                {
                                    //contour.z0 += shape.gThickness;// shape.gVeletta.height;
                                    //contour.z1 += shape.gThickness;// shape.gVeletta.height;
                                }
                                else
                                {
                                    down = true;
                                }
                            }

                            coloreSagoma = Color.Black;

                            if (contour.line)
                            {

                                DrawLine(contour, coloreSagoma, ref al, shape.gAngle);
                                if (shape.HasVeletta)
                                {
                                    DrawLine((Shape.Contour)upperContour[intI], coloreSagoma, ref alUp, shape.gAngle);
                                }

                                if (shape.gBackSplash && contour.join)
                                {

                                    Transformation transformation = new Transformation();

                                    ////transformation.RotateZ(-shape.gAngle);
                                    transformation.Rotation(Utility.DegToRad(-shape.gAngle), Vector3D.AxisZ);

                                    Point3D p0 = new Point3D(contour.x0, contour.y0);
                                    Point3D p1 = new Point3D(contour.x1, contour.y1);

                                    ////transformation.Apply(ref p0);
                                    ////transformation.Apply(ref p1);
                                    p0 = transformation * p0;
                                    p1 = transformation * p1;

                                    ////lAttacco = CreateEyeLine(p0, p1, coloreSagoma);
                                    lAttacco = CreateEyeLine(p0, p1, coloreSagoma);

                                }

                            }

                            else
                            {

                                DrawArc(contour, ref al, shape.gAngle, coloreSagoma);
                                if (shape.HasVeletta)
                                {
                                    DrawArc((Shape.Contour)upperContour[intI], ref alUp, shape.gAngle, coloreSagoma);
                                }


                            }

                            if (al.Count != 1)
                            {

                                if (id != contour.id)
                                {

                                    ArrL newArrL = new ArrL();
                                    newArrL.indice = al.Count - 1;

                                    if (contour.code == "LAM01")
                                    {

                                        newArrL.tipo = "LAM01";
                                        newArrL.cw = 0;
                                        newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;

                                    }

                                    else if (contour.pricipalStruct)
                                    {

                                        newArrL.tipo = "PS";
                                        newArrL.cw = 0;
                                        if (shape.IsVeletta)
                                        {
                                            newArrL.spessore = shape.gVeletta.height;
                                        }
                                        else
                                        {
                                            newArrL.spessore = shape.gThickness;
                                        }
                                        newArrL.passante = true;

                                    }

                                    else if (contour.code.Substring(0, 2) != "CP" &&
                                             contour.code != "SNP0002" &&
                                             contour.code != "SNP0008" &&
                                             contour.subType != "Groove" &&
                                             contour.subType != "Pocket")
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        //newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;
                                        newArrL.insertMode = contour.insertMode;

                                        if (contour.code == "SNP0004" && contour.insertMode == "RIBASSATO")
                                            bolRibasso = true;

                                    }

                                    else if (contour.code == "SNP0002")
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaB;
                                        newArrL.passante = false;
                                        newArrL.insertMode = contour.insertMode;
                                        bolBussola = true;

                                    }

                                    else if (contour.subType != "Pocket")
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaC;
                                        newArrL.passante = false;
                                        newArrL.insertMode = contour.insertMode;

                                    }

                                    else
                                    {

                                        newArrL.tipo = contour.code;
                                        newArrL.sottoTipo = contour.subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaT;
                                        newArrL.passante = false;
                                        newArrL.insertMode = contour.insertMode;
                                        bolTasca = true;

                                    }

                                    alIndice.Add(newArrL);

                                    id = contour.id;

                                }

                            }

                            else
                            {

                                ArrL newArrL = new ArrL();
                                newArrL.indice = 0;

                                if (contour.code == "LAM01")
                                {

                                    newArrL.tipo = "LAM01";
                                    newArrL.cw = 0;
                                    newArrL.spessore = shape.gThickness;
                                    newArrL.passante = true;

                                }

                                else if (contour.pricipalStruct)
                                {

                                    newArrL.tipo = "PS"; // contour.pricipalStruct ? "PS" : contour.code;
                                    newArrL.cw = 0;
                                    if (shape.IsVeletta)
                                    {
                                        //if (!down)
                                        //{

                                        // De Conti 20161124
                                        //newArrL.spessore = shape.gThickness + shape.gVeletta.height;
                                        newArrL.spessore = shape.gVeletta.height;


                                        //}
                                        ////else
                                        ////{
                                        ////    newArrL.spessore = shape.gVeletta.height + 10;
                                        ////}
                                    }
                                    else
                                    {
                                        newArrL.spessore = shape.gThickness;
                                    }
                                    newArrL.passante = true;

                                }

                                else if (contour.code.Substring(0, 2) != "CP" &&
                                         contour.code != "SNP0002" &&
                                         contour.code != "SNP0008" &&
                                         contour.subType != "Groove" &&
                                         contour.subType != "Pocket")
                                {

                                    newArrL.tipo = contour.code; // contour.pricipalStruct ? "PS" : contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    //newArrL.spessore = shape.gThickness;
                                    newArrL.passante = true;
                                    newArrL.insertMode = contour.insertMode;

                                    if (contour.code == "SNP0004" && contour.insertMode == "RIBASSATO")
                                        bolRibasso = true;

                                }

                                else if (contour.code == "SNP0002")
                                {

                                    newArrL.tipo = contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaB;
                                    newArrL.passante = false;
                                    newArrL.insertMode = contour.insertMode;
                                    bolBussola = true;

                                }

                                else if (contour.subType != "Pocket")
                                {

                                    newArrL.tipo = contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaC;
                                    newArrL.passante = false;
                                    newArrL.insertMode = contour.insertMode;

                                }

                                else
                                {

                                    newArrL.tipo = contour.code;
                                    newArrL.sottoTipo = contour.subType;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaT;
                                    newArrL.passante = false;
                                    newArrL.insertMode = contour.insertMode;
                                    bolTasca = true;

                                }

                                alIndice.Add(newArrL);

                                id = contour.id;

                            }

                            #region Has veletta

                            if (shape.HasVeletta)
                            {
                                if (alUp.Count != 1)
                                {
                                    if (idUp != ((Shape.Contour)upperContour[intI]).id)
                                    {

                                        ArrL newArrL = new ArrL();
                                        newArrL.indice = alUp.Count - 1;

                                        if (((Shape.Contour)upperContour[intI]).code == "LAM01")
                                        {

                                            newArrL.tipo = "LAM01";
                                            newArrL.cw = 0;
                                            newArrL.spessore = shape.gThickness;
                                            newArrL.passante = true;

                                        }

                                        else if (((Shape.Contour)upperContour[intI]).pricipalStruct)
                                        {

                                            newArrL.tipo = "PS";
                                            newArrL.cw = 0;
                                            if (shape.IsVeletta)
                                            {
                                                newArrL.spessore = shape.gVeletta.height;
                                            }
                                            else
                                            {
                                                newArrL.spessore = shape.gThickness;
                                            }
                                            newArrL.passante = true;

                                        }

                                        else if (((Shape.Contour)upperContour[intI]).code.Substring(0, 2) != "CP" &&
                                                 ((Shape.Contour)upperContour[intI]).code != "SNP0002" &&
                                                 ((Shape.Contour)upperContour[intI]).code != "SNP0008" &&
                                                 ((Shape.Contour)upperContour[intI]).subType != "Groove" &&
                                                 ((Shape.Contour)upperContour[intI]).subType != "Pocket")
                                        {

                                            newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            //newArrL.spessore = shape.gThickness;
                                            newArrL.passante = true;
                                            newArrL.insertMode = contour.insertMode;

                                            if (((Shape.Contour)upperContour[intI]).code == "SNP0004" &&
                                                ((Shape.Contour)upperContour[intI]).insertMode == "RIBASSATO")
                                                bolRibasso = true;

                                        }

                                        else if (((Shape.Contour)upperContour[intI]).code == "SNP0002")
                                        {

                                            newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            newArrL.spessore = profonditaB;
                                            newArrL.passante = false;
                                            newArrL.insertMode = ((Shape.Contour)upperContour[intI]).insertMode;
                                            bolBussola = true;

                                        }

                                        else if (contour.subType != "Pocket")
                                        {

                                            newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            newArrL.spessore = profonditaC;
                                            newArrL.passante = false;
                                            newArrL.insertMode = ((Shape.Contour)upperContour[intI]).insertMode;

                                        }

                                        else
                                        {

                                            newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                            newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                            newArrL.cw = 1;
                                            newArrL.spessore = profonditaT;
                                            newArrL.passante = false;
                                            newArrL.insertMode = ((Shape.Contour)upperContour[intI]).insertMode;
                                            bolTasca = true;

                                        }

                                        alIndiceUp.Add(newArrL);

                                        idUp = ((Shape.Contour)upperContour[intI]).id;

                                    }

                                }

                                else
                                {

                                    ArrL newArrL = new ArrL();
                                    newArrL.indice = 0;

                                    if (((Shape.Contour)upperContour[intI]).code == "LAM01")
                                    {

                                        newArrL.tipo = "LAM01";
                                        newArrL.cw = 0;
                                        newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;

                                    }

                                    else if (((Shape.Contour)upperContour[intI]).pricipalStruct)
                                    {

                                        newArrL.tipo = "PS"; // contour.pricipalStruct ? "PS" : contour.code;
                                        newArrL.cw = 0;
                                        if (shape.IsVeletta)
                                        {
                                            //if (!down)
                                            //{
                                            newArrL.spessore = shape.gThickness + shape.gVeletta.height;
                                            //}
                                            ////else
                                            ////{
                                            ////    newArrL.spessore = shape.gVeletta.height + 10;
                                            ////}
                                        }
                                        else
                                        {
                                            newArrL.spessore = shape.gThickness;
                                        }
                                        newArrL.passante = true;

                                    }

                                    else if (((Shape.Contour)upperContour[intI]).code.Substring(0, 2) != "CP" &&
                                             ((Shape.Contour)upperContour[intI]).code != "SNP0002" &&
                                             ((Shape.Contour)upperContour[intI]).code != "SNP0008" &&
                                             ((Shape.Contour)upperContour[intI]).subType != "Groove" &&
                                             ((Shape.Contour)upperContour[intI]).subType != "Pocket")
                                    {

                                        newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                        // contour.pricipalStruct ? "PS" : contour.code;
                                        newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        //newArrL.spessore = shape.gThickness;
                                        newArrL.passante = true;
                                        newArrL.insertMode = contour.insertMode;

                                        if (((Shape.Contour)upperContour[intI]).code == "SNP0004" &&
                                            ((Shape.Contour)upperContour[intI]).insertMode == "RIBASSATO")
                                            bolRibasso = true;

                                    }

                                    else if (((Shape.Contour)upperContour[intI]).code == "SNP0002")
                                    {

                                        newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                        newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaB;
                                        newArrL.passante = false;
                                        newArrL.insertMode = ((Shape.Contour)upperContour[intI]).insertMode;
                                        bolBussola = true;

                                    }

                                    else if (contour.subType != "Pocket")
                                    {

                                        newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                        newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaC;
                                        newArrL.passante = false;
                                        newArrL.insertMode = ((Shape.Contour)upperContour[intI]).insertMode;

                                    }

                                    else
                                    {

                                        newArrL.tipo = ((Shape.Contour)upperContour[intI]).code;
                                        newArrL.sottoTipo = ((Shape.Contour)upperContour[intI]).subType;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaT;
                                        newArrL.passante = false;
                                        newArrL.insertMode = ((Shape.Contour)upperContour[intI]).insertMode;
                                        bolTasca = true;

                                    }

                                    alIndiceUp.Add(newArrL);

                                    idUp = ((Shape.Contour)upperContour[intI]).id;

                                }
                            }
                            #endregion Has veletta

                            //}
                        }

                        #endregion

                        id = -1;

                        #region Ciclo per ogni contorno delle bussole.

                        for (int intI = 0; intI <= shape.gBussole.Count - 1; intI++)
                        {

                            Shape.Bussola bussola = (Shape.Bussola)shape.gBussole[intI];

                            coloreSagoma = Color.Blue;

                            for (int intJ = 0; intJ < bussola.sides.Count; intJ++)
                            {

                                Shape.Contour contour = (Shape.Contour)bussola.sides[intJ];

                                if (contour.line)
                                {

                                    DrawLine(contour, coloreSagoma, ref alBussole, shape.gAngle);

                                }

                                else
                                {

                                    DrawArc(contour, ref alBussole, shape.gAngle, coloreSagoma);

                                }

                                if (alBussole.Count != 1)
                                {

                                    if (id != bussola.id)
                                    {

                                        ArrL newArrL = new ArrL();
                                        newArrL.indice = alBussole.Count - 1;

                                        newArrL.tipo = contour.code;
                                        newArrL.cw = 1;
                                        newArrL.spessore = profonditaB;
                                        newArrL.passante = false;

                                        alIndiceBussole.Add(newArrL);

                                        id = bussola.id;

                                    }

                                }

                                else
                                {

                                    ArrL newArrL = new ArrL();
                                    newArrL.indice = 0;

                                    newArrL.tipo = contour.code;
                                    newArrL.cw = 1;
                                    newArrL.spessore = profonditaB;
                                    newArrL.passante = false;

                                    alIndiceBussole.Add(newArrL);

                                    id = bussola.id;

                                }

                            }

                        }

                        #endregion

                        #region Definizione del materiale.

                        Material mat = null;

                        //string m = pathImage[intS];
                        using (Bitmap m = (Bitmap) Image.FromFile(pathImage[intS]))
                        {
                            if (m != null)
                            {
                                Bitmap bitmap = (Bitmap)BuildSquareBitmap(m);
                                {
                                    mat = new Material(bitmap);

                                    string matKey = shapes[intS].gId.ToString();
                                    mat.Description = matKey;
                                    mat.MinifyingFunction = textureFilteringFunctionType.Linear;
                                    if (!vppRender.Materials.ContainsKey(matKey))
                                    {
                                        vppRender.Materials.Add(matKey, mat);
                                    }
                                }


                            }

                            else
                            {
                                mat = new Material();
                                string matKey = "NOMAT";
                                mat.Description = matKey;
                                if (!vppRender.Materials.ContainsKey(matKey))
                                    vppRender.Materials.Add(matKey, mat);

                            }
                        }


                        #endregion

                        Mesh model = null; // Mesh contenente il modello da disegnare.
                        Mesh modelSopra = null; // Mesh contenente il modello da disegnare sopra.
                        Mesh modelRibasso = null; // Mesh contenente il modello del ribasso.
                        Mesh modelRibassoT = null; // Mesh contenente il modello del ribasso per le tasche.

                        if (shape.IsVeletta)
                        {
                            DisegnaFacciaSottoVeletta(al, alIndice, mat, vppRender, ref model);
                        }
                        else
                        {
                            //TODO:DEC
                            DisegnaFacciaSotto(al, alIndice, mat, vppRender, ref model);
                            //model.FlipNormal();
                            //model.Regen(CHORDAL_ERROR);
                        }
                        //if (model.Normals != null)
                        {
                            model.FlipNormal();
                            model.Regen(CHORDAL_ERROR);
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        Mesh[] models = null;

                        if (shape.IsVeletta)
                        {
                            models = DisegnaBordiVeletta(al, shape, mat, pathProfiliLav, vppRender, profonditaR,
                                profonditaT);
                        }
                        else if (shape.HasVeletta)
                        {
                            models = DisegnaBordi(al, alUp, shape, mat, pathProfiliLav, vppRender, profonditaR,
                                profonditaT);
                        }
                        else
                        {
                            models = DisegnaBordi(al, shape, mat, pathProfiliLav, vppRender, profonditaR, profonditaT);
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        //Mesh[] modelsFittizio = DisegnaRaccordiFittizi(al, shape, mat, pathProfiliLav, vppRender, profonditaR, profonditaT);
                        //prbMain.Value++;

                        Mesh[] modelsBussole = null;

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        if (bolBussola)
                            modelsBussole = DisegnaBordiBussole(alBussole, alIndiceBussole, shape, mat, vppRender);

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (shape.IsVeletta)
                        {
                            DisegnaFacciaSopraVeletta(al, alIndice, alBussole, alIndiceBussole, mat, vppRender,
                                ref modelSopra, ref alCanaletti);
                        }
                        else if (shape.HasVeletta)
                        {
                            DisegnaFacciaSopra(alUp, alIndiceUp, alBussole, alIndiceBussole, mat, vppRender,
                                ref modelSopra, ref alCanaletti);
                        }
                        else
                        {
                            DisegnaFacciaSopra(al, alIndice, alBussole, alIndiceBussole, mat, vppRender, ref modelSopra,
                                ref alCanaletti);
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        if (bolRibasso) DisegnaRibasso(al, alIndice, mat, vppRender, ref modelRibasso, profonditaR);

                        //prbMain.Value++;
                        CurrentTopRenderStep++;

                        if (bolTasca) DisegnaRibassoTasca(al, alIndice, mat, vppRender, ref modelRibassoT, profonditaT);

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        Mesh[] modelsCanaletti = null;

                        if (alCanaletti.Count > 0)
                        {

                            modelsCanaletti = DisegnaBordiCanaletti(alCanaletti, shape, mat, vppRender, profonditaC);

                        }

                        //prbMain.Value++; 
                        CurrentTopRenderStep++;


                        Mesh[] modelVGroove = null;

                        if (shape.gVGrooves.Count > 0)
                        {

                            modelVGroove = DisegnaVGroove(shape, mat, vppRender, shape.gAngle);

                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        #region Unione delle parti disegnate

                        //29/06/2015

                        //if (model != null) // Faccia sotto
                        //{
                        //    model.FlipNormal();
                        //    model.EdgeStyle = Mesh.edgeStyleType.Sharp;
                        //    model.ComputeEdges();
                        //    model.Regen(CHORDAL_ERROR);
                        //}
                        //

                        if (models != null)
                        {

                            for (int intI = 0; intI < models.Length; intI++)
                            {
                                if (model == null && intI == 0)
                                {
                                    model = models[intI];
                                }
                                else if (models[intI] != null)
                                {
                                    model.MergeWith(models[intI], true);
                                }
                            }

                        }
                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelsBussole != null)
                        {

                            for (int intI = 0; intI < modelsBussole.Length; intI++)
                            {

                                model.MergeWith(modelsBussole[intI], false);

                            }

                        }
                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelsCanaletti != null)
                        {

                            for (int intI = 0; intI < modelsCanaletti.Length; intI++)
                            {

                                model.MergeWith(modelsCanaletti[intI], false);

                            }

                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelVGroove != null)
                        {

                            for (int intI = 0; intI < modelVGroove.Length; intI++)
                            {

                                model.MergeWith(modelVGroove[intI], false);

                            }

                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        if (modelRibasso != null)
                        {

                            model.MergeWith(modelRibasso, false);

                        }

                        if (modelRibassoT != null)
                        {

                            model.MergeWith(modelRibassoT, false);

                        }

                        if (modelSopra != null)
                        {
                            if (shape.HasVeletta)
                            {
                                model.MergeWith(modelSopra, true);

                                //vppRender.Entities.Add(modelSopra);
                            }
                            else
                            {
                                if (model == null)
                                {
                                    model = modelSopra;
                                }
                                else
                                {
                                    //if (!shape.IsVeletta)
                                    model.MergeWith(modelSopra, true);
                                }
                            }
                        }

                        //prbMain.Value++;
                        CurrentTopRenderStep++;


                        #endregion

                        ////model.EdgeStyleMode = Mesh.edgeStyleType.Sharp;
                        /// 
                        /// 

                        model.UpdateNormals();

                        bool isMaterialApplied = false;


                        ////try
                        ////{
                        ////    var sectionPlane = (Plane) Plane.XY.Clone();
                        ////    //sectionPlane.Translate(0, 0, shape.gThickness / 2);
                        ////    sectionPlane.Translate(0, 0, 0.1);
                        ////    Mesh[] parts;
                        ////    var retVal = model.SplitBy(sectionPlane, false, out parts);

                        ////    string mirrorMatName = mat.Description + "_Mirror";

                        ////    if (!vppRender.Materials.ContainsKey(mirrorMatName))
                        ////    {
                        ////        var mirrorMat = (Material) mat.Clone();
                        ////        var texture = (Image) mat.TextureImage.Clone();
                        ////        //texture.RotateFlip(RotateFlipType.Rotate180FlipXY);
                        ////        mirrorMat.TextureImage = texture;
                        ////        vppRender.Materials.Add(mirrorMatName, mirrorMat);
                        ////    }

                        ////    parts[1].Weld();
                        ////    parts[1].EdgeStyle = Mesh.edgeStyleType.Sharp;
                        ////    parts[1].ComputeEdges();
                        ////    //parts[1].Scale(1, 1, -1);
                        ////    //parts[1].FlipOutside();
                        ////    //parts[1].UpdateNormals();
                        ////    //parts[1].Scale(1, 1, -1);
                        ////    //parts[1].ComputeEdges();
                        ////    parts[1].ApplyMaterial(mirrorMatName, textureMappingType.Cubic, 1, 1);
                        ////    parts[1].FlipNormal();
                        ////    parts[1].Regen(CHORDAL_ERROR);

                        ////    parts[0].Weld();
                        ////    //parts[0].FlipOutside();
                        ////    parts[0].ComputeEdges();
                        ////    parts[0].Regen(CHORDAL_ERROR);
                        ////    parts[0].ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);


                        ////    parts[0].MergeWith(parts[1], true);
                        ////    model = parts[0];
                        ////    isMaterialApplied = true;
                        ////}
                        ////catch (Exception ex)
                        ////{
                        ////    TraceLog.WriteLine("DrawTop splitBy Error. ", ex);

                        ////}

                        //if (!isMaterialApplied)
                        //model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);
                        model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1, model.BoxMin, model.BoxMax);


                        model.EdgeStyle = Mesh.edgeStyleType.Sharp;


                        //model.ComputeEdges();
                        //model.Regen(CHORDAL_ERROR);

                        //vppRender.Entities.RegenAllCurved(1e-3);
                        //vppRender.Entities.Regen();
                        //var eList = new List<Entity>();
                        //var layerList = new List<Layer>();
                        //layerList.AddRange(vppRender.Layers);
                        //eList.Add(model);
                        //string name = "Model " + intS + ".stl";
                        //var writer = new WriteSTL(eList, layerList, new Dictionary<string, Block>(), name);
                        //writer.DoWork();




                        //model.NormalAveragingMode = Mesh.normalAveragingType.AveragedByAngle;

                        #region Gestione BackSplash

                        if (shape.gBackSplash)
                        {

                            // Leggo il punto di partenza della linea di attacco per spostare la sagoma in 0, 0
                            ////Point3D pMin = lAttacco.Start;
                            Point3D pMin = lAttacco.StartPoint;

                            // Sposto la sagoma in 0, 0
                            ////model.Move(-pMin.X, -pMin.Y, -pMin.Z);
                            model.Translate(-pMin.X, -pMin.Y, -pMin.Z);

                            // Leggo l'angolo dell'entit� di attacco
                            ////double angolo = GetAngolo(lAttacco.Start.X, lAttacco.Start.Y, lAttacco.End.X, lAttacco.End.Y);
                            double angolo = GetAngolo(lAttacco.StartPoint.X, lAttacco.StartPoint.Y, lAttacco.EndPoint.X,
                                lAttacco.EndPoint.Y);

                            // Ruoto la sagoma di un angolo pari al negativo dell'angolo dell'entit� di attacco,
                            // cos� porto la sagoma ad avere l'entit� di attacco in corrispondenza dell'asse X
                            ////model.RotateZ(-angolo);
                            model.Rotate(Utility.DegToRad(-angolo), Vector3D.AxisZ);

                            // Ruoto la sagoma di un angolo di 90� intorno all'asse X
                            ////model.RotateX(90);
                            model.Rotate(Utility.DegToRad(90), Vector3D.AxisX);

                            // Ruoto la sagoma di un angolo pari all'angolo dell'entit� di attacco,
                            // cos� riporto la sagoma alla posizione di partenza
                            ////model.RotateZ(angolo);
                            model.Rotate(Utility.DegToRad(angolo), Vector3D.AxisZ);

                            // Risposto la sagoma alla posizione originale
                            ////model.Move(pMin.X, pMin.Y, pMin.Z);
                            model.Translate(pMin.X, pMin.Y, pMin.Z);

                            try
                            {
                                var deltaZ = 0.0;
                                if (dSpessori.ContainsKey(shape.gShapeIdJoin))
                                {
                                    deltaZ = dSpessori[shape.gShapeIdJoin];
                                    // Alzo la sagoma di un valore pari allo spessore della sagoma di attacco
                                    ////model.MoveZ(dSpessori[shape.gShapeIdJoin]);
                                    model.Translate(0, 0, dSpessori[shape.gShapeIdJoin]);
                                }


                            }

                            catch (Exception ex)
                            {
                                TraceLog.WriteLine("DrawTop error ", ex);
                            }

                        }

                        #endregion

                        ////model.RotateZ(shape.gAngle);
                        model.Rotate(Utility.DegToRad(shape.gAngle), Vector3D.AxisZ);

                        ////model.Name = shape.gShapeNumber.ToString();
                        model.EntityData = shape.gShapeNumber.ToString();

                        if (elements.ContainsKey(shape.gId))
                        {
                            var item = new KeyValuePair<short, object>();
                            short key = 0;
                            object value = elements[shape.gId];

                            if (model.XData == null)
                            {
                                model.XData = new List<KeyValuePair<short, object>>();
                            }
                            model.XData.Add(new KeyValuePair<short, object>(key, value));
                        }



                        shapeTmp shpTmp = new shapeTmp();
                        if (shape.IsVeletta)
                            shpTmp.veletta = shape.gVeletta.link;
                        else
                            shpTmp.veletta = "";
                        shpTmp.velette = lstVelette;
                        shpTmp.rMesh = (Mesh)model.Clone();
                        shpTmp.rMesh.XData = model.XData;
                        lstObjects.Add(shpTmp);

                    }
                }

                for (int i = 0; i < lstObjects.Count; i++)
                {
                    try
                    {
                        shapeTmp shpTmp = lstObjects[i];



                        Mesh model = shpTmp.rMesh;

                        model.Regen(CHORDAL_ERROR);

                        vppRender.Entities.Add(model);


                        //vppRender.ZoomFit();
                        //vppRender.Invalidate();
                        //Application.DoEvents();


                        int shapeId = int.Parse(shpTmp.rMesh.EntityData.ToString());
                        var shape = shapes.FirstOrDefault(s => s.gId == shapeId);

                        if (shape != null && (shape.IsVeletta || shape.gBackSplash))
                        {
                            NormalizeMesh(model, vppRender);
                        }



                    }
                    catch (Exception ex)
                    {
                        TraceLog.WriteLine(string.Format("DrawTop error mesh #", i), ex);
                    }


                }

                #endregion


            }
            catch (Exception ex)
            {

                TraceLog.WriteLine("DrawTop Error. ", ex);
            }
            finally
            {
                //vppRender.SetView(viewType.Isometric);

                vppRender.DisplayMode = displayType.Rendered;



                //TODO DeConti: Scopo??
                ////vppRender.RescaleModel();
                vppRender.Entities.Regen();

                vppRender.Viewports[0].OriginSymbol.Visible = false;

                vppRender.Invalidate();

                //prbMain.Visible = false;

                OnTopRenderCompleted();
            }
        }




        /// <summary>
        /// Procedura inserimento di un raccordo da 1mm sui lati consecutivi con lavorazione
        /// </summary>
        /// <param name="shape"></param>
		private void InsertFillet(Shape shape)
		{
			try
			{

				ArrayList al = new ArrayList();
				bool raccordi = true;
                bool nextIsPrincipalStruct = true;

				while (raccordi)
				{
				    int iteration = 0;
					raccordi = false;

					for (int intI = 0; intI < shape.gContours.Count; intI++)
					{
						Shape.Contour c0 = (Shape.Contour)shape.gContours[intI];
						Shape.Contour c1 = new Shape.Contour();

						if (c0.pricipalStruct && c0.line && c0.processing != "")
						{

							if (intI < shape.gContours.Count - 1)
							{
								//se l'elemento successivo non � pi� la struttutra principale provo con il primo elemento
                                nextIsPrincipalStruct = true;
                                if (!((Shape.Contour)shape.gContours[intI + 1]).pricipalStruct)
                                {
                                    c1 = (Shape.Contour)shape.gContours[0];
                                    nextIsPrincipalStruct = false;
                                }
                                else
                                    c1 = (Shape.Contour)shape.gContours[intI + 1];

							}
							else //ultimo elemento
							{
								if (al.Count>0)
								    c1 = (Shape.Contour)al[0];
								else
									c1 = (Shape.Contour)shape.gContours[0];
							}

							//verifico che non abbiano lo stesso angolo
							double ac0 = Utils.GetAngolo(c0.x0, c0.y0, c0.x1, c0.y1);
							double ac1 = Utils.GetAngolo(c1.x0, c1.y0, c1.x1, c1.y1);
							if (Math.Round(Math.Abs(ac0), 2) == 360) ac0 = 0;
							if (Math.Round(Math.Abs(ac1), 2) == 360) ac1 = 0;

							if (Math.Abs(Math.Round(Math.Abs(ac0), 2) - Math.Round(Math.Abs(ac1), 2)) < ANGLE_TOOLERANCE ||
								Math.Abs(Math.Round(Math.Abs(ac0), 2) - Math.Round(Math.Abs(ac1), 2)) > 360 - ANGLE_TOOLERANCE)
							{
								al.Add(c0);
								continue;
							}


							if (c1.pricipalStruct && c1.line && c1.processing == c0.processing)
							{
								double p0x = 0, p0y = 0, p1x = 0, p1y = 0, xc = 0, yc = 0, ang0 = 0, ang1 = 0;
								Utils.Fillet(c0.x0, c0.y0, c0.x1, c0.y1,
												  c1.x0, c1.y0, c1.x1, c1.y1,
												  0.5, ref xc, ref yc,
												  ref p0x, ref p0y,
												  ref p1x, ref p1y,
												  ref ang0, ref ang1);

								double dblA0 = Utils.GetAngolo(xc, yc, p0x, p0y);
								double dblA1 = Utils.GetAngolo(xc, yc, p1x, p1y);

								Shape.Contour cArc = new Shape.Contour();
								cArc.processing = c0.processing;
								cArc.pricipalStruct = c0.pricipalStruct;
								cArc.code = "Penisola";
								cArc.startAngle = dblA0;
								cArc.endAngle = dblA1;
								cArc.r = 0.5;
								cArc.x0 = p0x;
								cArc.y0 = p0y;
								cArc.x1 = p1x;
								cArc.y1 = p1y;
								cArc.xC = xc;
								cArc.yC = yc;

                                // De conti
                                ////cArc.cw = 0;
								cArc.cw = (dblA1 > dblA0) ? 0 : 1;

								c0.x1 = p0x;
								c0.y1 = p0y;

								c1.x0 = p1x;
								c1.y0 = p1y;

								if (intI == shape.gContours.Count - 1)
								{
									//se si sta raccordando l'ultimo elemento con il primo, elimino il primo elemento che era gi� stato inserito e lo rimetto corretto
									al.RemoveAt(0);
									al.Add(c0);
									al.Add(cArc);
									al.Insert(0, c1);

								}
								else
								{
									al.Add(c0);
									al.Add(cArc);
									al.Add(c1);
								}

								raccordi = true;

                                //13/05/2010 aggiunto variabile nextIsPrincipalStruct. Prima ogni volta si aumentava l'indice
                                //cos� facendo per� se il raccordo era tra l'ultimo elemento "principalstructure" e l'elemento 0
                                //veniva saltato il primo elemento non appartenente a principalstructure.
                                //Per esempio: rettangolo con un raccordo e lavorazione su tutti i lati e con uno o pi� fori. quando si raccorda
                                //l'ultimo lato della sagoma con l'elemento 0 se si fa automaticamente intI++ si perde il primo lato del primo foro
                                if(nextIsPrincipalStruct)
                                    intI++;
							}
							else
							{
								al.Add(c0);
							}

						}
						else
						{
							al.Add(shape.gContours[intI]);
						}

					}

					shape.gContours.Clear();

					for (int i = 0; i < al.Count; i++)
						shape.gContours.Add(al[i]);

					al.Clear();

				}

			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("InsertFillet Error. ", ex);
			}

		}

        /// <summary>
        /// Procedura di disegno della base della sagoma.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="alIndice">ArrayList degli indici delle sagome, utilizzato per disegnare una sagoma alla volta e tagliare.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="model">Modello disegnato.</param>
        private void DisegnaFacciaSotto(ArrayList al, ArrayList alIndice, Material mat, ViewportLayout vppRender, ref Mesh model)
        {
            try
            {

                #region Ciclo per trovare il numero di elementi da restituire
                int intCount = 0;

                for (int intI = 0; intI < alIndice.Count; intI++)
                {

                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo != "PS" && arrL.passante)
                    {

                        intCount++;

                    }

                }
                #endregion

                Point3D[] profiloEsterno = null;
                Point3D[][] profiloInterno = new Point3D[intCount][];
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));
                int intJ = -1;

                #region Ciclo per creare i contorni della sagoma principale e delle sagome figlie.

                var alToList = al.Cast<ICurve>().ToList();

                for (int intI = 0; intI < alIndice.Count; intI++)
                {

                    ArrL arrL = (ArrL)alIndice[intI];



                    if (arrL.tipo == "PS" || arrL.tipo == "LAM01")
                    {

                        ////profiloEsterno = ViewportProfessional.MakeLoop(al, arrL.indice, CHORDAL_ERROR, false);
                        profiloEsterno = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);

                    }

                    else if (arrL.passante)
                    {

                        intJ++;

                        ////profiloInterno[intJ] = ViewportProfessional.MakeLoop(al, arrL.indice, CHORDAL_ERROR, true);
                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);

                    }

                }

                #endregion

                ////model = new RichMesh(mat);
                model = new Mesh(Mesh.natureType.RichSmooth);

                LinearPath path = new LinearPath(profiloEsterno);
                if (path.IsOrientedClockwise(Plane.XY))
                {
                    path.Reverse();
                    path.Regen(CHORDAL_ERROR);
                    profiloEsterno = (Point3D[])path.Vertices.Clone();
                }

                var profiloInternoList = GetListOfListOfPoints(profiloInterno); 

                if (intJ == -1)
                {

                    //normal = Vector3D.CalcNormal(profiloEsterno[0], profiloEsterno[1], profiloEsterno[2]);
                    //direzione = Vector3D.CalcNormal(profiloEsterno[1], profiloEsterno[2], profiloEsterno[3]);
                    //model.MakeFace(profiloEsterno, null, normal);                                       // La sagoma della base non contiene sagome figlie.
                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // La sagoma della base non contiene sagome figlie.
                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);                                       // La sagoma della base non contiene sagome figlie.

                }

                else
                {

                    try
                    {

                        ////model.MakeFace(profiloEsterno, profiloInterno, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                         // La sagoma della base contiene sagome figlie, quindi bisogna forare.
                        model = Mesh.CreatePlanar(profiloEsterno.ToList(), profiloInternoList, Mesh.natureType.RichSmooth);                                       // La sagoma della base non contiene sagome figlie.
                    }

                    catch(Exception ex)                                                                       // Errore
                    {

                        System.Diagnostics.Debug.WriteLine(ex.ToString());

                        #region Stampo in output la lista dei punti dei profili

                        System.Diagnostics.Debug.WriteLine("Profilo esterno faccia sotto");

                        for (int intI = 0; intI < profiloEsterno.Length; intI++)
                        {

                            System.Diagnostics.Debug.WriteLine(profiloEsterno[intI].ToString());

                        }

                        System.Diagnostics.Debug.WriteLine("Profilo interno faccia sotto");

                        for (int intI = 0; intI < profiloInterno.Length; intI++)
                        {

                            for (int intK = 0; intK < profiloInterno[intI].Length; intK++)
                            {

                                System.Diagnostics.Debug.WriteLine(profiloInterno[intI][intK].ToString());

                            }

                        }

                        #endregion

                        ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                        model = Mesh.CreatePlanar(profiloEsterno, null, Mesh.natureType.RichSmooth);                                       // La sagoma della base non contiene sagome figlie.
                    }

                }

                ////model.SmoothingAngle = Utility.DegToRad(20);
                ////APPLY MATERIAL
                ////model.MaterialName = mat.Description;
                ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                model.SmoothingAngle = Utility.DegToRad(20);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaFacciaSotto Error. ", ex);
            }

        }

        /// <summary>
        /// Procedura di disegno della base della sagoma.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="alIndice">ArrayList degli indici delle sagome, utilizzato per disegnare una sagoma alla volta e tagliare.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="model">Modello disegnato.</param>
        private void DisegnaFacciaSottoVeletta(ArrayList al, ArrayList alIndice, Material mat, ViewportLayout vppRender, ref Mesh model)
        {

            try
            {

                #region Ciclo per trovare il numero di elementi da restituire
                int intCount = 0;
                double altezza = 0;

                for (int intI = 0; intI < alIndice.Count; intI++)
                {

                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo != "PS" && arrL.passante)
                    {

                        intCount++;

                    }

                    else if (arrL.tipo == "PS")
                    {
                        altezza = arrL.spessore;
                    }
                }
                #endregion

                Point3D[] profiloEsterno = null;
                Point3D[][] profiloInterno = new Point3D[intCount][];
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));
                int intJ = -1;

                #region Ciclo per creare i contorni della sagoma principale e delle sagome figlie.

                var alToList = al.Cast<ICurve>().ToList();

                for (int intI = 0; intI < alIndice.Count; intI++)
                {

                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo == "PS" || arrL.tipo == "LAM01")
                    {

                        var loop =  ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);
                        profiloEsterno = new Point3D[loop.Length];
                        for(int i = 0; i< loop.Length; i++)
                        {
                            Point3D newPoint = new Point3D(loop[i].ToArray());
                            profiloEsterno[i] = newPoint;
                        }

                    }

                    else if (arrL.passante)
                    {

                        intJ++;

                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);

                    }

                }

                #endregion

                ////model = new Mesh(mat);
                model = new Mesh();

                var profiloInternoList = GetListOfListOfPoints(profiloInterno);
                if (intJ == -1)
                {
                    //TODO: De Conti 20161119
                    double maxZ = 0;
                    foreach (var point in profiloEsterno)
                    {
                        if (point.Z > maxZ)
                            maxZ = point.Z;
                    }
                    foreach (var point in profiloEsterno)
                    {
                        point.Z = maxZ;
                    }


                    //normal = Vector3D.CalcNormal(profiloEsterno[0], profiloEsterno[1], profiloEsterno[2]);
                    //direzione = Vector3D.CalcNormal(profiloEsterno[1], profiloEsterno[2], profiloEsterno[3]);
                    //model.MakeFace(profiloEsterno, null, normal);                                       // La sagoma della base non contiene sagome figlie.
                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // La sagoma della base non contiene sagome figlie.
                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);                                       // La sagoma della base non contiene sagome figlie.

                }

                else
                {

                    try
                    {

                        ////model.MakeFace(profiloEsterno, profiloInterno, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                         // La sagoma della base contiene sagome figlie, quindi bisogna forare.
                        model = Mesh.CreatePlanar(profiloEsterno.ToList(), profiloInternoList, Mesh.natureType.RichSmooth);                                       // La sagoma della base non contiene sagome figlie.
                    }

                    catch (Exception ex)                                                                       // Errore
                    {

                        System.Diagnostics.Debug.WriteLine(ex.ToString());

                        #region Stampo in output la lista dei punti dei profili

                        System.Diagnostics.Debug.WriteLine("Profilo esterno faccia sotto");

                        for (int intI = 0; intI < profiloEsterno.Length; intI++)
                        {

                            System.Diagnostics.Debug.WriteLine(profiloEsterno[intI].ToString());

                        }

                        System.Diagnostics.Debug.WriteLine("Profilo interno faccia sotto");

                        for (int intI = 0; intI < profiloInterno.Length; intI++)
                        {

                            for (int intK = 0; intK < profiloInterno[intI].Length; intK++)
                            {

                                System.Diagnostics.Debug.WriteLine(profiloInterno[intI][intK].ToString());

                            }

                        }

                        #endregion

                        ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                        model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);                                       // La sagoma della base non contiene sagome figlie.
                    }

                }

                model.SmoothingAngle = Utility.DegToRad(20);
                ////APPLY MATERIAL
                ////model.MaterialName = mat.Description;
                ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                ////model.MoveZ(-altezza);
                model.Translate(0, 0, -altezza);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaFacciaSotto Error. ", ex);
            }

        }

        private IList<IList<Point3D>> GetListOfListOfPoints(Point3D[][] pointsArray, bool clockwiseOriented = true)
        {
            IList<IList<Point3D>> newList = new List<IList<Point3D>>();

            if (pointsArray.Length > 0)
            {
                for (int i = 0; i < pointsArray.GetLength(0); i++)
                {
                    var points = pointsArray[i];
                    
                    // De Conti 2016/02/08 per problema sorto su laminazioni (points null)
                    if (points == null) continue;


                    var pointsList = points.ToList();

                    if (clockwiseOriented)
                    {
                        LinearPath path = new LinearPath(pointsList);
                        if (!path.IsOrientedClockwise(Plane.XY))
                        {
                            path.Reverse();
                            pointsList = path.Vertices.ToList();
                        }
                    }
                    newList.Add(pointsList);
                }
            }
            return newList;
        }

        /// <summary>
        /// Procedura di disegno della base superiore della sagoma.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="alIndice">ArrayList degli indici delle sagome, utilizzato per disegnare una sagoma alla volta e tagliare.</param>
        /// <param name="alBussole">ArrayList delle entit� che compongono le bussole.</param>
        /// <param name="alIndiceBussole">ArrayList degli indici delle bussole, utilizzato per disegnare le bussole singolarmente per tagliarle.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="model">Modello disegnato.</param>
        /// <param name="alCanaletti">ArrayList delle entit� che compongono i canaletti.</param>
        private void DisegnaFacciaSopra(ArrayList al, ArrayList alIndice, ArrayList alBussole, ArrayList alIndiceBussole, Material mat, ViewportLayout vppRender, ref Mesh model, ref ArrayList alCanaletti)
        {

            try
            {

                Point3D[] profiloEsterno = null;

                int iCount = -1;
                for (int intI = 0; intI < alIndice.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndice[intI];

                    if ((arrL.tipo != "SNP0004" || arrL.insertMode != "RIBASSATO") &&
                        arrL.sottoTipo != "Groove" &&
                        (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole" && arrL.sottoTipo != "Pocket")))
                        iCount++;

                }

                if (iCount == 0) iCount++;

                Point3D[][] profiloInterno = new Point3D[alIndiceBussole.Count + iCount][];
                //Point3D[][] profiloInterno = new Point3D[alIndiceBussole.Count + alIndice.Count - 1][];
                
                int intJ = -1;

                double altezza = 0;

                #region Ciclo per creare i contorni della sagoma principale e delle sagome figlie.

                var alToList = al.Cast<ICurve>().ToList();

                for (int intI = 0; intI < alIndice.Count; intI++)
                {

                    ArrL arrL = (ArrL)alIndice[intI];

                    List<ICurve> currentSegment = new List<ICurve>();
                    int nextIndex = alToList.Count;
                    if (intI < alIndice.Count - 1)
                    {
                         nextIndex = ((ArrL) alIndice[intI + 1]).indice;
                    }
                    for (int i = arrL.indice; i < nextIndex; i++)
                    {
                        currentSegment.Add(alToList[i]);
                    }

                    if (arrL.tipo == "PS")
                    {

                        altezza = arrL.spessore;

                        //profiloEsterno = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);
                        profiloEsterno = ViewportLayout.MakeLoop(currentSegment, 0, CHORDAL_ERROR, false);

                    }

                    else if (arrL.tipo == "LAM01")
                    {

                        altezza = -arrL.spessore;

                        //profiloEsterno = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);
                        profiloEsterno = ViewportLayout.MakeLoop(currentSegment, 0, CHORDAL_ERROR, false);

                    }

                    else if (arrL.tipo.Substring(0, 2) != "CP" &&
                             arrL.tipo != "SNP0002" && arrL.sottoTipo != "Groove" && arrL.tipo != "SNP0004" &&
                             (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole" && arrL.sottoTipo != "Pocket"))) //Aggiunto pocket De NTI 20151006  && arrL.sottoTipo != "Pocket"
                    {

                        intJ++;

                        //profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);
                        profiloInterno[intJ] = ViewportLayout.MakeLoop(currentSegment, 0, CHORDAL_ERROR, true);

                    }
                    else if (arrL.tipo == "SNP0006" && arrL.sottoTipo == "Pocket") //Aggiunto pocket De NTI 20151006  && arrL.sottoTipo != "Pocket"
                    {
                    }
                    else if (arrL.tipo == "SNP0004" && arrL.insertMode != "RIBASSATO" &&
                             (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole")))
                    {

                        intJ++;

                        //profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);
                        profiloInterno[intJ] = ViewportLayout.MakeLoop(currentSegment, 0, CHORDAL_ERROR, true);

                    }

                    else if (arrL.tipo == "SNP0004" && arrL.insertMode == "RIBASSATO")
                    {
                    }

                    else if (arrL.sottoTipo == "Groove")            // Canaletto di uno scivolo con/senza tasca
                    {
                    }

                    else if (arrL.tipo == "SNP0006" && arrL.sottoTipo == "SinkHole")
                    {
                    }

                    else                                            // Canaletto
                    {

                        ArrayList alCanaletto = new ArrayList();
                        ArrayList pCan = new ArrayList();

                        int intCountIndiceMax = intI != alIndice.Count - 1 ? ((ArrL)alIndice[intI + 1]).indice : al.Count;

                        #region Ciclo per copiare gli elementi in un nuovo ArrayList che contenga solo i canaletti
                        for (int intK = arrL.indice; intK < intCountIndiceMax; intK++)
                        {

                            Entity eTmp = (Entity)al[intK];

                            if (eTmp.GetType() == typeof(Line))
                            {

                                double x0 = 0;
                                double y0 = 0;
                                double x1 = 0;
                                double y1 = 0;

                                Line l0 = (Line)eTmp;
                                FindParallelLine(ref x0, ref y0, ref x1, ref y1, l0.StartPoint.X, l0.StartPoint.Y, l0.EndPoint.X, l0.EndPoint.Y, 5);

                                Point3D p0 = new Point3D(x0, y0);
                                Point3D p1 = new Point3D(x1, y1);

                                FindParallelLine(ref x0, ref y0, ref x1, ref y1, l0.StartPoint.X, l0.StartPoint.Y, l0.EndPoint.X, l0.EndPoint.Y, -5);

                                Point3D p2 = new Point3D(x1, y1);
                                Point3D p3 = new Point3D(x0, y0);

                                //Line l1 = CreateEyeLine(p0, p1, Color.Black);
                                Line l1 = CreateEyeLine(p0, p1, Color.Black);


                                ////Line l2 = CreateEyeLine(p2, p3, Color.Black);
                                Line l2 = CreateEyeLine(p2, p3, Color.Black);

                                alCanaletto.Add(l1);
                                alCanaletto.Add(l2);

                                bool bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(l0.StartPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = l0.StartPoint;
                                    arrLC.punto1 = p0;
                                    arrLC.punto2 = p3;

                                    pCan.Add(arrLC);

                                }

                                bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(l0.EndPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = l0.EndPoint;
                                    arrLC.punto1 = p1;
                                    arrLC.punto2 = p2;

                                    pCan.Add(arrLC);

                                }

                            }

                            else
                            {

                                Arc a0 = (Arc)eTmp;

                                ////Arc a1 = new Arc(a0.Center.X, a0.Center.Y, a0.Radius + 5, a0.StartAngle, a0.EndAngle, Color.Black);
                                ////Arc a2 = new Arc(a0.Center.X, a0.Center.Y, a0.Radius - 5, a0.StartAngle, a0.EndAngle, Color.Black);
                                /// 
                                double startAngle = a0.StartTangent.AngleInXY - Math.PI / 2;
                                double endAngle = a0.EndTangent.AngleInXY - Math.PI / 2;
                                Arc a1 = CreateEyeArc(a0.Center, a0.Radius + 5, startAngle, endAngle, Color.Black);
                                Arc a2 = CreateEyeArc(a0.Center, a0.Radius - 5, startAngle, endAngle, Color.Black);

                                ////var a = (Arc) a1.Clone();
                                ////a.Color = Color.Fuchsia;
                                ////a.ColorMethod = colorMethodType.byEntity;
                                ////a.LineWeight = 2;
                                ////a.LineWeightMethod = colorMethodType.byEntity;
                                ////vppRender.Entities.Add(a);

                                ////a = (Arc)a2.Clone();
                                ////a.Color = Color.Fuchsia;
                                ////a.ColorMethod = colorMethodType.byEntity;
                                ////a.LineWeight = 2;
                                ////a.LineWeightMethod = colorMethodType.byEntity;
                                ////vppRender.Entities.Add(a);

                                alCanaletto.Add(a1);
                                alCanaletto.Add(a2);

                                bool bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(a0.StartPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = a0.StartPoint;
                                    arrLC.punto1 = a1.StartPoint;
                                    arrLC.punto2 = a2.EndPoint;

                                    pCan.Add(arrLC);

                                }

                                bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(a0.EndPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = a0.EndPoint;
                                    arrLC.punto1 = a2.StartPoint;
                                    arrLC.punto2 = a1.EndPoint;

                                    pCan.Add(arrLC);

                                }

                            }

                        }
                        #endregion

                        // Aggiungo le entit� di chiusura del canaletto
                        for (int intC = 0; intC < pCan.Count; intC++)
                        {

                            ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                            Line l = CreateEyeLine(arrLC.punto1, arrLC.punto2, Color.Black);

                            alCanaletto.Add(l);

                        }

                        intJ++;

                        var alCanalettoToList = alCanaletto.Cast<ICurve>().ToList();


                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alCanalettoToList, 0, CHORDAL_ERROR, false);

                        alCanaletti.Add(alCanaletto);

                    }

                }

                #endregion

                #region Ciclo per creare i contorni delle bussole.

                var alBussoleToList = alBussole.Cast<ICurve>().ToList();

                for (int intI = 0; intI < alIndiceBussole.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndiceBussole[intI];

                    intJ++;

                    profiloInterno[intJ] = ViewportLayout.MakeLoop(alBussoleToList, arrL.indice, CHORDAL_ERROR, true);

                }

                #endregion

                model = new Mesh();
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));


                LinearPath path = new LinearPath(profiloEsterno);
                if (path.IsOrientedClockwise(Plane.XY))
                {
                    path.Reverse();
                    path.Regen(CHORDAL_ERROR);
                    profiloEsterno = path.Vertices;
                }

                if (intJ == -1)
                {

                    //normal.X = profiloEsterno[1].X - profiloEsterno[0].X;
                    //normal.Y = profiloEsterno[1].Y - profiloEsterno[0].Y;
                    //normal.Z = profiloEsterno[1].Z - profiloEsterno[0].Z;

                    //direzione.X = profiloEsterno[profiloEsterno.Length - 2].X - profiloEsterno[profiloEsterno.Length - 1].X;
                    //direzione.Y = profiloEsterno[profiloEsterno.Length - 2].Y - profiloEsterno[profiloEsterno.Length - 1].Y;
                    //direzione.Z = profiloEsterno[profiloEsterno.Length - 2].Z - profiloEsterno[profiloEsterno.Length - 1].Z;

                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // La sagoma della base non contiene sagome figlie.
                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);
                }

                else
                {

                    try
                    {
                        var profiloInternoList = GetListOfListOfPoints(profiloInterno, true);

                        ////model.MakeFace(profiloEsterno, profiloInterno, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                         // La sagoma della base contiene sagome figlie, quindi bisogna forare.


                        model = Mesh.CreatePlanar(profiloEsterno.ToList(), profiloInternoList, Mesh.natureType.RichSmooth);


                    }

                    catch                                                                       // Errore
                    {

                        #region Stampo in output la lista dei punti dei profili

                        System.Diagnostics.Debug.WriteLine("Profilo eEsterno faccia sopra");

                        for (int intI = 0; intI < profiloEsterno.Length; intI++)
                        {

                            System.Diagnostics.Debug.WriteLine(profiloEsterno[intI].ToString());

                        }

                        System.Diagnostics.Debug.WriteLine("Profilo interno faccia sopra");

                        for (int intI = 0; intI < profiloInterno.Length; intI++)
                        {

                            for (int intK = 0; intK < profiloInterno[intI].Length; intK++)
                            {

                                System.Diagnostics.Debug.WriteLine(profiloInterno[intI][intK].ToString());

                            }

                        }

                        #endregion

                        ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                        model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);
                    }

                }

                model.SmoothingAngle = Utility.DegToRad(20);
                ////APPLY MATERIAL
                ////model.MaterialName = mat.Description;
                ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                ////model.MoveZ(altezza);
                model.Translate(0, 0, altezza);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaFacciaSopra Error. ", ex);
            }

        }

        private static Arc CreateEyeArc(Point3D center, double radius, double startAngle, double endAngle, Color color)
        {
            double start = startAngle;
            double end = endAngle;
            bool reverse = false;

            ////////while (start <0)
            ////////{
            ////////    start += Math.PI * 2;
            ////////}
            ////////while (end <0)
            ////////{
            ////////    end += Math.PI * 2;
            ////////}


            ////////while (start >= Math.PI*2)
            ////////{
            ////////    start -= Math.PI*2;
            ////////}
            ////////while (end > Math.PI * 2)
            ////////{
            ////////    end -= Math.PI * 2;
            ////////}

            ////while (end < start)
            ////{
            ////    end += Math.PI * 2;
            ////    reverse = true;
            ////}

            ////if (end < start)
            ////    reverse = true;

            Arc arc;
            ////if (!reverse)
            ////{
            ////    arc = new Arc(center, radius, start, end);
            ////}
            ////else
            ////{
            ////    arc = new Arc(center, radius, end, start);
            ////    arc.Reverse();

            ////}

            arc = new Arc(center, radius, start, end);

            arc.Color = color;


            arc.Regen(CHORDAL_ERROR);
            return arc;
        }

        private static Line CreateEyeLine(Point3D startPoint, Point3D endPoint, Color color)
        {
            Line line = new Line(startPoint, endPoint);
            line.Color = color;
            line.Regen(CHORDAL_ERROR);
            return line;
        }

        private static Line CreateEyeLine(double xStart, double yStart, double xEnd, double yEnd, Color color)
        {
            Point3D startPoint = new Point3D(xStart, yStart);
            Point3D endPoint = new Point3D(xEnd, yEnd);
            return CreateEyeLine(startPoint, endPoint, color);
        }

        private static Line CreateEyeLine(double xStart, double yStart, double zStart, double xEnd, double yEnd, double zEnd, Color color)
        {
            Point3D startPoint = new Point3D(xStart, yStart, zStart);
            Point3D endPoint = new Point3D(xEnd, yEnd, zEnd);
            return CreateEyeLine(startPoint, endPoint, color);
        }


        /// <summary>
        /// Procedura di disegno della base superiore della sagoma.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="alIndice">ArrayList degli indici delle sagome, utilizzato per disegnare una sagoma alla volta e tagliare.</param>
        /// <param name="alBussole">ArrayList delle entit� che compongono le bussole.</param>
        /// <param name="alIndiceBussole">ArrayList degli indici delle bussole, utilizzato per disegnare le bussole singolarmente per tagliarle.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="model">Modello disegnato.</param>
        /// <param name="alCanaletti">ArrayList delle entit� che compongono i canaletti.</param>
        private void DisegnaFacciaSopraVeletta(ArrayList al, ArrayList alIndice, ArrayList alBussole, ArrayList alIndiceBussole, Material mat, ViewportLayout vppRender, ref Mesh model, ref ArrayList alCanaletti)
        {

            try
            {

                Point3D[] profiloEsterno = null;

                int iCount = -1;
                for (int intI = 0; intI < alIndice.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndice[intI];

                    if ((arrL.tipo != "SNP0004" || arrL.insertMode != "RIBASSATO") &&
                        arrL.sottoTipo != "Groove" &&
                        (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole")))
                        iCount++;

                }

                if (iCount == 0) iCount++;

                Point3D[][] profiloInterno = new Point3D[alIndiceBussole.Count + iCount][];
                //Point3D[][] profiloInterno = new Point3D[alIndiceBussole.Count + alIndice.Count - 1][];

                int intJ = -1;

                double altezza = 0;

                #region Ciclo per creare i contorni della sagoma principale e delle sagome figlie.

                var alToList = al.Cast<ICurve>().ToList();

                for (int intI = 0; intI < alIndice.Count; intI++)
                {

                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo == "PS")
                    {

                        altezza = arrL.spessore;

                        profiloEsterno = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);

                    }

                    else if (arrL.tipo == "LAM01")
                    {

                        altezza = -arrL.spessore;

                        profiloEsterno = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);

                    }

                    else if (arrL.tipo.Substring(0, 2) != "CP" &&
                             arrL.tipo != "SNP0002" && arrL.sottoTipo != "Groove" && arrL.tipo != "SNP0004" &&
                             (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole")))
                    {

                        intJ++;

                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);

                    }

                    else if (arrL.tipo == "SNP0004" && arrL.insertMode != "RIBASSATO" &&
                             (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole")))
                    {

                        intJ++;

                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);

                    }

                    else if (arrL.tipo == "SNP0004" && arrL.insertMode == "RIBASSATO")
                    {
                    }

                    else if (arrL.sottoTipo == "Groove")            // Canaletto di uno scivolo con/senza tasca
                    {
                    }

                    else if (arrL.tipo == "SNP0006" && arrL.sottoTipo == "SinkHole")
                    {
                    }

                    else                                            // Canaletto
                    {

                        ArrayList alCanaletto = new ArrayList();
                        ArrayList pCan = new ArrayList();

                        int intCountIndiceMax = intI != alIndice.Count - 1 ? ((ArrL)alIndice[intI + 1]).indice : al.Count;

                        #region Ciclo per copiare gli elementi in un nuovo ArrayList che contenga solo i canaletti
                        for (int intK = arrL.indice; intK < intCountIndiceMax; intK++)
                        {

                            Entity eTmp = (Entity)al[intK];

                            if (eTmp.GetType() == typeof(Line))
                            {

                                double x0 = 0;
                                double y0 = 0;
                                double x1 = 0;
                                double y1 = 0;

                                Line l0 = (Line)eTmp;
                                FindParallelLine(ref x0, ref y0, ref x1, ref y1, l0.StartPoint.X, l0.StartPoint.Y, l0.EndPoint.X, l0.EndPoint.Y, 5);

                                Point3D p0 = new Point3D(x0, y0);
                                Point3D p1 = new Point3D(x1, y1);

                                FindParallelLine(ref x0, ref y0, ref x1, ref y1, l0.StartPoint.X, l0.StartPoint.Y, l0.EndPoint.X, l0.EndPoint.Y, -5);

                                Point3D p2 = new Point3D(x1, y1);
                                Point3D p3 = new Point3D(x0, y0);

                                Line l1 = CreateEyeLine(p0, p1, Color.Black);

                                Line l2 = CreateEyeLine(p2, p3, Color.Black);

                                alCanaletto.Add(l1);
                                alCanaletto.Add(l2);

                                bool bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(l0.StartPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = l0.StartPoint;
                                    arrLC.punto1 = p0;
                                    arrLC.punto2 = p3;

                                    pCan.Add(arrLC);

                                }

                                bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(l0.EndPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = l0.EndPoint;
                                    arrLC.punto1 = p1;
                                    arrLC.punto2 = p2;

                                    pCan.Add(arrLC);

                                }

                            }

                            else
                            {

                                Arc a0 = (Arc)eTmp;
                                //////Arc a1 = new Arc(a0.Center.X, a0.Center.Y, a0.Radius + 5, a0.StartAngle, a0.EndAngle, Color.Black);
                                double startAngle = a0.StartTangent.AngleInXY - Math.PI / 2;
                                double endAngle = a0.EndTangent.AngleInXY - Math.PI / 2;
                                Arc a1 = CreateEyeArc(a0.Center, a0.Radius + 5, startAngle, endAngle, Color.Black);
                                Arc a2 = CreateEyeArc(a0.Center, a0.Radius - 5, startAngle, endAngle, Color.Black);


                                alCanaletto.Add(a1);
                                alCanaletto.Add(a2);

                                bool bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(a0.StartPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = a0.StartPoint;
                                    arrLC.punto1 = a1.StartPoint;
                                    arrLC.punto2 = a2.EndPoint;

                                    pCan.Add(arrLC);

                                }

                                bolAgg = true;

                                for (int intC = 0; intC < pCan.Count; intC++)
                                {

                                    ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                                    if (arrLC.puntoOr.Equals(a0.EndPoint))
                                    {

                                        pCan.RemoveAt(intC);
                                        bolAgg = false;

                                        break;

                                    }

                                }

                                if (bolAgg)
                                {

                                    ArrLineeCan arrLC = new ArrLineeCan();

                                    arrLC.puntoOr = a0.EndPoint;
                                    arrLC.punto1 = a2.StartPoint;
                                    arrLC.punto2 = a1.EndPoint;

                                    pCan.Add(arrLC);

                                }

                            }

                        }
                        #endregion

                        // Aggiungo le entit� di chiusura del canaletto
                        for (int intC = 0; intC < pCan.Count; intC++)
                        {

                            ArrLineeCan arrLC = (ArrLineeCan)pCan[intC];

                            Line l = CreateEyeLine(arrLC.punto1, arrLC.punto2, Color.Black);

                            alCanaletto.Add(l);

                        }

                        intJ++;

                        var alCanalettoToList = alCanaletto.Cast<ICurve>().ToList();
                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alCanalettoToList, 0, CHORDAL_ERROR, false);

                        alCanaletti.Add(alCanaletto);

                    }

                }

                #endregion

                #region Ciclo per creare i contorni delle bussole.

                var alBussoleToList = alBussole.Cast<ICurve>().ToList();

                for (int intI = 0; intI < alIndiceBussole.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndiceBussole[intI];

                    intJ++;

                    profiloInterno[intJ] = ViewportLayout.MakeLoop(alBussoleToList, arrL.indice, CHORDAL_ERROR, true);

                }

                #endregion

                model = new Mesh(Mesh.natureType.RichSmooth);

                Vector3D normal = new Vector3D(profiloEsterno[1].X - profiloEsterno[0].X, profiloEsterno[1].Y - profiloEsterno[0].Y, profiloEsterno[1].Z - profiloEsterno[0].Z);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(profiloEsterno[profiloEsterno.Length - 2].X - profiloEsterno[profiloEsterno.Length - 1].X, profiloEsterno[profiloEsterno.Length - 2].Y - profiloEsterno[profiloEsterno.Length - 1].Y, profiloEsterno[profiloEsterno.Length - 2].Z - profiloEsterno[profiloEsterno.Length - 1].Z);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                if (intJ == -1)
                {

                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // La sagoma della base non contiene sagome figlie.
                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);
                }

                else
                {

                    try
                    {
                        var profiloInternoList = GetListOfListOfPoints(profiloInterno);
                        ////model.MakeFace(profiloEsterno, profiloInterno, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                         // La sagoma della base contiene sagome figlie, quindi bisogna forare.
                        model = Mesh.CreatePlanar(profiloEsterno.ToList(), profiloInternoList, Mesh.natureType.RichSmooth);
                    }

                    catch                                                                       // Errore
                    {

                        #region Stampo in output la lista dei punti dei profili

                        System.Diagnostics.Debug.WriteLine("Profilo eEsterno faccia sopra");

                        for (int intI = 0; intI < profiloEsterno.Length; intI++)
                        {

                            System.Diagnostics.Debug.WriteLine(profiloEsterno[intI].ToString());

                        }

                        System.Diagnostics.Debug.WriteLine("Profilo interno faccia sopra");

                        for (int intI = 0; intI < profiloInterno.Length; intI++)
                        {

                            for (int intK = 0; intK < profiloInterno[intI].Length; intK++)
                            {

                                System.Diagnostics.Debug.WriteLine(profiloInterno[intI][intK].ToString());

                            }

                        }

                        #endregion

                        ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                        model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);

                    }

                }

                model.SmoothingAngle = Utility.DegToRad(20);
                ////APPLY MATERIAL
                ////model.MaterialName = mat.Description;
                ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                //model.MoveZ(altezza);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaFacciaSopraVeletta Error. ", ex);
            }

        }

        /// <summary>
        /// Procedura di disegno dei lati della sagoma.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="shape">Sagoma.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="profonditaRibasso">Profondit� del ribasso.</param>
        /// <param name="profonditaTasca">Profondit� della tasca.</param>
        private Mesh[] DisegnaBordi(ArrayList al, Shape shape, Material mat, string pathProfiliLav, ViewportLayout vppRender, double profonditaRibasso, double profonditaTasca)
        {
            //return null;
            List<Mesh> models = new List<Mesh>();
            try
            {

                int intJ = 0;

                int currentMainId = -1;
                string currentProcessing = "NONE";
                ShapeContourSweep currentContour = null;
                ProfileSweep currentSweep = null;


                #region Ciclo per tutte le entit� della sagoma.

                for (int intI = 0; intI < al.Count; intI++)
                {

                    Shape.Contour entita = (Shape.Contour) shape.gContours[intI];

                    if (entita.code.Substring(0, 2) != "CP" &&
                        entita.code != "SNP0002" &&
                        entita.subType != "Groove")
                    {

                        string profilo = entita.processing;
                        
                        double altezza = shape.gThickness;


                        if (shape.IsVeletta)
                            altezza = shape.gVeletta.height;

                        //scalino per il bordo del foro ribassato
                        if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") ||
                            (entita.subType == "SinkHole" && entita.code != "SNP0006"))
                            altezza -= profonditaRibasso;

                        else if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") ||
                                 entita.subType == "SinkHole")
                            altezza -= profonditaTasca;

                        else if (entita.code == "LAM01")
                            altezza = -altezza;

                        //else if (!string.IsNullOrEmpty(profilo) && entita.processing_thick > 0)
                        //    altezza = entita.processing_thick;


                        //De Conti 2016/02/08: test modifica per laminazioni tipo 1
                        double profileHeigth = ((entita.processing_thick > 0 ) && (entita.code != "LAM01")) ? entita.processing_thick : altezza;

                        //De Conti 2016/02/08: test modifica per laminazioni tipo 1
                        //string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", altezza.ToString(), ".dxf");

                        //string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", profileHeigth.ToString("#0"), ".dxf");
                        string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", Math.Abs(profileHeigth).ToString("#0"), ".dxf");

                        if (!string.IsNullOrEmpty(nomeFileProfilo) && !dxfProfiles.ContainsKey(nomeFileProfilo))
                        {
                            if (System.IO.File.Exists(nomeFileProfilo))
                            {
                                AddDxfProfile(dxfProfiles, nomeFileProfilo);
                            }
                        }

                        if (entita.id != currentMainId) // Nuovo percorso
                        {
                            if (currentContour != null)
                            {
                                ReorderAndAddCurrentContour(currentContour, models, vppRender);
                            }
                            currentMainId = entita.id;
                            currentContour = new ShapeContourSweep();
                            currentContour.MainId = currentMainId;

                            //currentContour.IsClockwise = (entita.id != 0);

                            if (entita.pricipalStruct)
                            {
                                currentContour.IsClockwise = false;
                            }
                            else
                            {
                                currentContour.IsClockwise = (entita.cw != 0);
                            }
                            //currentContour.IsClockwise = (entita.cw != 0);


                            currentContour.IsExternal = (entita.pricipalStruct);
                            currentContour.DxfProfiles = dxfProfiles;

                        }

                        ICurve currentCurve = al[intI] as ICurve;

                        Point3D currentStartPoint = null;

                        if (currentCurve == null || currentContour == null)
                            continue;

                        currentContour.AddCurve(currentCurve, altezza, nomeFileProfilo, profileHeigth);

                        ////if (string.IsNullOrEmpty(profilo) || !dxfProfiles.ContainsKey(nomeFileProfilo))
                        ////    // Profilo lineare
                        ////{
                        ////    if (currentSweep == null ||
                        ////        Math.Abs(currentSweep.Thickness - altezza) > COMPARISON_TOLERANCE
                        ////        || currentSweep.EndPoint != currentStartPoint)
                        ////    {
                        ////        currentSweep = new ProfileSweep(altezza);
                        ////        currentSweep.IsClockwise = currentContour.IsClockwise;
                        ////        currentContour.ProfileSweeps.Add(currentSweep);
                        ////    }

                        ////    currentSweep.Rail.CurveList.Add(al[intI] as ICurve);

                        ////}
                        ////else
                        ////{
                        ////    if (currentSweep == null || currentSweep.DxfProfileName != nomeFileProfilo
                        ////        || currentSweep.EndPoint != currentStartPoint)
                        ////    {
                        ////        currentSweep = new ProfileSweep(dxfProfiles[nomeFileProfilo]);
                        ////        currentSweep.DxfProfileName = nomeFileProfilo;
                        ////        currentSweep.IsClockwise = currentContour.IsClockwise;
                        ////        currentContour.ProfileSweeps.Add(currentSweep);
                        ////    }

                        ////    currentSweep.Rail.CurveList.Add(al[intI] as ICurve);

                        ////}
                    }

                }

                if (currentContour != null)
                {
                    ReorderAndAddCurrentContour(currentContour, models, vppRender);
                }

                #endregion
                //ReadAutodesk.OnApplicationExit(null, new EventArgs());


            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaBordi Error. ", ex);
                return null;
            }
            finally
            {
                ReadAutodesk.OnApplicationExit(null, new EventArgs());
            }

            return models.ToArray();

        }

        private void ReorderAndAddCurrentContour(ShapeContourSweep currentContour, List<Mesh> models, ViewportLayout vppRender)
        {
            #region Trace contour to log file

            //TraceLog.WriteLine("NEW CONTOUR");
            //foreach (var profileSweep in currentContour.ProfileSweeps)
            //{
            //    TraceLog.WriteLine("--->SWEEP " + "Thickness=" + profileSweep.Thickness + " Profile = " + profileSweep.DxfProfileName);
            //    foreach (var path in profileSweep.Rail.CurveList)
            //    {
            //        TraceLog.WriteLine("------->CURVE " + path.GetType().ToString()
            //            + ((path is Arc) ? (" Center " + ((Arc)path).Center.ToString()) : "")
            //            + " Start " + path.StartPoint.ToString()
            //            + " End " + path.EndPoint.ToString()
            //            + "\n          Decreasing = " + path.Domain.IsDecreasing + " Increasing = " + path.Domain.IsIncreasing + " IsExternal = " + currentContour.IsExternal);


            //    }
            //} 

            #endregion

            Mesh newMesh = currentContour.CreateMesh();

            ////newMesh.ComputeEdges();

            //foreach (var sweep in currentContour.ProfileSweeps)
            //{
            //    Thread.Sleep(5);
            //    Random r = new Random(DateTime.Now.Millisecond);
            //    Color random = Color.FromArgb(r.Next(0, 255), r.Next(0, 255), r.Next(0, 255));
            //    var path = sweep.Rail;
            //    path.Color = random;
            //    path.ColorMethod = colorMethodType.byEntity;
            //    path.LineWeight = 4F;
            //    path.LineWeightMethod = colorMethodType.byEntity;
            //    vppRender.Entities.Add(path);
            //}

            ////newMesh.Regen(CHORDAL_ERROR);

            models.Add(newMesh);
        }

        private void AddDxfProfile(Dictionary<string, CompositeCurve> dxfProfiles, string nomeFileProfilo)
        {
            if (dxfProfiles.ContainsKey(nomeFileProfilo))
                return;

            var ra = new ReadAutodesk(nomeFileProfilo);
            ra.DoWork();
            var raEntitiesList = ra.Entities.Cast<ICurve>().ToList();


            CompositeCurve profile = new CompositeCurve(raEntitiesList, 1e-2, true);


            PrepareProfile(ref profile);


            profile.Rotate(Math.PI / 2, Vector3D.AxisY);
            profile.Rotate(Math.PI / 2, Vector3D.AxisZ);

            profile.Regen(1e-2);

            dxfProfiles.Add(nomeFileProfilo, profile);

        }

        private void  PrepareProfile(ref CompositeCurve profile)
        {
            int i;

            List<int> baseLineIndexes = new List<int>();
            for (i = 0; i < profile.CurveList.Count; i++)
            {
                ICurve curve = profile.CurveList[i];
                if (curve is Line)
                {
                    if ((Math.Abs(curve.StartPoint.Y) < 1e-4) && (Math.Abs(curve.EndPoint.Y) < 1e-4))
                    {
                        baseLineIndexes.Add(i);
                    }
                }
            }
            // Procedo se c'� una sola line con Y=0
            if (baseLineIndexes.Count == 1)
            {
                int originalIndex = baseLineIndexes[0];
                Line originalLine = (Line) profile.CurveList[originalIndex];

                // Creo i due nuovi segmenti
                var midPoint = originalLine.MidPoint;
                Line startHalfLine = new Line((Point3D)midPoint.Clone(), (Point3D)originalLine.EndPoint.Clone());
                Line endHalfLine = new Line((Point3D)originalLine.StartPoint.Clone(), (Point3D)midPoint.Clone());

                // Aggiungo i due nuovi segmenti
                profile.CurveList.Insert(originalIndex, startHalfLine);
                profile.CurveList.Insert(originalIndex, endHalfLine);

                // Rimuovo linea originale
                profile.CurveList.RemoveAt(originalIndex + 2);

                // Riordino sequenza partendo dal punto medio linea originale
                for (int k = 0; k <= originalIndex; k++)
                {
                    var firstCurve = profile.CurveList[0];
                    profile.CurveList.RemoveAt(0);
                    profile.CurveList.Add(firstCurve);
                }

                profile.Regen(1e-2);
            }

        }

        [Obsolete]
 //       private Mesh[] DisegnaBordi_ORI(ArrayList al, Shape shape, Material mat, string pathProfiliLav, ViewportLayout vppRender, double profonditaRibasso, double profonditaTasca)
 //       {
 //           try
 //           {

 //               #region Ciclo per trovare il numero di elementi da restituire
 //               int intCount = 0;

 //               for (int intI = 0; intI < al.Count; intI++)
 //               {

 //                   Shape.Contour entita = (Shape.Contour)shape.gContours[intI];

 //                   if (entita.code.Substring(0, 2) != "CP" &&
 //                       entita.code != "SNP0002" &&
 //                       entita.subType != "Groove")
 //                   {

 //                       if (((Shape.Contour)shape.gContours[intI]).processing == "")
 //                       {

 //                           intCount++;

 //                       }

 //                       else
 //                       {

 //                           string profilo = entita.processing;

 //                           double altezza = shape.gThickness;

 //                           //scalino per il bordo del foro ribassato
 //                           if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || (entita.subType == "SinkHole" && entita.code != "SNP0006"))
 //                               altezza -= profonditaRibasso;

 //                           else if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || entita.subType == "SinkHole")
 //                               altezza -= profonditaTasca;

 //                           else if (entita.code == "LAM01")
 //                               altezza = -altezza;

 //                           string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", altezza.ToString(), ".dxf");

 //                           if (System.IO.File.Exists(nomeFileProfilo))
 //                           {

 //                               intCount += 3;

 //                           }

 //                           else
 //                           {

 //                               intCount++;

 //                           }

 //                       }

 //                   }

 //               }

 //               #endregion

 //               Mesh[] models = new Mesh[intCount];
 //               int intJ = 0;
 //               Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
 //               Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

 //               #region Ciclo per tutte le entit� della sagoma.

 //               for (int intI = 0; intI < al.Count; intI++)
 //               {

 //                   Shape.Contour entita = (Shape.Contour)shape.gContours[intI];

 //                   if (entita.code.Substring(0, 2) != "CP" &&
 //                       entita.code != "SNP0002" &&
 //                       entita.subType != "Groove")
 //                   {

 //                       string profilo = entita.processing;

 //                       double altezza = shape.gThickness;

 //                       if (shape.IsVeletta)
 //                           altezza = shape.gVeletta.height;

 //                       //scalino per il bordo del foro ribassato
 //                       if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || (entita.subType == "SinkHole" && entita.code != "SNP0006"))
 //                           altezza -= profonditaRibasso;

 //                       else if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || entita.subType == "SinkHole")
 //                           altezza -= profonditaTasca;

 //                       else if (entita.code == "LAM01")
 //                           altezza = -altezza;

 //                       string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", altezza.ToString(), ".dxf");

 //                       // Se non esiste il profilo o il relativo file dxf estrudo l'entit�
 //                       if (profilo == "" || !System.IO.File.Exists(nomeFileProfilo))
 //                       {
 //                           //continue;
 //                           if (al[intI].GetType() == typeof(Line))
 //                           {

 //                               Line l = (Line)al[intI];

 //                               //if (shape.IsVeletta)
 //                               //{
 //                               //    Line lTmp = CreateEyeLine(l.StartPoint.X, l.StartPoint.Y, 0, l.EndPoint.X, l.EndPoint.Y, 0, l.Color);

 //                               //    lTmp.Regen(CHORDAL_ERROR);
 //                               //    models[intJ] = lTmp.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));
 //                               //}
 //                               //else
 //                               //{
 //                                   l.Regen(CHORDAL_ERROR);
 //                                   //models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                                    
 //                                   ////models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));
 //                                   models[intJ] = l.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
 //                                   ////APPLY MATERIAL
 //                                   ////models[intJ].MaterialName = mat.Description;
 //                                   ////models[intJ].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

 //                               //}
 //                               intJ++;

 //                           }

 //                           else if (al[intI].GetType() == typeof(Arc))
 //                           {

 //                               Arc a = (Arc)al[intI];
 //                               a.Regen(CHORDAL_ERROR);
 //                               //models[intJ] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                                
 //                               ////models[intJ] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / a.Length()));

 ////TODO: Verify
 //                               //models[intJ] = a.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
 //                               models[intJ] = a.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

 //                               ////APPLY MATERIAL
 //                               ////models[intJ].MaterialName = mat.Description;
 //                               ////models[intJ].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

 //                               intJ++;

 //                           }

 //                           else
 //                           {

 //                               return null;

 //                           }

 //                       }

 //                       // Se esiste il profilo ed il relativo file dxf lo disegno
 //                       else
 //                       {
 //                           //if (!entita.pricipalStruct) continue;
 //                           ArrayList cf = new ArrayList();

 //                           ////vppRender.ReadAutodesk(cf, nomeFileProfilo, null);

 //                           var ra = new ReadAutodesk(nomeFileProfilo);
 //                           ra.DoWork();
 //                           var raEntitiesList = ra.Entities.Cast<ICurve>().ToList();
 //                           Point3D[] p = ViewportLayout.MakeLoop(raEntitiesList, 0, CHORDAL_ERROR, false);
 //                           List<Point3D> pointsProfile = p.ToList();
 //                           //var additionalPoint = (Point3D) pointsProfile[0].Clone();
 //                           //additionalPoint.TransformBy(new Translation(1e-4, 0, 0));
 //                           //pointsProfile.Add(additionalPoint);
 //                           var embossPath = new LinearPath(p);


 //                           ////Point3D[] p = ViewportProfessional.MakeLoop(cf, 0, CHORDAL_ERROR, false);

 //                           //TODO: Verificare l'intera generazione che segue
 //                           Mesh emboss = new Mesh();

 //                           //Entity emboss = new Mesh();
 //                           ////((Mesh)emboss).MakeFace(p, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));

 //                           emboss = Mesh.CreatePlanar(pointsProfile,  Mesh.natureType.RichSmooth); 

 //                           emboss.MaterialName = mat.Description;
 //                           emboss.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

 //                           ////emboss.RotateY(90);
 //                           emboss.Rotate(Utility.DegToRad(90), Vector3D.AxisY);
 //                           embossPath.Rotate(Utility.DegToRad(90), Vector3D.AxisY);
                            
 //                           emboss.Regen(CHORDAL_ERROR);
 //                           embossPath.Regen(CHORDAL_ERROR);

 //                           // L'entit� � una linea, quindi estrudo il profilo per tutta la sua lunghezza
 //                           if (al[intI].GetType() == typeof(Line))
 //                           {
 //                               Line l = (Line)al[intI];
 //                               l.Regen(CHORDAL_ERROR);

 //                               Mesh embossClone = emboss.Clone() as Mesh;
 //                               Mesh embossCloneEnd = emboss.Clone() as Mesh;

 //                               double angolo = 0;
 //                               if (entita.pricipalStruct)
 //                               {
 //                                   if (Math.Abs(l.StartPoint.X - entita.x0) < CHORDAL_ERROR)
 //                                   {
 //                                       angolo = GetAngolo(entita.x0, entita.y0, entita.x1, entita.y1);
 //                                   }
 //                                   else
 //                                   {
 //                                       angolo = GetAngolo(l.StartPoint.X, l.StartPoint.Y, l.EndPoint.X, l.EndPoint.Y);
 //                                   }
 //                                   //angolo = GetAngolo(entita.x0, entita.y0, entita.x1, entita.y1);

 //                               }

 //                               else
 //                               {

 //                                   angolo = GetAngolo(l.EndPoint.X, l.EndPoint.Y, l.StartPoint.X, l.StartPoint.Y);

 //                               }

 //                               ////embossClone.RotateZ(angolo);
 //                               ////embossCloneEnd.RotateZ(angolo);
 //                               ////embossClone.Move(l.StartPoint.X, l.StartPoint.Y, 0);
 //                               ////embossCloneEnd.Move(l.EndPoint.X, l.EndPoint.Y, 0);

 //                               embossClone.Rotate(Utility.DegToRad(angolo), Vector3D.AxisZ);
 //                               embossCloneEnd.Rotate(Utility.DegToRad(angolo), Vector3D.AxisZ);
 //                               embossClone.Translate(l.StartPoint.X, l.StartPoint.Y, 0);
 //                               embossCloneEnd.Translate(l.EndPoint.X, l.EndPoint.Y, 0);



 //                               embossClone.Regen(CHORDAL_ERROR);
 //                               embossCloneEnd.Regen(CHORDAL_ERROR);

 //                               //models[intJ] = embossClone.ExtrudeAsRichMesh(l.End.X - l.Start.X, l.End.Y - l.Start.Y, l.End.Z - l.Start.Z, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
 //                               ////models[intJ] = embossClone.ExtrudeAsRichMesh(l.End.X - l.Start.X, l.End.Y - l.Start.Y, l.End.Z - l.Start.Z, mat, new Point2D(0, 0), new Point2D(shape.gThickness / l.Length(), 1));

 //                               var embossCloneStart = embossClone.Clone() as Mesh;

 //                               //TODO: Verify
 //                               //embossClone.Extrude(l.EndPoint.X - l.StartPoint.X, l.EndPoint.Y - l.StartPoint.Y, l.EndPoint.Z - l.StartPoint.Z);
 //                               embossClone.ExtrudePlanar(l.EndPoint.X - l.StartPoint.X, l.EndPoint.Y - l.StartPoint.Y, l.EndPoint.Z - l.StartPoint.Z);

 //                               //29/06/2015
 //                               if (entita.cw == 0)
 //                               {
 //                                   //TODO: Verify
 //                                   //embossClone.FlipOutside();
 //                                   embossClone.FlipOutward();
 //                               }


 //                               //


 //                               embossClone.MaterialName = mat.Description;
 //                               embossClone.ApplyMaterial(mat.Description, textureMappingType.Cylindrical, 1, 1);
 //                               models[intJ] = embossClone;

 //                               intJ++;
 //                               // Aggiungo le chiusure iniziale e finale del profilo
 //                               models[intJ] = (Mesh)embossCloneStart;
 //                               intJ++;
 //                               models[intJ] = (Mesh)embossCloneEnd;
 //                               intJ++;

 //                           }

 //                           // L'entit� � un arco, quindi applico una rivoluzione del profilo per tutta la circonferenza
 //                           else if (al[intI].GetType() == typeof(Arc))
 //                           {

 //                               //Entity e = (Entity)al[intI];
 //                               //e.Regen(CHORDAL_ERROR);
 //                               Arc a = (Arc)(Entity)al[intI];
 //                               a.Regen(CHORDAL_ERROR);


 //                               Mesh embossClone = emboss.Clone() as Mesh;
 //                               Mesh embossCloneEnd = emboss.Clone() as Mesh;

 //                               double startAngle, endAngle;
 //                               bool IsClockwise = Utils.IsClockwiseOriented(a);
 //                               if (IsClockwise)
 //                               {
 //                                   startAngle = (a.StartTangent.AngleInXY) + Math.PI / 2;
 //                                   endAngle = (a.EndTangent.AngleInXY) + Math.PI / 2;
 //                               }
 //                               else
 //                               {
 //                                   startAngle = (a.StartTangent.AngleInXY) -Math.PI / 2;
 //                                   endAngle = (a.EndTangent.AngleInXY) - Math.PI / 2;
 //                               }

 //                               if (entita.pricipalStruct)
 //                               {

 //                                   ////embossClone.RotateZ(90 + a.StartAngle + (entita.cw == 0 ? 0 : 180));
 //                                   ////embossCloneEnd.RotateZ(90 + a.EndAngle + (entita.cw == 0 ? 0 : 180));

 //                                   embossClone.Rotate(Math.PI / 2 + startAngle + (entita.cw == 0 ? 0  : Math.PI), Vector3D.AxisZ);

 //                                   //embossPath.Rotate(Utility.DegToRad(90) + a.Domain.t0 + (entita.cw == 0 ? 0 : Math.PI), Vector3D.AxisZ);


 //                                   embossCloneEnd.Rotate(Math.PI / 2 + endAngle + (entita.cw == 0 ? 0 : Math.PI ), Vector3D.AxisZ);

 //                                   embossClone.Regen(CHORDAL_ERROR);
 //                                   embossCloneEnd.Regen(CHORDAL_ERROR);


 //                               }

 //                               else
 //                               {
 //                                   ////embossClone.RotateZ(180 + 90 + a.StartAngle + (entita.cw != 0 ? 0 : 180));
 //                                   ////embossCloneEnd.RotateZ(180 + 90 + a.EndAngle + (entita.cw != 0 ? 0 : 180));

 //                                   ////embossClone.Rotate(Utility.DegToRad(180 + 90) + a.Domain.t0 + (entita.cw != 0 ? 0 : Math.PI), Vector3D.AxisZ);
 //                                   ////embossCloneEnd.Rotate(Utility.DegToRad(180 + 90) + a.Domain.t1 + (entita.cw != 0 ? 0 : Math.PI), Vector3D.AxisZ);


 //                                   embossClone.Rotate(Math.PI * 3 / 2 + startAngle + (entita.cw != 0 ? 0 : Math.PI), Vector3D.AxisZ);
 //                                   embossCloneEnd.Rotate(Math.PI * 3 / 2 + endAngle + (entita.cw != 0 ? 0 : Math.PI), Vector3D.AxisZ);

 //                                   embossClone.Regen(CHORDAL_ERROR);
 //                                   embossCloneEnd.Regen(CHORDAL_ERROR);
 //                               }

 //                               ////embossClone.Move(a.Start.X, a.Start.Y, 0);
 //                               ////embossCloneEnd.Move(a.End.X, a.End.Y, 0);
 //                               embossClone.Translate(a.StartPoint.X, a.StartPoint.Y, 0);
 //                               //embossPath.Translate(a.StartPoint.X, a.StartPoint.Y, 0);
 //                               embossCloneEnd.Translate(a.EndPoint.X, a.EndPoint.Y, 0);


 //                               embossClone.Regen(CHORDAL_ERROR);
 //                               embossCloneEnd.Regen(CHORDAL_ERROR);

 //                               ////double angolo = (Utils.IsClockwiseOriented(a) ? a.Domain.t0 - a.Domain.t1 : a.Domain.t1 - a.Domain.t0);
 //                               //double angolo = IsClockwise ? (startAngle - endAngle) : (endAngle - startAngle);
 //                               double angolo = endAngle - startAngle;
 //                               if (!IsClockwise && angolo < 0)
 //                               {
 //                                   while (angolo < 0)
 //                                   {
 //                                       angolo += Math.PI*2;
 //                                   }
 //                               }


 //                               var embossCloneStart = embossClone.Clone() as Mesh;
 //                               //models[intJ] = embossClone.RevolveAsRichMesh(a.Center, Vector3D.zAxis, angolo, a.Vertices.Length - 1, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
 //                               ////models[intJ] = embossClone.RevolveAsRichMesh(a.Center, Vector3D.zAxis, angolo, a.Vertices.Length - 1, mat, new Point2D(0, 0), new Point2D(1, 1));
 //                               //var xx = Utility.NumberOfSegments(a.BoxMax.X, (angolo),
 //                               //            CHORDAL_ERROR);
 //                               Plane meshPlane; // = new Plane(embossClone.Vertices[0], embossClone.Vertices[2], embossClone.Vertices[4]); // Piano della mesh
 //                               //if (!embossPath.IsClosed)
 //                               //{
 //                               //    //path.Vertices.
 //                               //}
 //                               //if (embossPath.IsPlanar(CHORDAL_ERROR, out meshPlane))
 //                               {
 //                                   //if (entita.cw == 1)
 //                                       //embossPath.Reverse();
 //                               }
 //                               Point3D[] vertices = embossClone.Vertices;
 //                               //if (entita.cw == 1)
 //                               //    Array.Reverse(vertices);

 //                               embossClone = Mesh.Revolve(vertices, 0, (angolo),
 //                                       Vector3D.AxisZ, a.Center,
 //                                       (int)a.AngleInDegrees / 3, false, Mesh.natureType.RichSmooth);

 //                               //29/06/2015
 //                               if (entita.cw == 0)
 //                               {
 //                                   //embossClone.Scale(1, 1, -1); //mirror
 //                                   embossClone.FlipNormal();
 //                               }



 //                               embossClone.MaterialName = mat.Description;
 //                               embossClone.ApplyMaterial(mat.Description, textureMappingType.Cylindrical, 1, 1);

 //                               models[intJ] = embossClone;
 //                               intJ++;
 //                               // Aggiungo le chiusure iniziale e finale del profilo
 //                               models[intJ] = (Mesh)embossCloneStart;
 //                               intJ++;
 //                               models[intJ] = (Mesh)embossCloneEnd;
 //                               intJ++;

 //                           }

 //                           else
 //                           {

 //                               return null;

 //                           }

 //                       }

 //                   }

 //               }
 //               #endregion

 //               return models;

 //           }
 //           catch (Exception ex)
 //           {
 //               TraceLog.WriteLine("DisegnaBordi Error. ", ex);
 //               return null;
 //           }

 //       }

        /// <summary>
        /// Procedura di disegno dei lati della sagoma.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="shape">Sagoma.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="profonditaRibasso">Profondit� del ribasso.</param>
        /// <param name="profonditaTasca">Profondit� della tasca.</param>
        private Mesh[] DisegnaBordi(ArrayList al, ArrayList alUp, Shape shape, Material mat, string pathProfiliLav, ViewportLayout vppRender, double profonditaRibasso, double profonditaTasca)
        {

            try
            {

                #region Ciclo per trovare il numero di elementi da restituire

                int intCount = 0;

                for (int intI = 0; intI < al.Count; intI++)
                {

                    Shape.Contour entita = (Shape.Contour) shape.gContours[intI];

                    if (entita.code.Substring(0, 2) != "CP" &&
                        entita.code != "SNP0002" &&
                        entita.subType != "Groove")
                    {

                        if (((Shape.Contour) shape.gContours[intI]).processing == "")
                        {

                            intCount++;

                        }

                        else
                        {

                            string profilo = entita.processing;

                            double altezza = shape.gThickness;

                            //scalino per il bordo del foro ribassato
                            if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") ||
                                (entita.subType == "SinkHole" && entita.code != "SNP0006"))
                                altezza -= profonditaRibasso;

                            else if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") ||
                                     entita.subType == "SinkHole")
                                altezza -= profonditaTasca;

                            else if (entita.code == "LAM01")
                                altezza = -altezza;

                            string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", altezza.ToString(),
                                ".dxf");

                            if (System.IO.File.Exists(nomeFileProfilo))
                            {

                                intCount += 3;

                            }

                            else
                            {

                                intCount++;

                            }

                        }

                    }

                }

                #endregion

                Mesh[] models = new Mesh[intCount];
                int intJ = 0;
                Vector3D normal = new Vector3D(1, 0, 0);
                    // new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);
                    //Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                #region Ciclo per tutte le entit� della sagoma.

                for (int intI = 0; intI < al.Count; intI++)
                {

                    Shape.Contour entita = (Shape.Contour) shape.gContours[intI];

                    if (entita.code.Substring(0, 2) != "CP" &&
                        entita.code != "SNP0002" &&
                        entita.subType != "Groove")
                    {

                        string profilo = entita.processing;

                        double altezza = shape.gThickness;

                        if (shape.IsVeletta)
                            altezza = shape.gVeletta.height;

                        //scalino per il bordo del foro ribassato
                        if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") ||
                            (entita.subType == "SinkHole" && entita.code != "SNP0006"))
                            altezza -= profonditaRibasso;

                        else if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") ||
                                 entita.subType == "SinkHole")
                            altezza -= profonditaTasca;

                        else if (entita.code == "LAM01")
                            altezza = -altezza;

                        string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", altezza.ToString(), ".dxf");

                        // Se non esiste il profilo o il relativo file dxf estrudo l'entit�
                        if (profilo == "" || !System.IO.File.Exists(nomeFileProfilo))
                        {

                            if (al[intI].GetType() == typeof (Line))
                            {
                                ArrayList alTmp = new ArrayList();

                                Line lineD = (Line) al[intI];
                                Line lineTmp = (Line) alUp[intI];
                                Line lineUp = CreateEyeLine(lineTmp.StartPoint.X, lineTmp.StartPoint.Y,
                                    lineTmp.StartPoint.Z, lineTmp.EndPoint.X, lineTmp.EndPoint.Y, lineTmp.EndPoint.Z,
                                    lineTmp.Color);

                                if (lineUp.StartPoint == lineD.StartPoint && lineUp.EndPoint == lineD.EndPoint)
                                {

                                    Line l = (Line) al[intI];

                                    //if (shape.IsVeletta)
                                    //{
                                    //    Line lTmp = CreateEyeLine(l.StartPoint.X, l.StartPoint.Y, 0, l.EndPoint.X, l.EndPoint.Y, 0, l.Color);

                                    //    lTmp.Regen(CHORDAL_ERROR);
                                    //    models[intJ] = lTmp.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));
                                    //}
                                    //else
                                    //{
                                    l.Regen(CHORDAL_ERROR);
                                    //models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                                    ////models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));

                                    //TODO: Verify
                                    //models[intJ] = l.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                                    models[intJ] = l.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR,
                                        Mesh.natureType.RichSmooth);


                                    ////APPLY MATERIAL
                                    ////models[intJ].MaterialName = mat.Description;
                                    ////models[intJ].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);


                                    intJ++;


                                }
                                else
                                {
                                    Point3D pTmp = new Point3D();
                                    pTmp = lineUp.EndPoint;
                                    lineUp.EndPoint = lineUp.StartPoint;
                                    lineUp.StartPoint = pTmp;
                                    ////lineUp.MoveZ(altezza);
                                    lineUp.Translate(0, 0, altezza);

                                    Line lineDX = CreateEyeLine(lineD.EndPoint, lineUp.StartPoint, lineD.Color);

                                    alTmp.Add(lineD);
                                    alTmp.Add(lineDX);
                                    //if (lineUp.StartPoint.X != lineUp.EndPoint.X || lineUp.StartPoint.Y != lineUp.EndPoint.Y)
                                    //{
                                    alTmp.Add(lineUp);
                                    //}
                                    //else
                                    //{
                                    //    continue;
                                    //}

                                    Line lineSX = CreateEyeLine(lineUp.EndPoint, lineD.StartPoint, lineD.Color);

                                    alTmp.Add(lineSX);

                                    var alTmpToList = alTmp.Cast<ICurve>().ToList();
                                    ////Point3D[] profiloEsterno = ViewportProfessional.MakeLoop(alTmp, 0, CHORDAL_ERROR, false);
                                    Point3D[] profiloEsterno = ViewportLayout.MakeLoop(alTmpToList, 0, CHORDAL_ERROR,
                                        false);


                                    Mesh model = new Mesh();
                                    normal = new Vector3D(profiloEsterno[1].X - profiloEsterno[0].X,
                                        profiloEsterno[1].Y - profiloEsterno[0].Y,
                                        profiloEsterno[1].Z - profiloEsterno[0].Z);
                                        // new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                                    direzione =
                                        new Vector3D(
                                            profiloEsterno[profiloEsterno.Length - 2].X -
                                            profiloEsterno[profiloEsterno.Length - 1].X,
                                            profiloEsterno[profiloEsterno.Length - 2].Y -
                                            profiloEsterno[profiloEsterno.Length - 1].Y,
                                            profiloEsterno[profiloEsterno.Length - 2].Z -
                                            profiloEsterno[profiloEsterno.Length - 1].Z);
                                        //Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // La sagoma della base non contiene sagome figlie.
                                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);

                                    //model.SmoothingAngle = 20;
                                    ////APPLY MATERIAL
                                    ////model.MaterialName = mat.Description;
                                    ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                                    models[intJ] = model;
                                    intJ++;

                                    //pTmp = lineUp.EndPoint;
                                    //lineUp.EndPoint = lineUp.StartPoint;
                                    //lineUp.StartPoint = pTmp;
                                    //lineUp.MoveZ(-altezza);

                                    //Line l = (Line)al[intI];

                                    ////if (shape.IsVeletta)
                                    ////{
                                    ////    Line lTmp = CreateEyeLine(l.StartPoint.X, l.StartPoint.Y, 0, l.EndPoint.X, l.EndPoint.Y, 0, l.Color);

                                    ////    lTmp.Regen(CHORDAL_ERROR);
                                    ////    models[intJ] = lTmp.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));
                                    ////}
                                    ////else
                                    ////{
                                    //l.Regen(CHORDAL_ERROR);
                                    ////models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                                    //models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));
                                    ////}
                                    //intJ++;
                                }
                            }

                            else if (al[intI].GetType() == typeof (Arc))
                            {

                                Arc a = (Arc) al[intI];
                                a.Regen(CHORDAL_ERROR);
                                //models[intJ] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                                ////models[intJ] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / a.Length()));

                                //TODO: Verify
                                //models[intJ] = a.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                                models[intJ] = a.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);


                                ////APPLY MATERIAL
                                ////models[intJ].MaterialName = mat.Description;
                                ////models[intJ].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);


                                intJ++;

                            }

                            else
                            {

                                return null;

                            }

                        }

                            // Se esiste il profilo ed il relativo file dxf lo disegno
                        else
                        {
                            normal = new Vector3D(1, 0, 0);
                                // new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                            direzione = new Vector3D(0, 1, 0);
                                //Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                            ArrayList cf = new ArrayList();

                            ////vppRender.ReadAutodesk(cf, nomeFileProfilo, null);
                            ////Point3D[] p = ViewportProfessional.MakeLoop(cf, 0, CHORDAL_ERROR, false);

                            var ra = new ReadAutodesk(nomeFileProfilo);
                            ra.DoWork();
                            var raEntitiesList = ra.Entities.Cast<ICurve>().ToList();
                            Point3D[] p = ViewportLayout.MakeLoop(raEntitiesList, 0, CHORDAL_ERROR, false);


                            Mesh emboss = new Mesh();
                            //Entity emboss = new Mesh();
                            ////((Mesh)emboss).MakeFace(p, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));

                            emboss = Mesh.CreatePlanar(p.ToList(), null, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////emboss.MaterialName = mat.Description;
                            ////emboss.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);


                            ////emboss.RotateY(90);
                            emboss.Rotate(Utility.DegToRad(90), Vector3D.AxisY);
                            emboss.Regen(CHORDAL_ERROR);

                            // L'entit� � una linea, quindi estrudo il profilo per tutta la sua lunghezza
                            if (al[intI].GetType() == typeof (Line))
                            {

                                Line l = (Line) al[intI];
                                l.Regen(CHORDAL_ERROR);

                                Mesh embossClone = emboss.Clone() as Mesh;
                                Mesh embossCloneEnd = emboss.Clone() as Mesh;

                                double angolo = 0;
                                if (entita.pricipalStruct)
                                {

                                    angolo = GetAngolo(l.StartPoint.X, l.StartPoint.Y, l.EndPoint.X, l.EndPoint.Y);

                                }

                                else
                                {

                                    angolo = GetAngolo(l.EndPoint.X, l.EndPoint.Y, l.StartPoint.X, l.StartPoint.Y);

                                }

                                ////embossClone.RotateZ(angolo);
                                ////embossCloneEnd.RotateZ(angolo);
                                ////embossClone.Move(l.Start.X, l.Start.Y, 0);
                                ////embossCloneEnd.Move(l.End.X, l.End.Y, 0);

                                embossClone.Rotate(Utility.DegToRad(angolo), Vector3D.AxisZ);
                                embossCloneEnd.Rotate(Utility.DegToRad(angolo), Vector3D.AxisZ);
                                embossClone.Translate(l.StartPoint.X, l.StartPoint.Y, 0);
                                embossCloneEnd.Translate(l.EndPoint.X, l.EndPoint.Y, 0);


                                embossClone.Regen(CHORDAL_ERROR);
                                embossCloneEnd.Regen(CHORDAL_ERROR);

                                var embossCloneStart = embossClone.Clone() as Mesh;

                                //models[intJ] = embossClone.ExtrudeAsRichMesh(l.End.X - l.Start.X, l.End.Y - l.Start.Y, l.End.Z - l.Start.Z, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                                ////models[intJ] = embossClone.ExtrudeAsRichMesh(l.End.X - l.Start.X, l.End.Y - l.Start.Y, l.End.Z - l.Start.Z, mat, new Point2D(0, 0), new Point2D(shape.gThickness / l.Length(), 1));

                                //TODO: Verify
                                //embossClone.Extrude(l.EndPoint.X - l.StartPoint.X, l.EndPoint.Y - l.StartPoint.Y, l.EndPoint.Z - l.StartPoint.Z);
                                embossClone.ExtrudePlanar(l.EndPoint.X - l.StartPoint.X, l.EndPoint.Y - l.StartPoint.Y,
                                    l.EndPoint.Z - l.StartPoint.Z);


                                ////APPLY MATERIAL
                                ////embossClone.MaterialName = mat.Description;
                                ////embossClone.ApplyMaterial(mat.Description, textureMappingType.Cylindrical, 1, 1);
                                models[intJ] = embossClone;

                                intJ++;
                                // Aggiungo le chiusure iniziale e finale del profilo
                                models[intJ] = (Mesh) embossCloneStart;
                                intJ++;
                                models[intJ] = (Mesh) embossCloneEnd;
                                intJ++;

                            }

                                // L'entit� � un arco, quindi applico una rivoluzione del profilo per tutta la circonferenza
                            else if (al[intI].GetType() == typeof (Arc))
                            {

                                //Entity e = (Entity)al[intI];
                                //e.Regen(CHORDAL_ERROR);
                                Arc a = (Arc) (Entity) al[intI];
                                a.Regen(CHORDAL_ERROR);


                                Mesh embossClone = emboss.Clone() as Mesh;
                                Mesh embossCloneEnd = emboss.Clone() as Mesh;

                                if (entita.pricipalStruct)
                                {

                                    ////embossClone.RotateZ(90 + a.StartAngle + (entita.cw == 0 ? 0 : 180));
                                    ////embossCloneEnd.RotateZ(90 + a.EndAngle + (entita.cw == 0 ? 0 : 180));
                                    embossClone.Rotate(
                                        Utility.DegToRad(90) + a.Domain.t0 + (entita.cw == 0 ? 0 : Math.PI),
                                        Vector3D.AxisZ);
                                    embossCloneEnd.Rotate(
                                        Utility.DegToRad(90) + a.Domain.t1 + (entita.cw == 0 ? 0 : Math.PI),
                                        Vector3D.AxisZ);

                                }

                                else
                                {

                                    ////embossClone.RotateZ(180 + 90 + a.StartAngle + (entita.cw != 0 ? 0 : 180));
                                    ////embossCloneEnd.RotateZ(180 + 90 + a.EndAngle + (entita.cw != 0 ? 0 : 180));
                                    embossClone.Rotate(
                                        Utility.DegToRad(180 + 90) + a.Domain.t0 + (entita.cw == 0 ? 0 : Math.PI),
                                        Vector3D.AxisZ);
                                    embossCloneEnd.Rotate(
                                        Utility.DegToRad(180 + 90) + a.Domain.t1 + (entita.cw == 0 ? 0 : Math.PI),
                                        Vector3D.AxisZ);

                                }

                                ////embossClone.Move(a.Start.X, a.Start.Y, 0);
                                ////embossCloneEnd.Move(a.End.X, a.End.Y, 0);
                                embossClone.Translate(a.StartPoint.X, a.StartPoint.Y, 0);
                                embossCloneEnd.Translate(a.EndPoint.X, a.EndPoint.Y, 0);


                                embossClone.Regen(CHORDAL_ERROR);
                                embossCloneEnd.Regen(CHORDAL_ERROR);
                                double angolo = a.Domain.t1 - a.Domain.t0;

                                var embossCloneStart = embossClone.Clone() as Mesh;

                                //models[intJ] = embossClone.RevolveAsRichMesh(a.Center, Vector3D.zAxis, angolo, a.Vertices.Length - 1, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                                ////models[intJ] = embossClone.RevolveAsRichMesh(a.Center, Vector3D.zAxis, angolo, a.Vertices.Length - 1, mat, new Point2D(0, 0), new Point2D(1, 1));


                                //TODO: Verify
                                //embossClone = Mesh.Revolve(embossClone.Vertices, 0, (angolo),
                                //        Vector3D.AxisZ, a.Center,
                                //        (int)a.AngleInDegrees / 3, false, Mesh.natureType.RichSmooth);

                                var curve = new LinearPath(embossClone.Vertices);
                                embossClone = curve.RevolveAsMesh(0, (angolo),
                                    Vector3D.AxisZ, a.Center,
                                    (int) a.AngleInDegrees/3, CHORDAL_ERROR, Mesh.natureType.RichSmooth);


                                ////APPLY MATERIAL
                                ////embossClone.MaterialName = mat.Description;
                                ////embossClone.ApplyMaterial(mat.Description, textureMappingType.Cylindrical, 1, 1);

                                models[intJ] = embossClone;

                                intJ++;
                                // Aggiungo le chiusure iniziale e finale del profilo
                                models[intJ] = (Mesh) embossCloneStart;
                                intJ++;
                                models[intJ] = (Mesh) embossCloneEnd;
                                intJ++;

                            }

                            else
                            {

                                return null;

                            }

                        }

                    }

                }

                #endregion

                return models;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaBordi Error. ", ex);
                return null;
            }

            finally
            {
                devDept.Eyeshot.Translators.ReadAutodesk.OnApplicationExit(null, new EventArgs());
            }

        }

        /// <summary>
        /// Procedura di disegno dei lati della sagoma.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="shape">Sagoma.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="profonditaRibasso">Profondit� del ribasso.</param>
        /// <param name="profonditaTasca">Profondit� della tasca.</param>
        private Mesh[] DisegnaBordiVeletta(ArrayList al, Shape shape, Material mat, string pathProfiliLav, ViewportLayout vppRender, double profonditaRibasso, double profonditaTasca)
        {

            try
            {

                #region Ciclo per trovare il numero di elementi da restituire
                int intCount = 0;

                for (int intI = 0; intI < al.Count; intI++)
                {

                    Shape.Contour entita = (Shape.Contour)shape.gContours[intI];

                    if (entita.code.Substring(0, 2) != "CP" &&
                        entita.code != "SNP0002" &&
                        entita.subType != "Groove")
                    {

                        if (((Shape.Contour)shape.gContours[intI]).processing == "")
                        {

                            intCount++;

                        }

                        else
                        {

                            string profilo = entita.processing;

                            double altezza = shape.gThickness;

                            //scalino per il bordo del foro ribassato
                            if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || (entita.subType == "SinkHole" && entita.code != "SNP0006"))
                                altezza -= profonditaRibasso;

                            else if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || entita.subType == "SinkHole")
                                altezza -= profonditaTasca;

                            else if (entita.code == "LAM01")
                                altezza = -altezza;

                            string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", altezza.ToString(), ".dxf");

                            if (System.IO.File.Exists(nomeFileProfilo))
                            {

                                intCount += 3;

                            }

                            else
                            {

                                intCount++;

                            }

                        }

                    }

                }

                #endregion

                Mesh[] models = new Mesh[intCount];
                int intJ = 0;
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                #region Ciclo per tutte le entit� della sagoma.
                for (int intI = 0; intI < al.Count; intI++)
                {

                    Shape.Contour entita = (Shape.Contour)shape.gContours[intI];

                    if (entita.code.Substring(0, 2) != "CP" &&
                        entita.code != "SNP0002" &&
                        entita.subType != "Groove")
                    {

                        string profilo = entita.processing;

                        double altezza = shape.gVeletta.height;

                        //scalino per il bordo del foro ribassato
                        if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || (entita.subType == "SinkHole" && entita.code != "SNP0006"))
                            altezza -= profonditaRibasso;

                        else if ((entita.code == "SNP0004" && entita.insertMode == "RIBASSATO") || entita.subType == "SinkHole")
                            altezza -= profonditaTasca;

                        else if (entita.code == "LAM01")
                            altezza = -altezza;

                        string nomeFileProfilo = String.Concat(pathProfiliLav, profilo, "_", altezza.ToString(), ".dxf");

                        // Se non esiste il profilo o il relativo file dxf estrudo l'entit�
                        if (profilo == "" || !System.IO.File.Exists(nomeFileProfilo))
                        {

                            if (al[intI].GetType() == typeof(Line))
                            {

                                Line otherLine = null;

                                if (intI == 0)
                                {
                                    otherLine = (Line)al[1];
                                }
                                else
                                {
                                    otherLine = (Line)al[0];
                                }

                                double minZ = otherLine.StartPoint.Z;

                                if (otherLine.EndPoint.Z < minZ)
                                {
                                    minZ = otherLine.EndPoint.Z;
                                }

                                Line l = (Line)al[intI];

                                if (l.StartPoint.Z < minZ)
                                {
                                    minZ = l.StartPoint.Z;
                                }

                                if (l.EndPoint.Z < minZ)
                                {
                                    minZ = l.EndPoint.Z;
                                }

                                Line l0, l1, l2, l3;

                                l.Regen(CHORDAL_ERROR);

                                    ArrayList alTmp = new ArrayList();
                                    Point3D[] profiloEsterno = null;


                                if (l.StartPoint.Z == l.EndPoint.Z)
                                {
                                    //models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                                    if (l.StartPoint.Z > minZ)
                                    {
                                        l0 = CreateEyeLine(l.EndPoint, l.StartPoint, l.Color);

                                        alTmp.Add(l0);

                                        l1 = CreateEyeLine(l0.EndPoint,
                                            new Point3D(l.StartPoint.X, l.StartPoint.Y, l.StartPoint.Z - (altezza)),
                                            l.Color);

                                        alTmp.Add(l1);

                                        l2 = CreateEyeLine(l1.EndPoint,
                                            new Point3D(l.EndPoint.X, l.EndPoint.Y, l.EndPoint.Z - (altezza)), l.Color);

                                        alTmp.Add(l2);

                                        l3 = CreateEyeLine(l2.EndPoint, l0.StartPoint, l.Color);

                                        alTmp.Add(l3);
                                    }
                                    else
                                    {
                                        l0 = CreateEyeLine(l.EndPoint, l.StartPoint, l.Color);

                                        alTmp.Add(l0);

                                        l1 = CreateEyeLine(l0.EndPoint,
                                            new Point3D(l.StartPoint.X, l.StartPoint.Y,
                                                l.StartPoint.Z - (altezza - shape.gThickness)), l.Color);

                                        alTmp.Add(l1);

                                        l2 = CreateEyeLine(l1.EndPoint,
                                            new Point3D(l.EndPoint.X, l.EndPoint.Y,
                                                l.EndPoint.Z - (altezza - shape.gThickness)), l.Color);

                                        alTmp.Add(l2);

                                        l3 = CreateEyeLine(l2.EndPoint, l0.StartPoint, l.Color);

                                        alTmp.Add(l3);

                                    }

                                        var alTmpToList = alTmp.Cast<ICurve>().ToList();

                                    profiloEsterno = ViewportLayout.MakeLoop(alTmpToList, 0, CHORDAL_ERROR, false);

                                }

                                else
                                {
                                    if (l.StartPoint.Z > l.EndPoint.Z)
                                    {
                                        l0 = CreateEyeLine(l.StartPoint, l.EndPoint, l.Color);
                                        alTmp.Add(l0);

                                        //Line l1 = CreateEyeLine(l0.EndPoint, new Point3D(l0.EndPoint.X, l0.EndPoint.Y, l0.EndPoint.Z - altezza), l.Color);
                                        l1 = CreateEyeLine(l0.EndPoint,
                                            new Point3D(l0.EndPoint.X, l0.EndPoint.Y,
                                                l0.EndPoint.Z - (altezza - shape.gThickness)), l.Color);
                                        alTmp.Add(l1);

                                        l2 = CreateEyeLine(l1.EndPoint,
                                            new Point3D(l.StartPoint.X, l.StartPoint.Y, l.StartPoint.Z - altezza),
                                            l.Color);
                                        //Line l2 = CreateEyeLine(l1.EndPoint, new Point3D(l.StartPoint.X, l.StartPoint.Y, l.StartPoint.Z - (altezza - shape.gThickness)), l.Color);
                                        alTmp.Add(l2);

                                        l3 = CreateEyeLine(l2.EndPoint, l0.StartPoint, l.Color);
                                        alTmp.Add(l3);

                                    }
                                    else
                                    {
                                        l0 = CreateEyeLine(l.StartPoint, l.EndPoint, l.Color);
                                        alTmp.Add(l0);

                                        l1 = CreateEyeLine(l0.EndPoint,
                                            new Point3D(l0.EndPoint.X, l0.EndPoint.Y, l0.EndPoint.Z - (altezza)), l.Color);
                                        alTmp.Add(l1);

                                        l2 = CreateEyeLine(l1.EndPoint,
                                            new Point3D(l.StartPoint.X, l.StartPoint.Y, l.StartPoint.Z - (altezza - shape.gThickness)), l.Color);

                                        alTmp.Add(l2);

                                        l3 = CreateEyeLine(l2.EndPoint, l0.StartPoint, l.Color);
                                        alTmp.Add(l3);
                                    }

                                    var alTmpToList = alTmp.Cast<ICurve>().ToList();

                                    profiloEsterno = ViewportLayout.MakeLoop(alTmpToList, 0, CHORDAL_ERROR, false);
                                }

                                    Mesh model = new Mesh();
                                    normal = new Vector3D(profiloEsterno[1].X - profiloEsterno[0].X, profiloEsterno[1].Y - profiloEsterno[0].Y, profiloEsterno[1].Z - profiloEsterno[0].Z);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                                    direzione = new Vector3D(profiloEsterno[profiloEsterno.Length - 2].X - profiloEsterno[profiloEsterno.Length - 1].X, profiloEsterno[profiloEsterno.Length - 2].Y - profiloEsterno[profiloEsterno.Length - 1].Y, profiloEsterno[profiloEsterno.Length - 2].Z - profiloEsterno[profiloEsterno.Length - 1].Z);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0,0), new Point2D(1,1));
                                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);

                                    ////APPLY MATERIAL
                                    ////model.MaterialName = mat.Description;
                                    ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                                    models[intJ] = model;

                                intJ++;


                                //models[intJ] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));
                            }

                                //else
                                //{
                                //    l.Regen(CHORDAL_ERROR);

                                //    ArrayList alTmp = new ArrayList();
                                //    alTmp.Add(l);
                                //    Point3D[] profiloEsterno = null;

                                //    if (l.StartPoint.Z > minZ)
                                //    {
                                //        Line l0 = CreateEyeLine(l.EndPoint, new Point3D(l.EndPoint.X, l.EndPoint.Y, l.EndPoint.Z - altezza), l.Color);

                                //        Line l1 = CreateEyeLine(l0.EndPoint, new Point3D(l.StartPoint.X, l.StartPoint.Y, l0.EndPoint.Z), l.Color);

                                //        Line l2 = CreateEyeLine(l1.EndPoint, l.StartPoint, l.Color);

                                //        alTmp.Add(l0);
                                //        alTmp.Add(l1);
                                //        alTmp.Add(l2);

                                //        var alTmpToList = alTmp.Cast<ICurve>().ToList();
                                //        ////profiloEsterno = ViewportProfessional.MakeLoop(alTmp, 0, CHORDAL_ERROR, false);
                                //        profiloEsterno = ViewportLayout.MakeLoop(alTmpToList, 0, CHORDAL_ERROR, false);
                                //    }
                                //    else
                                //    {
                                //        Line l0 = CreateEyeLine(l.EndPoint, new Point3D(l.EndPoint.X, l.EndPoint.Y, l.EndPoint.Z - (altezza + shape.gThickness)), l.Color);

                                //        Line l1 = CreateEyeLine(l0.EndPoint, new Point3D(l.StartPoint.X, l.StartPoint.Y, l0.EndPoint.Z), l.Color);

                                //        Line l2 = CreateEyeLine(l1.EndPoint, l.StartPoint, l.Color);

                                //        alTmp.Add(l0);
                                //        alTmp.Add(l1);
                                //        alTmp.Add(l2);

                                //        var alTmpToList = alTmp.Cast<ICurve>().ToList();

                                //        ////profiloEsterno = ViewportProfessional.MakeLoop(alTmp, 0, CHORDAL_ERROR, false);
                                //        profiloEsterno = ViewportLayout.MakeLoop(alTmpToList, 0, CHORDAL_ERROR, false);
                                //    }

                                //    Mesh model = new Mesh();
                                //    normal = new Vector3D(profiloEsterno[1].X - profiloEsterno[0].X, profiloEsterno[1].Y - profiloEsterno[0].Y, profiloEsterno[1].Z - profiloEsterno[0].Z);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                                //    direzione = new Vector3D(profiloEsterno[profiloEsterno.Length - 2].X - profiloEsterno[profiloEsterno.Length - 1].X, profiloEsterno[profiloEsterno.Length - 2].Y - profiloEsterno[profiloEsterno.Length - 1].Y, profiloEsterno[profiloEsterno.Length - 2].Z - profiloEsterno[profiloEsterno.Length - 1].Z);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                                //    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0,0), new Point2D(1,1));
                                //    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);

                                //    ////APPLY MATERIAL
                                //    ////model.MaterialName = mat.Description;
                                //    ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                                //    models[intJ] = model;

                                //}
                                //if (shape.IsVeletta)
                                //{
                                //    Line lTmp = CreateEyeLine(l.StartPoint.X, l.StartPoint.Y, 0, l.EndPoint.X, l.EndPoint.Y, 0, l.Color);

                                //    lTmp.Regen(CHORDAL_ERROR);
                                //    models[intJ] = lTmp.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / l.Length()));
                                //}
                                //else
                                //{
                                //}

                            //}

                            else if (al[intI].GetType() == typeof(Arc))
                            {

                                Arc a = (Arc)al[intI];
                                a.Regen(CHORDAL_ERROR);
                                //models[intJ] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                                
                                ////models[intJ] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, altezza / a.Length()));


                                //TODO: Verify
                                //models[intJ] = a.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                                models[intJ] = a.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                                
                                
                                ////APPLY MATERIAL
                                ////models[intJ].MaterialName = mat.Description;
                                ////models[intJ].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

                                intJ++;

                            }

                            else
                            {

                                return null;

                            }

                        }

                        // Se esiste il profilo ed il relativo file dxf lo disegno
                        else
                        {

                            ArrayList cf = new ArrayList();

                            ////vppRender.ReadAutodesk(cf, nomeFileProfilo, null);
                            ////Point3D[] p = ViewportProfessional.MakeLoop(cf, 0, CHORDAL_ERROR, false);

                            var ra = new ReadAutodesk(nomeFileProfilo);
                            ra.DoWork();
                            var raEntitiesList = ra.Entities.Cast<ICurve>().ToList();
                            Point3D[] p = ViewportLayout.MakeLoop(raEntitiesList, 0, CHORDAL_ERROR, false);



                            Mesh emboss = new Mesh();
                            //Entity emboss = new Mesh();

                            ////((Mesh)emboss).MakeFace(p, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                            emboss = Mesh.CreatePlanar(p.ToList(), null, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////emboss.MaterialName = mat.Description;
                            ////emboss.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                            ////emboss.RotateY(90);
                            emboss.Rotate(Utility.DegToRad(90), Vector3D.AxisY);
                            emboss.Regen(CHORDAL_ERROR);

                            // L'entit� � una linea, quindi estrudo il profilo per tutta la sua lunghezza
                            if (al[intI].GetType() == typeof(Line))
                            {

                                Line l = (Line)al[intI];
                                l.Regen(CHORDAL_ERROR);

                                Mesh embossClone = emboss.Clone() as Mesh;
                                Mesh embossCloneEnd = emboss.Clone() as Mesh;

                                double angolo = 0;
                                if (entita.pricipalStruct)
                                {

                                    angolo = GetAngolo(l.StartPoint.X, l.StartPoint.Y, l.EndPoint.X, l.EndPoint.Y);

                                }

                                else
                                {

                                    angolo = GetAngolo(l.EndPoint.X, l.EndPoint.Y, l.StartPoint.X, l.StartPoint.Y);

                                }

                                //embossClone.RotateZ(angolo);
                                //embossCloneEnd.RotateZ(angolo);
                                //embossClone.Move(l.Start.X, l.Start.Y, 0);
                                //embossCloneEnd.Move(l.End.X, l.End.Y, 0);

                                embossClone.Rotate(Utility.DegToRad(angolo),Vector3D.AxisZ);
                                embossCloneEnd.Rotate(Utility.DegToRad(angolo), Vector3D.AxisZ);
                                embossClone.Translate(l.StartPoint.X, l.StartPoint.Y, 0);
                                embossCloneEnd.Translate(l.EndPoint.X, l.EndPoint.Y, 0);

                                embossClone.Regen(CHORDAL_ERROR);
                                embossCloneEnd.Regen(CHORDAL_ERROR);

                                //models[intJ] = embossClone.ExtrudeAsRichMesh(l.End.X - l.Start.X, l.End.Y - l.Start.Y, l.End.Z - l.Start.Z, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));

                                var embossCloneStart = embossClone.Clone() as Mesh;

                                ////models[intJ] = embossClone.ExtrudeAsRichMesh(l.End.X - l.Start.X, l.End.Y - l.Start.Y, l.End.Z - l.Start.Z, mat, new Point2D(0, 0), new Point2D(shape.gThickness / l.Length(), 1));


                                //TODO: Verify
                                //embossClone.Extrude(l.EndPoint.X - l.StartPoint.X, l.EndPoint.Y - l.StartPoint.Y, l.EndPoint.Z - l.StartPoint.Z);
                                embossClone.ExtrudePlanar(l.EndPoint.X - l.StartPoint.X, l.EndPoint.Y - l.StartPoint.Y, l.EndPoint.Z - l.StartPoint.Z);
                                
                                ////APPLY MATERIAL
                                ////embossClone.MaterialName = mat.Description;
                                ////embossClone.ApplyMaterial(mat.Description, textureMappingType.Cylindrical, 1, 1);
                                models[intJ] = embossClone;

                                
                                intJ++;
                                // Aggiungo le chiusure iniziale e finale del profilo
                                models[intJ] = (Mesh)embossCloneStart;
                                intJ++;
                                models[intJ] = (Mesh)embossCloneEnd;
                                intJ++;

                            }

                            // L'entit� � un arco, quindi applico una rivoluzione del profilo per tutta la circonferenza
                            else if (al[intI].GetType() == typeof(Arc))
                            {

                                //Entity e = (Entity)al[intI];
                                //e.Regen(CHORDAL_ERROR);
                                Arc a = (Arc)(Entity)al[intI];
                                a.Regen(CHORDAL_ERROR);


                                Mesh embossClone = emboss.Clone() as Mesh;
                                Mesh embossCloneEnd = emboss.Clone() as Mesh;

                                if (entita.pricipalStruct)
                                {

                                    ////embossClone.RotateZ(90 + a.StartAngle + (entita.cw == 0 ? 0 : 180));
                                    ////embossCloneEnd.RotateZ(90 + a.EndAngle + (entita.cw == 0 ? 0 : 180));
                                    embossClone.Rotate(Utility.DegToRad(90) + a.Domain.t0 + (entita.cw == 0 ? 0 : Math.PI), Vector3D.AxisZ);
                                    embossCloneEnd.Rotate(Utility.DegToRad(90) + a.Domain.t1 + (entita.cw == 0 ? 0 : Math.PI), Vector3D.AxisZ);

                                }

                                else
                                {

                                    ////embossClone.RotateZ(180 + 90 + a.StartAngle + (entita.cw != 0 ? 0 : 180));
                                    ////embossCloneEnd.RotateZ(180 + 90 + a.EndAngle + (entita.cw != 0 ? 0 : 180));
                                    embossClone.Rotate(Utility.DegToRad(180 + 90) + a.Domain.t0 + (entita.cw == 0 ? 0 : Math.PI), Vector3D.AxisZ);
                                    embossCloneEnd.Rotate(Utility.DegToRad(180 + 90) + a.Domain.t1 + (entita.cw == 0 ? 0 : Math.PI), Vector3D.AxisZ);
                                }

                                ////embossClone.Move(a.Start.X, a.Start.Y, 0);
                                ////embossCloneEnd.Move(a.End.X, a.End.Y, 0);
                                embossClone.Translate(a.StartPoint.X, a.StartPoint.Y, 0);
                                embossCloneEnd.Translate(a.EndPoint.X, a.EndPoint.Y, 0);

                                embossClone.Regen(CHORDAL_ERROR);
                                embossCloneEnd.Regen(CHORDAL_ERROR);

                                ////double angolo = a.EndAngle - a.StartAngle;
                                double angolo = a.Domain.t1 - a.Domain.t0;

                                //models[intJ] = embossClone.RevolveAsRichMesh(a.Center, Vector3D.zAxis, angolo, a.Vertices.Length - 1, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                                var embossCloneStart = embossClone.Clone() as Mesh;
                                
                                ////models[intJ] = embossClone.RevolveAsRichMesh(a.Center, Vector3D.zAxis, angolo, a.Vertices.Length - 1, mat, new Point2D(0, 0), new Point2D(1, 1));
                                
                                //TODO: Verify
                                //embossClone = Mesh.Revolve(embossClone.Vertices, 0, (angolo),
                                //        Vector3D.AxisZ, a.Center,
                                //        (int)a.AngleInDegrees / 3, false, Mesh.natureType.RichSmooth);

                                var curve = new LinearPath(embossClone.Vertices);
                                embossClone = curve.RevolveAsMesh(0, (angolo),
                                        Vector3D.AxisZ, a.Center,
                                        (int)a.AngleInDegrees / 3, CHORDAL_ERROR, Mesh.natureType.RichSmooth);



                                ////APPLY MATERIAL
                                ////embossClone.MaterialName = mat.Description;
                                ////embossClone.ApplyMaterial(mat.Description, textureMappingType.Cylindrical, 1, 1);

                                models[intJ] = embossClone;
                                intJ++;
                                // Aggiungo le chiusure iniziale e finale del profilo
                                models[intJ] = (Mesh)embossCloneStart;
                                intJ++;
                                models[intJ] = (Mesh)embossCloneEnd;
                                intJ++;

                            }

                            else
                            {

                                return null;

                            }

                        }

                    }

                }
                #endregion

                return models;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaBordi Error. ", ex);
                return null;
            }

        }

        /// <summary>
        /// Procedura di disegno dei lati della sagoma.
        /// </summary>
        /// <param name="alBussole">ArrayList delle entit� che compongono le bussole.</param>
        /// <param name="alIndiceBussole">ArrayList degli indici delle bussole, utilizzato per disegnare i bordi delle bussole singolarmente per chiudere i profili.</param>
        /// <param name="shape">Sagoma.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        private Mesh[] DisegnaBordiBussole(ArrayList alBussole, ArrayList alIndiceBussole, Shape shape, Material mat, ViewportLayout vppRender)
        {

            try
            {

                Mesh[] models = new Mesh[alBussole.Count + alIndiceBussole.Count];
                int intK = 0;
                int intCount = 0;
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                #region Ciclo per tutte le Bussole.
                for (int intI = 0; intI < shape.gBussole.Count; intI++)
                {
                    ArrayList alEntita = new ArrayList();                    // Lista di entit� che compongono la base della bussola

                    double altezza = ((ArrL)alIndiceBussole[intI]).spessore; // shape.gThickness;

                    #region Ciclo per tutte le entit� della bussola.
                    for (int intJ = 0; intJ < ((Shape.Bussola)shape.gBussole[intI]).sides.Count; intJ++, intCount++)
                    {

                        if (alBussole[intCount].GetType() == typeof(Line))
                        {

                            Line l = (Line)alBussole[intCount];
                            l.Regen(CHORDAL_ERROR);

                            //models[intK] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                            ////models[intK] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, .1));


                            //TODO: Verify
                            //models[intK] = l.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                            models[intK] = l.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////models[intK].MaterialName = mat.Description;
                            ////models[intK].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

                            intK++;

                            alEntita.Add((Line)alBussole[intCount]);

                        }

                        else if (alBussole[intCount].GetType() == typeof(Arc))
                        {

                            Arc a = (Arc)alBussole[intCount];
                            a.Regen(CHORDAL_ERROR);
                            //models[intK] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                            ////models[intK] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, .1));

                            //TODO: Verify
                            //models[intK] = a.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                            models[intK] = a.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////models[intK].MaterialName = mat.Description;
                            ////models[intK].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

                            intK++;

                            alEntita.Add((Arc)alBussole[intCount]);

                        }

                        else
                        {

                            return null;

                        }

                        ////models[intK - 1].MoveZ(shape.gThickness - altezza);
                        models[intK - 1].Translate(0, 0, shape.gThickness - altezza);

                    }
                    #endregion

                    //Mesh model = new Mesh(mat);
                    Mesh model = new Mesh();

                    var alEntitaToList = alEntita.Cast<ICurve>().ToList();

                    ////Point3D[] profiloEsterno = ViewportProfessional.MakeLoop(alEntita, 0, CHORDAL_ERROR, false);
                    Point3D[] profiloEsterno = ViewportLayout.MakeLoop(alEntitaToList, 0, CHORDAL_ERROR, false);

                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);

                    ////APPLY MATERIAL
                    ////model.MaterialName = mat.Description;
                    ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                    ////model.MoveZ(shape.gThickness - altezza);
                    model.Translate(0, 0, shape.gThickness - altezza);
                    models[intK] = model;
                    intK++;

                }
                #endregion

                return models;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaBordiBussole Error. ", ex);

                return null;
            }

        }

        /// <summary>
        /// Procedura di disegno dei bordi dei canaletti.
        /// </summary>
        /// <param name="alCanaletti">ArrayList delle entit� dei canaletti.</param>
        /// <param name="shape">Sagoma.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="profonditaCanaletti">Profondit� del canaletto.</param>
        private Mesh[] DisegnaBordiCanaletti(ArrayList alCanaletti, Shape shape, Material mat, ViewportLayout vppRender, double profonditaCanaletti)
        {

            try
            {

                int iNumCan = alCanaletti.Count;

                for (int intI = 0; intI < alCanaletti.Count; intI++)
                {
                    ArrayList alCanaletto = (ArrayList)alCanaletti[intI];
                    iNumCan += alCanaletto.Count;

                }

                Mesh[] models = new Mesh[iNumCan];
                int intK = 0;
                //int intCount = 0;
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                #region Ciclo per tutti i canaletti.
                for (int intI = 0; intI < alCanaletti.Count; intI++)
                {
                    ArrayList alCanaletto = (ArrayList) alCanaletti[intI];                    // Lista di entit� che compongono il canaletto

                    double altezza = profonditaCanaletti;

                    #region Ciclo per tutte le entit� del canaletto.
                    for (int intJ = 0; intJ < alCanaletto.Count; intJ++)
                    {

                        if (alCanaletto[intJ].GetType() == typeof(Line))
                        {

                            Line l = (Line)alCanaletto[intJ];
                            l.Regen(CHORDAL_ERROR);
                            //models[intK] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                            ////models[intK] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, .1));
                            
                            //TODO: Verify
                            //models[intK] = l.Extrude(0, 0, -altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                            models[intK] = l.ExtrudeAsMesh(0, 0, -altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////models[intK].MaterialName = mat.Description;
                            ////models[intK].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

                            intK++;

                        }

                        else if (alCanaletto[intJ].GetType() == typeof(Arc))
                        {

                            Arc a = (Arc)alCanaletto[intJ];
                            a.Regen(CHORDAL_ERROR);
                            //models[intK] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                            ////models[intK] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, .1));
                            
                            //TODO: Verify
                            //models[intK] = a.Extrude(0, 0, -altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                            models[intK] = a.ExtrudeAsMesh(0, 0, -altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////models[intK].MaterialName = mat.Description;
                            ////models[intK].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

                            intK++;

                        }

                        else
                        {

                            return null;

                        }

                        ////models[intK - 1].MoveZ(shape.gThickness - altezza);
                        models[intK - 1].Translate(0, 0, shape.gThickness );

                    }
                    #endregion

                    Mesh model = new Mesh();

                    var alCanalettoToList = alCanaletto.Cast<ICurve>().ToList();

                    ////Point3D[] profiloEsterno = ViewportProfessional.MakeLoop(alCanaletto, 0, CHORDAL_ERROR, false);
                    Point3D[] profiloEsterno = ViewportLayout.MakeLoop(alCanalettoToList, 0, CHORDAL_ERROR, false);

                    CompositeCurve profileCurve = new CompositeCurve(alCanalettoToList);
                    model = Mesh.CreatePlanar(profileCurve, MESH_CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                    model.FlipNormal();

                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // Base della bussola.
                    //model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);

                    ////model.

                    ////APPLY MATERIAL
                    ////model.MaterialName = mat.Description;
                    ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                    ////model.MoveZ(shape.gThickness - altezza);
                    model.Translate(0, 0, shape.gThickness - altezza);

                    models[intK] = model;
                    intK++;

                }
                #endregion

                return models;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaBordiCanaletti Error. ", ex);

                return null;
            }

        }
        private Mesh[] DisegnaBordiCanaletti_ORI(ArrayList alCanaletti, Shape shape, Material mat, ViewportLayout vppRender, double profonditaCanaletti)
        {

            try
            {

                int iNumCan = alCanaletti.Count;

                for (int intI = 0; intI < alCanaletti.Count; intI++)
                {
                    ArrayList alCanaletto = (ArrayList)alCanaletti[intI];
                    iNumCan += alCanaletto.Count;

                }

                Mesh[] models = new Mesh[iNumCan];
                int intK = 0;
                //int intCount = 0;
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                #region Ciclo per tutti i canaletti.
                for (int intI = 0; intI < alCanaletti.Count; intI++)
                {
                    ArrayList alCanaletto = (ArrayList) alCanaletti[intI];                    // Lista di entit� che compongono il canaletto

                    double altezza = profonditaCanaletti;

                    #region Ciclo per tutte le entit� del canaletto.
                    for (int intJ = 0; intJ < alCanaletto.Count; intJ++)
                    {

                        if (alCanaletto[intJ].GetType() == typeof(Line))
                        {

                            Line l = (Line)alCanaletto[intJ];
                            l.Regen(CHORDAL_ERROR);
                            //models[intK] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(l.Start.X, l.Start.Y), new Point2D(l.End.X, l.End.Y));
                            ////models[intK] = l.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, .1));
                            
                            //TODO: Verify
                            //models[intK] = l.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                            models[intK] = l.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////models[intK].MaterialName = mat.Description;
                            ////models[intK].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

                            intK++;

                        }

                        else if (alCanaletto[intJ].GetType() == typeof(Arc))
                        {

                            Arc a = (Arc)alCanaletto[intJ];
                            a.Regen(CHORDAL_ERROR);
                            //models[intK] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(a.Start.X, a.Start.Y), new Point2D(a.End.X, a.End.Y));
                            ////models[intK] = a.ExtrudeAsRichMesh(0, 0, altezza, mat, new Point2D(0, 0), new Point2D(1, .1));
                           
                            //TODO: Verify
                            //models[intK] = a.Extrude(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                            models[intK] = a.ExtrudeAsMesh(0, 0, altezza, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////models[intK].MaterialName = mat.Description;
                            ////models[intK].ApplyMaterial(mat.Description, textureMappingType.Spherical, 1, 1);

                            intK++;

                        }

                        else
                        {

                            return null;

                        }

                        ////models[intK - 1].MoveZ(shape.gThickness - altezza);
                        models[intK - 1].Translate(0, 0, shape.gThickness - altezza);

                    }
                    #endregion

                    Mesh model = new Mesh();

                    var alCanalettoToList = alCanaletto.Cast<ICurve>().ToList();

                    ////Point3D[] profiloEsterno = ViewportProfessional.MakeLoop(alCanaletto, 0, CHORDAL_ERROR, false);
                    Point3D[] profiloEsterno = ViewportLayout.MakeLoop(alCanalettoToList, 0, CHORDAL_ERROR, false);


                    ////model.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // Base della bussola.
                    model = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth);


                    ////APPLY MATERIAL
                    ////model.MaterialName = mat.Description;
                    ////model.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                    ////model.MoveZ(shape.gThickness - altezza);
                    model.Translate(0, 0, shape.gThickness - altezza);

                    models[intK] = model;
                    intK++;

                }
                #endregion

                return models;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaBordiCanaletti Error. ", ex);

                return null;
            }

        }

        /// <summary>
        /// Procedura di disegno delle basi del vgroove.
        /// </summary>
        /// <param name="shape">Sagoma.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="angolo">Angolo di rotazione della sagoma.</param>
        private Mesh[] DisegnaVGroove(Shape shape, Material mat, ViewportLayout vppRender, double angolo)
        {

            try
            {

                #region Ciclo per trovare il numero di entit� del vgroove.
                int intCount = 0;

                for (int intI = 0; intI <= shape.gVGrooves.Count - 1; intI++)
                {

                    Shape.VGroove vgroove = (Shape.VGroove)shape.gVGrooves[intI];

                    for (int intJ = 0; intJ < vgroove.sides.Count; intJ++)
                    {

                        if(vgroove.height > 0) intCount++;

                    }

                }
                #endregion

                Mesh[] models = new Mesh[intCount * 5];

                #region Ciclo per ogni contorno del vgroove.
                intCount = 0;

                Transformation transformation = new Transformation();

                ////transformation.RotateZ(-angolo);
                transformation.Rotation(Utility.DegToRad(-angolo), Vector3D.AxisZ);

                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                for (int intI = 0; intI <= shape.gVGrooves.Count - 1; intI++)
                {

                    Shape.VGroove vgroove = (Shape.VGroove)shape.gVGrooves[intI];

                    if (vgroove.height > 0)
                    {

                        for (int intJ = 0; intJ < vgroove.sides.Count; intJ++)
                        {

                            Shape.Contour contour = (Shape.Contour)vgroove.sides[intJ];

                            //per ogni vgroove cerco la linea parallela pi� vicina che � sulla sagoma principale
                            //con questa poi chiudo il profilo
                            Line lOr = GetLineaOriginale(shape, contour);

                            Line l = CreateEyeLine(Math.Round(contour.x0, 2), Math.Round(contour.y0, 2), Math.Round(contour.x1, 2), Math.Round(contour.y1, 2), Color.Red);

                            lOr.TransformBy(transformation);
                            l.TransformBy(transformation);
                            ArrayList al = new ArrayList();
                            //al.Add(l);

                            var line = CreateEyeLine(Math.Round(l.StartPoint.X, 2), Math.Round(l.StartPoint.Y, 2), Math.Round(l.EndPoint.X, 2), Math.Round(l.EndPoint.Y, 2), Color.Red);
                            al.Add(line);

                            line = CreateEyeLine(Math.Round(l.StartPoint.X, 2), Math.Round(l.StartPoint.Y, 2), Math.Round(lOr.StartPoint.X, 2), Math.Round(lOr.StartPoint.Y, 2), Color.Red);
                            al.Add(line);

                            line = CreateEyeLine(Math.Round(lOr.StartPoint.X, 2), Math.Round(lOr.StartPoint.Y, 2), Math.Round(lOr.EndPoint.X, 2), Math.Round(lOr.EndPoint.Y, 2), Color.Red);
                            al.Add(line);

                            line = CreateEyeLine(Math.Round(l.EndPoint.X, 2), Math.Round(l.EndPoint.Y, 2), Math.Round(lOr.EndPoint.X, 2), Math.Round(lOr.EndPoint.Y, 2), Color.Red);
                            al.Add(line);

                            var alToList = al.Cast<ICurve>().ToList();
                            ////Point3D[] profilo = ViewportProfessional.MakeLoop(al, 0, CHORDAL_ERROR, true);
                            Point3D[] profilo = ViewportLayout.MakeLoop(alToList, 0, CHORDAL_ERROR, true);

                            models[intCount] = new Mesh();

                            ////models[intCount].MakeFace(profilo, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                            models[intCount] = Mesh.CreatePlanar(profilo.ToList(), null, Mesh.natureType.RichSmooth);

                            ////APPLY MATERIAL
                            ////models[intCount].MaterialName = mat.Description;
                            ////models[intCount].ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                            //sposto la faccia in basso per un valore pari all'altezza del VGR
                            ////models[intCount].MoveZ(-vgroove.height);
                            models[intCount].Translate(0, 0, -vgroove.height);

                            //vppRender.Entities.Add(models[intCount]);
                            intCount++;

                            //chiudo con le facce laterali
                            for (int intS = 0; intS < al.Count; intS++)
                            {

                                Mesh modelTmp = new Mesh();

                                Line lTmp = (Line)al[intS];
                                lTmp.Regen(CHORDAL_ERROR);

                                ////models[intCount] = lTmp.ExtrudeAsRichMesh(0, 0, -vgroove.height, mat, new Point2D(0, 0), new Point2D(1, vgroove.height / lTmp.Length()));
 
                                //TODO:verify
                                //models[intCount] = lTmp.Extrude(0, 0, -vgroove.height, CHORDAL_ERROR, Mesh.natureType.RichSmooth);
                                models[intCount] = lTmp.ExtrudeAsMesh(0, 0, -vgroove.height, CHORDAL_ERROR, Mesh.natureType.RichSmooth);

                                ////APPLY MATERIAL
                                ////models[intCount].MaterialName = mat.Description;
                                ////models[intCount].ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                                //vppRender.Entities.Add(models[intCount]);
                                intCount++;

                            }

                        }

                    }

                }

                return models;

                #endregion

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaVGroove Error. ", ex);
                return null;
            }

        }

        /// <summary>
        /// Procedura di disegno dello scalino dei fori ribassati.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="alIndice">ArrayList degli indici delle sagome, utilizzato per disegnare un ribasso alla volta.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="modelRibasso">Modello disegnato.</param>
        /// <param name="profonditaRibasso">Profondit� del ribasso.</param>
        private void DisegnaRibasso(ArrayList al, ArrayList alIndice, Material mat, ViewportLayout vppRender, ref Mesh modelRibasso, double profonditaRibasso)
        {

            try
            {

                Point3D[] profiloEsterno = null;

                #region Ciclo per trovare il numero di entit� del Ribasso.
                int iCount = -1;
                for (int intI = 0; intI < alIndice.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo.Substring(0, 2) != "CP" &&
                        arrL.tipo != "SNP0002" && arrL.sottoTipo != "Groove" && arrL.tipo != "SNP0008" &&
                        (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole")))
                          iCount++;

                }
                #endregion

                if (iCount == 0) iCount++;

                Point3D[][] profiloInterno = new Point3D[iCount][];

                int intJ = -1;

                double altezza = 0;

                #region Ciclo per creare i contorni della sagoma principale e delle sagome figlie.

                var alToList = al.Cast<ICurve>().ToList();


                for (int intI = 0; intI < alIndice.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo == "PS")
                    {

                        altezza = arrL.spessore;


                        ////profiloEsterno = ViewportProfessional.MakeLoop(al, arrL.indice, CHORDAL_ERROR, false);
                        profiloEsterno = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);

                    }

                    else if (arrL.tipo.Substring(0, 2) != "CP" &&
                             arrL.tipo != "SNP0002" && arrL.sottoTipo != "Groove" && arrL.tipo != "SNP0008" &&
                             (arrL.tipo != "SNP0006" || (arrL.tipo == "SNP0006" && arrL.sottoTipo != "SinkHole")))
                    {

                        intJ++;

                        ////profiloInterno[intJ] = ViewportProfessional.MakeLoop(al, arrL.indice, CHORDAL_ERROR, true);
                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);

                    }

                }
                #endregion

                modelRibasso = new Mesh();
                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                if (intJ == -1)
                {

                    ////modelRibasso.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // La sagoma della base non contiene sagome figlie.
                    modelRibasso = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth); 
                }

                else
                {

                    try
                    {
                        var profiloInternoList = GetListOfListOfPoints(profiloInterno); 
                        ////modelRibasso.MakeFace(profiloEsterno, profiloInterno, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                         // La sagoma della base contiene sagome figlie, quindi bisogna forare.
                        modelRibasso = Mesh.CreatePlanar(profiloEsterno.ToList(), profiloInternoList, Mesh.natureType.RichSmooth); 

                    }

                    catch                                                                       // Errore
                    {

                        #region Stampo in output la lista dei punti dei profili

                        System.Diagnostics.Debug.WriteLine("Profilo eEsterno faccia sopra");

                        for (int intI = 0; intI < profiloEsterno.Length; intI++)
                        {

                            System.Diagnostics.Debug.WriteLine(profiloEsterno[intI].ToString());

                        }

                        System.Diagnostics.Debug.WriteLine("Profilo interno faccia sopra");

                        for (int intI = 0; intI < profiloInterno.Length; intI++)
                        {

                            for (int intK = 0; intK < profiloInterno[intI].Length; intK++)
                            {

                                System.Diagnostics.Debug.WriteLine(profiloInterno[intI][intK].ToString());

                            }

                        }

                        #endregion

                        ////modelRibasso.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                        modelRibasso = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth); 

                    }

                }

                modelRibasso.SmoothingAngle = Utility.DegToRad(20);

                ////APPLY MATERIAL
                ////modelRibasso.MaterialName = mat.Description;
                ////modelRibasso.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                ////modelRibasso.MoveZ(altezza - profonditaRibasso);
                modelRibasso.Translate(0, 0, altezza - profonditaRibasso);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaRibasso Error. ", ex);
            }
        
        }

        /// <summary>
        /// Procedura di disegno dello scalino delle tasche.
        /// </summary>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="alIndice">ArrayList degli indici delle sagome, utilizzato per disegnare un ribasso alla volta.</param>
        /// <param name="mat">Materiale della sagoma.</param>
        /// <param name="vppRender">Area di disegno.</param>
        /// <param name="modelRibasso">Modello disegnato.</param>
        /// <param name="profonditaTasca">Profondit� della tasca.</param>
        private void DisegnaRibassoTasca(ArrayList al, ArrayList alIndice, Material mat, ViewportLayout vppRender, ref Mesh modelRibasso, double profonditaTasca)
        {

            try
            {

                Point3D[] profiloEsterno = null;

                #region Ciclo per trovare il numero di entit� del ribasso.
                int iCount = -1;
                for (int intI = 0; intI < alIndice.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo.Substring(0, 2) != "CP" &&
                        arrL.tipo != "SNP0002" && arrL.sottoTipo != "Groove" && arrL.tipo != "SNP0008" && arrL.sottoTipo != "Pocket")
                        iCount++;

                }
                #endregion

                if (iCount == 0) iCount++;

                Point3D[][] profiloInterno = new Point3D[iCount][];

                int intJ = -1;

                double altezza = 0;

                #region Ciclo per creare i contorni della sagoma principale e delle sagome figlie.

                var alToList = al.Cast<ICurve>().ToList();

                for (int intI = 0; intI < alIndice.Count; intI++)
                {
                    ArrL arrL = (ArrL)alIndice[intI];

                    if (arrL.tipo == "PS")
                    {

                        altezza = arrL.spessore;

                        ////profiloEsterno = ViewportProfessional.MakeLoop(al, arrL.indice, CHORDAL_ERROR, false);
                        profiloEsterno = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, false);

                    }

                    else if (arrL.tipo.Substring(0, 2) != "CP" &&
                             arrL.tipo != "SNP0002" && arrL.sottoTipo != "Groove" && arrL.tipo != "SNP0008" && arrL.sottoTipo != "Pocket")
                    {

                        intJ++;

                        ////profiloInterno[intJ] = ViewportProfessional.MakeLoop(al, arrL.indice, CHORDAL_ERROR, true);
                        profiloInterno[intJ] = ViewportLayout.MakeLoop(alToList, arrL.indice, CHORDAL_ERROR, true);

                    }

                }

                #endregion

                modelRibasso = new Mesh();

                Vector3D normal = new Vector3D(1, 0, 0);// new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 1));
                Vector3D direzione = new Vector3D(0, 1, 0);//Vector3D direzione = new Vector3D(new Point3D(0, 0), new Point3D(1, 0), new Point3D(0, 0, -1));

                if (intJ == -1)
                {

                    ////modelRibasso.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                                       // La sagoma della base non contiene sagome figlie.
                    modelRibasso = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth); 
                }

                else
                {

                    try
                    {
                        var profiloInternoList = GetListOfListOfPoints(profiloInterno); 
                        ////modelRibasso.MakeFace(profiloEsterno, profiloInterno, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));                         // La sagoma della base contiene sagome figlie, quindi bisogna forare.
                        modelRibasso = Mesh.CreatePlanar(profiloEsterno.ToList(), profiloInternoList, Mesh.natureType.RichSmooth); 
                    }

                    catch                                                                       // Errore
                    {

                        #region Stampo in output la lista dei punti dei profili

                        System.Diagnostics.Debug.WriteLine("Profilo eEsterno faccia sopra");

                        for (int intI = 0; intI < profiloEsterno.Length; intI++)
                        {

                            System.Diagnostics.Debug.WriteLine(profiloEsterno[intI].ToString());

                        }

                        System.Diagnostics.Debug.WriteLine("Profilo interno faccia sopra");

                        for (int intI = 0; intI < profiloInterno.Length; intI++)
                        {

                            for (int intK = 0; intK < profiloInterno[intI].Length; intK++)
                            {

                                System.Diagnostics.Debug.WriteLine(profiloInterno[intI][intK].ToString());

                            }

                        }

                        #endregion

                        ////modelRibasso.MakeFace(profiloEsterno, null, normal, direzione, new Point2D(0, 0), new Point2D(1, 1));
                        modelRibasso = Mesh.CreatePlanar(profiloEsterno.ToList(), null, Mesh.natureType.RichSmooth); 
                    }

                }

                modelRibasso.SmoothingAngle = Utility.DegToRad(20);

                ////APPLY MATERIAL
                ////modelRibasso.MaterialName = mat.Description;
                ////modelRibasso.ApplyMaterial(mat.Description, textureMappingType.Cubic, 1, 1);

                ////modelRibasso.MoveZ(altezza - profonditaTasca);
                modelRibasso.Translate(0, 0, altezza - profonditaTasca);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DisegnaRibasso Error. ", ex);
            }

        }
        #endregion

        #region Disegno Entit�
        /// <summary>
        /// Procedura di disegno della linea.
        /// </summary>
        /// <param name="cont">Singola entit�.</param>
        /// <param name="coloreSagoma">Colore da dare alla linea.</param>
        /// <param name="angolo">Angolo di rotazione assoluto della sagoma in Design Master, serve per riportare la sagoma ad un angolo 0.</param>
        /// <param name="al">ArrayList delle entit�.</param>
        private void DrawLine(Shape.Contour cont, Color coloreSagoma, ref ArrayList al, double angolo)
        {

            try
            {

                Point3D pS = new Point3D();
                Point3D pE = new Point3D();

                pS.X = cont.x0;
                pS.Y = cont.y0;
                pS.Z = cont.z0;// +incremento;

                pE.X = cont.x1;
                pE.Y = cont.y1;
                pE.Z = cont.z1;// + incremento;

                Transformation transformation = new Transformation();
                ////transformation.RotateZ(-angolo);
                transformation.Rotation(Utility.DegToRad(-angolo), Vector3D.AxisZ);

                ////transformation.Apply(ref pS);
                ////transformation.Apply(ref pE);
                pS = transformation*pS;
                pE = transformation*pE;

                if (cont.pricipalStruct)
                {

                    //Line l = CreateEyeLine(pS, pE, coloreSagoma);

                    ////al.Add(CreateEyeLine(pS, pE, coloreSagoma));
                    var line = CreateEyeLine(pS, pE, coloreSagoma);
                    al.Add(line);


                    //if (angoloPrimoElemento == -361)
                    //{

                    //    angoloPrimoElemento = GetAngolo(cont.x0, cont.y0, cont.x1, cont.y1) - angolo;

                    //}

                }

                else /*if (cont.code.Substring(0, 2) != "CP" &&
                   cont.code != "SNP0002")*/
                {

                    //Line l = CreateEyeLine(pE, pS, coloreSagoma);

                    ////al.Add(CreateEyeLine(pE, pS, coloreSagoma));
                    var line = CreateEyeLine(pE, pS, coloreSagoma);
                    al.Add(line);


                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawLine Error. ", ex);
            }

        }

        /// <summary>
        /// Procedura di disegno dell'arco.
        /// </summary>
        /// <param name="cont">Singola entit�.</param>
        /// <param name="al">ArrayList delle entit�.</param>
        /// <param name="angolo">Angolo di rotazione assoluto della sagoma in Design Master, serve per riportare la sagoma ad un angolo 0.</param>
        /// <param name="coloreSagoma">Colore da dare alla linea.</param>
        private void DrawArc_NEW(Shape.Contour cont, ref ArrayList al, double angolo, Color coloreSagoma)
        {

            try
            {

                Point3D pC = new Point3D();

                double raggio = cont.r;
                double startAngle = cont.startAngle - angolo;
                double endAngle = cont.endAngle - angolo;

                while (startAngle < 0 )
                {
                    startAngle += 360;
                }
                while (endAngle < 0)
                {
                    endAngle += 360;
                }


                while (endAngle> 360)
                {
                    endAngle -= 360;
                }
                while (startAngle> 360)
                {
                    startAngle -= 360;
                }



                pC.X = cont.xC;
                pC.Y = cont.yC;
                pC.Z = cont.zC; // + incremento;

                Transformation transformation = new Transformation();

                //transformation.RotateZ(-angolo);
                transformation.Rotation(Utility.DegToRad(-angolo), Vector3D.AxisZ);

                ////transformation.Apply(ref pC);
                pC = transformation*pC;

                ////var arc = CreateEyeArc(pC, raggio, Utility.DegToRad(startAngle), Utility.DegToRad(endAngle), coloreSagoma);
                ////al.Add(arc);

                if (cont.cw == 0)
                {

                    //Arc a = new Arc(pC.X, pC.Y, pS.X, pS.Y, pE.X, pE.Y);
                    ////al.Add(new Arc(pC, raggio, startAngle, endAngle, coloreSagoma));//(pC.X, pC.Y, pS.X, pS.Y, pE.X, pE.Y));

                    var arc = CreateEyeArc(pC, raggio, Utility.DegToRad(startAngle), Utility.DegToRad(endAngle), coloreSagoma);

                    al.Add(arc);

                }

                else
                {

                    //Arc a = new Arc(pC.X, pC.Y, pE.X, pE.Y, pS.X, pS.Y);

                    ////al.Add(new Arc(pC, raggio, endAngle, startAngle, coloreSagoma));//(pC.X, pC.Y, pE.X, pE.Y, pS.X, pS.Y));
                    var arc = CreateEyeArc(pC, raggio, Utility.DegToRad(endAngle), Utility.DegToRad(startAngle), coloreSagoma);

                    al.Add(arc);

                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawArc Error. ", ex);
            }

        }
        private void DrawArc(Shape.Contour cont, ref ArrayList al, double angolo, Color coloreSagoma)
        {

            try
            {

                Point3D pC = new Point3D();

                double raggio = cont.r;
                double startAngle = cont.startAngle - angolo;


                double endAngle = cont.endAngle == 0 ? 360 - angolo : cont.endAngle - angolo;
                //double endAngle =  cont.endAngle - angolo;

                ////double min = Math.Min(startAngle, endAngle);
                ////while (min < 0)
                ////{
                ////    endAngle += 360;
                ////    startAngle += 360;
                ////    min = Math.Min(startAngle, endAngle);
                ////}

                ////while ((endAngle - startAngle) > 360)
                ////{
                ////    if (endAngle < 0)
                ////        endAngle += 360;
                ////    else
                ////    {
                ////        endAngle -= 360;
                ////    }
                ////}
                ////while ((startAngle - endAngle) > 360)
                ////{
                ////    if (startAngle < 0)
                ////        startAngle += 360;
                ////    else
                ////    {
                ////        startAngle -= 360;
                ////    }
                ////}

                ////double diff = endAngle - startAngle;


                while (startAngle < 0) startAngle += 360;
                while (endAngle < 0) endAngle += 360;

                ////endAngle = startAngle + diff;

                //if (startAngle < 0) startAngle += 360;
                //if (endAngle < 0) endAngle += 360;

                pC.X = cont.xC;
                pC.Y = cont.yC;
                pC.Z = cont.zC;// + incremento;

                Transformation transformation = new Transformation();

                //transformation.RotateZ(-angolo);
                transformation.Rotation(Utility.DegToRad(-angolo), Vector3D.AxisZ);

                //transformation.Apply(ref pS);
                //transformation.Apply(ref pE);

                ////transformation.Apply(ref pC);
                pC = transformation * pC;




                if (cont.cw == 0)
                {
                    if (endAngle < startAngle)
                        endAngle += 360;
                    //Arc a = new Arc(pC.X, pC.Y, pS.X, pS.Y, pE.X, pE.Y);
                    ////al.Add(new Arc(pC, raggio, startAngle, endAngle, coloreSagoma));//(pC.X, pC.Y, pS.X, pS.Y, pE.X, pE.Y));

                    var arc = CreateEyeArc(pC, raggio, Utility.DegToRad(startAngle), Utility.DegToRad(endAngle), coloreSagoma);

                    al.Add(arc);

                }

                else
                {
                    if (endAngle > startAngle)
                        endAngle -= 360;
                    var arc = CreateEyeArc(pC, raggio, Utility.DegToRad(startAngle), Utility.DegToRad(endAngle), coloreSagoma);

                    //Arc a = new Arc(pC.X, pC.Y, pE.X, pE.Y, pS.X, pS.Y);

                    ////al.Add(new Arc(pC, raggio, endAngle, startAngle, coloreSagoma));//(pC.X, pC.Y, pE.X, pE.Y, pS.X, pS.Y));
                    ////var arc = CreateEyeArc(pC, raggio, Utility.DegToRad(endAngle), Utility.DegToRad(startAngle), coloreSagoma);

                    al.Add(arc);

                }

                //if (cont.pricipalStruct)
                //{

                //    if (angoloPrimoElemento == -361)
                //    {

                //        Point3D pS = new Point3D();
                //        Point3D pE = new Point3D();

                //        if (cont.cw == 0)
                //        {

                //            pS.X = cont.x0;
                //            pS.Y = cont.y0;
                //            pS.Z = cont.z0;// + incremento;

                //            pE.X = cont.x1;
                //            pE.Y = cont.y1;
                //            pE.Z = cont.z1;// + incremento;

                //        }

                //        else
                //        {

                //            pS.X = cont.x1;
                //            pS.Y = cont.y1;
                //            pS.Z = cont.z1;// + incremento;

                //            pE.X = cont.x0;
                //            pE.Y = cont.y0;
                //            pE.Z = cont.z0;// + incremento;

                //        }

                //        angoloPrimoElemento = GetAngolo(pS.X, pS.Y, pE.X, pE.Y) - angolo;

                //    }

                //}

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawArc Error. ", ex);
            }

        }

        #endregion

        #region Modifica sagome
		/// <summary>
		/// Procedura di modifica del materiale di una o pi� sagome.
		/// </summary>
		/// <param name="shapeNumber">Vettore di interi, contiene gli indici delle Sagome.</param>
		/// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
		/// <param name="vppRender">Area di disegno.</param>
        internal void ChangeMaterial(int[] shapeNumber, Bitmap[] pathImage, ViewportLayout vppRender)
		{

			try
			{

			    for (int i = 0; i < shapeNumber.Length; i++)
			    {
			        if (pathImage.Length > i)
			        {
			            ChangeMaterial(shapeNumber[i], pathImage[i], vppRender, false);
			        }
			    }

                vppRender.Entities.Regen();
                vppRender.Invalidate();




                ////for (int intI = 0; intI <= shapeNumber.Length - 1; intI++)
                ////{

                ////    #region Definizione del materiale.
                ////    Material mat = null;
                ////    int mText = 0;

                ////    Bitmap m = pathImage[intI];

                ////    if (m != null)
                ////    {
                ////    }

                ////    else
                ////    {

                ////        ////mat = new Material("NOMAT");
                ////        mat = new Material();
                ////        mat.Description = "NOMAT";

                ////    }

                ////    #endregion

                ////    int idshape = shapeNumber[intI];

                ////    Mesh rm = null;

                ////    for (int intM = 0; intM <= vppRender.Entities.Count - 1; intM++)
                ////    {

                ////        if (((Mesh)vppRender.Entities[intM]).EntityData.ToString() == idshape.ToString())
                ////        {

                ////            rm = (Mesh)vppRender.Entities[intM];

                ////            break;

                ////        }

                ////    }

                ////    if (rm != null && m != null)
                ////    {

                ////        ////////Texture t = new Texture(m, Texture.minifyingFunctionType.Linear);
                ////        ////Texture t = new Texture(m);
                ////        //////vppRender.Textures.(rm.Materials[0].TextureIndex, t);
                ////        ////mText = rm.Materials[0].TextureIndex;
                ////        ////vppRender.Textures.RemoveAt(mText);
                ////        ////vppRender.Textures.Insert(mText, t);
                ////        //////mat = new Material("mat" + mText.ToString(), mText);

                ////        //////rm.Materials.SetValue(mat, 0);
                ////        ////vppRender.Entities.Compile(rm);

                ////        var materialName = rm.MaterialName;
                ////        if (vppRender.Materials.ContainsKey(materialName))
                ////        {
                ////            vppRender.Materials[materialName].TextureImage = m;
                ////        }
                ////    }

                ////}

                ////////vppRender.Refresh();
                ////vppRender.Entities.Regen();
                ////vppRender.Invalidate();
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ChangeMaterial Error. ", ex);
			}
		}

		/// <summary>
		/// Procedura di modifica del materiale di una sagoma.
		/// </summary>
		/// <param name="shapeNumber">id della Sagoma.</param>
		/// <param name="image">immagine.</param>
		/// <param name="vppRender">Area di disegno.</param>
		/// <param name="regen">Ridisegno immediato.</param>
		internal void ChangeMaterial_O(int shapeNumber, Bitmap image, ViewportLayout vppRender, bool regen = true)
		{
			try
			{
			    if (image == null || vppRender == null) return;

				var mesh = (Mesh)vppRender.Entities.FirstOrDefault(
			            e => (e is Mesh && e.EntityData != null && e.EntityData.ToString() == shapeNumber.ToString()));



				if (mesh != null )
				{
				    Material material = null;

				    string materialName = mesh.MaterialName;

                    mesh.RemoveMaterial();

				    Material oldMaterial = null;
				    if (vppRender.Materials.ContainsKey(materialName))
				    {
                        oldMaterial = vppRender.Materials[materialName];
                        vppRender.Materials.Remove(materialName);
                        //oldMaterial.TextureImage = image;
                        oldMaterial.Dispose();
				    }

				    if (string.IsNullOrEmpty(materialName))
				    {
                        materialName = shapeNumber.ToString();
				    }

                    DateTime start;

                    //material = oldMaterial;
                    //var bitmap = (Bitmap)image;
                    using (Bitmap bitmap = (Bitmap) image)
                    {
                        material = new Material((Bitmap)bitmap.Clone()) {Description = materialName};
                        start = DateTime.Now;
                        vppRender.Materials.Add(materialName, material);

                        //bitmap.Dispose();



                        DateTime stop = DateTime.Now;
                        double dur = (stop - start).TotalMilliseconds;
                    //////TraceLog.WriteLine("vppRender.Materials.Add(materialName, material) DURATION: " + dur.ToString());

                    // De Conti 20161123: trasformazione e controtrasformazione per elementi a sviluppo verticale
                    bool transform = false;
                    RenderedElement element = null;
                    Point3D rotationCenter = new Point3D(0, 0, 0);
                    Mesh newMesh = null;

                    Vector3D rotationAxis = Vector3D.AxisY;
                    if (mesh.XData != null)
                    {
                        var kvp = mesh.XData.FirstOrDefault(k => k.Key == 0);
                        if (kvp.Key == 0)
                        {
                            element = kvp.Value as RenderedElement;
                            if (element != null)
                            {
                                if (element.ElementType == ProjectElementType.Rib
                                    || element.ElementType == ProjectElementType.Backsplash)
                                {
                                    transform = true;
                                }
                            }
                        }
                    }

                    if (transform) // put horizontal
                    {
                        //mesh.ApplyMaterial(string.Empty, textureMappingType.Cubic, 1.0, 1.0);
                        rotationCenter = mesh.BoxMin;
                        switch (element.ElementType)
                        {
                            case ProjectElementType.Backsplash:
                                rotationAxis = new Vector3D(Math.Cos(Math.PI - element.OriginalProjectAngle), Math.Sin(Math.PI - element.OriginalProjectAngle), 0);
                                break;

                            case ProjectElementType.Rib:
                                rotationAxis = new Vector3D(Math.Cos(element.OriginalProjectAngle), Math.Sin(element.OriginalProjectAngle), 0);
                                break;
                        }
                        switch (element.ElementType)
                        {
                            case ProjectElementType.Rib:
                                //mesh.Rotate(-element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);
                                mesh.Rotate(-Math.PI / 2, rotationAxis, rotationCenter);
                                break;

                            case ProjectElementType.Backsplash:
                                //mesh.Rotate(-element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);
                                mesh.Rotate(Math.PI / 2, rotationAxis, rotationCenter);

                                break;
                        }
                    }

                    // Cut mesh to invert normals and then merge the two parts
                    Point3D minQuote = mesh.BoxMin;
                    minQuote.Z += 0.01;
                    Plane cutPlane = Plane.XY;
                    cutPlane.Translate(0, 0, minQuote.Z);
                    Mesh[] meshParts = null;

                    mesh.UpdateNormals();

                    var result = mesh.SplitBy(cutPlane, out meshParts);
                    if (result == booleanFailureType.Success && meshParts.Length == 2)
                    {
                        meshParts[0].UpdateNormals();
                        meshParts[1].UpdateNormals();

                        if (!element.IsFlipped)
                        {
                            meshParts[0].FlipNormal();
                            element.IsFlipped = true;
                        }

                        newMesh = meshParts[0];
                        newMesh.MergeWith(meshParts[1]);
                        newMesh.EntityData = mesh.EntityData;
                        newMesh.XData = new List<KeyValuePair<short, object>>();
                        newMesh.XData.Add(new KeyValuePair<short, object>(0, element));
                        //newMesh.MaterialName = mesh.MaterialName;
                    }


                    //mesh.Regen(CHORDAL_ERROR);
                    vppRender.Entities.Regen();
                    vppRender.Invalidate();

                    ////    ////Application.DoEvents();


                        if (newMesh != null)
                        {
                            newMesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                        }
                        else
                        {
                            mesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                            mesh.Regen(CHORDAL_ERROR);
                        }


                        if (transform && newMesh != null) // reset position
                        {
                            switch (element.ElementType)
                            {
                                case ProjectElementType.Rib:
                                    newMesh.Rotate(Math.PI / 2, rotationAxis, rotationCenter);
                                    //mesh.Rotate(element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);

                                    break;

                                case ProjectElementType.Backsplash:
                                    newMesh.Rotate(-Math.PI / 2, rotationAxis, rotationCenter);
                                    //mesh.Rotate(element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);

                                    break;
                            }

                        }
                        newMesh.Regen(CHORDAL_ERROR);
                        vppRender.Entities.Remove(mesh);
                        vppRender.Entities.Add(newMesh);


                    if (regen)
                    {
                        vppRender.Entities.Regen();
                        vppRender.Invalidate();
                    }

                    if (oldMaterial != null)
                        oldMaterial.Dispose();

                    DateTime stop2 = DateTime.Now;
                    double dur2 = (stop2 - stop).TotalMilliseconds;

                        //TraceLog.WriteLine("regen DURATION: " + dur2.ToString());

                            OnMaterialChanged(new List<object>(){material, newMesh});
                            //material.Dispose();
                        }
                }
			}
			catch (Exception ex)
			{
				TraceLog.WriteLine("ChangeMaterial Error. ", ex);
			}
		}

        internal void ChangeMaterial(int shapeNumber, Bitmap image, ViewportLayout vppRender, bool regen = true)
        {
            try
            {
                if (image == null || vppRender == null) return;

                var mesh = (Mesh)vppRender.Entities.FirstOrDefault(
                        e => (e is Mesh && e.EntityData != null && e.EntityData.ToString() == shapeNumber.ToString()));



                if (mesh != null)
                {
                    Material material = null;

                    string materialName = mesh.MaterialName;

                    mesh.RemoveMaterial();

                    Material oldMaterial = null;
                    if (vppRender.Materials.ContainsKey(materialName))
                    {
                        oldMaterial = vppRender.Materials[materialName];
                        vppRender.Materials.Remove(materialName);
                    }

                    if (string.IsNullOrEmpty(materialName))
                    {
                        materialName = shapeNumber.ToString();
                    }

                    DateTime start;

                    //var bitmap = (Bitmap)BuildSquareBitmap(image);
                    var bitmap = (Bitmap)image;
                    //using (Bitmap bitmap = (Bitmap) BuildSquareBitmap(image))
                    {
                        material = new Material(bitmap);
                        material.Description = materialName;
                        start = DateTime.Now;


                        vppRender.Materials.Add(materialName, material);

                        
                        //bitmap.Dispose();



                        DateTime stop = DateTime.Now;
                        double dur = (stop - start).TotalMilliseconds;
                        //TraceLog.WriteLine("vppRender.Materials.Add(materialName, material) DURATION: " + dur.ToString());

                        // De Conti 20161123: trasformazione e controtrasformazione per elementi a sviluppo verticale
                        bool transform = false;
                        RenderedElement element = null;
                        Point3D rotationCenter = new Point3D(0, 0, 0);
                        Mesh newMesh = null;

                        Vector3D rotationAxis = Vector3D.AxisY;
                        if (mesh.XData != null)
                        {
                            var kvp = mesh.XData.FirstOrDefault(k => k.Key == 0);
                            if (kvp.Key == 0)
                            {
                                element = kvp.Value as RenderedElement;
                                if (element != null)
                                {
                                    if (element.ElementType == ProjectElementType.Rib
                                        || element.ElementType == ProjectElementType.Backsplash)
                                    {
                                        transform = true;
                                    }
                                }
                            }
                        }

                        if (transform) // put horizontal
                        {
                            //mesh.ApplyMaterial(string.Empty, textureMappingType.Cubic, 1.0, 1.0);
                            rotationCenter = mesh.BoxMin;
                            switch (element.ElementType)
                            {
                                case ProjectElementType.Backsplash:
                                    rotationAxis = new Vector3D(Math.Cos(Math.PI - element.OriginalProjectAngle), Math.Sin(Math.PI - element.OriginalProjectAngle), 0);
                                    break;

                                case ProjectElementType.Rib:
                                    rotationAxis = new Vector3D(Math.Cos(element.OriginalProjectAngle), Math.Sin(element.OriginalProjectAngle), 0);
                                    break;
                            }
                            switch (element.ElementType)
                            {
                                case ProjectElementType.Rib:
                                    //mesh.Rotate(-element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);
                                    mesh.Rotate(-Math.PI / 2, rotationAxis, rotationCenter);
                                    break;

                                case ProjectElementType.Backsplash:
                                    //mesh.Rotate(-element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);
                                    mesh.Rotate(Math.PI / 2, rotationAxis, rotationCenter);

                                    break;
                            }
                        }


                        // 20171005 avoid splitting and recompose mesh

                        ////// Cut mesh to invert normals and then merge the two parts
                        ////Point3D minQuote = mesh.BoxMin;
                        //////minQuote.Z += 0.01;
                        ////minQuote.Z += 0.05;
                        ////Plane cutPlane = Plane.XY;
                        ////cutPlane.Translate(0, 0, minQuote.Z);
                        ////Mesh[] meshParts = null;

                        ////mesh.UpdateNormals();

                        ////var result = mesh.SplitBy(cutPlane, out meshParts);
                        //////if (result == booleanFailureType.Success && meshParts.Length == 2)
                        ////if (result == booleanFailureType.Success)
                        ////{
                        ////    meshParts[0].UpdateNormals();
                        ////    meshParts[meshParts.Length -1].UpdateNormals();

                        ////    if (!element.IsFlipped)
                        ////    {
                        ////        meshParts[0].FlipNormal();
                        ////        element.IsFlipped = true;
                        ////    }

                        ////    newMesh = meshParts[0];
                        ////    //newMesh.MergeWith(meshParts[1]);
                        ////    for (int i = 1; i < meshParts.Length; i++)
                        ////    {
                        ////        newMesh.MergeWith(meshParts[i]);
                        ////    }
                        ////    newMesh.EntityData = mesh.EntityData;
                        ////    newMesh.XData = new List<KeyValuePair<short, object>>();
                        ////    newMesh.XData.Add(new KeyValuePair<short, object>(0, element));
                        ////    //newMesh.MaterialName = mesh.MaterialName;
                        ////}


                        //////mesh.Regen(CHORDAL_ERROR);
                        ////vppRender.Entities.Regen();
                        ////vppRender.Invalidate();
                        /// 
                        newMesh = (Mesh)mesh.FullClone();

                        //Application.DoEvents();

                        if (newMesh != null)
                        {
                            newMesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                            newMesh.Regen(CHORDAL_ERROR);
                        }
                        else
                        {
                            mesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                            mesh.Regen(CHORDAL_ERROR);
                        }


                        if (transform && newMesh != null) // reset position
                        {
                            switch (element.ElementType)
                            {
                                case ProjectElementType.Rib:
                                    newMesh.Rotate(Math.PI / 2, rotationAxis, rotationCenter);
                                    //mesh.Rotate(element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);

                                    break;

                                case ProjectElementType.Backsplash:
                                    newMesh.Rotate(-Math.PI / 2, rotationAxis, rotationCenter);
                                    //mesh.Rotate(element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);

                                    break;
                            }

                            newMesh.Regen(CHORDAL_ERROR);
                        }

                        if (newMesh != null)
                        {
                            vppRender.Entities.Remove(mesh);
                            vppRender.Entities.Add(newMesh);

                        }


                        if (regen)
                        {
                            vppRender.Entities.Regen();
                            vppRender.Invalidate();
                        }

                        if (oldMaterial != null)
                            oldMaterial.Dispose();

                        DateTime stop2 = DateTime.Now;
                        double dur2 = (stop2 - stop).TotalMilliseconds;

                        //TraceLog.WriteLine("regen DURATION: " + dur2.ToString());

                        OnMaterialChanged(new List<object>() { material, newMesh });
                        //material.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ChangeMaterial Error. ", ex);
            }
        }


        internal Mesh NormalizeMesh(Mesh mesh, ViewportLayout vppRender, bool regen = false)
        {
            Mesh newMesh = null;
            try
            {
                if (mesh != null && vppRender != null)
                {
                        bool transform = false;
                        RenderedElement element = null;
                        Point3D rotationCenter = new Point3D(0, 0, 0);
                        

                        Vector3D rotationAxis = Vector3D.AxisY;
                        if (mesh.XData != null)
                        {
                            var kvp = mesh.XData.FirstOrDefault(k => k.Key == 0);
                            if (kvp.Key == 0)
                            {
                                element = kvp.Value as RenderedElement;
                                if (element != null)
                                {
                                    if (element.ElementType == ProjectElementType.Rib
                                        || element.ElementType == ProjectElementType.Backsplash)
                                    {
                                        transform = true;
                                    }
                                }
                            }
                        }

                        if (transform) // put horizontal
                        {
                            //mesh.ApplyMaterial(string.Empty, textureMappingType.Cubic, 1.0, 1.0);
                            rotationCenter = mesh.BoxMin;
                            switch (element.ElementType)
                            {
                                case ProjectElementType.Backsplash:
                                    rotationAxis = new Vector3D(Math.Cos(Math.PI - element.OriginalProjectAngle), Math.Sin(Math.PI - element.OriginalProjectAngle), 0);
                                    break;

                                case ProjectElementType.Rib:
                                    rotationAxis = new Vector3D(Math.Cos(element.OriginalProjectAngle), Math.Sin(element.OriginalProjectAngle), 0);
                                    break;
                            }
                            switch (element.ElementType)
                            {
                                case ProjectElementType.Rib:
                                    //mesh.Rotate(-element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);
                                    mesh.Rotate(-Math.PI / 2, rotationAxis, rotationCenter);
                                    break;

                                case ProjectElementType.Backsplash:
                                    //mesh.Rotate(-element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);
                                    mesh.Rotate(Math.PI / 2, rotationAxis, rotationCenter);

                                    break;
                            }
                        }

                        // Cut mesh to invert normals and then merge the two parts
                        Point3D minQuote = mesh.BoxMin;
                        minQuote.Z += 0.01;
                        Plane cutPlane = Plane.XY;
                        cutPlane.Translate(0, 0, minQuote.Z);
                        Mesh[] meshParts = null;

                        //mesh.UpdateNormals();

                        var result = mesh.SplitBy(cutPlane, out meshParts);
                        if (result == booleanFailureType.Success && meshParts.Length == 2)
                        {
                            meshParts[0].UpdateNormals();
                            meshParts[1].UpdateNormals();

                            if (!element.IsFlipped)
                            {
                                meshParts[0].FlipNormal();
                                element.IsFlipped = true;
                            }

                            newMesh = meshParts[0];
                            newMesh.MergeWith(meshParts[1]);
                            newMesh.EntityData = mesh.EntityData;
                            newMesh.XData = new List<KeyValuePair<short, object>>();
                            newMesh.XData.Add(new KeyValuePair<short, object>(0, element));
                            newMesh.MaterialName = mesh.MaterialName;
                        }

                        //vppRender.Entities.Regen();
                        //vppRender.Invalidate();
                        //Application.DoEvents();

                        if (newMesh != null)
                        {
                            newMesh.ApplyMaterial(newMesh.MaterialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                        }


                        //if (newMesh != null)
                        //{
                        //    newMesh.ApplyMaterial(mesh.MaterialName, textureMappingType.Cubic, 1, 1, newMesh.BoxMin, newMesh.BoxMax);
                        //}


                        if (transform && newMesh != null) // reset position
                        {
                            switch (element.ElementType)
                            {
                                case ProjectElementType.Rib:
                                    newMesh.Rotate(Math.PI / 2, rotationAxis, rotationCenter);
                                    //mesh.Rotate(element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);

                                    break;

                                case ProjectElementType.Backsplash:
                                    newMesh.Rotate(-Math.PI / 2, rotationAxis, rotationCenter);
                                    //mesh.Rotate(element.OriginalProjectAngle, Vector3D.AxisZ, rotationCenter);

                                    break;
                            }

                            newMesh.Regen(CHORDAL_ERROR);

                            vppRender.Entities.Remove(mesh);
                            vppRender.Entities.Add(newMesh);
                        }

                    }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("UpdateMeshNormals Error. ", ex);
            }

            return newMesh;
        }


        private Image BuildSquareBitmap_ORI(Image bitmap)
        {
            if (bitmap == null)
                return null;

            int size = (bitmap.Width > bitmap.Height) ? bitmap.Width : bitmap.Height;

            Bitmap newImage = new Bitmap(size, size);

            using (Graphics g = Graphics.FromImage(newImage))
            {
                g.Clear(Color.DarkBlue);
                int destY = (newImage.Height - bitmap.Height) /2;
                int destX = (newImage.Width - bitmap.Width) /2;
                g.DrawImage(bitmap, destX, destY, bitmap.Width, bitmap.Height);
            }

            //newImage.Save(("E:\\image.jpg"));
            return newImage;
        }

        private Image BuildSquareBitmap(Image bitmap)
        {
            if (bitmap == null)
                return null;

            //return bitmap;

            //bitmap.Save(("E:\\image.jpg"));


            int size = (bitmap.Width > bitmap.Height) ? bitmap.Width : bitmap.Height;

            size = Convert.ToInt32(size * TEXTURE_SIZE_RATIO);


            //Bitmap newImage = new Bitmap(TEXTURE_SIZE, TEXTURE_SIZE);

            //using (Graphics g = Graphics.FromImage(newImage))
            //{
            //    //g.Clear(Color.DarkBlue);
            //    int destY = 0;
            //    int destX = 0;
            //    g.DrawImage(bitmap, destX, destY, TEXTURE_SIZE, TEXTURE_SIZE);
            //}

            Bitmap newImage = new Bitmap(size, size);

            using (Graphics g = Graphics.FromImage(newImage))
            {
                //g.Clear(Color.DarkBlue);
                int destY = 0;
                int destX = 0;
                g.DrawImage(bitmap, destX, destY, size, size);
            }

            //newImage.Save(("E:\\image1.jpg"));
            return newImage;
        }


        #endregion




        #region Funzioni matematiche
        private Line GetLineaOriginale(Shape shape, Shape.Contour contVGR)
        {

            try
            {

                if (shape.gContours.Count > 0)
                {

                    #region Ciclo per ogni contorno delle sagome.

                    double distX0 = 1E+38;
                    double distX1 = 1E+38;
                    double distMin = 1E+38;

                    Line l = null;

                    for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                    {
                        //ricerco tra i contour quello parallelo pi� vicino a quello del VGR
                        Shape.Contour contour = (Shape.Contour)shape.gContours[intI];
                        if (contour.line && contour.pricipalStruct)
                        {

                            double xRet = 1E+38;
                            double yRet = 1E+38;

                            FindLineIntersection(Math.Round(contour.x0, 2), Math.Round(contour.y0, 2), Math.Round(contour.x1, 2), Math.Round(contour.y1, 2),
                                                 Math.Round(contVGR.x0, 2), Math.Round(contVGR.y0, 2), Math.Round(contVGR.x1, 2), Math.Round(contVGR.y1, 2),
                                                 ref xRet, ref yRet);

                            //la linea � parallela. memorizzo la pi� vicina
                            if (Math.Abs(xRet) > 1E+5 || Math.Abs(yRet) > 1E+5)
                            {

                                distX0 = GetDistance2Point(contVGR.x0, contVGR.y0, contour.x0, contour.y0);

                                distX1 = GetDistance2Point(contVGR.x0, contVGR.y0, contour.x1, contour.y1);

                                if (distX0 < distX1 && distX0 < distMin)
                                {
                                    distMin = distX0;
                                    ////l = CreateEyeLine(Math.Round(contour.x0, 2), Math.Round(contour.y0, 2), Math.Round(contour.x1, 2), Math.Round(contour.y1, 2), Color.Red);
                                    l = CreateEyeLine(Math.Round(contour.x0, 2), Math.Round(contour.y0, 2), Math.Round(contour.x1, 2), Math.Round(contour.y1, 2), Color.Red);
                                }
                                else if (distX1 < distX0 && distX1 < distMin)
                                {
                                    distMin = distX1;
                                    ////l = CreateEyeLine(Math.Round(contour.x1, 2), Math.Round(contour.y1, 2), Math.Round(contour.x0, 2), Math.Round(contour.y0, 2), Color.Red);
                                    l = CreateEyeLine(Math.Round(contour.x1, 2), Math.Round(contour.y1, 2), Math.Round(contour.x0, 2), Math.Round(contour.y0, 2), Color.Red);
                                }


                            }
                        
                        }

                    }

                    #endregion

                    return l;

                }
                else
                    return null;

            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("GetLineaOriginale Error. ", ex);
                return null;
            }

        }

        /// <summary>
        /// Procedura di calcolo dell'angolo di una retta sul piano.
        /// </summary>
        /// <param name="x0">Start X del segmento.</param>
        /// <param name="y0">Start Y del segmento.</param>
        /// <param name="x1">End X del segmento.</param>
        /// <param name="y1">End Y del segmento.</param>
        private double GetAngolo(double x0, double y0, double x1, double y1)
        {

            try
            {

                if (x0 == x1)
                {

                    if (y0 < y1)
                    {

                        return 90;

                    }

                    else
                    {

                        return 270;

                    }

                }

                else if (y0 == y1)
                {

                    if (x0 < x1)
                    {

                        return 0;

                    }

                    else
                    {

                        return 180;

                    }

                }

                else
                {

                    if (y0 < y1)
                    {

                        if (x0 < x1)
                        {

                            return 90 - Utility.RadToDeg(System.Math.Atan(System.Math.Abs(x1 - x0) / System.Math.Abs(y1 - y0)));

                        }

                        else
                        {

                            return 180 - Utility.RadToDeg(System.Math.Atan(System.Math.Abs(y1 - y0) / System.Math.Abs(x1 - x0)));

                        }

                    }

                    else
                    {

                        if (x0 < x1)
                        {

                            return 360 - Utility.RadToDeg(System.Math.Atan(System.Math.Abs(y1 - y0) / System.Math.Abs(x1 - x0)));

                        }

                        else
                        {

                            return 270 - Utility.RadToDeg(System.Math.Atan(System.Math.Abs(x1 - x0) / System.Math.Abs(y1 - y0)));

                        }

                    }

                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("GetAngolo Error. ", ex);

                return 0;
            }

        }

        /// <summary>
        /// Procedura di calcolo del punto di intersezione tra due linee.
        /// </summary>
        /// <param name="x11">Start X del 1� segmento.</param>
        /// <param name="y11">Start Y del 1� segmento.</param>
        /// <param name="x12">End X del 1� segmento.</param>
        /// <param name="y12">End Y del 1� segmento.</param>
        /// <param name="x21">Start X del 2� segmento.</param>
        /// <param name="y21">Start Y del 2� segmento.</param>
        /// <param name="x22">End X del 2� segmento.</param>
        /// <param name="y22">End Y del 2� segmento.</param>
        /// <param name="xRet">X dell'intersezione.</param>
        /// <param name="yRet">Y dell'intersezione.</param>
        private void FindLineIntersection(double x11, double y11, double x12, double y12, double x21, double y21, double x22, double y22, ref double xRet, ref double yRet)
        {

	        double dx1 = 0;
	        double dy1 = 0;
	        double dx2 = 0;
	        double dy2 = 0;
	        double t1 = 0;
	        double t2 = 0;
	        double denominator = 0;

	        try
	        {

		        //Get the segments' parameters.
		        dx1 = x12 - x11;
		        dy1 = y12 - y11;
		        dx2 = x22 - x21;
		        dy2 = y22 - y21;

		        //Solve for t1 and t2.
		        denominator = (dy1 * dx2 - dx1 * dy2);
		        if(denominator == 0.0)
		        {

                    xRet = 1E+38;
                    yRet = 1E+38;
			        return;

		        }

		        t1 = ((x11 - x21) * dy2 + (y21 - y11) * dx2) / denominator;

		        t2 = ((x21 - x11) * dy1 + (y11 - y21) * dx1) / -denominator;

                xRet = Math.Round(x11 + dx1 * t1, 3);
                yRet = Math.Round(y11 + dy1 * t1, 3);

                ////    Find the closest points on the segments.
                //if (t1 < 0)
                //{
                //    t1 = 0;

                //}
                //else if (t1 > 1)
                //{

                //    t1 = 1;

                //}

                //if (t2 < 0)
                //{

                //    t2 = 0;

                //}
                //else if (t2 > 1)
                //{

                //    t2 = 1;

                //}

                ////le coordinate del segmento di intersezione vengono confrontate con il punto in comune
                //double inter_x1 = Math.Round(x11 + dx1 * t1, 3);
                //double inter_y1 = Math.Round(y11 + dy1 * t1, 3);
                //double inter_x2 = Math.Round(x21 + dx2 * t2, 3);
                //double inter_y2 = Math.Round(y21 + dy2 * t2, 3);

        //        if ((coorConfrontoX == inter_x1 && coorConfrontoY == inter_y1) ||
        //           (coorConfrontoX == inter_x2 && coorConfrontoY == inter_y2))
        //        {

        //            xRet = 1E+38;
        //            yRet = 1E+38;
        //            return;

        //        }
        //        else
        //        {

        //            //Find the point of intersection
        //            return;

        //        }

	        }
	        catch(Exception ex)
	        {

                TraceLog.WriteLine("FindLineIntersection Error. ", ex);
                xRet = 1E+38;
                yRet = 1E+38;
		        return;

	        }

        }

        /// <summary>
        /// Procedura di calcolo della linea parallela.
        /// </summary>
        /// <param name="x0Par">Start X del segmento parallelo.</param>
        /// <param name="y0Par">Start Y del segmento parallelo.</param>
        /// <param name="x1Par">End X del segmento parallelo.</param>
        /// <param name="y1Par">End Y del segmento parallelo.</param>
        /// <param name="x0">Start X del segmento.</param>
        /// <param name="y0">Start Y del segmento.</param>
        /// <param name="x1">End X del segmento.</param>
        /// <param name="y1">End Y del segmento.</param>
        /// <param name="dist">Distanza tra le linee.</param>
        private void FindParallelLine(ref double x0Par, ref double y0Par, ref double x1Par, ref double y1Par, double x0, double y0, double x1, double y1, double dist)
        {

            try
            {

                double dx = x1 - x0;
                double dy = y1 - y0;

                double perp_x = dx;
                double perp_y = -dy;

                double len = Math.Sqrt(perp_x * perp_x + perp_y * perp_y);

                perp_x /= len;
                perp_y /= len;

                perp_x *= dist;
                perp_y *= dist;

                if (Math.Round(dx,8) == 0)
                {

                    x0Par = x0 + perp_y;
                    y0Par = y0 + perp_x;
                    x1Par = x1 + perp_y;
                    y1Par = y1 + perp_x;

                }

                else if (Math.Round(dy,8) == 0)
                {

                    x0Par = x0 + perp_y;
                    y0Par = y0 + perp_x;
                    x1Par = x1 + perp_y;
                    y1Par = y1 + perp_x;

                }

                else
                {

                    x0Par = x0 + perp_x;
                    y0Par = y0 + perp_y;
                    x1Par = x1 + perp_x;
                    y1Par = y1 + perp_y;

                }

            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("FindParallelLine Error. ", ex);
            }
        }

        private double GetDistance2Point(double x0, double y0, double x1, double y1)
        {
            
            return Math.Sqrt(Math.Abs(x0 - x1) * Math.Abs(x0 - x1) + Math.Abs(y0 - y1) * Math.Abs(y0 - y1));

        }
        #endregion


        #region Sagoma con angolo 0
        /// <summary>
        /// Procedura per ruotare la sagoma con angolo 0.
        /// </summary>
        /// <param name="shape">sagoma.</param>
        private void SetShapeInOrigine(Shape shape)
        {

            try
            {

                if (shape.gContours.Count > 0)
                {

                    Transformation transformation = new Transformation();

                    ////transformation.RotateZ(-shape.gAngle);
                    transformation.Rotation(Utility.DegToRad(-shape.gAngle), Vector3D.AxisZ);

                    #region Ciclo per ogni contorno delle sagome.

                    for (int intI = 0; intI <= shape.gContours.Count - 1; intI++)
                    {

                        Shape.Contour contour = (Shape.Contour)shape.gContours[intI];

                        if (contour.line)
                        {

                            Point3D puntoX0 = new Point3D(contour.x0, contour.y0);
                            Point3D puntoX1 = new Point3D(contour.x1, contour.y1);

                            ////transformation.Apply(ref puntoX0);
                            ////transformation.Apply(ref puntoX1);
                            puntoX0 = transformation * puntoX0;
                            puntoX1 = transformation * puntoX1;

                            contour.x0 = puntoX0.X;
                            contour.y0 = puntoX0.Y;
                            contour.x1 = puntoX1.X;
                            contour.y1 = puntoX1.Y;

                        }

                        else
                        {

                            Point3D puntoX0 = new Point3D(contour.x0, contour.y0);
                            Point3D puntoX1 = new Point3D(contour.x1, contour.y1);
                            Point3D puntoXC = new Point3D(contour.xC, contour.yC);

                            ////transformation.Apply(ref puntoX0);
                            ////transformation.Apply(ref puntoX1);
                            ////transformation.Apply(ref puntoXC);
                            puntoX0 = transformation*puntoX0;
                            puntoX1 = transformation * puntoX1;
                            puntoXC = transformation * puntoXC;


                            contour.x0 = puntoX0.X;
                            contour.y0 = puntoX0.Y;
                            contour.x1 = puntoX1.X;
                            contour.y1 = puntoX1.Y;
                            contour.xC = puntoXC.X;
                            contour.yC = puntoXC.Y;

                        }

                    }

                    #endregion

                }

            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SetShapeInOrigine Error. ", ex);
            }
        }
        #endregion

        internal void ClearProfiles()
        {
            if (dxfProfiles != null)
            {
                dxfProfiles.Clear();
            }
        }
    }
}