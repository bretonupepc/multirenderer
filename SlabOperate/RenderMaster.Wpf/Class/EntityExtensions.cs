﻿using System.Collections.Generic;
using devDept.Eyeshot.Entities;

namespace RenderMaster.Wpf.Class
{
    public static class EntityExtensions
    {
        public static Entity FullClone(this Entity entity)
        {
            Entity cloned = (Entity) entity.Clone();
            if (entity.XData != null)
            {
                cloned.XData = new List<KeyValuePair<short, object>>();
                cloned.XData.AddRange(entity.XData);
            }

            return cloned;
        }
    }
}
