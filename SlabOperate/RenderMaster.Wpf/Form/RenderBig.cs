using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Collections;

using Breton.DesignUtil;
using Breton.CutImages;

namespace RenderMaster.Wpf.Form
{
    public partial class RenderBig : System.Windows.Forms.Form
    {
        #region Variables

        private static bool formCreate = false;
		private Shape[] mShapes;

		public struct ToBigRender
		{
			public int idSag;
			public Bitmap imgSag;

		    public string imgSagPath;

            /// <summary>
            /// Costruttore con parametri
            /// </summary>
            /// <param name="id">idSag</param>
            /// <param name="sag">imgSag</param>
		    public ToBigRender(int id, Bitmap sag)
		    {
		        this.idSag = id;
		        this.imgSag = sag;
                this.imgSagPath = string.Empty;
		    }

            /// <summary>
            /// Costruttore con path immagine
            /// </summary>
            /// <param name="id">Id sagoma</param>
            /// <param name="sagPath">Image path sagoma</param>
            public ToBigRender(int id, string sagPath)
            {
                this.idSag = id;
                this.imgSagPath = sagPath;
                this.imgSag = null;
            }

		}

		private List<ToBigRender> mLstRender;

		private CutImageThread.CutChangeHandler ccHandler;
        #endregion

        public RenderBig(Shape[] shapes, List<ToBigRender> lstRender, string language)
        {
            InitializeComponent();

			mShapes = shapes;

            //mLstRender = lstRender;
            mLstRender = new List<ToBigRender>();
            for (int i = 0; i < lstRender.Count; i++)
            {
                if (lstRender[i].idSag > 0)
                {
                    ToBigRender tbg = new ToBigRender();
                    tbg.idSag = lstRender[i].idSag;
					if (lstRender[i].imgSag != null)
					{
						Bitmap b = new Bitmap(lstRender[i].imgSag);
						tbg.imgSag = b;
						mLstRender.Add(tbg);
					}
                }

            }
			ccHandler = new CutImageThread.CutChangeHandler(ChangeImage);
			CutImageThread.CutChange += ccHandler;

            renderMaster.Lingua = language;
        }

        #region Properties

        public static bool gCreate
        {
            set { formCreate = value; }
            get { return formCreate; }
        }

        #endregion

		private void RenderBig_Load(object sender, EventArgs e)
		{
			////disegno modello 3D
			//Bitmap[] fileImg = new Bitmap[mShapes.Length];

			////carico l'immagine di default
			//for (int ibmp = 0; ibmp < fileImg.Length; ibmp++)
			//    fileImg[ibmp] = new Bitmap("default.jpg");

			//for (int ibmp = 0; ibmp < mLstRender.Count; ibmp++)
			//{
			//    for (int jbmp = 0; jbmp < mShapes.Length; jbmp++)
			//    {
			//        if (mShapes[jbmp].gId == mLstRender[ibmp].idSag)
			//            fileImg[jbmp] = mLstRender[ibmp].imgSag;
			//    }
			//}

			////disegno modello 3D
			//renderMaster.DrawTop(mShapes, fileImg, Application.StartupPath + "\\Profili\\");


			LoadRender();

			gCreate = true;
		}

        private void RenderBig_FormClosing(object sender, FormClosingEventArgs e)
        {
			CutImageThread.CutChange -= ccHandler;
			gCreate = false;
        }

        #region private methods
		private delegate void Invoke_Void_Param(object sender, int n, Bitmap img);

        private void ChangeImage(object sender, int numSag, Bitmap ritaglio)
        {
			if (renderMaster.InvokeRequired)
			{
				Invoke_Void_Param ci = new Invoke_Void_Param(ChangeImage);
				this.Invoke(ci, new Object[] { sender, numSag, ritaglio });
			}
			else 
				renderMaster.ChangeMaterial(numSag, ritaglio);
        }

		private void LoadRender()
		{
			ReloadRender(null);
		}

        #endregion

		#region Public Methods

		public void ReloadRender(Shape[] shapes)
		{
			if (shapes != null)
				mShapes = shapes;

			//disegno modello 3D
			Bitmap[] fileImg = new Bitmap[mShapes.Length];

			//carico l'immagine di default
			for (int ibmp = 0; ibmp < fileImg.Length; ibmp++)
				fileImg[ibmp] = new Bitmap("default.jpg");

			for (int ibmp = 0; ibmp < mLstRender.Count; ibmp++)
			{
				for (int jbmp = 0; jbmp < mShapes.Length; jbmp++)
				{
					if (mShapes[jbmp].gId == mLstRender[ibmp].idSag)
						fileImg[jbmp] = mLstRender[ibmp].imgSag;
				}
			}

			//disegno modello 3D
			renderMaster.DrawTop(mShapes, fileImg, Application.StartupPath + "\\Profili\\");
		}
		#endregion
	}
}