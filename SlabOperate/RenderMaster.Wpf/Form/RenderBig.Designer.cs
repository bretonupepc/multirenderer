namespace RenderMaster.Wpf.Form
{
    partial class RenderBig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.renderMaster = new Breton.RenderMaster.RenderMaster();
			this.SuspendLayout();
			// 
			// renderMaster
			// 
			this.renderMaster.BackgroundBottomColor = System.Drawing.Color.Silver;
			this.renderMaster.BackgroundTopColor = System.Drawing.Color.Black;
			this.renderMaster.Dock = System.Windows.Forms.DockStyle.Fill;
			this.renderMaster.Lingua = "ITA";
			this.renderMaster.Location = new System.Drawing.Point(0, 0);
			this.renderMaster.Margin = new System.Windows.Forms.Padding(2);
			this.renderMaster.Name = "renderMaster";
			this.renderMaster.ProfonditaBussole = 4D;
			this.renderMaster.ProfonditaCanaletti = 6D;
			this.renderMaster.ProfonditaRibasso = 15D;
			this.renderMaster.ProfonditaTasca = 15D;
			this.renderMaster.ShowOrigin = false;
			this.renderMaster.ShowUcsIcon = true;
			this.renderMaster.Size = new System.Drawing.Size(681, 531);
			this.renderMaster.TabIndex = 0;
			// 
			// RenderBig
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(681, 531);
			this.Controls.Add(this.renderMaster);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "RenderBig";
			this.Text = "RenderBig";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RenderBig_FormClosing);
			this.Load += new System.EventHandler(this.RenderBig_Load);
			this.ResumeLayout(false);

        }

        #endregion

		private RenderMaster renderMaster;
    }
}