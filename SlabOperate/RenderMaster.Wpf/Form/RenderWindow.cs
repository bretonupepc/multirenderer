using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;

using Breton.DesignUtil;
using Breton.CutImages;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Graphics;
using TraceLoggers;
using Wpf.Common;

namespace RenderMaster.Wpf.Form
{
    public partial class RenderWindow : System.Windows.Forms.Form
    {
        #region Variables

        private static bool formCreate = false;

        private Render _sourceRender = null;

        private ViewportLayout  _sourceLayout = null;

        private ViewportLayout  _targetLayout = null;

        private static RenderWindow _instance = null;

        private bool _canCloseInstance = false;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="language"></param>
        /// <param name="sourceRender"></param>
        private RenderWindow(string language, Render sourceRender, ViewportLayout sourceLayout)
        {
            InitializeComponent();

            Init(language, sourceRender, sourceLayout);
        }

        private RenderWindow()
        {
            InitializeComponent();

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (!CanCloseInstance)
            {
                e.Cancel = true;
                Hide();
            }
            else
            {
                DetachRenderEvents();
                gCreate = false;
                base.OnClosing(e);
                _instance = null;
            }
        }

        public void Init(string language, Render sourceRender, ViewportLayout sourceLayout)
        {
            RenderWindowUc.Lingua = language;

            _sourceLayout = sourceLayout;
            SourceRender = sourceRender;

            _targetLayout = this.RenderWindowUc._viewportLayout;
        }


        private void UpdateMaterial(Material material)
        {
            RunOnUIThread(delegate
            {
                string materialName = material.Description;
                var original = _targetLayout.Materials.FirstOrDefault(m => m.Key == materialName).Value;

                if (original != null)
                {
                    _targetLayout.Materials.Remove(materialName);
                    _targetLayout.Materials.Add(materialName, (Material)material.Clone());

                    var entitiesToRefresh = _targetLayout.Entities.Where(e => (e is Mesh) &&  e.MaterialName == materialName);
                    foreach (Entity entity in entitiesToRefresh)
                    {
                        Mesh mesh = entity as Mesh;
                        if (mesh != null)
                        {
                            
                        mesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                        }
                    }

                    _targetLayout.Entities.Regen();
                    _targetLayout.Invalidate();

                    original.Dispose();
                }

            });
        }

        private void UpdateMaterial(Material material, Mesh originalMesh)
        {
            RunOnUIThread(delegate
            {
                string materialName = material.Description; 
                var original = _targetLayout.Materials.FirstOrDefault(m => m.Key == materialName).Value;

                if (original != null)
                {
                    _targetLayout.Materials.Remove(materialName);
                    _targetLayout.Materials.Add(materialName, (Material)material.Clone());

                    if (originalMesh != null)
                    {
                        var localMesh =
                            _targetLayout.Entities.FirstOrDefault(
                                e => (e is Mesh) && e.EntityData == originalMesh.EntityData);
                        if (localMesh != null)
                        {
                            _targetLayout.Entities.Remove(localMesh);
                            localMesh.Dispose();

                            Mesh newMesh = (Mesh) originalMesh.FullClone();
                            _targetLayout.Entities.Add(newMesh);




                        }
                    }

                    else
                    {
                        var entitiesToRefresh = _targetLayout.Entities.Where(e => (e is Mesh) && e.MaterialName == materialName);
                        foreach (Entity entity in entitiesToRefresh)
                        {
                            Mesh mesh = entity as Mesh;
                            if (mesh != null)
                            {

                                mesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                            }
                        }
                    }

                    _targetLayout.Entities.Regen();
                    _targetLayout.Invalidate();

                    original.Dispose();
                }

            });
        }


        private void UpdateAllContent()
        {
            if (_sourceLayout != null)
            {
                RunOnUIThread(delegate
                {
                    try
                    {
                        _targetLayout.Clear();

                        //foreach (var layer in _sourceLayout.Layers)
                        //{
                        //    _targetLayout.Layers.Add((Layer) layer.Clone());
                        //}

                        _targetLayout.Layers.AddRange(
                            _sourceLayout.Layers.Where(l => l.Name != "Default").Select(l => (Layer) l.Clone()).ToList());
                        _targetLayout.UpdateViewportLayout();

                        foreach (var material in _sourceLayout.Materials)
                        {
                            try
                            {
                                var newMaterial = (Material) material.Value.Clone();

                                _targetLayout.Materials.Add(material.Key, newMaterial);
                            }
                            catch (Exception ex)
                            {
                                TraceLog.WriteLine(this.Name + " UpdateAllContent", ex);
                            }
                        }

                        _targetLayout.UpdateViewportLayout();

                        _targetLayout.Entities.AddRange(_sourceLayout.Entities.Select(e => (Entity) e.FullClone()).ToList());
                        _targetLayout.UpdateViewportLayout();

                    }
                    catch (Exception ex)
                    {
                        TraceLog.WriteLine(this.Name + " UpdateAllContent", ex);
                    }
                    finally
                    {
                        _targetLayout.Entities.Regen();

                        _targetLayout.ZoomFit();
                        _targetLayout.Invalidate();

                    }
                });
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CreateContent()
        {
            if (SourceRender != null)
            {
                var dummy = _targetLayout.Handle;
                _targetLayout.Viewports[0].Camera.ProjectionMode = projectionType.Orthographic;
                _targetLayout.SetView(viewType.Isometric);

                UpdateAllContent();
            }

        }

        public void ResetLayout()
        {
            RunOnUIThread(() =>
            {
                foreach (var ent in _targetLayout.Entities)
                {
                    ent.MaterialName = string.Empty;
                }
                _targetLayout.Entities.ClearSelection();
                _targetLayout.Entities.Clear();

                while (_targetLayout.Materials.Count > 0)
                {
                    var element = _targetLayout.Materials.ElementAt(0);
                    _targetLayout.Materials.Remove(element.Key);
                    element.Value.Dispose();
                }
                _targetLayout.Layers.Clear(new Layer("Default"));

            });
        }

        #region Properties

        public static bool gCreate
        {
            set { formCreate = value; }
            get { return formCreate; }
        }

        public Render SourceRender
        {
            get { return _sourceRender; }
            set
            {
                bool hasChanged = (value != _sourceRender);

                if (hasChanged)
                {
                    DetachRenderEvents();
                }
                _sourceRender = value;
                if (hasChanged)
                {
                    AttachRenderEvents();
                }

            }
        }

        public static RenderWindow Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RenderWindow();
                    gCreate = true;
                }
                return _instance;
            }
        }

        public bool CanCloseInstance
        {
            get { return _canCloseInstance; }
            set { _canCloseInstance = value; }
        }

        private void AttachRenderEvents()
        {
            if (_sourceRender != null)
            {
                _sourceRender.MaterialChanged += _sourceRender_MaterialChanged;
                _sourceRender.TopRenderCompleted += SourceRenderTopRenderCompleted;
            }
        }

        void SourceRenderTopRenderCompleted(object sender, EventArgs e)
        {
            UpdateAllContent();
        }

        private void DetachRenderEvents()
        {
            if (_sourceRender != null)
            {
                _sourceRender.MaterialChanged -= _sourceRender_MaterialChanged;
                _sourceRender.TopRenderCompleted -= SourceRenderTopRenderCompleted;
            }
        }


        void _sourceRender_MaterialChanged_ORI(object sender, Common.Events.EventArgs<object> e)
        {
            Material material = e.Value as Material;
            if (material != null)
            {
                UpdateMaterial_ORI(material);
            }
        }

        void _sourceRender_MaterialChanged(object sender, Common.Events.EventArgs<object> e)
        {
            List<object> items = e.Value as List<object>;
            if (items != null && items.Count > 1)
            {
                Material material = items[0] as Material;
                Mesh mesh = items[1] as Mesh;
                if (material != null)
                {
                    UpdateMaterial(material, mesh);
                }
            }
        }

        private void RunOnUIThread(MethodInvoker code)
        {
            if (IsDisposed)
            {
                return;
            }

            if (InvokeRequired)
            {
                Invoke(code);
                return;
            }

            code.Invoke();
        }


        #endregion


        #region private methods



        #endregion

		#region Public Methods

		#endregion
	}
}