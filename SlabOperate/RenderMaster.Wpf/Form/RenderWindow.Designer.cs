namespace RenderMaster.Wpf.Form
{
    partial class RenderWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.RenderWindowUc = new RenderWindowUc();
			this.SuspendLayout();
			// 
			// RenderWindowUc
			// 
			this.RenderWindowUc.BackgroundBottomColor = System.Drawing.Color.Silver;
			this.RenderWindowUc.BackgroundTopColor = System.Drawing.Color.Black;
			this.RenderWindowUc.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RenderWindowUc.Lingua = "ITA";
			this.RenderWindowUc.Location = new System.Drawing.Point(0, 0);
			this.RenderWindowUc.Margin = new System.Windows.Forms.Padding(2);
			this.RenderWindowUc.Name = "RenderWindowUc";
			this.RenderWindowUc.ShowOrigin = false;
			this.RenderWindowUc.ShowUcsIcon = true;
			this.RenderWindowUc.Size = new System.Drawing.Size(681, 531);
			this.RenderWindowUc.TabIndex = 0;
			// 
			// RenderWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(681, 531);
			this.Controls.Add(this.RenderWindowUc);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "RenderWindow";
			this.Text = "Render Window";
			this.ResumeLayout(false);

        }

        #endregion

		private RenderWindowUc RenderWindowUc;
    }
}