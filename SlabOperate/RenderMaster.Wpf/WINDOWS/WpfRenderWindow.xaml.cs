﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Graphics;
using RenderMaster.Wpf.Class;
using TraceLoggers;
using Wpf.Common;

namespace RenderMaster.Wpf.WINDOWS
{
    /// <summary>
    /// Interaction logic for RenderWindow.xaml
    /// </summary>
    public partial class WpfRenderWindow : Window
    {
        private static bool formCreate = false;

        private Render _sourceRender = null;

        private ViewportLayout _sourceLayout = null;

        private ViewportLayout _targetLayout = null;

        private bool _canCloseInstance = false;

        private static  WpfRenderWindow _instance = null;

        private WpfRenderWindow()
        {
            InitializeComponent();
            Closing += WpfRenderWindow_Closing;
        }

        void WpfRenderWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!CanCloseInstance)
            {
                e.Cancel = true;
                Hide();
            }
            else
            {
                DetachRenderEvents();
                gCreate = false;
                //base.OnClosing(e);
                _instance = null;
            }
        }

        public static bool gCreate
        {
            set { formCreate = value; }
            get { return formCreate; }
        }


        public void Init(string language, Render sourceRender, ViewportLayout sourceLayout)
        {
            //RenderWindowUc.Lingua = language;

            _sourceLayout = sourceLayout;
            SourceRender = sourceRender;

            _targetLayout = this.RenderWindowUc.vppRender1;
        }

        private void AttachRenderEvents()
        {
            if (_sourceRender != null)
            {
                _sourceRender.MaterialChanged += _sourceRender_MaterialChanged;
                _sourceRender.TopRenderCompleted += SourceRenderTopRenderCompleted;
            }
        }
        private void DetachRenderEvents()
        {
            if (_sourceRender != null)
            {
                _sourceRender.MaterialChanged -= _sourceRender_MaterialChanged;
                _sourceRender.TopRenderCompleted -= SourceRenderTopRenderCompleted;
            }
        }

        void SourceRenderTopRenderCompleted(object sender, EventArgs e)
        {
            UpdateAllContent();
        }

        void _sourceRender_MaterialChanged(object sender, EventArgs<object> e)
        {
            List<object> items = e.Value as List<object>;
            if (items != null && items.Count > 1)
            {
                Material material = items[0] as Material;
                Mesh mesh = items[1] as Mesh;
                if (material != null)
                {
                    UpdateMaterial(material, mesh);
                }
            }
        }



        private void UpdateMaterial_ORI(Material material)
        {
            RunOnUiThread(()=>
            {
                string materialName = material.Description;
                var original = _targetLayout.Materials.FirstOrDefault(m => m.Key == materialName).Value;

                if (original != null)
                {
                    _targetLayout.Materials.Remove(materialName);
                    _targetLayout.Materials.Add(materialName, (Material)material.Clone());

                    var entitiesToRefresh = _targetLayout.Entities.Where(e => (e is Mesh) && e.MaterialName == materialName);
                    foreach (Entity entity in entitiesToRefresh)
                    {
                        Mesh mesh = entity as Mesh;
                        mesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                    }

                    _targetLayout.Entities.Regen();
                    _targetLayout.Invalidate();

                    original.Dispose();
                }

            });
        }

        private void UpdateMaterial(Material material, Mesh originalMesh)
        {
            RunOnUiThread(()=>
            {
                string materialName = material.Description;
                var original = _targetLayout.Materials.FirstOrDefault(m => m.Key == materialName).Value;


                if (original != null)
                {
                    Mesh localMesh = null;

                    if (originalMesh != null)
                    {
                        localMesh = (Mesh)
                            _targetLayout.Entities.FirstOrDefault(
                                e => (e is Mesh) && e.EntityData == originalMesh.EntityData);
                        if (localMesh != null)
                        {
                            localMesh.RemoveMaterial();
                        }
                    }


                    _targetLayout.Materials.Remove(materialName);
                    _targetLayout.Materials.Add(materialName, (Material)material.Clone());

                    if (originalMesh != null)
                    {
                        if (localMesh != null)
                        {
                            _targetLayout.Entities.Remove(localMesh);
                            localMesh.Dispose();

                            Mesh newMesh = (Mesh)originalMesh.FullClone();
                            _targetLayout.Entities.Add(newMesh);
                        }
                    }

                    else
                    {
                        var entitiesToRefresh = _targetLayout.Entities.Where(e => (e is Mesh) && e.MaterialName == materialName);
                        foreach (Entity entity in entitiesToRefresh)
                        {
                            Mesh mesh = entity as Mesh;
                            if (mesh != null)
                            {
                                mesh.ApplyMaterial(materialName, textureMappingType.Cubic, 1, 1, mesh.BoxMin, mesh.BoxMax);
                            }
                        }
                    }

                    _targetLayout.Entities.Regen();
                    _targetLayout.Invalidate();

                    original.Dispose();
                }

            });
        }


        public void CreateContent()
        {
            if (SourceRender != null)
            {
                var dummy = _targetLayout.windowHandle;
                _targetLayout.Viewports[0].Camera.ProjectionMode = projectionType.Orthographic;
                _targetLayout.SetView(viewType.Isometric);

                UpdateAllContent();
            }

        }



        public void ResetLayout()
        {
            RunOnUiThread(() =>
            {
                foreach (var ent in _targetLayout.Entities)
                {
                    ent.MaterialName = string.Empty;
                }
                _targetLayout.Entities.ClearSelection();
                _targetLayout.Entities.Clear();

                while (_targetLayout.Materials.Count > 0)
                {
                    var element = _targetLayout.Materials.ElementAt(0);
                    _targetLayout.Materials.Remove(element.Key);
                    element.Value.Dispose();
                }
                _targetLayout.Layers.Clear(new Layer("Default"));

            });
        }

        private void UpdateAllContent()
        {
            if (_sourceLayout != null)
            {
                RunOnUiThread(()=>
                {
                    try
                    {
                        _targetLayout.Clear();

                        //foreach (var layer in _sourceLayout.Layers)
                        //{
                        //    _targetLayout.Layers.Add((Layer) layer.Clone());
                        //}

                        _targetLayout.Layers.AddRange(
                            _sourceLayout.Layers.Where(l => l.Name != "Default").Select(l => (Layer)l.Clone()).ToList());
                        _targetLayout.UpdateViewportLayout();

                        foreach (var material in _sourceLayout.Materials)
                        {
                            try
                            {
                                var newMaterial = (Material)material.Value.Clone();

                                _targetLayout.Materials.Add(material.Key, newMaterial);
                            }
                            catch (Exception ex)
                            {
                                TraceLog.WriteLine(this.Name + " UpdateAllContent", ex);
                            }
                        }

                        _targetLayout.UpdateViewportLayout();

                        _targetLayout.Entities.AddRange(_sourceLayout.Entities.Select(e => (Entity)e.Clone()).ToList());
                        _targetLayout.UpdateViewportLayout();

                    }
                    catch (Exception ex)
                    {
                        TraceLog.WriteLine(this.Name + " UpdateAllContent", ex);
                    }
                    finally
                    {
                        _targetLayout.Entities.Regen();

                        _targetLayout.ZoomFit();
                        _targetLayout.Invalidate();

                    }
                });

            }
        }


        private void RunOnUiThread(Action action)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                Dispatcher.InvokeAsync(action);
            }
            else
            {
                action.Invoke();
            }
        }





        public Render SourceRender
        {
            get { return _sourceRender; }
            set
            {
                DetachRenderEvents();
                _sourceRender = value;
                AttachRenderEvents();

            }
        }


        public static WpfRenderWindow Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WpfRenderWindow();
                    gCreate = true;
                }
                return _instance;
            }
        }

        public bool CanCloseInstance
        {
            get { return _canCloseInstance; }
            set { _canCloseInstance = value; }
        }
    }
}
