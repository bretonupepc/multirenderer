﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Breton.DesignUtil;
using devDept.Eyeshot;
using devDept.Graphics;
using RenderMaster.Wpf.BUSINESS;
using RenderMaster.Wpf.Class;
using RenderMaster.Wpf.USERCONTROLS;
using RenderMaster.Wpf.WINDOWS;
using TraceLoggers;
using Wpf.Common;
using Wpf.Common.Commands;
using Brush = System.Windows.Media.Brush;
using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;

namespace RenderMaster.Wpf.VIEWMODELS
{
    public class RenderMasterViewModel: ObservableObject
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        #region Variables

        private System.Drawing.Color _backgroundTopColor;
        private System.Drawing.Color _backgroundBottomColor;
        private bool _showOrigin = false;
        private bool _showUcsIcon = true;

        private bool _renderInProgress = false;

        private string _progressMessage = string.Empty;

        private int _progressTotalSteps = 10;

        private int _progressCurrentStep = 0;

        private object _lockObj = new object();

        private RenderMasterUc _view;


        private DelegateCommand _openRenderWindowCommand = null;

        private Window _parentWindow = null;

        private WpfRenderWindow _renderWindow = null;

        //private List<ToBigRender> _lstToBigRender = new List<ToBigRender>();

        //private string _defaultMaterialImageFilepath = string.Empty;

        //private Breton.DesignUtil.Shape[] s;

        private Render _render;


        private double profonditaTasca = 15;                        //profonditaTasca deve avere un valore maggiore o uguale delle altre profondità
        private double profonditaRibasso = 15;                       //profonditaRibasso deve avere un valore maggiore delle altre profondità
        //perchè il ribasso è un profilo esterno con tutti i fori
        //tranne gli SNP0008
        private double profonditaCanaletti = 6;
        private double profonditaBussole = 4;
        private string lingua = "ITA";


        private bool _lightOn1 = true;

        private bool _lightOn2 = true;

        private bool _lightOn3 = true;

        private bool _lightOn4 = true;

        private bool _showEdges = false;

        private bool _viewModeRendered = true;

        private bool _viewModeWireframe = false;

        private bool _showNormals = false;


        #endregion

        #region >-------------- Private Fields - Commands


        #endregion Private Fields - Commands --------<
        #endregion Private Fields --------<

        #region >-------------- Constructors

        public RenderMasterViewModel()
        {
            View = new RenderMasterUc();

            View.DataContext = this;

            _render = new Render();

            InitView();
        }

        private void InitView()
        {
            View.vppRender1.Viewports[0].InitialView = viewType.Top;
            ShowEdges = true;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        /// <summary>
        /// Lingua attiva.
        /// </summary>
        public string Lingua
        {
            get { return lingua; }
            set
            {
                lingua = value;
                //CaricaInterfacciaMultiLingua();
            }
        }

        /// <summary>
        /// Profondità della tasca.
        /// </summary>
        public double ProfonditaTasca
        {
            get { return profonditaTasca; }
            set
            {
                if (value >= profonditaRibasso)
                    profonditaTasca = value;
            }
        }

        /// <summary>
        /// Profondità del ribasso. Il valore deve essere maggiore delle altre profondità perchè il ribasso è un profilo esterno con tutti i fori.
        /// </summary>
        public double ProfonditaRibasso
        {
            get { return profonditaRibasso; }
            set
            {
                if (value > profonditaCanaletti && value > profonditaBussole && value < profonditaTasca)
                    profonditaRibasso = value;
            }
        }

        /// <summary>
        /// Profondità del canaletto. Il valore deve essere minore della profondità del ribasso.
        /// </summary>
        public double ProfonditaCanaletti
        {
            get { return profonditaCanaletti; }
            set
            {
                if (value < profonditaRibasso)
                    profonditaCanaletti = value;
            }
        }

        /// <summary>
        /// Procedura della bussola. Il valore deve essere minore della profondità del ribasso.
        /// </summary>
        public double ProfonditaBussole
        {
            get { return profonditaBussole; }
            set
            {
                if (value < profonditaRibasso)
                    profonditaBussole = value;
            }
        }

        #region >-------------- Public Properties - Commands



        #endregion Public Properties - Commands --------<
        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        #region Disegno sagome
        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="shapes">Vettore di sagome, contiene le sagome da disegnare.</param>
        /// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="azzeraArea">Indica se l'area deve essere azzerata prima di disegnare.</param>
        public void DrawTop(Shape[] shapes, Bitmap[] pathImage, string pathProfiliLav, bool azzeraArea)
        {

            if (azzeraArea) View.vppRender1.Entities.Clear();

            ShowOrigin = false;

            //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
            _render.DrawTop(shapes, pathImage, pathProfiliLav, View.vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca);

            View.vppRender1.ZoomFit();

        }

        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="shapes">Vettore di sagome, contiene le sagome da disegnare.</param>
        /// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        public void DrawTop(Shape[] shapes, Bitmap[] pathImage, string pathProfiliLav)
        {

            View.vppRender1.Entities.Clear();
            View.vppRender1.Materials.Clear();

            ShowOrigin = false;

            //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
            _render.DrawTop(shapes, pathImage, pathProfiliLav, View.vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca);

            View.vppRender1.ZoomFit();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="shapes"></param>
        /// <param name="pathImage"></param>
        /// <param name="pathProfiliLav"></param>
        public void DrawTopOnFiles(Shape[] shapes, string[] pathImage, string pathProfiliLav, Dictionary<int, RenderedElement> elements)
        {

            View.vppRender1.Entities.Clear();
            View.vppRender1.Materials.Clear();

            View.vppRender1.Viewports[0].OriginSymbol.Visible = false;

            //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
            _render.DrawTopOnFiles(shapes, pathImage, pathProfiliLav, View.vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, elements);


            //vppRender1.ZoomFit();
            //vppRender1.Invalidate();
        }




        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente non ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="pathXML">Vettore di percorsi, contiene i percorsi dei files XML da elaborare.</param>
        /// <param name="pathImage">ArrayList di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="azzeraArea">Indica se l'area deve essere azzerata prima di disegnare.</param>
        public void DrawTop(string[] pathXML, Bitmap[] pathImage, string pathProfiliLav, bool azzeraArea)
        {

            try
            {

                Shape[] shapes = new Shape[pathXML.Length];

                for (int i = 0; i < pathXML.Length; i++)
                {

                    Shape tmp = new Shape();
                    XmlAnalizer.ReadShapeXml(pathXML[i], ref tmp, true);
                    shapes[i] = tmp;

                }

                DrawTop(shapes, pathImage, pathProfiliLav, azzeraArea);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawTop Error. ", ex);
            }

        }

        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente non ha già eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="pathXML">Vettore di percorsi, contiene i percorsi dei files XML da elaborare.</param>
        /// <param name="pathImage">ArrayList di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        public void DrawTop(string[] pathXML, Bitmap[] pathImage, string pathProfiliLav)
        {

            try
            {

                Shape[] shapes = new Shape[pathXML.Length];

                for (int i = 0; i < pathXML.Length; i++)
                {

                    Shape tmp = new Shape();
                    XmlAnalizer.ReadShapeXml(pathXML[i], ref tmp, true);
                    shapes[i] = tmp;

                }

                DrawTop(shapes, pathImage, pathProfiliLav);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawTop Error. ", ex);
            }

        }

        /// <summary>
        /// Procedura di modifica del materiale di una sagoma.
        /// </summary>
        /// <param name="shapeNumber">Vettore di interi, contiene gli indici delle Sagome.</param>
        /// <param name="pathImage">Vettore di Bitmap, contiene le singole immagini.</param>
        public void ChangeMaterial(int[] shapeNumber, Bitmap[] pathImage)
        {
            RunOnUiThread(() =>
            {
                try
                {

                    _render.ChangeMaterial(shapeNumber, pathImage, View.vppRender1);

                }
                catch (Exception ex)
                {
                    TraceLog.WriteLine("ChangMaterial Error. ", ex);
                }
            });
            //if (!Dispatcher.CurrentDispatcher.CheckAccess())
            //{
            //    Dispatcher.CurrentDispatcher.Invoke(() =>
            //    {
            //        try
            //        {

            //            _render.ChangeMaterial(shapeNumber, pathImage, View.vppRender1);

            //        }
            //        catch (Exception ex)
            //        {
            //            TraceLog.WriteLine("ChangMaterial Error. ", ex);
            //        }
            //    });
            //}
            //else
            //{
            //    try
            //    {

            //        _render.ChangeMaterial(shapeNumber, pathImage, View.vppRender1);

            //    }
            //    catch (Exception ex)
            //    {
            //        TraceLog.WriteLine("ChangMaterial Error. ", ex);
            //    }
            //}


        }

        /// <summary>
        /// Procedura di modifica del materiale di una sagoma.
        /// </summary>
        /// <param name="shapeNumber">id sagoma.</param>
        /// <param name="pathImage">immagine</param>
        public void ChangeMaterial(int shapeNumber, Bitmap pathImage)
        {

            try
            {
                RunOnUiThread(() => _render.ChangeMaterial(shapeNumber, pathImage, View.vppRender1));

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ChangeMaterial Error. ", ex);
            }

        }



        #endregion

        public void InitProgressBar(int maxValue)
        {
            ProgressCurrentStep = 0;
            ProgressTotalSteps = maxValue;
            RenderInProgress = true;
            ProgressMessage = string.Empty;

        }

        /// <summary>
        /// Incrementa +1 il valore progress bar
        /// </summary>
        public void IncrementProgressBarValue()
        {
            RunOnUiThread(() =>
            {
                lock (_lockObj)
                {
                    if (ProgressTotalSteps > ProgressCurrentStep)
                    {
                        ProgressCurrentStep++;
                        ProgressMessage = string.Format("Creating texture for shape # {0}", ProgressCurrentStep);
                    }
                }
            });
        }

        /// <summary>
        /// Aggiorna valore progress bar
        /// </summary>
        /// <param name="value">Valore</param>
        public void UpdateProgressBarValue(int value)
        {
            RunOnUiThread(() =>
            {
                if (ProgressTotalSteps >= value)
                {
                    ProgressCurrentStep = value;
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caption"></param>
        public void UpdateProgressBarCaption(string caption)
        {
            RunOnUiThread(() =>
            {
                ProgressMessage = caption;
            });
        }

        public void InvalidateAndFit()
        {
            View.vppRender1.Invalidate();
            //View.vppRender1.ZoomFit();
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void AttachParentWindowEvents()
        {
            if (_parentWindow != null)
            {
                _parentWindow.Closing += _mainParent_Closing;
            }
        }

        private void DetachParentWindowEvents()
        {
            if (_parentWindow != null)
            {
                _parentWindow.Closing -= _mainParent_Closing;
            }
        }

        private void OpenRenderWindow(object obj)
        {
            try
            {
                if (!WpfRenderWindow.gCreate)
                {
                    _renderWindow = WpfRenderWindow.Instance;

                    var rmvm = new RenderMasterWindowViewModel()
                    {
                        View = _renderWindow.RenderWindowUc,
                        ParentWindow = ParentWindow,
                        LightOn1 = LightOn1,
                        LightOn2 = LightOn2,
                        LightOn3 = LightOn3,
                        LightOn4 = LightOn4,
                        ViewModeRendered = ViewModeRendered,
                        ViewModeWireframe = ViewModeWireframe,
                        ShowNormals = ShowNormals,
                        ShowEdges = ShowEdges,
                    };

                    _renderWindow.DataContext = rmvm;

                    _renderWindow.Init(Lingua, _render, _view.vppRender1);

                    if (ParentWindow != null)
                    {
                        _renderWindow.Owner = ParentWindow;
                    }
                    _renderWindow.Show();

                    _renderWindow.CreateContent();
                }
                else
                {
                    if (_renderWindow != null)
                    {
                        if (_renderWindow.IsVisible)
                            _renderWindow.Activate();
                        else
                            _renderWindow.Show();
                    }
                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("OnRenderDoubleClick Error. ", ex);
            }
        }        

        private void AttachRenderEvents()
        {
            if (_render != null)
            {
                _render.TopRenderStarted += new EventHandler<EventArgs<int>>(render_TopRenderStarted);
                _render.TopRenderProgress += new EventHandler<EventArgs<int>>(render_TopRenderProgress);
                _render.TopRenderCompleted += new EventHandler<EventArgs>(render_TopRenderCompleted);
            }
        }

        private void DetachRenderEvents()
        {
            if (_render != null)
            {
                _render.TopRenderStarted += new EventHandler<EventArgs<int>>(render_TopRenderStarted);
                _render.TopRenderProgress += new EventHandler<EventArgs<int>>(render_TopRenderProgress);
                _render.TopRenderCompleted += new EventHandler<EventArgs>(render_TopRenderCompleted);
            }
        }

        private void RunOnUiThread(Action action)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.InvokeAsync(action);
            }
            else
            {
                action.Invoke();
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates

        void render_TopRenderStarted(object sender, EventArgs<int> e)
        {
            RunOnUiThread(() => InitProgressBar(e.Value));
        }


        void render_TopRenderCompleted(object sender, EventArgs e)
        {
            RunOnUiThread(() =>
            {
                RenderInProgress = false;
            });
        }

        void render_TopRenderProgress(object sender, EventArgs<int> e)
        {
            RunOnUiThread(() =>
            {
                ProgressCurrentStep = e.Value;
            });
        }

        void _mainParent_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_renderWindow != null)
            {
                //TODO
                _renderWindow.CanCloseInstance = true;

                _renderWindow.Close();

                _renderWindow = null;
            }
        }

        #endregion Delegates  -----------<

        #region Proprietà



            /// <summary>
            /// Procedura del colore di sfondo inferiore dell'area di disegno.
            /// </summary>
            public Brush BackgroundBottomColor
            {
                get
                {
                    return RenderContextUtility.ConvertColor(_backgroundBottomColor);
                }
                set
                {
                    _backgroundBottomColor = RenderContextUtility.ConvertColor(value);
                    OnPropertyChanged("BackgroundBottomColor");
                }
            }

            /// <summary>
            /// Procedura del colore di sfondo superiore dell'area di disegno.
            /// </summary>
            public Brush BackgroundTopColor
            {
                get
                {
                    return RenderContextUtility.ConvertColor(_backgroundTopColor);
                }
                set
                {
                    _backgroundTopColor = RenderContextUtility.ConvertColor(value);
                    OnPropertyChanged("BackgroundTopColor");
                }
            }

            /// <summary>
            /// Mostrare l'origine S/N.
            /// </summary>
            public bool ShowOrigin
            {
                get { return _showOrigin; }
                set
                {
                    _showOrigin = value;
                    OnPropertyChanged("ShowOrigin");
                }
            }

            /// <summary>
            /// Mostrare l'UCS S/N.
            /// </summary>
            public bool ShowUcsIcon
            {
                get { return _showUcsIcon; }
                set
                {
                    _showUcsIcon = value;
                    OnPropertyChanged("ShowUcsIcon");
                }
            }

            public Window ParentWindow
            {
                get { return _parentWindow; }
                set
                {
                    DetachParentWindowEvents();
                    _parentWindow = value;
                    AttachParentWindowEvents();

                }
            }

        public bool RenderInProgress
        {
            get { return _renderInProgress; }
            set
            {
                _renderInProgress = value;
                OnPropertyChanged("RenderInProgress");
            }
        }

        public string ProgressMessage
        {
            get { return _progressMessage; }
            set
            {
                _progressMessage = value;
                OnPropertyChanged("ProgressMessage");
            }
        }

        public int ProgressTotalSteps
        {
            get { return _progressTotalSteps; }
            set
            {
                _progressTotalSteps = value;
                OnPropertyChanged("ProgressTotalSteps");
            }
        }

        public int ProgressCurrentStep
        {
            get { return _progressCurrentStep; }
            set
            {
                _progressCurrentStep = value;
                OnPropertyChanged("ProgressCurrentStep");
            }
        }

        public Render Render
        {
            get { return _render; }
            set
            {
                DetachRenderEvents();
                _render = value;
                AttachRenderEvents();
            }
        }

        public RenderMasterUc View
        {
            get { return _view; }
            set
            {
                _view = value;
                OnPropertyChanged("View");
            }
        }

        public DelegateCommand OpenRenderWindowCommand
        {
            get
            {
                if (_openRenderWindowCommand == null)
                    _openRenderWindowCommand = new DelegateCommand(OpenRenderWindow);
                return _openRenderWindowCommand;
            }
        }

        public bool LightOn1
        {
            get { return _lightOn1; }
            set
            {
                _lightOn1 = value;
                OnPropertyChanged("LightOn1");
                SetLight(1, _lightOn1);
            }
        }

        private void SetLight(int lightNumber, bool onOff)
        {
            if (lightNumber < 1 || lightNumber > 4) return;

            LightSettings settings = null;

            switch (lightNumber)
            {
                case 1:
                    settings = View.vppRender1.Light1;
                    break;

                case 2:
                    settings = View.vppRender1.Light2;
                    break;

                case 3:
                    settings = View.vppRender1.Light3;
                    break;

                case 4:
                    settings = View.vppRender1.Light4;
                    break;
            }

            if (settings != null)
            {
                if (settings.Active != onOff)
                {
                    settings.Active = onOff;
                    View.vppRender1.Invalidate();
                }
            }
        }

        public bool LightOn2
        {
            get { return _lightOn2; }
            set
            {
                _lightOn2 = value;
                OnPropertyChanged("LightOn2");
                SetLight(2, value);
            }
        }

        public bool LightOn3
        {
            get
            {
                return _lightOn3;

            }
            set 
            {
                _lightOn3 = value;
                OnPropertyChanged("LightOn3");
                SetLight(3, value);
            }
        }

        public bool LightOn4
        {
            get { return _lightOn4; }
            set
            {
                _lightOn4 = value;
                OnPropertyChanged("LightOn4");
                SetLight(4, value);
            }
        }

        public bool ShowEdges
        {
            get { return _showEdges; }
            set
            {
                _showEdges = value;
                OnPropertyChanged("ShowEdges");
                View.vppRender1.Rendered.ShowEdges = _showEdges;
                View.vppRender1.Invalidate();

            }
        }

        public bool ViewModeRendered
        {
            get { return _viewModeRendered; }
            set
            {
                _viewModeRendered = value;
                OnPropertyChanged("ViewModeRendered");
                _viewModeWireframe = !_viewModeRendered;
                OnPropertyChanged("ViewModeWireframe");
                View.vppRender1.Viewports[0].DisplayMode = _viewModeRendered
                    ? displayType.Rendered
                    : displayType.Wireframe;
                View.vppRender1.Invalidate();

            }
        }

        public bool ViewModeWireframe
        {
            get { return _viewModeWireframe; }
            set
            {
                _viewModeWireframe = value;
                OnPropertyChanged("ViewModeWireframe");
                _viewModeRendered = !_viewModeWireframe;
                OnPropertyChanged("ViewModeRendered");
                View.vppRender1.Viewports[0].DisplayMode = _viewModeRendered
                    ? displayType.Rendered
                    : displayType.Wireframe;
                View.vppRender1.Invalidate();

            }
        }

        public bool ShowNormals
        {
            get { return _showNormals; }
            set
            {
                _showNormals = value;
                View.vppRender1.ShowNormals = _showNormals;
                View.vppRender1.Invalidate();
                OnPropertyChanged("ShowNormals");
            }
        }


        //public List<ToBigRender> LstToBigRender
        //{
        //    get { return _lstToBigRender; }
        //}

        //public string DefaultMaterialImageFilepath
        //{
        //    get { return _defaultMaterialImageFilepath; }
        //    set
        //    {
        //        _defaultMaterialImageFilepath = value; 
                
        //    }
        //}

        #endregion
    }
}
