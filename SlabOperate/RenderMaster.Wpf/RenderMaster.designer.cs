﻿using devDept.Eyeshot;
using devDept.Graphics;

namespace RenderMaster.Wpf
{
    partial class RenderMaster
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione componenti

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare 
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RenderMaster));
            devDept.Graphics.LightSettings lightSettings1 = new devDept.Graphics.LightSettings(new devDept.Geometry.Vector3D(0.5D, 0.5D, -0.5D), System.Drawing.Color.Gainsboro, System.Drawing.Color.White, true, true, false, devDept.Graphics.lightType.Directional, new devDept.Geometry.Point3D(0D, 0D, 0D), 0.78539816339744828D, 0D, 1D, 0D, 0D);
            devDept.Eyeshot.DisplayModeSettingsRendered displayModeSettingsRendered1 = new devDept.Eyeshot.DisplayModeSettingsRendered(false, devDept.Eyeshot.edgeColorMethodType.EntityColor, System.Drawing.Color.Black, 1F, 2F, devDept.Eyeshot.silhouettesDrawingType.LastFrame, false, devDept.Graphics.shadowType.Planar, null, false, true, 0.2F, devDept.Graphics.realisticShadowQualityType.High);
            devDept.Graphics.BackgroundSettings backgroundSettings1 = new devDept.Graphics.BackgroundSettings(devDept.Graphics.backgroundStyleType.LinearGradient, System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(245)))), ((int)(((byte)(245))))), System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(163)))), ((int)(((byte)(210))))), 0.75D, null);
            devDept.Eyeshot.Camera camera1 = new devDept.Eyeshot.Camera(new devDept.Geometry.Point3D(0D, 0D, 49.999999999999972D), 600D, new devDept.Geometry.Quaternion(0.086824088833465166D, 0.15038373318043533D, 0.492403876506104D, 0.85286853195244339D), devDept.Graphics.projectionType.Orthographic, 50D, 4.3200002210559072D, false);
            devDept.Eyeshot.ZoomWindowToolBarButton zoomWindowToolBarButton1 = new devDept.Eyeshot.ZoomWindowToolBarButton("Zoom Window", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.ZoomToolBarButton zoomToolBarButton1 = new devDept.Eyeshot.ZoomToolBarButton("Zoom", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.PanToolBarButton panToolBarButton1 = new devDept.Eyeshot.PanToolBarButton("Pan", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.RotateToolBarButton rotateToolBarButton1 = new devDept.Eyeshot.RotateToolBarButton("Rotate", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.ZoomFitToolBarButton zoomFitToolBarButton1 = new devDept.Eyeshot.ZoomFitToolBarButton("Zoom Fit", devDept.Eyeshot.toolBarButtonStyleType.PushButton, true, true);
            devDept.Eyeshot.ToolBar toolBar1 = new devDept.Eyeshot.ToolBar(devDept.Eyeshot.toolBarPositionType.HorizontalTopCenter, false, new devDept.Eyeshot.ToolBarButton[] {
            ((devDept.Eyeshot.ToolBarButton)(zoomWindowToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(zoomToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(panToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(rotateToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(zoomFitToolBarButton1))});
            devDept.Eyeshot.Grid grid1 = new devDept.Eyeshot.Grid(new devDept.Geometry.Point3D(-100D, -100D, 0D), new devDept.Geometry.Point3D(100D, 100D, 0D), 5D, new devDept.Geometry.Plane(new devDept.Geometry.Point3D(0D, 0D, 0D), new devDept.Geometry.Vector3D(0D, 0D, 1D)), System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))), System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))), System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))), false, false, false, false, 10, 100, 5, System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90))))), System.Drawing.Color.Transparent);
            devDept.Eyeshot.OriginSymbol originSymbol1 = new devDept.Eyeshot.OriginSymbol(10, devDept.Eyeshot.originSymbolStyleType.Ball, new System.Drawing.Font("Tahoma", 8.25F), System.Drawing.Color.Black, System.Drawing.Color.Red, System.Drawing.Color.Green, System.Drawing.Color.Blue, "Origin", "X", "Y", "Z", true);
            devDept.Eyeshot.RotateSettings rotateSettings1 = new devDept.Eyeshot.RotateSettings(new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Middle, devDept.Eyeshot.modifierKeys.None), 10D, true, 1D, devDept.Eyeshot.rotationType.Trackball, devDept.Eyeshot.rotationCenterType.CursorLocation, new devDept.Geometry.Point3D(0D, 0D, 0D));
            devDept.Eyeshot.ZoomSettings zoomSettings1 = new devDept.Eyeshot.ZoomSettings(new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Middle, devDept.Eyeshot.modifierKeys.Shift), 25, true, devDept.Eyeshot.zoomStyleType.AtCursorLocation, false, 1D, System.Drawing.Color.DeepSkyBlue, devDept.Eyeshot.Camera.perspectiveFitType.Accurate, false, 10);
            devDept.Eyeshot.PanSettings panSettings1 = new devDept.Eyeshot.PanSettings(new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Middle, devDept.Eyeshot.modifierKeys.Ctrl), 25, true);
            devDept.Eyeshot.NavigationSettings navigationSettings1 = new devDept.Eyeshot.NavigationSettings(devDept.Eyeshot.Camera.navigationType.Examine, new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Left, devDept.Eyeshot.modifierKeys.None), new devDept.Geometry.Point3D(-1000D, -1000D, -1000D), new devDept.Geometry.Point3D(1000D, 1000D, 1000D), 8D, 50D, 50D);
            devDept.Eyeshot.CoordinateSystemIcon coordinateSystemIcon1 = new devDept.Eyeshot.CoordinateSystemIcon(new System.Drawing.Font("Tahoma", 8.25F), System.Drawing.Color.Black, System.Drawing.Color.Red, System.Drawing.Color.Green, System.Drawing.Color.MediumBlue, "Origin", "X", "Y", "Z", true, devDept.Eyeshot.coordinateSystemPositionType.BottomLeft, 37);
            devDept.Eyeshot.ViewCubeIcon viewCubeIcon1 = new devDept.Eyeshot.ViewCubeIcon(devDept.Eyeshot.coordinateSystemPositionType.TopRight, true, System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(69)))), ((int)(((byte)(0))))), true, 300, "FRONT", "BACK", "LEFT", "RIGHT", "TOP", "BOTTOM", System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), 'S', 'N', 'W', 'E', true, null, System.Drawing.Color.White, System.Drawing.Color.Black, 120, true, true, null, null, null, null, null, null);
            devDept.Eyeshot.Viewport.SavedViewsManager savedViewsManager1 = new devDept.Eyeshot.Viewport.SavedViewsManager(8);
            devDept.Eyeshot.Viewport viewport1 = new devDept.Eyeshot.Viewport(new System.Drawing.Point(0, 0), new System.Drawing.Size(711, 432), backgroundSettings1, camera1, toolBar1, new devDept.Eyeshot.Legend[0], devDept.Eyeshot.displayType.Rendered, true, false, false, false, new devDept.Eyeshot.Grid[] {
            grid1}, originSymbol1, false, rotateSettings1, zoomSettings1, panSettings1, navigationSettings1, coordinateSystemIcon1, viewCubeIcon1, savedViewsManager1, devDept.Eyeshot.viewType.Trimetric);
            this.tlsMain = new System.Windows.Forms.ToolStrip();
            this.cmdZoomTutto = new System.Windows.Forms.ToolStripButton();
            this.cmdZoomFinestra = new System.Windows.Forms.ToolStripButton();
            this.cmdZoomIn = new System.Windows.Forms.ToolStripButton();
            this.cmdZoomOut = new System.Windows.Forms.ToolStripButton();
            this.cmdZoomPan = new System.Windows.Forms.ToolStripButton();
            this.cmdVistaIsometrica = new System.Windows.Forms.ToolStripButton();
            this.cmdVistaAlto = new System.Windows.Forms.ToolStripButton();
            this.spl1 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdPrintPreview = new System.Windows.Forms.ToolStripButton();
            this.cmdPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsChkLight1 = new System.Windows.Forms.ToolStripButton();
            this.tsChkLight2 = new System.Windows.Forms.ToolStripButton();
            this.tsChkLight3 = new System.Windows.Forms.ToolStripButton();
            this.tsChkLight4 = new System.Windows.Forms.ToolStripButton();
            this.cmdVistaFrontale = new System.Windows.Forms.ToolStripButton();
            this.cmdVistaLaterale = new System.Windows.Forms.ToolStripButton();
            this.cmdSalvaVista = new System.Windows.Forms.ToolStripButton();
            this.cmdRipristinaVista = new System.Windows.Forms.ToolStripButton();
            this.stsMain = new System.Windows.Forms.StatusStrip();
            this.prbMain = new System.Windows.Forms.ToolStripProgressBar();
            this.lblProgressCaption = new System.Windows.Forms.ToolStripStatusLabel();
            this.vppRender1 = new devDept.Eyeshot.ViewportLayout();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnShowNormals = new System.Windows.Forms.Button();
            this.tlsMain.SuspendLayout();
            this.stsMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vppRender1)).BeginInit();
            this.SuspendLayout();
            // 
            // tlsMain
            // 
            this.tlsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdZoomTutto,
            this.cmdZoomFinestra,
            this.cmdZoomIn,
            this.cmdZoomOut,
            this.cmdZoomPan,
            this.cmdVistaIsometrica,
            this.cmdVistaAlto,
            this.spl1,
            this.cmdPrintPreview,
            this.cmdPrint,
            this.toolStripSeparator1,
            this.tsChkLight1,
            this.tsChkLight2,
            this.tsChkLight3,
            this.tsChkLight4,
            this.cmdVistaFrontale,
            this.cmdVistaLaterale,
            this.cmdSalvaVista,
            this.cmdRipristinaVista});
            this.tlsMain.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.tlsMain.Location = new System.Drawing.Point(0, 0);
            this.tlsMain.Name = "tlsMain";
            this.tlsMain.Size = new System.Drawing.Size(711, 23);
            this.tlsMain.TabIndex = 0;
            // 
            // cmdZoomTutto
            // 
            this.cmdZoomTutto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdZoomTutto.Image = ((System.Drawing.Image)(resources.GetObject("cmdZoomTutto.Image")));
            this.cmdZoomTutto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdZoomTutto.Name = "cmdZoomTutto";
            this.cmdZoomTutto.Size = new System.Drawing.Size(23, 20);
            this.cmdZoomTutto.Click += new System.EventHandler(this.cmdZoomTutto_Click);
            // 
            // cmdZoomFinestra
            // 
            this.cmdZoomFinestra.CheckOnClick = true;
            this.cmdZoomFinestra.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdZoomFinestra.Image = ((System.Drawing.Image)(resources.GetObject("cmdZoomFinestra.Image")));
            this.cmdZoomFinestra.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdZoomFinestra.Name = "cmdZoomFinestra";
            this.cmdZoomFinestra.Size = new System.Drawing.Size(23, 20);
            this.cmdZoomFinestra.CheckedChanged += new System.EventHandler(this.cmdZoomFinestra_CheckedChanged);
            // 
            // cmdZoomIn
            // 
            this.cmdZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("cmdZoomIn.Image")));
            this.cmdZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdZoomIn.Name = "cmdZoomIn";
            this.cmdZoomIn.Size = new System.Drawing.Size(23, 20);
            this.cmdZoomIn.Click += new System.EventHandler(this.cmdZoomIn_Click);
            // 
            // cmdZoomOut
            // 
            this.cmdZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("cmdZoomOut.Image")));
            this.cmdZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdZoomOut.Name = "cmdZoomOut";
            this.cmdZoomOut.Size = new System.Drawing.Size(23, 20);
            this.cmdZoomOut.Click += new System.EventHandler(this.cmdZoomOut_Click);
            // 
            // cmdZoomPan
            // 
            this.cmdZoomPan.CheckOnClick = true;
            this.cmdZoomPan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdZoomPan.Image = ((System.Drawing.Image)(resources.GetObject("cmdZoomPan.Image")));
            this.cmdZoomPan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdZoomPan.Name = "cmdZoomPan";
            this.cmdZoomPan.Size = new System.Drawing.Size(23, 20);
            this.cmdZoomPan.CheckedChanged += new System.EventHandler(this.cmdZoomPan_CheckedChanged);
            // 
            // cmdVistaIsometrica
            // 
            this.cmdVistaIsometrica.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdVistaIsometrica.Image = ((System.Drawing.Image)(resources.GetObject("cmdVistaIsometrica.Image")));
            this.cmdVistaIsometrica.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdVistaIsometrica.Name = "cmdVistaIsometrica";
            this.cmdVistaIsometrica.Size = new System.Drawing.Size(23, 20);
            this.cmdVistaIsometrica.Click += new System.EventHandler(this.cmdVistaIsometrica_Click);
            // 
            // cmdVistaAlto
            // 
            this.cmdVistaAlto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdVistaAlto.Image = ((System.Drawing.Image)(resources.GetObject("cmdVistaAlto.Image")));
            this.cmdVistaAlto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdVistaAlto.Name = "cmdVistaAlto";
            this.cmdVistaAlto.Size = new System.Drawing.Size(23, 20);
            this.cmdVistaAlto.Click += new System.EventHandler(this.cmdVistaAlto_Click);
            // 
            // spl1
            // 
            this.spl1.Name = "spl1";
            this.spl1.Size = new System.Drawing.Size(6, 23);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdPrintPreview.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrintPreview.Image")));
            this.cmdPrintPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Size = new System.Drawing.Size(23, 20);
            this.cmdPrintPreview.Text = "toolStripButton1";
            this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdPrint.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrint.Image")));
            this.cmdPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(23, 20);
            this.cmdPrint.Text = "toolStripButton1";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 23);
            // 
            // tsChkLight1
            // 
            this.tsChkLight1.AutoToolTip = false;
            this.tsChkLight1.CheckOnClick = true;
            this.tsChkLight1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsChkLight1.Image = global::RenderMaster.Properties.Resources.BulbOff;
            this.tsChkLight1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsChkLight1.Name = "tsChkLight1";
            this.tsChkLight1.Size = new System.Drawing.Size(23, 20);
            this.tsChkLight1.Visible = false;
            this.tsChkLight1.CheckedChanged += new System.EventHandler(this.tsChkLight_CheckedChanged);
            // 
            // tsChkLight2
            // 
            this.tsChkLight2.AutoToolTip = false;
            this.tsChkLight2.CheckOnClick = true;
            this.tsChkLight2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsChkLight2.Image = global::RenderMaster.Properties.Resources.BulbOff;
            this.tsChkLight2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsChkLight2.Name = "tsChkLight2";
            this.tsChkLight2.Size = new System.Drawing.Size(23, 20);
            this.tsChkLight2.Visible = false;
            this.tsChkLight2.CheckedChanged += new System.EventHandler(this.tsChkLight_CheckedChanged);
            // 
            // tsChkLight3
            // 
            this.tsChkLight3.AutoToolTip = false;
            this.tsChkLight3.CheckOnClick = true;
            this.tsChkLight3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsChkLight3.Image = global::RenderMaster.Properties.Resources.BulbOff;
            this.tsChkLight3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsChkLight3.Name = "tsChkLight3";
            this.tsChkLight3.Size = new System.Drawing.Size(23, 20);
            this.tsChkLight3.Visible = false;
            this.tsChkLight3.CheckedChanged += new System.EventHandler(this.tsChkLight_CheckedChanged);
            // 
            // tsChkLight4
            // 
            this.tsChkLight4.AutoToolTip = false;
            this.tsChkLight4.CheckOnClick = true;
            this.tsChkLight4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsChkLight4.Image = global::RenderMaster.Properties.Resources.BulbOff;
            this.tsChkLight4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsChkLight4.Name = "tsChkLight4";
            this.tsChkLight4.Size = new System.Drawing.Size(23, 20);
            this.tsChkLight4.Visible = false;
            this.tsChkLight4.CheckedChanged += new System.EventHandler(this.tsChkLight_CheckedChanged);
            // 
            // cmdVistaFrontale
            // 
            this.cmdVistaFrontale.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdVistaFrontale.Image = ((System.Drawing.Image)(resources.GetObject("cmdVistaFrontale.Image")));
            this.cmdVistaFrontale.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdVistaFrontale.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdVistaFrontale.Name = "cmdVistaFrontale";
            this.cmdVistaFrontale.Size = new System.Drawing.Size(23, 20);
            this.cmdVistaFrontale.Visible = false;
            this.cmdVistaFrontale.Click += new System.EventHandler(this.cmdVistaFrontale_Click);
            // 
            // cmdVistaLaterale
            // 
            this.cmdVistaLaterale.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdVistaLaterale.Image = ((System.Drawing.Image)(resources.GetObject("cmdVistaLaterale.Image")));
            this.cmdVistaLaterale.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdVistaLaterale.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdVistaLaterale.Name = "cmdVistaLaterale";
            this.cmdVistaLaterale.Size = new System.Drawing.Size(23, 20);
            this.cmdVistaLaterale.Visible = false;
            this.cmdVistaLaterale.Click += new System.EventHandler(this.cmdVistaLaterale_Click);
            // 
            // cmdSalvaVista
            // 
            this.cmdSalvaVista.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdSalvaVista.Image = ((System.Drawing.Image)(resources.GetObject("cmdSalvaVista.Image")));
            this.cmdSalvaVista.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdSalvaVista.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSalvaVista.Name = "cmdSalvaVista";
            this.cmdSalvaVista.Size = new System.Drawing.Size(23, 20);
            this.cmdSalvaVista.Visible = false;
            this.cmdSalvaVista.Click += new System.EventHandler(this.cmdSalvaVista_Click);
            // 
            // cmdRipristinaVista
            // 
            this.cmdRipristinaVista.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdRipristinaVista.Image = ((System.Drawing.Image)(resources.GetObject("cmdRipristinaVista.Image")));
            this.cmdRipristinaVista.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cmdRipristinaVista.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRipristinaVista.Name = "cmdRipristinaVista";
            this.cmdRipristinaVista.Size = new System.Drawing.Size(23, 20);
            this.cmdRipristinaVista.Visible = false;
            this.cmdRipristinaVista.Click += new System.EventHandler(this.cmdRipristinaVista_Click);
            // 
            // stsMain
            // 
            this.stsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prbMain,
            this.lblProgressCaption});
            this.stsMain.Location = new System.Drawing.Point(0, 455);
            this.stsMain.Name = "stsMain";
            this.stsMain.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.stsMain.Size = new System.Drawing.Size(711, 22);
            this.stsMain.TabIndex = 2;
            // 
            // prbMain
            // 
            this.prbMain.Name = "prbMain";
            this.prbMain.Size = new System.Drawing.Size(200, 16);
            this.prbMain.Visible = false;
            // 
            // lblProgressCaption
            // 
            this.lblProgressCaption.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.lblProgressCaption.Name = "lblProgressCaption";
            this.lblProgressCaption.Size = new System.Drawing.Size(95, 17);
            this.lblProgressCaption.Text = "toolStripStatusLabel1";
            // 
            // vppRender1
            // 
            this.vppRender1.AntiAliasing = true;
            this.vppRender1.AskForFsaa = true;
            this.vppRender1.Cursor = System.Windows.Forms.Cursors.Default;
            this.vppRender1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vppRender1.GroundPlaneDistance = 0.5D;
            lightSettings1.LinearAttenuation = 0D;
            this.vppRender1.Light4 = lightSettings1;
            this.vppRender1.Location = new System.Drawing.Point(0, 23);
            this.vppRender1.Name = "vppRender1";
            this.vppRender1.Rendered = displayModeSettingsRendered1;
            this.vppRender1.Size = new System.Drawing.Size(711, 432);
            this.vppRender1.TabIndex = 3;
            this.vppRender1.Text = "viewportLayout1";
            viewport1.CoordinateSystemIcon = coordinateSystemIcon1;
            this.vppRender1.Viewports.Add(viewport1);
            this.vppRender1.DoubleClick += new System.EventHandler(this.vppRender_DoubleClick);
            this.vppRender1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.vppRender_MouseClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(461, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 18);
            this.button1.TabIndex = 4;
            this.button1.Text = "W";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(515, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(48, 18);
            this.button2.TabIndex = 4;
            this.button2.Text = "R";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnShowNormals
            // 
            this.btnShowNormals.Location = new System.Drawing.Point(569, 6);
            this.btnShowNormals.Name = "btnShowNormals";
            this.btnShowNormals.Size = new System.Drawing.Size(48, 18);
            this.btnShowNormals.TabIndex = 4;
            this.btnShowNormals.Text = "N";
            this.btnShowNormals.UseVisualStyleBackColor = true;
            this.btnShowNormals.Visible = false;
            this.btnShowNormals.Click += new System.EventHandler(this.btnShowNormals_Click);
            // 
            // RenderMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnShowNormals);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.vppRender1);
            this.Controls.Add(this.stsMain);
            this.Controls.Add(this.tlsMain);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "RenderMaster";
            this.Size = new System.Drawing.Size(711, 477);
            this.Load += new System.EventHandler(this.RenderMaster_Load);
            this.tlsMain.ResumeLayout(false);
            this.tlsMain.PerformLayout();
            this.stsMain.ResumeLayout(false);
            this.stsMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vppRender1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tlsMain;
        private System.Windows.Forms.ToolStripButton cmdZoomTutto;
        private System.Windows.Forms.ToolStripButton cmdZoomFinestra;
        private System.Windows.Forms.ToolStripButton cmdZoomIn;
        private System.Windows.Forms.ToolStripButton cmdZoomOut;
        private System.Windows.Forms.ToolStripButton cmdZoomPan;
        private System.Windows.Forms.ToolStripSeparator spl1;
        private System.Windows.Forms.ToolStripButton cmdVistaIsometrica;
        private System.Windows.Forms.ToolStripButton cmdVistaFrontale;
        private System.Windows.Forms.ToolStripButton cmdVistaAlto;
        private System.Windows.Forms.ToolStripButton cmdVistaLaterale;
        private System.Windows.Forms.ToolStripButton cmdSalvaVista;
        private System.Windows.Forms.ToolStripButton cmdRipristinaVista;
        private System.Windows.Forms.StatusStrip stsMain;
        private System.Windows.Forms.ToolStripProgressBar prbMain;
        private System.Windows.Forms.ToolStripButton cmdPrint;
        private System.Windows.Forms.ToolStripButton cmdPrintPreview;
        private ViewportLayout vppRender1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnShowNormals;
        private System.Windows.Forms.ToolStripStatusLabel lblProgressCaption;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsChkLight1;
        private System.Windows.Forms.ToolStripButton tsChkLight2;
        private System.Windows.Forms.ToolStripButton tsChkLight3;
        private System.Windows.Forms.ToolStripButton tsChkLight4;
    }
}
