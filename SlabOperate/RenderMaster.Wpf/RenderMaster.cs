using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Breton.DesignUtil;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Graphics;
using RenderMaster.Wpf.Form;
using TraceLoggers;
using Wpf.Common;

namespace RenderMaster.Wpf
{

    /// <summary>
    /// Controllo per la visualizzazione del Render.
    /// </summary>
    public partial class RenderMaster : UserControl
    {
		private const string LICENSE_CODE = "EYEPRO-8126-LUFAW-9ET8A-MH9P2";

        private RenderWindow _bigWindow = null;

        private System.Windows.Forms.Form _mainParent = null;

        private object _locker = new object();

        Camera vista;

        private bool _loading = true;

        public static event EventHandler OpenBigRender;

        /// <summary>
        /// Costruttore della classe.
        /// </summary>
        public RenderMaster()
        {
            InitializeComponent();
            vppRender1.Unlock(LICENSE_CODE);
            vppRender1.Anchor = AnchorStyles.Top | AnchorStyles.Left;


            vppRender1.Viewports[0].Camera.ProjectionMode = projectionType.Orthographic;


            tsChkLight1.Checked = vppRender1.Light1.Active;
            tsChkLight2.Checked = vppRender1.Light2.Active;
            tsChkLight3.Checked = vppRender1.Light3.Active;
            tsChkLight4.Checked = vppRender1.Light4.Active;

            tsChkLight1.Visible = true;
            tsChkLight2.Visible = true;
            tsChkLight3.Visible = true;
            tsChkLight4.Visible = true;



            _loading = false;
        }

        private void RenderMaster_Load(object sender, EventArgs e)
        {

            render = new Render();
            AttachRenderEvents();

            //TODO: DeC
            ////// Carica le lingue del form
            ////ProjResource.gResource.ChangeLanguage(lingua);
            ////ProjResource.gResource.LoadStringControl(this);

            DrawToolStrip();

        }

        private void AttachRenderEvents()
        {
            render.TopRenderStarted += new EventHandler<EventArgs<int>>(render_TopRenderStarted);
            render.TopRenderProgress += new EventHandler<EventArgs<int>>(render_TopRenderProgress);
            render.TopRenderCompleted += new EventHandler<EventArgs>(render_TopRenderCompleted);
        }

        private void DetachRenderEvents()
        {
            if (render != null)
            {
                render.TopRenderStarted += new EventHandler<EventArgs<int>>(render_TopRenderStarted);
                render.TopRenderProgress += new EventHandler<EventArgs<int>>(render_TopRenderProgress);
                render.TopRenderCompleted += new EventHandler<EventArgs>(render_TopRenderCompleted);
            }
        }


        void render_TopRenderCompleted(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                RunOnUIThread(delegate
                {
                    prbMain.Visible = false;
                });
            }
            else
            {
                prbMain.Visible = false;
            }
        }

        void render_TopRenderProgress(object sender, EventArgs<int> e)
        {
            if (this.InvokeRequired)
            {
                RunOnUIThread(delegate
                {
                    UpdateProgressBarValue(e.Value);
                });
            }
            else
            {
                UpdateProgressBarValue(e.Value);
            }
        }

        /// <summary>
        /// Aggiorna valore progress bar
        /// </summary>
        /// <param name="value">Valore</param>
        public void UpdateProgressBarValue(int value)
        {
            RunOnUIThread(() =>
            {
                if (prbMain.Maximum >= value)
                {
                    prbMain.Value = value;
                    stsMain.Refresh();
                    Application.DoEvents();
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="caption"></param>
        public void UpdateProgressBarCaption(string caption)
        {
            RunOnUIThread(() =>
            {
                lblProgressCaption.Text = caption;
                stsMain.Refresh();
                Application.DoEvents();
            });
        }



        /// <summary>
        /// Incrementa +1 il valore progress bar
        /// </summary>
        public void IncrementProgressBarValue()
        {
            if (stsMain.InvokeRequired)
            {
                //Application.DoEvents();
                lock (_locker)
                {
                    MethodInvoker invoker = new MethodInvoker(IncrementProgressBarValue);

                    stsMain.Invoke(invoker);
                    //Application.DoEvents();
                }
                
            }
            else
            {
                if (prbMain.Maximum > prbMain.Value)
                {
                    prbMain.Value++;
                    lblProgressCaption.Text = string.Format("Creating texture for shape # {0}", prbMain.Value);
                    stsMain.Refresh();
                    Application.DoEvents();
                }
            }
        }

        void render_TopRenderStarted(object sender, EventArgs<int> e)
        {
            //if (this.InvokeRequired)
            //{
                RunOnUIThread(() => InitProgressBar(e.Value));
            //}
            //else
            //{
            //    InitProgressBar(e.Value);

            //}
        }



        /// <summary>
        /// Inizializza barra avanzamento render
        /// </summary>
        /// <param name="maxValue">Massimo valore barra</param>
        public void InitProgressBar(int maxValue)
        {
            prbMain.Value = 0;
            prbMain.Maximum = maxValue;
            prbMain.Visible = true;
            lblProgressCaption.Text = string.Empty;
            stsMain.Refresh();
        }

        private void RunOnUIThread(MethodInvoker code)
        {
            if (IsDisposed)
            {
                return;
            }

            if (InvokeRequired)
            {
                Invoke(code);
                return;
            }

            code.Invoke();
        }


        private void cmdZoomTutto_Click(object sender, EventArgs e)
        {

            vppRender1.ZoomFit();
            vppRender1.Refresh();

        }

        private void cmdZoomFinestra_CheckedChanged(object sender, EventArgs e)
        {

            bool bolCheck = cmdZoomFinestra.Checked;
            if(cmdZoomPan.Checked) cmdZoomPan.Checked = false;
            cmdZoomFinestra.Checked = bolCheck;
            if (bolCheck)
            {

                vppRender1.ActionMode = actionType.ZoomWindow;

            }

            else
            {

                vppRender1.ActionMode = actionType.None;

            }

        }

        private void cmdZoomIn_Click(object sender, EventArgs e)
        {

            vppRender1.ZoomIn(25);
            vppRender1.Invalidate();

        }

        private void cmdZoomOut_Click(object sender, EventArgs e)
        {

            vppRender1.ZoomOut(25);
            vppRender1.Invalidate();

        }

        private void cmdZoomPan_CheckedChanged(object sender, EventArgs e)
        {

            bool bolCheck = cmdZoomPan.Checked;
            if (cmdZoomFinestra.Checked) cmdZoomFinestra.Checked = false;
            cmdZoomPan.Checked = bolCheck;

            if (bolCheck)
            {

                vppRender1.ActionMode = actionType.Pan;

            }

            else
            {

                vppRender1.ActionMode = actionType.None;

            }

        }

        // Gestione click destro per uscire dai comandi azione (Zoom Finestra, Pan)
        private void vppRender_MouseClick(object sender, MouseEventArgs e)
        {

            if (e.Button.ToString() == "Right")
            {

                cmdZoomFinestra.Checked = false;
                cmdZoomPan.Checked = false;

                //vppRender1.Action = devDept.Eyeshot.Viewport.actionType.None;

            }

        }

        private void vppRender_DoubleClick(object sender, EventArgs e)
        {
            OnRenderDoubleClick();
        }

        private void cmdVistaIsometrica_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Isometric);
            vppRender1.Refresh();

        }

        private void cmdVistaFrontale_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Front);
            vppRender1.Refresh();

        }

        private void cmdVistaAlto_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Top);
            vppRender1.Refresh();

        }

        private void cmdVistaLaterale_Click(object sender, EventArgs e)
        {

            vppRender1.SetView(viewType.Left);
            vppRender1.Refresh();

        }

        private void cmdSalvaVista_Click(object sender, EventArgs e)
        {
            vppRender1.SaveView(out vista);
        }

        private void cmdRipristinaVista_Click(object sender, EventArgs e)
        {
            vppRender1.RestoreView(vista);
            vppRender1.Invalidate();

        }

        private void cmdPrintPreview_Click(object sender, EventArgs e)
        {
            //vppRender1.PrintPreview(new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height));
            bool landscape = true, blackwhite = false;
            int bottom = 10, top = 10, left = 10, right = 10;
            string paper = "A4";

            PrintPreview(ref landscape, ref bottom, ref top, ref right, ref left, ref blackwhite, ref paper, true, false, false, false, string.Empty, this.Font);
        }

        private void cmdPrint_Click(object sender, EventArgs e)
        {
            //vppRender1.PageSetup();
            //vppRender1.Print();
            bool landscape = true, blackwhite = false;
            int bottom = 10, top = 10, left = 10, right = 10;
            string paper = "A4";
			
            Print(ref landscape, ref bottom, ref top, ref right, ref left, ref blackwhite, ref paper, true, false, false, false, string.Empty, this.Font);
        }

        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalit� grigio</param>
        /// <param name="paper">Formato carta</param>
        /// <param name="rasterPrint">Stampa raster</param>
        /// <param name="showBorder">Disegna bordo di tampa</param>
        /// <param name="showTitle">Visualizza titolo stampa</param>
        /// <param name="showDateTime">Visualizza data e ora</param>
        /// <param name="title">Titolo stampa</param>
        /// <param name="titlesFont">Font titoli stampa</param>
        private void PrintPreview(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper,
            bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont)
        {
            PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left, blackwhite, paper);

            printDocument.Layout = vppRender1;

            printDocument.RasterPrint = rasterPrint;
            printDocument.ShowBorder = showBorder;
            printDocument.ShowTitle = showTitle;
            printDocument.ShowDateTime = showDateTime;
            printDocument.Title = title;

            if (titlesFont != null)
                printDocument.TitlesFont = titlesFont;

            printDocument.ShowPreview();

            // Save printer options
            landscape = printDocument.LandscapeOrientation;
            bottom = printDocument.MarginBottom;
            top = printDocument.MarginTop;
            right = printDocument.MarginRight;
            left = printDocument.MarginLeft;
            blackwhite = printDocument.BlackAndWhitePrint;
            paper = printDocument.PaperFormat;

            printDocument.Layout = null;

            printDocument = null;
        }

        /// <summary>
        /// Apre preview di stampa per il documento corrente
        /// </summary>
        /// <param name="landscape">Orientazione landscape</param>
        /// <param name="bottom">Margine inferiore in mm</param>
        /// <param name="top">Margine superiore in mm</param>
        /// <param name="right">Margine destro in mm</param>
        /// <param name="left">Margine sinistro in mm</param>
        /// <param name="blackwhite">Stampa bianco nero / tonalit� grigio</param>
        /// <param name="paper">Formato carta</param>
        /// <param name="rasterPrint">Stampa raster</param>
        /// <param name="showBorder">Disegna bordo di tampa</param>
        /// <param name="showTitle">Visualizza titolo stampa</param>
        /// <param name="showDateTime">Visualizza data e ora</param>
        /// <param name="title">Titolo stampa</param>
        /// <param name="titlesFont">Font titoli stampa</param>
        private void Print(ref bool landscape,
            ref int bottom,
            ref int top,
            ref int right,
            ref int left,
            ref bool blackwhite,
            ref string paper,
            bool rasterPrint, bool showBorder, bool showTitle, bool showDateTime, string title, Font titlesFont)
        {
            PrintableDocument printDocument = new PrintableDocument(landscape, bottom, top, right, left, blackwhite, paper);

            printDocument.Layout = vppRender1;

            printDocument.RasterPrint = rasterPrint;
            printDocument.ShowBorder = showBorder;
            printDocument.ShowTitle = showTitle;
            printDocument.ShowDateTime = showDateTime;
            printDocument.Title = title;

            if (titlesFont != null)
                printDocument.TitlesFont = titlesFont;

            printDocument.PrintDirect();

            // Save printer options
            landscape = printDocument.LandscapeOrientation;
            bottom = printDocument.MarginBottom;
            top = printDocument.MarginTop;
            right = printDocument.MarginRight;
            left = printDocument.MarginLeft;
            blackwhite = printDocument.BlackAndWhitePrint;
            paper = printDocument.PaperFormat;

            printDocument.Layout = null;

            printDocument = null;
        }



        #region Variables
        private Render render;
        private double profonditaTasca = 15;                        //profonditaTasca deve avere un valore maggiore o uguale delle altre profondit�
        private double profonditaRibasso = 15;                       //profonditaRibasso deve avere un valore maggiore delle altre profondit�
                                                                    //perch� il ribasso � un profilo esterno con tutti i fori
                                                                    //tranne gli SNP0008
        private double profonditaCanaletti = 6;
        private double profonditaBussole = 4;
        private string lingua = "ITA";
        #endregion

        #region Propriet�

        /// <summary>
        /// Lingua attiva.
        /// </summary>
        public string Lingua
        {
            get { return lingua; }
            set {
                  lingua = value;
                  //CaricaInterfacciaMultiLingua();
                }
        }

        /// <summary>
        /// Profondit� della tasca.
        /// </summary>
        public double ProfonditaTasca
        {
            get { return profonditaTasca; }
            set
            {
                if (value >= profonditaRibasso)
                    profonditaTasca = value;
            }
        }

        /// <summary>
        /// Profondit� del ribasso. Il valore deve essere maggiore delle altre profondit� perch� il ribasso � un profilo esterno con tutti i fori.
        /// </summary>
        public double ProfonditaRibasso
        {
            get { return profonditaRibasso; }
            set
            {
                if (value > profonditaCanaletti && value > profonditaBussole && value < profonditaTasca)
                    profonditaRibasso = value;
            }
        }

        /// <summary>
        /// Profondit� del canaletto. Il valore deve essere minore della profondit� del ribasso.
        /// </summary>
        public double ProfonditaCanaletti
        {
            get { return profonditaCanaletti; }
            set
            {
                if (value < profonditaRibasso)
                    profonditaCanaletti = value;
            }
        }

        /// <summary>
        /// Procedura della bussola. Il valore deve essere minore della profondit� del ribasso.
        /// </summary>
        public double ProfonditaBussole
        {
            get { return profonditaBussole; }
            set
            {
                if (value < profonditaRibasso)
                    profonditaBussole = value;
            }
        }

        /// <summary>
        /// Procedura del colore di sfondo inferiore dell'area di disegno.
        /// </summary>
        public Color BackgroundBottomColor
        {
            get { return vppRender1.Background.BottomColor; }
            set { vppRender1.Background.BottomColor = value; }
        }

        /// <summary>
        /// Procedura del colore di sfondo superiore dell'area di disegno.
        /// </summary>
        public Color BackgroundTopColor
        {
            get { return vppRender1.Background.TopColor; }
            set { vppRender1.Background.TopColor = value; }
        }

        /// <summary>
        /// Mostrare l'origine S/N.
        /// </summary>
        public bool ShowOrigin
        {
            get { return vppRender1.OriginSymbol.Visible; }
            set { vppRender1.OriginSymbol.Visible = value; }
        }

        /// <summary>
        /// Mostrare l'UCS S/N.
        /// </summary>
        public bool ShowUcsIcon
        {
            get { return vppRender1.CoordinateSystemIcon.Visible; }
            set { vppRender1.CoordinateSystemIcon.Visible = value; }
        }

        private System.Windows.Forms.Form MainParent
        {
            get { return _mainParent; }
            set
            {
                bool hasChanged = (_mainParent != value);
                if (hasChanged)
                {
                    DetachParentFormEvents();
                }
                _mainParent = value; 
                if (hasChanged)
                {
                    AttachParentFormEvents();
                }
                
            }
        }

        private void AttachParentFormEvents()
        {
            if (_mainParent != null)
            {
                _mainParent.Closing += _mainParent_Closing;
            }
        }

        private void DetachParentFormEvents()
        {
            if (_mainParent != null)
            {
                _mainParent.Closing -= _mainParent_Closing;
            }
        }


        void _mainParent_Closing(object sender, CancelEventArgs e)
        {
            if (_bigWindow != null)
            {
                _bigWindow.CanCloseInstance = true;
                _bigWindow.Close();
                _bigWindow.Dispose();
                _bigWindow = null;
            }
        }

        #endregion

        #region Disegno sagome
        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente ha gi� eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="shapes">Vettore di sagome, contiene le sagome da disegnare.</param>
        /// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="azzeraArea">Indica se l'area deve essere azzerata prima di disegnare.</param>
        public void DrawTop(Shape[] shapes, Bitmap[] pathImage, string pathProfiliLav, bool azzeraArea)
        {

            if(azzeraArea) vppRender1.Entities.Clear();

            vppRender1.OriginSymbol.Visible = false;

            //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
            render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca);

            vppRender1.ZoomFit();

        }

        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente ha gi� eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="shapes">Vettore di sagome, contiene le sagome da disegnare.</param>
        /// <param name="pathImage">Vettore di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        public void DrawTop(Shape[] shapes, Bitmap[] pathImage, string pathProfiliLav)
        {

            vppRender1.Entities.Clear();
            vppRender1.Materials.Clear();

            vppRender1.OriginSymbol.Visible = false;

            //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
            render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca);

            vppRender1.ZoomFit();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="shapes"></param>
        /// <param name="pathImage"></param>
        /// <param name="pathProfiliLav"></param>
        /// <param name="elements"></param>
        public void DrawTopOnFiles(Shape[] shapes, string[] pathImage, string pathProfiliLav, Dictionary<int, RenderedElement> elements)
        {

            vppRender1.Entities.Clear();
            vppRender1.Materials.Clear();

            vppRender1.OriginSymbol.Visible = false;

            //render.DrawTop(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, prbMain);
            render.DrawTopOnFiles(shapes, pathImage, pathProfiliLav, vppRender1, profonditaRibasso, profonditaCanaletti, profonditaBussole, profonditaTasca, elements);


            //vppRender1.ZoomFit();
            //vppRender1.Invalidate();
        }




        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente non ha gi� eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="pathXML">Vettore di percorsi, contiene i percorsi dei files XML da elaborare.</param>
        /// <param name="pathImage">ArrayList di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        /// <param name="azzeraArea">Indica se l'area deve essere azzerata prima di disegnare.</param>
        public void DrawTop(string[] pathXML, Bitmap[] pathImage, string pathProfiliLav, bool azzeraArea)
        {

            try
            {

                Shape[] shapes = new Shape[pathXML.Length];

                for (int i = 0; i < pathXML.Length; i++)
                {

                    Shape tmp = new Shape();
                    XmlAnalizer.ReadShapeXml(pathXML[i], ref tmp, true);
                    shapes[i] = tmp;

                }

                DrawTop(shapes, pathImage, pathProfiliLav, azzeraArea);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawTop Error. ", ex);
            }

        }

        /// <summary>
        /// Procedura di disegno delle sagome, utilizzata se l'utente non ha gi� eseguito singolarmente il comando 'ReadShapeXml'.
        /// </summary>
        /// <param name="pathXML">Vettore di percorsi, contiene i percorsi dei files XML da elaborare.</param>
        /// <param name="pathImage">ArrayList di stringhe, contiene i percorsi delle singole immagini compreso il nome.</param>
        /// <param name="pathProfiliLav">Percorso dei profili.</param>
        public void DrawTop(string[] pathXML, Bitmap[] pathImage, string pathProfiliLav)
        {

            try
            {

                Shape[] shapes = new Shape[pathXML.Length];

                for (int i = 0; i < pathXML.Length; i++)
                {

                    Shape tmp = new Shape();
                    XmlAnalizer.ReadShapeXml(pathXML[i], ref tmp, true);
                    shapes[i] = tmp;

                }

                DrawTop(shapes, pathImage, pathProfiliLav);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DrawTop Error. ", ex);
            }

        }
        #endregion

        /// <summary>
        /// Procedura di modifica del materiale di una sagoma.
        /// </summary>
        /// <param name="shapeNumber">Vettore di interi, contiene gli indici delle Sagome.</param>
        /// <param name="pathImage">Vettore di Bitmap, contiene le singole immagini.</param>
        public void ChangeMaterial(int[] shapeNumber, Bitmap[] pathImage)
        {

            try
            {

                render.ChangeMaterial(shapeNumber, pathImage, vppRender1);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ChangMaterial Error. ", ex);
            }

        }

        /// <summary>
        /// Procedura di modifica del materiale di una sagoma.
        /// </summary>
        /// <param name="shapeNumber">id sagoma.</param>
        /// <param name="pathImage">immagine</param>
        public void ChangeMaterial(int shapeNumber, Bitmap pathImage)
        {

            try
            {

                render.ChangeMaterial(shapeNumber, pathImage, vppRender1);

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("ChangeMaterial Error. ", ex);
            }

        }

        #region Gestione Toolbar men�/buttons

        private void DrawToolStrip()
        {
            Color color1 = System.Drawing.SystemColors.Control;
            Color color2 = System.Drawing.SystemColors.ControlDark;
            Color color3 = System.Drawing.SystemColors.ControlLightLight;

            Bitmap bmp = new Bitmap(10, 45);

            Rectangle rc1 = Rectangle.Empty;
            rc1.Height = bmp.Height / 3;
            rc1.Width = bmp.Size.Width;
            Rectangle rc2 = Rectangle.Empty;
            rc2.Height = bmp.Height / 3;
            rc2.Width = bmp.Size.Width;
            rc2.Offset(0, rc1.Bottom + 1);
            Rectangle rc3 = Rectangle.Empty;
            rc3.Height = (bmp.Height / 3);
            rc3.Width = bmp.Size.Width;
            rc3.Offset(0, rc2.Bottom + 1);

            Rectangle line1 = Rectangle.Empty;
            line1.Height = 1;
            line1.Width = bmp.Size.Width;
            line1.Offset(0, 0);
            Rectangle line2 = Rectangle.Empty;
            line2.Height = 1;
            line2.Width = bmp.Size.Width;
            line2.Offset(0, 1);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color3, color1, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
                    g.FillRectangle(b, rc1);
                using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color1, color1, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
                    g.FillRectangle(b, rc2);
                using (Brush b = new System.Drawing.Drawing2D.LinearGradientBrush(rc1, color1, color2, System.Drawing.Drawing2D.LinearGradientMode.Vertical))
                    g.FillRectangle(b, rc3);
                using (Brush b = new System.Drawing.SolidBrush(System.Drawing.SystemColors.ControlDark))
                    g.FillRectangle(b, line1);
                using (Brush b = new System.Drawing.SolidBrush(System.Drawing.SystemColors.ControlLight))
                    g.FillRectangle(b, line2);
            }

            tlsMain.BackgroundImage = bmp;
            tlsMain.BackgroundImageLayout = ImageLayout.Stretch;

        }

        #endregion

        #region protected methods for events


        /// <summary>
        /// Svuota il layout
        /// </summary>
        public void ClearLayout()
        {
            if (render != null)
            {
                render.ClearProfiles();
            }

            foreach (var ent in vppRender1.Entities)
            {
                ent.MaterialName = string.Empty;
            }
            vppRender1.Entities.ClearSelection();
            vppRender1.Entities.Clear();

            while (vppRender1.Materials.Count > 0)
            {
                var element = vppRender1.Materials.ElementAt(0);
                vppRender1.Materials.Remove(element.Key);
                element.Value.Dispose();
            }
            vppRender1.Layers.Clear(new Layer("Default"));

            //this.vppRender1.Clear();

            if (_bigWindow != null)
            {
                _bigWindow.ResetLayout();
            }
        }

        /// <summary>
        /// Invalidate and zoom all
        /// </summary>
        public void InvalidateAndFit()
        {
            vppRender1.ZoomFit();
            vppRender1.Invalidate();
        }

        protected virtual void OnRenderDoubleClick()
        {
            try
            {
                if (!RenderWindow.gCreate)
                {
                    MainParent = this.ParentForm;
                    _bigWindow = RenderWindow.Instance;
                    _bigWindow.Init(Lingua, render, vppRender1);
                    if (MainParent != null)
                    {
                        _bigWindow.Show(MainParent);
                    }
                    else
                    {
                        _bigWindow.Show();
                    }

                    _bigWindow.CreateContent();
                }
                else
                {
                    if (_bigWindow != null)
                    {
                        if (_bigWindow.Visible)
                            _bigWindow.Activate();
                        else
                            _bigWindow.Show();
                    }
                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("OnRenderDoubleClick Error. ", ex);
            }
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            vppRender1.Viewports[0].DisplayMode = displayType.Wireframe;
            vppRender1.ShowCurveDirection = true;
            vppRender1.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            vppRender1.Viewports[0].DisplayMode = displayType.Rendered;
            vppRender1.Invalidate();
        }

        private void btnShowNormals_Click(object sender, EventArgs e)
        {
            vppRender1.ShowNormals = !vppRender1.ShowNormals;
            vppRender1.Invalidate();

        }

        private void tsChkLight_CheckedChanged(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            if (button != null)
            {
                button.Image = button.Checked ? Resources.BulbOn : Resources.BulbOff;

                if (_loading) return;

                LightSettings settings = null;
                switch (button.Name)
                {
                    case "tsChkLight1":
                        settings = vppRender1.Light1;
                        break;

                    case "tsChkLight2":
                        settings = vppRender1.Light2;
                        break;

                    case "tsChkLight3":
                        settings = vppRender1.Light3;
                        break;

                    case "tsChkLight4":
                        settings = vppRender1.Light4;
                        break;

                }

                if (settings != null)
                {
                    if (settings.Active != button.Checked)
                    {
                        settings.Active = button.Checked;
                        vppRender1.Invalidate();
                    }
                }
            }
        }

    }

}