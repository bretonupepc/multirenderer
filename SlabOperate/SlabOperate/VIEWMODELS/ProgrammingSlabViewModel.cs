﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Timers;
using System.Windows;
using Breton.BCamPath;
using Breton.CutImages;
using Breton.DesignCenterInterface;
using Breton.MathUtils;
using SlabOperate.BUSINESS;
using SlabOperate.Business.Entities;
using SlabOperate.SESSION;
using SlabOperate.USERCONTROLS;
using TraceLoggers;
using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;
using Path = Breton.Polygons.Path;

namespace SlabOperate.VIEWMODELS
{
    public class ProgrammingSlabViewModel: ObservableObject
    {
        #region >-------------- Constants and Enums

        private const double THICKNESS_STEP = 10;

        private const int RENDER_DELAY = 20;


        private const bool mAllowSlopedSideLock = false;

        private const double mDistanceJoin = 10;

        private struct DeleteBlock
        {
            public CBPExtension cbpe;
            public Block deleteBlock;
        }


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private SlabNestingUc _view;

        private FksqlCbpe _cbpe;

        private OperationsViewModel _parentModel;

        private bool _isMainModel;

        private SlabEntity _slab;

        private ObservableCollection<PieceBlock> _slabPieces = new ObservableCollection<PieceBlock>();

        private Operation _currentOperation = Operation.NONE;

        private List<DeleteBlock> _blocksToBeDeleted = new List<DeleteBlock>();

        private List<int> _currentBlocks = new List<int>();

        private int _currentBlock;

        protected SortedList<int, Block> mBlocks;

        private Block mRightClickSelBlock;

        private TecnoSideInfo mRightClickTecnoSideInfo;
        private TecnoPath mRightClickTecnoPath;
        private int mRightClickSide;


        private bool _isControlKeyPressed;

        private bool _isDeletingBlock = false;

        private bool _canRemoveSlab = false;

        private bool _canResetSlab = false;

        private string _currentCoordinatesString = string.Empty;

        private CutImage _cut;

        private CutImageThread mCutImageThread;

        #region Toggles

        private bool _showSlab;

        private bool _showDefects;

        private bool _showShapes;

        private bool _showRaw;

        private bool _showCutTool;

        private bool _showCutToolRemoval;

        private bool _showSymbol;

        private bool _showText;

        private bool _setOrtho;

        private bool _isMagnetEnabled;

        private bool _isCollisionDetectionEnabled;

        private Timer _renderRefreshTimer = new Timer();

        private List<Block> _renderRefreshBlocks;

        private bool _renderRefreshUseThread;

        #endregion

        #region Handlers
        private CBasicPaint.MouseMoveAfterHandler evMouseMoveAfter;
        private CBasicPaintEventArgsHandler evClick;

        private CBasicPaintEventArgsHandler evStartMove;
        private CBasicPaintEventArgsHandler evMove;
        private CBasicPaintEventArgsHandler evEndMove;

        private CBasicPaintEventArgsHandler evStartRotate;
        private CBasicPaintEventArgsHandler evRotate;
        private CBasicPaintEventArgsHandler evEndRotate;

        private CBasicPaint.DragEnterHandler evDragEnter;
        private CBasicPaint.DragDropHandler evDragDrop;
        private CBasicPaint.DragLeaveHandler evDragLeave;
        #endregion

        private bool _moveAction;

        private bool _rotateAction;

        private bool _deleteAction;


        #region >-------------- Private Fields - Commands


        #endregion Private Fields - Commands --------<

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ProgrammingSlabViewModel(OperationsViewModel operationsViewModel)
        {
            _parentModel = operationsViewModel;

            _parentModel.PropertyChanged += _parentModel_PropertyChanged;

            _view = new SlabNestingUc();

            InitCpbeView();

            _view.DataContext = this;

            GetParentTogglesSet();

            _slabPieces.CollectionChanged += _slabPieces_CollectionChanged;

            _renderRefreshTimer.Interval = RENDER_DELAY;
            _renderRefreshTimer.Elapsed += _renderRefreshTimer_Elapsed;

        }

        void _renderRefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _renderRefreshTimer.Stop();

            RefreshRender(_renderRefreshBlocks, _renderRefreshUseThread);
        }

        private void RefreshRender( List<Block> blocks, bool useThread)
        {
            foreach (Block b in blocks)
                RefreshRender(b, useThread);
        }


        void _parentModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ShowSlab":
                    ShowSlab = _parentModel.ShowSlab;
                    break;
                case "ShowShapes":
                    ShowShapes = _parentModel.ShowShapes;
                    break;
                case "ShowRaw":
                    ShowRaw = _parentModel.ShowRaw;
                    break;
                case "ShowCutTool":
                    ShowCutTool = _parentModel.ShowCutTool;
                    break;
                case "ShowCutToolRemoval":
                    ShowCutToolRemoval = _parentModel.ShowCutToolRemoval;
                    break;
                case "ShowText":
                    ShowText = _parentModel.ShowText;
                    break;
                case "IsCollisionDetectionEnabled":
                    IsCollisionDetectionEnabled = _parentModel.IsCollisionDetectionEnabled;
                    break;
            }
        }

        private void GetParentTogglesSet()
        {
            ShowSlab = _parentModel.ShowSlab;
            ShowShapes = _parentModel.ShowShapes;
            ShowRaw = _parentModel.ShowRaw;
            ShowCutTool = _parentModel.ShowCutTool;
            ShowCutToolRemoval = _parentModel.ShowCutToolRemoval;
            ShowText = _parentModel.ShowText;
            IsCollisionDetectionEnabled = _parentModel.IsCollisionDetectionEnabled;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        #region Toggles

        public bool ShowSlab
        {
            get { return _showSlab; }
            set
            {
                _showSlab = value;
                OnPropertyChanged("ShowSlab");
                SetLayerVisible(CBPExtension.LayersType.PHOTO, value);
            }
        }

        public bool ShowDefects
        {
            get { return _showDefects; }
            set
            {
                _showDefects = value;
                OnPropertyChanged("ShowDefects");
                SetLayerVisible(CBPExtension.LayersType.DEFECT, value);
            }
        }

        public bool ShowShapes
        {
            get { return _showShapes; }
            set
            {
                _showShapes = value;
                OnPropertyChanged("ShowShapes");
                SetLayerVisible(CBPExtension.LayersType.SHAPE, value);
                SetLayerVisible(CBPExtension.LayersType.SHAPE_HOLE, value);
                SetLayerVisible(CBPExtension.LayersType.GROOVE, value);
                SetLayerVisible(CBPExtension.LayersType.POCKET, value);
            }
        }

        public bool ShowRaw
        {
            get { return _showRaw; }
            set
            {
                _showRaw = value;
                OnPropertyChanged("ShowRaw");
                SetLayerVisible(CBPExtension.LayersType.RAW, value);
                SetLayerVisible(CBPExtension.LayersType.RAW_HOLE, value);
                SetLayerVisible(CBPExtension.LayersType.BLIND_HOLE, value);
            }
        }

        public bool ShowCutTool
        {
            get { return _showCutTool; }
            set
            {
                _showCutTool = value;
                OnPropertyChanged("ShowCutTool");
                SetLayerVisible(CBPExtension.LayersType.FINAL_TOOL, value);
                SetLayerVisible(CBPExtension.LayersType.SLOPED_CUTS_NEG, value);
                SetLayerVisible(CBPExtension.LayersType.SLOPED_CUTS_POS, value);

            }
        }

        public bool ShowCutToolRemoval
        {
            get { return _showCutToolRemoval; }
            set
            {
                _showCutToolRemoval = value;
                OnPropertyChanged("ShowCutToolRemoval");
                SetLayerVisible(CBPExtension.LayersType.FINAL_TOOL_REM, value);
                SetLayerVisible(CBPExtension.LayersType.SLOPED_CUTS_NEG_REM, value);
                SetLayerVisible(CBPExtension.LayersType.SLOPED_CUTS_POS_REM, value);
            }
        }

        public bool ShowSymbol
        {
            get { return _showSymbol; }
            set
            {
                _showSymbol = value;
                OnPropertyChanged("ShowSymbol");
                SetLayerVisible(CBPExtension.LayersType.SYMBOL, value);
            }
        }

        public bool ShowText
        {
            get { return _showText; }
            set
            {
                _showText = value;
                OnPropertyChanged("ShowText");
                SetLayerVisible(CBPExtension.LayersType.TEXT, value);
            }
        }

        public bool SetOrtho
        {
            get { return _setOrtho; }
            set
            {
                _setOrtho = value;
                OnPropertyChanged("SetOrtho");

            }
        }

        public bool IsMainModel
        {
            get { return _isMainModel; }
            set
            {
                _isMainModel = value;
                OnPropertyChanged("IsMainModel");
                if (_isMainModel)
                {
                    _parentModel.SetMainModel(this);
                }
            }
        }

        public SlabNestingUc View
        {
            get { return _view; }
        }

        public FksqlCbpe Cbpe
        {
            get { return _cbpe; }
            set { _cbpe = value; }
        }

        public bool IsMagnetEnabled
        {
            get { return _isMagnetEnabled; }
            set
            {
                _isMagnetEnabled = value;
                OnPropertyChanged("IsMagnetEnabled");
            }
        }

        public bool IsCollisionDetectionEnabled
        {
            get { return _isCollisionDetectionEnabled; }
            set
            {
                _isCollisionDetectionEnabled = value;
                OnPropertyChanged("IsCollisionDetectionEnabled");
                Cbpe.CollisionEnable = !value;
            }
        }

        public SlabEntity Slab
        {
            get { return _slab; }
            set
            {
                _slab = value;
                SetupGraphicSlab(_slab);
                OnPropertyChanged("Slab");
            }
        }

        private void SetupGraphicSlab(SlabEntity slab)
        {
            Cbpe.Reset();
            InitCbpeLayers();

            if (slab == null)
            {
                if (Cut != null)
                {
                    Cut.Dispose();
                    Cut = null;
                }
                if (mCutImageThread != null)
                {
                    DetachCutImageThreadEvents();
                    mCutImageThread.Dispose();
                    mCutImageThread = null;
                }
                return;
            }

            Cbpe.CurrentSlab = new GraphicSlab(slab);

            Cut = new CutImage(slab.SlabImage.ImagePath);

            string defaultBitmapPath = System.IO.Path.Combine(ApplicationRun.Instance.StartupPath,
                ProjectRenderViewModel.DEFAULT_MATERIAL_IMAGE_NAME);

            if (File.Exists(defaultBitmapPath))
            {
			    Bitmap tmpBitmap;
                using (var tmp = Image.FromFile(defaultBitmapPath))
			    {
			        tmpBitmap = (Bitmap)tmp.Clone();
                    mCutImageThread = new CutImageThread(tmpBitmap);
                    mCutImageThread.CreateSquareImage = true;
                    AttachCutImageThreadEvents();
			    }
            }
        }

        private void AttachCutImageThreadEvents()
        {
            if (mCutImageThread != null)
                mCutImageThread.CutChange += mCutImageThread_CutChange;
        }
        private void DetachCutImageThreadEvents()
        {
            if (mCutImageThread != null)
                mCutImageThread.CutChange += mCutImageThread_CutChange;
        }

        private void RunOnUiThread(Action action)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke(action);
            }
            else
            {
                action.Invoke();
            }
        }

        void mCutImageThread_CutChange(object sender, int numSag, Bitmap ritaglio)
        {
            RunOnUiThread(()=>_parentModel.ProjectViewModel.RenderViewModel.RenderMasterViewModel.ChangeMaterial(numSag, ritaglio));

        }

        private void InitCbpeLayers()
        {
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.PHOTO.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLAB.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.DEFECT.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.RAW.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.RAW_HOLE.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.BLIND_HOLE.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.DISC.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.DISC_REM.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.COMBINED.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.COMBINED_REM.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.MILL.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.MILL_REM.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.EDGE_FINISHING_DIM.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.EDGE_FINISHING_TOOL.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.FINAL_TOOL.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.FINAL_TOOL_REM.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_POS.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_POS_REM.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_NEG.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_NEG_REM.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.REFERENCE_POINT1.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.REFERENCE_POINT2.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLAB_CUTS.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.TEXT.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SYMBOL.ToString()], true, true, false);

            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SHAPE.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SHAPE_HOLE.ToString()], true, true);

        }

        public ObservableCollection<PieceBlock> SlabPieces
        {
            get { return _slabPieces; }
        }

        public Operation CurrentOperation
        {
            get { return _currentOperation; }
            set
            {
                _currentOperation = value;
                OnPropertyChanged("CurrentOperation");
            }
        }

        private List<DeleteBlock> BlocksToBeDeleted
        {
            get { return _blocksToBeDeleted; }
            set { _blocksToBeDeleted = value; }
        }

        public List<int> CurrentBlocks
        {
            get { return _currentBlocks; }
            set { _currentBlocks = value; }
        }

        public int CurrentBlock
        {
            get { return _currentBlock; }
            set { _currentBlock = value; }
        }

        public bool IsDeletingBlock
        {
            get { return _isDeletingBlock; }
            set
            {
                _isDeletingBlock = value;
                OnPropertyChanged("IsDeletingBlock");
            }
        }

        public bool CanRemoveSlab
        {
            get { return _canRemoveSlab; }
            set
            {
                _canRemoveSlab = value;
                OnPropertyChanged("CanRemoveSlab");
            }
        }

        public bool CanResetSlab
        {
            get { return _canResetSlab; }
            set
            {
                _canResetSlab = value;
                OnPropertyChanged("CanResetSlab");
            }
        }

        public string CurrentCoordinatesString
        {
            get { return _currentCoordinatesString; }
            set
            {
                _currentCoordinatesString = value;
                OnPropertyChanged("CurrentCoordinatesString");

            }
        }

        public bool MoveAction
        {
            get { return _moveAction; }
            set
            {
                _moveAction = value;
                OnPropertyChanged("MoveAction");
                Cbpe.Move = value;
                if (value)
                {
                    RotateAction = false;
                    DeleteAction = false;
                }
                CheckDragDropEnabling();
            }
        }

        public bool RotateAction
        {
            get { return _rotateAction; }
            set
            {
                _rotateAction = value;
                OnPropertyChanged("RotateAction");

                Cbpe.Rotate = value;
                if (value)
                {
                    MoveAction = false;
                    DeleteAction = false;
                }
                CheckDragDropEnabling();
            }
        }

        private void CheckDragDropEnabling()
        {
            if (!MoveAction && !RotateAction && CurrentOperation == Operation.NONE)
            {
                Cbpe.DragDrop = true;

            }
            else
            {
                Cbpe.DragDrop = false;
            }
        }

        public bool DeleteAction
        {
            get { return _deleteAction; }
            set
            {
                _deleteAction = value;
                OnPropertyChanged("DeleteAction");
                SetupDeleteAction(value);
            }
        }

        public CutImage Cut
        {
            get { return _cut; }
            set { _cut = value; }
        }


        private void SetupDeleteAction(bool set)
        {
            CurrentOperation = (set ? Operation.DELETING : Operation.NONE);

            // Se abilita l'eliminazione, disabilita il movimento e la rotazione
            if (set)
            {
                BlocksToBeDeleted = new List<DeleteBlock>();

                if (_parentModel.CbpeSource != _parentModel.CbpeProject && _parentModel.CbpeSource != null && !_parentModel.CbpeSource.Locked)
                {
                    //if (mCurrentBlock >= 0)
                    if (CurrentBlocks.Count > 0)
                    {
                        foreach (int idb in CurrentBlocks)
                        {
                            DeleteBlock d;
                            d.cbpe = _parentModel.CbpeSource;
                            d.deleteBlock = _parentModel.CbpeSource.GetBlock(idb);
                            _parentModel.CbpeSource.Select(idb, CBasicPaint.HighLigthType.DOT);
                            BlocksToBeDeleted.Add(d);
                        }
                    }

                    RotateAction = false;
                    MoveAction = false;

                }
            }
            else
            {
                UnSelectAll();
            }

            CheckDragDropEnabling();
        }

        #endregion

        #region >-------------- Public Properties - Commands




        #endregion Public Properties - Commands --------<

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        internal void UnSelectAll()
        {
            CurrentBlocks.Clear();
            if (Cbpe != null)
            {
                for (int i = Cbpe.Selections.Count - 1; i >= 0; i--)
                    Cbpe.UnSelect(Cbpe.Selections[i].SelectedBlock);
            }
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void SetLayerVisible(CBPExtension.LayersType layersType, bool visible)
        {
            Cbpe.LayerMode(Cbpe.Layers[layersType.ToString()], visible, true);
        }

        private void InitCpbeView()
        {
            if (Cbpe == null)
            {
                Cbpe = new FksqlCbpe(View.Viewer);
                AttachCbpeEvents();
                Cbpe.DragDrop = true;
                Cbpe.MagnetEnable = IsMagnetEnabled;
                Cbpe.CollisionEnable = IsCollisionDetectionEnabled;
            }
        }

        private void AttachCbpeEvents()
        {
            Cbpe.evMouseAfter += Cbpe_evMouseAfter;
            Cbpe.evClick += Cbpe_evClick;
            Cbpe.evStartMove += Cbpe_evStartMove;
            Cbpe.evMove += Cbpe_evMove;
            Cbpe.evEndMove += Cbpe_evEndMove;
            Cbpe.evStartRotate += Cbpe_evStartRotate;
            Cbpe.evRotate += Cbpe_evRotate;
            Cbpe.evEndRotate += Cbpe_evEndRotate;
            Cbpe.evDragEnter += Cbpe_evDragEnter;
            Cbpe.evDragDrop += Cbpe_evDragDrop;
            Cbpe.evDragLeave += Cbpe_evDragLeave;
        }

        /// <summary>
        /// Elimina i blocchi selezionati
        /// </summary>
        private void DeleteBlocks()
        {
            string message, caption;
            ArrayList blocksName = new ArrayList();

            message = "Eliminare i blocchi selezionati?";
            caption = "Eliminazione blocchi";

            if (MessageBox.Show(message, caption, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                for (int i = 0; i < BlocksToBeDeleted.Count; i++)
                {
                    //Cbpe.RemoveBlock(BlocksToBeDeleted[i].deleteBlock.Id);

                    var pieceBlock = SlabPieces.FirstOrDefault(p => p.PlacedBlockId == BlocksToBeDeleted[i].deleteBlock.Id);
                    if (pieceBlock != null)
                    {
                        this.SlabPieces.Remove(pieceBlock);
                    }

                    //////if (mDeleteBlocks[i].cbpe == cbpMain)
                    //////    cbpMain.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);
                    //////else if (mDeleteBlocks[i].cbpe == cbpOpt1)
                    //////    cbpOpt1.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);
                    //////else if (mDeleteBlocks[i].cbpe == cbpOpt2)
                    //////    cbpOpt2.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);
                    //////else if (mDeleteBlocks[i].cbpe == cbpOpt3)
                    //////    cbpOpt3.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);

                    //////if (RemnantDefinition.IsRemnantBlock(mDeleteBlocks[i].deleteBlock))
                    //////{
                    //////    RemoveOpRemnantPieceFromSlab(mDeleteBlocks[i].cbpe.Slab.Name, mDeleteBlocks[i].deleteBlock);
                    //////}
                    //////else
                    //{
                    //    // Rimuove il pezzo dalle strutture dati di OptiMaster
                    //    RemoveOpPieceFromSlab(mDeleteBlocks[i].cbpe.Slab.Name, mDeleteBlocks[i].deleteBlock);
                    //    blocksName.Add(mDeleteBlocks[i].deleteBlock.Code);
                    //}

                }

                //TODO: Verificare

                ////ReActiveShape(blocksName);

                //////mIsModified = true;
                ////mIsModified = (blocksName.Count > 0);


                //PreviewRender();
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates

        void _slabPieces_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    // Add piece to viewer
                    foreach (PieceBlock pb in e.NewItems)
                    {
                        int newBlockId = -1;
                        Block existingBlock = Cbpe.GetBlockByCode(pb.Block.Code);
                        if (existingBlock == null)
                            newBlockId = Cbpe.InsertBlock(pb.Block);
                        else
                        {
                            newBlockId = existingBlock.Id;
                        }
                        pb.IsAssignedToSLab = true;
                        pb.Piece.Slab = this.Slab;
                        pb.PlacedBlockId = newBlockId;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    // Remove piece from viewer; 
                    // Set piece as "Not assigned"
                    foreach (PieceBlock pb in e.OldItems)
                    {
                        var block = Cbpe.GetBlockByCode(pb.Block.Code);
                        if (block != null)
                        {
                            Cbpe.RemoveBlock(block.Id);
                            if (mCutImageThread != null)
                            {
                                mCutImageThread.AddDel(int.Parse(block.Name));
                            }
                        }
                        pb.IsAssignedToSLab = false;
                        pb.Piece.Slab = null;
                        pb.PlacedBlockId = -1;

                    }

                    break;

                case NotifyCollectionChangedAction.Reset:
                    // For each piece:
                    // Remove piece from viewer; 
                    // Set piece as "Not assigned"
                    if (e.OldItems != null)
                        foreach (PieceBlock pb in e.OldItems)
                        {
                            var block = Cbpe.GetBlockByCode(pb.Block.Code);
                            if (block != null)
                            {
                                Cbpe.RemoveBlock(block.Id);
                                if (mCutImageThread != null)
                                {
                                    mCutImageThread.AddDel(int.Parse(block.Name));
                                }
                            }
                            pb.IsAssignedToSLab = false;
                            pb.Piece.Slab = null;
                            pb.PlacedBlockId = -1;

                        }

                    break;
            }
            
        }

        /// <summary>
        /// Evento dell'uscita del drag di un pezzo da un finestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        /// <param name="bEnter"></param>
        private void Cbpe_evDragLeave(object sender, CBasicPaintEventArgs ev, Block bEnter)
        {
            //TODO:
            //tsslMessageMain.Text = "";
            //tsslMessageMain.BackColor = SystemColors.Control;
            //tsslMessageMain.ForeColor = Color.Black;


            ev.Accepted = true;
        }


        /// <summary>
        /// Evento della fine del DragDropo di un pezzo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        /// <param name="bDrop"></param>
        private void Cbpe_evDragDrop(object sender, CBasicPaintEventArgs ev, Block bDrop)
        {
            // dato il blocco, aggiungo la shape relativa nella finestra di destinazione
            CBPExtension cbpe = (CBPExtension)sender;

            //if (cbpe == Cbpe || cbpe == cbpProject)       // se è la stessa finestra, se è la finestra di progetto o se la finestra di destinazione non ha una lastra
            if (Cbpe == _parentModel.CbpeSource )       // se è la stessa finestra, se è la finestra di progetto o se la finestra di destinazione non ha una lastra
            {
                ev.Accepted = false;
            }

            else if (_parentModel.CbpeSource == _parentModel.CbpeProject)      // se la finestra sorgente è l'anteprima progetto
            {
                ev.Accepted = true;
                int idBlock = Cbpe.AddShape(bDrop);	// accetta e  inserisce la shape nella finestra di destinazione
                Cbpe.Repaint(idBlock);	// accetta e  inserisce la shape nella finestra di destinazione

                // Imposta che il pezzo non possa più essere selezionato
                if (_parentModel.SelectedPieceBlock != null)
                {
                    //_parentModel.SelectedPieceBlock.IsAssignedToSLab = true;
                    SlabPieces.Add(_parentModel.SelectedPieceBlock);

                }
                ////foreach (KeyValuePair<int, Block> b in cbpProject.Blocks)
                ////{
                ////    if (b.Value.Code == bDrop.Code)
                ////    {
                ////        b.Value.AllowDragDrop = false;
                ////        b.Value.SetColor(Color.LimeGreen);
                ////        cbpProject.Repaint(b.Value.Id);
                ////        int i = GetDetailOptiIndex(bDrop.Code.Trim());
                ////        if (i >= 0)
                ////        {
                ////            DetailOpti dop = mDetailsOpti[i];
                ////            dop.state = DetailOptiState.AVAILABLE_ASSIGNED;
                ////            mDetailsOpti[i] = dop;
                ////        }
                ////        LoadTree();
                ////        break;
                ////    }
                ////}

                // Aggiunge il pezzo nelle strutture dati di OptiMaster
                ////AddOpPieceInSlab(cbpe.Slab.Name, bDrop);

                //cbpe.ZoomAll();
                ////TryZoomAll(cbpe);


                Cbpe.ZoomAll();

                //TODO: Render
                //if (RenderBig.gCreate || panelRender.Visible)
                ////if (RenderWindow.gCreate || panelRender.Visible)
                RefreshRender(bDrop, true);

                ////mIsModified = true;
            }

            else if (_parentModel.CbpeSource != _parentModel.CbpeProject)      // se la finestra sorgente è una finestra di posizionamento
            {
                ev.Accepted = true;

                ////int idBlock = Cbpe.AddShape(bDrop);
                ////Cbpe.Repaint(idBlock);	// accetta e  inserisce la shape nella finestra di destinazione

                // dato il CBasicPaintExtension salvato nel MouseDown, cancello il blocco selezionato
                var psvm = _parentModel.GetViewModelFromCbpe(_parentModel.CbpeSource);
                if (psvm != null)
                {
                    psvm.SlabPieces.Remove(_parentModel.SelectedPieceBlock);
                }
                ////cbpeSource.RemoveBlock(cbpe.BlockDragDrop.Id);

                ////// Rimuove il pezzo dalle strutture dati di OptiMaster
                ////RemoveOpPieceFromSlab(cbpeSource.Slab.Name, bDrop);
                ////// Aggiunge il pezzo nelle strutture dati di OptiMaster
                ////AddOpPieceInSlab(cbpe.Slab.Name, bDrop);

                int idBlock = Cbpe.AddShape(bDrop);
                Cbpe.Repaint(idBlock);	// accetta e  inserisce la shape nella finestra di destinazione
                SlabPieces.Add(_parentModel.SelectedPieceBlock);


                //cbpe.ZoomAll();
                ////TryZoomAll(cbpe);
                Cbpe.ZoomAll();


                //TODO: Render
                //if (RenderBig.gCreate || panelRender.Visible)
                ////if (RenderWindow.gCreate || panelRender.Visible)
                ////    RefreshRender(cbpe, bDrop, true);

                ////mIsModified = true;
                RefreshRender(bDrop, true);

            }

            Cbpe.BlockDragDrop = null;

            _parentModel.CbpeSource.BlockDragDrop =null;
        }

        private PointF[] GetPointsBoundingBox(Breton.Polygons.Path pathRender)
        {
            PointF[] pbb = {
                new PointF((float)pathRender.GetVertexRT(0).X, (float)pathRender.GetVertexRT(0).Y),  
                new PointF((float)pathRender.GetVertexRT(1).X, (float)pathRender.GetVertexRT(1).Y), 
                new PointF((float)pathRender.GetVertexRT(2).X, (float)pathRender.GetVertexRT(2).Y),
                new PointF((float)pathRender.GetVertexRT(3).X, (float)pathRender.GetVertexRT(3).Y)};

            return pbb;
        }

        private Block GetBlockByCode(CBPExtension cbpe, string blockCode)
        {
            foreach (KeyValuePair<int, Block> b in cbpe.Blocks)
            {
                if (b.Value.Code == blockCode)
                    return b.Value;
            }

            return null;
        }

        public void DelayedRefreshRender( List<Block> blocks, bool useThread)
        {
            _renderRefreshTimer.Stop();

            _renderRefreshBlocks = blocks;
            _renderRefreshUseThread = useThread;

            _renderRefreshTimer.Start();
        }


        private void RefreshRender(Block block, bool useThread)
        {
            if (block == Cbpe.Slab)
                return;

            int numSag;
            // faccio il primo ritaglio per il RENDER
            if (!Int32.TryParse(block.Name, out numSag))
                return;

            PointF[] mlstPtiBB = GetPointsBoundingBox(Cbpe.GetRenderPath(block.Code));

            Block bOrigin =  GetBlockByCode(_parentModel.CbpeProject, block.Code);

            if (bOrigin == null)
                return;

            double mAngle = 0d;
            double xOrigin, yOrigin, angleOrigin;
            double x, y, angle;
            if (block != null)
            {
                bOrigin.GetRotoTransl(out xOrigin, out yOrigin, out angleOrigin);
                block.GetRotoTransl(out x, out y, out angle);
                mAngle = MathUtil.gdGradi(angle - angleOrigin);
            }

            if (mlstPtiBB.Length > 0)
            {
                // cerco il cut corretto nella lista Cuts e lo imposto come corrente
                //SetCut(cbpe.Slab.Name);

                if (!useThread)
                {
                    Cut.Scala = 1;
                    //cut.SetBitmapOut(mlstPtiBB);
                    Cut.SetSquareCenteredBitmapOut(ref mlstPtiBB);

                }
            }
            else
                return;

            if (useThread)
                //mCutImageThread.AddCut(Cut, numSag, (int)Slab.Length, (int)Slab.Width, (float)mAngle, mlstPtiBB);
                mCutImageThread.AddCut(Cut, numSag, Cut.LastraImageInfo.ImageWidth, Cut.LastraImageInfo.ImageHeight, (float)mAngle, mlstPtiBB);
            else
            {
                ////ritaglio immagine della lastra caricata
                //using (Bitmap ritaglio = Cut.GetImage((int)Slab.Length, (int)Slab.Width, (float)mAngle, mlstPtiBB))
                //{
                //    //ritaglio.Save(@"E:\Ritaglio.jpg");
                //    // assegna materiale
                //    ToBigRender tbg = new ToBigRender();
                //    tbg.idSag = numSag;
                //    tbg.imgSag = new Bitmap(ritaglio, TEXTURE_SIZE, TEXTURE_SIZE);
                //    lstToBigRender.Insert(numSag, tbg);
                //    var tmp = lstToBigRender[numSag + 1];
                //    lstToBigRender.RemoveAt(numSag + 1);

                //    if (tmp.imgSag != null)
                //    {
                //        tmp.imgSag.Dispose();
                //    }

                //}

            }
        }


        /// <summary>
        /// Evento dell'inizio del drag di un pezzo da un finestra ad un'altra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        /// <param name="bEnter"></param>
        private void Cbpe_evDragEnter(object sender, CBasicPaintEventArgs ev, Block bEnter)
        {
            

            //var details = new PersistableEntityCollection<WkOrderDetailEntity>(ApplicationRun.Instance.DataScope);
            //WkOrderDetailEntity detail = null;

            //var slabs = new PersistableEntityCollection<SlabEntity>(ApplicationRun.Instance.DataScope);
            //SlabEntity slab = null;

            //DetailCollection tmpDets = new DetailCollection(mDbInterface);
            //Detail det;
            //ArticleCollection tmpArts = new ArticleCollection(mDbInterface);
            //Article art;

            if (Cbpe != _parentModel.CbpeSource)
            {
                // Verifica che nella destinazione ci sia una lastra
                if (Cbpe.Slab == null)
                {
                    ev.Accepted = false;

                    Cbpe.BlockDragDrop = null;

                    //if ((CBPExtension)sender == cbpMain)
                    //{
                    //    tsslMessageMain.Text = ProjResource.gResource.LoadFixString(this, 3);
                    //    tsslMessageMain.BackColor = Color.Red;
                    //    tsslMessageMain.ForeColor = Color.White;
                    //}
                    //else if ((CBPExtension)sender == cbpOpt1)
                    //{
                    //    tsslMessage1.Text = ProjResource.gResource.LoadFixString(this, 3);
                    //    tsslMessage1.BackColor = Color.Red;
                    //    tsslMessage1.ForeColor = Color.White;
                    //}
                    //else if ((CBPExtension)sender == cbpOpt2)
                    //{
                    //    tsslMessage2.Text = ProjResource.gResource.LoadFixString(this, 3);
                    //    tsslMessage2.BackColor = Color.Red;
                    //    tsslMessage2.ForeColor = Color.White;
                    //}
                    //else if ((CBPExtension)sender == cbpOpt3)
                    //{
                    //    tsslMessage3.Text = ProjResource.gResource.LoadFixString(this, 3);
                    //    tsslMessage3.BackColor = Color.Red;
                    //    tsslMessage3.ForeColor = Color.White;
                    //}
                    return;
                }

                // Verifica che il componente non sia bloccato
                if (Cbpe.Locked)
                {
                    ev.Accepted = false;

                    Cbpe.BlockDragDrop = null;

                    //if ((CBPExtension)sender == cbpMain)
                    //{
                    //    tsslMessageMain.Text = ProjResource.gResource.LoadFixString(this, 10);
                    //    tsslMessageMain.BackColor = Color.Red;
                    //    tsslMessageMain.ForeColor = Color.White;
                    //}
                    //else if ((CBPExtension)sender == cbpOpt1)
                    //{
                    //    tsslMessage1.Text = ProjResource.gResource.LoadFixString(this, 10);
                    //    tsslMessage1.BackColor = Color.Red;
                    //    tsslMessage1.ForeColor = Color.White;
                    //}
                    //else if ((CBPExtension)sender == cbpOpt2)
                    //{
                    //    tsslMessage2.Text = ProjResource.gResource.LoadFixString(this, 10);
                    //    tsslMessage2.BackColor = Color.Red;
                    //    tsslMessage2.ForeColor = Color.White;
                    //}
                    //else if ((CBPExtension)sender == cbpOpt3)
                    //{
                    //    tsslMessage3.Text = ProjResource.gResource.LoadFixString(this, 10);
                    //    tsslMessage3.BackColor = Color.Red;
                    //    tsslMessage3.ForeColor = Color.White;
                    //}
                    return;
                }

                // Legge dal database il dettaglio legato allo shape che si sta usando per il Drag&Drop	
                ////FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("Project.Code"),ConditionOperator.Eq, _parentModel.CbpeSource.BlockDragDrop.Code );
                ////details.GetItemsFromRepository(clause: filterClause);
                ////if (details.Count > 0)
                ////{
                ////    detail = details[0];
                ////}

                ////filterClause = new FilterClause(new FieldItemBaseInfo("Code"), ConditionOperator.Eq, cbpe.Slab.Name);
                ////slabs.GetItemsFromRepository(clause: filterClause);
                ////if (slabs.Count >= 0)
                ////{
                ////    slab = slabs[0];
                ////}

                // Legge dal database il dettaglio legato alla lastra di destinazione

                if (_parentModel.SelectedPieceBlock == null)
                    return;
                var piece = _parentModel.SelectedPieceBlock.Piece;
                if (piece.OrderDetail.Material != null
                    && this.Slab != null
                    && this.Slab.ThicknessObject != null
                    && this.Slab.ThicknessObject.Material != null)
                {
                    var pieceMaterial = piece.OrderDetail.Material;
                    var slabMaterial = Slab.ThicknessObject.Material;
                    if (pieceMaterial.Code != slabMaterial.Code
                        || piece.OrderDetail.Thickness < Slab.Thickness - THICKNESS_STEP/2
                        || piece.OrderDetail.Thickness > Slab.Thickness + THICKNESS_STEP/2)
                    {
                        ev.Accepted = false;

                        Cbpe.BlockDragDrop = null;
                        return;

                    }
                }



                // Verifica che il pezzo e la lastra di destinazione siano compatibili
                Cbpe.XDragDrop = _parentModel.CbpeSource.XDragDrop;
                Cbpe.YDragDrop = _parentModel.CbpeSource.YDragDrop;
                Cbpe.BlockDragDrop = new Block(_parentModel.CbpeSource.BlockDragDrop);
            }
            ev.Accepted = true;
        }


        /// <summary>
        /// Evento della fine della rotazione nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cbpe_evEndRotate(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            //CBPExtension cbpe = (CBPExtension)sender;

            if (e != null)
            {
                e.Accepted = true;

                if (block_no.Count == 0)
                    return;

                List<int> idBlocks = new List<int>();

                // Prepara delle liste univoche di blocchi
                foreach (int i in block_no)
                {
                    if (!idBlocks.Contains(i))
                    {
                        Block block = Cbpe.Blocks[i];
                        if (block.AllowRotate)
                            idBlocks.Add(i);
                    }
                }

            }

            _parentModel.IsModified = true;
        }


        /// <summary>
        /// Evento della rotazione nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void Cbpe_evRotate(object sender, CBasicPaintEventArgs ev, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            CBPExtension cbpe = (CBPExtension)sender;

            if (ev != null)
            {
                List<int> idBlocks = new List<int>();
                List<Block> blocks = new List<Block>();

                // Prepara delle liste univoche di blocchi
                foreach (int i in block_no)
                {
                    // De Conti 03/11/2016
                    //if (!idBlocks.Contains(i))
                    //{
                    //    idBlocks.Add(i);
                    //    blocks.Add(cbpe.GetBlock(i));
                    //}
                    if (!idBlocks.Contains(i))
                    {
                        var block = cbpe.GetBlock(i);
                        if (block != null && block.AllowRotate)
                        {
                            idBlocks.Add(i);
                            blocks.Add(block);
                        }
                    }

                }

                if (cbpe.IsCollision(blocks))
                    ev.Accepted = false;
                else
                    ev.Accepted = true;



                DelayedRefreshRender(blocks, true);

                ////if (RenderBig.gCreate || panelRender.Visible)
                //if (WpfRenderWindow.gCreate)
                //{
                //    //for (int i = 0; i < idBlocks.Count; i++)
                //    _parentModel.DelayedRefreshRender(cbpe, blocks, true);
                //}
            }
        }


        /// <summary>
        /// Evento dell'inizio della rotazione nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void Cbpe_evStartRotate(object sender, CBasicPaintEventArgs ev, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (ev != null)
            {
                ev.Accepted = true;
            }
        }


        /// <summary>
        /// Evento della fine del movimento nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void Cbpe_evEndMove(object sender, CBasicPaintEventArgs ev, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            

            if (ev != null)
            {
                ev.Accepted = true;

                if (block_no.Count == 0)
                {
                    Cbpe.DeleteGenericLine();
                    return;
                }

                List<int> idBlocks = new List<int>();

                // Prepara delle liste univoche di blocchi
                foreach (int i in block_no)
                {
                    if (!idBlocks.Contains(i))
                    {
                        Block block = Cbpe.Blocks[i];
                        if (block.AllowMove)
                            idBlocks.Add(i);
                    }
                }

                if (Cbpe.MagnetEnable && idBlocks.Count == 1 && ev.MouseKey != CBasicPaintEventArgs.enMouseKey.MouseKey_None)
                    Cbpe.LockPieces();

                // Aggiorna i valori delle posizioni
                for (int i = 0; i < idBlocks.Count; i++)
                    UpdateOpPieceInSlab(Cbpe.Slab.Name, Cbpe.Blocks[idBlocks[i]]);


            }

            ////mIsModified = true;
        }

        /// <summary>
        /// Aggiorna un OpPiece nella lastra
        /// </summary>
        /// <param name="slab"></param>
        /// <param name="piece"></param>
        private bool UpdateOpPieceInSlab(string slab, Block piece)
        {

            bool retVal = false;
            try
            {
                #region info origin
                double traslX, traslY, rotAngle;

                piece.GetRotoTransl(out traslX, out traslY, out rotAngle);

                traslX = traslX - Cbpe.CurrentSlab.OriginX - Cbpe.CurrentSlab.GraphicOffsetX;
                traslY = traslY - Cbpe.CurrentSlab.OriginY - Cbpe.CurrentSlab.GraphicOffsetY;



                #endregion

                var slabPiece =
                    _parentModel.ProjectPieceBlocks.FirstOrDefault(p => p.Block.Code == piece.Code);
                // Aggiorna le 
                if (slabPiece != null)
                {

                    slabPiece.Piece.PosX = traslX;
                    slabPiece.Piece.PosY = traslY;
                    slabPiece.Piece.RotationAngle = rotAngle;


                }


                retVal = true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine(" ProgrammingSlabViewModel AddOpPieceInSlab error", ex);
                retVal = false;
            }

            return retVal;
        }



        void Cbpe_evMove(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            //CBPExtension cbpe = (CBPExtension)sender;

            if (e != null)
            {
                List<int> idBlocks = new List<int>();
                List<Block> blocks = new List<Block>();

                // Prepara delle liste univoche di blocchi
                foreach (int i in block_no)
                {
                    if (!idBlocks.Contains(i))
                    {
                        var block = Cbpe.GetBlock(i);
                        if (block.AllowMove)
                        {
                            idBlocks.Add(i);
                            blocks.Add(block);
                        }
                    }
                }

                if (Cbpe.MagnetEnable && idBlocks.Count == 1 &&
                    e.MouseKey != CBasicPaintEventArgs.enMouseKey.MouseKey_None)
                {
                    double thickness = Slab.Thickness;
                    Cbpe.CheckMagnet(Cbpe.GetBlock(idBlocks[0]), mAllowSlopedSideLock, thickness, mDistanceJoin);
                }

                if (Cbpe.IsCollision(blocks))
                    e.Accepted = false;
                else
                    e.Accepted = true;


                //TODO: Render
                //////if (RenderBig.gCreate || panelRender.Visible)
                ////if (RenderWindow.gCreate || panelRender.Visible)
                ////{
                ////    //for (int i = 0; i < idBlocks.Count; i++)
                ////    DelayedRefreshRender(cbpe, blocks, true);
                ////}

                DelayedRefreshRender(blocks, true);
            }
        }

        void Cbpe_evStartMove(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            if (e != null)
            {
                e.Accepted = true;
            }
        }

        void Cbpe_evClick(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {
            //int mCurrentBlock = -1;	// Usato solo dal reload.
            //int mCurrentPath = -1;	// Per il momento non usato
            //int mCurrentSide = -1;	// Per il momento non usato

            // clicco e rendo attivo uno dei CBPExtension
            if (_parentModel.CbpeSource != (CBPExtension)sender || e.MouseKey == CBasicPaintEventArgs.enMouseKey.MouseKey_Right)
            {
                _parentModel.UnSelectAll();
                _parentModel.CbpeSource = (CBPExtension)sender;
                if (CurrentOperation == Operation.NONE)
                    _parentModel.SelectedPieceBlock = null;
            }
            CBasicPaint.HighLigthType ht = CBasicPaint.HighLigthType.NONE;

            try
            {
                if (e != null)
                {
                    if (e.MouseKey == CBasicPaintEventArgs.enMouseKey.MouseKey_Left)
                    {
                        if (_parentModel.CbpeSource == _parentModel.CbpeProject)
                        {
                            // Deseleziona tutti i blocchi solo dalla visualizzazione del progetto
                            _parentModel.ProjectViewModel.UnSelectAll(); 

                            ht = CBasicPaint.HighLigthType.NORMAL;
                        }
                        else
                        {
                            if (CurrentOperation == Operation.DELETING)
                            {
                                if (Cbpe.Locked)
                                    return;

                                // Se è in corso la rimozione di alcuni pezzi, non è possibile la deselezione di un pezzo
                                DeleteBlock d;
                                d.cbpe = Cbpe;
                                d.deleteBlock = Cbpe.GetBlock(block_no[0]);

                                if (BlocksToBeDeleted.IndexOf(d) < 0)
                                    BlocksToBeDeleted.Add(d);

                                ht = CBasicPaint.HighLigthType.DOT;
                            }
                            else if (CurrentOperation == Operation.DRILLING)
                            {
                                return;
                                ////if (_parentModel.CbpeSource.Locked)
                                ////    return;

                                ////if (AddDrill())
                                ////    e.Accepted = true;
                                ////else
                                ////    e.Accepted = false;
                                ////return;
                            }
                            else if (CurrentOperation == Operation.RECOVERING)
                            {
                                return;
                                ////if (cbpeSource.Locked)
                                ////    return;

                                ////if (startPoint == null)
                                ////{
                                ////    startPoint = cbpeSource.CursorPosition();
                                ////    cbpeSource.DrawRecover(startPoint);
                                ////}
                                ////else
                                ////{
                                ////    //AddRecover();
                                ////    startPoint = null;
                                ////}
                            }

                            else if (CurrentOperation == Operation.REMNANT_DEFINING)
                            {
                                e.Accepted = false;
                                return;
                                //if (cbpeSource.Locked)
                                //    return;

                                //if (startPoint == null)
                                //{
                                //    startPoint = cbpeSource.CursorPosition();
                                //    cbpeSource.DrawRecover(startPoint);
                                //}
                                //else
                                //{
                                //    //AddRecover();
                                //    startPoint = null;
                                //}
                            }
                            else
                            {
                                if (!_isControlKeyPressed)
                                {
                                    // Deseleziona tutti i blocchi
                                    UnSelectAll();
                                }
                                ht = CBasicPaint.HighLigthType.NORMAL;
                            }
                        }

                        foreach (int idb in block_no)
                        {
                            if (idb != -1 && !CurrentBlocks.Contains(idb))
                                CurrentBlocks.Add(idb);
                        }

                        // Imposta il valore del blocco correntemente selezionato nel CBPE selezionato
                        if (Cbpe.Slab != null)
                        {
                            //for (int i = 0; i < mProgrammingSlabs.Count; i++)
                            {
                                //if (mProgrammingSlabs[i].code == cbpeSource.Slab.Name)
                                {
                                    //ProgrammingSlab p = mProgrammingSlabs[i];
                                    // Imposta il valore del blocco
                                    if (CurrentBlocks.Count > 0)
                                    {
                                        CurrentBlock = CurrentBlocks[0];
                                        // De Conti 20161108
                                        ////    if (cbpeSource != cbpProject)
                                        ////    {
                                        ////        var block = cbpeSource.Blocks[p.currentBlock];
                                        ////        if (block != null)
                                        ////        {
                                        ////            if (RemnantDefinition.IsRemnantBlock(block))
                                        ////            {
                                        ////                treeViewProjects.SelectedNode = SelectRemnantNode(block.Code);
                                        ////            }
                                        ////            else
                                        ////            {
                                        ////                treeViewProjects.SelectedNode = SelectNode(block.Code.Trim());
                                        ////            }
                                        ////        }
                                        ////    }

                                        foreach (var psvm in _parentModel.SlabViewModels)
                                            if (psvm != this)
                                                psvm.UnSelectAll();
                                        if (_parentModel.MainSlabModel != null && _parentModel.MainSlabModel != this)
                                            _parentModel.MainSlabModel.UnSelectAll();

                                        var selectedBlock =
                                            _parentModel.ProjectPieceBlocks.FirstOrDefault(
                                                p => p.Block.Code == Cbpe.Blocks[CurrentBlock].Code.Trim());

                                        //TODO: Aggiornare blocco selezionato corrente
                                        if (selectedBlock != null)
                                        {
                                            _parentModel.SelectedPieceBlock = selectedBlock;
                                        }


                                    }



                                    ////mProgrammingSlabs[i] = p;

                                    //break;
                                }
                            }
                        }

                        if (block_no.Count != 0)
                        {
                            foreach (int idb in block_no)
                            {
                                if (idb != -1 && !CurrentBlocks.Contains(idb))
                                    CurrentBlocks.Add(idb);
                            }

                            //for (int i = 0; i < block_no.Count; i++)
                            for (int i = 0; i < CurrentBlocks.Count; i++)
                            {
                                //cbpeSource.Select(block_no[i], ht);
                                Cbpe.Select(CurrentBlocks[i], ht);


                                // TODO: Manage CurrentBlock in parent viewmodel
                                ////if (cbpeSource == cbpProject)
                                ////{
                                ////    // Deseleziona tutti i pezzi nelle finestre tranne che sulla finestra di anteprima progetto
                                ////    UnSelectAll(cbpMain);
                                ////    UnSelectAll(cbpOpt1);
                                ////    UnSelectAll(cbpOpt2);
                                ////    UnSelectAll(cbpOpt3);
                                ////    //SelectBlockInSlab(cbpeSource.Blocks[block_no[i]].Code.Trim());
                                ////    SelectBlockInSlab(cbpeSource.Blocks[mCurrentBlocks[i]].Code.Trim());

                                ////    //treeViewProjects.SelectedNode = SelectNode(cbpeSource.Blocks[block_no[i]].Code.Trim());
                                ////    treeViewProjects.SelectedNode = SelectNode(cbpeSource.Blocks[mCurrentBlocks[i]].Code.Trim());
                                ////}
                            }
                        }
                        else
                        {
                            //mCurrentBlock = -1;
                            //mCurrentPath = -1;
                            //mCurrentSide = -1;
                        }

                        if (Cbpe.DragDrop)
                        {
                            // Se non ci sono blocchi selezionati
                            if (block_no.Count == 0)
                            {
                                e.Accepted = false;
                                return;
                            }

                            //TODO: 28/04/2016
                            ////// Indica qual'è il blocco per il Drag & Drop
                            //TODO: Verifica
                            ////if (cbpeSource != cbpProject)
                            {
                                int ind = Cbpe.Blocks.IndexOfKey(block_no[0]);

                                if (ind >= 0)
                                    Cbpe.BlockDragDrop = new Block(Cbpe.Blocks.Values[ind]);
                            }
                            ////else
                            ////{
                            ////    Cbpe.BlockDragDrop = cbpProject.BlockDragDrop;
                            ////}
                        }

                    }
                    else if (e.MouseKey == CBasicPaintEventArgs.enMouseKey.MouseKey_Right)
                    {
                        if (CurrentOperation == Operation.REMNANT_DEFINING)
                        {

                        }

                            // Se sono in cancellazione elimino i pezzi al momento selezionati
                        else if (CurrentOperation == Operation.DELETING)
                        {
                            if (BlocksToBeDeleted != null && BlocksToBeDeleted.Count > 0)
                            {
                                DeleteBlocks();
                            }

                            IsDeletingBlock = false;
                            DeleteAction = false;

                        }
                        else
                        {
                            //if (cbpeSource != cbpProject)
                            {
                                if (block_no.Count == 0 || (block_no.Count == 1 && block_no[0] == -1))
                                {
                                    CanRemoveSlab = !Cbpe.Locked;
                                    CanResetSlab = !Cbpe.Locked;

                                    (sender as CBasicPaint).SetDefaultCursor();

                                    
                                    ////contextMenuCBPE.Show(System.Windows.Forms.Control.MousePosition);

                                }
                                else if (block_no.Count == 1 && (elem_no.Count == 0 || (elem_no.Count == 1 && elem_no[0] == -1)))
                                {
                                    if (!Cbpe.Locked)
                                    {
                                        ht = CBasicPaint.HighLigthType.NORMAL;
                                        Cbpe.Select(block_no[0], ht);
                                        CurrentBlocks.Add(block_no[0]);

                                        (sender as CBasicPaint).SetDefaultCursor();

                                        //TODO: Context menu piece
                                        ////contextMenuPiece.Show(System.Windows.Forms.Control.MousePosition);

                                        mRightClickSelBlock = Cbpe.GetBlock(block_no[0]);

                                    }
                                }
                                else if (elem_no.Count > 0)
                                {
                                    // Scorre tutti gli elementi cliccati finchè non trova un lato RAW opp RAW_HOLE
                                    for (int i = 0; i < elem_no.Count; i++)
                                    {
                                        if (elem_no[i] == -1)
                                            continue;

                                        // Estrae il path a cui appartiene il lato cliccato e i vari tecnopaths 
                                        if (block_no[i] != -1 && path_no[i] != -1)
                                        {
                                            mRightClickSelBlock = Cbpe.GetBlock(block_no[0]);

                                            Path p = Cbpe.Blocks[block_no[i]].GetPath(path_no[i]);

                                            List<Path> pTecno = new List<Path>();
                                            Cbpe.GetPathType(block_no[i], -1, CBPExtension.LayersType.TECNO_INFO.ToString(), ref pTecno);

                                            TecnoSideInfo tsi = null;

                                            if (p.Layer == CBPExtension.LayersType.RAW.ToString())
                                            {
                                                // Scorre tutti i tecnopaths finchè non trova il percorso esterno
                                                foreach (TecnoPath tp in pTecno)
                                                {
                                                    if (tp.Type == Path.EN_PATH_MODE.EN_PATH_MODE_EXTERIOR)
                                                    {
                                                        // Legge le informazioni relative agli utensili attivi
                                                        tsi = tp.GetTecnoSideInfo(elem_no[i]);

                                                        mRightClickTecnoPath = tp;
                                                        mRightClickSide = elem_no[i];
                                                    }
                                                }
                                            }
                                            else if (p.Layer == CBPExtension.LayersType.RAW_HOLE.ToString())
                                            {
                                                // Scorre tutti i tecnopaths finchè non trova il percorso interno corretto
                                                foreach (TecnoPath tp in pTecno)
                                                {
                                                    if (tp.Type == Path.EN_PATH_MODE.EN_PATH_MODE_INTERIOR && tp.Attributes.ContainsKey("LINK_RAW_HOLE") && tp.Attributes["LINK_RAW_HOLE"] == p.Name)
                                                    {
                                                        // Legge le informazioni relative agli utensili attivi
                                                        tsi = tp.GetTecnoSideInfo(elem_no[i]);
                                                        mRightClickTecnoPath = tp;
                                                        mRightClickSide = elem_no[i];

                                                        break;
                                                    }
                                                }
                                            }

                                            if (tsi != null)
                                            {
                                                mRightClickTecnoSideInfo = tsi;

                                                // Inserisce tutti gli utensili letti in un contextMenu
                                                //TODO: Context menu
                                                ////contextMenuSide.Items.Clear();

                                                ////for (int j = 0; j < tsi.TecnoPathInfo.AvailableTools.Count; j++)
                                                ////{
                                                ////    ToolStripMenuItem tli = new ToolStripMenuItem();

                                                ////    tli.Name = tsi.TecnoPathInfo.AvailableTools[j].Name;
                                                ////    tli.Text = tsi.TecnoPathInfo.AvailableTools[j].Name;
                                                ////    tli.CheckOnClick = true;
                                                ////    if (tsi.TecnoPathInfo.AvailableTools[j].State == EN_TOOL_STATE.AUTOMATIC)
                                                ////        tli.Checked = true;
                                                ////    else
                                                ////        tli.Checked = false;
                                                ////    tli.CheckedChanged += new System.EventHandler(AvailableToolSide_CheckedChanged);

                                                ////    contextMenuSide.Items.Add(tli);
                                                ////}

                                                ////if (contextMenuSide.Items.Count > 0)
                                                ////{
                                                ////    (sender as CBasicPaint).SetDefaultCursor();
                                                ////    contextMenuSide.Show(System.Windows.Forms.Control.MousePosition);
                                                ////}

                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    e.Accepted = true;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DesignOperation.MouseDownEv ", ex);
                e.Accepted = false;
            }
        }

        /// <summary>
        /// Aggiunge un foretto associandolo alla lastra o al pezzo
        /// </summary>
        /// <returns></returns>
        ////private bool AddDrill()
        ////{
        ////    try
        ////    {
        ////        string caption, message;
        ////        // Prepara i dati per il calcolo del foretto
        ////        Article art;
        ////        ArticleCollection tmpArts = new ArticleCollection(mDbInterface);
        ////        TecnoPaths tempTecnoPaths = new TecnoPaths();
        ////        ToolPaths tempToolPaths = new ToolPaths();
        ////        TecnoPathInfo tpi = new TecnoPathInfo();
        ////        tmpArts.GetSingleElementFromDb(cbpeSource.Slab.Name);
        ////        tmpArts.GetObject(out art);
        ////        tpi.MaterialThickness = art.gDimZ;
        ////        tpi.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;

        ////        foreach (KeyValuePair<int, ToolInfo> t in mTools)
        ////        {
        ////            if (t.Value.Type == EN_TOOL_TYPE.EN_TOOL_TYPE_DRILL)
        ////                tpi.AvailableTools.Add(t.Value);
        ////        }

        ////        TecnoPath tp = new TecnoPath();
        ////        Breton.Polygons.Point fp = cbpeSource.CursorPosition();
        ////        TecnoSegment seg = new TecnoSegment(fp, fp);
        ////        int ids = tp.AddSide(seg);

        ////        TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
        ////        tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
        ////        tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
        ////        tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
        ////        tsi.TecnoPathInfo.MaterialThickness = art.gDimZ;

        ////        tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
        ////        // Indica se il tipo di percorso è esterno o se è un foro
        ////        tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_EXTERIOR;
        ////        tp.AllowSelect = true;
        ////        tempTecnoPaths.AddPath(tp);

        ////        mCam.CreateStandardToolPaths(tempTecnoPaths, out tempToolPaths, tpi, cbpeSource.Slab.Name);

        ////        Path DrillRem = mCam.RemovalPaths.GetPath(0);

        ////        PathCollection paths;
        ////        Path intZone = new Path(mCam.RemovalPaths.GetPath(0));
        ////        ZoneOffset.PathOffset(ref intZone, -20, ZoneOffset.EXPANSION_MODE.EXPANSION_MODE_RECT, out paths);
        ////        intZone.Layer = CBPExtension.LayersType.RAW_INTEREST_ZONE.ToString();

        ////        string blockcollision = null;
        ////        bool insideHole = false;
        ////        // Verifica se la zona di interesse del foretto interseca il pezzo
        ////        foreach (KeyValuePair<int, Block> bl in cbpeSource.Blocks)
        ////        {
        ////            // Verifica che il blocco che si sta analizzando non sia ne la lastra
        ////            // ne uno degli altri tagli aggiuntivi

        ////            if (bl.Value != cbpeSource.Slab &&
        ////                !cbpeSource.ExtraSlabCuts.Exists(delegate(Block x) { return x.Code == bl.Value.Code; }))
        ////            {
        ////                // Estrae tutti i grezzi del pezzo 
        ////                List<Path> raws = cbpeSource.GetRaw(bl.Value.Id, -1);
        ////                List<Path> holes = cbpeSource.GetRawHole(bl.Value.Id, -1);
        ////                foreach (Path r in raws)
        ////                {
        ////                    // Verica che il foretto non intersechi il pezzo
        ////                    if (DrillRem.IntersectRT(r, out intersectPoint))
        ////                    {
        ////                        ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////                        MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////                        throw new ApplicationException("Position Not valid");
        ////                    }

        ////                    // se il foretto è interno, verifica che non sia all'interno di un foro
        ////                    if (r.IsInsideRT(fp))
        ////                    {
        ////                        insideHole = false;
        ////                        foreach (Path h in holes)
        ////                        {
        ////                            // Se interseca un foro da errore
        ////                            if (DrillRem.IntersectRT(h, out intersectPoint))
        ////                            {
        ////                                ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////                                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////                                throw new ApplicationException("Position Not valid");
        ////                            }
        ////                            if (h.IsInsideRT(fp))
        ////                                insideHole = true;

        ////                            // Controllo di collisione										
        ////                            if (intZone.IntersectRT(h, out intersectPoint))
        ////                            {
        ////                                blockcollision = bl.Value.Code;
        ////                                break;
        ////                            }
        ////                        }

        ////                        if (!insideHole)
        ////                        {
        ////                            ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////                            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////                            throw new ApplicationException("Position Not valid");
        ////                        }
        ////                    }

        ////                    // Controllo di collisione										
        ////                    if (blockcollision == null && intZone.IntersectRT(r, out intersectPoint))
        ////                    {
        ////                        blockcollision = bl.Value.Code;
        ////                        break;
        ////                    }
        ////                }
        ////            }
        ////        }

        ////        // Se il foretto non deve essere associato a nessun pezzo esistente, lo aggiunge alla lastra
        ////        if (blockcollision == null)
        ////        {
        ////            //// Crea il blocco inserendo il percorso utensile del foretto								
        ////            //int drill = cbpeSource.AddSlabCut(tempToolPaths[0]);

        ////            //Block eb = cbpeSource.ExtraSlabCuts.Find(delegate(Block s) { return s.Name == CBPExtension.SlabCutsCode + drill.ToString(); });
        ////            //// Aggiunge al blocco il tecno path
        ////            //eb.AddPath(tempTecnoPaths[0]);
        ////            //// Aggiunge al blocco l'asportazione del foretto
        ////            //RemovalPath rp = (RemovalPath)mCam.RemovalPaths.GetPath(0);
        ////            //rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();
        ////            //eb.AddPath(rp);
        ////            //// Aggiunge al blocco la zona di interesse
        ////            //eb.AddPath(intZone);

        ////            //cbpeSource.Repaint(drill);

        ////            //*****************************
        ////            // Per il momento non è possibile inserire un foretto sulla lastra ma è necessario agganciarlo a un pezzo
        ////            ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////            throw new ApplicationException("Position Not valid");
        ////        }
        ////        else
        ////        // Altrimenti aggiunge il foretto a un pezzo già esistente
        ////        {
        ////            Block bl = cbpeSource.GetBlockByCode(blockcollision);
        ////            RTMatrix m = bl.MatrixRT.InvMat();

        ////            // Aggiunge al blocco il tecno path
        ////            TecnoPath tecnop = new TecnoPath(tempTecnoPaths[0]);
        ////            while (tecnop.Count > 0)
        ////                tecnop.DeleteSide(0);
        ////            for (int i = 0; i < tempTecnoPaths[0].Count; i++)
        ////                tecnop.AddSide(tempTecnoPaths[0].GetSide(i).RotoTrasl(m));
        ////            bl.AddPath(tecnop);

        ////            // Aggiunge al blocco l'asportazione del foretto
        ////            RemovalPath rp = new RemovalPath(mCam.RemovalPaths.GetPath(0));
        ////            rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();
        ////            while (rp.Count > 0)
        ////                rp.DeleteSide(0);
        ////            for (int i = 0; i < mCam.RemovalPaths.GetPath(0).Count; i++)
        ////                rp.AddSide(mCam.RemovalPaths.GetPath(0).GetSide(i).RotoTrasl(m));
        ////            // Aggiunge e ridisegna il path
        ////            cbpeSource.Repaint(bl.Id, bl.AddPath(rp));

        ////            // Aggiunge al blocco il tool path
        ////            ToolPath toolp = new ToolPath(tempToolPaths[0]);
        ////            toolp.Layer = CBPExtension.LayersType.FINAL_TOOL.ToString();
        ////            while (toolp.Count > 0)
        ////                toolp.DeleteSide(0);
        ////            for (int i = 0; i < tempToolPaths[0].Count; i++)
        ////                toolp.AddSide(tempToolPaths[0].GetSide(i).RotoTrasl(m));
        ////            // Aggiunge e ridisegna il path
        ////            cbpeSource.Repaint(bl.Id, bl.AddPath(toolp));

        ////            // Aggiunge al blocco la zona di interesse
        ////            Path iz = new Path(intZone);
        ////            while (iz.Count > 0)
        ////                iz.DeleteSide(0);
        ////            for (int i = 0; i < intZone.Count; i++)
        ////                iz.AddSide(intZone.GetSide(i).RotoTrasl(m));
        ////            bl.AddPath(iz);


        ////        }

        ////        mIsModified = true;
        ////        return true;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        TraceLog.WriteLine(this.ToString() + ".AddDrill", ex);
        ////        return false;
        ////    }
        ////}


        void Cbpe_evMouseAfter(object sender, double x, double y, double z)
        {
            string strCoord;

            if (x < 0d)
                strCoord = string.Concat("X: ", x.ToString("0.0000"));
            else
                strCoord = string.Concat("X:  ", x.ToString("0.0000"));

            if (y < 0d)
                strCoord = string.Concat(strCoord, " ; Y: ", y.ToString("0.0000"));
            else
                strCoord = string.Concat(strCoord, " ; Y:  ", y.ToString("0.0000"));

            CurrentCoordinatesString = strCoord;


            //if ((CBPExtension)sender == cbpMain)
            //    tsslCoordMain.Text = strCoord;
            //else if ((CBPExtension)sender == cbpOpt1)
            //    tsslCoordOpt1.Text = strCoord;
            //else if ((CBPExtension)sender == cbpOpt2)
            //    tsslCoordOpt2.Text = strCoord;
            //else if ((CBPExtension)sender == cbpOpt3)
            //    tsslCoordOpt3.Text = strCoord;
            //else if ((CBPExtension)sender == cbpProject)
            //    tsslCoordProject.Text = strCoord;

            //if (cbpeSource != null && mOperation == Operation.DRILLING)
            //    cbpeSource.DrawDrill(mDrill.Radius);

            //else if (cbpeSource != null && mOperation == Operation.RECOVERING)
            //    cbpeSource.DrawRecover(startPoint);
        }

        #endregion Delegates  -----------<

        internal void Terminate()
        {
            if (mCutImageThread != null)
            {
                DetachCutImageThreadEvents();
                mCutImageThread.Dispose();
                mCutImageThread = null;
            }
            SlabPieces.Clear();

            Cbpe = null;
            _view = null;
        }
    }
}
