﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Breton.BCamPath;
using Breton.CutImages;
using Breton.DesignCenterInterface;
using Breton.DesignUtil;
using Breton.MathUtils;
using Breton.Polygons;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using RenderMaster.Wpf.VIEWMODELS;
using RenderMaster.Wpf.WINDOWS;
using SlabOperate.BUSINESS;
using SlabOperate.Business.Entities;
using SlabOperate.Business.Entities.WORK.DETAILS;
using SlabOperate.Business.Entities.WORK.ORDER;
using SlabOperate.SESSION;
using SlabOperate.USERCONTROLS;
using TraceLoggers;
using Wpf.Common.Commands;
using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;
using Timer = System.Timers.Timer;

namespace SlabOperate.VIEWMODELS
{
    public enum Operation
    {
        NONE,
        DELETING,
        DRILLING,
        RECOVERING,
        MOVING,
        ROTATING,
        REMNANT_DEFINING
    }

    public class OperationsViewModel : ObservableObject
    {
        #region >-------------- Constants and Enums

        public struct ProgrammingSlab
        {
            public int idSlab;
            public int idOpSlab;
            public string code;
            public double dimX, dimY, originX, originY;
            public CBPExtension cbpe;
            public Partition partition;
            public CutImage cut;
            public int currentBlock;
        }

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private ProgrammingSlabViewModel _mainSlabModel;

        private ObservableCollection<ProgrammingSlabViewModel> _slabViewModels;

        private ProjectViewModel _projectViewModel;

        private ObservableCollection<SlabNestingUc> _slabViews; 
        
        private WkOrderEntity _currentOrder;

        private WkProjectEntity _currentProject;

        private CBPExtension _cbpeSource;

        private bool _isModified;

        private WpfRenderWindow _renderWindow;

        #region Refresh render

        private Timer _renderRefreshTimer = new Timer();

        private CBPExtension _renderRefreshCbpExtension = null;
        
        private List<Block> _renderRefreshBlocks;

        //private ObservableCollection<WkOrderDetailPieceEntity> _projectPieces = new ObservableCollection<WkOrderDetailPieceEntity>(); 

        private ObservableCollection<PieceBlock> _projectPieceBlocks = new ObservableCollection<PieceBlock>();

        private PieceBlock _selectedPieceBlock = null;

        private CBPExtension _cbpeProject = null;
        
        private bool _renderRefreshUseThread;


        private List<int> _currentBlocks = new List<int>();

        private RenderMasterViewModel _renderViewModel;

        private double _projectWindowSize = 300;

        #endregion


        #region Toggles

        private bool _showSlab = true;

        private bool _showDefects;

        private bool _showShapes = true;

        private bool _showRaw = true;

        private bool _showCutTool = true;

        private bool _showCutToolRemoval = true;

        private bool _showSymbol;

        private bool _showText = true;

        private bool _setOrtho;

        private bool _isCollisionDetectionEnabled;

        private bool _is2DProjectVisible = true;


        #endregion


        #region >-------------- Private Fields - Commands


        private DelegateCommand _selectSlabsCommand;

        #endregion Private Fields - Commands --------<

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        #region Toggles

        public bool ShowSlab
        {
            get { return _showSlab; }
            set
            {
                _showSlab = value;
                OnPropertyChanged("ShowSlab");
            }
        }

        public bool ShowDefects
        {
            get { return _showDefects; }
            set
            {
                _showDefects = value;
                OnPropertyChanged("ShowDefects");
            }
        }

        public bool ShowShapes
        {
            get { return _showShapes; }
            set
            {
                _showShapes = value;
                OnPropertyChanged("ShowShapes");
            }
        }

        public bool ShowRaw
        {
            get { return _showRaw; }
            set
            {
                _showRaw = value;
                OnPropertyChanged("ShowRaw");
            }
        }

        public bool ShowCutTool
        {
            get { return _showCutTool; }
            set
            {
                _showCutTool = value;
                OnPropertyChanged("ShowCutTool");
            }
        }

        public bool ShowCutToolRemoval
        {
            get { return _showCutToolRemoval; }
            set
            {
                _showCutToolRemoval = value;
                OnPropertyChanged("ShowCutToolRemoval");
            }
        }

        public bool ShowSymbol
        {
            get { return _showSymbol; }
            set
            {
                _showSymbol = value;
                OnPropertyChanged("ShowSymbol");
            }
        }

        public bool ShowText
        {
            get { return _showText; }
            set
            {
                _showText = value;
                OnPropertyChanged("ShowText");
            }
        }

        public bool SetOrtho
        {
            get { return _setOrtho; }
            set
            {
                _setOrtho = value;
                OnPropertyChanged("SetOrtho");
            }
        }
        #endregion


        public ProgrammingSlabViewModel MainSlabModel
        {
            get { return _mainSlabModel; }
            set
            {
                _mainSlabModel = value;
                OnPropertyChanged("MainSlabModel");
            }
        }

        public WkOrderEntity CurrentOrder
        {
            get { return _currentOrder; }
            set
            {
                _currentOrder = value;
                OnPropertyChanged("CurrentOrder");
                GetCurrentOrderData();

            }
        }


        public WkProjectEntity CurrentProject
        {
            get { return _currentProject; }
            set
            {
                _currentProject = value;
                OnPropertyChanged("CurrentProject");
            }
        }

        public ObservableCollection<ProgrammingSlabViewModel> SlabViewModels
        {
            get
            {
                if (_slabViewModels == null)
                {
                    _slabViewModels = new ObservableCollection<ProgrammingSlabViewModel>();
                    _slabViewModels.CollectionChanged += _slabViewModels_CollectionChanged;
                }
                return _slabViewModels;
            }
        }

        public ObservableCollection<SlabNestingUc> SlabViews
        {
            get
            {
                if (_slabViews == null)
                    _slabViews = new ObservableCollection<SlabNestingUc>();
                return _slabViews;
            }
        }

        public CBPExtension CbpeSource
        {
            get { return _cbpeSource; }
            set
            {
                _cbpeSource = value;
                OnPropertyChanged("CbpeSource");
            }
        }

        public bool IsModified
        {
            get { return _isModified; }
            set { _isModified = value; }
        }

        public WpfRenderWindow RenderWindow
        {
            get { return _renderWindow; }
            set { _renderWindow = value; }
        }

        public List<Block> RenderRefreshBlocks
        {
            get { return _renderRefreshBlocks; }
            set { _renderRefreshBlocks = value; }
        }

        public bool RenderRefreshUseThread
        {
            get { return _renderRefreshUseThread; }
            set { _renderRefreshUseThread = value; }
        }

        public CBPExtension RenderRefreshCbpExtension
        {
            get { return _renderRefreshCbpExtension; }
            set { _renderRefreshCbpExtension = value; }
        }

        //public ObservableCollection<WkOrderDetailPieceEntity> ProjectPieces
        //{
        //    get { return _projectPieces; }
        //    set { _projectPieces = value; }
        //}


        public ObservableCollection<PieceBlock> ProjectPieceBlocks
        {
            get { return _projectPieceBlocks; }
            set { _projectPieceBlocks = value; }
        }

        public DelegateCommand SelectSlabsCommand
        {
            get
            {
                if (_selectSlabsCommand == null)
                    _selectSlabsCommand = new DelegateCommand(SelectSlabs, CanselectSlabs);
                return _selectSlabsCommand;
            }
        }

        public bool IsMasterViewVisible
        {
            get { return MainSlabModel != null; }
        }

        public bool AreChildrenViewsVisible
        {
            get { return SlabViewModels.Count > 0; }
        }

        public List<int> CurrentBlocks
        {
            get { return _currentBlocks; }
            set { _currentBlocks = value; }
        }

        public ProjectViewModel ProjectViewModel
        {
            get
            {
                return _projectViewModel;
            }
            set
            {
                _projectViewModel = value;
                OnPropertyChanged("ProjectViewModel");
            }
        }


        public CBPExtension CbpeProject
        {
            get { return _cbpeProject; }
            set
            {
                _cbpeProject = value;
                OnPropertyChanged("CbpeProject");
            }
        }

        public PieceBlock SelectedPieceBlock
        {
            get { return _selectedPieceBlock; }
            set
            {
                _selectedPieceBlock = value;
                OnPropertyChanged("SelectedPieceBlock");
                SetSelectedDragBlock(_selectedPieceBlock);
                HighLightPieceBlocks(_selectedPieceBlock);
            }
        }

        public bool IsCollisionDetectionEnabled
        {
            get { return _isCollisionDetectionEnabled; }
            set
            {
                _isCollisionDetectionEnabled = value;
                OnPropertyChanged("IsCollisionDetectionEnabled");
            }
        }

        public bool Is2DProjectVisible
        {
            get { return _is2DProjectVisible; }
            set
            {
                _is2DProjectVisible = value;
                OnPropertyChanged("Is2DProjectVisible");
            }
        }

        public RenderMasterViewModel RenderViewModel
        {
            get { return _renderViewModel; }
            set
            {
                _renderViewModel = value;
                OnPropertyChanged("RenderViewModel");
            }
        }

        public double ProjectWindowSize
        {
            get { return _projectWindowSize; }
            set
            {
                _projectWindowSize = value;
                OnPropertyChanged("ProjectWindowSize");
            }
        }

        public Timer RenderRefreshTimer
        {
            get { return _renderRefreshTimer; }
        }

        #region >-------------- Public Properties - Commands



        #endregion Public Properties - Commands --------<

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public ProgrammingSlabViewModel GetViewModelFromCbpe(CBPExtension cbpe)
        {
            if (MainSlabModel != null && MainSlabModel.Cbpe == cbpe)
            {
                return MainSlabModel;
            }
            var psvm = SlabViewModels.FirstOrDefault(p => p.Cbpe == cbpe);
            if (psvm != null)
            {
                return psvm;
            }
            return null;
        }

        public void UnSelectAll()
        {
            CurrentBlocks.Clear();

            if (MainSlabModel != null)
            {
                MainSlabModel.UnSelectAll();
            }
            foreach (var vm in SlabViewModels)
            {
                vm.UnSelectAll();
            }
            if (_projectViewModel != null)
            {
                _projectViewModel.UnSelectAll();
            }
        }


        public void DelayedRefreshRender(CBPExtension cbpe, List<Block> blocks, bool useThread)
        {
            RenderRefreshTimer.Stop();

            RenderRefreshCbpExtension = cbpe;
            RenderRefreshBlocks = blocks;
            RenderRefreshUseThread = useThread;

            RenderRefreshTimer.Start();
        }


        public void SetMainModel(ProgrammingSlabViewModel slabModel)
        {
            int currentIndex = -1;
            for (int i = 0; i < SlabViewModels.Count(); i++)
            {
                var model = SlabViewModels.ElementAt(i);

                if (model == slabModel)
                {
                    currentIndex = i;
                    break;
                }
            }
            if (currentIndex != -1)
            {
                SlabViewModels.Remove(slabModel);

                if (MainSlabModel != null)
                {
                    MainSlabModel.IsMainModel = false;
                    SlabViewModels.Add(MainSlabModel);
                    SlabViewModels.Move(SlabViewModels.Count() - 1, currentIndex);
                }

                MainSlabModel = slabModel;
            }
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void HighLightPieceBlocks(PieceBlock pieceBlock)
        {
            UnSelectAll();

            if (pieceBlock == null) return;

            string pieceCode = pieceBlock.Piece.OrderDetail.Code;
            Block block = CbpeProject.GetBlockByCode(pieceCode);
            if (block != null)
            {
                CbpeProject.SelectBlock(block);
            }
            if (MainSlabModel != null)
            {
                block = MainSlabModel.Cbpe.GetBlockByCode(pieceCode);
                if (block != null)
                {
                    MainSlabModel.Cbpe.SelectBlock(block);
                    return;
                }
            }
            foreach (var psvm in SlabViewModels)
            {
                block = psvm.Cbpe.GetBlockByCode(pieceCode);
                if (block != null)
                {
                    psvm.Cbpe.SelectBlock(block);
                    return;
                }
            }

        }

        private void SetSelectedDragBlock(PieceBlock selectedPieceBlock)
        {
            if (CbpeSource != CbpeProject) return;

            if (selectedPieceBlock == null)
            {
                CbpeSource.BlockDragDrop = null;
                return;
            }

            if (!selectedPieceBlock.IsAssignedToSLab)
            {
                CbpeSource.BlockDragDrop = new Block(CbpeSource.GetBlockByCode(selectedPieceBlock.Piece.OrderDetail.Code));
            }
            else
            {
                CbpeSource.BlockDragDrop = null;
            }
        }

        private void GetCurrentOrderData()
        {
            ClearOrderData();

            if (_currentOrder == null)
            {
                return;
            }
            CurrentOrder.GetAvailableSlabs();

            CurrentOrder.GetProjects();

            if (CurrentOrder.Projects.Count > 0)
            {
                CurrentProject = _currentOrder.Projects[0];
                CurrentProject.GetDetails();
                foreach (var detail in CurrentProject.Details)
                {
                    if (detail.OrderDetailPiece == null)
                        detail.OrderDetailPiece = new WkOrderDetailPieceEntity()
                        {
                            Scope = detail.Scope,
                            OrderDetail = detail
                        };
                    var piece = detail.OrderDetailPiece;

                    //ProjectPieces.Add(piece);
                    ProjectPieceBlocks.Add(new PieceBlock(piece));
                }

                if (_projectViewModel == null)
                {
                    _projectViewModel = new ProjectViewModel(this);
                    CbpeProject = _projectViewModel.Cbpe;
                }
                if (ProjectViewModel != null)
                {

                    foreach (var pb in ProjectPieceBlocks)
                        ProjectViewModel.SlabPieces.Add(pb);

                    // Build rendering structures
                    ProjectViewModel.RenderViewModel = new ProjectRenderViewModel(this);
                    ProjectViewModel.RenderViewModel.PreviewRender();

                }



            }
            else
            {
                CurrentProject = null;
            }

            foreach (var availableSlab in CurrentOrder.AvailableSlabs)
            {
                var newSlabVm = new ProgrammingSlabViewModel(this);
                newSlabVm.Slab = availableSlab.Slab;

                var slabPieces = ProjectPieceBlocks.Where(p => p.Piece.SlabId == availableSlab.Id);
                newSlabVm.SlabPieces.Clear();
                foreach(var piece in slabPieces)
                    newSlabVm.SlabPieces.Add(piece);

                SlabViewModels.Add(newSlabVm);

            }

            // Create graphic items
            //CreateGraphicPieces();

            // Create graphic slabs
            CreateGraphicSlabs();

        }

        private bool CreateGraphicSlabs()
        {
            foreach (var wkSlab in CurrentOrder.AvailableSlabs)
            {
                var graphicSlab = new GraphicSlab(wkSlab.Slab);
                double length;
                double height;
                double origX;
                double origY;

                graphicSlab.GetSlabImagePosition(out length, out height, out origX, out origY);

            }
            return true;
            //var op = CurrentOrder;

            //SlabCollection slabs = new SlabCollection(mDbInterface);

            //SlabEntity slb;

            //CBPExtension cbpe;
            //OpSlab opslb;
            //double length, height, origX, origY;

            //var mProgrammingSlabs = new List<ProgrammingSlab>();

            ////cbpMain.Reset();
            ////InitCBPELayer(cbpMain);
            ////cbpOpt1.Reset();
            ////InitCBPELayer(cbpOpt1);
            ////cbpOpt2.Reset();
            ////InitCBPELayer(cbpOpt2);
            ////cbpOpt3.Reset();
            ////InitCBPELayer(cbpOpt3);


            //try
            //{
            //    ////op.gOpSlabs.MoveFirst();
            //    ////while (!op.gOpSlabs.IsEOF())

            //    foreach(var slabCode in CurrentOrder.AvailableSlabs)
            //    {

            //        slb = ApplicationRun.Instance.DataSources.Slabs.GetById(slabCode.SlabId);

            //        ////op.gOpSlabs.GetObject(out opslb);
            //        ////slabs.GetSingleElementFromDb(opslb.gIdArticle);
            //        ////slabs.GetObject(out slb);

            //        ProgrammingSlab ps = new ProgrammingSlab();
            //        ps.idSlab = slb.Id;

            //        ps.idOpSlab = slabCode.Id;

            //        ps.code = slb.Code;

            //        // De Conti 05/05/2016
            //        ////OnLoadProgressStep(slb.gCode);

            //        var img = slb.SlabImage.ImagePath;
            //        var xml = slb.BoundaryImageFilepath;

            //        var graphicSlab = new GraphicSlab(slb);

            //        if (!System.IO.File.Exists(tmpFormPath + "\\" + slb.gCode + ".jpg"))
            //            slabs.GetImageFromDb(slb, tmpFormPath + "\\" + slb.gCode + ".jpg");

            //        if (!System.IO.File.Exists(tmpFormPath + "\\" + slb.gCode + ".xml"))
            //            slabs.GetXmlParameterFromDb(slb, tmpFormPath + "\\" + slb.gCode + ".xml");

            //        if (!System.IO.File.Exists(tmpFormPath + "\\" + slb.gCode + ".ini"))
            //            slb.ReadFileXml(tmpFormPath + "\\" + slb.gCode + ".xml", "", "", tmpFormPath + "\\" + slb.gCode + ".ini");


            //        graphicSlab.GetImageParam(graphicSlab.SlabImage.ImagePath, tmpFormPath + "\\" + slb.gCode + ".ini", out length, out height, out origX, out origY);

            //        ps.dimX = length;
            //        ps.dimY = height;
            //        ps.originX = origX;
            //        ps.originY = origY;
            //        ps.currentBlock = -1;
            //        //ps.cut = new CutImage(new Bitmap(tmpFormPath + "\\" + slb.gCode + ".jpg"));

            //        //TODO:CutImage
            //        ////using (var bitmap = Image.FromFile(tmpFormPath + "\\" + slb.gCode + ".jpg"))
            //        ////{
            //        ////    ps.cut = new CutImage((Bitmap)bitmap);
            //        ////}

            //        ps.cut = new CutImage(tmpFormPath + "\\" + slb.gCode + ".jpg");

            //        ps.partition = null;

            //        cbpe = GetFreeCBPE();

            //        if (cbpe == null)
            //        {
            //            cbpe = new CBPExtension(Breton2DViewerTmp);

            //            // Inserisce l'immagine della lastra
            //            InsertSlab(cbpe, ps, false);
            //        }
            //        else
            //        {
            //            // Inserisce l'immagine della lastra
            //            InsertSlab(cbpe, ps);
            //        }

            //        TryZoomAll(cbpe);
            //        //if  (_isFullyVisible)
            //        //    cbpe.ZoomAll();

            //        ps.cbpe = cbpe;
            //        mProgrammingSlabs.Add(ps);

            //        //if (_isFullyVisible)
            //        //    cbpe.ZoomAll();
            //        TryZoomAll(cbpe);

            //        op.gOpSlabs.MoveNext();
            //    }

            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    TraceLog.WriteLine("SlabOperations.LoadSlabs", ex);
            //    return false;
            //}
        }

        private void CreateGraphicPieces()
        {
            //    Breton.OptiMasterInterface.Shape shp;
            //    ShapeCollection shapes;

            //    var shapesDU = new  Shape[ProjectPieces.Count];
            //    List<Block> blocks = new List<Block>();

            //    int i = 0;
            //    foreach (var piece in ProjectPieces)
            //    {
            //        #region Shape (RENDER)
            //        Breton.DesignUtil.Shape shape = new Breton.DesignUtil.Shape();

            //        XmlAnalizer.ReadShapeXml(piece.XmlFilepath, ref shape, true);
            //        shapesDU[i] = shape;
            //        i++;
            //        #endregion

            //        #region Block
            //        Block b = new Block();
            //        double x, y, a;

            //        RTMatrix rtm = XmlAnalizer.GetMatrice(piece.XmlFilepath);
            //        rtm.GetRotoTransl(out x, out y, out a);

            //        shape = new Breton.DesignUtil.Shape();
            //        XmlAnalizer.ReadShapeXml(piece.XmlFilepath, ref shape, false);

            //        #region Pezzo Finito (Layer SHAPE e SHAPE_HOLE)
            //        Path ext = new Path();
            //        List<Path> holes = new List<Path>();
            //        List<Path> pockets = new List<Path>();
            //        List<Path> grooves = new List<Path>();
            //        int lastHoleId = -1;
            //        int lastPocketId = -1;
            //        int lastGrooveId = -1;

            //        for (int j = 0; j < shape.gContours.Count; j++)
            //        {
            //            Breton.DesignUtil.Shape.Contour c = (Breton.DesignUtil.Shape.Contour)shape.gContours[j];

            //            if (c.pricipalStruct)
            //            {
            //                ext.Layer = CBPExtension.LayersType.SHAPE.ToString();

            //                if (c.line)
            //                    ext.AddSide(new Segment(c.x0, c.y0, c.x1, c.y1));
            //                else
            //                {
            //                    double alpha = MathUtil.gdRadianti(c.startAngle);
            //                    double beta = MathUtil.gdRadianti(c.endAngle);
            //                    double amp = beta - alpha;
            //                    if (c.cw == 0)                                          // antiorario
            //                        amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                    else
            //                        amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                    Arc aTmp = new Arc(c.xC, c.yC, c.r, alpha, amp);
            //                    ext.AddSide(aTmp);
            //                }
            //            }
            //            else if (c.throughHole)
            //            {
            //                if (c.id != lastHoleId)
            //                {
            //                    lastHoleId = c.id;
            //                    holes.Add(new Path());
            //                }

            //                holes[holes.Count - 1].Layer = CBPExtension.LayersType.SHAPE_HOLE.ToString();

            //                if (c.line)
            //                    holes[holes.Count - 1].AddSide(new Segment(c.x0, c.y0, c.x1, c.y1));
            //                else
            //                {
            //                    double alpha = MathUtil.gdRadianti(c.startAngle);
            //                    double beta = MathUtil.gdRadianti(c.endAngle);
            //                    double amp = beta - alpha;
            //                    if (c.cw == 0)                                          // antiorario
            //                        amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                    else
            //                        amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                    Arc aTmp = new Arc(c.xC, c.yC, c.r, alpha, amp);

            //                    holes[holes.Count - 1].AddSide(aTmp);
            //                }
            //            }
            //            else if (c.pocket)
            //            {
            //                if (c.id != lastPocketId)
            //                {
            //                    lastPocketId = c.id;
            //                    pockets.Add(new Path());
            //                }

            //                pockets[pockets.Count - 1].Layer = CBPExtension.LayersType.POCKET.ToString();

            //                if (c.line)
            //                    pockets[pockets.Count - 1].AddSide(new Segment(c.x0, c.y0, c.x1, c.y1));
            //                else
            //                {
            //                    double alpha = MathUtil.gdRadianti(c.startAngle);
            //                    double beta = MathUtil.gdRadianti(c.endAngle);
            //                    double amp = beta - alpha;
            //                    if (c.cw == 0)                                          // antiorario
            //                        amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                    else
            //                        amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                    Arc aTmp = new Arc(c.xC, c.yC, c.r, alpha, amp);

            //                    pockets[pockets.Count - 1].AddSide(aTmp);
            //                }
            //            }
            //            else if (c.groove)
            //            {
            //                if (c.id != lastGrooveId)
            //                {
            //                    lastGrooveId = c.id;
            //                    grooves.Add(new Path());
            //                }

            //                grooves[grooves.Count - 1].Layer = CBPExtension.LayersType.GROOVE.ToString();

            //                if (c.line)
            //                    grooves[grooves.Count - 1].AddSide(new Segment(c.x0, c.y0, c.x1, c.y1));
            //                else
            //                {
            //                    double alpha = MathUtil.gdRadianti(c.startAngle);
            //                    double beta = MathUtil.gdRadianti(c.endAngle);
            //                    double amp = beta - alpha;
            //                    if (c.cw == 0)                                          // antiorario
            //                        amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                    else
            //                        amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                    Arc aTmp = new Arc(c.xC, c.yC, c.r, alpha, amp);

            //                    grooves[grooves.Count - 1].AddSide(aTmp);
            //                }
            //            }
            //        }

            //        b.AddPath(ext);

            //        foreach (Path h in holes)
            //            b.AddPath(h);

            //        foreach (Path g in grooves)
            //            b.AddPath(g);

            //        foreach (Path p in pockets)
            //            b.AddPath(p);
            //        #endregion

            //        #region Bussole
            //        Path blindHole;
            //        for (int j = 0; j < shape.gBussole.Count; j++)
            //        {
            //            blindHole = new Path();
            //            Breton.DesignUtil.Shape.Bussola bus = (Breton.DesignUtil.Shape.Bussola)shape.gBussole[j];

            //            for (int k = 0; k < bus.sides.Count; k++)
            //            {
            //                Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)bus.sides[k];

            //                blindHole.Layer = CBPExtension.LayersType.BLIND_HOLE.ToString();

            //                if (s.line)
            //                    blindHole.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
            //                else
            //                {
            //                    double alpha = MathUtil.gdRadianti(s.startAngle);
            //                    double beta = MathUtil.gdRadianti(s.endAngle);
            //                    double amp = beta - alpha;
            //                    if (s.cw == 0)                                          // antiorario
            //                        amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                    else
            //                        amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                    Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

            //                    blindHole.AddSide(aTmp);
            //                }
            //            }

            //            b.AddPath(blindHole);
            //        }
            //        #endregion

            //        ////shapes.GetDataFromDb(Breton.OptiMasterInterface.Shape.Keys.F_NONE, -1, -1, op.gId, -1, det.gCode, true);
            //        ////shapes.GetObject(out shp);

            //        var shp = piece.OrderDetail;

            //        #region	Grezzo di taglio (Layer RAW e RAW_HOLE) e Tecno path
            //        Path cut = new Path();
            //        Path holeCut;

            //        // Se il pezzo non è ancora stato piazzato sulla lastra, lancia il calcolo dei percorsi 
            //        // altrimenti legge i percorsi dal database

            //        TecnoPaths tecnoPaths = new TecnoPaths();

            //        DetailOpti dop = GetDetailOpti(det.gArticleCode);

            //        int idSide = -1;
            //        shape = new Breton.DesignUtil.Shape();

            //        if (shp != null)
            //        {

            //            //shapes.GetXmlPolygonFromDb(shp, tmpFormPath + "\\WORKPHASE" + shp.gIdWorkPhase + ".xml");
            //            //XmlAnalizer.ReadShapeXml(tmpFormPath + "\\WORKPHASE" + shp.gIdWorkPhase + ".xml", ref shape, false);


            //            shapes.GetXmlPolygonFromDb(shp, tmpFormPath + "\\WORKPHASE" + shp.gIdWorkPhase + ".xml");
            //            XmlAnalizer.ReadShapeXml(tmpFormPath + "\\WORKPHASE" + shp.gIdWorkPhase + ".xml", ref shape, false);

            //            // Scorre i grezzi per cercare quello attivo
            //            for (int j = 0; j < shape.gCutToSize.Count; j++)
            //            {
            //                Breton.DesignUtil.Shape.Cut c = (Breton.DesignUtil.Shape.Cut)shape.gCutToSize[j];

            //                if (c.active)
            //                {
            //                    TecnoPath tp = new TecnoPath();
            //                    for (int k = 0; k < c.sides.Count; k++)
            //                    {
            //                        Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)c.sides[k];

            //                        cut.Layer = DesignCenterInterface.CBPExtension.LayersType.RAW.ToString();

            //                        if (s.line)
            //                            idSide = cut.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
            //                        else
            //                        {
            //                            double alpha = MathUtil.gdRadianti(s.startAngle);
            //                            double beta = MathUtil.gdRadianti(s.endAngle);
            //                            double amp = beta - alpha;
            //                            if (s.cw == 0)                                          // antiorario
            //                                amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                            else
            //                                amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                            Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

            //                            idSide = cut.AddSide(aTmp);
            //                        }

            //                        if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //                        {
            //                            int ids = tp.AddSide(cut.GetSide(idSide - 1));

            //                            TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
            //                            tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
            //                            tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
            //                            tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
            //                            tsi.TecnoPathInfo.MaterialThickness = det.gThickness;
            //                            tsi.Slope = MathUtil.gdRadianti(s.slope);
            //                            if (tp.GetSide(ids - 1) is TecnoArc)
            //                            {
            //                                TecnoArc ta = (TecnoArc)tp.GetSide(ids - 1);
            //                                ta.CoordError = mCordErr;
            //                            }
            //                        }
            //                    }
            //                    // Aggiornamento 2015_10_24: Non viene più eseguita la semplificazione della geometria perchè così facendo venivano eliminati ed esclusi
            //                    // tagli inclinati parziali
            //                    // Cerca di semplificare il grezzo di taglio nel caso ci siano lati allineati
            //                    //cut.Simplify(0d);
            //                    //tp.Simplify(0d);

            //                    if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //                    {
            //                        tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
            //                        // Indica se il tipo di percorso è esterno o se è un foro
            //                        tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_EXTERIOR;
            //                        tecnoPaths.AddPath(tp);
            //                    }
            //                    if (!cut.IsPathFullyClosed(MathUtil.QUOTE_EPSILON))
            //                    {
            //                        ProjResource.gResource.LoadMessageBox(this, 20, out caption, out message);
            //                        MessageBox.Show(message + ": " + det.gCode + "(" + det.gArticleCode + ")", caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

            //                        dop.state = DetailOptiState.PIECE_OPEN;
            //                        UpdateGetDetailOpti(dop);
            //                    }

            //                    b.AddPath(cut);
            //                    break;
            //                }
            //            }
            //            int activeHoleIndex = 0;
            //            // Scorre i fori e aggiunge quelli attivi
            //            for (int j = 0; j < shape.gHolesToCut.Count; j++)
            //            {
            //                holeCut = new Path();
            //                Breton.DesignUtil.Shape.Holes h = (Breton.DesignUtil.Shape.Holes)shape.gHolesToCut[j];

            //                if (h.active)
            //                {
            //                    activeHoleIndex++;

            //                    TecnoPath tp = new TecnoPath();
            //                    for (int k = 0; k < h.sides.Count; k++)
            //                    {
            //                        Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)h.sides[k];

            //                        holeCut.Layer = DesignCenterInterface.CBPExtension.LayersType.RAW_HOLE.ToString();
            //                        holeCut.Name = "HOLE" + activeHoleIndex;

            //                        if (s.line)
            //                            idSide = holeCut.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
            //                        else
            //                        {
            //                            double alpha = MathUtil.gdRadianti(s.startAngle);
            //                            double beta = MathUtil.gdRadianti(s.endAngle);
            //                            double amp = beta - alpha;
            //                            if (s.cw == 0)                                          // antiorario
            //                                amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                            else
            //                                amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                            Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

            //                            idSide = holeCut.AddSide(aTmp);
            //                        }

            //                        if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //                        {
            //                            int ids = tp.AddSide(holeCut.GetSide(idSide - 1));

            //                            TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
            //                            tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
            //                            tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
            //                            tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
            //                            tsi.TecnoPathInfo.MaterialThickness = det.gThickness;
            //                            tsi.Slope = MathUtil.gdRadianti(s.slope);
            //                        }
            //                    }
            //                    if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //                    {
            //                        tp.Info.BridgeMode = h.isBridge;
            //                        tp.Attributes.Add("LINK_RAW_HOLE", holeCut.Name);

            //                        tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
            //                        // Indica se il tipo di percorso è esterno o se è un foro
            //                        tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_INTERIOR;
            //                        tecnoPaths.AddPath(tp);
            //                    }
            //                    if (!holeCut.IsPathFullyClosed(MathUtil.QUOTE_EPSILON))
            //                    {
            //                        ProjResource.gResource.LoadMessageBox(this, 20, out caption, out message);
            //                        MessageBox.Show(message + ": " + det.gCode + "(" + det.gArticleCode + ")", caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

            //                        dop.state = DetailOptiState.PIECE_OPEN;
            //                        UpdateGetDetailOpti(dop);
            //                    }
            //                    b.AddPath(holeCut);
            //                }
            //            }

            //            // Scorre le bussole e le aggiunge al tecno path
            //            for (int j = 0; j < shape.gBussole.Count; j++)
            //            {
            //                Breton.DesignUtil.Shape.Bussola bus = (Breton.DesignUtil.Shape.Bussola)shape.gBussole[j];

            //                TecnoPath tp = new TecnoPath();
            //                for (int k = 0; k < bus.sides.Count; k++)
            //                {
            //                    Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)bus.sides[k];

            //                    if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //                    {
            //                        int ids;
            //                        if (s.line)
            //                            ids = tp.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
            //                        else
            //                        {
            //                            double alpha = MathUtil.gdRadianti(s.startAngle);
            //                            double beta = MathUtil.gdRadianti(s.endAngle);
            //                            double amp = beta - alpha;
            //                            if (s.cw == 0)                                          // antiorario
            //                                amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                            else
            //                                amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                            Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

            //                            ids = tp.AddSide(aTmp);
            //                        }

            //                        TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
            //                        tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
            //                        tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
            //                        tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
            //                        tsi.TecnoPathInfo.MaterialThickness = det.gThickness;
            //                        tsi.Slope = MathUtil.gdRadianti(s.slope);
            //                    }
            //                }
            //                if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //                {
            //                    tp.Info.BlindHole = true;

            //                    tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
            //                    // Indica se il tipo di percorso è esterno o se è un foro
            //                    tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_INTERIOR;
            //                    tecnoPaths.AddPath(tp);
            //                }
            //            }

            //        }
            //        #endregion

            //        #region Simboli esterni
            //        for (int j = 0; j < shape.gSymbols.Count; j++)
            //        {
            //            Path sym = new Path();
            //            Breton.DesignUtil.Shape.Symbol sy = (Breton.DesignUtil.Shape.Symbol)shape.gSymbols[j];

            //            if (symbolsToImport.IndexOf(sy.type.ToUpper()) >= 0)
            //            {
            //                TecnoPath tp = new TecnoPath();
            //                for (int k = 0; k < sy.sides.Count; k++)
            //                {
            //                    Breton.DesignUtil.Shape.Contour c = (Breton.DesignUtil.Shape.Contour)sy.sides[k];

            //                    sym.Layer = DesignCenterInterface.CBPExtension.LayersType.SYMBOL.ToString();

            //                    if (c.line)
            //                        idSide = sym.AddSide(new Segment(c.x0, c.y0, c.x1, c.y1));
            //                    else
            //                    {
            //                        double alpha = MathUtil.gdRadianti(c.startAngle);
            //                        double beta = MathUtil.gdRadianti(c.endAngle);
            //                        double amp = beta - alpha;
            //                        if (c.cw == 0)                                          // antiorario
            //                            amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            //                        else
            //                            amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            //                        Arc aTmp = new Arc(c.xC, c.yC, c.r, alpha, amp);

            //                        idSide = sym.AddSide(aTmp);
            //                    }

            //                    tp.AddSide(sym.GetSide(idSide - 1));

            //                }

            //                if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //                {
            //                    tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
            //                    // Indica se è un percorso che non deve essere tagliato
            //                    tp.Info.NoCutPolygon = true;

            //                    tecnoPaths.AddPath(tp);
            //                }
            //            }

            //            b.AddPath(sym);
            //        }
            //        #endregion

            //        #region Calcolo per il RENDER
            //        // costruisco il percorso che stà attorno al blocco considerando il rettangolo RT
            //        // il rettangolo da considerare è quello intorno al percorso della shape, altrimenti può essere
            //        // troppo grande se considero il blocco (ci sono anche gli altri percorsi)
            //        Path pTmp = new Path(ext);
            //        pTmp.MatrixRT = new RTMatrix(x, y, a);
            //        Path pRect = new Path();
            //        if (pTmp != null)
            //            pRect = new Path(pTmp.GetRectangleRT());

            //        pRect.Layer = CBPExtension.LayersType.RENDER_PATH.ToString();

            //        // applico al percorso del rettangolo circoscritto la matrice inversa per riportarlo nell'origine
            //        RTMatrix mat = new RTMatrix(x, y, a);
            //        mat = mat.InvMat();

            //        // trasformo i vertici del rettangolo
            //        double px, py, pxt, pyt;
            //        List<Breton.Polygons.Point> pointTransform = new List<Breton.Polygons.Point>();
            //        Breton.Polygons.Point p1 = pRect.GetVertexRT(0, out px, out py);
            //        mat.PointTransform(px, py, out pxt, out pyt);
            //        pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            //        p1 = pRect.GetVertexRT(1, out px, out py);
            //        mat.PointTransform(px, py, out pxt, out pyt);
            //        pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            //        p1 = pRect.GetVertexRT(2, out px, out py);
            //        mat.PointTransform(px, py, out pxt, out pyt);
            //        pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            //        p1 = pRect.GetVertexRT(3, out px, out py);
            //        mat.PointTransform(px, py, out pxt, out pyt);
            //        pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            //        // con questi vertici faccio le side per il path riferito allo 0,0
            //        Path pRectNew = new Path();
            //        Side sd = new Segment(pointTransform[0], pointTransform[1]);
            //        pRectNew.AddSide(sd);
            //        sd = new Segment(pointTransform[1], pointTransform[2]);
            //        pRectNew.AddSide(sd);
            //        sd = new Segment(pointTransform[2], pointTransform[3]);
            //        pRectNew.AddSide(sd);
            //        sd = new Segment(pointTransform[3], pointTransform[0]);
            //        pRectNew.AddSide(sd);

            //        pRectNew.Layer = CBPExtension.LayersType.RENDER_PATH.ToString();

            //        // aggiungo il rettangolo al blocco
            //        b.AddPath(pRectNew);

            //        #endregion

            //        #region Calcolo dei layer necessari per le collisioni

            //        PathCollection paths;
            //        Path intZone = new Path(ext);
            //        ZoneOffset.PathOffset(ref intZone, -300, ZoneOffset.EXPANSION_MODE.EXPANSION_MODE_RECT, out paths);
            //        intZone.Layer = CBPExtension.LayersType.RAW_INTEREST_ZONE.ToString();
            //        b.AddPath(intZone);

            //        #endregion

            //        #region Percorsi utensili di taglio
            //        // Se il pezzo non è ancora stato piazzato sulla lastra, lancia il calcolo dei percorsi 
            //        // altrimenti legge i percorsi dal database
            //        ToolPaths toolPaths = new ToolPaths();
            //        RemovalPaths removalPaths = new RemovalPaths();
            //        // Carica i tool path info (utensili)
            //        TecnoPathInfo tpi;
            //        ToolsLeadMode tslm;
            //        GetTecnoPathInfo(det.gThickness, out tpi, out tslm);

            //        if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            //        {
            //            // applicazione dei parametri specifici per ogni percorso
            //            for (int m = 0; m < tecnoPaths.Count; m++)
            //            {
            //                TecnoPath tp = tecnoPaths[m];
            //                if (tp.Info.NoCutPolygon)
            //                    continue;

            //                if (tp.Info.AvailableTools.Count == 0)
            //                {
            //                    TecnoPathInfo tmptpInf = new TecnoPathInfo(tpi);
            //                    tmptpInf.BridgeMode = tp.Info.BridgeMode;
            //                    tmptpInf.BlindHole = tp.Info.BlindHole;
            //                    tp.Info = tmptpInf;
            //                }
            //                tp.Info.ToolsLead = tslm;

            //                tp.PropagatesTecnoPathInfo(false);
            //            }

            //            if (mCam.CreateStandardToolPaths(tecnoPaths, out toolPaths, tpi, det.gArticleCode))
            //                tecnoPaths = mCam.OriginalGeometries;

            //            // Scorre i TecnoPaths creati e li aggiunge alla lista di path
            //            for (int j = 0; j < tecnoPaths.Count; j++)
            //                b.AddPath(tecnoPaths[j]);

            //            // Scorre i risultati ottenuti per inserirli nel blocco
            //            for (int j = 0; j < toolPaths.Count; j++)
            //            {
            //                ToolPath tp = (ToolPath)toolPaths.GetPath(j);

            //                if (((IToolPathSide)tp.GetSide(0)).Info.Slope > 0)
            //                    tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS.ToString();
            //                else if (((IToolPathSide)tp.GetSide(0)).Info.Slope < 0)
            //                    tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG.ToString();
            //                else
            //                    tp.Layer = CBPExtension.LayersType.FINAL_TOOL.ToString();

            //                b.AddPath(tp);
            //            }

            //            for (int j = 0; j < mCam.RemovalPaths.Count; j++)
            //            {
            //                RemovalPath rp = (RemovalPath)mCam.RemovalPaths.GetPath(j);
            //                if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope > 0)
            //                    rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS_REM.ToString();
            //                else if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope < 0)
            //                    rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG_REM.ToString();
            //                else
            //                    rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();

            //                b.AddPath(rp);
            //            }
            //        }
            //        else if (dop.state == DetailOptiState.AVAILABLE_ASSIGNED)
            //        {
            //            // Cerca i percorsi utensili salvati
            //            Piece pie;
            //            GroupOpCollection tmpgroups = new GroupOpCollection(mDbInterface);
            //            PieceOpCollection tmppieces = new PieceOpCollection(mDbInterface);
            //            tmpgroups.GetDataFromDb(Group.Keys.F_NONE, null, null, new int[] { mCurrentOP.gId });
            //            tmpgroups.MoveFirst();
            //            while (!tmpgroups.IsEOF())
            //            {
            //                tmpgroups.GetObject(out gr);

            //                if (gr != null && shp != null)
            //                {
            //                    tmppieces.GetDataFromDb(Piece.Keys.F_NONE, null, new int[] { gr.gId }, new int[] { shp.gIdOpShape }, null);
            //                    tmppieces.GetObject(out pie);
            //                    if (pie != null)
            //                    {
            //                        tmppieces.GetXmlPolygonFromDb(pie, tmpFormPath + "\\" + dop.piefCode + "_in.xml");

            //                        XmlDocument doc = new XmlDocument();
            //                        doc.Load(tmpFormPath + "\\" + dop.piefCode + "_in.xml");
            //                        XmlNode root = doc.SelectSingleNode("EXPORT");

            //                        // Legge il file xml
            //                        foreach (XmlNode nd in root.ChildNodes)
            //                        {
            //                            string ot = PathCollection.GetOriginalTypeFromXML(nd);

            //                            if (ot == "TecnoPaths")
            //                                tecnoPaths.ReadFileXml(nd);
            //                            else if (ot == "ToolPaths")
            //                                toolPaths.ReadFileXml(nd);
            //                            else if (ot == "RemovalPaths")
            //                                removalPaths.ReadFileXml(nd);
            //                        }

            //                        bool diffTools = false;

            //                        // Se il pezzo non fa parte di una lastra bloccata effettua la verifica 
            //                        if (!lockedPieces.Contains(pie.gIdShape))
            //                        {
            //                            // *****************************************************************************
            //                            // Verifica se è necessario rigenerare i percorsi anche per i pezzi già piazzati

            //                            // Recupera gli utensili usati per questo pezzo e verifica se sono uguali
            //                            if (tecnoPaths[0].Info.AvailableTools.Count != tpi.AvailableTools.Count)
            //                                diffTools = true;
            //                            else
            //                            {
            //                                for (int j = 0; j < tecnoPaths[0].Info.AvailableTools.Count; j++)
            //                                {
            //                                    for (int k = 0; k < tpi.AvailableTools.Count; k++)
            //                                    {
            //                                        if (tecnoPaths[0].Info.AvailableTools[j].Name == tpi.AvailableTools[k].Name)
            //                                        {
            //                                            if (tecnoPaths[0].Info.AvailableTools[j] != tpi.AvailableTools[k])
            //                                            {
            //                                                diffTools = true;
            //                                                break;
            //                                            }
            //                                        }
            //                                    }
            //                                    if (diffTools)
            //                                        break;
            //                                }
            //                            }


            //                            if (diffTools && mReloadToolpathMessage != 2)
            //                            {
            //                                ProjResource.gResource.LoadMessageBox(this, 21, out caption, out message);

            //                                if (mReloadToolpathMessage == 0)
            //                                    if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            //                                        mReloadToolpathMessage = 1;
            //                                    else
            //                                        mReloadToolpathMessage = 2;

            //                                if (mReloadToolpathMessage == 1)
            //                                {
            //                                    mReloadToolpathMessage = 1;
            //                                    toolPaths = new ToolPaths();
            //                                    removalPaths = new RemovalPaths();

            //                                    // applicazione dei parametri specifici per ogni percorso
            //                                    for (int m = 0; m < tecnoPaths.Count; m++)
            //                                    {
            //                                        TecnoPath tp = tecnoPaths[m];
            //                                        if (tp.Info.NoCutPolygon)
            //                                            continue;


            //                                        TecnoPathInfo tmptpInf = new TecnoPathInfo(tpi);
            //                                        tmptpInf.BridgeMode = tp.Info.BridgeMode;
            //                                        tmptpInf.BlindHole = tp.Info.BlindHole;
            //                                        tp.Info = tmptpInf;

            //                                        tp.Info.ToolsLead = tslm;
            //                                        tp.PropagatesTecnoPathInfo(true);
            //                                    }


            //                                    if (mCam.CreateStandardToolPaths(tecnoPaths, out toolPaths, tpi, det.gArticleCode))
            //                                        tecnoPaths = mCam.OriginalGeometries;


            //                                    // *********************************************

            //                                    // Scorre i TecnoPaths creati e li aggiunge alla lista di path
            //                                    for (int j = 0; j < tecnoPaths.Count; j++)
            //                                        b.AddPath(tecnoPaths[j]);

            //                                    // Scorre i risultati ottenuti per inserirli nel blocco
            //                                    for (int j = 0; j < toolPaths.Count; j++)
            //                                    {
            //                                        ToolPath tp = (ToolPath)toolPaths.GetPath(j);

            //                                        if (((IToolPathSide)tp.GetSide(0)).Info.Slope > 0)
            //                                            tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS.ToString();
            //                                        else if (((IToolPathSide)tp.GetSide(0)).Info.Slope < 0)
            //                                            tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG.ToString();
            //                                        else
            //                                            tp.Layer = CBPExtension.LayersType.FINAL_TOOL.ToString();

            //                                        b.AddPath(tp);
            //                                    }

            //                                    for (int j = 0; j < mCam.RemovalPaths.Count; j++)
            //                                    {
            //                                        RemovalPath rp = (RemovalPath)mCam.RemovalPaths.GetPath(j);
            //                                        if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope > 0)
            //                                            rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS_REM.ToString();
            //                                        else if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope < 0)
            //                                            rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG_REM.ToString();
            //                                        else
            //                                            rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();

            //                                        b.AddPath(rp);
            //                                    }

            //                                    break;
            //                                }
            //                            }
            //                        }

            //                        if (!diffTools || mReloadToolpathMessage == 2)
            //                        {
            //                            // Recupera le informazioni dei toolpaths
            //                            for (int j = 0; j < toolPaths.Count; j++)
            //                                toolPaths[j].RecoverLinkToTecnoPath(tecnoPaths);

            //                            // Aggiunge i tecno path letti
            //                            for (int j = 0; j < tecnoPaths.Count; j++)
            //                                b.AddPath(tecnoPaths.GetPath(j));

            //                            // Aggiunge i tool path letti
            //                            for (int j = 0; j < toolPaths.Count; j++)
            //                                b.AddPath(toolPaths.GetPath(j));

            //                            // Aggiunge le asportazioni lette
            //                            for (int j = 0; j < removalPaths.Count; j++)
            //                                b.AddPath(removalPaths.GetPath(j));

            //                            break;
            //                        }
            //                    }
            //                }

            //                tmpgroups.MoveNext();
            //            }
            //        }
            //        else if (dop.state == DetailOptiState.UNAVAILABLE || dop.state == DetailOptiState.PIECE_OPEN)
            //        {
            //            // Non fa niente
            //        }

            //        // Se il pezzo non aveva i percorsi utensile perchè risultato di un ottimizzazione automatica allora imposta il pezzo come già assegnato
            //        if (dop.state == DetailOptiState.NO_TOOLPATH)
            //        {
            //            dop.state = DetailOptiState.AVAILABLE_ASSIGNED;
            //            UpdateGetDetailOpti(dop);
            //        }
            //        #endregion

            //        b.Code = det.gArticleCode.Trim();
            //        b.Name = shape.gShapeNumber.ToString();
            //        // Imposta la rototraslazione per posizionare il pezzo all'interno del top
            //        b.SetRotoTransl(x, y, a);
            //        blocks.Add(b);
            //        #endregion
            //    }
        }

        private void ClearOrderData()
        {
//TODO:
            CurrentProject = null;
            //ProjectPieces.Clear();
            ProjectPieceBlocks.Clear();
            SlabViewModels.Clear();
        }

        private void SelectSlabs(object obj)
        {
            var actualSlabs = new List<string>();

            if (MainSlabModel != null && MainSlabModel.Slab != null)
                actualSlabs.Add(MainSlabModel.Slab.Code);

            foreach (var svm in SlabViewModels)
            {
                if (svm.Slab != null)
                    actualSlabs.Add(svm.Slab.Code);
            }

            //TODO: test
            var selectedSlabs = new List<string>()
            {
                "SLAB01000000019852",
                "SLAB01000000022301",
                "SLAB01000000099999"
                //"SLAB01000000021172",
                //"SLAB01000000021344",
                //"SLAB01000000021345",
                //"SLAB01000000021346",
                //"SLAB01000000023148",
                //"SLAB01000000022428",
                //"SLAB01000000023123"
            };

            UpdateSlabViewModelList(selectedSlabs, actualSlabs);

        }

        private void UpdateSlabViewModelList(List<string> selectedSlabs, List<string> actualSlabs)
        {
            var slabCodesToLoad = new List<string>();
            var slabCodesToRemove = new List<string>();


            foreach (var item in selectedSlabs)
            {
                if (!actualSlabs.Contains(item))
                {
                    slabCodesToLoad.Add(item);
                }
            }
            foreach (var actualSlab in actualSlabs)
            {
                if (!selectedSlabs.Contains(actualSlab))
                {
                    slabCodesToRemove.Add(actualSlab);
                }
            }

            #region removed

            // Remove existing and free pieces
            foreach (var removeCode in slabCodesToRemove)
            {
                if (MainSlabModel != null && MainSlabModel.Slab != null && MainSlabModel.Slab.Code == removeCode)
                {
                    MainSlabModel.SlabPieces.Clear();
                    MainSlabModel.Slab = null;
                    MainSlabModel.Terminate();
                    MainSlabModel = null;
                }
                else
                {
                    var svm = SlabViewModels.FirstOrDefault(s => s.Slab != null && s.Slab.Code == removeCode);
                    if (svm != null)
                    {
                        svm.SlabPieces.Clear();
                        svm.Slab = null;

                        SlabViewModels.Remove(svm);
                        svm = null;
                    }
                }
                var prjPiecesInvolved = ProjectPieceBlocks.Where(p => p.Piece.Slab.Code == removeCode).ToList();
                foreach (var pieceBlock in prjPiecesInvolved)
                {
                    pieceBlock.Piece.Slab = null;
                }
            }

            #endregion

            #region Added

            //FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("Code"), ConditionOperator.In, slabCodesToLoad);

            foreach (var slabCode in slabCodesToLoad)
            {
                var slab = ApplicationRun.Instance.DataSources.Slabs.GetByCode(slabCode);

                if (slab != null)
                {
                    var newPsvm = new ProgrammingSlabViewModel(this);
                    newPsvm.Slab = slab;

                    SlabViewModels.Add(newPsvm);
                }
            }

            //var newSlabs = new PersistableEntityCollection<SlabEntity>(ApplicationRun.Instance.DataScope);
            //if (newSlabs.GetItemsFromRepository(clause: filterClause))
            //{
            //    foreach (var slabEntity in newSlabs)
            //    {
            //        var newPsvm = new ProgrammingSlabViewModel(this);
            //        newPsvm.Slab = slabEntity;

            //        //var graphicSlab = new GraphicSlab(slabEntity);
            //        //double length;
            //        //double height;
            //        //double origX;
            //        //double origY;

            //        //graphicSlab.GetSlabImagePosition(out length, out height, out origX, out origY);



            //        SlabViewModels.Add(newPsvm);
            //    }
            //}

            #endregion

            if (MainSlabModel == null && SlabViewModels.Count > 0)
            {
                SlabViewModels[0].IsMainModel = true;
            }
        }

        private bool CanselectSlabs(object obj)
        {
            return (CurrentOrder != null);
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates

        void _slabViewModels_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in e.NewItems)
                    {
                        SlabViews.Add((item as ProgrammingSlabViewModel).View);
                        //SlabViews.Move(SlabViews.Count() -1, e.OldStartingIndex);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems)
                    {
                        SlabViews.Remove((item as ProgrammingSlabViewModel).View);
                    }
                    break;

                case NotifyCollectionChangedAction.Move:
                    foreach (var item in e.OldItems)
                    {
                        if (e.OldStartingIndex != e.NewStartingIndex)
                            SlabViews.Move(e.OldStartingIndex, e.NewStartingIndex);
                    }
                    break;
            }
        }

        #endregion Delegates  -----------<
    }

}
