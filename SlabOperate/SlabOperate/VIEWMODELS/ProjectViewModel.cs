﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Drawing;
using System.Linq;
using System.Windows;
using Breton.BCamPath;
using Breton.DesignCenterInterface;
using Breton.MathUtils;
using RenderMaster.Wpf.USERCONTROLS;
using SlabOperate.BUSINESS;
using SlabOperate.Business.Entities;
using SlabOperate.USERCONTROLS;
using TraceLoggers;
using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;

namespace SlabOperate.VIEWMODELS
{
    public class ProjectViewModel : ObservableObject
    {
        #region >-------------- Constants and Enums

        private const double THICKNESS_STEP = 10;

        private const bool mAllowSlopedSideLock = false;

        private const double mDistanceJoin = 10;

        private struct DeleteBlock
        {
            public CBPExtension cbpe;
            public Block deleteBlock;
        }


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private ProjectViewUc _view;

        private FksqlCbpe _cbpe;

        private OperationsViewModel _parentModel;

        private bool _isMainModel;

        private SlabEntity _slab;

        private ObservableCollection<PieceBlock> _slabPieces = new ObservableCollection<PieceBlock>();

        private Operation _currentOperation;

        private List<DeleteBlock> _blocksToBeDeleted = new List<DeleteBlock>();

        private List<int> _currentBlocks = new List<int>();

        private int _currentBlock;

        protected SortedList<int, Block> mBlocks;

        private Block mRightClickSelBlock;

        private TecnoSideInfo mRightClickTecnoSideInfo;
        private TecnoPath mRightClickTecnoPath;
        private int mRightClickSide;


        private bool _isControlKeyPressed;

        private bool _isDeletingBlock = false;

        private bool _canRemoveSlab = false;

        private bool _canResetSlab = false;

        private string _currentCoordinatesString = string.Empty;

        private ProjectRenderViewModel _renderViewModel;

        private bool _isDraggingMesh = false;

        private bool _isMovingMesh = false;

        private ProgrammingSlabViewModel _movingMeshSlabViewModel = null;

        private Block _movingMeshBlock = null;

        private PieceBlock _movingPieceBlock = null;

        #region Toggles

        private bool _showSlab;

        private bool _showDefects;

        private bool _showShapes;

        private bool _showRaw;

        private bool _showCutTool;

        private bool _showCutToolRemoval;

        private bool _showSymbol;

        private bool _showText;

        private bool _setOrtho;

        private bool _isMagnetEnabled;

        private bool _isCollisionEnabled;

        #endregion

        #region Handlers
        private CBasicPaint.MouseMoveAfterHandler evMouseMoveAfter;
        private CBasicPaintEventArgsHandler evClick;

        private CBasicPaintEventArgsHandler evStartMove;
        private CBasicPaintEventArgsHandler evMove;
        private CBasicPaintEventArgsHandler evEndMove;

        private CBasicPaintEventArgsHandler evStartRotate;
        private CBasicPaintEventArgsHandler evRotate;
        private CBasicPaintEventArgsHandler evEndRotate;

        private CBasicPaint.DragEnterHandler evDragEnter;
        private CBasicPaint.DragDropHandler evDragDrop;
        private CBasicPaint.DragLeaveHandler evDragLeave;
        #endregion


        #region >-------------- Private Fields - Commands


        #endregion Private Fields - Commands --------<

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ProjectViewModel(OperationsViewModel operationsViewModel)
        {
            _parentModel = operationsViewModel;
            _view = new ProjectViewUc();

            InitCpbeView();

            _view.DataContext = this;

            _slabPieces.CollectionChanged += _slabPieces_CollectionChanged;

            AttachRenderControlEvents();
        }

        private void AttachRenderControlEvents()
        {
            RenderMasterUc.MoveStarted += RenderMasterUc_MoveStarted;
            RenderMasterUc.Move += RenderMasterUc_Move;
            RenderMasterUc.MoveCompleted += RenderMasterUc_MoveCompleted;
            RenderMaster4WindowUc.MoveStarted += RenderMasterUc_MoveStarted;
            RenderMaster4WindowUc.Move += RenderMasterUc_Move;
            RenderMaster4WindowUc.MoveCompleted += RenderMasterUc_MoveCompleted;
        }

        public void DetachRenderControlEvents()
        {
            RenderMasterUc.MoveStarted -= RenderMasterUc_MoveStarted;
            RenderMasterUc.Move -= RenderMasterUc_Move;
            RenderMasterUc.MoveCompleted -= RenderMasterUc_MoveCompleted;
            RenderMaster4WindowUc.MoveStarted -= RenderMasterUc_MoveStarted;
            RenderMaster4WindowUc.Move -= RenderMasterUc_Move;
            RenderMaster4WindowUc.MoveCompleted -= RenderMasterUc_MoveCompleted;
        }


        void RenderMasterUc_MoveCompleted(object sender, RenderMaster.Wpf.BUSINESS.MeshMoveEventArgs e)
        {
            _isDraggingMesh = false;
            _isMovingMesh = false;
            _movingMeshSlabViewModel = null;
            _movingMeshBlock = null;
            _movingPieceBlock = null;

        }

        void RenderMasterUc_Move(object sender, RenderMaster.Wpf.BUSINESS.MeshMoveEventArgs e)
        {
            if (_isDraggingMesh) return;
            if (_isMovingMesh && _movingMeshSlabViewModel != null && _movingMeshBlock != null)
            {
                var blockList = new List<Block>() {_movingMeshBlock};
                double shapeRotationAngle, blockRotationAngle, offsetX, offsetY;
                _movingPieceBlock.Block.MatrixRT.InvMat().GetRotoTransl(out offsetX, out offsetY, out shapeRotationAngle);

                _movingMeshBlock.MatrixRT.GetRotoTransl(out offsetX, out offsetY, out blockRotationAngle);


                Breton.Polygons.Point bretonPoint = new Breton.Polygons.Point(-e.X, -e.Y);

                var rotationMatrix = new RTMatrix(0, 0, shapeRotationAngle) * new RTMatrix(0,0,blockRotationAngle);

                bretonPoint = rotationMatrix * bretonPoint;
                _movingMeshBlock.MatrixRT.Save();
                //_movingMeshBlock.RotoTranslRel(-e.X, -e.Y, 0d, 0d, 0d);
                _movingMeshBlock.RotoTranslRel(bretonPoint.X, bretonPoint.Y, 0d, 0d, 0d);

                if (_movingMeshSlabViewModel.Cbpe.IsCollision(blockList))
                {
                    _movingMeshBlock.MatrixRT.Restore();
                }
                _movingMeshSlabViewModel.Cbpe.Repaint(_movingMeshBlock);
                _movingMeshSlabViewModel.DelayedRefreshRender(blockList, true);
            }
        }

        void RenderMasterUc_MoveStarted(object sender, RenderMaster.Wpf.BUSINESS.MeshMoveEventArgs e)
        {
            _isDraggingMesh = false;
            _isMovingMesh = false;
            _movingMeshSlabViewModel = null;
            _movingMeshBlock = null;
            _movingPieceBlock = null;

            string blockName = e.MeshNumber.ToString();
            var pb = SlabPieces.FirstOrDefault(p => p.Block.Name == blockName);
            if (pb != null)
            {
                _movingPieceBlock = pb;
                if (pb.IsAssignedToSLab) // Move piece
                {
                    if (_parentModel.MainSlabModel.Slab.Id == pb.Piece.SlabId)
                        _movingMeshSlabViewModel = _parentModel.MainSlabModel;
                    else
                    {
                        foreach (var psvm in _parentModel.SlabViewModels)
                        {
                            if (psvm.Slab.Id == pb.Piece.SlabId)
                            {
                                _movingMeshSlabViewModel = psvm;
                                break;
                            }
                        }
                    }
                    if (_movingMeshSlabViewModel != null)
                    {
                        _movingMeshBlock = _movingMeshSlabViewModel.Cbpe.GetBlockByCode(pb.Block.Code);
                    }
                    _isMovingMesh = true;
                }
                else // drag piece
                {
                    var block = Cbpe.GetBlockByName(blockName);
                    if (block != null)
                    {
                        //Cbpe.UnSelect();
                        //Cbpe.SelectBlock(block);
                        _parentModel.CbpeSource = Cbpe;
                        _parentModel.SelectedPieceBlock = pb;
                        Cbpe.PrepareAndStartDrag(block, e.X, e.Y);
                        _isDraggingMesh = true;
                    }
                    //DragDrop.DoDragDrop()
                }
            }

        }

        private void RunOnUiThread(Action action)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.BeginInvoke(action);
            }
            else
            {
                action.Invoke();
            }
        }

        void _slabPieces_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    // Add piece to viewer
                    foreach (PieceBlock pb in e.NewItems)
                    {
                        int newBlockNumber = Cbpe.InsertBlock(pb.Block);
                        UpdatePieceBlockColor(pb, newBlockNumber);
                        //pb.IsAssignedToSLab = true;
                        //pb.Piece.Slab = this.Slab;
                        AttachPieceBlockEvents(pb);
                    }
                    Cbpe.Repaint();

                    break;

                case NotifyCollectionChangedAction.Remove:
                    // Remove piece from viewer; 
                    // Set piece as "Not assigned"
                    foreach (PieceBlock pb in e.OldItems)
                    {
                        Cbpe.RemoveBlock(pb.Block.Id);
                        pb.IsAssignedToSLab = false;
                        pb.Piece.Slab = null;
                        DetachPieceBlockEvents(pb);

                    }

                    break;

                case NotifyCollectionChangedAction.Reset:
                    // For each piece:
                    // Remove piece from viewer; 
                    // Set piece as "Not assigned"
                    foreach (PieceBlock pb in e.OldItems)
                    {
                        Cbpe.RemoveBlock(pb.Block.Id);
                        pb.IsAssignedToSLab = false;
                        pb.Piece.Slab = null;
                        DetachPieceBlockEvents(pb);

                    }

                    break;
            }

        }

        private void AttachPieceBlockEvents(PieceBlock pb)
        {
            pb.PropertyChanged += pb_PropertyChanged;
        }

        private void DetachPieceBlockEvents(PieceBlock pb)
        {
            pb.PropertyChanged -= pb_PropertyChanged;
        }

        void pb_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var pieceBlock = sender as PieceBlock;
            if (pieceBlock == null) return;
            if (e.PropertyName == "IsAssignedToSLab")
            {
                var block = Cbpe.GetBlockByCode(pieceBlock.Block.Code);
                if (block != null)
                    UpdatePieceBlockColor(pieceBlock, block.Id);

            }
        }

        private void UpdatePieceBlockColor(PieceBlock pb, int newBlockNumber)
        {
            if (pb.IsAssignedToSLab)
            {
                Cbpe.Blocks[newBlockNumber].AllowDragDrop = false;
                Cbpe.Blocks[newBlockNumber].SetColor(Color.SteelBlue);
            }
            else
            {
                Cbpe.Blocks[newBlockNumber].AllowDragDrop = true;
                Cbpe.Blocks[newBlockNumber].SetColor(Color.LightGoldenrodYellow);
            }
            Cbpe.Repaint(newBlockNumber);
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        #region Toggles

        public bool ShowSlab
        {
            get { return _showSlab; }
            set
            {
                _showSlab = value;
                OnPropertyChanged("ShowSlab");
            }
        }

        public bool ShowDefects
        {
            get { return _showDefects; }
            set
            {
                _showDefects = value;
                OnPropertyChanged("ShowDefects");

            }
        }

        public bool ShowShapes
        {
            get { return _showShapes; }
            set
            {
                _showShapes = value;
                OnPropertyChanged("ShowShapes");

            }
        }

        public bool ShowRaw
        {
            get { return _showRaw; }
            set
            {
                _showRaw = value;
                OnPropertyChanged("ShowRaw");

            }
        }

        public bool ShowCutTool
        {
            get { return _showCutTool; }
            set
            {
                _showCutTool = value;
                OnPropertyChanged("ShowCutTool");

            }
        }

        public bool ShowCutToolRemoval
        {
            get { return _showCutToolRemoval; }
            set
            {
                _showCutToolRemoval = value;
                OnPropertyChanged("ShowCutToolRemoval");

            }
        }

        public bool ShowSymbol
        {
            get { return _showSymbol; }
            set
            {
                _showSymbol = value;
                OnPropertyChanged("ShowSymbol");

            }
        }

        public bool ShowText
        {
            get { return _showText; }
            set
            {
                _showText = value;
                OnPropertyChanged("ShowText");

            }
        }



        public ProjectViewUc View
        {
            get { return _view; }
        }

        public FksqlCbpe Cbpe
        {
            get { return _cbpe; }
            set { _cbpe = value; }
        }





        private void InitCbpeLayers()
        {
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.PHOTO.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLAB.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.DEFECT.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.RAW.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.RAW_HOLE.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.BLIND_HOLE.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.DISC.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.DISC_REM.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.COMBINED.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.COMBINED_REM.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.MILL.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.MILL_REM.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.EDGE_FINISHING_DIM.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.EDGE_FINISHING_TOOL.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.FINAL_TOOL.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.FINAL_TOOL_REM.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_POS.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_POS_REM.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_NEG.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLOPED_CUTS_NEG_REM.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.REFERENCE_POINT1.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.REFERENCE_POINT2.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SLAB_CUTS.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.TEXT.ToString()], false, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SYMBOL.ToString()], false, true, false);

            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SHAPE.ToString()], true, true, false);
            Cbpe.LayerMode(Cbpe.Layers[CBPExtension.LayersType.SHAPE_HOLE.ToString()], true, true, true);

        }

        public ObservableCollection<PieceBlock> SlabPieces
        {
            get { return _slabPieces; }
        }

        public Operation CurrentOperation
        {
            get { return _currentOperation; }
            set
            {
                _currentOperation = value;
                OnPropertyChanged("CurrentOperation");
            }
        }

        private List<DeleteBlock> BlocksToBeDeleted
        {
            get { return _blocksToBeDeleted; }
            set { _blocksToBeDeleted = value; }
        }

        public List<int> CurrentBlocks
        {
            get { return _currentBlocks; }
            set { _currentBlocks = value; }
        }

        public int CurrentBlock
        {
            get { return _currentBlock; }
            set { _currentBlock = value; }
        }

        public bool IsDeletingBlock
        {
            get { return _isDeletingBlock; }
            set
            {
                _isDeletingBlock = value;
                OnPropertyChanged("IsDeletingBlock");
            }
        }

        public bool CanRemoveSlab
        {
            get { return _canRemoveSlab; }
            set
            {
                _canRemoveSlab = value;
                OnPropertyChanged("CanRemoveSlab");
            }
        }

        public bool CanResetSlab
        {
            get { return _canResetSlab; }
            set
            {
                _canResetSlab = value;
                OnPropertyChanged("CanResetSlab");
            }
        }

        public string CurrentCoordinatesString
        {
            get { return _currentCoordinatesString; }
            set
            {
                _currentCoordinatesString = value;
                OnPropertyChanged("CurrentCoordinatesString");

            }
        }

        public ProjectRenderViewModel RenderViewModel
        {
            get { return _renderViewModel; }
            set
            {
                _renderViewModel = value;
                OnPropertyChanged("RenderViewModel");
            }
        }

        #endregion

        #region >-------------- Public Properties - Commands



        #endregion Public Properties - Commands --------<

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        internal void UnSelectAll()
        {
            CurrentBlocks.Clear();
            if (Cbpe != null)
            {
                for (int i = Cbpe.Selections.Count - 1; i >= 0; i--)
                    Cbpe.UnSelect(Cbpe.Selections[i].SelectedBlock);
            }
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void InitCpbeView()
        {
            if (Cbpe == null)
            {
                Cbpe = new FksqlCbpe(View.Viewer);
                AttachCbpeEvents();
                InitCbpeLayers();

                Cbpe.DragDrop = true;

            }
        }

        private void AttachCbpeEvents()
        {
            Cbpe.evMouseAfter += Cbpe_evMouseAfter;
            Cbpe.evClick += Cbpe_evClick;

            //Cbpe.evStartMove += Cbpe_evStartMove;
            //Cbpe.evMove += Cbpe_evMove;
            //Cbpe.evEndMove += Cbpe_evEndMove;
            //Cbpe.evStartRotate += Cbpe_evStartRotate;
            //Cbpe.evRotate += Cbpe_evRotate;
            //Cbpe.evEndRotate += Cbpe_evEndRotate;

            Cbpe.evDragEnter += Cbpe_evDragEnter;
            Cbpe.evDragDrop += Cbpe_evDragDrop;
            Cbpe.evDragLeave += Cbpe_evDragLeave;

        }

        public void DetachCbpeEvents()
        {
            if (Cbpe == null) return; 

            Cbpe.evMouseAfter -= Cbpe_evMouseAfter;
            Cbpe.evClick -= Cbpe_evClick;

            //Cbpe.evStartMove += Cbpe_evStartMove;
            //Cbpe.evMove += Cbpe_evMove;
            //Cbpe.evEndMove += Cbpe_evEndMove;
            //Cbpe.evStartRotate += Cbpe_evStartRotate;
            //Cbpe.evRotate += Cbpe_evRotate;
            //Cbpe.evEndRotate += Cbpe_evEndRotate;

            Cbpe.evDragEnter -= Cbpe_evDragEnter;
            Cbpe.evDragDrop -= Cbpe_evDragDrop;
            Cbpe.evDragLeave -= Cbpe_evDragLeave;

        }


        /// <summary>
        /// Elimina i blocchi selezionati
        /// </summary>
        private void DeleteBlocks()
        {
            string message, caption;
            ArrayList blocksName = new ArrayList();

            message = "Eliminare i blocchi selezionati?";
            caption = "Eliminazione blocchi";

            if (MessageBox.Show(message, caption, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                for (int i = 0; i < BlocksToBeDeleted.Count; i++)
                {
                    //Cbpe.RemoveBlock(BlocksToBeDeleted[i].deleteBlock.Id);

                    var pieceBlock = SlabPieces.FirstOrDefault(p => p.Block.Id == BlocksToBeDeleted[i].deleteBlock.Id);
                    if (pieceBlock != null)
                    {
                        this.SlabPieces.Remove(pieceBlock);
                    }

                    //////if (mDeleteBlocks[i].cbpe == cbpMain)
                    //////    cbpMain.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);
                    //////else if (mDeleteBlocks[i].cbpe == cbpOpt1)
                    //////    cbpOpt1.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);
                    //////else if (mDeleteBlocks[i].cbpe == cbpOpt2)
                    //////    cbpOpt2.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);
                    //////else if (mDeleteBlocks[i].cbpe == cbpOpt3)
                    //////    cbpOpt3.RemoveBlock(mDeleteBlocks[i].deleteBlock.Id);

                    //////if (RemnantDefinition.IsRemnantBlock(mDeleteBlocks[i].deleteBlock))
                    //////{
                    //////    RemoveOpRemnantPieceFromSlab(mDeleteBlocks[i].cbpe.Slab.Name, mDeleteBlocks[i].deleteBlock);
                    //////}
                    //////else
                    //{
                    //    // Rimuove il pezzo dalle strutture dati di OptiMaster
                    //    RemoveOpPieceFromSlab(mDeleteBlocks[i].cbpe.Slab.Name, mDeleteBlocks[i].deleteBlock);
                    //    blocksName.Add(mDeleteBlocks[i].deleteBlock.Code);
                    //}

                }

                //TODO: Verificare

                ////ReActiveShape(blocksName);

                //////mIsModified = true;
                ////mIsModified = (blocksName.Count > 0);


                //PreviewRender();
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates


        /// <summary>
        /// Evento dell'uscita del drag di un pezzo da un finestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        /// <param name="bEnter"></param>
        private void Cbpe_evDragLeave(object sender, CBasicPaintEventArgs ev, Block bEnter)
        {
            ev.Accepted = true;
        }


        /// <summary>
        /// Evento della fine del DragDropo di un pezzo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        /// <param name="bDrop"></param>
        private void Cbpe_evDragDrop(object sender, CBasicPaintEventArgs ev, Block bDrop)
        {
            ev.Accepted = false;
        }


        /// <summary>
        /// Evento dell'inizio del drag di un pezzo da un finestra ad un'altra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        /// <param name="bEnter"></param>
        private void Cbpe_evDragEnter(object sender, CBasicPaintEventArgs ev, Block bEnter)
        {

            if (Cbpe != _parentModel.CbpeSource)
            {
                    ev.Accepted = false;

                    Cbpe.BlockDragDrop = null;

                    return;
            }
            ev.Accepted = true;
        }


        /// <summary>
        /// Evento della fine della rotazione nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void Cbpe_evEndRotate(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        //{
        //    //CBPExtension cbpe = (CBPExtension)sender;

        //    if (e != null)
        //    {
        //        e.Accepted = true;

        //        if (block_no.Count == 0)
        //            return;

        //        List<int> idBlocks = new List<int>();

        //        // Prepara delle liste univoche di blocchi
        //        foreach (int i in block_no)
        //        {
        //            if (!idBlocks.Contains(i))
        //            {
        //                Block block = Cbpe.Blocks[i];
        //                if (block.AllowRotate)
        //                    idBlocks.Add(i);
        //            }
        //        }

        //    }

        //    _parentModel.IsModified = true;
        //}


        /// <summary>
        /// Evento della rotazione nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        //private void Cbpe_evRotate(object sender, CBasicPaintEventArgs ev, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        //{
        //    CBPExtension cbpe = (CBPExtension)sender;

        //    if (ev != null)
        //    {
        //        List<int> idBlocks = new List<int>();
        //        List<Block> blocks = new List<Block>();

        //        // Prepara delle liste univoche di blocchi
        //        foreach (int i in block_no)
        //        {
        //            // De Conti 03/11/2016
        //            //if (!idBlocks.Contains(i))
        //            //{
        //            //    idBlocks.Add(i);
        //            //    blocks.Add(cbpe.GetBlock(i));
        //            //}
        //            if (!idBlocks.Contains(i))
        //            {
        //                var block = cbpe.GetBlock(i);
        //                if (block != null && block.AllowRotate)
        //                {
        //                    idBlocks.Add(i);
        //                    blocks.Add(block);
        //                }
        //            }

        //        }

        //        if (cbpe.IsCollision(blocks))
        //            ev.Accepted = false;
        //        else
        //            ev.Accepted = true;

        //        //if (RenderBig.gCreate || panelRender.Visible)
        //        if (WpfRenderWindow.gCreate)
        //        {
        //            //for (int i = 0; i < idBlocks.Count; i++)
        //            _parentModel.DelayedRefreshRender(cbpe, blocks, true);
        //        }
        //    }
        //}


        /// <summary>
        /// Evento dell'inizio della rotazione nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        //private void Cbpe_evStartRotate(object sender, CBasicPaintEventArgs ev, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        //{
        //    if (ev != null)
        //    {
        //        ev.Accepted = true;
        //    }
        //}


        /// <summary>
        /// Evento della fine del movimento nella finestra principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void Cbpe_evEndMove(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        //{


        //    if (e != null)
        //    {
        //        e.Accepted = true;

        //        if (block_no.Count == 0)
        //        {
        //            Cbpe.DeleteGenericLine();
        //            return;
        //        }

        //        List<int> idBlocks = new List<int>();

        //        // Prepara delle liste univoche di blocchi
        //        foreach (int i in block_no)
        //        {
        //            if (!idBlocks.Contains(i))
        //            {
        //                Block block = Cbpe.Blocks[i];
        //                if (block.AllowMove)
        //                    idBlocks.Add(i);
        //            }
        //        }

        //        if (Cbpe.MagnetEnable && idBlocks.Count == 1 && e.MouseKey != CBasicPaintEventArgs.enMouseKey.MouseKey_None)
        //            Cbpe.LockPieces();

        //        // Aggiorna i valori delle posizioni
        //        for (int i = 0; i < idBlocks.Count; i++)
        //            UpdateOpPieceInSlab(Cbpe.Slab.Name, Cbpe.Blocks[idBlocks[i]]);


        //    }

        //    ////mIsModified = true;
        //}

        /// <summary>
        /// Aggiorna un OpPiece nella lastra
        /// </summary>
        /// <param name="slab"></param>
        /// <param name="piece"></param>
        //private bool UpdateOpPieceInSlab(string slab, Block piece)
        //{

        //    bool retVal = false;
        //    try
        //    {
        //        #region info origin
        //        double traslX, traslY, rotAngle;

        //        piece.GetRotoTransl(out traslX, out traslY, out rotAngle);

        //        traslX = traslX - Cbpe.CurrentSlab.OriginX - Cbpe.CurrentSlab.GraphicOffsetX;
        //        traslY = traslY - Cbpe.CurrentSlab.OriginY - Cbpe.CurrentSlab.GraphicOffsetY;



        //        #endregion

        //        var slabPiece =
        //            _parentModel.ProjectPieceBlocks.FirstOrDefault(p => p.Block.Code == piece.Code);
        //        // Aggiorna le 
        //        if (slabPiece != null)
        //        {

        //            slabPiece.Piece.PosX = traslX;
        //            slabPiece.Piece.PosY = traslY;
        //            slabPiece.Piece.RotationAngle = rotAngle;


        //        }


        //        retVal = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine(" ProgrammingSlabViewModel AddOpPieceInSlab error", ex);
        //        retVal = false;
        //    }

        //    return retVal;
        //}



        //void Cbpe_evMove(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        //{
        //    //CBPExtension cbpe = (CBPExtension)sender;

        //    if (e != null)
        //    {
        //        List<int> idBlocks = new List<int>();
        //        List<Block> blocks = new List<Block>();

        //        // Prepara delle liste univoche di blocchi
        //        foreach (int i in block_no)
        //        {
        //            if (!idBlocks.Contains(i))
        //            {
        //                var block = Cbpe.GetBlock(i);
        //                if (block.AllowMove)
        //                {
        //                    idBlocks.Add(i);
        //                    blocks.Add(block);
        //                }
        //            }
        //        }

        //        if (Cbpe.MagnetEnable && idBlocks.Count == 1 &&
        //            e.MouseKey != CBasicPaintEventArgs.enMouseKey.MouseKey_None)
        //        {
        //            double thickness = Slab.Thickness;
        //            Cbpe.CheckMagnet(Cbpe.GetBlock(idBlocks[0]), mAllowSlopedSideLock, thickness, mDistanceJoin);
        //        }

        //        if (Cbpe.IsCollision(blocks))
        //            e.Accepted = false;
        //        else
        //            e.Accepted = true;


        //        //TODO: Render
        //        //////if (RenderBig.gCreate || panelRender.Visible)
        //        ////if (RenderWindow.gCreate || panelRender.Visible)
        //        ////{
        //        ////    //for (int i = 0; i < idBlocks.Count; i++)
        //        ////    DelayedRefreshRender(cbpe, blocks, true);
        //        ////}
        //    }
        //}

        //void Cbpe_evStartMove(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        //{
        //    if (e != null)
        //    {
        //        e.Accepted = true;
        //    }
        //}

        void Cbpe_evClick(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        {

            // clicco e rendo attivo uno dei CBPExtension
            if (_parentModel.CbpeSource != (CBPExtension)sender || e.MouseKey == CBasicPaintEventArgs.enMouseKey.MouseKey_Right)
            {
                _parentModel.UnSelectAll();
                _parentModel.CbpeSource = Cbpe;
                _parentModel.SelectedPieceBlock = null;

            }
            CBasicPaint.HighLigthType ht = CBasicPaint.HighLigthType.NONE;

            try
            {
                if (e != null)
                {
                    if (e.MouseKey == CBasicPaintEventArgs.enMouseKey.MouseKey_Left)
                    {
                        if (_parentModel.CbpeSource == _parentModel.CbpeProject)
                        {
                            // Deseleziona tutti i blocchi solo dalla visualizzazione del progetto
                            _parentModel.ProjectViewModel.UnSelectAll(); 

                            ht = CBasicPaint.HighLigthType.NORMAL;
                        }

                        CurrentBlocks.Clear();
                        if (block_no.Count > 0)
                            CurrentBlocks.Add(block_no[0]);

                        //foreach (int idb in block_no)
                        //{
                        //    //if (idb != -1 && !CurrentBlocks.Contains(idb))
                        //        CurrentBlocks.Add(idb);
                        //}

                        // Imposta il valore del blocco correntemente selezionato nel CBPE selezionato


                        if (block_no.Count != 0)
                        {
                            //for (int i = 0; i < block_no.Count; i++)
                            for (int i = 0; i < CurrentBlocks.Count; i++)
                            {
                                //cbpeSource.Select(block_no[i], ht);
                                Cbpe.Select(CurrentBlocks[i], ht);


                                // TODO: Manage CurrentBlock in parent viewmodel
                                //if (cbpeSource == cbpProject)
                                {
                                    // Deseleziona tutti i pezzi nelle finestre tranne che sulla finestra di anteprima progetto
                                    foreach(var psvm in _parentModel.SlabViewModels)
                                        psvm.UnSelectAll();
                                    if (_parentModel.MainSlabModel != null)
                                        _parentModel.MainSlabModel.UnSelectAll();

                                    var selectedBlock =
                                        _parentModel.ProjectPieceBlocks.FirstOrDefault(
                                            p => p.Block.Code == Cbpe.Blocks[CurrentBlocks[i]].Code.Trim());

                                    //TODO: Aggiornare blocco selezionato corrente
                                    if (selectedBlock != null)
                                    {
                                        _parentModel.SelectedPieceBlock = selectedBlock;
                                    }

                                    //////SelectBlockInSlab(cbpeSource.Blocks[block_no[i]].Code.Trim());
                                    ////SelectBlockInSlab(cbpeSource.Blocks[CurrentBlocks[i]].Code.Trim());

                                    //////treeViewProjects.SelectedNode = SelectNode(cbpeSource.Blocks[block_no[i]].Code.Trim());
                                    ////treeViewProjects.SelectedNode = SelectNode(cbpeSource.Blocks[mCurrentBlocks[i]].Code.Trim());
                                }
                            }
                        }


                        if (Cbpe.DragDrop)
                        {
                            // Se non ci sono blocchi selezionati
                            if (block_no.Count == 0)
                            {
                                e.Accepted = false;
                                return;
                            }

                            //TODO: 28/04/2016
                            ////// Indica qual'è il blocco per il Drag & Drop
                            //TODO: Verifica

                            _parentModel.CbpeSource.BlockDragDrop = Cbpe.BlockDragDrop;

                        }

                    }


                    e.Accepted = true;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DesignOperation.MouseDownEv ", ex);
                e.Accepted = false;
            }
        }

        /// <summary>
        /// Aggiunge un foretto associandolo alla lastra o al pezzo
        /// </summary>
        /// <returns></returns>
        ////private bool AddDrill()
        ////{
        ////    try
        ////    {
        ////        string caption, message;
        ////        // Prepara i dati per il calcolo del foretto
        ////        Article art;
        ////        ArticleCollection tmpArts = new ArticleCollection(mDbInterface);
        ////        TecnoPaths tempTecnoPaths = new TecnoPaths();
        ////        ToolPaths tempToolPaths = new ToolPaths();
        ////        TecnoPathInfo tpi = new TecnoPathInfo();
        ////        tmpArts.GetSingleElementFromDb(cbpeSource.Slab.Name);
        ////        tmpArts.GetObject(out art);
        ////        tpi.MaterialThickness = art.gDimZ;
        ////        tpi.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;

        ////        foreach (KeyValuePair<int, ToolInfo> t in mTools)
        ////        {
        ////            if (t.Value.Type == EN_TOOL_TYPE.EN_TOOL_TYPE_DRILL)
        ////                tpi.AvailableTools.Add(t.Value);
        ////        }

        ////        TecnoPath tp = new TecnoPath();
        ////        Breton.Polygons.Point fp = cbpeSource.CursorPosition();
        ////        TecnoSegment seg = new TecnoSegment(fp, fp);
        ////        int ids = tp.AddSide(seg);

        ////        TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
        ////        tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
        ////        tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
        ////        tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
        ////        tsi.TecnoPathInfo.MaterialThickness = art.gDimZ;

        ////        tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
        ////        // Indica se il tipo di percorso è esterno o se è un foro
        ////        tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_EXTERIOR;
        ////        tp.AllowSelect = true;
        ////        tempTecnoPaths.AddPath(tp);

        ////        mCam.CreateStandardToolPaths(tempTecnoPaths, out tempToolPaths, tpi, cbpeSource.Slab.Name);

        ////        Path DrillRem = mCam.RemovalPaths.GetPath(0);

        ////        PathCollection paths;
        ////        Path intZone = new Path(mCam.RemovalPaths.GetPath(0));
        ////        ZoneOffset.PathOffset(ref intZone, -20, ZoneOffset.EXPANSION_MODE.EXPANSION_MODE_RECT, out paths);
        ////        intZone.Layer = CBPExtension.LayersType.RAW_INTEREST_ZONE.ToString();

        ////        string blockcollision = null;
        ////        bool insideHole = false;
        ////        // Verifica se la zona di interesse del foretto interseca il pezzo
        ////        foreach (KeyValuePair<int, Block> bl in cbpeSource.Blocks)
        ////        {
        ////            // Verifica che il blocco che si sta analizzando non sia ne la lastra
        ////            // ne uno degli altri tagli aggiuntivi

        ////            if (bl.Value != cbpeSource.Slab &&
        ////                !cbpeSource.ExtraSlabCuts.Exists(delegate(Block x) { return x.Code == bl.Value.Code; }))
        ////            {
        ////                // Estrae tutti i grezzi del pezzo 
        ////                List<Path> raws = cbpeSource.GetRaw(bl.Value.Id, -1);
        ////                List<Path> holes = cbpeSource.GetRawHole(bl.Value.Id, -1);
        ////                foreach (Path r in raws)
        ////                {
        ////                    // Verica che il foretto non intersechi il pezzo
        ////                    if (DrillRem.IntersectRT(r, out intersectPoint))
        ////                    {
        ////                        ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////                        MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////                        throw new ApplicationException("Position Not valid");
        ////                    }

        ////                    // se il foretto è interno, verifica che non sia all'interno di un foro
        ////                    if (r.IsInsideRT(fp))
        ////                    {
        ////                        insideHole = false;
        ////                        foreach (Path h in holes)
        ////                        {
        ////                            // Se interseca un foro da errore
        ////                            if (DrillRem.IntersectRT(h, out intersectPoint))
        ////                            {
        ////                                ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////                                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////                                throw new ApplicationException("Position Not valid");
        ////                            }
        ////                            if (h.IsInsideRT(fp))
        ////                                insideHole = true;

        ////                            // Controllo di collisione										
        ////                            if (intZone.IntersectRT(h, out intersectPoint))
        ////                            {
        ////                                blockcollision = bl.Value.Code;
        ////                                break;
        ////                            }
        ////                        }

        ////                        if (!insideHole)
        ////                        {
        ////                            ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////                            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////                            throw new ApplicationException("Position Not valid");
        ////                        }
        ////                    }

        ////                    // Controllo di collisione										
        ////                    if (blockcollision == null && intZone.IntersectRT(r, out intersectPoint))
        ////                    {
        ////                        blockcollision = bl.Value.Code;
        ////                        break;
        ////                    }
        ////                }
        ////            }
        ////        }

        ////        // Se il foretto non deve essere associato a nessun pezzo esistente, lo aggiunge alla lastra
        ////        if (blockcollision == null)
        ////        {
        ////            //// Crea il blocco inserendo il percorso utensile del foretto								
        ////            //int drill = cbpeSource.AddSlabCut(tempToolPaths[0]);

        ////            //Block eb = cbpeSource.ExtraSlabCuts.Find(delegate(Block s) { return s.Name == CBPExtension.SlabCutsCode + drill.ToString(); });
        ////            //// Aggiunge al blocco il tecno path
        ////            //eb.AddPath(tempTecnoPaths[0]);
        ////            //// Aggiunge al blocco l'asportazione del foretto
        ////            //RemovalPath rp = (RemovalPath)mCam.RemovalPaths.GetPath(0);
        ////            //rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();
        ////            //eb.AddPath(rp);
        ////            //// Aggiunge al blocco la zona di interesse
        ////            //eb.AddPath(intZone);

        ////            //cbpeSource.Repaint(drill);

        ////            //*****************************
        ////            // Per il momento non è possibile inserire un foretto sulla lastra ma è necessario agganciarlo a un pezzo
        ////            ProjResource.gResource.LoadMessageBox(this, 17, out caption, out message);
        ////            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
        ////            throw new ApplicationException("Position Not valid");
        ////        }
        ////        else
        ////        // Altrimenti aggiunge il foretto a un pezzo già esistente
        ////        {
        ////            Block bl = cbpeSource.GetBlockByCode(blockcollision);
        ////            RTMatrix m = bl.MatrixRT.InvMat();

        ////            // Aggiunge al blocco il tecno path
        ////            TecnoPath tecnop = new TecnoPath(tempTecnoPaths[0]);
        ////            while (tecnop.Count > 0)
        ////                tecnop.DeleteSide(0);
        ////            for (int i = 0; i < tempTecnoPaths[0].Count; i++)
        ////                tecnop.AddSide(tempTecnoPaths[0].GetSide(i).RotoTrasl(m));
        ////            bl.AddPath(tecnop);

        ////            // Aggiunge al blocco l'asportazione del foretto
        ////            RemovalPath rp = new RemovalPath(mCam.RemovalPaths.GetPath(0));
        ////            rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();
        ////            while (rp.Count > 0)
        ////                rp.DeleteSide(0);
        ////            for (int i = 0; i < mCam.RemovalPaths.GetPath(0).Count; i++)
        ////                rp.AddSide(mCam.RemovalPaths.GetPath(0).GetSide(i).RotoTrasl(m));
        ////            // Aggiunge e ridisegna il path
        ////            cbpeSource.Repaint(bl.Id, bl.AddPath(rp));

        ////            // Aggiunge al blocco il tool path
        ////            ToolPath toolp = new ToolPath(tempToolPaths[0]);
        ////            toolp.Layer = CBPExtension.LayersType.FINAL_TOOL.ToString();
        ////            while (toolp.Count > 0)
        ////                toolp.DeleteSide(0);
        ////            for (int i = 0; i < tempToolPaths[0].Count; i++)
        ////                toolp.AddSide(tempToolPaths[0].GetSide(i).RotoTrasl(m));
        ////            // Aggiunge e ridisegna il path
        ////            cbpeSource.Repaint(bl.Id, bl.AddPath(toolp));

        ////            // Aggiunge al blocco la zona di interesse
        ////            Path iz = new Path(intZone);
        ////            while (iz.Count > 0)
        ////                iz.DeleteSide(0);
        ////            for (int i = 0; i < intZone.Count; i++)
        ////                iz.AddSide(intZone.GetSide(i).RotoTrasl(m));
        ////            bl.AddPath(iz);


        ////        }

        ////        mIsModified = true;
        ////        return true;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        TraceLog.WriteLine(this.ToString() + ".AddDrill", ex);
        ////        return false;
        ////    }
        ////}


        void Cbpe_evMouseAfter(object sender, double x, double y, double z)
        {
            string strCoord;

            if (x < 0d)
                strCoord = string.Concat("X: ", x.ToString("0.0000"));
            else
                strCoord = string.Concat("X:  ", x.ToString("0.0000"));

            if (y < 0d)
                strCoord = string.Concat(strCoord, " ; Y: ", y.ToString("0.0000"));
            else
                strCoord = string.Concat(strCoord, " ; Y:  ", y.ToString("0.0000"));

            CurrentCoordinatesString = strCoord;

        }

        #endregion Delegates  -----------<
    }
}
