﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Breton.DesignCenterInterface;
using Breton.DesignUtil;
using Breton.MathUtils;
using RenderMaster.Wpf.BUSINESS;
using RenderMaster.Wpf.Class;
using RenderMaster.Wpf.VIEWMODELS;
using SlabOperate.BUSINESS;
using SlabOperate.SESSION;
using TraceLoggers;
using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;

namespace SlabOperate.VIEWMODELS
{
    public class ProjectRenderViewModel : ObservableObject
    {
        #region >-------------- Constants and Enums

        public const string DEFAULT_MATERIAL_IMAGE_NAME = "default.jpg";

        private const string DEFAULT_SHAPE_CODE_PREFIX = "ORDET";

        private const int RENDER_DELAY = 200;


        private readonly int TEXTURE_SIZE = RenderMaster.Wpf.Render.TEXTURE_SIZE;

        // De Conti 20170310
        private readonly double TEXTURE_SIZE_RATIO = RenderMaster.Wpf.Render.TEXTURE_SIZE_RATIO;

        private readonly string tempFolder = ApplicationRun.Instance.TempFolder;

        private readonly string startupPath = ApplicationRun.Instance.StartupPath;

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private OperationsViewModel _parentModel;

        private ObservableCollection<PieceBlock> _projectPieces;

        private ObservableCollection<ProgrammingSlabViewModel> _slabViewModels;

        private RenderMasterViewModel _renderMasterViewModel;


        private ObservableCollection<PieceBlock> ProjectPieces
        {
            get { return _projectPieces; }
        }

        private ObservableCollection<ProgrammingSlabViewModel> SlabViewModels
        {
            get { return _slabViewModels; }
        }

        public RenderMasterViewModel RenderMasterViewModel
        {
            get
            {
                return _renderMasterViewModel;
            }
            set
            {
                _renderMasterViewModel = value;
                OnPropertyChanged("RenderMasterViewModel");
            }
        }

        private List<ToBigRender> lstToBigRender = new List<ToBigRender>();


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ProjectRenderViewModel(OperationsViewModel operationsViewModel)
        {
            _parentModel = operationsViewModel;
            if (_parentModel != null)
            {
                _projectPieces = _parentModel.ProjectPieceBlocks;
                _slabViewModels = _parentModel.SlabViewModels;

                RenderMasterViewModel = new RenderMasterViewModel();
            }
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        //public void PreviewRender_ImagesInRam()
        //{
        //    if (mProjectsRender == null || mProjectsRender.Count == 0
        //        || lstToBigRender == null
        //        || mCurrentProject == null)
        //        return;

        //    //disegno modello 3D
        //    Breton.DesignUtil.Shape[] s = (Breton.DesignUtil.Shape[])mProjectsRender[mCurrentProject];
        //    Bitmap[] fileImg = new Bitmap[s.Length];

        //    //////carico l'immagine di default
        //    ////using (Bitmap defaultBitmap = new Bitmap(imgDefault))
        //    ////{
        //    ////    for (int j = 0; j < fileImg.Length; j++)
        //    ////        fileImg[j] = (Bitmap)defaultBitmap.Clone();
        //    ////}



        //    //for (int j = 0; j < fileImg.Length; j++)
        //    //    fileImg[j] = new Bitmap(imgDefault);

        //    RefreshAllRender(false);

        //    for (int j = 0; j < lstToBigRender.Count; j++)
        //    {
        //        for (int k = 0; k < s.Length; k++)
        //        {
        //            if (s[k].gId == lstToBigRender[j].idSag)
        //                fileImg[k] = lstToBigRender[j].imgSag;
        //        }
        //    }

        //    //disegno modello 3D
        //    renderMaster.DrawTop(s, fileImg, Application.StartupPath + "\\Profili\\");

        //    foreach (var bitmap in fileImg)
        //    {
        //        bitmap.Dispose();
        //    }

        //    //if (RenderBig.gCreate)
        //    //    frmRenderBig.ReloadRender(s);
        //}

        public void PreviewRender()
        {
            if (ProjectPieces.Count == 0
                || lstToBigRender == null)
                return;

            //disegno modello 3D
            ////Breton.DesignUtil.Shape[] s = (Breton.DesignUtil.Shape[])mProjectsRender[mCurrentProject];

            string[] fileImg = new string[ProjectPieces.Count];

            InitRenderMasterProgress(ProjectPieces.Count);

            ////carico l'immagine di default
            //using (Bitmap defaultBitmap = new Bitmap(imgDefault))
            //{
            //    for (int j = 0; j < fileImg.Length; j++)
            //        fileImg[j] = (Bitmap)defaultBitmap.Clone();
            //}


            //string defaultBitmapPath = System.IO.Path.Combine(tmpFormPath, DEFAULT_MATERIAL_IMAGE_NAME);

            //for (int j = 0; j < fileImg.Length; j++)
            //    fileImg[j] = defaultBitmapPath;

            RefreshAllRenderOnFiles();

            ////for (int j = 0; j < lstToBigRender.Count; j++)
            ////{
            ////    for (int k = 0; k < s.Length; k++)
            ////    {
            ////        if (s[k].gId == lstToBigRender[j].idSag)
            ////            fileImg[k] = lstToBigRender[j].imgSagPath;
            ////    }
            ////}



            // Costruisco i riferimenti per velette e backsplash
            Dictionary<int, RenderedElement> elements = new Dictionary<int, RenderedElement>();
            foreach (var piece in ProjectPieces)
            {
                int shapeId = piece.ProjectShape.gId;

                //var projectBlock = cbpProject.GetBlockByName(shapeId.ToString());
                var projectBlock = piece.Block;

                if (projectBlock != null && projectBlock.Code.StartsWith(DEFAULT_SHAPE_CODE_PREFIX))
                {
                    var elementType = piece.ProjectShape.IsVeletta
                        ? ProjectElementType.Rib
                        : (piece.ProjectShape.gBackSplash
                            ? ProjectElementType.Backsplash
                            : ProjectElementType.None);
                    double xOrigin, yOrigin, angleOrigin;
                    projectBlock.GetRotoTransl(out xOrigin, out yOrigin, out angleOrigin);

                    elements.Add(shapeId, new RenderedElement(elementType, angleOrigin));

                }
            }
            //disegno modello 3D
            var s = ProjectPieces.Select(p => p.ProjectShape).ToArray();
            var fileImgs = ProjectPieces.Select(p => p.ToBigRender.imgSagPath).ToArray();
            RenderMasterViewModel.DrawTopOnFiles(s, fileImgs, ApplicationRun.Instance.StartupPath + "\\Profili\\", elements);

            //RefreshRenderSpecialsOnFiles(false, elements);

            RenderMasterViewModel.InvalidateAndFit();

            //foreach (var bitmap in fileImg)
            //{
            //    bitmap.Dispose();
            //}

            //if (RenderBig.gCreate)
            //    frmRenderBig.ReloadRender(s);
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private delegate void Invoke_Void_Param(object sender, int n, Bitmap img);

        /// <summary>
        /// Apre la finestra grande indipendente per il render
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ////private void OpenBigRender(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        if (!RenderBig.gCreate)
        ////        {
        ////            RefreshAllRender(false);

        ////            frmRenderBig = new RenderBig(mProjectsRender[mCurrentProject], lstToBigRender, mLanguage);
        ////            frmRenderBig.Show();
        ////        }
        ////        else
        ////        {
        ////            AttivaForm("RenderBig");
        ////        }
        ////    }
        ////    catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }
        ////}

        private void InitBigRender(int itemsNumber = 100)
        {
            //return;
            // RENDER
            if (lstToBigRender != null)
            {
                foreach (var item in lstToBigRender)
                {
                    if (item.imgSag != null)
                    {
                        item.imgSag.Dispose();
                    }
                }
                lstToBigRender.Clear();
            }
            lstToBigRender = new List<ToBigRender>();
            ////RenderBig.ToBigRender tbrInit = new RenderBig.ToBigRender();
            ////tbrInit.idSag = -1;
            ////tbrInit.imgSag = new Bitmap(imgDefault);

            ////using (Bitmap defaultBitmap = new Bitmap(imgDefault))
            ////{
            ////    for (int k = 0; k < itemsNumber; k++)
            ////    {
            ////        lstToBigRender.Add(new RenderBig.ToBigRender(-1, (Bitmap)defaultBitmap.Clone()));
            ////    }
            ////}


            ////for (int k = 0; k < 200; k++)
            ////{
            ////    lstToBigRender.Add(tbrInit);
            ////}
        }

        //private void InitBigRenderOnFiles_ORI(int itemsNumber = 100)
        //{
        //    //return;
        //    // RENDER
        //    if (lstToBigRender != null)
        //    {
        //        foreach (var item in lstToBigRender)
        //        {
        //            if (item.imgSag != null)
        //            {
        //                item.imgSag.Dispose();
        //            }
        //        }
        //        lstToBigRender.Clear();
        //    }
        //    lstToBigRender = new List<ToBigRender>();


        //    string defaultBitmapPath = System.IO.Path.Combine(startupPath, DEFAULT_MATERIAL_IMAGE_NAME);
        //    //if (File.Exists(defaultBitmapPath))
        //    //    File.Copy(PieceBlock.DEFAULT_MATERIAL_IMAGE_NAME, defaultBitmapPath, true);


        //    foreach (var piece in ProjectPieces)
        //    {
        //        piece.ToBigRender = new ToBigRender(-1, defaultBitmapPath);
        //    }
        //    //for (int k = 0; k < itemsNumber; k++)
        //    //{
        //    //    lstToBigRender.Add(new ToBigRender(-1, defaultBitmapPath));
        //    //}
        //}

        private void InitBigRenderOnFiles()
        {
            string defaultBitmapPath = System.IO.Path.Combine(startupPath, DEFAULT_MATERIAL_IMAGE_NAME);
            string targetDefaultBitmapPath = System.IO.Path.Combine(tempFolder, DEFAULT_MATERIAL_IMAGE_NAME);
            if (File.Exists(defaultBitmapPath))
                File.Copy( defaultBitmapPath, targetDefaultBitmapPath, true);


            foreach (var piece in ProjectPieces)
            {
                piece.ToBigRender = new ToBigRender(-1, targetDefaultBitmapPath);
            }
            return;

        }

        //private void SetCut(string slabCode)
        //{
        //    try
        //    {
        //        // cerco il cut corretto nella lista Cuts
        //        for (int i = 0; i < mProgrammingSlabs.Count; i++)
        //        {
        //            ProgrammingSlab prgTmp = mProgrammingSlabs[i];
        //            if (prgTmp.code == slabCode)
        //            {
        //                cut = prgTmp.cut;
        //                slabLength = Convert.ToInt32(prgTmp.dimX);
        //                slabHeight = Convert.ToInt32(prgTmp.dimY);
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex) { TraceLog.WriteLine("ERROR: ", ex); }
        //}

        private PointF[] GetPointsBoundingBox(Breton.Polygons.Path pathRender)
        {
            PointF[] pbb = {
                new PointF((float)pathRender.GetVertexRT(0).X, (float)pathRender.GetVertexRT(0).Y),  
                new PointF((float)pathRender.GetVertexRT(1).X, (float)pathRender.GetVertexRT(1).Y), 
                new PointF((float)pathRender.GetVertexRT(2).X, (float)pathRender.GetVertexRT(2).Y),
                new PointF((float)pathRender.GetVertexRT(3).X, (float)pathRender.GetVertexRT(3).Y)};

            return pbb;
        }

        ////private void ChangeImage(object sender, int numSag, Bitmap ritaglio)
        ////{
        ////    if (renderMaster.InvokeRequired)
        ////    {
        ////        Invoke_Void_Param ci = new Invoke_Void_Param(ChangeImage);
        ////        this.Invoke(ci, new Object[] { sender, numSag, ritaglio });
        ////    }
        ////    else
        ////        renderMaster.ChangeMaterial(numSag, ritaglio);
        ////}

        private void InitRenderMasterProgress(int shapeNumber)
        {
            RenderMasterViewModel.InitProgressBar(shapeNumber);
        }

        /// <summary>
        /// Aggiorna il render per il blocco interessato
        /// </summary>
        /// <param name="cbpe"></param>
        /// <param name="b"></param>
        ////private void RefreshRender(CBPExtension cbpe, List<Block> blocks, bool useThread)
        ////{
        ////    foreach (Block b in blocks)
        ////        RefreshRender(cbpe, b, useThread);
        ////}

        ////private void RefreshRender(CBPExtension cbpe, Block b, bool useThread)
        ////{
        ////    if (b == cbpe.Slab)
        ////        return;

        ////    int numSag;
        ////    // faccio il primo ritaglio per il RENDER
        ////    if (!Int32.TryParse(b.Name, out numSag))
        ////        return;

        ////    PointF[] mlstPtiBB = GetPointsBoundingBox(cbpe.GetRenderPath(b.Code));
        ////    Block bOrigin = GetBlockByCode(cbpProject, b.Code);

        ////    if (bOrigin == null)
        ////        return;

        ////    double mAngle = 0d;
        ////    double xOrigin, yOrigin, angleOrigin;
        ////    double x, y, angle;
        ////    if (b != null)
        ////    {
        ////        bOrigin.GetRotoTransl(out xOrigin, out yOrigin, out angleOrigin);
        ////        b.GetRotoTransl(out x, out y, out angle);
        ////        mAngle = MathUtil.gdGradi(angle - angleOrigin);
        ////    }

        ////    if (mlstPtiBB.Length > 0)
        ////    {
        ////        // cerco il cut corretto nella lista Cuts e lo imposto come corrente
        ////        SetCut(cbpe.Slab.Name);

        ////        if (!useThread)
        ////        {
        ////            cut.Scala = 1;
        ////            //cut.SetBitmapOut(mlstPtiBB);
        ////            cut.SetSquareCenteredBitmapOut(ref mlstPtiBB);

        ////        }
        ////    }
        ////    else
        ////        return;

        ////    if (useThread)
        ////        mCutImageThread.AddCut(cut, numSag, slabLength, slabHeight, (float)mAngle, mlstPtiBB);
        ////    else
        ////    {
        ////        //ritaglio immagine della lastra caricata
        ////        using (Bitmap ritaglio = cut.GetImage(slabLength, slabHeight, (float)mAngle, mlstPtiBB))
        ////        {
        ////            //ritaglio.Save(@"E:\Ritaglio.jpg");
        ////            // assegna materiale
        ////            RenderBig.ToBigRender tbg = new RenderBig.ToBigRender();
        ////            tbg.idSag = numSag;
        ////            tbg.imgSag = new Bitmap(ritaglio, TEXTURE_SIZE, TEXTURE_SIZE);
        ////            lstToBigRender.Insert(numSag, tbg);
        ////            var tmp = lstToBigRender[numSag + 1];
        ////            lstToBigRender.RemoveAt(numSag + 1);

        ////            if (tmp.imgSag != null)
        ////            {
        ////                tmp.imgSag.Dispose();
        ////            }

        ////        }

        ////    }
        ////}

        ////private void RefreshRenderOnFiles(CBPExtension cbpe, Block b, bool useThread = false)
        ////{
        ////    if (b == cbpe.Slab || (!b.Code.StartsWith(DEFAULT_SHAPE_CODE_PREFIX)))
        ////        return;


        ////    int numSagTmp;
        ////    // faccio il primo ritaglio per il RENDER
        ////    if (!Int32.TryParse(b.Name, out numSagTmp))
        ////        return;



        ////    PointF[] mlstPtiBBTmp = GetPointsBoundingBox(cbpe.GetRenderPath(b.Code));
        ////    if (mlstPtiBBTmp.Length == 0) return;

        ////    Block bOriginTmp = GetBlockByCode(cbpProject, b.Code);

        ////    if (bOriginTmp == null)
        ////        return;

        ////    //double mAngle = 0d;
        ////    //double xOrigin, yOrigin, angleOrigin;
        ////    //double x, y, angle;
        ////    //if (b != null)
        ////    //{
        ////    //    bOrigin.GetRotoTransl(out xOrigin, out yOrigin, out angleOrigin);
        ////    //    b.GetRotoTransl(out x, out y, out angle);
        ////    //    mAngle = MathUtil.gdGradi(angle - angleOrigin);
        ////    //}

        ////    //if (mlstPtiBB.Length > 0)
        ////    {
        ////        lock (_bitmapLocker)
        ////        {
        ////            try
        ////            {
        ////                double mAngle = 0d;
        ////                double xOrigin, yOrigin, angleOrigin;
        ////                double x, y, angle;

        ////                Block bOrigin = GetBlockByCode(cbpProject, b.Code);

        ////                if (b != null)
        ////                {
        ////                    bOrigin.GetRotoTransl(out xOrigin, out yOrigin, out angleOrigin);
        ////                    b.GetRotoTransl(out x, out y, out angle);
        ////                    mAngle = MathUtil.gdGradi(angle - angleOrigin);
        ////                }

        ////                int numSag = Int32.Parse(b.Name);
        ////                // cerco il cut corretto nella lista Cuts e lo imposto come corrente
        ////                SetCut(cbpe.Slab.Name);

        ////                cut.Scala = 1;

        ////                PointF[] mlstPtiBB = GetPointsBoundingBox(cbpe.GetRenderPath(b.Code));

        ////                cut.SetSquareCenteredBoundingBox(ref mlstPtiBB);

        ////                //OnMaterialPieceRendered();
        ////                //renderMaster.IncrementProgressBarValue();

        ////                //ritaglio immagine della lastra caricata
        ////                using (Bitmap ritaglio = cut.GetSquaredBitmap(slabLength, slabHeight, (float)mAngle, mlstPtiBB))
        ////                {
        ////                    string bitmapPath = System.IO.Path.Combine(tmpFormPath,
        ////                        string.Format("MAT_{0:000}.jpg", numSag));

        ////                    //ritaglio.Save(bitmapPath);
        ////                    using (var matBitmap = new Bitmap(ritaglio, TEXTURE_SIZE, TEXTURE_SIZE))
        ////                    {
        ////                        matBitmap.Save(bitmapPath);
        ////                        ritaglio.Save(bitmapPath + "_BIG.jpg");
        ////                    }

        ////                    // assegna materiale
        ////                    ToBigRender tbg = new ToBigRender(numSag, bitmapPath);

        ////                    lstToBigRender.Insert(numSag, tbg);
        ////                    var tmp = lstToBigRender[numSag + 1];
        ////                    lstToBigRender.RemoveAt(numSag + 1);

        ////                    if (tmp.imgSag != null)
        ////                    {
        ////                        tmp.imgSag.Dispose();
        ////                    }

        ////                }
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                TraceLog.WriteLine("RefreshRenderOnFile error ", ex);
        ////            }

        ////        }
        ////        if (!useThread) renderMaster.IncrementProgressBarValue();

        ////    }
        ////}

        private void RefreshRenderOnFiles(ProgrammingSlabViewModel psvm , Block b, Bitmap slabBitmap, PieceBlock piece)
        {
            if (b == psvm.Cbpe.Slab || (!b.Code.StartsWith(DEFAULT_SHAPE_CODE_PREFIX)))
                return;


            int numSagTmp;
            // faccio il primo ritaglio per il RENDER
            if (!Int32.TryParse(b.Name, out numSagTmp))
                return;




            PointF[] mlstPtiBBTmp = GetPointsBoundingBox(psvm.Cbpe.GetRenderPath(b.Code));
            if (mlstPtiBBTmp.Length == 0) return;

            //Block bOriginTmp = _parentModel.CbpeProject.GetBlockByCode(b.Code);
            Block bOriginTmp = piece.Block;

            if (bOriginTmp == null)
                return;

            {
                //lock (_bitmapLocker)
                {
                    try
                    {
                        double mAngle = 0d;
                        double xOrigin, yOrigin, angleOrigin = 0;
                        double x, y, angle;

                        Block bOrigin = piece.Block;

                        if (b != null)
                        {
                            bOrigin.GetRotoTransl(out xOrigin, out yOrigin, out angleOrigin);
                            b.GetRotoTransl(out x, out y, out angle);
                            mAngle = MathUtil.gdGradi(angle - angleOrigin);
                            //mAngle = MathUtil.gdGradi(angle);
                        }

                        int numSag = Int32.Parse(b.Name);

                        // cerco il cut corretto nella lista Cuts e lo imposto come corrente
                        //SetCut(cbpe.Slab.Name);

                        {
                            psvm.Cut.Scala = 1;

                            PointF[] mlstPtiBB = GetPointsBoundingBox(psvm.Cbpe.GetRenderPath(b.Code));

                            psvm.Cut.SetSquareCenteredBoundingBox(ref mlstPtiBB);

                            //OnMaterialPieceRendered();
                            //renderMaster.IncrementProgressBarValue();

                            //ritaglio immagine della lastra caricata
                            using (
                                Bitmap ritaglio = psvm.Cut.GetSquaredBitmapFromBitmap((int)psvm.Slab.Length, (int)psvm.Slab.Width, (float)mAngle,
                                    mlstPtiBB, slabBitmap))
                            {
                                string bitmapPath = System.IO.Path.Combine(tempFolder,
                                    string.Format("MAT_{0:000}.jpg", numSag));


                                // De Conti 20170310
                                ritaglio.Save(bitmapPath, ImageFormat.Jpeg);
                                //using (var matBitmap = new Bitmap(ritaglio, TEXTURE_SIZE, TEXTURE_SIZE))
                                //{
                                //    // De Conti 20161123
                                //    if (angleOrigin != 0)
                                //    {
                                //        //matBitmap.;
                                //    }

                                //    matBitmap.Save(bitmapPath);
                                //    //ritaglio.Save(bitmapPath + "_BIG.jpg");
                                //}

                                //////int size = ritaglio.Width;

                                //////size = Convert.ToInt32(size * TEXTURE_SIZE_RATIO);

                                //////using (var matBitmap = new Bitmap(ritaglio, size, size))
                                //////{
                                //////    // De Conti 20161123
                                //////    if (angleOrigin != 0)
                                //////    {
                                //////        //matBitmap.;
                                //////    }

                                //////    matBitmap.Save(bitmapPath);
                                //////    //ritaglio.Save(bitmapPath + "_BIG.jpg");
                                //////}

                                //---FINE De Conti 20170310

                                // assegna materiale

                                piece.ToBigRender = new ToBigRender(numSag, bitmapPath);

                                ToBigRender tbg = new ToBigRender(numSag, bitmapPath);

                                //lstToBigRender.Insert(numSag, tbg);
                                //var tmp = lstToBigRender[numSag + 1];
                                //lstToBigRender.RemoveAt(numSag + 1);

                                //if (tmp.imgSag != null)
                                //{
                                //    tmp.imgSag.Dispose();
                                //}
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        TraceLog.WriteLine("RefreshRenderOnFile error ", ex);
                    }

                }

                _renderMasterViewModel.IncrementProgressBarValue();

            }
        }

        ////private void DelayedRefreshRender(CBPExtension cbpe, List<Block> blocks, bool useThread)
        ////{
        ////    _renderRefreshTimer.Stop();

        ////    _renderRefreshCbpExtension = cbpe;
        ////    _renderRefreshBlocks = blocks;
        ////    _renderRefreshUseThread = useThread;

        ////    _renderRefreshTimer.Start();
        ////}

        ////private void RefreshAllRender(bool usethread, int itemsNumber = 100)
        ////{
        ////    InitBigRender(itemsNumber);

        ////    foreach (KeyValuePair<int, Block> b in cbpMain.Blocks)
        ////        RefreshRender(cbpMain, b.Value, usethread);
        ////    foreach (KeyValuePair<int, Block> b in cbpOpt1.Blocks)
        ////        RefreshRender(cbpOpt1, b.Value, usethread);
        ////    foreach (KeyValuePair<int, Block> b in cbpOpt2.Blocks)
        ////        RefreshRender(cbpOpt2, b.Value, usethread);
        ////    foreach (KeyValuePair<int, Block> b in cbpOpt3.Blocks)
        ////        RefreshRender(cbpOpt3, b.Value, usethread);

        ////    if (mProgrammingSlabs != null)
        ////    {
        ////        for (int j = 0; j < mProgrammingSlabs.Count; j++)
        ////        {
        ////            //foreach (KeyValuePair<int, Block> b in cbpMain.Blocks)
        ////            //    RefreshRender(cbpMain, b.Value, usethread);
        ////            //foreach (KeyValuePair<int, Block> b in cbpOpt1.Blocks)
        ////            //    RefreshRender(cbpOpt1, b.Value, usethread);


        ////            //foreach (KeyValuePair<int, Block> b in cbpOpt2.Blocks)
        ////            //    RefreshRender(cbpOpt2, b.Value, usethread);
        ////            //foreach (KeyValuePair<int, Block> b in cbpOpt3.Blocks)
        ////            //    RefreshRender(cbpOpt3, b.Value, usethread);

        ////            if (j > 3 && mProgrammingSlabs[j].cbpe != null)
        ////            {
        ////                foreach (KeyValuePair<int, Block> b in mProgrammingSlabs[j].cbpe.Blocks)
        ////                    RefreshRender(mProgrammingSlabs[j].cbpe, b.Value, usethread);
        ////            }
        ////        }
        ////    }
        ////}

        //private void RefreshAllRenderOnFiles_ORI(bool usethread, int itemsNumber = 200)
        //{
        //    if (!usethread)
        //    {
        //        InitBigRenderOnFiles(itemsNumber);



        //        if (SlabViewModels != null)
        //        {
        //            for (int j = 0; j < SlabViewModels.Count; j++)
        //            {
        //                var pSlab = SlabViewModels[j];
        //                var pSlabCbpe = pSlab.Cbpe;


        //                if (pSlab.Cut != null && pSlab.Cut.LastraImageInfo != null && !string.IsNullOrEmpty(pSlab.Cut.LastraImageInfo.ImagePath))
        //                    if (pSlabCbpe != null && pSlabCbpe.Blocks != null)
        //                    {
        //                        var image = Image.FromFile(pSlab.Cut.LastraImageInfo.ImagePath);
        //                        using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
        //                        {
        //                            image.Dispose();
        //                            for (int i = 0; i < pSlabCbpe.Blocks.Count; i++)
        //                            {
        //                                var block = pSlabCbpe.Blocks.Values[i];
        //                                RefreshRenderOnFiles(pSlabCbpe, block, bitmapImage);
        //                            }
        //                        }

        //                    }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        ////List<Task> taskList = new List<Task>();

        //        //////taskList.Add(Task.Factory.StartNew(() =>
        //        //////{
        //        ////InitBigRenderOnFiles(itemsNumber);
        //        //////}));


        //        ////for (int i = 0; i < cbpMain.Blocks.Count; i++)
        //        ////{
        //        ////    //Application.DoEvents();
        //        ////    var block = cbpMain.Blocks.Values[i];

        //        ////    taskList.Add(Task.Factory.StartNew(() =>
        //        ////    {

        //        ////        RefreshRenderOnFiles(cbpMain, block, true);
        //        ////    }));

        //        ////}

        //        ////for (int i = 0; i < cbpOpt1.Blocks.Count; i++)
        //        ////{
        //        ////    //Application.DoEvents();
        //        ////    var block = cbpOpt1.Blocks.Values[i];
        //        ////    taskList.Add(Task.Factory.StartNew(() =>
        //        ////    {
        //        ////        RefreshRenderOnFiles(cbpOpt1, block, true);
        //        ////    }));

        //        ////}

        //        ////for (int i = 0; i < cbpOpt2.Blocks.Count; i++)
        //        ////{
        //        ////    //Application.DoEvents();
        //        ////    var block = cbpOpt2.Blocks.Values[i];
        //        ////    taskList.Add(Task.Factory.StartNew(() =>
        //        ////    {
        //        ////        RefreshRenderOnFiles(cbpOpt2, block, true);
        //        ////    }));

        //        ////}

        //        ////for (int i = 0; i < cbpOpt3.Blocks.Count; i++)
        //        ////{
        //        ////    //Application.DoEvents();
        //        ////    var block = cbpOpt3.Blocks.Values[i];
        //        ////    taskList.Add(Task.Factory.StartNew(() =>
        //        ////    {
        //        ////        RefreshRenderOnFiles(cbpOpt3, block, true);
        //        ////    }));

        //        ////}

        //        ////if (mProgrammingSlabs != null)
        //        ////{
        //        ////    for (int j = 4; j < mProgrammingSlabs.Count; j++)
        //        ////    {
        //        ////        var pSlabCbpe = mProgrammingSlabs[j].cbpe;
        //        ////        if (pSlabCbpe != null && pSlabCbpe.Blocks != null)
        //        ////        {
        //        ////            for (int i = 0; i < pSlabCbpe.Blocks.Count; i++)
        //        ////            {
        //        ////                //Application.DoEvents();
        //        ////                var block = pSlabCbpe.Blocks.Values[i];
        //        ////                taskList.Add(Task.Factory.StartNew(() =>
        //        ////                {
        //        ////                    RefreshRenderOnFiles(pSlabCbpe, block, true);
        //        ////                }));

        //        ////            }

        //        ////        }
        //        ////    }
        //        ////}

        //        ////Task.WaitAll(taskList.ToArray());

        //    }
        //}

        private void RefreshAllRenderOnFiles()
        {
            //if (!usethread)
            {
                InitBigRenderOnFiles();



                if (SlabViewModels != null && SlabViewModels.Count() > 0)
                {
                    foreach(var pSlab in SlabViewModels)
                    {
                        var pSlabCbpe = pSlab.Cbpe;


                        if (pSlab.Cut != null && pSlab.Cut.LastraImageInfo != null && !string.IsNullOrEmpty(pSlab.Cut.LastraImageInfo.ImagePath))
                            if (pSlabCbpe != null && pSlabCbpe.Blocks != null)
                            {
                                var image = Image.FromFile(pSlab.Cut.LastraImageInfo.ImagePath);
                                using (var bitmapImage = new Bitmap(image, image.Width, image.Height))
                                {
                                    image.Dispose();
                                    foreach (var piece in pSlab.SlabPieces)
                                    {
                                        var block = pSlabCbpe.GetBlock(piece.PlacedBlockId);
                                        RefreshRenderOnFiles(pSlab, block, bitmapImage, piece);
                                    }

                                }

                            }
                    }
                }
            }


            //else // USe thread
            {
                ////List<Task> taskList = new List<Task>();

                //////taskList.Add(Task.Factory.StartNew(() =>
                //////{
                ////InitBigRenderOnFiles(itemsNumber);
                //////}));


                ////for (int i = 0; i < cbpMain.Blocks.Count; i++)
                ////{
                ////    //Application.DoEvents();
                ////    var block = cbpMain.Blocks.Values[i];

                ////    taskList.Add(Task.Factory.StartNew(() =>
                ////    {

                ////        RefreshRenderOnFiles(cbpMain, block, true);
                ////    }));

                ////}

                ////for (int i = 0; i < cbpOpt1.Blocks.Count; i++)
                ////{
                ////    //Application.DoEvents();
                ////    var block = cbpOpt1.Blocks.Values[i];
                ////    taskList.Add(Task.Factory.StartNew(() =>
                ////    {
                ////        RefreshRenderOnFiles(cbpOpt1, block, true);
                ////    }));

                ////}

                ////for (int i = 0; i < cbpOpt2.Blocks.Count; i++)
                ////{
                ////    //Application.DoEvents();
                ////    var block = cbpOpt2.Blocks.Values[i];
                ////    taskList.Add(Task.Factory.StartNew(() =>
                ////    {
                ////        RefreshRenderOnFiles(cbpOpt2, block, true);
                ////    }));

                ////}

                ////for (int i = 0; i < cbpOpt3.Blocks.Count; i++)
                ////{
                ////    //Application.DoEvents();
                ////    var block = cbpOpt3.Blocks.Values[i];
                ////    taskList.Add(Task.Factory.StartNew(() =>
                ////    {
                ////        RefreshRenderOnFiles(cbpOpt3, block, true);
                ////    }));

                ////}

                ////if (mProgrammingSlabs != null)
                ////{
                ////    for (int j = 4; j < mProgrammingSlabs.Count; j++)
                ////    {
                ////        var pSlabCbpe = mProgrammingSlabs[j].cbpe;
                ////        if (pSlabCbpe != null && pSlabCbpe.Blocks != null)
                ////        {
                ////            for (int i = 0; i < pSlabCbpe.Blocks.Count; i++)
                ////            {
                ////                //Application.DoEvents();
                ////                var block = pSlabCbpe.Blocks.Values[i];
                ////                taskList.Add(Task.Factory.StartNew(() =>
                ////                {
                ////                    RefreshRenderOnFiles(pSlabCbpe, block, true);
                ////                }));

                ////            }

                ////        }
                ////    }
                ////}

                ////Task.WaitAll(taskList.ToArray());

            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        #region Render

        #endregion



    }

     
}
