﻿using System;
using System.ComponentModel;
using System.Windows;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer;
using SlabOperate.Business.Entities;
using SlabOperate.Business.Entities.WORK.ORDER;
using SlabOperate.SESSION;
using SlabOperate.VIEWMODELS;
using TraceLoggers;

namespace SlabOperate.WINDOWS
{
    /// <summary>
    /// Interaction logic for TestWindow.xaml
    /// </summary>
    public partial class TestWindow : Window, INotifyPropertyChanged
    {
        private OperationsViewModel mainModel;

        private PersistableEntityCollection<SlabEntity> _slabs; 
        public TestWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        public PersistableEntityCollection<SlabEntity> Slabs
        {
            get { return _slabs; }
            set
            {
                _slabs = value;
                OnPropertyChanged("Slabs");
            }
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var filepath = @"E:\Progetti\Breton-FabMasterNew\Test XML Proj\175.xml";

            OfferEntity newOffer;

            bool retVal = false;

            SqlServerProvider provider = null;

            try
            {
                provider =
                    ApplicationRun.Instance.DataScope.Manager.GetProvider(PersistenceProviderType.SqlServer) as
                        SqlServerProvider;
                if (provider != null)
                {
                    provider.BeginTransaction();


                    if (LEGACY_FM.IMPORT_OFFER.ImportOffer.Import(filepath, out newOffer))
                    {
                        WkOrderEntity newOrder;


                        retVal = WkOrderEntity.CreateOrderFromOffer(newOffer, out newOrder);
                    }
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Import New offer error. ", ex);
            }
            finally
            {
                if (provider != null)
                    provider.EndTransaction(retVal);
            }


        }

        private void GoButton_OnClick(object sender, RoutedEventArgs e)
        {
            var orders = new PersistableEntityCollection<WkOrderEntity>();
            orders.Scope = ApplicationRun.Instance.DataScope;

            orders.GetItemsFromRepository();

            mainModel = new OperationsViewModel();
            if (orders.Count > 0)
            {
                var order = orders[orders.Count -1];
                order.GetProjects();
                mainModel.CurrentOrder = order;
            }


            var win = new SlabOperationsWindow();
            win.DataContext = mainModel;
            win.Closed += win_Closed;
            win.Show();


            //var newSlab = new ProgrammingSlabViewModel(mainModel);
            //mainModel.SlabViewModels.Add(newSlab);
            //newSlab = new ProgrammingSlabViewModel(mainModel);
            //mainModel.SlabViewModels.Add(newSlab);
            //newSlab = new ProgrammingSlabViewModel(mainModel);
            //mainModel.SlabViewModels.Add(newSlab);
            //newSlab.IsMainModel = true;

        }

        void win_Closed(object sender, EventArgs e)
        {
            if (mainModel != null)
            {
                mainModel.RenderRefreshTimer.Stop();

                foreach(var vm in mainModel.SlabViewModels)
                    vm.Terminate();
                if (mainModel.MainSlabModel != null)
                    mainModel.MainSlabModel.Terminate();

                mainModel.ProjectViewModel.DetachCbpeEvents();
                mainModel.ProjectViewModel.DetachRenderControlEvents();

                mainModel.SlabViews.Clear();

                mainModel.ProjectPieceBlocks.Clear();



                mainModel.CurrentProject = null;
                mainModel.CurrentOrder = null;

                mainModel = null;
            }
        }

        private void BtnSelectSlabs_OnClick(object sender, RoutedEventArgs e)
        {
            ////var selector = new ViewModelSlabItemSelector();

            ////var slabsList = new List<string>();
            ////selector.LoadSlabSelectionFromShapesToCreate(slabsList, "MAT01000014", onlyIfHasImage:true);

            var slabs = new PersistableEntityCollection<SlabEntity>();
            slabs.Scope = ApplicationRun.Instance.DataScope;

            var filterClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Material.Code"), ConditionOperator.IsNotNull, null);

            // aggiungi condizione per spessore

            var secondClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"), ConditionOperator.Ge, 10);
            secondClause.AddClause(LogicalConnector.And, new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"), ConditionOperator.Le, 40));

            filterClause.AddClause(LogicalConnector.And, secondClause);

            slabs.GetItemsFromRepository(clause: filterClause);

        }

        private void BtnListSlabs_OnClick(object sender, RoutedEventArgs e)
        {
            var slabs = new PersistableEntityCollection<SlabEntity>();
            slabs.Scope = ApplicationRun.Instance.DataScope;
            slabs.GetItemsFromRepository();
            Slabs = slabs;

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
