﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer;


namespace SlabOperate.SETTINGS
{
    public class Settings:BasePersistableEntity
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public Settings()
        {
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public bool ReadFromFile(string filepath, out string errors)
        {
            errors = string.Empty;

            
            if (!File.Exists(filepath))
            {
                errors += string.Format("Settings file <{0}> not found.", filepath);
            }
            else
            {
                if (GetFromRepository(filepath))
                {
                    errors += CheckSettings();
                }
            }

            return string.IsNullOrEmpty(errors);
        }

        private string CheckSettings()
        {
            string errors = string.Empty;


            return errors;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
