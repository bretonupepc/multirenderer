﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer;
using MessageProvider;
using SlabOperate.MANAGERS;
using SlabOperate.SETTINGS;
using TraceLoggers;

using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;
using BusinessErrorManager = SlabOperate.Business.Objects.MANAGERS.ErrorManager;


namespace SlabOperate.SESSION
{
    public class ApplicationRun:ObservableObject
    {
        #region >-------------- Constants and Enums

        public const string APP_NAME = "Slab operate";

        public const string CONFIG_FOLDER_NAME = "CONFIG";

        public const string LOGS_FOLDER_NAME = "LOGS";

        public const string PLANT_BITMAPS_FOLDER_NAME = "PLANT_BITMAPS";

        private  const string SETTINGS_FILENAME = "SlabOperate.Settings.xml";

        private  const string UDL_FILENAME = "DbConnection.udl";

        private const string DEFAULT_TEMP_FOLDER = "SlabOperate";



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private static ApplicationRun _instance;


        private MessageBoxProvider _messageService;

        private string _startupPath = string.Empty;

        private string _plantBitmapsFolder = string.Empty;

        private string _logsFolder = string.Empty;

        private string _tempFolder = string.Empty;


        private Settings _settings;


        private BusinessErrorManager _errorManager;

        private PersistenceScope _dataScope;

        private DbParametersManager _parameterManager;

        private ApplicationDataSources _dataSources;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        private ApplicationRun()
        {
            _errorManager = new BusinessErrorManager();

        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public static ApplicationRun Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ApplicationRun();
                }
                return _instance;
            }
        }


        public MessageBoxProvider MessageService
        {
            get
            {
                if (_messageService == null)
                    _messageService = new MessageBoxProvider();
                return _messageService;
            }
        }


        public string PlantBitmapsFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_plantBitmapsFolder))
                {
                    _plantBitmapsFolder = Path.Combine(StartupPath, CONFIG_FOLDER_NAME, PLANT_BITMAPS_FOLDER_NAME);
                }
                return _plantBitmapsFolder;
            }
        }



        public string StartupPath
        {
            get
            {
                if (string.IsNullOrEmpty(_startupPath))
                {
                    _startupPath = GetStartupPath();
                }
                return _startupPath;
            }
        }

        public Settings Settings
        {
            get { return _settings; }
        }

        public string LogsFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_logsFolder))
                {
                    _logsFolder = Path.Combine(StartupPath, LOGS_FOLDER_NAME);
                    if (!Directory.Exists(_logsFolder))
                    {
                        Directory.CreateDirectory(_logsFolder);
                    }
                }
                return _logsFolder;
            }
        }



        public BusinessErrorManager ErrorManager
        {
            get { return _errorManager; }
        }

        public string TempFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_tempFolder))
                {
                    _tempFolder = Path.Combine(Path.GetTempPath(), DEFAULT_TEMP_FOLDER);
                    {
                        if (!Directory.Exists(_tempFolder))
                        {
                            Directory.CreateDirectory(_tempFolder);
                        }
                    }
                }
                return _tempFolder;
            }
        }


        public PersistenceScope DataScope
        {
            get
            {
                if (_dataScope == null)
                {
                    _dataScope = new PersistenceScope("Main");
                    var sqlProvider = _dataScope.Manager.AvailableProviders.FirstOrDefault(p => p.ProviderType == PersistenceProviderType.SqlServer);
                    if (sqlProvider as SqlServerProvider != null)
                    {
                        (sqlProvider as SqlServerProvider).UdlFilename = Path.Combine(StartupPath, UDL_FILENAME);
                    }
                }
                return _dataScope;
            }
        }

        public DbParametersManager ParameterManager
        {
            get
            {
                if (_parameterManager == null)
                    _parameterManager = new DbParametersManager(DataScope);
                return _parameterManager;
            }
        }

        public ApplicationDataSources DataSources
        {
            get
            {
                if (_dataSources == null)
                    _dataSources = new ApplicationDataSources(DataScope);
                return _dataSources;
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public bool Initialize(out string errors)
        {
            bool retVal = true;
            errors = string.Empty;

            //todo:
            TraceLog.ProcessName = APP_NAME;
            TraceLog.StartTraceLog(LogsFolder, 0);



            string settingsErrors;
            if (!ReadSettings(out settingsErrors))
            {
                errors += "Settings errors\n" + settingsErrors;
            }



            retVal = string.IsNullOrEmpty(errors);

            return retVal;
        }



        private bool ReadSettings(out string errors)
        {
            errors = string.Empty;

            _settings = new Settings();

            _settings.ReadDbSettings();

            return string.IsNullOrEmpty(errors);
        }

        public void Terminate()
        {

            TraceLog.Close();

        }
        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods


        private string GetStartupPath()
        {
            string startupPath = System.IO.Path.GetDirectoryName
                (System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
            return startupPath;
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
