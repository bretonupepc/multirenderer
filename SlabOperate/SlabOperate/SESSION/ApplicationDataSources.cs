﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Business.DATASOURCES;
using DataAccess.Business.Persistence;
using SlabOperate.Business.Entities;

namespace SlabOperate.SESSION
{
    public class ApplicationDataSources : DataAccess.Business.DATASOURCES.DataSources
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ApplicationDataSources(PersistenceScope scope)
            : base(scope)
        {
        }



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties


        public BaseDataSource<SlabEntity> Slabs
        {
            get
            {
                var items = GetDataSource<SlabEntity>(typeof(SlabEntity).ToString());
                //if (items.Count == 0)
                //    items.GetAllItemsFromRepository();
                return items;
            }
        }

        public BaseDataSource<SlabImageEntity> SlabImages
        {
            get
            {
                var items = GetDataSource<SlabImageEntity>(typeof(SlabImageEntity).ToString());
                //if (items.Count == 0)
                //    items.GetAllItemsFromRepository();
                return items;
            }
        }

        public BaseDataSource<MaterialEntity> Materials
        {
            get
            {
                var items = GetDataSource<MaterialEntity>(typeof(MaterialEntity).ToString());
                //if (items.Count == 0)
                //    items.GetAllItemsFromRepository();
                return items;
            }
        }

        public BaseDataSource<MaterialClassEntity> MaterialClasses
        {
            get
            {
                var items = GetDataSource<MaterialClassEntity>(typeof(MaterialClassEntity).ToString());
                //if (items.Count == 0)
                //    items.GetAllItemsFromRepository();
                return items;
            }
        }




        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public void ClearDictionaries()
        {
            if (Dictionaries.ContainsKey(PersistenceProviderType.SqlServer))
            {
                var dict = Dictionaries[PersistenceProviderType.SqlServer];
                dict.Clear();
            }
        }

        //public BaseDataSource<T> GetDataSource<T>(string typeName) where T:BasePersistableEntity
        //{
        //    BaseDataSource<T> dictionary;

        //    if (Dictionaries.ContainsKey(typeName))
        //    {
        //        dictionary = Dict[typeName] as BaseDataSource<T>;
        //    }
        //    else
        //    {
        //        dictionary = new BaseDataSource<T>();
        //        Dict.Add(typeName, dictionary);
        //    }

        //    return dictionary;
        //}

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }

}
