﻿using System;
using TraceLoggers;
using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;

namespace SlabOperate.SESSION
{
    public enum Um
    {
        Mm,
        Inches
    }

    public enum GraphicLayoutOrientation
    {
        Degrees0 = 0,
        Degrees90 = 1,
        Degrees180 = 2,
        Degrees270 = 3
    }

    public class Settings : ObservableObject
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        private double _rifXSlabOnTable = -1;

        private double _rifYSlabOnTable = -1;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        public bool ReadDbSettings()
        {
            bool retVal = true;

            try
            {
                //DB Parameters
                string readValue = string.Empty;
                double doubleValue;


                if (ApplicationRun.Instance.ParameterManager.GetSingleParameter("RifSlabOnTable.X", out readValue))
                {
                    if (!string.IsNullOrEmpty(readValue) && double.TryParse(readValue, out doubleValue))
                    {
                        _rifXSlabOnTable = doubleValue;
                    }
                }
                else retVal = false;

                if (ApplicationRun.Instance.ParameterManager.GetSingleParameter("RifSlabOnTable.Y", out readValue))
                {
                    if (!string.IsNullOrEmpty(readValue) && double.TryParse(readValue, out doubleValue))
                    {
                        _rifYSlabOnTable = doubleValue;
                    }
                }
                else retVal = false;


            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("Settings:ReadDbSettings error: ", ex);
                retVal = false;
            }

            return retVal;
        }

        //public bool SaveSettings()
        //{
        //    bool retVal = false;

        //    int saves = 0;

        //    if (_iniManager != null)
        //    {
        //        try
        //        {
        //            var value = _iniManager.WriteItem("Programma", "Lingua", CultureId);
        //            saves -= (value == 0) ? 1 : 0;



        //            value = _iniManager.WriteItem("Programma", "UM", (MeasureUnit == Um.Mm ? "0" : "1"));
        //            saves -= (value == 0) ? 1 : 0;

        //            value = _iniManager.WriteItem("Programma", "PrintEnabled", (PrintEnabled ? "1" : "0"));
        //            saves -= (value == 0) ? 1 : 0;

        //            value = _iniManager.WriteItem("Stampa", "NameLabel", PreferredLabelName);
        //            saves -= (value == 0) ? 1 : 0;

        //            value = _iniManager.WriteItem("Stampa", "UseDbLabelWhenAvailable", (UseDbLabelWhenAvailable ? "1" : "0"));
        //            saves -= (value == 0) ? 1 : 0;


        //            retVal = (saves == 0);


        //        }

        //        catch (Exception ex)
        //        {
        //            TraceLog.WriteLine("Settings:SaveSettings error: ", ex);
        //            retVal = false;
        //        }
        //        finally
        //        {
        //            if (retVal)
        //            {
        //                TraceLog.WriteLine("Settings: SaveSettings successfully executed. ");
        //            }
        //            else
        //            {
        //                TraceLog.WriteLine("Settings: SaveSettings executed with errors. ");
        //            }
        //        }
        //    }

        //    return retVal;
        //}

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        public double RifXSlabOnTable
        {
            get { return _rifXSlabOnTable; }
        }

        public double RifYSlabOnTable
        {
            get { return _rifYSlabOnTable; }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
