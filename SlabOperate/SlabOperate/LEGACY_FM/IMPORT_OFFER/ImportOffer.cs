﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DbAccess;
using SlabOperate.Business.Entities;
using SlabOperate.SESSION;
using TraceLoggers;

namespace SlabOperate.LEGACY_FM.IMPORT_OFFER
{
    public class ImportOffer
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /// <summary>
        /// Importa l'offerta
        /// </summary>
        /// <param name="filename">Nome del file da importare</param>
        /// <param name="offercode"></param>
        /// <param name="reloaded"></param>
        /// <returns></returns>
        public static  bool Import(string filename, out OfferEntity newOffer)
        {
            bool retVal = false;

            XmlDocument originalDoc = new XmlDocument();

            newOffer = null;

            var offerEntity = new OfferEntity()
            {
                Scope = ApplicationRun.Instance.DataScope
            };

            try
            {
                offerEntity.ReadFileXml(filename);



                //mOffers = new OfferCollection(DbInterface, null);

                originalDoc.Load(filename);
                if (DeleteNodes(ref originalDoc))
                {
                    var tempPath = Path.Combine(ApplicationRun.Instance.TempFolder,
                        string.Format("OFF_{0}.xml", offerEntity.Code));
                    originalDoc.Save(tempPath);

                    offerEntity.XmlFilepath = tempPath;
                }

                offerEntity.OfferStatus = OfferStatus.ToAnalyze;
                offerEntity.Revision = 0;
                offerEntity.Date = DateTime.Now;


                retVal = offerEntity.SaveToRepository();

                if (retVal)
                {
                    newOffer = offerEntity;
                }
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("ImportOffer error ", ex);
                retVal = false;
            }



            return retVal;
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        /// <summary>
        /// Cancella tutti i nodi che non interessano a questo livello
        /// </summary>
        /// <param name="doc">xml document da modificare</param>
        /// <returns></returns>
        private static bool DeleteNodes(ref XmlDocument doc)
        {
            ArrayList nodes = new ArrayList();
            XmlNode node = doc.SelectSingleNode("/EXPORT");

            try
            {
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (nd.Name.ToUpperInvariant() != "FABMASTER")
                        nodes.Add(nd);
                }

                node.RemoveAll();
                for (int i = 0; i < nodes.Count; i++)
                    node.AppendChild((XmlNode)nodes[i]);
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("ImportOffer.DeleteNodes Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine( "ImportOffer.DeleteNodes Error. ", ex);
                return false;
            }
        }


        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
