namespace SlabOperate.LEGACY_FM.UTILS
{
	public class GlobDef
	{
        private const string mCFG_DB_FILENAME = "lastredb.mdb";

        private const string mNEW_PARAMS_FILENAME = "ImgCapture.ini";
        private const string mEDIT_PARAMS_FILENAME = "in_ImgCapture.ini";
        private string mCFG_PARAMS_FILENAME;

		private const string mTHICKNESS_FILENAME = "Thickness.ini";
		private string mCFG_THICKNESS_FILENAME;

        private const string mNEW_IMAGE_FILENAME = "tmp_image.jpg";
        private const string mEDIT_IMAGE_FILENAME = "in_image.jpg";
        private string mCFG_IMAGE_FILENAME;

        //MultiPhoto
        private const string mNEW_IMAGE_FILENAME_MULTIPHOTO = "tmp_image_1.jpg";


        private const string mCFG_OUT_IMAGE_FILENAME = "out_image.jpg";
        private const string mCFG_OUT_RASTER_FILENAME = "out_raster.txt";
        private const string mCFG_OUT_BOUNDARIES_FILENAME = "out_boudaries.txt";
        private const string mCFG_OUT_PARAMS_FILENAME = "out_ImgCapture.ini";
        private const string mCFG_OUT_XML_PARAMETER_FILENAME = "out_Xml_Parameter.XML";

		private const string mCFG_IMAGE_REFPOINT1 = "refpoint1.jpg";
		private const string mCFG_IMAGE_REFPOINT2 = "refpoint2.jpg";

        private const double mCFG_DEFAULT_IMAGE_ASPECT = (4.0000 / 3.0000);
        private const double mCFG_SETUP_TOP_BORDER_HEIGHT = 130;		//bordo sopra l'immagine
        private const double mCFG_WORKING_TOP_BORDER_HEIGHT = 130;	//bordo sopra l'immagine
        private const int mCFG_REFERENCE_POINTS_COUNT_MAX = 100;

        private const int mNPOINTS = 4;

        // 2016-01-26
        //public const string STITCH_CONFIG_DEFAULT_FILENAME = "ImgCaptureStitch.ini";

	    //private string _stitchConfigFilepath = string.Empty;


        public GlobDef()
        { 
        }

        public string CFG_DB_FILENAME
        {
            get { return mCFG_DB_FILENAME; }
        }

        public string NEW_PARAMS_FILENAME
        {
            get { return mNEW_PARAMS_FILENAME; }
        }

        public string EDIT_PARAMS_FILENAME
        {
            get { return mEDIT_PARAMS_FILENAME; }
        }

		public string THICKNESS_FILENAME
		{		
			get { return mTHICKNESS_FILENAME; }
		}

		public string CFG_THICKNESS_FILENAME
        {
			set { mCFG_THICKNESS_FILENAME = value; }
			get { return mCFG_THICKNESS_FILENAME; }
        }
		
        public string CFG_PARAMS_FILENAME
        {
            set { mCFG_PARAMS_FILENAME = value; }
            get { return mCFG_PARAMS_FILENAME; }
        }

        public string NEW_IMAGE_FILENAME
        {
            get { return mNEW_IMAGE_FILENAME; }
        }

        public string NEW_IMAGE_FILENAME_MULTIPHOTO
        {
            get { return mNEW_IMAGE_FILENAME_MULTIPHOTO; }
        }

        public string EDIT_IMAGE_FILENAME
        {
            get { return mEDIT_IMAGE_FILENAME; }
        }

        public string CFG_IMAGE_FILENAME
        {
            set { mCFG_IMAGE_FILENAME = value; }
            get { return mCFG_IMAGE_FILENAME; }
        }

        public string CFG_OUT_IMAGE_FILENAME
        {
            get { return mCFG_OUT_IMAGE_FILENAME; }
        }

        public string CFG_OUT_RASTER_FILENAME
        {
            get { return mCFG_OUT_RASTER_FILENAME; }
        }

        public string CFG_OUT_BOUNDARIES_FILENAME
        {
            get { return mCFG_OUT_BOUNDARIES_FILENAME; }
        }

        public string CFG_OUT_PARAMS_FILENAME
        {
            get { return mCFG_OUT_PARAMS_FILENAME; }
        }

        public string CFG_OUT_XML_PARAMETER_FILENAME
        {
            get { return mCFG_OUT_XML_PARAMETER_FILENAME; }
        }

        public double CFG_DEFAULT_IMAGE_ASPECT
        {
            get { return mCFG_DEFAULT_IMAGE_ASPECT; }
        }

        public double CFG_SETUP_TOP_BORDER_HEIGHT
        {
            get { return mCFG_SETUP_TOP_BORDER_HEIGHT; }
        }

        public double CFG_WORKING_TOP_BORDER_HEIGHT
        {
            get { return mCFG_WORKING_TOP_BORDER_HEIGHT; }
        }

        public int CFG_REFERENCE_POINTS_COUNT_MAX
        {
            get { return mCFG_REFERENCE_POINTS_COUNT_MAX; }
        }

        public int NPOINTS
        {
            get { return mNPOINTS; }
        }

		//public string StitchConfigFilepath
		//{
		//	get
		//	{
		//		if (string.IsNullOrEmpty(_stitchConfigFilepath) && !string.IsNullOrEmpty(mCFG_PARAMS_FILENAME))
		//		{
		//			_stitchConfigFilepath = Path.Combine(Path.GetDirectoryName(mCFG_PARAMS_FILENAME),
		//				STITCH_CONFIG_DEFAULT_FILENAME);
		//		}
		//		return _stitchConfigFilepath;
		//	}
		//	set { _stitchConfigFilepath = value; }
		//}

	    public string CFG_IMAGE_REFPOINT(int i)
		{
			if (i == 1)
				return mCFG_IMAGE_REFPOINT1;
			if (i == 2)
				return mCFG_IMAGE_REFPOINT2;

			return "";
		}
	}
}
