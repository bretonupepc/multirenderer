using System;
using Breton.Polygons;

namespace SlabOperate.LEGACY_FM.UTILS
{
	public class Coords
	{
		private Point[] p = new Point[3];
		private Point[] q = new Point[3];

		private double[,] M = new double[2,2];
		private double[,] MI = new double[2,2];
		private double[,] ID = new double[2,2];
		private Origin mOrigin;
		private bool initOk;

		public enum Origin
		{
			SX_DW = 0,
			DX_DW = 1,
			DX_UP = 2,
			SX_UP = 3
		}

		public Coords()
			: this(Origin.SX_DW)
		{
		}

		public Coords(Origin org)
		{
			initOk = false;

			M[0, 0] = 1;
			M[0, 1] = 0;
			M[1, 0] = 0;
			M[1, 1] = 1;

			mOrigin = org;
		}

		~Coords()
		{
			initOk = false;

			for (int i = 0; i <= 2; i++)
			{
				p[i]=null;
				q[i]=null;
			}
		}
		
		// Il metodo init() calcola la matrice di conversione di coordinate pixel->metri
		// in base a due punti di cui si conoscono le coordinate nello spazio "pixel"
		// e nello spazio "metri".
		// Tipicamente i due punti sono localizzati in angoli apposti del campo visivo
		// della fotocamera.
		// Posto:
		//   P1, P2 -> spazio metrico
		//   Q1, Q2 -> spazio pixel
		//   P0 := (P1.x, P2.y)
		//   Q0 := (Q1.x, Q2.y)
		//   M := (matrice di conversione) := [ m11 m12 ]
		//                                    [ m21 m22 ]
		//
		// la formula di conversione di risulta:
		//   P-P0 = M * (Q-Q0)
		//   ==> P = M * (!-!0) + P0
		//
		// Per ricavare M da P1, P2, Q1, Q2, si pone inoltre:
		//   A := P1-P0 := (ax, ay)
		//   B := P2-P0 := (bx, by)
		//   C := Q1-Q0 := (cx, cy)
		//   D := Q2-Q0 := (dx, dy)
		//
		// Dalle relazioni P1<->Q1 e P2<->Q2 si ottiene il sistema:
		//   { P1-P0 = M*(Q1-Q0)     ==> { A = M*C
		//   { P2-P0 = M*(Q2-Q0)         { B = M*D
		//
		//   ==> { m11*cx + m12*cy = ax
		//       { m21*cx + m22*cy = ay
		//       { m11*dx + m12*dy = bx
		//       { m21*dx + m22*dy = by
		//
		// che in realta' sono due sistemi in due incognite con la stessa matrice di sistema:
		//   [ cx cy ] (m11) = (ax)
		//   [ dx dy ] (m12)   (bx)
		// e
		//   [ cx cy ] (m21) = (ay)
		//   [ dx dy ] (m22)   (by)
		//
		// quindi detta M1 la matrice del sistema e risolvendo col metodo di Cramer:
		//
		//          | ax cy |                   | cx ax |
		//          | bx dy |                   | dx bx |
		//   m11 = -----------           m12 = -----------
		//            |M1|                        |M1|
		//
		//          | ay cy |                   | cx ay |
		//          | by dy |                   | dx by |
		//   m21 = -----------           m22 = -----------
		//            |M1|                        |M1|
		//
		public bool init(Point p1_metric, Point p2_metric, Point p1_pixel, Point p2_pixel)
		{
			double detM1;
			double ax, ay, bx, by, cx, cy, dx, dy;

			for (int i=0;i<=2;i++)
			{
				p[i]=new Point();
				q[i]=new Point();
			}

			try
			{    
				p[1].X = p1_metric.X;
				p[1].Y = p1_metric.Y;
				p[2].X = p2_metric.X;
				p[2].Y = p2_metric.Y;
			    
				q[1].X = p1_pixel.X;
				q[1].Y = p1_pixel.Y;
				q[2].X = p2_pixel.X;
				q[2].Y = p2_pixel.Y;
			    
				p[0].X = p[1].X;
				p[0].Y = p[2].Y;
			    
				q[0].X = q[1].X;
				q[0].Y = q[2].Y;

				ax = p[1].X - p[0].X;
				ay = p[1].Y - p[0].Y;
				bx = p[2].X - p[0].X;
				by = p[2].Y - p[0].Y;
			    
				cx = q[1].X - q[0].X;
				cy = q[1].Y - q[0].Y;
				dx = q[2].X - q[0].X;
				dy = q[2].Y - q[0].Y;
			    
				//calcolo della matrice di trasformazione delle coordinate
				detM1 = det(cx, cy, dx, dy);

				switch (mOrigin)
				{
					case Origin.SX_DW:
						M[0, 0] = det(ax, cy, bx, dy) / detM1;
						M[0, 1] = det(cx, ax, dx, bx) / detM1;
						M[1, 0] = det(ay, cy, by, dy) / detM1;
						M[1, 1] = det(cx, ay, dx, by) / detM1;
						break;

					case Origin.DX_DW:
						M[0, 0] = -det(ax, cy, bx, dy) / detM1;
						M[0, 1] = det(cx, ax, dx, bx) / detM1;
						M[1, 0] = det(ay, cy, by, dy) / detM1;
						M[1, 1] = det(cx, ay, dx, by) / detM1;
						break;

					case Origin.DX_UP:
						M[0, 0] = -det(ax, cy, bx, dy) / detM1;
						M[0, 1] = det(cx, ax, dx, bx) / detM1;
						M[1, 0] = det(ay, cy, by, dy) / detM1;
						M[1, 1] = -det(cx, ay, dx, by) / detM1;
						break;

					case Origin.SX_UP:
						M[0, 0] = det(ax, cy, bx, dy) / detM1;
						M[0, 1] = det(cx, ax, dx, bx) / detM1;
						M[1, 0] = det(ay, cy, by, dy) / detM1;
						M[1, 1] = -det(cx, ay, dx, by) / detM1;
						break;
				}
				


				//M[0, 1] = det(cx, ax, dx, bx) / detM1;
				//M[1, 0] = det(ay, cy, by, dy) / detM1;
				//M[1, 1] = det(cx, ay, dx, by) / detM1;
			    
				detM1 = det(M[0, 0], M[0, 1], M[1, 0], M[1, 1]);
			    
				MI[0, 0] = M[1, 1] / detM1;
				MI[0, 1] = -M[0, 1] / detM1;
				MI[1, 0] = -M[1, 0] / detM1;
				MI[1, 1] = M[0, 0] / detM1;

				initOk = true;

				return true;
			}
			catch(Exception)
			{
				return false;
			}
		}

		public void pixel2metric(Point p_metric, Point p_pixel)
		{
			double dqx,dqy;

			if (!initOk)
			{
				p_metric.X = p_pixel.X;
				p_metric.Y = p_pixel.Y;
				return;
			}

			try
			{
				dqx = p_pixel.X - q[0].X;
				dqy = p_pixel.Y - q[0].Y;

				switch(mOrigin)
				{
					case Origin.SX_DW:
						p_metric.X = M[0, 0] * dqx + M[0, 1] * dqy + p[0].X;
						p_metric.Y = M[1, 0] * dqx + M[1, 1] * dqy + p[0].Y;
						break;

					case Origin.DX_DW:
						p_metric.X = M[0, 0] * dqx + M[0, 1] * dqy + p[2].X;
						p_metric.Y = M[1, 0] * dqx + M[1, 1] * dqy + p[0].Y;
						break;

					case Origin.DX_UP:
						p_metric.X = M[0, 0] * dqx + M[0, 1] * dqy + p[2].X;
						p_metric.Y = M[1, 0] * dqx + M[1, 1] * dqy + p[1].Y;
						break;

					case Origin.SX_UP:
						p_metric.X = M[0, 0] * dqx + M[0, 1] * dqy + p[0].X;
						p_metric.Y = M[1, 0] * dqx + M[1, 1] * dqy + p[1].Y;
						break;
				}
			}
			catch(Exception)
			{
				p_metric.X = 0;
				p_metric.Y = 0;
			}
		}
				
		public void metric2pixel(Point p_pixel, Point p_metric)
		{
			double dqx,dqy;

			if (!initOk)
			{
				p_pixel.X = p_metric.X;
				p_pixel.Y = p_metric.Y;
				return;
			}

			try
			{
				dqx = p_metric.X - p[0].X;
				dqy = p_metric.Y - p[0].Y;

				switch (mOrigin)
				{
					case Origin.SX_DW:
						p_pixel.X = MI[0, 0] * dqx + MI[0, 1] * dqy + q[0].X;
						p_pixel.Y = MI[1, 0] * dqx + MI[1, 1] * dqy + q[0].Y;
						break;

					case Origin.DX_DW:
						p_pixel.X = MI[0, 0] * dqx + MI[0, 1] * dqy + q[2].X;
						p_pixel.Y = MI[1, 0] * dqx + MI[1, 1] * dqy + q[0].Y;
						break;

					case Origin.DX_UP:
						p_pixel.X = MI[0, 0] * dqx + MI[0, 1] * dqy + q[2].X;
						p_pixel.Y = MI[1, 0] * dqx + MI[1, 1] * dqy + q[1].Y;
						break;

					case Origin.SX_UP:
						p_pixel.X = MI[0, 0] * dqx + MI[0, 1] * dqy + q[0].X;
						p_pixel.Y = MI[1, 0] * dqx + MI[1, 1] * dqy + q[1].Y;
						break;
				}
			}
			catch(Exception)
			{
				p_pixel.X = 0;
				p_pixel.Y = 0;
			}
		}

		public static Origin GetOriginFromString(string org)
		{
			switch (org)
			{
				case "SX_DW":
					return Origin.SX_DW;

				case "DX_DW":
					return Origin.DX_DW;

				case "DX_UP":
					return Origin.DX_UP;

				case "SX_UP":
					return Origin.SX_UP;
			}

			return Origin.SX_DW;
		}

		//----------------------------------------------------------------------
		//   Metodi locali
		//----------------------------------------------------------------------

		private double det(double m11, double m12, double m21, double m22)
		{                      
			return m11 * m22 - m12 * m21;
		}


	}
}
