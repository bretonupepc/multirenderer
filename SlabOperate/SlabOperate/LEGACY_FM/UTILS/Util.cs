//using System.Drawing;

using System;
using System.Windows.Forms;
using Breton.MathUtils;
using Breton.Polygons;
using TraceLoggers;

namespace SlabOperate.LEGACY_FM.UTILS
{
	public class Util
	{
		private delegate DialogResult Invoke_DialogResult_MsgBoxParam(System.Windows.Forms.Form owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon);

		private enum drawType
		{
			Line,
			Rectangle
		}

        private GlobDef mGlobDef;
        private Params gPar;
        private Coords gCoord;

		public Util(GlobDef glob, Params par, Coords coord)
		{
            mGlobDef = glob;
            gPar = par;
            gCoord = coord;
		}

		

	    public bool InitCoordinateSystemByTile()
	    {
            // Crop non trattato

            Point[] ppix = new Point[2];
            Point[] pmet = new Point[2];

	        for (int i = 0; i < 2; i++)
	        {
	            ppix[i] = new Point();
                pmet[i] = new Point();
	        }

            MathUtil.DBL_RECT rectTmp;

            int frameX = 0, frameY = 0;
            int finalDimX = 0, finalDimY = 0;

	        bool retVal = false;

	        try
	        {
	            gPar.read_perspective_correction_frame_params_tile(out frameX, out frameY, out finalDimX,
	                out finalDimY);

	            //Inizializzazione del calcolo delle coordinate metriche
	            gPar.read_p_metric_Tile(0, pmet[0]);
	            gPar.read_p_metric_Tile(2, pmet[1]);
	            rectTmp = MathUtil.gDblRectDefine(pmet[0].X, pmet[0].Y, pmet[1].X, pmet[1].Y);
	            pmet[0].X = (int) (rectTmp.dLeft);
	            pmet[0].Y = (int) (rectTmp.dTop);
	            pmet[1].X = (int) (rectTmp.dRight);
	            pmet[1].Y = (int) (rectTmp.dBottom);
	            ppix[0].X = frameX;
	            ppix[0].Y = frameY;
	            ppix[1].X = finalDimX - frameX;
	            ppix[1].Y = finalDimY - frameY;

	            retVal = gCoord.init(pmet[0], pmet[1], ppix[0], ppix[1]);
	        }

	        catch (Exception ex)
	        {
	            TraceLog.WriteLine("Utils InitCoordinateSystem error ", ex);
	            retVal = false;
	        }

	        return retVal;
	    }


        public bool InitCoordinateSystem()
        {
            // Crop non trattato

            Point[] ppix = new Point[2];
            Point[] pmet = new Point[2];

            for (int i = 0; i < 2; i++)
            {
                ppix[i] = new Point();
                pmet[i] = new Point();
            }

            MathUtil.DBL_RECT rectTmp;

            int frameX = 0, frameY = 0;
            int finalDimX = 0, finalDimY = 0;

            bool retVal = false;

            try
            {
                gPar.read_perspective_correction_frame_params(out frameX, out frameY, out finalDimX,
                    out finalDimY);

                //Inizializzazione del calcolo delle coordinate metriche
                gPar.read_p_metric(0, pmet[0]);
                gPar.read_p_metric(2, pmet[1]);
                rectTmp = MathUtil.gDblRectDefine(pmet[0].X, pmet[0].Y, pmet[1].X, pmet[1].Y);
                pmet[0].X = (int)(rectTmp.dLeft);
                pmet[0].Y = (int)(rectTmp.dTop);
                pmet[1].X = (int)(rectTmp.dRight);
                pmet[1].Y = (int)(rectTmp.dBottom);
                ppix[0].X = frameX;
                ppix[0].Y = frameY;
                ppix[1].X = finalDimX - frameX;
                ppix[1].Y = finalDimY - frameY;

                retVal = gCoord.init(pmet[0], pmet[1], ppix[0], ppix[1]);
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("Utils InitCoordinateSystem error ", ex);
                retVal = false;
            }

            return retVal;
        }


		//private Function gvntGetCommandLine(Optional ByVal vnMaxArgs As Integer = 10) As Variant

		//Public Function gbGetTitleDim(ByRef rForm As Form, ByRef rlWidth As Long, 
		//			ByRef rlHeight As Long, Optional ByVal vnUM As ScaleModeConstants = vbPixels)


		public static DialogResult FormMessageBox(System.Windows.Forms.Form owner, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
		{
			if (owner.InvokeRequired)
			{
				Invoke_DialogResult_MsgBoxParam fmb = new Invoke_DialogResult_MsgBoxParam(FormMessageBox);
				DialogResult dr = (DialogResult)owner.Invoke(fmb, new Object[] { owner, text, caption, buttons, icon });
				return dr;
			}
			else
			{
				return MessageBox.Show(owner, text, caption, buttons, icon);
			}
			
		}
	}
}
