using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;
using Breton.Polygons;
using SlabOperate.MANAGERS;

namespace SlabOperate.LEGACY_FM.UTILS
{
	public class Params
	{
		private string msFileNameINI;
        private GlobDef mGlobDef;

	    //private IniFileManager _stitchConfigFileManager = null;
	    private IniFileManager _defaultManager = null;

	    //private string _stitchConfigFilepath = string.Empty;

		public Params(GlobDef glob)
			: this(glob, glob.CFG_PARAMS_FILENAME)
		{
            
		}

		public Params(GlobDef glob, string param_file)
		{
			mGlobDef = glob;
			msFileNameINI = param_file;
		    _defaultManager = IniFileManager.CreateManager(msFileNameINI);
			//_stitchConfigFilepath = glob.StitchConfigFilepath;
			//_stitchConfigFileManager = IniFileManager.CreateManager(_stitchConfigFilepath);
		}

		//Ini files manipulations
		[DllImport("kernel32", EntryPoint="WritePrivateProfileStringA")]
        private static extern int WritePrivateProfileString(string section, string key, 
			string setting, string filename); 
		
		[DllImport("kernel32.dll", EntryPoint="GetPrivateProfileStringA")]
        static extern Int32 GetPrivateProfileString(string lpAppName, string lpKeyName,
			string lpDefault, StringBuilder lpReturnedString, Int32 nSize, string lpFileName);

		[DllImport("kernel32", EntryPoint="GetPrivateProfileIntA")]
        private static extern Int32 GetPrivateProfileInt(string section, string key, 
			Int32 defaultValue, string filename); 

		
//		public bool is_camera_simulation()
//		{
//			int lVal;
//			lVal = ReadItemInt("Reserved", "IS_CAMERA_SIMULATION", 0);
//			return (lVal != 0);
//		}

		public bool is_setup
		{
			get
			{
				int lVal;
				lVal = ReadItemInt("Reserved", "IS_SETUP", 0);
				return (lVal!=0);
			}
			set
			{
				string s;
				if (value)	s="1";
				else		s="0";
				WriteItem ("Reserved", "IS_SETUP", s);
			}
		}

		
		public void read_p_metric(int index, Point p)
		{
			index++;
            string var = index.ToString(CultureInfo.InvariantCulture).TrimStart(char.Parse(" "));
			string item;  
			item = ReadItem("Params", "P" + var + "_METRIC_X", "0.0");
			p.X = double.Parse(item,CultureInfo.InvariantCulture);
		    
			item = ReadItem("Params", "P" + var + "_METRIC_Y", "0.0");
			p.Y = double.Parse(item,CultureInfo.InvariantCulture);
		}

		public void write_p_metric(int index, Point p)
		{
			index++;
			WriteItem("Params", "P" + (index.ToString().TrimStart()) + "_METRIC_X", (p.X).ToString(CultureInfo.InvariantCulture));
			WriteItem("Params", "P" + (index.ToString().TrimStart()) + "_METRIC_Y", (p.Y).ToString(CultureInfo.InvariantCulture));
		}

        public void read_p_metric_Tile(int index, Point p)
        {
            index++;
            string var = index.ToString(CultureInfo.InvariantCulture).TrimStart(char.Parse(" "));
            string item;
            item = ReadItem("MultiPhotoParams", "P" + var + "_METRIC_X", "0.0");
            p.X = double.Parse(item,CultureInfo.InvariantCulture);

            item = ReadItem("MultiPhotoParams", "P" + var + "_METRIC_Y", "0.0");
            p.Y = double.Parse(item,CultureInfo.InvariantCulture);
        }

        public void write_p_metric_Tile(int index, Point p)
        {
            index++;
            WriteItem("MultiPhotoParams", "P" + (index.ToString().TrimStart()) + "_METRIC_X", (p.X).ToString(CultureInfo.InvariantCulture));
            WriteItem("MultiPhotoParams", "P" + (index.ToString().TrimStart()) + "_METRIC_Y", (p.Y).ToString(CultureInfo.InvariantCulture));
        }


		public void read_p_pixel(int index, Point p)
		{
			index++;
			string var=index.ToString().TrimStart(char.Parse(" "));
			string item;        
			item = ReadItem("Reserved", "P" + var + "_PIXEL_X", "0.0");
			p.X = double.Parse(item,CultureInfo.InvariantCulture);
    
			item = ReadItem("Reserved", "P" + var + "_PIXEL_Y", "0.0");
			p.Y = double.Parse(item,CultureInfo.InvariantCulture);
		}

		public void write_p_pixel(int index, Point p)
		{
			index++;
			WriteItem("Reserved", "P" + (index.ToString().TrimStart()) + "_PIXEL_X", (p.X).ToString(CultureInfo.InvariantCulture));
			WriteItem("Reserved", "P" + (index.ToString().TrimStart()) + "_PIXEL_Y", (p.Y).ToString(CultureInfo.InvariantCulture));
		}

        public void read_p_pixel_Tile(int index, Point p)
        {
            index++;
            string var = index.ToString().TrimStart(char.Parse(" "));
            string item;
            item = ReadItem("MultiPhotoReserved", "P" + var + "_PIXEL_X", "0.0");
            p.X = double.Parse(item,CultureInfo.InvariantCulture);

            item = ReadItem("MultiPhotoReserved", "P" + var + "_PIXEL_Y", "0.0");
            p.Y = double.Parse(item,CultureInfo.InvariantCulture);
        }

        public void write_p_pixel_Tile(int index, Point p)
        {
            index++;
            WriteItem("MultiPhotoReserved", "P" + (index.ToString().TrimStart()) + "_PIXEL_X", (p.X).ToString(CultureInfo.InvariantCulture));
            WriteItem("MultiPhotoReserved", "P" + (index.ToString().TrimStart()) + "_PIXEL_Y", (p.Y).ToString(CultureInfo.InvariantCulture));
        }


		public int get_reference_points_count()
		{
			int lVal;
		    
			lVal = ReadItemInt("Params", "REF_POINT_COUNT", 0);
			if (lVal < 0)
				lVal = 0;
            else if (lVal > mGlobDef.CFG_REFERENCE_POINTS_COUNT_MAX)
                lVal = mGlobDef.CFG_REFERENCE_POINTS_COUNT_MAX;
		    
			return lVal;
		}

		public void read_reference_point(int index, Point p)
		{
			string var=index.ToString().TrimStart(char.Parse(" "));
			string item;
			item = ReadItem("Params", "REFP" + var + "_X", "0.0");
			p.X = double.Parse(item,CultureInfo.InvariantCulture);
		
			item = ReadItem("Params", "REFP" + var + "_Y", "0.0");
			p.Y = double.Parse(item,CultureInfo.InvariantCulture);
		}

		public void read_lens_correction_params(ref bool rbEnable, ref int rnMethod,ref double rdA,
			ref double rdB, ref double rdC, ref double rdD, ref bool rbNormalize)
		{
			string item;
			item = ReadItem("LensCorrection", "Enable", "0");
			if (int.Parse(item)==1) rbEnable=true;
			else				 rbEnable=false;
    
			item = ReadItem("LensCorrection", "Method", "2");
			rnMethod = int.Parse(item);

			item = ReadItem("LensCorrection", "A", "0.0");
			rdA = double.Parse(item,CultureInfo.InvariantCulture);
    
			item = ReadItem("LensCorrection", "B", "-0.013");
			rdB = double.Parse(item,CultureInfo.InvariantCulture);
    
			item = ReadItem("LensCorrection", "C", "0.0");
			rdC = double.Parse(item,CultureInfo.InvariantCulture);
    
			item = ReadItem("LensCorrection", "D", "1.013");
			rdD = double.Parse(item,CultureInfo.InvariantCulture);
    
			item = ReadItem("LensCorrection", "Normalize", "1");
			if (int.Parse(item)==1)	rbNormalize=true;
			else					rbNormalize=false;
		}

		public void read_light_autocorrection_params(ref bool rbEnable,ref int rnMethod)
		{
			string item;        
			item = ReadItem("LightAutoCorrection", "Enable", "0");
			if (int.Parse(item)==1)	rbEnable=true;
			else					rbEnable=false;
    
			item = ReadItem("LightAutoCorrection", "Method", "2");
			rnMethod = int.Parse(item);
		}
		
		public void read_perspective_correction_params(ref bool rbEnable, ref int rnMethod,
			ref int rlFrameX, ref int rlFrameY,
			ref int rlFinalDimX, ref int rlFinalDimY,
			ref float rfPixelRatio, 
			ref int dpi)
		{
			string item;   
			item = ReadItem("PerspectiveCorrection", "Enable", "0");
			if (int.Parse(item)==1)	rbEnable=true;
			else					rbEnable=false;
    
			item = ReadItem("PerspectiveCorrection", "Method", "2");
			rnMethod = int.Parse(item);

			item = ReadItem("PerspectiveCorrection", "FrameX", "50");
			rlFrameX = int.Parse(item);
    
			item = ReadItem("PerspectiveCorrection", "FrameY", "50");
			rlFrameY = int.Parse(item);
    
			item = ReadItem("PerspectiveCorrection", "FinalDimensionX", "3690");
			rlFinalDimX = int.Parse(item);
    
			item = ReadItem("PerspectiveCorrection", "FinalDimensionY", "2100");
			rlFinalDimY = int.Parse(item);
    
			item = ReadItem("PerspectiveCorrection", "PixelRatio", "0");
			rfPixelRatio = float.Parse(item);

			item = ReadItem("PerspectiveCorrection", "DPI", "0");
			dpi = int.Parse(item);
		}    

		public void write_perspective_correction_params(ref bool rbEnable, ref int rnMethod,
			ref int rlFrameX, ref int rlFrameY,
			ref int rlFinalDimX, ref int rlFinalDimY,
			ref float rfPixelRatio, 
			ref int dpi)
		{
			string s;
			if (rbEnable)	s="1";
			else			s="0";
	
			WriteItem("PerspectiveCorrection", "Enable", s);
    
			WriteItem("PerspectiveCorrection", "Method", rnMethod.ToString());

			WriteItem("PerspectiveCorrection", "FrameX", rlFrameX.ToString(CultureInfo.InvariantCulture));
    
			WriteItem("PerspectiveCorrection", "FrameY", rlFrameY.ToString(CultureInfo.InvariantCulture));
    
			WriteItem("PerspectiveCorrection", "FinalDimensionX", rlFinalDimX.ToString(CultureInfo.InvariantCulture));
    
			WriteItem("PerspectiveCorrection", "FinalDimensionY", rlFinalDimY.ToString(CultureInfo.InvariantCulture));
    
			WriteItem("PerspectiveCorrection", "PixelRatio", rfPixelRatio.ToString(CultureInfo.InvariantCulture));

			WriteItem("PerspectiveCorrection", "DPI", dpi.ToString(CultureInfo.InvariantCulture));
		}



        public void read_perspective_correction_frame_params(out int frameX, out int frameY,
            out int finalDimensionX, out int finalDimensionY)
        {
            string item;

            item = ReadItem("PerspectiveCorrection", "FrameX", "50");
            frameX = int.Parse(item);

            item = ReadItem("PerspectiveCorrection", "FrameY", "50");
            frameY = int.Parse(item);

            item = ReadItem("PerspectiveCorrection", "FinalDimensionX", "3690");
            finalDimensionX = int.Parse(item);

            item = ReadItem("PerspectiveCorrection", "FinalDimensionY", "2100");
            finalDimensionY = int.Parse(item);

        }    

        public void read_perspective_correction_frame_params_tile(out int frameX, out int frameY,
            out int finalDimensionX, out int finalDimensionY)
        {
            string item;

            item = ReadItem("PerspectiveCorrection", "FrameX", "50");
            frameX = int.Parse(item);

            item = ReadItem("PerspectiveCorrection", "FrameY", "50");
            frameY = int.Parse(item);

            item = ReadItem("MultiPhotoPerspectiveCorrection", "FinalDimensionX", "3690");
            finalDimensionX = int.Parse(item);

            item = ReadItem("MultiPhotoPerspectiveCorrection", "FinalDimensionY", "2100");
            finalDimensionY = int.Parse(item);

        }    


		public void read_pos_stop(ref double x, ref bool rbEnable)
		{
			string item;
			int lRet;

		    double value;

			item = ReadItem("Reserved", "P_STOP_X", "");
		    if (!string.IsNullOrEmpty(item) && double.TryParse(item, out value))
		    {
                x = value;
                rbEnable = true;
            }
		    else
		    {
                x = 0;
                rbEnable = false;
            }
		}
	
		public void write_pos_stop(double x)
		{
			WriteItem("Reserved", "P_STOP_X", x.ToString(CultureInfo.InvariantCulture));
		}

		public void write_pos_stop(string x)
		{
			WriteItem("Reserved", "P_STOP_X", x);
		}
	
		public void read_pt_refers(ref Point pt1, ref Point pt2, ref bool rbEnable)
		{
		    string item;
		    double value;
			try
			{
				item = ReadItem("Reserved", "P1_REF_X", "", true);
			    if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
			    {
                    pt1.X = value;
			    }
				

                item = ReadItem("Reserved", "P1_REF_Y", "", true);
                if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    pt1.Y = value;
                }


                item = ReadItem("Reserved", "P2_REF_X", "", true);
                if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    pt2.X = value;
                }

                item = ReadItem("Reserved", "P2_REF_Y", "", true);
                if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    pt2.Y = value;
                }

	    
				rbEnable = true;
			}
			catch (Exception)
			{ 
				pt1.X = 0;
				pt1.Y = 0;
				pt2.X = 0;
				pt2.Y = 0;
				rbEnable = false;
			}
		}
		
		public void write_pt_refers(ref Point pt1, ref Point pt2)
		{    
			WriteItem("Reserved", "P1_REF_X", pt1.X.ToString(CultureInfo.InvariantCulture));
			WriteItem("Reserved", "P1_REF_Y", pt1.Y.ToString(CultureInfo.InvariantCulture));
			WriteItem("Reserved", "P2_REF_X", pt2.X.ToString(CultureInfo.InvariantCulture));
			WriteItem("Reserved", "P2_REF_Y", pt2.Y.ToString(CultureInfo.InvariantCulture));
		}

		public void write_pt_refers(string pt1X, string pt1Y, string pt2X, string pt2Y)
		{    
			WriteItem("Reserved", "P1_REF_X", pt1X);
			WriteItem("Reserved", "P1_REF_Y", pt1Y);
			WriteItem("Reserved", "P2_REF_X", pt2X);
			WriteItem("Reserved", "P2_REF_Y", pt2Y);
		}

		public void read_camera_params(ref int rnRetry, ref int rlWaitRetry, ref int rlWaitConnect, ref int rlWaitGateOpen)
		{
			string item;
			item = ReadItem("Camera", "Retry", "3");
			rnRetry = int.Parse(item);

			item = ReadItem("Camera", "WaitRetry", "5000");
			rlWaitRetry = int.Parse(item);

			item = ReadItem("Camera", "WaitConnect", "5000");
			rlWaitConnect = int.Parse(item);

			item = ReadItem("Camera", "WaitGateOpen", "3000");
			rlWaitGateOpen = int.Parse(item);
		}

		public void read_thickness(ref float rfValue)
		{
			string item;
			item = ReadItem("General", "SlabThickness ", "20.0");
			rfValue = float.Parse(item);
		}

		public void write_thickness(float vfValue)
		{
			WriteItem("General", "SlabThickness ", vfValue.ToString(CultureInfo.InvariantCulture));
		}

		public int read_um()
		{
			return ReadItemInt("General", "UM", 0);
		}

		/// **************************************************************************
		/// <summary>
		/// Legge i punti di ritaglio immagine
		/// </summary>
		/// <param name="pt1">punto 1 del rettangolo da ritagliare</param>
		/// <param name="pt2">punto 2 del rettangolo da ritagliare</param>
		/// <param name="rbEnable">abilitazione del ritaglio immagine</param>
		/// **************************************************************************
		public void read_pt_cropimage(ref Point pt1, ref Point pt2, ref bool rbEnable)
		{
			int lRet;
		    string item;
		    int value;
		    double dblValue;
			try
			{
                item = ReadItem("CropImage", "Enable", "0");
			    if (int.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out value) && value==1)
			    {
                    rbEnable = true;
			    }
			    else
			    {
                    rbEnable = false;
			    }


				if (rbEnable)
				{
                    item = ReadItem("CropImage", "P1_CROP_X", "", true);
                    if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out dblValue))
                    {
                        pt1.X = dblValue;
                    }


                    item = ReadItem("CropImage", "P1_CROP_Y", "", true);
                    if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out dblValue))
                    {
                        pt1.Y = dblValue;
                    }


                    item = ReadItem("CropImage", "P2_CROP_X", "", true);
                    if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out dblValue))
                    {
                        pt2.X = dblValue;
                    }

                    item = ReadItem("CropImage", "P2_CROP_Y", "", true);
                    if (double.TryParse(item, NumberStyles.Any, CultureInfo.InvariantCulture, out dblValue))
                    {
                        pt2.Y = dblValue;
                    }

				}
				else
				{
					pt1.X = 0;
					pt1.Y = 0;
					pt2.X = 0;
					pt2.Y = 0;
				}

			}
			catch (Exception)
			{
				pt1.X = 0;
				pt1.Y = 0;
				pt2.X = 0;
				pt2.Y = 0;
				rbEnable = false;
			}
		}

		/// **************************************************************************
		/// <summary>
		/// Scrive i punti di ritaglio immagine
		/// </summary>
		/// <param name="pt1">punto 1 del rettangolo da ritagliare</param>
		/// <param name="pt2">punto 2 del rettangolo da ritagliare</param>
		/// <param name="rbEnable">abilitazione del ritaglio immagine</param>
		/// **************************************************************************
		public void write_pt_cropimage(ref Point pt1, ref Point pt2, ref bool enable)
		{
			string s;
			if (enable) s = "1";
			else s = "0";

			WriteItem("CropImage", "Enable", s);
			WriteItem("CropImage", "P1_CROP_X", pt1.X.ToString(CultureInfo.InvariantCulture));
			WriteItem("CropImage", "P1_CROP_Y", pt1.Y.ToString(CultureInfo.InvariantCulture));
			WriteItem("CropImage", "P2_CROP_X", pt2.X.ToString(CultureInfo.InvariantCulture));
			WriteItem("CropImage", "P2_CROP_Y", pt2.Y.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Legge i parametri di riferimento per relativi agli spessori
		/// </summary>
		/// <param name="rbEnable">indica se la gestione spessori � attiva</param>
		/// <param name="rnLastThicknessIndex">ultimo spessore utilizzato</param>
		/// <param name="thicknesslist">lista spessori</param>
		public void read_thickness_param(out bool rbEnable, out int rnLastThicknessIndex, out SortedList<int, int> thicknesslist)
		{
			string item;
			item = ReadItem("General", "Enable ", "0");
			if (int.Parse(item) == 1) 
				rbEnable = true;
			else
				rbEnable = false;

			item = ReadItem("General", "LastUsedThicknessIndex", "0");
			rnLastThicknessIndex = int.Parse(item);

			thicknesslist = new SortedList<int, int>();
			// Continua il ciclo finch� trova dei valori
			bool bContinue = true;
			int i = 0;
			while (bContinue)
			{
				item = ReadItem("Thicknesses", i.ToString(CultureInfo.InvariantCulture), "");
				if (item == "")
					bContinue = false;
				else
				{
					thicknesslist.Add(i, int.Parse(item));
					i++;
				}
			}
		}

		/// <summary>
		/// Scrive il valore dell'ultimo spessore utilizzato
		/// </summary>
		/// <param name="rnLastThicknessIndex">ultimo spessore utilizzato</param>
		public void write_last_thickness(int rnLastThicknessIndex)
		{
			WriteItem("General", "LastUsedThicknessIndex", rnLastThicknessIndex.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Legge il punto di riferimento interessato in base allo spessore richiesto
		/// </summary>
		/// <param name="index">punto di riferimento richiesto</param>
		/// <param name="thickness_index">spessore richiesto</param>
		/// <param name="p">punto</param>
		public void read_p_pixel_thickness(int index, int thickness_index, Point p)
		{
			index++;
			string var = index.ToString().TrimStart(char.Parse(" "));
			string item;
            item = ReadItem("Setting_" + thickness_index.ToString(CultureInfo.InvariantCulture), "P" + var + "_PIXEL_X", "0.0");
			p.X = double.Parse(item,CultureInfo.InvariantCulture);

            item = ReadItem("Setting_" + thickness_index.ToString(CultureInfo.InvariantCulture), "P" + var + "_PIXEL_Y", "0.0");
			p.Y = double.Parse(item,CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Scrive il punto di riferimento interessato in base allo spessore richiesto
		/// </summary>
		/// <param name="index">punto di riferimento richiesto</param>
		/// <param name="thickness_index">spessore richiesto</param>
		/// <param name="p">punto</param>
		public void write_p_pixel_thickness(int index, int thickness_index, Point p)
		{
			index++;
            WriteItem("Setting_" + thickness_index.ToString(CultureInfo.InvariantCulture), "P" + (index.ToString(CultureInfo.InvariantCulture).TrimStart()) + "_PIXEL_X", (p.X).ToString(CultureInfo.InvariantCulture));
            WriteItem("Setting_" + thickness_index.ToString(CultureInfo.InvariantCulture), "P" + (index.ToString(CultureInfo.InvariantCulture).TrimStart()) + "_PIXEL_Y", (p.Y).ToString(CultureInfo.InvariantCulture));
        }


        

        private int ReadItemInt(string section, string key, int defaultValue = 0)
        {
            if (_defaultManager != null)
                return _defaultManager.ReadItemInt(section, key, defaultValue);
            
            return defaultValue;
        }

        private string ReadItem(string section, string key, string defaultValue = "", bool throwExceptionIfVoid = false)
        {
            if (_defaultManager != null)
                return _defaultManager.ReadItem(section, key, defaultValue, throwExceptionIfVoid);

            return defaultValue;
            
        }

        private int WriteItem(string section, string key, string value)
        {
            if (_defaultManager != null)
                return _defaultManager.WriteItem(section, key, value);

            return -1;
        }

        public bool EnableLensCorrection
        {
            get
            {
                int lVal = ReadItemInt("LensCorrection", "Enable", 0);
                return (lVal != 0);
            }
            set
            {
                WriteItem("LensCorrection", "Enable", (value ? "1" : "0"));
            }
        }

        public bool EnableLightCorrection
        {
            get
            {
                int lVal = ReadItemInt("LightAutoCorrection", "Enable", 0);
                return (lVal != 0);
            }
            set
            {
                WriteItem("LightAutoCorrection", "Enable", (value ? "1" : "0"));
            }
        }


        public int FrameX
        {
            get
            {
                return ReadItemInt("PerspectiveCorrection", "FrameX", 50);
            }
        }

        public int FrameY
        {
            get
            {
                return ReadItemInt("PerspectiveCorrection", "FrameY", 50);
            }
        }




    }
}
