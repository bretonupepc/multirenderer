﻿using System;
using System.Globalization;
using System.Xml;
using Breton.Polygons;
using TraceLoggers;

namespace SlabOperate.LEGACY_FM.UTILS
{
    public static class SideExtensions
    {
        private static bool GetFromXml(this Segment segment, XmlNode node)
        {

            try
            {
                var x1 = node.SelectSingleNode("X1");
                var y1 = node.SelectSingleNode("Y1");
                var x2 = node.SelectSingleNode("X2");
                var y2 = node.SelectSingleNode("Y2");
                segment.mP1.X = Convert.ToDouble(x1.InnerText, CultureInfo.InvariantCulture);
                segment.mP1.Y = Convert.ToDouble(y1.InnerText, CultureInfo.InvariantCulture);
                segment.mP2.X = Convert.ToDouble(x2.InnerText, CultureInfo.InvariantCulture);
                segment.mP2.Y = Convert.ToDouble(y2.InnerText, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Segment.GetFromXml Error.", ex);
                return false;
            }

        }

        private static bool GetFromXml(this Arc arc, XmlNode node)
        {

            try
            {
                Point centerpoint = new Point();
                centerpoint.X = Convert.ToDouble(node.SelectSingleNode("Xc").InnerText, CultureInfo.InvariantCulture);
                centerpoint.Y = Convert.ToDouble(node.SelectSingleNode("Yc").InnerText, CultureInfo.InvariantCulture);
                arc.CenterPoint = centerpoint;

                arc.Radius = Convert.ToDouble(node.SelectSingleNode("Radius").InnerText, CultureInfo.InvariantCulture);
                arc.StartAngle = Convert.ToDouble(node.SelectSingleNode("Alpha").InnerText, CultureInfo.InvariantCulture);
                arc.AmplAngle = Convert.ToDouble(node.SelectSingleNode("Beta").InnerText, CultureInfo.InvariantCulture);
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Arc.GetFromXml Error.", ex);
                return false;
            }

        }

        public static bool GetFromXml(this Side side, XmlNode node)
        {

            try
            {
                var arc = side as Arc;
                if (arc != null)
                {
                    return arc.GetFromXml(node);
                }

                var segment = side as Segment;
                if (segment != null)
                {
                    return segment.GetFromXml(node);
                }
                return false;

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Side.GetFromXml Error.", ex);
                return false;
            }

        }

    }
    public static class StringExtension
    {
        // This is the extension method.
        // The first parameter takes the "this" modifier
        // and specifies the type for which the method is defined.
        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }

}
