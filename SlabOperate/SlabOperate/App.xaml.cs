﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using SlabOperate.SESSION;
using SlabOperate.WINDOWS;
using TraceLoggers;

namespace SlabOperate
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Mutex _mutex;

        bool _createdNew = true;

        public App()
        {
            this.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

        }

        protected override void OnStartup(StartupEventArgs e)
        {

            _mutex = new Mutex(true, ApplicationRun.APP_NAME, out _createdNew);

            if (_createdNew)
            {
                base.OnStartup(e);

                string errors;
                if (!ApplicationRun.Instance.Initialize(out errors))
                {
                    ApplicationRun.Instance.MessageService.ShowMessage(string.Format("Errori in avvio:\n\n{0}", errors));
                    Shutdown();
                }


                //var win = new MainWindow();
                //var win = new SlabOperationsWindow();
                var win = new TestWindow();

                win.Show();

                this.MainWindow = win;


            }

            else
            {

                MessageBox.Show("Applicazione già avviata",
                                ApplicationRun.APP_NAME,
                                MessageBoxButton.OK, MessageBoxImage.Exclamation);

                Current.Shutdown();
            }


        }


        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // Handle the exception when AdomdClient is not installed
            //if (e.Exception.InnerException != null)
            //{
            //    var result = MessageBox.Show(e.Exception.Message, "Errore applicazione", MessageBoxButton.OK, MessageBoxImage.Hand);
            //    //if (result == MessageBoxResult.Yes)
            //    //{
            //    //    System.Diagnostics.Process.Start("http://www.microsoft.com/en-us/download/details.aspx?id=16978");
            //    //}

            //    //((SamplesBrowserWindow)(App.Current.MainWindow)).frSample.Navigate(new Uri("Infragistics.SamplesBrowser;component/WelcomePages/PivotGridWelcome.xaml", UriKind.Relative));
            //    e.Handled = true;
            //    return;
            //}

            // TODO: Uncomment the following lines after debug phase. (GH 9-21-2007)
            TraceLog.WriteLine("Application error ", e.Exception);
            //System.Diagnostics.Debug.WriteLine(e.Exception.ToString());
            HandleException(e.Exception);
            e.Handled = true;
        }
        public static void HandleException(Exception ex)
        {
            if (ex == null)
                return;
            var displayMessage = "Eccezione applicazione " + System.Environment.NewLine + System.Environment.NewLine;
            while ((ex is System.Reflection.TargetInvocationException || ex is System.Windows.Markup.XamlParseException) && ex.InnerException != null)
                ex = ex.InnerException;
#if (DEBUG)
            displayMessage += ex.ToString();
#else
            displayMessage += ex.Message;
#endif
            MessageBox.Show(displayMessage, "Eccezione applicazione ", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            TraceLog.Close();
        }
    }


}
