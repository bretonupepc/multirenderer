﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using SlabOperate.Business.Entities;
using TraceLoggers;

namespace SlabOperate.MANAGERS
{
    public class DbParametersManager
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private PersistenceScope _dataScope;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public DbParametersManager(PersistenceScope scope)
        {
            _dataScope = scope;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        internal  bool GetSingleParameter(string parameterName, out string readValue)
        {
            bool retVal = false;
            readValue = string.Empty;

            try
            {
                var dbParameters = new PersistableEntityCollection<ParameterEntity>(_dataScope);
                FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("Name"), ConditionOperator.Eq, parameterName);

                if (dbParameters.GetItemsFromRepository(clause: filterClause) && dbParameters.Count > 0)
                {
                    readValue = dbParameters[0].Value;
                    retVal = true;
                }
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DbParameterManager:GetSingleParameter error: ", ex);
                retVal = false;
            }

            return retVal;
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
