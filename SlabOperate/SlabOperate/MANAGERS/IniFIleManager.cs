﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TraceLoggers;

namespace SlabOperate.MANAGERS
{
    public class IniFileManager
    {
        string _iniFilepath;

        [DllImport("kernel32")]
        static extern int WritePrivateProfileString(string section, string key, string value, string filePath);

        [DllImport("kernel32")]
        static extern Int32 GetPrivateProfileString(string section, string key, string Default, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32", EntryPoint = "GetPrivateProfileIntA")]
        private static extern Int32 GetPrivateProfileInt(string section, string key,
            Int32 defaultValue, string filename);


        public static IniFileManager CreateManager(string iniFilepath)
        {
            if (File.Exists(iniFilepath))
            {
                return new IniFileManager(iniFilepath);
            }
            else
            {
                TraceLog.WriteLine(string.Format("IniFileManager CreateManager error: File <{0}> does not exists", iniFilepath));
                return null;
            }
        }

        private IniFileManager(string iniFilepath)
        {
            _iniFilepath = iniFilepath;
        }

        public int ReadItemInt(string section, string key, int defaultValue = 0)
        {
            int retVal = GetPrivateProfileInt(section, key, defaultValue, _iniFilepath);
            return retVal;
        }

        public string ReadItem(string section, string key, string defaultValue = "", bool throwExceptionIfVoid = false)
        {
            var retVal = new StringBuilder(255);
            int ret = GetPrivateProfileString(section, key, defaultValue, retVal, 255, _iniFilepath);
            if (throwExceptionIfVoid && ret <= 0)
            {
                throw new Exception(string.Format("INI File void item [{0}].[{1}] in file <{3}>", section, key,
                    _iniFilepath));
            }
            return retVal.ToString();
        }

        public int WriteItem(string section, string key, string value)
        {
            return WritePrivateProfileString(section, key, value, _iniFilepath);
        }

        public void DeleteKey(string section, string key)
        {
            WriteItem(section, key, null);
        }

        public void DeleteSection(string section)
        {
            WriteItem(section, null, null);
        }

        public bool KeyExists(string section, string key)
        {
            return ReadItem(section, key).Length > 0;
        }

        public List<string> GetSections()
        {
            List<string> result = new List<string>();
            var strList = new StringBuilder(2048);
            var retVal = GetPrivateProfileString(null, null, "", strList, strList.Length, _iniFilepath);
            if (retVal > 0)
            {
                string[] sections = strList.ToString().Split('\0');
                for (int i = 0; i < sections.Length; i++)
                {
                    var item = sections[i];
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (item.StartsWith("\0"))
                            break;
                        else
                        {
                            result.Add(item);
                        }
                    }
                }
            }

            return result;
        }

        public bool SectionExists(string section)
        {
            return GetSections().Contains(section);
        }

        public void CleanFile(List<string> sectionsToPreserve)
        {
            var sections = GetSections();
            foreach (var section in sections)
            {
                if (!sectionsToPreserve.Contains(section))
                {
                    DeleteSection(section);
                }
            }
        }
    }
}
