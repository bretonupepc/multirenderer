﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace SlabOperate.CONVERTERS
{
    public static class Validator
    {

        public static bool IsValid(DependencyObject parent)
        {
            // Validate all the bindings on the parent
            bool valid = true;
            LocalValueEnumerator localValues = parent.GetLocalValueEnumerator();

            while (localValues.MoveNext())
            {
                LocalValueEntry entry = localValues.Current;
                if (entry.Property.Name == "Text")
                {
                    if (BindingOperations.IsDataBound(parent, entry.Property))
                    {
                        Binding binding = BindingOperations.GetBinding(parent, entry.Property);
                        if (binding != null)
                            foreach (ValidationRule rule in binding.ValidationRules)
                            {
                                ValidationResult result = rule.Validate(parent.GetValue(entry.Property), null);
                                if (!result.IsValid)
                                {
                                    BindingExpression expression = BindingOperations.GetBindingExpression(parent, entry.Property);
                                    if (expression != null)
                                    {
                                        System.Windows.Controls.Validation.MarkInvalid(expression, new ValidationError(rule, expression, result.ErrorContent, null));
                                        valid = false;
                                        break;
                                    }
                                }
                            }
                    }
                }

            }

            // Validate all the bindings on the children
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child is TextBox)
                {
                    if (!IsValid(child))
                    {
                        valid = false;
                        break;
                    }
                }
            }

            return valid;
        }

    }
}
