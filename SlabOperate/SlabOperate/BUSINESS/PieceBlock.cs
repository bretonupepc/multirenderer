﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Breton.BCamPath;
using Breton.DesignCenterInterface;
using Breton.DesignUtil;
using Breton.MathUtils;
using Breton.Polygons;
using RenderMaster.Wpf.BUSINESS;
using SlabOperate.Business.Entities.WORK.DETAILS;
using SlabOperate.SESSION;
using TraceLoggers;
using ObservableObject = DataAccess.Business.BusinessBase.ObservableObject;
using Path = Breton.Polygons.Path;
using Shape = Breton.DesignUtil.Shape;

namespace SlabOperate.BUSINESS
{
    public class PieceBlock: ObservableObject
    {
        #region >-------------- Constants and Enums

        private const double CAM_CHORDAL_ERROR = 1e-2;

        public const string DEFAULT_MATERIAL_IMAGE_NAME = "default.jpg";

        private const int RENDER_DELAY = 200;

        private readonly int TEXTURE_SIZE = RenderMaster.Wpf.Render.TEXTURE_SIZE;

        // De Conti 20170310
        private readonly double TEXTURE_SIZE_RATIO = RenderMaster.Wpf.Render.TEXTURE_SIZE_RATIO;


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private WkOrderDetailPieceEntity _piece;

        private Block _block;

        private Shape _shape;

        private Shape _projectShape;

        private Shape _pieceShape;

        private bool _isAssignedToSLab = false;

        private System.Windows.Shapes.Path _wpfPath;

        private string _wpfPathData;

        private int _placedBlockId = -1;


        #region Render related

        private string _materialImageFilepath = System.IO.Path.Combine(ApplicationRun.Instance.TempFolder, DEFAULT_MATERIAL_IMAGE_NAME);
        private ToBigRender _toBigRender ;

        #endregion

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public PieceBlock(WkOrderDetailPieceEntity piece)
        {
            _piece = piece;
            BuildBlock();

            _toBigRender = new ToBigRender(-1, _materialImageFilepath);
        }

        private LineGeometry NewWpfLine(double x0, double y0, double x1, double y1)
        {
            return new LineGeometry(new System.Windows.Point(x0, y0), new System.Windows.Point(x1, y1));
        }
        private LineSegment NewLineSegment(double x1, double y1)
        {
            return new LineSegment( new System.Windows.Point(x1, y1), false);
        }
        private ArcSegment NewArcSegment(double x1, double y1, double radius, double rotationAngle, bool isLargeArc = false, SweepDirection sweepDirection = SweepDirection.Clockwise)
        {
            return new ArcSegment(new System.Windows.Point(x1, y1),new Size(radius *2, radius * 2), rotationAngle, isLargeArc, sweepDirection, true);
        }

        private void BuildBlock()
        {
            //TODO:
            #region Project shape
            // Estrae le geometrie della shape di progetto,
            // usata per la creazione del blocco di base e delle entità Path wpf


            #endregion

            Shape = new Shape();
            _projectShape = new Shape();
            XmlAnalizer.ReadShapeXml(Piece.OrderDetail.ShapeInfoFilepath, ref _projectShape, true);
            XmlAnalizer.ReadShapeXml(Piece.OrderDetail.ShapeInfoFilepath, ref _shape, false);

            #region Block
            Block b = new Block();
            double x, y, a;
            RTMatrix rtm = XmlAnalizer.GetMatrice(Piece.OrderDetail.ShapeInfoFilepath);
            rtm.GetRotoTransl(out x, out y, out a);

            //_shape.gMatrix = new Shape.Matrix();
            //_shape.gMatrix.

            #region Pezzo Finito (Layer SHAPE e SHAPE_HOLE)

            Path externalPath = new Path();
            List<Path> holes = new List<Path>();
            List<Path> pockets = new List<Path>();
            List<Path> grooves = new List<Path>();
            int lastHoleId = -1;
            int lastPocketId = -1;
            int lastGrooveId = -1;

            WpfPath = new System.Windows.Shapes.Path();

            var wpfGeometry = new PathGeometry();
            var wpfFigures = new PathFigureCollection();

            PathFigure wpfFigure = null;

            PathFigure wpfHoleFigure = null;

            PathFigure wpfPocketFigure = null;

            PathSegment wpfSegment;
            


            for (int j = 0; j < _shape.gContours.Count; j++)
            {
                Shape.Contour contour = (Breton.DesignUtil.Shape.Contour)_shape.gContours[j];

                if (contour.pricipalStruct)

                #region Main structure
                {
                    externalPath.Layer = CBPExtension.LayersType.SHAPE.ToString();

                    if (contour.line)
                    {
                        externalPath.AddSide(new Segment(contour.x0, contour.y0, contour.x1, contour.y1));

                        if (wpfFigure == null)
                        {
                            wpfFigure = new PathFigure();
                            wpfFigure.StartPoint = new System.Windows.Point(contour.x0, contour.y0);
                        }

                        wpfSegment = NewLineSegment(contour.x1, contour.y1);
                        wpfFigure.Segments.Add(wpfSegment);

                    }
                    else
                    {
                        double alpha = MathUtil.gdRadianti(contour.startAngle);
                        double beta = MathUtil.gdRadianti(contour.endAngle);
                        double amp = beta - alpha;
                        if (contour.cw == 0)                                          // antiorario
                            amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                        else
                            amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                        Arc aTmp = new Arc(contour.xC, contour.yC, contour.r, alpha, amp);
                        externalPath.AddSide(aTmp);


                        if (wpfFigure == null)
                        {
                            wpfFigure = new PathFigure();
                            wpfFigure.StartPoint = new System.Windows.Point(aTmp.P1.X, aTmp.P1.Y);
                        }

                        wpfSegment = NewArcSegment(aTmp.P2.X, aTmp.P2.Y, aTmp.Radius, contour.endAngle - contour.startAngle);
                        wpfFigure.Segments.Add(wpfSegment);

                    }
                } 
                #endregion

                else if (contour.throughHole)

                #region Holes
                {
                    if (contour.id != lastHoleId)
                    {
                        lastHoleId = contour.id;
                        holes.Add(new Path());

                        if (wpfHoleFigure != null)
                        {
                            wpfFigures.Add(wpfHoleFigure);
                            wpfHoleFigure = null;
                        }

                    }

                    holes[holes.Count - 1].Layer = CBPExtension.LayersType.SHAPE_HOLE.ToString();

                    if (contour.line)
                    {
                        holes[holes.Count - 1].AddSide(new Segment(contour.x0, contour.y0, contour.x1, contour.y1));

                        if (wpfHoleFigure == null)
                        {
                            wpfHoleFigure = new PathFigure();
                            wpfHoleFigure.StartPoint = new System.Windows.Point(contour.x0, contour.y0);
                        }
                        wpfSegment = NewLineSegment(contour.x1, contour.y1);
                        wpfHoleFigure.Segments.Add(wpfSegment);


                    }
                    else
                    {
                        double alpha = MathUtil.gdRadianti(contour.startAngle);
                        double beta = MathUtil.gdRadianti(contour.endAngle);
                        double amp = beta - alpha;
                        if (contour.cw == 0)                                          // antiorario
                            amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                        else
                            amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                        Arc aTmp = new Arc(contour.xC, contour.yC, contour.r, alpha, amp);

                        holes[holes.Count - 1].AddSide(aTmp);

                        if (wpfHoleFigure == null)
                        {
                            wpfHoleFigure = new PathFigure();
                            wpfHoleFigure.StartPoint = new System.Windows.Point(aTmp.P1.X, aTmp.P1.Y);
                        }

                        wpfSegment = NewArcSegment(aTmp.P2.X, aTmp.P2.Y, aTmp.Radius, contour.endAngle - contour.startAngle);
                        wpfHoleFigure.Segments.Add(wpfSegment);

                    }
                } 
                #endregion

                else if (contour.pocket)

                #region Pockets
                {
                    if (contour.id != lastPocketId)
                    {
                        lastPocketId = contour.id;
                        pockets.Add(new Path());

                        if (wpfPocketFigure != null)
                        {
                            wpfFigures.Add(wpfPocketFigure);
                            wpfPocketFigure = null;
                        }

                    }

                    pockets[pockets.Count - 1].Layer = CBPExtension.LayersType.POCKET.ToString();

                    if (contour.line)
                    {
                        pockets[pockets.Count - 1].AddSide(new Segment(contour.x0, contour.y0, contour.x1, contour.y1));

                        if (wpfPocketFigure == null)
                        {
                            wpfPocketFigure = new PathFigure();
                            wpfPocketFigure.StartPoint = new System.Windows.Point(contour.x0, contour.y0);
                        }
                        wpfSegment = NewLineSegment(contour.x1, contour.y1);
                        wpfPocketFigure.Segments.Add(wpfSegment);

                    }
                    else
                    {
                        double alpha = MathUtil.gdRadianti(contour.startAngle);
                        double beta = MathUtil.gdRadianti(contour.endAngle);
                        double amp = beta - alpha;
                        if (contour.cw == 0)                                          // antiorario
                            amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                        else
                            amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                        Arc aTmp = new Arc(contour.xC, contour.yC, contour.r, alpha, amp);

                        pockets[pockets.Count - 1].AddSide(aTmp);

                        if (wpfPocketFigure == null)
                        {
                            wpfPocketFigure = new PathFigure();
                            wpfPocketFigure.StartPoint = new System.Windows.Point(aTmp.P1.X, aTmp.P1.Y);
                        }

                        wpfSegment = NewArcSegment(aTmp.P2.X, aTmp.P2.Y, aTmp.Radius, contour.endAngle - contour.startAngle);
                        wpfPocketFigure.Segments.Add(wpfSegment);

                    }
                } 
                #endregion

                else if (contour.groove)

                #region Grooves
                {
                    if (contour.id != lastGrooveId)
                    {
                        lastGrooveId = contour.id;
                        grooves.Add(new Path());
                    }

                    grooves[grooves.Count - 1].Layer = CBPExtension.LayersType.GROOVE.ToString();

                    if (contour.line)
                        grooves[grooves.Count - 1].AddSide(new Segment(contour.x0, contour.y0, contour.x1, contour.y1));
                    else
                    {
                        double alpha = MathUtil.gdRadianti(contour.startAngle);
                        double beta = MathUtil.gdRadianti(contour.endAngle);
                        double amp = beta - alpha;
                        if (contour.cw == 0)                                          // antiorario
                            amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                        else
                            amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                        Arc aTmp = new Arc(contour.xC, contour.yC, contour.r, alpha, amp);

                        grooves[grooves.Count - 1].AddSide(aTmp);
                    }
                } 
                #endregion
            }



            b.AddPath(externalPath);

            if (wpfFigure != null)
                wpfFigures.Add(wpfFigure);
            if (wpfHoleFigure != null)
            {
                wpfFigures.Add(wpfHoleFigure);
            }
            if (wpfPocketFigure != null)
            {
                wpfFigures.Add(wpfPocketFigure);
            }

            WpfPath.Data = new PathGeometry(wpfFigures);

            WpfPathData = WpfPath.Data.ToString(CultureInfo.InvariantCulture);

            foreach (Path h in holes)
                b.AddPath(h);

            foreach (Path g in grooves)
                b.AddPath(g);

            foreach (Path p in pockets)
                b.AddPath(p);
            #endregion

            #region Bussole
            Path blindHole;
            for (int j = 0; j < _shape.gBussole.Count; j++)
            {
                blindHole = new Path();
                Breton.DesignUtil.Shape.Bussola bus = (Breton.DesignUtil.Shape.Bussola)_shape.gBussole[j];

                for (int k = 0; k < bus.sides.Count; k++)
                {
                    Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)bus.sides[k];

                    blindHole.Layer = CBPExtension.LayersType.BLIND_HOLE.ToString();

                    if (s.line)
                        blindHole.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
                    else
                    {
                        double alpha = MathUtil.gdRadianti(s.startAngle);
                        double beta = MathUtil.gdRadianti(s.endAngle);
                        double amp = beta - alpha;
                        if (s.cw == 0)                                          // antiorario
                            amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                        else
                            amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                        Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

                        blindHole.AddSide(aTmp);
                    }
                }

                b.AddPath(blindHole);
            }
            #endregion

            ////shapes.GetDataFromDb(Breton.OptiMasterInterface.Shape.Keys.F_NONE, -1, -1, op.gId, -1, det.gCode, true);
            ////shapes.GetObject(out shp);

            #region	Grezzo di taglio (Layer RAW e RAW_HOLE) e Tecno path (Piece shape)
            Path cut = new Path();
            Path holeCut;

            // Se il pezzo non è ancora stato piazzato sulla lastra, lancia il calcolo dei percorsi 
            // altrimenti legge i percorsi dal database

            TecnoPaths tecnoPaths = new TecnoPaths();

            ////DetailOpti dop = GetDetailOpti(det.gArticleCode);


            int idSide = -1;


            _pieceShape = new Breton.DesignUtil.Shape();


            Shape shp;
            string pieceShapeFilepath;
            if (_piece.IsNewItem) // Nuovo item da Progetto
            {
                pieceShapeFilepath = _piece.OrderDetail.ShapeInfoFilepath;
            }
            else
            {
                pieceShapeFilepath = _piece.XmlFilepath;
            }

            //if (shp != null)
            {
                //shapes.GetXmlPolygonFromDb(shp, tmpFormPath + "\\WORKPHASE" + shp.gIdWorkPhase + ".xml");
                //XmlAnalizer.ReadShapeXml(tmpFormPath + "\\WORKPHASE" + shp.gIdWorkPhase + ".xml", ref _shape, false);

                XmlAnalizer.ReadShapeXml(pieceShapeFilepath, ref _pieceShape, false);


                // Scorre i grezzi per cercare quello attivo
                for (int j = 0; j < _shape.gCutToSize.Count; j++)
                {
                    Breton.DesignUtil.Shape.Cut c = (Breton.DesignUtil.Shape.Cut)_shape.gCutToSize[j];

                    if (c.active)
                    {
                        TecnoPath tp = new TecnoPath();
                        for (int k = 0; k < c.sides.Count; k++)
                        {
                            Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)c.sides[k];

                            cut.Layer = CBPExtension.LayersType.RAW.ToString();

                            if (s.line)
                                idSide = cut.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
                            else
                            {
                                double alpha = MathUtil.gdRadianti(s.startAngle);
                                double beta = MathUtil.gdRadianti(s.endAngle);
                                double amp = beta - alpha;
                                if (s.cw == 0)                                          // antiorario
                                    amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                                else
                                    amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                                Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

                                idSide = cut.AddSide(aTmp);
                            }

                            //if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
                            if (!this.IsAssignedToSLab || _piece.SlabId == -1)
                            {
                                int ids = tp.AddSide(cut.GetSide(idSide - 1));

                                TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
                                tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
                                tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
                                tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
                                tsi.TecnoPathInfo.MaterialThickness = _piece.OrderDetail.Thickness;
                                tsi.Slope = MathUtil.gdRadianti(s.slope);
                                if (tp.GetSide(ids - 1) is TecnoArc)
                                {
                                    TecnoArc ta = (TecnoArc)tp.GetSide(ids - 1);
                                    ta.CoordError = CAM_CHORDAL_ERROR;
                                }
                            }
                        }
                        // Aggiornamento 2015_10_24: Non viene più eseguita la semplificazione della geometria perchè così facendo venivano eliminati ed esclusi
                        // tagli inclinati parziali
                        // Cerca di semplificare il grezzo di taglio nel caso ci siano lati allineati
                        //cut.Simplify(0d);
                        //tp.Simplify(0d);

                        //if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
                        if (!this.IsAssignedToSLab || _piece.SlabId == -1)
                        {
                            tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
                            // Indica se il tipo di percorso è esterno o se è un foro
                            tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_EXTERIOR;
                            tecnoPaths.AddPath(tp);
                        }

                        //TODO: Verificare con Reginato
                        if (!cut.IsPathFullyClosed(MathUtil.QUOTE_EPSILON))
                        {
                            TraceLog.WriteLine(string.Format("Cut path not fully closed. Code =<{0}>.", _piece.OrderDetail.Code));
                            //ProjResource.gResource.LoadMessageBox(this, 20, out caption, out message);
                            //MessageBox.Show(message + ": " + det.gCode + "(" + det.gArticleCode + ")", caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            //dop.state = DetailOptiState.PIECE_OPEN;
                            //UpdateGetDetailOpti(dop);
                        }

                        b.AddPath(cut);
                        break;
                    }
                }
                int activeHoleIndex = 0;
                // Scorre i fori e aggiunge quelli attivi
                for (int j = 0; j < _shape.gHolesToCut.Count; j++)
                {
                    holeCut = new Path();
                    Breton.DesignUtil.Shape.Holes h = (Breton.DesignUtil.Shape.Holes)_shape.gHolesToCut[j];

                    if (h.active)
                    {
                        activeHoleIndex++;

                        TecnoPath tp = new TecnoPath();
                        for (int k = 0; k < h.sides.Count; k++)
                        {
                            Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)h.sides[k];

                            holeCut.Layer = CBPExtension.LayersType.RAW_HOLE.ToString();
                            holeCut.Name = "HOLE" + activeHoleIndex;

                            if (s.line)
                                idSide = holeCut.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
                            else
                            {
                                double alpha = MathUtil.gdRadianti(s.startAngle);
                                double beta = MathUtil.gdRadianti(s.endAngle);
                                double amp = beta - alpha;
                                if (s.cw == 0)                                          // antiorario
                                    amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                                else
                                    amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                                Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

                                idSide = holeCut.AddSide(aTmp);
                            }

                            //if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
                            if (!this.IsAssignedToSLab || _piece.SlabId == -1)
                            {
                                int ids = tp.AddSide(holeCut.GetSide(idSide - 1));

                                TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
                                tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
                                tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
                                tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
                                tsi.TecnoPathInfo.MaterialThickness = _piece.OrderDetail.Thickness;
                                tsi.Slope = MathUtil.gdRadianti(s.slope);
                            }
                        }

                        //if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
                        if (!this.IsAssignedToSLab || _piece.SlabId == -1)
                        {
                            tp.Info.BridgeMode = h.isBridge;
                            tp.Attributes.Add("LINK_RAW_HOLE", holeCut.Name);

                            tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
                            // Indica se il tipo di percorso è esterno o se è un foro
                            tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_INTERIOR;
                            tecnoPaths.AddPath(tp);
                        }

                        //TODO: Verificare con Reginato
                        if (!holeCut.IsPathFullyClosed(MathUtil.QUOTE_EPSILON))
                        {
                            TraceLog.WriteLine(string.Format("HoleCut path not fully closed. Code =<{0}>.", _piece.OrderDetail.Code));

                            //ProjResource.gResource.LoadMessageBox(this, 20, out caption, out message);
                            //MessageBox.Show(message + ": " + det.gCode + "(" + det.gArticleCode + ")", caption, MessageBoxButtons.OK, MessageBoxIcon.Error);

                            //dop.state = DetailOptiState.PIECE_OPEN;
                            //UpdateGetDetailOpti(dop);
                        }

                        b.AddPath(holeCut);
                    }
                }

                // Scorre le bussole e le aggiunge al tecno path
                for (int j = 0; j < _shape.gBussole.Count; j++)
                {
                    Breton.DesignUtil.Shape.Bussola bus = (Breton.DesignUtil.Shape.Bussola)_shape.gBussole[j];

                    TecnoPath tp = new TecnoPath();
                    for (int k = 0; k < bus.sides.Count; k++)
                    {
                        Breton.DesignUtil.Shape.Contour s = (Breton.DesignUtil.Shape.Contour)bus.sides[k];

                        //if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
                        {
                            int ids;
                            if (s.line)
                                ids = tp.AddSide(new Segment(s.x0, s.y0, s.x1, s.y1));
                            else
                            {
                                double alpha = MathUtil.gdRadianti(s.startAngle);
                                double beta = MathUtil.gdRadianti(s.endAngle);
                                double amp = beta - alpha;
                                if (s.cw == 0)                                          // antiorario
                                    amp = (amp < 0d ? Math.PI * 2d + amp : amp);
                                else
                                    amp = (amp > 0d ? amp - Math.PI * 2d : amp);

                                Arc aTmp = new Arc(s.xC, s.yC, s.r, alpha, amp);

                                ids = tp.AddSide(aTmp);
                            }

                            TecnoSideInfo tsi = tp.GetTecnoSideInfo(ids - 1);
                            tsi.FirstVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
                            tsi.SecondVertexDiskMode = EN_VERTEX_DISK_MODE.EN_VERTEX_DISK_MODE_AUTO_COLLISION;
                            tsi.TecnoPathInfo.ToolSide = EN_TOOL_SIDE.EN_TOOL_SIDE_RIGHT;
                            tsi.TecnoPathInfo.MaterialThickness = _piece.OrderDetail.Thickness;
                            tsi.Slope = MathUtil.gdRadianti(s.slope);
                        }
                    }

                    //if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
                    if (!this.IsAssignedToSLab || _piece.SlabId == -1)
                    {
                        tp.Info.BlindHole = true;

                        tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
                        // Indica se il tipo di percorso è esterno o se è un foro
                        tp.Type = Path.EN_PATH_MODE.EN_PATH_MODE_INTERIOR;
                        tecnoPaths.AddPath(tp);
                    }
                }

                if (_piece.IsNewItem)
                {
                    _piece.XmlFilepath = pieceShapeFilepath;
                    bool retval = _piece.SaveToRepository();
                }
            }
            #endregion

            #region Simboli esterni
            ////for (int j = 0; j < _shape.gSymbols.Count; j++)
            ////{
            ////    Path sym = new Path();
            ////    Breton.DesignUtil.Shape.Symbol sy = (Breton.DesignUtil.Shape.Symbol)_shape.gSymbols[j];

            ////    if (symbolsToImport.IndexOf(sy.type.ToUpper()) >= 0)
            ////    {
            ////        TecnoPath tp = new TecnoPath();
            ////        for (int k = 0; k < sy.sides.Count; k++)
            ////        {
            ////            Breton.DesignUtil.Shape.Contour c = (Breton.DesignUtil.Shape.Contour)sy.sides[k];

            ////            sym.Layer = DesignCenterInterface.CBPExtension.LayersType.SYMBOL.ToString();

            ////            if (c.line)
            ////                idSide = sym.AddSide(new Segment(c.x0, c.y0, c.x1, c.y1));
            ////            else
            ////            {
            ////                double alpha = MathUtil.gdRadianti(c.startAngle);
            ////                double beta = MathUtil.gdRadianti(c.endAngle);
            ////                double amp = beta - alpha;
            ////                if (c.cw == 0)                                          // antiorario
            ////                    amp = (amp < 0d ? Math.PI * 2d + amp : amp);
            ////                else
            ////                    amp = (amp > 0d ? amp - Math.PI * 2d : amp);

            ////                Arc aTmp = new Arc(c.xC, c.yC, c.r, alpha, amp);

            ////                idSide = sym.AddSide(aTmp);
            ////            }

            ////            tp.AddSide(sym.GetSide(idSide - 1));

            ////        }

            ////        if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            ////        {
            ////            tp.Layer = CBPExtension.LayersType.TECNO_INFO.ToString();
            ////            // Indica se è un percorso che non deve essere tagliato
            ////            tp.Info.NoCutPolygon = true;

            ////            tecnoPaths.AddPath(tp);
            ////        }
            ////    }

            ////    b.AddPath(sym);
            ////}
            #endregion

            #region Calcolo per il RENDER

            // costruisco il percorso che sta attorno al blocco considerando il rettangolo RT
            // il rettangolo da considerare è quello intorno al percorso della _shape, altrimenti può essere
            // troppo grande se considero il blocco (ci sono anche gli altri percorsi)

            Path pTmp = new Path(externalPath);
            pTmp.MatrixRT = new RTMatrix(x, y, a);
            Path pRect = new Path();
            if (pTmp != null)
                pRect = new Path(pTmp.GetRectangleRT());

            pRect.Layer = CBPExtension.LayersType.RENDER_PATH.ToString();

            // applico al percorso del rettangolo circoscritto la matrice inversa per riportarlo nell'origine
            RTMatrix mat = new RTMatrix(x, y, a);
            mat = mat.InvMat();

            // trasformo i vertici del rettangolo
            double px, py, pxt, pyt;
            List<Breton.Polygons.Point> pointTransform = new List<Breton.Polygons.Point>();
            Breton.Polygons.Point p1 = pRect.GetVertexRT(0, out px, out py);
            mat.PointTransform(px, py, out pxt, out pyt);
            pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            p1 = pRect.GetVertexRT(1, out px, out py);
            mat.PointTransform(px, py, out pxt, out pyt);
            pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            p1 = pRect.GetVertexRT(2, out px, out py);
            mat.PointTransform(px, py, out pxt, out pyt);
            pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            p1 = pRect.GetVertexRT(3, out px, out py);
            mat.PointTransform(px, py, out pxt, out pyt);
            pointTransform.Add(new Breton.Polygons.Point(pxt, pyt));

            // con questi vertici faccio le side per il path riferito allo 0,0
            Path pRectNew = new Path();
            Side sd = new Segment(pointTransform[0], pointTransform[1]);
            pRectNew.AddSide(sd);
            sd = new Segment(pointTransform[1], pointTransform[2]);
            pRectNew.AddSide(sd);
            sd = new Segment(pointTransform[2], pointTransform[3]);
            pRectNew.AddSide(sd);
            sd = new Segment(pointTransform[3], pointTransform[0]);
            pRectNew.AddSide(sd);

            pRectNew.Layer = CBPExtension.LayersType.RENDER_PATH.ToString();

            // aggiungo il rettangolo al blocco
            b.AddPath(pRectNew);

            #endregion

            #region Calcolo dei layer necessari per le collisioni

            PathCollection paths;
            Path intZone = new Path(externalPath);
            ZoneOffset.PathOffset(ref intZone, -300, ZoneOffset.EXPANSION_MODE.EXPANSION_MODE_RECT, out paths);
            intZone.Layer = CBPExtension.LayersType.RAW_INTEREST_ZONE.ToString();
            b.AddPath(intZone);

            #endregion

            ////#region Percorsi utensili di taglio
            ////// Se il pezzo non è ancora stato piazzato sulla lastra, lancia il calcolo dei percorsi 
            ////// altrimenti legge i percorsi dal database
            ////ToolPaths toolPaths = new ToolPaths();
            ////RemovalPaths removalPaths = new RemovalPaths();
            ////// Carica i tool path info (utensili)
            ////TecnoPathInfo tpi;
            ////ToolsLeadMode tslm;
            ////GetTecnoPathInfo(det.gThickness, out tpi, out tslm);

            ////if (dop.state == DetailOptiState.AVAILABLE_NOT_ASSIGNED || dop.state == DetailOptiState.NO_TOOLPATH)
            ////{
            ////    // applicazione dei parametri specifici per ogni percorso
            ////    for (int m = 0; m < tecnoPaths.Count; m++)
            ////    {
            ////        TecnoPath tp = tecnoPaths[m];
            ////        if (tp.Info.NoCutPolygon)
            ////            continue;

            ////        if (tp.Info.AvailableTools.Count == 0)
            ////        {
            ////            TecnoPathInfo tmptpInf = new TecnoPathInfo(tpi);
            ////            tmptpInf.BridgeMode = tp.Info.BridgeMode;
            ////            tmptpInf.BlindHole = tp.Info.BlindHole;
            ////            tp.Info = tmptpInf;
            ////        }
            ////        tp.Info.ToolsLead = tslm;

            ////        tp.PropagatesTecnoPathInfo(false);
            ////    }

            ////    if (mCam.CreateStandardToolPaths(tecnoPaths, out toolPaths, tpi, det.gArticleCode))
            ////        tecnoPaths = mCam.OriginalGeometries;

            ////    // Scorre i TecnoPaths creati e li aggiunge alla lista di path
            ////    for (int j = 0; j < tecnoPaths.Count; j++)
            ////        b.AddPath(tecnoPaths[j]);

            ////    // Scorre i risultati ottenuti per inserirli nel blocco
            ////    for (int j = 0; j < toolPaths.Count; j++)
            ////    {
            ////        ToolPath tp = (ToolPath)toolPaths.GetPath(j);

            ////        if (((IToolPathSide)tp.GetSide(0)).Info.Slope > 0)
            ////            tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS.ToString();
            ////        else if (((IToolPathSide)tp.GetSide(0)).Info.Slope < 0)
            ////            tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG.ToString();
            ////        else
            ////            tp.Layer = CBPExtension.LayersType.FINAL_TOOL.ToString();

            ////        b.AddPath(tp);
            ////    }

            ////    for (int j = 0; j < mCam.RemovalPaths.Count; j++)
            ////    {
            ////        RemovalPath rp = (RemovalPath)mCam.RemovalPaths.GetPath(j);
            ////        if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope > 0)
            ////            rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS_REM.ToString();
            ////        else if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope < 0)
            ////            rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG_REM.ToString();
            ////        else
            ////            rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();

            ////        b.AddPath(rp);
            ////    }
            ////}
            ////else if (dop.state == DetailOptiState.AVAILABLE_ASSIGNED)
            ////{
            ////    // Cerca i percorsi utensili salvati
            ////    Piece pie;
            ////    GroupOpCollection tmpgroups = new GroupOpCollection(mDbInterface);
            ////    PieceOpCollection tmppieces = new PieceOpCollection(mDbInterface);
            ////    tmpgroups.GetDataFromDb(Group.Keys.F_NONE, null, null, new int[] { mCurrentOP.gId });
            ////    tmpgroups.MoveFirst();
            ////    while (!tmpgroups.IsEOF())
            ////    {
            ////        tmpgroups.GetObject(out gr);

            ////        if (gr != null && shp != null)
            ////        {
            ////            tmppieces.GetDataFromDb(Piece.Keys.F_NONE, null, new int[] { gr.gId }, new int[] { shp.gIdOpShape }, null);
            ////            tmppieces.GetObject(out pie);
            ////            if (pie != null)
            ////            {
            ////                tmppieces.GetXmlPolygonFromDb(pie, tmpFormPath + "\\" + dop.piefCode + "_in.xml");

            ////                XmlDocument doc = new XmlDocument();
            ////                doc.Load(tmpFormPath + "\\" + dop.piefCode + "_in.xml");
            ////                XmlNode root = doc.SelectSingleNode("EXPORT");

            ////                // Legge il file xml
            ////                foreach (XmlNode nd in root.ChildNodes)
            ////                {
            ////                    string ot = PathCollection.GetOriginalTypeFromXML(nd);

            ////                    if (ot == "TecnoPaths")
            ////                        tecnoPaths.ReadFileXml(nd);
            ////                    else if (ot == "ToolPaths")
            ////                        toolPaths.ReadFileXml(nd);
            ////                    else if (ot == "RemovalPaths")
            ////                        removalPaths.ReadFileXml(nd);
            ////                }

            ////                bool diffTools = false;

            ////                // Se il pezzo non fa parte di una lastra bloccata effettua la verifica 
            ////                if (!lockedPieces.Contains(pie.gIdShape))
            ////                {
            ////                    // *****************************************************************************
            ////                    // Verifica se è necessario rigenerare i percorsi anche per i pezzi già piazzati

            ////                    // Recupera gli utensili usati per questo pezzo e verifica se sono uguali
            ////                    if (tecnoPaths[0].Info.AvailableTools.Count != tpi.AvailableTools.Count)
            ////                        diffTools = true;
            ////                    else
            ////                    {
            ////                        for (int j = 0; j < tecnoPaths[0].Info.AvailableTools.Count; j++)
            ////                        {
            ////                            for (int k = 0; k < tpi.AvailableTools.Count; k++)
            ////                            {
            ////                                if (tecnoPaths[0].Info.AvailableTools[j].Name == tpi.AvailableTools[k].Name)
            ////                                {
            ////                                    if (tecnoPaths[0].Info.AvailableTools[j] != tpi.AvailableTools[k])
            ////                                    {
            ////                                        diffTools = true;
            ////                                        break;
            ////                                    }
            ////                                }
            ////                            }
            ////                            if (diffTools)
            ////                                break;
            ////                        }
            ////                    }


            ////                    if (diffTools && mReloadToolpathMessage != 2)
            ////                    {
            ////                        ProjResource.gResource.LoadMessageBox(this, 21, out caption, out message);

            ////                        if (mReloadToolpathMessage == 0)
            ////                            if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            ////                                mReloadToolpathMessage = 1;
            ////                            else
            ////                                mReloadToolpathMessage = 2;

            ////                        if (mReloadToolpathMessage == 1)
            ////                        {
            ////                            mReloadToolpathMessage = 1;
            ////                            toolPaths = new ToolPaths();
            ////                            removalPaths = new RemovalPaths();

            ////                            // applicazione dei parametri specifici per ogni percorso
            ////                            for (int m = 0; m < tecnoPaths.Count; m++)
            ////                            {
            ////                                TecnoPath tp = tecnoPaths[m];
            ////                                if (tp.Info.NoCutPolygon)
            ////                                    continue;


            ////                                TecnoPathInfo tmptpInf = new TecnoPathInfo(tpi);
            ////                                tmptpInf.BridgeMode = tp.Info.BridgeMode;
            ////                                tmptpInf.BlindHole = tp.Info.BlindHole;
            ////                                tp.Info = tmptpInf;

            ////                                tp.Info.ToolsLead = tslm;
            ////                                tp.PropagatesTecnoPathInfo(true);
            ////                            }


            ////                            if (mCam.CreateStandardToolPaths(tecnoPaths, out toolPaths, tpi, det.gArticleCode))
            ////                                tecnoPaths = mCam.OriginalGeometries;


            ////                            // *********************************************

            ////                            // Scorre i TecnoPaths creati e li aggiunge alla lista di path
            ////                            for (int j = 0; j < tecnoPaths.Count; j++)
            ////                                b.AddPath(tecnoPaths[j]);

            ////                            // Scorre i risultati ottenuti per inserirli nel blocco
            ////                            for (int j = 0; j < toolPaths.Count; j++)
            ////                            {
            ////                                ToolPath tp = (ToolPath)toolPaths.GetPath(j);

            ////                                if (((IToolPathSide)tp.GetSide(0)).Info.Slope > 0)
            ////                                    tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS.ToString();
            ////                                else if (((IToolPathSide)tp.GetSide(0)).Info.Slope < 0)
            ////                                    tp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG.ToString();
            ////                                else
            ////                                    tp.Layer = CBPExtension.LayersType.FINAL_TOOL.ToString();

            ////                                b.AddPath(tp);
            ////                            }

            ////                            for (int j = 0; j < mCam.RemovalPaths.Count; j++)
            ////                            {
            ////                                RemovalPath rp = (RemovalPath)mCam.RemovalPaths.GetPath(j);
            ////                                if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope > 0)
            ////                                    rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_POS_REM.ToString();
            ////                                else if (((IToolPathSide)rp.LinkToToolPath.GetSide(0)).Info.Slope < 0)
            ////                                    rp.Layer = CBPExtension.LayersType.SLOPED_CUTS_NEG_REM.ToString();
            ////                                else
            ////                                    rp.Layer = CBPExtension.LayersType.FINAL_TOOL_REM.ToString();

            ////                                b.AddPath(rp);
            ////                            }

            ////                            break;
            ////                        }
            ////                    }
            ////                }

            ////                if (!diffTools || mReloadToolpathMessage == 2)
            ////                {
            ////                    // Recupera le informazioni dei toolpaths
            ////                    for (int j = 0; j < toolPaths.Count; j++)
            ////                        toolPaths[j].RecoverLinkToTecnoPath(tecnoPaths);

            ////                    // Aggiunge i tecno path letti
            ////                    for (int j = 0; j < tecnoPaths.Count; j++)
            ////                        b.AddPath(tecnoPaths.GetPath(j));

            ////                    // Aggiunge i tool path letti
            ////                    for (int j = 0; j < toolPaths.Count; j++)
            ////                        b.AddPath(toolPaths.GetPath(j));

            ////                    // Aggiunge le asportazioni lette
            ////                    for (int j = 0; j < removalPaths.Count; j++)
            ////                        b.AddPath(removalPaths.GetPath(j));

            ////                    break;
            ////                }
            ////            }
            ////        }

            ////        tmpgroups.MoveNext();
            ////    }
            ////}
            ////else if (dop.state == DetailOptiState.UNAVAILABLE || dop.state == DetailOptiState.PIECE_OPEN)
            ////{
            ////    // Non fa niente
            ////}

            ////// Se il pezzo non aveva i percorsi utensile perchè risultato di un ottimizzazione automatica allora imposta il pezzo come già assegnato
            ////if (dop.state == DetailOptiState.NO_TOOLPATH)
            ////{
            ////    dop.state = DetailOptiState.AVAILABLE_ASSIGNED;
            ////    UpdateGetDetailOpti(dop);
            ////}
            ////#endregion

            b.Code = this.Piece.OrderDetail.Code.Trim();

            b.Name = _shape.gShapeNumber.ToString();


            // TODO: per riposizionamento sulProject

            //// Imposta la rototraslazione per posizionare il pezzo all'interno del top
            b.SetRotoTransl(x, y, a);



            this.Block = b;

            #endregion

        }

        public PieceBlock(WkOrderDetailPieceEntity piece, Block block):this(piece)
        {
            _block = block;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public WkOrderDetailPieceEntity Piece
        {
            get { return _piece; }
            set { _piece = value; }
        }

        public Block Block
        {
            get { return _block; }
            set { _block = value; }
        }

        public bool IsAssignedToSLab
        {
            get { return _isAssignedToSLab; }
            set
            {
                _isAssignedToSLab = value;
                OnPropertyChanged("IsAssignedToSLab");
            }
        }

        public Shape Shape
        {
            get { return _shape; }
            set { _shape = value; }
        }

        public System.Windows.Shapes.Path WpfPath
        {
            get
            {
                return _wpfPath;
            }
            set
            {
                _wpfPath = value;
                OnPropertyChanged("WpfPath");
            }
        }

        public string WpfPathData
        {
            get { return _wpfPathData; }
            set
            {
                _wpfPathData = value;
                OnPropertyChanged("WpfPathData");
            }
        }

        public int PlacedBlockId
        {
            get { return _placedBlockId; }
            set { _placedBlockId = value; }
        }

        public string MaterialImageFilepath
        {
            get { return _materialImageFilepath; }
            set
            {
                _materialImageFilepath = value;
                OnPropertyChanged("MaterialImageFilepath");
            }
        }

        public ToBigRender ToBigRender
        {
            get { return _toBigRender; }
            set { _toBigRender = value; }
        }

        public Shape ProjectShape
        {
            get { return _projectShape; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
