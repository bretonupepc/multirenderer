﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Breton.MathUtils;
using Breton.Polygons;
using DataAccess.Business.BusinessBase;
using SlabOperate.Business.Entities;
using SlabOperate.Business.Entities.WORK.DETAILS;
using SlabOperate.Business.Entities.WORK.ORDER;
using SlabOperate.LEGACY_FM.UTILS;
using SlabOperate.SESSION;
using TraceLoggers;

namespace SlabOperate.BUSINESS
{
    public enum SlabDbStatus
    {
        F_SLAB_STATE_INVALID = -1,
        F_SLAB_STATE_READY,                              //Lastra pronta all'ottimizzazione
        F_SLAB_STATE_PROGRAMMING,                        //Lastra in programmazione
        F_SLAB_STATE_CONFIRMED,                          //Lastra confermata
        F_SLAB_STATE_IN_PROCESS,                         //Lastra in lavorazione
        F_SLAB_STATE_COMPLETED,                          //Lastra prodotta
        F_SLAB_STATE_SUSPENDED,                          //Lastra sospesa
        F_SLAB_STATE_LOCKED,                             //Lastra prenotata
        F_SLAB_STATE_PROGRAMMED,                         //Lastra programmata
        F_SLAB_STATE_DISCARDED,                          //Lastra scartata
        F_SLAB_STATE_DESTROYED,                          //Lastra distrutta

        F_SLAB_STATE_STOCK = 100,                        //Lastra in magazzino

        F_SLAB_STATE_VIRTUAL_READY = 1000,               //Lastra virtuale pronta all'ottimizzazione
        F_SLAB_STATE_VIRTUAL_PROGRAMMING,                //Lastra virtuale in programmazione
        F_SLAB_STATE_VIRTUAL_CONFIRMED,                  //Lastra virtuale confermata
        F_SLAB_STATE_VIRTUAL_IN_PROCESS,                 //Lastra virtuale in lavorazione
        F_SLAB_STATE_VIRTUAL_COMPLETED,                  //Lastra virtuale prodotta
        F_SLAB_STATE_VIRTUAL_SUSPENDED,                  //Lastra virtuale sospesa
        F_SLAB_STATE_VIRTUAL_LOCKED,                     //Lastra virtuale prenotata
        F_SLAB_STATE_VIRTUAL_PROGRAMMED,                 //Lastra virtuale programmata
        F_SLAB_STATE_VIRTUAL_DISCARDED,                  //Lastra virtuale scartata
        F_SLAB_STATE_VIRTUAL_DESTROYED                  //Lastra virtuale distrutta

    }

    public enum UnloadConfirm
    {
        F_UNLOAD_CONFIRM_NONE = 0,
        F_UNLOAD_CONFIRM_FKSCAR = 1
    }

    public enum CUT_TYPES
    {
        F_CUT_TYPE_NONE = 0,
        F_CUT_TYPE_QUADRANTS,
        F_CUT_TYPE_STRIPS,
        F_CUT_TYPE_POLYGONS,
        F_CUT_TYPE_PROFILES,
        F_CUT_TYPE_MAX
    }

    public enum SlabProcessStatus
    {
        InProcess,
        Processed
    }

    public class GraphicSlab : DataAccess.Business.BusinessBase.ObservableObject
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private string _code = string.Empty;

        private string _name = string.Empty;

        private string _materialName = string.Empty;

        private double _dimX;

        private double _dimY;

        private double _dimZ;

        private double _offsetX = 0;

        private double _offsetY = 0;

        private double _photoOffsetX = 0;

        private double _photoOffsetY = 0;

        private double _photoAdjustX = 0;

        private double _photoAdjustY = 0;

        private double _originX;

        private double _originY;


        private CUT_TYPES _cutType;

        private SlabDbStatus _dbStatus = SlabDbStatus.F_SLAB_STATE_INVALID;

        private SlabProcessStatus _processStatus = SlabProcessStatus.InProcess;

        private UnloadConfirm _unloadConfirmStatus = BUSINESS.UnloadConfirm.F_UNLOAD_CONFIRM_NONE;

        private Bitmap _slabImageBitmap = null;

        private string _xmlContent = string.Empty;


        //orders and pieces

        private PersistableEntityCollection<WkOrderEntity> _orders = new PersistableEntityCollection<WkOrderEntity>();

        private PersistableEntityCollection<WkOrderSlab> _orderSlabs = new PersistableEntityCollection<WkOrderSlab>();

        private PersistableEntityCollection<WkOrderDetailPieceEntity> _pieces = new PersistableEntityCollection<WkOrderDetailPieceEntity>();


        private PersistableEntityCollection<WkOrderDetailEntity> _shapes = new PersistableEntityCollection<WkOrderDetailEntity>();

        private SlabEntity _innerEntity;

        private bool _hasPiecesChanged = false;

        private Zone _zone = null;

        private bool _zoneLoaded = false;



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public GraphicSlab(SlabEntity slabEntity)
        {
            InnerEntity = slabEntity;
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public int Id
        {
            get { return _innerEntity.Id; }
            set
            {
                InnerEntity.Id = value;
                OnPropertyChanged("Id");
            }
        }

        public string Code
        {
            get { return InnerEntity.Code; }
            set
            {
                InnerEntity.Code = value;
                OnPropertyChanged("Code");

            }
        }

        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    _name = string.Format("LST_{0}", Id.ToString("0000000000"));
                }
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public string LongName
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                {
                    Name = string.Format("LST_{0}", Id.ToString("0000000000"));
                }
                return string.Format("{0} - {1}", Name, Code);
            }
        }

        public string MaterialName
        {
            get { return InnerEntity.ThicknessObject.Material.Description; }
            set
            {
                InnerEntity.ThicknessObject.Material.Description = value;
                OnPropertyChanged("MaterialName");

            }
        }

        public double DimX
        {
            get { return InnerEntity.Length; }
            set
            {
                InnerEntity.Length = value;
                OnPropertyChanged("DimX");

            }
        }

        public double DimY
        {
            get { return InnerEntity.Width; }
            set
            {
                InnerEntity.Width = value;
                OnPropertyChanged("DimY");

            }
        }

        public double DimZ
        {
            get { return InnerEntity.Thickness; }
            set
            {
                InnerEntity.Thickness = value;
                OnPropertyChanged("DimZ");

            }
        }

        public double OffsetX
        {
            get { return InnerEntity.OffsetX; }
            set
            {
                InnerEntity.OffsetX = value;
                OnPropertyChanged("OffsetX");

            }
        }

        public double OffsetY
        {
            get { return InnerEntity.OffsetY; }
            set
            {
                InnerEntity.OffsetY = value;
                OnPropertyChanged("OffsetY");

            }
        }

        public double GraphicOffsetX
        {
            get
            {
                //return _offsetX - _photoOffsetX;
                if (InnerEntity.SlabImageId != -1)
                {
                    //return _photoOffsetX;
                    return OffsetX - PhotoOffsetX;
                }
                return _offsetX;
            }
        }

        public double GraphicOffsetY
        {
            get
            {
                //return _offsetY - _photoOffsetY;
                if (InnerEntity.SlabImageId != -1)
                {
                    //return _photoOffsetY;
                    return OffsetY - PhotoOffsetY;
                }
                return _offsetY;
            }
        }


        public SlabDbStatus DbStatus
        {
            get { return (SlabDbStatus)InnerEntity.SlabStatus; }
            set
            {
                InnerEntity.SlabStatus = (Business.Entities.SlabStatus)value;
                OnPropertyChanged("DbStatus");

            }
        }

        public PersistableEntityCollection<WkOrderEntity> Orders
        {
            get { return _orders; }
            set
            {
                _orders = value;
                OnPropertyChanged("Orders");
            }
        }

        public SlabProcessStatus ProcessStatus
        {
            get { return _processStatus; }
            set { _processStatus = value; }
        }

        public UnloadConfirm UnloadConfirmStatus
        {
            get { return (BUSINESS.UnloadConfirm)InnerEntity.UnloadConfirm; }
            set { InnerEntity.UnloadConfirm = (SlabUnloadConfirm)value; }
        }

        public PersistableEntityCollection<WkOrderSlab> OrderSlabs
        {
            get { return _orderSlabs; }
            set { _orderSlabs = value; }
        }

        public CUT_TYPES CutType
        {
            get { return (CUT_TYPES)InnerEntity.CutType; }
            set { InnerEntity.CutType = (SlabCutType)value; }
        }

        public Bitmap SlabImageBitmap
        {
            get
            {
                if (_slabImageBitmap == null)
                {
                    if (InnerEntity.SlabImageId != -1 && string.IsNullOrEmpty(InnerEntity.SlabImage.ImagePath))
                    {
                        FieldStatus status;
                        InnerEntity.SlabImage.GetPropertyStatus("ImagePath", out status);

                        while (status == FieldStatus.Unknown || status == FieldStatus.Reading)
                        {
                            Thread.Sleep(50);
                            InnerEntity.SlabImage.GetPropertyStatus("ImagePath", out status);
                        }
                    }

                    if (!string.IsNullOrEmpty(InnerEntity.SlabImage.ImagePath) && File.Exists(InnerEntity.SlabImage.ImagePath))
                        _slabImageBitmap = (Bitmap)Image.FromFile(InnerEntity.SlabImage.ImagePath).Clone();
                }

                return _slabImageBitmap;
            }
            set { _slabImageBitmap = value; }
        }


        public string XmlContent
        {
            get
            {
                if (string.IsNullOrEmpty(_xmlContent))
                {
                    if (string.IsNullOrEmpty(InnerEntity.BoundaryImageFilepath))
                    {
                        FieldStatus status;
                        InnerEntity.GetPropertyStatus("BoundaryImageFilepath", out status);

                        while (status == FieldStatus.Unknown || status == FieldStatus.Reading)
                        {
                            Thread.Sleep(50);
                            InnerEntity.GetPropertyStatus("BoundaryImageFilepath", out status);
                        }
                    }

                    if (!string.IsNullOrEmpty(InnerEntity.BoundaryImageFilepath) && File.Exists(InnerEntity.BoundaryImageFilepath))
                        _xmlContent = File.ReadAllText(InnerEntity.BoundaryImageFilepath);

                }
                return _xmlContent;
            }
            set { _xmlContent = value; }
        }

        public PersistableEntityCollection<WkOrderDetailPieceEntity> Pieces
        {
            get { return _pieces; }
            set
            {
                _pieces = value;
                OnPropertyChanged("Pieces");

            }
        }

        public bool HasPiecesChanged
        {
            get { return _hasPiecesChanged; }
            set
            {
                _hasPiecesChanged = value;
                OnPropertyChanged("HasPiecesChanged");
            }
        }

        public Zone Zone
        {
            get { return _zone; }
            set { _zone = value; }
        }

        public bool ZoneLoaded
        {
            get { return _zoneLoaded; }
            set { _zoneLoaded = value; }
        }

        public PersistableEntityCollection<WkOrderDetailEntity> Shapes
        {
            get { return _shapes; }
        }

        public double PhotoOffsetX
        {
            get { return _photoOffsetX; }
            set { _photoOffsetX = value; }
        }

        public double PhotoOffsetY
        {
            get { return _photoOffsetY; }
            set { _photoOffsetY = value; }
        }

        public double PhotoAdjustX
        {
            get { return _photoAdjustX; }
            set { _photoAdjustX = value; }
        }

        public double PhotoAdjustY
        {
            get { return _photoAdjustY; }
            set { _photoAdjustY = value; }
        }

        public SlabEntity InnerEntity
        {
            get { return _innerEntity; }
            set
            {
                _innerEntity = value;
                OnPropertyChanged("InnerEntity");
            }
        }

        public double OriginX
        {
            get { return _originX; }
            set { _originX = value; }
        }

        public double OriginY
        {
            get { return _originY; }
            set { _originY = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public bool GetAllDataFromDb()
        {
            bool retVal = false;


            try
            {
                //OrderSlabs.Clear();
                //Orders.Clear();

                object dummy = XmlContent;

                //dummy = SlabImageBitmap;

                //if (DbConnection.GetSlabData(this))
                {
                    //ATTENZIONE: Correzione offsets lastra come da indicazione Gino Stocco 30/05/2016
                    if (InnerEntity.SlabImageId == -1
                        && PhotoOffsetX != 0
                        && PhotoOffsetY != 0)
                    {
                        PhotoAdjustX = 0;
                        PhotoAdjustY = 0;

                        if (PhotoOffsetX != OffsetX
                            && ApplicationRun.Instance.Settings.RifXSlabOnTable >= 0)
                        {
                            OffsetX = ApplicationRun.Instance.Settings.RifXSlabOnTable;
                        }
                        else
                        {
                            OffsetX = PhotoOffsetX;
                        }

                        if (PhotoOffsetY != OffsetY
                            && ApplicationRun.Instance.Settings.RifYSlabOnTable >= 0)
                        {
                            OffsetY = ApplicationRun.Instance.Settings.RifYSlabOnTable;
                        }
                        else
                        {
                            OffsetY = PhotoOffsetY;
                        }

                        PhotoOffsetX = 0;
                        PhotoOffsetY = 0;
                    }

                    retVal = true;


                    //if (GetGroups())
                    //{
                    //    if (BuildOrdersTree())
                    //    {
                    //        retVal = true;
                    //    }
                    //    else
                    //    {
                    //        string error =
                    //            string.Format(
                    //                ResourceHelper.GetString("ERROR_BUILDING_SLAB_ORDERS_TREE",
                    //                    "Error building orders tree for slab <{0}>"), Code);
                    //        CurrentSession.AddError(error);

                    //    }
                    //}
                    //else
                    //{
                    //    string error =
                    //        string.Format(
                    //            ResourceHelper.GetString("ERROR_RETRIEVING_SLAB_GROUPS",
                    //                "Error retrieving slab groups for slab <{0}>"), Code);
                    //    CurrentSession.AddError(error);
                    //}

                }

            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Slab.GetAllDataFromDb error: ", ex);

            }
            return retVal;
        }

        //private bool BuildOrdersTree()
        //{
        //    bool retVal = false;
        //    try
        //    {
        //        Orders.Clear();
        //        foreach (var group in OrderSlabs)
        //        {
        //            foreach (var piece in group.Pieces)
        //            {
        //                if (!(Orders.Any(o => o.Id == piece.ReferredOrderId)))
        //                {
        //                    Order newOrder = null;
        //                    if (Order.GetOrderFromDb(piece.ReferredOrderId, out newOrder))
        //                    {
        //                        Orders.Add(newOrder);
        //                    }
        //                    else
        //                    {
        //                        string error =
        //                            string.Format(
        //                                ResourceHelper.GetString("ERROR_RETRIEVING_ORDER",
        //                                    "Error retrieving order for order <{0}>"), piece.ReferredOrderId);
        //                        CurrentSession.AddError(error);

        //                    }

        //                }
        //            }
        //        }
        //        Pieces.Clear();
        //        // Add pieces to orders
        //        int pieceProg = 1;
        //        foreach (var order in Orders)
        //        {
        //            var orderId = order.Id;
        //            var piecesList = OrderSlabs.SelectMany(g => g.Pieces).Where(p => p.ReferredOrderId == orderId).ToList();
        //            foreach (var piece in piecesList)
        //            {
        //                piece.ReferredOrder = order;

        //                var gr = OrderSlabs.FirstOrDefault(g => g.Id == piece.ReferredGroupId);
        //                if (gr != null)
        //                {
        //                    piece.ReferredGroup = gr;
        //                }
        //                order.Pieces.Add(piece);

        //                piece.Prog = pieceProg++;
        //                piece.ParentSlab = this;
        //                piece.HasChanged = false;
        //                Pieces.Add(piece);
        //            }
        //        }

        //        HasPiecesChanged = false;

        //        // Retireve Shapes data

        //        foreach (var piece in Pieces)
        //        {
        //            if (!(Shapes.Any(s => s.Id == piece.ReferredShapeId)))
        //            {
        //                var newShape = new Shape();
        //                newShape.Id = piece.ReferredShapeId;
        //                Shapes.Add(newShape);
        //            }
        //        }
        //        Shapes.GetDetailsData();
        //        // TODO Debug
        //        foreach (var sh in Shapes)
        //        {

        //            if (!string.IsNullOrEmpty(sh.XmlPolygon))
        //            {
        //                var filepath = System.IO.Path.Combine(CurrentSession.TempFolder, string.Format("SH_{0}.xml", sh.Id));
        //                if (File.Exists(filepath)) File.Delete(filepath);
        //                File.WriteAllText(filepath, sh.XmlPolygon);
        //            }

        //        }



        //        foreach (var piece in Pieces)
        //        {
        //            var shape = Shapes.FirstOrDefault(s => s.Id == piece.ReferredShapeId);
        //            if (shape != null)
        //            {
        //                piece.ReferredShape = shape;

        //                // TODO Debug
        //                if (!string.IsNullOrEmpty(piece.XmlPolygon))
        //                {
        //                    var filepath = System.IO.Path.Combine(CurrentSession.TempFolder, piece.Name +
        //                        string.Format("SH_{0}.xml", shape.Id));
        //                    if (File.Exists(filepath)) File.Delete(filepath);
        //                    File.WriteAllText(filepath, piece.XmlPolygon);
        //                }

        //            }
        //        }


        //        retVal = true;


        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("Slab:BuildOrdersTree error:", ex);
        //    }

        //    return retVal;
        //}

        //private bool GetGroups()
        //{
        //    bool retVal = false;

        //    try
        //    {
        //        if (DbConnection.GetSlabGroupList(this))
        //        {
        //            retVal = true;
        //            foreach (Group group in OrderSlabs)
        //            {
        //                if (!group.GetAllDataFromDb())
        //                {
        //                    retVal = false;
        //                    break;
        //                }
        //            }
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("Slab:GetGroups error: ", ex);
        //    }

        //    return retVal;
        //}

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        //internal bool UpdatePiecesOnDb()
        //{
        //    bool retVal = false;

        //    bool uncompletedSlab = false;

        //    try
        //    {
        //        var unloadedGroups = new GroupCollection();
        //        foreach (var group in OrderSlabs)
        //        {
        //            if (group.DbStatus == F_GROUP_STATE.F_GROUP_STATE_UNLOADING)
        //            {
        //                unloadedGroups.Add(group);

        //                foreach (var piece in group.Pieces)
        //                {
        //                    if (piece.DbType == PieceDbType.F_PIECE_TYPE_DISCARD_UNLOAD)
        //                    {
        //                        if (!piece.UpdateAsDiscarded())
        //                        {
        //                            return retVal;
        //                        }
        //                    }
        //                }
        //            }
        //            else if (!group.IsRaffilo && group.DbStatus != F_GROUP_STATE.F_GROUP_STATE_COMPLETED)
        //            {
        //                uncompletedSlab = true;
        //            }
        //        }

        //        if (!CurrentSession.CurrentSettings.MachineAutomaticUnload)
        //        {
        //            if (uncompletedSlab && unloadedGroups.Count > 0)
        //            {
        //                if (!unloadedGroups.UpdateGroupStatusOnDb(F_GROUP_STATE.F_GROUP_STATE_COMPLETED, false))
        //                {
        //                    return false;
        //                }
        //            }
        //            else // l'intera lastra è stata completata
        //            {
        //                if (!ChangeGroupsStatusOnDb(F_GROUP_STATE.F_GROUP_STATE_COMPLETED, false))
        //                {
        //                    return false;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            if (uncompletedSlab)
        //            {
        //                // Non fare nulla
        //            }
        //            else
        //            {
        //                if (!SetUnloadConfirm(UnloadConfirm.F_UNLOAD_CONFIRM_FKSCAR))
        //                {
        //                    return false;
        //                }
        //            }
        //        }

        //        if (CurrentSession.CurrentSettings.PrintEnabled)
        //        {
        //            CurrentSession.LabelPrintManager.EnqueueGroupsPieces(unloadedGroups);
        //        }

        //        retVal = true;

        //    }
        //    catch (Exception ex)
        //    {
        //        retVal = false;
        //        TraceLog.WriteLine("Slab: UpdatePiecesOnDb error: ", ex);
        //        string error =
        //                string.Format(
        //                    ResourceHelper.GetString("ERROR_RETRIEVING_ORDER",
        //                        "Error updating pieces for slab <{0}>"), Code);
        //        CurrentSession.AddError(error);

        //    }

        //    return retVal;
        //}

        //private bool SetUnloadConfirm(UnloadConfirm unloadConfirm)
        //{
        //    bool retVal = false;

        //    retVal = DbConnection.UpdateSlabUnloadConfirm(Id, unloadConfirm);

        //    if (!retVal)
        //    {
        //        string error =
        //            string.Format(
        //                ResourceHelper.GetString("ERROR_UPDATING_SLAB_UNLOAD_CONFIRM_STATUS",
        //                "Error updating slab unload-confirm status for slab <{0}>"), Code);
        //        CurrentSession.AddError(error);

        //    }
        //    return retVal;
        //}

        //private bool ChangeGroupsStatusOnDb(F_GROUP_STATE newDbStatus, bool noGroupsCompleted)
        //{
        //    bool retVal = false;

        //    retVal = DbConnection.UpdateSlabGroupsStatus(Id, newDbStatus, noGroupsCompleted);

        //    if (!retVal)
        //    {
        //        string error =
        //            string.Format(
        //                ResourceHelper.GetString("ERROR_UPDATING_SLAB_GROUPS_STATUS",
        //                "Error updating groups status for slab <{0}>"), Code);
        //        CurrentSession.AddError(error);

        //    }
        //    return retVal;

        //}

        //internal void RefreshUnloadStatusFromDb()
        //{
        //    bool retVal = false;

        //    retVal = DbConnection.RefreshSlabUnloadStatusFromDb(this);

        //    if (!retVal)
        //    {
        //        string error =
        //            string.Format(
        //                ResourceHelper.GetString("ERROR_REFRESHING_SLAB_UNLOAD_STATUS",
        //                "Error refreshing unload status for slab <{0}>"), Code);
        //        CurrentSession.AddError(error);

        //    }
        //}

        #region Breton Slab

        /// <summary>
        /// Ritorna i parametri dell'immagine
        /// </summary>
        /// <param name="paramsFile"></param>
        /// <param name="length"></param>
        /// <param name="height"></param>
        /// <param name="origX"></param>
        /// <param name="origY"></param>
        public void GetImageParam(string paramsFile, out double length, out double height, out double origX, out double origY)
        {
            bool bPerspEnable = false;
            int nPerspMethod = 0;
            int lFrameX = 0, lFrameY = 0;
            int lFinalDimX = 0, lFinalDimY = 0;
            float fPixelRatio = 0;
            int dpi = 0;

            GlobDef mGlobDef = new GlobDef();
            //mGlobDef.CFG_IMAGE_FILENAME = imageFile;
            mGlobDef.CFG_PARAMS_FILENAME = paramsFile;

            Params gPar = new Params(mGlobDef);
            Coords gCoord = new Coords();
            Util mUtil = new Util(mGlobDef, gPar, gCoord);

            Breton.Polygons.Point PointTmp = new Breton.Polygons.Point();
            int[] alPtImage = new int[mGlobDef.NPOINTS * 2];
            int[] alPtReal = new int[mGlobDef.NPOINTS * 2];

            //Legge i parametri di impostazione della prospettiva
            int j = 0;
            for (int i = 0; i <= mGlobDef.NPOINTS - 1; i++)
            {
                gPar.read_p_metric(i, PointTmp);
                alPtReal[j] = (int)PointTmp.X;
                alPtReal[j + 1] = (int)PointTmp.Y;

                gPar.read_p_pixel(i, PointTmp);
                alPtImage[j] = (int)PointTmp.X;
                alPtImage[j + 1] = (int)PointTmp.Y;

                j = j + 2;
            }

            gPar.read_perspective_correction_params(ref bPerspEnable, ref nPerspMethod, ref lFrameX,
                        ref lFrameY, ref lFinalDimX, ref lFinalDimY, ref fPixelRatio, ref dpi);

            length = GetRatio(lFrameX, lFrameY, lFinalDimX, lFinalDimY, alPtReal, true);
            height = GetRatio(lFrameX, lFrameY, lFinalDimX, lFinalDimY, alPtReal, false);

            origX = (length / lFinalDimX) * lFrameX;
            origY = (height / lFinalDimY) * lFrameY;

        }

        /// <summary>
        /// Restituisce il rapporto tra le dimensioni della foto
        /// </summary>
        /// <param name="frameX"></param>
        /// <param name="frameY"></param>
        /// <param name="finalDimX"></param>
        /// <param name="finalDimY"></param>
        /// <param name="ptReal"></param>
        /// <param name="isLength"></param>
        /// <returns></returns>
        private double GetRatio(int frameX, int frameY, int finalDimX, int finalDimY, int[] ptReal, bool isLength)
        {
            double dim;

            if (isLength)
                dim = Math.Abs(((ptReal[0] - ptReal[6]) * finalDimX) / (2 * frameX - finalDimX));
            else
                dim = Math.Abs(((ptReal[1] - ptReal[3]) * finalDimY) / (2 * frameY - finalDimY));
            return dim;
        }

        /// *****************************************************************************
        /// ReadFileXml
        /// <summary>
        /// Legge la lastra dal file XML
        /// </summary>
        /// <param name="parametersFilepath">nome del file parametri</param>
        /// <returns>
        /// true	lettura eseguita con successo
        /// false	errore nella lettura
        /// </returns>
        public bool GetXmlGraphicParameters(string parametersFilepath)
        {
            System.IO.FileStream fs = null;
            System.IO.BinaryWriter bw = null;
            XmlDocument doc = new XmlDocument();
            char[] buffer;
            string stbuffer;

            try
            {
                string filename = Code + ".xml";
                string filepath = System.IO.Path.Combine(ApplicationRun.Instance.TempFolder, filename);

                int start = XmlContent.IndexOf("<Slab", StringComparison.InvariantCulture);
                string pureContent = XmlContent.Substring(start);
                File.WriteAllText(filepath, XmlContent);


                // Legge il file XML
                doc.LoadXml(XmlContent);

                XmlNodeList l = doc.SelectNodes("/Slab/*");


                // Scorre tutti i sottonodi
                foreach (XmlNode chn in l)
                {
                    // Legge le informazioni generali della lastra
                    if (chn.Name == "GeneralInfo")
                    {
                        //// Legge la sorgente del file xml
                        //if (chn.ParentNode.Attributes.GetNamedItem("Source") != null)
                        //    mSource = chn.ParentNode.Attributes.GetNamedItem("Source").Value;

                        //// Legge l'attributo che indica se l'immagine è normalizzata
                        //if (chn.ParentNode.Attributes.GetNamedItem("Normalized") != null)
                        //    mNormalized = bool.Parse(chn.ParentNode.Attributes.GetNamedItem("Normalized").Value);

                        if (!ReadXmlGeneralInfo(chn))
                            return false;
                    }

                    //// Legge i percorsi
                    if (chn.Name == "Zone")
                    {
                        _zone = new Zone();
                        if (!_zone.ReadFileXml(chn, -1.0))
                            return false;

                        _zoneLoaded = true;
                    }

                    // Legge il file boundaries
                    //if (chn.Name == "Boundaries" && fileBoundaries != "")
                    //{
                    //    fs = new System.IO.FileStream(fileBoundaries, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    //    bw = new System.IO.BinaryWriter(fs);
                    //    stbuffer = chn.FirstChild.Value;
                    //    buffer = stbuffer.ToCharArray();
                    //    bw.Write(buffer);
                    //    bw.Close();
                    //    fs.Close();
                    //    boundariesFileLoaded = true;
                    //}

                    // Legge il file scanner
                    //else if (chn.Name == "ScannerSlab" && fileScanner != "")
                    //{
                    //    fs = new System.IO.FileStream(fileScanner, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    //    bw = new System.IO.BinaryWriter(fs);
                    //    stbuffer = chn.FirstChild.Value;
                    //    buffer = stbuffer.ToCharArray();
                    //    bw.Write(buffer);
                    //    bw.Close();
                    //    fs.Close();
                    //}

                    // Legge il file parametri
                    else if (chn.Name == "PhotoParameters" && parametersFilepath != "")
                    {
                        //fs = new System.IO.FileStream(fileParam, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                        //bw = new System.IO.BinaryWriter(fs);
                        //stbuffer = chn.FirstChild.Value;
                        //buffer = stbuffer.ToCharArray();

                        string content = chn.FirstChild.Value;
                        File.WriteAllText(parametersFilepath, content);
                        //bw.Write(buffer);
                        //bw.Close();
                        //fs.Close();
                    }
                    //else if (chn.Name == "DeformationY")
                    //{
                    //    if (!ReadDeformationParameters(chn))
                    //        return false;
                    //}
                    //else if (chn.Name == "PostElab")
                    //{
                    //    if (!ReadPostElabParameters(chn))
                    //        return false;
                    //}
                    //else if (chn.Name == "PixelDimension")
                    //{
                    //    // Verifica se è richiesta anche la lettura delle dimensioni pixel
                    //    if (pixelZone)
                    //    {
                    //        // Scorre tutti i sottonodi
                    //        foreach (XmlNode nd in chn.ChildNodes)
                    //        {
                    //            if (nd.Name == "Zone")
                    //            {
                    //                mPixelZone = new Zone();
                    //                if (!mPixelZone.ReadFileXml(nd, autoSimplify))
                    //                    return false;

                    //                zonePixelLoaded = true;
                    //            }

                    //            // Legge il file boundaries
                    //            else if (nd.Name == "Boundaries" && fileBoundariesPixel != "")
                    //            {
                    //                fs = new System.IO.FileStream(fileBoundariesPixel, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                    //                bw = new System.IO.BinaryWriter(fs);
                    //                stbuffer = nd.FirstChild.Value;
                    //                buffer = stbuffer.ToCharArray();
                    //                bw.Write(buffer);
                    //                bw.Close();
                    //                fs.Close();
                    //                boundariesFilePixelLoaded = true;
                    //            }
                    //        }

                    //        // Se non è stato trovato il nodo Zone, cerca di caricare la Zone usando il fileBoundaries
                    //        if (!zonePixelLoaded && boundariesFilePixelLoaded)
                    //        {
                    //            mPixelZone = new Zone();

                    //            if (!ReadBoundariesFile(fileBoundariesPixel, true))
                    //                throw new ApplicationException("Error reading boundaries file (Pixel)");
                    //        }
                    //    }
                    //}
                }

                // Se non è stato trovato il nodo Zone, cerca di caricare la Zone usando il fileBoundaries
                //if (!zoneLoaded && boundariesFileLoaded)
                //{
                //    mZone = new Zone();

                //    if (!ReadBoundariesFile(fileBoundaries, false))
                //        throw new ApplicationException("Error reading boundaries file");
                //}

                return true;
            }

            catch (XmlException ex)
            {
                TraceLog.WriteLine("Slab.ReadFileXml Error.", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Slab.ReadFileXml Error.", ex);
                return false;
            }
            finally
            {
                if (bw != null) bw.Close();
                if (fs != null) fs.Close();
                doc = null;
            }
        }

        private bool ReadXmlGeneralInfo(XmlNode n, bool force = true)
        {
            try
            {
                // Scorre tutti i sottonodi
                foreach (XmlNode chn in n.ChildNodes)
                {
                    // Legge i percorsi
                    if (chn.Name == "SlabIdentity")
                    {
                        //if ((mCode == "" || force) && chn.Attributes.GetNamedItem("Code") != null)
                        //    mCode = chn.Attributes.GetNamedItem("Code").Value;

                        //if ((mMaterialCode == "" || force) && chn.Attributes.GetNamedItem("Material_code") != null)
                        //    mMaterialCode = chn.Attributes.GetNamedItem("Material_code").Value;

                        //if ((mSupplierCode == "" || force) && chn.Attributes.GetNamedItem("Supplier_code") != null)
                        //    mSupplierCode = chn.Attributes.GetNamedItem("Supplier_code").Value;

                        //if ((mQualityCode == "" || force) && chn.Attributes.GetNamedItem("Quality_code") != null)
                        //    mQualityCode = chn.Attributes.GetNamedItem("Quality_code").Value;

                        //if ((mBatchCode == "" || force) && chn.Attributes.GetNamedItem("BatchCode") != null)
                        //    mBatchCode = chn.Attributes.GetNamedItem("BatchCode").Value;

                        //if ((mDescription == "" || force) && chn.Attributes.GetNamedItem("Description") != null)
                        //    mDescription = chn.Attributes.GetNamedItem("Description").Value;

                        //if ((mQty == 0d || force) && chn.Attributes.GetNamedItem("Qty") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Qty").Value, out mQty, CultureInfo.InvariantCulture);

                        //if ((mSurface == 0d || force) && chn.Attributes.GetNamedItem("Surface") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Surface").Value, out mSurface, CultureInfo.InvariantCulture);

                        //if ((mCommSurface == 0d || force) && chn.Attributes.GetNamedItem("Commercial_Surface") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Commercial_Surface").Value, out mCommSurface, CultureInfo.InvariantCulture);

                        //if ((mInsertDate == DateTime.MinValue || force) && chn.Attributes.GetNamedItem("Insert_date") != null)
                        //    mInsertDate = DateTime.Parse(chn.Attributes.GetNamedItem("Insert_date").Value);

                        //if ((mTone == "" || force) && chn.Attributes.GetNamedItem("Tone") != null)
                        //    mTone = chn.Attributes.GetNamedItem("Tone").Value;

                        //if ((mPosStopX == 0d || force) && chn.Attributes.GetNamedItem("Stop_x") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Stop_x").Value, out mPosStopX, CultureInfo.InvariantCulture);

                        //if ((mPosStopY == 0d || force) && chn.Attributes.GetNamedItem("Stop_y") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Stop_y").Value, out mPosStopY, CultureInfo.InvariantCulture);

                        //if ((mSlabCost == 0d || force) && chn.Attributes.GetNamedItem("Unit_cost") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Unit_cost").Value, out mSlabCost, CultureInfo.InvariantCulture);

                        //if ((mDefectNumber == 0 || force) && chn.Attributes.GetNamedItem("Defects") != null)
                        //    mDefectNumber = int.Parse(chn.Attributes.GetNamedItem("Defects").Value);

                        //if ((mTypeCode == "" || force) && chn.Attributes.GetNamedItem("Type") != null)
                        //    mTypeCode = chn.Attributes.GetNamedItem("Type").Value;

                        //if ((mStateArt == "" || force) && chn.Attributes.GetNamedItem("State") != null)
                        //    mStateArt = chn.Attributes.GetNamedItem("State").Value;
                    }
                    else if (chn.Name == "Dimension" && chn.Attributes != null)
                    {
                        if ((_dimX == 0d || force) && chn.Attributes.GetNamedItem("X") != null)
                            MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out _dimX, CultureInfo.InvariantCulture);
                        if ((_dimY == 0d || force) && chn.Attributes.GetNamedItem("Y") != null)
                            MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out _dimY, CultureInfo.InvariantCulture);
                        if ((_dimZ == 0d || force) && chn.Attributes.GetNamedItem("Z") != null)
                            MathUtil.IsDouble(chn.Attributes.GetNamedItem("Z").Value, out _dimZ, CultureInfo.InvariantCulture);
                        //if ((mInternalRectLeft == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_left") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_left").Value, out mInternalRectLeft, CultureInfo.InvariantCulture);
                        //if ((mInternalRectTop == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_top") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_top").Value, out mInternalRectTop, CultureInfo.InvariantCulture);
                        //if ((mInternalRectWidth == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_width") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_width").Value, out mInternalRectWidth, CultureInfo.InvariantCulture);
                        //if ((mInternalRectHeight == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_height") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_height").Value, out mInternalRectHeight, CultureInfo.InvariantCulture);
                        //if ((mInternalRectAngle == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_angle") != null)
                        //    MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_angle").Value, out mInternalRectAngle, CultureInfo.InvariantCulture);
                    }
                    //else if (chn.Name == "DimensionPx")
                    //{
                    //    if ((mXPx == 0d || force) && chn.Attributes.GetNamedItem("X") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out mXPx, CultureInfo.InvariantCulture);
                    //    if ((mYPx == 0d || force) && chn.Attributes.GetNamedItem("Y") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out mYPx, CultureInfo.InvariantCulture);
                    //    if ((mInternalRectLeftPx == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_left") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_left").Value, out mInternalRectLeftPx, CultureInfo.InvariantCulture);
                    //    if ((mInternalRectTopPx == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_top") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_top").Value, out mInternalRectTopPx, CultureInfo.InvariantCulture);
                    //    if ((mInternalRectWidthPx == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_width") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_width").Value, out mInternalRectWidthPx, CultureInfo.InvariantCulture);
                    //    if ((mInternalRectHeightPx == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_height") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_height").Value, out mInternalRectHeightPx, CultureInfo.InvariantCulture);
                    //    if ((mInternalRectAnglePx == 0d || force) && chn.Attributes.GetNamedItem("Internal_rect_angle") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Internal_rect_angle").Value, out mInternalRectAnglePx, CultureInfo.InvariantCulture);
                    //}
                    else if (chn.Name == "Offset" && chn.Attributes != null)
                    {
                        if ((_photoOffsetX == 0d || force) && chn.Attributes.GetNamedItem("X") != null)
                            MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out _photoOffsetX, CultureInfo.InvariantCulture);
                        if ((_photoOffsetY == 0d || force) && chn.Attributes.GetNamedItem("Y") != null)
                            MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out _photoOffsetY, CultureInfo.InvariantCulture);
                    }
                    //else if (chn.Name == "OffsetPx")
                    //{
                    //    if ((mOffsetXPx == 0d || force) && chn.Attributes.GetNamedItem("X") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out mOffsetXPx, CultureInfo.InvariantCulture);
                    //    if ((mOffsetYPx == 0d || force) && chn.Attributes.GetNamedItem("Y") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out mOffsetYPx, CultureInfo.InvariantCulture);
                    //}
                    //else if (chn.Name == "Target0")
                    //{
                    //    double x, y;
                    //    if (chn.Attributes.GetNamedItem("X") != null && chn.Attributes.GetNamedItem("Y") != null)
                    //    {
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out x, CultureInfo.InvariantCulture);
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out y, CultureInfo.InvariantCulture);
                    //        mTarget0 = new Breton.Polygons.Point(x, y);
                    //    }
                    //}
                    //else if (chn.Name == "Target1")
                    //{
                    //    double x, y;
                    //    if (chn.Attributes.GetNamedItem("X") != null && chn.Attributes.GetNamedItem("Y") != null)
                    //    {
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out x, CultureInfo.InvariantCulture);
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out y, CultureInfo.InvariantCulture);
                    //        mTarget1 = new Breton.Polygons.Point(x, y);
                    //    }
                    //}
                    //else if (chn.Name == "Target0Px")
                    //{
                    //    double x, y;
                    //    if (chn.Attributes.GetNamedItem("X") != null && chn.Attributes.GetNamedItem("Y") != null)
                    //    {
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out x, CultureInfo.InvariantCulture);
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out y, CultureInfo.InvariantCulture);
                    //        mTarget0Px = new Breton.Polygons.Point(x, y);
                    //    }
                    //}
                    //else if (chn.Name == "Target1Px")
                    //{
                    //    double x, y;
                    //    if (chn.Attributes.GetNamedItem("X") != null && chn.Attributes.GetNamedItem("Y") != null)
                    //    {
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out x, CultureInfo.InvariantCulture);
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out y, CultureInfo.InvariantCulture);
                    //        mTarget1Px = new Breton.Polygons.Point(x, y);
                    //    }
                    //}
                    //else if (chn.Name == "Scale")
                    //{
                    //    if ((mScaleX == 0d || force) && chn.Attributes.GetNamedItem("X") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("X").Value, out mScaleX, CultureInfo.InvariantCulture);
                    //    if ((mScaleY == 0d || force) && chn.Attributes.GetNamedItem("Y") != null)
                    //        MathUtil.IsDouble(chn.Attributes.GetNamedItem("Y").Value, out mScaleY, CultureInfo.InvariantCulture);
                    //}
                }
                return true;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Slab.ReadXmlGeneralInfo Error.", ex);
                return false;
            }
        }


        /// ********************************************************************
        /// <summary>
        /// Legge il lato dal file XML
        /// </summary>
        /// <param name="n">nodo XML contenete il lato</param>
        /// <param name="force"></param>
        /// <returns>
        ///		true	lettura eseguita con successo
        ///		false	errore nella lettura
        ///	</returns>
        /// ********************************************************************


        #endregion

        internal void GetSlabImagePosition(out double length, out double height, out double origX, out double origY)
        {
            length = height = origX = origY = 0;
            string filename = Code + ".ini";
            string filepath = System.IO.Path.Combine(ApplicationRun.Instance.TempFolder, filename);
            if (GetXmlGraphicParameters(filepath))
            {
                GetImageParam(filepath, out length, out height, out  origX, out origY);
                _originX = origX;
                _originY = origY;
            }
        }
    }
}
