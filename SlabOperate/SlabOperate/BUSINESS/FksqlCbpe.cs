﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Breton.DesignCenterInterface;
using Breton.MathUtils;
using BretonViewportLayout.Wpf;
using SlabOperate.SESSION;
using TraceLoggers;
using Path = Breton.Polygons.Path;

namespace SlabOperate.BUSINESS
{
    public class FksqlCbpe : CBPExtension, INotifyPropertyChanged
    {
        #region >-------------- Constants and Enums

        private enum StatiLavorazione
        {
            STATOLAV_NULL = 100,             // nessuno stato lavorazione
            STATOLAV_IN_LAVORO,              // in lavoro
            STATOLAV_LAVORATO                // lavorato
        }


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private GraphicSlab _currentSlab = null;

        //private PrintablePiece _currentPiece = null;

        private bool _isNotifyingCurrentPiece = false;

        private bool _externalUpdatingPiece = false;

        private Timer _pieceRedrawTimer = new Timer();

        private object _lockObj = new object();

        ////private PieceDbStatus _previousCurrentPieceDbStatus;

        ////private PieceDbType _previousCurrentPieceDbType;

        ////private readonly int pieceFontSize = CurrentSession.CurrentSettings.PiecesFontSize;

        ////private readonly double pieceFontLineSpacing = CurrentSession.CurrentSettings.PiecesFontLineSpacing;

        private object _pieceLabelSymbolEntities = null;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public FksqlCbpe(Breton2DViewer viewer, AxisDirection axisDirection = AxisDirection.LEFT_BOTTOM)
            : base(viewer, axisDirection, true, false, false)
        {

            //_pieceRedrawTimer.Interval = 50;
            //_pieceRedrawTimer.Elapsed += _pieceRedrawTimer_Elapsed;

            DrawMouseCoordinates = false;

            DrawMouseTooltip = false;

            //base.evMouseAfter += FkCbpe_evMouseAfter;

            //base.evClick += FkCbpe_evClick;



            regionColorAlphaLevel = 100;
            //RegionHilightColor = Color.Cyan;
            //RegionHilightOnlyBorder = true;
            //UseHilightColorOnColorRegions = false;
        }

        void _pieceRedrawTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ////_pieceRedrawTimer.Stop();
            ////Dispatcher.CurrentDispatcher.Invoke(new Action(RedrawCurrentPiece));
            //////RedrawCurrentPiece();
        }

        //void FkCbpe_evClick(object sender, CBasicPaintEventArgs e, List<int> block_no, List<int> path_no, List<int> elem_no, List<int> vertex_no)
        //{
        //    if (e.MouseKey != CBasicPaintEventArgs.enMouseKey.MouseKey_Left) return;

        //    //SelectPiece(_currentPiece, false);
        //    bool found = false;

        //    if (block_no.Count > 0)
        //    {
        //        foreach (var b in block_no)
        //        {
        //            if (b != -1)
        //            {
        //                var block = Blocks[b];
        //                var piece = CurrentSlab.Pieces.FirstOrDefault(p => p.Name == block.Name);
        //                if (piece != null)
        //                {
        //                    //SelectPiece(piece, true);
        //                    CurrentPiece = piece;
        //                    found = true;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    if (!found)
        //    {
        //        CurrentPiece = null;
        //    }
        //    e.Accepted = true;
        //}

        void FkCbpe_evMouseAfter(object sender, double x, double y, double z)
        {
            //throw new NotImplementedException();
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public GraphicSlab CurrentSlab
        {
            get { return _currentSlab; }
            set
            {
                _currentSlab = value;
                RefreshCurrentSlab();
            }
        }

        private void RefreshCurrentSlab()
        {
            LoadSlab();
            AddSlabText();
        }

        /// <summary>
        /// Inserisce i testi con le informazioni relative alla lastra
        /// </summary>
        /// <param name="cbpe"></param>
        private void AddSlabText()
        {
            //ArticleCollection tmpArts = new ArticleCollection(mDbInterface);
            //Article art;
            //MaterialCollection tmpMats = new MaterialCollection(mDbInterface);
            //Material mat;

            //// Legge dal database il dettaglio legato allo shape che si sta usando per il Drag&Drop				
            //tmpArts.GetSingleElementFromDb(cbpe.Slab.Name);
            //tmpArts.GetObject(out art);
            //tmpMats.GetSingleElementFromDb(art.gMaterialCode);
            //tmpMats.GetObject(out mat);
            //fThickness.DoubleValue = art.gDimZ;


            AddText(new CText(string.Format("Lastra: {0}\nMateriale: {1}\nDimensioni: {2}\nSpessore: {3:#0}",
                            _currentSlab.Code, _currentSlab.MaterialName, _currentSlab.InnerEntity.Dimensions, CurrentSlab.InnerEntity.Thickness), Color.White, 30, 0, -100, CBPExtension.LayersType.TEXT.ToString()) { Alignment = TextAlignment.Left });

            //cbpe.ZoomAll();
            
        }


        ////public PrintablePiece CurrentPiece
        ////{
        ////    get { return _currentPiece; }
        ////    private set
        ////    {
        ////        bool hasChanged = (value != _currentPiece);
        ////        DetachCurrentPieceEvents();
        ////        SelectPiece(_currentPiece, false);

        ////        _currentPiece = value;

        ////        if (_currentPiece != null)
        ////        {
        ////            _previousCurrentPieceDbStatus = _currentPiece.DbStatus;
        ////            _previousCurrentPieceDbType = _currentPiece.DbType;
        ////        }
        ////        SelectPiece(_currentPiece, true);
        ////        AttachCurrentPieceEvents();
        ////        if (_externalUpdatingPiece)
        ////        {
        ////            RefreshCurrentPiece();
        ////        }
        ////        else
        ////        {
        ////            _isNotifyingCurrentPiece = true;
        ////            OnPropertyChanged("CurrentPiece");
        ////        }

        ////    }
        ////}

        ////private object PieceLabelSymbolEntities
        ////{
        ////    get
        ////    {
        ////        if (_pieceLabelSymbolEntities == null)
        ////        {
        ////            _pieceLabelSymbolEntities = GetDxfFileEntities(CurrentSession.PieceLabelSymbolDxfFilepath);
        ////        }
        ////        return _pieceLabelSymbolEntities;
        ////    }
        ////    set { _pieceLabelSymbolEntities = value; }
        ////}

        ////private void AttachCurrentPieceEvents()
        ////{
        ////    if (_currentPiece != null)
        ////    {
        ////        _currentPiece.PropertyChanged += _currentPiece_PropertyChanged;
        ////    }
        ////}

        ////private void DetachCurrentPieceEvents()
        ////{
        ////    if (_currentPiece != null)
        ////    {
        ////        _currentPiece.PropertyChanged -= _currentPiece_PropertyChanged;
        ////    }
        ////}

        ////void _currentPiece_PropertyChanged(object sender, PropertyChangedEventArgs e)
        ////{
        ////    switch (e.PropertyName)
        ////    {
        ////        case "DbType":
        ////            if (CurrentPiece.DbType != _previousCurrentPieceDbType)
        ////            {
        ////                _previousCurrentPieceDbType = CurrentPiece.DbType;
        ////                RedrawCurrentPiece();
        ////            }
        ////            break;

        ////        case "DbStatus":
        ////            if (CurrentPiece.DbStatus != _previousCurrentPieceDbStatus)
        ////            {
        ////                _previousCurrentPieceDbStatus = CurrentPiece.DbStatus;
        ////                RedrawCurrentPiece();
        ////            }
        ////            //_pieceRedrawTimer.Stop();
        ////            //_pieceRedrawTimer.Start();
        ////            break;

        ////        default:
        ////            break;
        ////    }
        ////}

        ////private void RedrawCurrentPiece()
        ////{
        ////    lock (_lockObj)
        ////    {
        ////        if (_currentPiece == null || _currentSlab == null) return;

        ////        if (_currentSlab.CutType == CUT_TYPES.F_CUT_TYPE_QUADRANTS
        ////            || _currentSlab.CutType == CUT_TYPES.F_CUT_TYPE_STRIPS)
        ////        {
        ////            foreach (var piece in _currentPiece.ReferredGroup.Pieces)
        ////            {
        ////                DeletePiece(piece);
        ////                //LoadPiece(piece, _currentSlab.GraphicOffsetX, _currentSlab.GraphicOffsetY);
        ////                LoadPiece(piece, _currentSlab.OffsetX, _currentSlab.OffsetY);
        ////            }
        ////        }
        ////        else
        ////        {
        ////            DeletePiece(_currentPiece, false);
        ////            //LoadPiece(_currentPiece, _currentSlab.GraphicOffsetX, _currentSlab.GraphicOffsetY);
        ////            LoadPiece(_currentPiece, _currentSlab.OffsetX, _currentSlab.OffsetY);
        ////        }

        ////        this.RepaintPiece(_currentPiece);
        ////        SelectPiece(_currentPiece, true);
        ////    }

        ////}

        ////private void RepaintPiece(PrintablePiece piece)
        ////{
        ////    if (piece == null) return;

        ////    var block = Blocks.Values.FirstOrDefault(b => b.Name == piece.Name);
        ////    if (block != null)
        ////    {
        ////        base.Repaint(block.Id);
        ////    }
        ////}

        ////private void DeletePiece(PrintablePiece piece, bool redraw = true)
        ////{
        ////    if (piece == null) return;

        ////    var block = Blocks.Values.FirstOrDefault(b => b.Name == piece.Name);
        ////    if (block != null)
        ////    {
        ////        base.RemoveBlock(block.Id, redraw);
        ////    }


        ////}

        ////private void SelectPiece(PrintablePiece piece, bool select)
        ////{
        ////    if (piece != null)
        ////    {
        ////        var block = Blocks.Values.FirstOrDefault(b => b.Name == piece.Name);
        ////        if (block != null)
        ////        {
        ////            if (select)
        ////                Select(block.Id);
        ////            else
        ////            {
        ////                UnSelect(block.Id);
        ////            }
        ////        }
        ////    }

        ////}
        ////private void RefreshCurrentPiece()
        ////{
        ////    SelectPiece(_currentPiece, true);

        ////}

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        //public void SetCurrentPiece(PrintablePiece piece)
        //{
        //    _externalUpdatingPiece = true;
        //    if (!_isNotifyingCurrentPiece)
        //    {
        //        CurrentPiece = piece;
        //    }
        //    _isNotifyingCurrentPiece = false;
        //    _externalUpdatingPiece = false;
        //}

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void LoadSlab()
        {
            ////ClearAll();

            if (_currentSlab == null)
            {
                return;
            }

            ////if (_currentSlab.CutType == CUT_TYPES.F_CUT_TYPE_POLYGONS)
            ////{
            ////    RegionHilightColor = Color.Cyan;
            ////    UseHilightColorOnColorRegions = true;
            ////    RegionHilightOnlyBorder = false;
            ////    //regionColorAlphaLevel = 100;
            ////}
            ////else
            ////{
            ////    RegionHilightColor = Color.Cyan;
            ////    UseHilightColorOnColorRegions = true;
            ////    RegionHilightOnlyBorder = true;
            ////    //regionColorAlphaLevel = 255;
            ////}

            double length;
            double height;
            double origX;
            double origY;

            try
            {
                _currentSlab.GetSlabImagePosition(out length, out height, out origX, out origY);


                //int num_slab = AddSlab(_currentSlab.Code, _currentSlab.Zone.GetPath(), origX, origY);

                int num_slab = AddSlab(_currentSlab.Code, _currentSlab.Zone.GetPath(), _currentSlab.GraphicOffsetX, _currentSlab.GraphicOffsetY);

                //LoadSlabImage(num_slab, length, height, origX, origY);
                LoadSlabImage(num_slab, length, height, origX - _currentSlab.PhotoAdjustX, origY - _currentSlab.PhotoAdjustY);



                ////LoadSlabPieces(_currentSlab.OffsetX, _currentSlab.OffsetY);
                //LoadSlabPieces(_currentSlab.GraphicOffsetX, _currentSlab.GraphicOffsetY);
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("FkCbpe LoadSlab error ", ex);
            }

            this.Repaint();
            this.ZoomAll();

        }

        ////private void LoadSlabPieces(double slabOriginX, double slabOriginY)
        ////{
        ////    try
        ////    {
        ////        foreach (var piece in _currentSlab.Pieces)
        ////        {
        ////            LoadPiece(piece, slabOriginX, slabOriginY);
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        TraceLog.WriteLine("FkCbpe LoadSlabPieces error ", ex);
        ////    }
        ////}

        ////private void LoadPiece(PrintablePiece piece, double slabOriginX, double slabOriginY)
        ////{
        ////    if (piece == null) return;

        ////    piece.BuildGraphicPaths(slabOriginX, slabOriginY);
        ////    int blockNumber = -1;


        ////    var surfaceGraphicPath = piece.GraphicPaths.FirstOrDefault(p => p.PathType == ToolpathType.Surface
        ////        || p.PathType == ToolpathType.ShapeSurface);

        ////    #region Sagoma principale

        ////    if (surfaceGraphicPath != null)
        ////    {
        ////        LayersType destinationLayer = GetDestinationLayer(piece);
        ////        blockNumber = AddShape(surfaceGraphicPath.Contour.GetPathRT(), destinationLayer);

        ////        //Breton.Polygons.Point baricenter = surfaceGraphicPath.Contour.BarycentreRT();

        ////        double distance;

        ////        Breton.Polygons.Point baricenter = surfaceGraphicPath.Contour.BarycentreExRT(true, out distance);


        ////        //CText text = new CText(piece.Prog.ToString(), Color.White, pieceFontSize, baricenter.X, baricenter.Y, LayersType.TEXT.ToString());

        ////        string labelContent = piece.Prog.ToString();
        ////        if (!string.IsNullOrEmpty(CurrentSession.CurrentSettings.PiecesLabelStringFormat))
        ////        {
        ////            labelContent = BuildPieceLabelContent(piece, CurrentSession.CurrentSettings.PiecesLabelStringFormat);
        ////        }

        ////        CText text = new CText(labelContent, Color.White, pieceFontSize, baricenter.X, baricenter.Y, LayersType.TEXT.ToString(), TextAlignment.Left);


        ////        object entityData = blockNumber.ToString(CultureInfo.InvariantCulture) + "#0" + "#-1";

        ////        AddText(text, pieceFontLineSpacing, entityData, surfaceGraphicPath.Contour, true);
        ////    }
        ////    #endregion Sagoma principale

        ////    if (blockNumber != -1)
        ////    {
        ////        Blocks[blockNumber].Name = piece.Name;

        ////        #region Fori

        ////        var holesGraphicPaths = piece.GraphicPaths.Where(p => p.PathType == ToolpathType.Hole).ToList();
        ////        foreach (var hole in holesGraphicPaths)
        ////        {
        ////            AddShapeHole(blockNumber, hole.Contour.GetPathRT());
        ////        }

        ////        #endregion Fori

        ////        #region Campiture

        ////        // Aggiungi croce se serve su pezzi di filagna
        ////        if (piece.IsDiscarded
        ////            && (_currentSlab.CutType == CUT_TYPES.F_CUT_TYPE_QUADRANTS
        ////                || _currentSlab.CutType == CUT_TYPES.F_CUT_TYPE_STRIPS)
        ////            && surfaceGraphicPath != null)
        ////        {
        ////            Path crossPath = new Path();
        ////            crossPath.AddSide(new Segment(surfaceGraphicPath.Contour.GetVertexRT(0),
        ////                surfaceGraphicPath.Contour.GetVertexRT(2)));
        ////            crossPath.Layer = LayersType.FK_PIECE_FIL_MISC.ToString();
        ////            Blocks[blockNumber].AddPath(crossPath);

        ////            crossPath = new Path();
        ////            crossPath.AddSide(new Segment(surfaceGraphicPath.Contour.GetVertexRT(1),
        ////                surfaceGraphicPath.Contour.GetVertexRT(3)));
        ////            crossPath.Layer = LayersType.FK_PIECE_FIL_MISC.ToString();
        ////            Blocks[blockNumber].AddPath(crossPath);

        ////        }
        ////        // Aggiungi campitura se serve su pezzi di tipo Polygon
        ////        if (piece.IsDiscarded
        ////            && (_currentSlab.CutType == CUT_TYPES.F_CUT_TYPE_POLYGONS)
        ////            && surfaceGraphicPath != null)
        ////        {
        ////            string attributeKey = "HATCHNAME";
        ////            string hatchName = "ANSI37";
        ////            var mainPath = Blocks[blockNumber][0];
        ////            var hatchAttribute = mainPath.Attributes.Keys.FirstOrDefault(k => k == attributeKey);
        ////            if (hatchAttribute == null)
        ////            {
        ////                mainPath.Attributes.Add(attributeKey, hatchName);
        ////            }
        ////            else
        ////            {
        ////                mainPath.Attributes[attributeKey] = hatchName;
        ////            }

        ////        }

        ////        #endregion Campiture

        ////        #region Simboli

        ////        // Aggiungi eventuali simboli

        ////        if (piece.GraphicSymbolPaths.Count > 0)
        ////        {
        ////            LayersType destinationLayer = LayersType.SYMBOL;

        ////            foreach (var symbolPath in piece.GraphicSymbolPaths)
        ////            {
        ////                //Path contourRT = symbolPath.Contour.GetPathRT();
        ////                //contourRT.Layer = destinationLayer.ToString();
        ////                //Blocks[blockNumber].AddPath(contourRT);
        ////                int dummyNumber = AddShape(symbolPath.Contour.GetPathRT(), destinationLayer);
        ////            }
        ////        }

        ////        #endregion Simboli

        ////        #region Piazzamento etichetta definita su DB

        ////        if (surfaceGraphicPath != null && !string.IsNullOrEmpty(piece.XmlDbLabelContent))
        ////        {
        ////            LayersType destinationLayer = LayersType.PIECE_LABEL;

        ////            double xPosition, yPosition;

        ////            GetLabelPosition(piece, out xPosition, out yPosition);

        ////            Path labelSymbolPath = null;

        ////            if (File.Exists(CurrentSession.PieceLabelSymbolDxfFilepath))
        ////            {
        ////                object entityData = blockNumber.ToString(CultureInfo.InvariantCulture) + "#0" + "#-1";

        ////                //InsertDxfFile(CurrentSession.PieceLabelSymbolDxfFilepath, xPosition, yPosition,
        ////                //    surfaceGraphicPath.Contour.MatrixRT, destinationLayer.ToString(), entityData);
        ////                InsertEntities(PieceLabelSymbolEntities, xPosition, yPosition,
        ////                    surfaceGraphicPath.Contour.MatrixRT, destinationLayer.ToString(), entityData);

        ////            }
        ////            else
        ////            {
        ////                labelSymbolPath = BuildDefaultLabelSymbolPath();
        ////                AddLabelSymbol(surfaceGraphicPath.Contour, labelSymbolPath, xPosition, yPosition, destinationLayer);
        ////            }
        ////        }

        ////        #endregion Piazzamento etichetta definita su DB

        ////    }

        ////}

        private Path GetDxfLabelSymbolPath(string dxfFilepath)
        {
            Path returnPath = null;

            //if (!File.Exists(dxfFilepath))
            //    return returnPath;

            //try
            //{
            //    var ra = new ReadAutodesk(nomeFileProfilo);
            //    ra.DoWork();
            //    var raEntitiesList = ra.Entities.Cast<ICurve>().ToList();


            //    CompositeCurve profile = new CompositeCurve(raEntitiesList, 1e-2, true);

            //}
            //catch (Exception ex)
            //{
            //    TraceLog.WriteLine("FkCbpe GetDxfLabelSymbolPath error ", ex);

            //}

            return returnPath;
        }

        ////private Path BuildDefaultLabelSymbolPath()
        ////{
        ////    double halfWidth = (double)CurrentSession.CurrentSettings.PiecesLabelSymbolWidth / 2;
        ////    double halfHeight = (double)CurrentSession.CurrentSettings.PiecesLabelSymbolHeight / 2;

        ////    Path newPath = new Path(new Rect(-halfWidth, -halfHeight, halfWidth, halfHeight));
        ////    return newPath;
        ////}


        ////private void AddLabelSymbol(Path parentPath, Path symbolPath, double xPosition, double yPosition, LayersType destinationLayer)
        ////{
        ////    symbolPath.SetRotoTransl(xPosition, yPosition, 0);

        ////    RTMatrix m1 = symbolPath.MatrixRT;
        ////    RTMatrix m2 = parentPath.MatrixRT;
        ////    symbolPath.MatrixRT = m2 * m1;

        ////    AddShape(symbolPath.GetPathRT(), destinationLayer);
        ////}

        ////private void GetLabelPosition(PrintablePiece piece, out double xPosition, out double yPosition)
        ////{
        ////    xPosition = 0;
        ////    yPosition = 0;

        ////    try
        ////    {
        ////        XDocument document = XDocument.Parse(piece.XmlDbLabelContent);
        ////        var originNode = document.Descendants().FirstOrDefault(e => e.Name == "Origin");
        ////        if (originNode != null)
        ////        {
        ////            var node = originNode.Attributes().FirstOrDefault(e => e.Name == "x");
        ////            if (node != null)
        ////            {
        ////                double.TryParse(node.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out xPosition);
        ////            }
        ////            node = originNode.Attributes().FirstOrDefault(e => e.Name == "y");
        ////            if (node != null)
        ////            {
        ////                double.TryParse(node.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out yPosition);
        ////            }
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        TraceLog.WriteLine("FkCbpe GetLabelPosition error ", ex);
        ////        xPosition = 0;
        ////        yPosition = 0;
        ////    }
        ////}

        ////private string BuildPieceLabelContent(PrintablePiece piece, string formatString)
        ////{
        ////    if (string.IsNullOrEmpty(formatString))
        ////        return formatString;

        ////    var resultString = string.Empty;

        ////    var items = formatString.Split("@".ToCharArray()[0]);

        ////    try
        ////    {
        ////        if (items.Length > 1)
        ////        {
        ////            List<object> itemsValues = new List<object>();
        ////            for (int i = 1; i < items.Length; i++)
        ////            {
        ////                switch (items[i].Trim().ToUpperInvariant())
        ////                {
        ////                    case "CODE":
        ////                        itemsValues.Add(piece.Code.Trim());
        ////                        break;

        ////                    case "NAME":
        ////                        itemsValues.Add(piece.Name.Trim());
        ////                        break;

        ////                    case "ORDERCODE":
        ////                        if (piece.ReferredOrder != null)
        ////                            itemsValues.Add(piece.ReferredOrder.Code.Trim());
        ////                        else
        ////                        {
        ////                            itemsValues.Add(string.Empty);
        ////                        }
        ////                        break;

        ////                    case "SHAPECODE":
        ////                        if (piece.ReferredShape != null)
        ////                            itemsValues.Add(piece.ReferredShape.Code.Trim());
        ////                        else
        ////                        {
        ////                            itemsValues.Add(string.Empty);
        ////                        }
        ////                        break;

        ////                    case "DIMX":
        ////                        itemsValues.Add(piece.DimX);
        ////                        break;

        ////                    case "DIMY":
        ////                        itemsValues.Add(piece.DimY);
        ////                        break;

        ////                    case "DIMZ":
        ////                        itemsValues.Add(piece.ParentSlab.DimZ);
        ////                        break;

        ////                    default:
        ////                        itemsValues.Add(string.Empty);
        ////                        break;
        ////                }
        ////            }
        ////            resultString = string.Format(items[0].Trim(), itemsValues.ToArray());
        ////        }
        ////        else
        ////        {
        ////            resultString = formatString.Trim();
        ////        }
        ////    }

        ////    catch (Exception ex)
        ////    {
        ////        TraceLog.WriteLine("BuildPieceLabelContent error ", ex);
        ////        resultString = items[0].Trim();
        ////    }


        ////    resultString = resultString.Replace(@"\n", Environment.NewLine);


        ////    return resultString;
        ////}

        ////private LayersType GetDestinationLayer(PrintablePiece piece)
        ////{
        ////    LayersType targetLayer = LayersType.SHAPE;

        ////    if (piece != null && _currentSlab != null)
        ////    {
        ////        try
        ////        {
        ////            int pieceType = 0;
        ////            switch (_currentSlab.CutType)
        ////            {
        ////                #region Filagne

        ////                case CUT_TYPES.F_CUT_TYPE_QUADRANTS:
        ////                case CUT_TYPES.F_CUT_TYPE_STRIPS:

        ////                    switch (piece.DbType)
        ////                    {
        ////                        case PieceDbType.F_PIECE_TYPE_DISCARD:
        ////                        case PieceDbType.F_PIECE_TYPE_DISCARD_UNLOAD:
        ////                            pieceType = 0;
        ////                            break;

        ////                        case PieceDbType.F_PIECE_TYPE_FILLING:
        ////                        case PieceDbType.F_PIECE_TYPE_PROD:
        ////                            pieceType = 3;
        ////                            break;

        ////                        case PieceDbType.F_PIECE_TYPE_GOOD:
        ////                            pieceType = (piece.ReferredShape.Id > 0) ? 1 : 2;
        ////                            break;


        ////                        case PieceDbType.F_PIECE_TYPE_RUN:
        ////                            pieceType = 4;
        ////                            break;
        ////                    }
        ////                    switch (piece.ReferredGroup.DbStatus)
        ////                    {
        ////                        case F_GROUP_STATE.F_GROUP_STATE_READY:
        ////                            switch (pieceType)
        ////                            {
        ////                                case 0:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_QUAD0;
        ////                                    break;

        ////                                case 1:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_QUAD1;
        ////                                    break;

        ////                                case 2:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_QUAD2;
        ////                                    break;

        ////                                case 3:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_QUAD3;
        ////                                    break;

        ////                                case 4:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_QUAD4;
        ////                                    break;

        ////                            }
        ////                            break;

        ////                        case F_GROUP_STATE.F_GROUP_STATE_IN_PROGRESS:
        ////                            switch (pieceType)
        ////                            {
        ////                                case 0:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD8;
        ////                                    break;

        ////                                case 3:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD6;
        ////                                    break;

        ////                                case 4:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD4;
        ////                                    break;

        ////                                default:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD1;
        ////                                    break;
        ////                            }
        ////                            break;

        ////                        case F_GROUP_STATE.F_GROUP_STATE_UNLOADING:
        ////                            targetLayer = LayersType.FK_PIECE_FIL_SQUAD2;
        ////                            break;

        ////                        case F_GROUP_STATE.F_GROUP_STATE_COMPLETED:
        ////                            switch (pieceType)
        ////                            {
        ////                                case 0:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD9;
        ////                                    break;

        ////                                case 3:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD7;
        ////                                    break;

        ////                                case 4:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD5;
        ////                                    break;

        ////                                default:
        ////                                    targetLayer = LayersType.FK_PIECE_FIL_SQUAD3;
        ////                                    break;
        ////                            }
        ////                            break;

        ////                        case F_GROUP_STATE.STATOLAV_NULL:
        ////                            targetLayer = LayersType.FK_PIECE_FIL_QUAD1;
        ////                            break;

        ////                        case F_GROUP_STATE.STATOLAV_IN_LAVORO:
        ////                            targetLayer = LayersType.FK_PIECE_FIL_LQUAD1;
        ////                            break;

        ////                        case F_GROUP_STATE.STATOLAV_LAVORATO:
        ////                            targetLayer = LayersType.FK_PIECE_FIL_LQUAD2;
        ////                            break;


        ////                    }
        ////                    break;
        ////                #endregion Filagne

        ////                #region Poligoni

        ////                case CUT_TYPES.F_CUT_TYPE_POLYGONS:

        ////                    switch (piece.DbType)
        ////                    {
        ////                        case PieceDbType.F_PIECE_TYPE_DISCARD:
        ////                        case PieceDbType.F_PIECE_TYPE_DISCARD_UNLOAD:
        ////                            pieceType = 0;
        ////                            break;

        ////                        case PieceDbType.F_PIECE_TYPE_FILLING:
        ////                            pieceType = 3;
        ////                            break;

        ////                        case PieceDbType.F_PIECE_TYPE_PROD:
        ////                            pieceType = 3;
        ////                            break;

        ////                        case PieceDbType.F_PIECE_TYPE_GOOD:
        ////                            pieceType = 1;
        ////                            break;


        ////                        case PieceDbType.F_PIECE_TYPE_RUN:
        ////                            pieceType = 4;
        ////                            break;
        ////                    }
        ////                    switch (piece.ReferredGroup.DbStatus)
        ////                    {
        ////                        case F_GROUP_STATE.F_GROUP_STATE_READY:
        ////                            switch (pieceType)
        ////                            {
        ////                                case 0:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_QUAD0;
        ////                                    break;

        ////                                case 1:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_QUAD1;
        ////                                    break;

        ////                                case 2:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_QUAD2;
        ////                                    break;

        ////                                case 3:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_QUAD3;
        ////                                    break;

        ////                                case 4:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_QUAD4;
        ////                                    break;

        ////                            }
        ////                            break;

        ////                        case F_GROUP_STATE.F_GROUP_STATE_IN_PROGRESS:
        ////                            switch (pieceType)
        ////                            {
        ////                                case 0:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD8;
        ////                                    break;

        ////                                case 3:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD6;
        ////                                    break;

        ////                                case 4:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD4;
        ////                                    break;

        ////                                default:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD1;
        ////                                    break;
        ////                            }
        ////                            break;

        ////                        case F_GROUP_STATE.F_GROUP_STATE_UNLOADING:
        ////                            targetLayer = LayersType.FK_PIECE_POL_SQUAD2;
        ////                            break;

        ////                        case F_GROUP_STATE.F_GROUP_STATE_COMPLETED:
        ////                            switch (pieceType)
        ////                            {
        ////                                case 0:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD9;
        ////                                    break;

        ////                                case 3:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD7;
        ////                                    break;

        ////                                case 4:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD5;
        ////                                    break;

        ////                                default:
        ////                                    targetLayer = LayersType.FK_PIECE_POL_SQUAD3;
        ////                                    break;
        ////                            }
        ////                            break;

        ////                        case F_GROUP_STATE.STATOLAV_NULL:
        ////                            targetLayer = LayersType.FK_PIECE_POL_QUAD1;
        ////                            break;

        ////                        case F_GROUP_STATE.STATOLAV_IN_LAVORO:
        ////                            targetLayer = LayersType.FK_PIECE_POL_LQUAD1;
        ////                            break;

        ////                        case F_GROUP_STATE.STATOLAV_LAVORATO:
        ////                            targetLayer = LayersType.FK_PIECE_POL_LQUAD2;
        ////                            break;


        ////                    }
        ////                    break;

        ////                #endregion Poligoni

        ////            }
        ////        }
        ////        catch (Exception ex)
        ////        {
        ////            TraceLog.WriteLine(string.Format("FkCbpe.GetDestinationLayer error for piece {0}", piece.Name), ex);
        ////        }
        ////    }

        ////    return targetLayer;
        ////}

        private void LoadSlabImage(int num_slab, double length, double height, double originX, double originY)
        {
            if (!string.IsNullOrEmpty(_currentSlab.InnerEntity.SlabImage.ImagePath ))
            {
                string imagePath = _currentSlab.InnerEntity.SlabImage.ImagePath;

                //string imageName = _currentSlab.Code + ".jpg";
                //string imagePath = System.IO.Path.Combine(ApplicationRun.Instance.TempFolder, imageName);
                //if (File.Exists(imagePath))
                //{
                //    File.Delete(imagePath);
                //}
                //using (Bitmap bitmap = new Bitmap(_currentSlab.SlabImageBitmap))
                //{
                //    bitmap.Save(imagePath, ImageFormat.Jpeg);
                //}

                //this.AddBlockPhoto(mBlocks[num_slab], imagePath, length, height, _currentSlab.Code);

                this.AddBlockPhoto(mBlocks[num_slab], imagePath, length, height, _currentSlab.Code);


                //this.LoadPhoto(mBlocks[num_slab], -originX, -originY);

                this.LoadPhoto(mBlocks[num_slab], 0, 0);
            }
        }

        public new int AddSlab(string name, Path slabPath, double x, double y)
        {
            try
            {
                if (mSlab == null)
                    mSlab = new Block();

                //CLayer layer = GetLayer(LayersType.SLAB);
                //layer.Fill = true;
                //SetLayer(layer);

                if (slabPath == null || slabPath.Count == 0) // va creato
                {
                    slabPath = new Path(new Rect(_currentSlab.DimX, _currentSlab.DimY));
                    //slabPath.SetRotoTransl(_currentSlab.OffsetX, _currentSlab.OffsetY, 0);
                    slabPath.SetRotoTransl(_currentSlab.GraphicOffsetX, _currentSlab.GraphicOffsetY, 0);

                    CLayer layer = GetLayer(LayersType.SLAB);
                    layer.Fill = true;
                    SetLayer(layer);
                }
                else
                {
                    if (x != 0 || y != 0)
                        slabPath.SetRotoTransl(x, y, 0);
                }

                slabPath.AllowSelect = false;


                slabPath.Layer = LayersType.SLAB.ToString();

                //if (x != 0 && y != 0)
                //    slabPath.SetRotoTransl(-x, -y, 0);

                mSlab.AddPath(slabPath.GetPathRT());

                mSlab.Name = name;

                mSlab.AllowSelect = false;
                mSlab.AllowMove = false;
                mSlab.AllowRotate = false;

                //if (x != 0 || y != 0)
                //    mSlab.SetRotoTransl(x, y, 0);

                int block_no = AddBlock();
                mSlab.Id = block_no;
                mBlocks[block_no] = mSlab;

                return block_no;
            }
            catch (Exception e) { return -1; }
        }

        protected override void InitLayer(string layerXml)
        {
            base.InitLayer(layerXml);
            RearrangeLayers();
            return;

            // inizializzazione dei (19) layer
            CLayer layer = new CLayer(LayersType.PHOTO.ToString(), false, true, System.Drawing.Color.Olive, "", 30);
            layer.ZLevel = -2;
            AddLayer(layer);
            layer = new CLayer(LayersType.SLAB.ToString(), true, true, System.Drawing.Color.White, "", 30);
            layer.ZLevel = -1;
            layer.Fill = true;
            //layer = new CLayer(LayersType.SLAB.ToString(), true, true, true, System.Drawing.Color.SaddleBrown, "", 30); 
            AddLayer(layer);

            //layer = new CLayer(LayersType.SLAB_NOIMAGE.ToString(), true, true, System.Drawing.Color.SaddleBrown, "", 30);
            ////layer = new CLayer(LayersType.SLAB.ToString(), true, true, true, System.Drawing.Color.SaddleBrown, "", 30); 
            //AddLayer(layer);

            layer = new CLayer(LayersType.DEFECT.ToString(), true, true, System.Drawing.Color.Sienna, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.REFERENCE_POINT1.ToString(), true, true, System.Drawing.Color.SaddleBrown, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.REFERENCE_POINT2.ToString(), true, true, System.Drawing.Color.SaddleBrown, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.SHAPE.ToString(), true, true, true, false, System.Drawing.Color.Red, "", 30);
            //layer = new CLayer(LayersType.SHAPE.ToString(), true, true, System.Drawing.Color.Red, "", 30); 
            AddLayer(layer);
            layer = new CLayer(LayersType.SHAPE_HOLE.ToString(), true, true, false, true, System.Drawing.Color.Red, "", 30);
            //layer = new CLayer(LayersType.SHAPE_HOLE.ToString(), true, true, System.Drawing.Color.Red, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.RAW.ToString(), true, true, System.Drawing.Color.Green, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.RAW_HOLE.ToString(), true, true, System.Drawing.Color.Green, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.BLIND_HOLE.ToString(), true, true, System.Drawing.Color.White, "", 30);
            AddLayer(layer);

            #region Tipo pezzi FkScar

            #region Filagne

            layer = new CLayer(LayersType.FK_PIECE_FIL_QUAD0.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_QUAD1.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0xFF, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_QUAD2.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_QUAD3.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x7F, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_QUAD4.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0x80), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_LQUAD1.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0xFF, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_LQUAD2.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0xF0, 0xFF, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD1.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0, 255, 0), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD2.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(255, 232, 0), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD3.ToString(), true, true, true, false, System.Drawing.Color.DarkGreen, "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD4.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0x80), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD5.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0xFF, 0x80, 0x80), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD6.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x7F, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD7.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x3F, 0xBF, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD8.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_SQUAD9.ToString(), true, true, true, false, System.Drawing.Color.DarkRed, "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_FIL_MISC.ToString(), true, true, true, false, System.Drawing.Color.Black, "", 100);
            AddLayer(layer);

            #endregion Filagne

            #region Poligoni

            layer = new CLayer(LayersType.FK_PIECE_POL_QUAD0.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_QUAD1.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0xFF, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_QUAD2.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_QUAD3.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x7F, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_QUAD4.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0x80), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_LQUAD1.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0xFF, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_LQUAD2.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0xF0, 0xFF, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD1.ToString(), true, true, true, false, System.Drawing.Color.GreenYellow, "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD2.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(255, 185, 0), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD3.ToString(), true, true, true, false, System.Drawing.Color.DarkGreen, "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD4.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0x80), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD5.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0xFF, 0x80, 0x80), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD6.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(255, 162, 0), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD7.ToString(), true, true, true, false, System.Drawing.Color.OrangeRed, "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD8.ToString(), true, true, true, false, System.Drawing.Color.FromArgb(0x00, 0x00, 0xFF), "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_SQUAD9.ToString(), true, true, true, false, System.Drawing.Color.DarkRed, "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.FK_PIECE_POL_MISC.ToString(), true, true, true, false, System.Drawing.Color.Black, "", 100);
            AddLayer(layer);

            #endregion Poligoni

            #endregion



            //layer = new CLayer(LayersType.DISC.ToString(), true, true, System.Drawing.Color.Gold, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.DISC_REM.ToString(), true, true, System.Drawing.Color.Orange, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.COMBINED.ToString(), true, true, System.Drawing.Color.DodgerBlue, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.COMBINED_REM.ToString(), true, true, System.Drawing.Color.DarkCyan, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.MILL.ToString(), true, true, System.Drawing.Color.DeepPink, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.MILL_REM.ToString(), true, true, System.Drawing.Color.DarkOrchid, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.FINAL_TOOL.ToString(), true, true, System.Drawing.Color.CornflowerBlue, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.FINAL_TOOL_REM.ToString(), true, true, true, System.Drawing.Color.Blue, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.EDGE_FINISHING_TOOL.ToString(), true, true, System.Drawing.Color.DarkSeaGreen, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.EDGE_FINISHING_DIM.ToString(), true, true, System.Drawing.Color.Lime, "", 30);
            //AddLayer(layer);
            //layer = new CLayer(LayersType.COLLISION.ToString(), true, true, System.Drawing.Color.Red, "", 30);
            //AddLayer(layer);
            layer = new CLayer(LayersType.SLAB_CUTS.ToString(), true, true, System.Drawing.Color.Red, "", 30);
            AddLayer(layer);
            layer = new CLayer(LayersType.TEXT.ToString(), true, true, System.Drawing.Color.Black, "", 30);
            layer.ZLevel = 1;
            AddLayer(layer);

            layer = new CLayer(LayersType.SYMBOL.ToString(), true, true, System.Drawing.Color.Yellow, "", 30);
            AddLayer(layer);

            layer = new CLayer(LayersType.PIECE_LABEL.ToString(), true, true, System.Drawing.Color.WhiteSmoke, "", 30);
            AddLayer(layer);



            //LoadConfiguredLayer(layerXml);

            RearrangeLayers();
        }



        //private bool InsertSlabDefectsRefPointPath(CBPExtension cbpe, string xmlFile, Slab slb, double originX, double originY)
        //{
        //    Block bs;

        //    try
        //    {
        //        bs = new Block();
        //        Breton.Polygons.Zone zn;

        //        if (slb.gNormalized)
        //            zn = slb.gZone;
        //        else
        //            zn = slb.gPixelZone;

        //        // Inserisce il percorso della lastra
        //        int num_slab = cbpe.AddSlab(slb.gCode, zn.GetPath(), originX, originY);

        //        // Verifica ora se nel nodo sono presenti difetti e in caso li inserisce
        //        for (int i = 0; i < zn.ZoneCount; i++)
        //        {
        //            bs = new Block();
        //            cbpe.AddDefect(zn.GetZone(i).GetPath(), originX, originY);
        //        }

        //        // Legge i punti in cui sono stati piazzati i bollini
        //        Breton.Polygons.Point pt1 = new Breton.Polygons.Point();
        //        Breton.Polygons.Point pt2 = new Breton.Polygons.Point();

        //        if (!slb.gNormalized)
        //        {
        //            pt1 = slb.gTarget0Px;
        //            pt2 = slb.gTarget1Px;
        //        }


        //        // Se la sorgente dell'immagine non è ImagePLUS cerca i punti di riferimento nel file .ini
        //        //if ((pt1 != null || (pt1.X == 0 && pt1.Y == 0)) && (pt2 != null || (pt2.X == 0 && pt2.Y == 0)))
        //        //if (slb.gSource != "ImagePLUS")
        //        //{
        //        bool enable = false;

        //        GlobDef mGlobDef = new GlobDef();
        //        mGlobDef.CFG_PARAMS_FILENAME = tmpImageDir + mGlobDef.EDIT_PARAMS_FILENAME;
        //        Slab _slb = new Slab();
        //        _slb.ReadFileXml(xmlFile, "", "", mGlobDef.CFG_PARAMS_FILENAME);

        //        Params gPar = new Params(mGlobDef);
        //        gPar.read_pt_refers(ref pt1, ref pt2, ref enable);
        //        //}

        //        // Aggiunge i punti di riferimento per i bollini
        //        //if ((pt1 != null && (pt1.X != 0 || pt1.Y != 0)) && (pt2 != null && (pt2.X != 0 || pt2.Y != 0)))
        //        {
        //            cbpe.AddReferencePoint(pt1, true); //, originX, originY);
        //            cbpe.AddReferencePoint(pt2, false); //, originX, originY);
        //        }

        //        // Disegna i vari path
        //        cbpe.Repaint();
        //        cbpe.ZoomAll();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("Slabs.InsertSlabDefectsPath ", ex);
        //        return false;
        //    }
        //}


        private void ClearViewer()
        {
            ClearAll();
        }


        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<





        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

}
