﻿using System;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace SlabOperate.Business.Entities
{
    public enum SlabStatus
    {
        F_SLAB_STATE_INVALID = -1,
        F_SLAB_STATE_READY,                              //Lastra pronta all'ottimizzazione
        F_SLAB_STATE_PROGRAMMING,                        //Lastra in programmazione
        F_SLAB_STATE_CONFIRMED,                          //Lastra confermata
        F_SLAB_STATE_IN_PROCESS,                         //Lastra in lavorazione
        F_SLAB_STATE_COMPLETED,                          //Lastra prodotta
        F_SLAB_STATE_SUSPENDED,                          //Lastra sospesa
        F_SLAB_STATE_LOCKED,                             //Lastra prenotata
        F_SLAB_STATE_PROGRAMMED,                         //Lastra programmata
        F_SLAB_STATE_DISCARDED,                          //Lastra scartata
        F_SLAB_STATE_DESTROYED,                          //Lastra distrutta

        F_SLAB_STATE_STOCK = 100,                        //Lastra in magazzino

        F_SLAB_STATE_VIRTUAL_READY = 1000,               //Lastra virtuale pronta all'ottimizzazione
        F_SLAB_STATE_VIRTUAL_PROGRAMMING,                //Lastra virtuale in programmazione
        F_SLAB_STATE_VIRTUAL_CONFIRMED,                  //Lastra virtuale confermata
        F_SLAB_STATE_VIRTUAL_IN_PROCESS,                 //Lastra virtuale in lavorazione
        F_SLAB_STATE_VIRTUAL_COMPLETED,                  //Lastra virtuale prodotta
        F_SLAB_STATE_VIRTUAL_SUSPENDED,                  //Lastra virtuale sospesa
        F_SLAB_STATE_VIRTUAL_LOCKED,                     //Lastra virtuale prenotata
        F_SLAB_STATE_VIRTUAL_PROGRAMMED,                 //Lastra virtuale programmata
        F_SLAB_STATE_VIRTUAL_DISCARDED,                  //Lastra virtuale scartata
        F_SLAB_STATE_VIRTUAL_DESTROYED                  //Lastra virtuale distrutta
    }

    public enum SlabRepresentationMode
    {
        /// <summary>
        /// Standard
        /// </summary>
        Standard = 0,
        /// <summary>
        /// Vettoriale
        /// </summary>
        Vector = 1                              
    }

    public enum SlabCutType
    {
        F_CUT_TYPE_NONE = 0,
        F_CUT_TYPE_QUADRANTS,
        F_CUT_TYPE_STRIPS,
        F_CUT_TYPE_POLYGONS,
        F_CUT_TYPE_PROFILES,
        F_CUT_TYPE_ROMBI,
        F_CUT_TYPE_MAX
    }

    public enum SlabReverseSides
    {
        F_REVERSE_SIDE_NONE = 0,
        F_REVERSE_SIDE_UP,
        F_REVERSE_SIDE_DOWN
    }

    public enum SlabUnloadConfirm
    {
        F_UNLOAD_CONFIRM_NONE = 0,
        F_UNLOAD_CONFIRM_FKSCAR = 1
    }


    [MainDbTable(PersistenceProviderType.SqlServer, "T_SLABS")]
    public class SlabEntity : BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private string _code = string.Empty;

        private string _description = string.Empty;

        private SlabStatus _slabStatus;

        private int _thicknessId = -1;

        private DateTime _insertDate;

        private DateTime _startDate;

        private DateTime _stopDate;

        private SlabRepresentationMode _internalMode = SlabRepresentationMode.Standard;

        // F_ROW_DATA

        private double _offsetX;

        private double _offsetY;

        private double _surface;

        private double _offsetYAlign;

        // F_COMMANDS

        private double _length;

        private double _width;

        private double _thickness;

        private SlabCutType _cutType = SlabCutType.F_CUT_TYPE_NONE;

        // F_BOUNDARY_IMAGE
        // F_BOUNDARY_IMAGE
        // F_PHOTO_PARAM

        private int _slabImageId = -1;

        private string _stationCode = string.Empty;

        private double _photoOffsetX;

        private double _photoOffsetY;

        private double _photoAdjustX;

        private double _photoAdjustY;

        private double _photoMachineRefX1;

        private double _photoMachineRefY1;

        private double _photoMachineRefX2;

        private double _photoMachineRefY2;

        private SlabReverseSides _reverseSide = SlabReverseSides.F_REVERSE_SIDE_NONE;

        private double _photoMachineRefRevX1;

        private double _photoMachineRefRevY1;

        private double _photoMachineRefRevX2;

        private double _photoMachineRefRevY2;

        private string _boundaryImageFilepath = string.Empty;

        private SlabUnloadConfirm _unloadConfirm = SlabUnloadConfirm.F_UNLOAD_CONFIRM_NONE;

        private ThicknessEntity _thicknessObject;

        private SlabImageEntity _slabImage = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return Id; }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_SLAB", true)]
        public int Id
        {
            get {return GetProperty("Id", ref _id);}
            set { SetProperty("Id", value, ref _id);}
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE", true)]
        public string Code
        {
            get { return GetProperty("Code", ref _code); }
            set { SetProperty("Code", value, ref _code); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION", true)]
        public string Description
        {
            get { return GetProperty("Description", ref _description); }
            set { SetProperty("Description", value, ref _description); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_STATE", true)]
        public SlabStatus SlabStatus
        {
            get { return GetProperty("SlabStatus", ref _slabStatus); }
            set { SetProperty("SlabStatus", value, ref _slabStatus); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_THICKNESS", true)]
        public int ThicknessId
        {
            get { return GetProperty("ThicknessId", ref _thicknessId); }
            set { SetProperty("ThicknessId", value, ref _thicknessId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_UNLOAD_CONFIRM", true)]
        public SlabUnloadConfirm UnloadConfirm
        {
            get { return GetProperty("UnloadConfirm", ref _unloadConfirm); }
            set { SetProperty("UnloadConfirm", value, ref _unloadConfirm); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_INSERT_DATE", true)]
        public DateTime InsertDate
        {
            get { return GetProperty("InsertDate", ref _insertDate); }
            set { SetProperty("InsertDate", value, ref _insertDate); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_START_DATE", true)]
        public DateTime StartDate
        {
            get { return GetProperty("StartDate", ref _startDate); }
            set { SetProperty("StartDate", value, ref _startDate); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_STOP_DATE", true)]
        public DateTime StopDate
        {
            get { return GetProperty("StopDate", ref _stopDate); }
            set { SetProperty("StopDate", value, ref _stopDate); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_INTERNAL_MODE", true)]
        public SlabRepresentationMode InternalMode
        {
            get { return GetProperty("InternalMode", ref _internalMode); }
            set { SetProperty("InternalMode", value, ref _internalMode); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_OFFSET_X", true)]
        public double OffsetX
        {
            get { return GetProperty("OffsetX", ref _offsetX); }
            set { SetProperty("OffsetX", value, ref _offsetX); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_OFFSET_Y", true)]
        public double OffsetY
        {
            get { return GetProperty("OffsetY", ref _offsetY); }
            set { SetProperty("OffsetY", value, ref _offsetY); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_SURFACE", true)]
        public double Surface
        {
            get { return GetProperty("Surface", ref _surface); }
            set { SetProperty("Surface", value, ref _surface); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_OFFSET_Y_ALIGN", true)]
        public double OffsetYAlign
        {
            get { return GetProperty("OffsetYAlign", ref _offsetYAlign); }
            set { SetProperty("OffsetYAlign", value, ref _offsetYAlign); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_LENGTH", true)]
        public double Length
        {
            get { return GetProperty("Length", ref _length); }
            set { SetProperty("Length", value, ref _length); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_WIDTH", true)]
        public double Width
        {
            get { return GetProperty("Width", ref _width); }
            set { SetProperty("Width", value, ref _width); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_THICKNESS", true)]
        public double Thickness
        {
            get { return GetProperty("Thickness", ref _thickness); }
            set { SetProperty("Thickness", value, ref _thickness); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CUT_TYPE", true)]
        public SlabCutType CutType
        {
            get { return GetProperty("CutType", ref _cutType); }
            set { SetProperty("CutType", value, ref _cutType); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_AN_SLAB_IMAGES", true)]
        public int SlabImageId
        {
            get { return GetProperty("SlabImageId", ref _slabImageId); }
            set { SetProperty("SlabImageId", value, ref _slabImageId); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_STATION_CODE", true)]
        public string StationCode
        {
            get { return GetProperty("StationCode", ref _stationCode); }
            set { SetProperty("StationCode", value, ref _stationCode); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_OFFSET_X", true)]
        public double PhotoOffsetX
        {
            get { return GetProperty("PhotoOffsetX", ref _photoOffsetX); }
            set { SetProperty("PhotoOffsetX", value, ref _photoOffsetX); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_OFFSET_Y", true)]
        public double PhotoOffsetY
        {
            get { return GetProperty("PhotoOffsetY", ref _photoOffsetY); }
            set { SetProperty("PhotoOffsetY", value, ref _photoOffsetY); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_ADJUST_X", true)]
        public double PhotoAdjustX
        {
            get { return GetProperty("PhotoAdjustX", ref _photoAdjustX); }
            set { SetProperty("PhotoAdjustX", value, ref _photoAdjustX); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_ADJUST_Y", true)]
        public double PhotoAdjustY
        {
            get { return GetProperty("PhotoAdjustY", ref _photoAdjustY); }
            set { SetProperty("PhotoAdjustY", value, ref _photoAdjustY); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_X1", true)]
        public double PhotoMachineRefX1
        {
            get { return GetProperty("PhotoMachineRefX1", ref _photoMachineRefX1); }
            set { SetProperty("PhotoMachineRefX1", value, ref _photoMachineRefX1); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_Y1", true)]
        public double PhotoMachineRefY1
        {
            get { return GetProperty("PhotoMachineRefY1", ref _photoMachineRefY1); }
            set { SetProperty("PhotoMachineRefY1", value, ref _photoMachineRefY1); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_X2", true)]
        public double PhotoMachineRefX2
        {
            get { return GetProperty("PhotoMachineRefX2", ref _photoMachineRefX2); }
            set { SetProperty("PhotoMachineRefX2", value, ref _photoMachineRefX2); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_Y2", true)]
        public double PhotoMachineRefY2
        {
            get { return GetProperty("PhotoMachineRefY2", ref _photoMachineRefY2); }
            set { SetProperty("PhotoMachineRefY2", value, ref _photoMachineRefY2); }
        }

        // STOCK




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_REVERSE_SIDE", true)]
        public SlabReverseSides ReverseSide
        {
            get { return GetProperty("ReverseSide", ref _reverseSide); }
            set { SetProperty("ReverseSide", value, ref _reverseSide); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_REV_X1", true)]
        public double PhotoMachineRefRevX1
        {
            get { return GetProperty("PhotoMachineRefRevX1", ref _photoMachineRefRevX1); }
            set { SetProperty("PhotoMachineRefRevX1", value, ref _photoMachineRefRevX1); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_REV_Y1", true)]
        public double PhotoMachineRefRevY1
        {
            get { return GetProperty("PhotoMachineRefRevY1", ref _photoMachineRefRevY1); }
            set { SetProperty("PhotoMachineRefRevY1", value, ref _photoMachineRefRevY1); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_REV_X2", true)]
        public double PhotoMachineRefRevX2
        {
            get { return GetProperty("PhotoMachineRefRevX2", ref _photoMachineRefRevX2); }
            set { SetProperty("PhotoMachineRefRevX2", value, ref _photoMachineRefRevX2); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PHOTO_MACHINEREF_REV_Y2", true)]
        public double PhotoMachineRefRevY2
        {
            get { return GetProperty("PhotoMachineRefRevY2", ref _photoMachineRefRevY2); }
            set { SetProperty("PhotoMachineRefRevY2", value, ref _photoMachineRefRevY2); }
        }


        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("ThicknessId", "Id", RelationType.OneToOne, true)]
        public ThicknessEntity ThicknessObject
        {
            get
            {
                return GetProperty("ThicknessObject", ref _thicknessObject);
            }
            set { SetProperty("ThicknessObject", value, ref _thicknessObject); }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("SlabImageId", "Id", RelationType.OneToOne, true)]
        public SlabImageEntity SlabImage
        {
            get { return GetProperty("SlabImage", ref _slabImage); }
            set { SetProperty("SlabImage", value, ref _slabImage); }
        }

        public string Dimensions
        {
            get { return string.Format("{0:#0}mm x {1:#0}mm", Length, Width); }
        }

        [PersistableField(FieldRetrieveMode.Deferred, FieldManagementType.ToFilepath, "XML_", "XML")]
        [DbField(PersistenceProviderType.SqlServer, "F_BOUNDARY_IMAGE", false, "", "", true)]
        public string BoundaryImageFilepath
        {
            get
            {
                return GetProperty("BoundaryImageFilepath", ref _boundaryImageFilepath);
            }
            set
            {
                SetProperty("BoundaryImageFilepath", value, ref _boundaryImageFilepath);
            }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        //public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
        //    FieldStatus newStatus = FieldStatus.Set,
        //    bool forceOnChanged = false,
        //    bool avoidOnChanged = false,
        //    bool avoidOnStatusChanged = false)
        //{
        //    base.SetProperty(propertyName, value, forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //    return;

        //    try
        //    {
        //        switch (propertyName)
        //        {
        //            case "Id":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _id,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "Code":
        //                base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
        //                    ref _code,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "Description":
        //                base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
        //                    ref _description,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "BatchCode":
        //                base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
        //                    ref _batchCode,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;


        //            case "DimX":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _dimX,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "DimY":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _dimY,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "DimZ":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _dimZ,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "OffsetX":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _offsetX,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "OffsetY":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _offsetY,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "Surface":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _surface,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "CommercialSurface":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _commercialSurface,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "InternalRectLeft":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _internalRectLeft,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "InternalRectTop":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _internalRectTop,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "InternalRectWidth":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _internalRectWidth,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "InternalRectHeight":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _internalRectHeight,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "Tone":
        //                base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
        //                    ref _tone,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "XmlParameter":
        //                base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
        //                    ref _xmlParameterFilepath,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "PositionStopX":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _positionStopX,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "PositionStopY":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _positionStopY,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "UnitCost":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _unitCost,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "Defects":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _defects,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "QualityId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _qualityId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "ImageId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _imageId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "ImagePath":
        //                base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
        //                    ref _imagePath,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "ImageThumbnailPath":
        //                base.SetProperty(propertyName, (string) Convert.ChangeType(value, typeof (string)),
        //                    ref _imageThumbnailPath,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "ImageSource":
        //                base.SetProperty(propertyName, (BitmapImage) Convert.ChangeType(value, typeof (BitmapImage)),
        //                    ref _imageSource,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "ArticleType":
        //                base.SetProperty(propertyName, (ArticleType) Convert.ChangeType(value, typeof (int)),
        //                    ref _articleType,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "ArticleStatus":
        //                base.SetProperty(propertyName, (ArticleStatus) Convert.ChangeType(value, typeof (int)),
        //                    ref _articleStatus,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "MaterialId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _materialId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "StockHusId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _stockHusId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "DdtaDetailsId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _ddtaDetailsId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "SupplierId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _supplierId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "CustomerProjectId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _customerProjectId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "Qty":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _qty,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "InsertDate":
        //                base.SetProperty(propertyName, (DateTime) Convert.ChangeType(value, typeof (DateTime)),
        //                    ref _insertDate,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "HusPosition":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _husPosition,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "BlobsPartProgramId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _blobsPartProgramId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "BlobsPolygonId":
        //                base.SetProperty(propertyName, (int) Convert.ChangeType(value, typeof (int)),
        //                    ref _blobsPolygonId,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "Weight":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _weight,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("SetProperty error ", ex);
        //    }
        //}

        //public override T GetProperty<T>(string propertyName)
        //{
        //    return base.GetProperty<T>(propertyName);

        //    switch (propertyName)
        //    {
        //        case "Id":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _id), typeof(T));
        //            break;

        //        case "Code":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _code), typeof(T));
        //            break;

        //        case "Description":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _description), typeof(T));
        //            break;

        //        case "BatchCode":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _batchCode), typeof(T));
        //            break;

        //        case "DimX":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimX), typeof(T));
        //            break;

        //        case "DimY":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimY), typeof(T));
        //            break;

        //        case "DimZ":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _dimZ), typeof(T));
        //            break;

        //        case "OffsetX":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _offsetX), typeof(T));
        //            break;

        //        case "OffsetY":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _offsetY), typeof(T));
        //            break;

        //        case "Surface":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _surface), typeof(T));
        //            break;

        //        case "CommercialSurface":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _commercialSurface), typeof(T));
        //            break;

        //        case "InternalRectLeft":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectLeft), typeof(T));
        //            break;

        //        case "InternalRectTop":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectTop), typeof(T));
        //            break;

        //        case "InternalRectWidth":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectWidth), typeof(T));
        //            break;

        //        case "InternalRectHeight":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _internalRectHeight), typeof(T));
        //            break;

        //        case "Tone":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _tone), typeof(T));
        //            break;

        //        case "XmlParameter":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _xmlParameter), typeof(T));
        //            break;

        //        case "PositionStopX":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _positionStopX), typeof(T));
        //            break;

        //        case "PositionStopY":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _positionStopY), typeof(T));
        //            break;

        //        case "UnitCost":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _unitCost), typeof(T));
        //            break;

        //        case "Defects":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _defects), typeof(T));
        //            break;

        //        case "QualityId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _qualityId), typeof(T));
        //            break;

        //        case "ImageId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageId), typeof(T));
        //            break;

        //        case "ImagePath":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _imagePath), typeof(T));
        //            break;

        //        case "ImageThumbnailPath":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageThumbnailPath), typeof(T));
        //            break;

        //        case "ImageSource":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _imageSource), typeof(T));
        //            break;

        //        case "ArticleType":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _articleType), typeof(T));
        //            break;

        //        case "ArticleStatus":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _articleStatus), typeof(T));
        //            break;

        //        case "MaterialId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _materialId), typeof(T));
        //            break;

        //        case "StockHusId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _stockHusId), typeof(T));
        //            break;

        //        case "DdtaDetailsId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _ddtaDetailsId), typeof(T));
        //            break;

        //        case "SupplierId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _supplierId), typeof(T));
        //            break;

        //        case "CustomerProjectId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _customerProjectId), typeof(T));
        //            break;

        //        case "Qty":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _qty), typeof(T));
        //            break;

        //        case "InsertDate":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _insertDate), typeof(T));
        //            break;

        //        case "HusPosition":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _husPosition), typeof(T));
        //            break;

        //        case "BlobsPartProgramId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _blobsPartProgramId), typeof(T));
        //            break;

        //        case "BlobsPolygonId":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _blobsPolygonId), typeof(T));
        //            break;

        //        case "Weight":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _weight), typeof(T));
        //            break;

        //    }

        //    return default(T);
        //}

        //protected override PersistableEntityProxy GetProxy(PersistenceProviderType providerType, bool saveMode = false)
        //{
        //    switch (providerType)
        //    {
        //        case PersistenceProviderType.SqlServer:
        //            return new SqlArticleProxy(this, (SqlServerProvider)PersistenceManager.Instance.GetProvider(providerType), saveMode:saveMode);
        //            break;

        //        case PersistenceProviderType.Xml:
        //            break;
        //    }

        //    return base.GetProxy(providerType);
        //}


    }
}
