﻿using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace SlabOperate.Business.Entities
{
    [MainDbTable(PersistenceProviderType.SqlServer, "T_MATERIALS")]
    public class MaterialEntity : BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private int _materialClassId = -1;

        private string _code = string.Empty;

        private string _description = string.Empty;

        private string _finishingCode;

        private string _layerCode;



        private MaterialClassEntity _materialClass = null;

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_MATERIAL", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE")]
        public string Code
        {
            get
            {
                return GetProperty("Code", ref _code);
            }
            set
            {
                SetProperty("Code", value, ref _code);
            }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION")]
        public string Description
        {
            get
            {
                return GetProperty("Description", ref _description);
            }
            set
            {
                SetProperty("Description", value, ref _description);
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_CLASS", true)]
        public int MaterialClassId
        {
            get
            {
                return GetProperty("MaterialClassId", ref _materialClassId);
            }
            set
            {
                SetProperty("MaterialClassId", value, ref _materialClassId);
            }
        }


        [PersistableField]
        [DbField(PersistenceProviderType.SqlServer, "F_FINISHING_CODE")]
        public string FinishingCode
        {
            get
            {
                return GetProperty("FinishingCode", ref _finishingCode);
            }
            set
            {
                SetProperty("FinishingCode", value, ref _finishingCode);
            }
        }


        [PersistableField]
        [DbField(PersistenceProviderType.SqlServer, "F_LAYER_CODE")]
        public string LayerCode
        {
            get
            {
                return GetProperty("LayerCode", ref _layerCode);
            }
            set
            {
                SetProperty("LayerCode", value, ref _layerCode);
            }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("MaterialClassId", "Id", RelationType.OneToOne, true)]
        public MaterialClassEntity MaterialClass
        {
            get
            {
                return GetProperty("MaterialClass", ref _materialClass);
            }
            set
            {
                SetProperty("MaterialClass", value, ref _materialClass);
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        #region >-------------- Overrides


        public override string this[string columnName]
        {
            get
            {
                string retError = string.Empty;
                switch (columnName)
                {
                    case "MaterialClassId":
                        if (_materialClassId == -1)
                            retError = "MaterialClassId value missing.";
                        break;

                    default:
                        break;

                }
                return retError;
            }
        }

        public override bool CanSave()
        {
            return _materialClassId != -1;
        }



        #endregion Overrides  -----------<
    }
}
