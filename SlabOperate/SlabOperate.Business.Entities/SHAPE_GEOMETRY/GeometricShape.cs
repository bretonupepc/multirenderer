﻿using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Breton.MathUtils;
using Breton.Polygons;
using SlabOperate.Business.Entities.SHAPE_GEOMETRY.WORKINGS;
using TraceLoggers;
using Path = Breton.Polygons.Path;

namespace SlabOperate.Business.Entities.SHAPE_GEOMETRY
{
    public class GeometricShape
    {

        private int _id = -1;
        private string _code = string.Empty;

        private string _description = string.Empty;
        private string _notes = string.Empty;


        private double _dimX = 0;
        private double _dimY = 0;
        private double _dimZ = 0;


        private int _orderId = -1;
        private int _materialId = -1;

        private int _initialQuantity = 1;

        


        private List<BaseWorkingElement> _workings;
        private List<BaseWorkingElement> _roddings;


        private Zone _geometryInfo = null;
        private Zone _boundingBox = null;

        private Path _boundingBoxExtended;



        public GeometricShape()
        {
            _geometryInfo = new Zone();
            _workings = new List<BaseWorkingElement>();
            _roddings = new List<BaseWorkingElement>();
        }

        public GeometricShape(string code) : this()
        {
            _code = code;
        }

        public GeometricShape(Zone zone) : this()
        {
            _geometryInfo = zone;

            Rect rect = _geometryInfo.GetRectangle();

            _boundingBox = new Zone(rect);
            DimX = rect.Width;
            DimY = rect.Height;

            _boundingBoxExtended = new Path(rect.ExtendRect(5, 5));
        }

        public GeometricShape(Path path): this()
        {
            _geometryInfo.SetPath(path);

            Rect rect = _geometryInfo.GetRectangle();

            _boundingBox = new Zone(rect);
            DimX = rect.Width;
            DimY = rect.Height;

            _boundingBoxExtended = new Path(rect.ExtendRect(5,5));
        }


        public GeometricShape(Rect rectangle)
            : this(new Path(rectangle))
        {
        }


        public string XmlFilepath
        {
            get 
            { 
                return string.Concat(_code, ".xml");
            }
        }


        #region public variables

        public string OrderCode = "";
        public string Material = "";
        public int AssignedQuantity = 0;
        public int ProducedQuantity = 0;
        //public string XmlFileName = "";

        #endregion

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }

        public int MaterialId
        {
            get { return _materialId; }
            set { _materialId = value; }
        }

        public Zone GeometryInfo
        {
            get { return _geometryInfo; }
            set { _geometryInfo = value; }
        }

        public double DimX
        {
            get { return _dimX; }
            set { _dimX = value; }
        }

        public double DimY
        {
            get { return _dimY; }
            set { _dimY = value; }
        }

        public double DimZ
        {
            get { return _dimZ; }
            set { _dimZ = value; }
        }

        public Zone BoundingBox
        {
            get { return _boundingBox; }
            set { _boundingBox = value; }
        }

        public Path BoundingBoxExtended
        {
            get { return _boundingBoxExtended; }
        }

        public int InitialQuantity
        {
            get { return _initialQuantity; }
            set { _initialQuantity = value; }
        }



        public List<BaseWorkingElement> Workings
        {
            get { return _workings; }
        }

        public List<BaseWorkingElement> Roddings
        {
            get { return _roddings; }
        }


        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }


        //public XmlDocument SaveToXml()
        //{
        //    return SaveToXml(XmlFilepath);
        //}

        //public XmlDocument SaveToXml(string xmlFilePath)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.PreserveWhitespace = true;
        //    XmlDeclaration xmldecl;
        //    xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
        //    doc.AppendChild(xmldecl);

        //    XmlNode root = doc.CreateNode(XmlNodeType.Element, "EXPORT", "");
        //    XmlAttribute attr = doc.CreateAttribute("version");
        //    attr.Value = "1.0";
        //    root.Attributes.Append(attr);

        //    XmlNode construction = doc.CreateNode(XmlNodeType.Element, "Construction", "");
        //    attr = doc.CreateAttribute("ConstructionName");
        //    attr.Value = Code;
        //    construction.Attributes.Append(attr);
        //    root.AppendChild(construction);

        //    //XmlNode toolpathindex = doc.CreateNode(XmlNodeType.Element, "ToolPathsIndex", "");
        //    //construction.AppendChild(toolpathindex);

        //    //XmlNode polygon = doc.CreateNode(XmlNodeType.Element, "Polygon", "");
        //    //construction.AppendChild(polygon);

        //    PathCollection pcoll = new PathCollection();
        //    pcoll.AddPath(_geometryInfo.GetPath());
        //    for (int i = 0; i < _geometryInfo.ZoneCount; i++)
        //    {
        //        Path internalPath = _geometryInfo.GetZone(i).GetPath();
        //        internalPath.SetRotationSense(MathUtil.RotationSense.CW);
        //        pcoll.AddPath(internalPath);
        //    }


        //    #region Add Polygon data

        //    for (int i = 0; i < pcoll.Count; i++)
        //    {
        //        Path p = pcoll[i];

        //        XmlNode polygon = doc.CreateNode(XmlNodeType.Element, "Polygon", "");
        //        construction.AppendChild(polygon);


        //        XmlNode toolpath = doc.CreateNode(XmlNodeType.Element, "ToolPath", "");
        //        toolpath.InnerText = p.IsCCW ? "20" : "12";
        //        polygon.AppendChild(toolpath);

        //        for (int j = 0; j < p.Count; j++)
        //        {
        //            XmlNode side = doc.CreateNode(XmlNodeType.Element, "Side", "");
        //            polygon.AppendChild(side);

        //            XmlNode node;
        //            if (p[j] is Segment)
        //            {
        //                AddSegmentNode(doc, p[j], side);
        //            }
        //            else
        //            {
        //                TraceLog.WriteLine("GeometricShape.SaveToXml error: Shape with arcs");
        //            }


        //        }
        //    }

        //    #endregion Add polygon data


        //    # region Add Workings data

        //    foreach (BaseWorkingElement workingElement in Workings)
        //    {
        //        XmlNode working = doc.CreateNode(XmlNodeType.Element, "Working", "");
        //        construction.AppendChild(working);

        //        XmlNode node = doc.CreateNode(XmlNodeType.Element, "WorkingType", "");
        //        node.InnerText = workingElement.Type.ToString();
        //        working.AppendChild(node);

        //        switch (workingElement.Type)
        //        {
        //            case WorkingElementType.Groove:

        //                node = doc.CreateNode(XmlNodeType.Element, "Depth", "");
        //                node.InnerText = ((WorkingGroove) workingElement).Depth.ToString("0.00",
        //                                    CultureInfo.InvariantCulture);
        //                working.AppendChild(node);
        //                break;
        //        }

        //        // Add Working path
        //        Path workingPath = workingElement.GeometryInfo.GetPath();
        //        workingPath.SetRotationSense(MathUtil.RotationSense.CW);
        //        for (int i = 0; i < workingPath.Count; i++)
        //        {
        //            XmlNode side = doc.CreateNode(XmlNodeType.Element, "Side", "");
        //            working.AppendChild(side);

        //            if (workingPath[i] is Segment)
        //            {
        //                AddSegmentNode(doc, workingPath[i], side);
        //            }
        //            else
        //            {
        //                AddArcNode(doc, workingPath[i], side);
        //            }
        //        }
        //    }

        //    # endregion Workings data

        //    //#region Add Roddings data

        //    //foreach (BaseWorkingElement roddingElement in Roddings)
        //    //{
        //    //    XmlNode rodding = doc.CreateNode(XmlNodeType.Element, "Reinforcement", "");

        //    //    XmlAttribute depthAttribute = doc.CreateAttribute("Depth");
        //    //    depthAttribute.Value = ((WorkingRodding)roddingElement).Depth.ToString("0", CultureInfo.InvariantCulture);
        //    //    rodding.Attributes.Append(depthAttribute);

        //    //    construction.AppendChild(rodding);


        //    //    // Add Rodding path
        //    //    Path workingPath = roddingElement.GeometryInfo.GetPath();
        //    //    workingPath.SetRotationSense(MathUtil.RotationSense.CW);
        //    //    for (int i = 0; i < workingPath.Count; i++)
        //    //    {
        //    //        XmlNode side = doc.CreateNode(XmlNodeType.Element, "Side", "");
        //    //        rodding.AppendChild(side);

        //    //        if (workingPath[i] is Segment)
        //    //        {
        //    //            AddSegmentNode(doc, workingPath[i], side);
        //    //        }
        //    //        else
        //    //        {
        //    //            AddArcNode(doc, workingPath[i], side);
        //    //        }
        //    //    }
        //    //}

        //    //#endregion Roddings data


        //    doc.AppendChild(root);

        //    if (!string.IsNullOrEmpty(xmlFilePath))
        //        doc.Save(xmlFilePath);

        //    return doc;
        //}



        private static void AddSegmentNode(XmlDocument doc, Side side, XmlNode inputNode)
        {
            XmlNode node;
            node = doc.CreateNode(XmlNodeType.Element, "X1", "");
            node.InnerText = side.P1.X.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Y1", "");
            node.InnerText = side.P1.Y.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extreme1", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extension1", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "X2", "");
            node.InnerText = side.P2.X.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Y2", "");
            node.InnerText = side.P2.Y.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extreme2", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extension2", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);
        }




    }
}
