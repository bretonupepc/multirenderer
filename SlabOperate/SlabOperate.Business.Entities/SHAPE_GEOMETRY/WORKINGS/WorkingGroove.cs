﻿using Breton.Polygons;

namespace SlabOperate.Business.Entities.SHAPE_GEOMETRY.WORKINGS
{
    public class WorkingGroove : BaseWorkingElement
    {
        private double _depth = 0;

        private Path _innerPath = null;


        public WorkingGroove() : base()
        {
            _type = WorkingElementType.Groove;
        }

        public WorkingGroove(Path path) : base(path)
        {
            _type = WorkingElementType.Groove;
        }

        public WorkingGroove(Path path, double depth): this(path)

        {
            _depth = depth;
        }





        public double Depth
        {
            get { return _depth; }
            set { _depth = value; }
        }

        public Path InnerPath
        {
            get { return _innerPath; }
            set { _innerPath = value; }
        }
    }
}
