﻿using Breton.Polygons;

namespace SlabOperate.Business.Entities.SHAPE_GEOMETRY.WORKINGS
{
    public class WorkingRodding : BaseWorkingElement
    {
        private double _depth = 0;

        public WorkingRodding() : base()
        {
            _type = WorkingElementType.Rodding;
        }

        public WorkingRodding(Path path) : base(path)
        {
            _type = WorkingElementType.Rodding;
        }

        public WorkingRodding(Path path, double depth): this(path)

        {
            _depth = depth;
        }





        public double Depth
        {
            get { return _depth; }
            set { _depth = value; }
        }
    }
}
