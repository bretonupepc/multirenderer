﻿using Breton.Polygons;

namespace SlabOperate.Business.Entities.SHAPE_GEOMETRY.WORKINGS
{
    public enum WorkingElementType
    {
        Groove,
        Rodding,
        Undefined
    }

    public class BaseWorkingElement
    {

        protected string _name = string.Empty;

        protected WorkingElementType _type = WorkingElementType.Undefined;

        protected Zone _geometryInfo; 



        public BaseWorkingElement()
        {
            _geometryInfo = new Zone();
        }


        public BaseWorkingElement(Path path) : this()
        {
            _geometryInfo.SetPath(path);
        }



        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Zone GeometryInfo
        {
            get { return _geometryInfo; }
            set { _geometryInfo = value; }
        }

        public WorkingElementType Type
        {
            get { return _type; }
        }
    }
}
