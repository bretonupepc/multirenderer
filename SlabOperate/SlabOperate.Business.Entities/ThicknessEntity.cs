﻿using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace SlabOperate.Business.Entities
{
    [MainDbTable(PersistenceProviderType.SqlServer, "T_THICKNESSES")]
    public class ThicknessEntity : BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private int _materialId = -1;

        private double _thickness;

        private MaterialEntity _material = null;


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return Id; }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_THICKNESS", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_MATERIAL", true)]
        public int MaterialId
        {
            get { return GetProperty("MaterialId", ref _materialId); }
            set { SetProperty("MaterialId", value, ref _materialId); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_THICKNESS", true)]
        public double Thickness
        {
            get { return GetProperty("Thickness", ref _thickness); }
            set { SetProperty("Thickness", value, ref _thickness); }
        }


        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("MaterialId", "Id", RelationType.OneToOne, true)]
        public MaterialEntity Material
        {
            get
            {
                return GetProperty("Material", ref _material);
            }
            set
            {
                SetProperty("Material", value, ref _material);
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<





    }
}
