﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using TraceLoggers;

namespace SlabOperate.Business.Entities.ORDERS
{
    public enum OrderStatus
    {
        F_ORDER_STATE_INVALID = -1,
        F_ORDER_STATE_READY,             //Disponibile all//attivazione
        F_ORDER_STATE_IN_PROGRESS,       //In lavoro
        F_ORDER_STATE_COMPLETED,         //Tutti i pezzi sono stati assegnati
        F_ORDER_CLOSED,                  //Tutti i pezzi sono stati prodotti
        F_ORDER_STATE_SUSPENDED         //Sospeso
    }

    public enum OrderType
    {
        F_ORDER_TYPE_INVALID = -1,
        F_ORDER_TYPE_ODL,                //Ordine cliente o lavoro
        F_ORDER_TYPE_OSC,                //Ordine scarti
        F_ORDER_TYPE_ODR,                //Ordine riempimento
        F_ORDER_TYPE_OAC,                //Ordine a correre
        F_ORDER_TYPE_OFR                //Ordine di recupero ROCAMAT        

    }
    [MainDbTable(PersistenceProviderType.SqlServer, "T_ORDERS")]
    public class OrderEntity : BasePersistableEntity
    {
        #region >-------------- Constants and Enums

        private const string XML_FILE_NAME = "ORDER_ANALIZE.xml";


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private DateTime _date = DateTime.MinValue;

        private DateTime _startDate = DateTime.MinValue;

        private DateTime _stopDate = DateTime.MinValue;

        private OrderStatus _orderStatus = OrderStatus.F_ORDER_STATE_INVALID;

        private string _orderCode = string.Empty;

        private string _description = string.Empty;

        private int _sequence ;

        private int _customerOrder;

        private string _customerName = string.Empty;

        private int _customerSuborder ;

        private string _customerOrderName = string.Empty;

        private OrderType _orderType = OrderType.F_ORDER_TYPE_INVALID;

        private PersistableEntityCollection<ShapeEntity> _shapes = new PersistableEntityCollection<ShapeEntity>(); 

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_ORDER", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DATE", true)]
        public DateTime Date
        {
            get { return GetProperty("Date", ref _date); }
            set { SetProperty("Date", value, ref _date); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_START_DATE", false)]
        public DateTime StartDate
        {
            get { return GetProperty("StartDate", ref _startDate); }
            set { SetProperty("StartDate", value, ref _startDate); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_STOP_DATE", false)]
        public DateTime StopDate
        {
            get { return GetProperty("StopDate", ref _stopDate); }
            set { SetProperty("StopDate", value, ref _stopDate); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_STATE", true)]
        public OrderStatus OrderStatus
        {
            get { return GetProperty("OrderStatus", ref _orderStatus); }
            set { SetProperty("OrderStatus", value, ref _orderStatus); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ORDER_CODE", false)]
        public string OrderCode
        {
            get { return GetProperty("OrderCode", ref _orderCode); }
            set { SetProperty("OrderCode", value, ref _orderCode); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION")]
        public string Description
        {
            get { return GetProperty("Description", ref _description); }
            set { SetProperty("Description", value, ref _description); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_SEQUENCE", true)]
        public int Sequence
        {
            get { return GetProperty("Sequence", ref _sequence); }
            set { SetProperty("Sequence", value, ref _sequence); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CUSTOMER_ORDER", true)]
        public int CustomerOrder
        {
            get { return GetProperty("CustomerOrder", ref _customerOrder); }
            set { SetProperty("CustomerOrder", value, ref _customerOrder); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CUSTOMER_NAME", false)]
        public string CustomerName
        {
            get { return GetProperty("CustomerName", ref _customerName); }
            set { SetProperty("CustomerName", value, ref _customerName); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CUSTOMER_SUBORDER", true)]
        public int CustomerSuborder
        {
            get { return GetProperty("CustomerSuborder", ref _customerSuborder); }
            set { SetProperty("CustomerSuborder", value, ref _customerSuborder); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CUSTOMER_ORDER_NAME", false)]
        public string CustomerOrderName
        {
            get { return GetProperty("CustomerOrderName", ref _customerOrderName); }
            set { SetProperty("CustomerOrderName", value, ref _customerOrderName); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ORDER_TYPE", true)]
        public OrderType OrderType
        {
            get { return GetProperty("OrderType", ref _orderType); }
            set { SetProperty("OrderType", value, ref _orderType); }
        }

        public PersistableEntityCollection<ShapeEntity> Shapes
        {
            get { return _shapes; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public override bool SaveToRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            bool retVal = base.SaveToRepository(providerType);

            if (retVal)
            {
                foreach (var shape in Shapes)
                {
                    shape.OrderId = Id;
                    if (!shape.SaveToRepository())
                    {
                        retVal = false;
                        break;
                    }
                }
            }
            return retVal;

        }



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods





        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
