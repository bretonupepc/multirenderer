﻿using System;
using System.Xml;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using SlabOperate.Business.Entities.ORDERS;
using TraceLoggers;

namespace SlabOperate.Business.Entities
{
    public enum OfferStatus
    {
        ToAnalyze = 1,
        Analyzed,             //Disponibile all//attivazione
        Error,       //In lavoro
        Reloaded         //Tutti i pezzi sono stati assegnati
    }

    //public enum OrderType
    //{
    //    F_ORDER_TYPE_INVALID = -1,
    //    F_ORDER_TYPE_ODL,                //Ordine cliente o lavoro
    //    F_ORDER_TYPE_OSC,                //Ordine scarti
    //    F_ORDER_TYPE_ODR,                //Ordine riempimento
    //    F_ORDER_TYPE_OAC,                //Ordine a correre
    //    F_ORDER_TYPE_OFR                //Ordine di recupero ROCAMAT        

    //}
    [MainDbTable(PersistenceProviderType.SqlServer, "T_WK_XML_OFFERS")]
    public class OfferEntity : BasePersistableEntity
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private DateTime _date = DateTime.MinValue;


        private OfferStatus _offerStatus = OfferStatus.ToAnalyze;

        private string _code = string.Empty;

        private string _description = string.Empty;

        private string _notes = string.Empty;

        //private int _orderId = -1;

        private string _xmlFilepath = string.Empty;

        private int _revision = -1;


        private OrderEntity _order ; 

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DATE", true)]
        public DateTime Date
        {
            get { return GetProperty("Date", ref _date); }
            set { SetProperty("Date", value, ref _date); }
        }




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_CO_XML_OFFER_STATES", true)]
        public OfferStatus OfferStatus
        {
            get { return GetProperty("OfferStatus", ref _offerStatus); }
            set { SetProperty("OfferStatus", value, ref _offerStatus); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE", true)]
        public string Code
        {
            get { return GetProperty("Code", ref _code); }
            set { SetProperty("Code", value, ref _code); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION")]
        public string Description
        {
            get { return GetProperty("Description", ref _description); }
            set { SetProperty("Description", value, ref _description); }
        }




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_NOTE")]
        public string Notes
        {
            get { return GetProperty("Notes", ref _notes); }
            set { SetProperty("Notes", value, ref _notes); }
        }

        [PersistableField(FieldRetrieveMode.Deferred, FieldManagementType.ToFilepath, "XML_", "XML")]
        [DbField(PersistenceProviderType.SqlServer, "F_XML", false, "", "", true)]
        public string XmlFilepath
        {
            get { return GetProperty("XmlFilepath", ref _xmlFilepath); }
            set { SetProperty("XmlFilepath", value, ref _xmlFilepath); }
        }

        //[PersistableField(FieldRetrieveMode.Deferred)]
        //[RelatedEntity("OrderId", "Id", RelationType.OneToOne, true)]
        //public OrderEntity Order
        //{
        //    get
        //    {
        //        return GetProperty("Order", ref _order);
        //    }
        //    set
        //    {
        //        SetProperty("Order", value, ref _order);
        //    }
        //}

        //[PersistableField(FieldRetrieveMode.Immediate)]
        //[DbField(PersistenceProviderType.SqlServer, "F_ID_T_ORDERS", true)]
        //public int OrderId
        //{
        //    get { return GetProperty("OrderId", ref _orderId); }
        //    set { SetProperty("OrderId", value, ref _orderId); }
        //}

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_REVISION", true)]
        public int Revision
        {
            get { return GetProperty("Revision", ref _revision); }
            set { SetProperty("Revision", value, ref _revision); }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        /***************************************************************************
		 * ReadFileXml
		 * Legge il codice dal file XML
		 * Parametri:
		 *			fileXML			: nome del file XML
		 * Ritorna:
		 *			true	lettura eseguita con successo
		 *			false	errore nella lettura
		 * **************************************************************************/
        public bool ReadFileXml(string fileXML)
        {
            XmlDocument doc = new XmlDocument();

            bool exist;

            try
            {
                // Legge il file XML
                doc.Load(fileXML);

                XmlNode chn = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

                // Legge il codice dell'offerta
                if (chn.Name == "Offer")
                {
                    //if (mOffer.CheckExistingCode(chn.Attributes.GetNamedItem("F_CODE").Value, "T_WK_XML_OFFERS", out exist) && !exist)
                    //    mCode = chn.Attributes.GetNamedItem("F_CODE").Value;
                    //else
                    //    errorCode = errorType.DUPLICATE_CODE;

                    Code = chn.Attributes.GetNamedItem("F_CODE").Value;

                    if (chn.Attributes.GetNamedItem("Code") != null)
                        if (chn.Attributes.GetNamedItem("Code").Value.Length > 50)
                            Description = chn.Attributes.GetNamedItem("Code").Value.Substring(0, 50);
                        else
                            Description = chn.Attributes.GetNamedItem("Code").Value;

                    if (chn.Attributes.GetNamedItem("Adjustement_index") != null)
                        Revision = int.Parse(chn.Attributes.GetNamedItem("Adjustement_index").Value);
                    else
                        Revision = -1;
                }

                return true;
            }

            catch (XmlException ex)
            {
                TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
                return false;
            }
            catch (ApplicationException ex)
            {
                TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
                return false;
            }
            finally
            {
                doc = null;
            }
        }



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
