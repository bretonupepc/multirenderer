﻿using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using Breton.MathUtils;
using Breton.Polygons;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using SlabOperate.Business.Entities.SHAPE_GEOMETRY.WORKINGS;
using TraceLoggers;

namespace SlabOperate.Business.Entities.ORDERS
{
    public enum ShapeType
    {
        F_SHAPE_TYPE_RETT,
        F_SHAPE_TYPE_POLY
    }

    [MainDbTable(PersistenceProviderType.SqlServer, "T_SHAPES")]
    public class ShapeEntity : BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private int _orderId = -1;

        private int _materialId = -1;

        private int _initialQuantity = 1;

        private int _assignedQuantity = 1;

        private int _producedQuantity = 1;
        
        private ShapeType _shapeType = ShapeType.F_SHAPE_TYPE_RETT;

        private double _length;
        private double _width;
        private double _thickness;

        private int _priority = -1;

        private string _description = string.Empty;

        private bool _stamp;

        private string _code = string.Empty;

        private string _notes = string.Empty;

        private string _xmlPolygon = string.Empty;

        private int _shapeRun;

        private double _angle = 0;

        private double _rotation = 0;

        private OrderEntity _order = null;

        private MaterialEntity _material = null;

        private List<BaseWorkingElement> _workings = new List<BaseWorkingElement>();


        private Zone _geometryInfo = null;
        private Zone _boundingBox = null;

        private Path _boundingBoxExtended;



        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_SHAPE", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_ORDER", true)]
        public int OrderId
        {
            get { return GetProperty("OrderId", ref _orderId); }
            set { SetProperty("OrderId", value, ref _orderId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_MATERIAL", true)]
        public int MaterialId
        {
            get { return GetProperty("MaterialId", ref _materialId); }
            set { SetProperty("MaterialId", value, ref _materialId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_INITIAL_QUANTITY", true)]
        public int InitialQuantity
        {
            get { return GetProperty("InitialQuantity", ref _initialQuantity); }
            set { SetProperty("InitialQuantity", value, ref _initialQuantity); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ASSIGNED_QUANTITY", true)]
        public int AssignedQuantity
        {
            get { return GetProperty("AssignedQuantity", ref _assignedQuantity); }
            set { SetProperty("AssignedQuantity", value, ref _assignedQuantity); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PRODUCED_QUANTITY", true)]
        public int ProducedQuantity
        {
            get { return GetProperty("ProducedQuantity", ref _producedQuantity); }
            set { SetProperty("ProducedQuantity", value, ref _producedQuantity); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_SHAPE_TYPE", true)]
        public ShapeType ShapeType
        {
            get { return GetProperty("ShapeType", ref _shapeType); }
            set { SetProperty("ShapeType", value, ref _shapeType); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_LENGTH", true)]
        public double Length
        {
            get { return GetProperty("Length", ref _length); }
            set { SetProperty("Length", value, ref _length); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_WIDTH", true)]
        public double Width
        {
            get { return GetProperty("Width", ref _width); }
            set { SetProperty("Width", value, ref _width); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_THICKNESS", true)]
        public double Thickness
        {
            get { return GetProperty("Thickness", ref _thickness); }
            set { SetProperty("Thickness", value, ref _thickness); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PRIORITY", false)]
        public int Priority
        {
            get { return GetProperty("Priority", ref _priority); }
            set { SetProperty("Priority", value, ref _priority); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION", false)]
        public string Description
        {
            get { return GetProperty("Description", ref _description); }
            set { SetProperty("Description", value, ref _description); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_STAMP", true)]
        public bool Stamp
        {
            get { return GetProperty("Stamp", ref _stamp); }
            set { SetProperty("Stamp", value, ref _stamp); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_SHAPE_CODE", false)]
        public string Code
        {
            get { return GetProperty("Code", ref _code); }
            set { SetProperty("Code", value, ref _code); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_NOTE", false)]
        public string Notes
        {
            get { return GetProperty("Notes", ref _notes); }
            set { SetProperty("Notes", value, ref _notes); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_POLYGON", false)]
        public string XmlPolygon
        {
            get { return GetProperty("XmlPolygon", ref _xmlPolygon); }
            set { SetProperty("XmlPolygon", value, ref _xmlPolygon); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_SHAPE_RUN", true)]
        public int ShapeRun
        {
            get { return GetProperty("ShapeRun", ref _shapeRun); }
            set { SetProperty("ShapeRun", value, ref _shapeRun); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ANGLE", false)]
        public double Angle
        {
            get { return GetProperty("Angle", ref _angle); }
            set { SetProperty("Angle", value, ref _angle); }
        }



        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("MaterialId", "Id", RelationType.OneToOne, true)]
        public MaterialEntity Material
        {
            get { return GetProperty("Material", ref _material); }
            set { SetProperty("Material", value, ref _material); }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("OrderId", "Id", RelationType.OneToOne, true)]
        public OrderEntity Order
        {
            get { return GetProperty("Material", ref _order); }
            set { SetProperty("Order", value, ref _order); }
        }

        //TODO: Rotation
        public double Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }


        public List<BaseWorkingElement> Workings
        {
            get { return _workings; }
        }

        public Zone GeometryInfo
        {
            get
            {
                if (_geometryInfo == null)
                    CreateGeometryInfo();
                return _geometryInfo;
            }
        }

        public Zone BoundingBox
        {
            get { return _boundingBox; }
            set { _boundingBox = value; }
        }

        public Path BoundingBoxExtended
        {
            get { return _boundingBoxExtended; }
            set { _boundingBoxExtended = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public XmlDocument CreatePolygonXml()
        {
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            XmlDeclaration xmldecl;
            xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(xmldecl);

            XmlNode root = doc.CreateNode(XmlNodeType.Element, "EXPORT", "");
            XmlAttribute attr = doc.CreateAttribute("version");
            attr.Value = "1.0";
            root.Attributes.Append(attr);

            XmlNode construction = doc.CreateNode(XmlNodeType.Element, "Construction", "");
            attr = doc.CreateAttribute("ConstructionName");
            attr.Value = Code;
            construction.Attributes.Append(attr);
            root.AppendChild(construction);

            //XmlNode toolpathindex = doc.CreateNode(XmlNodeType.Element, "ToolPathsIndex", "");
            //construction.AppendChild(toolpathindex);

            //XmlNode polygon = doc.CreateNode(XmlNodeType.Element, "Polygon", "");
            //construction.AppendChild(polygon);

            PathCollection pcoll = new PathCollection();
            pcoll.AddPath(_geometryInfo.GetPath());
            for (int i = 0; i < _geometryInfo.ZoneCount; i++)
            {
                Path internalPath = _geometryInfo.GetZone(i).GetPath();
                internalPath.SetRotationSense(MathUtil.RotationSense.CW);
                pcoll.AddPath(internalPath);
            }


            #region Add Polygon data

            for (int i = 0; i < pcoll.Count; i++)
            {
                Path p = pcoll[i];

                XmlNode polygon = doc.CreateNode(XmlNodeType.Element, "Polygon", "");
                construction.AppendChild(polygon);


                XmlNode toolpath = doc.CreateNode(XmlNodeType.Element, "ToolPath", "");
                toolpath.InnerText = p.IsCCW ? "20" : "12";
                polygon.AppendChild(toolpath);

                for (int j = 0; j < p.Count; j++)
                {
                    XmlNode side = doc.CreateNode(XmlNodeType.Element, "Side", "");
                    polygon.AppendChild(side);

                    XmlNode node;
                    if (p[j] is Segment)
                    {
                        AddSegmentNode(doc, p[j], side);
                    }
                    else
                    {
                        TraceLog.WriteLine("GeometricShape.SaveToXml error: Shape with arcs");
                    }
                }
            }

            #endregion Add polygon data


            # region Add Workings data

            foreach (BaseWorkingElement workingElement in Workings)
            {
                XmlNode working = doc.CreateNode(XmlNodeType.Element, "Working", "");
                construction.AppendChild(working);

                XmlNode node = doc.CreateNode(XmlNodeType.Element, "WorkingType", "");
                node.InnerText = workingElement.Type.ToString();
                working.AppendChild(node);

                switch (workingElement.Type)
                {
                    case WorkingElementType.Groove:

                        node = doc.CreateNode(XmlNodeType.Element, "Depth", "");
                        node.InnerText = ((WorkingGroove)workingElement).Depth.ToString("0.00",
                                            CultureInfo.InvariantCulture);
                        working.AppendChild(node);
                        break;
                }

                // Add Working path
                Path workingPath = workingElement.GeometryInfo.GetPath();
                workingPath.SetRotationSense(MathUtil.RotationSense.CW);
                for (int i = 0; i < workingPath.Count; i++)
                {
                    XmlNode side = doc.CreateNode(XmlNodeType.Element, "Side", "");
                    working.AppendChild(side);

                    if (workingPath[i] is Segment)
                    {
                        AddSegmentNode(doc, workingPath[i], side);
                    }
                    //else
                    //{
                    //    AddArcNode(doc, workingPath[i], side);
                    //}
                }
            }

            # endregion Workings data

            //#region Add Roddings data

            //foreach (BaseWorkingElement roddingElement in Roddings)
            //{
            //    XmlNode rodding = doc.CreateNode(XmlNodeType.Element, "Reinforcement", "");

            //    XmlAttribute depthAttribute = doc.CreateAttribute("Depth");
            //    depthAttribute.Value = ((WorkingRodding)roddingElement).Depth.ToString("0", CultureInfo.InvariantCulture);
            //    rodding.Attributes.Append(depthAttribute);

            //    construction.AppendChild(rodding);


            //    // Add Rodding path
            //    Path workingPath = roddingElement.GeometryInfo.GetPath();
            //    workingPath.SetRotationSense(MathUtil.RotationSense.CW);
            //    for (int i = 0; i < workingPath.Count; i++)
            //    {
            //        XmlNode side = doc.CreateNode(XmlNodeType.Element, "Side", "");
            //        rodding.AppendChild(side);

            //        if (workingPath[i] is Segment)
            //        {
            //            AddSegmentNode(doc, workingPath[i], side);
            //        }
            //        else
            //        {
            //            AddArcNode(doc, workingPath[i], side);
            //        }
            //    }
            //}

            //#endregion Roddings data


            doc.AppendChild(root);


            return doc;
        }


        public override bool SaveToRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            if (_geometryInfo != null)
            {
                XmlPolygon = CreatePolygonXml().InnerXml;
            }
            return base.SaveToRepository(providerType);
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void CreateGeometryInfo()
        {
            _geometryInfo = new Zone();

            Rect baseRect = new Rect(Length, Width);

            var path = new Path(baseRect);

            _geometryInfo.SetPath(path);

            Rect rect = _geometryInfo.GetRectangle();

            _boundingBox = new Zone(rect);


            _boundingBoxExtended = new Path(rect.ExtendRect(5, 5));

        }

        private static void AddSegmentNode(XmlDocument doc, Side side, XmlNode inputNode)
        {
            XmlNode node;
            node = doc.CreateNode(XmlNodeType.Element, "X1", "");
            node.InnerText = side.P1.X.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Y1", "");
            node.InnerText = side.P1.Y.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extreme1", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extension1", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "X2", "");
            node.InnerText = side.P2.X.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Y2", "");
            node.InnerText = side.P2.Y.ToString(CultureInfo.InvariantCulture);
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extreme2", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);

            node = doc.CreateNode(XmlNodeType.Element, "Extension2", "");
            node.InnerText = "0";
            inputNode.AppendChild(node);
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<






    }
}
