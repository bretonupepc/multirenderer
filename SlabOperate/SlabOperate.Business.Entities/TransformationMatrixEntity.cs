﻿using DataAccess.Business.BusinessBase;

namespace SlabOperate.Business.Entities
{
    public class TransformationMatrixEntity: BasePersistableEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        private double _a00 = 1.0;
        private double _a01;
        private double _a02;

        private double _a10;
        private double _a11 = 1.0;
        private double _a12;

        private double _a20;
        private double _a21;
        private double _a22 = 1.0;

        private double _v0;
        private double _v1;
        private double _v2;

 


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A00
        {
            get { return GetProperty("A00", ref _a00); }
            set { SetProperty("A00", value, ref _a00); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A01
        {
            get { return GetProperty("A01", ref _a01); }
            set { SetProperty("A01", value, ref _a01); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A02
        {
            get { return GetProperty("A02", ref _a02); }
            set { SetProperty("A02", value, ref _a02); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A10
        {
            get { return GetProperty("A10", ref _a10); }
            set { SetProperty("A10", value, ref _a10); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A11
        {
            get { return GetProperty("A11", ref _a11); }
            set { SetProperty("A11", value, ref _a11); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A12
        {
            get { return GetProperty("A12", ref _a12); }
            set { SetProperty("A12", value, ref _a12); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A20
        {
            get { return GetProperty("A20", ref _a20); }
            set { SetProperty("A20", value, ref _a20); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A21
        {
            get { return GetProperty("A21", ref _a21); }
            set { SetProperty("A21", value, ref _a21); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double A22
        {
            get { return GetProperty("A22", ref _a22); }
            set { SetProperty("A22", value, ref _a22); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double V0
        {
            get { return GetProperty("V0", ref _v0); }
            set { SetProperty("V0", value, ref _v0); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double V1
        {
            get { return GetProperty("V1", ref _v1); }
            set { SetProperty("V1", value, ref _v1); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        public double V2
        {
            get { return GetProperty("V2", ref _v2); }
            set { SetProperty("V2", value, ref _v2); }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




        //public override void SetProperty<T>(string propertyName, T value, bool forceStatus = false,
        //    FieldStatus newStatus = FieldStatus.Set,
        //    bool forceOnChanged = false,
        //    bool avoidOnChanged = false,
        //    bool avoidOnStatusChanged = false)
        //{
        //    try
        //    {
        //        switch (propertyName)
        //        {
        //            case "A00":
        //                base.SetProperty(propertyName, (double) Convert.ChangeType(value, typeof (double)),
        //                    ref _a00,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A01":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a01,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A02":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a02,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A10":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a10,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A11":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a11,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A12":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a12,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A20":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a20,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A21":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a21,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "A22":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _a22,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "V0":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _v0,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "V1":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _v1,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //            case "V2":
        //                base.SetProperty(propertyName, (double)Convert.ChangeType(value, typeof(double)),
        //                    ref _v2,
        //                    forceStatus, newStatus, forceOnChanged, avoidOnChanged, avoidOnStatusChanged);
        //                break;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TraceLog.WriteLine("SetProperty error ", ex);
        //    }
        //}

        //public override T GetProperty<T>(string propertyName)
        //{
        //    switch (propertyName)
        //    {
        //        case "A00":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a00), typeof(T));
        //            break;

        //        case "A01":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a01), typeof(T));
        //            break;

        //        case "A02":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a02), typeof(T));
        //            break;

        //        case "A10":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a10), typeof(T));
        //            break;

        //        case "A11":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a11), typeof(T));
        //            break;

        //        case "A12":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a12), typeof(T));
        //            break;

        //        case "A20":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a20), typeof(T));
        //            break;

        //        case "A21":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a21), typeof(T));
        //            break;

        //        case "A22":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _a22), typeof(T));
        //            break;

        //        case "V0":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _v0), typeof(T));
        //            break;

        //        case "V1":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _v1), typeof(T));
        //            break;

        //        case "V2":
        //            return (T)Convert.ChangeType(GetProperty(propertyName, ref _v2), typeof(T));
        //            break;


        //    }

        //    return default(T);
        //}


    }
}
