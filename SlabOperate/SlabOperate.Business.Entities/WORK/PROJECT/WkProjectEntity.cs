﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Xml;
using Breton.DesignUtil;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using SlabOperate.Business.Entities.ORDERS;
using SlabOperate.Business.Entities.WORK.ORDER;
using TraceLoggers;

namespace SlabOperate.Business.Entities
{
    public enum WkProjectStatus
    {
        PROJ_NDISP = 1, //        	Non disponibile
        PROJ_LOADED  = 2, //     	Caricato
        PROJ_ANALIZED = 3, //    	Analizzato
        PROJ_IN_PROGRESS = 4, // 	In produzione
        PROJ_COMPLETED = 5, //   	Completato
        PROJ_RELOADED  = 6 //   	Ricaricato   
    }

    [MainDbTable(PersistenceProviderType.SqlServer, "T_WK_CUSTOMER_PROJECTS")]
    public class WkProjectEntity : BasePersistableEntity
    {
        #region >-------------- Constants and Enums
        public enum Message
        {
            NONE,
            DA_CONFERMARE,
            NO_MATERIAL,
            NO_CUSTOMER,
            NO_CUT_TO_SIZE
        }

        private const string DEFAULT_TMP_DIR = "Temp_Detail_Xml";
        private const string XML_FILE_NAME = "PROJECTANALIZE";
        private string mTempDir = Path.Combine(System.IO.Path.GetTempPath() , DEFAULT_TMP_DIR);


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        private int _id = -1;

        private int _orderId = -1;

        private WkProjectStatus _projectStatus = WkProjectStatus.PROJ_NDISP;

        private string _code = "--------";

        private string _originalProjectCode = string.Empty;

        private DateTime _date = DateTime.Now;

        private DateTime _deliveryDate = DateTime.Now;

        private string _description = string.Empty;

        private int _revision = 0;

        private string _previewFilepath = string.Empty;

        private WkOrderEntity _order;

        private  PersistableEntityCollection<WkOrderDetailEntity> _details = new PersistableEntityCollection<WkOrderDetailEntity>();

        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }





        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE", true)]
        public string Code
        {
            get { return GetProperty("Code", ref _code); }
            set { SetProperty("Code", value, ref _code); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION")]
        public string Description
        {
            get { return GetProperty("Description", ref _description); }
            set { SetProperty("Description", value, ref _description); }
        }

        [PersistableField(FieldRetrieveMode.Deferred, FieldManagementType.ToFilepath, "IMG_", "PNG")]
        [DbField(PersistenceProviderType.SqlServer, "F_IMAGE_PREVIEW", false, "", "", true)]
        public string PreviewFilepath
        {
            get { return GetProperty("PreviewFilepath", ref _previewFilepath); }
            set { SetProperty("PreviewFilepath", value, ref _previewFilepath); }
        }




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_REVISION", true)]
        public int Revision
        {
            get { return GetProperty("Revision", ref _revision); }
            set { SetProperty("Revision", value, ref _revision); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_WK_CUSTOMER_ORDERS", true)]
        public int OrderId
        {
            get { return GetProperty("OrderId", ref _orderId); }
            set { SetProperty("OrderId", value, ref _orderId); }
        }




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_CO_CUSTOMER_PROJECT_STATES", true)]
        public WkProjectStatus ProjectStatus
        {
            get { return GetProperty("ProjectStatus", ref _projectStatus); }
            set { SetProperty("ProjectStatus", value, ref _projectStatus); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE_ORIGINAL_PROJECT", false)]
        public string OriginalProjectCode
        {
            get { return GetProperty("OriginalProjectCode", ref _originalProjectCode); }
            set { SetProperty("OriginalProjectCode", value, ref _originalProjectCode); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DATE", true)]
        public DateTime Date
        {
            get { return GetProperty("Date", ref _date); }
            set { SetProperty("Date", value, ref _date); }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("OrderId", "Id", RelationType.OneToOne, true)]
        public WkOrderEntity Order
        {
            get { return _order; }
            set { _order = value; }
        }

        public PersistableEntityCollection<WkOrderDetailEntity> Details
        {
            get { return _details; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public bool GetDetails()
        {
            bool retVal = true;
            _details.Scope = Scope;
            FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("ProjectId"), ConditionOperator.Eq, Id);
            retVal = _details.GetItemsFromRepository(clause: filterClause);

            return retVal;
        }


        /***************************************************************************
		 * ReadFileXml
		 * Legge il codice dal file XML
		 * Parametri:
		 *			fileXML			: nome del file XML
		 * Ritorna:
		 *			true	lettura eseguita con successo
		 *			false	errore nella lettura
		 * **************************************************************************/
        public bool ReadXmlNode(string fileXml, XmlNode node, out Message msg)
        {
            //Article art = new Article();
            WkOrderDetailEntity det;
            XmlDocument originalDoc = new XmlDocument();
            int qty;
            //Coordinate coord;
            string matCode;

            MaterialEntity material;
            string shapeNumber;

            msg = Message.NONE;

            _details.Scope = Scope;

            try
            {
                // Legge il nodo XML
                XmlNode chn = node.SelectSingleNode("Shapes");

                if (!Directory.Exists(mTempDir))
                    Directory.CreateDirectory(mTempDir);

                foreach (XmlNode nd in chn.ChildNodes)
                {
                    if (nd.Name == "Shape")
                    {
                        qty = int.Parse(nd.Attributes.GetNamedItem("Quantity").Value); // Quanti sono i dettagli da caricare di questo tipo
                        shapeNumber = nd.Attributes.GetNamedItem("Shape_Number").Value;

                        if (fileXml != "" && fileXml != null)
                        {
                            originalDoc.Load(fileXml);
                            if (DeleteAllShapeNodes_Except(ref originalDoc, shapeNumber))
                                originalDoc.Save(Path.Combine(mTempDir , XML_FILE_NAME + "_" + OriginalProjectCode.Trim() + "_" + shapeNumber.Trim() + ".xml"));
                        }

                        for (int i = 0; i < qty; i++)
                        {
                            det = new WkOrderDetailEntity();
                            det.Scope = Scope;

                            //art = new Article();
                            ////coord = new Coordinate();
                            //mArticles = new ArticleCollection(mCollectionDbInterface);

                            //matCode = MaterialCode(nd, out msg);

                            material = GetMaterial(nd, out msg);

                            if (material == null)	// Materiale non assegnato
                            {
                                msg = Message.NO_MATERIAL;
                                throw new ApplicationException("Some detail have no associate material");
                            }

                            if (!ExistCutToSize(nd))
                            {
                                msg = Message.NO_CUT_TO_SIZE;
                                throw new ApplicationException("Some detail have no cut to size");
                            }

                            // Aggiunge il pezzo trovato al progetto
                            det.MaterialId = material.Id;													// Materiale
                            det.Thickness = double.Parse(nd.Attributes.GetNamedItem("Thickness").Value);	// Spessore
                            det.DetailStatus = WkOrderDetailStatus.SHAPE_LOADED; 						// Stato dettaglio di default
                            det.OriginalShapeCode = nd.Attributes.GetNamedItem("Shape_Number").Value;		// Codice del dettaglio nel file xml
                            if (nd.Attributes.GetNamedItem("Description").Value.Length > 50)
                                det.Description = nd.Attributes.GetNamedItem("Description").Value.Substring(0, 50);    // Descrizione del dettaglio
                            else
                                det.Description = nd.Attributes.GetNamedItem("Description").Value;	        // Descrizione del dettaglio
                            det.ShapeInfoFilepath = Path.Combine(mTempDir,
                                XML_FILE_NAME + "_" + OriginalProjectCode.Trim() + "_" + shapeNumber.Trim() + ".xml");	// Xml del dettagli

                            if (nd.Attributes.GetNamedItem("Adjustement_index") != null)
                                det.Revision = int.Parse(nd.Attributes.GetNamedItem("Adjustement_index").Value);       // Revisione

                            _details.Add(det);
                        }
                    }
                }

                // Imposta le informazioni dei pezzi
                if (fileXml != "" && fileXml != null)
                    SetDetailsInfo();

                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("Project.ReadXmlNode Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Project.ReadXmlNode Error. ", ex);
                return false;
            }
            finally
            {
                originalDoc = null;
            }
        }

        private void SetDetailsInfo()
        {
            ShapeGeometry spGeo;
            WkOrderDetailEntity det;
            ArrayList shapes = new ArrayList();

            // Carica tutte le shape
            foreach(var detail in _details)
            {

                Shape s = new Shape();
                spGeo = new ShapeGeometry();
                // Analisi del file xml
                XmlAnalizer.ReadShapeXml(detail.ShapeInfoFilepath, ref s, false);
                shapes.Add(s);

                spGeo.LoadShapePath(s);
                detail.Width = spGeo.Width();
                detail.Length = spGeo.Length();

            }

            SetFinalThickness(shapes);
        }

        private void SetFinalThickness(ArrayList shapes)
        {
            double maxThick = 0d;

            // Scorre tutti i pezzi trovati
            for (int i = 0; i < shapes.Count; i++)
            {
                Shape s1 = (Shape)shapes[i];

                switch (s1.gType)
                {
                    // Tipo di sagoma normale
                    case Shape.PieceType.NORMAL:
                        // Scorre tutti i pezzi per verificare se ci sono pezzi che devono essere attaccati a questo
                        for (int j = 0; j < shapes.Count; j++)
                        {
                            Shape s2 = (Shape)shapes[j];

                            switch (s2.gType)
                            {
                                // Verifica se l'alzatina è collegata al pezzo principale
                                case Shape.PieceType.VELETTA:
                                    if (s2.gShapeIdJoin == s1.gId)
                                    {
                                        double tmpHeight;
                                        ShapeGeometry spGeo = new ShapeGeometry();
                                        spGeo.LoadShapePath(s2);
                                        tmpHeight = spGeo.Width();
                                        if (tmpHeight > maxThick)
                                            maxThick = tmpHeight;

                                    }
                                    break;

                                // Verifica se la laminazione è collegata al pezzo principale
                                case Shape.PieceType.LAMINATION:
                                    if (s2.gShapeNumberJoin == s1.gShapeNumber)
                                    {
                                        if (s1.gThickness + s2.gThickness > maxThick)
                                            maxThick = s1.gThickness + s2.gThickness;
                                    }
                                    break;
                            }
                        }

                        // Verifica i vgroove
                        for (int j = 0; j < s1.gVGrooves.Count; j++)
                        {
                            Shape.VGroove v = (Shape.VGroove)s1.gVGrooves[j];

                            if (v.height > maxThick)
                                maxThick = v.height;

                        }

                        foreach (var detail in _details)
                        {
                            if (detail.OriginalShapeCode.Trim() == s1.gShapeNumber.ToString())
                            {
                                if (maxThick > 0)
                                    detail.FinalThickness = maxThick;
                                else
                                    detail.FinalThickness = detail.Thickness;
                                break;
                            }
                        }

                        break;

                    // Sagoma di tipo alzatina o laminazione
                    case Shape.PieceType.VELETTA:
                    case Shape.PieceType.LAMINATION:
                        foreach (var detail in _details)
                        {
                            if (detail.OriginalShapeCode.Trim() == s1.gShapeNumber.ToString())
                            {
                                detail.FinalThickness = detail.Thickness;
                                break;
                            }
                        }
                        break;
                }

                maxThick = 0d;
            }
        }



        /// <summary>
        /// Verifica se nel pezzo sono presenti i grezzi di taglio corretti
        /// </summary>
        /// <param name="nd">nodo shape da verificare</param>
        /// <returns></returns>
        private bool ExistCutToSize(XmlNode nd)
        {
            bool find = false;

            try
            {
                // Se non ha figli ritorna false
                if (!nd.HasChildNodes)
                    return false;

                // Scorre tutti i sottonodi
                foreach (XmlNode chn in nd.ChildNodes)
                {
                    if (chn.Name == "Cut_To_Size")
                    {
                        foreach (XmlNode n in chn.ChildNodes)
                        {
                            // Se il nodo trovato è diverso da Origin o Polygon ritorna errore
                            if (n.Name != "Origin" && n.Name != "Polygon" && n.Name != "#comment")
                                return false;
                        }

                        find = true;
                    }
                }

                if (find)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Project.ExistCutToSize Error.", ex);
                return false;
            }
        }


        private MaterialEntity GetMaterial(XmlNode node, out Message msg)
        {
            string matCode;
            bool exist;

            bool retVal = false;

            MaterialEntity material = null;

            msg = Message.NONE;
            try
            {
                // Scorre tutti i sottonodi
                foreach (XmlNode chn in node.ChildNodes)
                {
                    if (chn.Name == "Material")
                    {
                        matCode = chn.Attributes.GetNamedItem("Code").Value;

                        if (matCode == null || matCode.Length == 0)
                            return null;

                        var materials = new PersistableEntityCollection<MaterialEntity>(Scope);
                        FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("Code"), ConditionOperator.Eq, matCode );
                        materials.GetItemsFromRepository(clause: filterClause);

                        if (!materials.Any())
                        {
                            string materialClassCode = chn.Attributes.GetNamedItem("Class_Code").Value;
                            string materialClassDescription = chn.Attributes.GetNamedItem("Class_Description").Value;
                            var materialClasses =  new PersistableEntityCollection<MaterialClassEntity>(Scope);
                            filterClause = new FilterClause(new FieldItemBaseInfo("Code"), ConditionOperator.Eq, materialClassCode );
                            materialClasses.GetItemsFromRepository(clause: filterClause);
                            MaterialClassEntity materialClass;
                            if (!materialClasses.Any())
                            {
                                materialClass = new MaterialClassEntity()
                                {
                                    Scope = Scope,
                                    Code = materialClassCode,
                                    Name = materialClassDescription
                                };
                                retVal = materialClass.SaveToRepository();
                                if (!retVal) return null;

                            }
                            else
                            {
                                materialClass = materialClasses[0];
                            }

                            material = new MaterialEntity()
                            {
                                Scope = Scope,
                                MaterialClassId = materialClass.Id,
                                Code = chn.Attributes.GetNamedItem("Code").Value,
                                Description = chn.Attributes.GetNamedItem("Description").Value
                            };
                            retVal = material.SaveToRepository();
                            if (!retVal) return null;

                        }
                        else
                        {
                            material = materials[0];
                        }



                        return material;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Project.GetMaterialCode Error.", ex);
                return null;
            }
        }

        //**********************************************************************
        // DeleteAllShapeNodes_Except
        // Cancella tutti i nodi shape che non hanno l'id interessato
        // Parametri:
        //			doc		    : xml document da modificare
        //			shapeNumber	: shape number da non eliminare
        //**********************************************************************
        private bool DeleteAllShapeNodes_Except(ref XmlDocument doc, string shapeNumber)
        {
            ArrayList nodes = new ArrayList();
            XmlNode node = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer/Top/Shapes");

            try
            {
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (nd.Name != "Shape" || nd.Attributes.GetNamedItem("Shape_Number").Value != shapeNumber)
                        nodes.Add(nd);
                }

                for (int i = 0; i < nodes.Count; i++)
                {
                    node.RemoveChild((XmlNode)nodes[i]);
                }

                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
                return false;
            }
        }


        public override bool SaveToRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            bool retVal = base.SaveToRepository(providerType);
            if (retVal)
            {
                foreach (var detail in _details)
                {
                    detail.ProjectId = Id;
                    retVal = detail.SaveToRepository();
                    if (!retVal) break;

                }
            }
            return retVal;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
