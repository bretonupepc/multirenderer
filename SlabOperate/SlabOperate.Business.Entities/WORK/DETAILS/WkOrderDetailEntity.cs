﻿using System;
using System.Xml;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using SlabOperate.Business.Entities.ORDERS;
using SlabOperate.Business.Entities.WORK.DETAILS;
using TraceLoggers;

namespace SlabOperate.Business.Entities
{
    public enum WkOrderDetailStatus
    {
        SHAPE_LOADED = 1, //      	Caricato
        SHAPE_ANALIZED = 2, //   	Analizzato
        SHAPE_IN_PROGRESS = 3, //	In produzione
        SHAPE_COMPLETED = 4, //  	Completato
        SHAPE_STANDBY = 5, //   	Bloccato
        SHAPE_RELOADED = 6 //   	Ricaricato    
    }

    [MainDbTable(PersistenceProviderType.SqlServer, "T_WK_CUSTOMER_ORDER_DETAILS")]
    public class WkOrderDetailEntity : BasePersistableEntity
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private int _projectId = -1;

        private int _materialId = -1;

        private double _thickness;

        private int _slabId = -1;

        private WkOrderDetailStatus _detailStatus = WkOrderDetailStatus.SHAPE_ANALIZED;

        private string _code = "----------";

        private string _originalShapeCode = string.Empty;

        private string _shapeInfoFilepath = string.Empty;

        private string _shapeInfo = string.Empty;

        private string _description = string.Empty;

        private int _revision = -1;

        private double _finalThickness;

        private double _length;

        private double _width;

        private int _piecesQty = 1;

        private WkProjectEntity _project;

        private MaterialEntity _material;

        private WkOrderDetailPieceEntity _orderDetailPiece;

        private bool _isAssignedToSlab = false;


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }





        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE", true)]
        public string Code
        {
            get { return GetProperty("Code", ref _code); }
            set { SetProperty("Code", value, ref _code); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION")]
        public string Description
        {
            get { return GetProperty("Description", ref _description); }
            set { SetProperty("Description", value, ref _description); }
        }

        [PersistableField(FieldRetrieveMode.Deferred, FieldManagementType.ToFilepath, "XML_", "XML")]
        [DbField(PersistenceProviderType.SqlServer, "F_SHAPE_INFO", false, "", "", true)]
        public string ShapeInfoFilepath
        {
            get { return GetProperty("ShapeInfoFilepath", ref _shapeInfoFilepath); }
            set { SetProperty("ShapeInfoFilepath", value, ref _shapeInfoFilepath); }
        }




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_REVISION", true)]
        public int Revision
        {
            get { return GetProperty("Revision", ref _revision); }
            set { SetProperty("Revision", value, ref _revision); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_WK_CUSTOMER_PROJECTS", true)]
        public int ProjectId
        {
            get { return GetProperty("ProjectId", ref _projectId); }
            set { SetProperty("ProjectId", value, ref _projectId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_AN_MATERIALS", true)]
        public int MaterialId
        {
            get { return GetProperty("MaterialId", ref _materialId); }
            set { SetProperty("MaterialId", value, ref _materialId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_THICKNESS", true)]
        public double Thickness
        {
            get { return GetProperty("Thickness", ref _thickness); }
            set { SetProperty("Thickness", value, ref _thickness); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_AN_ARTICLES", true)]
        public int SlabId
        {
            get { return GetProperty("SlabId", ref _slabId); }
            set { SetProperty("SlabId", value, ref _slabId); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_CO_CUSTOMER_ORDER_DETAIL_STATES", true)]
        public WkOrderDetailStatus DetailStatus
        {
            get { return GetProperty("DetailStatus", ref _detailStatus); }
            set { SetProperty("DetailStatus", value, ref _detailStatus); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE_ORIGINAL_SHAPE", true)]
        public string OriginalShapeCode
        {
            get { return GetProperty("OriginalShapeCode", ref _originalShapeCode); }
            set { SetProperty("OriginalShapeCode", value, ref _originalShapeCode); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_FINAL_THICKNESS", true)]
        public double FinalThickness
        {
            get { return GetProperty("FinalThickness", ref _finalThickness); }
            set { SetProperty("FinalThickness", value, ref _finalThickness); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_LENGTH", true)]
        public double Length
        {
            get { return GetProperty("Length", ref _length); }
            set { SetProperty("Length", value, ref _length); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_WIDTH", true)]
        public double Width
        {
            get { return GetProperty("Width", ref _width); }
            set { SetProperty("Width", value, ref _width); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_PIECES_QTY", true)]
        public int PiecesQty
        {
            get { return GetProperty("PieceQty", ref _piecesQty); }
            set { SetProperty("PieceQty", value, ref _piecesQty); }
        }


        public string ShapeInfo
        {
            get { return _shapeInfo; }
            set { _shapeInfo = value; }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("ProjectId", "Id", RelationType.OneToOne, true)]
        public WkProjectEntity Project
        {
            get { return GetProperty("Project", ref _project); }
            set { SetProperty("Project", value, ref _project); }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("MaterialId", "Id", RelationType.OneToOne, true)]
        public MaterialEntity Material
        {
            get { return GetProperty("Material", ref _material); }
            set { SetProperty("Material", value, ref _material); }
        }


        public string Dimensions
        {
            get
            {
                return string.Format("L {0:#.00} x W {1:#.00}", Length, Width);
            }
        }


        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("Id", "OrderDetailId", RelationType.OneToOne, true)]
        public WkOrderDetailPieceEntity OrderDetailPiece
        {
            get
            {
                return GetProperty("OrderDetailPiece", ref _orderDetailPiece);
            }
            set { SetProperty("OrderDetailPiece", value, ref _orderDetailPiece); }
        }

        public bool IsAssignedToSlab
        {
            get { return _isAssignedToSlab; }
            set
            {
                _isAssignedToSlab = value;
                OnPropertyChanged("IsAssignedToSlab");
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public override string ToString()
        {
            return string.Format("L {0:#.00} x W {1:#.00}", Length, Width);
        }

        /***************************************************************************
		 * ReadFileXml
		 * Legge il codice dal file XML
		 * Parametri:
		 *			fileXML			: nome del file XML
		 * Ritorna:
		 *			true	lettura eseguita con successo
		 *			false	errore nella lettura
		 * **************************************************************************/
        public bool ReadFileXml(string fileXML)
        {
            XmlDocument doc = new XmlDocument();

            bool exist;

            try
            {
                // Legge il file XML
                doc.Load(fileXML);

                XmlNode chn = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

                // Legge il codice dell'offerta
                if (chn.Name == "Offer")
                {
                    //if (mOffer.CheckExistingCode(chn.Attributes.GetNamedItem("F_CODE").Value, "T_WK_XML_OFFERS", out exist) && !exist)
                    //    mCode = chn.Attributes.GetNamedItem("F_CODE").Value;
                    //else
                    //    errorCode = errorType.DUPLICATE_CODE;

                    Code = chn.Attributes.GetNamedItem("F_CODE").Value;

                    if (chn.Attributes.GetNamedItem("Code") != null)
                        if (chn.Attributes.GetNamedItem("Code").Value.Length > 50)
                            Description = chn.Attributes.GetNamedItem("Code").Value.Substring(0, 50);
                        else
                            Description = chn.Attributes.GetNamedItem("Code").Value;

                    if (chn.Attributes.GetNamedItem("Adjustement_index") != null)
                        Revision = int.Parse(chn.Attributes.GetNamedItem("Adjustement_index").Value);
                    else
                        Revision = -1;
                }

                return true;
            }

            catch (XmlException ex)
            {
                TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
                return false;
            }
            catch (ApplicationException ex)
            {
                TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Offer.ReadFileXml Error. ", ex);
                return false;
            }
            finally
            {
                doc = null;
            }
        }

        public bool SavePieceDetail()
        {
            bool retVal = false;

            if (OrderDetailPiece != null)
            {
                OrderDetailPiece.OrderDetailId = Id;
                retVal = OrderDetailPiece.SaveToRepository();
            }
            return retVal;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
