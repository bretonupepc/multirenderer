﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Breton.DesignUtil;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace SlabOperate.Business.Entities.WORK.DETAILS
{
    [MainDbTable(PersistenceProviderType.SqlServer, "T_WK_CUSTOMER_ORDER_DETAILS_PIECES")]
    public class WkOrderDetailPieceEntity : BasePersistableEntity
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private int _orderDetailId = -1;

        private int _slabId = -1;

        private double _posX;

        private double _posY;

        private double _rotationAngle;

        private string _xmlFilepath = string.Empty;

        private WkOrderDetailEntity _orderDetail;

        private SlabEntity _slab;

        private Shape _shape;



        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_WK_CUSTOMER_ORDER_DETAILS", true)]
        public int OrderDetailId
        {
            get { return GetProperty("OrderDetailId", ref _orderDetailId); }
            set { SetProperty("OrderDetailId", value, ref _orderDetailId); }
        }




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_SLABS", true)]
        public int SlabId
        {
            get { return GetProperty("SlabId", ref _slabId); }
            set { SetProperty("SlabId", value, ref _slabId); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_POS_X", true)]
        public double PosX
        {
            get { return GetProperty("PosX", ref _posX); }
            set { SetProperty("PosX", value, ref _posX); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_POS_Y", true)]
        public double PosY
        {
            get { return GetProperty("PosY", ref _posY); }
            set { SetProperty("PosY", value, ref _posY); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ROTATION_ANGLE", true)]
        public double RotationAngle
        {
            get { return GetProperty("RotationAngle", ref _rotationAngle); }
            set { SetProperty("RotationAngle", value, ref _rotationAngle); }
        }



        [PersistableField(FieldRetrieveMode.Deferred, FieldManagementType.ToFilepath, "XML_", "XML")]
        [DbField(PersistenceProviderType.SqlServer, "F_POLYGON", false, "", "", true)]
        public string XmlFilepath
        {
            get { return GetProperty("XmlFilepath", ref _xmlFilepath); }
            set { SetProperty("XmlFilepath", value, ref _xmlFilepath); }
        }


        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("OrderDetailId", "Id", RelationType.OneToOne, true)]
        public WkOrderDetailEntity OrderDetail
        {
            get { return GetProperty("OrderDetail", ref _orderDetail); }
            set
            {
                SetProperty("OrderDetail", value, ref _orderDetail);
                OrderDetailId = (_orderDetail != null) ? _orderDetail.Id : -1;
            }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("SlabId", "Id", RelationType.OneToOne, true)]
        public SlabEntity Slab
        {
            get { return GetProperty("Slab", ref _slab); }
            set
            {
                SetProperty("Slab", value, ref _slab);
                SlabId = (_slab != null) ? _slab.Id : -1;
            }
        }

        public Shape Shape
        {
            get { return _shape; }
            set { _shape = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
