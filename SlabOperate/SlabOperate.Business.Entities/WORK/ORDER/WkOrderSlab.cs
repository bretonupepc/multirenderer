﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace SlabOperate.Business.Entities.WORK.ORDER
{
    [MainDbTable(PersistenceProviderType.SqlServer, "T_WK_CUSTOMER_ORDERS_SLABS")]
    public class WkOrderSlab : BasePersistableEntity
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private int _orderId = -1;

        private int _slabId = -1;

        private bool _isUsed = false;

        private WkOrderEntity _order;

        private SlabEntity _slab;


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_WK_CUSTOMER_ORDERS", true)]
        public int OrderId
        {
            get { return GetProperty("OrderId", ref _orderId); }
            set { SetProperty("OrderId", value, ref _orderId); }
        }




        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_SLABS", true)]
        public int SlabId
        {
            get { return GetProperty("SlabId", ref _slabId); }
            set { SetProperty("SlabId", value, ref _slabId); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_USED", true)]
        public bool IsUsed
        {
            get { return GetProperty("IsUsed", ref _isUsed); }
            set { SetProperty("IsUsed", value, ref _isUsed); }
        }




        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("OrderId", "Id", RelationType.OneToOne, true)]
        public WkOrderEntity Order
        {
            get { return GetProperty("Order", ref _order); }
            set
            {
                SetProperty("Order", value, ref _order);
                OrderId = (_order != null) ? _order.Id : -1;
            }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("SlabId", "Id", RelationType.OneToOne, true)]
        public SlabEntity Slab
        {
            get { return GetProperty("Slab", ref _slab); }
            set
            {
                SetProperty("Slab", value, ref _slab);
                SlabId = (_slab != null) ? _slab.Id : -1;
            }
        }



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }

}
