﻿using System;
using System.Collections;
using System.IO;
using System.Xml;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using SlabOperate.Business.Entities.ORDERS;
using TraceLoggers;

namespace SlabOperate.Business.Entities.WORK.ORDER
{
    public enum WkOrderStatus
    {
        ORDER_NDISP = 1,       //	Non disponibile
        ORDER_LOADED   = 2,   	//Caricato
        ORDER_ANALIZED = 3,   	//Analizzato
        ORDER_CONFIRMED = 4,  	//Confermato
        ORDER_IN_PROGRESS = 5,	//In produzione
        ORDER_COMPLETED = 6,  	//Completato
        ORDER_UNLOADED = 7,   	//Scaricato
        ORDER_RELOADED = 8   	//Ricaricato   
    }

    [MainDbTable(PersistenceProviderType.SqlServer, "T_WK_CUSTOMER_ORDERS")]
    public class WkOrderEntity : BasePersistableEntity
    {
        #region >-------------- Constants and Enums

        private const string DEFAULT_TMP_DIR = "Temp_Detail_Xml";
        private const string XML_FILE_NAME = "ORDER_ANALIZE.xml";
        private string mTempDir = Path.Combine(System.IO.Path.GetTempPath(), DEFAULT_TMP_DIR);


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields


        private int _id = -1;

        private int _customerId = -1;

        private WkOrderStatus _orderStatus = WkOrderStatus.ORDER_ANALIZED;

        private int _offerId = -1;

        private string _orderCode = string.Empty;

        private DateTime _date = DateTime.MinValue;

        private DateTime _deliveryDate = DateTime.MinValue;

        private string _description = string.Empty;

        private DateTime _lastOfferValidityDate = DateTime.MinValue;

        private int _revision = 0;

        private string _workNumber = string.Empty;

        private string _customerName = string.Empty;

        private OfferEntity _offer;

        private PersistableEntityCollection<WkProjectEntity> _projects = new PersistableEntityCollection<WkProjectEntity>(); 

        private PersistableEntityCollection<WkOrderSlab> _availableSlabs = new PersistableEntityCollection<WkOrderSlab>();


        #endregion Private Fields --------<

        #region >-------------- Constructors



        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return _id; }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DATE", true)]
        public DateTime Date
        {
            get { return GetProperty("Date", ref _date); }
            set { SetProperty("Date", value, ref _date); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DELIVERY_DATE", false)]
        public DateTime DeliveryDate
        {
            get { return GetProperty("DeliveryDate", ref _deliveryDate); }
            set { SetProperty("DeliveryDate", value, ref _deliveryDate); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_LAST_OFFER_VALIDITY_DATE", false)]
        public DateTime LastOfferValidityDate
        {
            get { return GetProperty("LastOfferValidityDate", ref _lastOfferValidityDate); }
            set { SetProperty("LastOfferValidityDate", value, ref _lastOfferValidityDate); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_CO_CUSTOMER_ORDER_STATES", true)]
        public WkOrderStatus OrderStatus
        {
            get { return GetProperty("OrderStatus", ref _orderStatus); }
            set { SetProperty("OrderStatus", value, ref _orderStatus); }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE", true)]
        public string OrderCode
        {
            get { return GetProperty("OrderCode", ref _orderCode); }
            set
            {
                SetProperty("OrderCode", value, ref _orderCode);
                if (!string.IsNullOrEmpty(_orderCode)) _orderCode = _orderCode.Trim();
            }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_DESCRIPTION")]
        public string Description
        {
            get { return GetProperty("Description", ref _description); }
            set { SetProperty("Description", value, ref _description); }
        }



        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_AN_CUSTOMERS", false)]
        public int CustomerId
        {
            get { return GetProperty("CustomerId", ref _customerId); }
            set { SetProperty("CustomerId", value, ref _customerId); }
        }






        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID_T_WK_XML_OFFERS", true)]
        public int OfferId
        {
            get { return GetProperty("OfferId", ref _offerId); }
            set { SetProperty("OfferId", value, ref _offerId); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_REVISION", true)]
        public int Revision
        {
            get { return GetProperty("Revision", ref _revision); }
            set { SetProperty("Revision", value, ref _revision); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_WORK_NUMBER", false)]
        public string WorkNumber
        {
            get { return GetProperty("WorkNumber", ref _workNumber); }
            set { SetProperty("WorkNumber", value, ref _workNumber); }
        }

        [PersistableField(FieldRetrieveMode.Deferred)]
        [RelatedEntity("OfferId", "Id", RelationType.OneToOne, true)]
        public OfferEntity Offer
        {
            get { return _offer; }
            set { _offer = value; }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CUSTOMER_NAME", false)]
        public string CustomerName
        {
            get { return GetProperty("CustomerName", ref _customerName); }
            set { SetProperty("CustomerName", value, ref _customerName); }
        }

        public PersistableEntityCollection<WkProjectEntity> Projects
        {
            get { return _projects; }
        }

        public PersistableEntityCollection<WkOrderSlab> AvailableSlabs
        {
            get { return _availableSlabs; }
            set
            {
                _availableSlabs = value;
                OnPropertyChanged("AvailableSlabs");
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public bool GetProjects()
        {
            bool retVal = true;
            _projects.Scope = Scope;
            FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("OrderId"), ConditionOperator.Eq, Id );
            retVal = _projects.GetItemsFromRepository(clause: filterClause);
            if (retVal)
            {
                foreach (var prj in _projects)
                {
                    retVal = prj.GetDetails();
                    if (!retVal) break;
                }
            }

            return retVal;
        }

        public bool GetAvailableSlabs()
        {
            bool retVal = false;
            _availableSlabs.Scope = Scope;

            FilterClause filterClause = new FilterClause(new FieldItemBaseInfo("OrderId"), ConditionOperator.Eq, Id);

            retVal = _availableSlabs.GetItemsFromRepository(clause: filterClause);


            return retVal;
        }


        public static bool CreateOrderFromOffer(OfferEntity offer, out WkOrderEntity newOrder)
        {
            bool retVal = false;
            newOrder = null;

            XmlDocument originalDoc = new XmlDocument();
            bool find = false;

            try
            {

                var tempOrder = new WkOrderEntity()
                {
                    Scope = offer.Scope
                };


                if (offer.OfferStatus == OfferStatus.ToAnalyze)
                {
                    originalDoc.Load(offer.XmlFilepath);

                    if (DeleteNodes(ref originalDoc))
                        originalDoc.Save(offer.XmlFilepath);

                    // Creazione ordine eseguita con successo

                    if (!tempOrder.ReadFileXml(offer.XmlFilepath))
                        return false;

                    // Genero il codice ordine modificando il prefisso del codice offerta                   
                    tempOrder.OrderCode = "ORD" + offer.Code.Substring(3);
                    tempOrder.Description = offer.Description;
                    tempOrder.OrderStatus = WkOrderStatus.ORDER_IN_PROGRESS; //Order.State.ORDER_LOADED.ToString();	// Stato ordine di default
                    tempOrder.Date = DateTime.Now;
                    tempOrder.OfferId = offer.Id;


                    if (tempOrder.SaveToRepository())
                    {
                        newOrder = tempOrder;


                        retVal = true;

                    }
                    else
                    {
                        TraceLog.WriteLine(string.Format("Error on order save, order <{0}>.", tempOrder.OrderCode));

                    }
                }



            }
            catch (Exception ex)
            {
                TraceLog.WriteLine( "CreateOrderFromOffer error : ", ex);


                retVal = false;
            }
            finally
            {
                originalDoc = null;
            }

            return retVal;
        }

        public override bool SaveToRepository(PersistenceProviderType providerType = PersistenceProviderType.SqlServer)
        {
            bool retVal =  base.SaveToRepository(providerType);

            if (retVal)
            {
                foreach (var project in _projects)
                {
                    project.OrderId = Id;
                    retVal = project.SaveToRepository();

                    if (!retVal)
                        break;
                }
            }

            return retVal;
        }

        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        /// <summary>
        /// Cancella tutti i nodi che non interessano a questo livello
        /// </summary>
        /// <param name="doc">xml document da modificare</param>
        /// <returns></returns>
        private static bool DeleteNodes(ref XmlDocument doc)
        {
            ArrayList nodes = new ArrayList();
            XmlNode node = doc.SelectSingleNode("/EXPORT");

            try
            {
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (nd.Name.ToUpperInvariant() != "FABMASTER")
                        nodes.Add(nd);
                }

                node.RemoveAll();
                for (int i = 0; i < nodes.Count; i++)
                    node.AppendChild((XmlNode)nodes[i]);
                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("OrderEntity.DeleteNodes Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("OrderEntity.DeleteNodes Error. ", ex);
                return false;
            }
        }


        public bool ReadFileXml(string xmlFilepath)
        {
            bool retVal = false;

            XmlDocument doc = new XmlDocument();
            XmlDocument originalDoc = new XmlDocument();

            bool exist;

            WkProjectEntity prj;
            int qty;
            string idProject;

            _projects.Scope = Scope;

            if (!Directory.Exists(mTempDir))
                Directory.CreateDirectory(mTempDir);

            // Nome file xml parziale da salvare in seguito nel database
            string tmpXmlFile = Path.Combine(mTempDir, XML_FILE_NAME);

            var msg = WkProjectEntity.Message.NONE;

            try
            {
                // Legge il file XML
                doc.Load(xmlFilepath);

                XmlNode chn = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

                //// Imposto il codice dell'ordine
                if (chn.Attributes.GetNamedItem("F_CODE") != null)
                    OrderCode = chn.Attributes.GetNamedItem("F_CODE").Value;

                //// Imposto la data di consegna dell'ordine
                if (chn.Attributes.GetNamedItem("Terms_of_delivery") != null)
                    DeliveryDate = DateTime.Parse(chn.Attributes.GetNamedItem("Terms_of_delivery").Value);

                //if (chn.Attributes.GetNamedItem("Code") != null)
                if (chn.Attributes.GetNamedItem("Code").Value.Length > 50)
                    Description = chn.Attributes.GetNamedItem("Code").Value.Substring(0, 50);
                else
                    Description = chn.Attributes.GetNamedItem("Code").Value;

                //if (chn.Attributes.GetNamedItem("Number") != null)
                if (chn.Attributes.GetNamedItem("Number").Value.Length > 50)
                    WorkNumber = chn.Attributes.GetNamedItem("Number").Value.Substring(0, 50);
                else
                    WorkNumber = chn.Attributes.GetNamedItem("Number").Value;

                if (chn.Attributes.GetNamedItem("Adjustement_index") != null)
                    Revision = int.Parse(chn.Attributes.GetNamedItem("Adjustement_index").Value);


                foreach (XmlNode nd in chn.ChildNodes)
                {
                    if (nd.Name == "Customer")
                    {
                        //Imposta il codice del cliente all'ordine
                        if (nd.Attributes.GetNamedItem("F_CODE").Value != null && nd.Attributes.GetNamedItem("F_CODE").Value != "0")
                            this.CustomerName = nd.Attributes.GetNamedItem("F_CODE").Value.Trim();
                        else
                        {
                            //if (mCollectionDbInterface != null)
                            //{
                            //    msg = Project.Message.NO_CUSTOMER;
                            //    throw new ApplicationException("No associate customer");
                            //}
                            //else
                                this.CustomerName = "void";
                        }

                        //if (mCollectionDbInterface != null)
                        //{
                        //    OrderCollection orders = new OrderCollection(mCollectionDbInterface, null);

                        //    orders.CheckExistingCode(nd.Attributes.GetNamedItem("F_CODE").Value, "T_AN_CUSTOMERS", out exist);
                        //    if (!exist)	// Inserisce un nuovo cliente
                        //    {
                        //        if (!NewCustomer(nd, mCollectionDbInterface)) return false;
                        //    }
                        //    else		// Aggiorna i dati del cliente
                        //    {
                        //        if (!UpdateCustomer(nd, mCollectionDbInterface)) return false;
                        //    }
                        //}
                    }
                    if (nd.Name == "Top")
                    {
                        qty = int.Parse(nd.Attributes.GetNamedItem("Quantity").Value);	// Quanti sono i progetti da caricare di questo tipo

                        if (!string.IsNullOrEmpty(tmpXmlFile))
                        {
                            originalDoc.Load(xmlFilepath);
                            idProject = nd.Attributes.GetNamedItem("Id_Top").Value;
                            if (DeleteAllProjectNodes_Except(ref originalDoc, idProject))
                                originalDoc.Save(tmpXmlFile);
                        }

                        for (int i = 0; i < qty; i++)
                        {
                            prj = new WkProjectEntity()
                            {
                                Scope = Scope
                            };

                            //prj.TempDir = mTempDir;
                            prj.ProjectStatus = WkProjectStatus.PROJ_LOADED;					    // Stato progetto di default
                            prj.OriginalProjectCode = nd.Attributes.GetNamedItem("Id_Top").Value;      // Codice del progetto nel file xml
                            //prj.gDeliveryDate = mDeliveryDate;
                            if (nd.Attributes.GetNamedItem("Code").Value.Length > 50)
                                prj.Description = nd.Attributes.GetNamedItem("Code").Value.Substring(0, 50);			// Descrizione
                            else
                                prj.Description = nd.Attributes.GetNamedItem("Code").Value;		    // Descrizione

                            if (!prj.ReadXmlNode(tmpXmlFile, nd, out msg))// Legge il progetto per caricare tutti i pezzi
                                throw new ApplicationException("Error creating Project");
                            
                            _projects.Add(prj);

                            if (nd.Attributes.GetNamedItem("Adjustement_index") != null)
                                prj.Revision = int.Parse(nd.Attributes.GetNamedItem("Adjustement_index").Value);   // Revisione
                        }
                    }
                }

                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("Order.ReadFileXml Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Order.ReadFileXml Error. ", ex);
                return false;
            }
            finally
            {
                doc = null;
                originalDoc = null;
            }
        }

        //**********************************************************************
        // DeleteAllProjectNodes_Except
        // Cancella tutti i nodi progetto che non hanno l'id interessato
        // Parametri:
        //			doc		: xml document da modificare
        //			idPrj	: id del progetto da non eliminare
        //**********************************************************************
        private bool DeleteAllProjectNodes_Except(ref XmlDocument doc, string idPrj)
        {
            ArrayList nodes = new ArrayList();
            XmlNode node = doc.SelectSingleNode("/EXPORT/CAM/Version/Project/Offer");

            try
            {
                foreach (XmlNode nd in node.ChildNodes)
                {
                    if (nd.Name != "Top" || nd.Attributes.GetNamedItem("Id_Top").Value != idPrj)
                        nodes.Add(nd);
                }

                for (int i = 0; i < nodes.Count; i++)
                {
                    node.RemoveChild((XmlNode)nodes[i]);
                }

                return true;
            }
            catch (XmlException ex)
            {
                TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
                return false;
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("Order.DeleteAllProjectNodes_Except Error. ", ex);
                return false;
            }
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
