﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;

namespace SlabOperate.Business.Entities
{
    [MainDbTable(PersistenceProviderType.SqlServer, "T_AN_SLAB_IMAGES")]
    public class SlabImageEntity : BasePersistableEntity
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private int _id = -1;

        private string _code = string.Empty;

        private string _imagePath = string.Empty;

        private string _imageThumbnailPath = string.Empty;

        private static object _lockObj = new object();

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public SlabImageEntity()
        {
            //PropertyStatusChanged += ArticleImage_PropertyStatusChanged;
        }

        void ArticleImage_PropertyStatusChanged(object sender, Wpf.Common.EventArgs<FieldItemInfo> e)
        {
            if (e.Value.BaseInfo.PropertyName == "ImagePath")
            {
                if (e.Value.Status == FieldStatus.Set)
                {
                    BuildThumbnail(e.Value.Status);
                }
            }
        }

        private void BuildThumbnail(FieldStatus status)
        {
            string fieldName = "ImageThumbnailPath";
            string filepath = string.Format("TH_{0}.JPG", (Code != "" ? Code : Id.ToString("0000000000")));

            filepath = Path.Combine(ClassTempFolder, filepath);

            lock (_lockObj)
            {
                if (!File.Exists(filepath))
                {
                    using (var bmpTemp = new Bitmap(ImagePath))
                    {

                        var thumbnail = new Bitmap(bmpTemp, new System.Drawing.Size(300, 200));
                        thumbnail.Save(filepath);
                    }
                }
            }
            SetProperty(fieldName, filepath, true, status);

        }


        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public override int UniqueId
        {
            get { return Id; }
        }


        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_ID", true)]
        public int Id
        {
            get { return GetProperty("Id", ref _id); }
            set { SetProperty("Id", value, ref _id); }
        }

        [PersistableField(FieldRetrieveMode.Immediate)]
        [DbField(PersistenceProviderType.SqlServer, "F_CODE", true)]
        public string Code
        {
            get { return GetProperty("Code", ref _code); }
            set { SetProperty("Code", value, ref _code); }
        }

        [PersistableField(FieldRetrieveMode.Deferred, FieldManagementType.ToFilepath, "IMG_", "PNG")]
        [DbField(PersistenceProviderType.SqlServer, "F_IMAGE", false, "", "", true)]
        public string ImagePath
        {
            get
            {
                return GetProperty("ImagePath", ref _imagePath);
            }
            set
            {
                SetProperty("ImagePath", value, ref _imagePath);
            }
        }

        public string ImageThumbnailPath
        {
            get
            {
                if (string.IsNullOrEmpty(_imageThumbnailPath))
                {
                    if (string.IsNullOrEmpty(_imagePath))
                    {
                        var dummy = ImagePath;
                    }
                    else
                    {
                        FieldStatus status;
                        GetPropertyStatus("ImagePath", out status);
                        BuildThumbnail(status);
                    }
                }
                return GetProperty("ImageThumbnailPath", ref _imageThumbnailPath);
            }
            set
            {
                SetProperty("ImageThumbnailPath", value, ref _imageThumbnailPath);
            }
        }


        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
