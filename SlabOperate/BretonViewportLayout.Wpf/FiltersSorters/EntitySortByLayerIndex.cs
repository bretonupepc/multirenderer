﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using devDept.Eyeshot.Entities;

namespace BretonViewportLayout
{
    public class EntitySortByLayerIndex: IComparer<Entity>
    {
        #region IComparer<Entity> Members

        public int Compare(Entity x, Entity y)
        {
            if (x.LayerIndex > y.LayerIndex) return 1;
            else if (x.LayerIndex < y.LayerIndex) return -1;
            else return 0;
        }

        #endregion
    }

    public class EntitySortByLayerIndexDescending : IComparer<Entity>
    {
        #region IComparer<Entity> Members

        public int Compare(Entity x, Entity y)
        {
            if (x.LayerIndex > y.LayerIndex) return -1;
            else if (x.LayerIndex < y.LayerIndex) return 1;
            else return 0;
        }

        #endregion
    }

}
