using System.Drawing;
using BretonViewportLayout.Wpf;
using devDept.Geometry;
using devDept.Eyeshot.Entities;
using devDept.Graphics;

namespace Breton.DesignCenterInterface.Wpf
{
    /// <summary>
    /// Contains all methods required to draw different entities interactively
    /// </summary>
    partial class DraftingViewportLayout
    {

        private void Draw(ICurve theCurve, Color color = default(Color), bool drawBorder = true, bool fill = false, bool dottedLines = false, float lineThickness = 1)
        {
            if (theCurve is CompositeCurve)
            {
                CompositeCurve compositeCurve = theCurve as CompositeCurve;
                Entity[] explodedCurves = compositeCurve.Explode();
                foreach (Entity ent in explodedCurves)

                    DrawScreenCurve((ICurve)ent, color, drawBorder, fill, dottedLines, lineThickness);                   
            }
            else
            {
                DrawScreenCurve(theCurve, color, drawBorder, fill, dottedLines, lineThickness);    
            }
        }

        private void DrawScreenCurve(ICurve curve, Color color, bool drawBorder = true, bool fill = false, bool dottedBorder = false, float pixelThickness = 1)
        {
            const int subd = 100;

            var originalColor = renderContext.CurrentWireColor;

            var originalThickness = renderContext.CurrentLineWidth;

            renderContext.SetLineSize(pixelThickness); 


            if (color == default(Color))
            {
                color = DrawingColor;
            }

            Point3D[] pts = new Point3D[subd + 1];

            for (int i = 0; i <= subd; i++)
            {
                pts[i] = WorldToScreen(curve.PointAt(curve.Domain.ParameterAt((double) i/subd)));
            }

            if (fill)
            {
                renderContext.SetState(blendStateType.Blend);
                renderContext.SetColorWireframe(Color.FromArgb(40, color));
                renderContext.SetState(rasterizerStateType.CCW_PolygonFill_CullFaceBack_NoPolygonOffset);
                renderContext.DrawTrianglesFan(pts, new Vector3D(0, 0, 1));
                renderContext.SetState(blendStateType.NoBlend);
            }
            if (drawBorder)
            {
                renderContext.SetColorWireframe(color);
                if (dottedBorder)
                {
                    renderContext.SetLineStipple(1, 0x0F0F, Viewports[0].Camera);
                    renderContext.EnableLineStipple(true);
                }

                renderContext.DrawLineStrip(pts);

                if (dottedBorder)
                {
                    renderContext.EnableLineStipple(false);
                }


                renderContext.SetLineSize(originalThickness); 

                renderContext.SetColorWireframe(originalColor);
            }
        }
        

        // Draws a plus sign (+) at current mouse location
        private void DrawPositionMark(Point3D current)
        {
            if (current == null)
                return;

            double crossSide = (CursorMode == GraphicCursorMode.Cross) ? CursorCrossSize : CursorCrossSize + 20;

            double finderSize = _cursorFinderSize;

            Color original = renderContext.CurrentWireColor;

            //renderContext.SetColorWireframe(_cursorColor);

            Point3D currentScreen = WorldToScreen(current);

            SetXOR(true);

            // Compute the direction on screen of the horizontal line
            Point2D left = WorldToScreen(current.X - 1, current.Y, 0);
            Vector2D dirHorizontal = Vector2D.Subtract(left, currentScreen);
            dirHorizontal.Normalize();

            // Compute the position on screen of the line endpoints
            left = currentScreen + dirHorizontal*crossSide ;
            Point2D right = currentScreen - dirHorizontal * crossSide;

            

            renderContext.DrawLine(left, right);


            // Compute the direction on screen of the vertical line
            Point2D bottom = WorldToScreen(current.X, current.Y - 1, 0);
            Vector2D dirVertical = Vector2D.Subtract(bottom, currentScreen);
            dirVertical.Normalize();

            // Compute the position on screen of the line endpoints
            bottom = currentScreen + dirVertical * crossSide;
            Point2D top = currentScreen - dirVertical * crossSide;

            renderContext.DrawLine(bottom, top);

            // Internal square horizontal segments

            if (CursorMode == GraphicCursorMode.ViewFinder)
            {

                Point2D leftTop = new Point2D(currentScreen.X - finderSize, currentScreen.Y - finderSize);
                Point2D rightTop = new Point2D(currentScreen.X + finderSize, currentScreen.Y - finderSize);

                Point2D rightBottom = new Point2D(currentScreen.X + finderSize, currentScreen.Y + finderSize);
                Point2D leftBottom = new Point2D(currentScreen.X - finderSize, currentScreen.Y + finderSize);

                renderContext.DrawLine(leftTop, rightTop);
                renderContext.DrawLine(rightTop, rightBottom);
                renderContext.DrawLine(rightBottom, leftBottom);
                renderContext.DrawLine(leftBottom, leftTop);
            }

            SetXOR(false);

            renderContext.SetColorWireframe(original);

            renderContext.SetLineSize(1);
        }

    }

}