using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Globalization;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using BretonViewportLayout;
using BretonViewportLayout.Common;
using BretonViewportLayout.Wpf;
using devDept.Eyeshot.Translators;
using OpenGL;
using devDept.Eyeshot;
using devDept.Graphics;
using devDept.Geometry;
using devDept.Eyeshot.Entities;
using TraceLoggers;
using Cursor = System.Windows.Input.Cursor;
using Cursors = System.Windows.Input.Cursors;
using DragEventArgs = System.Windows.DragEventArgs;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;
using MouseEventArgs = System.Windows.Input.MouseEventArgs;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace Breton.DesignCenterInterface.Wpf
{
    public enum ObjectSnapType
    {
        Point,
        End,
        Mid,
        Center,
        Near
    }

    public enum DimensionMode
    {
        Aligned,
        Horizontal,
        Vertical
    }

    /// <summary>
    /// This is ViewportLayout which will extend behaviour required for a drafting application.
    /// </summary>
    public partial class DraftingViewportLayout : devDept.Eyeshot.ViewportLayout
    {
        public bool IsMouseDown = false;
        public bool IsMouseDownAndUp = false;


        #region >-------------- Constants and Enums

        private const int magnetRange = 3;

        private const string SERIAL_NUMBER = "PROWPF-0612-VGS0V-UNV3T-4HMMS";


        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private bool _useTouchLayout = false;

        private bool cursorOutside;

        SnapPoint _lastSnapPoint;

        // Active layer index

        // Always draw on XY plane, view is alwyas topview
        private Plane plane = Plane.XY;

        // Current selection/position
        private Point3D _currentWorldPoint;

        private Point3D _currentOrthoPoint;


        // Current mouse position
        private System.Drawing.Point mouseLocation;

        
        private ColorPalette _palette = new ColorPalette();

        private List<EyeSelection> _selections = new List<EyeSelection>();

        private Layer _activeLayer = null;

        private Color _activeColor;

        private bool _displayMouseCoordinates = true;

        private string _mouseTooltip = string.Empty;
        private bool _displayMouseTooltip = true;


        private TemporaryEntity _temporaryCircle = new TemporaryEntity(TemporaryEntityType.Circle, new Point2D(), 0);
        private TemporaryEntity _temporaryArc = new TemporaryEntity(TemporaryEntityType.Arc, new Point2D(), new Point2D(), new Point2D());
        private TemporaryEntity _temporaryRectangle = new TemporaryEntity(TemporaryEntityType.Rectangle, new Point2D(), new Point2D());
        private TemporaryEntity _temporaryLine = new TemporaryEntity(TemporaryEntityType.Line, new Point2D(), new Point2D());
        private TemporaryEntity _moveRotateRubberbandLine = new TemporaryEntity(TemporaryEntityType.Line, new Point2D(), new Point2D());
        private List<TemporaryEntity> _temporaryResizingSymbols = new List<TemporaryEntity>();

        private bool _orthoMode = false;

        private bool _orthoModeStarted = false;

        private System.Drawing.Point _downMouseLocation = new System.Drawing.Point(0, 0);

        private System.Drawing.Point _moveMouseLocation = new System.Drawing.Point(0, 0);

        private System.Drawing.Point _moveOrthoMouseLocation = new System.Drawing.Point(0, 0);

        private bool _locked = false;

        private bool _activateOnEnter = true;

        private Color _cursorColor = Color.Black;

        private Color _cursorCoordinatesColor = Color.Black;

        private Color _cursorTipsColor = Color.Black;

        private GraphicCursorMode _cursorMode = GraphicCursorMode.ViewFinder;

        private int _cursorCrossSize = 30;

        private double _cursorFinderSize = 10;

        private EntitiesSortByLayer _sortEntities = EntitiesSortByLayer.None;

        private Cursor _panCursor = null;

        private string _lastSaveFolder = string.Empty;

        private List<Entity> _dragEntityList = new List<Entity>();

        private bool _dragging = false;

        private Point3D _startDraggingPoint;

        private System.Drawing.Point _moveDragMouseLocation;

        private bool _showDrag = false;


        #region draw dimension

        private Point3D _dimensionPoint1;
        private Point3D _dimensionPoint2;

        private bool _drawingDimension = false;
        private DimensionMode _dimensionMode;

        private double _dimensionTextHeight = 40;

        #endregion


        private bool _showLocker = false;

        private Bitmap _transparentLocker;

        private Cursor _voidCursor = null;

        private Cursor _defaultCursor = null;


        #endregion Private Fields --------<

        #region >-------------- Constructors

        public DraftingViewportLayout() : base()
        {
            Unlock(SERIAL_NUMBER);
            _defaultCursor = Cursors.Arrow;
            ActiveLayer = Layers[0];
            ActiveColor = Palette[0];
            _moveRotateRubberbandLine.DrawColor = Color.WhiteSmoke;

            SetupLocker();
        }

        private void SetupLocker()
        {
            if ( Properties.Resources.Locker != null)
                _transparentLocker = (Bitmap)SetImageOpacity(Properties.Resources.Locker, 0.8F);

        }

        private Image SetImageOpacity(Image image, float opacity)
        {
            try
            {
                //create a Bitmap the size of the image provided  
                Bitmap bmp = new Bitmap(image.Width, image.Height);

                //create a graphics object from the image  
                using (Graphics gfx = Graphics.FromImage(bmp))
                {

                    //create a color matrix object  
                    ColorMatrix matrix = new ColorMatrix();

                    //set the opacity  
                    matrix.Matrix33 = opacity;

                    //create image attributes  
                    ImageAttributes attributes = new ImageAttributes();

                    //set the color(opacity) of the image  
                    attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                    //now draw the image  
                    gfx.DrawImage(image, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, attributes);
                }
                return bmp;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                return null;
            }
        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields


        public Color DrawingColor = Color.WhiteSmoke;

        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public ColorPalette Palette
        {
            get { return _palette; }
        }

        [Browsable(false)]
        public List<EyeSelection> Selections
        {
            get { return _selections; }
        }

        [Browsable(false)]
        public Layer ActiveLayer
        {
            get
            {
                return _activeLayer ?? Layers[0];
            }
            set { _activeLayer = value; }
        }

        public Color ActiveColor
        {
            get { return _activeColor; }
            set { _activeColor = value; }
        }

        public System.Drawing.Point MouseLocation
        {
            get { return RenderContextUtility.ConvertPoint(GetMousePosition(null)); }
        }

        public double ViewSize
        {
            get
            {
                Point3D topLeftOnPlane, bottomLeftOnPlane;
                System.Drawing.Point topLeft = new System.Drawing.Point(0, 0);
                System.Drawing.Point bottomLeft = new System.Drawing.Point(0, Viewports[0].Size.Height);
                ScreenToPlane(topLeft, plane, out topLeftOnPlane);
                ScreenToPlane(bottomLeft, plane, out bottomLeftOnPlane);

                double viewHeight = Point3D.Distance(topLeftOnPlane, bottomLeftOnPlane);
                return viewHeight;

            }
        }

        public TemporaryEntity TemporaryCircle
        {
            get { return _temporaryCircle; }
        }

        public TemporaryEntity TemporaryRectangle
        {
            get { return _temporaryRectangle; }
        }

        public TemporaryEntity TemporaryLine
        {
            get { return _temporaryLine; }
        }

        public TemporaryEntity MoveRotateRubberbandLine
        {
            get { return _moveRotateRubberbandLine; }
        }

        public bool OrthoMode
        {
            get { return _orthoMode; }
            set
            {
                _orthoMode = value;
            }
        }

        public Point DownMouseLocation
        {
            get { return _downMouseLocation; }
        }

        public Point MoveMouseLocation
        {
            get { return _moveMouseLocation; }
        }

        public Point MoveOrthoMouseLocation
        {
            get { return _moveOrthoMouseLocation; }
            private set
            {
                _moveOrthoMouseLocation = value;
                //ScreenToPlane(_moveOrthoMouseLocation, plane, out _)
            }
        }

        public Breton.Polygons.Point DownWorldPosition
        {
            get
            {
                Point3D p;
                ScreenToPlane(_downMouseLocation, plane, out p);
                return new Polygons.Point(p.X, p.Y);
            }
        }

        private Point3D MoveDragWorldPosition
        {
            get
            {
                Point3D p;
                ScreenToPlane(MoveDragMouseLocation, plane, out p);
                return p;
            }
        }


        public Breton.Polygons.Point MoveWorldPosition
        {
            get
            {
                Point3D p;
                ScreenToPlane(_moveMouseLocation, plane, out p);
                return new Polygons.Point(p.X, p.Y);
            }
        }

        public Breton.Polygons.Point MoveOrthoWorldPosition
        {
            get
            {
                Point3D p;
                ScreenToPlane(_moveOrthoMouseLocation, plane, out p);
                return new Polygons.Point(p.X, p.Y);
            }
        }

        public string MouseTooltip
        {
            get { return _mouseTooltip; }
            set { _mouseTooltip = value; }
        }

        public bool DisplayMouseTooltip
        {
            get { return _displayMouseTooltip; }
            set { _displayMouseTooltip = value; }
        }

        public bool DisplayMouseCoordinates
        {
            get { return _displayMouseCoordinates; }
            set { _displayMouseCoordinates = value; }
        }

        public Point3D CurrentWorldPoint
        {
            get { return _currentWorldPoint; }
        }

        public Point3D CurrentOrthoPoint
        {
            get { return _currentOrthoPoint; }
        }

        public bool Locked
        {
            get { return _locked; }
            set
            {
                _locked = value;
                IsEnabled = !value;
            }
        }

        public bool ActivateOnEnter
        {
            get { return _activateOnEnter; }
            set { _activateOnEnter = value; }
        }

        public Color CursorColor
        {
            get { return _cursorColor; }
            set { _cursorColor = value; }
        }

        public Color CursorCoordinatesColor
        {
            get { return _cursorCoordinatesColor; }
            set { _cursorCoordinatesColor = value; }
        }

        public Color CursorTipsColor
        {
            get { return _cursorTipsColor; }
            set { _cursorTipsColor = value; }
        }

        public GraphicCursorMode CursorMode
        {
            get { return _cursorMode; }
            set
            {
                
                _cursorMode = value;
                _cursorFinderSize = (double)PickBoxSize / 2;
                SNAP_SYMBOL_SIZE = (int)PickBoxSize;
            }
        }

        public new Cursor Cursor
        {
            get
            {
                return base.Cursor;
            }
            set
            {

                if (ActionMode == actionType.Pan && _panCursor != null)
                    value = _panCursor;
                else if (value == Cursors.Arrow && _voidCursor != null)
                {
                    value = _voidCursor;
                }
                base.Cursor = value;
            }
        }



        public EntitiesSortByLayer SortEntities
        {
            get { return _sortEntities; }
            set { _sortEntities = value; }
        }


        public int CursorCrossSize
        {
            get { return _cursorCrossSize; }
            set { _cursorCrossSize = value; }
        }

        public TemporaryEntity TemporaryArc
        {
            get { return _temporaryArc; }
        }

        public Cursor PanCursor
        {
            get { return _panCursor; }
            set { _panCursor = value; }
        }

        public List<Entity> DragEntityList
        {
            get { return _dragEntityList; }
            set { _dragEntityList = value; }
        }

        public bool Dragging
        {
            get { return _dragging; }
            set { _dragging = value; }
        }

        public Point3D StartDraggingPoint
        {
            get { return _startDraggingPoint; }
            set { _startDraggingPoint = value; }
        }

        public Point3D DimensionPoint1
        {
            get { return _dimensionPoint1; }
            set { _dimensionPoint1 = value; }
        }

        public Point3D DimensionPoint2
        {
            get { return _dimensionPoint2; }
            set { _dimensionPoint2 = value; }
        }

        public bool DrawingDimension
        {
            get { return _drawingDimension; }
            set { _drawingDimension = value; }
        }

        public DimensionMode DimensionMode
        {
            get { return _dimensionMode; }
            set { _dimensionMode = value; }
        }

        public double DimensionTextHeight
        {
            get { return _dimensionTextHeight; }
            set { _dimensionTextHeight = value; }
        }

        public bool OrthoModeStarted
        {
            get { return _orthoModeStarted; }
            set { _orthoModeStarted = value; }
        }

        public Point MoveDragMouseLocation
        {
            get { return _moveDragMouseLocation; }
            set { _moveDragMouseLocation = value; }
        }

        public bool ShowDrag
        {
            get { return _showDrag; }
            set { _showDrag = value; }
        }

        public bool ShowLocker
        {
            get { return _showLocker; }
            set { _showLocker = value; }
        }

        public Cursor VoidCursor
        {
            get { return _voidCursor; }
            set { _voidCursor = value; }
        }

        public bool UseTouchLayout
        {
            get { return _useTouchLayout; }
            set { _useTouchLayout = value; }
        }

        public List<TemporaryEntity> TemporaryResizingSymbols
        {
            get { return _temporaryResizingSymbols; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public void SetDefaultCursor()
        {
            if (_defaultCursor != null)
                base.Cursor = _defaultCursor;
        }


        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        void SetXOR(bool enable)
        {          

            if (Renderer == rendererType.OpenGL)
            {
                // white color
                //renderContext.SetColorWireframe(Color.White);
                //renderContext.SetColorWireframe(Color.White);

                if (enable)
                {
                    gl.Enable(gl.COLOR_LOGIC_OP);
                    gl.LogicOp(gl.XOR);
                }
                else
                    gl.Disable(gl.COLOR_LOGIC_OP);
            }
            else
            {
                // Direct3D
                renderContext.SetColorWireframe(DrawingColor);
            }            
        }


        private void DrawMouseTooltip()
        {
            //SetXOR(true);

            if (OrthoMode && CurrentOrthoPoint != null)
            {
                DrawText(_moveOrthoMouseLocation.X + 10, (int)Height - _moveOrthoMouseLocation.Y - 10,
                    _mouseTooltip,
                    new Font("Tahoma", 8.25f),
                    _cursorTipsColor, ContentAlignment.TopLeft);
            }
            else
            {
                DrawText(mouseLocation.X + 10, (int)Height - mouseLocation.Y - 10,
                    _mouseTooltip,
                    new Font("Tahoma", 8.25f),
                    _cursorTipsColor, Color.WhiteSmoke, ContentAlignment.TopLeft);
            }

            //SetXOR(false);

        }

        private void DrawMouseCoordinates()
        {
            //SetXOR(true);
            if (OrthoMode && CurrentOrthoPoint != null)
            {
                DrawText(_moveOrthoMouseLocation.X + 10, (int)Height - _moveOrthoMouseLocation.Y + 10,
                    "X = " + CurrentOrthoPoint.X.ToString("f2", CultureInfo.InvariantCulture) + ", " +
                    "Y = " + CurrentOrthoPoint.Y.ToString("f2", CultureInfo.InvariantCulture),
                    new Font("Tahoma", 8.25f),
                    _cursorCoordinatesColor, ContentAlignment.BottomLeft);
            }
            else
            {
                DrawText(mouseLocation.X + 10, (int)Height - mouseLocation.Y + 10,
                    "X = " + CurrentWorldPoint.X.ToString("f2", CultureInfo.InvariantCulture) + ", " +
                    "Y = " + CurrentWorldPoint.Y.ToString("f2", CultureInfo.InvariantCulture),
                    new Font("Tahoma", 8.25f),
                    _cursorCoordinatesColor, ContentAlignment.BottomLeft);
            }
            SetXOR(false);

        }

        private void DrawResizerSymbol(Point2D onMapPoint, Color color)
        {
            Point2D point = WorldToScreen(new Point3D(onMapPoint.X, onMapPoint.Y));

            System.Drawing.Point onScreen = new System.Drawing.Point((int)point.X, (int)(point.Y));

            double dim1 = onScreen.X + (RESIZER_SYMBOL_SIZE / 2);
            double dim2 = onScreen.Y + (RESIZER_SYMBOL_SIZE / 2);
            double dim3 = onScreen.X - (RESIZER_SYMBOL_SIZE / 2);
            double dim4 = onScreen.Y - (RESIZER_SYMBOL_SIZE / 2);

            Point3D topLeftVertex = new Point3D(dim3, dim2);
            Point3D topRightVertex = new Point3D(dim1, dim2);
            Point3D bottomRightVertex = new Point3D(dim1, dim4);
            Point3D bottomLeftVertex = new Point3D(dim3, dim4);

            var currentColor = renderContext.CurrentWireColor;
            renderContext.SetColorWireframe(color);

            renderContext.DrawLineLoop(new Point3D[]
            {
                bottomLeftVertex,
                bottomRightVertex,
                topRightVertex,
                topLeftVertex
            });

            renderContext.SetColorWireframe(currentColor);
        }


        private void DrawTemporaryEntities()
        {
            if (_temporaryRectangle.DrawEntity)
            {
                DrawGenericRectangle(_temporaryRectangle.StartPoint, _temporaryRectangle.EndPoint,
                    _temporaryRectangle.Fill, _temporaryRectangle.DrawColor,
                    _temporaryRectangle.DrawBorder, _temporaryRectangle.DottedBorder);
            }

            foreach (var item in _temporaryResizingSymbols)
            {
                //DrawGenericRectangle(item.StartPoint, item.EndPoint,
                //    item.Fill, item.DrawColor,
                //    item.DrawBorder, item.DottedBorder);
                DrawResizerSymbol(item.OriginPoint, item.DrawColor);
            }

            if (_temporaryCircle.DrawEntity)
            {
                DrawGenericCircle(_temporaryCircle.StartPoint, _temporaryCircle.Radius,
                    _temporaryCircle.Fill, _temporaryCircle.DrawColor,
                    _temporaryCircle.DrawBorder, _temporaryCircle.DottedBorder);
            }
            if (_temporaryLine.DrawEntity)
            {
                DrawGenericLine(_temporaryLine.StartPoint, _temporaryLine.EndPoint,
                    _temporaryLine.DrawColor, _temporaryLine.DottedBorder);
            }

            if (_temporaryArc.DrawEntity)
            {
                DrawGenericArc(_temporaryArc.ArcCenter, _temporaryArc.StartPoint, _temporaryArc.EndPoint,
                    false, _temporaryArc.DrawColor, true, _temporaryArc.DottedBorder);
            }

            if (_moveRotateRubberbandLine.DrawEntity)
            {
                DrawGenericLine(_moveRotateRubberbandLine.StartPoint, _moveRotateRubberbandLine.EndPoint,
                    _moveRotateRubberbandLine.DrawColor, _moveRotateRubberbandLine.DottedBorder);
            }
        }

        private void DrawGenericRectangle(Point2D startPoint, Point2D endPoint, bool fill, Color color, bool drawBorder = true, bool dottedBorder = false)
        {
            Point3D p1 = WorldToScreen(startPoint.X, startPoint.Y, 0);
            Point3D p2 = WorldToScreen(endPoint.X, endPoint.Y, 0);
            System.Drawing.Point p1Screen = new System.Drawing.Point((int)p1.X, (int)p1.Y);
            System.Drawing.Point p2Screen = new System.Drawing.Point((int)p2.X, (int)p2.Y);

            DrawSelectionBox(p1Screen, p2Screen, color, fill,drawBorder, dottedBorder);
        }

        private void DrawGenericCircle(Point2D startPoint, double circleRadius, bool fill, Color color, bool drawBorder = true, bool dottedBorder = false)
        {
            Circle tempCircle = new Circle(plane, startPoint, circleRadius);
            Draw(tempCircle,color, drawBorder, fill, dottedBorder);


        }

        private void DrawGenericArc(Point2D center, Point2D startPoint, Point2D endPoint, bool fill, Color color, bool drawBorder = true, bool dottedBorder = false)
        {
            if (startPoint.DistanceTo(endPoint) < 1e-2)
                return;

            Arc tempArc = new Arc( plane, center, startPoint, endPoint);

            if (tempArc.AngleInRadians > Math.PI)
            {
                tempArc = new Arc(plane, center, endPoint, startPoint);
            }

            Draw(tempArc, color, drawBorder, fill, dottedBorder);


        }

        private void DrawGenericLine(Point2D startPoint, Point2D endPoint, Color color, bool dottedBorder = false)
        {


            Line tempLine = new Line(startPoint.X, startPoint.Y, endPoint.X, endPoint.Y);


            Draw(tempLine,color, true, false, dottedBorder);


        }

        private static void NormalizeBox(ref System.Drawing.Point p1, ref System.Drawing.Point p2)
        {

            int firstX = Math.Min(p1.X, p2.X);
            int firstY = Math.Min(p1.Y, p2.Y);
            int secondX = Math.Max(p1.X, p2.X);
            int secondY = Math.Max(p1.Y, p2.Y);

            p1.X = firstX;
            p1.Y = firstY;
            p2.X = secondX;
            p2.Y = secondY;

        }

        private  void DrawSelectionBox(System.Drawing.Point p1, System.Drawing.Point p2, Color transparentColor, bool drawFill, bool drawBorder, bool dottedBorder)
        {
            //p1.Y = Height - p1.Y;
            //p2.Y = Height - p2.Y;

            var originalColor = renderContext.CurrentWireColor;

            NormalizeBox(ref p1, ref p2);

            // Adjust the bounds so that it doesn't go outside the current viewport frame
            int[] viewFrame = Viewports[ActiveViewport].GetViewFrame();
            int left = viewFrame[0];
            int top = viewFrame[1] + viewFrame[3];
            int right = left + viewFrame[2];
            int bottom = viewFrame[1];

            if (p2.X > right - 1)
                p2.X = right - 1;

            if (p2.Y > top - 1)
                p2.Y = top - 1;

            if (p1.X < left + 1)
                p1.X = left + 1;

            if (p1.Y < bottom + 1)
                p1.Y = bottom + 1;

            if (drawFill)
            {
                renderContext.SetState(blendStateType.Blend);
                renderContext.SetColorWireframe(Color.FromArgb(40, transparentColor));
                renderContext.SetState(rasterizerStateType.CCW_PolygonFill_CullFaceBack_NoPolygonOffset);

                int w = p2.X - p1.X;
                int h = p2.Y - p1.Y;

                renderContext.DrawQuad(new RectangleF(p1.X + 1, p1.Y + 1, w - 1, h - 1));
                renderContext.SetState(blendStateType.NoBlend);
            }


            if (drawBorder)
            {
                renderContext.SetColorWireframe(transparentColor);

                List<Point3D> pts = null;

                if (dottedBorder)
                {
                    renderContext.SetLineStipple(1, 0x0F0F, Viewports[0].Camera);
                    renderContext.EnableLineStipple(true);
                }

                int l = p1.X;
                int r = p2.X;
                if (renderContext.IsDirect3D)
                {
                    l += 1;
                    r += 1;
                }

                pts = new List<Point3D>(new Point3D[] 
                { 
                    new Point3D(l, p1.Y), new Point3D(p2.X, p1.Y),
                    new Point3D(r, p1.Y), new Point3D(r, p2.Y),
                    new Point3D(r, p2.Y), new Point3D(l, p2.Y),
                    new Point3D(l, p2.Y), new Point3D(l, p1.Y),
                });

                renderContext.DrawLines(pts.ToArray());

                if (dottedBorder)
                    renderContext.EnableLineStipple(false);
            }
            renderContext.SetColorWireframe(originalColor);

        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        #region >-------------- Overrides

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            var mousePos = RenderContextUtility.ConvertPoint(GetMousePosition(e));
            if (OrthoMode && OrthoModeStarted)
            {
                _downMouseLocation = _moveOrthoMouseLocation;
                _moveMouseLocation = _moveOrthoMouseLocation;
            }
            else
            {
                _downMouseLocation = mousePos;
                _moveMouseLocation = _downMouseLocation;
                OrthoModeStarted = true;
            }
            base.OnMouseDown(e);
        }


        protected override void OnDragOver(DragEventArgs drgevent)
        {
            ShowDrag = true;

            
            var mousePos = drgevent.GetPosition(this);

            _moveDragMouseLocation = new Point((int)mousePos.X, (int)mousePos.Y);

            //_moveDragMouseLocation = this.PointToClient(_moveDragMouseLocation);

            base.OnDragOver(drgevent);
        }

        protected override void OnDragLeave(DragEventArgs e)
        {
            ShowDrag = false;

            ForcePaint();
            base.OnDragLeave(e);
        }

        protected override void OnDrop(DragEventArgs e)
        {
            ShowDrag = false;

            ForcePaint();
            base.OnDrop(e);
        }


        protected override void OnMouseLeave(EventArgs e)
        {
            cursorOutside = true;
            SetDefaultCursor();

            base.OnMouseLeave(e);

            Invalidate();

        }

        public new void Invalidate()
        {
            base.Invalidate();
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);

            this.ZoomFit();
            Invalidate();
            //if (_useTouchLayout)
            //{
            //    SetTouchMode();
            //}
        }

        [Obsolete]
        public void SetTouchMode(bool showRuler = true, bool showRulerCoordinates = true)
        {
            //SetupUserInterface(showRuler, showRulerCoordinates);
            //BuildToolbar();
            //base.CompileUserInterfaceElements();
            //base.ToolBar.Visible = true;
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            base.OnHandleDestroyed(e);
        }

        protected override void OnPaint()
        {
            try
            {
                if (Height <= 0 || Width <= 0) 
                    return;
                base.OnPaint();
            }
            catch (Exception ex)
            {
                
            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            cursorOutside = false;

            if (_voidCursor != null)
            {
                this.Cursor = _voidCursor;
            }

            base.OnMouseEnter(e);
            if (_activateOnEnter)
                this.Focus();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            try
            {
                _moveMouseLocation = RenderContextUtility.ConvertPoint(GetMousePosition(e));

                if (_orthoMode && _orthoModeStarted)
                {
                    //_orthoModeStarted = true;
                    int deltaX = Math.Abs(MoveMouseLocation.X - DownMouseLocation.X);
                    int deltaY = Math.Abs(MoveMouseLocation.Y - DownMouseLocation.Y);
                    if (deltaY > deltaX)
                    {
                        _moveOrthoMouseLocation = new System.Drawing.Point(DownMouseLocation.X, MoveMouseLocation.Y);
                    }
                    else
                    {
                        _moveOrthoMouseLocation = new System.Drawing.Point(MoveMouseLocation.X, DownMouseLocation.Y);
                    }
                }
                // save the current mouse position
                mouseLocation = RenderContextUtility.ConvertPoint(GetMousePosition(e));



                base.OnMouseMove(e);

                if (Dragging)
                {

                }

                PaintBackBuffer();

                SwapBuffers();
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("DWL OnMouseMove error ", ex);

                for (int i = 0; i < Entities.Count; i++)
                {
                    if (Entities[i].BoxMin == null || Entities[i].BoxMax == null)
                        TraceLog.WriteLine(string.Format("Invalid entity box. Entity index: {0}", i));
                }
            }
        }

        public void ForcePaint()
        {
            try
            {
                PaintBackBuffer();

                SwapBuffers();
            }
            catch (Exception ex)
            {
                TraceLog.WriteLine("DWL ForcePaint error ", ex);
            }
        }

        protected override void DrawOverlay(ViewportLayout.DrawSceneParams myParams)
        {
            try
            {
                
                if (_showRuler)
                    DrawRuler();

                ScreenToPlane(mouseLocation, plane, out _currentWorldPoint);
                ScreenToPlane(_moveOrthoMouseLocation, plane, out _currentOrthoPoint);


                if (Dragging && ShowDrag)
                {
                    DrawDragEntities();
                }

                if (ObjectSnapEnabled && _lastSnapPoint != null)
                {
                    DisplaySnappedVertex(_lastSnapPoint);

                }

                renderContext.SetLineSize(1);

                DrawTemporaryEntities();

                if (_drawingDimension)
                {
                    Color currentColor = renderContext.CurrentWireColor;

                    renderContext.SetColorWireframe(Color.Red);
                    switch (_dimensionMode)
                    {
                        case DimensionMode.Aligned:
                            DrawInteractiveAlignedDim();
                            break;

                        case DimensionMode.Horizontal:
                            DrawInteractiveHorizontalDim();
                            break;

                        case DimensionMode.Vertical:
                            DrawInteractiveVerticalDim();
                            break;

                    }
                    renderContext.SetColorWireframe(currentColor);
                }


                //if (_dragging)
                //{
                //    DrawDragEntities();
                //}

                //SetXOR(true);

                renderContext.SetState(depthStencilStateType.DepthTestOff);

                //if (!cursorOutside && ActionMode == actionType.None)
                if (!cursorOutside)
                {
                    if (OrthoMode && OrthoModeStarted)
                    {
                        DrawPositionMark(CurrentOrthoPoint);
                    }
                    else
                    {
                        DrawPositionMark(CurrentWorldPoint);
                    }
                }

                //SetXOR(false);


                if (!cursorOutside && ActionMode == actionType.None)
                {
                    if (DisplayMouseCoordinates && ActionMode == actionType.None && !_drawingDimension)
                    {
                        DrawMouseCoordinates();
                    }
                    if (_displayMouseTooltip && !string.IsNullOrEmpty(_mouseTooltip) && ActionMode == actionType.None)
                    {
                        DrawMouseTooltip();
                    }
                }


                base.DrawOverlay(myParams);

                if (ShowLocker && _transparentLocker != null)
                {
                    int lockerDim = (int)ActualWidth/8;
                    Size lockerSize = new Size(lockerDim, _transparentLocker.Height*lockerDim/_transparentLocker.Width);
                    using (Bitmap lockBitmap = new Bitmap(_transparentLocker, lockerSize))
                    {
                        DrawImage(0, 0, lockBitmap);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }

        }


        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (((Keyboard.Modifiers & ModifierKeys.Control)>0)  && (e.Key == Key.A || e.Key == Key.I))
            {
                return;
            }

            base.OnKeyDown(e);
        }

        private void DrawInteractiveAlignedDim()
        {
            if (_dimensionPoint2.X < _dimensionPoint1.X || _dimensionPoint2.Y < _dimensionPoint1.Y)
            {
                Point3D p0 = _dimensionPoint1;
                Point3D p1 = _dimensionPoint2;

                Utility.Swap(ref p0, ref p1);

                _dimensionPoint1 = p0;
                _dimensionPoint2 = p1;
            }

            Vector3D axisX = new Vector3D(_dimensionPoint1, _dimensionPoint2);
            Vector3D axisY = Vector3D.Cross(Vector3D.AxisZ, axisX);

            Plane drawingPlane = new Plane(_dimensionPoint1, axisX, axisY);

            Vector2D v1 = new Vector2D(_dimensionPoint1, _dimensionPoint2);
            Vector2D v2 = new Vector2D(_dimensionPoint1, _currentWorldPoint);

            double sign = Math.Sign(Vector2D.SignedAngleBetween(v1, v2));

            //offset p0-p1 at current
            Segment2D segment = new Segment2D(_dimensionPoint1, _dimensionPoint2);
            double offsetDist = _currentWorldPoint.DistanceTo(segment);
            Point3D extPt1 = _dimensionPoint1 + sign * drawingPlane.AxisY * (offsetDist + _dimensionTextHeight / 2);
            Point3D extPt2 = _dimensionPoint2 + sign * drawingPlane.AxisY * (offsetDist + _dimensionTextHeight / 2);
            Point3D dimPt1 = _dimensionPoint1 + sign * drawingPlane.AxisY * offsetDist;
            Point3D dimPt2 = _dimensionPoint2 + sign * drawingPlane.AxisY * offsetDist;

            List<Point3D> pts = new List<Point3D>();

            // Draw extension line1
            pts.Add(WorldToScreen(_dimensionPoint1));
            pts.Add(WorldToScreen(extPt1));

            // Draw extension line2
            pts.Add(WorldToScreen(_dimensionPoint2));
            pts.Add(WorldToScreen(extPt2));

            //Draw dimension line
            pts.Add(WorldToScreen(dimPt1));
            pts.Add(WorldToScreen(dimPt2));

            renderContext.DrawLines(pts.ToArray());

            //draw dimension text
            SetXOR(false);

            string dimText = "L " + extPt1.DistanceTo(extPt2).ToString("f3");
            DrawText(mouseLocation.X + 10, (int)Height - mouseLocation.Y + 10, dimText,
                new Font("Tahoma", 10f), Color.Red, ContentAlignment.BottomLeft);
        }

        private void DrawInteractiveHorizontalDim()
        {

            Vector3D axisX;


            axisX = Vector3D.AxisX;

            Point3D extPt1 = new Point3D(_dimensionPoint1.X, _currentWorldPoint.Y);
            Point3D extPt2 = new Point3D(_dimensionPoint2.X, _currentWorldPoint.Y);

            if (_currentWorldPoint.Y > _dimensionPoint1.Y && _currentWorldPoint.Y > _dimensionPoint2.Y)
            {
                extPt1.Y += _dimensionTextHeight / 2;
                extPt2.Y += _dimensionTextHeight / 2;
            }
            else
            {
                extPt1.Y -= _dimensionTextHeight / 2;
                extPt2.Y -= _dimensionTextHeight / 2;
            }


            Vector3D axisY = Vector3D.Cross(Vector3D.AxisZ, axisX);


            List<Point3D> pts = new List<Point3D>();

            // Draw extension line1
            pts.Add(WorldToScreen(_dimensionPoint1));
            pts.Add(WorldToScreen(extPt1));

            // Draw extension line2
            pts.Add(WorldToScreen(_dimensionPoint2));
            pts.Add(WorldToScreen(extPt2));

            //Draw dimension line
            Segment3D extLine1 = new Segment3D(_dimensionPoint1, extPt1);
            Segment3D extLine2 = new Segment3D(_dimensionPoint2, extPt2);
            Point3D pt1 = _currentWorldPoint.ProjectTo(extLine1);
            Point3D pt2 = _currentWorldPoint.ProjectTo(extLine2);

            pts.Add(WorldToScreen(pt1));
            pts.Add(WorldToScreen(pt2));

            renderContext.DrawLines(pts.ToArray());

            //store dimensioning plane
            Plane drawingPlane = new Plane(_dimensionPoint1, axisX, axisY);

            //draw dimension text
            SetXOR(false);

            string dimText = "L " + extPt1.DistanceTo(extPt2).ToString("f3");
            DrawText(mouseLocation.X + 10, (int)Height - mouseLocation.Y + 10, dimText,
                new Font("Tahoma", 10f), Color.Red, ContentAlignment.BottomLeft);
        }

        private void DrawInteractiveVerticalDim()
        {


            Vector3D axisX;

            axisX = Vector3D.AxisY;

            Point3D extPt1 = new Point3D(_currentWorldPoint.X, _dimensionPoint1.Y);
            Point3D extPt2 = new Point3D(_currentWorldPoint.X, _dimensionPoint2.Y);

            if (_currentWorldPoint.X > _dimensionPoint1.X && _currentWorldPoint.X > _dimensionPoint2.X)
            {
                extPt1.X += _dimensionTextHeight / 2;
                extPt2.X += _dimensionTextHeight / 2;
            }
            else
            {
                extPt1.X -= _dimensionTextHeight / 2;
                extPt2.X -= _dimensionTextHeight / 2;
            }


            Vector3D axisY = Vector3D.Cross(Vector3D.AxisZ, axisX);


            List<Point3D> pts = new List<Point3D>();

            // Draw extension line1
            pts.Add(WorldToScreen(_dimensionPoint1));
            pts.Add(WorldToScreen(extPt1));

            // Draw extension line2
            pts.Add(WorldToScreen(_dimensionPoint2));
            pts.Add(WorldToScreen(extPt2));

            //Draw dimension line
            Segment3D extLine1 = new Segment3D(_dimensionPoint1, extPt1);
            Segment3D extLine2 = new Segment3D(_dimensionPoint2, extPt2);
            Point3D pt1 = _currentWorldPoint.ProjectTo(extLine1);
            Point3D pt2 = _currentWorldPoint.ProjectTo(extLine2);

            pts.Add(WorldToScreen(pt1));
            pts.Add(WorldToScreen(pt2));

            renderContext.DrawLines(pts.ToArray());

            //store dimensioning plane
            Plane drawingPlane = new Plane(_dimensionPoint1, axisX, axisY);

            //draw dimension text
            SetXOR(false);

            string dimText = "L " + extPt1.DistanceTo(extPt2).ToString("f3");
            DrawText(mouseLocation.X + 10, (int)Height - mouseLocation.Y + 10, dimText,
                new Font("Tahoma", 10f), Color.Red, ContentAlignment.BottomLeft);
        }


        public void DrawDragEntities()
        {

                SetXOR(false);
                Vector3D tempMovement = new Vector3D(_startDraggingPoint, MoveDragWorldPosition);

                // Show temp entity for current movement state
                foreach (Entity ent in this.DragEntityList)
                {
                    if (Layers.Count > ent.LayerIndex && Layers[ent.LayerIndex].Name.ToUpperInvariant().Contains("RAW"))
                    {
                        Entity tempEntity = (Entity)ent.Clone();
                        tempEntity.Color = ent.GetColor();
                        ////Vector3D tempMovement = new Vector3D(points[0], CurrentWorldPoint);


                        tempEntity.Translate(tempMovement);

                        DrawDragEntity(tempEntity);
                    }

                }
        }


        private void DrawDragEntity(Entity tempEntity)
        {
            if (tempEntity is ICurve)
            {
                Draw(tempEntity as ICurve, tempEntity.Color, lineThickness:3F);
            }
        }


        //protected override void DrawViewport(DrawSceneParams myParams)
        //{
        //    if (_sortEntities != EntitiesSortByLayer.None)
        //    {
        //        //DateTime start = DateTime.Now;

        //        //IComparer<Entity> comparer = null;
        //        //if (_sortEntities == EntitiesSortByLayer.Ascending)
        //        //{
        //        //    comparer = new EntitySortByLayerIndex();
        //        //}
        //        //else if (_sortEntities == EntitiesSortByLayer.Descending)
        //        //{
        //        //    comparer = new EntitySortByLayerIndexDescending();
        //        //}

        //        ////Entities.Sort(comparer);

        //        //newList.AddRange(Entities);
        //        //newList.Sort(comparer);

        //        List<Entity> sortedList = null;
        //        if (_sortEntities == EntitiesSortByLayer.Ascending)
        //        {
        //            sortedList = myParams.Entities.OrderBy(e => e.LayerIndex).ToList();
        //        }
        //        else if (_sortEntities == EntitiesSortByLayer.Descending)
        //        {
        //            sortedList = myParams.Entities.OrderByDescending(e => e.LayerIndex).ToList();
        //        }

        //        EntityList newList = new EntityList();

        //        newList.AddRange(sortedList);

        //        myParams.Entities = newList;

        //        //DateTime end = DateTime.Now;
        //        //TimeSpan diff = end.Subtract(start);

        //    }

        //    base.DrawViewport(myParams);
        //}


        public void TryExportToDxf()
        {
            DialogResult dialogResult =DialogResult.Cancel;

            using (var dialog = new System.Windows.Forms.SaveFileDialog())
            {
                dialog.DefaultExt = "dxf";
                dialog.AddExtension = true;
                dialog.Filter = "Dxf files|*.dxf";
                dialog.CheckFileExists = false;
                dialog.Title = "Save DXF file";
                if (!string.IsNullOrEmpty(_lastSaveFolder) && Directory.Exists(_lastSaveFolder))
                    dialog.InitialDirectory = _lastSaveFolder;
                //dialogResult = dialog.ShowDialog(this);
                dialogResult = dialog.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    _lastSaveFolder = Path.GetDirectoryName(dialog.FileName);
                    if (SaveDxf(dialog.FileName))
                    {
                        MessageBox.Show("Operation successful.", "Save DXF file",MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Operation failed.", "Save DXF file", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }

            }
        }

        private bool SaveDxf(string filename)
        {
            bool retVal = false;
            try
            {
                var dxfWriter = new WriteAutodesk(this, filename,WriteAutodesk.versionType.Release12);

                //WriteAutodesk dxfWriter = new WriteAutodesk(this., filename, linearUnitsType.Millimeters, WriteAutodesk.versionType.Release12, false, false,lineWeightUnitsType.Millimeters);

                dxfWriter.DoWork();

                retVal = true;
            }

            catch (Exception ex)
            {
                TraceLog.WriteLine("SaveDxf error ", ex);
            }


            return retVal;
        }


        #endregion Overrides  -----------<


        public void ClearTemporaryEntities()
        {
        }


        public void CreateResizingSymbols()
        {
            
        }
        //internal void ValidateHandle()
        //{
        //    if (!IsHandleCreated)
        //        CreateHandle();
        //    Invalidate();
        //}
    }

}