﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Breton.DesignCenterInterface;
using devDept.Eyeshot.Entities;
using devDept.Geometry;

namespace BretonViewportLayout
{
    public class DragBlockInfo
    {
        private List<Entity> _dragEntities = null;

        private Point3D _offset = new Point3D();

        private object _dragSource = null;

        public DragBlockInfo(List<Entity> selection, Point3D offset, object source)
        {
            _dragEntities = selection;
            _offset = offset;
            _dragSource = source;
        }



        public List<Entity> DragEntities
        {
            get { return _dragEntities; }
            set { _dragEntities = value; }
        }

        public Point3D Offset
        {
            get { return _offset; }
            set { _offset = value; }
        }

        public object DragSource
        {
            get { return _dragSource; }
            set { _dragSource = value; }
        }
    }
}
