﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Breton.DesignCenterInterface;
using Breton.DesignCenterInterface.Wpf;
using devDept.Graphics;
using Color = System.Drawing.Color;
using Cursor = System.Windows.Input.Cursor;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using UserControl = System.Windows.Controls.UserControl;

namespace BretonViewportLayout.Wpf
{
    public enum ViewportBackgroundMode
    {
        Solid = backgroundStyleType.Solid,
        LinearGradient = backgroundStyleType.LinearGradient
    }

    public enum GraphicCursorMode
    {
        Cross,
        ViewFinder
    }
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class Breton2DViewer : UserControl
    {
        public Breton2DViewer()
        {
            InitializeComponent();

            draftingViewportLayout1.UseShaders = false;




            //if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
            //{
            //    SetStyle(ControlStyles.ResizeRedraw | ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
                LoadVoidCursor();
                LoadPanCursor();


            //}

            SetupControls();

            Loaded += Breton2DViewer_Loaded;

        }

        private void Breton2DViewer_Loaded(object sender, RoutedEventArgs e)
        {
            // Get PresentationSource
            PresentationSource presentationSource = PresentationSource.FromVisual((Visual) sender);

            // Subscribe to PresentationSource's ContentRendered event
            if (presentationSource != null)
                presentationSource.ContentRendered += TestUserControl_ContentRendered;
        }

        void TestUserControl_ContentRendered(object sender, EventArgs e)
        {
            // Don't forget to unsubscribe from the event
            ((PresentationSource)sender).ContentRendered -= TestUserControl_ContentRendered;
            Loaded -= Breton2DViewer_Loaded;

            //if (!BretonLayout.LineTypes.ContainsKey("Dash"))
            //{
            //    BretonLayout.LineTypes.Add("Dash", new float[] { 20f, -20f });

            //}

            BretonLayout.ZoomFit();
            BretonLayout.Invalidate();

        }        


        void Breton2DViewer_LayoutUpdated(object sender, EventArgs e)
        {
            if (this.ActualHeight > 0 || this.ActualWidth > 0)
            {
                LayoutUpdated -= Breton2DViewer_LayoutUpdated;

                if (!BretonLayout.LineTypes.ContainsKey("Dash"))
                {
                    BretonLayout.LineTypes.Add("Dash", new float[] { 20f, -20f });

                }

                BretonLayout.ZoomFit();
                BretonLayout.Invalidate();
            }

        }
        #region >-------------- Constants and Enums

        //private const string SERIAL_NUMBER = "EYEPRO-812M-F9J2L-A0HA1-7HWG2";

        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<



        #region >-------------- Private Fields

        private bool _showProperties = false;

        ////private Bitmap _viewerBitmap = null;

        private System.Drawing.Color _cursorColor = System.Drawing.Color.Black;

        private System.Drawing.Color _cursorCoordinatesColor = System.Drawing.Color.Black;

        private System.Drawing.Color _cursorTipsColor = System.Drawing.Color.Black;

        private System.Drawing.Color _zoomWindowColor = System.Drawing.Color.Orange;

        private bool _zoomWindowDottedBorder = false;

        private bool _zoomWindowShowBorder = true;

        private bool _zoomWindowFill = true;

        private bool _keyShortcutsEnabled = false;

        private System.Windows.Input.Cursor _voidCursor;

        private Cursor _defaultCursor;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        //public Breton2DViewer()
        //{
        //    InitializeComponent();

        //    draftingViewportLayout1.UseShaders = false;

        //    draftingViewportLayout1.Dock = DockStyle.None;



        //    if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
        //    {
        //        SetStyle(ControlStyles.ResizeRedraw | ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
        //        LoadVoidCursor();
        //        LoadPanCursor();


        //    }

        //    SetupControls();
        //    propertyGrid1.SelectedObject = null;

        //    ResizeRedraw = true;

        //}

        //protected override void InitLayout()
        //{
        //    base.InitLayout();

        //    draftingViewportLayout1.Dock = DockStyle.Fill;

        //}

        private void LoadVoidCursor()
        {
            try
            {
                _defaultCursor = draftingViewportLayout1.GetDefaultCursor();
                System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();

                using (System.IO.Stream imgStream = a.GetManifestResourceStream("Breton.DesignCenterInterface.Wpf.VoidCursor.cur"))
                {
                    _voidCursor =
                    draftingViewportLayout1.VoidCursor = new Cursor(imgStream);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void LoadPanCursor()
        {
            try
            {
                System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();

                using (System.IO.Stream imgStream = a.GetManifestResourceStream("Breton.DesignCenterInterface.Wpf.PanCursor.cur"))
                {
                    draftingViewportLayout1.PanCursor = new Cursor(imgStream);
                }
            }
            catch (Exception ex)
            {

            }
        }




        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        [Browsable(false)]
        public DraftingViewportLayout BretonLayout
        {
            get { return draftingViewportLayout1; }
        }

        public bool ShowProperties
        {
            get
            {
                return _showProperties;

            }
            set
            {
                _showProperties = value;
                SetupControls();
            }
        }


        [Category("Viewer background")]
        public Brush BackgroundTopColor
        {
            get { return BretonLayout.Viewports[0].Background.TopColor; }
            set
            {
                BretonLayout.Viewports[0].Background.TopColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer background")]
        public Brush BackgroundIntermediateColor
        {
            get { return BretonLayout.Viewports[0].Background.IntermediateColor; }
            set
            {
                BretonLayout.Viewports[0].Background.IntermediateColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer background")]
        public Brush BackgroundBottomColor
        {
            get { return BretonLayout.Viewports[0].Background.BottomColor; }
            set
            {
                BretonLayout.Viewports[0].Background.BottomColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer background")]
        public double BackgroundIntermediateColorPosition
        {
            get { return BretonLayout.Viewports[0].Background.IntermediateColorPosition; }
            set
            {
                BretonLayout.Viewports[0].Background.IntermediateColorPosition = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public Brush AxisLabelsColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelColor; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelColor = value;
                BretonLayout.UpdateViewportLayout();
            }

        }

        [Category("Viewer Axes")]
        public Brush AxisXColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.ArrowColorX; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.ArrowColorX = value;
                BretonLayout.UpdateViewportLayout();
            }
        }
        [Category("Viewer Axes")]
        public Brush AxisYColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.ArrowColorY; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.ArrowColorY = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public Brush AxisZColor
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.ArrowColorZ; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.ArrowColorZ = value;
                BretonLayout.UpdateViewportLayout();
            }
        }


        [Category("Viewer Axes")]
        public string AxisXLabel
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelAxisX; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelAxisX = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public string AxisYLabel
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelAxisY; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelAxisY = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer Axes")]
        public string AxisZLabel
        {
            get { return BretonLayout.Viewports[0].OriginSymbol.LabelAxisZ; }
            set
            {
                BretonLayout.Viewports[0].OriginSymbol.LabelAxisZ = value;
                BretonLayout.UpdateViewportLayout();
            }
        }




        [Category("Viewer background")]
        public ViewportBackgroundMode BackgroundMode
        {
            get
            {
                return (ViewportBackgroundMode)BretonLayout.Viewports[0].Background.StyleMode;
            }
            set
            {
                BretonLayout.Viewports[0].Background.StyleMode = (backgroundStyleType)value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer misc colors")]
        public System.Drawing.Color CursorColor
        {
            get { return BretonLayout.CursorColor; }
            set
            {
                BretonLayout.CursorColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer misc colors")]
        public System.Drawing.Color CursorCoordinatesColor
        {
            get { return BretonLayout.CursorCoordinatesColor; }
            set
            {
                BretonLayout.CursorCoordinatesColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Viewer misc colors")]
        public System.Drawing.Color CursorTipsColor
        {
            get { return BretonLayout.CursorTipsColor; }
            set
            {
                BretonLayout.CursorTipsColor = value;
                BretonLayout.UpdateViewportLayout();
            }
        }

        [Category("Zoom window")]
        public Color ZoomWindowColor
        {
            get { return _zoomWindowColor; }
            set
            {
                _zoomWindowColor = value;

            }
        }

        [Category("Zoom window")]
        public bool ZoomWindowShowBorder
        {
            get { return _zoomWindowShowBorder; }
            set { _zoomWindowShowBorder = value; }
        }

        [Category("Zoom window")]
        public bool ZoomWindowDottedBorder
        {
            get { return _zoomWindowDottedBorder; }
            set
            {
                _zoomWindowDottedBorder = value;

            }
        }


        [Category("Zoom window")]
        public bool ZoomWindowFill
        {
            get { return _zoomWindowFill; }
            set { _zoomWindowFill = value; }
        }

        public bool KeyShortcutsEnabled
        {
            get { return _keyShortcutsEnabled; }
            set { _keyShortcutsEnabled = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods

        public void ShowCrossCursor(bool show)
        {
            ////draftingViewportLayout1.Cursor = show ? _defaultCursor : _voidCursor;
        }

        public void Clear()
        {
            this.draftingViewportLayout1.Clear();
        }
        #endregion Public Methods -----------<

        #region >-------------- Protected Methods


        ////protected override void OnEnabledChanged(EventArgs e)
        ////{
        ////    if (!(DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime))
        ////    {
        ////        if (!Enabled)
        ////        {
        ////            _viewerBitmap = (Bitmap)SetImageOpacity(BretonLayout.RenderToBitmap(this.Size), 0.7F);
        ////        }
        ////        BretonLayout.Visible = Enabled;
        ////    }

        ////    base.OnEnabledChanged(e);
        ////}

        ////protected override void OnPaint(PaintEventArgs e)
        ////{
        ////    if (DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime)
        ////    {
        ////        base.OnPaint(e);
        ////        return;
        ////    }


        ////    if (!Enabled)
        ////    {
        ////        using (var bitmap = new Bitmap(Width, Height))
        ////        {
        ////            using (var g = Graphics.FromImage(bitmap))
        ////            {
        ////                g.Clear(Color.White);
        ////                Bitmap transparentBckg = null;

        ////                if (_viewerBitmap != null)
        ////                {
        ////                    g.DrawImage(_viewerBitmap, new Rectangle(0,0,Width, Height));
        ////                }
        ////                if (BretonViewportLayout.Properties.Resources.Lucchetto != null)
        ////                {
        ////                    transparentBckg = (Bitmap)SetImageOpacity(BretonViewportLayout.Properties.Resources.Locker, 0.8F);
        ////                    Point topLeft = new Point((Width - transparentBckg.Width) / 2, (Height - transparentBckg.Height) / 2);
        ////                    g.DrawImage(transparentBckg, topLeft);
        ////                }
        ////                if (transparentBckg != null)
        ////                    transparentBckg.Dispose();

        ////            }

        ////            e.Graphics.DrawImage(bitmap, new Point(0, 0));
        ////        }

        ////    }
        ////    else
        ////    {
        ////        base.OnPaint(e);

        ////    }

        ////}

        ////protected override void OnPaintBackground(PaintEventArgs e)
        ////{
        ////    if (DesignMode || LicenseManager.UsageMode == LicenseUsageMode.Designtime)
        ////        base.OnPaintBackground(e);
        ////}

        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void draftingViewportLayout1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            //if (!_keyShortcutsEnabled)
            //{

            //    return;
            //}

            //if (e.Alt && e.KeyCode == Keys.F7)
            //{
            //    ShowProperties = !ShowProperties;
            //}
            //if (e.Alt && e.KeyCode == Keys.F8)
            //{
            //    BretonLayout.TryExportToDxf();
            //}

            //if (e.KeyCode == Keys.F12)
            //{
            //    BretonLayout.Entities.Regen();
            //    BretonLayout.Invalidate();
            //}

        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (!_keyShortcutsEnabled)
            {
                return;
            }

            if (((Keyboard.Modifiers & ModifierKeys.Alt) > 0) && e.Key == Key.F8)
            {
                BretonLayout.TryExportToDxf();
            }

            if (e.Key == Key.F12)
            {
                BretonLayout.Entities.Regen();
                BretonLayout.Invalidate();
            }
        }




        private void SetupControls()
        {
        }

        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<

        public bool UseTouchLayout
        {
            get { return BretonLayout.UseTouchLayout; }
            set { BretonLayout.UseTouchLayout = value; }
        }

    }
}
