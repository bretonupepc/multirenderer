﻿namespace Breton.DesignCenterInterface
{
    partial class Breton2DViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Breton2DViewer));
            devDept.Graphics.LightSettings lightSettings1 = new devDept.Graphics.LightSettings(new devDept.Geometry.Vector3D(1D, 0.8D, -0.5D), System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224))))), System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(146)))), ((int)(((byte)(146))))), true, true, true, devDept.Graphics.lightType.Directional, new devDept.Geometry.Point3D(0D, 0D, 0D), 0.78539816339744828D, 0D, 1D, 0D, 0D);
            devDept.Graphics.LightSettings lightSettings2 = new devDept.Graphics.LightSettings(new devDept.Geometry.Vector3D(0D, 0D, -1D), System.Drawing.Color.White, System.Drawing.Color.Black, true, true, false, devDept.Graphics.lightType.Directional, new devDept.Geometry.Point3D(500D, 500D, 0D), 0.78539816339744828D, 0D, 1D, 0D, 0D);
            devDept.Eyeshot.DisplayModeSettingsRendered displayModeSettingsRendered1 = new devDept.Eyeshot.DisplayModeSettingsRendered(true, devDept.Eyeshot.edgeColorMethodType.EntityColor, System.Drawing.Color.Black, 1F, 2F, devDept.Eyeshot.silhouettesDrawingType.LastFrame, false, devDept.Graphics.shadowType.None, null, false, false, 0.3F, devDept.Graphics.realisticShadowQualityType.High);
            devDept.Graphics.BackgroundSettings backgroundSettings1 = new devDept.Graphics.BackgroundSettings(devDept.Graphics.backgroundStyleType.LinearGradient, System.Drawing.Color.WhiteSmoke, System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(163)))), ((int)(((byte)(210))))), 0.75D, null);
            devDept.Eyeshot.Camera camera1 = new devDept.Eyeshot.Camera(new devDept.Geometry.Point3D(-281.90778623577251D, 488.2786088096243D, 14.122014004598725D), 147.11380000000003D, new devDept.Geometry.Quaternion(0.49999999999999989D, 0.5D, 0.5D, 0.50000000000000011D), devDept.Graphics.projectionType.Orthographic, 50D, 1.2870717427981582D, false);
            devDept.Eyeshot.ZoomWindowToolBarButton zoomWindowToolBarButton1 = new devDept.Eyeshot.ZoomWindowToolBarButton("Zoom Window", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.ZoomToolBarButton zoomToolBarButton1 = new devDept.Eyeshot.ZoomToolBarButton("Zoom", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.PanToolBarButton panToolBarButton1 = new devDept.Eyeshot.PanToolBarButton("Pan", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.RotateToolBarButton rotateToolBarButton1 = new devDept.Eyeshot.RotateToolBarButton("Rotate", devDept.Eyeshot.toolBarButtonStyleType.ToggleButton, true, true);
            devDept.Eyeshot.ZoomFitToolBarButton zoomFitToolBarButton1 = new devDept.Eyeshot.ZoomFitToolBarButton("Zoom Fit", devDept.Eyeshot.toolBarButtonStyleType.PushButton, true, true);
            devDept.Eyeshot.ToolBar toolBar1 = new devDept.Eyeshot.ToolBar(devDept.Eyeshot.toolBarPositionType.HorizontalTopCenter, false, new devDept.Eyeshot.ToolBarButton[] {
            ((devDept.Eyeshot.ToolBarButton)(zoomWindowToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(zoomToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(panToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(rotateToolBarButton1)),
            ((devDept.Eyeshot.ToolBarButton)(zoomFitToolBarButton1))});
            devDept.Eyeshot.Grid grid1 = new devDept.Eyeshot.Grid(new devDept.Geometry.Point3D(-100D, -100D, 0D), new devDept.Geometry.Point3D(100D, 100D, 0D), 1D, new devDept.Geometry.Plane(new devDept.Geometry.Point3D(0D, 0D, 0D), new devDept.Geometry.Vector3D(0D, 0D, 1D)), System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))), System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32))))), System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32))))), false, false, false, false, 10, 100, 10, System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))), ((int)(((byte)(90))))), System.Drawing.Color.Transparent);
            devDept.Eyeshot.OriginSymbol originSymbol1 = new devDept.Eyeshot.OriginSymbol(5, devDept.Eyeshot.originSymbolStyleType.CoordinateSystem, new System.Drawing.Font("Tahoma", 8.25F), System.Drawing.Color.WhiteSmoke, System.Drawing.Color.Red, System.Drawing.Color.Green, System.Drawing.Color.DodgerBlue, "Origin", "X", "Y", "Z", true);
            devDept.Eyeshot.RotateSettings rotateSettings1 = new devDept.Eyeshot.RotateSettings(new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Middle, devDept.Eyeshot.modifierKeys.None), 10D, false, 1D, devDept.Eyeshot.rotationType.Trackball, devDept.Eyeshot.rotationCenterType.CursorLocation, new devDept.Geometry.Point3D(0D, 0D, 0D));
            devDept.Eyeshot.ZoomSettings zoomSettings1 = new devDept.Eyeshot.ZoomSettings(new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Middle, devDept.Eyeshot.modifierKeys.Shift), 25, true, devDept.Eyeshot.zoomStyleType.AtCursorLocation, false, 1D, System.Drawing.Color.DeepSkyBlue, devDept.Eyeshot.Camera.perspectiveFitType.Accurate, false, 10);
            devDept.Eyeshot.PanSettings panSettings1 = new devDept.Eyeshot.PanSettings(new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Middle, devDept.Eyeshot.modifierKeys.None), 25, true);
            devDept.Eyeshot.NavigationSettings navigationSettings1 = new devDept.Eyeshot.NavigationSettings(devDept.Eyeshot.Camera.navigationType.Examine, new devDept.Eyeshot.MouseButton(devDept.Eyeshot.mouseButtonsZPR.Left, devDept.Eyeshot.modifierKeys.None), new devDept.Geometry.Point3D(-1000D, -1000D, -1000D), new devDept.Geometry.Point3D(1000D, 1000D, 1000D), 8D, 50D, 50D);
            devDept.Eyeshot.CoordinateSystemIcon coordinateSystemIcon1 = new devDept.Eyeshot.CoordinateSystemIcon(new System.Drawing.Font("Tahoma", 8.25F), System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80))))), System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80))))), System.Drawing.Color.OrangeRed, "Origin", "X", "Y", "Z", false, devDept.Eyeshot.coordinateSystemPositionType.BottomLeft, 37);
            devDept.Eyeshot.ViewCubeIcon viewCubeIcon1 = new devDept.Eyeshot.ViewCubeIcon(devDept.Eyeshot.coordinateSystemPositionType.TopRight, false, System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(69)))), ((int)(((byte)(0))))), true, 300, "FRONT", "BACK", "LEFT", "RIGHT", "TOP", "BOTTOM", System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(77)))), ((int)(((byte)(77)))), ((int)(((byte)(77))))), 'S', 'N', 'W', 'E', true, null, System.Drawing.Color.White, System.Drawing.Color.Black, 120, true, true, null, null, null, null, null, null);
            devDept.Eyeshot.Viewport.SavedViewsManager savedViewsManager1 = new devDept.Eyeshot.Viewport.SavedViewsManager(1);
            devDept.Eyeshot.Viewport viewport1 = new devDept.Eyeshot.Viewport(new System.Drawing.Point(0, 0), new System.Drawing.Size(383, 395), backgroundSettings1, camera1, toolBar1, new devDept.Eyeshot.Legend[0], devDept.Eyeshot.displayType.Rendered, true, false, false, false, new devDept.Eyeshot.Grid[] {
            grid1}, originSymbol1, false, rotateSettings1, zoomSettings1, panSettings1, navigationSettings1, coordinateSystemIcon1, viewCubeIcon1, savedViewsManager1, devDept.Eyeshot.viewType.Top);
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.picLock = new System.Windows.Forms.PictureBox();
            this.pnlProperties = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.draftingViewportLayout1 = new Breton.DesignCenterInterface.DraftingViewportLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).BeginInit();
            this.pnlProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.draftingViewportLayout1)).BeginInit();
            this.SuspendLayout();
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 24);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(250, 371);
            this.propertyGrid1.TabIndex = 1;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(383, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 395);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // picLock
            // 
            this.picLock.BackColor = System.Drawing.Color.Transparent;
            this.picLock.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picLock.Location = new System.Drawing.Point(135, 39);
            this.picLock.Name = "picLock";
            this.picLock.Size = new System.Drawing.Size(48, 48);
            this.picLock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picLock.TabIndex = 3;
            this.picLock.TabStop = false;
            this.picLock.Visible = false;
            // 
            // pnlProperties
            // 
            this.pnlProperties.BackColor = System.Drawing.Color.LightSlateGray;
            this.pnlProperties.Controls.Add(this.propertyGrid1);
            this.pnlProperties.Controls.Add(this.label1);
            this.pnlProperties.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlProperties.Location = new System.Drawing.Point(386, 0);
            this.pnlProperties.Name = "pnlProperties";
            this.pnlProperties.Size = new System.Drawing.Size(250, 395);
            this.pnlProperties.TabIndex = 4;
            this.pnlProperties.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(250, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Drafting layout properties";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // draftingViewportLayout1
            // 
            this.draftingViewportLayout1.ActivateOnEnter = true;
            this.draftingViewportLayout1.ActiveColor = System.Drawing.Color.Blue;
            this.draftingViewportLayout1.ActiveLayer = ((devDept.Eyeshot.Layer)(resources.GetObject("draftingViewportLayout1.ActiveLayer")));
            this.draftingViewportLayout1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.draftingViewportLayout1.AskForFsaa = true;
            this.draftingViewportLayout1.AxisMode = Breton.DesignCenterInterface.AxisOrientation.LeftBottom;
            this.draftingViewportLayout1.Cursor = System.Windows.Forms.Cursors.Default;
            this.draftingViewportLayout1.CursorColor = System.Drawing.Color.Black;
            this.draftingViewportLayout1.CursorCoordinatesColor = System.Drawing.Color.Black;
            this.draftingViewportLayout1.CursorCrossSize = 30;
            this.draftingViewportLayout1.CursorMode = Breton.DesignCenterInterface.GraphicCursorMode.ViewFinder;
            this.draftingViewportLayout1.CursorTipsColor = System.Drawing.Color.Black;
            this.draftingViewportLayout1.DefaultActionType = devDept.Eyeshot.actionType.None;
            this.draftingViewportLayout1.DimensionMode = Breton.DesignCenterInterface.DimensionMode.Aligned;
            this.draftingViewportLayout1.DimensionPoint1 = null;
            this.draftingViewportLayout1.DimensionPoint2 = null;
            this.draftingViewportLayout1.DimensionTextHeight = 40D;
            this.draftingViewportLayout1.DisplayMouseCoordinates = false;
            this.draftingViewportLayout1.DisplayMouseTooltip = false;
            this.draftingViewportLayout1.DragEntityList = null;
            this.draftingViewportLayout1.Dragging = false;
            this.draftingViewportLayout1.DrawingDimension = false;
            this.draftingViewportLayout1.ForeColor = System.Drawing.Color.White;
            this.draftingViewportLayout1.GridSnapEnabled = false;
            lightSettings1.LinearAttenuation = 0D;
            this.draftingViewportLayout1.Light1 = lightSettings1;
            lightSettings2.LinearAttenuation = 0D;
            this.draftingViewportLayout1.Light4 = lightSettings2;
            this.draftingViewportLayout1.Location = new System.Drawing.Point(0, 0);
            this.draftingViewportLayout1.MeasureUnit = Breton.DesignCenterInterface.UM.Millimeters;
            this.draftingViewportLayout1.MouseTooltip = "";
            this.draftingViewportLayout1.MoveDragMouseLocation = new System.Drawing.Point(0, 0);
            this.draftingViewportLayout1.Name = "draftingViewportLayout1";
            this.draftingViewportLayout1.ObjectSnapEnabled = false;
            this.draftingViewportLayout1.OrthoMode = false;
            this.draftingViewportLayout1.OrthoModeStarted = false;
            this.draftingViewportLayout1.PanCursor = null;
            this.draftingViewportLayout1.Rendered = displayModeSettingsRendered1;
            this.draftingViewportLayout1.ShowDrag = false;
            this.draftingViewportLayout1.ShowLocker = false;
            this.draftingViewportLayout1.ShowRulerCoordinates = false;
            this.draftingViewportLayout1.Size = new System.Drawing.Size(383, 395);
            this.draftingViewportLayout1.SnapBoxSize = 20;
            this.draftingViewportLayout1.SnapLayers = ((System.Collections.Generic.List<string>)(resources.GetObject("draftingViewportLayout1.SnapLayers")));
            this.draftingViewportLayout1.SnapModes = ((System.Collections.Generic.List<Breton.DesignCenterInterface.ObjectSnapType>)(resources.GetObject("draftingViewportLayout1.SnapModes")));
            this.draftingViewportLayout1.SnapSymbolColor = System.Drawing.Color.Red;
            this.draftingViewportLayout1.SortEntities = BretonViewportLayout.EntitiesSortByLayer.None;
            this.draftingViewportLayout1.StartDraggingPoint = null;
            this.draftingViewportLayout1.TabIndex = 0;
            this.draftingViewportLayout1.UseTouchLayout = false;
            viewport1.CoordinateSystemIcon = coordinateSystemIcon1;
            viewport1.ViewCubeIcon = viewCubeIcon1;
            this.draftingViewportLayout1.Viewports.Add(viewport1);
            this.draftingViewportLayout1.VoidCursor = null;
            this.draftingViewportLayout1.WaitingForSelection = false;
            // 
            // Breton2DViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.draftingViewportLayout1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlProperties);
            this.Controls.Add(this.picLock);
            this.Name = "Breton2DViewer";
            this.Size = new System.Drawing.Size(636, 395);
            ((System.ComponentModel.ISupportInitialize)(this.picLock)).EndInit();
            this.pnlProperties.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.draftingViewportLayout1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DraftingViewportLayout draftingViewportLayout1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.PictureBox picLock;
        private System.Windows.Forms.Panel pnlProperties;
        private System.Windows.Forms.Label label1;
    }
}
