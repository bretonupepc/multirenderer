﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using BretonViewportLayout.Common;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using devDept.Graphics;
using OpenGL;
using Region = devDept.Eyeshot.Entities.Region;

namespace BretonViewportLayout
{
    public class ZTopRegion: ColorRegion
    {
        private float _level = -1F;


        public ZTopRegion(CompositeCurve curve, Plane plane, float level)
            : base(curve, plane)
        {
            _level = - level * 3;
        }

        //protected override void Draw(DrawParams data)
        //{
        //    gl.PushAttrib(gl.ENABLE_BIT | gl.POLYGON_BIT);
        //    gl.Enable(gl.POLYGON_OFFSET_FILL);
        //    gl.PolygonOffset(_level, -2);

        //    base.Draw(data);

        //    gl.PopAttrib();
        //}

        //protected override void Render(RenderParams data)
        //{
        //    gl.PushAttrib(gl.ENABLE_BIT | gl.POLYGON_BIT);
        //    gl.Enable(gl.POLYGON_OFFSET_FILL);
        //    gl.PolygonOffset(_level, -2);

        //    base.Render(data);

        //    gl.PopAttrib();
        //}


        //protected override void Draw(DrawParams data)
        //{
        //    data.RenderContext.PushRasterizerState();
        //    data.RenderContext.SetState(rasterizerStateType.CCW_PolygonFill_NoCullFace_PolygonOffset_Minus3Minus2);

        //    base.Draw(data);

        //    data.RenderContext.PopRasterizerState();
        //}


        //protected override void Render(RenderParams data)
        //{
        //    data.RenderContext.PushRasterizerState();
        //    data.RenderContext.SetState(rasterizerStateType.CCW_PolygonFill_NoCullFace_PolygonOffset_Minus3Minus2);

        //    base.Render(data);

        //    data.RenderContext.PopRasterizerState();
        //}

        public float Level
        {
            get { return _level; }
            set { _level = value; }
        }
    }
}
