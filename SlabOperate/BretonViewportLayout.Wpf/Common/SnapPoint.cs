﻿using Breton.DesignCenterInterface;
using Breton.DesignCenterInterface.Wpf;
using devDept.Geometry;

namespace BretonViewportLayout
{
    public class SnapPoint : Point3D
    {
        public ObjectSnapType Type;

        public SnapPoint()
            : base()
        {
            Type = ObjectSnapType.End;
        }

        public SnapPoint(Point3D point3D, ObjectSnapType objectSnapType)
            : base(point3D.X, point3D.Y, point3D.Z)
        {
            Type = objectSnapType;
        }

        public override string ToString()
        {
            return base.ToString() + " | " + Type;
        }
    }
}
