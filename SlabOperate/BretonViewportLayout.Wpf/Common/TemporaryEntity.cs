﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using devDept.Geometry;

namespace BretonViewportLayout
{
    public enum TemporaryEntityType
    {
        Line,
        Rectangle,
        Circle,
        Arc
    }

    public class TemporaryEntity
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private TemporaryEntityType _entityType;

        private Point2D _startPoint;

        private Point2D _endPoint;

        private Point2D _arcCenter;

        private double _radius;

        private bool _fill;

        private bool _drawBorder = true;

        private bool _dottedBorder = false;

        private bool _drawEntity = false;

        private Color _drawColor = Color.White;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public TemporaryEntity()
        {

        }

        public TemporaryEntity(TemporaryEntityType entityType, Point2D startPoint, Point2D endPoint)
        {
            _entityType = entityType;
            _startPoint = startPoint;
            _endPoint = endPoint;
        }

        public TemporaryEntity(TemporaryEntityType entityType, Point2D center, double radius)
        {
            _entityType = entityType;
            _startPoint = center;
            _radius = radius;
        }

        public TemporaryEntity(TemporaryEntityType entityType, Point2D arcCenter, Point2D startPoint, Point2D endPoint)
        {
            _entityType = entityType;
            _startPoint = startPoint;
            _endPoint = endPoint;
            _arcCenter = arcCenter;
        }
    #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public TemporaryEntityType EntityType
        {
            get { return _entityType; }
            set { _entityType = value; }
        }

        public Point2D StartPoint
        {
            get { return _startPoint; }
            set { _startPoint = value; }
        }

        public Point2D EndPoint
        {
            get { return _endPoint; }
            set { _endPoint = value; }
        }

        public Point2D OriginPoint
        {
            get { return (_startPoint + _endPoint)/2; }
        }


        public double Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }

        public bool Fill
        {
            get { return _fill; }
            set { _fill = value; }
        }

        public bool DrawBorder
        {
            get { return _drawBorder; }
            set { _drawBorder = value; }
        }

        public bool DottedBorder
        {
            get { return _dottedBorder; }
            set { _dottedBorder = value; }
        }

        public bool DrawEntity
        {
            get { return _drawEntity; }
            set { _drawEntity = value; }
        }

        public Color DrawColor
        {
            get { return _drawColor; }
            set { _drawColor = value; }
        }

        public Point2D ArcCenter
        {
            get { return _arcCenter; }
            set { _arcCenter = value; }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<
    }
}
