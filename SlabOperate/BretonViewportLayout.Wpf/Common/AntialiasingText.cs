﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;

namespace BretonViewportLayout.Common
{
    public class AntialiasingText:Text
    {
        public AntialiasingText(devDept.Geometry.Point3D insPoint, string textString, double height)
            : base(insPoint, textString, height)
        {
            
        }
        protected override void Draw(DrawParams data)
        {
            data.RenderContext.EnableMultisample(true);
            base.Draw(data);
            data.RenderContext.EnableMultisample(false);
        }

    }

    public class AntialiasingMultilineText : MultilineText
    {
        public AntialiasingMultilineText(devDept.Geometry.Point3D insPoint, string textString, double width, double height, double lineSpaceDistance)
            : base(insPoint, textString, width, height, lineSpaceDistance)
        {

        }
        protected override void Draw(DrawParams data)
        {
            data.RenderContext.EnableMultisample(true);
            base.Draw(data);
            data.RenderContext.EnableMultisample(false);
        }

    }

}
