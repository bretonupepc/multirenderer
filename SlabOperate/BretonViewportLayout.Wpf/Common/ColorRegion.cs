﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using devDept.Graphics;
using Region = devDept.Eyeshot.Entities.Region;

namespace BretonViewportLayout.Common
{
    public class ColorRegion: Region
    {
        private static Color REGION_HILIGHT_COLOR = System.Drawing.Color.FromArgb(100, System.Drawing.Color.Fuchsia);

        private static bool _hilightOnlyBorder = false;


        private readonly Color HIGHLIGHT_COLOR = System.Drawing.Color.FromArgb(100,System.Drawing.Color.Fuchsia);

        public ushort Stipple = 0xF0F0;

        private Color _baseColor = System.Drawing.Color.Empty;

        private double _regenTolerance = 1e-2;

        private bool _isHilighted = false;


        public ColorRegion(CompositeCurve curve, Plane plane)
            : base(curve, plane)
        {
        }

        public ColorRegion(Region region) : base(region)
        {
        }

        public static Color RegionHilightColor
        {
            get { return REGION_HILIGHT_COLOR; }
            set
            {
                //REGION_HILIGHT_COLOR = value;
                REGION_HILIGHT_COLOR = System.Drawing.Color.FromArgb(100, value);
            }
        }

        public static bool HilightOnlyBorder
        {
            get { return _hilightOnlyBorder; }
            set { _hilightOnlyBorder = value; }
        }


        public void HighLight(bool onOff)
        {
            if (_baseColor == Color.Empty)
            {
                _baseColor = this.Color;
            }
            if (onOff)
            {
                
                //this.Color = HighLightedColor(_baseColor, 50);
                if (!HilightOnlyBorder)
                    this.Color = RegionHilightColor;
            }
            else
            {
                this.Color = _baseColor;
            }
            _isHilighted = onOff;
        }

        private Color HighLightedColor(Color c, int highlightfactor)
        {
            return Color.FromArgb(100, c.R + highlightfactor > 255 ? 255 : c.R + highlightfactor,
                                  c.G + highlightfactor > 255 ? 255 : c.G + highlightfactor,
                                  c.B + highlightfactor > 255 ? 255 : c.B + highlightfactor
              );
        }

        //public override void Regen(RegenParams data)
        //{
        //    base.Regen(data);

        //    _regenTolerance = data.ChordalErr; //I want to use the same tolerance for the edges.
        //}


        protected override void DrawEdges(DrawParams data)
        {
            data.RenderContext.EndDrawBufferedLines();

            float currentLineWidth = data.RenderContext.CurrentLineWidth;

            if (this.LineWeightMethod == colorMethodType.byEntity)
            {
                data.RenderContext.SetLineSize(this.LineWeight, true, false);
            }

            if (this.LineTypeMethod == colorMethodType.byEntity /*&& this.LineTypePattern != null*/)
            {
                data.RenderContext.SetLineStipple(2, Stipple, data.Viewport.Camera);
                data.RenderContext.EnableLineStipple(true);

            }

            var currentColor = data.RenderContext.CurrentWireColor;
            if (_isHilighted)
            {
                data.RenderContext.SetColorWireframe(System.Drawing.Color.FromArgb(255,RegionHilightColor));
            }

            //data.RenderContext.SetLineSize(10F, true);

            //base.DrawEdges(data);
            foreach (var curve in ContourList)
            {
                Entity ent = (Entity)curve;
                if (ent.Vertices == null)
                    ent.Regen(_regenTolerance);

                data.RenderContext.DrawLineStrip(ent.Vertices);
                //data.RenderContext.DrawLine(ent.Vertices[0], ent.Vertices[2]);
            }

            data.RenderContext.EndDrawBufferedLines();

            data.RenderContext.EnableLineStipple(false);
            data.RenderContext.SetLineSize(currentLineWidth, true);

            data.RenderContext.SetColorWireframe(currentColor);

        }

        #region Obsolete

        [Obsolete]
        private void DrawContour(DrawParams data)
        {
            Transformation scale = new devDept.Geometry.Scaling(data.ScreenToWorld);


            Point3D origin;
            data.Viewport.ScreenToPlane(new System.Drawing.Point(0, data.Viewport.Size.Height), Plane.XY, out origin);
            Transformation translate = new Translation(origin.X / data.ScreenToWorld, origin.Y / data.ScreenToWorld, 0);
            Transformation transformation = scale * translate;

            foreach (ICurve curve in this.ContourList)
            {
                Draw(curve, data, transformation);
            }
        }


        [Obsolete]
        private void Draw(ICurve theCurve, DrawParams data, Transformation transformation)
        {
            if (theCurve is CompositeCurve)
            {
                CompositeCurve compositeCurve = theCurve as CompositeCurve;
                Entity[] explodedCurves = compositeCurve.Explode();
                foreach (Entity ent in explodedCurves)

                    DrawScreenCurve((ICurve)ent, data, transformation);
            }
            else
            {
                DrawScreenCurve(theCurve, data, transformation);
            }
        }

        [Obsolete]
        private void DrawScreenCurve(ICurve curve, DrawParams data, Transformation transformation)
        {
            const int subd = 100;

            Point3D[] pts = new Point3D[subd + 1];

            for (int i = 0; i <= subd; i++)
            {
                pts[i] = transformation * data.Viewport.WorldToScreen(curve.PointAt(curve.Domain.ParameterAt((double)i / subd)));
            }

            data.RenderContext.DrawLineStrip(pts);
        }

        #endregion
    }
}
