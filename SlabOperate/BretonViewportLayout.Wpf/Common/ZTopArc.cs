﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using devDept.Eyeshot;
using devDept.Eyeshot.Entities;
using devDept.Geometry;
using devDept.Graphics;

namespace BretonViewportLayout
{
    public class ZTopArc: Arc
    {

        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields



        #endregion Private Fields --------<

        #region >-------------- Constructors

        public ZTopArc(Arc another) : base(another)
        {}

        public ZTopArc(ZTopArc another)
            : base(another)
        {}

        public ZTopArc(Plane arcPlane, Point2D center, Point2D start, Point2D end)
            : base(arcPlane, center, start, end)
        { }
        public ZTopArc(Plane arcPlane, Point2D center, double radius, double startAngleInRadians, double endAngleInRadians)
            : base(arcPlane, center, radius, startAngleInRadians, endAngleInRadians)
        { }
        public ZTopArc(Plane arcPlane, Point2D first, Point2D second, Point2D third, bool flip)
            : base(arcPlane, first, second, third, flip)
        { }
        public ZTopArc(Plane arcPlane, Point3D center, double radius, Point3D start, Point3D end, bool flip)
            : base(arcPlane, center, radius, start, end, flip)
        { }
        public ZTopArc(Plane arcPlane, Point3D center, double radius, double angleInRadians)
            : base(arcPlane, center, radius, angleInRadians)
        { }


        public ZTopArc(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }

        public ZTopArc(Point3D first, Point3D second, Point3D third, bool flip):
            base(first, second, third, flip){}

        public ZTopArc(Point3D center, Point3D start, Point3D end)
            : base(center, start, end)
        { }

        public ZTopArc(Plane arcPlane, Point3D center, double radius, double startAngleInRadians, double endAngleInRadians):
        base(arcPlane, center, radius, startAngleInRadians, endAngleInRadians){}

        public ZTopArc(double x, double y, double z, double radius, double startAngleInRadians, double endAngleInRadians):
        base(x, y, z, radius, startAngleInRadians, endAngleInRadians){}

        public ZTopArc(Point3D center, double radius, double startAngleInRadians, double endAngleInRadians) :
        base(center,radius, startAngleInRadians, endAngleInRadians){}


        public ZTopArc(Point3D center, double radius, double angleInRadians):
            base(center, radius, angleInRadians){}



        #endregion Constructors  -----------<

        #region >-------------- Overrides

        //protected override void Draw(DrawParams data)
        //{
        //    data.RenderContext.PushRasterizerState();
        //    data.RenderContext.SetState(rasterizerStateType.CCW_PolygonLine_CullFace_Front);

        //    base.Draw(data);

        //    data.RenderContext.PopRasterizerState();
        //}



        #endregion Overrides --------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties



        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods



        #endregion Private Methods ----------<

        #region >-------------- Delegates



        #endregion Delegates  -----------<




    }
}
