﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using TraceLoggers;

namespace TestEntities
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            CleanBusinessTempFiles();

            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            CleanBusinessTempFiles();
        }

        private void CleanBusinessTempFiles()
        {
            string tempFolder = System.IO.Path.GetTempPath();

            tempFolder = Path.Combine(tempFolder, "DataAccess.Objects.Main" );

            if (Directory.Exists(tempFolder))
            {
                try
                {
                    Directory.Delete(tempFolder, true);
                }

                catch (Exception ex)
                {
                    //TraceLog.WriteLine("CurrentSession.CleanTempFiles error: ", ex);
                }

                try
                {
                    if (Directory.Exists(tempFolder))
                    {
                        var filesList = Directory.GetFiles(tempFolder, "*.*", SearchOption.AllDirectories);
                        foreach (var file in filesList)
                        {
                            try
                            {
                                File.Delete(file);
                            }
                            catch (Exception ex)
                            {
                                //TraceLog.WriteLine("CurrentSession.CleanTempFiles error: ", ex);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    //TraceLog.WriteLine("CurrentSession.CleanTempFiles error: ", ex);
                }

            }
        }
    }
}
