﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataAccess.Business.BusinessBase;
using DataAccess.Business.Persistence;
using DataAccess.Persistence.SqlServer;
using SlabOperate.Business.Entities;
using TraceLoggers;
using Path = System.IO.Path;

namespace TestEntities
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region >-------------- Constants and Enums



        #endregion Constants and Enums -----------<

        #region >-------------- Events declaration



        #endregion Events declaration  -----------<

        #region >-------------- Private Fields

        private PersistableEntityCollection<SlabEntity> _slabs;

        private PersistenceScope _dataScope;

        #endregion Private Fields --------<

        #region >-------------- Constructors

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            Loaded += MainWindow_Loaded;

        }

        #endregion Constructors  -----------<


        #region >-------------- Protected Fields



        #endregion Protected Fields --------<

        #region >-------------- Public Fields



        #endregion Public Fields --------<


        #region >-------------- Public Properties

        public PersistableEntityCollection<SlabEntity> Slabs
        {
            get { return _slabs; }
            set
            {
                _slabs = value;
                OnPropertyChanged("Slabs");
            }
        }

        #endregion Public Properties -----------<

        #region >-------------- Protected Properties



        #endregion Protected Properties --------<

        #region >-------------- Public Methods



        #endregion Public Methods -----------<

        #region >-------------- Protected Methods



        #endregion Protected Methods --------<

        #region >-------------- Private Methods

        private void SetDataScope()
        {
            _dataScope = new PersistenceScope("Main");
            var sqlProvider = _dataScope.Manager.AvailableProviders.FirstOrDefault(p => p.ProviderType == PersistenceProviderType.SqlServer);
            if (sqlProvider as SqlServerProvider != null)
            {
                (sqlProvider as SqlServerProvider).UdlFilename ="DbConnection.udl";
            }

        }

        private void GetSlabs()
        {
            var slabs = new PersistableEntityCollection<SlabEntity>(_dataScope);

            //Filtro per materiale con codice specifico
            var filterClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Material.Code"),
                ConditionOperator.Eq, "MAT01000013");
            //var filterClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Material.Code"),
            //    ConditionOperator.Like, "%");

            // aggiungo condizione per spessore compreso tra 10 e 40

            var secondClause = new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"),
                ConditionOperator.Ge, 10);
            secondClause.AddClause(LogicalConnector.And,
                new FilterClause(new FieldItemBaseInfo("ThicknessObject.Thickness"),
                    ConditionOperator.Le, 40));

            filterClause.AddClause(LogicalConnector.And, secondClause);

            if (slabs.GetItemsFromRepository(clause: filterClause, suppressNotifications: true))
            {
                Slabs = slabs;
            }


        }



        #endregion Private Methods ----------<

        #region >-------------- Delegates


        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            SetDataScope();
            GetSlabs();

            var parameters = new PersistableEntityCollection<ParameterEntity>(_dataScope);
            parameters.GetItemsFromRepository();
        }

        #endregion Delegates  -----------<

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
